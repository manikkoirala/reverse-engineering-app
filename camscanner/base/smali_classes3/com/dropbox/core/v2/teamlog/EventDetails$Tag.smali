.class public final enum Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;
.super Ljava/lang/Enum;
.source "EventDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/core/v2/teamlog/EventDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Tag"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum ACCOUNT_CAPTURE_CHANGE_AVAILABILITY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum ACCOUNT_CAPTURE_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum ACCOUNT_CAPTURE_MIGRATE_ACCOUNT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum ACCOUNT_CAPTURE_NOTIFICATION_EMAILS_SENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum ACCOUNT_CAPTURE_RELINQUISH_ACCOUNT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum ACCOUNT_LOCK_OR_UNLOCKED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum ALLOW_DOWNLOAD_DISABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum ALLOW_DOWNLOAD_ENABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum APP_LINK_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum APP_LINK_USER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum APP_UNLINK_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum APP_UNLINK_USER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum BINDER_ADD_PAGE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum BINDER_ADD_SECTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum BINDER_REMOVE_PAGE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum BINDER_REMOVE_SECTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum BINDER_RENAME_PAGE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum BINDER_RENAME_SECTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum BINDER_REORDER_PAGE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum BINDER_REORDER_SECTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum CAMERA_UPLOADS_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum CHANGED_ENTERPRISE_ADMIN_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum CHANGED_ENTERPRISE_CONNECTED_TEAM_STATUS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum COLLECTION_SHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum CREATE_FOLDER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum CREATE_TEAM_INVITE_LINK_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DATA_PLACEMENT_RESTRICTION_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DATA_PLACEMENT_RESTRICTION_SATISFY_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DELETE_TEAM_INVITE_LINK_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_APPROVALS_ADD_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_APPROVALS_CHANGE_DESKTOP_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_APPROVALS_CHANGE_MOBILE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_APPROVALS_CHANGE_OVERAGE_ACTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_APPROVALS_CHANGE_UNLINK_ACTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_APPROVALS_REMOVE_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_CHANGE_IP_DESKTOP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_CHANGE_IP_MOBILE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_CHANGE_IP_WEB_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_DELETE_ON_UNLINK_FAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_DELETE_ON_UNLINK_SUCCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_LINK_FAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_LINK_SUCCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_MANAGEMENT_DISABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_MANAGEMENT_ENABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_UNLINK_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DIRECTORY_RESTRICTIONS_ADD_MEMBERS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DIRECTORY_RESTRICTIONS_REMOVE_MEMBERS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DISABLED_DOMAIN_INVITES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DOMAIN_INVITES_APPROVE_REQUEST_TO_JOIN_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DOMAIN_INVITES_DECLINE_REQUEST_TO_JOIN_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DOMAIN_INVITES_EMAIL_EXISTING_USERS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DOMAIN_INVITES_REQUEST_TO_JOIN_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_NO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_YES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DOMAIN_VERIFICATION_ADD_DOMAIN_FAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DOMAIN_VERIFICATION_ADD_DOMAIN_SUCCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DOMAIN_VERIFICATION_REMOVE_DOMAIN_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum EMM_ADD_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum EMM_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum EMM_CREATE_EXCEPTIONS_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum EMM_CREATE_USAGE_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum EMM_ERROR_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum EMM_REFRESH_AUTH_TOKEN_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum EMM_REMOVE_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum ENABLED_DOMAIN_INVITES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum ENDED_ENTERPRISE_ADMIN_SESSION_DEPRECATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum ENDED_ENTERPRISE_ADMIN_SESSION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum ENTERPRISE_SETTINGS_LOCKING_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum EXPORT_MEMBERS_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum EXPORT_MEMBERS_REPORT_FAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum EXTENDED_VERSION_HISTORY_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_ADD_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_ADD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_CHANGE_COMMENT_SUBSCRIPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_COMMENTS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_COPY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_DELETE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_EDIT_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_EDIT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_GET_COPY_REFERENCE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_LIKE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_LOCKING_LOCK_STATUS_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_LOCKING_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_MOVE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_PERMANENTLY_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_PREVIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_RENAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_REQUESTS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_REQUESTS_EMAILS_ENABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_REQUESTS_EMAILS_RESTRICTED_TO_TEAM_ONLY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_REQUEST_CHANGE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_REQUEST_CLOSE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_REQUEST_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_REQUEST_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_REQUEST_RECEIVE_FILE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_RESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_RESTORE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_REVERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_ROLLBACK_CHANGES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_SAVE_COPY_REFERENCE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_TRANSFERS_FILE_ADD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_TRANSFERS_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_TRANSFERS_TRANSFER_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_TRANSFERS_TRANSFER_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_TRANSFERS_TRANSFER_SEND_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_TRANSFERS_TRANSFER_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_UNLIKE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_UNRESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FOLDER_OVERVIEW_DESCRIPTION_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FOLDER_OVERVIEW_ITEM_PINNED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FOLDER_OVERVIEW_ITEM_UNPINNED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GOOGLE_SSO_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_ADD_EXTERNAL_ID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_ADD_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_CHANGE_EXTERNAL_ID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_CHANGE_MANAGEMENT_TYPE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_CHANGE_MEMBER_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_DESCRIPTION_UPDATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_JOIN_POLICY_UPDATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_MOVED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_REMOVE_EXTERNAL_ID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_REMOVE_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_RENAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_USER_MANAGEMENT_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GUEST_ADMIN_CHANGE_STATUS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GUEST_ADMIN_SIGNED_IN_VIA_TRUSTED_TEAMS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GUEST_ADMIN_SIGNED_OUT_VIA_TRUSTED_TEAMS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum INTEGRATION_CONNECTED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum INTEGRATION_DISCONNECTED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum INTEGRATION_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum LEGAL_HOLDS_ACTIVATE_A_HOLD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum LEGAL_HOLDS_ADD_MEMBERS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum LEGAL_HOLDS_CHANGE_HOLD_DETAILS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum LEGAL_HOLDS_CHANGE_HOLD_NAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum LEGAL_HOLDS_EXPORT_A_HOLD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum LEGAL_HOLDS_EXPORT_CANCELLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum LEGAL_HOLDS_EXPORT_DOWNLOADED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum LEGAL_HOLDS_EXPORT_REMOVED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum LEGAL_HOLDS_RELEASE_A_HOLD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum LEGAL_HOLDS_REMOVE_MEMBERS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum LEGAL_HOLDS_REPORT_A_HOLD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum LOGIN_FAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum LOGIN_SUCCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum LOGOUT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_ADD_EXTERNAL_ID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_ADD_NAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_CHANGE_ADMIN_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_CHANGE_EMAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_CHANGE_EXTERNAL_ID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_CHANGE_MEMBERSHIP_TYPE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_CHANGE_NAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_CHANGE_STATUS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_DELETE_MANUAL_CONTACTS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_DELETE_PROFILE_PHOTO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_PERMANENTLY_DELETE_ACCOUNT_CONTENTS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_REMOVE_EXTERNAL_ID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_REQUESTS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_SEND_INVITE_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_SET_PROFILE_PHOTO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_SPACE_LIMITS_ADD_CUSTOM_QUOTA_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_SPACE_LIMITS_ADD_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_SPACE_LIMITS_CHANGE_CAPS_TYPE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_SPACE_LIMITS_CHANGE_CUSTOM_QUOTA_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_SPACE_LIMITS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_SPACE_LIMITS_CHANGE_STATUS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_SPACE_LIMITS_REMOVE_CUSTOM_QUOTA_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_SPACE_LIMITS_REMOVE_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_SUGGESTIONS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_SUGGEST_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_TRANSFER_ACCOUNT_CONTENTS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MICROSOFT_OFFICE_ADDIN_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MISSING_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum NETWORK_CONTROL_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum NOTE_ACL_INVITE_ONLY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum NOTE_ACL_LINK_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum NOTE_ACL_TEAM_LINK_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum NOTE_SHARED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum NOTE_SHARE_RECEIVE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum NO_EXPIRATION_LINK_GEN_CREATE_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum NO_EXPIRATION_LINK_GEN_REPORT_FAILED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum NO_PASSWORD_LINK_GEN_CREATE_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum NO_PASSWORD_LINK_GEN_REPORT_FAILED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum NO_PASSWORD_LINK_VIEW_CREATE_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum NO_PASSWORD_LINK_VIEW_REPORT_FAILED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum OPEN_NOTE_SHARED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum OTHER:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum OUTDATED_LINK_VIEW_CREATE_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum OUTDATED_LINK_VIEW_REPORT_FAILED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_ADMIN_EXPORT_START_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CHANGE_DEPLOYMENT_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CHANGE_MEMBER_LINK_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CHANGE_MEMBER_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CONTENT_ADD_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CONTENT_ADD_TO_FOLDER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CONTENT_ARCHIVE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CONTENT_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CONTENT_PERMANENTLY_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CONTENT_REMOVE_FROM_FOLDER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CONTENT_REMOVE_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CONTENT_RENAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CONTENT_RESTORE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DEFAULT_FOLDER_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DESKTOP_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_ADD_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_CHANGE_MEMBER_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_CHANGE_SHARING_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_CHANGE_SUBSCRIPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_DELETED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_DELETE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_EDIT_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_EDIT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_FOLLOWED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_MENTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_OWNERSHIP_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_REQUEST_ACCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_RESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_REVERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_SLACK_SHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_TEAM_INVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_TRASHED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_UNRESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_UNTRASHED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_ENABLED_USERS_GROUP_ADDITION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_ENABLED_USERS_GROUP_REMOVAL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_EXTERNAL_VIEW_ALLOW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_EXTERNAL_VIEW_DEFAULT_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_EXTERNAL_VIEW_FORBID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_FOLDER_CHANGE_SUBSCRIPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_FOLDER_DELETED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_FOLDER_FOLLOWED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_FOLDER_TEAM_INVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_PUBLISHED_LINK_CHANGE_PERMISSION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_PUBLISHED_LINK_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_PUBLISHED_LINK_DISABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_PUBLISHED_LINK_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PASSWORD_CHANGE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PASSWORD_RESET_ALL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PASSWORD_RESET_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PASSWORD_STRENGTH_REQUIREMENTS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PENDING_SECONDARY_EMAIL_ADDED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PERMANENT_DELETE_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum RESELLER_SUPPORT_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum RESELLER_SUPPORT_SESSION_END_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum RESELLER_SUPPORT_SESSION_START_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum REWIND_FOLDER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum REWIND_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SECONDARY_EMAIL_DELETED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SECONDARY_EMAIL_VERIFIED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SECONDARY_MAILS_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_ADD_GROUP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_ALLOW_NON_MEMBERS_TO_VIEW_SHARED_LINKS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_EXTERNAL_INVITE_WARN_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_FB_INVITE_CHANGE_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_FB_INVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_FB_UNINVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_INVITE_GROUP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_TEAM_GRANT_ACCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_TEAM_INVITE_CHANGE_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_TEAM_INVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_TEAM_JOIN_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_TEAM_JOIN_FROM_OOB_LINK_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_TEAM_UNINVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_ADD_INVITEES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_ADD_LINK_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_ADD_LINK_PASSWORD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_ADD_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_CHANGE_DOWNLOADS_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_CHANGE_INVITEE_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_CHANGE_LINK_AUDIENCE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_CHANGE_LINK_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_CHANGE_LINK_PASSWORD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_CHANGE_MEMBER_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_CHANGE_VIEWER_INFO_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_CLAIM_INVITATION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_COPY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_RELINQUISH_MEMBERSHIP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_REMOVE_INVITEES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_REMOVE_LINK_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_REMOVE_LINK_PASSWORD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_REMOVE_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_REQUEST_ACCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_RESTORE_INVITEES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_RESTORE_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_UNSHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_FOLDER_CHANGE_LINK_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_FOLDER_CHANGE_MEMBERS_INHERITANCE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_FOLDER_CHANGE_MEMBERS_MANAGEMENT_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_FOLDER_CHANGE_MEMBERS_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_FOLDER_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_FOLDER_DECLINE_INVITATION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_FOLDER_MOUNT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_FOLDER_NEST_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_FOLDER_TRANSFER_OWNERSHIP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_FOLDER_UNMOUNT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_ADD_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_CHANGE_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_CHANGE_VISIBILITY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_COPY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_DISABLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_REMOVE_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_SETTINGS_ADD_EXPIRATION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_SETTINGS_ADD_PASSWORD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_SETTINGS_ALLOW_DOWNLOAD_DISABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_SETTINGS_ALLOW_DOWNLOAD_ENABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_SETTINGS_CHANGE_AUDIENCE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_SETTINGS_CHANGE_EXPIRATION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_SETTINGS_CHANGE_PASSWORD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_SETTINGS_REMOVE_EXPIRATION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_SETTINGS_REMOVE_PASSWORD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_SHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_NOTE_OPENED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARING_CHANGE_FOLDER_JOIN_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARING_CHANGE_LINK_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARING_CHANGE_MEMBER_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHMODEL_GROUP_SHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_ACCESS_GRANTED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_ADD_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_ARCHIVED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_CHANGE_DOWNLOAD_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_CHANGE_ENABLED_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_CHANGE_EXTERNAL_SHARING_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_CREATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_DELETE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_EDITED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_EDIT_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_FILE_ADDED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_FILE_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_FILE_REMOVED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_FILE_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_PERMANENTLY_DELETED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_POST_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_REMOVE_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_RENAMED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_REQUEST_ACCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_RESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_RESTORED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_TRASHED_DEPRECATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_TRASHED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_UNRESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_UNTRASHED_DEPRECATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_UNTRASHED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SIGN_IN_AS_SESSION_END_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SIGN_IN_AS_SESSION_START_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SMARTER_SMART_SYNC_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SMART_SYNC_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SMART_SYNC_CREATE_ADMIN_PRIVILEGE_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SMART_SYNC_NOT_OPT_OUT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SMART_SYNC_OPT_OUT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SSO_ADD_CERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SSO_ADD_LOGIN_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SSO_ADD_LOGOUT_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SSO_CHANGE_CERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SSO_CHANGE_LOGIN_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SSO_CHANGE_LOGOUT_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SSO_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SSO_CHANGE_SAML_IDENTITY_MODE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SSO_ERROR_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SSO_REMOVE_CERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SSO_REMOVE_LOGIN_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SSO_REMOVE_LOGOUT_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum STARTED_ENTERPRISE_ADMIN_SESSION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_ACTIVITY_CREATE_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_ACTIVITY_CREATE_REPORT_FAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_EXTENSIONS_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_FOLDER_CHANGE_STATUS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_FOLDER_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_FOLDER_DOWNGRADE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_FOLDER_PERMANENTLY_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_FOLDER_RENAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_MERGE_FROM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_MERGE_REQUEST_ACCEPTED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_MERGE_REQUEST_ACCEPTED_SHOWN_TO_PRIMARY_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_MERGE_REQUEST_ACCEPTED_SHOWN_TO_SECONDARY_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_MERGE_REQUEST_AUTO_CANCELED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_MERGE_REQUEST_CANCELED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_MERGE_REQUEST_CANCELED_SHOWN_TO_PRIMARY_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_MERGE_REQUEST_CANCELED_SHOWN_TO_SECONDARY_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_MERGE_REQUEST_EXPIRED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_MERGE_REQUEST_EXPIRED_SHOWN_TO_PRIMARY_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_MERGE_REQUEST_EXPIRED_SHOWN_TO_SECONDARY_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_MERGE_REQUEST_REJECTED_SHOWN_TO_PRIMARY_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_MERGE_REQUEST_REJECTED_SHOWN_TO_SECONDARY_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_MERGE_REQUEST_REMINDER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_MERGE_REQUEST_REMINDER_SHOWN_TO_PRIMARY_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_MERGE_REQUEST_REMINDER_SHOWN_TO_SECONDARY_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_MERGE_REQUEST_REVOKED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_MERGE_REQUEST_SENT_SHOWN_TO_PRIMARY_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_MERGE_REQUEST_SENT_SHOWN_TO_SECONDARY_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_MERGE_TO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_PROFILE_ADD_LOGO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_PROFILE_CHANGE_DEFAULT_LANGUAGE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_PROFILE_CHANGE_LOGO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_PROFILE_CHANGE_NAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_PROFILE_REMOVE_LOGO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_SELECTIVE_SYNC_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_SELECTIVE_SYNC_SETTINGS_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_SHARING_WHITELIST_SUBJECTS_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TFA_ADD_BACKUP_PHONE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TFA_ADD_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TFA_ADD_SECURITY_KEY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TFA_CHANGE_BACKUP_PHONE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TFA_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TFA_CHANGE_STATUS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TFA_REMOVE_BACKUP_PHONE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TFA_REMOVE_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TFA_REMOVE_SECURITY_KEY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TFA_RESET_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TWO_ACCOUNT_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum VIEWER_INFO_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum WATERMARKING_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum WEB_SESSIONS_CHANGE_ACTIVE_SESSION_LIMIT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum WEB_SESSIONS_CHANGE_FIXED_LENGTH_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum WEB_SESSIONS_CHANGE_IDLE_LENGTH_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;


# direct methods
.method static constructor <clinit>()V
    .locals 428

    .line 1
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "APP_LINK_TEAM_DETAILS"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->APP_LINK_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 2
    new-instance v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v3, "APP_LINK_USER_DETAILS"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->APP_LINK_USER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 3
    new-instance v3, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v5, "APP_UNLINK_TEAM_DETAILS"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->APP_UNLINK_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 4
    new-instance v5, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v7, "APP_UNLINK_USER_DETAILS"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->APP_UNLINK_USER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 5
    new-instance v7, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v9, "INTEGRATION_CONNECTED_DETAILS"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->INTEGRATION_CONNECTED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 6
    new-instance v9, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v11, "INTEGRATION_DISCONNECTED_DETAILS"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->INTEGRATION_DISCONNECTED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 7
    new-instance v11, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v13, "FILE_ADD_COMMENT_DETAILS"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_ADD_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 8
    new-instance v13, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v15, "FILE_CHANGE_COMMENT_SUBSCRIPTION_DETAILS"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_CHANGE_COMMENT_SUBSCRIPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 9
    new-instance v15, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v14, "FILE_DELETE_COMMENT_DETAILS"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_DELETE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 10
    new-instance v14, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v12, "FILE_EDIT_COMMENT_DETAILS"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_EDIT_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 11
    new-instance v12, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v10, "FILE_LIKE_COMMENT_DETAILS"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_LIKE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 12
    new-instance v10, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v8, "FILE_RESOLVE_COMMENT_DETAILS"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_RESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 13
    new-instance v8, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_UNLIKE_COMMENT_DETAILS"

    const/16 v4, 0xc

    invoke-direct {v8, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_UNLIKE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 14
    new-instance v6, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v4, "FILE_UNRESOLVE_COMMENT_DETAILS"

    const/16 v2, 0xd

    invoke-direct {v6, v4, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_UNRESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 15
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v2, "DEVICE_CHANGE_IP_DESKTOP_DETAILS"

    move-object/from16 v16, v6

    const/16 v6, 0xe

    invoke-direct {v4, v2, v6}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_CHANGE_IP_DESKTOP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 16
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "DEVICE_CHANGE_IP_MOBILE_DETAILS"

    move-object/from16 v17, v4

    const/16 v4, 0xf

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_CHANGE_IP_MOBILE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 17
    new-instance v6, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v4, "DEVICE_CHANGE_IP_WEB_DETAILS"

    move-object/from16 v18, v2

    const/16 v2, 0x10

    invoke-direct {v6, v4, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_CHANGE_IP_WEB_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 18
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v2, "DEVICE_DELETE_ON_UNLINK_FAIL_DETAILS"

    move-object/from16 v19, v6

    const/16 v6, 0x11

    invoke-direct {v4, v2, v6}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_DELETE_ON_UNLINK_FAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 19
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "DEVICE_DELETE_ON_UNLINK_SUCCESS_DETAILS"

    move-object/from16 v20, v4

    const/16 v4, 0x12

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_DELETE_ON_UNLINK_SUCCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 20
    new-instance v6, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v4, "DEVICE_LINK_FAIL_DETAILS"

    move-object/from16 v21, v2

    const/16 v2, 0x13

    invoke-direct {v6, v4, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_LINK_FAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 21
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v2, "DEVICE_LINK_SUCCESS_DETAILS"

    move-object/from16 v22, v6

    const/16 v6, 0x14

    invoke-direct {v4, v2, v6}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_LINK_SUCCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 22
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "DEVICE_MANAGEMENT_DISABLED_DETAILS"

    move-object/from16 v23, v4

    const/16 v4, 0x15

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_MANAGEMENT_DISABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 23
    new-instance v6, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v4, "DEVICE_MANAGEMENT_ENABLED_DETAILS"

    move-object/from16 v24, v2

    const/16 v2, 0x16

    invoke-direct {v6, v4, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_MANAGEMENT_ENABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 24
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v4, "DEVICE_UNLINK_DETAILS"

    move-object/from16 v25, v6

    const/16 v6, 0x17

    invoke-direct {v2, v4, v6}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_UNLINK_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 25
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "EMM_REFRESH_AUTH_TOKEN_DETAILS"

    move-object/from16 v26, v2

    const/16 v2, 0x18

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EMM_REFRESH_AUTH_TOKEN_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 26
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "ACCOUNT_CAPTURE_CHANGE_AVAILABILITY_DETAILS"

    move-object/from16 v27, v4

    const/16 v4, 0x19

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ACCOUNT_CAPTURE_CHANGE_AVAILABILITY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 27
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "ACCOUNT_CAPTURE_MIGRATE_ACCOUNT_DETAILS"

    move-object/from16 v28, v2

    const/16 v2, 0x1a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ACCOUNT_CAPTURE_MIGRATE_ACCOUNT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 28
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "ACCOUNT_CAPTURE_NOTIFICATION_EMAILS_SENT_DETAILS"

    move-object/from16 v29, v4

    const/16 v4, 0x1b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ACCOUNT_CAPTURE_NOTIFICATION_EMAILS_SENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 29
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "ACCOUNT_CAPTURE_RELINQUISH_ACCOUNT_DETAILS"

    move-object/from16 v30, v2

    const/16 v2, 0x1c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ACCOUNT_CAPTURE_RELINQUISH_ACCOUNT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 30
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "DISABLED_DOMAIN_INVITES_DETAILS"

    move-object/from16 v31, v4

    const/16 v4, 0x1d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DISABLED_DOMAIN_INVITES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 31
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "DOMAIN_INVITES_APPROVE_REQUEST_TO_JOIN_TEAM_DETAILS"

    move-object/from16 v32, v2

    const/16 v2, 0x1e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_INVITES_APPROVE_REQUEST_TO_JOIN_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 32
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "DOMAIN_INVITES_DECLINE_REQUEST_TO_JOIN_TEAM_DETAILS"

    move-object/from16 v33, v4

    const/16 v4, 0x1f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_INVITES_DECLINE_REQUEST_TO_JOIN_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 33
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "DOMAIN_INVITES_EMAIL_EXISTING_USERS_DETAILS"

    move-object/from16 v34, v2

    const/16 v2, 0x20

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_INVITES_EMAIL_EXISTING_USERS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 34
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "DOMAIN_INVITES_REQUEST_TO_JOIN_TEAM_DETAILS"

    move-object/from16 v35, v4

    const/16 v4, 0x21

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_INVITES_REQUEST_TO_JOIN_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 35
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_NO_DETAILS"

    move-object/from16 v36, v2

    const/16 v2, 0x22

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_NO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 36
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_YES_DETAILS"

    move-object/from16 v37, v4

    const/16 v4, 0x23

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_YES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 37
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "DOMAIN_VERIFICATION_ADD_DOMAIN_FAIL_DETAILS"

    move-object/from16 v38, v2

    const/16 v2, 0x24

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_VERIFICATION_ADD_DOMAIN_FAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 38
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "DOMAIN_VERIFICATION_ADD_DOMAIN_SUCCESS_DETAILS"

    move-object/from16 v39, v4

    const/16 v4, 0x25

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_VERIFICATION_ADD_DOMAIN_SUCCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 39
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "DOMAIN_VERIFICATION_REMOVE_DOMAIN_DETAILS"

    move-object/from16 v40, v2

    const/16 v2, 0x26

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_VERIFICATION_REMOVE_DOMAIN_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 40
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "ENABLED_DOMAIN_INVITES_DETAILS"

    move-object/from16 v41, v4

    const/16 v4, 0x27

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ENABLED_DOMAIN_INVITES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 41
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "CREATE_FOLDER_DETAILS"

    move-object/from16 v42, v2

    const/16 v2, 0x28

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->CREATE_FOLDER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 42
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_ADD_DETAILS"

    move-object/from16 v43, v4

    const/16 v4, 0x29

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_ADD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 43
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_COPY_DETAILS"

    move-object/from16 v44, v2

    const/16 v2, 0x2a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_COPY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 44
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_DELETE_DETAILS"

    move-object/from16 v45, v4

    const/16 v4, 0x2b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 45
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_DOWNLOAD_DETAILS"

    move-object/from16 v46, v2

    const/16 v2, 0x2c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 46
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_EDIT_DETAILS"

    move-object/from16 v47, v4

    const/16 v4, 0x2d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_EDIT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 47
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_GET_COPY_REFERENCE_DETAILS"

    move-object/from16 v48, v2

    const/16 v2, 0x2e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_GET_COPY_REFERENCE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 48
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_LOCKING_LOCK_STATUS_CHANGED_DETAILS"

    move-object/from16 v49, v4

    const/16 v4, 0x2f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_LOCKING_LOCK_STATUS_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 49
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_MOVE_DETAILS"

    move-object/from16 v50, v2

    const/16 v2, 0x30

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_MOVE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 50
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_PERMANENTLY_DELETE_DETAILS"

    move-object/from16 v51, v4

    const/16 v4, 0x31

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_PERMANENTLY_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 51
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_PREVIEW_DETAILS"

    move-object/from16 v52, v2

    const/16 v2, 0x32

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_PREVIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 52
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_RENAME_DETAILS"

    move-object/from16 v53, v4

    const/16 v4, 0x33

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_RENAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 53
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_RESTORE_DETAILS"

    move-object/from16 v54, v2

    const/16 v2, 0x34

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_RESTORE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 54
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_REVERT_DETAILS"

    move-object/from16 v55, v4

    const/16 v4, 0x35

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REVERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 55
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_ROLLBACK_CHANGES_DETAILS"

    move-object/from16 v56, v2

    const/16 v2, 0x36

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_ROLLBACK_CHANGES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 56
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_SAVE_COPY_REFERENCE_DETAILS"

    move-object/from16 v57, v4

    const/16 v4, 0x37

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_SAVE_COPY_REFERENCE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 57
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FOLDER_OVERVIEW_DESCRIPTION_CHANGED_DETAILS"

    move-object/from16 v58, v2

    const/16 v2, 0x38

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FOLDER_OVERVIEW_DESCRIPTION_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 58
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FOLDER_OVERVIEW_ITEM_PINNED_DETAILS"

    move-object/from16 v59, v4

    const/16 v4, 0x39

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FOLDER_OVERVIEW_ITEM_PINNED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 59
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FOLDER_OVERVIEW_ITEM_UNPINNED_DETAILS"

    move-object/from16 v60, v2

    const/16 v2, 0x3a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FOLDER_OVERVIEW_ITEM_UNPINNED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 60
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "REWIND_FOLDER_DETAILS"

    move-object/from16 v61, v4

    const/16 v4, 0x3b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->REWIND_FOLDER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 61
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_REQUEST_CHANGE_DETAILS"

    move-object/from16 v62, v2

    const/16 v2, 0x3c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REQUEST_CHANGE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 62
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_REQUEST_CLOSE_DETAILS"

    move-object/from16 v63, v4

    const/16 v4, 0x3d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REQUEST_CLOSE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 63
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_REQUEST_CREATE_DETAILS"

    move-object/from16 v64, v2

    const/16 v2, 0x3e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REQUEST_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 64
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_REQUEST_DELETE_DETAILS"

    move-object/from16 v65, v4

    const/16 v4, 0x3f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REQUEST_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 65
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_REQUEST_RECEIVE_FILE_DETAILS"

    move-object/from16 v66, v2

    const/16 v2, 0x40

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REQUEST_RECEIVE_FILE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 66
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "GROUP_ADD_EXTERNAL_ID_DETAILS"

    move-object/from16 v67, v4

    const/16 v4, 0x41

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_ADD_EXTERNAL_ID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 67
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "GROUP_ADD_MEMBER_DETAILS"

    move-object/from16 v68, v2

    const/16 v2, 0x42

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_ADD_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 68
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "GROUP_CHANGE_EXTERNAL_ID_DETAILS"

    move-object/from16 v69, v4

    const/16 v4, 0x43

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_CHANGE_EXTERNAL_ID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 69
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "GROUP_CHANGE_MANAGEMENT_TYPE_DETAILS"

    move-object/from16 v70, v2

    const/16 v2, 0x44

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_CHANGE_MANAGEMENT_TYPE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 70
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "GROUP_CHANGE_MEMBER_ROLE_DETAILS"

    move-object/from16 v71, v4

    const/16 v4, 0x45

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_CHANGE_MEMBER_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 71
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "GROUP_CREATE_DETAILS"

    move-object/from16 v72, v2

    const/16 v2, 0x46

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 72
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "GROUP_DELETE_DETAILS"

    move-object/from16 v73, v4

    const/16 v4, 0x47

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 73
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "GROUP_DESCRIPTION_UPDATED_DETAILS"

    move-object/from16 v74, v2

    const/16 v2, 0x48

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_DESCRIPTION_UPDATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 74
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "GROUP_JOIN_POLICY_UPDATED_DETAILS"

    move-object/from16 v75, v4

    const/16 v4, 0x49

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_JOIN_POLICY_UPDATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 75
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "GROUP_MOVED_DETAILS"

    move-object/from16 v76, v2

    const/16 v2, 0x4a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_MOVED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 76
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "GROUP_REMOVE_EXTERNAL_ID_DETAILS"

    move-object/from16 v77, v4

    const/16 v4, 0x4b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_REMOVE_EXTERNAL_ID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 77
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "GROUP_REMOVE_MEMBER_DETAILS"

    move-object/from16 v78, v2

    const/16 v2, 0x4c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_REMOVE_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 78
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "GROUP_RENAME_DETAILS"

    move-object/from16 v79, v4

    const/16 v4, 0x4d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_RENAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 79
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "LEGAL_HOLDS_ACTIVATE_A_HOLD_DETAILS"

    move-object/from16 v80, v2

    const/16 v2, 0x4e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->LEGAL_HOLDS_ACTIVATE_A_HOLD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 80
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "LEGAL_HOLDS_ADD_MEMBERS_DETAILS"

    move-object/from16 v81, v4

    const/16 v4, 0x4f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->LEGAL_HOLDS_ADD_MEMBERS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 81
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "LEGAL_HOLDS_CHANGE_HOLD_DETAILS_DETAILS"

    move-object/from16 v82, v2

    const/16 v2, 0x50

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->LEGAL_HOLDS_CHANGE_HOLD_DETAILS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 82
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "LEGAL_HOLDS_CHANGE_HOLD_NAME_DETAILS"

    move-object/from16 v83, v4

    const/16 v4, 0x51

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->LEGAL_HOLDS_CHANGE_HOLD_NAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 83
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "LEGAL_HOLDS_EXPORT_A_HOLD_DETAILS"

    move-object/from16 v84, v2

    const/16 v2, 0x52

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->LEGAL_HOLDS_EXPORT_A_HOLD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 84
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "LEGAL_HOLDS_EXPORT_CANCELLED_DETAILS"

    move-object/from16 v85, v4

    const/16 v4, 0x53

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->LEGAL_HOLDS_EXPORT_CANCELLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 85
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "LEGAL_HOLDS_EXPORT_DOWNLOADED_DETAILS"

    move-object/from16 v86, v2

    const/16 v2, 0x54

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->LEGAL_HOLDS_EXPORT_DOWNLOADED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 86
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "LEGAL_HOLDS_EXPORT_REMOVED_DETAILS"

    move-object/from16 v87, v4

    const/16 v4, 0x55

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->LEGAL_HOLDS_EXPORT_REMOVED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 87
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "LEGAL_HOLDS_RELEASE_A_HOLD_DETAILS"

    move-object/from16 v88, v2

    const/16 v2, 0x56

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->LEGAL_HOLDS_RELEASE_A_HOLD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 88
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "LEGAL_HOLDS_REMOVE_MEMBERS_DETAILS"

    move-object/from16 v89, v4

    const/16 v4, 0x57

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->LEGAL_HOLDS_REMOVE_MEMBERS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 89
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "LEGAL_HOLDS_REPORT_A_HOLD_DETAILS"

    move-object/from16 v90, v2

    const/16 v2, 0x58

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->LEGAL_HOLDS_REPORT_A_HOLD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 90
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "ACCOUNT_LOCK_OR_UNLOCKED_DETAILS"

    move-object/from16 v91, v4

    const/16 v4, 0x59

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ACCOUNT_LOCK_OR_UNLOCKED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 91
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "EMM_ERROR_DETAILS"

    move-object/from16 v92, v2

    const/16 v2, 0x5a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EMM_ERROR_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 92
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "GUEST_ADMIN_SIGNED_IN_VIA_TRUSTED_TEAMS_DETAILS"

    move-object/from16 v93, v4

    const/16 v4, 0x5b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GUEST_ADMIN_SIGNED_IN_VIA_TRUSTED_TEAMS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 93
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "GUEST_ADMIN_SIGNED_OUT_VIA_TRUSTED_TEAMS_DETAILS"

    move-object/from16 v94, v2

    const/16 v2, 0x5c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GUEST_ADMIN_SIGNED_OUT_VIA_TRUSTED_TEAMS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 94
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "LOGIN_FAIL_DETAILS"

    move-object/from16 v95, v4

    const/16 v4, 0x5d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->LOGIN_FAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 95
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "LOGIN_SUCCESS_DETAILS"

    move-object/from16 v96, v2

    const/16 v2, 0x5e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->LOGIN_SUCCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 96
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "LOGOUT_DETAILS"

    move-object/from16 v97, v4

    const/16 v4, 0x5f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->LOGOUT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 97
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "RESELLER_SUPPORT_SESSION_END_DETAILS"

    move-object/from16 v98, v2

    const/16 v2, 0x60

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->RESELLER_SUPPORT_SESSION_END_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 98
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "RESELLER_SUPPORT_SESSION_START_DETAILS"

    move-object/from16 v99, v4

    const/16 v4, 0x61

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->RESELLER_SUPPORT_SESSION_START_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 99
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SIGN_IN_AS_SESSION_END_DETAILS"

    move-object/from16 v100, v2

    const/16 v2, 0x62

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SIGN_IN_AS_SESSION_END_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 100
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SIGN_IN_AS_SESSION_START_DETAILS"

    move-object/from16 v101, v4

    const/16 v4, 0x63

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SIGN_IN_AS_SESSION_START_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 101
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SSO_ERROR_DETAILS"

    move-object/from16 v102, v2

    const/16 v2, 0x64

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_ERROR_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 102
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "CREATE_TEAM_INVITE_LINK_DETAILS"

    move-object/from16 v103, v4

    const/16 v4, 0x65

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->CREATE_TEAM_INVITE_LINK_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 103
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "DELETE_TEAM_INVITE_LINK_DETAILS"

    move-object/from16 v104, v2

    const/16 v2, 0x66

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DELETE_TEAM_INVITE_LINK_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 104
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_ADD_EXTERNAL_ID_DETAILS"

    move-object/from16 v105, v4

    const/16 v4, 0x67

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_ADD_EXTERNAL_ID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 105
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_ADD_NAME_DETAILS"

    move-object/from16 v106, v2

    const/16 v2, 0x68

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_ADD_NAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 106
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_CHANGE_ADMIN_ROLE_DETAILS"

    move-object/from16 v107, v4

    const/16 v4, 0x69

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_CHANGE_ADMIN_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 107
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_CHANGE_EMAIL_DETAILS"

    move-object/from16 v108, v2

    const/16 v2, 0x6a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_CHANGE_EMAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 108
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_CHANGE_EXTERNAL_ID_DETAILS"

    move-object/from16 v109, v4

    const/16 v4, 0x6b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_CHANGE_EXTERNAL_ID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 109
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_CHANGE_MEMBERSHIP_TYPE_DETAILS"

    move-object/from16 v110, v2

    const/16 v2, 0x6c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_CHANGE_MEMBERSHIP_TYPE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 110
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_CHANGE_NAME_DETAILS"

    move-object/from16 v111, v4

    const/16 v4, 0x6d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_CHANGE_NAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 111
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_CHANGE_STATUS_DETAILS"

    move-object/from16 v112, v2

    const/16 v2, 0x6e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_CHANGE_STATUS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 112
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_DELETE_MANUAL_CONTACTS_DETAILS"

    move-object/from16 v113, v4

    const/16 v4, 0x6f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_DELETE_MANUAL_CONTACTS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 113
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_DELETE_PROFILE_PHOTO_DETAILS"

    move-object/from16 v114, v2

    const/16 v2, 0x70

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_DELETE_PROFILE_PHOTO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 114
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_PERMANENTLY_DELETE_ACCOUNT_CONTENTS_DETAILS"

    move-object/from16 v115, v4

    const/16 v4, 0x71

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_PERMANENTLY_DELETE_ACCOUNT_CONTENTS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 115
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_REMOVE_EXTERNAL_ID_DETAILS"

    move-object/from16 v116, v2

    const/16 v2, 0x72

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_REMOVE_EXTERNAL_ID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 116
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_SET_PROFILE_PHOTO_DETAILS"

    move-object/from16 v117, v4

    const/16 v4, 0x73

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SET_PROFILE_PHOTO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 117
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_SPACE_LIMITS_ADD_CUSTOM_QUOTA_DETAILS"

    move-object/from16 v118, v2

    const/16 v2, 0x74

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SPACE_LIMITS_ADD_CUSTOM_QUOTA_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 118
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_SPACE_LIMITS_CHANGE_CUSTOM_QUOTA_DETAILS"

    move-object/from16 v119, v4

    const/16 v4, 0x75

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SPACE_LIMITS_CHANGE_CUSTOM_QUOTA_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 119
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_SPACE_LIMITS_CHANGE_STATUS_DETAILS"

    move-object/from16 v120, v2

    const/16 v2, 0x76

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SPACE_LIMITS_CHANGE_STATUS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 120
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_SPACE_LIMITS_REMOVE_CUSTOM_QUOTA_DETAILS"

    move-object/from16 v121, v4

    const/16 v4, 0x77

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SPACE_LIMITS_REMOVE_CUSTOM_QUOTA_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 121
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_SUGGEST_DETAILS"

    move-object/from16 v122, v2

    const/16 v2, 0x78

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SUGGEST_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 122
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_TRANSFER_ACCOUNT_CONTENTS_DETAILS"

    move-object/from16 v123, v4

    const/16 v4, 0x79

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_TRANSFER_ACCOUNT_CONTENTS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 123
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PENDING_SECONDARY_EMAIL_ADDED_DETAILS"

    move-object/from16 v124, v2

    const/16 v2, 0x7a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PENDING_SECONDARY_EMAIL_ADDED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 124
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SECONDARY_EMAIL_DELETED_DETAILS"

    move-object/from16 v125, v4

    const/16 v4, 0x7b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SECONDARY_EMAIL_DELETED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 125
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SECONDARY_EMAIL_VERIFIED_DETAILS"

    move-object/from16 v126, v2

    const/16 v2, 0x7c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SECONDARY_EMAIL_VERIFIED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 126
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SECONDARY_MAILS_POLICY_CHANGED_DETAILS"

    move-object/from16 v127, v4

    const/16 v4, 0x7d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SECONDARY_MAILS_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 127
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "BINDER_ADD_PAGE_DETAILS"

    move-object/from16 v128, v2

    const/16 v2, 0x7e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->BINDER_ADD_PAGE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 128
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "BINDER_ADD_SECTION_DETAILS"

    move-object/from16 v129, v4

    const/16 v4, 0x7f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->BINDER_ADD_SECTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 129
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "BINDER_REMOVE_PAGE_DETAILS"

    move-object/from16 v130, v2

    const/16 v2, 0x80

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->BINDER_REMOVE_PAGE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 130
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "BINDER_REMOVE_SECTION_DETAILS"

    move-object/from16 v131, v4

    const/16 v4, 0x81

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->BINDER_REMOVE_SECTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 131
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "BINDER_RENAME_PAGE_DETAILS"

    move-object/from16 v132, v2

    const/16 v2, 0x82

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->BINDER_RENAME_PAGE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 132
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "BINDER_RENAME_SECTION_DETAILS"

    move-object/from16 v133, v4

    const/16 v4, 0x83

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->BINDER_RENAME_SECTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 133
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "BINDER_REORDER_PAGE_DETAILS"

    move-object/from16 v134, v2

    const/16 v2, 0x84

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->BINDER_REORDER_PAGE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 134
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "BINDER_REORDER_SECTION_DETAILS"

    move-object/from16 v135, v4

    const/16 v4, 0x85

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->BINDER_REORDER_SECTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 135
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_CONTENT_ADD_MEMBER_DETAILS"

    move-object/from16 v136, v2

    const/16 v2, 0x86

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_ADD_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 136
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_CONTENT_ADD_TO_FOLDER_DETAILS"

    move-object/from16 v137, v4

    const/16 v4, 0x87

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_ADD_TO_FOLDER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 137
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_CONTENT_ARCHIVE_DETAILS"

    move-object/from16 v138, v2

    const/16 v2, 0x88

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_ARCHIVE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 138
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_CONTENT_CREATE_DETAILS"

    move-object/from16 v139, v4

    const/16 v4, 0x89

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 139
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_CONTENT_PERMANENTLY_DELETE_DETAILS"

    move-object/from16 v140, v2

    const/16 v2, 0x8a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_PERMANENTLY_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 140
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_CONTENT_REMOVE_FROM_FOLDER_DETAILS"

    move-object/from16 v141, v4

    const/16 v4, 0x8b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_REMOVE_FROM_FOLDER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 141
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_CONTENT_REMOVE_MEMBER_DETAILS"

    move-object/from16 v142, v2

    const/16 v2, 0x8c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_REMOVE_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 142
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_CONTENT_RENAME_DETAILS"

    move-object/from16 v143, v4

    const/16 v4, 0x8d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_RENAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 143
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_CONTENT_RESTORE_DETAILS"

    move-object/from16 v144, v2

    const/16 v2, 0x8e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_RESTORE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 144
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_DOC_ADD_COMMENT_DETAILS"

    move-object/from16 v145, v4

    const/16 v4, 0x8f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_ADD_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 145
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_DOC_CHANGE_MEMBER_ROLE_DETAILS"

    move-object/from16 v146, v2

    const/16 v2, 0x90

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_CHANGE_MEMBER_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 146
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_DOC_CHANGE_SHARING_POLICY_DETAILS"

    move-object/from16 v147, v4

    const/16 v4, 0x91

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_CHANGE_SHARING_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 147
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_DOC_CHANGE_SUBSCRIPTION_DETAILS"

    move-object/from16 v148, v2

    const/16 v2, 0x92

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_CHANGE_SUBSCRIPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 148
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_DOC_DELETED_DETAILS"

    move-object/from16 v149, v4

    const/16 v4, 0x93

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_DELETED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 149
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_DOC_DELETE_COMMENT_DETAILS"

    move-object/from16 v150, v2

    const/16 v2, 0x94

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_DELETE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 150
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_DOC_DOWNLOAD_DETAILS"

    move-object/from16 v151, v4

    const/16 v4, 0x95

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 151
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_DOC_EDIT_DETAILS"

    move-object/from16 v152, v2

    const/16 v2, 0x96

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_EDIT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 152
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_DOC_EDIT_COMMENT_DETAILS"

    move-object/from16 v153, v4

    const/16 v4, 0x97

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_EDIT_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 153
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_DOC_FOLLOWED_DETAILS"

    move-object/from16 v154, v2

    const/16 v2, 0x98

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_FOLLOWED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 154
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_DOC_MENTION_DETAILS"

    move-object/from16 v155, v4

    const/16 v4, 0x99

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_MENTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 155
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_DOC_OWNERSHIP_CHANGED_DETAILS"

    move-object/from16 v156, v2

    const/16 v2, 0x9a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_OWNERSHIP_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 156
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_DOC_REQUEST_ACCESS_DETAILS"

    move-object/from16 v157, v4

    const/16 v4, 0x9b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_REQUEST_ACCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 157
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_DOC_RESOLVE_COMMENT_DETAILS"

    move-object/from16 v158, v2

    const/16 v2, 0x9c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_RESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 158
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_DOC_REVERT_DETAILS"

    move-object/from16 v159, v4

    const/16 v4, 0x9d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_REVERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 159
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_DOC_SLACK_SHARE_DETAILS"

    move-object/from16 v160, v2

    const/16 v2, 0x9e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_SLACK_SHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 160
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_DOC_TEAM_INVITE_DETAILS"

    move-object/from16 v161, v4

    const/16 v4, 0x9f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_TEAM_INVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 161
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_DOC_TRASHED_DETAILS"

    move-object/from16 v162, v2

    const/16 v2, 0xa0

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_TRASHED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 162
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_DOC_UNRESOLVE_COMMENT_DETAILS"

    move-object/from16 v163, v4

    const/16 v4, 0xa1

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_UNRESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 163
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_DOC_UNTRASHED_DETAILS"

    move-object/from16 v164, v2

    const/16 v2, 0xa2

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_UNTRASHED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 164
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_DOC_VIEW_DETAILS"

    move-object/from16 v165, v4

    const/16 v4, 0xa3

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 165
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_EXTERNAL_VIEW_ALLOW_DETAILS"

    move-object/from16 v166, v2

    const/16 v2, 0xa4

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_EXTERNAL_VIEW_ALLOW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 166
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_EXTERNAL_VIEW_DEFAULT_TEAM_DETAILS"

    move-object/from16 v167, v4

    const/16 v4, 0xa5

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_EXTERNAL_VIEW_DEFAULT_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 167
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_EXTERNAL_VIEW_FORBID_DETAILS"

    move-object/from16 v168, v2

    const/16 v2, 0xa6

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_EXTERNAL_VIEW_FORBID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 168
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_FOLDER_CHANGE_SUBSCRIPTION_DETAILS"

    move-object/from16 v169, v4

    const/16 v4, 0xa7

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_FOLDER_CHANGE_SUBSCRIPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 169
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_FOLDER_DELETED_DETAILS"

    move-object/from16 v170, v2

    const/16 v2, 0xa8

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_FOLDER_DELETED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 170
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_FOLDER_FOLLOWED_DETAILS"

    move-object/from16 v171, v4

    const/16 v4, 0xa9

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_FOLDER_FOLLOWED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 171
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_FOLDER_TEAM_INVITE_DETAILS"

    move-object/from16 v172, v2

    const/16 v2, 0xaa

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_FOLDER_TEAM_INVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 172
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_PUBLISHED_LINK_CHANGE_PERMISSION_DETAILS"

    move-object/from16 v173, v4

    const/16 v4, 0xab

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_PUBLISHED_LINK_CHANGE_PERMISSION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 173
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_PUBLISHED_LINK_CREATE_DETAILS"

    move-object/from16 v174, v2

    const/16 v2, 0xac

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_PUBLISHED_LINK_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 174
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_PUBLISHED_LINK_DISABLED_DETAILS"

    move-object/from16 v175, v4

    const/16 v4, 0xad

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_PUBLISHED_LINK_DISABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 175
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_PUBLISHED_LINK_VIEW_DETAILS"

    move-object/from16 v176, v2

    const/16 v2, 0xae

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_PUBLISHED_LINK_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 176
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PASSWORD_CHANGE_DETAILS"

    move-object/from16 v177, v4

    const/16 v4, 0xaf

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PASSWORD_CHANGE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 177
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PASSWORD_RESET_DETAILS"

    move-object/from16 v178, v2

    const/16 v2, 0xb0

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PASSWORD_RESET_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 178
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PASSWORD_RESET_ALL_DETAILS"

    move-object/from16 v179, v4

    const/16 v4, 0xb1

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PASSWORD_RESET_ALL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 179
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "EMM_CREATE_EXCEPTIONS_REPORT_DETAILS"

    move-object/from16 v180, v2

    const/16 v2, 0xb2

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EMM_CREATE_EXCEPTIONS_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 180
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "EMM_CREATE_USAGE_REPORT_DETAILS"

    move-object/from16 v181, v4

    const/16 v4, 0xb3

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EMM_CREATE_USAGE_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 181
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "EXPORT_MEMBERS_REPORT_DETAILS"

    move-object/from16 v182, v2

    const/16 v2, 0xb4

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EXPORT_MEMBERS_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 182
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "EXPORT_MEMBERS_REPORT_FAIL_DETAILS"

    move-object/from16 v183, v4

    const/16 v4, 0xb5

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EXPORT_MEMBERS_REPORT_FAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 183
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "NO_EXPIRATION_LINK_GEN_CREATE_REPORT_DETAILS"

    move-object/from16 v184, v2

    const/16 v2, 0xb6

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->NO_EXPIRATION_LINK_GEN_CREATE_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 184
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "NO_EXPIRATION_LINK_GEN_REPORT_FAILED_DETAILS"

    move-object/from16 v185, v4

    const/16 v4, 0xb7

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->NO_EXPIRATION_LINK_GEN_REPORT_FAILED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 185
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "NO_PASSWORD_LINK_GEN_CREATE_REPORT_DETAILS"

    move-object/from16 v186, v2

    const/16 v2, 0xb8

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->NO_PASSWORD_LINK_GEN_CREATE_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 186
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "NO_PASSWORD_LINK_GEN_REPORT_FAILED_DETAILS"

    move-object/from16 v187, v4

    const/16 v4, 0xb9

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->NO_PASSWORD_LINK_GEN_REPORT_FAILED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 187
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "NO_PASSWORD_LINK_VIEW_CREATE_REPORT_DETAILS"

    move-object/from16 v188, v2

    const/16 v2, 0xba

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->NO_PASSWORD_LINK_VIEW_CREATE_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 188
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "NO_PASSWORD_LINK_VIEW_REPORT_FAILED_DETAILS"

    move-object/from16 v189, v4

    const/16 v4, 0xbb

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->NO_PASSWORD_LINK_VIEW_REPORT_FAILED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 189
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "OUTDATED_LINK_VIEW_CREATE_REPORT_DETAILS"

    move-object/from16 v190, v2

    const/16 v2, 0xbc

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->OUTDATED_LINK_VIEW_CREATE_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 190
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "OUTDATED_LINK_VIEW_REPORT_FAILED_DETAILS"

    move-object/from16 v191, v4

    const/16 v4, 0xbd

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->OUTDATED_LINK_VIEW_REPORT_FAILED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 191
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_ADMIN_EXPORT_START_DETAILS"

    move-object/from16 v192, v2

    const/16 v2, 0xbe

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_ADMIN_EXPORT_START_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 192
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SMART_SYNC_CREATE_ADMIN_PRIVILEGE_REPORT_DETAILS"

    move-object/from16 v193, v4

    const/16 v4, 0xbf

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SMART_SYNC_CREATE_ADMIN_PRIVILEGE_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 193
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_ACTIVITY_CREATE_REPORT_DETAILS"

    move-object/from16 v194, v2

    const/16 v2, 0xc0

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_ACTIVITY_CREATE_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 194
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_ACTIVITY_CREATE_REPORT_FAIL_DETAILS"

    move-object/from16 v195, v4

    const/16 v4, 0xc1

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_ACTIVITY_CREATE_REPORT_FAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 195
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "COLLECTION_SHARE_DETAILS"

    move-object/from16 v196, v2

    const/16 v2, 0xc2

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->COLLECTION_SHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 196
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_TRANSFERS_FILE_ADD_DETAILS"

    move-object/from16 v197, v4

    const/16 v4, 0xc3

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_TRANSFERS_FILE_ADD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 197
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_TRANSFERS_TRANSFER_DELETE_DETAILS"

    move-object/from16 v198, v2

    const/16 v2, 0xc4

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_TRANSFERS_TRANSFER_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 198
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_TRANSFERS_TRANSFER_DOWNLOAD_DETAILS"

    move-object/from16 v199, v4

    const/16 v4, 0xc5

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_TRANSFERS_TRANSFER_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 199
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_TRANSFERS_TRANSFER_SEND_DETAILS"

    move-object/from16 v200, v2

    const/16 v2, 0xc6

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_TRANSFERS_TRANSFER_SEND_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 200
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_TRANSFERS_TRANSFER_VIEW_DETAILS"

    move-object/from16 v201, v4

    const/16 v4, 0xc7

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_TRANSFERS_TRANSFER_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 201
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "NOTE_ACL_INVITE_ONLY_DETAILS"

    move-object/from16 v202, v2

    const/16 v2, 0xc8

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->NOTE_ACL_INVITE_ONLY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 202
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "NOTE_ACL_LINK_DETAILS"

    move-object/from16 v203, v4

    const/16 v4, 0xc9

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->NOTE_ACL_LINK_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 203
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "NOTE_ACL_TEAM_LINK_DETAILS"

    move-object/from16 v204, v2

    const/16 v2, 0xca

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->NOTE_ACL_TEAM_LINK_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 204
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "NOTE_SHARED_DETAILS"

    move-object/from16 v205, v4

    const/16 v4, 0xcb

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->NOTE_SHARED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 205
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "NOTE_SHARE_RECEIVE_DETAILS"

    move-object/from16 v206, v2

    const/16 v2, 0xcc

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->NOTE_SHARE_RECEIVE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 206
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "OPEN_NOTE_SHARED_DETAILS"

    move-object/from16 v207, v4

    const/16 v4, 0xcd

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->OPEN_NOTE_SHARED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 207
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SF_ADD_GROUP_DETAILS"

    move-object/from16 v208, v2

    const/16 v2, 0xce

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_ADD_GROUP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 208
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SF_ALLOW_NON_MEMBERS_TO_VIEW_SHARED_LINKS_DETAILS"

    move-object/from16 v209, v4

    const/16 v4, 0xcf

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_ALLOW_NON_MEMBERS_TO_VIEW_SHARED_LINKS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 209
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SF_EXTERNAL_INVITE_WARN_DETAILS"

    move-object/from16 v210, v2

    const/16 v2, 0xd0

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_EXTERNAL_INVITE_WARN_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 210
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SF_FB_INVITE_DETAILS"

    move-object/from16 v211, v4

    const/16 v4, 0xd1

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_FB_INVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 211
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SF_FB_INVITE_CHANGE_ROLE_DETAILS"

    move-object/from16 v212, v2

    const/16 v2, 0xd2

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_FB_INVITE_CHANGE_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 212
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SF_FB_UNINVITE_DETAILS"

    move-object/from16 v213, v4

    const/16 v4, 0xd3

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_FB_UNINVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 213
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SF_INVITE_GROUP_DETAILS"

    move-object/from16 v214, v2

    const/16 v2, 0xd4

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_INVITE_GROUP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 214
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SF_TEAM_GRANT_ACCESS_DETAILS"

    move-object/from16 v215, v4

    const/16 v4, 0xd5

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_TEAM_GRANT_ACCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 215
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SF_TEAM_INVITE_DETAILS"

    move-object/from16 v216, v2

    const/16 v2, 0xd6

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_TEAM_INVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 216
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SF_TEAM_INVITE_CHANGE_ROLE_DETAILS"

    move-object/from16 v217, v4

    const/16 v4, 0xd7

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_TEAM_INVITE_CHANGE_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 217
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SF_TEAM_JOIN_DETAILS"

    move-object/from16 v218, v2

    const/16 v2, 0xd8

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_TEAM_JOIN_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 218
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SF_TEAM_JOIN_FROM_OOB_LINK_DETAILS"

    move-object/from16 v219, v4

    const/16 v4, 0xd9

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_TEAM_JOIN_FROM_OOB_LINK_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 219
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SF_TEAM_UNINVITE_DETAILS"

    move-object/from16 v220, v2

    const/16 v2, 0xda

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_TEAM_UNINVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 220
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_CONTENT_ADD_INVITEES_DETAILS"

    move-object/from16 v221, v4

    const/16 v4, 0xdb

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_ADD_INVITEES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 221
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_CONTENT_ADD_LINK_EXPIRY_DETAILS"

    move-object/from16 v222, v2

    const/16 v2, 0xdc

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_ADD_LINK_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 222
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_CONTENT_ADD_LINK_PASSWORD_DETAILS"

    move-object/from16 v223, v4

    const/16 v4, 0xdd

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_ADD_LINK_PASSWORD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 223
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_CONTENT_ADD_MEMBER_DETAILS"

    move-object/from16 v224, v2

    const/16 v2, 0xde

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_ADD_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 224
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_CONTENT_CHANGE_DOWNLOADS_POLICY_DETAILS"

    move-object/from16 v225, v4

    const/16 v4, 0xdf

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_CHANGE_DOWNLOADS_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 225
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_CONTENT_CHANGE_INVITEE_ROLE_DETAILS"

    move-object/from16 v226, v2

    const/16 v2, 0xe0

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_CHANGE_INVITEE_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 226
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_CONTENT_CHANGE_LINK_AUDIENCE_DETAILS"

    move-object/from16 v227, v4

    const/16 v4, 0xe1

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_CHANGE_LINK_AUDIENCE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 227
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_CONTENT_CHANGE_LINK_EXPIRY_DETAILS"

    move-object/from16 v228, v2

    const/16 v2, 0xe2

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_CHANGE_LINK_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 228
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_CONTENT_CHANGE_LINK_PASSWORD_DETAILS"

    move-object/from16 v229, v4

    const/16 v4, 0xe3

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_CHANGE_LINK_PASSWORD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 229
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_CONTENT_CHANGE_MEMBER_ROLE_DETAILS"

    move-object/from16 v230, v2

    const/16 v2, 0xe4

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_CHANGE_MEMBER_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 230
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_CONTENT_CHANGE_VIEWER_INFO_POLICY_DETAILS"

    move-object/from16 v231, v4

    const/16 v4, 0xe5

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_CHANGE_VIEWER_INFO_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 231
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_CONTENT_CLAIM_INVITATION_DETAILS"

    move-object/from16 v232, v2

    const/16 v2, 0xe6

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_CLAIM_INVITATION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 232
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_CONTENT_COPY_DETAILS"

    move-object/from16 v233, v4

    const/16 v4, 0xe7

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_COPY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 233
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_CONTENT_DOWNLOAD_DETAILS"

    move-object/from16 v234, v2

    const/16 v2, 0xe8

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 234
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_CONTENT_RELINQUISH_MEMBERSHIP_DETAILS"

    move-object/from16 v235, v4

    const/16 v4, 0xe9

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_RELINQUISH_MEMBERSHIP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 235
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_CONTENT_REMOVE_INVITEES_DETAILS"

    move-object/from16 v236, v2

    const/16 v2, 0xea

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_REMOVE_INVITEES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 236
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_CONTENT_REMOVE_LINK_EXPIRY_DETAILS"

    move-object/from16 v237, v4

    const/16 v4, 0xeb

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_REMOVE_LINK_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 237
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_CONTENT_REMOVE_LINK_PASSWORD_DETAILS"

    move-object/from16 v238, v2

    const/16 v2, 0xec

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_REMOVE_LINK_PASSWORD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 238
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_CONTENT_REMOVE_MEMBER_DETAILS"

    move-object/from16 v239, v4

    const/16 v4, 0xed

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_REMOVE_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 239
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_CONTENT_REQUEST_ACCESS_DETAILS"

    move-object/from16 v240, v2

    const/16 v2, 0xee

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_REQUEST_ACCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 240
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_CONTENT_RESTORE_INVITEES_DETAILS"

    move-object/from16 v241, v4

    const/16 v4, 0xef

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_RESTORE_INVITEES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 241
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_CONTENT_RESTORE_MEMBER_DETAILS"

    move-object/from16 v242, v2

    const/16 v2, 0xf0

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_RESTORE_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 242
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_CONTENT_UNSHARE_DETAILS"

    move-object/from16 v243, v4

    const/16 v4, 0xf1

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_UNSHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 243
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_CONTENT_VIEW_DETAILS"

    move-object/from16 v244, v2

    const/16 v2, 0xf2

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 244
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_FOLDER_CHANGE_LINK_POLICY_DETAILS"

    move-object/from16 v245, v4

    const/16 v4, 0xf3

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_CHANGE_LINK_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 245
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_FOLDER_CHANGE_MEMBERS_INHERITANCE_POLICY_DETAILS"

    move-object/from16 v246, v2

    const/16 v2, 0xf4

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_CHANGE_MEMBERS_INHERITANCE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 246
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_FOLDER_CHANGE_MEMBERS_MANAGEMENT_POLICY_DETAILS"

    move-object/from16 v247, v4

    const/16 v4, 0xf5

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_CHANGE_MEMBERS_MANAGEMENT_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 247
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_FOLDER_CHANGE_MEMBERS_POLICY_DETAILS"

    move-object/from16 v248, v2

    const/16 v2, 0xf6

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_CHANGE_MEMBERS_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 248
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_FOLDER_CREATE_DETAILS"

    move-object/from16 v249, v4

    const/16 v4, 0xf7

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 249
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_FOLDER_DECLINE_INVITATION_DETAILS"

    move-object/from16 v250, v2

    const/16 v2, 0xf8

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_DECLINE_INVITATION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 250
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_FOLDER_MOUNT_DETAILS"

    move-object/from16 v251, v4

    const/16 v4, 0xf9

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_MOUNT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 251
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_FOLDER_NEST_DETAILS"

    move-object/from16 v252, v2

    const/16 v2, 0xfa

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_NEST_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 252
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_FOLDER_TRANSFER_OWNERSHIP_DETAILS"

    move-object/from16 v253, v4

    const/16 v4, 0xfb

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_TRANSFER_OWNERSHIP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 253
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_FOLDER_UNMOUNT_DETAILS"

    move-object/from16 v254, v2

    const/16 v2, 0xfc

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_UNMOUNT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 254
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_LINK_ADD_EXPIRY_DETAILS"

    move-object/from16 v255, v4

    const/16 v4, 0xfd

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_ADD_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 255
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_LINK_CHANGE_EXPIRY_DETAILS"

    move-object/16 v256, v2

    const/16 v2, 0xfe

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_CHANGE_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 256
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_LINK_CHANGE_VISIBILITY_DETAILS"

    move-object/16 v257, v4

    const/16 v4, 0xff

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_CHANGE_VISIBILITY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 257
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_LINK_COPY_DETAILS"

    move-object/16 v258, v2

    const/16 v2, 0x100

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_COPY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 258
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_LINK_CREATE_DETAILS"

    move-object/16 v259, v4

    const/16 v4, 0x101

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 259
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_LINK_DISABLE_DETAILS"

    move-object/16 v260, v2

    const/16 v2, 0x102

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_DISABLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 260
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_LINK_DOWNLOAD_DETAILS"

    move-object/16 v261, v4

    const/16 v4, 0x103

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 261
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_LINK_REMOVE_EXPIRY_DETAILS"

    move-object/16 v262, v2

    const/16 v2, 0x104

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_REMOVE_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 262
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_LINK_SETTINGS_ADD_EXPIRATION_DETAILS"

    move-object/16 v263, v4

    const/16 v4, 0x105

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_SETTINGS_ADD_EXPIRATION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 263
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_LINK_SETTINGS_ADD_PASSWORD_DETAILS"

    move-object/16 v264, v2

    const/16 v2, 0x106

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_SETTINGS_ADD_PASSWORD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 264
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_LINK_SETTINGS_ALLOW_DOWNLOAD_DISABLED_DETAILS"

    move-object/16 v265, v4

    const/16 v4, 0x107

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_SETTINGS_ALLOW_DOWNLOAD_DISABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 265
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_LINK_SETTINGS_ALLOW_DOWNLOAD_ENABLED_DETAILS"

    move-object/16 v266, v2

    const/16 v2, 0x108

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_SETTINGS_ALLOW_DOWNLOAD_ENABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 266
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_LINK_SETTINGS_CHANGE_AUDIENCE_DETAILS"

    move-object/16 v267, v4

    const/16 v4, 0x109

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_SETTINGS_CHANGE_AUDIENCE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 267
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_LINK_SETTINGS_CHANGE_EXPIRATION_DETAILS"

    move-object/16 v268, v2

    const/16 v2, 0x10a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_SETTINGS_CHANGE_EXPIRATION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 268
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_LINK_SETTINGS_CHANGE_PASSWORD_DETAILS"

    move-object/16 v269, v4

    const/16 v4, 0x10b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_SETTINGS_CHANGE_PASSWORD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 269
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_LINK_SETTINGS_REMOVE_EXPIRATION_DETAILS"

    move-object/16 v270, v2

    const/16 v2, 0x10c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_SETTINGS_REMOVE_EXPIRATION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 270
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_LINK_SETTINGS_REMOVE_PASSWORD_DETAILS"

    move-object/16 v271, v4

    const/16 v4, 0x10d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_SETTINGS_REMOVE_PASSWORD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 271
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_LINK_SHARE_DETAILS"

    move-object/16 v272, v2

    const/16 v2, 0x10e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_SHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 272
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_LINK_VIEW_DETAILS"

    move-object/16 v273, v4

    const/16 v4, 0x10f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 273
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARED_NOTE_OPENED_DETAILS"

    move-object/16 v274, v2

    const/16 v2, 0x110

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_NOTE_OPENED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 274
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHMODEL_GROUP_SHARE_DETAILS"

    move-object/16 v275, v4

    const/16 v4, 0x111

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHMODEL_GROUP_SHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 275
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_ACCESS_GRANTED_DETAILS"

    move-object/16 v276, v2

    const/16 v2, 0x112

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_ACCESS_GRANTED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 276
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_ADD_MEMBER_DETAILS"

    move-object/16 v277, v4

    const/16 v4, 0x113

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_ADD_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 277
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_ARCHIVED_DETAILS"

    move-object/16 v278, v2

    const/16 v2, 0x114

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_ARCHIVED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 278
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_CREATED_DETAILS"

    move-object/16 v279, v4

    const/16 v4, 0x115

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_CREATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 279
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_DELETE_COMMENT_DETAILS"

    move-object/16 v280, v2

    const/16 v2, 0x116

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_DELETE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 280
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_EDITED_DETAILS"

    move-object/16 v281, v4

    const/16 v4, 0x117

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_EDITED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 281
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_EDIT_COMMENT_DETAILS"

    move-object/16 v282, v2

    const/16 v2, 0x118

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_EDIT_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 282
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_FILE_ADDED_DETAILS"

    move-object/16 v283, v4

    const/16 v4, 0x119

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_FILE_ADDED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 283
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_FILE_DOWNLOAD_DETAILS"

    move-object/16 v284, v2

    const/16 v2, 0x11a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_FILE_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 284
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_FILE_REMOVED_DETAILS"

    move-object/16 v285, v4

    const/16 v4, 0x11b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_FILE_REMOVED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 285
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_FILE_VIEW_DETAILS"

    move-object/16 v286, v2

    const/16 v2, 0x11c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_FILE_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 286
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_PERMANENTLY_DELETED_DETAILS"

    move-object/16 v287, v4

    const/16 v4, 0x11d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_PERMANENTLY_DELETED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 287
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_POST_COMMENT_DETAILS"

    move-object/16 v288, v2

    const/16 v2, 0x11e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_POST_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 288
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_REMOVE_MEMBER_DETAILS"

    move-object/16 v289, v4

    const/16 v4, 0x11f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_REMOVE_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 289
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_RENAMED_DETAILS"

    move-object/16 v290, v2

    const/16 v2, 0x120

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_RENAMED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 290
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_REQUEST_ACCESS_DETAILS"

    move-object/16 v291, v4

    const/16 v4, 0x121

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_REQUEST_ACCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 291
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_RESOLVE_COMMENT_DETAILS"

    move-object/16 v292, v2

    const/16 v2, 0x122

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_RESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 292
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_RESTORED_DETAILS"

    move-object/16 v293, v4

    const/16 v4, 0x123

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_RESTORED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 293
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_TRASHED_DETAILS"

    move-object/16 v294, v2

    const/16 v2, 0x124

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_TRASHED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 294
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_TRASHED_DEPRECATED_DETAILS"

    move-object/16 v295, v4

    const/16 v4, 0x125

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_TRASHED_DEPRECATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 295
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_UNRESOLVE_COMMENT_DETAILS"

    move-object/16 v296, v2

    const/16 v2, 0x126

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_UNRESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 296
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_UNTRASHED_DETAILS"

    move-object/16 v297, v4

    const/16 v4, 0x127

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_UNTRASHED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 297
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_UNTRASHED_DEPRECATED_DETAILS"

    move-object/16 v298, v2

    const/16 v2, 0x128

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_UNTRASHED_DEPRECATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 298
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_VIEW_DETAILS"

    move-object/16 v299, v4

    const/16 v4, 0x129

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 299
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SSO_ADD_CERT_DETAILS"

    move-object/16 v300, v2

    const/16 v2, 0x12a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_ADD_CERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 300
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SSO_ADD_LOGIN_URL_DETAILS"

    move-object/16 v301, v4

    const/16 v4, 0x12b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_ADD_LOGIN_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 301
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SSO_ADD_LOGOUT_URL_DETAILS"

    move-object/16 v302, v2

    const/16 v2, 0x12c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_ADD_LOGOUT_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 302
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SSO_CHANGE_CERT_DETAILS"

    move-object/16 v303, v4

    const/16 v4, 0x12d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_CHANGE_CERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 303
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SSO_CHANGE_LOGIN_URL_DETAILS"

    move-object/16 v304, v2

    const/16 v2, 0x12e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_CHANGE_LOGIN_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 304
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SSO_CHANGE_LOGOUT_URL_DETAILS"

    move-object/16 v305, v4

    const/16 v4, 0x12f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_CHANGE_LOGOUT_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 305
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SSO_CHANGE_SAML_IDENTITY_MODE_DETAILS"

    move-object/16 v306, v2

    const/16 v2, 0x130

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_CHANGE_SAML_IDENTITY_MODE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 306
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SSO_REMOVE_CERT_DETAILS"

    move-object/16 v307, v4

    const/16 v4, 0x131

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_REMOVE_CERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 307
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SSO_REMOVE_LOGIN_URL_DETAILS"

    move-object/16 v308, v2

    const/16 v2, 0x132

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_REMOVE_LOGIN_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 308
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SSO_REMOVE_LOGOUT_URL_DETAILS"

    move-object/16 v309, v4

    const/16 v4, 0x133

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_REMOVE_LOGOUT_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 309
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_FOLDER_CHANGE_STATUS_DETAILS"

    move-object/16 v310, v2

    const/16 v2, 0x134

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_FOLDER_CHANGE_STATUS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 310
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_FOLDER_CREATE_DETAILS"

    move-object/16 v311, v4

    const/16 v4, 0x135

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_FOLDER_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 311
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_FOLDER_DOWNGRADE_DETAILS"

    move-object/16 v312, v2

    const/16 v2, 0x136

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_FOLDER_DOWNGRADE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 312
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_FOLDER_PERMANENTLY_DELETE_DETAILS"

    move-object/16 v313, v4

    const/16 v4, 0x137

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_FOLDER_PERMANENTLY_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 313
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_FOLDER_RENAME_DETAILS"

    move-object/16 v314, v2

    const/16 v2, 0x138

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_FOLDER_RENAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 314
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_SELECTIVE_SYNC_SETTINGS_CHANGED_DETAILS"

    move-object/16 v315, v4

    const/16 v4, 0x139

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_SELECTIVE_SYNC_SETTINGS_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 315
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "ACCOUNT_CAPTURE_CHANGE_POLICY_DETAILS"

    move-object/16 v316, v2

    const/16 v2, 0x13a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ACCOUNT_CAPTURE_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 316
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "ALLOW_DOWNLOAD_DISABLED_DETAILS"

    move-object/16 v317, v4

    const/16 v4, 0x13b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ALLOW_DOWNLOAD_DISABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 317
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "ALLOW_DOWNLOAD_ENABLED_DETAILS"

    move-object/16 v318, v2

    const/16 v2, 0x13c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ALLOW_DOWNLOAD_ENABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 318
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "CAMERA_UPLOADS_POLICY_CHANGED_DETAILS"

    move-object/16 v319, v4

    const/16 v4, 0x13d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->CAMERA_UPLOADS_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 319
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "DATA_PLACEMENT_RESTRICTION_CHANGE_POLICY_DETAILS"

    move-object/16 v320, v2

    const/16 v2, 0x13e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DATA_PLACEMENT_RESTRICTION_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 320
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "DATA_PLACEMENT_RESTRICTION_SATISFY_POLICY_DETAILS"

    move-object/16 v321, v4

    const/16 v4, 0x13f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DATA_PLACEMENT_RESTRICTION_SATISFY_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 321
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "DEVICE_APPROVALS_ADD_EXCEPTION_DETAILS"

    move-object/16 v322, v2

    const/16 v2, 0x140

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_APPROVALS_ADD_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 322
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "DEVICE_APPROVALS_CHANGE_DESKTOP_POLICY_DETAILS"

    move-object/16 v323, v4

    const/16 v4, 0x141

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_APPROVALS_CHANGE_DESKTOP_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 323
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "DEVICE_APPROVALS_CHANGE_MOBILE_POLICY_DETAILS"

    move-object/16 v324, v2

    const/16 v2, 0x142

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_APPROVALS_CHANGE_MOBILE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 324
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "DEVICE_APPROVALS_CHANGE_OVERAGE_ACTION_DETAILS"

    move-object/16 v325, v4

    const/16 v4, 0x143

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_APPROVALS_CHANGE_OVERAGE_ACTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 325
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "DEVICE_APPROVALS_CHANGE_UNLINK_ACTION_DETAILS"

    move-object/16 v326, v2

    const/16 v2, 0x144

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_APPROVALS_CHANGE_UNLINK_ACTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 326
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "DEVICE_APPROVALS_REMOVE_EXCEPTION_DETAILS"

    move-object/16 v327, v4

    const/16 v4, 0x145

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_APPROVALS_REMOVE_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 327
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "DIRECTORY_RESTRICTIONS_ADD_MEMBERS_DETAILS"

    move-object/16 v328, v2

    const/16 v2, 0x146

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DIRECTORY_RESTRICTIONS_ADD_MEMBERS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 328
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "DIRECTORY_RESTRICTIONS_REMOVE_MEMBERS_DETAILS"

    move-object/16 v329, v4

    const/16 v4, 0x147

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DIRECTORY_RESTRICTIONS_REMOVE_MEMBERS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 329
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "EMM_ADD_EXCEPTION_DETAILS"

    move-object/16 v330, v2

    const/16 v2, 0x148

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EMM_ADD_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 330
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "EMM_CHANGE_POLICY_DETAILS"

    move-object/16 v331, v4

    const/16 v4, 0x149

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EMM_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 331
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "EMM_REMOVE_EXCEPTION_DETAILS"

    move-object/16 v332, v2

    const/16 v2, 0x14a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EMM_REMOVE_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 332
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "EXTENDED_VERSION_HISTORY_CHANGE_POLICY_DETAILS"

    move-object/16 v333, v4

    const/16 v4, 0x14b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EXTENDED_VERSION_HISTORY_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 333
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_COMMENTS_CHANGE_POLICY_DETAILS"

    move-object/16 v334, v2

    const/16 v2, 0x14c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_COMMENTS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 334
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_LOCKING_POLICY_CHANGED_DETAILS"

    move-object/16 v335, v4

    const/16 v4, 0x14d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_LOCKING_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 335
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_REQUESTS_CHANGE_POLICY_DETAILS"

    move-object/16 v336, v2

    const/16 v2, 0x14e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REQUESTS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 336
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_REQUESTS_EMAILS_ENABLED_DETAILS"

    move-object/16 v337, v4

    const/16 v4, 0x14f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REQUESTS_EMAILS_ENABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 337
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_REQUESTS_EMAILS_RESTRICTED_TO_TEAM_ONLY_DETAILS"

    move-object/16 v338, v2

    const/16 v2, 0x150

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REQUESTS_EMAILS_RESTRICTED_TO_TEAM_ONLY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 338
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "FILE_TRANSFERS_POLICY_CHANGED_DETAILS"

    move-object/16 v339, v4

    const/16 v4, 0x151

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_TRANSFERS_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 339
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "GOOGLE_SSO_CHANGE_POLICY_DETAILS"

    move-object/16 v340, v2

    const/16 v2, 0x152

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GOOGLE_SSO_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 340
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "GROUP_USER_MANAGEMENT_CHANGE_POLICY_DETAILS"

    move-object/16 v341, v4

    const/16 v4, 0x153

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_USER_MANAGEMENT_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 341
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "INTEGRATION_POLICY_CHANGED_DETAILS"

    move-object/16 v342, v2

    const/16 v2, 0x154

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->INTEGRATION_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 342
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_REQUESTS_CHANGE_POLICY_DETAILS"

    move-object/16 v343, v4

    const/16 v4, 0x155

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_REQUESTS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 343
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_SEND_INVITE_POLICY_CHANGED_DETAILS"

    move-object/16 v344, v2

    const/16 v2, 0x156

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SEND_INVITE_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 344
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_SPACE_LIMITS_ADD_EXCEPTION_DETAILS"

    move-object/16 v345, v4

    const/16 v4, 0x157

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SPACE_LIMITS_ADD_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 345
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_SPACE_LIMITS_CHANGE_CAPS_TYPE_POLICY_DETAILS"

    move-object/16 v346, v2

    const/16 v2, 0x158

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SPACE_LIMITS_CHANGE_CAPS_TYPE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 346
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_SPACE_LIMITS_CHANGE_POLICY_DETAILS"

    move-object/16 v347, v4

    const/16 v4, 0x159

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SPACE_LIMITS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 347
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_SPACE_LIMITS_REMOVE_EXCEPTION_DETAILS"

    move-object/16 v348, v2

    const/16 v2, 0x15a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SPACE_LIMITS_REMOVE_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 348
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MEMBER_SUGGESTIONS_CHANGE_POLICY_DETAILS"

    move-object/16 v349, v4

    const/16 v4, 0x15b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SUGGESTIONS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 349
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MICROSOFT_OFFICE_ADDIN_CHANGE_POLICY_DETAILS"

    move-object/16 v350, v2

    const/16 v2, 0x15c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MICROSOFT_OFFICE_ADDIN_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 350
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "NETWORK_CONTROL_CHANGE_POLICY_DETAILS"

    move-object/16 v351, v4

    const/16 v4, 0x15d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->NETWORK_CONTROL_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 351
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_CHANGE_DEPLOYMENT_POLICY_DETAILS"

    move-object/16 v352, v2

    const/16 v2, 0x15e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CHANGE_DEPLOYMENT_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 352
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_CHANGE_MEMBER_LINK_POLICY_DETAILS"

    move-object/16 v353, v4

    const/16 v4, 0x15f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CHANGE_MEMBER_LINK_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 353
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_CHANGE_MEMBER_POLICY_DETAILS"

    move-object/16 v354, v2

    const/16 v2, 0x160

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CHANGE_MEMBER_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 354
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_CHANGE_POLICY_DETAILS"

    move-object/16 v355, v4

    const/16 v4, 0x161

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 355
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_DEFAULT_FOLDER_POLICY_CHANGED_DETAILS"

    move-object/16 v356, v2

    const/16 v2, 0x162

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DEFAULT_FOLDER_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 356
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_DESKTOP_POLICY_CHANGED_DETAILS"

    move-object/16 v357, v4

    const/16 v4, 0x163

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DESKTOP_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 357
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_ENABLED_USERS_GROUP_ADDITION_DETAILS"

    move-object/16 v358, v2

    const/16 v2, 0x164

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_ENABLED_USERS_GROUP_ADDITION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 358
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PAPER_ENABLED_USERS_GROUP_REMOVAL_DETAILS"

    move-object/16 v359, v4

    const/16 v4, 0x165

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_ENABLED_USERS_GROUP_REMOVAL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 359
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PASSWORD_STRENGTH_REQUIREMENTS_CHANGE_POLICY_DETAILS"

    move-object/16 v360, v2

    const/16 v2, 0x166

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PASSWORD_STRENGTH_REQUIREMENTS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 360
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "PERMANENT_DELETE_CHANGE_POLICY_DETAILS"

    move-object/16 v361, v4

    const/16 v4, 0x167

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PERMANENT_DELETE_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 361
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "RESELLER_SUPPORT_CHANGE_POLICY_DETAILS"

    move-object/16 v362, v2

    const/16 v2, 0x168

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->RESELLER_SUPPORT_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 362
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "REWIND_POLICY_CHANGED_DETAILS"

    move-object/16 v363, v4

    const/16 v4, 0x169

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->REWIND_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 363
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARING_CHANGE_FOLDER_JOIN_POLICY_DETAILS"

    move-object/16 v364, v2

    const/16 v2, 0x16a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARING_CHANGE_FOLDER_JOIN_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 364
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARING_CHANGE_LINK_POLICY_DETAILS"

    move-object/16 v365, v4

    const/16 v4, 0x16b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARING_CHANGE_LINK_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 365
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHARING_CHANGE_MEMBER_POLICY_DETAILS"

    move-object/16 v366, v2

    const/16 v2, 0x16c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARING_CHANGE_MEMBER_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 366
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_CHANGE_DOWNLOAD_POLICY_DETAILS"

    move-object/16 v367, v4

    const/16 v4, 0x16d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_CHANGE_DOWNLOAD_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 367
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_CHANGE_ENABLED_POLICY_DETAILS"

    move-object/16 v368, v2

    const/16 v2, 0x16e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_CHANGE_ENABLED_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 368
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SHOWCASE_CHANGE_EXTERNAL_SHARING_POLICY_DETAILS"

    move-object/16 v369, v4

    const/16 v4, 0x16f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_CHANGE_EXTERNAL_SHARING_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 369
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SMARTER_SMART_SYNC_POLICY_CHANGED_DETAILS"

    move-object/16 v370, v2

    const/16 v2, 0x170

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SMARTER_SMART_SYNC_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 370
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SMART_SYNC_CHANGE_POLICY_DETAILS"

    move-object/16 v371, v4

    const/16 v4, 0x171

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SMART_SYNC_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 371
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SMART_SYNC_NOT_OPT_OUT_DETAILS"

    move-object/16 v372, v2

    const/16 v2, 0x172

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SMART_SYNC_NOT_OPT_OUT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 372
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SMART_SYNC_OPT_OUT_DETAILS"

    move-object/16 v373, v4

    const/16 v4, 0x173

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SMART_SYNC_OPT_OUT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 373
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "SSO_CHANGE_POLICY_DETAILS"

    move-object/16 v374, v2

    const/16 v2, 0x174

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 374
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_EXTENSIONS_POLICY_CHANGED_DETAILS"

    move-object/16 v375, v4

    const/16 v4, 0x175

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_EXTENSIONS_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 375
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_SELECTIVE_SYNC_POLICY_CHANGED_DETAILS"

    move-object/16 v376, v2

    const/16 v2, 0x176

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_SELECTIVE_SYNC_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 376
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_SHARING_WHITELIST_SUBJECTS_CHANGED_DETAILS"

    move-object/16 v377, v4

    const/16 v4, 0x177

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_SHARING_WHITELIST_SUBJECTS_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 377
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TFA_ADD_EXCEPTION_DETAILS"

    move-object/16 v378, v2

    const/16 v2, 0x178

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_ADD_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 378
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TFA_CHANGE_POLICY_DETAILS"

    move-object/16 v379, v4

    const/16 v4, 0x179

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 379
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TFA_REMOVE_EXCEPTION_DETAILS"

    move-object/16 v380, v2

    const/16 v2, 0x17a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_REMOVE_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 380
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TWO_ACCOUNT_CHANGE_POLICY_DETAILS"

    move-object/16 v381, v4

    const/16 v4, 0x17b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TWO_ACCOUNT_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 381
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "VIEWER_INFO_POLICY_CHANGED_DETAILS"

    move-object/16 v382, v2

    const/16 v2, 0x17c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->VIEWER_INFO_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 382
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "WATERMARKING_POLICY_CHANGED_DETAILS"

    move-object/16 v383, v4

    const/16 v4, 0x17d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->WATERMARKING_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 383
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "WEB_SESSIONS_CHANGE_ACTIVE_SESSION_LIMIT_DETAILS"

    move-object/16 v384, v2

    const/16 v2, 0x17e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->WEB_SESSIONS_CHANGE_ACTIVE_SESSION_LIMIT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 384
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "WEB_SESSIONS_CHANGE_FIXED_LENGTH_POLICY_DETAILS"

    move-object/16 v385, v4

    const/16 v4, 0x17f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->WEB_SESSIONS_CHANGE_FIXED_LENGTH_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 385
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "WEB_SESSIONS_CHANGE_IDLE_LENGTH_POLICY_DETAILS"

    move-object/16 v386, v2

    const/16 v2, 0x180

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->WEB_SESSIONS_CHANGE_IDLE_LENGTH_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 386
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_MERGE_FROM_DETAILS"

    move-object/16 v387, v4

    const/16 v4, 0x181

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_MERGE_FROM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 387
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_MERGE_TO_DETAILS"

    move-object/16 v388, v2

    const/16 v2, 0x182

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_MERGE_TO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 388
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_PROFILE_ADD_LOGO_DETAILS"

    move-object/16 v389, v4

    const/16 v4, 0x183

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_PROFILE_ADD_LOGO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 389
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_PROFILE_CHANGE_DEFAULT_LANGUAGE_DETAILS"

    move-object/16 v390, v2

    const/16 v2, 0x184

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_PROFILE_CHANGE_DEFAULT_LANGUAGE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 390
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_PROFILE_CHANGE_LOGO_DETAILS"

    move-object/16 v391, v4

    const/16 v4, 0x185

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_PROFILE_CHANGE_LOGO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 391
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_PROFILE_CHANGE_NAME_DETAILS"

    move-object/16 v392, v2

    const/16 v2, 0x186

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_PROFILE_CHANGE_NAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 392
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_PROFILE_REMOVE_LOGO_DETAILS"

    move-object/16 v393, v4

    const/16 v4, 0x187

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_PROFILE_REMOVE_LOGO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 393
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TFA_ADD_BACKUP_PHONE_DETAILS"

    move-object/16 v394, v2

    const/16 v2, 0x188

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_ADD_BACKUP_PHONE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 394
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TFA_ADD_SECURITY_KEY_DETAILS"

    move-object/16 v395, v4

    const/16 v4, 0x189

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_ADD_SECURITY_KEY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 395
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TFA_CHANGE_BACKUP_PHONE_DETAILS"

    move-object/16 v396, v2

    const/16 v2, 0x18a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_CHANGE_BACKUP_PHONE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 396
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TFA_CHANGE_STATUS_DETAILS"

    move-object/16 v397, v4

    const/16 v4, 0x18b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_CHANGE_STATUS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 397
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TFA_REMOVE_BACKUP_PHONE_DETAILS"

    move-object/16 v398, v2

    const/16 v2, 0x18c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_REMOVE_BACKUP_PHONE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 398
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TFA_REMOVE_SECURITY_KEY_DETAILS"

    move-object/16 v399, v4

    const/16 v4, 0x18d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_REMOVE_SECURITY_KEY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 399
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TFA_RESET_DETAILS"

    move-object/16 v400, v2

    const/16 v2, 0x18e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_RESET_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 400
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "CHANGED_ENTERPRISE_ADMIN_ROLE_DETAILS"

    move-object/16 v401, v4

    const/16 v4, 0x18f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->CHANGED_ENTERPRISE_ADMIN_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 401
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "CHANGED_ENTERPRISE_CONNECTED_TEAM_STATUS_DETAILS"

    move-object/16 v402, v2

    const/16 v2, 0x190

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->CHANGED_ENTERPRISE_CONNECTED_TEAM_STATUS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 402
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "ENDED_ENTERPRISE_ADMIN_SESSION_DETAILS"

    move-object/16 v403, v4

    const/16 v4, 0x191

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ENDED_ENTERPRISE_ADMIN_SESSION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 403
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "ENDED_ENTERPRISE_ADMIN_SESSION_DEPRECATED_DETAILS"

    move-object/16 v404, v2

    const/16 v2, 0x192

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ENDED_ENTERPRISE_ADMIN_SESSION_DEPRECATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 404
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "ENTERPRISE_SETTINGS_LOCKING_DETAILS"

    move-object/16 v405, v4

    const/16 v4, 0x193

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ENTERPRISE_SETTINGS_LOCKING_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 405
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "GUEST_ADMIN_CHANGE_STATUS_DETAILS"

    move-object/16 v406, v2

    const/16 v2, 0x194

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GUEST_ADMIN_CHANGE_STATUS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 406
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "STARTED_ENTERPRISE_ADMIN_SESSION_DETAILS"

    move-object/16 v407, v4

    const/16 v4, 0x195

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->STARTED_ENTERPRISE_ADMIN_SESSION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 407
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_MERGE_REQUEST_ACCEPTED_DETAILS"

    move-object/16 v408, v2

    const/16 v2, 0x196

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_MERGE_REQUEST_ACCEPTED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 408
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_MERGE_REQUEST_ACCEPTED_SHOWN_TO_PRIMARY_TEAM_DETAILS"

    move-object/16 v409, v4

    const/16 v4, 0x197

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_MERGE_REQUEST_ACCEPTED_SHOWN_TO_PRIMARY_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 409
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_MERGE_REQUEST_ACCEPTED_SHOWN_TO_SECONDARY_TEAM_DETAILS"

    move-object/16 v410, v2

    const/16 v2, 0x198

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_MERGE_REQUEST_ACCEPTED_SHOWN_TO_SECONDARY_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 410
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_MERGE_REQUEST_AUTO_CANCELED_DETAILS"

    move-object/16 v411, v4

    const/16 v4, 0x199

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_MERGE_REQUEST_AUTO_CANCELED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 411
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_MERGE_REQUEST_CANCELED_DETAILS"

    move-object/16 v412, v2

    const/16 v2, 0x19a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_MERGE_REQUEST_CANCELED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 412
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_MERGE_REQUEST_CANCELED_SHOWN_TO_PRIMARY_TEAM_DETAILS"

    move-object/16 v413, v4

    const/16 v4, 0x19b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_MERGE_REQUEST_CANCELED_SHOWN_TO_PRIMARY_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 413
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_MERGE_REQUEST_CANCELED_SHOWN_TO_SECONDARY_TEAM_DETAILS"

    move-object/16 v414, v2

    const/16 v2, 0x19c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_MERGE_REQUEST_CANCELED_SHOWN_TO_SECONDARY_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 414
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_MERGE_REQUEST_EXPIRED_DETAILS"

    move-object/16 v415, v4

    const/16 v4, 0x19d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_MERGE_REQUEST_EXPIRED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 415
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_MERGE_REQUEST_EXPIRED_SHOWN_TO_PRIMARY_TEAM_DETAILS"

    move-object/16 v416, v2

    const/16 v2, 0x19e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_MERGE_REQUEST_EXPIRED_SHOWN_TO_PRIMARY_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 416
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_MERGE_REQUEST_EXPIRED_SHOWN_TO_SECONDARY_TEAM_DETAILS"

    move-object/16 v417, v4

    const/16 v4, 0x19f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_MERGE_REQUEST_EXPIRED_SHOWN_TO_SECONDARY_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 417
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_MERGE_REQUEST_REJECTED_SHOWN_TO_PRIMARY_TEAM_DETAILS"

    move-object/16 v418, v2

    const/16 v2, 0x1a0

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_MERGE_REQUEST_REJECTED_SHOWN_TO_PRIMARY_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 418
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_MERGE_REQUEST_REJECTED_SHOWN_TO_SECONDARY_TEAM_DETAILS"

    move-object/16 v419, v4

    const/16 v4, 0x1a1

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_MERGE_REQUEST_REJECTED_SHOWN_TO_SECONDARY_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 419
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_MERGE_REQUEST_REMINDER_DETAILS"

    move-object/16 v420, v2

    const/16 v2, 0x1a2

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_MERGE_REQUEST_REMINDER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 420
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_MERGE_REQUEST_REMINDER_SHOWN_TO_PRIMARY_TEAM_DETAILS"

    move-object/16 v421, v4

    const/16 v4, 0x1a3

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_MERGE_REQUEST_REMINDER_SHOWN_TO_PRIMARY_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 421
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_MERGE_REQUEST_REMINDER_SHOWN_TO_SECONDARY_TEAM_DETAILS"

    move-object/16 v422, v2

    const/16 v2, 0x1a4

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_MERGE_REQUEST_REMINDER_SHOWN_TO_SECONDARY_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 422
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_MERGE_REQUEST_REVOKED_DETAILS"

    move-object/16 v423, v4

    const/16 v4, 0x1a5

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_MERGE_REQUEST_REVOKED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 423
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_MERGE_REQUEST_SENT_SHOWN_TO_PRIMARY_TEAM_DETAILS"

    move-object/16 v424, v2

    const/16 v2, 0x1a6

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_MERGE_REQUEST_SENT_SHOWN_TO_PRIMARY_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 424
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "TEAM_MERGE_REQUEST_SENT_SHOWN_TO_SECONDARY_TEAM_DETAILS"

    move-object/16 v425, v4

    const/16 v4, 0x1a7

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_MERGE_REQUEST_SENT_SHOWN_TO_SECONDARY_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 425
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "MISSING_DETAILS"

    move-object/16 v426, v2

    const/16 v2, 0x1a8

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MISSING_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 426
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v6, "OTHER"

    move-object/16 v427, v4

    const/16 v4, 0x1a9

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->OTHER:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v4, 0x1aa

    new-array v4, v4, [Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/4 v6, 0x0

    aput-object v0, v4, v6

    const/4 v0, 0x1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    aput-object v3, v4, v0

    const/4 v0, 0x3

    aput-object v5, v4, v0

    const/4 v0, 0x4

    aput-object v7, v4, v0

    const/4 v0, 0x5

    aput-object v9, v4, v0

    const/4 v0, 0x6

    aput-object v11, v4, v0

    const/4 v0, 0x7

    aput-object v13, v4, v0

    const/16 v0, 0x8

    aput-object v15, v4, v0

    const/16 v0, 0x9

    aput-object v14, v4, v0

    const/16 v0, 0xa

    aput-object v12, v4, v0

    const/16 v0, 0xb

    aput-object v10, v4, v0

    const/16 v0, 0xc

    aput-object v8, v4, v0

    const/16 v0, 0xd

    aput-object v16, v4, v0

    const/16 v0, 0xe

    aput-object v17, v4, v0

    const/16 v0, 0xf

    aput-object v18, v4, v0

    const/16 v0, 0x10

    aput-object v19, v4, v0

    const/16 v0, 0x11

    aput-object v20, v4, v0

    const/16 v0, 0x12

    aput-object v21, v4, v0

    const/16 v0, 0x13

    aput-object v22, v4, v0

    const/16 v0, 0x14

    aput-object v23, v4, v0

    const/16 v0, 0x15

    aput-object v24, v4, v0

    const/16 v0, 0x16

    aput-object v25, v4, v0

    const/16 v0, 0x17

    aput-object v26, v4, v0

    const/16 v0, 0x18

    aput-object v27, v4, v0

    const/16 v0, 0x19

    aput-object v28, v4, v0

    const/16 v0, 0x1a

    aput-object v29, v4, v0

    const/16 v0, 0x1b

    aput-object v30, v4, v0

    const/16 v0, 0x1c

    aput-object v31, v4, v0

    const/16 v0, 0x1d

    aput-object v32, v4, v0

    const/16 v0, 0x1e

    aput-object v33, v4, v0

    const/16 v0, 0x1f

    aput-object v34, v4, v0

    const/16 v0, 0x20

    aput-object v35, v4, v0

    const/16 v0, 0x21

    aput-object v36, v4, v0

    const/16 v0, 0x22

    aput-object v37, v4, v0

    const/16 v0, 0x23

    aput-object v38, v4, v0

    const/16 v0, 0x24

    aput-object v39, v4, v0

    const/16 v0, 0x25

    aput-object v40, v4, v0

    const/16 v0, 0x26

    aput-object v41, v4, v0

    const/16 v0, 0x27

    aput-object v42, v4, v0

    const/16 v0, 0x28

    aput-object v43, v4, v0

    const/16 v0, 0x29

    aput-object v44, v4, v0

    const/16 v0, 0x2a

    aput-object v45, v4, v0

    const/16 v0, 0x2b

    aput-object v46, v4, v0

    const/16 v0, 0x2c

    aput-object v47, v4, v0

    const/16 v0, 0x2d

    aput-object v48, v4, v0

    const/16 v0, 0x2e

    aput-object v49, v4, v0

    const/16 v0, 0x2f

    aput-object v50, v4, v0

    const/16 v0, 0x30

    aput-object v51, v4, v0

    const/16 v0, 0x31

    aput-object v52, v4, v0

    const/16 v0, 0x32

    aput-object v53, v4, v0

    const/16 v0, 0x33

    aput-object v54, v4, v0

    const/16 v0, 0x34

    aput-object v55, v4, v0

    const/16 v0, 0x35

    aput-object v56, v4, v0

    const/16 v0, 0x36

    aput-object v57, v4, v0

    const/16 v0, 0x37

    aput-object v58, v4, v0

    const/16 v0, 0x38

    aput-object v59, v4, v0

    const/16 v0, 0x39

    aput-object v60, v4, v0

    const/16 v0, 0x3a

    aput-object v61, v4, v0

    const/16 v0, 0x3b

    aput-object v62, v4, v0

    const/16 v0, 0x3c

    aput-object v63, v4, v0

    const/16 v0, 0x3d

    aput-object v64, v4, v0

    const/16 v0, 0x3e

    aput-object v65, v4, v0

    const/16 v0, 0x3f

    aput-object v66, v4, v0

    const/16 v0, 0x40

    aput-object v67, v4, v0

    const/16 v0, 0x41

    aput-object v68, v4, v0

    const/16 v0, 0x42

    aput-object v69, v4, v0

    const/16 v0, 0x43

    aput-object v70, v4, v0

    const/16 v0, 0x44

    aput-object v71, v4, v0

    const/16 v0, 0x45

    aput-object v72, v4, v0

    const/16 v0, 0x46

    aput-object v73, v4, v0

    const/16 v0, 0x47

    aput-object v74, v4, v0

    const/16 v0, 0x48

    aput-object v75, v4, v0

    const/16 v0, 0x49

    aput-object v76, v4, v0

    const/16 v0, 0x4a

    aput-object v77, v4, v0

    const/16 v0, 0x4b

    aput-object v78, v4, v0

    const/16 v0, 0x4c

    aput-object v79, v4, v0

    const/16 v0, 0x4d

    aput-object v80, v4, v0

    const/16 v0, 0x4e

    aput-object v81, v4, v0

    const/16 v0, 0x4f

    aput-object v82, v4, v0

    const/16 v0, 0x50

    aput-object v83, v4, v0

    const/16 v0, 0x51

    aput-object v84, v4, v0

    const/16 v0, 0x52

    aput-object v85, v4, v0

    const/16 v0, 0x53

    aput-object v86, v4, v0

    const/16 v0, 0x54

    aput-object v87, v4, v0

    const/16 v0, 0x55

    aput-object v88, v4, v0

    const/16 v0, 0x56

    aput-object v89, v4, v0

    const/16 v0, 0x57

    aput-object v90, v4, v0

    const/16 v0, 0x58

    aput-object v91, v4, v0

    const/16 v0, 0x59

    aput-object v92, v4, v0

    const/16 v0, 0x5a

    aput-object v93, v4, v0

    const/16 v0, 0x5b

    aput-object v94, v4, v0

    const/16 v0, 0x5c

    aput-object v95, v4, v0

    const/16 v0, 0x5d

    aput-object v96, v4, v0

    const/16 v0, 0x5e

    aput-object v97, v4, v0

    const/16 v0, 0x5f

    aput-object v98, v4, v0

    const/16 v0, 0x60

    aput-object v99, v4, v0

    const/16 v0, 0x61

    aput-object v100, v4, v0

    const/16 v0, 0x62

    aput-object v101, v4, v0

    const/16 v0, 0x63

    aput-object v102, v4, v0

    const/16 v0, 0x64

    aput-object v103, v4, v0

    const/16 v0, 0x65

    aput-object v104, v4, v0

    const/16 v0, 0x66

    aput-object v105, v4, v0

    const/16 v0, 0x67

    aput-object v106, v4, v0

    const/16 v0, 0x68

    aput-object v107, v4, v0

    const/16 v0, 0x69

    aput-object v108, v4, v0

    const/16 v0, 0x6a

    aput-object v109, v4, v0

    const/16 v0, 0x6b

    aput-object v110, v4, v0

    const/16 v0, 0x6c

    aput-object v111, v4, v0

    const/16 v0, 0x6d

    aput-object v112, v4, v0

    const/16 v0, 0x6e

    aput-object v113, v4, v0

    const/16 v0, 0x6f

    aput-object v114, v4, v0

    const/16 v0, 0x70

    aput-object v115, v4, v0

    const/16 v0, 0x71

    aput-object v116, v4, v0

    const/16 v0, 0x72

    aput-object v117, v4, v0

    const/16 v0, 0x73

    aput-object v118, v4, v0

    const/16 v0, 0x74

    aput-object v119, v4, v0

    const/16 v0, 0x75

    aput-object v120, v4, v0

    const/16 v0, 0x76

    aput-object v121, v4, v0

    const/16 v0, 0x77

    aput-object v122, v4, v0

    const/16 v0, 0x78

    aput-object v123, v4, v0

    const/16 v0, 0x79

    aput-object v124, v4, v0

    const/16 v0, 0x7a

    aput-object v125, v4, v0

    const/16 v0, 0x7b

    aput-object v126, v4, v0

    const/16 v0, 0x7c

    aput-object v127, v4, v0

    const/16 v0, 0x7d

    aput-object v128, v4, v0

    const/16 v0, 0x7e

    aput-object v129, v4, v0

    const/16 v0, 0x7f

    aput-object v130, v4, v0

    const/16 v0, 0x80

    aput-object v131, v4, v0

    const/16 v0, 0x81

    aput-object v132, v4, v0

    const/16 v0, 0x82

    aput-object v133, v4, v0

    const/16 v0, 0x83

    aput-object v134, v4, v0

    const/16 v0, 0x84

    aput-object v135, v4, v0

    const/16 v0, 0x85

    aput-object v136, v4, v0

    const/16 v0, 0x86

    aput-object v137, v4, v0

    const/16 v0, 0x87

    aput-object v138, v4, v0

    const/16 v0, 0x88

    aput-object v139, v4, v0

    const/16 v0, 0x89

    aput-object v140, v4, v0

    const/16 v0, 0x8a

    aput-object v141, v4, v0

    const/16 v0, 0x8b

    aput-object v142, v4, v0

    const/16 v0, 0x8c

    aput-object v143, v4, v0

    const/16 v0, 0x8d

    aput-object v144, v4, v0

    const/16 v0, 0x8e

    aput-object v145, v4, v0

    const/16 v0, 0x8f

    aput-object v146, v4, v0

    const/16 v0, 0x90

    aput-object v147, v4, v0

    const/16 v0, 0x91

    aput-object v148, v4, v0

    const/16 v0, 0x92

    aput-object v149, v4, v0

    const/16 v0, 0x93

    aput-object v150, v4, v0

    const/16 v0, 0x94

    aput-object v151, v4, v0

    const/16 v0, 0x95

    aput-object v152, v4, v0

    const/16 v0, 0x96

    aput-object v153, v4, v0

    const/16 v0, 0x97

    aput-object v154, v4, v0

    const/16 v0, 0x98

    aput-object v155, v4, v0

    const/16 v0, 0x99

    aput-object v156, v4, v0

    const/16 v0, 0x9a

    aput-object v157, v4, v0

    const/16 v0, 0x9b

    aput-object v158, v4, v0

    const/16 v0, 0x9c

    aput-object v159, v4, v0

    const/16 v0, 0x9d

    aput-object v160, v4, v0

    const/16 v0, 0x9e

    aput-object v161, v4, v0

    const/16 v0, 0x9f

    aput-object v162, v4, v0

    const/16 v0, 0xa0

    aput-object v163, v4, v0

    const/16 v0, 0xa1

    aput-object v164, v4, v0

    const/16 v0, 0xa2

    aput-object v165, v4, v0

    const/16 v0, 0xa3

    aput-object v166, v4, v0

    const/16 v0, 0xa4

    aput-object v167, v4, v0

    const/16 v0, 0xa5

    aput-object v168, v4, v0

    const/16 v0, 0xa6

    aput-object v169, v4, v0

    const/16 v0, 0xa7

    aput-object v170, v4, v0

    const/16 v0, 0xa8

    aput-object v171, v4, v0

    const/16 v0, 0xa9

    aput-object v172, v4, v0

    const/16 v0, 0xaa

    aput-object v173, v4, v0

    const/16 v0, 0xab

    aput-object v174, v4, v0

    const/16 v0, 0xac

    aput-object v175, v4, v0

    const/16 v0, 0xad

    aput-object v176, v4, v0

    const/16 v0, 0xae

    aput-object v177, v4, v0

    const/16 v0, 0xaf

    aput-object v178, v4, v0

    const/16 v0, 0xb0

    aput-object v179, v4, v0

    const/16 v0, 0xb1

    aput-object v180, v4, v0

    const/16 v0, 0xb2

    aput-object v181, v4, v0

    const/16 v0, 0xb3

    aput-object v182, v4, v0

    const/16 v0, 0xb4

    aput-object v183, v4, v0

    const/16 v0, 0xb5

    aput-object v184, v4, v0

    const/16 v0, 0xb6

    aput-object v185, v4, v0

    const/16 v0, 0xb7

    aput-object v186, v4, v0

    const/16 v0, 0xb8

    aput-object v187, v4, v0

    const/16 v0, 0xb9

    aput-object v188, v4, v0

    const/16 v0, 0xba

    aput-object v189, v4, v0

    const/16 v0, 0xbb

    aput-object v190, v4, v0

    const/16 v0, 0xbc

    aput-object v191, v4, v0

    const/16 v0, 0xbd

    aput-object v192, v4, v0

    const/16 v0, 0xbe

    aput-object v193, v4, v0

    const/16 v0, 0xbf

    aput-object v194, v4, v0

    const/16 v0, 0xc0

    aput-object v195, v4, v0

    const/16 v0, 0xc1

    aput-object v196, v4, v0

    const/16 v0, 0xc2

    aput-object v197, v4, v0

    const/16 v0, 0xc3

    aput-object v198, v4, v0

    const/16 v0, 0xc4

    aput-object v199, v4, v0

    const/16 v0, 0xc5

    aput-object v200, v4, v0

    const/16 v0, 0xc6

    aput-object v201, v4, v0

    const/16 v0, 0xc7

    aput-object v202, v4, v0

    const/16 v0, 0xc8

    aput-object v203, v4, v0

    const/16 v0, 0xc9

    aput-object v204, v4, v0

    const/16 v0, 0xca

    aput-object v205, v4, v0

    const/16 v0, 0xcb

    aput-object v206, v4, v0

    const/16 v0, 0xcc

    aput-object v207, v4, v0

    const/16 v0, 0xcd

    aput-object v208, v4, v0

    const/16 v0, 0xce

    aput-object v209, v4, v0

    const/16 v0, 0xcf

    aput-object v210, v4, v0

    const/16 v0, 0xd0

    aput-object v211, v4, v0

    const/16 v0, 0xd1

    aput-object v212, v4, v0

    const/16 v0, 0xd2

    aput-object v213, v4, v0

    const/16 v0, 0xd3

    aput-object v214, v4, v0

    const/16 v0, 0xd4

    aput-object v215, v4, v0

    const/16 v0, 0xd5

    aput-object v216, v4, v0

    const/16 v0, 0xd6

    aput-object v217, v4, v0

    const/16 v0, 0xd7

    aput-object v218, v4, v0

    const/16 v0, 0xd8

    aput-object v219, v4, v0

    const/16 v0, 0xd9

    aput-object v220, v4, v0

    const/16 v0, 0xda

    aput-object v221, v4, v0

    const/16 v0, 0xdb

    aput-object v222, v4, v0

    const/16 v0, 0xdc

    aput-object v223, v4, v0

    const/16 v0, 0xdd

    aput-object v224, v4, v0

    const/16 v0, 0xde

    aput-object v225, v4, v0

    const/16 v0, 0xdf

    aput-object v226, v4, v0

    const/16 v0, 0xe0

    aput-object v227, v4, v0

    const/16 v0, 0xe1

    aput-object v228, v4, v0

    const/16 v0, 0xe2

    aput-object v229, v4, v0

    const/16 v0, 0xe3

    aput-object v230, v4, v0

    const/16 v0, 0xe4

    aput-object v231, v4, v0

    const/16 v0, 0xe5

    aput-object v232, v4, v0

    const/16 v0, 0xe6

    aput-object v233, v4, v0

    const/16 v0, 0xe7

    move-object/from16 v1, v234

    aput-object v1, v4, v0

    const/16 v0, 0xe8

    move-object/from16 v1, v235

    aput-object v1, v4, v0

    const/16 v0, 0xe9

    move-object/from16 v1, v236

    aput-object v1, v4, v0

    const/16 v0, 0xea

    move-object/from16 v1, v237

    aput-object v1, v4, v0

    const/16 v0, 0xeb

    move-object/from16 v1, v238

    aput-object v1, v4, v0

    const/16 v0, 0xec

    move-object/from16 v1, v239

    aput-object v1, v4, v0

    const/16 v0, 0xed

    move-object/from16 v1, v240

    aput-object v1, v4, v0

    const/16 v0, 0xee

    move-object/from16 v1, v241

    aput-object v1, v4, v0

    const/16 v0, 0xef

    move-object/from16 v1, v242

    aput-object v1, v4, v0

    const/16 v0, 0xf0

    move-object/from16 v1, v243

    aput-object v1, v4, v0

    const/16 v0, 0xf1

    move-object/from16 v1, v244

    aput-object v1, v4, v0

    const/16 v0, 0xf2

    move-object/from16 v1, v245

    aput-object v1, v4, v0

    const/16 v0, 0xf3

    move-object/from16 v1, v246

    aput-object v1, v4, v0

    const/16 v0, 0xf4

    move-object/from16 v1, v247

    aput-object v1, v4, v0

    const/16 v0, 0xf5

    move-object/from16 v1, v248

    aput-object v1, v4, v0

    const/16 v0, 0xf6

    move-object/from16 v1, v249

    aput-object v1, v4, v0

    const/16 v0, 0xf7

    move-object/from16 v1, v250

    aput-object v1, v4, v0

    const/16 v0, 0xf8

    move-object/from16 v1, v251

    aput-object v1, v4, v0

    const/16 v0, 0xf9

    move-object/from16 v1, v252

    aput-object v1, v4, v0

    const/16 v0, 0xfa

    move-object/from16 v1, v253

    aput-object v1, v4, v0

    const/16 v0, 0xfb

    move-object/from16 v1, v254

    aput-object v1, v4, v0

    const/16 v0, 0xfc

    move-object/from16 v1, v255

    aput-object v1, v4, v0

    const/16 v0, 0xfd

    move-object/from16 v1, v256

    aput-object v1, v4, v0

    const/16 v0, 0xfe

    move-object/from16 v1, v257

    aput-object v1, v4, v0

    const/16 v0, 0xff

    move-object/from16 v1, v258

    aput-object v1, v4, v0

    const/16 v0, 0x100

    move-object/from16 v1, v259

    aput-object v1, v4, v0

    const/16 v0, 0x101

    move-object/from16 v1, v260

    aput-object v1, v4, v0

    const/16 v0, 0x102

    move-object/from16 v1, v261

    aput-object v1, v4, v0

    const/16 v0, 0x103

    move-object/from16 v1, v262

    aput-object v1, v4, v0

    const/16 v0, 0x104

    move-object/from16 v1, v263

    aput-object v1, v4, v0

    const/16 v0, 0x105

    move-object/from16 v1, v264

    aput-object v1, v4, v0

    const/16 v0, 0x106

    move-object/from16 v1, v265

    aput-object v1, v4, v0

    const/16 v0, 0x107

    move-object/from16 v1, v266

    aput-object v1, v4, v0

    const/16 v0, 0x108

    move-object/from16 v1, v267

    aput-object v1, v4, v0

    const/16 v0, 0x109

    move-object/from16 v1, v268

    aput-object v1, v4, v0

    const/16 v0, 0x10a

    move-object/from16 v1, v269

    aput-object v1, v4, v0

    const/16 v0, 0x10b

    move-object/from16 v1, v270

    aput-object v1, v4, v0

    const/16 v0, 0x10c

    move-object/from16 v1, v271

    aput-object v1, v4, v0

    const/16 v0, 0x10d

    move-object/from16 v1, v272

    aput-object v1, v4, v0

    const/16 v0, 0x10e

    move-object/from16 v1, v273

    aput-object v1, v4, v0

    const/16 v0, 0x10f

    move-object/from16 v1, v274

    aput-object v1, v4, v0

    const/16 v0, 0x110

    move-object/from16 v1, v275

    aput-object v1, v4, v0

    const/16 v0, 0x111

    move-object/from16 v1, v276

    aput-object v1, v4, v0

    const/16 v0, 0x112

    move-object/from16 v1, v277

    aput-object v1, v4, v0

    const/16 v0, 0x113

    move-object/from16 v1, v278

    aput-object v1, v4, v0

    const/16 v0, 0x114

    move-object/from16 v1, v279

    aput-object v1, v4, v0

    const/16 v0, 0x115

    move-object/from16 v1, v280

    aput-object v1, v4, v0

    const/16 v0, 0x116

    move-object/from16 v1, v281

    aput-object v1, v4, v0

    const/16 v0, 0x117

    move-object/from16 v1, v282

    aput-object v1, v4, v0

    const/16 v0, 0x118

    move-object/from16 v1, v283

    aput-object v1, v4, v0

    const/16 v0, 0x119

    move-object/from16 v1, v284

    aput-object v1, v4, v0

    const/16 v0, 0x11a

    move-object/from16 v1, v285

    aput-object v1, v4, v0

    const/16 v0, 0x11b

    move-object/from16 v1, v286

    aput-object v1, v4, v0

    const/16 v0, 0x11c

    move-object/from16 v1, v287

    aput-object v1, v4, v0

    const/16 v0, 0x11d

    move-object/from16 v1, v288

    aput-object v1, v4, v0

    const/16 v0, 0x11e

    move-object/from16 v1, v289

    aput-object v1, v4, v0

    const/16 v0, 0x11f

    move-object/from16 v1, v290

    aput-object v1, v4, v0

    const/16 v0, 0x120

    move-object/from16 v1, v291

    aput-object v1, v4, v0

    const/16 v0, 0x121

    move-object/from16 v1, v292

    aput-object v1, v4, v0

    const/16 v0, 0x122

    move-object/from16 v1, v293

    aput-object v1, v4, v0

    const/16 v0, 0x123

    move-object/from16 v1, v294

    aput-object v1, v4, v0

    const/16 v0, 0x124

    move-object/from16 v1, v295

    aput-object v1, v4, v0

    const/16 v0, 0x125

    move-object/from16 v1, v296

    aput-object v1, v4, v0

    const/16 v0, 0x126

    move-object/from16 v1, v297

    aput-object v1, v4, v0

    const/16 v0, 0x127

    move-object/from16 v1, v298

    aput-object v1, v4, v0

    const/16 v0, 0x128

    move-object/from16 v1, v299

    aput-object v1, v4, v0

    const/16 v0, 0x129

    move-object/from16 v1, v300

    aput-object v1, v4, v0

    const/16 v0, 0x12a

    move-object/from16 v1, v301

    aput-object v1, v4, v0

    const/16 v0, 0x12b

    move-object/from16 v1, v302

    aput-object v1, v4, v0

    const/16 v0, 0x12c

    move-object/from16 v1, v303

    aput-object v1, v4, v0

    const/16 v0, 0x12d

    move-object/from16 v1, v304

    aput-object v1, v4, v0

    const/16 v0, 0x12e

    move-object/from16 v1, v305

    aput-object v1, v4, v0

    const/16 v0, 0x12f

    move-object/from16 v1, v306

    aput-object v1, v4, v0

    const/16 v0, 0x130

    move-object/from16 v1, v307

    aput-object v1, v4, v0

    const/16 v0, 0x131

    move-object/from16 v1, v308

    aput-object v1, v4, v0

    const/16 v0, 0x132

    move-object/from16 v1, v309

    aput-object v1, v4, v0

    const/16 v0, 0x133

    move-object/from16 v1, v310

    aput-object v1, v4, v0

    const/16 v0, 0x134

    move-object/from16 v1, v311

    aput-object v1, v4, v0

    const/16 v0, 0x135

    move-object/from16 v1, v312

    aput-object v1, v4, v0

    const/16 v0, 0x136

    move-object/from16 v1, v313

    aput-object v1, v4, v0

    const/16 v0, 0x137

    move-object/from16 v1, v314

    aput-object v1, v4, v0

    const/16 v0, 0x138

    move-object/from16 v1, v315

    aput-object v1, v4, v0

    const/16 v0, 0x139

    move-object/from16 v1, v316

    aput-object v1, v4, v0

    const/16 v0, 0x13a

    move-object/from16 v1, v317

    aput-object v1, v4, v0

    const/16 v0, 0x13b

    move-object/from16 v1, v318

    aput-object v1, v4, v0

    const/16 v0, 0x13c

    move-object/from16 v1, v319

    aput-object v1, v4, v0

    const/16 v0, 0x13d

    move-object/from16 v1, v320

    aput-object v1, v4, v0

    const/16 v0, 0x13e

    move-object/from16 v1, v321

    aput-object v1, v4, v0

    const/16 v0, 0x13f

    move-object/from16 v1, v322

    aput-object v1, v4, v0

    const/16 v0, 0x140

    move-object/from16 v1, v323

    aput-object v1, v4, v0

    const/16 v0, 0x141

    move-object/from16 v1, v324

    aput-object v1, v4, v0

    const/16 v0, 0x142

    move-object/from16 v1, v325

    aput-object v1, v4, v0

    const/16 v0, 0x143

    move-object/from16 v1, v326

    aput-object v1, v4, v0

    const/16 v0, 0x144

    move-object/from16 v1, v327

    aput-object v1, v4, v0

    const/16 v0, 0x145

    move-object/from16 v1, v328

    aput-object v1, v4, v0

    const/16 v0, 0x146

    move-object/from16 v1, v329

    aput-object v1, v4, v0

    const/16 v0, 0x147

    move-object/from16 v1, v330

    aput-object v1, v4, v0

    const/16 v0, 0x148

    move-object/from16 v1, v331

    aput-object v1, v4, v0

    const/16 v0, 0x149

    move-object/from16 v1, v332

    aput-object v1, v4, v0

    const/16 v0, 0x14a

    move-object/from16 v1, v333

    aput-object v1, v4, v0

    const/16 v0, 0x14b

    move-object/from16 v1, v334

    aput-object v1, v4, v0

    const/16 v0, 0x14c

    move-object/from16 v1, v335

    aput-object v1, v4, v0

    const/16 v0, 0x14d

    move-object/from16 v1, v336

    aput-object v1, v4, v0

    const/16 v0, 0x14e

    move-object/from16 v1, v337

    aput-object v1, v4, v0

    const/16 v0, 0x14f

    move-object/from16 v1, v338

    aput-object v1, v4, v0

    const/16 v0, 0x150

    move-object/from16 v1, v339

    aput-object v1, v4, v0

    const/16 v0, 0x151

    move-object/from16 v1, v340

    aput-object v1, v4, v0

    const/16 v0, 0x152

    move-object/from16 v1, v341

    aput-object v1, v4, v0

    const/16 v0, 0x153

    move-object/from16 v1, v342

    aput-object v1, v4, v0

    const/16 v0, 0x154

    move-object/from16 v1, v343

    aput-object v1, v4, v0

    const/16 v0, 0x155

    move-object/from16 v1, v344

    aput-object v1, v4, v0

    const/16 v0, 0x156

    move-object/from16 v1, v345

    aput-object v1, v4, v0

    const/16 v0, 0x157

    move-object/from16 v1, v346

    aput-object v1, v4, v0

    const/16 v0, 0x158

    move-object/from16 v1, v347

    aput-object v1, v4, v0

    const/16 v0, 0x159

    move-object/from16 v1, v348

    aput-object v1, v4, v0

    const/16 v0, 0x15a

    move-object/from16 v1, v349

    aput-object v1, v4, v0

    const/16 v0, 0x15b

    move-object/from16 v1, v350

    aput-object v1, v4, v0

    const/16 v0, 0x15c

    move-object/from16 v1, v351

    aput-object v1, v4, v0

    const/16 v0, 0x15d

    move-object/from16 v1, v352

    aput-object v1, v4, v0

    const/16 v0, 0x15e

    move-object/from16 v1, v353

    aput-object v1, v4, v0

    const/16 v0, 0x15f

    move-object/from16 v1, v354

    aput-object v1, v4, v0

    const/16 v0, 0x160

    move-object/from16 v1, v355

    aput-object v1, v4, v0

    const/16 v0, 0x161

    move-object/from16 v1, v356

    aput-object v1, v4, v0

    const/16 v0, 0x162

    move-object/from16 v1, v357

    aput-object v1, v4, v0

    const/16 v0, 0x163

    move-object/from16 v1, v358

    aput-object v1, v4, v0

    const/16 v0, 0x164

    move-object/from16 v1, v359

    aput-object v1, v4, v0

    const/16 v0, 0x165

    move-object/from16 v1, v360

    aput-object v1, v4, v0

    const/16 v0, 0x166

    move-object/from16 v1, v361

    aput-object v1, v4, v0

    const/16 v0, 0x167

    move-object/from16 v1, v362

    aput-object v1, v4, v0

    const/16 v0, 0x168

    move-object/from16 v1, v363

    aput-object v1, v4, v0

    const/16 v0, 0x169

    move-object/from16 v1, v364

    aput-object v1, v4, v0

    const/16 v0, 0x16a

    move-object/from16 v1, v365

    aput-object v1, v4, v0

    const/16 v0, 0x16b

    move-object/from16 v1, v366

    aput-object v1, v4, v0

    const/16 v0, 0x16c

    move-object/from16 v1, v367

    aput-object v1, v4, v0

    const/16 v0, 0x16d

    move-object/from16 v1, v368

    aput-object v1, v4, v0

    const/16 v0, 0x16e

    move-object/from16 v1, v369

    aput-object v1, v4, v0

    const/16 v0, 0x16f

    move-object/from16 v1, v370

    aput-object v1, v4, v0

    const/16 v0, 0x170

    move-object/from16 v1, v371

    aput-object v1, v4, v0

    const/16 v0, 0x171

    move-object/from16 v1, v372

    aput-object v1, v4, v0

    const/16 v0, 0x172

    move-object/from16 v1, v373

    aput-object v1, v4, v0

    const/16 v0, 0x173

    move-object/from16 v1, v374

    aput-object v1, v4, v0

    const/16 v0, 0x174

    move-object/from16 v1, v375

    aput-object v1, v4, v0

    const/16 v0, 0x175

    move-object/from16 v1, v376

    aput-object v1, v4, v0

    const/16 v0, 0x176

    move-object/from16 v1, v377

    aput-object v1, v4, v0

    const/16 v0, 0x177

    move-object/from16 v1, v378

    aput-object v1, v4, v0

    const/16 v0, 0x178

    move-object/from16 v1, v379

    aput-object v1, v4, v0

    const/16 v0, 0x179

    move-object/from16 v1, v380

    aput-object v1, v4, v0

    const/16 v0, 0x17a

    move-object/from16 v1, v381

    aput-object v1, v4, v0

    const/16 v0, 0x17b

    move-object/from16 v1, v382

    aput-object v1, v4, v0

    const/16 v0, 0x17c

    move-object/from16 v1, v383

    aput-object v1, v4, v0

    const/16 v0, 0x17d

    move-object/from16 v1, v384

    aput-object v1, v4, v0

    const/16 v0, 0x17e

    move-object/from16 v1, v385

    aput-object v1, v4, v0

    const/16 v0, 0x17f

    move-object/from16 v1, v386

    aput-object v1, v4, v0

    const/16 v0, 0x180

    move-object/from16 v1, v387

    aput-object v1, v4, v0

    const/16 v0, 0x181

    move-object/from16 v1, v388

    aput-object v1, v4, v0

    const/16 v0, 0x182

    move-object/from16 v1, v389

    aput-object v1, v4, v0

    const/16 v0, 0x183

    move-object/from16 v1, v390

    aput-object v1, v4, v0

    const/16 v0, 0x184

    move-object/from16 v1, v391

    aput-object v1, v4, v0

    const/16 v0, 0x185

    move-object/from16 v1, v392

    aput-object v1, v4, v0

    const/16 v0, 0x186

    move-object/from16 v1, v393

    aput-object v1, v4, v0

    const/16 v0, 0x187

    move-object/from16 v1, v394

    aput-object v1, v4, v0

    const/16 v0, 0x188

    move-object/from16 v1, v395

    aput-object v1, v4, v0

    const/16 v0, 0x189

    move-object/from16 v1, v396

    aput-object v1, v4, v0

    const/16 v0, 0x18a

    move-object/from16 v1, v397

    aput-object v1, v4, v0

    const/16 v0, 0x18b

    move-object/from16 v1, v398

    aput-object v1, v4, v0

    const/16 v0, 0x18c

    move-object/from16 v1, v399

    aput-object v1, v4, v0

    const/16 v0, 0x18d

    move-object/from16 v1, v400

    aput-object v1, v4, v0

    const/16 v0, 0x18e

    move-object/from16 v1, v401

    aput-object v1, v4, v0

    const/16 v0, 0x18f

    move-object/from16 v1, v402

    aput-object v1, v4, v0

    const/16 v0, 0x190

    move-object/from16 v1, v403

    aput-object v1, v4, v0

    const/16 v0, 0x191

    move-object/from16 v1, v404

    aput-object v1, v4, v0

    const/16 v0, 0x192

    move-object/from16 v1, v405

    aput-object v1, v4, v0

    const/16 v0, 0x193

    move-object/from16 v1, v406

    aput-object v1, v4, v0

    const/16 v0, 0x194

    move-object/from16 v1, v407

    aput-object v1, v4, v0

    const/16 v0, 0x195

    move-object/from16 v1, v408

    aput-object v1, v4, v0

    const/16 v0, 0x196

    move-object/from16 v1, v409

    aput-object v1, v4, v0

    const/16 v0, 0x197

    move-object/from16 v1, v410

    aput-object v1, v4, v0

    const/16 v0, 0x198

    move-object/from16 v1, v411

    aput-object v1, v4, v0

    const/16 v0, 0x199

    move-object/from16 v1, v412

    aput-object v1, v4, v0

    const/16 v0, 0x19a

    move-object/from16 v1, v413

    aput-object v1, v4, v0

    const/16 v0, 0x19b

    move-object/from16 v1, v414

    aput-object v1, v4, v0

    const/16 v0, 0x19c

    move-object/from16 v1, v415

    aput-object v1, v4, v0

    const/16 v0, 0x19d

    move-object/from16 v1, v416

    aput-object v1, v4, v0

    const/16 v0, 0x19e

    move-object/from16 v1, v417

    aput-object v1, v4, v0

    const/16 v0, 0x19f

    move-object/from16 v1, v418

    aput-object v1, v4, v0

    const/16 v0, 0x1a0

    move-object/from16 v1, v419

    aput-object v1, v4, v0

    const/16 v0, 0x1a1

    move-object/from16 v1, v420

    aput-object v1, v4, v0

    const/16 v0, 0x1a2

    move-object/from16 v1, v421

    aput-object v1, v4, v0

    const/16 v0, 0x1a3

    move-object/from16 v1, v422

    aput-object v1, v4, v0

    const/16 v0, 0x1a4

    move-object/from16 v1, v423

    aput-object v1, v4, v0

    const/16 v0, 0x1a5

    move-object/from16 v1, v424

    aput-object v1, v4, v0

    const/16 v0, 0x1a6

    move-object/from16 v1, v425

    aput-object v1, v4, v0

    const/16 v0, 0x1a7

    move-object/from16 v1, v426

    aput-object v1, v4, v0

    const/16 v0, 0x1a8

    move-object/from16 v1, v427

    aput-object v1, v4, v0

    const/16 v0, 0x1a9

    aput-object v2, v4, v0

    .line 427
    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->$VALUES:[Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;
    .locals 1

    .line 1
    const-class v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static values()[Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;
    .locals 1

    .line 1
    sget-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->$VALUES:[Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.class Lcom/facebook/internal/logging/monitor/MonitorLoggingManager$2;
.super Ljava/lang/Object;
.source "MonitorLoggingManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/facebook/internal/logging/monitor/MonitorLoggingManager;->addLog(Lcom/facebook/internal/logging/ExternalLog;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/facebook/internal/logging/monitor/MonitorLoggingManager;

.field final synthetic val$log:Lcom/facebook/internal/logging/ExternalLog;


# direct methods
.method constructor <init>(Lcom/facebook/internal/logging/monitor/MonitorLoggingManager;Lcom/facebook/internal/logging/ExternalLog;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/facebook/internal/logging/monitor/MonitorLoggingManager$2;->this$0:Lcom/facebook/internal/logging/monitor/MonitorLoggingManager;

    .line 2
    .line 3
    iput-object p2, p0, Lcom/facebook/internal/logging/monitor/MonitorLoggingManager$2;->val$log:Lcom/facebook/internal/logging/ExternalLog;

    .line 4
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/facebook/internal/logging/monitor/MonitorLoggingManager$2;->this$0:Lcom/facebook/internal/logging/monitor/MonitorLoggingManager;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/facebook/internal/logging/monitor/MonitorLoggingManager;->access$000(Lcom/facebook/internal/logging/monitor/MonitorLoggingManager;)Lcom/facebook/internal/logging/LoggingCache;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/facebook/internal/logging/monitor/MonitorLoggingManager$2;->val$log:Lcom/facebook/internal/logging/ExternalLog;

    .line 8
    .line 9
    invoke-interface {v0, v1}, Lcom/facebook/internal/logging/LoggingCache;->addLog(Lcom/facebook/internal/logging/ExternalLog;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    iget-object v0, p0, Lcom/facebook/internal/logging/monitor/MonitorLoggingManager$2;->this$0:Lcom/facebook/internal/logging/monitor/MonitorLoggingManager;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/facebook/internal/logging/monitor/MonitorLoggingManager;->flushAndWait()V

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    iget-object v0, p0, Lcom/facebook/internal/logging/monitor/MonitorLoggingManager$2;->this$0:Lcom/facebook/internal/logging/monitor/MonitorLoggingManager;

    .line 22
    .line 23
    invoke-static {v0}, Lcom/facebook/internal/logging/monitor/MonitorLoggingManager;->access$100(Lcom/facebook/internal/logging/monitor/MonitorLoggingManager;)Ljava/util/concurrent/ScheduledFuture;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    if-nez v0, :cond_1

    .line 28
    .line 29
    iget-object v0, p0, Lcom/facebook/internal/logging/monitor/MonitorLoggingManager$2;->this$0:Lcom/facebook/internal/logging/monitor/MonitorLoggingManager;

    .line 30
    .line 31
    invoke-static {v0}, Lcom/facebook/internal/logging/monitor/MonitorLoggingManager;->access$300(Lcom/facebook/internal/logging/monitor/MonitorLoggingManager;)Ljava/util/concurrent/ScheduledExecutorService;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    iget-object v2, p0, Lcom/facebook/internal/logging/monitor/MonitorLoggingManager$2;->this$0:Lcom/facebook/internal/logging/monitor/MonitorLoggingManager;

    .line 36
    .line 37
    invoke-static {v2}, Lcom/facebook/internal/logging/monitor/MonitorLoggingManager;->access$200(Lcom/facebook/internal/logging/monitor/MonitorLoggingManager;)Ljava/lang/Runnable;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    const-wide/16 v3, 0x5

    .line 42
    .line 43
    sget-object v5, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    .line 44
    .line 45
    invoke-interface {v1, v2, v3, v4, v5}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    invoke-static {v0, v1}, Lcom/facebook/internal/logging/monitor/MonitorLoggingManager;->access$102(Lcom/facebook/internal/logging/monitor/MonitorLoggingManager;Ljava/util/concurrent/ScheduledFuture;)Ljava/util/concurrent/ScheduledFuture;

    .line 50
    .line 51
    .line 52
    :cond_1
    :goto_0
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

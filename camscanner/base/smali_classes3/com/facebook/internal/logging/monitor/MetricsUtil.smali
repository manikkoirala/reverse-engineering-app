.class public Lcom/facebook/internal/logging/monitor/MetricsUtil;
.super Ljava/lang/Object;
.source "MetricsUtil.java"


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$Scope;->LIBRARY_GROUP:Landroidx/annotation/RestrictTo$Scope;
    }
.end annotation

.annotation build Lcom/facebook/internal/instrument/crashshield/AutoHandleExceptions;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/facebook/internal/logging/monitor/MetricsUtil$TempMetrics;,
        Lcom/facebook/internal/logging/monitor/MetricsUtil$MetricsKey;
    }
.end annotation


# static fields
.field private static final CLASS_TAG:Ljava/lang/String; = "com.facebook.internal.logging.monitor.MetricsUtil"

.field protected static final INVALID_TIME:I = -0x1

.field private static metricsUtil:Lcom/facebook/internal/logging/monitor/MetricsUtil;


# instance fields
.field private final metricsDataMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/facebook/internal/logging/monitor/MetricsUtil$MetricsKey;",
            "Lcom/facebook/internal/logging/monitor/MetricsUtil$TempMetrics;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/HashMap;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/facebook/internal/logging/monitor/MetricsUtil;->metricsDataMap:Ljava/util/Map;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public static declared-synchronized getInstance()Lcom/facebook/internal/logging/monitor/MetricsUtil;
    .locals 2

    .line 1
    const-class v0, Lcom/facebook/internal/logging/monitor/MetricsUtil;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    :try_start_0
    sget-object v1, Lcom/facebook/internal/logging/monitor/MetricsUtil;->metricsUtil:Lcom/facebook/internal/logging/monitor/MetricsUtil;

    .line 5
    .line 6
    if-nez v1, :cond_0

    .line 7
    .line 8
    new-instance v1, Lcom/facebook/internal/logging/monitor/MetricsUtil;

    .line 9
    .line 10
    invoke-direct {v1}, Lcom/facebook/internal/logging/monitor/MetricsUtil;-><init>()V

    .line 11
    .line 12
    .line 13
    sput-object v1, Lcom/facebook/internal/logging/monitor/MetricsUtil;->metricsUtil:Lcom/facebook/internal/logging/monitor/MetricsUtil;

    .line 14
    .line 15
    :cond_0
    sget-object v1, Lcom/facebook/internal/logging/monitor/MetricsUtil;->metricsUtil:Lcom/facebook/internal/logging/monitor/MetricsUtil;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 16
    .line 17
    monitor-exit v0

    .line 18
    return-object v1

    .line 19
    :catchall_0
    move-exception v1

    .line 20
    monitor-exit v0

    .line 21
    throw v1
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method


# virtual methods
.method removeTempMetricsDataFor(Lcom/facebook/internal/logging/monitor/PerformanceEventName;J)V
    .locals 1

    .line 1
    new-instance v0, Lcom/facebook/internal/logging/monitor/MetricsUtil$MetricsKey;

    .line 2
    .line 3
    invoke-direct {v0, p1, p2, p3}, Lcom/facebook/internal/logging/monitor/MetricsUtil$MetricsKey;-><init>(Lcom/facebook/internal/logging/monitor/PerformanceEventName;J)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/facebook/internal/logging/monitor/MetricsUtil;->metricsDataMap:Ljava/util/Map;

    .line 7
    .line 8
    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method startMeasureFor(Lcom/facebook/internal/logging/monitor/PerformanceEventName;J)V
    .locals 1

    .line 1
    new-instance v0, Lcom/facebook/internal/logging/monitor/MetricsUtil$MetricsKey;

    .line 2
    .line 3
    invoke-direct {v0, p1, p2, p3}, Lcom/facebook/internal/logging/monitor/MetricsUtil$MetricsKey;-><init>(Lcom/facebook/internal/logging/monitor/PerformanceEventName;J)V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 7
    .line 8
    .line 9
    move-result-wide p1

    .line 10
    new-instance p3, Lcom/facebook/internal/logging/monitor/MetricsUtil$TempMetrics;

    .line 11
    .line 12
    invoke-direct {p3, p1, p2}, Lcom/facebook/internal/logging/monitor/MetricsUtil$TempMetrics;-><init>(J)V

    .line 13
    .line 14
    .line 15
    iget-object p1, p0, Lcom/facebook/internal/logging/monitor/MetricsUtil;->metricsDataMap:Ljava/util/Map;

    .line 16
    .line 17
    invoke-interface {p1, v0, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method stopMeasureFor(Lcom/facebook/internal/logging/monitor/PerformanceEventName;J)Lcom/facebook/internal/logging/monitor/MonitorLog;
    .locals 5

    .line 1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    new-instance v2, Lcom/facebook/internal/logging/monitor/MetricsUtil$MetricsKey;

    .line 6
    .line 7
    invoke-direct {v2, p1, p2, p3}, Lcom/facebook/internal/logging/monitor/MetricsUtil$MetricsKey;-><init>(Lcom/facebook/internal/logging/monitor/PerformanceEventName;J)V

    .line 8
    .line 9
    .line 10
    new-instance p2, Lcom/facebook/internal/logging/LogEvent;

    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/facebook/internal/logging/monitor/PerformanceEventName;->toString()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object p3

    .line 16
    sget-object v3, Lcom/facebook/internal/logging/LogCategory;->PERFORMANCE:Lcom/facebook/internal/logging/LogCategory;

    .line 17
    .line 18
    invoke-direct {p2, p3, v3}, Lcom/facebook/internal/logging/LogEvent;-><init>(Ljava/lang/String;Lcom/facebook/internal/logging/LogCategory;)V

    .line 19
    .line 20
    .line 21
    new-instance p3, Lcom/facebook/internal/logging/monitor/MonitorLog$LogBuilder;

    .line 22
    .line 23
    invoke-direct {p3, p2}, Lcom/facebook/internal/logging/monitor/MonitorLog$LogBuilder;-><init>(Lcom/facebook/internal/logging/LogEvent;)V

    .line 24
    .line 25
    .line 26
    const/4 v3, -0x1

    .line 27
    invoke-virtual {p3, v3}, Lcom/facebook/internal/logging/monitor/MonitorLog$LogBuilder;->timeSpent(I)Lcom/facebook/internal/logging/monitor/MonitorLog$LogBuilder;

    .line 28
    .line 29
    .line 30
    move-result-object p3

    .line 31
    invoke-virtual {p3}, Lcom/facebook/internal/logging/monitor/MonitorLog$LogBuilder;->build()Lcom/facebook/internal/logging/monitor/MonitorLog;

    .line 32
    .line 33
    .line 34
    move-result-object p3

    .line 35
    iget-object v3, p0, Lcom/facebook/internal/logging/monitor/MetricsUtil;->metricsDataMap:Ljava/util/Map;

    .line 36
    .line 37
    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    .line 38
    .line 39
    .line 40
    move-result v3

    .line 41
    if-nez v3, :cond_0

    .line 42
    .line 43
    new-instance p2, Ljava/lang/StringBuilder;

    .line 44
    .line 45
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    .line 47
    .line 48
    const-string v0, "Can\'t measure for "

    .line 49
    .line 50
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    const-string p1, ", startMeasureFor hasn\'t been called before."

    .line 57
    .line 58
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    sget-object p1, Lcom/facebook/internal/logging/monitor/MetricsUtil;->CLASS_TAG:Ljava/lang/String;

    .line 62
    .line 63
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object p2

    .line 67
    invoke-static {p1, p2}, Lcom/facebook/internal/Utility;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    return-object p3

    .line 71
    :cond_0
    iget-object p1, p0, Lcom/facebook/internal/logging/monitor/MetricsUtil;->metricsDataMap:Ljava/util/Map;

    .line 72
    .line 73
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    .line 75
    .line 76
    move-result-object p1

    .line 77
    check-cast p1, Lcom/facebook/internal/logging/monitor/MetricsUtil$TempMetrics;

    .line 78
    .line 79
    if-eqz p1, :cond_1

    .line 80
    .line 81
    invoke-static {p1}, Lcom/facebook/internal/logging/monitor/MetricsUtil$TempMetrics;->access$000(Lcom/facebook/internal/logging/monitor/MetricsUtil$TempMetrics;)J

    .line 82
    .line 83
    .line 84
    move-result-wide v3

    .line 85
    sub-long/2addr v0, v3

    .line 86
    long-to-int p1, v0

    .line 87
    new-instance p3, Lcom/facebook/internal/logging/monitor/MonitorLog$LogBuilder;

    .line 88
    .line 89
    invoke-direct {p3, p2}, Lcom/facebook/internal/logging/monitor/MonitorLog$LogBuilder;-><init>(Lcom/facebook/internal/logging/LogEvent;)V

    .line 90
    .line 91
    .line 92
    invoke-virtual {p3, p1}, Lcom/facebook/internal/logging/monitor/MonitorLog$LogBuilder;->timeSpent(I)Lcom/facebook/internal/logging/monitor/MonitorLog$LogBuilder;

    .line 93
    .line 94
    .line 95
    move-result-object p1

    .line 96
    invoke-virtual {p1}, Lcom/facebook/internal/logging/monitor/MonitorLog$LogBuilder;->build()Lcom/facebook/internal/logging/monitor/MonitorLog;

    .line 97
    .line 98
    .line 99
    move-result-object p3

    .line 100
    :cond_1
    iget-object p1, p0, Lcom/facebook/internal/logging/monitor/MetricsUtil;->metricsDataMap:Ljava/util/Map;

    .line 101
    .line 102
    invoke-interface {p1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    .line 104
    .line 105
    return-object p3
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

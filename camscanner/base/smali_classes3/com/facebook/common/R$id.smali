.class public final Lcom/facebook/common/R$id;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/common/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final accessibility_action_clickable_span:I = 0x7f0a001d

.field public static final accessibility_custom_action_0:I = 0x7f0a001e

.field public static final accessibility_custom_action_1:I = 0x7f0a001f

.field public static final accessibility_custom_action_10:I = 0x7f0a0020

.field public static final accessibility_custom_action_11:I = 0x7f0a0021

.field public static final accessibility_custom_action_12:I = 0x7f0a0022

.field public static final accessibility_custom_action_13:I = 0x7f0a0023

.field public static final accessibility_custom_action_14:I = 0x7f0a0024

.field public static final accessibility_custom_action_15:I = 0x7f0a0025

.field public static final accessibility_custom_action_16:I = 0x7f0a0026

.field public static final accessibility_custom_action_17:I = 0x7f0a0027

.field public static final accessibility_custom_action_18:I = 0x7f0a0028

.field public static final accessibility_custom_action_19:I = 0x7f0a0029

.field public static final accessibility_custom_action_2:I = 0x7f0a002a

.field public static final accessibility_custom_action_20:I = 0x7f0a002b

.field public static final accessibility_custom_action_21:I = 0x7f0a002c

.field public static final accessibility_custom_action_22:I = 0x7f0a002d

.field public static final accessibility_custom_action_23:I = 0x7f0a002e

.field public static final accessibility_custom_action_24:I = 0x7f0a002f

.field public static final accessibility_custom_action_25:I = 0x7f0a0030

.field public static final accessibility_custom_action_26:I = 0x7f0a0031

.field public static final accessibility_custom_action_27:I = 0x7f0a0032

.field public static final accessibility_custom_action_28:I = 0x7f0a0033

.field public static final accessibility_custom_action_29:I = 0x7f0a0034

.field public static final accessibility_custom_action_3:I = 0x7f0a0035

.field public static final accessibility_custom_action_30:I = 0x7f0a0036

.field public static final accessibility_custom_action_31:I = 0x7f0a0037

.field public static final accessibility_custom_action_4:I = 0x7f0a0038

.field public static final accessibility_custom_action_5:I = 0x7f0a0039

.field public static final accessibility_custom_action_6:I = 0x7f0a003a

.field public static final accessibility_custom_action_7:I = 0x7f0a003b

.field public static final accessibility_custom_action_8:I = 0x7f0a003c

.field public static final accessibility_custom_action_9:I = 0x7f0a003d

.field public static final action0:I = 0x7f0a0051

.field public static final action_bar:I = 0x7f0a005a

.field public static final action_bar_activity_content:I = 0x7f0a005b

.field public static final action_bar_container:I = 0x7f0a005c

.field public static final action_bar_root:I = 0x7f0a0060

.field public static final action_bar_spinner:I = 0x7f0a0061

.field public static final action_bar_subtitle:I = 0x7f0a0062

.field public static final action_bar_title:I = 0x7f0a0063

.field public static final action_container:I = 0x7f0a0066

.field public static final action_context_bar:I = 0x7f0a0067

.field public static final action_divider:I = 0x7f0a0068

.field public static final action_image:I = 0x7f0a0069

.field public static final action_menu_divider:I = 0x7f0a006b

.field public static final action_menu_presenter:I = 0x7f0a006c

.field public static final action_mode_bar:I = 0x7f0a006d

.field public static final action_mode_bar_stub:I = 0x7f0a006e

.field public static final action_mode_close_button:I = 0x7f0a006f

.field public static final action_text:I = 0x7f0a0072

.field public static final actions:I = 0x7f0a0073

.field public static final activity_chooser_view_content:I = 0x7f0a0075

.field public static final add:I = 0x7f0a008e

.field public static final alertTitle:I = 0x7f0a0133

.field public static final async:I = 0x7f0a0163

.field public static final blocking:I = 0x7f0a01db

.field public static final bottom:I = 0x7f0a01e2

.field public static final box_count:I = 0x7f0a01fd

.field public static final browser_actions_header_text:I = 0x7f0a0203

.field public static final browser_actions_menu_item_icon:I = 0x7f0a0204

.field public static final browser_actions_menu_item_text:I = 0x7f0a0205

.field public static final browser_actions_menu_items:I = 0x7f0a0206

.field public static final browser_actions_menu_view:I = 0x7f0a0207

.field public static final button:I = 0x7f0a02dd

.field public static final buttonPanel:I = 0x7f0a02e2

.field public static final cancel_action:I = 0x7f0a02ef

.field public static final cancel_button:I = 0x7f0a02f0

.field public static final center:I = 0x7f0a0346

.field public static final checkbox:I = 0x7f0a035f

.field public static final checked:I = 0x7f0a0363

.field public static final chronometer:I = 0x7f0a0365

.field public static final com_facebook_device_auth_instructions:I = 0x7f0a048a

.field public static final com_facebook_fragment_container:I = 0x7f0a048b

.field public static final com_facebook_login_fragment_progress_bar:I = 0x7f0a048c

.field public static final com_facebook_smart_instructions_0:I = 0x7f0a048d

.field public static final com_facebook_smart_instructions_or:I = 0x7f0a048e

.field public static final confirmation_code:I = 0x7f0a04a4

.field public static final content:I = 0x7f0a04ad

.field public static final contentPanel:I = 0x7f0a04ae

.field public static final custom:I = 0x7f0a04ed

.field public static final customPanel:I = 0x7f0a04ee

.field public static final decor_content_parent:I = 0x7f0a0509

.field public static final default_activity_button:I = 0x7f0a050a

.field public static final dialog_button:I = 0x7f0a0518

.field public static final edit_query:I = 0x7f0a0573

.field public static final end:I = 0x7f0a0581

.field public static final end_padder:I = 0x7f0a0584

.field public static final expand_activities_button:I = 0x7f0a05d1

.field public static final expanded_menu:I = 0x7f0a05d2

.field public static final forever:I = 0x7f0a066b

.field public static final group_divider:I = 0x7f0a06ca

.field public static final home:I = 0x7f0a0707

.field public static final icon:I = 0x7f0a071f

.field public static final icon_group:I = 0x7f0a0721

.field public static final image:I = 0x7f0a0730

.field public static final info:I = 0x7f0a078d

.field public static final inline:I = 0x7f0a078f

.field public static final italic:I = 0x7f0a07ad

.field public static final left:I = 0x7f0a0b3f

.field public static final line1:I = 0x7f0a0b47

.field public static final line3:I = 0x7f0a0b4a

.field public static final listMode:I = 0x7f0a0b59

.field public static final list_item:I = 0x7f0a0b5c

.field public static final media_actions:I = 0x7f0a0da4

.field public static final message:I = 0x7f0a0daf

.field public static final multiply:I = 0x7f0a0dea

.field public static final none:I = 0x7f0a0e06

.field public static final normal:I = 0x7f0a0e07

.field public static final notification_background:I = 0x7f0a0e10

.field public static final notification_main_column:I = 0x7f0a0e11

.field public static final notification_main_column_container:I = 0x7f0a0e12

.field public static final off:I = 0x7f0a0e29

.field public static final on:I = 0x7f0a0e2e

.field public static final open_graph:I = 0x7f0a0e35

.field public static final page:I = 0x7f0a0e42

.field public static final parentPanel:I = 0x7f0a0e51

.field public static final progress_bar:I = 0x7f0a0e99

.field public static final progress_circular:I = 0x7f0a0e9a

.field public static final progress_horizontal:I = 0x7f0a0e9b

.field public static final radio:I = 0x7f0a0ece

.field public static final right:I = 0x7f0a0f4f

.field public static final right_icon:I = 0x7f0a0f51

.field public static final right_side:I = 0x7f0a0f53

.field public static final screen:I = 0x7f0a1096

.field public static final scrollIndicatorDown:I = 0x7f0a1099

.field public static final scrollIndicatorUp:I = 0x7f0a109a

.field public static final scrollView:I = 0x7f0a109b

.field public static final search_badge:I = 0x7f0a10a7

.field public static final search_bar:I = 0x7f0a10a8

.field public static final search_button:I = 0x7f0a10aa

.field public static final search_close_btn:I = 0x7f0a10ab

.field public static final search_edit_frame:I = 0x7f0a10ac

.field public static final search_go_btn:I = 0x7f0a10ad

.field public static final search_mag_icon:I = 0x7f0a10af

.field public static final search_plate:I = 0x7f0a10b0

.field public static final search_src_text:I = 0x7f0a10b1

.field public static final search_voice_btn:I = 0x7f0a10bf

.field public static final select_dialog_listview:I = 0x7f0a10c3

.field public static final shortcut:I = 0x7f0a10d4

.field public static final spacer:I = 0x7f0a1111

.field public static final split_action_bar:I = 0x7f0a1116

.field public static final src_atop:I = 0x7f0a111b

.field public static final src_in:I = 0x7f0a111c

.field public static final src_over:I = 0x7f0a111d

.field public static final standard:I = 0x7f0a111e

.field public static final start:I = 0x7f0a111f

.field public static final status_bar_latest_event_content:I = 0x7f0a112a

.field public static final submenuarrow:I = 0x7f0a1154

.field public static final submit_area:I = 0x7f0a1155

.field public static final tabMode:I = 0x7f0a1174

.field public static final tag_accessibility_actions:I = 0x7f0a1180

.field public static final tag_accessibility_clickable_spans:I = 0x7f0a1181

.field public static final tag_accessibility_heading:I = 0x7f0a1182

.field public static final tag_accessibility_pane_title:I = 0x7f0a1183

.field public static final tag_screen_reader_focusable:I = 0x7f0a118f

.field public static final tag_transition_group:I = 0x7f0a1193

.field public static final tag_unhandled_key_event_manager:I = 0x7f0a1194

.field public static final tag_unhandled_key_listeners:I = 0x7f0a1195

.field public static final text:I = 0x7f0a119d

.field public static final text2:I = 0x7f0a119f

.field public static final textSpacerNoButtons:I = 0x7f0a11a3

.field public static final textSpacerNoTitle:I = 0x7f0a11a4

.field public static final time:I = 0x7f0a11c8

.field public static final title:I = 0x7f0a11d1

.field public static final titleDividerNoCustom:I = 0x7f0a11d2

.field public static final title_template:I = 0x7f0a11d6

.field public static final top:I = 0x7f0a11ee

.field public static final topPanel:I = 0x7f0a11ef

.field public static final unchecked:I = 0x7f0a196b

.field public static final uniform:I = 0x7f0a196d

.field public static final unknown:I = 0x7f0a196e

.field public static final up:I = 0x7f0a1971

.field public static final wrap_content:I = 0x7f0a1acd


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public Lcom/efs/sdk/launch/LaunchConfigManager;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private c:Lcom/efs/sdk/base/EfsReporter;

.field private d:I

.field private e:I

.field private f:Z

.field private g:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/efs/sdk/base/EfsReporter;)V
    .locals 17

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "LaunchConfigManager"

    .line 7
    .line 8
    iput-object v1, v0, Lcom/efs/sdk/launch/LaunchConfigManager;->a:Ljava/lang/String;

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    iput v1, v0, Lcom/efs/sdk/launch/LaunchConfigManager;->b:I

    .line 12
    .line 13
    const/16 v2, 0x64

    .line 14
    .line 15
    iput v2, v0, Lcom/efs/sdk/launch/LaunchConfigManager;->d:I

    .line 16
    .line 17
    iput-boolean v1, v0, Lcom/efs/sdk/launch/LaunchConfigManager;->f:Z

    .line 18
    .line 19
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 20
    .line 21
    .line 22
    move-result-object v3

    .line 23
    iput-object v3, v0, Lcom/efs/sdk/launch/LaunchConfigManager;->g:Landroid/content/Context;

    .line 24
    .line 25
    move-object/from16 v4, p2

    .line 26
    .line 27
    iput-object v4, v0, Lcom/efs/sdk/launch/LaunchConfigManager;->c:Lcom/efs/sdk/base/EfsReporter;

    .line 28
    .line 29
    const-string v4, "efs_launch"

    .line 30
    .line 31
    invoke-virtual {v3, v4, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    .line 32
    .line 33
    .line 34
    move-result-object v3

    .line 35
    const-string v5, "apm_startperf_sampling_rate_last"

    .line 36
    .line 37
    if-eqz v3, :cond_0

    .line 38
    .line 39
    invoke-interface {v3, v5, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    .line 40
    .line 41
    .line 42
    move-result v3

    .line 43
    iput v3, v0, Lcom/efs/sdk/launch/LaunchConfigManager;->e:I

    .line 44
    .line 45
    :cond_0
    iget-object v3, v0, Lcom/efs/sdk/launch/LaunchConfigManager;->g:Landroid/content/Context;

    .line 46
    .line 47
    invoke-virtual {v3, v4, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    .line 48
    .line 49
    .line 50
    move-result-object v3

    .line 51
    const-string v6, "apm_startperf_sampling_rate"

    .line 52
    .line 53
    const/4 v7, -0x1

    .line 54
    if-eqz v3, :cond_1

    .line 55
    .line 56
    invoke-interface {v3, v6, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    .line 57
    .line 58
    .line 59
    move-result v3

    .line 60
    goto :goto_0

    .line 61
    :cond_1
    const/4 v3, -0x1

    .line 62
    :goto_0
    iget-object v8, v0, Lcom/efs/sdk/launch/LaunchConfigManager;->c:Lcom/efs/sdk/base/EfsReporter;

    .line 63
    .line 64
    filled-new-array {v6}, [Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object v6

    .line 68
    new-instance v9, Lcom/efs/sdk/launch/LaunchConfigManager$1;

    .line 69
    .line 70
    invoke-direct {v9, v0}, Lcom/efs/sdk/launch/LaunchConfigManager$1;-><init>(Lcom/efs/sdk/launch/LaunchConfigManager;)V

    .line 71
    .line 72
    .line 73
    invoke-virtual {v8, v6, v9}, Lcom/efs/sdk/base/EfsReporter;->getAllSdkConfig([Ljava/lang/String;Lcom/efs/sdk/base/observer/IConfigCallback;)V

    .line 74
    .line 75
    .line 76
    if-eq v3, v7, :cond_2

    .line 77
    .line 78
    iput v3, v0, Lcom/efs/sdk/launch/LaunchConfigManager;->d:I

    .line 79
    .line 80
    :cond_2
    iget-object v3, v0, Lcom/efs/sdk/launch/LaunchConfigManager;->g:Landroid/content/Context;

    .line 81
    .line 82
    invoke-virtual {v3, v4, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    .line 83
    .line 84
    .line 85
    move-result-object v3

    .line 86
    const-string v6, "03f870871950c148387b251894ed3e88"

    .line 87
    .line 88
    const-wide/16 v7, 0x0

    .line 89
    .line 90
    if-eqz v3, :cond_3

    .line 91
    .line 92
    invoke-interface {v3, v6, v7, v8}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    .line 93
    .line 94
    .line 95
    move-result-wide v9

    .line 96
    goto :goto_1

    .line 97
    :cond_3
    move-wide v9, v7

    .line 98
    :goto_1
    const-string v11, "8f2f54c08600aa25915617fa1371441b"

    .line 99
    .line 100
    if-eqz v3, :cond_4

    .line 101
    .line 102
    invoke-interface {v3, v11, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    .line 103
    .line 104
    .line 105
    move-result v12

    .line 106
    goto :goto_2

    .line 107
    :cond_4
    const/4 v12, 0x0

    .line 108
    :goto_2
    iget v13, v0, Lcom/efs/sdk/launch/LaunchConfigManager;->d:I

    .line 109
    .line 110
    if-nez v13, :cond_7

    .line 111
    .line 112
    if-eqz v12, :cond_5

    .line 113
    .line 114
    if-eqz v3, :cond_5

    .line 115
    .line 116
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    .line 117
    .line 118
    .line 119
    move-result-object v2

    .line 120
    if-eqz v2, :cond_5

    .line 121
    .line 122
    invoke-interface {v2, v11, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 123
    .line 124
    .line 125
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 126
    .line 127
    .line 128
    :cond_5
    cmp-long v2, v9, v7

    .line 129
    .line 130
    if-eqz v2, :cond_6

    .line 131
    .line 132
    if-eqz v3, :cond_6

    .line 133
    .line 134
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    .line 135
    .line 136
    .line 137
    move-result-object v2

    .line 138
    if-eqz v2, :cond_6

    .line 139
    .line 140
    invoke-interface {v2, v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 141
    .line 142
    .line 143
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 144
    .line 145
    .line 146
    :cond_6
    :goto_3
    const/4 v7, 0x0

    .line 147
    goto/16 :goto_9

    .line 148
    .line 149
    :cond_7
    iget v3, v0, Lcom/efs/sdk/launch/LaunchConfigManager;->e:I

    .line 150
    .line 151
    const/4 v7, 0x1

    .line 152
    if-eq v13, v3, :cond_8

    .line 153
    .line 154
    const/4 v3, 0x1

    .line 155
    goto :goto_4

    .line 156
    :cond_8
    const/4 v3, 0x0

    .line 157
    :goto_4
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 158
    .line 159
    .line 160
    move-result-object v8

    .line 161
    iget v9, v0, Lcom/efs/sdk/launch/LaunchConfigManager;->d:I

    .line 162
    .line 163
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 164
    .line 165
    .line 166
    move-result-wide v13

    .line 167
    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 168
    .line 169
    .line 170
    move-result-object v10

    .line 171
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    .line 172
    .line 173
    .line 174
    move-result-wide v13

    .line 175
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    .line 176
    .line 177
    .line 178
    move-result-wide v15

    .line 179
    sub-long/2addr v13, v15

    .line 180
    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 181
    .line 182
    .line 183
    move-result-object v8

    .line 184
    const-wide/32 v13, 0x5265c00

    .line 185
    .line 186
    .line 187
    if-eqz v12, :cond_9

    .line 188
    .line 189
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    .line 190
    .line 191
    .line 192
    move-result-wide v15

    .line 193
    cmp-long v12, v15, v13

    .line 194
    .line 195
    if-gez v12, :cond_9

    .line 196
    .line 197
    if-nez v3, :cond_9

    .line 198
    .line 199
    sget-boolean v2, Lcom/efs/sdk/launch/LaunchManager;->isDebug:Z

    .line 200
    .line 201
    goto :goto_9

    .line 202
    :cond_9
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    .line 203
    .line 204
    .line 205
    move-result-wide v15

    .line 206
    cmp-long v8, v15, v13

    .line 207
    .line 208
    if-gez v8, :cond_b

    .line 209
    .line 210
    if-eqz v3, :cond_a

    .line 211
    .line 212
    goto :goto_5

    .line 213
    :cond_a
    sget-boolean v2, Lcom/efs/sdk/launch/LaunchManager;->isDebug:Z

    .line 214
    .line 215
    goto :goto_3

    .line 216
    :cond_b
    :goto_5
    if-eqz v9, :cond_d

    .line 217
    .line 218
    if-ne v9, v2, :cond_c

    .line 219
    .line 220
    :goto_6
    const/4 v2, 0x1

    .line 221
    goto :goto_7

    .line 222
    :cond_c
    new-instance v3, Ljava/util/Random;

    .line 223
    .line 224
    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    .line 225
    .line 226
    .line 227
    invoke-virtual {v3, v2}, Ljava/util/Random;->nextInt(I)I

    .line 228
    .line 229
    .line 230
    move-result v2

    .line 231
    if-gt v2, v9, :cond_d

    .line 232
    .line 233
    goto :goto_6

    .line 234
    :cond_d
    const/4 v2, 0x0

    .line 235
    :goto_7
    if-eqz v2, :cond_e

    .line 236
    .line 237
    sget-boolean v2, Lcom/efs/sdk/launch/LaunchManager;->isDebug:Z

    .line 238
    .line 239
    goto :goto_8

    .line 240
    :cond_e
    sget-boolean v2, Lcom/efs/sdk/launch/LaunchManager;->isDebug:Z

    .line 241
    .line 242
    const/4 v7, 0x0

    .line 243
    :goto_8
    iget-object v2, v0, Lcom/efs/sdk/launch/LaunchConfigManager;->g:Landroid/content/Context;

    .line 244
    .line 245
    invoke-virtual {v2, v4, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    .line 246
    .line 247
    .line 248
    move-result-object v2

    .line 249
    if-eqz v2, :cond_f

    .line 250
    .line 251
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    .line 252
    .line 253
    .line 254
    move-result-object v3

    .line 255
    if-eqz v3, :cond_f

    .line 256
    .line 257
    invoke-interface {v3, v11, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 258
    .line 259
    .line 260
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 261
    .line 262
    .line 263
    :cond_f
    if-eqz v2, :cond_10

    .line 264
    .line 265
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    .line 266
    .line 267
    .line 268
    move-result-object v2

    .line 269
    if-eqz v2, :cond_10

    .line 270
    .line 271
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    .line 272
    .line 273
    .line 274
    move-result-wide v8

    .line 275
    invoke-interface {v2, v6, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 276
    .line 277
    .line 278
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 279
    .line 280
    .line 281
    :cond_10
    :goto_9
    iput-boolean v7, v0, Lcom/efs/sdk/launch/LaunchConfigManager;->f:Z

    .line 282
    .line 283
    iget-object v2, v0, Lcom/efs/sdk/launch/LaunchConfigManager;->g:Landroid/content/Context;

    .line 284
    .line 285
    invoke-virtual {v2, v4, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    .line 286
    .line 287
    .line 288
    move-result-object v1

    .line 289
    if-eqz v1, :cond_11

    .line 290
    .line 291
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    .line 292
    .line 293
    .line 294
    move-result-object v1

    .line 295
    if-eqz v1, :cond_11

    .line 296
    .line 297
    iget v2, v0, Lcom/efs/sdk/launch/LaunchConfigManager;->d:I

    .line 298
    .line 299
    invoke-interface {v1, v5, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 300
    .line 301
    .line 302
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 303
    .line 304
    .line 305
    :cond_11
    return-void
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

.method static synthetic a(Lcom/efs/sdk/launch/LaunchConfigManager;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/efs/sdk/launch/LaunchConfigManager;->g:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public enableTracer()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/efs/sdk/launch/LaunchConfigManager;->f:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.class final Lcom/efs/sdk/memoryinfo/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final activity:Ljava/lang/String;

.field final bg:Ljava/lang/String;

.field final n:J

.field final o:J

.field final p:J

.field final q:J

.field final r:F

.field final s:J

.field final t:J


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 9

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    :try_start_0
    const-string v1, "activity"

    .line 6
    .line 7
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    check-cast p1, Landroid/app/ActivityManager;

    .line 12
    .line 13
    if-eqz p1, :cond_0

    .line 14
    .line 15
    invoke-static {}, Landroid/os/Process;->myPid()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    const/4 v2, 0x1

    .line 20
    new-array v2, v2, [I

    .line 21
    .line 22
    const/4 v3, 0x0

    .line 23
    aput v1, v2, v3

    .line 24
    .line 25
    invoke-virtual {p1, v2}, Landroid/app/ActivityManager;->getProcessMemoryInfo([I)[Landroid/os/Debug$MemoryInfo;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    if-eqz p1, :cond_0

    .line 30
    .line 31
    array-length v1, p1

    .line 32
    if-lez v1, :cond_0

    .line 33
    .line 34
    aget-object p1, p1, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 35
    .line 36
    move-object v0, p1

    .line 37
    goto :goto_0

    .line 38
    :catchall_0
    nop

    .line 39
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 40
    .line 41
    new-instance v0, Landroid/os/Debug$MemoryInfo;

    .line 42
    .line 43
    invoke-direct {v0}, Landroid/os/Debug$MemoryInfo;-><init>()V

    .line 44
    .line 45
    .line 46
    invoke-static {v0}, Landroid/os/Debug;->getMemoryInfo(Landroid/os/Debug$MemoryInfo;)V

    .line 47
    .line 48
    .line 49
    :cond_1
    invoke-static {}, Lcom/efs/sdk/memoryinfo/UMMemoryMonitor;->get()Lcom/efs/sdk/memoryinfo/UMMemoryMonitorApi;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    invoke-interface {p1}, Lcom/efs/sdk/memoryinfo/UMMemoryMonitorApi;->isForeground()Z

    .line 54
    .line 55
    .line 56
    move-result p1

    .line 57
    if-eqz p1, :cond_2

    .line 58
    .line 59
    const-string p1, "fg"

    .line 60
    .line 61
    goto :goto_1

    .line 62
    :cond_2
    const-string p1, "bg"

    .line 63
    .line 64
    :goto_1
    iput-object p1, p0, Lcom/efs/sdk/memoryinfo/c;->bg:Ljava/lang/String;

    .line 65
    .line 66
    invoke-virtual {v0}, Landroid/os/Debug$MemoryInfo;->getTotalPss()I

    .line 67
    .line 68
    .line 69
    move-result p1

    .line 70
    int-to-long v1, p1

    .line 71
    const-wide/16 v3, 0x400

    .line 72
    .line 73
    mul-long v1, v1, v3

    .line 74
    .line 75
    iput-wide v1, p0, Lcom/efs/sdk/memoryinfo/c;->n:J

    .line 76
    .line 77
    iget p1, v0, Landroid/os/Debug$MemoryInfo;->dalvikPss:I

    .line 78
    .line 79
    int-to-long v1, p1

    .line 80
    mul-long v1, v1, v3

    .line 81
    .line 82
    iput-wide v1, p0, Lcom/efs/sdk/memoryinfo/c;->o:J

    .line 83
    .line 84
    iget p1, v0, Landroid/os/Debug$MemoryInfo;->nativePss:I

    .line 85
    .line 86
    int-to-long v1, p1

    .line 87
    mul-long v1, v1, v3

    .line 88
    .line 89
    iput-wide v1, p0, Lcom/efs/sdk/memoryinfo/c;->p:J

    .line 90
    .line 91
    invoke-static {v0}, Lcom/efs/sdk/memoryinfo/f;->a(Landroid/os/Debug$MemoryInfo;)J

    .line 92
    .line 93
    .line 94
    move-result-wide v0

    .line 95
    mul-long v0, v0, v3

    .line 96
    .line 97
    iput-wide v0, p0, Lcom/efs/sdk/memoryinfo/c;->s:J

    .line 98
    .line 99
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    .line 100
    .line 101
    .line 102
    move-result-object p1

    .line 103
    invoke-virtual {p1}, Ljava/lang/Runtime;->totalMemory()J

    .line 104
    .line 105
    .line 106
    move-result-wide v0

    .line 107
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    .line 108
    .line 109
    .line 110
    move-result-object p1

    .line 111
    invoke-virtual {p1}, Ljava/lang/Runtime;->freeMemory()J

    .line 112
    .line 113
    .line 114
    move-result-wide v5

    .line 115
    sub-long/2addr v0, v5

    .line 116
    iput-wide v0, p0, Lcom/efs/sdk/memoryinfo/c;->q:J

    .line 117
    .line 118
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    .line 119
    .line 120
    .line 121
    move-result-object p1

    .line 122
    invoke-virtual {p1}, Ljava/lang/Runtime;->maxMemory()J

    .line 123
    .line 124
    .line 125
    move-result-wide v5

    .line 126
    const-wide/16 v7, 0x0

    .line 127
    .line 128
    const/high16 p1, 0x3f800000    # 1.0f

    .line 129
    .line 130
    cmp-long v2, v5, v7

    .line 131
    .line 132
    if-eqz v2, :cond_3

    .line 133
    .line 134
    long-to-float v0, v0

    .line 135
    mul-float v0, v0, p1

    .line 136
    .line 137
    long-to-float p1, v5

    .line 138
    div-float/2addr v0, p1

    .line 139
    iput v0, p0, Lcom/efs/sdk/memoryinfo/c;->r:F

    .line 140
    .line 141
    goto :goto_2

    .line 142
    :cond_3
    iput p1, p0, Lcom/efs/sdk/memoryinfo/c;->r:F

    .line 143
    .line 144
    :goto_2
    invoke-static {}, Lcom/efs/sdk/memoryinfo/f;->a()J

    .line 145
    .line 146
    .line 147
    move-result-wide v0

    .line 148
    mul-long v0, v0, v3

    .line 149
    .line 150
    iput-wide v0, p0, Lcom/efs/sdk/memoryinfo/c;->t:J

    .line 151
    .line 152
    invoke-static {}, Lcom/efs/sdk/memoryinfo/UMMemoryMonitor;->get()Lcom/efs/sdk/memoryinfo/UMMemoryMonitorApi;

    .line 153
    .line 154
    .line 155
    move-result-object p1

    .line 156
    invoke-interface {p1}, Lcom/efs/sdk/memoryinfo/UMMemoryMonitorApi;->getCurrentActivity()Ljava/lang/String;

    .line 157
    .line 158
    .line 159
    move-result-object p1

    .line 160
    iput-object p1, p0, Lcom/efs/sdk/memoryinfo/c;->activity:Ljava/lang/String;

    .line 161
    .line 162
    return-void
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

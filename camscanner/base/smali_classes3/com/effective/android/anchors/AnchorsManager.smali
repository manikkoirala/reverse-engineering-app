.class public final Lcom/effective/android/anchors/AnchorsManager;
.super Ljava/lang/Object;
.source "AnchorsManager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/effective/android/anchors/AnchorsManager$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final Oo08:Lcom/effective/android/anchors/AnchorsManager$Companion;


# instance fields
.field private final O8:Lcom/effective/android/anchors/AnchorsRuntime;

.field private 〇080:Z

.field private 〇o00〇〇Oo:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private 〇o〇:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/effective/android/anchors/task/lock/LockableAnchor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/effective/android/anchors/AnchorsManager$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/effective/android/anchors/AnchorsManager$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/effective/android/anchors/AnchorsManager;->Oo08:Lcom/effective/android/anchors/AnchorsManager$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/effective/android/anchors/AnchorsManager;->〇o00〇〇Oo:Ljava/util/Set;

    .line 4
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/effective/android/anchors/AnchorsManager;->〇o〇:Ljava/util/HashMap;

    .line 5
    new-instance v0, Lcom/effective/android/anchors/AnchorsRuntime;

    invoke-direct {v0, p1}, Lcom/effective/android/anchors/AnchorsRuntime;-><init>(Ljava/util/concurrent/ExecutorService;)V

    iput-object v0, p0, Lcom/effective/android/anchors/AnchorsManager;->O8:Lcom/effective/android/anchors/AnchorsRuntime;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/concurrent/ExecutorService;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/effective/android/anchors/AnchorsManager;-><init>(Ljava/util/concurrent/ExecutorService;)V

    return-void
.end method

.method private final 〇o00〇〇Oo()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/effective/android/anchors/AnchorsManager;->〇080:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const-string v0, "ANCHOR_DETAIL"

    .line 7
    .line 8
    const-string v1, "All anchors were released\uff01"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/effective/android/anchors/log/Logger;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
.end method

.method private final 〇o〇()Z
    .locals 6

    .line 1
    iget-boolean v0, p0, Lcom/effective/android/anchors/AnchorsManager;->〇080:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return v0

    .line 7
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    iget-object v1, p0, Lcom/effective/android/anchors/AnchorsManager;->O8:Lcom/effective/android/anchors/AnchorsRuntime;

    .line 13
    .line 14
    invoke-virtual {v1}, Lcom/effective/android/anchors/AnchorsRuntime;->OO0o〇〇〇〇0()Z

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    if-eqz v1, :cond_2

    .line 19
    .line 20
    const-string v2, "has some anchors\uff01"

    .line 21
    .line 22
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    const-string v2, "( "

    .line 26
    .line 27
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    iget-object v2, p0, Lcom/effective/android/anchors/AnchorsManager;->O8:Lcom/effective/android/anchors/AnchorsRuntime;

    .line 31
    .line 32
    invoke-virtual {v2}, Lcom/effective/android/anchors/AnchorsRuntime;->Oo08()Ljava/util/Set;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 37
    .line 38
    .line 39
    move-result-object v2

    .line 40
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 41
    .line 42
    .line 43
    move-result v3

    .line 44
    if-eqz v3, :cond_1

    .line 45
    .line 46
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 47
    .line 48
    .line 49
    move-result-object v3

    .line 50
    check-cast v3, Ljava/lang/String;

    .line 51
    .line 52
    new-instance v4, Ljava/lang/StringBuilder;

    .line 53
    .line 54
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 55
    .line 56
    .line 57
    const/16 v5, 0x22

    .line 58
    .line 59
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    const-string v3, "\" "

    .line 66
    .line 67
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v3

    .line 74
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_1
    const-string v2, ")"

    .line 79
    .line 80
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    goto :goto_1

    .line 84
    :cond_2
    const-string v2, "has no any anchor\uff01"

    .line 85
    .line 86
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    .line 88
    .line 89
    :goto_1
    iget-boolean v2, p0, Lcom/effective/android/anchors/AnchorsManager;->〇080:Z

    .line 90
    .line 91
    if-eqz v2, :cond_3

    .line 92
    .line 93
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    const-string/jumbo v2, "stringAnchorsManagerBuilder.toString()"

    .line 98
    .line 99
    .line 100
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->O8(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    .line 102
    .line 103
    const-string v2, "ANCHOR_DETAIL"

    .line 104
    .line 105
    invoke-static {v2, v0}, Lcom/effective/android/anchors/log/Logger;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/Object;)V

    .line 106
    .line 107
    .line 108
    :cond_3
    return v1
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final 〇〇888()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/effective/android/anchors/AnchorsManager;->O8:Lcom/effective/android/anchors/AnchorsRuntime;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/effective/android/anchors/AnchorsRuntime;->〇o〇()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/effective/android/anchors/AnchorsManager;->O8:Lcom/effective/android/anchors/AnchorsRuntime;

    .line 7
    .line 8
    iget-boolean v1, p0, Lcom/effective/android/anchors/AnchorsManager;->〇080:Z

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/effective/android/anchors/AnchorsRuntime;->OO0o〇〇(Z)V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/effective/android/anchors/AnchorsManager;->O8:Lcom/effective/android/anchors/AnchorsRuntime;

    .line 14
    .line 15
    iget-object v1, p0, Lcom/effective/android/anchors/AnchorsManager;->〇o00〇〇Oo:Ljava/util/Set;

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Lcom/effective/android/anchors/AnchorsRuntime;->〇080(Ljava/util/Set;)V

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/effective/android/anchors/AnchorsManager;->〇o00〇〇Oo:Ljava/util/Set;

    .line 21
    .line 22
    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method


# virtual methods
.method public final O8(Lcom/effective/android/anchors/task/Task;)Lcom/effective/android/anchors/task/lock/LockableAnchor;
    .locals 3
    .param p1    # Lcom/effective/android/anchors/task/Task;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string/jumbo v0, "task"

    .line 2
    .line 3
    .line 4
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇〇888(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    new-instance v0, Lcom/effective/android/anchors/task/lock/LockableAnchor;

    .line 8
    .line 9
    iget-object v1, p0, Lcom/effective/android/anchors/AnchorsManager;->O8:Lcom/effective/android/anchors/AnchorsRuntime;

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/effective/android/anchors/AnchorsRuntime;->〇〇888()Landroid/os/Handler;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-direct {v0, v1}, Lcom/effective/android/anchors/task/lock/LockableAnchor;-><init>(Landroid/os/Handler;)V

    .line 16
    .line 17
    .line 18
    new-instance v1, Lcom/effective/android/anchors/task/lock/LockableTask;

    .line 19
    .line 20
    invoke-direct {v1, p1, v0}, Lcom/effective/android/anchors/task/lock/LockableTask;-><init>(Lcom/effective/android/anchors/task/Task;Lcom/effective/android/anchors/task/lock/LockableAnchor;)V

    .line 21
    .line 22
    .line 23
    invoke-static {v1, p1}, Lcom/effective/android/anchors/util/Utils;->〇o〇(Lcom/effective/android/anchors/task/Task;Lcom/effective/android/anchors/task/Task;)V

    .line 24
    .line 25
    .line 26
    iget-object v1, p0, Lcom/effective/android/anchors/AnchorsManager;->〇o〇:Ljava/util/HashMap;

    .line 27
    .line 28
    invoke-virtual {p1}, Lcom/effective/android/anchors/task/Task;->getId()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    new-instance v1, Lcom/effective/android/anchors/AnchorsManager$requestBlockWhenFinish$1;

    .line 36
    .line 37
    invoke-direct {v1, p0, p1}, Lcom/effective/android/anchors/AnchorsManager$requestBlockWhenFinish$1;-><init>(Lcom/effective/android/anchors/AnchorsManager;Lcom/effective/android/anchors/task/Task;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v1}, Lcom/effective/android/anchors/task/lock/LockableAnchor;->〇o00〇〇Oo(Lcom/effective/android/anchors/task/lock/LockableAnchor$ReleaseListener;)V

    .line 41
    .line 42
    .line 43
    return-object v0
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public final Oo08(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/effective/android/anchors/AnchorsManager;->〇080:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final o〇0(Lcom/effective/android/anchors/task/Task;)V
    .locals 1
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    .line 1
    invoke-static {}, Lcom/effective/android/anchors/util/Utils;->〇080()V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_2

    .line 5
    .line 6
    invoke-direct {p0}, Lcom/effective/android/anchors/AnchorsManager;->〇〇888()V

    .line 7
    .line 8
    .line 9
    instance-of v0, p1, Lcom/effective/android/anchors/task/project/Project;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    check-cast p1, Lcom/effective/android/anchors/task/project/Project;

    .line 14
    .line 15
    invoke-virtual {p1}, Lcom/effective/android/anchors/task/project/Project;->〇oo〇()Lcom/effective/android/anchors/task/Task;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    :cond_0
    iget-object v0, p0, Lcom/effective/android/anchors/AnchorsManager;->O8:Lcom/effective/android/anchors/AnchorsRuntime;

    .line 20
    .line 21
    invoke-virtual {v0, p1}, Lcom/effective/android/anchors/AnchorsRuntime;->〇O00(Lcom/effective/android/anchors/task/Task;)V

    .line 22
    .line 23
    .line 24
    invoke-direct {p0}, Lcom/effective/android/anchors/AnchorsManager;->〇o〇()Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    invoke-virtual {p1}, Lcom/effective/android/anchors/task/Task;->〇0〇O0088o()V

    .line 29
    .line 30
    .line 31
    iget-object p1, p0, Lcom/effective/android/anchors/AnchorsManager;->O8:Lcom/effective/android/anchors/AnchorsRuntime;

    .line 32
    .line 33
    invoke-virtual {p1}, Lcom/effective/android/anchors/AnchorsRuntime;->〇0〇O0088o()V

    .line 34
    .line 35
    .line 36
    if-eqz v0, :cond_1

    .line 37
    .line 38
    invoke-direct {p0}, Lcom/effective/android/anchors/AnchorsManager;->〇o00〇〇Oo()V

    .line 39
    .line 40
    .line 41
    :cond_1
    return-void

    .line 42
    :cond_2
    new-instance p1, Ljava/lang/RuntimeException;

    .line 43
    .line 44
    const-string v0, "can no run a task that was null !"

    .line 45
    .line 46
    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    throw p1
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public final varargs 〇080([Ljava/lang/String;)Lcom/effective/android/anchors/AnchorsManager;
    .locals 6
    .param p1    # [Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string/jumbo v0, "taskIds"

    .line 2
    .line 3
    .line 4
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇〇888(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    array-length v0, p1

    .line 8
    const/4 v1, 0x0

    .line 9
    const/4 v2, 0x1

    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    const/4 v0, 0x1

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    :goto_0
    xor-int/2addr v0, v2

    .line 16
    if-eqz v0, :cond_3

    .line 17
    .line 18
    array-length v0, p1

    .line 19
    const/4 v3, 0x0

    .line 20
    :goto_1
    if-ge v3, v0, :cond_3

    .line 21
    .line 22
    aget-object v4, p1, v3

    .line 23
    .line 24
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    .line 25
    .line 26
    .line 27
    move-result v5

    .line 28
    if-lez v5, :cond_1

    .line 29
    .line 30
    const/4 v5, 0x1

    .line 31
    goto :goto_2

    .line 32
    :cond_1
    const/4 v5, 0x0

    .line 33
    :goto_2
    if-eqz v5, :cond_2

    .line 34
    .line 35
    iget-object v5, p0, Lcom/effective/android/anchors/AnchorsManager;->〇o00〇〇Oo:Ljava/util/Set;

    .line 36
    .line 37
    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 38
    .line 39
    .line 40
    :cond_2
    add-int/lit8 v3, v3, 0x1

    .line 41
    .line 42
    goto :goto_1

    .line 43
    :cond_3
    return-object p0
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

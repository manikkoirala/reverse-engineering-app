.class public final Lcom/effective/android/anchors/task/lock/LockableTask;
.super Lcom/effective/android/anchors/task/Task;
.source "LockableTask.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final o8〇OO0〇0o:Lcom/effective/android/anchors/task/lock/LockableAnchor;


# direct methods
.method public constructor <init>(Lcom/effective/android/anchors/task/Task;Lcom/effective/android/anchors/task/lock/LockableAnchor;)V
    .locals 2
    .param p1    # Lcom/effective/android/anchors/task/Task;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/effective/android/anchors/task/lock/LockableAnchor;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string/jumbo v0, "wait"

    .line 2
    .line 3
    .line 4
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇〇888(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    const-string v0, "lockableAnchor"

    .line 8
    .line 9
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->〇〇888(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    new-instance v0, Ljava/lang/StringBuilder;

    .line 13
    .line 14
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/effective/android/anchors/task/Task;->getId()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    const-string v1, "_waiter"

    .line 25
    .line 26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    const/4 v1, 0x1

    .line 34
    invoke-direct {p0, v0, v1}, Lcom/effective/android/anchors/task/Task;-><init>(Ljava/lang/String;Z)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p1}, Lcom/effective/android/anchors/task/Task;->getId()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    invoke-virtual {p2, p1}, Lcom/effective/android/anchors/task/lock/LockableAnchor;->Oo08(Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    iput-object p2, p0, Lcom/effective/android/anchors/task/lock/LockableTask;->o8〇OO0〇0o:Lcom/effective/android/anchors/task/lock/LockableAnchor;

    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
.end method


# virtual methods
.method public final oo88o8O()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/effective/android/anchors/task/lock/LockableTask;->o8〇OO0〇0o:Lcom/effective/android/anchors/task/lock/LockableAnchor;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/effective/android/anchors/task/lock/LockableAnchor;->o〇0()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method protected 〇O00(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "name"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇〇888(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/effective/android/anchors/task/lock/LockableTask;->o8〇OO0〇0o:Lcom/effective/android/anchors/task/lock/LockableAnchor;

    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/effective/android/anchors/task/lock/LockableAnchor;->〇o〇()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.class public final Lcom/effective/android/anchors/AnchorThreadPool;
.super Ljava/lang/Object;
.source "AnchorThreadPool.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8:I

.field private final Oo08:J

.field private final o〇0:Ljava/util/concurrent/ThreadFactory;

.field private 〇080:Ljava/util/concurrent/ExecutorService;

.field private final 〇o00〇〇Oo:I

.field private final 〇o〇:I

.field private final 〇〇888:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .locals 11

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    iput v0, p0, Lcom/effective/android/anchors/AnchorThreadPool;->〇o00〇〇Oo:I

    .line 13
    .line 14
    add-int/lit8 v1, v0, -0x1

    .line 15
    .line 16
    const/16 v2, 0x8

    .line 17
    .line 18
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    const/4 v2, 0x4

    .line 23
    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    .line 24
    .line 25
    .line 26
    move-result v4

    .line 27
    iput v4, p0, Lcom/effective/android/anchors/AnchorThreadPool;->〇o〇:I

    .line 28
    .line 29
    mul-int/lit8 v0, v0, 0x2

    .line 30
    .line 31
    const/4 v1, 0x1

    .line 32
    add-int/lit8 v5, v0, 0x1

    .line 33
    .line 34
    iput v5, p0, Lcom/effective/android/anchors/AnchorThreadPool;->O8:I

    .line 35
    .line 36
    const-wide/16 v6, 0x1e

    .line 37
    .line 38
    iput-wide v6, p0, Lcom/effective/android/anchors/AnchorThreadPool;->Oo08:J

    .line 39
    .line 40
    new-instance v10, Lcom/effective/android/anchors/AnchorThreadPool$sThreadFactory$1;

    .line 41
    .line 42
    invoke-direct {v10}, Lcom/effective/android/anchors/AnchorThreadPool$sThreadFactory$1;-><init>()V

    .line 43
    .line 44
    .line 45
    iput-object v10, p0, Lcom/effective/android/anchors/AnchorThreadPool;->o〇0:Ljava/util/concurrent/ThreadFactory;

    .line 46
    .line 47
    new-instance v9, Ljava/util/concurrent/PriorityBlockingQueue;

    .line 48
    .line 49
    const/16 v0, 0x80

    .line 50
    .line 51
    invoke-direct {v9, v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>(I)V

    .line 52
    .line 53
    .line 54
    iput-object v9, p0, Lcom/effective/android/anchors/AnchorThreadPool;->〇〇888:Ljava/util/concurrent/BlockingQueue;

    .line 55
    .line 56
    if-nez p1, :cond_0

    .line 57
    .line 58
    new-instance p1, Ljava/util/concurrent/ThreadPoolExecutor;

    .line 59
    .line 60
    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 61
    .line 62
    move-object v3, p1

    .line 63
    invoke-direct/range {v3 .. v10}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    .line 64
    .line 65
    .line 66
    invoke-virtual {p1, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->allowCoreThreadTimeOut(Z)V

    .line 67
    .line 68
    .line 69
    :cond_0
    iput-object p1, p0, Lcom/effective/android/anchors/AnchorThreadPool;->〇080:Ljava/util/concurrent/ExecutorService;

    .line 70
    .line 71
    return-void
.end method


# virtual methods
.method public final 〇080()Ljava/util/concurrent/ExecutorService;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/effective/android/anchors/AnchorThreadPool;->〇080:Ljava/util/concurrent/ExecutorService;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.class public Lcom/evernote/edam/limits/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static final EDAM_APPLICATIONDATA_ENTRY_LEN_MAX:I = 0xfff

.field public static final EDAM_APPLICATIONDATA_NAME_LEN_MAX:I = 0x20

.field public static final EDAM_APPLICATIONDATA_NAME_LEN_MIN:I = 0x3

.field public static final EDAM_APPLICATIONDATA_NAME_REGEX:Ljava/lang/String; = "^[A-Za-z0-9_.-]{3,32}$"

.field public static final EDAM_APPLICATIONDATA_VALUE_LEN_MAX:I = 0xffc

.field public static final EDAM_APPLICATIONDATA_VALUE_LEN_MIN:I = 0x0

.field public static final EDAM_APPLICATIONDATA_VALUE_REGEX:Ljava/lang/String; = "^[^\\p{Cc}]{0,4092}$"

.field public static final EDAM_ATTRIBUTE_LEN_MAX:I = 0x1000

.field public static final EDAM_ATTRIBUTE_LEN_MIN:I = 0x1

.field public static final EDAM_ATTRIBUTE_LIST_MAX:I = 0x64

.field public static final EDAM_ATTRIBUTE_MAP_MAX:I = 0x64

.field public static final EDAM_ATTRIBUTE_REGEX:Ljava/lang/String; = "^[^\\p{Cc}\\p{Zl}\\p{Zp}]{1,4096}$"

.field public static final EDAM_BUSINESS_NOTEBOOKS_MAX:I = 0x1388

.field public static final EDAM_BUSINESS_NOTEBOOK_DESCRIPTION_LEN_MAX:I = 0xc8

.field public static final EDAM_BUSINESS_NOTEBOOK_DESCRIPTION_LEN_MIN:I = 0x1

.field public static final EDAM_BUSINESS_NOTEBOOK_DESCRIPTION_REGEX:Ljava/lang/String; = "^[^\\p{Cc}\\p{Z}]([^\\p{Cc}\\p{Zl}\\p{Zp}]{0,198}[^\\p{Cc}\\p{Z}])?$"

.field public static final EDAM_BUSINESS_NOTES_MAX:I = 0x7a120

.field public static final EDAM_BUSINESS_PHONE_NUMBER_LEN_MAX:I = 0x14

.field public static final EDAM_BUSINESS_TAGS_MAX:I = 0x186a0

.field public static final EDAM_BUSINESS_URI_LEN_MAX:I = 0x20

.field public static final EDAM_CONTENT_CLASS_FOOD_MEAL:Ljava/lang/String; = "evernote.food.meal"

.field public static final EDAM_CONTENT_CLASS_HELLO_ENCOUNTER:Ljava/lang/String; = "evernote.hello.encounter"

.field public static final EDAM_CONTENT_CLASS_HELLO_PROFILE:Ljava/lang/String; = "evernote.hello.profile"

.field public static final EDAM_CONTENT_CLASS_PENULTIMATE_NOTEBOOK:Ljava/lang/String; = "evernote.penultimate.notebook"

.field public static final EDAM_CONTENT_CLASS_PENULTIMATE_PREFIX:Ljava/lang/String; = "evernote.penultimate."

.field public static final EDAM_CONTENT_CLASS_SKITCH:Ljava/lang/String; = "evernote.skitch"

.field public static final EDAM_CONTENT_CLASS_SKITCH_PDF:Ljava/lang/String; = "evernote.skitch.pdf"

.field public static final EDAM_CONTENT_CLASS_SKITCH_PREFIX:Ljava/lang/String; = "evernote.skitch"

.field public static final EDAM_DEVICE_DESCRIPTION_LEN_MAX:I = 0x40

.field public static final EDAM_DEVICE_DESCRIPTION_REGEX:Ljava/lang/String; = "^[^\\p{Cc}]{1,64}$"

.field public static final EDAM_DEVICE_ID_LEN_MAX:I = 0x20

.field public static final EDAM_DEVICE_ID_REGEX:Ljava/lang/String; = "^[^\\p{Cc}]{1,32}$"

.field public static final EDAM_EMAIL_DOMAIN_REGEX:Ljava/lang/String; = "^[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*\\.([A-Za-z]{2,})$"

.field public static final EDAM_EMAIL_LEN_MAX:I = 0xff

.field public static final EDAM_EMAIL_LEN_MIN:I = 0x6

.field public static final EDAM_EMAIL_LOCAL_REGEX:Ljava/lang/String; = "^[A-Za-z0-9!#$%&\'*+/=?^_`{|}~-]+(\\.[A-Za-z0-9!#$%&\'*+/=?^_`{|}~-]+)*$"

.field public static final EDAM_EMAIL_REGEX:Ljava/lang/String; = "^[A-Za-z0-9!#$%&\'*+/=?^_`{|}~-]+(\\.[A-Za-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*\\.([A-Za-z]{2,})$"

.field public static final EDAM_FOOD_APP_CONTENT_CLASS_PREFIX:Ljava/lang/String; = "evernote.food."

.field public static final EDAM_GUID_LEN_MAX:I = 0x24

.field public static final EDAM_GUID_LEN_MIN:I = 0x24

.field public static final EDAM_GUID_REGEX:Ljava/lang/String; = "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"

.field public static final EDAM_HASH_LEN:I = 0x10

.field public static final EDAM_HELLO_APP_CONTENT_CLASS_PREFIX:Ljava/lang/String; = "evernote.hello."

.field public static final EDAM_INDEXABLE_RESOURCE_MIME_TYPES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final EDAM_MAX_PREFERENCES:I = 0x64

.field public static final EDAM_MAX_VALUES_PER_PREFERENCE:I = 0x100

.field public static final EDAM_MIME_LEN_MAX:I = 0xff

.field public static final EDAM_MIME_LEN_MIN:I = 0x3

.field public static final EDAM_MIME_REGEX:Ljava/lang/String; = "^[A-Za-z]+/[A-Za-z0-9._+-]+$"

.field public static final EDAM_MIME_TYPES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final EDAM_MIME_TYPE_AAC:Ljava/lang/String; = "audio/aac"

.field public static final EDAM_MIME_TYPE_AMR:Ljava/lang/String; = "audio/amr"

.field public static final EDAM_MIME_TYPE_DEFAULT:Ljava/lang/String; = "application/octet-stream"

.field public static final EDAM_MIME_TYPE_GIF:Ljava/lang/String; = "image/gif"

.field public static final EDAM_MIME_TYPE_INK:Ljava/lang/String; = "application/vnd.evernote.ink"

.field public static final EDAM_MIME_TYPE_JPEG:Ljava/lang/String; = "image/jpeg"

.field public static final EDAM_MIME_TYPE_M4A:Ljava/lang/String; = "audio/mp4"

.field public static final EDAM_MIME_TYPE_MP3:Ljava/lang/String; = "audio/mpeg"

.field public static final EDAM_MIME_TYPE_MP4_VIDEO:Ljava/lang/String; = "video/mp4"

.field public static final EDAM_MIME_TYPE_PDF:Ljava/lang/String; = "application/pdf"

.field public static final EDAM_MIME_TYPE_PNG:Ljava/lang/String; = "image/png"

.field public static final EDAM_MIME_TYPE_WAV:Ljava/lang/String; = "audio/wav"

.field public static final EDAM_NOTEBOOK_NAME_LEN_MAX:I = 0x64

.field public static final EDAM_NOTEBOOK_NAME_LEN_MIN:I = 0x1

.field public static final EDAM_NOTEBOOK_NAME_REGEX:Ljava/lang/String; = "^[^\\p{Cc}\\p{Z}]([^\\p{Cc}\\p{Zl}\\p{Zp}]{0,98}[^\\p{Cc}\\p{Z}])?$"

.field public static final EDAM_NOTEBOOK_SHARED_NOTEBOOK_MAX:I = 0xfa

.field public static final EDAM_NOTEBOOK_STACK_LEN_MAX:I = 0x64

.field public static final EDAM_NOTEBOOK_STACK_LEN_MIN:I = 0x1

.field public static final EDAM_NOTEBOOK_STACK_REGEX:Ljava/lang/String; = "^[^\\p{Cc}\\p{Z}]([^\\p{Cc}\\p{Zl}\\p{Zp}]{0,98}[^\\p{Cc}\\p{Z}])?$"

.field public static final EDAM_NOTE_CONTENT_CLASS_LEN_MAX:I = 0x20

.field public static final EDAM_NOTE_CONTENT_CLASS_LEN_MIN:I = 0x3

.field public static final EDAM_NOTE_CONTENT_CLASS_REGEX:Ljava/lang/String; = "^[A-Za-z0-9_.-]{3,32}$"

.field public static final EDAM_NOTE_CONTENT_LEN_MAX:I = 0x500000

.field public static final EDAM_NOTE_CONTENT_LEN_MIN:I = 0x0

.field public static final EDAM_NOTE_RESOURCES_MAX:I = 0x3e8

.field public static final EDAM_NOTE_SIZE_MAX_FREE:I = 0x1900000

.field public static final EDAM_NOTE_SIZE_MAX_PREMIUM:I = 0x6400000

.field public static final EDAM_NOTE_TAGS_MAX:I = 0x64

.field public static final EDAM_NOTE_TITLE_LEN_MAX:I = 0xff

.field public static final EDAM_NOTE_TITLE_LEN_MIN:I = 0x1

.field public static final EDAM_NOTE_TITLE_REGEX:Ljava/lang/String; = "^[^\\p{Cc}\\p{Z}]([^\\p{Cc}\\p{Zl}\\p{Zp}]{0,253}[^\\p{Cc}\\p{Z}])?$"

.field public static final EDAM_PREFERENCE_NAME_LEN_MAX:I = 0x20

.field public static final EDAM_PREFERENCE_NAME_LEN_MIN:I = 0x3

.field public static final EDAM_PREFERENCE_NAME_REGEX:Ljava/lang/String; = "^[A-Za-z0-9_.-]{3,32}$"

.field public static final EDAM_PREFERENCE_SHORTCUTS:Ljava/lang/String; = "evernote.shortcuts"

.field public static final EDAM_PREFERENCE_SHORTCUTS_MAX_VALUES:I = 0xfa

.field public static final EDAM_PREFERENCE_VALUE_LEN_MAX:I = 0x400

.field public static final EDAM_PREFERENCE_VALUE_LEN_MIN:I = 0x1

.field public static final EDAM_PREFERENCE_VALUE_REGEX:Ljava/lang/String; = "^[^\\p{Cc}]{1,1024}$"

.field public static final EDAM_PUBLISHING_DESCRIPTION_LEN_MAX:I = 0xc8

.field public static final EDAM_PUBLISHING_DESCRIPTION_LEN_MIN:I = 0x1

.field public static final EDAM_PUBLISHING_DESCRIPTION_REGEX:Ljava/lang/String; = "^[^\\p{Cc}\\p{Z}]([^\\p{Cc}\\p{Zl}\\p{Zp}]{0,198}[^\\p{Cc}\\p{Z}])?$"

.field public static final EDAM_PUBLISHING_URI_LEN_MAX:I = 0xff

.field public static final EDAM_PUBLISHING_URI_LEN_MIN:I = 0x1

.field public static final EDAM_PUBLISHING_URI_PROHIBITED:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final EDAM_PUBLISHING_URI_REGEX:Ljava/lang/String; = "^[a-zA-Z0-9.~_+-]{1,255}$"

.field public static final EDAM_RELATED_MAX_NOTEBOOKS:I = 0x1

.field public static final EDAM_RELATED_MAX_NOTES:I = 0x19

.field public static final EDAM_RELATED_MAX_TAGS:I = 0x19

.field public static final EDAM_RELATED_PLAINTEXT_LEN_MAX:I = 0x20000

.field public static final EDAM_RELATED_PLAINTEXT_LEN_MIN:I = 0x1

.field public static final EDAM_RESOURCE_SIZE_MAX_FREE:I = 0x1900000

.field public static final EDAM_RESOURCE_SIZE_MAX_PREMIUM:I = 0x6400000

.field public static final EDAM_SAVED_SEARCH_NAME_LEN_MAX:I = 0x64

.field public static final EDAM_SAVED_SEARCH_NAME_LEN_MIN:I = 0x1

.field public static final EDAM_SAVED_SEARCH_NAME_REGEX:Ljava/lang/String; = "^[^\\p{Cc}\\p{Z}]([^\\p{Cc}\\p{Zl}\\p{Zp}]{0,98}[^\\p{Cc}\\p{Z}])?$"

.field public static final EDAM_SEARCH_QUERY_LEN_MAX:I = 0x400

.field public static final EDAM_SEARCH_QUERY_LEN_MIN:I = 0x0

.field public static final EDAM_SEARCH_QUERY_REGEX:Ljava/lang/String; = "^[^\\p{Cc}\\p{Zl}\\p{Zp}]{0,1024}$"

.field public static final EDAM_SEARCH_SUGGESTIONS_MAX:I = 0xa

.field public static final EDAM_SEARCH_SUGGESTIONS_PREFIX_LEN_MAX:I = 0x400

.field public static final EDAM_SEARCH_SUGGESTIONS_PREFIX_LEN_MIN:I = 0x2

.field public static final EDAM_TAG_NAME_LEN_MAX:I = 0x64

.field public static final EDAM_TAG_NAME_LEN_MIN:I = 0x1

.field public static final EDAM_TAG_NAME_REGEX:Ljava/lang/String; = "^[^,\\p{Cc}\\p{Z}]([^,\\p{Cc}\\p{Zl}\\p{Zp}]{0,98}[^,\\p{Cc}\\p{Z}])?$"

.field public static final EDAM_TIMEZONE_LEN_MAX:I = 0x20

.field public static final EDAM_TIMEZONE_LEN_MIN:I = 0x1

.field public static final EDAM_TIMEZONE_REGEX:Ljava/lang/String; = "^([A-Za-z_-]+(/[A-Za-z_-]+)*)|(GMT(-|\\+)[0-9]{1,2}(:[0-9]{2})?)$"

.field public static final EDAM_USER_LINKED_NOTEBOOK_MAX:I = 0x64

.field public static final EDAM_USER_LINKED_NOTEBOOK_MAX_PREMIUM:I = 0xfa

.field public static final EDAM_USER_MAIL_LIMIT_DAILY_FREE:I = 0x32

.field public static final EDAM_USER_MAIL_LIMIT_DAILY_PREMIUM:I = 0xc8

.field public static final EDAM_USER_NAME_LEN_MAX:I = 0xff

.field public static final EDAM_USER_NAME_LEN_MIN:I = 0x1

.field public static final EDAM_USER_NAME_REGEX:Ljava/lang/String; = "^[^\\p{Cc}\\p{Zl}\\p{Zp}]{1,255}$"

.field public static final EDAM_USER_NOTEBOOKS_MAX:I = 0xfa

.field public static final EDAM_USER_NOTES_MAX:I = 0x186a0

.field public static final EDAM_USER_PASSWORD_LEN_MAX:I = 0x40

.field public static final EDAM_USER_PASSWORD_LEN_MIN:I = 0x6

.field public static final EDAM_USER_PASSWORD_REGEX:Ljava/lang/String; = "^[A-Za-z0-9!#$%&\'()*+,./:;<=>?@^_`{|}~\\[\\]\\\\-]{6,64}$"

.field public static final EDAM_USER_RECENT_MAILED_ADDRESSES_MAX:I = 0xa

.field public static final EDAM_USER_SAVED_SEARCHES_MAX:I = 0x64

.field public static final EDAM_USER_TAGS_MAX:I = 0x186a0

.field public static final EDAM_USER_UPLOAD_LIMIT_BUSINESS:J = 0x7fffffffL

.field public static final EDAM_USER_UPLOAD_LIMIT_FREE:J = 0x3c00000L

.field public static final EDAM_USER_UPLOAD_LIMIT_PREMIUM:J = 0x40000000L

.field public static final EDAM_USER_USERNAME_LEN_MAX:I = 0x40

.field public static final EDAM_USER_USERNAME_LEN_MIN:I = 0x1

.field public static final EDAM_USER_USERNAME_REGEX:Ljava/lang/String; = "^[a-z0-9]([a-z0-9_-]{0,62}[a-z0-9])?$"

.field public static final EDAM_VAT_REGEX:Ljava/lang/String; = "^((AT)?U[0-9]{8}|(BE)?0?[0-9]{9}|(BG)?[0-9]{9,10}|(CY)?[0-9]{8}L|(CZ)?[0-9]{8,10}|(DE)?[0-9]{9}|(DK)?[0-9]{8}|(EE)?[0-9]{9}|(EL|GR)?[0-9]{9}|(ES)?[0-9A-Z][0-9]{7}[0-9A-Z]|(FI)?[0-9]{8}|(FR)?[0-9A-Z]{2}[0-9]{9}|(GB)?([0-9]{9}([0-9]{3})?|[A-Z]{2}[0-9]{3})|(HU)?[0-9]{8}|(IE)?[0-9]S[0-9]{5}L|(IT)?[0-9]{11}|(LT)?([0-9]{9}|[0-9]{12})|(LU)?[0-9]{8}|(LV)?[0-9]{11}|(MT)?[0-9]{8}|(NL)?[0-9]{9}B[0-9]{2}|(PL)?[0-9]{10}|(PT)?[0-9]{9}|(RO)?[0-9]{2,10}|(SE)?[0-9]{12}|(SI)?[0-9]{8}|(SK)?[0-9]{10})|[0-9]{9}MVA|[0-9]{6}|CHE[0-9]{9}(TVA|MWST|IVA)$"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Ljava/util/HashSet;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/evernote/edam/limits/Constants;->EDAM_MIME_TYPES:Ljava/util/Set;

    .line 7
    .line 8
    const-string v1, "image/gif"

    .line 9
    .line 10
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 11
    .line 12
    .line 13
    const-string v1, "image/jpeg"

    .line 14
    .line 15
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 16
    .line 17
    .line 18
    const-string v1, "image/png"

    .line 19
    .line 20
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 21
    .line 22
    .line 23
    const-string v1, "audio/wav"

    .line 24
    .line 25
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 26
    .line 27
    .line 28
    const-string v1, "audio/mpeg"

    .line 29
    .line 30
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 31
    .line 32
    .line 33
    const-string v1, "audio/amr"

    .line 34
    .line 35
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    const-string v1, "application/vnd.evernote.ink"

    .line 39
    .line 40
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 41
    .line 42
    .line 43
    const-string v1, "application/pdf"

    .line 44
    .line 45
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 46
    .line 47
    .line 48
    const-string/jumbo v1, "video/mp4"

    .line 49
    .line 50
    .line 51
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 52
    .line 53
    .line 54
    const-string v1, "audio/aac"

    .line 55
    .line 56
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 57
    .line 58
    .line 59
    const-string v1, "audio/mp4"

    .line 60
    .line 61
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 62
    .line 63
    .line 64
    new-instance v0, Ljava/util/HashSet;

    .line 65
    .line 66
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 67
    .line 68
    .line 69
    sput-object v0, Lcom/evernote/edam/limits/Constants;->EDAM_INDEXABLE_RESOURCE_MIME_TYPES:Ljava/util/Set;

    .line 70
    .line 71
    const-string v1, "application/msword"

    .line 72
    .line 73
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 74
    .line 75
    .line 76
    const-string v1, "application/mspowerpoint"

    .line 77
    .line 78
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 79
    .line 80
    .line 81
    const-string v1, "application/excel"

    .line 82
    .line 83
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 84
    .line 85
    .line 86
    const-string v1, "application/vnd.ms-word"

    .line 87
    .line 88
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 89
    .line 90
    .line 91
    const-string v1, "application/vnd.ms-powerpoint"

    .line 92
    .line 93
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 94
    .line 95
    .line 96
    const-string v1, "application/vnd.ms-excel"

    .line 97
    .line 98
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 99
    .line 100
    .line 101
    const-string v1, "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

    .line 102
    .line 103
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 104
    .line 105
    .line 106
    const-string v1, "application/vnd.openxmlformats-officedocument.presentationml.presentation"

    .line 107
    .line 108
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 109
    .line 110
    .line 111
    const-string v1, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    .line 112
    .line 113
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 114
    .line 115
    .line 116
    const-string v1, "application/vnd.apple.pages"

    .line 117
    .line 118
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 119
    .line 120
    .line 121
    const-string v1, "application/vnd.apple.numbers"

    .line 122
    .line 123
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 124
    .line 125
    .line 126
    const-string v1, "application/vnd.apple.keynote"

    .line 127
    .line 128
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 129
    .line 130
    .line 131
    const-string v1, "application/x-iwork-pages-sffpages"

    .line 132
    .line 133
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 134
    .line 135
    .line 136
    const-string v1, "application/x-iwork-numbers-sffnumbers"

    .line 137
    .line 138
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 139
    .line 140
    .line 141
    const-string v1, "application/x-iwork-keynote-sffkey"

    .line 142
    .line 143
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 144
    .line 145
    .line 146
    new-instance v0, Ljava/util/HashSet;

    .line 147
    .line 148
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 149
    .line 150
    .line 151
    sput-object v0, Lcom/evernote/edam/limits/Constants;->EDAM_PUBLISHING_URI_PROHIBITED:Ljava/util/Set;

    .line 152
    .line 153
    const-string v1, ".."

    .line 154
    .line 155
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 156
    .line 157
    .line 158
    return-void
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.class public Lcom/evernote/edam/type/SharedNotebookRecipientSettings;
.super Ljava/lang/Object;
.source "SharedNotebookRecipientSettings.java"

# interfaces
.implements Lcom/evernote/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/evernote/thrift/TBase<",
        "Lcom/evernote/edam/type/SharedNotebookRecipientSettings;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final REMINDER_NOTIFY_EMAIL_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final REMINDER_NOTIFY_IN_APP_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

.field private static final __REMINDERNOTIFYEMAIL_ISSET_ID:I = 0x0

.field private static final __REMINDERNOTIFYINAPP_ISSET_ID:I = 0x1


# instance fields
.field private __isset_vector:[Z

.field private reminderNotifyEmail:Z

.field private reminderNotifyInApp:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 1
    new-instance v0, Lcom/evernote/thrift/protocol/TStruct;

    .line 2
    .line 3
    const-string v1, "SharedNotebookRecipientSettings"

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TStruct;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    sput-object v0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    .line 9
    .line 10
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    .line 11
    .line 12
    const/4 v1, 0x1

    .line 13
    const-string v2, "reminderNotifyEmail"

    .line 14
    .line 15
    const/4 v3, 0x2

    .line 16
    invoke-direct {v0, v2, v3, v1}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    .line 17
    .line 18
    .line 19
    sput-object v0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->REMINDER_NOTIFY_EMAIL_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 20
    .line 21
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    .line 22
    .line 23
    const-string v1, "reminderNotifyInApp"

    .line 24
    .line 25
    invoke-direct {v0, v1, v3, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    .line 26
    .line 27
    .line 28
    sput-object v0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->REMINDER_NOTIFY_IN_APP_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [Z

    .line 2
    iput-object v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->__isset_vector:[Z

    return-void
.end method

.method public constructor <init>(Lcom/evernote/edam/type/SharedNotebookRecipientSettings;)V
    .locals 4

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [Z

    .line 4
    iput-object v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->__isset_vector:[Z

    .line 5
    iget-object v1, p1, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->__isset_vector:[Z

    const/4 v2, 0x0

    array-length v3, v1

    invoke-static {v1, v2, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6
    iget-boolean v0, p1, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyEmail:Z

    iput-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyEmail:Z

    .line 7
    iget-boolean p1, p1, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyInApp:Z

    iput-boolean p1, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyInApp:Z

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->setReminderNotifyEmailIsSet(Z)V

    .line 3
    .line 4
    .line 5
    iput-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyEmail:Z

    .line 6
    .line 7
    invoke-virtual {p0, v0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->setReminderNotifyInAppIsSet(Z)V

    .line 8
    .line 9
    .line 10
    iput-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyInApp:Z

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
.end method

.method public compareTo(Lcom/evernote/edam/type/SharedNotebookRecipientSettings;)I
    .locals 2

    .line 2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 4
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyEmail()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyEmail()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 5
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyEmail()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyEmail:Z

    iget-boolean v1, p1, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyEmail:Z

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    .line 6
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyInApp()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyInApp()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    .line 7
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyInApp()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyInApp:Z

    iget-boolean p1, p1, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyInApp:Z

    invoke-static {v0, p1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(ZZ)I

    move-result p1

    if-eqz p1, :cond_4

    return p1

    :cond_4
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 1
    check-cast p1, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->compareTo(Lcom/evernote/edam/type/SharedNotebookRecipientSettings;)I

    move-result p1

    return p1
.end method

.method public deepCopy()Lcom/evernote/edam/type/SharedNotebookRecipientSettings;
    .locals 1

    .line 2
    new-instance v0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    invoke-direct {v0, p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;-><init>(Lcom/evernote/edam/type/SharedNotebookRecipientSettings;)V

    return-object v0
.end method

.method public bridge synthetic deepCopy()Lcom/evernote/thrift/TBase;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->deepCopy()Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    move-result-object v0

    return-object v0
.end method

.method public equals(Lcom/evernote/edam/type/SharedNotebookRecipientSettings;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 3
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyEmail()Z

    move-result v1

    .line 4
    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyEmail()Z

    move-result v2

    if-nez v1, :cond_1

    if-eqz v2, :cond_3

    :cond_1
    if-eqz v1, :cond_7

    if-nez v2, :cond_2

    goto :goto_0

    .line 5
    :cond_2
    iget-boolean v1, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyEmail:Z

    iget-boolean v2, p1, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyEmail:Z

    if-eq v1, v2, :cond_3

    return v0

    .line 6
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyInApp()Z

    move-result v1

    .line 7
    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyInApp()Z

    move-result v2

    if-nez v1, :cond_4

    if-eqz v2, :cond_6

    :cond_4
    if-eqz v1, :cond_7

    if-nez v2, :cond_5

    goto :goto_0

    .line 8
    :cond_5
    iget-boolean v1, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyInApp:Z

    iget-boolean p1, p1, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyInApp:Z

    if-eq v1, p1, :cond_6

    return v0

    :cond_6
    const/4 p1, 0x1

    return p1

    :cond_7
    :goto_0
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 1
    :cond_0
    instance-of v1, p1, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    if-eqz v1, :cond_1

    .line 2
    check-cast p1, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->equals(Lcom/evernote/edam/type/SharedNotebookRecipientSettings;)Z

    move-result p1

    return p1

    :cond_1
    return v0
.end method

.method public hashCode()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public isReminderNotifyEmail()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyEmail:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public isReminderNotifyInApp()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyInApp:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public isSetReminderNotifyEmail()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->__isset_vector:[Z

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    aget-boolean v0, v0, v1

    .line 5
    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public isSetReminderNotifyInApp()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->__isset_vector:[Z

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    aget-boolean v0, v0, v1

    .line 5
    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public read(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructBegin()Lcom/evernote/thrift/protocol/TStruct;

    .line 2
    .line 3
    .line 4
    :goto_0
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldBegin()Lcom/evernote/thrift/protocol/TField;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    .line 9
    .line 10
    if-nez v1, :cond_0

    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructEnd()V

    .line 13
    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->validate()V

    .line 16
    .line 17
    .line 18
    return-void

    .line 19
    :cond_0
    iget-short v0, v0, Lcom/evernote/thrift/protocol/TField;->id:S

    .line 20
    .line 21
    const/4 v2, 0x2

    .line 22
    const/4 v3, 0x1

    .line 23
    if-eq v0, v3, :cond_3

    .line 24
    .line 25
    if-eq v0, v2, :cond_1

    .line 26
    .line 27
    invoke-static {p1, v1}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    .line 28
    .line 29
    .line 30
    goto :goto_1

    .line 31
    :cond_1
    if-ne v1, v2, :cond_2

    .line 32
    .line 33
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    iput-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyInApp:Z

    .line 38
    .line 39
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->setReminderNotifyInAppIsSet(Z)V

    .line 40
    .line 41
    .line 42
    goto :goto_1

    .line 43
    :cond_2
    invoke-static {p1, v1}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    .line 44
    .line 45
    .line 46
    goto :goto_1

    .line 47
    :cond_3
    if-ne v1, v2, :cond_4

    .line 48
    .line 49
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readBool()Z

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    iput-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyEmail:Z

    .line 54
    .line 55
    invoke-virtual {p0, v3}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->setReminderNotifyEmailIsSet(Z)V

    .line 56
    .line 57
    .line 58
    goto :goto_1

    .line 59
    :cond_4
    invoke-static {p1, v1}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    .line 60
    .line 61
    .line 62
    :goto_1
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldEnd()V

    .line 63
    .line 64
    .line 65
    goto :goto_0
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public setReminderNotifyEmail(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyEmail:Z

    .line 2
    .line 3
    const/4 p1, 0x1

    .line 4
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->setReminderNotifyEmailIsSet(Z)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setReminderNotifyEmailIsSet(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->__isset_vector:[Z

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    aput-boolean p1, v0, v1

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setReminderNotifyInApp(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyInApp:Z

    .line 2
    .line 3
    const/4 p1, 0x1

    .line 4
    invoke-virtual {p0, p1}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->setReminderNotifyInAppIsSet(Z)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setReminderNotifyInAppIsSet(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->__isset_vector:[Z

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    aput-boolean p1, v0, v1

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    const-string v1, "SharedNotebookRecipientSettings("

    .line 4
    .line 5
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyEmail()Z

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    if-eqz v1, :cond_0

    .line 13
    .line 14
    const-string v1, "reminderNotifyEmail:"

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    iget-boolean v1, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyEmail:Z

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    const/4 v1, 0x0

    .line 25
    goto :goto_0

    .line 26
    :cond_0
    const/4 v1, 0x1

    .line 27
    :goto_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyInApp()Z

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    if-eqz v2, :cond_2

    .line 32
    .line 33
    if-nez v1, :cond_1

    .line 34
    .line 35
    const-string v1, ", "

    .line 36
    .line 37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    :cond_1
    const-string v1, "reminderNotifyInApp:"

    .line 41
    .line 42
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    iget-boolean v1, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyInApp:Z

    .line 46
    .line 47
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    :cond_2
    const-string v1, ")"

    .line 51
    .line 52
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    return-object v0
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public unsetReminderNotifyEmail()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->__isset_vector:[Z

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    aput-boolean v1, v0, v1

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public unsetReminderNotifyInApp()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->__isset_vector:[Z

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    const/4 v2, 0x0

    .line 5
    aput-boolean v2, v0, v1

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public validate()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public write(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->validate()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    .line 5
    .line 6
    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructBegin(Lcom/evernote/thrift/protocol/TStruct;)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyEmail()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    sget-object v0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->REMINDER_NOTIFY_EMAIL_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 16
    .line 17
    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 18
    .line 19
    .line 20
    iget-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyEmail:Z

    .line 21
    .line 22
    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 26
    .line 27
    .line 28
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->isSetReminderNotifyInApp()Z

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    if-eqz v0, :cond_1

    .line 33
    .line 34
    sget-object v0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->REMINDER_NOTIFY_IN_APP_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 35
    .line 36
    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 37
    .line 38
    .line 39
    iget-boolean v0, p0, Lcom/evernote/edam/type/SharedNotebookRecipientSettings;->reminderNotifyInApp:Z

    .line 40
    .line 41
    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeBool(Z)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 45
    .line 46
    .line 47
    :cond_1
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldStop()V

    .line 48
    .line 49
    .line 50
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructEnd()V

    .line 51
    .line 52
    .line 53
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

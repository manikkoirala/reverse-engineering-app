.class public final enum Lcom/evernote/edam/type/PrivilegeLevel;
.super Ljava/lang/Enum;
.source "PrivilegeLevel.java"

# interfaces
.implements Lcom/evernote/thrift/TEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/evernote/edam/type/PrivilegeLevel;",
        ">;",
        "Lcom/evernote/thrift/TEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/evernote/edam/type/PrivilegeLevel;

.field public static final enum ADMIN:Lcom/evernote/edam/type/PrivilegeLevel;

.field public static final enum MANAGER:Lcom/evernote/edam/type/PrivilegeLevel;

.field public static final enum NORMAL:Lcom/evernote/edam/type/PrivilegeLevel;

.field public static final enum PREMIUM:Lcom/evernote/edam/type/PrivilegeLevel;

.field public static final enum SUPPORT:Lcom/evernote/edam/type/PrivilegeLevel;

.field public static final enum VIP:Lcom/evernote/edam/type/PrivilegeLevel;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .line 1
    new-instance v0, Lcom/evernote/edam/type/PrivilegeLevel;

    .line 2
    .line 3
    const-string v1, "NORMAL"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    const/4 v3, 0x1

    .line 7
    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/edam/type/PrivilegeLevel;-><init>(Ljava/lang/String;II)V

    .line 8
    .line 9
    .line 10
    sput-object v0, Lcom/evernote/edam/type/PrivilegeLevel;->NORMAL:Lcom/evernote/edam/type/PrivilegeLevel;

    .line 11
    .line 12
    new-instance v1, Lcom/evernote/edam/type/PrivilegeLevel;

    .line 13
    .line 14
    const-string v4, "PREMIUM"

    .line 15
    .line 16
    const/4 v5, 0x3

    .line 17
    invoke-direct {v1, v4, v3, v5}, Lcom/evernote/edam/type/PrivilegeLevel;-><init>(Ljava/lang/String;II)V

    .line 18
    .line 19
    .line 20
    sput-object v1, Lcom/evernote/edam/type/PrivilegeLevel;->PREMIUM:Lcom/evernote/edam/type/PrivilegeLevel;

    .line 21
    .line 22
    new-instance v4, Lcom/evernote/edam/type/PrivilegeLevel;

    .line 23
    .line 24
    const-string v6, "VIP"

    .line 25
    .line 26
    const/4 v7, 0x2

    .line 27
    const/4 v8, 0x5

    .line 28
    invoke-direct {v4, v6, v7, v8}, Lcom/evernote/edam/type/PrivilegeLevel;-><init>(Ljava/lang/String;II)V

    .line 29
    .line 30
    .line 31
    sput-object v4, Lcom/evernote/edam/type/PrivilegeLevel;->VIP:Lcom/evernote/edam/type/PrivilegeLevel;

    .line 32
    .line 33
    new-instance v6, Lcom/evernote/edam/type/PrivilegeLevel;

    .line 34
    .line 35
    const-string v9, "MANAGER"

    .line 36
    .line 37
    const/4 v10, 0x7

    .line 38
    invoke-direct {v6, v9, v5, v10}, Lcom/evernote/edam/type/PrivilegeLevel;-><init>(Ljava/lang/String;II)V

    .line 39
    .line 40
    .line 41
    sput-object v6, Lcom/evernote/edam/type/PrivilegeLevel;->MANAGER:Lcom/evernote/edam/type/PrivilegeLevel;

    .line 42
    .line 43
    new-instance v9, Lcom/evernote/edam/type/PrivilegeLevel;

    .line 44
    .line 45
    const/16 v10, 0x8

    .line 46
    .line 47
    const-string v11, "SUPPORT"

    .line 48
    .line 49
    const/4 v12, 0x4

    .line 50
    invoke-direct {v9, v11, v12, v10}, Lcom/evernote/edam/type/PrivilegeLevel;-><init>(Ljava/lang/String;II)V

    .line 51
    .line 52
    .line 53
    sput-object v9, Lcom/evernote/edam/type/PrivilegeLevel;->SUPPORT:Lcom/evernote/edam/type/PrivilegeLevel;

    .line 54
    .line 55
    new-instance v10, Lcom/evernote/edam/type/PrivilegeLevel;

    .line 56
    .line 57
    const-string v11, "ADMIN"

    .line 58
    .line 59
    const/16 v13, 0x9

    .line 60
    .line 61
    invoke-direct {v10, v11, v8, v13}, Lcom/evernote/edam/type/PrivilegeLevel;-><init>(Ljava/lang/String;II)V

    .line 62
    .line 63
    .line 64
    sput-object v10, Lcom/evernote/edam/type/PrivilegeLevel;->ADMIN:Lcom/evernote/edam/type/PrivilegeLevel;

    .line 65
    .line 66
    const/4 v11, 0x6

    .line 67
    new-array v11, v11, [Lcom/evernote/edam/type/PrivilegeLevel;

    .line 68
    .line 69
    aput-object v0, v11, v2

    .line 70
    .line 71
    aput-object v1, v11, v3

    .line 72
    .line 73
    aput-object v4, v11, v7

    .line 74
    .line 75
    aput-object v6, v11, v5

    .line 76
    .line 77
    aput-object v9, v11, v12

    .line 78
    .line 79
    aput-object v10, v11, v8

    .line 80
    .line 81
    sput-object v11, Lcom/evernote/edam/type/PrivilegeLevel;->$VALUES:[Lcom/evernote/edam/type/PrivilegeLevel;

    .line 82
    .line 83
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput p3, p0, Lcom/evernote/edam/type/PrivilegeLevel;->value:I

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public static findByValue(I)Lcom/evernote/edam/type/PrivilegeLevel;
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    if-eq p0, v0, :cond_5

    .line 3
    .line 4
    const/4 v0, 0x3

    .line 5
    if-eq p0, v0, :cond_4

    .line 6
    .line 7
    const/4 v0, 0x5

    .line 8
    if-eq p0, v0, :cond_3

    .line 9
    .line 10
    const/4 v0, 0x7

    .line 11
    if-eq p0, v0, :cond_2

    .line 12
    .line 13
    const/16 v0, 0x8

    .line 14
    .line 15
    if-eq p0, v0, :cond_1

    .line 16
    .line 17
    const/16 v0, 0x9

    .line 18
    .line 19
    if-eq p0, v0, :cond_0

    .line 20
    .line 21
    const/4 p0, 0x0

    .line 22
    return-object p0

    .line 23
    :cond_0
    sget-object p0, Lcom/evernote/edam/type/PrivilegeLevel;->ADMIN:Lcom/evernote/edam/type/PrivilegeLevel;

    .line 24
    .line 25
    return-object p0

    .line 26
    :cond_1
    sget-object p0, Lcom/evernote/edam/type/PrivilegeLevel;->SUPPORT:Lcom/evernote/edam/type/PrivilegeLevel;

    .line 27
    .line 28
    return-object p0

    .line 29
    :cond_2
    sget-object p0, Lcom/evernote/edam/type/PrivilegeLevel;->MANAGER:Lcom/evernote/edam/type/PrivilegeLevel;

    .line 30
    .line 31
    return-object p0

    .line 32
    :cond_3
    sget-object p0, Lcom/evernote/edam/type/PrivilegeLevel;->VIP:Lcom/evernote/edam/type/PrivilegeLevel;

    .line 33
    .line 34
    return-object p0

    .line 35
    :cond_4
    sget-object p0, Lcom/evernote/edam/type/PrivilegeLevel;->PREMIUM:Lcom/evernote/edam/type/PrivilegeLevel;

    .line 36
    .line 37
    return-object p0

    .line 38
    :cond_5
    sget-object p0, Lcom/evernote/edam/type/PrivilegeLevel;->NORMAL:Lcom/evernote/edam/type/PrivilegeLevel;

    .line 39
    .line 40
    return-object p0
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/evernote/edam/type/PrivilegeLevel;
    .locals 1

    .line 1
    const-class v0, Lcom/evernote/edam/type/PrivilegeLevel;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/evernote/edam/type/PrivilegeLevel;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static values()[Lcom/evernote/edam/type/PrivilegeLevel;
    .locals 1

    .line 1
    sget-object v0, Lcom/evernote/edam/type/PrivilegeLevel;->$VALUES:[Lcom/evernote/edam/type/PrivilegeLevel;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/evernote/edam/type/PrivilegeLevel;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/evernote/edam/type/PrivilegeLevel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/evernote/edam/type/PrivilegeLevel;->value:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

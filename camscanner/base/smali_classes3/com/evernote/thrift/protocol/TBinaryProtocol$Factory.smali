.class public Lcom/evernote/thrift/protocol/TBinaryProtocol$Factory;
.super Ljava/lang/Object;
.source "TBinaryProtocol.java"

# interfaces
.implements Lcom/evernote/thrift/protocol/TProtocolFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/evernote/thrift/protocol/TBinaryProtocol;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# instance fields
.field protected readLength_:I

.field protected strictRead_:Z

.field protected strictWrite_:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1
    invoke-direct {p0, v0, v1}, Lcom/evernote/thrift/protocol/TBinaryProtocol$Factory;-><init>(ZZ)V

    return-void
.end method

.method public constructor <init>(ZZ)V
    .locals 1

    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, p1, p2, v0}, Lcom/evernote/thrift/protocol/TBinaryProtocol$Factory;-><init>(ZZI)V

    return-void
.end method

.method public constructor <init>(ZZI)V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-boolean p1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol$Factory;->strictRead_:Z

    .line 5
    iput-boolean p2, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol$Factory;->strictWrite_:Z

    .line 6
    iput p3, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol$Factory;->readLength_:I

    return-void
.end method


# virtual methods
.method public getProtocol(Lcom/evernote/thrift/transport/TTransport;)Lcom/evernote/thrift/protocol/TProtocol;
    .locals 3

    .line 1
    new-instance v0, Lcom/evernote/thrift/protocol/TBinaryProtocol;

    .line 2
    .line 3
    iget-boolean v1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol$Factory;->strictRead_:Z

    .line 4
    .line 5
    iget-boolean v2, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol$Factory;->strictWrite_:Z

    .line 6
    .line 7
    invoke-direct {v0, p1, v1, v2}, Lcom/evernote/thrift/protocol/TBinaryProtocol;-><init>(Lcom/evernote/thrift/transport/TTransport;ZZ)V

    .line 8
    .line 9
    .line 10
    iget p1, p0, Lcom/evernote/thrift/protocol/TBinaryProtocol$Factory;->readLength_:I

    .line 11
    .line 12
    if-eqz p1, :cond_0

    .line 13
    .line 14
    invoke-virtual {v0, p1}, Lcom/evernote/thrift/protocol/TBinaryProtocol;->setReadLength(I)V

    .line 15
    .line 16
    .line 17
    :cond_0
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

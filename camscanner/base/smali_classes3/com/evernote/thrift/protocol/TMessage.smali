.class public final Lcom/evernote/thrift/protocol/TMessage;
.super Ljava/lang/Object;
.source "TMessage.java"


# instance fields
.field public final name:Ljava/lang/String;

.field public final seqid:I

.field public final type:B


# direct methods
.method public constructor <init>()V
    .locals 2

    const-string v0, ""

    const/4 v1, 0x0

    .line 1
    invoke-direct {p0, v0, v1, v1}, Lcom/evernote/thrift/protocol/TMessage;-><init>(Ljava/lang/String;BI)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;BI)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/evernote/thrift/protocol/TMessage;->name:Ljava/lang/String;

    .line 4
    iput-byte p2, p0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    .line 5
    iput p3, p0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    return-void
.end method


# virtual methods
.method public equals(Lcom/evernote/thrift/protocol/TMessage;)Z
    .locals 2

    .line 3
    iget-object v0, p0, Lcom/evernote/thrift/protocol/TMessage;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/thrift/protocol/TMessage;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-byte v0, p0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    iget-byte v1, p1, Lcom/evernote/thrift/protocol/TMessage;->type:B

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    iget p1, p1, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/evernote/thrift/protocol/TMessage;

    if-eqz v0, :cond_0

    .line 2
    check-cast p1, Lcom/evernote/thrift/protocol/TMessage;

    invoke-virtual {p0, p1}, Lcom/evernote/thrift/protocol/TMessage;->equals(Lcom/evernote/thrift/protocol/TMessage;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "<TMessage name:\'"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/evernote/thrift/protocol/TMessage;->name:Ljava/lang/String;

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v1, "\' type: "

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    iget-byte v1, p0, Lcom/evernote/thrift/protocol/TMessage;->type:B

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    const-string v1, " seqid:"

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    iget v1, p0, Lcom/evernote/thrift/protocol/TMessage;->seqid:I

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    const-string v1, ">"

    .line 37
    .line 38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    return-object v0
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

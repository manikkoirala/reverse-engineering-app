.class Lcom/evernote/client/android/login/EvernoteLoginTask;
.super Lnet/vrallev/android/task/Task;
.source "EvernoteLoginTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lnet/vrallev/android/task/Task<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final CAT:Lcom/evernote/client/android/helper/Cat;

.field public static final REQUEST_AUTH:I = 0x35a

.field public static final REQUEST_PROFILE_NAME:I = 0x35b


# instance fields
.field private mBootstrapCountDownLatch:Ljava/util/concurrent/CountDownLatch;

.field private mBootstrapIndex:I

.field private mBootstrapProfile:Lcom/evernote/edam/userstore/BootstrapProfile;

.field private mBootstrapProfiles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/evernote/edam/userstore/BootstrapProfile;",
            ">;"
        }
    .end annotation
.end field

.field private mData:Landroid/content/Intent;

.field private final mIsFragment:Z

.field private final mOAuthHelper:Lcom/evernote/client/android/EvernoteOAuthHelper;

.field private mResultCode:I

.field private mResultCountDownLatch:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/evernote/client/android/helper/Cat;

    .line 2
    .line 3
    const-string v1, "EvernoteLoginTask"

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/evernote/client/android/helper/Cat;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    sput-object v0, Lcom/evernote/client/android/login/EvernoteLoginTask;->CAT:Lcom/evernote/client/android/helper/Cat;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public constructor <init>(Lcom/evernote/client/android/EvernoteOAuthHelper;Z)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lnet/vrallev/android/task/Task;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mOAuthHelper:Lcom/evernote/client/android/EvernoteOAuthHelper;

    .line 5
    .line 6
    iput-boolean p2, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mIsFragment:Z

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private canContinue()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lnet/vrallev/android/task/Task;->isCancelled()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Lnet/vrallev/android/task/Task;->getActivity()Landroid/app/Activity;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private finishAuthorization()Z
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->canContinue()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mOAuthHelper:Lcom/evernote/client/android/EvernoteOAuthHelper;

    .line 8
    .line 9
    invoke-virtual {p0}, Lnet/vrallev/android/task/Task;->getActivity()Landroid/app/Activity;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    iget v2, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mResultCode:I

    .line 14
    .line 15
    iget-object v3, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mData:Landroid/content/Intent;

    .line 16
    .line 17
    invoke-virtual {v0, v1, v2, v3}, Lcom/evernote/client/android/EvernoteOAuthHelper;->finishAuthorization(Landroid/app/Activity;ILandroid/content/Intent;)Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-eqz v0, :cond_0

    .line 22
    .line 23
    const/4 v0, 0x1

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 v0, 0x0

    .line 26
    :goto_0
    return v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private getBootstrapProfileNameFromMainApp()Ljava/lang/String;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lnet/vrallev/android/task/Task;->getActivity()Landroid/app/Activity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    return-object v1

    .line 9
    :cond_0
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->getLoginTaskCallback()Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    if-nez v2, :cond_1

    .line 14
    .line 15
    return-object v1

    .line 16
    :cond_1
    invoke-static {}, Lcom/evernote/client/android/EvernoteSession;->getInstance()Lcom/evernote/client/android/EvernoteSession;

    .line 17
    .line 18
    .line 19
    move-result-object v3

    .line 20
    invoke-static {v0, v3}, Lcom/evernote/client/android/EvernoteUtil;->createGetBootstrapProfileNameIntent(Landroid/content/Context;Lcom/evernote/client/android/EvernoteSession;)Landroid/content/Intent;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    if-nez v0, :cond_2

    .line 25
    .line 26
    return-object v1

    .line 27
    :cond_2
    const/16 v3, 0x35b

    .line 28
    .line 29
    invoke-interface {v2, v0, v3}, Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;->startActivityForResult(Landroid/content/Intent;I)V

    .line 30
    .line 31
    .line 32
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    .line 33
    .line 34
    const/4 v2, 0x1

    .line 35
    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 36
    .line 37
    .line 38
    iput-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mResultCountDownLatch:Ljava/util/concurrent/CountDownLatch;

    .line 39
    .line 40
    :try_start_0
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 41
    .line 42
    const-wide/16 v3, 0x3

    .line 43
    .line 44
    invoke-virtual {v0, v3, v4, v2}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mData:Landroid/content/Intent;

    .line 48
    .line 49
    if-nez v0, :cond_3

    .line 50
    .line 51
    return-object v1

    .line 52
    :cond_3
    const-string v1, "bootstrap_profile_name"

    .line 53
    .line 54
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    return-object v0

    .line 59
    :catch_0
    return-object v1
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private getLoginTaskCallback()Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mIsFragment:Z

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    invoke-virtual {p0}, Lnet/vrallev/android/task/Task;->getFragment()Landroidx/fragment/app/Fragment;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    instance-of v2, v0, Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;

    .line 11
    .line 12
    if-eqz v2, :cond_0

    .line 13
    .line 14
    check-cast v0, Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;

    .line 15
    .line 16
    return-object v0

    .line 17
    :cond_0
    return-object v1

    .line 18
    :cond_1
    invoke-virtual {p0}, Lnet/vrallev/android/task/Task;->getActivity()Landroid/app/Activity;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    instance-of v2, v0, Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;

    .line 23
    .line 24
    if-eqz v2, :cond_2

    .line 25
    .line 26
    check-cast v0, Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;

    .line 27
    .line 28
    return-object v0

    .line 29
    :cond_2
    return-object v1
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private getNextBootstrapProfile()Lcom/evernote/edam/userstore/BootstrapProfile;
    .locals 2

    .line 1
    iget v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapIndex:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x1

    .line 4
    .line 5
    iget-object v1, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfiles:Ljava/util/List;

    .line 6
    .line 7
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    rem-int/2addr v0, v1

    .line 12
    iget-object v1, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfiles:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    check-cast v0, Lcom/evernote/edam/userstore/BootstrapProfile;

    .line 19
    .line 20
    return-object v0
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private getScreenName(Lcom/evernote/edam/userstore/BootstrapProfile;)Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapProfile;->getName()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "Evernote-China"

    .line 6
    .line 7
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    const-string/jumbo p1, "\u5370\u8c61\u7b14\u8bb0"

    .line 14
    .line 15
    .line 16
    return-object p1

    .line 17
    :cond_0
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapProfile;->getSettings()Lcom/evernote/edam/userstore/BootstrapSettings;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-virtual {v0}, Lcom/evernote/edam/userstore/BootstrapSettings;->getServiceHost()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const-string v1, "https://www.evernote.com"

    .line 26
    .line 27
    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-eqz v0, :cond_1

    .line 32
    .line 33
    const-string p1, "Evernote International"

    .line 34
    .line 35
    return-object p1

    .line 36
    :cond_1
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapProfile;->getName()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    return-object p1
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private showBootstrapOption()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->getLoginTaskCallback()Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->getNextBootstrapProfile()Lcom/evernote/edam/userstore/BootstrapProfile;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    invoke-direct {p0, v1}, Lcom/evernote/client/android/login/EvernoteLoginTask;->getScreenName(Lcom/evernote/edam/userstore/BootstrapProfile;)Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-interface {v0, v1}, Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;->show(Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    .line 20
    .line 21
    const/4 v1, 0x1

    .line 22
    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 23
    .line 24
    .line 25
    iput-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapCountDownLatch:Ljava/util/concurrent/CountDownLatch;

    .line 26
    .line 27
    :try_start_0
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 28
    .line 29
    const-wide/16 v2, 0x3

    .line 30
    .line 31
    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    if-eqz v0, :cond_1

    .line 36
    .line 37
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->showBootstrapOption()V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_1
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->getLoginTaskCallback()Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    if-eqz v0, :cond_2

    .line 46
    .line 47
    const/4 v1, 0x0

    .line 48
    invoke-interface {v0, v1}, Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;->show(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    .line 50
    .line 51
    goto :goto_0

    .line 52
    :catch_0
    move-exception v0

    .line 53
    sget-object v1, Lcom/evernote/client/android/login/EvernoteLoginTask;->CAT:Lcom/evernote/client/android/helper/Cat;

    .line 54
    .line 55
    invoke-virtual {v1, v0}, Lcom/evernote/client/android/helper/Cat;->e(Ljava/lang/Throwable;)V

    .line 56
    .line 57
    .line 58
    :cond_2
    :goto_0
    return-void
.end method

.method private startAuthorization()Z
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->canContinue()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    const/4 v0, 0x1

    .line 10
    :try_start_0
    iget-object v2, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mOAuthHelper:Lcom/evernote/client/android/EvernoteOAuthHelper;

    .line 11
    .line 12
    invoke-virtual {v2}, Lcom/evernote/client/android/EvernoteOAuthHelper;->fetchBootstrapProfiles()Ljava/util/List;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    iput-object v2, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfiles:Ljava/util/List;

    .line 17
    .line 18
    iget-object v3, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mOAuthHelper:Lcom/evernote/client/android/EvernoteOAuthHelper;

    .line 19
    .line 20
    invoke-virtual {v3, v2}, Lcom/evernote/client/android/EvernoteOAuthHelper;->getDefaultBootstrapProfile(Ljava/util/List;)Lcom/evernote/edam/userstore/BootstrapProfile;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    iput-object v2, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfile:Lcom/evernote/edam/userstore/BootstrapProfile;

    .line 25
    .line 26
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->canContinue()Z

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    if-nez v2, :cond_1

    .line 31
    .line 32
    return v1

    .line 33
    :cond_1
    iget-object v2, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfiles:Ljava/util/List;

    .line 34
    .line 35
    if-eqz v2, :cond_7

    .line 36
    .line 37
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    if-le v2, v0, :cond_7

    .line 42
    .line 43
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->getBootstrapProfileNameFromMainApp()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v2

    .line 47
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->canContinue()Z

    .line 48
    .line 49
    .line 50
    move-result v3

    .line 51
    if-nez v3, :cond_2

    .line 52
    .line 53
    return v1

    .line 54
    :cond_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 55
    .line 56
    .line 57
    move-result v3

    .line 58
    if-nez v3, :cond_4

    .line 59
    .line 60
    iget-object v3, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfiles:Ljava/util/List;

    .line 61
    .line 62
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 63
    .line 64
    .line 65
    move-result-object v3

    .line 66
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 67
    .line 68
    .line 69
    move-result v4

    .line 70
    if-eqz v4, :cond_4

    .line 71
    .line 72
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 73
    .line 74
    .line 75
    move-result-object v4

    .line 76
    check-cast v4, Lcom/evernote/edam/userstore/BootstrapProfile;

    .line 77
    .line 78
    invoke-virtual {v4}, Lcom/evernote/edam/userstore/BootstrapProfile;->getName()Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object v5

    .line 82
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 83
    .line 84
    .line 85
    move-result v5

    .line 86
    if-eqz v5, :cond_3

    .line 87
    .line 88
    iput-object v4, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfile:Lcom/evernote/edam/userstore/BootstrapProfile;

    .line 89
    .line 90
    const/4 v2, 0x0

    .line 91
    goto :goto_0

    .line 92
    :cond_4
    const/4 v2, 0x1

    .line 93
    :goto_0
    if-eqz v2, :cond_7

    .line 94
    .line 95
    const/4 v2, 0x0

    .line 96
    :goto_1
    iget-object v3, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfiles:Ljava/util/List;

    .line 97
    .line 98
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 99
    .line 100
    .line 101
    move-result v3

    .line 102
    if-ge v2, v3, :cond_6

    .line 103
    .line 104
    iget-object v3, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfile:Lcom/evernote/edam/userstore/BootstrapProfile;

    .line 105
    .line 106
    iget-object v4, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfiles:Ljava/util/List;

    .line 107
    .line 108
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 109
    .line 110
    .line 111
    move-result-object v4

    .line 112
    check-cast v4, Lcom/evernote/edam/userstore/BootstrapProfile;

    .line 113
    .line 114
    invoke-virtual {v3, v4}, Lcom/evernote/edam/userstore/BootstrapProfile;->equals(Lcom/evernote/edam/userstore/BootstrapProfile;)Z

    .line 115
    .line 116
    .line 117
    move-result v3

    .line 118
    if-eqz v3, :cond_5

    .line 119
    .line 120
    iput v2, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapIndex:I

    .line 121
    .line 122
    goto :goto_2

    .line 123
    :cond_5
    add-int/lit8 v2, v2, 0x1

    .line 124
    .line 125
    goto :goto_1

    .line 126
    :cond_6
    :goto_2
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->showBootstrapOption()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    .line 128
    .line 129
    goto :goto_3

    .line 130
    :catch_0
    move-exception v2

    .line 131
    sget-object v3, Lcom/evernote/client/android/login/EvernoteLoginTask;->CAT:Lcom/evernote/client/android/helper/Cat;

    .line 132
    .line 133
    invoke-virtual {v3, v2}, Lcom/evernote/client/android/helper/Cat;->e(Ljava/lang/Throwable;)V

    .line 134
    .line 135
    .line 136
    :cond_7
    :goto_3
    iget-object v2, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfile:Lcom/evernote/edam/userstore/BootstrapProfile;

    .line 137
    .line 138
    if-eqz v2, :cond_8

    .line 139
    .line 140
    iget-object v3, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mOAuthHelper:Lcom/evernote/client/android/EvernoteOAuthHelper;

    .line 141
    .line 142
    invoke-virtual {v3, v2}, Lcom/evernote/client/android/EvernoteOAuthHelper;->setBootstrapProfile(Lcom/evernote/edam/userstore/BootstrapProfile;)V

    .line 143
    .line 144
    .line 145
    :cond_8
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->canContinue()Z

    .line 146
    .line 147
    .line 148
    move-result v2

    .line 149
    if-nez v2, :cond_9

    .line 150
    .line 151
    return v1

    .line 152
    :cond_9
    iget-object v2, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mOAuthHelper:Lcom/evernote/client/android/EvernoteOAuthHelper;

    .line 153
    .line 154
    invoke-virtual {p0}, Lnet/vrallev/android/task/Task;->getActivity()Landroid/app/Activity;

    .line 155
    .line 156
    .line 157
    move-result-object v3

    .line 158
    invoke-virtual {v2, v3}, Lcom/evernote/client/android/EvernoteOAuthHelper;->startAuthorization(Landroid/app/Activity;)Landroid/content/Intent;

    .line 159
    .line 160
    .line 161
    move-result-object v2

    .line 162
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->canContinue()Z

    .line 163
    .line 164
    .line 165
    move-result v3

    .line 166
    if-eqz v3, :cond_b

    .line 167
    .line 168
    if-nez v2, :cond_a

    .line 169
    .line 170
    goto :goto_4

    .line 171
    :cond_a
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->getLoginTaskCallback()Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;

    .line 172
    .line 173
    .line 174
    move-result-object v3

    .line 175
    if-eqz v3, :cond_b

    .line 176
    .line 177
    const/16 v1, 0x35a

    .line 178
    .line 179
    invoke-interface {v3, v2, v1}, Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;->startActivityForResult(Landroid/content/Intent;I)V

    .line 180
    .line 181
    .line 182
    return v0

    .line 183
    :cond_b
    :goto_4
    return v1
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method


# virtual methods
.method public execute()Ljava/lang/Boolean;
    .locals 2

    .line 2
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->startAuthorization()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object v0

    .line 4
    :cond_0
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->canContinue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 5
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object v0

    .line 6
    :cond_1
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mResultCountDownLatch:Ljava/util/concurrent/CountDownLatch;

    .line 7
    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 8
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->finishAuthorization()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 9
    :catch_0
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object v0
.end method

.method public bridge synthetic execute()Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->execute()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public onActivityResult(ILandroid/content/Intent;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mResultCountDownLatch:Ljava/util/concurrent/CountDownLatch;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 6
    .line 7
    .line 8
    :cond_0
    iput p1, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mResultCode:I

    .line 9
    .line 10
    iput-object p2, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mData:Landroid/content/Intent;

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public switchBootstrapProfile()V
    .locals 2

    .line 1
    iget v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapIndex:I

    .line 2
    .line 3
    add-int/lit8 v0, v0, 0x1

    .line 4
    .line 5
    iget-object v1, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfiles:Ljava/util/List;

    .line 6
    .line 7
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    rem-int/2addr v0, v1

    .line 12
    iput v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapIndex:I

    .line 13
    .line 14
    iget-object v1, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfiles:Ljava/util/List;

    .line 15
    .line 16
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    check-cast v0, Lcom/evernote/edam/userstore/BootstrapProfile;

    .line 21
    .line 22
    iput-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfile:Lcom/evernote/edam/userstore/BootstrapProfile;

    .line 23
    .line 24
    iget-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapCountDownLatch:Ljava/util/concurrent/CountDownLatch;

    .line 25
    .line 26
    if-eqz v0, :cond_0

    .line 27
    .line 28
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 29
    .line 30
    .line 31
    :cond_0
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

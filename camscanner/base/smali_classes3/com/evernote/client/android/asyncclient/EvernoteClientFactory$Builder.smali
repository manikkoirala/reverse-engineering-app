.class public Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;
.super Ljava/lang/Object;
.source "EvernoteClientFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mByteStoreFactory:Lcom/evernote/client/conn/mobile/ByteStore$Factory;

.field private final mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

.field private mExecutorService:Ljava/util/concurrent/ExecutorService;

.field private final mHeaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHttpClient:Lcom/squareup/okhttp/OkHttpClient;


# direct methods
.method public constructor <init>(Lcom/evernote/client/android/EvernoteSession;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    invoke-static {p1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    check-cast p1, Lcom/evernote/client/android/EvernoteSession;

    .line 9
    .line 10
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    .line 11
    .line 12
    new-instance p1, Ljava/util/HashMap;

    .line 13
    .line 14
    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    .line 15
    .line 16
    .line 17
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->mHeaders:Ljava/util/Map;

    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
.end method

.method private addHeader(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->mHeaders:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private createDefaultByteStore(Landroid/content/Context;)Lcom/evernote/client/conn/mobile/ByteStore$Factory;
    .locals 4

    .line 1
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    .line 6
    .line 7
    .line 8
    move-result-wide v0

    .line 9
    const-wide/16 v2, 0x20

    .line 10
    .line 11
    div-long/2addr v0, v2

    .line 12
    long-to-int v1, v0

    .line 13
    new-instance v0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore$Factory;

    .line 14
    .line 15
    new-instance v2, Ljava/io/File;

    .line 16
    .line 17
    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    const-string v3, "evernoteCache"

    .line 22
    .line 23
    invoke-direct {v2, p1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    invoke-direct {v0, v2, v1}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore$Factory;-><init>(Ljava/io/File;I)V

    .line 27
    .line 28
    .line 29
    return-object v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private createDefaultHttpClient()Lcom/squareup/okhttp/OkHttpClient;
    .locals 5

    .line 1
    new-instance v0, Lcom/squareup/okhttp/OkHttpClient;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/squareup/okhttp/OkHttpClient;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 7
    .line 8
    const-wide/16 v2, 0xa

    .line 9
    .line 10
    invoke-virtual {v0, v2, v3, v1}, Lcom/squareup/okhttp/OkHttpClient;->o〇〇0〇(JLjava/util/concurrent/TimeUnit;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, v2, v3, v1}, Lcom/squareup/okhttp/OkHttpClient;->oo〇(JLjava/util/concurrent/TimeUnit;)V

    .line 14
    .line 15
    .line 16
    const-wide/16 v2, 0x14

    .line 17
    .line 18
    invoke-virtual {v0, v2, v3, v1}, Lcom/squareup/okhttp/OkHttpClient;->〇00〇8(JLjava/util/concurrent/TimeUnit;)V

    .line 19
    .line 20
    .line 21
    new-instance v1, Lcom/squareup/okhttp/ConnectionPool;

    .line 22
    .line 23
    const/16 v2, 0x14

    .line 24
    .line 25
    const-wide/32 v3, 0x1d4c0

    .line 26
    .line 27
    .line 28
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/okhttp/ConnectionPool;-><init>(IJ)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/OkHttpClient;->OOO〇O0(Lcom/squareup/okhttp/ConnectionPool;)Lcom/squareup/okhttp/OkHttpClient;

    .line 32
    .line 33
    .line 34
    return-object v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method


# virtual methods
.method public build()Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->mHttpClient:Lcom/squareup/okhttp/OkHttpClient;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->createDefaultHttpClient()Lcom/squareup/okhttp/OkHttpClient;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iput-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->mHttpClient:Lcom/squareup/okhttp/OkHttpClient;

    .line 10
    .line 11
    :cond_0
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->mByteStoreFactory:Lcom/evernote/client/conn/mobile/ByteStore$Factory;

    .line 12
    .line 13
    if-nez v0, :cond_1

    .line 14
    .line 15
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/evernote/client/android/EvernoteSession;->getApplicationContext()Landroid/content/Context;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-direct {p0, v0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->createDefaultByteStore(Landroid/content/Context;)Lcom/evernote/client/conn/mobile/ByteStore$Factory;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    iput-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->mByteStoreFactory:Lcom/evernote/client/conn/mobile/ByteStore$Factory;

    .line 26
    .line 27
    :cond_1
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 28
    .line 29
    if-nez v0, :cond_2

    .line 30
    .line 31
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    iput-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 36
    .line 37
    :cond_2
    const-string v0, "Cache-Control"

    .line 38
    .line 39
    const-string v1, "no-transform"

    .line 40
    .line 41
    invoke-direct {p0, v0, v1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;

    .line 42
    .line 43
    .line 44
    const-string v0, "Accept"

    .line 45
    .line 46
    const-string v1, "application/x-thrift"

    .line 47
    .line 48
    invoke-direct {p0, v0, v1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;

    .line 49
    .line 50
    .line 51
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    .line 52
    .line 53
    invoke-virtual {v0}, Lcom/evernote/client/android/EvernoteSession;->getApplicationContext()Landroid/content/Context;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    invoke-static {v0}, Lcom/evernote/client/android/EvernoteUtil;->generateUserAgentString(Landroid/content/Context;)Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    const-string v1, "User-Agent"

    .line 62
    .line 63
    invoke-direct {p0, v1, v0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;

    .line 64
    .line 65
    .line 66
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    .line 67
    .line 68
    iget-object v3, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    .line 69
    .line 70
    iget-object v4, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->mHttpClient:Lcom/squareup/okhttp/OkHttpClient;

    .line 71
    .line 72
    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->mByteStoreFactory:Lcom/evernote/client/conn/mobile/ByteStore$Factory;

    .line 73
    .line 74
    invoke-interface {v1}, Lcom/evernote/client/conn/mobile/ByteStore$Factory;->create()Lcom/evernote/client/conn/mobile/ByteStore;

    .line 75
    .line 76
    .line 77
    move-result-object v5

    .line 78
    iget-object v6, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->mHeaders:Ljava/util/Map;

    .line 79
    .line 80
    iget-object v7, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 81
    .line 82
    move-object v2, v0

    .line 83
    invoke-direct/range {v2 .. v7}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;-><init>(Lcom/evernote/client/android/EvernoteSession;Lcom/squareup/okhttp/OkHttpClient;Lcom/evernote/client/conn/mobile/ByteStore;Ljava/util/Map;Ljava/util/concurrent/ExecutorService;)V

    .line 84
    .line 85
    .line 86
    return-object v0
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public setByteStoreFactory(Lcom/evernote/client/conn/mobile/ByteStore$Factory;)Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->mByteStoreFactory:Lcom/evernote/client/conn/mobile/ByteStore$Factory;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setExecutorService(Ljava/util/concurrent/ExecutorService;)Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setHttpClient(Lcom/squareup/okhttp/OkHttpClient;)Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->mHttpClient:Lcom/squareup/okhttp/OkHttpClient;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

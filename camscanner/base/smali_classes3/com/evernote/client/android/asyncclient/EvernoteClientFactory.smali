.class public Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;
.super Ljava/lang/Object;
.source "EvernoteClientFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;
    }
.end annotation


# instance fields
.field private mBusinessAuthenticationResult:Lcom/evernote/edam/userstore/AuthenticationResult;

.field private mBusinessNotebookHelper:Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;

.field protected final mByteStore:Lcom/evernote/client/conn/mobile/ByteStore;

.field private final mCreateHelperClient:Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;

.field private mEvernoteSearchHelper:Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;

.field protected final mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

.field protected final mExecutorService:Ljava/util/concurrent/ExecutorService;

.field protected final mHeaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHtmlHelperBusiness:Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

.field private mHtmlHelperDefault:Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

.field protected final mHttpClient:Lcom/squareup/okhttp/OkHttpClient;

.field private final mLinkedHtmlHelper:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final mLinkedNotebookHelpers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final mNoteStoreClients:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;",
            ">;"
        }
    .end annotation
.end field

.field private final mUserStoreClients:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/evernote/client/android/EvernoteSession;Lcom/squareup/okhttp/OkHttpClient;Lcom/evernote/client/conn/mobile/ByteStore;Ljava/util/Map;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/EvernoteSession;",
            "Lcom/squareup/okhttp/OkHttpClient;",
            "Lcom/evernote/client/conn/mobile/ByteStore;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    invoke-static {p1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    check-cast p1, Lcom/evernote/client/android/EvernoteSession;

    .line 9
    .line 10
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    .line 11
    .line 12
    invoke-static {p2}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    check-cast p1, Lcom/squareup/okhttp/OkHttpClient;

    .line 17
    .line 18
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mHttpClient:Lcom/squareup/okhttp/OkHttpClient;

    .line 19
    .line 20
    invoke-static {p3}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    check-cast p1, Lcom/evernote/client/conn/mobile/ByteStore;

    .line 25
    .line 26
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mByteStore:Lcom/evernote/client/conn/mobile/ByteStore;

    .line 27
    .line 28
    iput-object p4, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mHeaders:Ljava/util/Map;

    .line 29
    .line 30
    invoke-static {p5}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    check-cast p1, Ljava/util/concurrent/ExecutorService;

    .line 35
    .line 36
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 37
    .line 38
    new-instance p2, Ljava/util/HashMap;

    .line 39
    .line 40
    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    .line 41
    .line 42
    .line 43
    iput-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mUserStoreClients:Ljava/util/Map;

    .line 44
    .line 45
    new-instance p2, Ljava/util/HashMap;

    .line 46
    .line 47
    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    .line 48
    .line 49
    .line 50
    iput-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mNoteStoreClients:Ljava/util/Map;

    .line 51
    .line 52
    new-instance p2, Ljava/util/HashMap;

    .line 53
    .line 54
    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    .line 55
    .line 56
    .line 57
    iput-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mLinkedNotebookHelpers:Ljava/util/Map;

    .line 58
    .line 59
    new-instance p2, Ljava/util/HashMap;

    .line 60
    .line 61
    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    .line 62
    .line 63
    .line 64
    iput-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mLinkedHtmlHelper:Ljava/util/Map;

    .line 65
    .line 66
    new-instance p2, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$1;

    .line 67
    .line 68
    invoke-direct {p2, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$1;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;Ljava/util/concurrent/ExecutorService;)V

    .line 69
    .line 70
    .line 71
    iput-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mCreateHelperClient:Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;

    .line 72
    .line 73
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
.end method


# virtual methods
.method protected final authenticateToBusiness()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->isBusinessAuthExpired()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getUserStoreClient()Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;->authenticateToBusiness()Lcom/evernote/edam/userstore/AuthenticationResult;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iput-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mBusinessAuthenticationResult:Lcom/evernote/edam/userstore/AuthenticationResult;

    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method protected checkLoggedIn()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/evernote/client/android/EvernoteSession;->isLoggedIn()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 11
    .line 12
    const-string/jumbo v1, "user not logged in"

    .line 13
    .line 14
    .line 15
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    throw v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method protected createBinaryProtocol(Ljava/lang/String;)Lcom/evernote/thrift/protocol/TBinaryProtocol;
    .locals 5

    .line 1
    new-instance v0, Lcom/evernote/thrift/protocol/TBinaryProtocol;

    .line 2
    .line 3
    new-instance v1, Lcom/evernote/client/conn/mobile/TAndroidTransport;

    .line 4
    .line 5
    iget-object v2, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mHttpClient:Lcom/squareup/okhttp/OkHttpClient;

    .line 6
    .line 7
    iget-object v3, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mByteStore:Lcom/evernote/client/conn/mobile/ByteStore;

    .line 8
    .line 9
    iget-object v4, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mHeaders:Ljava/util/Map;

    .line 10
    .line 11
    invoke-direct {v1, v2, v3, p1, v4}, Lcom/evernote/client/conn/mobile/TAndroidTransport;-><init>(Lcom/squareup/okhttp/OkHttpClient;Lcom/evernote/client/conn/mobile/ByteStore;Ljava/lang/String;Ljava/util/Map;)V

    .line 12
    .line 13
    .line 14
    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TBinaryProtocol;-><init>(Lcom/evernote/thrift/transport/TTransport;)V

    .line 15
    .line 16
    .line 17
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method protected createBusinessNotebookHelper()Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->authenticateToBusiness()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mBusinessAuthenticationResult:Lcom/evernote/edam/userstore/AuthenticationResult;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/evernote/edam/userstore/AuthenticationResult;->getNoteStoreUrl()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mBusinessAuthenticationResult:Lcom/evernote/edam/userstore/AuthenticationResult;

    .line 11
    .line 12
    invoke-virtual {v1}, Lcom/evernote/edam/userstore/AuthenticationResult;->getAuthenticationToken()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-virtual {p0, v0, v1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getNoteStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mBusinessAuthenticationResult:Lcom/evernote/edam/userstore/AuthenticationResult;

    .line 21
    .line 22
    invoke-virtual {v1}, Lcom/evernote/edam/userstore/AuthenticationResult;->getUser()Lcom/evernote/edam/type/User;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    new-instance v2, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;

    .line 27
    .line 28
    iget-object v3, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 29
    .line 30
    invoke-virtual {v1}, Lcom/evernote/edam/type/User;->getUsername()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v4

    .line 34
    invoke-virtual {v1}, Lcom/evernote/edam/type/User;->getShardId()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-direct {v2, v0, v3, v4, v1}, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    return-object v2
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method protected declared-synchronized createEvernoteNoteStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;
    .locals 2

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    .line 3
    .line 4
    invoke-virtual {p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createNoteStoreClient(Ljava/lang/String;)Lcom/evernote/edam/notestore/NoteStore$Client;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 9
    .line 10
    invoke-direct {v0, p1, p2, v1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;-><init>(Lcom/evernote/edam/notestore/NoteStore$Client;Ljava/lang/String;Ljava/util/concurrent/ExecutorService;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 11
    .line 12
    .line 13
    monitor-exit p0

    .line 14
    return-object v0

    .line 15
    :catchall_0
    move-exception p1

    .line 16
    monitor-exit p0

    .line 17
    throw p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method protected createEvernoteSearchHelper()Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;
    .locals 3

    .line 1
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    .line 4
    .line 5
    iget-object v2, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 6
    .line 7
    invoke-direct {v0, v1, v2}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;-><init>(Lcom/evernote/client/android/EvernoteSession;Ljava/util/concurrent/ExecutorService;)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method protected createHtmlHelper(Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;
    .locals 4

    .line 1
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mHttpClient:Lcom/squareup/okhttp/OkHttpClient;

    .line 4
    .line 5
    iget-object v2, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    .line 6
    .line 7
    invoke-virtual {v2}, Lcom/evernote/client/android/EvernoteSession;->getAuthenticationResult()Lcom/evernote/client/android/AuthenticationResult;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    invoke-virtual {v2}, Lcom/evernote/client/android/AuthenticationResult;->getEvernoteHost()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    iget-object v3, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 16
    .line 17
    invoke-direct {v0, v1, v2, p1, v3}, Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;-><init>(Lcom/squareup/okhttp/OkHttpClient;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/ExecutorService;)V

    .line 18
    .line 19
    .line 20
    return-object v0
    .line 21
    .line 22
.end method

.method protected final createKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 1
    if-nez p1, :cond_1

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 7
    .line 8
    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    .line 9
    .line 10
    .line 11
    throw p1

    .line 12
    :cond_1
    :goto_0
    if-nez p1, :cond_2

    .line 13
    .line 14
    return-object p2

    .line 15
    :cond_2
    if-nez p2, :cond_3

    .line 16
    .line 17
    return-object p1

    .line 18
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    .line 19
    .line 20
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    return-object p1
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method protected createLinkedNotebookHelper(Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;
    .locals 3
    .param p1    # Lcom/evernote/edam/type/LinkedNotebook;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Lcom/evernote/edam/type/LinkedNotebook;->getNoteStoreUrl()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/evernote/client/android/EvernoteSession;->getAuthToken()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-static {v1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotEmpty(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    check-cast v1, Ljava/lang/String;

    .line 16
    .line 17
    invoke-virtual {p0, v0, v1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getNoteStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-virtual {p1}, Lcom/evernote/edam/type/LinkedNotebook;->getShareKey()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    invoke-virtual {v1, v2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->authenticateToSharedNotebook(Ljava/lang/String;)Lcom/evernote/edam/userstore/AuthenticationResult;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-virtual {v1}, Lcom/evernote/edam/userstore/AuthenticationResult;->getAuthenticationToken()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    invoke-virtual {p0, v0, v1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getNoteStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    new-instance v1, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;

    .line 38
    .line 39
    iget-object v2, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 40
    .line 41
    invoke-direct {v1, v0, p1, v2}, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/type/LinkedNotebook;Ljava/util/concurrent/ExecutorService;)V

    .line 42
    .line 43
    .line 44
    return-object v1
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method protected createNoteStoreClient(Ljava/lang/String;)Lcom/evernote/edam/notestore/NoteStore$Client;
    .locals 1

    .line 1
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$Client;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createBinaryProtocol(Ljava/lang/String;)Lcom/evernote/thrift/protocol/TBinaryProtocol;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-direct {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;-><init>(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method protected createUserStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;
    .locals 2

    .line 1
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$Client;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createBinaryProtocol(Ljava/lang/String;)Lcom/evernote/thrift/protocol/TBinaryProtocol;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-direct {v0, p1}, Lcom/evernote/edam/userstore/UserStore$Client;-><init>(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 8
    .line 9
    .line 10
    new-instance p1, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;

    .line 11
    .line 12
    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 13
    .line 14
    invoke-direct {p1, v0, p2, v1}, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;-><init>(Lcom/evernote/edam/userstore/UserStore$Client;Ljava/lang/String;Ljava/util/concurrent/ExecutorService;)V

    .line 15
    .line 16
    .line 17
    return-object p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public declared-synchronized getBusinessNotebookHelper()Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;
        }
    .end annotation

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mBusinessNotebookHelper:Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;

    .line 3
    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->isBusinessAuthExpired()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createBusinessNotebookHelper()Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    iput-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mBusinessNotebookHelper:Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;

    .line 17
    .line 18
    :cond_1
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mBusinessNotebookHelper:Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 19
    .line 20
    monitor-exit p0

    .line 21
    return-object v0

    .line 22
    :catchall_0
    move-exception v0

    .line 23
    monitor-exit p0

    .line 24
    throw v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public getBusinessNotebookHelperAsync(Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 2
    .param p1    # Lcom/evernote/client/android/asyncclient/EvernoteCallback;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mCreateHelperClient:Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;

    .line 2
    .line 3
    new-instance v1, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$3;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$3;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1, p1}, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    return-object p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public getEvernoteSearchHelper()Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->checkLoggedIn()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSearchHelper:Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;

    .line 5
    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createEvernoteSearchHelper()Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iput-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSearchHelper:Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;

    .line 13
    .line 14
    :cond_0
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSearchHelper:Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;

    .line 15
    .line 16
    return-object v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public declared-synchronized getHtmlHelperBusiness()Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;
        }
    .end annotation

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mHtmlHelperBusiness:Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

    .line 3
    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->authenticateToBusiness()V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mBusinessAuthenticationResult:Lcom/evernote/edam/userstore/AuthenticationResult;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/evernote/edam/userstore/AuthenticationResult;->getAuthenticationToken()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {p0, v0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createHtmlHelper(Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    iput-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mHtmlHelperBusiness:Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

    .line 20
    .line 21
    :cond_0
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mHtmlHelperBusiness:Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    .line 23
    monitor-exit p0

    .line 24
    return-object v0

    .line 25
    :catchall_0
    move-exception v0

    .line 26
    monitor-exit p0

    .line 27
    throw v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public getHtmlHelperBusinessAsync(Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 2
    .param p1    # Lcom/evernote/client/android/asyncclient/EvernoteCallback;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mCreateHelperClient:Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;

    .line 2
    .line 3
    new-instance v1, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$5;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$5;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1, p1}, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    return-object p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public declared-synchronized getHtmlHelperDefault()Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;
    .locals 1

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->checkLoggedIn()V

    .line 3
    .line 4
    .line 5
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mHtmlHelperDefault:Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/evernote/client/android/EvernoteSession;->getAuthToken()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {p0, v0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createHtmlHelper(Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    iput-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mHtmlHelperDefault:Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

    .line 20
    .line 21
    :cond_0
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mHtmlHelperDefault:Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    .line 23
    monitor-exit p0

    .line 24
    return-object v0

    .line 25
    :catchall_0
    move-exception v0

    .line 26
    monitor-exit p0

    .line 27
    throw v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public getLinkedHtmlHelper(Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;
    .locals 3
    .param p1    # Lcom/evernote/edam/type/LinkedNotebook;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Lcom/evernote/edam/type/LinkedNotebook;->getGuid()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mLinkedHtmlHelper:Ljava/util/Map;

    .line 6
    .line 7
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    check-cast v1, Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

    .line 12
    .line 13
    if-nez v1, :cond_0

    .line 14
    .line 15
    invoke-virtual {p1}, Lcom/evernote/edam/type/LinkedNotebook;->getNoteStoreUrl()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    iget-object v2, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    .line 20
    .line 21
    invoke-virtual {v2}, Lcom/evernote/client/android/EvernoteSession;->getAuthToken()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    invoke-static {v2}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotEmpty(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    check-cast v2, Ljava/lang/String;

    .line 30
    .line 31
    invoke-virtual {p0, v1, v2}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getNoteStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    invoke-virtual {p1}, Lcom/evernote/edam/type/LinkedNotebook;->getShareKey()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    invoke-virtual {v1, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->authenticateToSharedNotebook(Ljava/lang/String;)Lcom/evernote/edam/userstore/AuthenticationResult;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/AuthenticationResult;->getAuthenticationToken()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    invoke-virtual {p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createHtmlHelper(Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    iget-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mLinkedHtmlHelper:Ljava/util/Map;

    .line 52
    .line 53
    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    .line 55
    .line 56
    :cond_0
    return-object v1
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public getLinkedHtmlHelperAsync(Lcom/evernote/edam/type/LinkedNotebook;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 2
    .param p1    # Lcom/evernote/edam/type/LinkedNotebook;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/evernote/client/android/asyncclient/EvernoteCallback;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mCreateHelperClient:Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;

    .line 2
    .line 3
    new-instance v1, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$4;

    .line 4
    .line 5
    invoke-direct {v1, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$4;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;Lcom/evernote/edam/type/LinkedNotebook;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    return-object p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public declared-synchronized getLinkedNotebookHelper(Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;
    .locals 2
    .param p1    # Lcom/evernote/edam/type/LinkedNotebook;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-virtual {p1}, Lcom/evernote/edam/type/LinkedNotebook;->getGuid()Ljava/lang/String;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mLinkedNotebookHelpers:Ljava/util/Map;

    .line 7
    .line 8
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    check-cast v1, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;

    .line 13
    .line 14
    if-nez v1, :cond_0

    .line 15
    .line 16
    invoke-virtual {p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createLinkedNotebookHelper(Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    iget-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mLinkedNotebookHelpers:Ljava/util/Map;

    .line 21
    .line 22
    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23
    .line 24
    .line 25
    :cond_0
    monitor-exit p0

    .line 26
    return-object v1

    .line 27
    :catchall_0
    move-exception p1

    .line 28
    monitor-exit p0

    .line 29
    throw p1
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public getLinkedNotebookHelperAsync(Lcom/evernote/edam/type/LinkedNotebook;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 2
    .param p1    # Lcom/evernote/edam/type/LinkedNotebook;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/evernote/client/android/asyncclient/EvernoteCallback;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mCreateHelperClient:Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;

    .line 2
    .line 3
    new-instance v1, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$2;

    .line 4
    .line 5
    invoke-direct {v1, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$2;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;Lcom/evernote/edam/type/LinkedNotebook;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    return-object p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public declared-synchronized getNoteStoreClient()Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;
    .locals 2

    monitor-enter p0

    .line 1
    :try_start_0
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->checkLoggedIn()V

    .line 2
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    invoke-virtual {v0}, Lcom/evernote/client/android/EvernoteSession;->getAuthenticationResult()Lcom/evernote/client/android/AuthenticationResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/evernote/client/android/AuthenticationResult;->getNoteStoreUrl()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    invoke-virtual {v1}, Lcom/evernote/client/android/EvernoteSession;->getAuthToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotEmpty(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getNoteStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNoteStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    monitor-enter p0

    .line 3
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4
    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mNoteStoreClients:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    if-nez v1, :cond_0

    .line 5
    invoke-virtual {p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createEvernoteNoteStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    move-result-object v1

    .line 6
    iget-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mNoteStoreClients:Ljava/util/Map;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7
    :cond_0
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getUserStoreClient()Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;
    .locals 2

    monitor-enter p0

    .line 1
    :try_start_0
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->checkLoggedIn()V

    .line 2
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "https"

    .line 3
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    .line 4
    invoke-virtual {v1}, Lcom/evernote/client/android/EvernoteSession;->getAuthenticationResult()Lcom/evernote/client/android/AuthenticationResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/evernote/client/android/AuthenticationResult;->getEvernoteHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/edam/user"

    .line 5
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 6
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 7
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 8
    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    invoke-virtual {v1}, Lcom/evernote/client/android/EvernoteSession;->getAuthToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getUserStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getUserStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    monitor-enter p0

    .line 9
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 10
    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mUserStoreClients:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;

    if-nez v1, :cond_0

    .line 11
    invoke-virtual {p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createUserStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;

    move-result-object v1

    .line 12
    iget-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mUserStoreClients:Ljava/util/Map;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13
    :cond_0
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method protected final isBusinessAuthExpired()Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mBusinessAuthenticationResult:Lcom/evernote/edam/userstore/AuthenticationResult;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/evernote/edam/userstore/AuthenticationResult;->getExpiration()J

    .line 6
    .line 7
    .line 8
    move-result-wide v0

    .line 9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 10
    .line 11
    .line 12
    move-result-wide v2

    .line 13
    cmp-long v4, v0, v2

    .line 14
    .line 15
    if-gez v4, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/4 v0, 0x0

    .line 19
    goto :goto_1

    .line 20
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 21
    :goto_1
    return v0
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

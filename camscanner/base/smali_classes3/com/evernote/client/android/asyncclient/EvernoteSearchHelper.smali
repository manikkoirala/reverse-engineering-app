.class public Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;
.super Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;
.source "EvernoteSearchHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;,
        Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;,
        Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;
    }
.end annotation


# instance fields
.field private final mClientFactory:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

.field private final mPrivateClient:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

.field private final mSession:Lcom/evernote/client/android/EvernoteSession;


# direct methods
.method public constructor <init>(Lcom/evernote/client/android/EvernoteSession;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p1    # Lcom/evernote/client/android/EvernoteSession;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 2
    .line 3
    .line 4
    invoke-static {p1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    check-cast p1, Lcom/evernote/client/android/EvernoteSession;

    .line 9
    .line 10
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->mSession:Lcom/evernote/client/android/EvernoteSession;

    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/evernote/client/android/EvernoteSession;->getEvernoteClientFactory()Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->mClientFactory:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    .line 17
    .line 18
    invoke-virtual {p1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getNoteStoreClient()Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->mPrivateClient:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private maybeRethrow(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Ljava/lang/Exception;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->isIgnoreExceptions()Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    throw p2
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method


# virtual methods
.method public execute(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;
    .locals 5
    .param p1    # Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$000(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$100(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-ge v0, v1, :cond_5

    .line 10
    .line 11
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;

    .line 12
    .line 13
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$200(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Ljava/util/EnumSet;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    const/4 v2, 0x0

    .line 18
    invoke-direct {v0, v1, v2}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;-><init>(Ljava/util/Set;Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$1;)V

    .line 19
    .line 20
    .line 21
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$200(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Ljava/util/EnumSet;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    invoke-virtual {v1}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 30
    .line 31
    .line 32
    move-result v2

    .line 33
    if-eqz v2, :cond_4

    .line 34
    .line 35
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    check-cast v2, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;

    .line 40
    .line 41
    sget-object v3, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$2;->$SwitchMap$com$evernote$client$android$asyncclient$EvernoteSearchHelper$Scope:[I

    .line 42
    .line 43
    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    aget v2, v3, v2

    .line 48
    .line 49
    const/4 v3, 0x1

    .line 50
    if-eq v2, v3, :cond_3

    .line 51
    .line 52
    const/4 v4, 0x2

    .line 53
    if-eq v2, v4, :cond_2

    .line 54
    .line 55
    const/4 v4, 0x3

    .line 56
    if-eq v2, v4, :cond_1

    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_1
    invoke-virtual {p0, p1, v3}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->getLinkedNotebooks(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Z)Ljava/util/List;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 64
    .line 65
    .line 66
    move-result-object v2

    .line 67
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 68
    .line 69
    .line 70
    move-result v3

    .line 71
    if-eqz v3, :cond_0

    .line 72
    .line 73
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 74
    .line 75
    .line 76
    move-result-object v3

    .line 77
    check-cast v3, Lcom/evernote/edam/type/LinkedNotebook;

    .line 78
    .line 79
    :try_start_0
    invoke-virtual {p0, p1, v3}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->findNotesInBusinessNotebook(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Lcom/evernote/edam/type/LinkedNotebook;)Ljava/util/List;

    .line 80
    .line 81
    .line 82
    move-result-object v4

    .line 83
    invoke-static {v0, v3, v4}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;->access$600(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;Lcom/evernote/edam/type/LinkedNotebook;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    .line 85
    .line 86
    goto :goto_1

    .line 87
    :catch_0
    move-exception v3

    .line 88
    invoke-direct {p0, p1, v3}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->maybeRethrow(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Ljava/lang/Exception;)V

    .line 89
    .line 90
    .line 91
    goto :goto_1

    .line 92
    :cond_2
    const/4 v2, 0x0

    .line 93
    invoke-virtual {p0, p1, v2}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->getLinkedNotebooks(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Z)Ljava/util/List;

    .line 94
    .line 95
    .line 96
    move-result-object v2

    .line 97
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 98
    .line 99
    .line 100
    move-result-object v2

    .line 101
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 102
    .line 103
    .line 104
    move-result v3

    .line 105
    if-eqz v3, :cond_0

    .line 106
    .line 107
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 108
    .line 109
    .line 110
    move-result-object v3

    .line 111
    check-cast v3, Lcom/evernote/edam/type/LinkedNotebook;

    .line 112
    .line 113
    :try_start_1
    invoke-virtual {p0, p1, v3}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->findNotesInLinkedNotebook(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Lcom/evernote/edam/type/LinkedNotebook;)Ljava/util/List;

    .line 114
    .line 115
    .line 116
    move-result-object v4

    .line 117
    invoke-static {v0, v3, v4}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;->access$500(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;Lcom/evernote/edam/type/LinkedNotebook;Ljava/util/List;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 118
    .line 119
    .line 120
    goto :goto_2

    .line 121
    :catch_1
    move-exception v3

    .line 122
    invoke-direct {p0, p1, v3}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->maybeRethrow(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Ljava/lang/Exception;)V

    .line 123
    .line 124
    .line 125
    goto :goto_2

    .line 126
    :cond_3
    :try_start_2
    invoke-virtual {p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->findPersonalNotes(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Ljava/util/List;

    .line 127
    .line 128
    .line 129
    move-result-object v2

    .line 130
    invoke-static {v0, v2}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;->access$400(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;Ljava/util/List;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 131
    .line 132
    .line 133
    goto :goto_0

    .line 134
    :catch_2
    move-exception v2

    .line 135
    invoke-direct {p0, p1, v2}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->maybeRethrow(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Ljava/lang/Exception;)V

    .line 136
    .line 137
    .line 138
    goto :goto_0

    .line 139
    :cond_4
    return-object v0

    .line 140
    :cond_5
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 141
    .line 142
    const-string v0, "offset must be less than max notes"

    .line 143
    .line 144
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 145
    .line 146
    .line 147
    throw p1
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method public executeAsync(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .param p1    # Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/evernote/client/android/asyncclient/EvernoteCallback;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$1;

    .line 2
    .line 3
    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$1;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    return-object p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method protected findAllNotes(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/notestore/NoteFilter;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;",
            "Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;",
            "Lcom/evernote/edam/notestore/NoteFilter;",
            ")",
            "Ljava/util/List<",
            "Lcom/evernote/edam/notestore/NotesMetadataList;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$100(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)I

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$000(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)I

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    sub-int v3, v1, v2

    .line 15
    .line 16
    :goto_0
    if-lez v3, :cond_0

    .line 17
    .line 18
    :try_start_0
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$800(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    .line 19
    .line 20
    .line 21
    move-result-object v4

    .line 22
    invoke-virtual {p2, p3, v2, v1, v4}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->findNotesMetadata(Lcom/evernote/edam/notestore/NoteFilter;IILcom/evernote/edam/notestore/NotesMetadataResultSpec;)Lcom/evernote/edam/notestore/NotesMetadataList;

    .line 23
    .line 24
    .line 25
    move-result-object v4

    .line 26
    invoke-virtual {v4}, Lcom/evernote/edam/notestore/NotesMetadataList;->getTotalNotes()I

    .line 27
    .line 28
    .line 29
    move-result v5

    .line 30
    invoke-virtual {v4}, Lcom/evernote/edam/notestore/NotesMetadataList;->getStartIndex()I

    .line 31
    .line 32
    .line 33
    move-result v6

    .line 34
    invoke-virtual {v4}, Lcom/evernote/edam/notestore/NotesMetadataList;->getNotesSize()I

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    add-int/2addr v6, v3

    .line 39
    sub-int v3, v5, v6

    .line 40
    .line 41
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/evernote/edam/error/EDAMUserException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/evernote/edam/error/EDAMSystemException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/evernote/thrift/TException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/evernote/edam/error/EDAMNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    .line 43
    .line 44
    goto :goto_2

    .line 45
    :catch_0
    move-exception v4

    .line 46
    goto :goto_1

    .line 47
    :catch_1
    move-exception v4

    .line 48
    goto :goto_1

    .line 49
    :catch_2
    move-exception v4

    .line 50
    goto :goto_1

    .line 51
    :catch_3
    move-exception v4

    .line 52
    :goto_1
    invoke-direct {p0, p1, v4}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->maybeRethrow(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Ljava/lang/Exception;)V

    .line 53
    .line 54
    .line 55
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$900(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)I

    .line 56
    .line 57
    .line 58
    move-result v4

    .line 59
    sub-int/2addr v3, v4

    .line 60
    :goto_2
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$900(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)I

    .line 61
    .line 62
    .line 63
    move-result v4

    .line 64
    add-int/2addr v2, v4

    .line 65
    goto :goto_0

    .line 66
    :cond_0
    return-object v0
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method protected findNotesInBusinessNotebook(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Lcom/evernote/edam/type/LinkedNotebook;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ")",
            "Ljava/util/List<",
            "Lcom/evernote/edam/notestore/NotesMetadataList;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->mClientFactory:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getBusinessNotebookHelper()Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->mClientFactory:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    .line 8
    .line 9
    invoke-virtual {v1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getLinkedNotebookHelper(Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;

    .line 10
    .line 11
    .line 12
    move-result-object p2

    .line 13
    invoke-virtual {p2}, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;->getCorrespondingNotebook()Lcom/evernote/edam/type/Notebook;

    .line 14
    .line 15
    .line 16
    move-result-object p2

    .line 17
    new-instance v1, Lcom/evernote/edam/notestore/NoteFilter;

    .line 18
    .line 19
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$700(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Lcom/evernote/edam/notestore/NoteFilter;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    invoke-direct {v1, v2}, Lcom/evernote/edam/notestore/NoteFilter;-><init>(Lcom/evernote/edam/notestore/NoteFilter;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p2}, Lcom/evernote/edam/type/Notebook;->getGuid()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object p2

    .line 30
    invoke-virtual {v1, p2}, Lcom/evernote/edam/notestore/NoteFilter;->setNotebookGuid(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0}, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->getClient()Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    .line 34
    .line 35
    .line 36
    move-result-object p2

    .line 37
    invoke-virtual {p0, p1, p2, v1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->findAllNotes(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/notestore/NoteFilter;)Ljava/util/List;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    return-object p1
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method protected findNotesInLinkedNotebook(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Lcom/evernote/edam/type/LinkedNotebook;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ")",
            "Ljava/util/List<",
            "Lcom/evernote/edam/notestore/NotesMetadataList;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->mClientFactory:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    .line 2
    .line 3
    invoke-virtual {v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getLinkedNotebookHelper(Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;

    .line 4
    .line 5
    .line 6
    move-result-object p2

    .line 7
    invoke-virtual {p2}, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;->getCorrespondingNotebook()Lcom/evernote/edam/type/Notebook;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    new-instance v1, Lcom/evernote/edam/notestore/NoteFilter;

    .line 12
    .line 13
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$700(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Lcom/evernote/edam/notestore/NoteFilter;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    invoke-direct {v1, v2}, Lcom/evernote/edam/notestore/NoteFilter;-><init>(Lcom/evernote/edam/notestore/NoteFilter;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/evernote/edam/type/Notebook;->getGuid()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-virtual {v1, v0}, Lcom/evernote/edam/notestore/NoteFilter;->setNotebookGuid(Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p2}, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;->getClient()Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    .line 28
    .line 29
    .line 30
    move-result-object p2

    .line 31
    invoke-virtual {p0, p1, p2, v1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->findAllNotes(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/notestore/NoteFilter;)Ljava/util/List;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    return-object p1
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method protected findPersonalNotes(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;",
            ")",
            "Ljava/util/List<",
            "Lcom/evernote/edam/notestore/NotesMetadataList;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->mPrivateClient:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    .line 2
    .line 3
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$700(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Lcom/evernote/edam/notestore/NoteFilter;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {p0, p1, v0, v1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->findAllNotes(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/notestore/NoteFilter;)Ljava/util/List;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    return-object p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method protected getLinkedNotebooks(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Z)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    if-eqz p2, :cond_1

    .line 2
    .line 3
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$1000(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object p2

    .line 7
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    .line 8
    .line 9
    .line 10
    move-result p2

    .line 11
    if-eqz p2, :cond_0

    .line 12
    .line 13
    :try_start_0
    iget-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->mClientFactory:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    .line 14
    .line 15
    invoke-virtual {p2}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getBusinessNotebookHelper()Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;

    .line 16
    .line 17
    .line 18
    move-result-object p2

    .line 19
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->mSession:Lcom/evernote/client/android/EvernoteSession;

    .line 20
    .line 21
    invoke-virtual {p2, v0}, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->listBusinessNotebooks(Lcom/evernote/client/android/EvernoteSession;)Ljava/util/List;

    .line 22
    .line 23
    .line 24
    move-result-object p1
    :try_end_0
    .catch Lcom/evernote/edam/error/EDAMUserException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/evernote/edam/error/EDAMSystemException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/evernote/edam/error/EDAMNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/evernote/thrift/TException; {:try_start_0 .. :try_end_0} :catch_0

    .line 25
    return-object p1

    .line 26
    :catch_0
    move-exception p2

    .line 27
    goto :goto_0

    .line 28
    :catch_1
    move-exception p2

    .line 29
    goto :goto_0

    .line 30
    :catch_2
    move-exception p2

    .line 31
    goto :goto_0

    .line 32
    :catch_3
    move-exception p2

    .line 33
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->maybeRethrow(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Ljava/lang/Exception;)V

    .line 34
    .line 35
    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    return-object p1

    .line 41
    :cond_0
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$1000(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Ljava/util/List;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    return-object p1

    .line 46
    :cond_1
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$1100(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Ljava/util/List;

    .line 47
    .line 48
    .line 49
    move-result-object p2

    .line 50
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    .line 51
    .line 52
    .line 53
    move-result p2

    .line 54
    if-eqz p2, :cond_2

    .line 55
    .line 56
    :try_start_1
    iget-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->mPrivateClient:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    .line 57
    .line 58
    invoke-virtual {p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->listLinkedNotebooks()Ljava/util/List;

    .line 59
    .line 60
    .line 61
    move-result-object p1
    :try_end_1
    .catch Lcom/evernote/edam/error/EDAMUserException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Lcom/evernote/edam/error/EDAMNotFoundException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Lcom/evernote/thrift/TException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Lcom/evernote/edam/error/EDAMSystemException; {:try_start_1 .. :try_end_1} :catch_4

    .line 62
    return-object p1

    .line 63
    :catch_4
    move-exception p2

    .line 64
    goto :goto_1

    .line 65
    :catch_5
    move-exception p2

    .line 66
    goto :goto_1

    .line 67
    :catch_6
    move-exception p2

    .line 68
    goto :goto_1

    .line 69
    :catch_7
    move-exception p2

    .line 70
    :goto_1
    invoke-direct {p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->maybeRethrow(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Ljava/lang/Exception;)V

    .line 71
    .line 72
    .line 73
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    .line 74
    .line 75
    .line 76
    move-result-object p1

    .line 77
    return-object p1

    .line 78
    :cond_2
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$1100(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Ljava/util/List;

    .line 79
    .line 80
    .line 81
    move-result-object p1

    .line 82
    return-object p1
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

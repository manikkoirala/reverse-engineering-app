.class public Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;
.super Ljava/lang/Object;
.source "EvernoteSearchHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Search"
.end annotation


# instance fields
.field private final mBusinessNotebooks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;"
        }
    .end annotation
.end field

.field private mIgnoreExceptions:Z

.field private final mLinkedNotebooks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;"
        }
    .end annotation
.end field

.field private mMaxNotes:I

.field private mNoteFilter:Lcom/evernote/edam/notestore/NoteFilter;

.field private mOffset:I

.field private mPageSize:I

.field private mResultSpec:Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

.field private final mScopes:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const-class v0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;

    .line 5
    .line 6
    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    iput-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mScopes:Ljava/util/EnumSet;

    .line 11
    .line 12
    new-instance v0, Ljava/util/ArrayList;

    .line 13
    .line 14
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 15
    .line 16
    .line 17
    iput-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mLinkedNotebooks:Ljava/util/List;

    .line 18
    .line 19
    new-instance v0, Ljava/util/ArrayList;

    .line 20
    .line 21
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 22
    .line 23
    .line 24
    iput-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mBusinessNotebooks:Ljava/util/List;

    .line 25
    .line 26
    const/4 v0, -0x1

    .line 27
    iput v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mOffset:I

    .line 28
    .line 29
    iput v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mMaxNotes:I

    .line 30
    .line 31
    iput v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mPageSize:I

    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method static synthetic access$000(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)I
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->getOffset()I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static synthetic access$100(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)I
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->getMaxNotes()I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static synthetic access$1000(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Ljava/util/List;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mBusinessNotebooks:Ljava/util/List;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static synthetic access$1100(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Ljava/util/List;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mLinkedNotebooks:Ljava/util/List;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static synthetic access$200(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Ljava/util/EnumSet;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->getScopes()Ljava/util/EnumSet;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static synthetic access$700(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Lcom/evernote/edam/notestore/NoteFilter;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->getNoteFilter()Lcom/evernote/edam/notestore/NoteFilter;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static synthetic access$800(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Lcom/evernote/edam/notestore/NotesMetadataResultSpec;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->getResultSpec()Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static synthetic access$900(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)I
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->getPageSize()I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private getMaxNotes()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mMaxNotes:I

    .line 2
    .line 3
    if-gez v0, :cond_0

    .line 4
    .line 5
    const/16 v0, 0xa

    .line 6
    .line 7
    :cond_0
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private getNoteFilter()Lcom/evernote/edam/notestore/NoteFilter;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mNoteFilter:Lcom/evernote/edam/notestore/NoteFilter;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/evernote/edam/notestore/NoteFilter;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteFilter;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mNoteFilter:Lcom/evernote/edam/notestore/NoteFilter;

    .line 11
    .line 12
    sget-object v1, Lcom/evernote/edam/type/NoteSortOrder;->UPDATED:Lcom/evernote/edam/type/NoteSortOrder;

    .line 13
    .line 14
    invoke-virtual {v1}, Lcom/evernote/edam/type/NoteSortOrder;->getValue()I

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteFilter;->setOrder(I)V

    .line 19
    .line 20
    .line 21
    :cond_0
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mNoteFilter:Lcom/evernote/edam/notestore/NoteFilter;

    .line 22
    .line 23
    return-object v0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private getOffset()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mOffset:I

    .line 2
    .line 3
    if-gez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    :cond_0
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private getPageSize()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mPageSize:I

    .line 2
    .line 3
    if-gez v0, :cond_0

    .line 4
    .line 5
    const/16 v0, 0xa

    .line 6
    .line 7
    :cond_0
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private getResultSpec()Lcom/evernote/edam/notestore/NotesMetadataResultSpec;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mResultSpec:Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mResultSpec:Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    .line 11
    .line 12
    const/4 v1, 0x1

    .line 13
    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeTitle(Z)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mResultSpec:Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeNotebookGuid(Z)V

    .line 19
    .line 20
    .line 21
    :cond_0
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mResultSpec:Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    .line 22
    .line 23
    return-object v0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private getScopes()Ljava/util/EnumSet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mScopes:Ljava/util/EnumSet;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mScopes:Ljava/util/EnumSet;

    .line 10
    .line 11
    sget-object v1, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;->PERSONAL_NOTES:Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    .line 14
    .line 15
    .line 16
    :cond_0
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mScopes:Ljava/util/EnumSet;

    .line 17
    .line 18
    return-object v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method


# virtual methods
.method public addLinkedNotebook(Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;
    .locals 1

    .line 1
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->isBusinessNotebook(Lcom/evernote/edam/type/LinkedNotebook;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;->BUSINESS:Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;

    .line 8
    .line 9
    invoke-virtual {p0, v0}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->addScope(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;)Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mBusinessNotebooks:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    sget-object v0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;->LINKED_NOTEBOOKS:Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;

    .line 19
    .line 20
    invoke-virtual {p0, v0}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->addScope(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;)Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mLinkedNotebooks:Ljava/util/List;

    .line 24
    .line 25
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 26
    .line 27
    .line 28
    :goto_0
    return-object p0
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public addScope(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;)Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mScopes:Ljava/util/EnumSet;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public isIgnoreExceptions()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mIgnoreExceptions:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public setIgnoreExceptions(Z)Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mIgnoreExceptions:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setMaxNotes(I)Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;
    .locals 1

    .line 1
    const-string v0, "maxNotes must be greater or equal 1"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkArgumentPositive(ILjava/lang/String;)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    iput p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mMaxNotes:I

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setNoteFilter(Lcom/evernote/edam/notestore/NoteFilter;)Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    check-cast p1, Lcom/evernote/edam/notestore/NoteFilter;

    .line 6
    .line 7
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mNoteFilter:Lcom/evernote/edam/notestore/NoteFilter;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setOffset(I)Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;
    .locals 1

    .line 1
    const-string v0, "negative value now allowed"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkArgumentNonnegative(ILjava/lang/String;)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    iput p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mOffset:I

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setPageSize(I)Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;
    .locals 1

    .line 1
    const-string v0, "pageSize must be greater or equal 1"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkArgumentPositive(ILjava/lang/String;)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    iput p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mPageSize:I

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setResultSpec(Lcom/evernote/edam/notestore/NotesMetadataResultSpec;)Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    check-cast p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    .line 6
    .line 7
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mResultSpec:Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

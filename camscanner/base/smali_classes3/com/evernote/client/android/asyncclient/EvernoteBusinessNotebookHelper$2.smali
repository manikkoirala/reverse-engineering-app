.class Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper$2;
.super Ljava/lang/Object;
.source "EvernoteBusinessNotebookHelper.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->createBusinessNotebookAsync(Lcom/evernote/edam/type/Notebook;Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/evernote/edam/type/LinkedNotebook;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;

.field final synthetic val$defaultClient:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

.field final synthetic val$notebook:Lcom/evernote/edam/type/Notebook;


# direct methods
.method constructor <init>(Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;Lcom/evernote/edam/type/Notebook;Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper$2;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;

    .line 2
    .line 3
    iput-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper$2;->val$notebook:Lcom/evernote/edam/type/Notebook;

    .line 4
    .line 5
    iput-object p3, p0, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper$2;->val$defaultClient:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    .line 6
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method


# virtual methods
.method public call()Lcom/evernote/edam/type/LinkedNotebook;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper$2;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper$2;->val$notebook:Lcom/evernote/edam/type/Notebook;

    iget-object v2, p0, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper$2;->val$defaultClient:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    invoke-virtual {v0, v1, v2}, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->createBusinessNotebook(Lcom/evernote/edam/type/Notebook;Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;)Lcom/evernote/edam/type/LinkedNotebook;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper$2;->call()Lcom/evernote/edam/type/LinkedNotebook;

    move-result-object v0

    return-object v0
.end method

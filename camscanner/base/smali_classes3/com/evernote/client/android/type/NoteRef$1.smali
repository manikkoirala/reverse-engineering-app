.class final Lcom/evernote/client/android/type/NoteRef$1;
.super Ljava/lang/Object;
.source "NoteRef.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/evernote/client/android/type/NoteRef;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/evernote/client/android/type/NoteRef;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/evernote/client/android/type/NoteRef;
    .locals 5

    .line 2
    new-instance v0, Lcom/evernote/client/android/type/NoteRef;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    const/4 v4, 0x1

    if-ne p1, v4, :cond_0

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/evernote/client/android/type/NoteRef;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/evernote/client/android/type/NoteRef$1;->createFromParcel(Landroid/os/Parcel;)Lcom/evernote/client/android/type/NoteRef;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/evernote/client/android/type/NoteRef;
    .locals 0

    .line 2
    new-array p1, p1, [Lcom/evernote/client/android/type/NoteRef;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/evernote/client/android/type/NoteRef$1;->newArray(I)[Lcom/evernote/client/android/type/NoteRef;

    move-result-object p1

    return-object p1
.end method

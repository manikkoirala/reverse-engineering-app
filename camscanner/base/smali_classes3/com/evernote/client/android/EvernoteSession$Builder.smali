.class public Lcom/evernote/client/android/EvernoteSession$Builder;
.super Ljava/lang/Object;
.source "EvernoteSession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/evernote/client/android/EvernoteSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mEvernoteService:Lcom/evernote/client/android/EvernoteSession$EvernoteService;

.field private mForceAuthenticationInThirdPartyApp:Z

.field private mLocale:Ljava/util/Locale;

.field private mSupportAppLinkedNotebooks:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    invoke-static {p1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 5
    .line 6
    .line 7
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mContext:Landroid/content/Context;

    .line 12
    .line 13
    const/4 p1, 0x1

    .line 14
    iput-boolean p1, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mSupportAppLinkedNotebooks:Z

    .line 15
    .line 16
    sget-object p1, Lcom/evernote/client/android/EvernoteSession$EvernoteService;->SANDBOX:Lcom/evernote/client/android/EvernoteSession$EvernoteService;

    .line 17
    .line 18
    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mEvernoteService:Lcom/evernote/client/android/EvernoteSession$EvernoteService;

    .line 19
    .line 20
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mLocale:Ljava/util/Locale;

    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private build(Lcom/evernote/client/android/EvernoteSession;)Lcom/evernote/client/android/EvernoteSession;
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mContext:Landroid/content/Context;

    invoke-static {p1, v0}, Lcom/evernote/client/android/EvernoteSession;->access$402(Lcom/evernote/client/android/EvernoteSession;Landroid/content/Context;)Landroid/content/Context;

    .line 7
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mLocale:Ljava/util/Locale;

    invoke-static {p1, v0}, Lcom/evernote/client/android/EvernoteSession;->access$502(Lcom/evernote/client/android/EvernoteSession;Ljava/util/Locale;)Ljava/util/Locale;

    .line 8
    iget-boolean v0, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mSupportAppLinkedNotebooks:Z

    invoke-static {p1, v0}, Lcom/evernote/client/android/EvernoteSession;->access$602(Lcom/evernote/client/android/EvernoteSession;Z)Z

    .line 9
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mEvernoteService:Lcom/evernote/client/android/EvernoteSession$EvernoteService;

    invoke-static {p1, v0}, Lcom/evernote/client/android/EvernoteSession;->access$702(Lcom/evernote/client/android/EvernoteSession;Lcom/evernote/client/android/EvernoteSession$EvernoteService;)Lcom/evernote/client/android/EvernoteSession$EvernoteService;

    .line 10
    iget-boolean v0, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mForceAuthenticationInThirdPartyApp:Z

    invoke-static {p1, v0}, Lcom/evernote/client/android/EvernoteSession;->access$802(Lcom/evernote/client/android/EvernoteSession;Z)Z

    return-object p1
.end method


# virtual methods
.method public build(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/EvernoteSession;
    .locals 2

    .line 1
    new-instance v0, Lcom/evernote/client/android/EvernoteSession;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/evernote/client/android/EvernoteSession;-><init>(Lcom/evernote/client/android/EvernoteSession$1;)V

    .line 2
    invoke-static {p1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotEmpty(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/evernote/client/android/EvernoteSession;->access$102(Lcom/evernote/client/android/EvernoteSession;Ljava/lang/String;)Ljava/lang/String;

    .line 3
    invoke-static {p2}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotEmpty(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/evernote/client/android/EvernoteSession;->access$202(Lcom/evernote/client/android/EvernoteSession;Ljava/lang/String;)Ljava/lang/String;

    .line 4
    iget-object p1, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/evernote/client/android/AuthenticationResult;->fromPreferences(Landroid/content/Context;)Lcom/evernote/client/android/AuthenticationResult;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/evernote/client/android/EvernoteSession;->access$302(Lcom/evernote/client/android/EvernoteSession;Lcom/evernote/client/android/AuthenticationResult;)Lcom/evernote/client/android/AuthenticationResult;

    .line 5
    invoke-direct {p0, v0}, Lcom/evernote/client/android/EvernoteSession$Builder;->build(Lcom/evernote/client/android/EvernoteSession;)Lcom/evernote/client/android/EvernoteSession;

    move-result-object p1

    return-object p1
.end method

.method public buildForSingleUser(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/EvernoteSession;
    .locals 3

    .line 1
    new-instance v0, Lcom/evernote/client/android/EvernoteSession;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/evernote/client/android/EvernoteSession;-><init>(Lcom/evernote/client/android/EvernoteSession$1;)V

    .line 5
    .line 6
    .line 7
    new-instance v1, Lcom/evernote/client/android/AuthenticationResult;

    .line 8
    .line 9
    invoke-static {p1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotEmpty(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    check-cast p1, Ljava/lang/String;

    .line 14
    .line 15
    invoke-static {p2}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotEmpty(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 16
    .line 17
    .line 18
    move-result-object p2

    .line 19
    check-cast p2, Ljava/lang/String;

    .line 20
    .line 21
    iget-boolean v2, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mSupportAppLinkedNotebooks:Z

    .line 22
    .line 23
    invoke-direct {v1, p1, p2, v2}, Lcom/evernote/client/android/AuthenticationResult;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 24
    .line 25
    .line 26
    invoke-static {v0, v1}, Lcom/evernote/client/android/EvernoteSession;->access$302(Lcom/evernote/client/android/EvernoteSession;Lcom/evernote/client/android/AuthenticationResult;)Lcom/evernote/client/android/AuthenticationResult;

    .line 27
    .line 28
    .line 29
    invoke-direct {p0, v0}, Lcom/evernote/client/android/EvernoteSession$Builder;->build(Lcom/evernote/client/android/EvernoteSession;)Lcom/evernote/client/android/EvernoteSession;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    return-object p1
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public setEvernoteService(Lcom/evernote/client/android/EvernoteSession$EvernoteService;)Lcom/evernote/client/android/EvernoteSession$Builder;
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    check-cast p1, Lcom/evernote/client/android/EvernoteSession$EvernoteService;

    .line 6
    .line 7
    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mEvernoteService:Lcom/evernote/client/android/EvernoteSession$EvernoteService;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setForceAuthenticationInThirdPartyApp(Z)Lcom/evernote/client/android/EvernoteSession$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mForceAuthenticationInThirdPartyApp:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setLocale(Ljava/util/Locale;)Lcom/evernote/client/android/EvernoteSession$Builder;
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    check-cast p1, Ljava/util/Locale;

    .line 6
    .line 7
    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mLocale:Ljava/util/Locale;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setSupportAppLinkedNotebooks(Z)Lcom/evernote/client/android/EvernoteSession$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mSupportAppLinkedNotebooks:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

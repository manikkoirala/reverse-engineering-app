.class public final Lcom/evernote/client/android/EvernoteSession;
.super Ljava/lang/Object;
.source "EvernoteSession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/evernote/client/android/EvernoteSession$Builder;,
        Lcom/evernote/client/android/EvernoteSession$EvernoteService;
    }
.end annotation


# static fields
.field private static final CAT:Lcom/evernote/client/android/helper/Cat;

.field public static final HOST_CHINA:Ljava/lang/String; = "https://app.yinxiang.com"

.field public static final HOST_PRODUCTION:Ljava/lang/String; = "https://www.evernote.com"

.field public static final HOST_SANDBOX:Ljava/lang/String; = "https://sandbox.evernote.com"

.field public static final REQUEST_CODE_LOGIN:I = 0x3836

.field public static final SCREEN_NAME_INTERNATIONAL:Ljava/lang/String; = "Evernote International"

.field public static final SCREEN_NAME_YXBIJI:Ljava/lang/String; = "\u5370\u8c61\u7b14\u8bb0"

.field private static sInstance:Lcom/evernote/client/android/EvernoteSession;


# instance fields
.field private mApplicationContext:Landroid/content/Context;

.field private mAuthenticationResult:Lcom/evernote/client/android/AuthenticationResult;

.field private mConsumerKey:Ljava/lang/String;

.field private mConsumerSecret:Ljava/lang/String;

.field private mEvernoteClientFactoryBuilder:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;

.field private mEvernoteService:Lcom/evernote/client/android/EvernoteSession$EvernoteService;

.field private mFactoryThreadLocal:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;",
            ">;"
        }
    .end annotation
.end field

.field private mForceAuthenticationInThirdPartyApp:Z

.field private mLocale:Ljava/util/Locale;

.field private mSupportAppLinkedNotebooks:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/evernote/client/android/helper/Cat;

    .line 2
    .line 3
    const-string v1, "EvernoteSession"

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/evernote/client/android/helper/Cat;-><init>(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    sput-object v0, Lcom/evernote/client/android/EvernoteSession;->CAT:Lcom/evernote/client/android/helper/Cat;

    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    sput-object v0, Lcom/evernote/client/android/EvernoteSession;->sInstance:Lcom/evernote/client/android/EvernoteSession;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
.end method

.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/evernote/client/android/EvernoteSession$1;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/evernote/client/android/EvernoteSession;-><init>()V

    return-void
.end method

.method static synthetic access$102(Lcom/evernote/client/android/EvernoteSession;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession;->mConsumerKey:Ljava/lang/String;

    .line 2
    .line 3
    return-object p1
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method static synthetic access$202(Lcom/evernote/client/android/EvernoteSession;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession;->mConsumerSecret:Ljava/lang/String;

    .line 2
    .line 3
    return-object p1
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method static synthetic access$302(Lcom/evernote/client/android/EvernoteSession;Lcom/evernote/client/android/AuthenticationResult;)Lcom/evernote/client/android/AuthenticationResult;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession;->mAuthenticationResult:Lcom/evernote/client/android/AuthenticationResult;

    .line 2
    .line 3
    return-object p1
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method static synthetic access$402(Lcom/evernote/client/android/EvernoteSession;Landroid/content/Context;)Landroid/content/Context;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession;->mApplicationContext:Landroid/content/Context;

    .line 2
    .line 3
    return-object p1
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method static synthetic access$502(Lcom/evernote/client/android/EvernoteSession;Ljava/util/Locale;)Ljava/util/Locale;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession;->mLocale:Ljava/util/Locale;

    .line 2
    .line 3
    return-object p1
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method static synthetic access$602(Lcom/evernote/client/android/EvernoteSession;Z)Z
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/evernote/client/android/EvernoteSession;->mSupportAppLinkedNotebooks:Z

    .line 2
    .line 3
    return p1
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method static synthetic access$702(Lcom/evernote/client/android/EvernoteSession;Lcom/evernote/client/android/EvernoteSession$EvernoteService;)Lcom/evernote/client/android/EvernoteSession$EvernoteService;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession;->mEvernoteService:Lcom/evernote/client/android/EvernoteSession$EvernoteService;

    .line 2
    .line 3
    return-object p1
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method static synthetic access$802(Lcom/evernote/client/android/EvernoteSession;Z)Z
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/evernote/client/android/EvernoteSession;->mForceAuthenticationInThirdPartyApp:Z

    .line 2
    .line 3
    return p1
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static getInstance()Lcom/evernote/client/android/EvernoteSession;
    .locals 1

    .line 1
    sget-object v0, Lcom/evernote/client/android/EvernoteSession;->sInstance:Lcom/evernote/client/android/EvernoteSession;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method


# virtual methods
.method public asSingleton()Lcom/evernote/client/android/EvernoteSession;
    .locals 0

    .line 1
    sput-object p0, Lcom/evernote/client/android/EvernoteSession;->sInstance:Lcom/evernote/client/android/EvernoteSession;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public authenticate(Landroid/app/Activity;)V
    .locals 4

    .line 3
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mConsumerKey:Ljava/lang/String;

    iget-object v1, p0, Lcom/evernote/client/android/EvernoteSession;->mConsumerSecret:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/evernote/client/android/EvernoteSession;->mSupportAppLinkedNotebooks:Z

    iget-object v3, p0, Lcom/evernote/client/android/EvernoteSession;->mLocale:Ljava/util/Locale;

    invoke-static {p1, v0, v1, v2, v3}, Lcom/evernote/client/android/login/EvernoteLoginActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/util/Locale;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x3836

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public authenticate(Landroidx/fragment/app/FragmentActivity;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mConsumerKey:Ljava/lang/String;

    iget-object v1, p0, Lcom/evernote/client/android/EvernoteSession;->mConsumerSecret:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/evernote/client/android/EvernoteSession;->mSupportAppLinkedNotebooks:Z

    iget-object v3, p0, Lcom/evernote/client/android/EvernoteSession;->mLocale:Ljava/util/Locale;

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/client/android/login/EvernoteLoginFragment;->create(Ljava/lang/String;Ljava/lang/String;ZLjava/util/Locale;)Lcom/evernote/client/android/login/EvernoteLoginFragment;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/evernote/client/android/EvernoteSession;->authenticate(Landroidx/fragment/app/FragmentActivity;Lcom/evernote/client/android/login/EvernoteLoginFragment;)V

    return-void
.end method

.method public authenticate(Landroidx/fragment/app/FragmentActivity;Lcom/evernote/client/android/login/EvernoteLoginFragment;)V
    .locals 1

    .line 2
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    const-string v0, "EvernoteDialogFragment"

    invoke-virtual {p2, p1, v0}, Landroidx/fragment/app/DialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public getApplicationContext()Landroid/content/Context;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mApplicationContext:Landroid/content/Context;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getAuthToken()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mAuthenticationResult:Lcom/evernote/client/android/AuthenticationResult;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/evernote/client/android/AuthenticationResult;->getAuthToken()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getAuthenticationResult()Lcom/evernote/client/android/AuthenticationResult;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mAuthenticationResult:Lcom/evernote/client/android/AuthenticationResult;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public declared-synchronized getEvernoteClientFactory()Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;
    .locals 2

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mFactoryThreadLocal:Ljava/lang/ThreadLocal;

    .line 3
    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    new-instance v0, Ljava/lang/ThreadLocal;

    .line 7
    .line 8
    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mFactoryThreadLocal:Ljava/lang/ThreadLocal;

    .line 12
    .line 13
    :cond_0
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mEvernoteClientFactoryBuilder:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;

    .line 14
    .line 15
    if-nez v0, :cond_1

    .line 16
    .line 17
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;

    .line 18
    .line 19
    invoke-direct {v0, p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;-><init>(Lcom/evernote/client/android/EvernoteSession;)V

    .line 20
    .line 21
    .line 22
    iput-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mEvernoteClientFactoryBuilder:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;

    .line 23
    .line 24
    :cond_1
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mFactoryThreadLocal:Ljava/lang/ThreadLocal;

    .line 25
    .line 26
    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    check-cast v0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    .line 31
    .line 32
    if-nez v0, :cond_2

    .line 33
    .line 34
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mEvernoteClientFactoryBuilder:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;

    .line 35
    .line 36
    invoke-virtual {v0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->build()Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    iget-object v1, p0, Lcom/evernote/client/android/EvernoteSession;->mFactoryThreadLocal:Ljava/lang/ThreadLocal;

    .line 41
    .line 42
    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 43
    .line 44
    .line 45
    :cond_2
    monitor-exit p0

    .line 46
    return-object v0

    .line 47
    :catchall_0
    move-exception v0

    .line 48
    monitor-exit p0

    .line 49
    throw v0
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method protected getEvernoteService()Lcom/evernote/client/android/EvernoteSession$EvernoteService;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mEvernoteService:Lcom/evernote/client/android/EvernoteSession$EvernoteService;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method isForceAuthenticationInThirdPartyApp()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/evernote/client/android/EvernoteSession;->mForceAuthenticationInThirdPartyApp:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public declared-synchronized isLoggedIn()Z
    .locals 1

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mAuthenticationResult:Lcom/evernote/client/android/AuthenticationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3
    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    monitor-exit p0

    .line 10
    return v0

    .line 11
    :catchall_0
    move-exception v0

    .line 12
    monitor-exit p0

    .line 13
    throw v0
    .line 14
    .line 15
.end method

.method public declared-synchronized logOut()Z
    .locals 1

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-virtual {p0}, Lcom/evernote/client/android/EvernoteSession;->isLoggedIn()Z

    .line 3
    .line 4
    .line 5
    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    monitor-exit p0

    .line 9
    const/4 v0, 0x0

    .line 10
    return v0

    .line 11
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mAuthenticationResult:Lcom/evernote/client/android/AuthenticationResult;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/evernote/client/android/AuthenticationResult;->clear()V

    .line 14
    .line 15
    .line 16
    const/4 v0, 0x0

    .line 17
    iput-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mAuthenticationResult:Lcom/evernote/client/android/AuthenticationResult;

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/evernote/client/android/EvernoteSession;->getApplicationContext()Landroid/content/Context;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-static {v0}, Lcom/evernote/client/android/EvernoteUtil;->removeAllCookies(Landroid/content/Context;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 24
    .line 25
    .line 26
    monitor-exit p0

    .line 27
    const/4 v0, 0x1

    .line 28
    return v0

    .line 29
    :catchall_0
    move-exception v0

    .line 30
    monitor-exit p0

    .line 31
    throw v0
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method protected declared-synchronized setAuthenticationResult(Lcom/evernote/client/android/AuthenticationResult;)V
    .locals 0

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession;->mAuthenticationResult:Lcom/evernote/client/android/AuthenticationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3
    .line 4
    monitor-exit p0

    .line 5
    return-void

    .line 6
    :catchall_0
    move-exception p1

    .line 7
    monitor-exit p0

    .line 8
    throw p1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public declared-synchronized setEvernoteClientFactoryBuilder(Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;)V
    .locals 0

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    invoke-static {p1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3
    .line 4
    .line 5
    move-result-object p1

    .line 6
    check-cast p1, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;

    .line 7
    .line 8
    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession;->mEvernoteClientFactoryBuilder:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;

    .line 9
    .line 10
    const/4 p1, 0x0

    .line 11
    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession;->mFactoryThreadLocal:Ljava/lang/ThreadLocal;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 12
    .line 13
    monitor-exit p0

    .line 14
    return-void

    .line 15
    :catchall_0
    move-exception p1

    .line 16
    monitor-exit p0

    .line 17
    throw p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

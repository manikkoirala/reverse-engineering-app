.class public Lcom/evernote/client/conn/mobile/DiskBackedByteStore;
.super Lcom/evernote/client/conn/mobile/ByteStore;
.source "DiskBackedByteStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/evernote/client/conn/mobile/DiskBackedByteStore$Factory;
    }
.end annotation


# static fields
.field private static final DEFAULT_MEMORY_BUFFER_SIZE:I = 0x200000


# instance fields
.field protected final mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

.field protected mBytesWritten:I

.field protected final mCacheDir:Ljava/io/File;

.field protected mCacheFile:Ljava/io/File;

.field protected mClosed:Z

.field protected mCurrentOutputStream:Ljava/io/OutputStream;

.field protected mData:[B

.field protected mFileBuffer:[B

.field protected mFileOutputStream:Ljava/io/FileOutputStream;

.field protected final mMaxMemory:I


# direct methods
.method protected constructor <init>(Ljava/io/File;I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/evernote/client/conn/mobile/ByteStore;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCacheDir:Ljava/io/File;

    .line 5
    .line 6
    iput p2, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mMaxMemory:I

    .line 7
    .line 8
    new-instance p1, Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    .line 9
    .line 10
    invoke-direct {p1}, Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;-><init>()V

    .line 11
    .line 12
    .line 13
    iput-object p1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    .line 14
    .line 15
    iput-object p1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCurrentOutputStream:Ljava/io/OutputStream;

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private initBuffers()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mClosed:Z

    .line 2
    .line 3
    if-nez v0, :cond_2

    .line 4
    .line 5
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCurrentOutputStream:Ljava/io/OutputStream;

    .line 6
    .line 7
    if-nez v0, :cond_1

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->swapped()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mFileOutputStream:Ljava/io/FileOutputStream;

    .line 16
    .line 17
    iput-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCurrentOutputStream:Ljava/io/OutputStream;

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    .line 21
    .line 22
    iput-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCurrentOutputStream:Ljava/io/OutputStream;

    .line 23
    .line 24
    :cond_1
    :goto_0
    return-void

    .line 25
    :cond_2
    new-instance v0, Ljava/io/IOException;

    .line 26
    .line 27
    const-string v1, "Already closed"

    .line 28
    .line 29
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    throw v0
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private isSwapRequired(I)Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->swapped()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    iget v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mBytesWritten:I

    .line 8
    .line 9
    add-int/2addr v0, p1

    .line 10
    iget p1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mMaxMemory:I

    .line 11
    .line 12
    if-le v0, p1, :cond_0

    .line 13
    .line 14
    const/4 p1, 0x1

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 p1, 0x0

    .line 17
    :goto_0
    return p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private static readFile(Ljava/io/File;[BI)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    .line 3
    .line 4
    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 5
    .line 6
    .line 7
    const/4 p0, 0x0

    .line 8
    move v0, p2

    .line 9
    const/4 p2, 0x0

    .line 10
    :goto_0
    if-lez v0, :cond_0

    .line 11
    .line 12
    if-ltz p0, :cond_0

    .line 13
    .line 14
    :try_start_1
    invoke-virtual {v1, p1, p2, v0}, Ljava/io/InputStream;->read([BII)I

    .line 15
    .line 16
    .line 17
    move-result p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 18
    add-int/2addr p2, p0

    .line 19
    sub-int/2addr v0, p0

    .line 20
    goto :goto_0

    .line 21
    :catchall_0
    move-exception p0

    .line 22
    move-object v0, v1

    .line 23
    goto :goto_1

    .line 24
    :cond_0
    invoke-static {v1}, Lcom/squareup/okhttp/internal/Util;->〇o〇(Ljava/io/Closeable;)V

    .line 25
    .line 26
    .line 27
    return-void

    .line 28
    :catchall_1
    move-exception p0

    .line 29
    :goto_1
    invoke-static {v0}, Lcom/squareup/okhttp/internal/Util;->〇o〇(Ljava/io/Closeable;)V

    .line 30
    .line 31
    .line 32
    throw p0
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private swapIfNecessary(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->isSwapRequired(I)Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->swapToDisk()V

    .line 8
    .line 9
    .line 10
    :cond_0
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mClosed:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mFileOutputStream:Ljava/io/FileOutputStream;

    .line 6
    .line 7
    invoke-static {v0}, Lcom/squareup/okhttp/internal/Util;->〇o〇(Ljava/io/Closeable;)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    .line 11
    .line 12
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 13
    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    iput-boolean v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mClosed:Z

    .line 17
    .line 18
    :cond_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public getBytesWritten()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mBytesWritten:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getData()[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mData:[B

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-object v0

    .line 6
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->close()V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->swapped()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_3

    .line 14
    .line 15
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mFileBuffer:[B

    .line 16
    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    array-length v0, v0

    .line 20
    iget v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mBytesWritten:I

    .line 21
    .line 22
    if-ge v0, v1, :cond_2

    .line 23
    .line 24
    :cond_1
    iget v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mBytesWritten:I

    .line 25
    .line 26
    new-array v0, v0, [B

    .line 27
    .line 28
    iput-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mFileBuffer:[B

    .line 29
    .line 30
    :cond_2
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCacheFile:Ljava/io/File;

    .line 31
    .line 32
    iget-object v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mFileBuffer:[B

    .line 33
    .line 34
    iget v2, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mBytesWritten:I

    .line 35
    .line 36
    invoke-static {v0, v1, v2}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->readFile(Ljava/io/File;[BI)V

    .line 37
    .line 38
    .line 39
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mFileBuffer:[B

    .line 40
    .line 41
    iput-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mData:[B

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_3
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    .line 45
    .line 46
    invoke-virtual {v0}, Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;->toByteArray()[B

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    iput-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mData:[B

    .line 51
    .line 52
    :goto_0
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mData:[B

    .line 53
    .line 54
    return-object v0
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public reset()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    :try_start_0
    invoke-virtual {p0}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->close()V

    .line 4
    .line 5
    .line 6
    iget-object v2, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCacheFile:Ljava/io/File;

    .line 7
    .line 8
    if-eqz v2, :cond_1

    .line 9
    .line 10
    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    if-eqz v2, :cond_1

    .line 15
    .line 16
    iget-object v2, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCacheFile:Ljava/io/File;

    .line 17
    .line 18
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    if-eqz v2, :cond_0

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    new-instance v2, Ljava/io/IOException;

    .line 26
    .line 27
    const-string v3, "could not delete cache file"

    .line 28
    .line 29
    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    :cond_1
    :goto_0
    iput-object v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mFileOutputStream:Ljava/io/FileOutputStream;

    .line 34
    .line 35
    iput-object v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCurrentOutputStream:Ljava/io/OutputStream;

    .line 36
    .line 37
    iput v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mBytesWritten:I

    .line 38
    .line 39
    iput-boolean v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mClosed:Z

    .line 40
    .line 41
    iput-object v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mData:[B

    .line 42
    .line 43
    return-void

    .line 44
    :catchall_0
    move-exception v2

    .line 45
    iput-object v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mFileOutputStream:Ljava/io/FileOutputStream;

    .line 46
    .line 47
    iput-object v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCurrentOutputStream:Ljava/io/OutputStream;

    .line 48
    .line 49
    iput v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mBytesWritten:I

    .line 50
    .line 51
    iput-boolean v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mClosed:Z

    .line 52
    .line 53
    iput-object v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mData:[B

    .line 54
    .line 55
    throw v2
    .line 56
    .line 57
    .line 58
.end method

.method protected swapToDisk()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCacheDir:Ljava/io/File;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_1

    .line 8
    .line 9
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCacheDir:Ljava/io/File;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    new-instance v0, Ljava/io/IOException;

    .line 19
    .line 20
    const-string v1, "could not create cache dir"

    .line 21
    .line 22
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    throw v0

    .line 26
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCacheDir:Ljava/io/File;

    .line 27
    .line 28
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    if-eqz v0, :cond_2

    .line 33
    .line 34
    const/4 v0, 0x0

    .line 35
    iget-object v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCacheDir:Ljava/io/File;

    .line 36
    .line 37
    const-string v2, "byte_store"

    .line 38
    .line 39
    invoke-static {v2, v0, v1}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    iput-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCacheFile:Ljava/io/File;

    .line 44
    .line 45
    new-instance v0, Ljava/io/FileOutputStream;

    .line 46
    .line 47
    iget-object v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCacheFile:Ljava/io/File;

    .line 48
    .line 49
    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 50
    .line 51
    .line 52
    iput-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mFileOutputStream:Ljava/io/FileOutputStream;

    .line 53
    .line 54
    iget-object v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    .line 55
    .line 56
    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->writeTo(Ljava/io/OutputStream;)V

    .line 57
    .line 58
    .line 59
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    .line 60
    .line 61
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 62
    .line 63
    .line 64
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mFileOutputStream:Ljava/io/FileOutputStream;

    .line 65
    .line 66
    iput-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCurrentOutputStream:Ljava/io/OutputStream;

    .line 67
    .line 68
    return-void

    .line 69
    :cond_2
    new-instance v0, Ljava/io/IOException;

    .line 70
    .line 71
    const-string v1, "cache dir is no directory"

    .line 72
    .line 73
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    throw v0
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method protected swapped()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mBytesWritten:I

    .line 2
    .line 3
    iget v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mMaxMemory:I

    .line 4
    .line 5
    if-le v0, v1, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :goto_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public write(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5
    invoke-direct {p0}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->initBuffers()V

    const/4 v0, 0x1

    .line 6
    invoke-direct {p0, v0}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->swapIfNecessary(I)V

    .line 7
    iget-object v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCurrentOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v1, p1}, Ljava/io/OutputStream;->write(I)V

    .line 8
    iget p1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mBytesWritten:I

    add-int/2addr p1, v0

    iput p1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mBytesWritten:I

    return-void
.end method

.method public write([BII)V
    .locals 1
    .param p1    # [B
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->initBuffers()V

    .line 2
    invoke-direct {p0, p3}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->swapIfNecessary(I)V

    .line 3
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCurrentOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 4
    iget p1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mBytesWritten:I

    add-int/2addr p1, p3

    iput p1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mBytesWritten:I

    return-void
.end method

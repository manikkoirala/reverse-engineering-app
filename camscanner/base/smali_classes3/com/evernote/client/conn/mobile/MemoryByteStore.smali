.class public Lcom/evernote/client/conn/mobile/MemoryByteStore;
.super Lcom/evernote/client/conn/mobile/ByteStore;
.source "MemoryByteStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/evernote/client/conn/mobile/MemoryByteStore$Factory;
    }
.end annotation


# instance fields
.field protected final mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

.field protected mBytesWritten:I

.field protected mClosed:Z

.field protected mData:[B


# direct methods
.method protected constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/evernote/client/conn/mobile/ByteStore;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    .line 5
    .line 6
    invoke-direct {v0}, Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private checkNotClosed()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mClosed:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    new-instance v0, Ljava/io/IOException;

    .line 7
    .line 8
    const-string v1, "Already closed"

    .line 9
    .line 10
    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    throw v0
    .line 14
    .line 15
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mClosed:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    .line 6
    .line 7
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    iput-boolean v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mClosed:Z

    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
.end method

.method public getBytesWritten()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mBytesWritten:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getData()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mData:[B

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-object v0

    .line 6
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/client/conn/mobile/MemoryByteStore;->close()V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;->toByteArray()[B

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iput-object v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mData:[B

    .line 16
    .line 17
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public reset()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    :try_start_0
    invoke-virtual {p0}, Lcom/evernote/client/conn/mobile/MemoryByteStore;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mData:[B

    .line 7
    .line 8
    iput v1, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mBytesWritten:I

    .line 9
    .line 10
    iput-boolean v1, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mClosed:Z

    .line 11
    .line 12
    return-void

    .line 13
    :catchall_0
    move-exception v2

    .line 14
    iput-object v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mData:[B

    .line 15
    .line 16
    iput v1, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mBytesWritten:I

    .line 17
    .line 18
    iput-boolean v1, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mClosed:Z

    .line 19
    .line 20
    throw v2
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public write(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4
    invoke-direct {p0}, Lcom/evernote/client/conn/mobile/MemoryByteStore;->checkNotClosed()V

    .line 5
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 6
    iget p1, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mBytesWritten:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mBytesWritten:I

    return-void
.end method

.method public write([BII)V
    .locals 1
    .param p1    # [B
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/evernote/client/conn/mobile/MemoryByteStore;->checkNotClosed()V

    .line 2
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 3
    iget p1, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mBytesWritten:I

    add-int/2addr p1, p3

    iput p1, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mBytesWritten:I

    return-void
.end method

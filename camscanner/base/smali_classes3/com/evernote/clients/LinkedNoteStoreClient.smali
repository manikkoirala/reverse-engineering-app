.class public Lcom/evernote/clients/LinkedNoteStoreClient;
.super Ljava/lang/Object;
.source "LinkedNoteStoreClient.java"


# instance fields
.field private authenticationResult:Lcom/evernote/edam/userstore/AuthenticationResult;

.field private linkedNoteStoreClient:Lcom/evernote/clients/NoteStoreClient;

.field private mainNoteStoreClient:Lcom/evernote/clients/NoteStoreClient;


# direct methods
.method constructor <init>(Lcom/evernote/clients/NoteStoreClient;Lcom/evernote/clients/NoteStoreClient;Lcom/evernote/edam/userstore/AuthenticationResult;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/evernote/clients/LinkedNoteStoreClient;->mainNoteStoreClient:Lcom/evernote/clients/NoteStoreClient;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/evernote/clients/LinkedNoteStoreClient;->linkedNoteStoreClient:Lcom/evernote/clients/NoteStoreClient;

    .line 7
    .line 8
    iput-object p3, p0, Lcom/evernote/clients/LinkedNoteStoreClient;->authenticationResult:Lcom/evernote/edam/userstore/AuthenticationResult;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method


# virtual methods
.method public createNote(Lcom/evernote/edam/type/Note;Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/edam/type/Note;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/evernote/clients/LinkedNoteStoreClient;->getClient()Lcom/evernote/clients/NoteStoreClient;

    .line 2
    .line 3
    .line 4
    move-result-object p2

    .line 5
    invoke-virtual {p2}, Lcom/evernote/clients/NoteStoreClient;->getSharedNotebookByAuth()Lcom/evernote/edam/type/SharedNotebook;

    .line 6
    .line 7
    .line 8
    move-result-object p2

    .line 9
    invoke-virtual {p2}, Lcom/evernote/edam/type/SharedNotebook;->getNotebookGuid()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object p2

    .line 13
    invoke-virtual {p1, p2}, Lcom/evernote/edam/type/Note;->setNotebookGuid(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/evernote/clients/LinkedNoteStoreClient;->getClient()Lcom/evernote/clients/NoteStoreClient;

    .line 17
    .line 18
    .line 19
    move-result-object p2

    .line 20
    invoke-virtual {p2, p1}, Lcom/evernote/clients/NoteStoreClient;->createNote(Lcom/evernote/edam/type/Note;)Lcom/evernote/edam/type/Note;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    return-object p1
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method getAuthenticationResult()Lcom/evernote/edam/userstore/AuthenticationResult;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/evernote/clients/LinkedNoteStoreClient;->authenticationResult:Lcom/evernote/edam/userstore/AuthenticationResult;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getClient()Lcom/evernote/clients/NoteStoreClient;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/evernote/clients/LinkedNoteStoreClient;->linkedNoteStoreClient:Lcom/evernote/clients/NoteStoreClient;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getCorrespondingNotebook(Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/edam/type/Notebook;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/evernote/clients/LinkedNoteStoreClient;->getClient()Lcom/evernote/clients/NoteStoreClient;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p1}, Lcom/evernote/clients/NoteStoreClient;->getSharedNotebookByAuth()Lcom/evernote/edam/type/SharedNotebook;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    invoke-virtual {p0}, Lcom/evernote/clients/LinkedNoteStoreClient;->getClient()Lcom/evernote/clients/NoteStoreClient;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->getNotebookGuid()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    invoke-virtual {v0, p1}, Lcom/evernote/clients/NoteStoreClient;->getNotebook(Ljava/lang/String;)Lcom/evernote/edam/type/Notebook;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    return-object p1
    .line 22
.end method

.method getPersonalClient()Lcom/evernote/clients/NoteStoreClient;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/evernote/clients/LinkedNoteStoreClient;->mainNoteStoreClient:Lcom/evernote/clients/NoteStoreClient;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method getToken()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/evernote/clients/LinkedNoteStoreClient;->getAuthenticationResult()Lcom/evernote/edam/userstore/AuthenticationResult;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/evernote/edam/userstore/AuthenticationResult;->getAuthenticationToken()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public isNotebookWritable(Lcom/evernote/edam/type/LinkedNotebook;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/evernote/clients/LinkedNoteStoreClient;->getCorrespondingNotebook(Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/edam/type/Notebook;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->getRestrictions()Lcom/evernote/edam/type/NotebookRestrictions;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isNoCreateNotes()Z

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    xor-int/lit8 p1, p1, 0x1

    .line 14
    .line 15
    return p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public listNotebooks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/evernote/clients/LinkedNoteStoreClient;->getPersonalClient()Lcom/evernote/clients/NoteStoreClient;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/evernote/clients/NoteStoreClient;->listLinkedNotebooks()Ljava/util/List;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.class public Lcom/github/happlebubble/BubbleLayout;
.super Landroid/widget/FrameLayout;
.source "BubbleLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/github/happlebubble/BubbleLayout$OnClickEdgeListener;,
        Lcom/github/happlebubble/BubbleLayout$Look;
    }
.end annotation


# instance fields
.field private O0O:I

.field private O88O:I

.field private O8o08O8O:I

.field private OO:Lcom/github/happlebubble/BubbleLayout$Look;

.field private OO〇00〇8oO:I

.field private Oo0〇Ooo:I

.field private Oo80:I

.field private final Ooo08:Landroid/graphics/Rect;

.field private O〇08oOOO0:Landroid/graphics/Bitmap;

.field private O〇o88o08〇:Landroid/graphics/Region;

.field private o0:Landroid/graphics/Paint;

.field private o8o:I

.field private o8oOOo:I

.field private final o8〇OO:Landroid/graphics/RectF;

.field private o8〇OO0〇0o:I

.field private oOO〇〇:I

.field private oOo0:I

.field private oOo〇8o008:I

.field private oo8ooo8O:I

.field private final ooO:Landroid/graphics/Paint;

.field private ooo0〇〇O:I

.field private o〇00O:I

.field private o〇oO:I

.field private 〇00O0:I

.field private 〇080OO8〇0:I

.field private 〇08O〇00〇o:I

.field private 〇08〇o0O:I

.field private 〇0O:I

.field private 〇8〇oO〇〇8o:I

.field private final 〇OO8ooO8〇:Landroid/graphics/Paint;

.field private 〇OOo8〇0:Landroid/graphics/Path;

.field private 〇OO〇00〇0O:I

.field private 〇O〇〇O8:I

.field private 〇o0O:I

.field private 〇〇08O:I

.field private 〇〇o〇:I

.field private 〇〇〇0o〇〇0:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, p2, v0}, Lcom/github/happlebubble/BubbleLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .line 2
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 3
    new-instance v0, Landroid/graphics/Region;

    invoke-direct {v0}, Landroid/graphics/Region;-><init>()V

    iput-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->O〇o88o08〇:Landroid/graphics/Region;

    const/4 v0, -0x1

    .line 4
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇00O0:I

    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->O〇08oOOO0:Landroid/graphics/Bitmap;

    .line 6
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO:Landroid/graphics/RectF;

    .line 7
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->Ooo08:Landroid/graphics/Rect;

    .line 8
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OO8ooO8〇:Landroid/graphics/Paint;

    .line 9
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/github/happlebubble/BubbleLayout;->ooO:Landroid/graphics/Paint;

    const/high16 v3, -0x1000000

    .line 10
    iput v3, p0, Lcom/github/happlebubble/BubbleLayout;->〇OO〇00〇0O:I

    const/4 v3, 0x0

    .line 11
    iput v3, p0, Lcom/github/happlebubble/BubbleLayout;->Oo0〇Ooo:I

    .line 12
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v4, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇〇0o〇〇0:Landroid/graphics/Paint;

    const/4 v4, 0x1

    .line 13
    invoke-virtual {p0, v4, v0}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 14
    invoke-virtual {p0, v3}, Landroid/view/View;->setWillNotDraw(Z)V

    .line 15
    sget-object v0, Lcom/intsig/comm/R$styleable;->BubbleLayout:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/github/happlebubble/BubbleLayout;->〇080(Landroid/content/res/TypedArray;)V

    .line 16
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p1, p0, Lcom/github/happlebubble/BubbleLayout;->o0:Landroid/graphics/Paint;

    .line 17
    sget-object p2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 18
    new-instance p1, Landroid/graphics/Path;

    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    iput-object p1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 19
    new-instance p1, Landroid/graphics/PorterDuffXfermode;

    sget-object p2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {p1, p2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 20
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->〇o〇()V

    return-void
.end method

.method private 〇080(Landroid/content/res/TypedArray;)V
    .locals 6

    .line 1
    sget v0, Lcom/intsig/comm/R$styleable;->BubbleLayout_lookAt:I

    .line 2
    .line 3
    sget-object v1, Lcom/github/happlebubble/BubbleLayout$Look;->BOTTOM:Lcom/github/happlebubble/BubbleLayout$Look;

    .line 4
    .line 5
    iget v1, v1, Lcom/github/happlebubble/BubbleLayout$Look;->o0:I

    .line 6
    .line 7
    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    invoke-static {v0}, Lcom/github/happlebubble/BubbleLayout$Look;->getType(I)Lcom/github/happlebubble/BubbleLayout$Look;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iput-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->OO:Lcom/github/happlebubble/BubbleLayout$Look;

    .line 16
    .line 17
    sget v0, Lcom/intsig/comm/R$styleable;->BubbleLayout_lookPosition:I

    .line 18
    .line 19
    const/4 v1, 0x0

    .line 20
    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->OO〇00〇8oO:I

    .line 25
    .line 26
    sget v0, Lcom/intsig/comm/R$styleable;->BubbleLayout_lookWidth:I

    .line 27
    .line 28
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    const/high16 v3, 0x41500000    # 13.0f

    .line 33
    .line 34
    invoke-static {v2, v3}, Lcom/github/happlebubble/Util;->〇080(Landroid/content/Context;F)I

    .line 35
    .line 36
    .line 37
    move-result v2

    .line 38
    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 43
    .line 44
    sget v0, Lcom/intsig/comm/R$styleable;->BubbleLayout_lookLength:I

    .line 45
    .line 46
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    const/high16 v3, 0x41400000    # 12.0f

    .line 51
    .line 52
    invoke-static {v2, v3}, Lcom/github/happlebubble/Util;->〇080(Landroid/content/Context;F)I

    .line 53
    .line 54
    .line 55
    move-result v2

    .line 56
    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    .line 57
    .line 58
    .line 59
    move-result v0

    .line 60
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 61
    .line 62
    sget v0, Lcom/intsig/comm/R$styleable;->BubbleLayout_shadowRadius:I

    .line 63
    .line 64
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 65
    .line 66
    .line 67
    move-result-object v2

    .line 68
    const v3, 0x40533333    # 3.3f

    .line 69
    .line 70
    .line 71
    invoke-static {v2, v3}, Lcom/github/happlebubble/Util;->〇080(Landroid/content/Context;F)I

    .line 72
    .line 73
    .line 74
    move-result v2

    .line 75
    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    .line 76
    .line 77
    .line 78
    move-result v0

    .line 79
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇08O:I

    .line 80
    .line 81
    sget v0, Lcom/intsig/comm/R$styleable;->BubbleLayout_shadowX:I

    .line 82
    .line 83
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 84
    .line 85
    .line 86
    move-result-object v2

    .line 87
    const/high16 v3, 0x3f800000    # 1.0f

    .line 88
    .line 89
    invoke-static {v2, v3}, Lcom/github/happlebubble/Util;->〇080(Landroid/content/Context;F)I

    .line 90
    .line 91
    .line 92
    move-result v2

    .line 93
    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    .line 94
    .line 95
    .line 96
    move-result v0

    .line 97
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->O0O:I

    .line 98
    .line 99
    sget v0, Lcom/intsig/comm/R$styleable;->BubbleLayout_shadowY:I

    .line 100
    .line 101
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 102
    .line 103
    .line 104
    move-result-object v2

    .line 105
    invoke-static {v2, v3}, Lcom/github/happlebubble/Util;->〇080(Landroid/content/Context;F)I

    .line 106
    .line 107
    .line 108
    move-result v2

    .line 109
    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    .line 110
    .line 111
    .line 112
    move-result v0

    .line 113
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->o8oOOo:I

    .line 114
    .line 115
    sget v0, Lcom/intsig/comm/R$styleable;->BubbleLayout_bubbleRadius:I

    .line 116
    .line 117
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 118
    .line 119
    .line 120
    move-result-object v2

    .line 121
    const/high16 v3, 0x41000000    # 8.0f

    .line 122
    .line 123
    invoke-static {v2, v3}, Lcom/github/happlebubble/Util;->〇080(Landroid/content/Context;F)I

    .line 124
    .line 125
    .line 126
    move-result v2

    .line 127
    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    .line 128
    .line 129
    .line 130
    move-result v0

    .line 131
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇O〇〇O8:I

    .line 132
    .line 133
    sget v0, Lcom/intsig/comm/R$styleable;->BubbleLayout_bubbleLeftTopRadius:I

    .line 134
    .line 135
    const/4 v2, -0x1

    .line 136
    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    .line 137
    .line 138
    .line 139
    move-result v0

    .line 140
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->O88O:I

    .line 141
    .line 142
    sget v0, Lcom/intsig/comm/R$styleable;->BubbleLayout_bubbleRightTopRadius:I

    .line 143
    .line 144
    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    .line 145
    .line 146
    .line 147
    move-result v0

    .line 148
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->oOO〇〇:I

    .line 149
    .line 150
    sget v0, Lcom/intsig/comm/R$styleable;->BubbleLayout_bubbleRightDownRadius:I

    .line 151
    .line 152
    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    .line 153
    .line 154
    .line 155
    move-result v0

    .line 156
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->o8o:I

    .line 157
    .line 158
    sget v0, Lcom/intsig/comm/R$styleable;->BubbleLayout_bubbleLeftDownRadius:I

    .line 159
    .line 160
    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    .line 161
    .line 162
    .line 163
    move-result v0

    .line 164
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->oo8ooo8O:I

    .line 165
    .line 166
    sget v0, Lcom/intsig/comm/R$styleable;->BubbleLayout_bubbleArrowTopLeftRadius:I

    .line 167
    .line 168
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 169
    .line 170
    .line 171
    move-result-object v4

    .line 172
    const/high16 v5, 0x40400000    # 3.0f

    .line 173
    .line 174
    invoke-static {v4, v5}, Lcom/github/happlebubble/Util;->〇080(Landroid/content/Context;F)I

    .line 175
    .line 176
    .line 177
    move-result v4

    .line 178
    invoke-virtual {p1, v0, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    .line 179
    .line 180
    .line 181
    move-result v0

    .line 182
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->o〇oO:I

    .line 183
    .line 184
    sget v0, Lcom/intsig/comm/R$styleable;->BubbleLayout_bubbleArrowTopRightRadius:I

    .line 185
    .line 186
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 187
    .line 188
    .line 189
    move-result-object v4

    .line 190
    invoke-static {v4, v5}, Lcom/github/happlebubble/Util;->〇080(Landroid/content/Context;F)I

    .line 191
    .line 192
    .line 193
    move-result v4

    .line 194
    invoke-virtual {p1, v0, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    .line 195
    .line 196
    .line 197
    move-result v0

    .line 198
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇08〇o0O:I

    .line 199
    .line 200
    sget v0, Lcom/intsig/comm/R$styleable;->BubbleLayout_bubbleArrowDownLeftRadius:I

    .line 201
    .line 202
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 203
    .line 204
    .line 205
    move-result-object v4

    .line 206
    const/high16 v5, 0x40c00000    # 6.0f

    .line 207
    .line 208
    invoke-static {v4, v5}, Lcom/github/happlebubble/Util;->〇080(Landroid/content/Context;F)I

    .line 209
    .line 210
    .line 211
    move-result v4

    .line 212
    invoke-virtual {p1, v0, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    .line 213
    .line 214
    .line 215
    move-result v0

    .line 216
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇o〇:I

    .line 217
    .line 218
    sget v0, Lcom/intsig/comm/R$styleable;->BubbleLayout_bubbleArrowDownRightRadius:I

    .line 219
    .line 220
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 221
    .line 222
    .line 223
    move-result-object v4

    .line 224
    invoke-static {v4, v5}, Lcom/github/happlebubble/Util;->〇080(Landroid/content/Context;F)I

    .line 225
    .line 226
    .line 227
    move-result v4

    .line 228
    invoke-virtual {p1, v0, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    .line 229
    .line 230
    .line 231
    move-result v0

    .line 232
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->Oo80:I

    .line 233
    .line 234
    sget v0, Lcom/intsig/comm/R$styleable;->BubbleLayout_bubblePadding:I

    .line 235
    .line 236
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 237
    .line 238
    .line 239
    move-result-object v4

    .line 240
    invoke-static {v4, v3}, Lcom/github/happlebubble/Util;->〇080(Landroid/content/Context;F)I

    .line 241
    .line 242
    .line 243
    move-result v3

    .line 244
    invoke-virtual {p1, v0, v3}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    .line 245
    .line 246
    .line 247
    move-result v0

    .line 248
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇08O〇00〇o:I

    .line 249
    .line 250
    sget v0, Lcom/intsig/comm/R$styleable;->BubbleLayout_shadowColor:I

    .line 251
    .line 252
    const v3, -0x777778

    .line 253
    .line 254
    .line 255
    invoke-virtual {p1, v0, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    .line 256
    .line 257
    .line 258
    move-result v0

    .line 259
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->ooo0〇〇O:I

    .line 260
    .line 261
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 262
    .line 263
    .line 264
    move-result-object v0

    .line 265
    sget v3, Lcom/intsig/comm/R$color;->cs_color_bg_0:I

    .line 266
    .line 267
    invoke-static {v0, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 268
    .line 269
    .line 270
    move-result v0

    .line 271
    sget v3, Lcom/intsig/comm/R$styleable;->BubbleLayout_bubbleColor:I

    .line 272
    .line 273
    invoke-virtual {p1, v3, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    .line 274
    .line 275
    .line 276
    move-result v0

    .line 277
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇o0O:I

    .line 278
    .line 279
    sget v0, Lcom/intsig/comm/R$styleable;->BubbleLayout_bubbleBgRes:I

    .line 280
    .line 281
    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    .line 282
    .line 283
    .line 284
    move-result v0

    .line 285
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇00O0:I

    .line 286
    .line 287
    if-eq v0, v2, :cond_0

    .line 288
    .line 289
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    .line 290
    .line 291
    .line 292
    move-result-object v0

    .line 293
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇00O0:I

    .line 294
    .line 295
    invoke-static {v0, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    .line 296
    .line 297
    .line 298
    move-result-object v0

    .line 299
    iput-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->O〇08oOOO0:Landroid/graphics/Bitmap;

    .line 300
    .line 301
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 302
    .line 303
    .line 304
    move-result-object v0

    .line 305
    sget v2, Lcom/intsig/comm/R$color;->cs_color_bg_4:I

    .line 306
    .line 307
    invoke-static {v0, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 308
    .line 309
    .line 310
    move-result v0

    .line 311
    sget v2, Lcom/intsig/comm/R$styleable;->BubbleLayout_bubbleBorderColor:I

    .line 312
    .line 313
    invoke-virtual {p1, v2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    .line 314
    .line 315
    .line 316
    move-result v0

    .line 317
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OO〇00〇0O:I

    .line 318
    .line 319
    sget v0, Lcom/intsig/comm/R$styleable;->BubbleLayout_bubbleBorderSize:I

    .line 320
    .line 321
    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    .line 322
    .line 323
    .line 324
    move-result v0

    .line 325
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->Oo0〇Ooo:I

    .line 326
    .line 327
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 328
    .line 329
    .line 330
    return-void
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
.end method

.method private 〇o00〇〇Oo()V
    .locals 12

    .line 1
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->o0:Landroid/graphics/Paint;

    .line 2
    .line 3
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇08O:I

    .line 4
    .line 5
    int-to-float v1, v1

    .line 6
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->O0O:I

    .line 7
    .line 8
    int-to-float v2, v2

    .line 9
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->o8oOOo:I

    .line 10
    .line 11
    int-to-float v3, v3

    .line 12
    iget v4, p0, Lcom/github/happlebubble/BubbleLayout;->ooo0〇〇O:I

    .line 13
    .line 14
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇〇0o〇〇0:Landroid/graphics/Paint;

    .line 18
    .line 19
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OO〇00〇0O:I

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇〇0o〇〇0:Landroid/graphics/Paint;

    .line 25
    .line 26
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->Oo0〇Ooo:I

    .line 27
    .line 28
    int-to-float v1, v1

    .line 29
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 30
    .line 31
    .line 32
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇〇0o〇〇0:Landroid/graphics/Paint;

    .line 33
    .line 34
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 35
    .line 36
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 37
    .line 38
    .line 39
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇08O:I

    .line 40
    .line 41
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->O0O:I

    .line 42
    .line 43
    const/4 v2, 0x0

    .line 44
    if-gez v1, :cond_0

    .line 45
    .line 46
    neg-int v3, v1

    .line 47
    goto :goto_0

    .line 48
    :cond_0
    const/4 v3, 0x0

    .line 49
    :goto_0
    add-int/2addr v3, v0

    .line 50
    iget-object v4, p0, Lcom/github/happlebubble/BubbleLayout;->OO:Lcom/github/happlebubble/BubbleLayout$Look;

    .line 51
    .line 52
    sget-object v5, Lcom/github/happlebubble/BubbleLayout$Look;->LEFT:Lcom/github/happlebubble/BubbleLayout$Look;

    .line 53
    .line 54
    if-ne v4, v5, :cond_1

    .line 55
    .line 56
    iget v5, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 57
    .line 58
    goto :goto_1

    .line 59
    :cond_1
    const/4 v5, 0x0

    .line 60
    :goto_1
    add-int/2addr v3, v5

    .line 61
    iput v3, p0, Lcom/github/happlebubble/BubbleLayout;->〇080OO8〇0:I

    .line 62
    .line 63
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->o8oOOo:I

    .line 64
    .line 65
    if-gez v3, :cond_2

    .line 66
    .line 67
    neg-int v5, v3

    .line 68
    goto :goto_2

    .line 69
    :cond_2
    const/4 v5, 0x0

    .line 70
    :goto_2
    add-int/2addr v5, v0

    .line 71
    sget-object v6, Lcom/github/happlebubble/BubbleLayout$Look;->TOP:Lcom/github/happlebubble/BubbleLayout$Look;

    .line 72
    .line 73
    if-ne v4, v6, :cond_3

    .line 74
    .line 75
    iget v6, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 76
    .line 77
    goto :goto_3

    .line 78
    :cond_3
    const/4 v6, 0x0

    .line 79
    :goto_3
    add-int/2addr v5, v6

    .line 80
    iput v5, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 81
    .line 82
    iget v5, p0, Lcom/github/happlebubble/BubbleLayout;->o〇00O:I

    .line 83
    .line 84
    sub-int/2addr v5, v0

    .line 85
    if-lez v1, :cond_4

    .line 86
    .line 87
    neg-int v1, v1

    .line 88
    goto :goto_4

    .line 89
    :cond_4
    const/4 v1, 0x0

    .line 90
    :goto_4
    add-int/2addr v5, v1

    .line 91
    sget-object v1, Lcom/github/happlebubble/BubbleLayout$Look;->RIGHT:Lcom/github/happlebubble/BubbleLayout$Look;

    .line 92
    .line 93
    if-ne v4, v1, :cond_5

    .line 94
    .line 95
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 96
    .line 97
    goto :goto_5

    .line 98
    :cond_5
    const/4 v1, 0x0

    .line 99
    :goto_5
    sub-int/2addr v5, v1

    .line 100
    iput v5, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 101
    .line 102
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->O8o08O8O:I

    .line 103
    .line 104
    sub-int/2addr v1, v0

    .line 105
    if-lez v3, :cond_6

    .line 106
    .line 107
    neg-int v0, v3

    .line 108
    goto :goto_6

    .line 109
    :cond_6
    const/4 v0, 0x0

    .line 110
    :goto_6
    add-int/2addr v1, v0

    .line 111
    sget-object v0, Lcom/github/happlebubble/BubbleLayout$Look;->BOTTOM:Lcom/github/happlebubble/BubbleLayout$Look;

    .line 112
    .line 113
    if-ne v4, v0, :cond_7

    .line 114
    .line 115
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 116
    .line 117
    :cond_7
    sub-int/2addr v1, v2

    .line 118
    iput v1, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 119
    .line 120
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->o0:Landroid/graphics/Paint;

    .line 121
    .line 122
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇o0O:I

    .line 123
    .line 124
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 125
    .line 126
    .line 127
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 128
    .line 129
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 130
    .line 131
    .line 132
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->OO〇00〇8oO:I

    .line 133
    .line 134
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 135
    .line 136
    add-int/2addr v1, v0

    .line 137
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 138
    .line 139
    if-le v1, v2, :cond_8

    .line 140
    .line 141
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 142
    .line 143
    sub-int v0, v2, v0

    .line 144
    .line 145
    :cond_8
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇08O:I

    .line 146
    .line 147
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    .line 148
    .line 149
    .line 150
    move-result v0

    .line 151
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->OO〇00〇8oO:I

    .line 152
    .line 153
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 154
    .line 155
    add-int/2addr v2, v1

    .line 156
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 157
    .line 158
    if-le v2, v3, :cond_9

    .line 159
    .line 160
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 161
    .line 162
    sub-int v1, v3, v1

    .line 163
    .line 164
    :cond_9
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇08O:I

    .line 165
    .line 166
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    .line 167
    .line 168
    .line 169
    move-result v1

    .line 170
    sget-object v2, Lcom/github/happlebubble/BubbleLayout$1;->〇080:[I

    .line 171
    .line 172
    iget-object v3, p0, Lcom/github/happlebubble/BubbleLayout;->OO:Lcom/github/happlebubble/BubbleLayout$Look;

    .line 173
    .line 174
    invoke-virtual {v3}, Ljava/lang/Enum;->ordinal()I

    .line 175
    .line 176
    .line 177
    move-result v3

    .line 178
    aget v2, v2, v3

    .line 179
    .line 180
    const/4 v3, 0x1

    .line 181
    const/high16 v4, 0x40000000    # 2.0f

    .line 182
    .line 183
    if-eq v2, v3, :cond_16

    .line 184
    .line 185
    const/4 v3, 0x2

    .line 186
    if-eq v2, v3, :cond_12

    .line 187
    .line 188
    const/4 v1, 0x3

    .line 189
    if-eq v2, v1, :cond_e

    .line 190
    .line 191
    const/4 v1, 0x4

    .line 192
    if-eq v2, v1, :cond_a

    .line 193
    .line 194
    goto/16 :goto_b

    .line 195
    .line 196
    :cond_a
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getRTR()I

    .line 197
    .line 198
    .line 199
    move-result v1

    .line 200
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇o〇:I

    .line 201
    .line 202
    add-int/2addr v1, v2

    .line 203
    if-lt v0, v1, :cond_b

    .line 204
    .line 205
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 206
    .line 207
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 208
    .line 209
    int-to-float v3, v3

    .line 210
    sub-int v2, v0, v2

    .line 211
    .line 212
    int-to-float v2, v2

    .line 213
    invoke-virtual {v1, v3, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 214
    .line 215
    .line 216
    iget-object v5, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 217
    .line 218
    const/4 v6, 0x0

    .line 219
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇o〇:I

    .line 220
    .line 221
    int-to-float v7, v1

    .line 222
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 223
    .line 224
    int-to-float v8, v2

    .line 225
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 226
    .line 227
    int-to-float v9, v3

    .line 228
    div-float/2addr v9, v4

    .line 229
    iget v10, p0, Lcom/github/happlebubble/BubbleLayout;->o〇oO:I

    .line 230
    .line 231
    int-to-float v10, v10

    .line 232
    sub-float/2addr v9, v10

    .line 233
    int-to-float v10, v1

    .line 234
    add-float/2addr v9, v10

    .line 235
    int-to-float v10, v2

    .line 236
    int-to-float v2, v3

    .line 237
    div-float/2addr v2, v4

    .line 238
    int-to-float v1, v1

    .line 239
    add-float v11, v2, v1

    .line 240
    .line 241
    invoke-virtual/range {v5 .. v11}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 242
    .line 243
    .line 244
    goto :goto_7

    .line 245
    :cond_b
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 246
    .line 247
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 248
    .line 249
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 250
    .line 251
    add-int/2addr v2, v3

    .line 252
    int-to-float v2, v2

    .line 253
    int-to-float v3, v0

    .line 254
    iget v5, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 255
    .line 256
    int-to-float v5, v5

    .line 257
    div-float/2addr v5, v4

    .line 258
    add-float/2addr v3, v5

    .line 259
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 260
    .line 261
    .line 262
    :goto_7
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 263
    .line 264
    add-int/2addr v1, v0

    .line 265
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 266
    .line 267
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getRDR()I

    .line 268
    .line 269
    .line 270
    move-result v3

    .line 271
    sub-int/2addr v2, v3

    .line 272
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->Oo80:I

    .line 273
    .line 274
    sub-int/2addr v2, v3

    .line 275
    if-ge v1, v2, :cond_c

    .line 276
    .line 277
    iget-object v5, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 278
    .line 279
    const/4 v6, 0x0

    .line 280
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇08〇o0O:I

    .line 281
    .line 282
    int-to-float v7, v1

    .line 283
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 284
    .line 285
    neg-int v2, v1

    .line 286
    int-to-float v8, v2

    .line 287
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 288
    .line 289
    int-to-float v9, v2

    .line 290
    div-float/2addr v9, v4

    .line 291
    neg-int v1, v1

    .line 292
    int-to-float v10, v1

    .line 293
    int-to-float v1, v2

    .line 294
    div-float/2addr v1, v4

    .line 295
    int-to-float v2, v3

    .line 296
    add-float v11, v1, v2

    .line 297
    .line 298
    invoke-virtual/range {v5 .. v11}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 299
    .line 300
    .line 301
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 302
    .line 303
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 304
    .line 305
    int-to-float v2, v2

    .line 306
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 307
    .line 308
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getRDR()I

    .line 309
    .line 310
    .line 311
    move-result v5

    .line 312
    sub-int/2addr v3, v5

    .line 313
    int-to-float v3, v3

    .line 314
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 315
    .line 316
    .line 317
    :cond_c
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 318
    .line 319
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 320
    .line 321
    int-to-float v3, v2

    .line 322
    iget v5, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 323
    .line 324
    int-to-float v5, v5

    .line 325
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getRDR()I

    .line 326
    .line 327
    .line 328
    move-result v6

    .line 329
    sub-int/2addr v2, v6

    .line 330
    int-to-float v2, v2

    .line 331
    iget v6, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 332
    .line 333
    int-to-float v6, v6

    .line 334
    invoke-virtual {v1, v3, v5, v2, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 335
    .line 336
    .line 337
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 338
    .line 339
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇080OO8〇0:I

    .line 340
    .line 341
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getLDR()I

    .line 342
    .line 343
    .line 344
    move-result v3

    .line 345
    add-int/2addr v2, v3

    .line 346
    int-to-float v2, v2

    .line 347
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 348
    .line 349
    int-to-float v3, v3

    .line 350
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 351
    .line 352
    .line 353
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 354
    .line 355
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇080OO8〇0:I

    .line 356
    .line 357
    int-to-float v3, v2

    .line 358
    iget v5, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 359
    .line 360
    int-to-float v6, v5

    .line 361
    int-to-float v2, v2

    .line 362
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getLDR()I

    .line 363
    .line 364
    .line 365
    move-result v7

    .line 366
    sub-int/2addr v5, v7

    .line 367
    int-to-float v5, v5

    .line 368
    invoke-virtual {v1, v3, v6, v2, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 369
    .line 370
    .line 371
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 372
    .line 373
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇080OO8〇0:I

    .line 374
    .line 375
    int-to-float v2, v2

    .line 376
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 377
    .line 378
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getLTR()I

    .line 379
    .line 380
    .line 381
    move-result v5

    .line 382
    add-int/2addr v3, v5

    .line 383
    int-to-float v3, v3

    .line 384
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 385
    .line 386
    .line 387
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 388
    .line 389
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇080OO8〇0:I

    .line 390
    .line 391
    int-to-float v3, v2

    .line 392
    iget v5, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 393
    .line 394
    int-to-float v5, v5

    .line 395
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getLTR()I

    .line 396
    .line 397
    .line 398
    move-result v6

    .line 399
    add-int/2addr v2, v6

    .line 400
    int-to-float v2, v2

    .line 401
    iget v6, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 402
    .line 403
    int-to-float v6, v6

    .line 404
    invoke-virtual {v1, v3, v5, v2, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 405
    .line 406
    .line 407
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 408
    .line 409
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 410
    .line 411
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getRTR()I

    .line 412
    .line 413
    .line 414
    move-result v3

    .line 415
    sub-int/2addr v2, v3

    .line 416
    int-to-float v2, v2

    .line 417
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 418
    .line 419
    int-to-float v3, v3

    .line 420
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 421
    .line 422
    .line 423
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getRTR()I

    .line 424
    .line 425
    .line 426
    move-result v1

    .line 427
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇o〇:I

    .line 428
    .line 429
    add-int/2addr v1, v2

    .line 430
    if-lt v0, v1, :cond_d

    .line 431
    .line 432
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 433
    .line 434
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 435
    .line 436
    int-to-float v2, v1

    .line 437
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 438
    .line 439
    int-to-float v4, v3

    .line 440
    int-to-float v1, v1

    .line 441
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getRTR()I

    .line 442
    .line 443
    .line 444
    move-result v5

    .line 445
    add-int/2addr v3, v5

    .line 446
    int-to-float v3, v3

    .line 447
    invoke-virtual {v0, v2, v4, v1, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 448
    .line 449
    .line 450
    goto/16 :goto_b

    .line 451
    .line 452
    :cond_d
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 453
    .line 454
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 455
    .line 456
    int-to-float v3, v2

    .line 457
    iget v5, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 458
    .line 459
    int-to-float v5, v5

    .line 460
    iget v6, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 461
    .line 462
    add-int/2addr v2, v6

    .line 463
    int-to-float v2, v2

    .line 464
    int-to-float v0, v0

    .line 465
    iget v6, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 466
    .line 467
    int-to-float v6, v6

    .line 468
    div-float/2addr v6, v4

    .line 469
    add-float/2addr v0, v6

    .line 470
    invoke-virtual {v1, v3, v5, v2, v0}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 471
    .line 472
    .line 473
    goto/16 :goto_b

    .line 474
    .line 475
    :cond_e
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getLTR()I

    .line 476
    .line 477
    .line 478
    move-result v1

    .line 479
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->Oo80:I

    .line 480
    .line 481
    add-int/2addr v1, v2

    .line 482
    if-lt v0, v1, :cond_f

    .line 483
    .line 484
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 485
    .line 486
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->〇080OO8〇0:I

    .line 487
    .line 488
    int-to-float v3, v3

    .line 489
    sub-int v2, v0, v2

    .line 490
    .line 491
    int-to-float v2, v2

    .line 492
    invoke-virtual {v1, v3, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 493
    .line 494
    .line 495
    iget-object v5, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 496
    .line 497
    const/4 v6, 0x0

    .line 498
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->Oo80:I

    .line 499
    .line 500
    int-to-float v7, v1

    .line 501
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 502
    .line 503
    neg-int v3, v2

    .line 504
    int-to-float v8, v3

    .line 505
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 506
    .line 507
    int-to-float v9, v3

    .line 508
    div-float/2addr v9, v4

    .line 509
    iget v10, p0, Lcom/github/happlebubble/BubbleLayout;->〇08〇o0O:I

    .line 510
    .line 511
    int-to-float v10, v10

    .line 512
    sub-float/2addr v9, v10

    .line 513
    int-to-float v10, v1

    .line 514
    add-float/2addr v9, v10

    .line 515
    neg-int v2, v2

    .line 516
    int-to-float v10, v2

    .line 517
    int-to-float v2, v3

    .line 518
    div-float/2addr v2, v4

    .line 519
    int-to-float v1, v1

    .line 520
    add-float v11, v2, v1

    .line 521
    .line 522
    invoke-virtual/range {v5 .. v11}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 523
    .line 524
    .line 525
    goto :goto_8

    .line 526
    :cond_f
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 527
    .line 528
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇080OO8〇0:I

    .line 529
    .line 530
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 531
    .line 532
    sub-int/2addr v2, v3

    .line 533
    int-to-float v2, v2

    .line 534
    int-to-float v3, v0

    .line 535
    iget v5, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 536
    .line 537
    int-to-float v5, v5

    .line 538
    div-float/2addr v5, v4

    .line 539
    add-float/2addr v3, v5

    .line 540
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 541
    .line 542
    .line 543
    :goto_8
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 544
    .line 545
    add-int/2addr v1, v0

    .line 546
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 547
    .line 548
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getLDR()I

    .line 549
    .line 550
    .line 551
    move-result v3

    .line 552
    sub-int/2addr v2, v3

    .line 553
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇o〇:I

    .line 554
    .line 555
    sub-int/2addr v2, v3

    .line 556
    if-ge v1, v2, :cond_10

    .line 557
    .line 558
    iget-object v5, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 559
    .line 560
    const/4 v6, 0x0

    .line 561
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->o〇oO:I

    .line 562
    .line 563
    int-to-float v7, v1

    .line 564
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 565
    .line 566
    int-to-float v8, v1

    .line 567
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 568
    .line 569
    int-to-float v9, v2

    .line 570
    div-float/2addr v9, v4

    .line 571
    int-to-float v10, v1

    .line 572
    int-to-float v1, v2

    .line 573
    div-float/2addr v1, v4

    .line 574
    int-to-float v2, v3

    .line 575
    add-float v11, v1, v2

    .line 576
    .line 577
    invoke-virtual/range {v5 .. v11}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 578
    .line 579
    .line 580
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 581
    .line 582
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇080OO8〇0:I

    .line 583
    .line 584
    int-to-float v2, v2

    .line 585
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 586
    .line 587
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getLDR()I

    .line 588
    .line 589
    .line 590
    move-result v5

    .line 591
    sub-int/2addr v3, v5

    .line 592
    int-to-float v3, v3

    .line 593
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 594
    .line 595
    .line 596
    :cond_10
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 597
    .line 598
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇080OO8〇0:I

    .line 599
    .line 600
    int-to-float v3, v2

    .line 601
    iget v5, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 602
    .line 603
    int-to-float v5, v5

    .line 604
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getLDR()I

    .line 605
    .line 606
    .line 607
    move-result v6

    .line 608
    add-int/2addr v2, v6

    .line 609
    int-to-float v2, v2

    .line 610
    iget v6, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 611
    .line 612
    int-to-float v6, v6

    .line 613
    invoke-virtual {v1, v3, v5, v2, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 614
    .line 615
    .line 616
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 617
    .line 618
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 619
    .line 620
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getRDR()I

    .line 621
    .line 622
    .line 623
    move-result v3

    .line 624
    sub-int/2addr v2, v3

    .line 625
    int-to-float v2, v2

    .line 626
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 627
    .line 628
    int-to-float v3, v3

    .line 629
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 630
    .line 631
    .line 632
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 633
    .line 634
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 635
    .line 636
    int-to-float v3, v2

    .line 637
    iget v5, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 638
    .line 639
    int-to-float v6, v5

    .line 640
    int-to-float v2, v2

    .line 641
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getRDR()I

    .line 642
    .line 643
    .line 644
    move-result v7

    .line 645
    sub-int/2addr v5, v7

    .line 646
    int-to-float v5, v5

    .line 647
    invoke-virtual {v1, v3, v6, v2, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 648
    .line 649
    .line 650
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 651
    .line 652
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 653
    .line 654
    int-to-float v2, v2

    .line 655
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 656
    .line 657
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getRTR()I

    .line 658
    .line 659
    .line 660
    move-result v5

    .line 661
    add-int/2addr v3, v5

    .line 662
    int-to-float v3, v3

    .line 663
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 664
    .line 665
    .line 666
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 667
    .line 668
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 669
    .line 670
    int-to-float v3, v2

    .line 671
    iget v5, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 672
    .line 673
    int-to-float v5, v5

    .line 674
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getRTR()I

    .line 675
    .line 676
    .line 677
    move-result v6

    .line 678
    sub-int/2addr v2, v6

    .line 679
    int-to-float v2, v2

    .line 680
    iget v6, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 681
    .line 682
    int-to-float v6, v6

    .line 683
    invoke-virtual {v1, v3, v5, v2, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 684
    .line 685
    .line 686
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 687
    .line 688
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇080OO8〇0:I

    .line 689
    .line 690
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getLTR()I

    .line 691
    .line 692
    .line 693
    move-result v3

    .line 694
    add-int/2addr v2, v3

    .line 695
    int-to-float v2, v2

    .line 696
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 697
    .line 698
    int-to-float v3, v3

    .line 699
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 700
    .line 701
    .line 702
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getLTR()I

    .line 703
    .line 704
    .line 705
    move-result v1

    .line 706
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->Oo80:I

    .line 707
    .line 708
    add-int/2addr v1, v2

    .line 709
    if-lt v0, v1, :cond_11

    .line 710
    .line 711
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 712
    .line 713
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇080OO8〇0:I

    .line 714
    .line 715
    int-to-float v2, v1

    .line 716
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 717
    .line 718
    int-to-float v4, v3

    .line 719
    int-to-float v1, v1

    .line 720
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getLTR()I

    .line 721
    .line 722
    .line 723
    move-result v5

    .line 724
    add-int/2addr v3, v5

    .line 725
    int-to-float v3, v3

    .line 726
    invoke-virtual {v0, v2, v4, v1, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 727
    .line 728
    .line 729
    goto/16 :goto_b

    .line 730
    .line 731
    :cond_11
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 732
    .line 733
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇080OO8〇0:I

    .line 734
    .line 735
    int-to-float v3, v2

    .line 736
    iget v5, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 737
    .line 738
    int-to-float v5, v5

    .line 739
    iget v6, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 740
    .line 741
    sub-int/2addr v2, v6

    .line 742
    int-to-float v2, v2

    .line 743
    int-to-float v0, v0

    .line 744
    iget v6, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 745
    .line 746
    int-to-float v6, v6

    .line 747
    div-float/2addr v6, v4

    .line 748
    add-float/2addr v0, v6

    .line 749
    invoke-virtual {v1, v3, v5, v2, v0}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 750
    .line 751
    .line 752
    goto/16 :goto_b

    .line 753
    .line 754
    :cond_12
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getLTR()I

    .line 755
    .line 756
    .line 757
    move-result v0

    .line 758
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇o〇:I

    .line 759
    .line 760
    add-int/2addr v0, v2

    .line 761
    if-lt v1, v0, :cond_13

    .line 762
    .line 763
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 764
    .line 765
    sub-int v2, v1, v2

    .line 766
    .line 767
    int-to-float v2, v2

    .line 768
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 769
    .line 770
    int-to-float v3, v3

    .line 771
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 772
    .line 773
    .line 774
    iget-object v5, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 775
    .line 776
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇o〇:I

    .line 777
    .line 778
    int-to-float v6, v0

    .line 779
    const/4 v7, 0x0

    .line 780
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 781
    .line 782
    int-to-float v3, v2

    .line 783
    div-float/2addr v3, v4

    .line 784
    iget v8, p0, Lcom/github/happlebubble/BubbleLayout;->o〇oO:I

    .line 785
    .line 786
    int-to-float v8, v8

    .line 787
    sub-float/2addr v3, v8

    .line 788
    int-to-float v8, v0

    .line 789
    add-float/2addr v8, v3

    .line 790
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 791
    .line 792
    neg-int v9, v3

    .line 793
    int-to-float v9, v9

    .line 794
    int-to-float v2, v2

    .line 795
    div-float/2addr v2, v4

    .line 796
    int-to-float v0, v0

    .line 797
    add-float v10, v2, v0

    .line 798
    .line 799
    neg-int v0, v3

    .line 800
    int-to-float v11, v0

    .line 801
    invoke-virtual/range {v5 .. v11}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 802
    .line 803
    .line 804
    goto :goto_9

    .line 805
    :cond_13
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 806
    .line 807
    int-to-float v2, v1

    .line 808
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 809
    .line 810
    int-to-float v3, v3

    .line 811
    div-float/2addr v3, v4

    .line 812
    add-float/2addr v2, v3

    .line 813
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 814
    .line 815
    iget v5, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 816
    .line 817
    sub-int/2addr v3, v5

    .line 818
    int-to-float v3, v3

    .line 819
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 820
    .line 821
    .line 822
    :goto_9
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 823
    .line 824
    add-int/2addr v0, v1

    .line 825
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 826
    .line 827
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getRTR()I

    .line 828
    .line 829
    .line 830
    move-result v3

    .line 831
    sub-int/2addr v2, v3

    .line 832
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->Oo80:I

    .line 833
    .line 834
    sub-int/2addr v2, v3

    .line 835
    if-ge v0, v2, :cond_14

    .line 836
    .line 837
    iget-object v5, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 838
    .line 839
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇08〇o0O:I

    .line 840
    .line 841
    int-to-float v6, v0

    .line 842
    const/4 v7, 0x0

    .line 843
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 844
    .line 845
    int-to-float v2, v0

    .line 846
    div-float v8, v2, v4

    .line 847
    .line 848
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 849
    .line 850
    int-to-float v9, v2

    .line 851
    int-to-float v0, v0

    .line 852
    div-float/2addr v0, v4

    .line 853
    int-to-float v3, v3

    .line 854
    add-float v10, v0, v3

    .line 855
    .line 856
    int-to-float v11, v2

    .line 857
    invoke-virtual/range {v5 .. v11}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 858
    .line 859
    .line 860
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 861
    .line 862
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 863
    .line 864
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getRTR()I

    .line 865
    .line 866
    .line 867
    move-result v3

    .line 868
    sub-int/2addr v2, v3

    .line 869
    int-to-float v2, v2

    .line 870
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 871
    .line 872
    int-to-float v3, v3

    .line 873
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 874
    .line 875
    .line 876
    :cond_14
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 877
    .line 878
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 879
    .line 880
    int-to-float v3, v2

    .line 881
    iget v5, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 882
    .line 883
    int-to-float v6, v5

    .line 884
    int-to-float v2, v2

    .line 885
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getRTR()I

    .line 886
    .line 887
    .line 888
    move-result v7

    .line 889
    add-int/2addr v5, v7

    .line 890
    int-to-float v5, v5

    .line 891
    invoke-virtual {v0, v3, v6, v2, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 892
    .line 893
    .line 894
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 895
    .line 896
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 897
    .line 898
    int-to-float v2, v2

    .line 899
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 900
    .line 901
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getRDR()I

    .line 902
    .line 903
    .line 904
    move-result v5

    .line 905
    sub-int/2addr v3, v5

    .line 906
    int-to-float v3, v3

    .line 907
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 908
    .line 909
    .line 910
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 911
    .line 912
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 913
    .line 914
    int-to-float v3, v2

    .line 915
    iget v5, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 916
    .line 917
    int-to-float v5, v5

    .line 918
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getRDR()I

    .line 919
    .line 920
    .line 921
    move-result v6

    .line 922
    sub-int/2addr v2, v6

    .line 923
    int-to-float v2, v2

    .line 924
    iget v6, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 925
    .line 926
    int-to-float v6, v6

    .line 927
    invoke-virtual {v0, v3, v5, v2, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 928
    .line 929
    .line 930
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 931
    .line 932
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇080OO8〇0:I

    .line 933
    .line 934
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getLDR()I

    .line 935
    .line 936
    .line 937
    move-result v3

    .line 938
    add-int/2addr v2, v3

    .line 939
    int-to-float v2, v2

    .line 940
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 941
    .line 942
    int-to-float v3, v3

    .line 943
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 944
    .line 945
    .line 946
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 947
    .line 948
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇080OO8〇0:I

    .line 949
    .line 950
    int-to-float v3, v2

    .line 951
    iget v5, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 952
    .line 953
    int-to-float v6, v5

    .line 954
    int-to-float v2, v2

    .line 955
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getLDR()I

    .line 956
    .line 957
    .line 958
    move-result v7

    .line 959
    sub-int/2addr v5, v7

    .line 960
    int-to-float v5, v5

    .line 961
    invoke-virtual {v0, v3, v6, v2, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 962
    .line 963
    .line 964
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 965
    .line 966
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇080OO8〇0:I

    .line 967
    .line 968
    int-to-float v2, v2

    .line 969
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 970
    .line 971
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getLTR()I

    .line 972
    .line 973
    .line 974
    move-result v5

    .line 975
    add-int/2addr v3, v5

    .line 976
    int-to-float v3, v3

    .line 977
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 978
    .line 979
    .line 980
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getLTR()I

    .line 981
    .line 982
    .line 983
    move-result v0

    .line 984
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇o〇:I

    .line 985
    .line 986
    add-int/2addr v0, v2

    .line 987
    if-lt v1, v0, :cond_15

    .line 988
    .line 989
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 990
    .line 991
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇080OO8〇0:I

    .line 992
    .line 993
    int-to-float v2, v1

    .line 994
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 995
    .line 996
    int-to-float v3, v3

    .line 997
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getLTR()I

    .line 998
    .line 999
    .line 1000
    move-result v4

    .line 1001
    add-int/2addr v1, v4

    .line 1002
    int-to-float v1, v1

    .line 1003
    iget v4, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 1004
    .line 1005
    int-to-float v4, v4

    .line 1006
    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 1007
    .line 1008
    .line 1009
    goto/16 :goto_b

    .line 1010
    .line 1011
    :cond_15
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 1012
    .line 1013
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇080OO8〇0:I

    .line 1014
    .line 1015
    int-to-float v2, v2

    .line 1016
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 1017
    .line 1018
    int-to-float v5, v3

    .line 1019
    int-to-float v1, v1

    .line 1020
    iget v6, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 1021
    .line 1022
    int-to-float v6, v6

    .line 1023
    div-float/2addr v6, v4

    .line 1024
    add-float/2addr v1, v6

    .line 1025
    iget v4, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 1026
    .line 1027
    sub-int/2addr v3, v4

    .line 1028
    int-to-float v3, v3

    .line 1029
    invoke-virtual {v0, v2, v5, v1, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 1030
    .line 1031
    .line 1032
    goto/16 :goto_b

    .line 1033
    .line 1034
    :cond_16
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getLDR()I

    .line 1035
    .line 1036
    .line 1037
    move-result v0

    .line 1038
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->Oo80:I

    .line 1039
    .line 1040
    add-int/2addr v0, v2

    .line 1041
    if-lt v1, v0, :cond_17

    .line 1042
    .line 1043
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 1044
    .line 1045
    sub-int v2, v1, v2

    .line 1046
    .line 1047
    int-to-float v2, v2

    .line 1048
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 1049
    .line 1050
    int-to-float v3, v3

    .line 1051
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1052
    .line 1053
    .line 1054
    iget-object v5, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 1055
    .line 1056
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->Oo80:I

    .line 1057
    .line 1058
    int-to-float v6, v0

    .line 1059
    const/4 v7, 0x0

    .line 1060
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 1061
    .line 1062
    int-to-float v3, v2

    .line 1063
    div-float/2addr v3, v4

    .line 1064
    iget v8, p0, Lcom/github/happlebubble/BubbleLayout;->〇08〇o0O:I

    .line 1065
    .line 1066
    int-to-float v8, v8

    .line 1067
    sub-float/2addr v3, v8

    .line 1068
    int-to-float v8, v0

    .line 1069
    add-float/2addr v8, v3

    .line 1070
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 1071
    .line 1072
    int-to-float v9, v3

    .line 1073
    int-to-float v2, v2

    .line 1074
    div-float/2addr v2, v4

    .line 1075
    int-to-float v0, v0

    .line 1076
    add-float v10, v2, v0

    .line 1077
    .line 1078
    int-to-float v11, v3

    .line 1079
    invoke-virtual/range {v5 .. v11}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 1080
    .line 1081
    .line 1082
    goto :goto_a

    .line 1083
    :cond_17
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 1084
    .line 1085
    int-to-float v2, v1

    .line 1086
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 1087
    .line 1088
    int-to-float v3, v3

    .line 1089
    div-float/2addr v3, v4

    .line 1090
    add-float/2addr v2, v3

    .line 1091
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 1092
    .line 1093
    iget v5, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 1094
    .line 1095
    add-int/2addr v3, v5

    .line 1096
    int-to-float v3, v3

    .line 1097
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1098
    .line 1099
    .line 1100
    :goto_a
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 1101
    .line 1102
    add-int/2addr v0, v1

    .line 1103
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 1104
    .line 1105
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getRDR()I

    .line 1106
    .line 1107
    .line 1108
    move-result v3

    .line 1109
    sub-int/2addr v2, v3

    .line 1110
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇o〇:I

    .line 1111
    .line 1112
    sub-int/2addr v2, v3

    .line 1113
    if-ge v0, v2, :cond_18

    .line 1114
    .line 1115
    iget-object v5, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 1116
    .line 1117
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->o〇oO:I

    .line 1118
    .line 1119
    int-to-float v6, v0

    .line 1120
    const/4 v7, 0x0

    .line 1121
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 1122
    .line 1123
    int-to-float v2, v0

    .line 1124
    div-float v8, v2, v4

    .line 1125
    .line 1126
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 1127
    .line 1128
    neg-int v9, v2

    .line 1129
    int-to-float v9, v9

    .line 1130
    int-to-float v0, v0

    .line 1131
    div-float/2addr v0, v4

    .line 1132
    int-to-float v3, v3

    .line 1133
    add-float v10, v0, v3

    .line 1134
    .line 1135
    neg-int v0, v2

    .line 1136
    int-to-float v11, v0

    .line 1137
    invoke-virtual/range {v5 .. v11}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 1138
    .line 1139
    .line 1140
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 1141
    .line 1142
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 1143
    .line 1144
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getRDR()I

    .line 1145
    .line 1146
    .line 1147
    move-result v3

    .line 1148
    sub-int/2addr v2, v3

    .line 1149
    int-to-float v2, v2

    .line 1150
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 1151
    .line 1152
    int-to-float v3, v3

    .line 1153
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1154
    .line 1155
    .line 1156
    :cond_18
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 1157
    .line 1158
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 1159
    .line 1160
    int-to-float v3, v2

    .line 1161
    iget v5, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 1162
    .line 1163
    int-to-float v6, v5

    .line 1164
    int-to-float v2, v2

    .line 1165
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getRDR()I

    .line 1166
    .line 1167
    .line 1168
    move-result v7

    .line 1169
    sub-int/2addr v5, v7

    .line 1170
    int-to-float v5, v5

    .line 1171
    invoke-virtual {v0, v3, v6, v2, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 1172
    .line 1173
    .line 1174
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 1175
    .line 1176
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 1177
    .line 1178
    int-to-float v2, v2

    .line 1179
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 1180
    .line 1181
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getRTR()I

    .line 1182
    .line 1183
    .line 1184
    move-result v5

    .line 1185
    add-int/2addr v3, v5

    .line 1186
    int-to-float v3, v3

    .line 1187
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1188
    .line 1189
    .line 1190
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 1191
    .line 1192
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 1193
    .line 1194
    int-to-float v3, v2

    .line 1195
    iget v5, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 1196
    .line 1197
    int-to-float v5, v5

    .line 1198
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getRTR()I

    .line 1199
    .line 1200
    .line 1201
    move-result v6

    .line 1202
    sub-int/2addr v2, v6

    .line 1203
    int-to-float v2, v2

    .line 1204
    iget v6, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 1205
    .line 1206
    int-to-float v6, v6

    .line 1207
    invoke-virtual {v0, v3, v5, v2, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 1208
    .line 1209
    .line 1210
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 1211
    .line 1212
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇080OO8〇0:I

    .line 1213
    .line 1214
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getLTR()I

    .line 1215
    .line 1216
    .line 1217
    move-result v3

    .line 1218
    add-int/2addr v2, v3

    .line 1219
    int-to-float v2, v2

    .line 1220
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 1221
    .line 1222
    int-to-float v3, v3

    .line 1223
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1224
    .line 1225
    .line 1226
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 1227
    .line 1228
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇080OO8〇0:I

    .line 1229
    .line 1230
    int-to-float v3, v2

    .line 1231
    iget v5, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 1232
    .line 1233
    int-to-float v6, v5

    .line 1234
    int-to-float v2, v2

    .line 1235
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getLTR()I

    .line 1236
    .line 1237
    .line 1238
    move-result v7

    .line 1239
    add-int/2addr v5, v7

    .line 1240
    int-to-float v5, v5

    .line 1241
    invoke-virtual {v0, v3, v6, v2, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 1242
    .line 1243
    .line 1244
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 1245
    .line 1246
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇080OO8〇0:I

    .line 1247
    .line 1248
    int-to-float v2, v2

    .line 1249
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 1250
    .line 1251
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getLDR()I

    .line 1252
    .line 1253
    .line 1254
    move-result v5

    .line 1255
    sub-int/2addr v3, v5

    .line 1256
    int-to-float v3, v3

    .line 1257
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1258
    .line 1259
    .line 1260
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getLDR()I

    .line 1261
    .line 1262
    .line 1263
    move-result v0

    .line 1264
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->Oo80:I

    .line 1265
    .line 1266
    add-int/2addr v0, v2

    .line 1267
    if-lt v1, v0, :cond_19

    .line 1268
    .line 1269
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 1270
    .line 1271
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇080OO8〇0:I

    .line 1272
    .line 1273
    int-to-float v2, v1

    .line 1274
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 1275
    .line 1276
    int-to-float v3, v3

    .line 1277
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->getLDR()I

    .line 1278
    .line 1279
    .line 1280
    move-result v4

    .line 1281
    add-int/2addr v1, v4

    .line 1282
    int-to-float v1, v1

    .line 1283
    iget v4, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 1284
    .line 1285
    int-to-float v4, v4

    .line 1286
    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 1287
    .line 1288
    .line 1289
    goto :goto_b

    .line 1290
    :cond_19
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 1291
    .line 1292
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇080OO8〇0:I

    .line 1293
    .line 1294
    int-to-float v2, v2

    .line 1295
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 1296
    .line 1297
    int-to-float v5, v3

    .line 1298
    int-to-float v1, v1

    .line 1299
    iget v6, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 1300
    .line 1301
    int-to-float v6, v6

    .line 1302
    div-float/2addr v6, v4

    .line 1303
    add-float/2addr v1, v6

    .line 1304
    iget v4, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 1305
    .line 1306
    add-int/2addr v3, v4

    .line 1307
    int-to-float v3, v3

    .line 1308
    invoke-virtual {v0, v2, v5, v1, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 1309
    .line 1310
    .line 1311
    :goto_b
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 1312
    .line 1313
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 1314
    .line 1315
    .line 1316
    return-void
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
.end method


# virtual methods
.method public getArrowDownLeftRadius()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇o〇:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getArrowDownRightRadius()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->Oo80:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getArrowTopLeftRadius()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->o〇oO:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getArrowTopRightRadius()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇08〇o0O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getBubbleColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇o0O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getBubbleRadius()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇O〇〇O8:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getLDR()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->oo8ooo8O:I

    .line 2
    .line 3
    const/4 v1, -0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇O〇〇O8:I

    .line 7
    .line 8
    :cond_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getLTR()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->O88O:I

    .line 2
    .line 3
    const/4 v1, -0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇O〇〇O8:I

    .line 7
    .line 8
    :cond_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getLook()Lcom/github/happlebubble/BubbleLayout$Look;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->OO:Lcom/github/happlebubble/BubbleLayout$Look;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getLookLength()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getLookPosition()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->OO〇00〇8oO:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getLookWidth()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getPaint()Landroid/graphics/Paint;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->o0:Landroid/graphics/Paint;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getPath()Landroid/graphics/Path;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getRDR()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->o8o:I

    .line 2
    .line 3
    const/4 v1, -0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇O〇〇O8:I

    .line 7
    .line 8
    :cond_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getRTR()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->oOO〇〇:I

    .line 2
    .line 3
    const/4 v1, -0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇O〇〇O8:I

    .line 7
    .line 8
    :cond_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getShadowColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->ooo0〇〇O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getShadowRadius()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇08O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getShadowX()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->O0O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getShadowY()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->o8oOOo:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public invalidate()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/github/happlebubble/BubbleLayout;->〇o00〇〇Oo()V

    .line 2
    .line 3
    .line 4
    invoke-super {p0}, Landroid/widget/FrameLayout;->invalidate()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .line 1
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 5
    .line 6
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->o0:Landroid/graphics/Paint;

    .line 7
    .line 8
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->O〇08oOOO0:Landroid/graphics/Bitmap;

    .line 12
    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 16
    .line 17
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO:Landroid/graphics/RectF;

    .line 18
    .line 19
    const/4 v2, 0x1

    .line 20
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO:Landroid/graphics/RectF;

    .line 24
    .line 25
    const/4 v1, 0x0

    .line 26
    const/16 v2, 0x1f

    .line 27
    .line 28
    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->saveLayer(Landroid/graphics/RectF;Landroid/graphics/Paint;I)I

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 33
    .line 34
    iget-object v2, p0, Lcom/github/happlebubble/BubbleLayout;->ooO:Landroid/graphics/Paint;

    .line 35
    .line 36
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 37
    .line 38
    .line 39
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO:Landroid/graphics/RectF;

    .line 40
    .line 41
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    .line 42
    .line 43
    .line 44
    move-result v1

    .line 45
    iget-object v2, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO:Landroid/graphics/RectF;

    .line 46
    .line 47
    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    .line 48
    .line 49
    .line 50
    move-result v2

    .line 51
    div-float/2addr v1, v2

    .line 52
    iget-object v2, p0, Lcom/github/happlebubble/BubbleLayout;->O〇08oOOO0:Landroid/graphics/Bitmap;

    .line 53
    .line 54
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    .line 55
    .line 56
    .line 57
    move-result v2

    .line 58
    int-to-float v2, v2

    .line 59
    const/high16 v3, 0x3f800000    # 1.0f

    .line 60
    .line 61
    mul-float v2, v2, v3

    .line 62
    .line 63
    iget-object v3, p0, Lcom/github/happlebubble/BubbleLayout;->O〇08oOOO0:Landroid/graphics/Bitmap;

    .line 64
    .line 65
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    .line 66
    .line 67
    .line 68
    move-result v3

    .line 69
    int-to-float v3, v3

    .line 70
    div-float/2addr v2, v3

    .line 71
    const/4 v3, 0x0

    .line 72
    const/high16 v4, 0x40000000    # 2.0f

    .line 73
    .line 74
    cmpl-float v2, v1, v2

    .line 75
    .line 76
    if-lez v2, :cond_0

    .line 77
    .line 78
    iget-object v2, p0, Lcom/github/happlebubble/BubbleLayout;->O〇08oOOO0:Landroid/graphics/Bitmap;

    .line 79
    .line 80
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    .line 81
    .line 82
    .line 83
    move-result v2

    .line 84
    int-to-float v2, v2

    .line 85
    iget-object v5, p0, Lcom/github/happlebubble/BubbleLayout;->O〇08oOOO0:Landroid/graphics/Bitmap;

    .line 86
    .line 87
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    .line 88
    .line 89
    .line 90
    move-result v5

    .line 91
    int-to-float v5, v5

    .line 92
    div-float/2addr v5, v1

    .line 93
    sub-float/2addr v2, v5

    .line 94
    div-float/2addr v2, v4

    .line 95
    float-to-int v2, v2

    .line 96
    iget-object v4, p0, Lcom/github/happlebubble/BubbleLayout;->O〇08oOOO0:Landroid/graphics/Bitmap;

    .line 97
    .line 98
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    .line 99
    .line 100
    .line 101
    move-result v4

    .line 102
    int-to-float v4, v4

    .line 103
    div-float/2addr v4, v1

    .line 104
    float-to-int v1, v4

    .line 105
    add-int/2addr v1, v2

    .line 106
    iget-object v4, p0, Lcom/github/happlebubble/BubbleLayout;->Ooo08:Landroid/graphics/Rect;

    .line 107
    .line 108
    iget-object v5, p0, Lcom/github/happlebubble/BubbleLayout;->O〇08oOOO0:Landroid/graphics/Bitmap;

    .line 109
    .line 110
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    .line 111
    .line 112
    .line 113
    move-result v5

    .line 114
    invoke-virtual {v4, v3, v2, v5, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 115
    .line 116
    .line 117
    goto :goto_0

    .line 118
    :cond_0
    iget-object v2, p0, Lcom/github/happlebubble/BubbleLayout;->O〇08oOOO0:Landroid/graphics/Bitmap;

    .line 119
    .line 120
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    .line 121
    .line 122
    .line 123
    move-result v2

    .line 124
    int-to-float v2, v2

    .line 125
    iget-object v5, p0, Lcom/github/happlebubble/BubbleLayout;->O〇08oOOO0:Landroid/graphics/Bitmap;

    .line 126
    .line 127
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    .line 128
    .line 129
    .line 130
    move-result v5

    .line 131
    int-to-float v5, v5

    .line 132
    mul-float v5, v5, v1

    .line 133
    .line 134
    sub-float/2addr v2, v5

    .line 135
    div-float/2addr v2, v4

    .line 136
    float-to-int v2, v2

    .line 137
    iget-object v4, p0, Lcom/github/happlebubble/BubbleLayout;->O〇08oOOO0:Landroid/graphics/Bitmap;

    .line 138
    .line 139
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    .line 140
    .line 141
    .line 142
    move-result v4

    .line 143
    int-to-float v4, v4

    .line 144
    mul-float v4, v4, v1

    .line 145
    .line 146
    float-to-int v1, v4

    .line 147
    add-int/2addr v1, v2

    .line 148
    iget-object v4, p0, Lcom/github/happlebubble/BubbleLayout;->Ooo08:Landroid/graphics/Rect;

    .line 149
    .line 150
    iget-object v5, p0, Lcom/github/happlebubble/BubbleLayout;->O〇08oOOO0:Landroid/graphics/Bitmap;

    .line 151
    .line 152
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    .line 153
    .line 154
    .line 155
    move-result v5

    .line 156
    invoke-virtual {v4, v2, v3, v1, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 157
    .line 158
    .line 159
    :goto_0
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->O〇08oOOO0:Landroid/graphics/Bitmap;

    .line 160
    .line 161
    iget-object v2, p0, Lcom/github/happlebubble/BubbleLayout;->Ooo08:Landroid/graphics/Rect;

    .line 162
    .line 163
    iget-object v3, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO:Landroid/graphics/RectF;

    .line 164
    .line 165
    iget-object v4, p0, Lcom/github/happlebubble/BubbleLayout;->〇OO8ooO8〇:Landroid/graphics/Paint;

    .line 166
    .line 167
    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 168
    .line 169
    .line 170
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 171
    .line 172
    .line 173
    :cond_1
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->Oo0〇Ooo:I

    .line 174
    .line 175
    if-eqz v0, :cond_2

    .line 176
    .line 177
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 178
    .line 179
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇〇0o〇〇0:Landroid/graphics/Paint;

    .line 180
    .line 181
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 182
    .line 183
    .line 184
    :cond_2
    return-void
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .line 1
    instance-of v0, p1, Landroid/os/Bundle;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    check-cast p1, Landroid/os/Bundle;

    .line 6
    .line 7
    const-string v0, "mLookPosition"

    .line 8
    .line 9
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->OO〇00〇8oO:I

    .line 14
    .line 15
    const-string v0, "mLookWidth"

    .line 16
    .line 17
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 22
    .line 23
    const-string v0, "mLookLength"

    .line 24
    .line 25
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 30
    .line 31
    const-string v0, "mShadowColor"

    .line 32
    .line 33
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->ooo0〇〇O:I

    .line 38
    .line 39
    const-string v0, "mShadowRadius"

    .line 40
    .line 41
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇08O:I

    .line 46
    .line 47
    const-string v0, "mShadowX"

    .line 48
    .line 49
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->O0O:I

    .line 54
    .line 55
    const-string v0, "mShadowY"

    .line 56
    .line 57
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->o8oOOo:I

    .line 62
    .line 63
    const-string v0, "mBubbleRadius"

    .line 64
    .line 65
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 66
    .line 67
    .line 68
    move-result v0

    .line 69
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇O〇〇O8:I

    .line 70
    .line 71
    const-string v0, "mLTR"

    .line 72
    .line 73
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 74
    .line 75
    .line 76
    move-result v0

    .line 77
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->O88O:I

    .line 78
    .line 79
    const-string v0, "mRTR"

    .line 80
    .line 81
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 82
    .line 83
    .line 84
    move-result v0

    .line 85
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->oOO〇〇:I

    .line 86
    .line 87
    const-string v0, "mRDR"

    .line 88
    .line 89
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 90
    .line 91
    .line 92
    move-result v0

    .line 93
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->o8o:I

    .line 94
    .line 95
    const-string v0, "mLDR"

    .line 96
    .line 97
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 98
    .line 99
    .line 100
    move-result v0

    .line 101
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->oo8ooo8O:I

    .line 102
    .line 103
    const-string v0, "mBubblePadding"

    .line 104
    .line 105
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 106
    .line 107
    .line 108
    move-result v0

    .line 109
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇08O〇00〇o:I

    .line 110
    .line 111
    const-string v0, "mArrowTopLeftRadius"

    .line 112
    .line 113
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 114
    .line 115
    .line 116
    move-result v0

    .line 117
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->o〇oO:I

    .line 118
    .line 119
    const-string v0, "mArrowTopRightRadius"

    .line 120
    .line 121
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 122
    .line 123
    .line 124
    move-result v0

    .line 125
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇08〇o0O:I

    .line 126
    .line 127
    const-string v0, "mArrowDownLeftRadius"

    .line 128
    .line 129
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 130
    .line 131
    .line 132
    move-result v0

    .line 133
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇o〇:I

    .line 134
    .line 135
    const-string v0, "mArrowDownRightRadius"

    .line 136
    .line 137
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 138
    .line 139
    .line 140
    move-result v0

    .line 141
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->Oo80:I

    .line 142
    .line 143
    const-string v0, "mWidth"

    .line 144
    .line 145
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 146
    .line 147
    .line 148
    move-result v0

    .line 149
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->o〇00O:I

    .line 150
    .line 151
    const-string v0, "mHeight"

    .line 152
    .line 153
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 154
    .line 155
    .line 156
    move-result v0

    .line 157
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->O8o08O8O:I

    .line 158
    .line 159
    const-string v0, "mLeft"

    .line 160
    .line 161
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 162
    .line 163
    .line 164
    move-result v0

    .line 165
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇080OO8〇0:I

    .line 166
    .line 167
    const-string v0, "mTop"

    .line 168
    .line 169
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 170
    .line 171
    .line 172
    move-result v0

    .line 173
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 174
    .line 175
    const-string v0, "mRight"

    .line 176
    .line 177
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 178
    .line 179
    .line 180
    move-result v0

    .line 181
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 182
    .line 183
    const-string v0, "mBottom"

    .line 184
    .line 185
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 186
    .line 187
    .line 188
    move-result v0

    .line 189
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 190
    .line 191
    const-string v0, "mBubbleBgRes"

    .line 192
    .line 193
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 194
    .line 195
    .line 196
    move-result v0

    .line 197
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇00O0:I

    .line 198
    .line 199
    const/4 v1, -0x1

    .line 200
    if-eq v0, v1, :cond_0

    .line 201
    .line 202
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    .line 203
    .line 204
    .line 205
    move-result-object v0

    .line 206
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇00O0:I

    .line 207
    .line 208
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    .line 209
    .line 210
    .line 211
    move-result-object v0

    .line 212
    iput-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->O〇08oOOO0:Landroid/graphics/Bitmap;

    .line 213
    .line 214
    :cond_0
    const-string v0, "mBubbleBorderSize"

    .line 215
    .line 216
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 217
    .line 218
    .line 219
    move-result v0

    .line 220
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->Oo0〇Ooo:I

    .line 221
    .line 222
    const-string v0, "mBubbleBorderColor"

    .line 223
    .line 224
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 225
    .line 226
    .line 227
    move-result v0

    .line 228
    iput v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇OO〇00〇0O:I

    .line 229
    .line 230
    const-string v0, "instanceState"

    .line 231
    .line 232
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 233
    .line 234
    .line 235
    move-result-object p1

    .line 236
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 237
    .line 238
    .line 239
    return-void

    .line 240
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 241
    .line 242
    .line 243
    return-void
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .line 1
    new-instance v0, Landroid/os/Bundle;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "instanceState"

    .line 7
    .line 8
    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 13
    .line 14
    .line 15
    const-string v1, "mLookPosition"

    .line 16
    .line 17
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->OO〇00〇8oO:I

    .line 18
    .line 19
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 20
    .line 21
    .line 22
    const-string v1, "mLookWidth"

    .line 23
    .line 24
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 25
    .line 26
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 27
    .line 28
    .line 29
    const-string v1, "mLookLength"

    .line 30
    .line 31
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 32
    .line 33
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 34
    .line 35
    .line 36
    const-string v1, "mShadowColor"

    .line 37
    .line 38
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->ooo0〇〇O:I

    .line 39
    .line 40
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 41
    .line 42
    .line 43
    const-string v1, "mShadowRadius"

    .line 44
    .line 45
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇08O:I

    .line 46
    .line 47
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 48
    .line 49
    .line 50
    const-string v1, "mShadowX"

    .line 51
    .line 52
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->O0O:I

    .line 53
    .line 54
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 55
    .line 56
    .line 57
    const-string v1, "mShadowY"

    .line 58
    .line 59
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->o8oOOo:I

    .line 60
    .line 61
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 62
    .line 63
    .line 64
    const-string v1, "mBubbleRadius"

    .line 65
    .line 66
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇O〇〇O8:I

    .line 67
    .line 68
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 69
    .line 70
    .line 71
    const-string v1, "mLTR"

    .line 72
    .line 73
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->O88O:I

    .line 74
    .line 75
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 76
    .line 77
    .line 78
    const-string v1, "mRTR"

    .line 79
    .line 80
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOO〇〇:I

    .line 81
    .line 82
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 83
    .line 84
    .line 85
    const-string v1, "mRDR"

    .line 86
    .line 87
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->o8o:I

    .line 88
    .line 89
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 90
    .line 91
    .line 92
    const-string v1, "mLDR"

    .line 93
    .line 94
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oo8ooo8O:I

    .line 95
    .line 96
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 97
    .line 98
    .line 99
    const-string v1, "mBubblePadding"

    .line 100
    .line 101
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇08O〇00〇o:I

    .line 102
    .line 103
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 104
    .line 105
    .line 106
    const-string v1, "mArrowTopLeftRadius"

    .line 107
    .line 108
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->o〇oO:I

    .line 109
    .line 110
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 111
    .line 112
    .line 113
    const-string v1, "mArrowTopRightRadius"

    .line 114
    .line 115
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇08〇o0O:I

    .line 116
    .line 117
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 118
    .line 119
    .line 120
    const-string v1, "mArrowDownLeftRadius"

    .line 121
    .line 122
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇o〇:I

    .line 123
    .line 124
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 125
    .line 126
    .line 127
    const-string v1, "mArrowDownRightRadius"

    .line 128
    .line 129
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->Oo80:I

    .line 130
    .line 131
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 132
    .line 133
    .line 134
    const-string v1, "mWidth"

    .line 135
    .line 136
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->o〇00O:I

    .line 137
    .line 138
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 139
    .line 140
    .line 141
    const-string v1, "mHeight"

    .line 142
    .line 143
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->O8o08O8O:I

    .line 144
    .line 145
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 146
    .line 147
    .line 148
    const-string v1, "mLeft"

    .line 149
    .line 150
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇080OO8〇0:I

    .line 151
    .line 152
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 153
    .line 154
    .line 155
    const-string v1, "mTop"

    .line 156
    .line 157
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇0O:I

    .line 158
    .line 159
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 160
    .line 161
    .line 162
    const-string v1, "mRight"

    .line 163
    .line 164
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOo〇8o008:I

    .line 165
    .line 166
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 167
    .line 168
    .line 169
    const-string v1, "mBottom"

    .line 170
    .line 171
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->oOo0:I

    .line 172
    .line 173
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 174
    .line 175
    .line 176
    const-string v1, "mBubbleBgRes"

    .line 177
    .line 178
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇00O0:I

    .line 179
    .line 180
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 181
    .line 182
    .line 183
    const-string v1, "mBubbleBorderColor"

    .line 184
    .line 185
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇OO〇00〇0O:I

    .line 186
    .line 187
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 188
    .line 189
    .line 190
    const-string v1, "mBubbleBorderSize"

    .line 191
    .line 192
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->Oo0〇Ooo:I

    .line 193
    .line 194
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 195
    .line 196
    .line 197
    return-object v0
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 2
    .line 3
    .line 4
    iput p1, p0, Lcom/github/happlebubble/BubbleLayout;->o〇00O:I

    .line 5
    .line 6
    iput p2, p0, Lcom/github/happlebubble/BubbleLayout;->O8o08O8O:I

    .line 7
    .line 8
    invoke-direct {p0}, Lcom/github/happlebubble/BubbleLayout;->〇o00〇〇Oo()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .line 1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    new-instance v0, Landroid/graphics/RectF;

    .line 8
    .line 9
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 10
    .line 11
    .line 12
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 13
    .line 14
    const/4 v2, 0x1

    .line 15
    invoke-virtual {v1, v0, v2}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 16
    .line 17
    .line 18
    iget-object v1, p0, Lcom/github/happlebubble/BubbleLayout;->O〇o88o08〇:Landroid/graphics/Region;

    .line 19
    .line 20
    iget-object v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇OOo8〇0:Landroid/graphics/Path;

    .line 21
    .line 22
    new-instance v3, Landroid/graphics/Region;

    .line 23
    .line 24
    iget v4, v0, Landroid/graphics/RectF;->left:F

    .line 25
    .line 26
    float-to-int v4, v4

    .line 27
    iget v5, v0, Landroid/graphics/RectF;->top:F

    .line 28
    .line 29
    float-to-int v5, v5

    .line 30
    iget v6, v0, Landroid/graphics/RectF;->right:F

    .line 31
    .line 32
    float-to-int v6, v6

    .line 33
    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    .line 34
    .line 35
    float-to-int v0, v0

    .line 36
    invoke-direct {v3, v4, v5, v6, v0}, Landroid/graphics/Region;-><init>(IIII)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Region;->setPath(Landroid/graphics/Path;Landroid/graphics/Region;)Z

    .line 40
    .line 41
    .line 42
    iget-object v0, p0, Lcom/github/happlebubble/BubbleLayout;->O〇o88o08〇:Landroid/graphics/Region;

    .line 43
    .line 44
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 45
    .line 46
    .line 47
    move-result v1

    .line 48
    float-to-int v1, v1

    .line 49
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 50
    .line 51
    .line 52
    move-result v2

    .line 53
    float-to-int v2, v2

    .line 54
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Region;->contains(II)Z

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 59
    .line 60
    .line 61
    move-result p1

    .line 62
    return p1
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public postInvalidate()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/github/happlebubble/BubbleLayout;->〇o00〇〇Oo()V

    .line 2
    .line 3
    .line 4
    invoke-super {p0}, Landroid/widget/FrameLayout;->postInvalidate()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public setArrowDownLeftRadius(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇o〇:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setArrowDownRightRadius(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/github/happlebubble/BubbleLayout;->Oo80:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setArrowTopLeftRadius(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/github/happlebubble/BubbleLayout;->o〇oO:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setArrowTopRightRadius(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/github/happlebubble/BubbleLayout;->〇08〇o0O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setBubbleBorderColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/github/happlebubble/BubbleLayout;->〇OO〇00〇0O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setBubbleBorderSize(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/github/happlebubble/BubbleLayout;->Oo0〇Ooo:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setBubbleColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/github/happlebubble/BubbleLayout;->〇o0O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setBubbleImageBg(Landroid/graphics/Bitmap;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/github/happlebubble/BubbleLayout;->O〇08oOOO0:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setBubbleImageBgRes(I)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    iput-object p1, p0, Lcom/github/happlebubble/BubbleLayout;->O〇08oOOO0:Landroid/graphics/Bitmap;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setBubblePadding(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/github/happlebubble/BubbleLayout;->〇08O〇00〇o:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setBubbleRadius(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/github/happlebubble/BubbleLayout;->〇O〇〇O8:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setLDR(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/github/happlebubble/BubbleLayout;->oo8ooo8O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setLTR(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/github/happlebubble/BubbleLayout;->O88O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setLook(Lcom/github/happlebubble/BubbleLayout$Look;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/github/happlebubble/BubbleLayout;->OO:Lcom/github/happlebubble/BubbleLayout$Look;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->〇o〇()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setLookLength(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/github/happlebubble/BubbleLayout;->〇o〇()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setLookPosition(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/github/happlebubble/BubbleLayout;->OO〇00〇8oO:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setLookWidth(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/github/happlebubble/BubbleLayout;->o8〇OO0〇0o:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setOnClickEdgeListener(Lcom/github/happlebubble/BubbleLayout$OnClickEdgeListener;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setRDR(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/github/happlebubble/BubbleLayout;->o8o:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setRTR(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/github/happlebubble/BubbleLayout;->oOO〇〇:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setShadowColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/github/happlebubble/BubbleLayout;->ooo0〇〇O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setShadowRadius(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇08O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setShadowX(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/github/happlebubble/BubbleLayout;->O0O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setShadowY(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/github/happlebubble/BubbleLayout;->o8oOOo:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public 〇o〇()V
    .locals 4

    .line 1
    iget v0, p0, Lcom/github/happlebubble/BubbleLayout;->〇08O〇00〇o:I

    .line 2
    .line 3
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇〇08O:I

    .line 4
    .line 5
    add-int/2addr v0, v1

    .line 6
    sget-object v1, Lcom/github/happlebubble/BubbleLayout$1;->〇080:[I

    .line 7
    .line 8
    iget-object v2, p0, Lcom/github/happlebubble/BubbleLayout;->OO:Lcom/github/happlebubble/BubbleLayout$Look;

    .line 9
    .line 10
    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    aget v1, v1, v2

    .line 15
    .line 16
    const/4 v2, 0x1

    .line 17
    if-eq v1, v2, :cond_3

    .line 18
    .line 19
    const/4 v2, 0x2

    .line 20
    if-eq v1, v2, :cond_2

    .line 21
    .line 22
    const/4 v2, 0x3

    .line 23
    if-eq v1, v2, :cond_1

    .line 24
    .line 25
    const/4 v2, 0x4

    .line 26
    if-eq v1, v2, :cond_0

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 30
    .line 31
    add-int/2addr v1, v0

    .line 32
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->O0O:I

    .line 33
    .line 34
    add-int/2addr v1, v2

    .line 35
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->o8oOOo:I

    .line 36
    .line 37
    add-int/2addr v2, v0

    .line 38
    invoke-virtual {p0, v0, v0, v1, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_1
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 43
    .line 44
    add-int/2addr v1, v0

    .line 45
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->O0O:I

    .line 46
    .line 47
    add-int/2addr v2, v0

    .line 48
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->o8oOOo:I

    .line 49
    .line 50
    add-int/2addr v3, v0

    .line 51
    invoke-virtual {p0, v1, v0, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 52
    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_2
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 56
    .line 57
    add-int/2addr v1, v0

    .line 58
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->O0O:I

    .line 59
    .line 60
    add-int/2addr v2, v0

    .line 61
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->o8oOOo:I

    .line 62
    .line 63
    add-int/2addr v3, v0

    .line 64
    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 65
    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_3
    iget v1, p0, Lcom/github/happlebubble/BubbleLayout;->O0O:I

    .line 69
    .line 70
    add-int/2addr v1, v0

    .line 71
    iget v2, p0, Lcom/github/happlebubble/BubbleLayout;->〇8〇oO〇〇8o:I

    .line 72
    .line 73
    add-int/2addr v2, v0

    .line 74
    iget v3, p0, Lcom/github/happlebubble/BubbleLayout;->o8oOOo:I

    .line 75
    .line 76
    add-int/2addr v2, v3

    .line 77
    invoke-virtual {p0, v0, v0, v1, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 78
    .line 79
    .line 80
    :goto_0
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

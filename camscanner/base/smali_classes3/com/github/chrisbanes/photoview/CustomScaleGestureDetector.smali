.class public Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;
.super Ljava/lang/Object;
.source "CustomScaleGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector$OnScaleGestureListener;
    }
.end annotation


# static fields
.field private static o〇O8〇〇o:F = 0.5f


# instance fields
.field private O8:F

.field private OO0o〇〇:F

.field private OO0o〇〇〇〇0:F

.field private Oo08:Z

.field private OoO8:F

.field private Oooo8o0〇:J

.field private o800o8O:F

.field private oO80:F

.field private oo88o8O:Landroid/view/GestureDetector;

.field private o〇0:Z

.field private final 〇080:Landroid/content/Context;

.field private final 〇0〇O0088o:Landroid/os/Handler;

.field private 〇80〇808〇O:F

.field private 〇8o8o〇:F

.field private 〇O00:I

.field private 〇O888o0o:I

.field private 〇O8o08O:F

.field private 〇O〇:Z

.field private final 〇o00〇〇Oo:Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector$OnScaleGestureListener;

.field private 〇oo〇:Z

.field private 〇o〇:F

.field private 〇〇808〇:J

.field private 〇〇888:F

.field private 〇〇8O0〇8:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector$OnScaleGestureListener;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, p2, v0}, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;-><init>(Landroid/content/Context;Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector$OnScaleGestureListener;Landroid/os/Handler;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector$OnScaleGestureListener;Landroid/os/Handler;)V
    .locals 2

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 3
    iput v0, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O888o0o:I

    .line 4
    iput-object p1, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇080:Landroid/content/Context;

    .line 5
    iput-object p2, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇o00〇〇Oo:Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector$OnScaleGestureListener;

    .line 6
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object p2

    .line 7
    invoke-virtual {p2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O00:I

    .line 8
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1d

    if-lt v0, v1, :cond_0

    .line 9
    invoke-static {p2}, L〇〇808〇/〇080;->〇080(Landroid/view/ViewConfiguration;)I

    move-result p2

    iput p2, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇〇8O0〇8:I

    .line 10
    :cond_0
    iput-object p3, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇0〇O0088o:Landroid/os/Handler;

    .line 11
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object p1

    iget p1, p1, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 p2, 0x12

    const/4 p3, 0x1

    if-le p1, p2, :cond_1

    .line 12
    invoke-virtual {p0, p3}, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->OO0o〇〇〇〇0(Z)V

    :cond_1
    const/16 p2, 0x16

    if-le p1, p2, :cond_2

    .line 13
    invoke-virtual {p0, p3}, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O8o08O(Z)V

    :cond_2
    return-void
.end method

.method static bridge synthetic 〇080(Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O888o0o:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->OoO8:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method static bridge synthetic 〇o〇(Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->o800o8O:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private 〇〇888()Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O888o0o:I

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v0, 0x0

    .line 8
    :goto_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method


# virtual methods
.method public O8()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇o〇:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public OO0o〇〇〇〇0(Z)V
    .locals 3

    .line 1
    iput-boolean p1, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->Oo08:Z

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    iget-object p1, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->oo88o8O:Landroid/view/GestureDetector;

    .line 6
    .line 7
    if-nez p1, :cond_0

    .line 8
    .line 9
    new-instance p1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector$1;

    .line 10
    .line 11
    invoke-direct {p1, p0}, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector$1;-><init>(Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;)V

    .line 12
    .line 13
    .line 14
    new-instance v0, Landroid/view/GestureDetector;

    .line 15
    .line 16
    iget-object v1, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇080:Landroid/content/Context;

    .line 17
    .line 18
    iget-object v2, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇0〇O0088o:Landroid/os/Handler;

    .line 19
    .line 20
    invoke-direct {v0, v1, p1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->oo88o8O:Landroid/view/GestureDetector;

    .line 24
    .line 25
    :cond_0
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public Oo08()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->O8:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public oO80()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O〇:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public o〇0()F
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇〇888()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/high16 v1, 0x3f800000    # 1.0f

    .line 6
    .line 7
    if-eqz v0, :cond_5

    .line 8
    .line 9
    iget-boolean v0, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇oo〇:Z

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    iget v2, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇〇888:F

    .line 14
    .line 15
    iget v3, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->oO80:F

    .line 16
    .line 17
    cmpg-float v2, v2, v3

    .line 18
    .line 19
    if-ltz v2, :cond_1

    .line 20
    .line 21
    :cond_0
    if-nez v0, :cond_2

    .line 22
    .line 23
    iget v0, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇〇888:F

    .line 24
    .line 25
    iget v2, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->oO80:F

    .line 26
    .line 27
    cmpl-float v0, v0, v2

    .line 28
    .line 29
    if-lez v0, :cond_2

    .line 30
    .line 31
    :cond_1
    const/4 v0, 0x1

    .line 32
    goto :goto_0

    .line 33
    :cond_2
    const/4 v0, 0x0

    .line 34
    :goto_0
    iget v2, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇〇888:F

    .line 35
    .line 36
    iget v3, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->oO80:F

    .line 37
    .line 38
    div-float/2addr v2, v3

    .line 39
    sub-float v2, v1, v2

    .line 40
    .line 41
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    .line 42
    .line 43
    .line 44
    move-result v2

    .line 45
    sget v3, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->o〇O8〇〇o:F

    .line 46
    .line 47
    mul-float v2, v2, v3

    .line 48
    .line 49
    iget v3, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->oO80:F

    .line 50
    .line 51
    iget v4, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O00:I

    .line 52
    .line 53
    int-to-float v4, v4

    .line 54
    cmpg-float v3, v3, v4

    .line 55
    .line 56
    if-gtz v3, :cond_3

    .line 57
    .line 58
    goto :goto_1

    .line 59
    :cond_3
    if-eqz v0, :cond_4

    .line 60
    .line 61
    add-float/2addr v1, v2

    .line 62
    goto :goto_1

    .line 63
    :cond_4
    sub-float/2addr v1, v2

    .line 64
    :goto_1
    return v1

    .line 65
    :cond_5
    iget v0, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->oO80:F

    .line 66
    .line 67
    const/4 v2, 0x0

    .line 68
    cmpl-float v2, v0, v2

    .line 69
    .line 70
    if-lez v2, :cond_6

    .line 71
    .line 72
    iget v1, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇〇888:F

    .line 73
    .line 74
    div-float/2addr v1, v0

    .line 75
    :cond_6
    return v1
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public 〇80〇808〇O(Landroid/view/MotionEvent;)Z
    .locals 17

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    move-object/from16 v0, p1

    .line 4
    .line 5
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    .line 6
    .line 7
    .line 8
    move-result-wide v2

    .line 9
    iput-wide v2, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->Oooo8o0〇:J

    .line 10
    .line 11
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    iget-boolean v3, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->Oo08:Z

    .line 16
    .line 17
    if-eqz v3, :cond_0

    .line 18
    .line 19
    iget-object v3, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->oo88o8O:Landroid/view/GestureDetector;

    .line 20
    .line 21
    invoke-virtual {v3, v0}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 22
    .line 23
    .line 24
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    .line 25
    .line 26
    .line 27
    move-result v3

    .line 28
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getButtonState()I

    .line 29
    .line 30
    .line 31
    move-result v4

    .line 32
    and-int/lit8 v4, v4, 0x20

    .line 33
    .line 34
    const/4 v5, 0x1

    .line 35
    const/4 v6, 0x0

    .line 36
    if-eqz v4, :cond_1

    .line 37
    .line 38
    const/4 v4, 0x1

    .line 39
    goto :goto_0

    .line 40
    :cond_1
    const/4 v4, 0x0

    .line 41
    :goto_0
    iget v7, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O888o0o:I

    .line 42
    .line 43
    const/4 v8, 0x2

    .line 44
    if-ne v7, v8, :cond_2

    .line 45
    .line 46
    if-nez v4, :cond_2

    .line 47
    .line 48
    const/4 v7, 0x1

    .line 49
    goto :goto_1

    .line 50
    :cond_2
    const/4 v7, 0x0

    .line 51
    :goto_1
    if-eq v2, v5, :cond_4

    .line 52
    .line 53
    const/4 v9, 0x3

    .line 54
    if-eq v2, v9, :cond_4

    .line 55
    .line 56
    if-eqz v7, :cond_3

    .line 57
    .line 58
    goto :goto_2

    .line 59
    :cond_3
    const/4 v9, 0x0

    .line 60
    goto :goto_3

    .line 61
    :cond_4
    :goto_2
    const/4 v9, 0x1

    .line 62
    :goto_3
    const/4 v10, 0x0

    .line 63
    if-eqz v2, :cond_5

    .line 64
    .line 65
    if-eqz v9, :cond_8

    .line 66
    .line 67
    :cond_5
    iget-boolean v11, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O〇:Z

    .line 68
    .line 69
    if-eqz v11, :cond_6

    .line 70
    .line 71
    iget-object v11, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇o00〇〇Oo:Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector$OnScaleGestureListener;

    .line 72
    .line 73
    invoke-interface {v11, v1}, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector$OnScaleGestureListener;->〇o00〇〇Oo(Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;)V

    .line 74
    .line 75
    .line 76
    iput-boolean v6, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O〇:Z

    .line 77
    .line 78
    iput v10, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇80〇808〇O:F

    .line 79
    .line 80
    iput v6, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O888o0o:I

    .line 81
    .line 82
    goto :goto_4

    .line 83
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇〇888()Z

    .line 84
    .line 85
    .line 86
    move-result v11

    .line 87
    if-eqz v11, :cond_7

    .line 88
    .line 89
    if-eqz v9, :cond_7

    .line 90
    .line 91
    iput-boolean v6, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O〇:Z

    .line 92
    .line 93
    iput v10, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇80〇808〇O:F

    .line 94
    .line 95
    iput v6, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O888o0o:I

    .line 96
    .line 97
    :cond_7
    :goto_4
    if-eqz v9, :cond_8

    .line 98
    .line 99
    return v5

    .line 100
    :cond_8
    iget-boolean v11, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O〇:Z

    .line 101
    .line 102
    if-nez v11, :cond_9

    .line 103
    .line 104
    iget-boolean v11, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->o〇0:Z

    .line 105
    .line 106
    if-eqz v11, :cond_9

    .line 107
    .line 108
    invoke-direct/range {p0 .. p0}, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇〇888()Z

    .line 109
    .line 110
    .line 111
    move-result v11

    .line 112
    if-nez v11, :cond_9

    .line 113
    .line 114
    if-nez v9, :cond_9

    .line 115
    .line 116
    if-eqz v4, :cond_9

    .line 117
    .line 118
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    .line 119
    .line 120
    .line 121
    move-result v4

    .line 122
    iput v4, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->OoO8:F

    .line 123
    .line 124
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    .line 125
    .line 126
    .line 127
    move-result v4

    .line 128
    iput v4, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->o800o8O:F

    .line 129
    .line 130
    iput v8, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O888o0o:I

    .line 131
    .line 132
    iput v10, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇80〇808〇O:F

    .line 133
    .line 134
    :cond_9
    const/4 v4, 0x6

    .line 135
    if-eqz v2, :cond_b

    .line 136
    .line 137
    if-eq v2, v4, :cond_b

    .line 138
    .line 139
    const/4 v9, 0x5

    .line 140
    if-eq v2, v9, :cond_b

    .line 141
    .line 142
    if-eqz v7, :cond_a

    .line 143
    .line 144
    goto :goto_5

    .line 145
    :cond_a
    const/4 v7, 0x0

    .line 146
    goto :goto_6

    .line 147
    :cond_b
    :goto_5
    const/4 v7, 0x1

    .line 148
    :goto_6
    if-ne v2, v4, :cond_c

    .line 149
    .line 150
    const/4 v4, 0x1

    .line 151
    goto :goto_7

    .line 152
    :cond_c
    const/4 v4, 0x0

    .line 153
    :goto_7
    if-eqz v4, :cond_d

    .line 154
    .line 155
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    .line 156
    .line 157
    .line 158
    move-result v9

    .line 159
    goto :goto_8

    .line 160
    :cond_d
    const/4 v9, -0x1

    .line 161
    :goto_8
    if-eqz v4, :cond_e

    .line 162
    .line 163
    add-int/lit8 v4, v3, -0x1

    .line 164
    .line 165
    goto :goto_9

    .line 166
    :cond_e
    move v4, v3

    .line 167
    :goto_9
    invoke-direct/range {p0 .. p0}, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇〇888()Z

    .line 168
    .line 169
    .line 170
    move-result v11

    .line 171
    if-eqz v11, :cond_10

    .line 172
    .line 173
    iget v11, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->OoO8:F

    .line 174
    .line 175
    iget v12, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->o800o8O:F

    .line 176
    .line 177
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    .line 178
    .line 179
    .line 180
    move-result v13

    .line 181
    cmpg-float v13, v13, v12

    .line 182
    .line 183
    if-gez v13, :cond_f

    .line 184
    .line 185
    iput-boolean v5, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇oo〇:Z

    .line 186
    .line 187
    goto :goto_c

    .line 188
    :cond_f
    iput-boolean v6, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇oo〇:Z

    .line 189
    .line 190
    goto :goto_c

    .line 191
    :cond_10
    const/4 v11, 0x0

    .line 192
    const/4 v12, 0x0

    .line 193
    const/4 v13, 0x0

    .line 194
    :goto_a
    if-ge v11, v3, :cond_12

    .line 195
    .line 196
    if-ne v9, v11, :cond_11

    .line 197
    .line 198
    goto :goto_b

    .line 199
    :cond_11
    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getX(I)F

    .line 200
    .line 201
    .line 202
    move-result v14

    .line 203
    add-float/2addr v12, v14

    .line 204
    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getY(I)F

    .line 205
    .line 206
    .line 207
    move-result v14

    .line 208
    add-float/2addr v13, v14

    .line 209
    :goto_b
    add-int/lit8 v11, v11, 0x1

    .line 210
    .line 211
    goto :goto_a

    .line 212
    :cond_12
    int-to-float v11, v4

    .line 213
    div-float/2addr v12, v11

    .line 214
    div-float v11, v13, v11

    .line 215
    .line 216
    move/from16 v16, v12

    .line 217
    .line 218
    move v12, v11

    .line 219
    move/from16 v11, v16

    .line 220
    .line 221
    :goto_c
    const/4 v13, 0x0

    .line 222
    const/4 v14, 0x0

    .line 223
    :goto_d
    if-ge v14, v3, :cond_14

    .line 224
    .line 225
    if-ne v9, v14, :cond_13

    .line 226
    .line 227
    goto :goto_e

    .line 228
    :cond_13
    :try_start_0
    invoke-virtual {v0, v14}, Landroid/view/MotionEvent;->getX(I)F

    .line 229
    .line 230
    .line 231
    move-result v15

    .line 232
    sub-float/2addr v15, v11

    .line 233
    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    .line 234
    .line 235
    .line 236
    move-result v15

    .line 237
    add-float/2addr v10, v15

    .line 238
    invoke-virtual {v0, v14}, Landroid/view/MotionEvent;->getY(I)F

    .line 239
    .line 240
    .line 241
    move-result v15

    .line 242
    sub-float/2addr v15, v12

    .line 243
    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    .line 244
    .line 245
    .line 246
    move-result v15
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 247
    add-float/2addr v13, v15

    .line 248
    :goto_e
    add-int/lit8 v14, v14, 0x1

    .line 249
    .line 250
    goto :goto_d

    .line 251
    :catch_0
    move-exception v0

    .line 252
    const-string v3, "ScaleGestureDetector"

    .line 253
    .line 254
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 255
    .line 256
    .line 257
    :cond_14
    int-to-float v0, v4

    .line 258
    div-float/2addr v10, v0

    .line 259
    div-float/2addr v13, v0

    .line 260
    const/high16 v0, 0x40000000    # 2.0f

    .line 261
    .line 262
    mul-float v10, v10, v0

    .line 263
    .line 264
    mul-float v13, v13, v0

    .line 265
    .line 266
    invoke-direct/range {p0 .. p0}, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇〇888()Z

    .line 267
    .line 268
    .line 269
    move-result v0

    .line 270
    if-eqz v0, :cond_15

    .line 271
    .line 272
    move v0, v13

    .line 273
    goto :goto_f

    .line 274
    :cond_15
    float-to-double v3, v10

    .line 275
    float-to-double v14, v13

    .line 276
    invoke-static {v3, v4, v14, v15}, Ljava/lang/Math;->hypot(DD)D

    .line 277
    .line 278
    .line 279
    move-result-wide v3

    .line 280
    double-to-float v0, v3

    .line 281
    :goto_f
    iget-boolean v3, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O〇:Z

    .line 282
    .line 283
    iput v11, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇o〇:F

    .line 284
    .line 285
    iput v12, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->O8:F

    .line 286
    .line 287
    invoke-direct/range {p0 .. p0}, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇〇888()Z

    .line 288
    .line 289
    .line 290
    move-result v4

    .line 291
    if-nez v4, :cond_17

    .line 292
    .line 293
    iget-boolean v4, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O〇:Z

    .line 294
    .line 295
    if-eqz v4, :cond_17

    .line 296
    .line 297
    iget v4, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇〇8O0〇8:I

    .line 298
    .line 299
    int-to-float v4, v4

    .line 300
    cmpg-float v4, v0, v4

    .line 301
    .line 302
    if-ltz v4, :cond_16

    .line 303
    .line 304
    if-eqz v7, :cond_17

    .line 305
    .line 306
    :cond_16
    iget-object v4, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇o00〇〇Oo:Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector$OnScaleGestureListener;

    .line 307
    .line 308
    invoke-interface {v4, v1}, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector$OnScaleGestureListener;->〇o00〇〇Oo(Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;)V

    .line 309
    .line 310
    .line 311
    iput-boolean v6, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O〇:Z

    .line 312
    .line 313
    iput v0, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇80〇808〇O:F

    .line 314
    .line 315
    :cond_17
    if-eqz v7, :cond_18

    .line 316
    .line 317
    iput v10, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->OO0o〇〇〇〇0:F

    .line 318
    .line 319
    iput v10, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O8o08O:F

    .line 320
    .line 321
    iput v13, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇8o8o〇:F

    .line 322
    .line 323
    iput v13, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->OO0o〇〇:F

    .line 324
    .line 325
    iput v0, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇〇888:F

    .line 326
    .line 327
    iput v0, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->oO80:F

    .line 328
    .line 329
    iput v0, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇80〇808〇O:F

    .line 330
    .line 331
    :cond_18
    invoke-direct/range {p0 .. p0}, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇〇888()Z

    .line 332
    .line 333
    .line 334
    move-result v4

    .line 335
    if-eqz v4, :cond_19

    .line 336
    .line 337
    iget v4, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O00:I

    .line 338
    .line 339
    goto :goto_10

    .line 340
    :cond_19
    iget v4, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇〇8O0〇8:I

    .line 341
    .line 342
    :goto_10
    iget-boolean v6, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O〇:Z

    .line 343
    .line 344
    if-nez v6, :cond_1b

    .line 345
    .line 346
    int-to-float v4, v4

    .line 347
    cmpl-float v4, v0, v4

    .line 348
    .line 349
    if-ltz v4, :cond_1b

    .line 350
    .line 351
    if-nez v3, :cond_1a

    .line 352
    .line 353
    iget v3, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇80〇808〇O:F

    .line 354
    .line 355
    sub-float v3, v0, v3

    .line 356
    .line 357
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    .line 358
    .line 359
    .line 360
    move-result v3

    .line 361
    iget v4, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O00:I

    .line 362
    .line 363
    int-to-float v4, v4

    .line 364
    cmpl-float v3, v3, v4

    .line 365
    .line 366
    if-lez v3, :cond_1b

    .line 367
    .line 368
    :cond_1a
    iput v10, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->OO0o〇〇〇〇0:F

    .line 369
    .line 370
    iput v10, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O8o08O:F

    .line 371
    .line 372
    iput v13, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇8o8o〇:F

    .line 373
    .line 374
    iput v13, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->OO0o〇〇:F

    .line 375
    .line 376
    iput v0, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇〇888:F

    .line 377
    .line 378
    iput v0, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->oO80:F

    .line 379
    .line 380
    iget-wide v3, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->Oooo8o0〇:J

    .line 381
    .line 382
    iput-wide v3, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇〇808〇:J

    .line 383
    .line 384
    iget-object v3, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇o00〇〇Oo:Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector$OnScaleGestureListener;

    .line 385
    .line 386
    invoke-interface {v3, v1}, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector$OnScaleGestureListener;->〇080(Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;)Z

    .line 387
    .line 388
    .line 389
    move-result v3

    .line 390
    iput-boolean v3, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O〇:Z

    .line 391
    .line 392
    :cond_1b
    if-ne v2, v8, :cond_1d

    .line 393
    .line 394
    iput v10, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->OO0o〇〇〇〇0:F

    .line 395
    .line 396
    iput v13, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇8o8o〇:F

    .line 397
    .line 398
    iput v0, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇〇888:F

    .line 399
    .line 400
    iget-boolean v0, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O〇:Z

    .line 401
    .line 402
    if-eqz v0, :cond_1c

    .line 403
    .line 404
    iget-object v0, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇o00〇〇Oo:Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector$OnScaleGestureListener;

    .line 405
    .line 406
    invoke-interface {v0, v1}, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector$OnScaleGestureListener;->〇o〇(Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;)Z

    .line 407
    .line 408
    .line 409
    move-result v0

    .line 410
    goto :goto_11

    .line 411
    :cond_1c
    const/4 v0, 0x1

    .line 412
    :goto_11
    if-eqz v0, :cond_1d

    .line 413
    .line 414
    iget v0, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->OO0o〇〇〇〇0:F

    .line 415
    .line 416
    iput v0, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇O8o08O:F

    .line 417
    .line 418
    iget v0, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇8o8o〇:F

    .line 419
    .line 420
    iput v0, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->OO0o〇〇:F

    .line 421
    .line 422
    iget v0, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇〇888:F

    .line 423
    .line 424
    iput v0, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->oO80:F

    .line 425
    .line 426
    iget-wide v2, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->Oooo8o0〇:J

    .line 427
    .line 428
    iput-wide v2, v1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->〇〇808〇:J

    .line 429
    .line 430
    :cond_1d
    return v5
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
.end method

.method 〇8o8o〇(F)V
    .locals 0

    .line 1
    sput p1, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->o〇O8〇〇o:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public 〇O8o08O(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/github/chrisbanes/photoview/CustomScaleGestureDetector;->o〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.class public final Lcom/google/android/gms/auth/api/R$styleable;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/auth/api/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final Capability:[I

.field public static final Capability_queryPatterns:I = 0x0

.field public static final Capability_shortcutMatchRequired:I = 0x1

.field public static final ColorStateListItem:[I

.field public static final ColorStateListItem_alpha:I = 0x3

.field public static final ColorStateListItem_android_alpha:I = 0x1

.field public static final ColorStateListItem_android_color:I = 0x0

.field public static final ColorStateListItem_android_lStar:I = 0x2

.field public static final ColorStateListItem_lStar:I = 0x4

.field public static final CoordinatorLayout:[I

.field public static final CoordinatorLayout_Layout:[I

.field public static final CoordinatorLayout_Layout_android_layout_gravity:I = 0x0

.field public static final CoordinatorLayout_Layout_layout_anchor:I = 0x1

.field public static final CoordinatorLayout_Layout_layout_anchorGravity:I = 0x2

.field public static final CoordinatorLayout_Layout_layout_behavior:I = 0x3

.field public static final CoordinatorLayout_Layout_layout_dodgeInsetEdges:I = 0x4

.field public static final CoordinatorLayout_Layout_layout_insetEdge:I = 0x5

.field public static final CoordinatorLayout_Layout_layout_keyline:I = 0x6

.field public static final CoordinatorLayout_keylines:I = 0x0

.field public static final CoordinatorLayout_statusBarBackground:I = 0x1

.field public static final DrawerLayout:[I

.field public static final DrawerLayout_elevation:I = 0x0

.field public static final FontFamily:[I

.field public static final FontFamilyFont:[I

.field public static final FontFamilyFont_android_font:I = 0x0

.field public static final FontFamilyFont_android_fontStyle:I = 0x2

.field public static final FontFamilyFont_android_fontVariationSettings:I = 0x4

.field public static final FontFamilyFont_android_fontWeight:I = 0x1

.field public static final FontFamilyFont_android_ttcIndex:I = 0x3

.field public static final FontFamilyFont_font:I = 0x5

.field public static final FontFamilyFont_fontStyle:I = 0x6

.field public static final FontFamilyFont_fontVariationSettings:I = 0x7

.field public static final FontFamilyFont_fontWeight:I = 0x8

.field public static final FontFamilyFont_ttcIndex:I = 0x9

.field public static final FontFamily_fontProviderAuthority:I = 0x0

.field public static final FontFamily_fontProviderCerts:I = 0x1

.field public static final FontFamily_fontProviderFetchStrategy:I = 0x2

.field public static final FontFamily_fontProviderFetchTimeout:I = 0x3

.field public static final FontFamily_fontProviderPackage:I = 0x4

.field public static final FontFamily_fontProviderQuery:I = 0x5

.field public static final FontFamily_fontProviderSystemFontFamily:I = 0x6

.field public static final Fragment:[I

.field public static final FragmentContainerView:[I

.field public static final FragmentContainerView_android_name:I = 0x0

.field public static final FragmentContainerView_android_tag:I = 0x1

.field public static final Fragment_android_id:I = 0x1

.field public static final Fragment_android_name:I = 0x0

.field public static final Fragment_android_tag:I = 0x2

.field public static final GradientColor:[I

.field public static final GradientColorItem:[I

.field public static final GradientColorItem_android_color:I = 0x0

.field public static final GradientColorItem_android_offset:I = 0x1

.field public static final GradientColor_android_centerColor:I = 0x7

.field public static final GradientColor_android_centerX:I = 0x3

.field public static final GradientColor_android_centerY:I = 0x4

.field public static final GradientColor_android_endColor:I = 0x1

.field public static final GradientColor_android_endX:I = 0xa

.field public static final GradientColor_android_endY:I = 0xb

.field public static final GradientColor_android_gradientRadius:I = 0x5

.field public static final GradientColor_android_startColor:I = 0x0

.field public static final GradientColor_android_startX:I = 0x8

.field public static final GradientColor_android_startY:I = 0x9

.field public static final GradientColor_android_tileMode:I = 0x6

.field public static final GradientColor_android_type:I = 0x2

.field public static final LoadingImageView:[I

.field public static final LoadingImageView_circleCrop:I = 0x0

.field public static final LoadingImageView_imageAspectRatio:I = 0x1

.field public static final LoadingImageView_imageAspectRatioAdjust:I = 0x2

.field public static final SignInButton:[I

.field public static final SignInButton_buttonSize:I = 0x0

.field public static final SignInButton_colorScheme:I = 0x1

.field public static final SignInButton_scopeUris:I = 0x2


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v1, v0, [I

    .line 3
    .line 4
    fill-array-data v1, :array_0

    .line 5
    .line 6
    .line 7
    sput-object v1, Lcom/google/android/gms/auth/api/R$styleable;->Capability:[I

    .line 8
    .line 9
    const/4 v1, 0x5

    .line 10
    new-array v1, v1, [I

    .line 11
    .line 12
    fill-array-data v1, :array_1

    .line 13
    .line 14
    .line 15
    sput-object v1, Lcom/google/android/gms/auth/api/R$styleable;->ColorStateListItem:[I

    .line 16
    .line 17
    new-array v1, v0, [I

    .line 18
    .line 19
    fill-array-data v1, :array_2

    .line 20
    .line 21
    .line 22
    sput-object v1, Lcom/google/android/gms/auth/api/R$styleable;->CoordinatorLayout:[I

    .line 23
    .line 24
    const/4 v1, 0x7

    .line 25
    new-array v2, v1, [I

    .line 26
    .line 27
    fill-array-data v2, :array_3

    .line 28
    .line 29
    .line 30
    sput-object v2, Lcom/google/android/gms/auth/api/R$styleable;->CoordinatorLayout_Layout:[I

    .line 31
    .line 32
    const/4 v2, 0x1

    .line 33
    new-array v2, v2, [I

    .line 34
    .line 35
    const/4 v3, 0x0

    .line 36
    const v4, 0x7f040239

    .line 37
    .line 38
    .line 39
    aput v4, v2, v3

    .line 40
    .line 41
    sput-object v2, Lcom/google/android/gms/auth/api/R$styleable;->DrawerLayout:[I

    .line 42
    .line 43
    new-array v1, v1, [I

    .line 44
    .line 45
    fill-array-data v1, :array_4

    .line 46
    .line 47
    .line 48
    sput-object v1, Lcom/google/android/gms/auth/api/R$styleable;->FontFamily:[I

    .line 49
    .line 50
    const/16 v1, 0xa

    .line 51
    .line 52
    new-array v1, v1, [I

    .line 53
    .line 54
    fill-array-data v1, :array_5

    .line 55
    .line 56
    .line 57
    sput-object v1, Lcom/google/android/gms/auth/api/R$styleable;->FontFamilyFont:[I

    .line 58
    .line 59
    const/4 v1, 0x3

    .line 60
    new-array v2, v1, [I

    .line 61
    .line 62
    fill-array-data v2, :array_6

    .line 63
    .line 64
    .line 65
    sput-object v2, Lcom/google/android/gms/auth/api/R$styleable;->Fragment:[I

    .line 66
    .line 67
    new-array v2, v0, [I

    .line 68
    .line 69
    fill-array-data v2, :array_7

    .line 70
    .line 71
    .line 72
    sput-object v2, Lcom/google/android/gms/auth/api/R$styleable;->FragmentContainerView:[I

    .line 73
    .line 74
    const/16 v2, 0xc

    .line 75
    .line 76
    new-array v2, v2, [I

    .line 77
    .line 78
    fill-array-data v2, :array_8

    .line 79
    .line 80
    .line 81
    sput-object v2, Lcom/google/android/gms/auth/api/R$styleable;->GradientColor:[I

    .line 82
    .line 83
    new-array v0, v0, [I

    .line 84
    .line 85
    fill-array-data v0, :array_9

    .line 86
    .line 87
    .line 88
    sput-object v0, Lcom/google/android/gms/auth/api/R$styleable;->GradientColorItem:[I

    .line 89
    .line 90
    new-array v0, v1, [I

    .line 91
    .line 92
    fill-array-data v0, :array_a

    .line 93
    .line 94
    .line 95
    sput-object v0, Lcom/google/android/gms/auth/api/R$styleable;->LoadingImageView:[I

    .line 96
    .line 97
    new-array v0, v1, [I

    .line 98
    .line 99
    fill-array-data v0, :array_b

    .line 100
    .line 101
    .line 102
    sput-object v0, Lcom/google/android/gms/auth/api/R$styleable;->SignInButton:[I

    .line 103
    .line 104
    return-void

    .line 105
    :array_0
    .array-data 4
        0x7f0404f1
        0x7f04054e
    .end array-data

    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    :array_1
    .array-data 4
        0x10101a5
        0x101031f
        0x1010647
        0x7f04005b
        0x7f04036e
    .end array-data

    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    :array_2
    .array-data 4
        0x7f04036d
        0x7f04059a
    .end array-data

    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    :array_3
    .array-data 4
        0x10100b3
        0x7f040379
        0x7f04037a
        0x7f04037b
        0x7f0403ac
        0x7f0403b9
        0x7f0403ba
    .end array-data

    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    :array_4
    .array-data 4
        0x7f0402c9
        0x7f0402ca
        0x7f0402cb
        0x7f0402cc
        0x7f0402cd
        0x7f0402ce
        0x7f0402cf
    .end array-data

    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    :array_5
    .array-data 4
        0x1010532
        0x1010533
        0x101053f
        0x101056f
        0x1010570
        0x7f0402c7
        0x7f0402d0
        0x7f0402d1
        0x7f0402d2
        0x7f040697
    .end array-data

    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    :array_6
    .array-data 4
        0x1010003
        0x10100d0
        0x10100d1
    .end array-data

    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    :array_7
    .array-data 4
        0x1010003
        0x10100d1
    .end array-data

    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    :array_8
    .array-data 4
        0x101019d
        0x101019e
        0x10101a1
        0x10101a2
        0x10101a3
        0x10101a4
        0x1010201
        0x101020b
        0x1010510
        0x1010511
        0x1010512
        0x1010513
    .end array-data

    :array_9
    .array-data 4
        0x10101a5
        0x1010514
    .end array-data

    :array_a
    .array-data 4
        0x7f040123
        0x7f04030a
        0x7f04030b
    .end array-data

    :array_b
    .array-data 4
        0x7f0400e5
        0x7f040173
        0x7f04051d
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public final Lcom/google/android/flexbox/R$attr;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/flexbox/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final alignContent:I = 0x7f040057

.field public static final alignItems:I = 0x7f040058

.field public static final alpha:I = 0x7f04005b

.field public static final dividerDrawable:I = 0x7f0401fe

.field public static final dividerDrawableHorizontal:I = 0x7f0401ff

.field public static final dividerDrawableVertical:I = 0x7f040200

.field public static final fastScrollEnabled:I = 0x7f040285

.field public static final fastScrollHorizontalThumbDrawable:I = 0x7f040286

.field public static final fastScrollHorizontalTrackDrawable:I = 0x7f040287

.field public static final fastScrollVerticalThumbDrawable:I = 0x7f040295

.field public static final fastScrollVerticalTrackDrawable:I = 0x7f040296

.field public static final flexDirection:I = 0x7f04029e

.field public static final flexWrap:I = 0x7f04029f

.field public static final font:I = 0x7f0402c7

.field public static final fontProviderAuthority:I = 0x7f0402c9

.field public static final fontProviderCerts:I = 0x7f0402ca

.field public static final fontProviderFetchStrategy:I = 0x7f0402cb

.field public static final fontProviderFetchTimeout:I = 0x7f0402cc

.field public static final fontProviderPackage:I = 0x7f0402cd

.field public static final fontProviderQuery:I = 0x7f0402ce

.field public static final fontStyle:I = 0x7f0402d0

.field public static final fontVariationSettings:I = 0x7f0402d1

.field public static final fontWeight:I = 0x7f0402d2

.field public static final justifyContent:I = 0x7f04036a

.field public static final layoutManager:I = 0x7f040377

.field public static final layout_alignSelf:I = 0x7f040378

.field public static final layout_flexBasisPercent:I = 0x7f0403af

.field public static final layout_flexGrow:I = 0x7f0403b0

.field public static final layout_flexShrink:I = 0x7f0403b1

.field public static final layout_maxHeight:I = 0x7f0403bc

.field public static final layout_maxWidth:I = 0x7f0403bd

.field public static final layout_minHeight:I = 0x7f0403be

.field public static final layout_minWidth:I = 0x7f0403bf

.field public static final layout_order:I = 0x7f0403c1

.field public static final layout_wrapBefore:I = 0x7f0403c6

.field public static final maxLine:I = 0x7f040441

.field public static final recyclerViewStyle:I = 0x7f0404fb

.field public static final reverseLayout:I = 0x7f040504

.field public static final showDivider:I = 0x7f040553

.field public static final showDividerHorizontal:I = 0x7f040554

.field public static final showDividerVertical:I = 0x7f040555

.field public static final spanCount:I = 0x7f04057a

.field public static final stackFromEnd:I = 0x7f040587

.field public static final ttcIndex:I = 0x7f040697


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

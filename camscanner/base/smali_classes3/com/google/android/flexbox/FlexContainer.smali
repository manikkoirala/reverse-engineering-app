.class interface abstract Lcom/google/android/flexbox/FlexContainer;
.super Ljava/lang/Object;
.source "FlexContainer.java"


# virtual methods
.method public abstract O8(I)Landroid/view/View;
.end method

.method public abstract OO0o〇〇〇〇0(Landroid/view/View;II)I
.end method

.method public abstract Oo08(III)I
.end method

.method public abstract O〇8O8〇008()Z
.end method

.method public abstract getAlignContent()I
.end method

.method public abstract getAlignItems()I
.end method

.method public abstract getFlexDirection()I
.end method

.method public abstract getFlexItemCount()I
.end method

.method public abstract getFlexLinesInternal()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/google/android/flexbox/FlexLine;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getFlexWrap()I
.end method

.method public abstract getLargestMainSize()I
.end method

.method public abstract getMaxLine()I
.end method

.method public abstract getPaddingBottom()I
.end method

.method public abstract getPaddingEnd()I
.end method

.method public abstract getPaddingLeft()I
.end method

.method public abstract getPaddingRight()I
.end method

.method public abstract getPaddingStart()I
.end method

.method public abstract getPaddingTop()I
.end method

.method public abstract getSumOfCrossSize()I
.end method

.method public abstract oO80(I)Landroid/view/View;
.end method

.method public abstract o〇0(Landroid/view/View;)I
.end method

.method public abstract setFlexLines(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/google/android/flexbox/FlexLine;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract 〇080(Landroid/view/View;IILcom/google/android/flexbox/FlexLine;)V
.end method

.method public abstract 〇O888o0o(Lcom/google/android/flexbox/FlexLine;)V
.end method

.method public abstract 〇O〇(III)I
.end method

.method public abstract 〇oo〇(ILandroid/view/View;)V
.end method

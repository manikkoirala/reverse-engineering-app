.class public final Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;
.super Ljava/lang/Object;
.source "ImageReaderPreviewImplementation.kt"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Landroid/opengl/GLSurfaceView$Renderer;


# annotations
.annotation build Landroidx/annotation/RequiresApi;
    value = 0x15
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/camera/preview/ImageReaderPreviewImplementation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "SurfaceRequestCallback"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private OO:Lcom/google/android/camera/size/CameraSize;

.field private o0:Lcom/google/android/camera/size/CameraSize;

.field final synthetic o〇00O:Lcom/google/android/camera/preview/ImageReaderPreviewImplementation;

.field private 〇08O〇00〇o:Z

.field private 〇OOo8〇0:Lcom/google/android/camera/compat/view/SurfaceRequest;


# direct methods
.method public constructor <init>(Lcom/google/android/camera/preview/ImageReaderPreviewImplementation;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->o〇00O:Lcom/google/android/camera/preview/ImageReaderPreviewImplementation;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final O8()V
    .locals 3
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->〇OOo8〇0:Lcom/google/android/camera/compat/view/SurfaceRequest;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v2, "Surface invalidated "

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    const-string v1, "CameraX-ImageReaderPreview"

    .line 23
    .line 24
    invoke-static {v1, v0}, Lcom/google/android/camera/log/CameraLog;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->〇OOo8〇0:Lcom/google/android/camera/compat/view/SurfaceRequest;

    .line 28
    .line 29
    if-eqz v0, :cond_0

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/google/android/camera/compat/view/SurfaceRequest;->〇8o8o〇()Lcom/google/android/camera/compat/view/DeferrableSurface;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    if-eqz v0, :cond_0

    .line 36
    .line 37
    invoke-virtual {v0}, Lcom/google/android/camera/compat/view/DeferrableSurface;->〇o〇()V

    .line 38
    .line 39
    .line 40
    :cond_0
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final o〇0()Z
    .locals 4
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->o〇00O:Lcom/google/android/camera/preview/ImageReaderPreviewImplementation;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation;->〇〇8O0〇8(Lcom/google/android/camera/preview/ImageReaderPreviewImplementation;)Lcom/google/android/camera/preview/ImageReaderPreview;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const/4 v0, 0x0

    .line 21
    :goto_0
    invoke-direct {p0}, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->〇o00〇〇Oo()Z

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    if-eqz v1, :cond_2

    .line 26
    .line 27
    if-eqz v0, :cond_2

    .line 28
    .line 29
    const-string v1, "CameraX-ImageReaderPreview"

    .line 30
    .line 31
    const-string v2, "Surface set on Preview."

    .line 32
    .line 33
    invoke-static {v1, v2}, Lcom/google/android/camera/log/CameraLog;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    iget-object v1, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->〇OOo8〇0:Lcom/google/android/camera/compat/view/SurfaceRequest;

    .line 37
    .line 38
    if-eqz v1, :cond_1

    .line 39
    .line 40
    iget-object v2, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->o〇00O:Lcom/google/android/camera/preview/ImageReaderPreviewImplementation;

    .line 41
    .line 42
    invoke-virtual {v2}, Lcom/google/android/camera/compat/view/PreviewViewImplementation;->〇o〇()Landroid/view/ViewGroup;

    .line 43
    .line 44
    .line 45
    move-result-object v2

    .line 46
    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    invoke-static {v2}, Landroidx/core/content/ContextCompat;->getMainExecutor(Landroid/content/Context;)Ljava/util/concurrent/Executor;

    .line 51
    .line 52
    .line 53
    move-result-object v2

    .line 54
    new-instance v3, Lcom/google/android/camera/preview/OoO8;

    .line 55
    .line 56
    invoke-direct {v3}, Lcom/google/android/camera/preview/OoO8;-><init>()V

    .line 57
    .line 58
    .line 59
    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/camera/compat/view/SurfaceRequest;->o800o8O(Landroid/view/Surface;Ljava/util/concurrent/Executor;Landroidx/core/util/Consumer;)V

    .line 60
    .line 61
    .line 62
    :cond_1
    const/4 v0, 0x1

    .line 63
    iput-boolean v0, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->〇08O〇00〇o:Z

    .line 64
    .line 65
    iget-object v1, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->o〇00O:Lcom/google/android/camera/preview/ImageReaderPreviewImplementation;

    .line 66
    .line 67
    invoke-virtual {v1}, Lcom/google/android/camera/compat/view/PreviewViewImplementation;->〇O8o08O()V

    .line 68
    .line 69
    .line 70
    return v0

    .line 71
    :cond_2
    const/4 v0, 0x0

    .line 72
    return v0
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public static synthetic 〇080(Lcom/google/android/camera/compat/view/SurfaceRequest$Result;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->〇〇888(Lcom/google/android/camera/compat/view/SurfaceRequest$Result;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final 〇o00〇〇Oo()Z
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->〇08O〇00〇o:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->〇OOo8〇0:Lcom/google/android/camera/compat/view/SurfaceRequest;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->o0:Lcom/google/android/camera/size/CameraSize;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    iget-object v1, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->OO:Lcom/google/android/camera/size/CameraSize;

    .line 14
    .line 15
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    const/4 v0, 0x1

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    const/4 v0, 0x0

    .line 24
    :goto_0
    return v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final 〇o〇()V
    .locals 3
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->〇OOo8〇0:Lcom/google/android/camera/compat/view/SurfaceRequest;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v2, "Request canceled: "

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    const-string v1, "CameraX-ImageReaderPreview"

    .line 23
    .line 24
    invoke-static {v1, v0}, Lcom/google/android/camera/log/CameraLog;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->〇OOo8〇0:Lcom/google/android/camera/compat/view/SurfaceRequest;

    .line 28
    .line 29
    if-eqz v0, :cond_0

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/google/android/camera/compat/view/SurfaceRequest;->〇oo〇()Z

    .line 32
    .line 33
    .line 34
    :cond_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private static final 〇〇888(Lcom/google/android/camera/compat/view/SurfaceRequest$Result;)V
    .locals 1

    .line 1
    const-string p0, "CameraX-ImageReaderPreview"

    .line 2
    .line 3
    const-string v0, "Safe to release surface."

    .line 4
    .line 5
    invoke-static {p0, v0}, Lcom/google/android/camera/log/CameraLog;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public final Oo08(Lcom/google/android/camera/compat/view/SurfaceRequest;)V
    .locals 2
    .param p1    # Lcom/google/android/camera/compat/view/SurfaceRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .line 1
    const-string/jumbo v0, "surfaceRequest"

    .line 2
    .line 3
    .line 4
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0}, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->〇o〇()V

    .line 8
    .line 9
    .line 10
    iput-object p1, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->〇OOo8〇0:Lcom/google/android/camera/compat/view/SurfaceRequest;

    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/google/android/camera/compat/view/SurfaceRequest;->〇O8o08O()Lcom/google/android/camera/size/CameraSize;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    const-string/jumbo v0, "surfaceRequest.resolution"

    .line 17
    .line 18
    .line 19
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    iput-object p1, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->o0:Lcom/google/android/camera/size/CameraSize;

    .line 23
    .line 24
    const/4 v0, 0x0

    .line 25
    iput-boolean v0, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->〇08O〇00〇o:Z

    .line 26
    .line 27
    invoke-direct {p0}, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->o〇0()Z

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-nez v0, :cond_0

    .line 32
    .line 33
    const-string v0, "CameraX-ImageReaderPreview"

    .line 34
    .line 35
    const-string v1, "Wait for new Surface creation."

    .line 36
    .line 37
    invoke-static {v0, v1}, Lcom/google/android/camera/log/CameraLog;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    iget-object v0, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->o〇00O:Lcom/google/android/camera/preview/ImageReaderPreviewImplementation;

    .line 41
    .line 42
    invoke-static {v0}, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation;->〇〇8O0〇8(Lcom/google/android/camera/preview/ImageReaderPreviewImplementation;)Lcom/google/android/camera/preview/ImageReaderPreview;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    if-eqz v0, :cond_0

    .line 47
    .line 48
    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    if-eqz v0, :cond_0

    .line 53
    .line 54
    invoke-virtual {p1}, Lcom/google/android/camera/size/CameraSize;->getWidth()I

    .line 55
    .line 56
    .line 57
    move-result v1

    .line 58
    invoke-virtual {p1}, Lcom/google/android/camera/size/CameraSize;->getHeight()I

    .line 59
    .line 60
    .line 61
    move-result p1

    .line 62
    invoke-interface {v0, v1, p1}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    .line 63
    .line 64
    .line 65
    :cond_0
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->o〇00O:Lcom/google/android/camera/preview/ImageReaderPreviewImplementation;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation;->〇〇8O0〇8(Lcom/google/android/camera/preview/ImageReaderPreviewImplementation;)Lcom/google/android/camera/preview/ImageReaderPreview;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0, p1}, Lcom/google/android/camera/preview/BaseGlSurfaceView;->〇〇888(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->o〇00O:Lcom/google/android/camera/preview/ImageReaderPreviewImplementation;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation;->〇〇8O0〇8(Lcom/google/android/camera/preview/ImageReaderPreviewImplementation;)Lcom/google/android/camera/preview/ImageReaderPreview;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/camera/preview/BaseGlSurfaceView;->oO80(Ljavax/microedition/khronos/opengles/GL10;II)V

    .line 10
    .line 11
    .line 12
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    .line 13
    .line 14
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 15
    .line 16
    .line 17
    const-string v0, "Surface changed. Size: "

    .line 18
    .line 19
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    const-string/jumbo v0, "x"

    .line 26
    .line 27
    .line 28
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    const-string v0, "CameraX-ImageReaderPreview"

    .line 39
    .line 40
    invoke-static {v0, p1}, Lcom/google/android/camera/log/CameraLog;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    new-instance p1, Lcom/google/android/camera/size/CameraSize;

    .line 44
    .line 45
    invoke-direct {p1, p2, p3}, Lcom/google/android/camera/size/CameraSize;-><init>(II)V

    .line 46
    .line 47
    .line 48
    iput-object p1, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->OO:Lcom/google/android/camera/size/CameraSize;

    .line 49
    .line 50
    invoke-direct {p0}, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->o〇0()Z

    .line 51
    .line 52
    .line 53
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->o〇00O:Lcom/google/android/camera/preview/ImageReaderPreviewImplementation;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation;->〇〇8O0〇8(Lcom/google/android/camera/preview/ImageReaderPreviewImplementation;)Lcom/google/android/camera/preview/ImageReaderPreview;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0, p1, p2}, Lcom/google/android/camera/preview/BaseGlSurfaceView;->〇80〇808〇O(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string/jumbo p2, "surfaceHolder"

    .line 2
    .line 3
    .line 4
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string/jumbo v0, "surfaceHolder"

    .line 2
    .line 3
    .line 4
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    const-string p1, "CameraX-ImageReaderPreview"

    .line 8
    .line 9
    const-string v0, "Surface created."

    .line 10
    .line 11
    invoke-static {p1, v0}, Lcom/google/android/camera/log/CameraLog;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string/jumbo v0, "surfaceHolder"

    .line 2
    .line 3
    .line 4
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    const-string p1, "CameraX-ImageReaderPreview"

    .line 8
    .line 9
    const-string v0, "Surface destroyed."

    .line 10
    .line 11
    invoke-static {p1, v0}, Lcom/google/android/camera/log/CameraLog;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    iget-boolean p1, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->〇08O〇00〇o:Z

    .line 15
    .line 16
    if-eqz p1, :cond_0

    .line 17
    .line 18
    invoke-direct {p0}, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->O8()V

    .line 19
    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    invoke-direct {p0}, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->〇o〇()V

    .line 23
    .line 24
    .line 25
    :goto_0
    const/4 p1, 0x0

    .line 26
    iput-boolean p1, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->〇08O〇00〇o:Z

    .line 27
    .line 28
    const/4 p1, 0x0

    .line 29
    iput-object p1, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->〇OOo8〇0:Lcom/google/android/camera/compat/view/SurfaceRequest;

    .line 30
    .line 31
    iput-object p1, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->OO:Lcom/google/android/camera/size/CameraSize;

    .line 32
    .line 33
    iput-object p1, p0, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation$SurfaceRequestCallback;->o0:Lcom/google/android/camera/size/CameraSize;

    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

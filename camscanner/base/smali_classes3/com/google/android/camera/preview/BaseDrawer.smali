.class public abstract Lcom/google/android/camera/preview/BaseDrawer;
.super Ljava/lang/Object;
.source "BaseDrawer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/camera/preview/BaseDrawer$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final O8:I

.field private static final Oo08:I

.field private static final o〇0:I

.field public static final 〇o〇:Lcom/google/android/camera/preview/BaseDrawer$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field protected 〇080:I

.field protected 〇o00〇〇Oo:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/google/android/camera/preview/BaseDrawer$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/google/android/camera/preview/BaseDrawer$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/google/android/camera/preview/BaseDrawer;->〇o〇:Lcom/google/android/camera/preview/BaseDrawer$Companion;

    .line 8
    .line 9
    const/4 v0, 0x4

    .line 10
    sput v0, Lcom/google/android/camera/preview/BaseDrawer;->O8:I

    .line 11
    .line 12
    sput v0, Lcom/google/android/camera/preview/BaseDrawer;->Oo08:I

    .line 13
    .line 14
    sput v0, Lcom/google/android/camera/preview/BaseDrawer;->o〇0:I

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method protected static final Oo08()I
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/camera/preview/BaseDrawer;->〇o〇:Lcom/google/android/camera/preview/BaseDrawer$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/google/android/camera/preview/BaseDrawer$Companion;->〇o00〇〇Oo()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method protected static final oO80()I
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/camera/preview/BaseDrawer;->〇o〇:Lcom/google/android/camera/preview/BaseDrawer$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/google/android/camera/preview/BaseDrawer$Companion;->O8()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public static final synthetic 〇080()I
    .locals 1

    .line 1
    sget v0, Lcom/google/android/camera/preview/BaseDrawer;->O8:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public static final synthetic 〇o00〇〇Oo()I
    .locals 1

    .line 1
    sget v0, Lcom/google/android/camera/preview/BaseDrawer;->Oo08:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public static final synthetic 〇o〇()I
    .locals 1

    .line 1
    sget v0, Lcom/google/android/camera/preview/BaseDrawer;->o〇0:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method protected static final 〇〇888()I
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/camera/preview/BaseDrawer;->〇o〇:Lcom/google/android/camera/preview/BaseDrawer$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/google/android/camera/preview/BaseDrawer$Companion;->〇o〇()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method


# virtual methods
.method public abstract O8([I[[F[Landroid/graphics/Rect;[Ljava/nio/FloatBuffer;)V
    .param p1    # [I
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # [[F
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # [Landroid/graphics/Rect;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # [Ljava/nio/FloatBuffer;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method protected final OO0o〇〇〇〇0(II)I
    .locals 2

    .line 1
    invoke-static {}, Lcom/google/android/camera/preview/〇o00〇〇Oo;->〇080()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static {v0, p1}, Lcom/google/android/camera/preview/O8;->〇080(II)V

    .line 8
    .line 9
    .line 10
    invoke-static {v0, p2}, Lcom/google/android/camera/preview/O8;->〇080(II)V

    .line 11
    .line 12
    .line 13
    invoke-static {v0}, Lcom/google/android/camera/preview/Oo08;->〇080(I)V

    .line 14
    .line 15
    .line 16
    sget-object p1, Lcom/google/android/camera/preview/BaseDrawer;->〇o〇:Lcom/google/android/camera/preview/BaseDrawer$Companion;

    .line 17
    .line 18
    invoke-virtual {p1}, Lcom/google/android/camera/preview/BaseDrawer$Companion;->〇080()V

    .line 19
    .line 20
    .line 21
    return v0

    .line 22
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    .line 23
    .line 24
    invoke-static {}, Lcom/google/android/camera/preview/〇o〇;->〇080()I

    .line 25
    .line 26
    .line 27
    move-result p2

    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    .line 29
    .line 30
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 31
    .line 32
    .line 33
    const-string v1, "linkProgram Failed!"

    .line 34
    .line 35
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object p2

    .line 45
    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    throw p1
    .line 49
.end method

.method protected abstract o〇0()Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public 〇80〇808〇O()V
    .locals 3
    .annotation build Landroidx/annotation/CallSuper;
    .end annotation

    .line 1
    const v0, 0x8b31

    .line 2
    .line 3
    .line 4
    const-string v1, "#version 300 es                                              \nuniform mat4 mvp_matrix;                                     \nlayout(location = 0) in vec4 a_position;                     \nlayout(location = 1) in vec4 a_texture_coord;                \nout vec2 v_texture_coord;                                    \nvoid main()                                                  \n{                                                            \n    gl_Position = a_position;                                \n    v_texture_coord = (mvp_matrix * a_texture_coord).xy;     \n}                                                            \n"

    .line 5
    .line 6
    invoke-virtual {p0, v0, v1}, Lcom/google/android/camera/preview/BaseDrawer;->〇8o8o〇(ILjava/lang/String;)I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const v1, 0x8b30

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/google/android/camera/preview/BaseDrawer;->o〇0()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    invoke-virtual {p0, v1, v2}, Lcom/google/android/camera/preview/BaseDrawer;->〇8o8o〇(ILjava/lang/String;)I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    invoke-virtual {p0, v0, v1}, Lcom/google/android/camera/preview/BaseDrawer;->OO0o〇〇〇〇0(II)I

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    iput v0, p0, Lcom/google/android/camera/preview/BaseDrawer;->〇080:I

    .line 26
    .line 27
    sget-object v0, Lcom/google/android/camera/preview/BaseDrawer;->〇o〇:Lcom/google/android/camera/preview/BaseDrawer$Companion;

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/google/android/camera/preview/BaseDrawer$Companion;->〇080()V

    .line 30
    .line 31
    .line 32
    iget v1, p0, Lcom/google/android/camera/preview/BaseDrawer;->〇080:I

    .line 33
    .line 34
    const-string v2, "mvp_matrix"

    .line 35
    .line 36
    invoke-static {v1, v2}, Lcom/google/android/camera/preview/〇080;->〇080(ILjava/lang/String;)I

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    iput v1, p0, Lcom/google/android/camera/preview/BaseDrawer;->〇o00〇〇Oo:I

    .line 41
    .line 42
    invoke-virtual {v0}, Lcom/google/android/camera/preview/BaseDrawer$Companion;->〇080()V

    .line 43
    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method protected final 〇8o8o〇(ILjava/lang/String;)I
    .locals 2

    .line 1
    invoke-static {p1}, Lcom/google/android/camera/preview/o〇0;->〇080(I)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    invoke-static {p1, p2}, Lcom/google/android/camera/preview/〇〇888;->〇080(ILjava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-static {p1}, Lcom/google/android/camera/preview/oO80;->〇080(I)V

    .line 11
    .line 12
    .line 13
    return p1

    .line 14
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    .line 15
    .line 16
    invoke-static {}, Lcom/google/android/camera/preview/〇o〇;->〇080()I

    .line 17
    .line 18
    .line 19
    move-result p2

    .line 20
    new-instance v0, Ljava/lang/StringBuilder;

    .line 21
    .line 22
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 23
    .line 24
    .line 25
    const-string v1, "loadShader Failed!"

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object p2

    .line 37
    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    throw p1
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.class public final Lcom/google/android/camera/Camera2$mOnImageAvailableListener$1;
.super Ljava/lang/Object;
.source "Camera2.kt"

# interfaces
.implements Lcom/google/android/camera/compat/imagereader/ImageReaderProxy$OnImageAvailableListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/camera/Camera2;-><init>(Landroid/content/Context;Lcom/google/android/camera/CameraViewImpl$Callback;Lcom/google/android/camera/PreviewImpl;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic o0:Lcom/google/android/camera/Camera2;


# direct methods
.method constructor <init>(Lcom/google/android/camera/Camera2;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/camera/Camera2$mOnImageAvailableListener$1;->o0:Lcom/google/android/camera/Camera2;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public 〇080(Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;)V
    .locals 5
    .param p1    # Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mOnImageAvailableListener error"

    .line 2
    .line 3
    const-string v1, "CameraX-Camera2"

    .line 4
    .line 5
    const-string v2, "imageReader"

    .line 6
    .line 7
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v2, 0x0

    .line 11
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;->acquireLatestImage()Lcom/google/android/camera/compat/imagereader/ImageProxy;

    .line 12
    .line 13
    .line 14
    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 15
    if-eqz p1, :cond_0

    .line 16
    .line 17
    :try_start_1
    invoke-interface {p1}, Lcom/google/android/camera/compat/imagereader/ImageProxy;->getImage()Landroid/media/Image;

    .line 18
    .line 19
    .line 20
    move-result-object v3

    .line 21
    if-eqz v3, :cond_0

    .line 22
    .line 23
    invoke-static {v3}, Lcom/google/android/camera/util/CameraExtKt;->Oo08(Landroid/media/Image;)[B

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    goto :goto_0

    .line 28
    :catchall_0
    move-exception v3

    .line 29
    move-object v4, v2

    .line 30
    move-object v2, p1

    .line 31
    move-object p1, v4

    .line 32
    goto :goto_1

    .line 33
    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    .line 34
    .line 35
    invoke-interface {p1}, Lcom/google/android/camera/compat/imagereader/ImageProxy;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 36
    .line 37
    .line 38
    :cond_1
    if-eqz p1, :cond_3

    .line 39
    .line 40
    :try_start_2
    invoke-interface {p1}, Lcom/google/android/camera/compat/imagereader/ImageProxy;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 41
    .line 42
    .line 43
    goto :goto_3

    .line 44
    :catchall_1
    move-exception p1

    .line 45
    invoke-static {v1, v0, p1}, Lcom/google/android/camera/log/CameraLog;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 46
    .line 47
    .line 48
    goto :goto_3

    .line 49
    :catchall_2
    move-exception v3

    .line 50
    move-object p1, v2

    .line 51
    :goto_1
    :try_start_3
    invoke-static {v1, v0, v3}, Lcom/google/android/camera/log/CameraLog;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 52
    .line 53
    .line 54
    if-eqz v2, :cond_2

    .line 55
    .line 56
    :try_start_4
    invoke-interface {v2}, Lcom/google/android/camera/compat/imagereader/ImageProxy;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 57
    .line 58
    .line 59
    goto :goto_2

    .line 60
    :catchall_3
    move-exception v2

    .line 61
    invoke-static {v1, v0, v2}, Lcom/google/android/camera/log/CameraLog;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 62
    .line 63
    .line 64
    :cond_2
    :goto_2
    move-object v2, p1

    .line 65
    :cond_3
    :goto_3
    :try_start_5
    iget-object p1, p0, Lcom/google/android/camera/Camera2$mOnImageAvailableListener$1;->o0:Lcom/google/android/camera/Camera2;

    .line 66
    .line 67
    invoke-virtual {p1}, Lcom/google/android/camera/CameraViewImpl;->〇oOO8O8()Lcom/google/android/camera/CameraViewImpl$Callback;

    .line 68
    .line 69
    .line 70
    move-result-object p1

    .line 71
    invoke-interface {p1, v2}, Lcom/google/android/camera/CameraViewImpl$Callback;->o〇0([B)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    .line 72
    .line 73
    .line 74
    goto :goto_4

    .line 75
    :catch_0
    move-exception p1

    .line 76
    const-string v0, "onPictureTaken error"

    .line 77
    .line 78
    invoke-static {v1, v0, p1}, Lcom/google/android/camera/log/CameraLog;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 79
    .line 80
    .line 81
    :goto_4
    return-void

    .line 82
    :catchall_4
    move-exception p1

    .line 83
    if-eqz v2, :cond_4

    .line 84
    .line 85
    :try_start_6
    invoke-interface {v2}, Lcom/google/android/camera/compat/imagereader/ImageProxy;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_5

    .line 86
    .line 87
    .line 88
    goto :goto_5

    .line 89
    :catchall_5
    move-exception v2

    .line 90
    invoke-static {v1, v0, v2}, Lcom/google/android/camera/log/CameraLog;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 91
    .line 92
    .line 93
    :cond_4
    :goto_5
    throw p1
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

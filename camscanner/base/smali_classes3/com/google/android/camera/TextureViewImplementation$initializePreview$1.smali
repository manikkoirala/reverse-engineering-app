.class public final Lcom/google/android/camera/TextureViewImplementation$initializePreview$1;
.super Ljava/lang/Object;
.source "TextureViewImplementation.kt"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/camera/TextureViewImplementation;->〇00()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic o0:Lcom/google/android/camera/TextureViewImplementation;


# direct methods
.method constructor <init>(Lcom/google/android/camera/TextureViewImplementation;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/camera/TextureViewImplementation$initializePreview$1;->o0:Lcom/google/android/camera/TextureViewImplementation;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 2
    .param p1    # Landroid/graphics/SurfaceTexture;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string/jumbo v0, "surfaceTexture"

    .line 2
    .line 3
    .line 4
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    new-instance v0, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v1, "SurfaceTexture available. Size: "

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    const-string/jumbo p2, "x"

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object p2

    .line 33
    const-string p3, "CameraX-TextureViewPreview"

    .line 34
    .line 35
    invoke-static {p3, p2}, Lcom/google/android/camera/log/CameraLog;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    iget-object p2, p0, Lcom/google/android/camera/TextureViewImplementation$initializePreview$1;->o0:Lcom/google/android/camera/TextureViewImplementation;

    .line 39
    .line 40
    invoke-static {p2, p1}, Lcom/google/android/camera/TextureViewImplementation;->o〇O8〇〇o(Lcom/google/android/camera/TextureViewImplementation;Landroid/graphics/SurfaceTexture;)V

    .line 41
    .line 42
    .line 43
    iget-object p1, p0, Lcom/google/android/camera/TextureViewImplementation$initializePreview$1;->o0:Lcom/google/android/camera/TextureViewImplementation;

    .line 44
    .line 45
    invoke-static {p1}, Lcom/google/android/camera/TextureViewImplementation;->〇O888o0o(Lcom/google/android/camera/TextureViewImplementation;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    if-eqz p1, :cond_0

    .line 50
    .line 51
    iget-object p1, p0, Lcom/google/android/camera/TextureViewImplementation$initializePreview$1;->o0:Lcom/google/android/camera/TextureViewImplementation;

    .line 52
    .line 53
    invoke-static {p1}, Lcom/google/android/camera/TextureViewImplementation;->oo88o8O(Lcom/google/android/camera/TextureViewImplementation;)Lcom/google/android/camera/compat/view/SurfaceRequest;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    invoke-static {p1}, Lcom/google/android/camera/compat/Preconditions;->〇o〇(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    .line 59
    .line 60
    iget-object p1, p0, Lcom/google/android/camera/TextureViewImplementation$initializePreview$1;->o0:Lcom/google/android/camera/TextureViewImplementation;

    .line 61
    .line 62
    invoke-static {p1}, Lcom/google/android/camera/TextureViewImplementation;->oo88o8O(Lcom/google/android/camera/TextureViewImplementation;)Lcom/google/android/camera/compat/view/SurfaceRequest;

    .line 63
    .line 64
    .line 65
    move-result-object p1

    .line 66
    new-instance p2, Ljava/lang/StringBuilder;

    .line 67
    .line 68
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    .line 70
    .line 71
    const-string v0, "Surface invalidated "

    .line 72
    .line 73
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object p1

    .line 83
    invoke-static {p3, p1}, Lcom/google/android/camera/log/CameraLog;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    iget-object p1, p0, Lcom/google/android/camera/TextureViewImplementation$initializePreview$1;->o0:Lcom/google/android/camera/TextureViewImplementation;

    .line 87
    .line 88
    invoke-static {p1}, Lcom/google/android/camera/TextureViewImplementation;->oo88o8O(Lcom/google/android/camera/TextureViewImplementation;)Lcom/google/android/camera/compat/view/SurfaceRequest;

    .line 89
    .line 90
    .line 91
    move-result-object p1

    .line 92
    if-eqz p1, :cond_1

    .line 93
    .line 94
    invoke-virtual {p1}, Lcom/google/android/camera/compat/view/SurfaceRequest;->〇8o8o〇()Lcom/google/android/camera/compat/view/DeferrableSurface;

    .line 95
    .line 96
    .line 97
    move-result-object p1

    .line 98
    if-eqz p1, :cond_1

    .line 99
    .line 100
    invoke-virtual {p1}, Lcom/google/android/camera/compat/view/DeferrableSurface;->〇o〇()V

    .line 101
    .line 102
    .line 103
    goto :goto_0

    .line 104
    :cond_0
    iget-object p1, p0, Lcom/google/android/camera/TextureViewImplementation$initializePreview$1;->o0:Lcom/google/android/camera/TextureViewImplementation;

    .line 105
    .line 106
    invoke-virtual {p1}, Lcom/google/android/camera/TextureViewImplementation;->〇0000OOO()V

    .line 107
    .line 108
    .line 109
    :cond_1
    :goto_0
    return-void
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 3
    .param p1    # Landroid/graphics/SurfaceTexture;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string/jumbo v0, "surfaceTexture"

    .line 2
    .line 3
    .line 4
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    const-string v0, "SurfaceTexture destroyed"

    .line 8
    .line 9
    const-string v1, "CameraX-TextureViewPreview"

    .line 10
    .line 11
    invoke-static {v1, v0}, Lcom/google/android/camera/log/CameraLog;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/google/android/camera/TextureViewImplementation$initializePreview$1;->o0:Lcom/google/android/camera/TextureViewImplementation;

    .line 15
    .line 16
    const/4 v2, 0x0

    .line 17
    invoke-static {v0, v2}, Lcom/google/android/camera/TextureViewImplementation;->o〇O8〇〇o(Lcom/google/android/camera/TextureViewImplementation;Landroid/graphics/SurfaceTexture;)V

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/google/android/camera/TextureViewImplementation$initializePreview$1;->o0:Lcom/google/android/camera/TextureViewImplementation;

    .line 21
    .line 22
    invoke-static {v0}, Lcom/google/android/camera/TextureViewImplementation;->〇O888o0o(Lcom/google/android/camera/TextureViewImplementation;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    if-eqz v0, :cond_0

    .line 27
    .line 28
    iget-object v0, p0, Lcom/google/android/camera/TextureViewImplementation$initializePreview$1;->o0:Lcom/google/android/camera/TextureViewImplementation;

    .line 29
    .line 30
    invoke-static {v0}, Lcom/google/android/camera/TextureViewImplementation;->〇O888o0o(Lcom/google/android/camera/TextureViewImplementation;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 35
    .line 36
    .line 37
    new-instance v1, Lcom/google/android/camera/TextureViewImplementation$initializePreview$1$onSurfaceTextureDestroyed$1;

    .line 38
    .line 39
    iget-object v2, p0, Lcom/google/android/camera/TextureViewImplementation$initializePreview$1;->o0:Lcom/google/android/camera/TextureViewImplementation;

    .line 40
    .line 41
    invoke-direct {v1, p1, v2}, Lcom/google/android/camera/TextureViewImplementation$initializePreview$1$onSurfaceTextureDestroyed$1;-><init>(Landroid/graphics/SurfaceTexture;Lcom/google/android/camera/TextureViewImplementation;)V

    .line 42
    .line 43
    .line 44
    iget-object v2, p0, Lcom/google/android/camera/TextureViewImplementation$initializePreview$1;->o0:Lcom/google/android/camera/TextureViewImplementation;

    .line 45
    .line 46
    invoke-virtual {v2}, Lcom/google/android/camera/compat/view/PreviewViewImplementation;->〇o〇()Landroid/view/ViewGroup;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 51
    .line 52
    .line 53
    move-result-object v2

    .line 54
    invoke-static {v2}, Landroidx/core/content/ContextCompat;->getMainExecutor(Landroid/content/Context;)Ljava/util/concurrent/Executor;

    .line 55
    .line 56
    .line 57
    move-result-object v2

    .line 58
    invoke-static {v0, v1, v2}, Lcom/google/android/camera/compat/utils/futures/Futures;->〇o00〇〇Oo(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/android/camera/compat/utils/futures/FutureCallback;Ljava/util/concurrent/Executor;)V

    .line 59
    .line 60
    .line 61
    iget-object v0, p0, Lcom/google/android/camera/TextureViewImplementation$initializePreview$1;->o0:Lcom/google/android/camera/TextureViewImplementation;

    .line 62
    .line 63
    invoke-static {v0, p1}, Lcom/google/android/camera/TextureViewImplementation;->〇oo〇(Lcom/google/android/camera/TextureViewImplementation;Landroid/graphics/SurfaceTexture;)V

    .line 64
    .line 65
    .line 66
    const/4 p1, 0x0

    .line 67
    return p1

    .line 68
    :cond_0
    const-string p1, "SurfaceTexture about to be destroyed"

    .line 69
    .line 70
    invoke-static {v1, p1}, Lcom/google/android/camera/log/CameraLog;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    const/4 p1, 0x1

    .line 74
    return p1
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 1
    .param p1    # Landroid/graphics/SurfaceTexture;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string/jumbo v0, "surfaceTexture"

    .line 2
    .line 3
    .line 4
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    new-instance p1, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v0, "SurfaceTexture size changed: "

    .line 13
    .line 14
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    const-string/jumbo p2, "x"

    .line 21
    .line 22
    .line 23
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    const-string p2, "CameraX-TextureViewPreview"

    .line 34
    .line 35
    invoke-static {p2, p1}, Lcom/google/android/camera/log/CameraLog;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 1
    .param p1    # Landroid/graphics/SurfaceTexture;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string/jumbo v0, "surfaceTexture"

    .line 2
    .line 3
    .line 4
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    iget-object p1, p0, Lcom/google/android/camera/TextureViewImplementation$initializePreview$1;->o0:Lcom/google/android/camera/TextureViewImplementation;

    .line 8
    .line 9
    invoke-static {p1}, Lcom/google/android/camera/TextureViewImplementation;->o800o8O(Lcom/google/android/camera/TextureViewImplementation;)Ljava/util/concurrent/atomic/AtomicReference;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    const/4 v0, 0x0

    .line 14
    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    check-cast p1, Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;

    .line 19
    .line 20
    if-eqz p1, :cond_0

    .line 21
    .line 22
    invoke-virtual {p1, v0}, Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;->set(Ljava/lang/Object;)Z

    .line 23
    .line 24
    .line 25
    :cond_0
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

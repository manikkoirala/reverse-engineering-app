.class public interface abstract Lcom/google/android/camera/compat/internal/CameraBurstCaptureCallback$CaptureSequenceCallback;
.super Ljava/lang/Object;
.source "CameraBurstCaptureCallback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/camera/compat/internal/CameraBurstCaptureCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CaptureSequenceCallback"
.end annotation


# virtual methods
.method public abstract onCaptureSequenceCompletedOrAborted(Landroid/hardware/camera2/CameraCaptureSession;IZ)V
    .param p1    # Landroid/hardware/camera2/CameraCaptureSession;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

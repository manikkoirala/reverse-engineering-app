.class public final Lcom/google/android/camera/compat/view/CameraCompatPreviewView$mSurfaceProvider$1;
.super Ljava/lang/Object;
.source "CameraCompatPreviewView.kt"

# interfaces
.implements Lcom/google/android/camera/compat/view/CameraCompatPreviewView$SurfaceProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/camera/compat/view/CameraCompatPreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic 〇080:Landroid/content/Context;

.field final synthetic 〇o00〇〇Oo:Lcom/google/android/camera/compat/view/CameraCompatPreviewView;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/camera/compat/view/CameraCompatPreviewView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView$mSurfaceProvider$1;->〇080:Landroid/content/Context;

    .line 2
    .line 3
    iput-object p2, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView$mSurfaceProvider$1;->〇o00〇〇Oo:Lcom/google/android/camera/compat/view/CameraCompatPreviewView;

    .line 4
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private static final O8(Lcom/google/android/camera/compat/view/CameraCompatPreviewView$mSurfaceProvider$1;Lcom/google/android/camera/compat/view/SurfaceRequest;)V
    .locals 1

    .line 1
    const-string/jumbo v0, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    const-string v0, "$request"

    .line 8
    .line 9
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p0, p1}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView$mSurfaceProvider$1;->〇080(Lcom/google/android/camera/compat/view/SurfaceRequest;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private static final Oo08(Lcom/google/android/camera/CameraViewImpl;Lcom/google/android/camera/compat/view/CameraCompatPreviewView;Lcom/google/android/camera/compat/view/SurfaceRequest;Lcom/google/android/camera/compat/view/SurfaceRequest$TransformationInfo;)V
    .locals 3

    .line 1
    const-string v0, "$camera"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string/jumbo v0, "this$0"

    .line 7
    .line 8
    .line 9
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    const-string v0, "$request"

    .line 13
    .line 14
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    const-string v0, "it"

    .line 18
    .line 19
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    .line 23
    .line 24
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 25
    .line 26
    .line 27
    const-string v1, "Preview transformation info updated. "

    .line 28
    .line 29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    const-string v1, "CameraX-CameraCompatPreview"

    .line 40
    .line 41
    invoke-static {v1, v0}, Lcom/google/android/camera/log/CameraLog;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {p0}, Lcom/google/android/camera/CameraViewImpl;->getCameraFacing()I

    .line 45
    .line 46
    .line 47
    move-result p0

    .line 48
    sget-object v0, Lcom/google/android/camera/data/CameraFacing;->〇o00〇〇Oo:Lcom/google/android/camera/data/CameraFacing$Companion;

    .line 49
    .line 50
    invoke-virtual {v0}, Lcom/google/android/camera/data/CameraFacing$Companion;->〇o00〇〇Oo()I

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    const/4 v1, 0x1

    .line 55
    const/4 v2, 0x0

    .line 56
    if-ne p0, v0, :cond_0

    .line 57
    .line 58
    const/4 p0, 0x1

    .line 59
    goto :goto_0

    .line 60
    :cond_0
    const/4 p0, 0x0

    .line 61
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->getMPreviewTransform()Lcom/google/android/camera/compat/view/PreviewTransformation;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    invoke-virtual {p2}, Lcom/google/android/camera/compat/view/SurfaceRequest;->〇O8o08O()Lcom/google/android/camera/size/CameraSize;

    .line 66
    .line 67
    .line 68
    move-result-object p2

    .line 69
    invoke-virtual {v0, p3, p2, p0}, Lcom/google/android/camera/compat/view/PreviewTransformation;->OO0o〇〇(Lcom/google/android/camera/compat/view/SurfaceRequest$TransformationInfo;Lcom/google/android/camera/size/CameraSize;Z)V

    .line 70
    .line 71
    .line 72
    invoke-virtual {p3}, Lcom/google/android/camera/compat/view/SurfaceRequest$TransformationInfo;->〇o〇()I

    .line 73
    .line 74
    .line 75
    move-result p0

    .line 76
    const/4 p2, -0x1

    .line 77
    if-eq p0, p2, :cond_2

    .line 78
    .line 79
    invoke-virtual {p1}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->getMImplementation()Lcom/google/android/camera/compat/view/PreviewViewImplementation;

    .line 80
    .line 81
    .line 82
    move-result-object p0

    .line 83
    if-eqz p0, :cond_1

    .line 84
    .line 85
    invoke-virtual {p1}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->getMImplementation()Lcom/google/android/camera/compat/view/PreviewViewImplementation;

    .line 86
    .line 87
    .line 88
    move-result-object p0

    .line 89
    instance-of p0, p0, Lcom/google/android/camera/SurfaceViewImplementation;

    .line 90
    .line 91
    if-eqz p0, :cond_1

    .line 92
    .line 93
    goto :goto_1

    .line 94
    :cond_1
    const/4 v1, 0x0

    .line 95
    :cond_2
    :goto_1
    invoke-virtual {p1, v1}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->setMUseDisplayRotation(Z)V

    .line 96
    .line 97
    .line 98
    invoke-virtual {p1}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇0〇O0088o()V

    .line 99
    .line 100
    .line 101
    invoke-virtual {p1}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇O8o08O()V

    .line 102
    .line 103
    .line 104
    return-void
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/google/android/camera/CameraViewImpl;Lcom/google/android/camera/compat/view/CameraCompatPreviewView;Lcom/google/android/camera/compat/view/SurfaceRequest;Lcom/google/android/camera/compat/view/SurfaceRequest$TransformationInfo;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView$mSurfaceProvider$1;->Oo08(Lcom/google/android/camera/CameraViewImpl;Lcom/google/android/camera/compat/view/CameraCompatPreviewView;Lcom/google/android/camera/compat/view/SurfaceRequest;Lcom/google/android/camera/compat/view/SurfaceRequest$TransformationInfo;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
.end method

.method public static synthetic 〇o〇(Lcom/google/android/camera/compat/view/CameraCompatPreviewView$mSurfaceProvider$1;Lcom/google/android/camera/compat/view/SurfaceRequest;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView$mSurfaceProvider$1;->O8(Lcom/google/android/camera/compat/view/CameraCompatPreviewView$mSurfaceProvider$1;Lcom/google/android/camera/compat/view/SurfaceRequest;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method


# virtual methods
.method public 〇080(Lcom/google/android/camera/compat/view/SurfaceRequest;)V
    .locals 4
    .param p1    # Lcom/google/android/camera/compat/view/SurfaceRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/AnyThread;
    .end annotation

    .line 1
    const-string v0, "request"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-nez v0, :cond_0

    .line 23
    .line 24
    iget-object v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView$mSurfaceProvider$1;->〇080:Landroid/content/Context;

    .line 25
    .line 26
    invoke-static {v0}, Landroidx/core/content/ContextCompat;->getMainExecutor(Landroid/content/Context;)Ljava/util/concurrent/Executor;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    new-instance v1, Lo〇O8〇〇o/〇o〇;

    .line 31
    .line 32
    invoke-direct {v1, p0, p1}, Lo〇O8〇〇o/〇o〇;-><init>(Lcom/google/android/camera/compat/view/CameraCompatPreviewView$mSurfaceProvider$1;Lcom/google/android/camera/compat/view/SurfaceRequest;)V

    .line 33
    .line 34
    .line 35
    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 36
    .line 37
    .line 38
    return-void

    .line 39
    :cond_0
    const-string v0, "CameraX-CameraCompatPreview"

    .line 40
    .line 41
    const-string v1, "Surface requested by Preview."

    .line 42
    .line 43
    invoke-static {v0, v1}, Lcom/google/android/camera/log/CameraLog;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {p1}, Lcom/google/android/camera/compat/view/SurfaceRequest;->OO0o〇〇〇〇0()Lcom/google/android/camera/CameraViewImpl;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    const-string v1, "request.camera"

    .line 51
    .line 52
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    iget-object v1, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView$mSurfaceProvider$1;->〇080:Landroid/content/Context;

    .line 56
    .line 57
    invoke-static {v1}, Landroidx/core/content/ContextCompat;->getMainExecutor(Landroid/content/Context;)Ljava/util/concurrent/Executor;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    iget-object v2, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView$mSurfaceProvider$1;->〇o00〇〇Oo:Lcom/google/android/camera/compat/view/CameraCompatPreviewView;

    .line 62
    .line 63
    new-instance v3, Lo〇O8〇〇o/O8;

    .line 64
    .line 65
    invoke-direct {v3, v0, v2, p1}, Lo〇O8〇〇o/O8;-><init>(Lcom/google/android/camera/CameraViewImpl;Lcom/google/android/camera/compat/view/CameraCompatPreviewView;Lcom/google/android/camera/compat/view/SurfaceRequest;)V

    .line 66
    .line 67
    .line 68
    invoke-virtual {p1, v1, v3}, Lcom/google/android/camera/compat/view/SurfaceRequest;->〇O888o0o(Ljava/util/concurrent/Executor;Lcom/google/android/camera/compat/view/SurfaceRequest$TransformationInfoListener;)V

    .line 69
    .line 70
    .line 71
    iget-object v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView$mSurfaceProvider$1;->〇o00〇〇Oo:Lcom/google/android/camera/compat/view/CameraCompatPreviewView;

    .line 72
    .line 73
    invoke-static {v0}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->O8(Lcom/google/android/camera/compat/view/CameraCompatPreviewView;)Lcom/google/android/camera/CameraViewImpl;

    .line 74
    .line 75
    .line 76
    move-result-object v1

    .line 77
    if-eqz v1, :cond_1

    .line 78
    .line 79
    invoke-interface {v1}, Lcom/google/android/camera/ICamera;->getCameraApi()I

    .line 80
    .line 81
    .line 82
    move-result v1

    .line 83
    goto :goto_0

    .line 84
    :cond_1
    sget-object v1, Lcom/google/android/camera/data/CameraApi;->〇080:Lcom/google/android/camera/data/CameraApi$Companion;

    .line 85
    .line 86
    invoke-virtual {v1}, Lcom/google/android/camera/data/CameraApi$Companion;->〇080()I

    .line 87
    .line 88
    .line 89
    move-result v1

    .line 90
    :goto_0
    iget-object v2, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView$mSurfaceProvider$1;->〇o00〇〇Oo:Lcom/google/android/camera/compat/view/CameraCompatPreviewView;

    .line 91
    .line 92
    invoke-virtual {v2}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->getMImplementationMode()I

    .line 93
    .line 94
    .line 95
    move-result v2

    .line 96
    invoke-static {v0, v1, v2}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇o〇(Lcom/google/android/camera/compat/view/CameraCompatPreviewView;II)Lcom/google/android/camera/compat/view/PreviewViewImplementation;

    .line 97
    .line 98
    .line 99
    move-result-object v1

    .line 100
    invoke-virtual {v0, v1}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->setMImplementation(Lcom/google/android/camera/compat/view/PreviewViewImplementation;)V

    .line 101
    .line 102
    .line 103
    iget-object v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView$mSurfaceProvider$1;->〇o00〇〇Oo:Lcom/google/android/camera/compat/view/CameraCompatPreviewView;

    .line 104
    .line 105
    invoke-virtual {v0}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->getMImplementation()Lcom/google/android/camera/compat/view/PreviewViewImplementation;

    .line 106
    .line 107
    .line 108
    move-result-object v0

    .line 109
    if-eqz v0, :cond_2

    .line 110
    .line 111
    invoke-virtual {v0, p1}, Lcom/google/android/camera/compat/view/PreviewViewImplementation;->OO0o〇〇(Lcom/google/android/camera/compat/view/SurfaceRequest;)V

    .line 112
    .line 113
    .line 114
    :cond_2
    return-void
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

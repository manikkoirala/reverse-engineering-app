.class public final Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy$CacheAnalyzingImageProxy;
.super Lcom/google/android/camera/compat/imagereader/ForwardingImageProxy;
.source "CameraImageReaderProxy.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CacheAnalyzingImageProxy"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final OO:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/camera/compat/imagereader/ImageProxy;Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;)V
    .locals 1
    .param p1    # Lcom/google/android/camera/compat/imagereader/ImageProxy;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "image"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "nonBlockingAnalyzer"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0, p1}, Lcom/google/android/camera/compat/imagereader/ForwardingImageProxy;-><init>(Lcom/google/android/camera/compat/imagereader/ImageProxy;)V

    .line 12
    .line 13
    .line 14
    new-instance p1, Ljava/lang/ref/WeakReference;

    .line 15
    .line 16
    invoke-direct {p1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 17
    .line 18
    .line 19
    iput-object p1, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy$CacheAnalyzingImageProxy;->OO:Ljava/lang/ref/WeakReference;

    .line 20
    .line 21
    new-instance p1, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy$CacheAnalyzingImageProxy$1;

    .line 22
    .line 23
    invoke-direct {p1, p0}, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy$CacheAnalyzingImageProxy$1;-><init>(Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy$CacheAnalyzingImageProxy;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0, p1}, Lcom/google/android/camera/compat/imagereader/ForwardingImageProxy;->Oo08(Lcom/google/android/camera/compat/imagereader/ForwardingImageProxy$OnImageCloseListener;)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method


# virtual methods
.method public final oO80()Ljava/lang/ref/WeakReference;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/ref/WeakReference<",
            "Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy$CacheAnalyzingImageProxy;->OO:Ljava/lang/ref/WeakReference;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.class public Lcom/google/android/camera/compat/workaround/MeteringRegionCorrection;
.super Ljava/lang/Object;
.source "MeteringRegionCorrection.java"


# instance fields
.field private final 〇080:Lcom/google/android/camera/compat/quirk/Quirks;


# direct methods
.method public constructor <init>(Lcom/google/android/camera/compat/quirk/Quirks;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/google/android/camera/compat/workaround/MeteringRegionCorrection;->〇080:Lcom/google/android/camera/compat/quirk/Quirks;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public 〇080(Lcom/google/android/camera/compat/internal/focus/MeteringPoint;I)Landroid/graphics/PointF;
    .locals 2
    .param p1    # Lcom/google/android/camera/compat/internal/focus/MeteringPoint;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p2, v0, :cond_0

    .line 3
    .line 4
    iget-object p2, p0, Lcom/google/android/camera/compat/workaround/MeteringRegionCorrection;->〇080:Lcom/google/android/camera/compat/quirk/Quirks;

    .line 5
    .line 6
    if-eqz p2, :cond_0

    .line 7
    .line 8
    const-class v0, Lcom/google/android/camera/compat/quirk/AfRegionFlipHorizontallyQuirk;

    .line 9
    .line 10
    invoke-virtual {p2, v0}, Lcom/google/android/camera/compat/quirk/Quirks;->〇080(Ljava/lang/Class;)Z

    .line 11
    .line 12
    .line 13
    move-result p2

    .line 14
    if-eqz p2, :cond_0

    .line 15
    .line 16
    new-instance p2, Landroid/graphics/PointF;

    .line 17
    .line 18
    invoke-virtual {p1}, Lcom/google/android/camera/compat/internal/focus/MeteringPoint;->〇o〇()F

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    const/high16 v1, 0x3f800000    # 1.0f

    .line 23
    .line 24
    sub-float/2addr v1, v0

    .line 25
    invoke-virtual {p1}, Lcom/google/android/camera/compat/internal/focus/MeteringPoint;->O8()F

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    invoke-direct {p2, v1, p1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 30
    .line 31
    .line 32
    return-object p2

    .line 33
    :cond_0
    new-instance p2, Landroid/graphics/PointF;

    .line 34
    .line 35
    invoke-virtual {p1}, Lcom/google/android/camera/compat/internal/focus/MeteringPoint;->〇o〇()F

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    invoke-virtual {p1}, Lcom/google/android/camera/compat/internal/focus/MeteringPoint;->O8()F

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    invoke-direct {p2, v0, p1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 44
    .line 45
    .line 46
    return-object p2
    .line 47
    .line 48
    .line 49
.end method

.class public final Lcom/google/android/camera/compat/cscompat/Camera2ZoomControl;
.super Ljava/lang/Object;
.source "Camera2ZoomControl.kt"

# interfaces
.implements Lcom/google/android/camera/compat/cscompat/Camera2ZoomImpl;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/camera/compat/cscompat/Camera2ZoomControl$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇o〇:Lcom/google/android/camera/compat/cscompat/Camera2ZoomControl$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final 〇080:Lcom/google/android/camera/compat/cscompat/Camera2ZoomImpl;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/google/android/camera/compat/cscompat/Camera2ZoomControl$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/google/android/camera/compat/cscompat/Camera2ZoomControl$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/google/android/camera/compat/cscompat/Camera2ZoomControl;->〇o〇:Lcom/google/android/camera/compat/cscompat/Camera2ZoomControl$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public constructor <init>(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)V
    .locals 1
    .param p1    # Lcom/google/android/camera/compat/CameraCharacteristicsCompat;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "cameraCharacteristics"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0, p1}, Lcom/google/android/camera/compat/cscompat/Camera2ZoomControl;->〇080(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)Lcom/google/android/camera/compat/cscompat/Camera2ZoomImpl;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    iput-object p1, p0, Lcom/google/android/camera/compat/cscompat/Camera2ZoomControl;->〇080:Lcom/google/android/camera/compat/cscompat/Camera2ZoomImpl;

    .line 14
    .line 15
    sget-object p1, Lcom/google/android/camera/lifecycle/Camera2CameraFactory;->〇o00〇〇Oo:Lcom/google/android/camera/lifecycle/Camera2CameraFactory;

    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/google/android/camera/lifecycle/CompatCameraFactory;->〇O〇()I

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    iput p1, p0, Lcom/google/android/camera/compat/cscompat/Camera2ZoomControl;->〇o00〇〇Oo:I

    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private final Oo08(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)Landroid/util/Range;
    .locals 2
    .annotation build Landroidx/annotation/RequiresApi;
        value = 0x1e
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/camera/compat/CameraCharacteristicsCompat;",
            ")",
            "Landroid/util/Range<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .line 1
    :try_start_0
    invoke-static {}, Landroidx/camera/camera2/internal/〇080;->〇080()Landroid/hardware/camera2/CameraCharacteristics$Key;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p1, v0}, Lcom/google/android/camera/compat/CameraCharacteristicsCompat;->〇080(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    check-cast p1, Landroid/util/Range;
    :try_end_0
    .catch Ljava/lang/AssertionError; {:try_start_0 .. :try_end_0} :catch_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :catch_0
    move-exception p1

    .line 13
    const-string v0, "CameraX-Camera2ZoomControl"

    .line 14
    .line 15
    const-string v1, "AssertionError, fail to get camera characteristic."

    .line 16
    .line 17
    invoke-static {v0, v1, p1}, Lcom/google/android/camera/log/CameraLog;->〇O8o08O(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 18
    .line 19
    .line 20
    const/4 p1, 0x0

    .line 21
    :goto_0
    return-object p1
    .line 22
.end method

.method private final o〇0(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)Z
    .locals 2

    .line 1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 2
    .line 3
    const/16 v1, 0x1e

    .line 4
    .line 5
    if-lt v0, v1, :cond_0

    .line 6
    .line 7
    invoke-direct {p0, p1}, Lcom/google/android/camera/compat/cscompat/Camera2ZoomControl;->Oo08(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)Landroid/util/Range;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    if-eqz p1, :cond_0

    .line 12
    .line 13
    const/4 p1, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 p1, 0x0

    .line 16
    :goto_0
    return p1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final 〇080(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)Lcom/google/android/camera/compat/cscompat/Camera2ZoomImpl;
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/google/android/camera/compat/cscompat/Camera2ZoomControl;->o〇0(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    new-instance v0, Lcom/google/android/camera/compat/cscompat/AndroidRCamera2ZoomImpl;

    .line 8
    .line 9
    invoke-direct {v0, p1}, Lcom/google/android/camera/compat/cscompat/AndroidRCamera2ZoomImpl;-><init>(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)V

    .line 10
    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    new-instance v0, Lcom/google/android/camera/compat/cscompat/CropRegionCamera2ZoomImpl;

    .line 14
    .line 15
    invoke-direct {v0, p1}, Lcom/google/android/camera/compat/cscompat/CropRegionCamera2ZoomImpl;-><init>(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-object v0
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public final O8(F)V
    .locals 3

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/android/camera/compat/cscompat/Camera2ZoomControl;->〇o〇(F)F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v2, "setLinearZoom ZoomLevel = "

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string p1, ", real zoomRatio = "

    .line 19
    .line 20
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    const-string v0, "CameraX-Camera2ZoomControl"

    .line 31
    .line 32
    invoke-static {v0, p1}, Lcom/google/android/camera/log/CameraLog;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public getMaxZoom()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/cscompat/Camera2ZoomControl;->〇080:Lcom/google/android/camera/compat/cscompat/Camera2ZoomImpl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/google/android/camera/compat/cscompat/Camera2ZoomImpl;->getMaxZoom()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getMinZoom()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/cscompat/Camera2ZoomControl;->〇080:Lcom/google/android/camera/compat/cscompat/Camera2ZoomImpl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/google/android/camera/compat/cscompat/Camera2ZoomImpl;->getMinZoom()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getZoomRange()Landroid/util/Range;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Range<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/cscompat/Camera2ZoomControl;->〇080:Lcom/google/android/camera/compat/cscompat/Camera2ZoomImpl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/google/android/camera/compat/cscompat/Camera2ZoomImpl;->getZoomRange()Landroid/util/Range;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final 〇o00〇〇Oo(F)F
    .locals 3

    .line 1
    sget-object v0, Lcom/google/android/camera/util/CameraSizeUtils;->〇080:Lcom/google/android/camera/util/CameraSizeUtils$Companion;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/google/android/camera/compat/cscompat/Camera2ZoomControl;->getMinZoom()F

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-virtual {p0}, Lcom/google/android/camera/compat/cscompat/Camera2ZoomControl;->getMaxZoom()F

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/camera/util/CameraSizeUtils$Companion;->〇O〇(FFF)F

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    return p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final 〇o〇(F)F
    .locals 3

    .line 1
    sget-object v0, Lcom/google/android/camera/util/CameraSizeUtils;->〇080:Lcom/google/android/camera/util/CameraSizeUtils$Companion;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/google/android/camera/compat/cscompat/Camera2ZoomControl;->getMinZoom()F

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-virtual {p0}, Lcom/google/android/camera/compat/cscompat/Camera2ZoomControl;->getMaxZoom()F

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/camera/util/CameraSizeUtils$Companion;->〇〇8O0〇8(FFF)F

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    return p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

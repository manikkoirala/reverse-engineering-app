.class public Lcom/google/android/camera/compat/quirk/CameraNoResponseWhenEnablingFlashQuirk;
.super Ljava/lang/Object;
.source "CameraNoResponseWhenEnablingFlashQuirk.java"

# interfaces
.implements Lcom/google/android/camera/compat/quirk/Quirk;


# static fields
.field public static final 〇080:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 19

    .line 1
    const-string v0, "SM-N9200"

    .line 2
    .line 3
    const-string v1, "SM-N9208"

    .line 4
    .line 5
    const-string v2, "SAMSUNG-SM-N920A"

    .line 6
    .line 7
    const-string v3, "SM-N920C"

    .line 8
    .line 9
    const-string v4, "SM-N920F"

    .line 10
    .line 11
    const-string v5, "SM-N920G"

    .line 12
    .line 13
    const-string v6, "SM-N920I"

    .line 14
    .line 15
    const-string v7, "SM-N920K"

    .line 16
    .line 17
    const-string v8, "SM-N920L"

    .line 18
    .line 19
    const-string v9, "SM-N920P"

    .line 20
    .line 21
    const-string v10, "SM-N920R4"

    .line 22
    .line 23
    const-string v11, "SM-N920R6"

    .line 24
    .line 25
    const-string v12, "SM-N920R7"

    .line 26
    .line 27
    const-string v13, "SM-N920S"

    .line 28
    .line 29
    const-string v14, "SM-N920T"

    .line 30
    .line 31
    const-string v15, "SM-N920V"

    .line 32
    .line 33
    const-string v16, "SM-N920W8"

    .line 34
    .line 35
    const-string v17, "SM-N920X"

    .line 36
    .line 37
    const-string v18, "SM-J510FN"

    .line 38
    .line 39
    filled-new-array/range {v0 .. v18}, [Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    sput-object v0, Lcom/google/android/camera/compat/quirk/CameraNoResponseWhenEnablingFlashQuirk;->〇080:Ljava/util/List;

    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method static 〇080(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)Z
    .locals 3
    .param p0    # Lcom/google/android/camera/compat/CameraCharacteristicsCompat;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    sget-object v0, Landroid/hardware/camera2/CameraCharacteristics;->LENS_FACING:Landroid/hardware/camera2/CameraCharacteristics$Key;

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/google/android/camera/compat/CameraCharacteristicsCompat;->〇080(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Ljava/lang/Integer;

    .line 8
    .line 9
    sget-object v0, Landroid/os/Build;->BRAND:Ljava/lang/String;

    .line 10
    .line 11
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    const-string v2, "SAMSUNG"

    .line 18
    .line 19
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-eqz v0, :cond_0

    .line 24
    .line 25
    sget-object v0, Lcom/google/android/camera/compat/quirk/CameraNoResponseWhenEnablingFlashQuirk;->〇080:Ljava/util/List;

    .line 26
    .line 27
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 28
    .line 29
    invoke-virtual {v2, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-eqz v0, :cond_0

    .line 38
    .line 39
    if-eqz p0, :cond_0

    .line 40
    .line 41
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    .line 42
    .line 43
    .line 44
    move-result p0

    .line 45
    const/4 v0, 0x1

    .line 46
    if-ne p0, v0, :cond_0

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_0
    const/4 v0, 0x0

    .line 50
    :goto_0
    return v0
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.class public final Lcom/google/android/camera/compat/quirk/DeviceQuirksLoader$Companion;
.super Ljava/lang/Object;
.source "DeviceQuirksLoader.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/camera/compat/quirk/DeviceQuirksLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/google/android/camera/compat/quirk/DeviceQuirksLoader$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final 〇080()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/google/android/camera/compat/quirk/Quirk;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/google/android/camera/compat/quirk/ImageCapturePixelHDRPlusQuirk;->〇080:Lcom/google/android/camera/compat/quirk/ImageCapturePixelHDRPlusQuirk$Companion;

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/google/android/camera/compat/quirk/ImageCapturePixelHDRPlusQuirk$Companion;->〇080()Z

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    if-eqz v1, :cond_0

    .line 13
    .line 14
    new-instance v1, Lcom/google/android/camera/compat/quirk/ImageCapturePixelHDRPlusQuirk;

    .line 15
    .line 16
    invoke-direct {v1}, Lcom/google/android/camera/compat/quirk/ImageCapturePixelHDRPlusQuirk;-><init>()V

    .line 17
    .line 18
    .line 19
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 20
    .line 21
    .line 22
    :cond_0
    sget-object v1, Lcom/google/android/camera/compat/quirk/ExtraCroppingQuirk;->〇080:Lcom/google/android/camera/compat/quirk/ExtraCroppingQuirk$Companion;

    .line 23
    .line 24
    invoke-virtual {v1}, Lcom/google/android/camera/compat/quirk/ExtraCroppingQuirk$Companion;->〇o00〇〇Oo()Z

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    if-eqz v1, :cond_1

    .line 29
    .line 30
    new-instance v1, Lcom/google/android/camera/compat/quirk/ExtraCroppingQuirk;

    .line 31
    .line 32
    invoke-direct {v1}, Lcom/google/android/camera/compat/quirk/ExtraCroppingQuirk;-><init>()V

    .line 33
    .line 34
    .line 35
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    :cond_1
    sget-object v1, Lcom/google/android/camera/compat/quirk/ExcludedSupportedSizesQuirk;->〇080:Lcom/google/android/camera/compat/quirk/ExcludedSupportedSizesQuirk$Companion;

    .line 39
    .line 40
    invoke-virtual {v1}, Lcom/google/android/camera/compat/quirk/ExcludedSupportedSizesQuirk$Companion;->〇80〇808〇O()Z

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    if-eqz v1, :cond_2

    .line 45
    .line 46
    new-instance v1, Lcom/google/android/camera/compat/quirk/ExcludedSupportedSizesQuirk;

    .line 47
    .line 48
    invoke-direct {v1}, Lcom/google/android/camera/compat/quirk/ExcludedSupportedSizesQuirk;-><init>()V

    .line 49
    .line 50
    .line 51
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    .line 53
    .line 54
    :cond_2
    sget-object v1, Lcom/google/android/camera/compat/quirk/CrashWhenTakingPhotoWithAutoFlashAEModeQuirk;->〇080:Lcom/google/android/camera/compat/quirk/CrashWhenTakingPhotoWithAutoFlashAEModeQuirk$Companion;

    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/google/android/camera/compat/quirk/CrashWhenTakingPhotoWithAutoFlashAEModeQuirk$Companion;->〇080()Z

    .line 57
    .line 58
    .line 59
    move-result v1

    .line 60
    if-eqz v1, :cond_3

    .line 61
    .line 62
    new-instance v1, Lcom/google/android/camera/compat/quirk/CrashWhenTakingPhotoWithAutoFlashAEModeQuirk;

    .line 63
    .line 64
    invoke-direct {v1}, Lcom/google/android/camera/compat/quirk/CrashWhenTakingPhotoWithAutoFlashAEModeQuirk;-><init>()V

    .line 65
    .line 66
    .line 67
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    .line 69
    .line 70
    :cond_3
    sget-object v1, Lcom/google/android/camera/compat/quirk/PreviewPixelHDRnetQuirk;->〇080:Lcom/google/android/camera/compat/quirk/PreviewPixelHDRnetQuirk$Companion;

    .line 71
    .line 72
    invoke-virtual {v1}, Lcom/google/android/camera/compat/quirk/PreviewPixelHDRnetQuirk$Companion;->〇080()Z

    .line 73
    .line 74
    .line 75
    move-result v1

    .line 76
    if-eqz v1, :cond_4

    .line 77
    .line 78
    new-instance v1, Lcom/google/android/camera/compat/quirk/PreviewPixelHDRnetQuirk;

    .line 79
    .line 80
    invoke-direct {v1}, Lcom/google/android/camera/compat/quirk/PreviewPixelHDRnetQuirk;-><init>()V

    .line 81
    .line 82
    .line 83
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    .line 85
    .line 86
    :cond_4
    sget-object v1, Lcom/google/android/camera/compat/quirk/StillCaptureFlashStopRepeatingQuirk;->〇080:Lcom/google/android/camera/compat/quirk/StillCaptureFlashStopRepeatingQuirk$Companion;

    .line 87
    .line 88
    invoke-virtual {v1}, Lcom/google/android/camera/compat/quirk/StillCaptureFlashStopRepeatingQuirk$Companion;->〇080()Z

    .line 89
    .line 90
    .line 91
    move-result v1

    .line 92
    if-eqz v1, :cond_5

    .line 93
    .line 94
    new-instance v1, Lcom/google/android/camera/compat/quirk/StillCaptureFlashStopRepeatingQuirk;

    .line 95
    .line 96
    invoke-direct {v1}, Lcom/google/android/camera/compat/quirk/StillCaptureFlashStopRepeatingQuirk;-><init>()V

    .line 97
    .line 98
    .line 99
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    .line 101
    .line 102
    :cond_5
    sget-object v1, Lcom/google/android/camera/compat/quirk/ExtraSupportedSurfaceCombinationsQuirk;->〇080:Lcom/google/android/camera/compat/quirk/ExtraSupportedSurfaceCombinationsQuirk$Companion;

    .line 103
    .line 104
    invoke-virtual {v1}, Lcom/google/android/camera/compat/quirk/ExtraSupportedSurfaceCombinationsQuirk$Companion;->o〇0()Z

    .line 105
    .line 106
    .line 107
    move-result v1

    .line 108
    if-eqz v1, :cond_6

    .line 109
    .line 110
    new-instance v1, Lcom/google/android/camera/compat/quirk/ExtraSupportedSurfaceCombinationsQuirk;

    .line 111
    .line 112
    invoke-direct {v1}, Lcom/google/android/camera/compat/quirk/ExtraSupportedSurfaceCombinationsQuirk;-><init>()V

    .line 113
    .line 114
    .line 115
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    .line 117
    .line 118
    :cond_6
    sget-object v1, Lcom/google/android/camera/compat/quirk/FlashAvailabilityBufferUnderflowQuirk;->〇080:Lcom/google/android/camera/compat/quirk/FlashAvailabilityBufferUnderflowQuirk$Companion;

    .line 119
    .line 120
    invoke-virtual {v1}, Lcom/google/android/camera/compat/quirk/FlashAvailabilityBufferUnderflowQuirk$Companion;->〇080()Z

    .line 121
    .line 122
    .line 123
    move-result v1

    .line 124
    if-eqz v1, :cond_7

    .line 125
    .line 126
    new-instance v1, Lcom/google/android/camera/compat/quirk/FlashAvailabilityBufferUnderflowQuirk;

    .line 127
    .line 128
    invoke-direct {v1}, Lcom/google/android/camera/compat/quirk/FlashAvailabilityBufferUnderflowQuirk;-><init>()V

    .line 129
    .line 130
    .line 131
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    .line 133
    .line 134
    :cond_7
    sget-object v1, Lcom/google/android/camera/compat/quirk/TextureViewIsClosedQuirk;->〇080:Lcom/google/android/camera/compat/quirk/TextureViewIsClosedQuirk$Companion;

    .line 135
    .line 136
    invoke-virtual {v1}, Lcom/google/android/camera/compat/quirk/TextureViewIsClosedQuirk$Companion;->〇080()Z

    .line 137
    .line 138
    .line 139
    move-result v1

    .line 140
    if-eqz v1, :cond_8

    .line 141
    .line 142
    new-instance v1, Lcom/google/android/camera/compat/quirk/TextureViewIsClosedQuirk;

    .line 143
    .line 144
    invoke-direct {v1}, Lcom/google/android/camera/compat/quirk/TextureViewIsClosedQuirk;-><init>()V

    .line 145
    .line 146
    .line 147
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    .line 149
    .line 150
    :cond_8
    sget-object v1, Lcom/google/android/camera/compat/quirk/CaptureSessionOnClosedNotCalledQuirk;->〇080:Lcom/google/android/camera/compat/quirk/CaptureSessionOnClosedNotCalledQuirk$Companion;

    .line 151
    .line 152
    invoke-virtual {v1}, Lcom/google/android/camera/compat/quirk/CaptureSessionOnClosedNotCalledQuirk$Companion;->〇080()Z

    .line 153
    .line 154
    .line 155
    move-result v1

    .line 156
    if-eqz v1, :cond_9

    .line 157
    .line 158
    new-instance v1, Lcom/google/android/camera/compat/quirk/CaptureSessionOnClosedNotCalledQuirk;

    .line 159
    .line 160
    invoke-direct {v1}, Lcom/google/android/camera/compat/quirk/CaptureSessionOnClosedNotCalledQuirk;-><init>()V

    .line 161
    .line 162
    .line 163
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    .line 165
    .line 166
    :cond_9
    invoke-static {}, Lcom/google/android/camera/compat/quirk/SurfaceViewStretchedQuirk;->〇o〇()Z

    .line 167
    .line 168
    .line 169
    move-result v1

    .line 170
    if-eqz v1, :cond_a

    .line 171
    .line 172
    new-instance v1, Lcom/google/android/camera/compat/quirk/SurfaceViewStretchedQuirk;

    .line 173
    .line 174
    invoke-direct {v1}, Lcom/google/android/camera/compat/quirk/SurfaceViewStretchedQuirk;-><init>()V

    .line 175
    .line 176
    .line 177
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 178
    .line 179
    .line 180
    :cond_a
    invoke-static {}, Lcom/google/android/camera/compat/quirk/SurfaceViewNotCroppedByParentQuirk;->〇080()Z

    .line 181
    .line 182
    .line 183
    move-result v1

    .line 184
    if-eqz v1, :cond_b

    .line 185
    .line 186
    new-instance v1, Lcom/google/android/camera/compat/quirk/SurfaceViewNotCroppedByParentQuirk;

    .line 187
    .line 188
    invoke-direct {v1}, Lcom/google/android/camera/compat/quirk/SurfaceViewNotCroppedByParentQuirk;-><init>()V

    .line 189
    .line 190
    .line 191
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 192
    .line 193
    .line 194
    :cond_b
    return-object v0
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

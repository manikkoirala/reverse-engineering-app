.class public final Lcom/google/android/camera/compat/workaround/ImageCapturePixelHDRPlus$Companion;
.super Ljava/lang/Object;
.source "ImageCapturePixelHDRPlus.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/camera/compat/workaround/ImageCapturePixelHDRPlus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/google/android/camera/compat/workaround/ImageCapturePixelHDRPlus$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final 〇080(Lcom/google/android/camera/Camera2Config;ILcom/google/android/camera/compat/CameraCharacteristicsCompat;)V
    .locals 1
    .param p1    # Lcom/google/android/camera/Camera2Config;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string p3, "config"

    .line 2
    .line 3
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget p3, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 7
    .line 8
    const/16 v0, 0x1a

    .line 9
    .line 10
    if-lt p3, v0, :cond_2

    .line 11
    .line 12
    sget-object p3, Lcom/google/android/camera/compat/quirk/DeviceQuirks;->〇080:Lcom/google/android/camera/compat/quirk/DeviceQuirks;

    .line 13
    .line 14
    const-class v0, Lcom/google/android/camera/compat/quirk/ImageCapturePixelHDRPlusQuirk;

    .line 15
    .line 16
    invoke-virtual {p3, v0}, Lcom/google/android/camera/compat/quirk/DeviceQuirks;->〇080(Ljava/lang/Class;)Lcom/google/android/camera/compat/quirk/Quirk;

    .line 17
    .line 18
    .line 19
    move-result-object p3

    .line 20
    check-cast p3, Lcom/google/android/camera/compat/quirk/ImageCapturePixelHDRPlusQuirk;

    .line 21
    .line 22
    if-nez p3, :cond_0

    .line 23
    .line 24
    return-void

    .line 25
    :cond_0
    const/16 p3, 0x64

    .line 26
    .line 27
    const-string v0, "CONTROL_ENABLE_ZSL"

    .line 28
    .line 29
    if-ne p2, p3, :cond_1

    .line 30
    .line 31
    invoke-static {}, Landroidx/camera/camera2/internal/compat/workaround/〇080;->〇080()Landroid/hardware/camera2/CaptureRequest$Key;

    .line 32
    .line 33
    .line 34
    move-result-object p2

    .line 35
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    sget-object p3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 39
    .line 40
    invoke-virtual {p1, p2, p3}, Lcom/google/android/camera/Camera2Config;->OO0o〇〇〇〇0(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_1
    invoke-static {}, Landroidx/camera/camera2/internal/compat/workaround/〇080;->〇080()Landroid/hardware/camera2/CaptureRequest$Key;

    .line 45
    .line 46
    .line 47
    move-result-object p2

    .line 48
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    sget-object p3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 52
    .line 53
    invoke-virtual {p1, p2, p3}, Lcom/google/android/camera/Camera2Config;->OO0o〇〇〇〇0(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 54
    .line 55
    .line 56
    :cond_2
    :goto_0
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.class public final Lcom/google/android/camera/compat/CameraDeviceCompat;
.super Ljava/lang/Object;
.source "CameraDeviceCompat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/camera/compat/CameraDeviceCompat$CameraDeviceCompatImpl;,
        Lcom/google/android/camera/compat/CameraDeviceCompat$StateCallbackExecutorWrapper;
    }
.end annotation


# instance fields
.field private final 〇080:Lcom/google/android/camera/compat/CameraDeviceCompat$CameraDeviceCompatImpl;


# direct methods
.method private constructor <init>(Landroid/hardware/camera2/CameraDevice;Landroid/os/Handler;)V
    .locals 2
    .param p1    # Landroid/hardware/camera2/CameraDevice;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/os/Handler;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 5
    .line 6
    const/16 v1, 0x1c

    .line 7
    .line 8
    if-lt v0, v1, :cond_0

    .line 9
    .line 10
    new-instance p2, Lcom/google/android/camera/compat/CameraDeviceCompatApi28Impl;

    .line 11
    .line 12
    invoke-direct {p2, p1}, Lcom/google/android/camera/compat/CameraDeviceCompatApi28Impl;-><init>(Landroid/hardware/camera2/CameraDevice;)V

    .line 13
    .line 14
    .line 15
    iput-object p2, p0, Lcom/google/android/camera/compat/CameraDeviceCompat;->〇080:Lcom/google/android/camera/compat/CameraDeviceCompat$CameraDeviceCompatImpl;

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/16 v1, 0x18

    .line 19
    .line 20
    if-lt v0, v1, :cond_1

    .line 21
    .line 22
    invoke-static {p1, p2}, Lcom/google/android/camera/compat/CameraDeviceCompatApi24Impl;->〇80〇808〇O(Landroid/hardware/camera2/CameraDevice;Landroid/os/Handler;)Lcom/google/android/camera/compat/CameraDeviceCompatApi24Impl;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    iput-object p1, p0, Lcom/google/android/camera/compat/CameraDeviceCompat;->〇080:Lcom/google/android/camera/compat/CameraDeviceCompat$CameraDeviceCompatImpl;

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_1
    const/16 v1, 0x17

    .line 30
    .line 31
    if-lt v0, v1, :cond_2

    .line 32
    .line 33
    invoke-static {p1, p2}, Lcom/google/android/camera/compat/CameraDeviceCompatApi23Impl;->oO80(Landroid/hardware/camera2/CameraDevice;Landroid/os/Handler;)Lcom/google/android/camera/compat/CameraDeviceCompatApi23Impl;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    iput-object p1, p0, Lcom/google/android/camera/compat/CameraDeviceCompat;->〇080:Lcom/google/android/camera/compat/CameraDeviceCompat$CameraDeviceCompatImpl;

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_2
    invoke-static {p1, p2}, Lcom/google/android/camera/compat/CameraDeviceCompatBaseImpl;->Oo08(Landroid/hardware/camera2/CameraDevice;Landroid/os/Handler;)Lcom/google/android/camera/compat/CameraDeviceCompatBaseImpl;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    iput-object p1, p0, Lcom/google/android/camera/compat/CameraDeviceCompat;->〇080:Lcom/google/android/camera/compat/CameraDeviceCompat$CameraDeviceCompatImpl;

    .line 45
    .line 46
    :goto_0
    return-void
    .line 47
    .line 48
    .line 49
.end method

.method public static O8(Landroid/hardware/camera2/CameraDevice;)Lcom/google/android/camera/compat/CameraDeviceCompat;
    .locals 1
    .param p0    # Landroid/hardware/camera2/CameraDevice;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    invoke-static {}, Lcom/google/android/camera/compat/imagereader/MainThreadAsyncHandler;->〇080()Landroid/os/Handler;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {p0, v0}, Lcom/google/android/camera/compat/CameraDeviceCompat;->Oo08(Landroid/hardware/camera2/CameraDevice;Landroid/os/Handler;)Lcom/google/android/camera/compat/CameraDeviceCompat;

    .line 6
    .line 7
    .line 8
    move-result-object p0

    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static Oo08(Landroid/hardware/camera2/CameraDevice;Landroid/os/Handler;)Lcom/google/android/camera/compat/CameraDeviceCompat;
    .locals 1
    .param p0    # Landroid/hardware/camera2/CameraDevice;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Landroid/os/Handler;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/google/android/camera/compat/CameraDeviceCompat;

    .line 2
    .line 3
    invoke-direct {v0, p0, p1}, Lcom/google/android/camera/compat/CameraDeviceCompat;-><init>(Landroid/hardware/camera2/CameraDevice;Landroid/os/Handler;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method


# virtual methods
.method public 〇080(I)Landroid/hardware/camera2/CaptureRequest$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/camera/compat/exception/CameraAccessExceptionCompat;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/CameraDeviceCompat;->〇080:Lcom/google/android/camera/compat/CameraDeviceCompat$CameraDeviceCompatImpl;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/google/android/camera/compat/CameraDeviceCompat$CameraDeviceCompatImpl;->〇o00〇〇Oo(I)Landroid/hardware/camera2/CaptureRequest$Builder;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public 〇o00〇〇Oo(Lcom/google/android/camera/compat/params/SessionConfigurationCompat;)V
    .locals 1
    .param p1    # Lcom/google/android/camera/compat/params/SessionConfigurationCompat;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/camera/compat/exception/CameraAccessExceptionCompat;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/CameraDeviceCompat;->〇080:Lcom/google/android/camera/compat/CameraDeviceCompat$CameraDeviceCompatImpl;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/google/android/camera/compat/CameraDeviceCompat$CameraDeviceCompatImpl;->〇080(Lcom/google/android/camera/compat/params/SessionConfigurationCompat;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public 〇o〇()Landroid/hardware/camera2/CameraDevice;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/CameraDeviceCompat;->〇080:Lcom/google/android/camera/compat/CameraDeviceCompat$CameraDeviceCompatImpl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/google/android/camera/compat/CameraDeviceCompat$CameraDeviceCompatImpl;->unwrap()Landroid/hardware/camera2/CameraDevice;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

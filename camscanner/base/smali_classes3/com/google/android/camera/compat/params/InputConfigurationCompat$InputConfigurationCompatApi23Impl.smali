.class Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatApi23Impl;
.super Ljava/lang/Object;
.source "InputConfigurationCompat.java"

# interfaces
.implements Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatImpl;


# annotations
.annotation build Landroidx/annotation/RequiresApi;
    value = 0x17
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/camera/compat/params/InputConfigurationCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InputConfigurationCompatApi23Impl"
.end annotation


# instance fields
.field private final 〇080:Landroid/hardware/camera2/params/InputConfiguration;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    check-cast p1, Landroid/hardware/camera2/params/InputConfiguration;

    .line 5
    .line 6
    iput-object p1, p0, Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatApi23Impl;->〇080:Landroid/hardware/camera2/params/InputConfiguration;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatImpl;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    return p1

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatApi23Impl;->〇080:Landroid/hardware/camera2/params/InputConfiguration;

    .line 8
    .line 9
    check-cast p1, Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatImpl;

    .line 10
    .line 11
    invoke-interface {p1}, Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatImpl;->getInputConfiguration()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-static {v0, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    return p1
    .line 20
    .line 21
    .line 22
.end method

.method public getInputConfiguration()Ljava/lang/Object;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatApi23Impl;->〇080:Landroid/hardware/camera2/params/InputConfiguration;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public hashCode()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatApi23Impl;->〇080:Landroid/hardware/camera2/params/InputConfiguration;

    .line 2
    .line 3
    invoke-static {v0}, Landroidx/camera/camera2/internal/compat/params/〇o〇;->〇080(Landroid/hardware/camera2/params/InputConfiguration;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public toString()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatApi23Impl;->〇080:Landroid/hardware/camera2/params/InputConfiguration;

    .line 2
    .line 3
    invoke-static {v0}, Landroidx/camera/camera2/internal/compat/params/〇080;->〇080(Landroid/hardware/camera2/params/InputConfiguration;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

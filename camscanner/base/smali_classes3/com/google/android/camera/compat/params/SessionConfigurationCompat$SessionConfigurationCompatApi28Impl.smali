.class final Lcom/google/android/camera/compat/params/SessionConfigurationCompat$SessionConfigurationCompatApi28Impl;
.super Ljava/lang/Object;
.source "SessionConfigurationCompat.java"

# interfaces
.implements Lcom/google/android/camera/compat/params/SessionConfigurationCompat$SessionConfigurationCompatImpl;


# annotations
.annotation build Landroidx/annotation/RequiresApi;
    value = 0x1c
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/camera/compat/params/SessionConfigurationCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SessionConfigurationCompatApi28Impl"
.end annotation


# instance fields
.field private final 〇080:Landroid/hardware/camera2/params/SessionConfiguration;

.field private final 〇o00〇〇Oo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/google/android/camera/compat/params/OutputConfigurationCompat;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(ILjava/util/List;Ljava/util/concurrent/Executor;Landroid/hardware/camera2/CameraCaptureSession$StateCallback;)V
    .locals 1
    .param p2    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Landroid/hardware/camera2/CameraCaptureSession$StateCallback;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/google/android/camera/compat/params/OutputConfigurationCompat;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            "Landroid/hardware/camera2/CameraCaptureSession$StateCallback;",
            ")V"
        }
    .end annotation

    .line 5
    new-instance v0, Landroid/hardware/camera2/params/SessionConfiguration;

    invoke-static {p2}, Lcom/google/android/camera/compat/params/SessionConfigurationCompat;->o〇0(Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    invoke-direct {v0, p1, p2, p3, p4}, Landroid/hardware/camera2/params/SessionConfiguration;-><init>(ILjava/util/List;Ljava/util/concurrent/Executor;Landroid/hardware/camera2/CameraCaptureSession$StateCallback;)V

    invoke-direct {p0, v0}, Lcom/google/android/camera/compat/params/SessionConfigurationCompat$SessionConfigurationCompatApi28Impl;-><init>(Ljava/lang/Object;)V

    return-void
.end method

.method constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    check-cast p1, Landroid/hardware/camera2/params/SessionConfiguration;

    iput-object p1, p0, Lcom/google/android/camera/compat/params/SessionConfigurationCompat$SessionConfigurationCompatApi28Impl;->〇080:Landroid/hardware/camera2/params/SessionConfiguration;

    .line 3
    invoke-static {p1}, Landroidx/camera/camera2/internal/compat/params/〇〇8O0〇8;->〇080(Landroid/hardware/camera2/params/SessionConfiguration;)Ljava/util/List;

    move-result-object p1

    .line 4
    invoke-static {p1}, Lcom/google/android/camera/compat/params/SessionConfigurationCompat;->〇〇888(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/camera/compat/params/SessionConfigurationCompat$SessionConfigurationCompatApi28Impl;->〇o00〇〇Oo:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    instance-of v0, p1, Lcom/google/android/camera/compat/params/SessionConfigurationCompat$SessionConfigurationCompatApi28Impl;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    return p1

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/google/android/camera/compat/params/SessionConfigurationCompat$SessionConfigurationCompatApi28Impl;->〇080:Landroid/hardware/camera2/params/SessionConfiguration;

    .line 8
    .line 9
    check-cast p1, Lcom/google/android/camera/compat/params/SessionConfigurationCompat$SessionConfigurationCompatApi28Impl;

    .line 10
    .line 11
    iget-object p1, p1, Lcom/google/android/camera/compat/params/SessionConfigurationCompat$SessionConfigurationCompatApi28Impl;->〇080:Landroid/hardware/camera2/params/SessionConfiguration;

    .line 12
    .line 13
    invoke-static {v0, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    return p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public getExecutor()Ljava/util/concurrent/Executor;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/params/SessionConfigurationCompat$SessionConfigurationCompatApi28Impl;->〇080:Landroid/hardware/camera2/params/SessionConfiguration;

    .line 2
    .line 3
    invoke-static {v0}, Landroidx/camera/camera2/internal/compat/params/〇O00;->〇080(Landroid/hardware/camera2/params/SessionConfiguration;)Ljava/util/concurrent/Executor;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getInputConfiguration()Lcom/google/android/camera/compat/params/InputConfigurationCompat;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/params/SessionConfigurationCompat$SessionConfigurationCompatApi28Impl;->〇080:Landroid/hardware/camera2/params/SessionConfiguration;

    .line 2
    .line 3
    invoke-static {v0}, Landroidx/camera/camera2/internal/compat/params/OoO8;->〇080(Landroid/hardware/camera2/params/SessionConfiguration;)Landroid/hardware/camera2/params/InputConfiguration;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Lcom/google/android/camera/compat/params/InputConfigurationCompat;->〇o00〇〇Oo(Ljava/lang/Object;)Lcom/google/android/camera/compat/params/InputConfigurationCompat;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getOutputConfigurations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/google/android/camera/compat/params/OutputConfigurationCompat;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/params/SessionConfigurationCompat$SessionConfigurationCompatApi28Impl;->〇o00〇〇Oo:Ljava/util/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getSessionConfiguration()Ljava/lang/Object;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/params/SessionConfigurationCompat$SessionConfigurationCompatApi28Impl;->〇080:Landroid/hardware/camera2/params/SessionConfiguration;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getSessionType()I
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongConstant"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/params/SessionConfigurationCompat$SessionConfigurationCompatApi28Impl;->〇080:Landroid/hardware/camera2/params/SessionConfiguration;

    .line 2
    .line 3
    invoke-static {v0}, Landroidx/camera/camera2/internal/compat/params/oo88o8O;->〇080(Landroid/hardware/camera2/params/SessionConfiguration;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getStateCallback()Landroid/hardware/camera2/CameraCaptureSession$StateCallback;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/params/SessionConfigurationCompat$SessionConfigurationCompatApi28Impl;->〇080:Landroid/hardware/camera2/params/SessionConfiguration;

    .line 2
    .line 3
    invoke-static {v0}, Landroidx/camera/camera2/internal/compat/params/〇O888o0o;->〇080(Landroid/hardware/camera2/params/SessionConfiguration;)Landroid/hardware/camera2/CameraCaptureSession$StateCallback;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public hashCode()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/params/SessionConfigurationCompat$SessionConfigurationCompatApi28Impl;->〇080:Landroid/hardware/camera2/params/SessionConfiguration;

    .line 2
    .line 3
    invoke-static {v0}, Landroidx/camera/camera2/internal/compat/params/o〇O8〇〇o;->〇080(Landroid/hardware/camera2/params/SessionConfiguration;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.class Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi24Impl;
.super Lcom/google/android/camera/compat/params/OutputConfigurationCompatBaseImpl;
.source "OutputConfigurationCompatApi24Impl.java"


# annotations
.annotation build Landroidx/annotation/RequiresApi;
    value = 0x18
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi24Impl$OutputConfigurationParamsApi24;
    }
.end annotation


# direct methods
.method constructor <init>(Landroid/view/Surface;)V
    .locals 2
    .param p1    # Landroid/view/Surface;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    new-instance v0, Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi24Impl$OutputConfigurationParamsApi24;

    new-instance v1, Landroid/hardware/camera2/params/OutputConfiguration;

    invoke-direct {v1, p1}, Landroid/hardware/camera2/params/OutputConfiguration;-><init>(Landroid/view/Surface;)V

    invoke-direct {v0, v1}, Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi24Impl$OutputConfigurationParamsApi24;-><init>(Landroid/hardware/camera2/params/OutputConfiguration;)V

    invoke-direct {p0, v0}, Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi24Impl;-><init>(Ljava/lang/Object;)V

    return-void
.end method

.method constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 2
    invoke-direct {p0, p1}, Lcom/google/android/camera/compat/params/OutputConfigurationCompatBaseImpl;-><init>(Ljava/lang/Object;)V

    return-void
.end method

.method static 〇080(Landroid/hardware/camera2/params/OutputConfiguration;)Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi24Impl;
    .locals 2
    .param p0    # Landroid/hardware/camera2/params/OutputConfiguration;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/RequiresApi;
        value = 0x18
    .end annotation

    .line 1
    new-instance v0, Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi24Impl;

    .line 2
    .line 3
    new-instance v1, Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi24Impl$OutputConfigurationParamsApi24;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi24Impl$OutputConfigurationParamsApi24;-><init>(Landroid/hardware/camera2/params/OutputConfiguration;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {v0, v1}, Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi24Impl;-><init>(Ljava/lang/Object;)V

    .line 9
    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public getOutputConfiguration()Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/params/OutputConfigurationCompatBaseImpl;->〇080:Ljava/lang/Object;

    .line 2
    .line 3
    instance-of v0, v0, Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi24Impl$OutputConfigurationParamsApi24;

    .line 4
    .line 5
    invoke-static {v0}, Lcom/google/android/camera/compat/Preconditions;->〇080(Z)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/google/android/camera/compat/params/OutputConfigurationCompatBaseImpl;->〇080:Ljava/lang/Object;

    .line 9
    .line 10
    check-cast v0, Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi24Impl$OutputConfigurationParamsApi24;

    .line 11
    .line 12
    iget-object v0, v0, Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi24Impl$OutputConfigurationParamsApi24;->〇080:Landroid/hardware/camera2/params/OutputConfiguration;

    .line 13
    .line 14
    return-object v0
    .line 15
.end method

.method public getPhysicalCameraId()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/params/OutputConfigurationCompatBaseImpl;->〇080:Ljava/lang/Object;

    .line 2
    .line 3
    check-cast v0, Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi24Impl$OutputConfigurationParamsApi24;

    .line 4
    .line 5
    iget-object v0, v0, Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi24Impl$OutputConfigurationParamsApi24;->〇o00〇〇Oo:Ljava/lang/String;

    .line 6
    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getSurface()Landroid/view/Surface;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi24Impl;->getOutputConfiguration()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Landroid/hardware/camera2/params/OutputConfiguration;

    .line 6
    .line 7
    invoke-static {v0}, Landroidx/camera/camera2/internal/compat/params/oO80;->〇080(Landroid/hardware/camera2/params/OutputConfiguration;)Landroid/view/Surface;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
.end method

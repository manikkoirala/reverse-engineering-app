.class public Lcom/google/android/camera/compat/quirk/CameraQuirks;
.super Ljava/lang/Object;
.source "CameraQuirks.java"


# direct methods
.method public static 〇080(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)Lcom/google/android/camera/compat/quirk/Quirks;
    .locals 2
    .param p0    # Lcom/google/android/camera/compat/CameraCharacteristicsCompat;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-static {p0}, Lcom/google/android/camera/compat/quirk/AeFpsRangeLegacyQuirk;->〇o〇(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)Z

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    new-instance v1, Lcom/google/android/camera/compat/quirk/AeFpsRangeLegacyQuirk;

    .line 13
    .line 14
    invoke-direct {v1, p0}, Lcom/google/android/camera/compat/quirk/AeFpsRangeLegacyQuirk;-><init>(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)V

    .line 15
    .line 16
    .line 17
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 18
    .line 19
    .line 20
    :cond_0
    invoke-static {p0}, Lcom/google/android/camera/compat/quirk/JpegHalCorruptImageQuirk;->〇080(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)Z

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    if-eqz v1, :cond_1

    .line 25
    .line 26
    new-instance v1, Lcom/google/android/camera/compat/quirk/JpegHalCorruptImageQuirk;

    .line 27
    .line 28
    invoke-direct {v1}, Lcom/google/android/camera/compat/quirk/JpegHalCorruptImageQuirk;-><init>()V

    .line 29
    .line 30
    .line 31
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32
    .line 33
    .line 34
    :cond_1
    invoke-static {p0}, Lcom/google/android/camera/compat/quirk/ImageCaptureWashedOutImageQuirk;->〇080(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)Z

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    if-eqz v1, :cond_2

    .line 39
    .line 40
    new-instance v1, Lcom/google/android/camera/compat/quirk/ImageCaptureWashedOutImageQuirk;

    .line 41
    .line 42
    invoke-direct {v1}, Lcom/google/android/camera/compat/quirk/ImageCaptureWashedOutImageQuirk;-><init>()V

    .line 43
    .line 44
    .line 45
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    .line 47
    .line 48
    :cond_2
    invoke-static {p0}, Lcom/google/android/camera/compat/quirk/CameraNoResponseWhenEnablingFlashQuirk;->〇080(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)Z

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    if-eqz v1, :cond_3

    .line 53
    .line 54
    new-instance v1, Lcom/google/android/camera/compat/quirk/CameraNoResponseWhenEnablingFlashQuirk;

    .line 55
    .line 56
    invoke-direct {v1}, Lcom/google/android/camera/compat/quirk/CameraNoResponseWhenEnablingFlashQuirk;-><init>()V

    .line 57
    .line 58
    .line 59
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    .line 61
    .line 62
    :cond_3
    invoke-static {p0}, Lcom/google/android/camera/compat/quirk/FlashTooSlowQuirk;->〇080(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)Z

    .line 63
    .line 64
    .line 65
    move-result v1

    .line 66
    if-eqz v1, :cond_4

    .line 67
    .line 68
    new-instance v1, Lcom/google/android/camera/compat/quirk/FlashTooSlowQuirk;

    .line 69
    .line 70
    invoke-direct {v1}, Lcom/google/android/camera/compat/quirk/FlashTooSlowQuirk;-><init>()V

    .line 71
    .line 72
    .line 73
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    .line 75
    .line 76
    :cond_4
    invoke-static {p0}, Lcom/google/android/camera/compat/quirk/YuvImageOnePixelShiftQuirk;->O8(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)Z

    .line 77
    .line 78
    .line 79
    move-result v1

    .line 80
    if-eqz v1, :cond_5

    .line 81
    .line 82
    new-instance v1, Lcom/google/android/camera/compat/quirk/YuvImageOnePixelShiftQuirk;

    .line 83
    .line 84
    invoke-direct {v1}, Lcom/google/android/camera/compat/quirk/YuvImageOnePixelShiftQuirk;-><init>()V

    .line 85
    .line 86
    .line 87
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    .line 89
    .line 90
    :cond_5
    invoke-static {p0}, Lcom/google/android/camera/compat/quirk/AfRegionFlipHorizontallyQuirk;->〇080(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)Z

    .line 91
    .line 92
    .line 93
    move-result p0

    .line 94
    if-eqz p0, :cond_6

    .line 95
    .line 96
    new-instance p0, Lcom/google/android/camera/compat/quirk/AfRegionFlipHorizontallyQuirk;

    .line 97
    .line 98
    invoke-direct {p0}, Lcom/google/android/camera/compat/quirk/AfRegionFlipHorizontallyQuirk;-><init>()V

    .line 99
    .line 100
    .line 101
    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    .line 103
    .line 104
    :cond_6
    new-instance p0, Lcom/google/android/camera/compat/quirk/Quirks;

    .line 105
    .line 106
    invoke-direct {p0, v0}, Lcom/google/android/camera/compat/quirk/Quirks;-><init>(Ljava/util/List;)V

    .line 107
    .line 108
    .line 109
    return-object p0
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

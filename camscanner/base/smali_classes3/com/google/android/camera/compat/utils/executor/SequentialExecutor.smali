.class final Lcom/google/android/camera/compat/utils/executor/SequentialExecutor;
.super Ljava/lang/Object;
.source "SequentialExecutor.java"

# interfaces
.implements Ljava/util/concurrent/Executor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$QueueWorker;,
        Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$WorkerRunningState;
    }
.end annotation


# instance fields
.field private final OO:Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$QueueWorker;

.field final o0:Ljava/util/Deque;
    .annotation build Landroidx/annotation/GuardedBy;
        value = "mQueue"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field o〇00O:J
    .annotation build Landroidx/annotation/GuardedBy;
        value = "mQueue"
    .end annotation
.end field

.field 〇08O〇00〇o:Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$WorkerRunningState;
    .annotation build Landroidx/annotation/GuardedBy;
        value = "mQueue"
    .end annotation
.end field

.field private final 〇OOo8〇0:Ljava/util/concurrent/Executor;


# direct methods
.method constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/ArrayDeque;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor;->o0:Ljava/util/Deque;

    .line 10
    .line 11
    new-instance v0, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$QueueWorker;

    .line 12
    .line 13
    invoke-direct {v0, p0}, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$QueueWorker;-><init>(Lcom/google/android/camera/compat/utils/executor/SequentialExecutor;)V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor;->OO:Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$QueueWorker;

    .line 17
    .line 18
    sget-object v0, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$WorkerRunningState;->o0:Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$WorkerRunningState;

    .line 19
    .line 20
    iput-object v0, p0, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor;->〇08O〇00〇o:Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$WorkerRunningState;

    .line 21
    .line 22
    const-wide/16 v0, 0x0

    .line 23
    .line 24
    iput-wide v0, p0, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor;->o〇00O:J

    .line 25
    .line 26
    invoke-static {p1}, Lcom/google/android/camera/compat/Preconditions;->〇o〇(Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    check-cast p1, Ljava/util/concurrent/Executor;

    .line 31
    .line 32
    iput-object p1, p0, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor;->〇OOo8〇0:Ljava/util/concurrent/Executor;

    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method


# virtual methods
.method public execute(Ljava/lang/Runnable;)V
    .locals 8

    .line 1
    invoke-static {p1}, Lcom/google/android/camera/compat/Preconditions;->〇o〇(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor;->o0:Ljava/util/Deque;

    .line 5
    .line 6
    monitor-enter v0

    .line 7
    :try_start_0
    iget-object v1, p0, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor;->〇08O〇00〇o:Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$WorkerRunningState;

    .line 8
    .line 9
    sget-object v2, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$WorkerRunningState;->〇08O〇00〇o:Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$WorkerRunningState;

    .line 10
    .line 11
    if-eq v1, v2, :cond_7

    .line 12
    .line 13
    sget-object v2, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$WorkerRunningState;->OO:Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$WorkerRunningState;

    .line 14
    .line 15
    if-ne v1, v2, :cond_0

    .line 16
    .line 17
    goto :goto_3

    .line 18
    :cond_0
    iget-wide v3, p0, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor;->o〇00O:J

    .line 19
    .line 20
    new-instance v1, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$1;

    .line 21
    .line 22
    invoke-direct {v1, p0, p1}, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$1;-><init>(Lcom/google/android/camera/compat/utils/executor/SequentialExecutor;Ljava/lang/Runnable;)V

    .line 23
    .line 24
    .line 25
    iget-object p1, p0, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor;->o0:Ljava/util/Deque;

    .line 26
    .line 27
    invoke-interface {p1, v1}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z

    .line 28
    .line 29
    .line 30
    sget-object p1, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$WorkerRunningState;->〇OOo8〇0:Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$WorkerRunningState;

    .line 31
    .line 32
    iput-object p1, p0, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor;->〇08O〇00〇o:Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$WorkerRunningState;

    .line 33
    .line 34
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 35
    const/4 v0, 0x1

    .line 36
    const/4 v5, 0x0

    .line 37
    :try_start_1
    iget-object v6, p0, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor;->〇OOo8〇0:Ljava/util/concurrent/Executor;

    .line 38
    .line 39
    iget-object v7, p0, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor;->OO:Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$QueueWorker;

    .line 40
    .line 41
    invoke-interface {v6, v7}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_0

    .line 42
    .line 43
    .line 44
    iget-object v1, p0, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor;->〇08O〇00〇o:Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$WorkerRunningState;

    .line 45
    .line 46
    if-eq v1, p1, :cond_1

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_1
    const/4 v0, 0x0

    .line 50
    :goto_0
    if-eqz v0, :cond_2

    .line 51
    .line 52
    return-void

    .line 53
    :cond_2
    iget-object v6, p0, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor;->o0:Ljava/util/Deque;

    .line 54
    .line 55
    monitor-enter v6

    .line 56
    :try_start_2
    iget-wide v0, p0, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor;->o〇00O:J

    .line 57
    .line 58
    cmp-long v5, v0, v3

    .line 59
    .line 60
    if-nez v5, :cond_3

    .line 61
    .line 62
    iget-object v0, p0, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor;->〇08O〇00〇o:Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$WorkerRunningState;

    .line 63
    .line 64
    if-ne v0, p1, :cond_3

    .line 65
    .line 66
    iput-object v2, p0, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor;->〇08O〇00〇o:Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$WorkerRunningState;

    .line 67
    .line 68
    :cond_3
    monitor-exit v6

    .line 69
    return-void

    .line 70
    :catchall_0
    move-exception p1

    .line 71
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 72
    throw p1

    .line 73
    :catch_0
    move-exception p1

    .line 74
    goto :goto_1

    .line 75
    :catch_1
    move-exception p1

    .line 76
    :goto_1
    iget-object v2, p0, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor;->o0:Ljava/util/Deque;

    .line 77
    .line 78
    monitor-enter v2

    .line 79
    :try_start_3
    iget-object v3, p0, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor;->〇08O〇00〇o:Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$WorkerRunningState;

    .line 80
    .line 81
    sget-object v4, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$WorkerRunningState;->o0:Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$WorkerRunningState;

    .line 82
    .line 83
    if-eq v3, v4, :cond_4

    .line 84
    .line 85
    sget-object v4, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$WorkerRunningState;->〇OOo8〇0:Lcom/google/android/camera/compat/utils/executor/SequentialExecutor$WorkerRunningState;

    .line 86
    .line 87
    if-ne v3, v4, :cond_5

    .line 88
    .line 89
    :cond_4
    iget-object v3, p0, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor;->o0:Ljava/util/Deque;

    .line 90
    .line 91
    invoke-interface {v3, v1}, Ljava/util/Deque;->removeLastOccurrence(Ljava/lang/Object;)Z

    .line 92
    .line 93
    .line 94
    move-result v1

    .line 95
    if-eqz v1, :cond_5

    .line 96
    .line 97
    goto :goto_2

    .line 98
    :cond_5
    const/4 v0, 0x0

    .line 99
    :goto_2
    instance-of v1, p1, Ljava/util/concurrent/RejectedExecutionException;

    .line 100
    .line 101
    if-eqz v1, :cond_6

    .line 102
    .line 103
    if-nez v0, :cond_6

    .line 104
    .line 105
    monitor-exit v2

    .line 106
    return-void

    .line 107
    :cond_6
    throw p1

    .line 108
    :catchall_1
    move-exception p1

    .line 109
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 110
    throw p1

    .line 111
    :cond_7
    :goto_3
    :try_start_4
    iget-object v1, p0, Lcom/google/android/camera/compat/utils/executor/SequentialExecutor;->o0:Ljava/util/Deque;

    .line 112
    .line 113
    invoke-interface {v1, p1}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z

    .line 114
    .line 115
    .line 116
    monitor-exit v0

    .line 117
    return-void

    .line 118
    :catchall_2
    move-exception p1

    .line 119
    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 120
    throw p1
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.class public final Lcom/google/android/camera/compat/internal/zoom/ZoomControl;
.super Ljava/lang/Object;
.source "ZoomControl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/camera/compat/internal/zoom/ZoomControl$ZoomImpl;
    }
.end annotation


# instance fields
.field private final O8:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Lcom/google/android/camera/compat/internal/zoom/ZoomState;",
            ">;"
        }
    .end annotation
.end field

.field final Oo08:Lcom/google/android/camera/compat/internal/zoom/ZoomControl$ZoomImpl;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field private o〇0:Z

.field private final 〇080:Lcom/google/android/camera/Camera2;

.field private final 〇o00〇〇Oo:Ljava/util/concurrent/Executor;

.field private final 〇o〇:Lcom/google/android/camera/compat/internal/zoom/ZoomStateImpl;
    .annotation build Landroidx/annotation/GuardedBy;
        value = "mCurrentZoomState"
    .end annotation
.end field

.field private final 〇〇888:Lcom/google/android/camera/compat/internal/CaptureResultListener;


# direct methods
.method public constructor <init>(Lcom/google/android/camera/Camera2;Lcom/google/android/camera/compat/CameraCharacteristicsCompat;Ljava/util/concurrent/Executor;)V
    .locals 2
    .param p1    # Lcom/google/android/camera/Camera2;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/camera/compat/CameraCharacteristicsCompat;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->o〇0:Z

    .line 6
    .line 7
    new-instance v0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl$1;

    .line 8
    .line 9
    invoke-direct {v0, p0}, Lcom/google/android/camera/compat/internal/zoom/ZoomControl$1;-><init>(Lcom/google/android/camera/compat/internal/zoom/ZoomControl;)V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->〇〇888:Lcom/google/android/camera/compat/internal/CaptureResultListener;

    .line 13
    .line 14
    iput-object p1, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->〇080:Lcom/google/android/camera/Camera2;

    .line 15
    .line 16
    iput-object p3, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->〇o00〇〇Oo:Ljava/util/concurrent/Executor;

    .line 17
    .line 18
    invoke-static {p2}, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->O8(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)Lcom/google/android/camera/compat/internal/zoom/ZoomControl$ZoomImpl;

    .line 19
    .line 20
    .line 21
    move-result-object p2

    .line 22
    iput-object p2, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->Oo08:Lcom/google/android/camera/compat/internal/zoom/ZoomControl$ZoomImpl;

    .line 23
    .line 24
    new-instance p3, Lcom/google/android/camera/compat/internal/zoom/ZoomStateImpl;

    .line 25
    .line 26
    invoke-interface {p2}, Lcom/google/android/camera/compat/internal/zoom/ZoomControl$ZoomImpl;->getMaxZoom()F

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    invoke-interface {p2}, Lcom/google/android/camera/compat/internal/zoom/ZoomControl$ZoomImpl;->getMinZoom()F

    .line 31
    .line 32
    .line 33
    move-result p2

    .line 34
    invoke-direct {p3, v1, p2}, Lcom/google/android/camera/compat/internal/zoom/ZoomStateImpl;-><init>(FF)V

    .line 35
    .line 36
    .line 37
    iput-object p3, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->〇o〇:Lcom/google/android/camera/compat/internal/zoom/ZoomStateImpl;

    .line 38
    .line 39
    const/high16 p2, 0x3f800000    # 1.0f

    .line 40
    .line 41
    invoke-virtual {p3, p2}, Lcom/google/android/camera/compat/internal/zoom/ZoomStateImpl;->〇o00〇〇Oo(F)V

    .line 42
    .line 43
    .line 44
    new-instance p2, Landroidx/lifecycle/MutableLiveData;

    .line 45
    .line 46
    invoke-static {p3}, Lcom/google/android/camera/compat/internal/zoom/ImmutableZoomState;->〇080(Lcom/google/android/camera/compat/internal/zoom/ZoomState;)Lcom/google/android/camera/compat/internal/zoom/ZoomState;

    .line 47
    .line 48
    .line 49
    move-result-object p3

    .line 50
    invoke-direct {p2, p3}, Landroidx/lifecycle/MutableLiveData;-><init>(Ljava/lang/Object;)V

    .line 51
    .line 52
    .line 53
    iput-object p2, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->O8:Landroidx/lifecycle/MutableLiveData;

    .line 54
    .line 55
    invoke-virtual {p1, v0}, Lcom/google/android/camera/Camera2;->O〇0〇o808〇(Lcom/google/android/camera/compat/internal/CaptureResultListener;)V

    .line 56
    .line 57
    .line 58
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private static O8(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)Lcom/google/android/camera/compat/internal/zoom/ZoomControl$ZoomImpl;
    .locals 1
    .param p0    # Lcom/google/android/camera/compat/CameraCharacteristicsCompat;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-static {p0}, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->oO80(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    new-instance v0, Lcom/google/android/camera/compat/internal/zoom/AndroidRZoomImpl;

    .line 8
    .line 9
    invoke-direct {v0, p0}, Lcom/google/android/camera/compat/internal/zoom/AndroidRZoomImpl;-><init>(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)V

    .line 10
    .line 11
    .line 12
    return-object v0

    .line 13
    :cond_0
    new-instance v0, Lcom/google/android/camera/compat/internal/zoom/CropRegionZoomImpl;

    .line 14
    .line 15
    invoke-direct {v0, p0}, Lcom/google/android/camera/compat/internal/zoom/CropRegionZoomImpl;-><init>(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)V

    .line 16
    .line 17
    .line 18
    return-object v0
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private OO0o〇〇(Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;Lcom/google/android/camera/compat/internal/zoom/ZoomState;Ljava/lang/Boolean;)V
    .locals 1
    .param p1    # Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/camera/compat/internal/zoom/ZoomState;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer<",
            "Ljava/lang/Void;",
            ">;",
            "Lcom/google/android/camera/compat/internal/zoom/ZoomState;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->o〇0:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->〇o〇:Lcom/google/android/camera/compat/internal/zoom/ZoomStateImpl;

    .line 6
    .line 7
    monitor-enter v0

    .line 8
    :try_start_0
    iget-object p2, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->〇o〇:Lcom/google/android/camera/compat/internal/zoom/ZoomStateImpl;

    .line 9
    .line 10
    const/high16 p3, 0x3f800000    # 1.0f

    .line 11
    .line 12
    invoke-virtual {p2, p3}, Lcom/google/android/camera/compat/internal/zoom/ZoomStateImpl;->〇o00〇〇Oo(F)V

    .line 13
    .line 14
    .line 15
    iget-object p2, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->〇o〇:Lcom/google/android/camera/compat/internal/zoom/ZoomStateImpl;

    .line 16
    .line 17
    invoke-static {p2}, Lcom/google/android/camera/compat/internal/zoom/ImmutableZoomState;->〇080(Lcom/google/android/camera/compat/internal/zoom/ZoomState;)Lcom/google/android/camera/compat/internal/zoom/ZoomState;

    .line 18
    .line 19
    .line 20
    move-result-object p2

    .line 21
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    invoke-direct {p0, p2}, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->Oooo8o0〇(Lcom/google/android/camera/compat/internal/zoom/ZoomState;)V

    .line 23
    .line 24
    .line 25
    new-instance p2, Lcom/google/android/camera/compat/exception/OperationCanceledException;

    .line 26
    .line 27
    const-string p3, "Camera is not active."

    .line 28
    .line 29
    invoke-direct {p2, p3}, Lcom/google/android/camera/compat/exception/OperationCanceledException;-><init>(Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p1, p2}, Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;->setException(Ljava/lang/Throwable;)Z

    .line 33
    .line 34
    .line 35
    return-void

    .line 36
    :catchall_0
    move-exception p1

    .line 37
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 38
    throw p1

    .line 39
    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->Oooo8o0〇(Lcom/google/android/camera/compat/internal/zoom/ZoomState;)V

    .line 40
    .line 41
    .line 42
    iget-object v0, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->Oo08:Lcom/google/android/camera/compat/internal/zoom/ZoomControl$ZoomImpl;

    .line 43
    .line 44
    invoke-interface {p2}, Lcom/google/android/camera/compat/internal/zoom/ZoomState;->getZoomRatio()F

    .line 45
    .line 46
    .line 47
    move-result p2

    .line 48
    invoke-interface {v0, p2, p1}, Lcom/google/android/camera/compat/internal/zoom/ZoomControl$ZoomImpl;->setZoomRatio(FLandroidx/concurrent/futures/CallbackToFutureAdapter$Completer;)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 52
    .line 53
    .line 54
    move-result p1

    .line 55
    if-eqz p1, :cond_1

    .line 56
    .line 57
    iget-object p1, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->〇080:Lcom/google/android/camera/Camera2;

    .line 58
    .line 59
    invoke-virtual {p1}, Lcom/google/android/camera/Camera2;->OO8〇()J

    .line 60
    .line 61
    .line 62
    :cond_1
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private synthetic OO0o〇〇〇〇0(Lcom/google/android/camera/compat/internal/zoom/ZoomState;Ljava/lang/Boolean;Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->〇o00〇〇Oo:Ljava/util/concurrent/Executor;

    .line 2
    .line 3
    new-instance v1, L〇O888o0o/〇o00〇〇Oo;

    .line 4
    .line 5
    invoke-direct {v1, p0, p3, p1, p2}, L〇O888o0o/〇o00〇〇Oo;-><init>(Lcom/google/android/camera/compat/internal/zoom/ZoomControl;Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;Lcom/google/android/camera/compat/internal/zoom/ZoomState;Ljava/lang/Boolean;)V

    .line 6
    .line 7
    .line 8
    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 9
    .line 10
    .line 11
    const-string p1, "setZoomRatio"

    .line 12
    .line 13
    return-object p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private Oooo8o0〇(Lcom/google/android/camera/compat/internal/zoom/ZoomState;)V
    .locals 2

    .line 1
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    if-ne v0, v1, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->O8:Landroidx/lifecycle/MutableLiveData;

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Landroidx/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    .line 14
    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->O8:Landroidx/lifecycle/MutableLiveData;

    .line 18
    .line 19
    invoke-virtual {v0, p1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 20
    .line 21
    .line 22
    :goto_0
    return-void
.end method

.method static oO80(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)Z
    .locals 2

    .line 1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 2
    .line 3
    const/16 v1, 0x1e

    .line 4
    .line 5
    if-lt v0, v1, :cond_0

    .line 6
    .line 7
    invoke-static {p0}, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->o〇0(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)Landroid/util/Range;

    .line 8
    .line 9
    .line 10
    move-result-object p0

    .line 11
    if-eqz p0, :cond_0

    .line 12
    .line 13
    const/4 p0, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 p0, 0x0

    .line 16
    :goto_0
    return p0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private static o〇0(Lcom/google/android/camera/compat/CameraCharacteristicsCompat;)Landroid/util/Range;
    .locals 2
    .annotation build Landroidx/annotation/RequiresApi;
        value = 0x1e
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/camera/compat/CameraCharacteristicsCompat;",
            ")",
            "Landroid/util/Range<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .line 1
    :try_start_0
    invoke-static {}, Landroidx/camera/camera2/internal/〇080;->〇080()Landroid/hardware/camera2/CameraCharacteristics$Key;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0, v0}, Lcom/google/android/camera/compat/CameraCharacteristicsCompat;->〇080(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    .line 6
    .line 7
    .line 8
    move-result-object p0

    .line 9
    check-cast p0, Landroid/util/Range;
    :try_end_0
    .catch Ljava/lang/AssertionError; {:try_start_0 .. :try_end_0} :catch_0

    .line 10
    .line 11
    return-object p0

    .line 12
    :catch_0
    move-exception p0

    .line 13
    const-string v0, "CameraX-ZoomControl"

    .line 14
    .line 15
    const-string v1, "AssertionError, fail to get camera characteristic."

    .line 16
    .line 17
    invoke-static {v0, v1, p0}, Lcom/google/android/camera/log/CameraLog;->〇O8o08O(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 18
    .line 19
    .line 20
    const/4 p0, 0x0

    .line 21
    return-object p0
    .line 22
.end method

.method public static synthetic 〇080(Lcom/google/android/camera/compat/internal/zoom/ZoomControl;Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;Lcom/google/android/camera/compat/internal/zoom/ZoomState;Ljava/lang/Boolean;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->〇80〇808〇O(Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;Lcom/google/android/camera/compat/internal/zoom/ZoomState;Ljava/lang/Boolean;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
.end method

.method private synthetic 〇80〇808〇O(Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;Lcom/google/android/camera/compat/internal/zoom/ZoomState;Ljava/lang/Boolean;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->OO0o〇〇(Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;Lcom/google/android/camera/compat/internal/zoom/ZoomState;Ljava/lang/Boolean;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/google/android/camera/compat/internal/zoom/ZoomControl;Lcom/google/android/camera/compat/internal/zoom/ZoomState;Ljava/lang/Boolean;Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->OO0o〇〇〇〇0(Lcom/google/android/camera/compat/internal/zoom/ZoomState;Ljava/lang/Boolean;Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
.end method


# virtual methods
.method public Oo08()Landroid/graphics/Rect;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->Oo08:Lcom/google/android/camera/compat/internal/zoom/ZoomControl$ZoomImpl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/google/android/camera/compat/internal/zoom/ZoomControl$ZoomImpl;->getCropSensorRegion()Landroid/graphics/Rect;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇8o8o〇(Z)V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->o〇0:Z

    .line 2
    .line 3
    if-ne v0, p1, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->o〇0:Z

    .line 7
    .line 8
    if-nez p1, :cond_1

    .line 9
    .line 10
    iget-object p1, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->〇o〇:Lcom/google/android/camera/compat/internal/zoom/ZoomStateImpl;

    .line 11
    .line 12
    monitor-enter p1

    .line 13
    :try_start_0
    iget-object v0, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->〇o〇:Lcom/google/android/camera/compat/internal/zoom/ZoomStateImpl;

    .line 14
    .line 15
    const/high16 v1, 0x3f800000    # 1.0f

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Lcom/google/android/camera/compat/internal/zoom/ZoomStateImpl;->〇o00〇〇Oo(F)V

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->〇o〇:Lcom/google/android/camera/compat/internal/zoom/ZoomStateImpl;

    .line 21
    .line 22
    invoke-static {v0}, Lcom/google/android/camera/compat/internal/zoom/ImmutableZoomState;->〇080(Lcom/google/android/camera/compat/internal/zoom/ZoomState;)Lcom/google/android/camera/compat/internal/zoom/ZoomState;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27
    invoke-direct {p0, v0}, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->Oooo8o0〇(Lcom/google/android/camera/compat/internal/zoom/ZoomState;)V

    .line 28
    .line 29
    .line 30
    iget-object p1, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->Oo08:Lcom/google/android/camera/compat/internal/zoom/ZoomControl$ZoomImpl;

    .line 31
    .line 32
    invoke-interface {p1}, Lcom/google/android/camera/compat/internal/zoom/ZoomControl$ZoomImpl;->resetZoom()V

    .line 33
    .line 34
    .line 35
    iget-object p1, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->〇080:Lcom/google/android/camera/Camera2;

    .line 36
    .line 37
    invoke-virtual {p1}, Lcom/google/android/camera/Camera2;->OO8〇()J

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :catchall_0
    move-exception v0

    .line 42
    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 43
    throw v0

    .line 44
    :cond_1
    :goto_0
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public 〇O8o08O(FLjava/lang/Boolean;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->〇o〇:Lcom/google/android/camera/compat/internal/zoom/ZoomStateImpl;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    :try_start_0
    iget-object v1, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->〇o〇:Lcom/google/android/camera/compat/internal/zoom/ZoomStateImpl;

    .line 5
    .line 6
    invoke-virtual {v1, p1}, Lcom/google/android/camera/compat/internal/zoom/ZoomStateImpl;->〇o00〇〇Oo(F)V

    .line 7
    .line 8
    .line 9
    iget-object p1, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->〇o〇:Lcom/google/android/camera/compat/internal/zoom/ZoomStateImpl;

    .line 10
    .line 11
    invoke-static {p1}, Lcom/google/android/camera/compat/internal/zoom/ImmutableZoomState;->〇080(Lcom/google/android/camera/compat/internal/zoom/ZoomState;)Lcom/google/android/camera/compat/internal/zoom/ZoomState;

    .line 12
    .line 13
    .line 14
    move-result-object p1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 16
    invoke-direct {p0, p1}, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->Oooo8o0〇(Lcom/google/android/camera/compat/internal/zoom/ZoomState;)V

    .line 17
    .line 18
    .line 19
    new-instance v0, L〇O888o0o/〇080;

    .line 20
    .line 21
    invoke-direct {v0, p0, p1, p2}, L〇O888o0o/〇080;-><init>(Lcom/google/android/camera/compat/internal/zoom/ZoomControl;Lcom/google/android/camera/compat/internal/zoom/ZoomState;Ljava/lang/Boolean;)V

    .line 22
    .line 23
    .line 24
    invoke-static {v0}, Landroidx/concurrent/futures/CallbackToFutureAdapter;->getFuture(Landroidx/concurrent/futures/CallbackToFutureAdapter$Resolver;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    return-object p1

    .line 29
    :catchall_0
    move-exception p1

    .line 30
    goto :goto_0

    .line 31
    :catch_0
    move-exception p1

    .line 32
    :try_start_2
    invoke-static {p1}, Lcom/google/android/camera/compat/utils/futures/Futures;->o〇0(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    monitor-exit v0

    .line 37
    return-object p1

    .line 38
    :goto_0
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 39
    throw p1
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public 〇o〇(Lcom/google/android/camera/Camera2Config;)V
    .locals 1
    .param p1    # Lcom/google/android/camera/Camera2Config;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->Oo08:Lcom/google/android/camera/compat/internal/zoom/ZoomControl$ZoomImpl;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/google/android/camera/compat/internal/zoom/ZoomControl$ZoomImpl;->〇080(Lcom/google/android/camera/Camera2Config;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public 〇〇888()Landroidx/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/LiveData<",
            "Lcom/google/android/camera/compat/internal/zoom/ZoomState;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/internal/zoom/ZoomControl;->O8:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.class public final Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;
.super Ljava/lang/Object;
.source "CameraImageReaderProxy.kt"

# interfaces
.implements Lcom/google/android/camera/compat/imagereader/ImageReaderProxy$OnImageAvailableListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy$CacheAnalyzingImageProxy;,
        Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final oOo〇8o008:Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy$CacheAnalyzingImageProxy;
    .annotation build Landroidx/annotation/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private final OO:Ljava/util/concurrent/locks/ReentrantLock;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o0:Ljava/util/concurrent/Executor;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Lcom/google/android/camera/compat/imagereader/ImageProxy;
    .annotation build Landroidx/annotation/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private 〇080OO8〇0:Z

.field private 〇08O〇00〇o:Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;

.field private 〇0O:Lkotlin/jvm/functions/Function3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function3<",
            "-[B-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private volatile 〇OOo8〇0:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->oOo〇8o008:Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "executor"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->o0:Ljava/util/concurrent/Executor;

    .line 10
    .line 11
    const/4 p1, 0x1

    .line 12
    iput p1, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->〇OOo8〇0:I

    .line 13
    .line 14
    new-instance p1, Ljava/util/concurrent/locks/ReentrantLock;

    .line 15
    .line 16
    invoke-direct {p1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    .line 17
    .line 18
    .line 19
    iput-object p1, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->OO:Ljava/util/concurrent/locks/ReentrantLock;

    .line 20
    .line 21
    return-void
    .line 22
.end method

.method public static final synthetic O8(Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->o〇0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final OO0o〇〇(Lcom/google/android/camera/compat/imagereader/ImageProxy;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->OO:Ljava/util/concurrent/locks/ReentrantLock;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->〇080OO8〇0:Z

    .line 5
    .line 6
    if-nez v1, :cond_0

    .line 7
    .line 8
    invoke-interface {p1}, Lcom/google/android/camera/compat/imagereader/ImageProxy;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9
    .line 10
    .line 11
    monitor-exit v0

    .line 12
    return-void

    .line 13
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->O8o08O8O:Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy$CacheAnalyzingImageProxy;

    .line 14
    .line 15
    if-eqz v1, :cond_3

    .line 16
    .line 17
    invoke-interface {p1}, Lcom/google/android/camera/compat/imagereader/ImageProxy;->getTimestamp()J

    .line 18
    .line 19
    .line 20
    move-result-wide v1

    .line 21
    iget-object v3, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->O8o08O8O:Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy$CacheAnalyzingImageProxy;

    .line 22
    .line 23
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v3}, Lcom/google/android/camera/compat/imagereader/ForwardingImageProxy;->getTimestamp()J

    .line 27
    .line 28
    .line 29
    move-result-wide v3

    .line 30
    cmp-long v5, v1, v3

    .line 31
    .line 32
    if-gtz v5, :cond_1

    .line 33
    .line 34
    invoke-interface {p1}, Lcom/google/android/camera/compat/imagereader/ImageProxy;->close()V

    .line 35
    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_1
    iget-object v1, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->o〇00O:Lcom/google/android/camera/compat/imagereader/ImageProxy;

    .line 39
    .line 40
    if-eqz v1, :cond_2

    .line 41
    .line 42
    if-eqz v1, :cond_2

    .line 43
    .line 44
    invoke-interface {v1}, Lcom/google/android/camera/compat/imagereader/ImageProxy;->close()V

    .line 45
    .line 46
    .line 47
    :cond_2
    iput-object p1, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->o〇00O:Lcom/google/android/camera/compat/imagereader/ImageProxy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 48
    .line 49
    :goto_0
    monitor-exit v0

    .line 50
    return-void

    .line 51
    :cond_3
    :try_start_2
    new-instance v1, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy$CacheAnalyzingImageProxy;

    .line 52
    .line 53
    invoke-direct {v1, p1, p0}, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy$CacheAnalyzingImageProxy;-><init>(Lcom/google/android/camera/compat/imagereader/ImageProxy;Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;)V

    .line 54
    .line 55
    .line 56
    iput-object v1, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->O8o08O8O:Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy$CacheAnalyzingImageProxy;

    .line 57
    .line 58
    invoke-direct {p0, v1}, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->〇〇888(Lcom/google/android/camera/compat/imagereader/ImageProxy;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    new-instance v2, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy$onValidImageAvailable$1$1;

    .line 63
    .line 64
    invoke-direct {v2, v1}, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy$onValidImageAvailable$1$1;-><init>(Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy$CacheAnalyzingImageProxy;)V

    .line 65
    .line 66
    .line 67
    invoke-static {}, Lcom/google/android/camera/compat/utils/executor/CameraXExecutors;->〇080()Ljava/util/concurrent/Executor;

    .line 68
    .line 69
    .line 70
    move-result-object v1

    .line 71
    invoke-static {p1, v2, v1}, Lcom/google/android/camera/compat/utils/futures/Futures;->〇o00〇〇Oo(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/android/camera/compat/utils/futures/FutureCallback;Ljava/util/concurrent/Executor;)V

    .line 72
    .line 73
    .line 74
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 75
    .line 76
    monitor-exit v0

    .line 77
    return-void

    .line 78
    :catchall_0
    move-exception p1

    .line 79
    monitor-exit v0

    .line 80
    throw p1
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method private final OO0o〇〇〇〇0()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->OO:Ljava/util/concurrent/locks/ReentrantLock;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    :try_start_0
    iget-object v1, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->o〇00O:Lcom/google/android/camera/compat/imagereader/ImageProxy;

    .line 5
    .line 6
    if-eqz v1, :cond_1

    .line 7
    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    invoke-interface {v1}, Lcom/google/android/camera/compat/imagereader/ImageProxy;->close()V

    .line 11
    .line 12
    .line 13
    :cond_0
    const/4 v1, 0x0

    .line 14
    iput-object v1, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->o〇00O:Lcom/google/android/camera/compat/imagereader/ImageProxy;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :catchall_0
    move-exception v1

    .line 18
    goto :goto_1

    .line 19
    :catch_0
    move-exception v1

    .line 20
    :try_start_1
    const-string v2, "CameraX-CameraImageReaderProxy"

    .line 21
    .line 22
    const-string v3, "Failed to close image."

    .line 23
    .line 24
    invoke-static {v2, v3, v1}, Lcom/google/android/camera/log/CameraLog;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 25
    .line 26
    .line 27
    :cond_1
    :goto_0
    sget-object v1, Lkotlin/Unit;->〇080:Lkotlin/Unit;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 28
    .line 29
    monitor-exit v0

    .line 30
    return-void

    .line 31
    :goto_1
    monitor-exit v0

    .line 32
    throw v1
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static final synthetic Oo08(Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;)Ljava/util/concurrent/Executor;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->o0:Ljava/util/concurrent/Executor;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private static final oO80(Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;Lcom/google/android/camera/compat/imagereader/ImageProxy;Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;)Lkotlin/Unit;
    .locals 2

    .line 1
    const-string/jumbo v0, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    const-string v0, "$image"

    .line 8
    .line 9
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    const-string v0, "completer"

    .line 13
    .line 14
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->o0:Ljava/util/concurrent/Executor;

    .line 18
    .line 19
    new-instance v1, L〇0〇O0088o/O8;

    .line 20
    .line 21
    invoke-direct {v1, p1, p0, p2}, L〇0〇O0088o/O8;-><init>(Lcom/google/android/camera/compat/imagereader/ImageProxy;Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;)V

    .line 22
    .line 23
    .line 24
    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 25
    .line 26
    .line 27
    sget-object p0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 28
    .line 29
    return-object p0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private final o〇0()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->OO:Ljava/util/concurrent/locks/ReentrantLock;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    const/4 v1, 0x0

    .line 5
    :try_start_0
    iput-object v1, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->O8o08O8O:Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy$CacheAnalyzingImageProxy;

    .line 6
    .line 7
    iget-object v2, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->o〇00O:Lcom/google/android/camera/compat/imagereader/ImageProxy;

    .line 8
    .line 9
    if-eqz v2, :cond_0

    .line 10
    .line 11
    iput-object v1, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->o〇00O:Lcom/google/android/camera/compat/imagereader/ImageProxy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 12
    .line 13
    :try_start_1
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0, v2}, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->OO0o〇〇(Lcom/google/android/camera/compat/imagereader/ImageProxy;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :catch_0
    move-exception v1

    .line 21
    :try_start_2
    const-string v2, "CameraX-CameraImageReaderProxy"

    .line 22
    .line 23
    const-string v3, "Failed to acquire image."

    .line 24
    .line 25
    invoke-static {v2, v3, v1}, Lcom/google/android/camera/log/CameraLog;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 26
    .line 27
    .line 28
    :cond_0
    :goto_0
    sget-object v1, Lkotlin/Unit;->〇080:Lkotlin/Unit;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 29
    .line 30
    monitor-exit v0

    .line 31
    return-void

    .line 32
    :catchall_0
    move-exception v1

    .line 33
    monitor-exit v0

    .line 34
    throw v1
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private static final 〇80〇808〇O(Lcom/google/android/camera/compat/imagereader/ImageProxy;Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;)V
    .locals 6

    .line 1
    const-string v0, "ImageAnalysis is detached"

    .line 2
    .line 3
    const-string v1, "$image"

    .line 4
    .line 5
    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string/jumbo v1, "this$0"

    .line 9
    .line 10
    .line 11
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    const-string v1, "$completer"

    .line 15
    .line 16
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    :try_start_0
    invoke-interface {p0}, Lcom/google/android/camera/compat/imagereader/ImageProxy;->getPlanes()[Landroid/media/Image$Plane;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    const/4 v2, 0x1

    .line 24
    if-eqz v1, :cond_1

    .line 25
    .line 26
    array-length v1, v1

    .line 27
    const/4 v3, 0x0

    .line 28
    if-nez v1, :cond_0

    .line 29
    .line 30
    const/4 v1, 0x1

    .line 31
    goto :goto_0

    .line 32
    :cond_0
    const/4 v1, 0x0

    .line 33
    :goto_0
    if-eqz v1, :cond_2

    .line 34
    .line 35
    :cond_1
    const/4 v3, 0x1

    .line 36
    :cond_2
    if-nez v3, :cond_7

    .line 37
    .line 38
    invoke-interface {p0}, Lcom/google/android/camera/compat/imagereader/ImageProxy;->getWidth()I

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    invoke-interface {p0}, Lcom/google/android/camera/compat/imagereader/ImageProxy;->getHeight()I

    .line 43
    .line 44
    .line 45
    move-result v3

    .line 46
    iget v4, p1, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->〇OOo8〇0:I

    .line 47
    .line 48
    const/4 v5, 0x0

    .line 49
    if-eq v4, v2, :cond_5

    .line 50
    .line 51
    const/4 v2, 0x3

    .line 52
    if-eq v4, v2, :cond_4

    .line 53
    .line 54
    :cond_3
    move-object p0, v5

    .line 55
    goto :goto_1

    .line 56
    :cond_4
    invoke-interface {p0}, Lcom/google/android/camera/compat/imagereader/ImageProxy;->getImage()Landroid/media/Image;

    .line 57
    .line 58
    .line 59
    move-result-object p0

    .line 60
    if-eqz p0, :cond_3

    .line 61
    .line 62
    invoke-static {p0}, Lcom/google/android/camera/util/CameraExtKt;->〇080(Landroid/media/Image;)[B

    .line 63
    .line 64
    .line 65
    move-result-object p0

    .line 66
    goto :goto_1

    .line 67
    :cond_5
    invoke-interface {p0}, Lcom/google/android/camera/compat/imagereader/ImageProxy;->getImage()Landroid/media/Image;

    .line 68
    .line 69
    .line 70
    move-result-object p0

    .line 71
    if-eqz p0, :cond_3

    .line 72
    .line 73
    invoke-static {p0}, Lcom/google/android/camera/util/CameraExtKt;->o〇0(Landroid/media/Image;)[B

    .line 74
    .line 75
    .line 76
    move-result-object p0

    .line 77
    :goto_1
    iget-object p1, p1, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->〇0O:Lkotlin/jvm/functions/Function3;

    .line 78
    .line 79
    if-eqz p1, :cond_6

    .line 80
    .line 81
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 82
    .line 83
    .line 84
    move-result-object v1

    .line 85
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 86
    .line 87
    .line 88
    move-result-object v2

    .line 89
    invoke-interface {p1, p0, v1, v2}, Lkotlin/jvm/functions/Function3;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    .line 91
    .line 92
    :cond_6
    invoke-virtual {p2, v5}, Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;->set(Ljava/lang/Object;)Z

    .line 93
    .line 94
    .line 95
    goto :goto_2

    .line 96
    :cond_7
    new-instance p0, Landroidx/core/os/OperationCanceledException;

    .line 97
    .line 98
    invoke-direct {p0, v0}, Landroidx/core/os/OperationCanceledException;-><init>(Ljava/lang/String;)V

    .line 99
    .line 100
    .line 101
    invoke-virtual {p2, p0}, Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;->setException(Ljava/lang/Throwable;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    .line 103
    .line 104
    goto :goto_2

    .line 105
    :catch_0
    new-instance p0, Landroidx/core/os/OperationCanceledException;

    .line 106
    .line 107
    invoke-direct {p0, v0}, Landroidx/core/os/OperationCanceledException;-><init>(Ljava/lang/String;)V

    .line 108
    .line 109
    .line 110
    invoke-virtual {p2, p0}, Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;->setException(Ljava/lang/Throwable;)Z

    .line 111
    .line 112
    .line 113
    :goto_2
    return-void
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/google/android/camera/compat/imagereader/ImageProxy;Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->〇80〇808〇O(Lcom/google/android/camera/compat/imagereader/ImageProxy;Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public static synthetic 〇o〇(Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;Lcom/google/android/camera/compat/imagereader/ImageProxy;Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->oO80(Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;Lcom/google/android/camera/compat/imagereader/ImageProxy;Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;)Lkotlin/Unit;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private final 〇〇888(Lcom/google/android/camera/compat/imagereader/ImageProxy;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/camera/compat/imagereader/ImageProxy;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, L〇0〇O0088o/〇o〇;

    .line 2
    .line 3
    invoke-direct {v0, p0, p1}, L〇0〇O0088o/〇o〇;-><init>(Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;Lcom/google/android/camera/compat/imagereader/ImageProxy;)V

    .line 4
    .line 5
    .line 6
    invoke-static {v0}, Landroidx/concurrent/futures/CallbackToFutureAdapter;->getFuture(Landroidx/concurrent/futures/CallbackToFutureAdapter$Resolver;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    const-string v0, "getFuture { completer: C\u2026}\n            }\n        }"

    .line 11
    .line 12
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    return-object p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public final Oooo8o0〇(Lcom/google/android/camera/size/CameraSize;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->〇08O〇00〇o:Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->o〇0()V

    .line 9
    .line 10
    .line 11
    :cond_0
    iput-object v1, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->〇08O〇00〇o:Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;

    .line 12
    .line 13
    :cond_1
    if-eqz p1, :cond_5

    .line 14
    .line 15
    invoke-virtual {p1}, Lcom/google/android/camera/size/CameraSize;->getWidth()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-lez v0, :cond_5

    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/google/android/camera/size/CameraSize;->getHeight()I

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-gtz v0, :cond_2

    .line 26
    .line 27
    goto :goto_1

    .line 28
    :cond_2
    new-instance v0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;

    .line 29
    .line 30
    sget-object v2, Lcom/google/android/camera/compat/imagereader/ImageReaderProxys;->〇080:Lcom/google/android/camera/compat/imagereader/ImageReaderProxys$Companion;

    .line 31
    .line 32
    invoke-virtual {p1}, Lcom/google/android/camera/size/CameraSize;->getWidth()I

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    invoke-virtual {p1}, Lcom/google/android/camera/size/CameraSize;->getHeight()I

    .line 37
    .line 38
    .line 39
    move-result p1

    .line 40
    const/16 v4, 0x23

    .line 41
    .line 42
    const/4 v5, 0x4

    .line 43
    invoke-virtual {v2, v3, p1, v4, v5}, Lcom/google/android/camera/compat/imagereader/ImageReaderProxys$Companion;->〇080(IIII)Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    invoke-direct {v0, p1}, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;-><init>(Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;)V

    .line 48
    .line 49
    .line 50
    iput-object v0, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->〇08O〇00〇o:Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;

    .line 51
    .line 52
    iget-object p1, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->o0:Ljava/util/concurrent/Executor;

    .line 53
    .line 54
    invoke-virtual {v0, p0, p1}, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->〇080(Lcom/google/android/camera/compat/imagereader/ImageReaderProxy$OnImageAvailableListener;Ljava/util/concurrent/Executor;)V

    .line 55
    .line 56
    .line 57
    const/4 p1, 0x2

    .line 58
    new-array p1, p1, [Ljava/lang/Object;

    .line 59
    .line 60
    iget-object v0, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->〇08O〇00〇o:Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;

    .line 61
    .line 62
    if-eqz v0, :cond_3

    .line 63
    .line 64
    invoke-virtual {v0}, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->getWidth()I

    .line 65
    .line 66
    .line 67
    move-result v0

    .line 68
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    goto :goto_0

    .line 73
    :cond_3
    move-object v0, v1

    .line 74
    :goto_0
    const/4 v2, 0x0

    .line 75
    aput-object v0, p1, v2

    .line 76
    .line 77
    iget-object v0, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->〇08O〇00〇o:Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;

    .line 78
    .line 79
    if-eqz v0, :cond_4

    .line 80
    .line 81
    invoke-virtual {v0}, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->getHeight()I

    .line 82
    .line 83
    .line 84
    move-result v0

    .line 85
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 86
    .line 87
    .line 88
    move-result-object v1

    .line 89
    :cond_4
    const/4 v0, 0x1

    .line 90
    aput-object v1, p1, v0

    .line 91
    .line 92
    const-string v0, "CameraX-CameraImageReaderProxy"

    .line 93
    .line 94
    const-string v1, "prepareImagePreviewReader, size: %d x %d"

    .line 95
    .line 96
    invoke-static {v0, v1, p1}, Lcom/google/android/camera/log/CameraLog;->oO80(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 97
    .line 98
    .line 99
    nop

    .line 100
    :cond_5
    :goto_1
    return-void
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method public 〇080(Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;)V
    .locals 2
    .param p1    # Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "imageReader"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;->acquireLatestImage()Lcom/google/android/camera/compat/imagereader/ImageProxy;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    invoke-direct {p0, p1}, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->OO0o〇〇(Lcom/google/android/camera/compat/imagereader/ImageProxy;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 13
    .line 14
    .line 15
    goto :goto_0

    .line 16
    :catchall_0
    move-exception p1

    .line 17
    const-string v0, "CameraX-CameraImageReaderProxy"

    .line 18
    .line 19
    const-string v1, "Failed to acquire image."

    .line 20
    .line 21
    invoke-static {v0, v1, p1}, Lcom/google/android/camera/log/CameraLog;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 22
    .line 23
    .line 24
    :cond_0
    :goto_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public final 〇8o8o〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->〇080OO8〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final 〇O00(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->〇OOo8〇0:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final 〇O8o08O()Landroid/view/Surface;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->〇08O〇00〇o:Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->getSurface()Landroid/view/Surface;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final 〇O〇(Lkotlin/jvm/functions/Function3;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function3<",
            "-[B-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->〇0O:Lkotlin/jvm/functions/Function3;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final 〇〇808〇()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->OO0o〇〇〇〇0()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->〇08O〇00〇o:Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;

    .line 5
    .line 6
    if-eqz v0, :cond_1

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->o〇0()V

    .line 11
    .line 12
    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    iput-object v0, p0, Lcom/google/android/camera/compat/imagereader/CameraImageReaderProxy;->〇08O〇00〇o:Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;

    .line 15
    .line 16
    :cond_1
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

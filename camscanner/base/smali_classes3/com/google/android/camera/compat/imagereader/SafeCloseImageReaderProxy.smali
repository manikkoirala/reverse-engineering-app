.class public final Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;
.super Ljava/lang/Object;
.source "SafeCloseImageReaderProxy.kt"

# interfaces
.implements Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8:Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;
    .annotation build Landroidx/annotation/GuardedBy;
        value = "mLock"
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final Oo08:Landroid/view/Surface;

.field private o〇0:Lcom/google/android/camera/compat/imagereader/ForwardingImageProxy$OnImageCloseListener;

.field private final 〇080:Ljava/lang/Object;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private volatile 〇o00〇〇Oo:I
    .annotation build Landroidx/annotation/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private volatile 〇o〇:Z
    .annotation build Landroidx/annotation/GuardedBy;
        value = "mLock"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;)V
    .locals 1
    .param p1    # Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "imageReaderProxy"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    new-instance v0, Ljava/lang/Object;

    .line 10
    .line 11
    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->〇080:Ljava/lang/Object;

    .line 15
    .line 16
    iput-object p1, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->O8:Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;

    .line 17
    .line 18
    invoke-interface {p1}, Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;->getSurface()Landroid/view/Surface;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    iput-object p1, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->Oo08:Landroid/view/Surface;

    .line 23
    .line 24
    new-instance p1, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy$mImageCloseListener$1;

    .line 25
    .line 26
    invoke-direct {p1, p0}, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy$mImageCloseListener$1;-><init>(Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;)V

    .line 27
    .line 28
    .line 29
    iput-object p1, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->o〇0:Lcom/google/android/camera/compat/imagereader/ForwardingImageProxy$OnImageCloseListener;

    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public static final synthetic O8(Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->〇o00〇〇Oo:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static final synthetic Oo08(Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->〇o00〇〇Oo:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->〇o〇:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static final synthetic 〇o〇(Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;)Ljava/lang/Object;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->〇080:Ljava/lang/Object;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final 〇〇888(Lcom/google/android/camera/compat/imagereader/ImageProxy;)Lcom/google/android/camera/compat/imagereader/ImageProxy;
    .locals 2
    .annotation build Landroidx/annotation/GuardedBy;
        value = "mLock"
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->〇080:Ljava/lang/Object;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    :try_start_0
    iget v1, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->〇o00〇〇Oo:I

    .line 7
    .line 8
    add-int/lit8 v1, v1, 0x1

    .line 9
    .line 10
    iput v1, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->〇o00〇〇Oo:I

    .line 11
    .line 12
    new-instance v1, Lcom/google/android/camera/compat/imagereader/SingleCloseImageProxy;

    .line 13
    .line 14
    invoke-direct {v1, p1}, Lcom/google/android/camera/compat/imagereader/SingleCloseImageProxy;-><init>(Lcom/google/android/camera/compat/imagereader/ImageProxy;)V

    .line 15
    .line 16
    .line 17
    iget-object p1, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->o〇0:Lcom/google/android/camera/compat/imagereader/ForwardingImageProxy$OnImageCloseListener;

    .line 18
    .line 19
    invoke-virtual {v1, p1}, Lcom/google/android/camera/compat/imagereader/ForwardingImageProxy;->Oo08(Lcom/google/android/camera/compat/imagereader/ForwardingImageProxy$OnImageCloseListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :catchall_0
    move-exception p1

    .line 24
    monitor-exit v0

    .line 25
    throw p1

    .line 26
    :cond_0
    const/4 v1, 0x0

    .line 27
    :goto_0
    monitor-exit v0

    .line 28
    return-object v1
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method


# virtual methods
.method public acquireLatestImage()Lcom/google/android/camera/compat/imagereader/ImageProxy;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->〇080:Ljava/lang/Object;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    :try_start_0
    iget-object v1, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->O8:Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;

    .line 5
    .line 6
    invoke-interface {v1}, Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;->acquireLatestImage()Lcom/google/android/camera/compat/imagereader/ImageProxy;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-direct {p0, v1}, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->〇〇888(Lcom/google/android/camera/compat/imagereader/ImageProxy;)Lcom/google/android/camera/compat/imagereader/ImageProxy;

    .line 11
    .line 12
    .line 13
    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 14
    monitor-exit v0

    .line 15
    return-object v1

    .line 16
    :catchall_0
    move-exception v1

    .line 17
    monitor-exit v0

    .line 18
    throw v1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public clearOnImageAvailableListener()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->〇080:Ljava/lang/Object;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    :try_start_0
    iget-object v1, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->O8:Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;

    .line 5
    .line 6
    invoke-interface {v1}, Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;->clearOnImageAvailableListener()V

    .line 7
    .line 8
    .line 9
    sget-object v1, Lkotlin/Unit;->〇080:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10
    .line 11
    monitor-exit v0

    .line 12
    return-void

    .line 13
    :catchall_0
    move-exception v1

    .line 14
    monitor-exit v0

    .line 15
    throw v1
.end method

.method public close()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->〇080:Ljava/lang/Object;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    :try_start_0
    iget-object v1, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->Oo08:Landroid/view/Surface;

    .line 5
    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    invoke-virtual {v1}, Landroid/view/Surface;->release()V

    .line 9
    .line 10
    .line 11
    :cond_0
    iget-object v1, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->O8:Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;

    .line 12
    .line 13
    invoke-interface {v1}, Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;->close()V

    .line 14
    .line 15
    .line 16
    sget-object v1, Lkotlin/Unit;->〇080:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17
    .line 18
    monitor-exit v0

    .line 19
    return-void

    .line 20
    :catchall_0
    move-exception v1

    .line 21
    monitor-exit v0

    .line 22
    throw v1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public getHeight()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->〇080:Ljava/lang/Object;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    :try_start_0
    iget-object v1, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->O8:Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;

    .line 5
    .line 6
    invoke-interface {v1}, Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;->getHeight()I

    .line 7
    .line 8
    .line 9
    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10
    monitor-exit v0

    .line 11
    return v1

    .line 12
    :catchall_0
    move-exception v1

    .line 13
    monitor-exit v0

    .line 14
    throw v1
    .line 15
.end method

.method public getSurface()Landroid/view/Surface;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->〇080:Ljava/lang/Object;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    :try_start_0
    iget-object v1, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->O8:Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;

    .line 5
    .line 6
    invoke-interface {v1}, Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;->getSurface()Landroid/view/Surface;

    .line 7
    .line 8
    .line 9
    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10
    monitor-exit v0

    .line 11
    return-object v1

    .line 12
    :catchall_0
    move-exception v1

    .line 13
    monitor-exit v0

    .line 14
    throw v1
    .line 15
.end method

.method public getWidth()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->〇080:Ljava/lang/Object;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    :try_start_0
    iget-object v1, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->O8:Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;

    .line 5
    .line 6
    invoke-interface {v1}, Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;->getWidth()I

    .line 7
    .line 8
    .line 9
    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10
    monitor-exit v0

    .line 11
    return v1

    .line 12
    :catchall_0
    move-exception v1

    .line 13
    monitor-exit v0

    .line 14
    throw v1
    .line 15
.end method

.method public final o〇0()V
    .locals 2
    .annotation build Landroidx/annotation/GuardedBy;
        value = "mLock"
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->〇080:Ljava/lang/Object;

    .line 2
    .line 3
    monitor-enter v0

    .line 4
    const/4 v1, 0x1

    .line 5
    :try_start_0
    iput-boolean v1, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->〇o〇:Z

    .line 6
    .line 7
    iget-object v1, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->O8:Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;

    .line 8
    .line 9
    invoke-interface {v1}, Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;->clearOnImageAvailableListener()V

    .line 10
    .line 11
    .line 12
    iget v1, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->〇o00〇〇Oo:I

    .line 13
    .line 14
    if-nez v1, :cond_0

    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->close()V

    .line 17
    .line 18
    .line 19
    :cond_0
    sget-object v1, Lkotlin/Unit;->〇080:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    .line 21
    monitor-exit v0

    .line 22
    return-void

    .line 23
    :catchall_0
    move-exception v1

    .line 24
    monitor-exit v0

    .line 25
    throw v1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public 〇080(Lcom/google/android/camera/compat/imagereader/ImageReaderProxy$OnImageAvailableListener;Ljava/util/concurrent/Executor;)V
    .locals 3
    .param p1    # Lcom/google/android/camera/compat/imagereader/ImageReaderProxy$OnImageAvailableListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "listener"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "executor"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->〇080:Ljava/lang/Object;

    .line 12
    .line 13
    monitor-enter v0

    .line 14
    :try_start_0
    iget-object v1, p0, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;->O8:Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;

    .line 15
    .line 16
    new-instance v2, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy$setOnImageAvailableListener$1$1;

    .line 17
    .line 18
    invoke-direct {v2, p1, p0}, Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy$setOnImageAvailableListener$1$1;-><init>(Lcom/google/android/camera/compat/imagereader/ImageReaderProxy$OnImageAvailableListener;Lcom/google/android/camera/compat/imagereader/SafeCloseImageReaderProxy;)V

    .line 19
    .line 20
    .line 21
    invoke-interface {v1, v2, p2}, Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;->〇080(Lcom/google/android/camera/compat/imagereader/ImageReaderProxy$OnImageAvailableListener;Ljava/util/concurrent/Executor;)V

    .line 22
    .line 23
    .line 24
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 25
    .line 26
    monitor-exit v0

    .line 27
    return-void

    .line 28
    :catchall_0
    move-exception p1

    .line 29
    monitor-exit v0

    .line 30
    throw p1
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

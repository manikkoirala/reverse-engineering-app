.class public final Lcom/google/android/camera/compat/params/OutputConfigurationCompat;
.super Ljava/lang/Object;
.source "OutputConfigurationCompat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/camera/compat/params/OutputConfigurationCompat$OutputConfigurationCompatImpl;
    }
.end annotation


# instance fields
.field private final 〇080:Lcom/google/android/camera/compat/params/OutputConfigurationCompat$OutputConfigurationCompatImpl;


# direct methods
.method public constructor <init>(Landroid/view/Surface;)V
    .locals 2
    .param p1    # Landroid/view/Surface;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-lt v0, v1, :cond_0

    .line 3
    new-instance v0, Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi28Impl;

    invoke-direct {v0, p1}, Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi28Impl;-><init>(Landroid/view/Surface;)V

    iput-object v0, p0, Lcom/google/android/camera/compat/params/OutputConfigurationCompat;->〇080:Lcom/google/android/camera/compat/params/OutputConfigurationCompat$OutputConfigurationCompatImpl;

    goto :goto_0

    :cond_0
    const/16 v1, 0x1a

    if-lt v0, v1, :cond_1

    .line 4
    new-instance v0, Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi26Impl;

    invoke-direct {v0, p1}, Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi26Impl;-><init>(Landroid/view/Surface;)V

    iput-object v0, p0, Lcom/google/android/camera/compat/params/OutputConfigurationCompat;->〇080:Lcom/google/android/camera/compat/params/OutputConfigurationCompat$OutputConfigurationCompatImpl;

    goto :goto_0

    :cond_1
    const/16 v1, 0x18

    if-lt v0, v1, :cond_2

    .line 5
    new-instance v0, Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi24Impl;

    invoke-direct {v0, p1}, Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi24Impl;-><init>(Landroid/view/Surface;)V

    iput-object v0, p0, Lcom/google/android/camera/compat/params/OutputConfigurationCompat;->〇080:Lcom/google/android/camera/compat/params/OutputConfigurationCompat$OutputConfigurationCompatImpl;

    goto :goto_0

    .line 6
    :cond_2
    new-instance v0, Lcom/google/android/camera/compat/params/OutputConfigurationCompatBaseImpl;

    invoke-direct {v0, p1}, Lcom/google/android/camera/compat/params/OutputConfigurationCompatBaseImpl;-><init>(Landroid/view/Surface;)V

    iput-object v0, p0, Lcom/google/android/camera/compat/params/OutputConfigurationCompat;->〇080:Lcom/google/android/camera/compat/params/OutputConfigurationCompat$OutputConfigurationCompatImpl;

    :goto_0
    return-void
.end method

.method private constructor <init>(Lcom/google/android/camera/compat/params/OutputConfigurationCompat$OutputConfigurationCompatImpl;)V
    .locals 0
    .param p1    # Lcom/google/android/camera/compat/params/OutputConfigurationCompat$OutputConfigurationCompatImpl;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object p1, p0, Lcom/google/android/camera/compat/params/OutputConfigurationCompat;->〇080:Lcom/google/android/camera/compat/params/OutputConfigurationCompat$OutputConfigurationCompatImpl;

    return-void
.end method

.method public static O8(Ljava/lang/Object;)Lcom/google/android/camera/compat/params/OutputConfigurationCompat;
    .locals 3
    .param p0    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p0, :cond_0

    .line 3
    .line 4
    return-object v0

    .line 5
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 6
    .line 7
    const/16 v2, 0x1c

    .line 8
    .line 9
    if-lt v1, v2, :cond_1

    .line 10
    .line 11
    check-cast p0, Landroid/hardware/camera2/params/OutputConfiguration;

    .line 12
    .line 13
    invoke-static {p0}, Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi28Impl;->〇o〇(Landroid/hardware/camera2/params/OutputConfiguration;)Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi28Impl;

    .line 14
    .line 15
    .line 16
    move-result-object p0

    .line 17
    goto :goto_0

    .line 18
    :cond_1
    const/16 v2, 0x1a

    .line 19
    .line 20
    if-lt v1, v2, :cond_2

    .line 21
    .line 22
    check-cast p0, Landroid/hardware/camera2/params/OutputConfiguration;

    .line 23
    .line 24
    invoke-static {p0}, Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi26Impl;->〇o00〇〇Oo(Landroid/hardware/camera2/params/OutputConfiguration;)Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi26Impl;

    .line 25
    .line 26
    .line 27
    move-result-object p0

    .line 28
    goto :goto_0

    .line 29
    :cond_2
    const/16 v2, 0x18

    .line 30
    .line 31
    if-lt v1, v2, :cond_3

    .line 32
    .line 33
    check-cast p0, Landroid/hardware/camera2/params/OutputConfiguration;

    .line 34
    .line 35
    invoke-static {p0}, Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi24Impl;->〇080(Landroid/hardware/camera2/params/OutputConfiguration;)Lcom/google/android/camera/compat/params/OutputConfigurationCompatApi24Impl;

    .line 36
    .line 37
    .line 38
    move-result-object p0

    .line 39
    goto :goto_0

    .line 40
    :cond_3
    move-object p0, v0

    .line 41
    :goto_0
    if-nez p0, :cond_4

    .line 42
    .line 43
    return-object v0

    .line 44
    :cond_4
    new-instance v0, Lcom/google/android/camera/compat/params/OutputConfigurationCompat;

    .line 45
    .line 46
    invoke-direct {v0, p0}, Lcom/google/android/camera/compat/params/OutputConfigurationCompat;-><init>(Lcom/google/android/camera/compat/params/OutputConfigurationCompat$OutputConfigurationCompatImpl;)V

    .line 47
    .line 48
    .line 49
    return-object v0
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/google/android/camera/compat/params/OutputConfigurationCompat;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    return p1

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/google/android/camera/compat/params/OutputConfigurationCompat;->〇080:Lcom/google/android/camera/compat/params/OutputConfigurationCompat$OutputConfigurationCompatImpl;

    .line 8
    .line 9
    check-cast p1, Lcom/google/android/camera/compat/params/OutputConfigurationCompat;

    .line 10
    .line 11
    iget-object p1, p1, Lcom/google/android/camera/compat/params/OutputConfigurationCompat;->〇080:Lcom/google/android/camera/compat/params/OutputConfigurationCompat$OutputConfigurationCompatImpl;

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    return p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public hashCode()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/params/OutputConfigurationCompat;->〇080:Lcom/google/android/camera/compat/params/OutputConfigurationCompat$OutputConfigurationCompatImpl;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇080()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation build Landroidx/annotation/RestrictTo;
        value = {
            .enum Landroidx/annotation/RestrictTo$Scope;->LIBRARY:Landroidx/annotation/RestrictTo$Scope;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/params/OutputConfigurationCompat;->〇080:Lcom/google/android/camera/compat/params/OutputConfigurationCompat$OutputConfigurationCompatImpl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/google/android/camera/compat/params/OutputConfigurationCompat$OutputConfigurationCompatImpl;->getPhysicalCameraId()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇o00〇〇Oo()Landroid/view/Surface;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/params/OutputConfigurationCompat;->〇080:Lcom/google/android/camera/compat/params/OutputConfigurationCompat$OutputConfigurationCompatImpl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/google/android/camera/compat/params/OutputConfigurationCompat$OutputConfigurationCompatImpl;->getSurface()Landroid/view/Surface;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇o〇()Ljava/lang/Object;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/params/OutputConfigurationCompat;->〇080:Lcom/google/android/camera/compat/params/OutputConfigurationCompat$OutputConfigurationCompatImpl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/google/android/camera/compat/params/OutputConfigurationCompat$OutputConfigurationCompatImpl;->getOutputConfiguration()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.class public final Lcom/google/android/camera/compat/impl/SurfaceConfig$Companion;
.super Ljava/lang/Object;
.source "SurfaceConfig.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/camera/compat/impl/SurfaceConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/google/android/camera/compat/impl/SurfaceConfig$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final 〇080(Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigType;Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;)Lcom/google/android/camera/compat/impl/SurfaceConfig;
    .locals 1
    .param p1    # Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string/jumbo v0, "type"

    .line 2
    .line 3
    .line 4
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    const-string/jumbo v0, "size"

    .line 8
    .line 9
    .line 10
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    new-instance v0, Lcom/google/android/camera/compat/impl/AutoValue_SurfaceConfig;

    .line 14
    .line 15
    invoke-direct {v0, p1, p2}, Lcom/google/android/camera/compat/impl/AutoValue_SurfaceConfig;-><init>(Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigType;Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;)V

    .line 16
    .line 17
    .line 18
    return-object v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

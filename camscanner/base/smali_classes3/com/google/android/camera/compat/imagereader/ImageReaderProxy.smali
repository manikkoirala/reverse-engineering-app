.class public interface abstract Lcom/google/android/camera/compat/imagereader/ImageReaderProxy;
.super Ljava/lang/Object;
.source "ImageReaderProxy.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/camera/compat/imagereader/ImageReaderProxy$OnImageAvailableListener;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# virtual methods
.method public abstract acquireLatestImage()Lcom/google/android/camera/compat/imagereader/ImageProxy;
.end method

.method public abstract clearOnImageAvailableListener()V
.end method

.method public abstract close()V
.end method

.method public abstract getHeight()I
.end method

.method public abstract getSurface()Landroid/view/Surface;
.end method

.method public abstract getWidth()I
.end method

.method public abstract 〇080(Lcom/google/android/camera/compat/imagereader/ImageReaderProxy$OnImageAvailableListener;Ljava/util/concurrent/Executor;)V
    .param p1    # Lcom/google/android/camera/compat/imagereader/ImageReaderProxy$OnImageAvailableListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

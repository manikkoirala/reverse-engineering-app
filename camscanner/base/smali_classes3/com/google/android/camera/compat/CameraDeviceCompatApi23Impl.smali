.class Lcom/google/android/camera/compat/CameraDeviceCompatApi23Impl;
.super Lcom/google/android/camera/compat/CameraDeviceCompatBaseImpl;
.source "CameraDeviceCompatApi23Impl.java"


# annotations
.annotation build Landroidx/annotation/RequiresApi;
    value = 0x17
.end annotation


# direct methods
.method constructor <init>(Landroid/hardware/camera2/CameraDevice;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/hardware/camera2/CameraDevice;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/google/android/camera/compat/CameraDeviceCompatBaseImpl;-><init>(Landroid/hardware/camera2/CameraDevice;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method static oO80(Landroid/hardware/camera2/CameraDevice;Landroid/os/Handler;)Lcom/google/android/camera/compat/CameraDeviceCompatApi23Impl;
    .locals 2
    .param p0    # Landroid/hardware/camera2/CameraDevice;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Landroid/os/Handler;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    new-instance v0, Lcom/google/android/camera/compat/CameraDeviceCompatApi23Impl;

    .line 2
    .line 3
    new-instance v1, Lcom/google/android/camera/compat/CameraDeviceCompatBaseImpl$CameraDeviceCompatParamsApi21;

    .line 4
    .line 5
    invoke-direct {v1, p1}, Lcom/google/android/camera/compat/CameraDeviceCompatBaseImpl$CameraDeviceCompatParamsApi21;-><init>(Landroid/os/Handler;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {v0, p0, v1}, Lcom/google/android/camera/compat/CameraDeviceCompatApi23Impl;-><init>(Landroid/hardware/camera2/CameraDevice;Ljava/lang/Object;)V

    .line 9
    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method


# virtual methods
.method public 〇080(Lcom/google/android/camera/compat/params/SessionConfigurationCompat;)V
    .locals 4
    .param p1    # Lcom/google/android/camera/compat/params/SessionConfigurationCompat;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/camera/compat/exception/CameraAccessExceptionCompat;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/CameraDeviceCompatBaseImpl;->〇080:Landroid/hardware/camera2/CameraDevice;

    .line 2
    .line 3
    invoke-static {v0, p1}, Lcom/google/android/camera/compat/CameraDeviceCompatBaseImpl;->O8(Landroid/hardware/camera2/CameraDevice;Lcom/google/android/camera/compat/params/SessionConfigurationCompat;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Lcom/google/android/camera/compat/CameraCaptureSessionCompat$StateCallbackExecutorWrapper;

    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/google/android/camera/compat/params/SessionConfigurationCompat;->〇080()Ljava/util/concurrent/Executor;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    invoke-virtual {p1}, Lcom/google/android/camera/compat/params/SessionConfigurationCompat;->Oo08()Landroid/hardware/camera2/CameraCaptureSession$StateCallback;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    invoke-direct {v0, v1, v2}, Lcom/google/android/camera/compat/CameraCaptureSessionCompat$StateCallbackExecutorWrapper;-><init>(Ljava/util/concurrent/Executor;Landroid/hardware/camera2/CameraCaptureSession$StateCallback;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1}, Lcom/google/android/camera/compat/params/SessionConfigurationCompat;->〇o〇()Ljava/util/List;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-static {v1}, Lcom/google/android/camera/compat/CameraDeviceCompatBaseImpl;->〇〇888(Ljava/util/List;)Ljava/util/List;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    iget-object v2, p0, Lcom/google/android/camera/compat/CameraDeviceCompatBaseImpl;->〇o00〇〇Oo:Ljava/lang/Object;

    .line 28
    .line 29
    check-cast v2, Lcom/google/android/camera/compat/CameraDeviceCompatBaseImpl$CameraDeviceCompatParamsApi21;

    .line 30
    .line 31
    invoke-static {v2}, Lcom/google/android/camera/compat/Preconditions;->〇o〇(Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    check-cast v2, Lcom/google/android/camera/compat/CameraDeviceCompatBaseImpl$CameraDeviceCompatParamsApi21;

    .line 36
    .line 37
    iget-object v2, v2, Lcom/google/android/camera/compat/CameraDeviceCompatBaseImpl$CameraDeviceCompatParamsApi21;->〇080:Landroid/os/Handler;

    .line 38
    .line 39
    invoke-virtual {p1}, Lcom/google/android/camera/compat/params/SessionConfigurationCompat;->〇o00〇〇Oo()Lcom/google/android/camera/compat/params/InputConfigurationCompat;

    .line 40
    .line 41
    .line 42
    move-result-object v3

    .line 43
    if-eqz v3, :cond_0

    .line 44
    .line 45
    :try_start_0
    invoke-virtual {v3}, Lcom/google/android/camera/compat/params/InputConfigurationCompat;->〇080()Ljava/lang/Object;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    check-cast p1, Landroid/hardware/camera2/params/InputConfiguration;

    .line 50
    .line 51
    invoke-static {p1}, Lcom/google/android/camera/compat/Preconditions;->〇o〇(Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    .line 53
    .line 54
    iget-object v3, p0, Lcom/google/android/camera/compat/CameraDeviceCompatBaseImpl;->〇080:Landroid/hardware/camera2/CameraDevice;

    .line 55
    .line 56
    invoke-static {v3, p1, v1, v0, v2}, Landroidx/camera/camera2/internal/compat/〇oOO8O8;->〇080(Landroid/hardware/camera2/CameraDevice;Landroid/hardware/camera2/params/InputConfiguration;Ljava/util/List;Landroid/hardware/camera2/CameraCaptureSession$StateCallback;Landroid/os/Handler;)V

    .line 57
    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/camera/compat/params/SessionConfigurationCompat;->O8()I

    .line 61
    .line 62
    .line 63
    move-result p1

    .line 64
    const/4 v3, 0x1

    .line 65
    if-ne p1, v3, :cond_1

    .line 66
    .line 67
    iget-object p1, p0, Lcom/google/android/camera/compat/CameraDeviceCompatBaseImpl;->〇080:Landroid/hardware/camera2/CameraDevice;

    .line 68
    .line 69
    invoke-static {p1, v1, v0, v2}, Landroidx/camera/camera2/internal/compat/〇0000OOO;->〇080(Landroid/hardware/camera2/CameraDevice;Ljava/util/List;Landroid/hardware/camera2/CameraCaptureSession$StateCallback;Landroid/os/Handler;)V

    .line 70
    .line 71
    .line 72
    goto :goto_0

    .line 73
    :cond_1
    iget-object p1, p0, Lcom/google/android/camera/compat/CameraDeviceCompatBaseImpl;->〇080:Landroid/hardware/camera2/CameraDevice;

    .line 74
    .line 75
    invoke-virtual {p0, p1, v1, v0, v2}, Lcom/google/android/camera/compat/CameraDeviceCompatBaseImpl;->o〇0(Landroid/hardware/camera2/CameraDevice;Ljava/util/List;Landroid/hardware/camera2/CameraCaptureSession$StateCallback;Landroid/os/Handler;)V
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    .line 77
    .line 78
    :goto_0
    return-void

    .line 79
    :catch_0
    move-exception p1

    .line 80
    invoke-static {p1}, Lcom/google/android/camera/compat/exception/CameraAccessExceptionCompat;->toCameraAccessExceptionCompat(Landroid/hardware/camera2/CameraAccessException;)Lcom/google/android/camera/compat/exception/CameraAccessExceptionCompat;

    .line 81
    .line 82
    .line 83
    move-result-object p1

    .line 84
    throw p1
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

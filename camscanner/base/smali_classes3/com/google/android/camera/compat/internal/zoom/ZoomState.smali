.class public interface abstract Lcom/google/android/camera/compat/internal/zoom/ZoomState;
.super Ljava/lang/Object;
.source "ZoomState.java"


# virtual methods
.method public abstract getLinearZoom()F
.end method

.method public abstract getMaxZoomRatio()F
.end method

.method public abstract getMinZoomRatio()F
.end method

.method public abstract getZoomRatio()F
.end method

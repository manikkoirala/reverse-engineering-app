.class public Lcom/google/android/camera/compat/internal/Camera2CapturePipeline;
.super Ljava/lang/Object;
.source "Camera2CapturePipeline.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;,
        Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$AfTask;,
        Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$PipelineTask;,
        Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$AePreCaptureTask;,
        Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$ResultListener;
    }
.end annotation


# instance fields
.field private final 〇080:Lcom/google/android/camera/Camera2;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/google/android/camera/compat/quirk/Quirks;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field private final 〇o〇:Ljava/util/concurrent/Executor;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/camera/Camera2;Lcom/google/android/camera/compat/quirk/Quirks;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1    # Lcom/google/android/camera/Camera2;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/camera/compat/quirk/Quirks;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline;->〇080:Lcom/google/android/camera/Camera2;

    .line 5
    .line 6
    iput-object p3, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline;->〇o〇:Ljava/util/concurrent/Executor;

    .line 7
    .line 8
    iput-object p2, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline;->〇o00〇〇Oo:Lcom/google/android/camera/compat/quirk/Quirks;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method static 〇080(ILandroid/hardware/camera2/TotalCaptureResult;)Z
    .locals 4
    .param p1    # Landroid/hardware/camera2/TotalCaptureResult;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    sget-object v0, Lcom/google/android/camera/data/Flash;->〇o〇:Lcom/google/android/camera/data/Flash$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/google/android/camera/data/Flash$Companion;->O8()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    const/4 v2, 0x1

    .line 8
    if-ne p0, v1, :cond_0

    .line 9
    .line 10
    return v2

    .line 11
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/camera/data/Flash$Companion;->〇080()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    const/4 v3, 0x0

    .line 16
    if-ne p0, v1, :cond_3

    .line 17
    .line 18
    if-eqz p1, :cond_1

    .line 19
    .line 20
    sget-object p0, Landroid/hardware/camera2/CaptureResult;->CONTROL_AE_STATE:Landroid/hardware/camera2/CaptureResult$Key;

    .line 21
    .line 22
    invoke-virtual {p1, p0}, Landroid/hardware/camera2/CaptureResult;->get(Landroid/hardware/camera2/CaptureResult$Key;)Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    move-result-object p0

    .line 26
    check-cast p0, Ljava/lang/Integer;

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_1
    const/4 p0, 0x0

    .line 30
    :goto_0
    if-eqz p0, :cond_2

    .line 31
    .line 32
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    .line 33
    .line 34
    .line 35
    move-result p0

    .line 36
    const/4 p1, 0x4

    .line 37
    if-ne p0, p1, :cond_2

    .line 38
    .line 39
    goto :goto_1

    .line 40
    :cond_2
    const/4 v2, 0x0

    .line 41
    :goto_1
    return v2

    .line 42
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/camera/data/Flash$Companion;->〇o〇()I

    .line 43
    .line 44
    .line 45
    return v3
    .line 46
    .line 47
    .line 48
    .line 49
.end method


# virtual methods
.method public 〇o00〇〇Oo(Lcom/google/android/camera/Camera2Config;II)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1    # Lcom/google/android/camera/Camera2Config;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/camera/Camera2Config;",
            "II)",
            "Lcom/google/common/util/concurrent/ListenableFuture<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/google/android/camera/compat/workaround/OverrideAeModeForStillCapture;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline;->〇o00〇〇Oo:Lcom/google/android/camera/compat/quirk/Quirks;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/google/android/camera/compat/workaround/OverrideAeModeForStillCapture;-><init>(Lcom/google/android/camera/compat/quirk/Quirks;)V

    .line 6
    .line 7
    .line 8
    new-instance v1, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;

    .line 9
    .line 10
    iget-object v2, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline;->〇o〇:Ljava/util/concurrent/Executor;

    .line 11
    .line 12
    iget-object v3, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline;->〇080:Lcom/google/android/camera/Camera2;

    .line 13
    .line 14
    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/camera/Camera2;Lcom/google/android/camera/compat/workaround/OverrideAeModeForStillCapture;)V

    .line 15
    .line 16
    .line 17
    if-nez p2, :cond_0

    .line 18
    .line 19
    new-instance p2, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$AfTask;

    .line 20
    .line 21
    iget-object v2, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline;->〇080:Lcom/google/android/camera/Camera2;

    .line 22
    .line 23
    invoke-direct {p2, v2}, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$AfTask;-><init>(Lcom/google/android/camera/Camera2;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1, p2}, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->o〇0(Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$PipelineTask;)V

    .line 27
    .line 28
    .line 29
    :cond_0
    new-instance p2, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$AePreCaptureTask;

    .line 30
    .line 31
    iget-object v2, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline;->〇080:Lcom/google/android/camera/Camera2;

    .line 32
    .line 33
    invoke-direct {p2, v2, p3, v0}, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$AePreCaptureTask;-><init>(Lcom/google/android/camera/Camera2;ILcom/google/android/camera/compat/workaround/OverrideAeModeForStillCapture;)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {v1, p2}, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->o〇0(Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$PipelineTask;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {v1, p1, p3}, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->oO80(Lcom/google/android/camera/Camera2Config;I)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    invoke-static {p1}, Lcom/google/android/camera/compat/utils/futures/Futures;->OO0o〇〇〇〇0(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    return-object p1
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.class public final Lcom/google/android/camera/compat/quirk/ExtraSupportedSurfaceCombinationsQuirk$Companion;
.super Ljava/lang/Object;
.source "ExtraSupportedSurfaceCombinationsQuirk.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/camera/compat/quirk/ExtraSupportedSurfaceCombinationsQuirk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/google/android/camera/compat/quirk/ExtraSupportedSurfaceCombinationsQuirk$Companion;-><init>()V

    return-void
.end method

.method private final O8()Lcom/google/android/camera/compat/impl/SurfaceCombination;
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/camera/compat/impl/SurfaceCombination;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/google/android/camera/compat/impl/SurfaceCombination;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/google/android/camera/compat/impl/SurfaceConfig;->〇080:Lcom/google/android/camera/compat/impl/SurfaceConfig$Companion;

    .line 7
    .line 8
    sget-object v2, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigType;->YUV:Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigType;

    .line 9
    .line 10
    sget-object v3, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;->ANALYSIS:Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 11
    .line 12
    invoke-virtual {v1, v2, v3}, Lcom/google/android/camera/compat/impl/SurfaceConfig$Companion;->〇080(Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigType;Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;)Lcom/google/android/camera/compat/impl/SurfaceConfig;

    .line 13
    .line 14
    .line 15
    move-result-object v3

    .line 16
    invoke-virtual {v0, v3}, Lcom/google/android/camera/compat/impl/SurfaceCombination;->〇080(Lcom/google/android/camera/compat/impl/SurfaceConfig;)Z

    .line 17
    .line 18
    .line 19
    sget-object v3, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;->PREVIEW:Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 20
    .line 21
    invoke-virtual {v1, v2, v3}, Lcom/google/android/camera/compat/impl/SurfaceConfig$Companion;->〇080(Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigType;Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;)Lcom/google/android/camera/compat/impl/SurfaceConfig;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    invoke-virtual {v0, v3}, Lcom/google/android/camera/compat/impl/SurfaceCombination;->〇080(Lcom/google/android/camera/compat/impl/SurfaceConfig;)Z

    .line 26
    .line 27
    .line 28
    sget-object v3, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;->MAXIMUM:Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 29
    .line 30
    invoke-virtual {v1, v2, v3}, Lcom/google/android/camera/compat/impl/SurfaceConfig$Companion;->〇080(Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigType;Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;)Lcom/google/android/camera/compat/impl/SurfaceConfig;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-virtual {v0, v1}, Lcom/google/android/camera/compat/impl/SurfaceCombination;->〇080(Lcom/google/android/camera/compat/impl/SurfaceConfig;)Z

    .line 35
    .line 36
    .line 37
    return-object v0
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final Oo08()Z
    .locals 3

    .line 1
    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "heroqltevzw"

    .line 4
    .line 5
    const/4 v2, 0x1

    .line 6
    invoke-static {v1, v0, v2}, Lkotlin/text/StringsKt;->〇0〇O0088o(Ljava/lang/String;Ljava/lang/String;Z)Z

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    if-nez v1, :cond_1

    .line 11
    .line 12
    const-string v1, "heroqltetmo"

    .line 13
    .line 14
    invoke-static {v1, v0, v2}, Lkotlin/text/StringsKt;->〇0〇O0088o(Ljava/lang/String;Ljava/lang/String;Z)Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-eqz v0, :cond_0

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v2, 0x0

    .line 22
    :cond_1
    :goto_0
    return v2
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static final synthetic 〇080(Lcom/google/android/camera/compat/quirk/ExtraSupportedSurfaceCombinationsQuirk$Companion;)Lcom/google/android/camera/compat/impl/SurfaceCombination;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/google/android/camera/compat/quirk/ExtraSupportedSurfaceCombinationsQuirk$Companion;->〇o〇()Lcom/google/android/camera/compat/impl/SurfaceCombination;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/google/android/camera/compat/quirk/ExtraSupportedSurfaceCombinationsQuirk$Companion;)Lcom/google/android/camera/compat/impl/SurfaceCombination;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/google/android/camera/compat/quirk/ExtraSupportedSurfaceCombinationsQuirk$Companion;->O8()Lcom/google/android/camera/compat/impl/SurfaceCombination;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final 〇o〇()Lcom/google/android/camera/compat/impl/SurfaceCombination;
    .locals 5

    .line 1
    new-instance v0, Lcom/google/android/camera/compat/impl/SurfaceCombination;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/google/android/camera/compat/impl/SurfaceCombination;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/google/android/camera/compat/impl/SurfaceConfig;->〇080:Lcom/google/android/camera/compat/impl/SurfaceConfig$Companion;

    .line 7
    .line 8
    sget-object v2, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigType;->YUV:Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigType;

    .line 9
    .line 10
    sget-object v3, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;->ANALYSIS:Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 11
    .line 12
    invoke-virtual {v1, v2, v3}, Lcom/google/android/camera/compat/impl/SurfaceConfig$Companion;->〇080(Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigType;Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;)Lcom/google/android/camera/compat/impl/SurfaceConfig;

    .line 13
    .line 14
    .line 15
    move-result-object v3

    .line 16
    invoke-virtual {v0, v3}, Lcom/google/android/camera/compat/impl/SurfaceCombination;->〇080(Lcom/google/android/camera/compat/impl/SurfaceConfig;)Z

    .line 17
    .line 18
    .line 19
    sget-object v3, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigType;->PRIV:Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigType;

    .line 20
    .line 21
    sget-object v4, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;->PREVIEW:Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 22
    .line 23
    invoke-virtual {v1, v3, v4}, Lcom/google/android/camera/compat/impl/SurfaceConfig$Companion;->〇080(Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigType;Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;)Lcom/google/android/camera/compat/impl/SurfaceConfig;

    .line 24
    .line 25
    .line 26
    move-result-object v3

    .line 27
    invoke-virtual {v0, v3}, Lcom/google/android/camera/compat/impl/SurfaceCombination;->〇080(Lcom/google/android/camera/compat/impl/SurfaceConfig;)Z

    .line 28
    .line 29
    .line 30
    sget-object v3, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;->MAXIMUM:Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 31
    .line 32
    invoke-virtual {v1, v2, v3}, Lcom/google/android/camera/compat/impl/SurfaceConfig$Companion;->〇080(Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigType;Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;)Lcom/google/android/camera/compat/impl/SurfaceConfig;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    invoke-virtual {v0, v1}, Lcom/google/android/camera/compat/impl/SurfaceCombination;->〇080(Lcom/google/android/camera/compat/impl/SurfaceConfig;)Z

    .line 37
    .line 38
    .line 39
    return-object v0
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final 〇〇888()Z
    .locals 3

    .line 1
    sget-object v0, Landroid/os/Build;->BRAND:Ljava/lang/String;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    const-string v2, "samsung"

    .line 5
    .line 6
    invoke-static {v2, v0, v1}, Lkotlin/text/StringsKt;->〇0〇O0088o(Ljava/lang/String;Ljava/lang/String;Z)Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    const/4 v0, 0x0

    .line 13
    return v0

    .line 14
    :cond_0
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 15
    .line 16
    const-string v1, "MODEL"

    .line 17
    .line 18
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    const-string/jumbo v1, "this as java.lang.String).toUpperCase(Locale.ROOT)"

    .line 28
    .line 29
    .line 30
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    invoke-static {}, Lcom/google/android/camera/compat/quirk/ExtraSupportedSurfaceCombinationsQuirk;->〇080()Ljava/util/Set;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    return v0
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method


# virtual methods
.method public final o〇0()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/android/camera/compat/quirk/ExtraSupportedSurfaceCombinationsQuirk$Companion;->Oo08()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/google/android/camera/compat/quirk/ExtraSupportedSurfaceCombinationsQuirk$Companion;->〇〇888()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    goto :goto_1

    .line 16
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 17
    :goto_1
    return v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

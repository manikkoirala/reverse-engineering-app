.class Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;
.super Ljava/lang/Object;
.source "Camera2CapturePipeline.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/camera/compat/internal/Camera2CapturePipeline;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Pipeline"
.end annotation


# static fields
.field private static final oO80:J

.field private static final 〇〇888:J


# instance fields
.field private O8:J

.field final Oo08:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$PipelineTask;",
            ">;"
        }
    .end annotation
.end field

.field private final o〇0:Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$PipelineTask;

.field private final 〇080:Ljava/util/concurrent/Executor;

.field private final 〇o00〇〇Oo:Lcom/google/android/camera/Camera2;

.field private final 〇o〇:Lcom/google/android/camera/compat/workaround/OverrideAeModeForStillCapture;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 1
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 2
    .line 3
    const-wide/16 v1, 0x1

    .line 4
    .line 5
    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    .line 6
    .line 7
    .line 8
    move-result-wide v1

    .line 9
    sput-wide v1, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->〇〇888:J

    .line 10
    .line 11
    const-wide/16 v1, 0x5

    .line 12
    .line 13
    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    .line 14
    .line 15
    .line 16
    move-result-wide v0

    .line 17
    sput-wide v0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->oO80:J

    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method constructor <init>(Ljava/util/concurrent/Executor;Lcom/google/android/camera/Camera2;Lcom/google/android/camera/compat/workaround/OverrideAeModeForStillCapture;)V
    .locals 2
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/camera/Camera2;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/google/android/camera/compat/workaround/OverrideAeModeForStillCapture;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    sget-wide v0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->〇〇888:J

    .line 5
    .line 6
    iput-wide v0, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->O8:J

    .line 7
    .line 8
    new-instance v0, Ljava/util/ArrayList;

    .line 9
    .line 10
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->Oo08:Ljava/util/List;

    .line 14
    .line 15
    new-instance v0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline$1;

    .line 16
    .line 17
    invoke-direct {v0, p0}, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline$1;-><init>(Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;)V

    .line 18
    .line 19
    .line 20
    iput-object v0, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->o〇0:Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$PipelineTask;

    .line 21
    .line 22
    iput-object p1, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->〇080:Ljava/util/concurrent/Executor;

    .line 23
    .line 24
    iput-object p2, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->〇o00〇〇Oo:Lcom/google/android/camera/Camera2;

    .line 25
    .line 26
    iput-object p3, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->〇o〇:Lcom/google/android/camera/compat/workaround/OverrideAeModeForStillCapture;

    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public static synthetic O8(Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;Lcom/google/android/camera/Camera2Config;ILandroid/hardware/camera2/TotalCaptureResult;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->〇O8o08O(Lcom/google/android/camera/Camera2Config;ILandroid/hardware/camera2/TotalCaptureResult;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
.end method

.method private synthetic OO0o〇〇(Lcom/google/android/camera/Camera2Config;Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline$2;

    .line 2
    .line 3
    invoke-direct {v0, p0, p2}, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline$2;-><init>(Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1, v0}, Lcom/google/android/camera/Camera2Config;->〇o00〇〇Oo(Lcom/google/android/camera/compat/impl/CameraCaptureCallback;)V

    .line 7
    .line 8
    .line 9
    const-string/jumbo p1, "submitStillCapture"

    .line 10
    .line 11
    .line 12
    return-object p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private synthetic OO0o〇〇〇〇0(ILandroid/hardware/camera2/TotalCaptureResult;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    invoke-static {p1, p2}, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline;->〇080(ILandroid/hardware/camera2/TotalCaptureResult;)Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    sget-wide v0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->oO80:J

    .line 8
    .line 9
    invoke-direct {p0, v0, v1}, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->Oooo8o0〇(J)V

    .line 10
    .line 11
    .line 12
    :cond_0
    iget-object p1, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->o〇0:Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$PipelineTask;

    .line 13
    .line 14
    invoke-interface {p1, p2}, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$PipelineTask;->preCapture(Landroid/hardware/camera2/TotalCaptureResult;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    return-object p1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static synthetic Oo08(Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;Lcom/google/android/camera/Camera2Config;Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->OO0o〇〇(Lcom/google/android/camera/Camera2Config;Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private Oooo8o0〇(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->O8:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static synthetic 〇080(Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;Landroid/hardware/camera2/TotalCaptureResult;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->〇80〇808〇O(Landroid/hardware/camera2/TotalCaptureResult;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private 〇80〇808〇O(Landroid/hardware/camera2/TotalCaptureResult;)Z
    .locals 7
    .param p1    # Landroid/hardware/camera2/TotalCaptureResult;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    new-instance v1, Lcom/google/android/camera/compat/internal/Camera2CameraCaptureResult;

    .line 6
    .line 7
    invoke-direct {v1, p1}, Lcom/google/android/camera/compat/internal/Camera2CameraCaptureResult;-><init>(Landroid/hardware/camera2/CaptureResult;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v1}, Lcom/google/android/camera/compat/internal/Camera2CameraCaptureResult;->〇o00〇〇Oo()Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    sget-object v2, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;->OFF:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

    .line 15
    .line 16
    const/4 v3, 0x1

    .line 17
    if-eq p1, v2, :cond_2

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/google/android/camera/compat/internal/Camera2CameraCaptureResult;->〇o00〇〇Oo()Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    sget-object v2, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;->UNKNOWN:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

    .line 24
    .line 25
    if-eq p1, v2, :cond_2

    .line 26
    .line 27
    invoke-virtual {v1}, Lcom/google/android/camera/compat/internal/Camera2CameraCaptureResult;->〇o〇()Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfState;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    sget-object v2, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfState;->PASSIVE_FOCUSED:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfState;

    .line 32
    .line 33
    if-eq p1, v2, :cond_2

    .line 34
    .line 35
    invoke-virtual {v1}, Lcom/google/android/camera/compat/internal/Camera2CameraCaptureResult;->〇o〇()Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfState;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    sget-object v2, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfState;->PASSIVE_NOT_FOCUSED:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfState;

    .line 40
    .line 41
    if-eq p1, v2, :cond_2

    .line 42
    .line 43
    invoke-virtual {v1}, Lcom/google/android/camera/compat/internal/Camera2CameraCaptureResult;->〇o〇()Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfState;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    sget-object v2, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfState;->LOCKED_FOCUSED:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfState;

    .line 48
    .line 49
    if-eq p1, v2, :cond_2

    .line 50
    .line 51
    invoke-virtual {v1}, Lcom/google/android/camera/compat/internal/Camera2CameraCaptureResult;->〇o〇()Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfState;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    sget-object v2, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfState;->LOCKED_NOT_FOCUSED:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfState;

    .line 56
    .line 57
    if-ne p1, v2, :cond_1

    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_1
    const/4 p1, 0x0

    .line 61
    goto :goto_1

    .line 62
    :cond_2
    :goto_0
    const/4 p1, 0x1

    .line 63
    :goto_1
    invoke-virtual {v1}, Lcom/google/android/camera/compat/internal/Camera2CameraCaptureResult;->〇080()Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 64
    .line 65
    .line 66
    move-result-object v2

    .line 67
    sget-object v4, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;->CONVERGED:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 68
    .line 69
    if-eq v2, v4, :cond_4

    .line 70
    .line 71
    invoke-virtual {v1}, Lcom/google/android/camera/compat/internal/Camera2CameraCaptureResult;->〇080()Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 72
    .line 73
    .line 74
    move-result-object v2

    .line 75
    sget-object v4, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;->FLASH_REQUIRED:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 76
    .line 77
    if-eq v2, v4, :cond_4

    .line 78
    .line 79
    invoke-virtual {v1}, Lcom/google/android/camera/compat/internal/Camera2CameraCaptureResult;->〇080()Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 80
    .line 81
    .line 82
    move-result-object v2

    .line 83
    sget-object v4, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;->UNKNOWN:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 84
    .line 85
    if-ne v2, v4, :cond_3

    .line 86
    .line 87
    goto :goto_2

    .line 88
    :cond_3
    const/4 v2, 0x0

    .line 89
    goto :goto_3

    .line 90
    :cond_4
    :goto_2
    const/4 v2, 0x1

    .line 91
    :goto_3
    invoke-virtual {v1}, Lcom/google/android/camera/compat/internal/Camera2CameraCaptureResult;->O8()Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AwbState;

    .line 92
    .line 93
    .line 94
    move-result-object v4

    .line 95
    sget-object v5, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AwbState;->CONVERGED:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AwbState;

    .line 96
    .line 97
    if-eq v4, v5, :cond_6

    .line 98
    .line 99
    invoke-virtual {v1}, Lcom/google/android/camera/compat/internal/Camera2CameraCaptureResult;->O8()Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AwbState;

    .line 100
    .line 101
    .line 102
    move-result-object v4

    .line 103
    sget-object v5, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AwbState;->UNKNOWN:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AwbState;

    .line 104
    .line 105
    if-ne v4, v5, :cond_5

    .line 106
    .line 107
    goto :goto_4

    .line 108
    :cond_5
    const/4 v4, 0x0

    .line 109
    goto :goto_5

    .line 110
    :cond_6
    :goto_4
    const/4 v4, 0x1

    .line 111
    :goto_5
    new-instance v5, Ljava/lang/StringBuilder;

    .line 112
    .line 113
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 114
    .line 115
    .line 116
    const-string v6, "checkCaptureResult, AE="

    .line 117
    .line 118
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    .line 120
    .line 121
    invoke-virtual {v1}, Lcom/google/android/camera/compat/internal/Camera2CameraCaptureResult;->〇080()Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 122
    .line 123
    .line 124
    move-result-object v6

    .line 125
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126
    .line 127
    .line 128
    const-string v6, " AF ="

    .line 129
    .line 130
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    .line 132
    .line 133
    invoke-virtual {v1}, Lcom/google/android/camera/compat/internal/Camera2CameraCaptureResult;->〇o〇()Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfState;

    .line 134
    .line 135
    .line 136
    move-result-object v6

    .line 137
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 138
    .line 139
    .line 140
    const-string v6, " AWB="

    .line 141
    .line 142
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    .line 144
    .line 145
    invoke-virtual {v1}, Lcom/google/android/camera/compat/internal/Camera2CameraCaptureResult;->O8()Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AwbState;

    .line 146
    .line 147
    .line 148
    move-result-object v1

    .line 149
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 150
    .line 151
    .line 152
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 153
    .line 154
    .line 155
    move-result-object v1

    .line 156
    const-string v5, "CameraX-Camera2CapturePipeline"

    .line 157
    .line 158
    invoke-static {v5, v1}, Lcom/google/android/camera/log/CameraLog;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    .line 160
    .line 161
    if-eqz p1, :cond_7

    .line 162
    .line 163
    if-eqz v2, :cond_7

    .line 164
    .line 165
    if-eqz v4, :cond_7

    .line 166
    .line 167
    const/4 v0, 0x1

    .line 168
    :cond_7
    return v0
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method private synthetic 〇8o8o〇(Ljava/lang/Boolean;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    iget-wide v0, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->O8:J

    .line 10
    .line 11
    new-instance p1, Lcom/google/android/camera/compat/internal/〇〇888;

    .line 12
    .line 13
    invoke-direct {p1, p0}, Lcom/google/android/camera/compat/internal/〇〇888;-><init>(Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;)V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->〇O〇(JLcom/google/android/camera/compat/internal/Camera2CapturePipeline$ResultListener$Checker;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    return-object p1

    .line 21
    :cond_0
    const/4 p1, 0x0

    .line 22
    invoke-static {p1}, Lcom/google/android/camera/compat/utils/futures/Futures;->oO80(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    return-object p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private synthetic 〇O8o08O(Lcom/google/android/camera/Camera2Config;ILandroid/hardware/camera2/TotalCaptureResult;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->〇〇808〇(Lcom/google/android/camera/Camera2Config;I)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    return-object p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private 〇O〇(JLcom/google/android/camera/compat/internal/Camera2CapturePipeline$ResultListener$Checker;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p3    # Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$ResultListener$Checker;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$ResultListener$Checker;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture<",
            "Landroid/hardware/camera2/TotalCaptureResult;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$ResultListener;

    .line 2
    .line 3
    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$ResultListener;-><init>(JLcom/google/android/camera/compat/internal/Camera2CapturePipeline$ResultListener$Checker;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->〇o00〇〇Oo:Lcom/google/android/camera/Camera2;

    .line 7
    .line 8
    invoke-virtual {p1, v0}, Lcom/google/android/camera/Camera2;->O〇0〇o808〇(Lcom/google/android/camera/compat/internal/CaptureResultListener;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$ResultListener;->〇o00〇〇Oo()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    return-object p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;ILandroid/hardware/camera2/TotalCaptureResult;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->OO0o〇〇〇〇0(ILandroid/hardware/camera2/TotalCaptureResult;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public static synthetic 〇o〇(Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;Ljava/lang/Boolean;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->〇8o8o〇(Ljava/lang/Boolean;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private 〇〇888(Lcom/google/android/camera/Camera2Config;)V
    .locals 2
    .param p1    # Lcom/google/android/camera/Camera2Config;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    sget-object v0, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    .line 2
    .line 3
    const/4 v1, 0x3

    .line 4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 5
    .line 6
    .line 7
    move-result-object v1

    .line 8
    invoke-virtual {p1, v0, v1}, Lcom/google/android/camera/Camera2Config;->OO0o〇〇〇〇0(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method oO80(Lcom/google/android/camera/Camera2Config;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p1    # Lcom/google/android/camera/Camera2Config;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/camera/Camera2Config;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-static {v0}, Lcom/google/android/camera/compat/utils/futures/Futures;->oO80(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 3
    .line 4
    .line 5
    move-result-object v1

    .line 6
    iget-object v2, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->Oo08:Ljava/util/List;

    .line 7
    .line 8
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    if-nez v2, :cond_1

    .line 13
    .line 14
    iget-object v1, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->o〇0:Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$PipelineTask;

    .line 15
    .line 16
    invoke-interface {v1}, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$PipelineTask;->isCaptureResultNeeded()Z

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    if-eqz v1, :cond_0

    .line 21
    .line 22
    const-wide/16 v1, 0x0

    .line 23
    .line 24
    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->〇O〇(JLcom/google/android/camera/compat/internal/Camera2CapturePipeline$ResultListener$Checker;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    goto :goto_0

    .line 29
    :cond_0
    invoke-static {v0}, Lcom/google/android/camera/compat/utils/futures/Futures;->oO80(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    :goto_0
    invoke-static {v0}, Lcom/google/android/camera/compat/utils/futures/FutureChain;->〇080(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/android/camera/compat/utils/futures/FutureChain;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    new-instance v1, Lcom/google/android/camera/compat/internal/〇o〇;

    .line 38
    .line 39
    invoke-direct {v1, p0, p2}, Lcom/google/android/camera/compat/internal/〇o〇;-><init>(Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;I)V

    .line 40
    .line 41
    .line 42
    iget-object v2, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->〇080:Ljava/util/concurrent/Executor;

    .line 43
    .line 44
    invoke-virtual {v0, v1, v2}, Lcom/google/android/camera/compat/utils/futures/FutureChain;->Oo08(Lcom/google/android/camera/compat/utils/futures/AsyncFunction;Ljava/util/concurrent/Executor;)Lcom/google/android/camera/compat/utils/futures/FutureChain;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    new-instance v1, Lcom/google/android/camera/compat/internal/O8;

    .line 49
    .line 50
    invoke-direct {v1, p0}, Lcom/google/android/camera/compat/internal/O8;-><init>(Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;)V

    .line 51
    .line 52
    .line 53
    iget-object v2, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->〇080:Ljava/util/concurrent/Executor;

    .line 54
    .line 55
    invoke-virtual {v0, v1, v2}, Lcom/google/android/camera/compat/utils/futures/FutureChain;->Oo08(Lcom/google/android/camera/compat/utils/futures/AsyncFunction;Ljava/util/concurrent/Executor;)Lcom/google/android/camera/compat/utils/futures/FutureChain;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    :cond_1
    invoke-static {v1}, Lcom/google/android/camera/compat/utils/futures/FutureChain;->〇080(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/android/camera/compat/utils/futures/FutureChain;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    new-instance v1, Lcom/google/android/camera/compat/internal/Oo08;

    .line 64
    .line 65
    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/camera/compat/internal/Oo08;-><init>(Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;Lcom/google/android/camera/Camera2Config;I)V

    .line 66
    .line 67
    .line 68
    iget-object p1, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->〇080:Ljava/util/concurrent/Executor;

    .line 69
    .line 70
    invoke-virtual {v0, v1, p1}, Lcom/google/android/camera/compat/utils/futures/FutureChain;->Oo08(Lcom/google/android/camera/compat/utils/futures/AsyncFunction;Ljava/util/concurrent/Executor;)Lcom/google/android/camera/compat/utils/futures/FutureChain;

    .line 71
    .line 72
    .line 73
    move-result-object p1

    .line 74
    iget-object p2, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->o〇0:Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$PipelineTask;

    .line 75
    .line 76
    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    .line 78
    .line 79
    new-instance v0, Lcom/google/android/camera/compat/internal/o〇0;

    .line 80
    .line 81
    invoke-direct {v0, p2}, Lcom/google/android/camera/compat/internal/o〇0;-><init>(Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$PipelineTask;)V

    .line 82
    .line 83
    .line 84
    iget-object p2, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->〇080:Ljava/util/concurrent/Executor;

    .line 85
    .line 86
    invoke-interface {p1, v0, p2}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 87
    .line 88
    .line 89
    return-object p1
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

.method o〇0(Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$PipelineTask;)V
    .locals 1
    .param p1    # Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$PipelineTask;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->Oo08:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method 〇〇808〇(Lcom/google/android/camera/Camera2Config;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p1    # Lcom/google/android/camera/Camera2Config;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/camera/Camera2Config;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {p1}, Lcom/google/android/camera/Camera2Config;->o〇0(Lcom/google/android/camera/Camera2Config;)Lcom/google/android/camera/Camera2Config;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iget-object v0, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->〇o〇:Lcom/google/android/camera/compat/workaround/OverrideAeModeForStillCapture;

    .line 6
    .line 7
    invoke-virtual {v0, p2}, Lcom/google/android/camera/compat/workaround/OverrideAeModeForStillCapture;->〇o〇(I)Z

    .line 8
    .line 9
    .line 10
    move-result p2

    .line 11
    if-eqz p2, :cond_0

    .line 12
    .line 13
    invoke-direct {p0, p1}, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->〇〇888(Lcom/google/android/camera/Camera2Config;)V

    .line 14
    .line 15
    .line 16
    :cond_0
    new-instance p2, Lcom/google/android/camera/compat/internal/oO80;

    .line 17
    .line 18
    invoke-direct {p2, p0, p1}, Lcom/google/android/camera/compat/internal/oO80;-><init>(Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;Lcom/google/android/camera/Camera2Config;)V

    .line 19
    .line 20
    .line 21
    invoke-static {p2}, Landroidx/concurrent/futures/CallbackToFutureAdapter;->getFuture(Landroidx/concurrent/futures/CallbackToFutureAdapter$Resolver;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 22
    .line 23
    .line 24
    move-result-object p2

    .line 25
    iget-object v0, p0, Lcom/google/android/camera/compat/internal/Camera2CapturePipeline$Pipeline;->〇o00〇〇Oo:Lcom/google/android/camera/Camera2;

    .line 26
    .line 27
    invoke-virtual {v0, p1}, Lcom/google/android/camera/Camera2;->O〇oO〇oo8o(Lcom/google/android/camera/Camera2Config;)V

    .line 28
    .line 29
    .line 30
    return-object p2
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

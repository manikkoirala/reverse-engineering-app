.class public final enum Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;
.super Ljava/lang/Enum;
.source "CameraCaptureMetaData.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

.field public static final enum OFF:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

.field public static final enum ON_CONTINUOUS_AUTO:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

.field public static final enum ON_MANUAL_AUTO:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

.field public static final enum UNKNOWN:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;


# direct methods
.method private static synthetic $values()[Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;
    .locals 3

    .line 1
    const/4 v0, 0x4

    .line 2
    new-array v0, v0, [Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    sget-object v2, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;->UNKNOWN:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

    .line 6
    .line 7
    aput-object v2, v0, v1

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    sget-object v2, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;->OFF:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

    .line 11
    .line 12
    aput-object v2, v0, v1

    .line 13
    .line 14
    const/4 v1, 0x2

    .line 15
    sget-object v2, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;->ON_MANUAL_AUTO:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

    .line 16
    .line 17
    aput-object v2, v0, v1

    .line 18
    .line 19
    const/4 v1, 0x3

    .line 20
    sget-object v2, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;->ON_CONTINUOUS_AUTO:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

    .line 21
    .line 22
    aput-object v2, v0, v1

    .line 23
    .line 24
    return-object v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

    .line 2
    .line 3
    const-string v1, "UNKNOWN"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-direct {v0, v1, v2}, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;-><init>(Ljava/lang/String;I)V

    .line 7
    .line 8
    .line 9
    sput-object v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;->UNKNOWN:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

    .line 10
    .line 11
    new-instance v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

    .line 12
    .line 13
    const-string v1, "OFF"

    .line 14
    .line 15
    const/4 v2, 0x1

    .line 16
    invoke-direct {v0, v1, v2}, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;-><init>(Ljava/lang/String;I)V

    .line 17
    .line 18
    .line 19
    sput-object v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;->OFF:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

    .line 20
    .line 21
    new-instance v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

    .line 22
    .line 23
    const-string v1, "ON_MANUAL_AUTO"

    .line 24
    .line 25
    const/4 v2, 0x2

    .line 26
    invoke-direct {v0, v1, v2}, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;-><init>(Ljava/lang/String;I)V

    .line 27
    .line 28
    .line 29
    sput-object v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;->ON_MANUAL_AUTO:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

    .line 30
    .line 31
    new-instance v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

    .line 32
    .line 33
    const-string v1, "ON_CONTINUOUS_AUTO"

    .line 34
    .line 35
    const/4 v2, 0x3

    .line 36
    invoke-direct {v0, v1, v2}, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;-><init>(Ljava/lang/String;I)V

    .line 37
    .line 38
    .line 39
    sput-object v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;->ON_CONTINUOUS_AUTO:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

    .line 40
    .line 41
    invoke-static {}, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;->$values()[Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    sput-object v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;->$VALUES:[Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;
    .locals 1

    .line 1
    const-class v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static values()[Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;->$VALUES:[Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AfMode;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

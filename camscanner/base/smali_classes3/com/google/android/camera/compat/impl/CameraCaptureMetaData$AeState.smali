.class public final enum Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;
.super Ljava/lang/Enum;
.source "CameraCaptureMetaData.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

.field public static final enum CONVERGED:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

.field public static final enum FLASH_REQUIRED:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

.field public static final enum INACTIVE:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

.field public static final enum LOCKED:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

.field public static final enum SEARCHING:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

.field public static final enum UNKNOWN:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;


# direct methods
.method private static synthetic $values()[Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;
    .locals 3

    .line 1
    const/4 v0, 0x6

    .line 2
    new-array v0, v0, [Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    sget-object v2, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;->UNKNOWN:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 6
    .line 7
    aput-object v2, v0, v1

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    sget-object v2, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;->INACTIVE:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 11
    .line 12
    aput-object v2, v0, v1

    .line 13
    .line 14
    const/4 v1, 0x2

    .line 15
    sget-object v2, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;->SEARCHING:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 16
    .line 17
    aput-object v2, v0, v1

    .line 18
    .line 19
    const/4 v1, 0x3

    .line 20
    sget-object v2, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;->FLASH_REQUIRED:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 21
    .line 22
    aput-object v2, v0, v1

    .line 23
    .line 24
    const/4 v1, 0x4

    .line 25
    sget-object v2, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;->CONVERGED:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 26
    .line 27
    aput-object v2, v0, v1

    .line 28
    .line 29
    const/4 v1, 0x5

    .line 30
    sget-object v2, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;->LOCKED:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 31
    .line 32
    aput-object v2, v0, v1

    .line 33
    .line 34
    return-object v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 2
    .line 3
    const-string v1, "UNKNOWN"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-direct {v0, v1, v2}, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;-><init>(Ljava/lang/String;I)V

    .line 7
    .line 8
    .line 9
    sput-object v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;->UNKNOWN:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 10
    .line 11
    new-instance v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 12
    .line 13
    const-string v1, "INACTIVE"

    .line 14
    .line 15
    const/4 v2, 0x1

    .line 16
    invoke-direct {v0, v1, v2}, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;-><init>(Ljava/lang/String;I)V

    .line 17
    .line 18
    .line 19
    sput-object v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;->INACTIVE:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 20
    .line 21
    new-instance v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 22
    .line 23
    const-string v1, "SEARCHING"

    .line 24
    .line 25
    const/4 v2, 0x2

    .line 26
    invoke-direct {v0, v1, v2}, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;-><init>(Ljava/lang/String;I)V

    .line 27
    .line 28
    .line 29
    sput-object v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;->SEARCHING:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 30
    .line 31
    new-instance v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 32
    .line 33
    const-string v1, "FLASH_REQUIRED"

    .line 34
    .line 35
    const/4 v2, 0x3

    .line 36
    invoke-direct {v0, v1, v2}, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;-><init>(Ljava/lang/String;I)V

    .line 37
    .line 38
    .line 39
    sput-object v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;->FLASH_REQUIRED:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 40
    .line 41
    new-instance v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 42
    .line 43
    const-string v1, "CONVERGED"

    .line 44
    .line 45
    const/4 v2, 0x4

    .line 46
    invoke-direct {v0, v1, v2}, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;-><init>(Ljava/lang/String;I)V

    .line 47
    .line 48
    .line 49
    sput-object v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;->CONVERGED:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 50
    .line 51
    new-instance v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 52
    .line 53
    const-string v1, "LOCKED"

    .line 54
    .line 55
    const/4 v2, 0x5

    .line 56
    invoke-direct {v0, v1, v2}, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;-><init>(Ljava/lang/String;I)V

    .line 57
    .line 58
    .line 59
    sput-object v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;->LOCKED:Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 60
    .line 61
    invoke-static {}, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;->$values()[Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    sput-object v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;->$VALUES:[Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 66
    .line 67
    return-void
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;
    .locals 1

    .line 1
    const-class v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static values()[Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;->$VALUES:[Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/google/android/camera/compat/impl/CameraCaptureMetaData$AeState;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.class public final Lcom/google/android/camera/compat/view/CameraCompatPreviewView;
.super Landroid/widget/FrameLayout;
.source "CameraCompatPreviewView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/camera/compat/view/CameraCompatPreviewView$DisplayRotationListener;,
        Lcom/google/android/camera/compat/view/CameraCompatPreviewView$SurfaceProvider;,
        Lcom/google/android/camera/compat/view/CameraCompatPreviewView$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final o8〇OO0〇0o:Lcom/google/android/camera/compat/view/CameraCompatPreviewView$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Lcom/google/android/camera/compat/view/PreviewViewImplementation;

.field private OO:Lcom/google/android/camera/size/CameraSize;

.field private OO〇00〇8oO:Lcom/google/android/camera/compat/view/CameraCompatPreviewView$SurfaceProvider;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o0:Lcom/google/android/camera/CameraViewImpl;

.field private final oOo0:Landroid/view/View$OnLayoutChangeListener;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oOo〇8o008:Lcom/google/android/camera/compat/view/CameraCompatPreviewView$DisplayRotationListener;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:I

.field private final 〇080OO8〇0:Lcom/google/android/camera/compat/view/PreviewTransformation;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇08O〇00〇o:Lcom/google/android/camera/compat/view/SurfaceRequest;

.field private 〇0O:Z

.field private 〇OOo8〇0:Lcom/google/android/camera/compat/view/DeferrableSurface;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->o8〇OO0〇0o:Lcom/google/android/camera/compat/view/CameraCompatPreviewView$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    sget-object p2, Lcom/google/android/camera/data/PreviewMode;->O8:Lcom/google/android/camera/data/PreviewMode$Companion;

    invoke-virtual {p2}, Lcom/google/android/camera/data/PreviewMode$Companion;->〇o00〇〇Oo()I

    move-result p2

    iput p2, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->o〇00O:I

    .line 5
    new-instance p2, Lcom/google/android/camera/compat/view/PreviewTransformation;

    invoke-direct {p2}, Lcom/google/android/camera/compat/view/PreviewTransformation;-><init>()V

    iput-object p2, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇080OO8〇0:Lcom/google/android/camera/compat/view/PreviewTransformation;

    const/4 p2, 0x1

    .line 6
    iput-boolean p2, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇0O:Z

    .line 7
    new-instance p2, Lcom/google/android/camera/compat/view/CameraCompatPreviewView$DisplayRotationListener;

    invoke-direct {p2, p0}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView$DisplayRotationListener;-><init>(Lcom/google/android/camera/compat/view/CameraCompatPreviewView;)V

    iput-object p2, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->oOo〇8o008:Lcom/google/android/camera/compat/view/CameraCompatPreviewView$DisplayRotationListener;

    .line 8
    new-instance p2, Lo〇O8〇〇o/〇080;

    invoke-direct {p2, p0}, Lo〇O8〇〇o/〇080;-><init>(Lcom/google/android/camera/compat/view/CameraCompatPreviewView;)V

    iput-object p2, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->oOo0:Landroid/view/View$OnLayoutChangeListener;

    .line 9
    new-instance p2, Lcom/google/android/camera/compat/view/CameraCompatPreviewView$mSurfaceProvider$1;

    invoke-direct {p2, p1, p0}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView$mSurfaceProvider$1;-><init>(Landroid/content/Context;Lcom/google/android/camera/compat/view/CameraCompatPreviewView;)V

    iput-object p2, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->OO〇00〇8oO:Lcom/google/android/camera/compat/view/CameraCompatPreviewView$SurfaceProvider;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 2
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic O8(Lcom/google/android/camera/compat/view/CameraCompatPreviewView;)Lcom/google/android/camera/CameraViewImpl;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->o0:Lcom/google/android/camera/CameraViewImpl;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final OO0o〇〇(J)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->OO〇00〇8oO:Lcom/google/android/camera/compat/view/CameraCompatPreviewView$SurfaceProvider;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/google/android/camera/compat/Preconditions;->〇o〇(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "checkNotNull(mSurfaceProvider)"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    check-cast v0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView$SurfaceProvider;

    .line 13
    .line 14
    iget-object v1, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇08O〇00〇o:Lcom/google/android/camera/compat/view/SurfaceRequest;

    .line 15
    .line 16
    invoke-static {v1}, Lcom/google/android/camera/compat/Preconditions;->〇o〇(Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    const-string v2, "checkNotNull(mCurrentSurfaceRequest)"

    .line 21
    .line 22
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    check-cast v1, Lcom/google/android/camera/compat/view/SurfaceRequest;

    .line 26
    .line 27
    invoke-static {}, Lcom/google/android/camera/compat/utils/executor/CameraXExecutors;->〇o00〇〇Oo()Ljava/util/concurrent/ScheduledExecutorService;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    new-instance v3, Lo〇O8〇〇o/〇o00〇〇Oo;

    .line 32
    .line 33
    invoke-direct {v3, p1, p2, v0, v1}, Lo〇O8〇〇o/〇o00〇〇Oo;-><init>(JLcom/google/android/camera/compat/view/CameraCompatPreviewView$SurfaceProvider;Lcom/google/android/camera/compat/view/SurfaceRequest;)V

    .line 34
    .line 35
    .line 36
    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private static final OO0o〇〇〇〇0(Lcom/google/android/camera/compat/view/CameraCompatPreviewView;Landroid/view/View;IIIIIIII)V
    .locals 0

    .line 1
    const-string/jumbo p1, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    sub-int/2addr p4, p2

    .line 8
    sub-int/2addr p8, p6

    .line 9
    const/4 p1, 0x1

    .line 10
    if-ne p4, p8, :cond_1

    .line 11
    .line 12
    sub-int/2addr p5, p3

    .line 13
    sub-int/2addr p9, p7

    .line 14
    if-eq p5, p9, :cond_0

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 p2, 0x0

    .line 18
    goto :goto_1

    .line 19
    :cond_1
    :goto_0
    const/4 p2, 0x1

    .line 20
    :goto_1
    if-eqz p2, :cond_2

    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇O8o08O()V

    .line 23
    .line 24
    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->Oo08(Z)V

    .line 26
    .line 27
    .line 28
    :cond_2
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
.end method

.method private final Oo08(Z)V
    .locals 2
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getDisplay()Landroid/view/Display;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->o0:Lcom/google/android/camera/CameraViewImpl;

    .line 6
    .line 7
    if-eqz v1, :cond_1

    .line 8
    .line 9
    invoke-virtual {p0}, Landroid/view/View;->isAttachedToWindow()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_1

    .line 14
    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    :try_start_0
    iget-object v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->o0:Lcom/google/android/camera/CameraViewImpl;

    .line 18
    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->oO80()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :catch_0
    move-exception v0

    .line 26
    if-eqz p1, :cond_0

    .line 27
    .line 28
    const-string p1, "CameraX-CameraCompatPreview"

    .line 29
    .line 30
    invoke-static {p1, v0}, Lcom/google/android/camera/log/CameraLog;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_0
    throw v0

    .line 35
    :cond_1
    :goto_0
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private static final Oooo8o0〇(JLcom/google/android/camera/compat/view/CameraCompatPreviewView$SurfaceProvider;Lcom/google/android/camera/compat/view/SurfaceRequest;)V
    .locals 2

    .line 1
    const-string v0, "$surfaceProvider"

    .line 2
    .line 3
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$surfaceRequest"

    .line 7
    .line 8
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    new-instance v0, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v1, "onSurfaceRequested "

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object p0

    .line 28
    const-string p1, "CameraX-CameraCompatPreview"

    .line 29
    .line 30
    invoke-static {p1, p0}, Lcom/google/android/camera/log/CameraLog;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    invoke-interface {p2, p3}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView$SurfaceProvider;->〇080(Lcom/google/android/camera/compat/view/SurfaceRequest;)V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private final getDisplayManager()Landroid/hardware/display/DisplayManager;
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    return-object v0

    .line 9
    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const-string v1, "display"

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const-string v1, "null cannot be cast to non-null type android.hardware.display.DisplayManager"

    .line 20
    .line 21
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    check-cast v0, Landroid/hardware/display/DisplayManager;

    .line 25
    .line 26
    return-object v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static synthetic getMImplementationMode$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private final oO80(Lcom/google/android/camera/size/CameraSize;)Landroid/graphics/Rect;
    .locals 3

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    new-instance v0, Landroid/graphics/Rect;

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/google/android/camera/size/CameraSize;->getWidth()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-virtual {p1}, Lcom/google/android/camera/size/CameraSize;->getHeight()I

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    const/4 v2, 0x0

    .line 14
    invoke-direct {v0, v2, v2, v1, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 15
    .line 16
    .line 17
    return-object v0

    .line 18
    :cond_0
    const/4 p1, 0x0

    .line 19
    return-object p1
    .line 20
    .line 21
    .line 22
.end method

.method public static synthetic 〇080(JLcom/google/android/camera/compat/view/CameraCompatPreviewView$SurfaceProvider;Lcom/google/android/camera/compat/view/SurfaceRequest;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->Oooo8o0〇(JLcom/google/android/camera/compat/view/CameraCompatPreviewView$SurfaceProvider;Lcom/google/android/camera/compat/view/SurfaceRequest;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private final 〇O00()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->getDisplayManager()Landroid/hardware/display/DisplayManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v1, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->oOo〇8o008:Lcom/google/android/camera/compat/view/CameraCompatPreviewView$DisplayRotationListener;

    .line 9
    .line 10
    new-instance v2, Landroid/os/Handler;

    .line 11
    .line 12
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 13
    .line 14
    .line 15
    move-result-object v3

    .line 16
    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1, v2}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final 〇O〇()Z
    .locals 5

    .line 1
    sget-object v0, Lcom/google/android/camera/compat/quirk/DeviceQuirks;->〇080:Lcom/google/android/camera/compat/quirk/DeviceQuirks;

    .line 2
    .line 3
    const-class v1, Lcom/google/android/camera/compat/quirk/SurfaceViewStretchedQuirk;

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/google/android/camera/compat/quirk/DeviceQuirks;->〇080(Ljava/lang/Class;)Lcom/google/android/camera/compat/quirk/Quirk;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    const/4 v2, 0x0

    .line 10
    const/4 v3, 0x1

    .line 11
    if-nez v1, :cond_1

    .line 12
    .line 13
    const-class v1, Lcom/google/android/camera/compat/quirk/SurfaceViewNotCroppedByParentQuirk;

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Lcom/google/android/camera/compat/quirk/DeviceQuirks;->〇080(Ljava/lang/Class;)Lcom/google/android/camera/compat/quirk/Quirk;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    const/4 v0, 0x0

    .line 23
    goto :goto_1

    .line 24
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 25
    :goto_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 26
    .line 27
    const/16 v4, 0x18

    .line 28
    .line 29
    if-le v1, v4, :cond_2

    .line 30
    .line 31
    if-eqz v0, :cond_3

    .line 32
    .line 33
    :cond_2
    const/4 v2, 0x1

    .line 34
    :cond_3
    return v2
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/google/android/camera/compat/view/CameraCompatPreviewView;Landroid/view/View;IIIIIIII)V
    .locals 0

    .line 1
    invoke-static/range {p0 .. p9}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->OO0o〇〇〇〇0(Lcom/google/android/camera/compat/view/CameraCompatPreviewView;Landroid/view/View;IIIIIIII)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
.end method

.method public static final synthetic 〇o〇(Lcom/google/android/camera/compat/view/CameraCompatPreviewView;II)Lcom/google/android/camera/compat/view/PreviewViewImplementation;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇〇888(II)Lcom/google/android/camera/compat/view/PreviewViewImplementation;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private final 〇〇808〇()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->OO:Lcom/google/android/camera/size/CameraSize;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->oO80(Lcom/google/android/camera/size/CameraSize;)Landroid/graphics/Rect;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇08O〇00〇o:Lcom/google/android/camera/compat/view/SurfaceRequest;

    .line 8
    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    if-eqz v1, :cond_1

    .line 12
    .line 13
    iget-object v2, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->o0:Lcom/google/android/camera/CameraViewImpl;

    .line 14
    .line 15
    const/4 v3, 0x0

    .line 16
    if-eqz v2, :cond_0

    .line 17
    .line 18
    const/4 v4, 0x1

    .line 19
    const/4 v5, 0x0

    .line 20
    invoke-static {v2, v5, v4, v5}, Lcom/google/android/camera/ICamera$DefaultImpls;->o〇0(Lcom/google/android/camera/ICamera;Ljava/lang/Integer;ILjava/lang/Object;)I

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 v2, 0x0

    .line 26
    :goto_0
    invoke-static {v0, v2, v3}, Lcom/google/android/camera/compat/view/SurfaceRequest$TransformationInfo;->O8(Landroid/graphics/Rect;II)Lcom/google/android/camera/compat/view/SurfaceRequest$TransformationInfo;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-virtual {v1, v0}, Lcom/google/android/camera/compat/view/SurfaceRequest;->oo88o8O(Lcom/google/android/camera/compat/view/SurfaceRequest$TransformationInfo;)V

    .line 31
    .line 32
    .line 33
    :cond_1
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final 〇〇888(II)Lcom/google/android/camera/compat/view/PreviewViewImplementation;
    .locals 5

    .line 1
    sget-object v0, Lcom/google/android/camera/data/CameraApi;->〇080:Lcom/google/android/camera/data/CameraApi$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/google/android/camera/data/CameraApi$Companion;->Oo08()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const-string v1, "CameraX-CameraCompatPreview"

    .line 8
    .line 9
    if-ne p1, v0, :cond_0

    .line 10
    .line 11
    sget-object p1, Lcom/google/android/camera/data/PreviewMode;->O8:Lcom/google/android/camera/data/PreviewMode$Companion;

    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/google/android/camera/data/PreviewMode$Companion;->〇o00〇〇Oo()I

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    iput p1, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->o〇00O:I

    .line 18
    .line 19
    const-string p1, "createPreviewImpl, camerao use ImageReaderPreview"

    .line 20
    .line 21
    invoke-static {v1, p1}, Lcom/google/android/camera/log/CameraLog;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    new-instance p1, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation;

    .line 25
    .line 26
    iget-object p2, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇080OO8〇0:Lcom/google/android/camera/compat/view/PreviewTransformation;

    .line 27
    .line 28
    invoke-direct {p1, p0, p2}, Lcom/google/android/camera/preview/ImageReaderPreviewImplementation;-><init>(Landroid/view/ViewGroup;Lcom/google/android/camera/compat/view/PreviewTransformation;)V

    .line 29
    .line 30
    .line 31
    return-object p1

    .line 32
    :cond_0
    sget-object v0, Lcom/google/android/camera/data/PreviewMode;->O8:Lcom/google/android/camera/data/PreviewMode$Companion;

    .line 33
    .line 34
    invoke-virtual {v0}, Lcom/google/android/camera/data/PreviewMode$Companion;->〇o00〇〇Oo()I

    .line 35
    .line 36
    .line 37
    move-result v2

    .line 38
    const/4 v3, 0x0

    .line 39
    const/4 v4, 0x1

    .line 40
    if-ne p2, v2, :cond_2

    .line 41
    .line 42
    invoke-direct {p0}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇O〇()Z

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    if-eqz p1, :cond_1

    .line 47
    .line 48
    invoke-virtual {v0}, Lcom/google/android/camera/data/PreviewMode$Companion;->〇o〇()I

    .line 49
    .line 50
    .line 51
    move-result p1

    .line 52
    iput p1, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->o〇00O:I

    .line 53
    .line 54
    new-array p1, v4, [Ljava/lang/Object;

    .line 55
    .line 56
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 57
    .line 58
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 59
    .line 60
    .line 61
    move-result-object p2

    .line 62
    aput-object p2, p1, v3

    .line 63
    .line 64
    const-string p2, "createPreviewImpl, sdk version = %d, create TextureViewPreview"

    .line 65
    .line 66
    invoke-static {v1, p2, p1}, Lcom/google/android/camera/log/CameraLog;->oO80(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    .line 68
    .line 69
    new-instance p1, Lcom/google/android/camera/TextureViewImplementation;

    .line 70
    .line 71
    iget-object p2, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇080OO8〇0:Lcom/google/android/camera/compat/view/PreviewTransformation;

    .line 72
    .line 73
    invoke-direct {p1, p0, p2}, Lcom/google/android/camera/TextureViewImplementation;-><init>(Landroid/view/ViewGroup;Lcom/google/android/camera/compat/view/PreviewTransformation;)V

    .line 74
    .line 75
    .line 76
    goto :goto_0

    .line 77
    :cond_1
    new-array p1, v4, [Ljava/lang/Object;

    .line 78
    .line 79
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 80
    .line 81
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 82
    .line 83
    .line 84
    move-result-object p2

    .line 85
    aput-object p2, p1, v3

    .line 86
    .line 87
    const-string p2, "createPreviewImpl, sdk version = %d, create SurfaceViewPreview"

    .line 88
    .line 89
    invoke-static {v1, p2, p1}, Lcom/google/android/camera/log/CameraLog;->oO80(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 90
    .line 91
    .line 92
    new-instance p1, Lcom/google/android/camera/SurfaceViewImplementation;

    .line 93
    .line 94
    iget-object p2, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇080OO8〇0:Lcom/google/android/camera/compat/view/PreviewTransformation;

    .line 95
    .line 96
    invoke-direct {p1, p0, p2}, Lcom/google/android/camera/SurfaceViewImplementation;-><init>(Landroid/view/ViewGroup;Lcom/google/android/camera/compat/view/PreviewTransformation;)V

    .line 97
    .line 98
    .line 99
    goto :goto_0

    .line 100
    :cond_2
    new-array p2, v4, [Ljava/lang/Object;

    .line 101
    .line 102
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 103
    .line 104
    .line 105
    move-result-object p1

    .line 106
    aput-object p1, p2, v3

    .line 107
    .line 108
    const-string p1, "createPreviewImpl, CameraApi = %d, create TextureViewPreview"

    .line 109
    .line 110
    invoke-static {v1, p1, p2}, Lcom/google/android/camera/log/CameraLog;->oO80(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 111
    .line 112
    .line 113
    new-instance p1, Lcom/google/android/camera/TextureViewImplementation;

    .line 114
    .line 115
    iget-object p2, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇080OO8〇0:Lcom/google/android/camera/compat/view/PreviewTransformation;

    .line 116
    .line 117
    invoke-direct {p1, p0, p2}, Lcom/google/android/camera/TextureViewImplementation;-><init>(Landroid/view/ViewGroup;Lcom/google/android/camera/compat/view/PreviewTransformation;)V

    .line 118
    .line 119
    .line 120
    :goto_0
    return-object p1
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

.method private final 〇〇8O0〇8()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->getDisplayManager()Landroid/hardware/display/DisplayManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v1, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->oOo〇8o008:Lcom/google/android/camera/compat/view/CameraCompatPreviewView$DisplayRotationListener;

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->unregisterDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
.end method


# virtual methods
.method public final getCameraBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->O8o08O8O:Lcom/google/android/camera/compat/view/PreviewViewImplementation;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/compat/view/PreviewViewImplementation;->〇o00〇〇Oo()Landroid/graphics/Bitmap;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getDeferrableSurface()Lcom/google/android/camera/compat/view/DeferrableSurface;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇OOo8〇0:Lcom/google/android/camera/compat/view/DeferrableSurface;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getMImplementation()Lcom/google/android/camera/compat/view/PreviewViewImplementation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->O8o08O8O:Lcom/google/android/camera/compat/view/PreviewViewImplementation;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getMImplementationMode()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->o〇00O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getMPreviewTransform()Lcom/google/android/camera/compat/view/PreviewTransformation;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇080OO8〇0:Lcom/google/android/camera/compat/view/PreviewTransformation;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getMUseDisplayRotation()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇0O:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getOutputTransform()Lcom/google/android/camera/compat/view/OutputTransform;
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    :try_start_0
    iget-object v1, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇080OO8〇0:Lcom/google/android/camera/compat/view/PreviewTransformation;

    .line 3
    .line 4
    new-instance v2, Lcom/google/android/camera/size/CameraSize;

    .line 5
    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 7
    .line 8
    .line 9
    move-result v3

    .line 10
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 11
    .line 12
    .line 13
    move-result v4

    .line 14
    invoke-direct {v2, v3, v4}, Lcom/google/android/camera/size/CameraSize;-><init>(II)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0}, Landroid/view/View;->getLayoutDirection()I

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    invoke-virtual {v1, v2, v3}, Lcom/google/android/camera/compat/view/PreviewTransformation;->o〇0(Lcom/google/android/camera/size/CameraSize;I)Landroid/graphics/Matrix;

    .line 22
    .line 23
    .line 24
    move-result-object v1
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 25
    goto :goto_0

    .line 26
    :catch_0
    nop

    .line 27
    move-object v1, v0

    .line 28
    :goto_0
    iget-object v2, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇080OO8〇0:Lcom/google/android/camera/compat/view/PreviewTransformation;

    .line 29
    .line 30
    invoke-virtual {v2}, Lcom/google/android/camera/compat/view/PreviewTransformation;->Oo08()Landroid/graphics/Rect;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    const-string v3, "CameraX-CameraCompatPreview"

    .line 35
    .line 36
    if-eqz v1, :cond_2

    .line 37
    .line 38
    if-nez v2, :cond_0

    .line 39
    .line 40
    goto :goto_2

    .line 41
    :cond_0
    invoke-static {v2}, Lcom/google/android/camera/compat/view/TransformUtils;->〇080(Landroid/graphics/Rect;)Landroid/graphics/Matrix;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    .line 46
    .line 47
    .line 48
    iget-object v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->O8o08O8O:Lcom/google/android/camera/compat/view/PreviewViewImplementation;

    .line 49
    .line 50
    instance-of v0, v0, Lcom/google/android/camera/TextureViewImplementation;

    .line 51
    .line 52
    if-eqz v0, :cond_1

    .line 53
    .line 54
    invoke-virtual {p0}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 59
    .line 60
    .line 61
    goto :goto_1

    .line 62
    :cond_1
    const-string v0, "PreviewView needs to be in COMPATIBLE mode for the transform to work correctly."

    .line 63
    .line 64
    invoke-static {v3, v0}, Lcom/google/android/camera/log/CameraLog;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    :goto_1
    new-instance v0, Lcom/google/android/camera/compat/view/OutputTransform;

    .line 68
    .line 69
    new-instance v3, Lcom/google/android/camera/size/CameraSize;

    .line 70
    .line 71
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    .line 72
    .line 73
    .line 74
    move-result v4

    .line 75
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    .line 76
    .line 77
    .line 78
    move-result v2

    .line 79
    invoke-direct {v3, v4, v2}, Lcom/google/android/camera/size/CameraSize;-><init>(II)V

    .line 80
    .line 81
    .line 82
    invoke-direct {v0, v1, v3}, Lcom/google/android/camera/compat/view/OutputTransform;-><init>(Landroid/graphics/Matrix;Lcom/google/android/camera/size/CameraSize;)V

    .line 83
    .line 84
    .line 85
    return-object v0

    .line 86
    :cond_2
    :goto_2
    const-string v1, "Transform info is not ready"

    .line 87
    .line 88
    invoke-static {v3, v1}, Lcom/google/android/camera/log/CameraLog;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    return-object v0
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public final getPreview()Landroid/view/View;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->O8o08O8O:Lcom/google/android/camera/compat/view/PreviewViewImplementation;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/compat/view/PreviewViewImplementation;->Oo08()Landroid/view/View;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getPreviewMode()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->o〇00O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇0〇O0088o()V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇O00()V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->oOo0:Landroid/view/View$OnLayoutChangeListener;

    .line 11
    .line 12
    invoke-virtual {p0, v0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->O8o08O8O:Lcom/google/android/camera/compat/view/PreviewViewImplementation;

    .line 16
    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/google/android/camera/compat/view/PreviewViewImplementation;->OO0o〇〇〇〇0()V

    .line 20
    .line 21
    .line 22
    :cond_0
    const/4 v0, 0x1

    .line 23
    invoke-direct {p0, v0}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->Oo08(Z)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->oOo0:Landroid/view/View$OnLayoutChangeListener;

    .line 5
    .line 6
    invoke-virtual {p0, v0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->O8o08O8O:Lcom/google/android/camera/compat/view/PreviewViewImplementation;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/google/android/camera/compat/view/PreviewViewImplementation;->〇8o8o〇()V

    .line 14
    .line 15
    .line 16
    :cond_0
    iget-object v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->o0:Lcom/google/android/camera/CameraViewImpl;

    .line 17
    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/google/android/camera/CameraViewImpl;->〇8o8o〇()V

    .line 23
    .line 24
    .line 25
    :cond_1
    invoke-direct {p0}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇〇8O0〇8()V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public final o〇0(Lcom/google/android/camera/CameraViewImpl;Lcom/google/android/camera/size/CameraSize;ZJ)V
    .locals 2
    .param p1    # Lcom/google/android/camera/CameraViewImpl;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/camera/size/CameraSize;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "cameraImpl"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "previewSize"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    new-instance v0, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string/jumbo v1, "start createPipeline "

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    const-string v1, "CameraX-CameraCompatPreview"

    .line 30
    .line 31
    invoke-static {v1, v0}, Lcom/google/android/camera/log/CameraLog;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    if-nez p3, :cond_0

    .line 35
    .line 36
    iget-object p3, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇OOo8〇0:Lcom/google/android/camera/compat/view/DeferrableSurface;

    .line 37
    .line 38
    if-eqz p3, :cond_0

    .line 39
    .line 40
    iget-object p3, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇08O〇00〇o:Lcom/google/android/camera/compat/view/SurfaceRequest;

    .line 41
    .line 42
    if-eqz p3, :cond_0

    .line 43
    .line 44
    iget-object p3, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->OO:Lcom/google/android/camera/size/CameraSize;

    .line 45
    .line 46
    invoke-static {p3, p2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 47
    .line 48
    .line 49
    move-result p3

    .line 50
    if-eqz p3, :cond_0

    .line 51
    .line 52
    const-string p1, "same preview size no need recreate surface!"

    .line 53
    .line 54
    invoke-static {v1, p1}, Lcom/google/android/camera/log/CameraLog;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    return-void

    .line 58
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇8o8o〇()V

    .line 59
    .line 60
    .line 61
    iput-object p2, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->OO:Lcom/google/android/camera/size/CameraSize;

    .line 62
    .line 63
    iput-object p1, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->o0:Lcom/google/android/camera/CameraViewImpl;

    .line 64
    .line 65
    new-instance p3, Lcom/google/android/camera/compat/view/SurfaceRequest;

    .line 66
    .line 67
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 68
    .line 69
    .line 70
    const/4 v0, 0x0

    .line 71
    invoke-direct {p3, p2, p1, v0}, Lcom/google/android/camera/compat/view/SurfaceRequest;-><init>(Lcom/google/android/camera/size/CameraSize;Lcom/google/android/camera/CameraViewImpl;Z)V

    .line 72
    .line 73
    .line 74
    iput-object p3, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇08O〇00〇o:Lcom/google/android/camera/compat/view/SurfaceRequest;

    .line 75
    .line 76
    invoke-direct {p0, p4, p5}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->OO0o〇〇(J)V

    .line 77
    .line 78
    .line 79
    invoke-direct {p0}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇〇808〇()V

    .line 80
    .line 81
    .line 82
    iget-object p1, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇08O〇00〇o:Lcom/google/android/camera/compat/view/SurfaceRequest;

    .line 83
    .line 84
    if-eqz p1, :cond_1

    .line 85
    .line 86
    invoke-virtual {p1}, Lcom/google/android/camera/compat/view/SurfaceRequest;->〇8o8o〇()Lcom/google/android/camera/compat/view/DeferrableSurface;

    .line 87
    .line 88
    .line 89
    move-result-object p1

    .line 90
    goto :goto_0

    .line 91
    :cond_1
    const/4 p1, 0x0

    .line 92
    :goto_0
    iput-object p1, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇OOo8〇0:Lcom/google/android/camera/compat/view/DeferrableSurface;

    .line 93
    .line 94
    return-void
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
.end method

.method public final setMImplementation(Lcom/google/android/camera/compat/view/PreviewViewImplementation;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->O8o08O8O:Lcom/google/android/camera/compat/view/PreviewViewImplementation;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final setMImplementationMode(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->o〇00O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final setMUseDisplayRotation(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇0O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final setPreviewMode(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->o〇00O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final setSurfaceCompat(Landroid/hardware/Camera;)V
    .locals 4

    .line 1
    if-eqz p1, :cond_4

    .line 2
    .line 3
    iget v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->o〇00O:I

    .line 4
    .line 5
    sget-object v1, Lcom/google/android/camera/data/PreviewMode;->O8:Lcom/google/android/camera/data/PreviewMode$Companion;

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/google/android/camera/data/PreviewMode$Companion;->〇o〇()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    const/4 v2, 0x0

    .line 12
    if-ne v0, v1, :cond_2

    .line 13
    .line 14
    invoke-virtual {p0}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->getPreview()Landroid/view/View;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    check-cast v0, Landroid/view/TextureView;

    .line 19
    .line 20
    if-eqz v0, :cond_0

    .line 21
    .line 22
    invoke-virtual {v0}, Landroid/view/TextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    :cond_0
    if-nez v2, :cond_1

    .line 27
    .line 28
    const/4 v0, 0x1

    .line 29
    goto :goto_0

    .line 30
    :cond_1
    const/4 v0, 0x0

    .line 31
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    .line 32
    .line 33
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .line 35
    .line 36
    const-string/jumbo v3, "surfaceTexture == null "

    .line 37
    .line 38
    .line 39
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    const-string v1, "CameraX-CameraCompatPreview"

    .line 50
    .line 51
    invoke-static {v1, v0}, Lcom/google/android/camera/log/CameraLog;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {p1, v2}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V

    .line 55
    .line 56
    .line 57
    goto :goto_1

    .line 58
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->getPreview()Landroid/view/View;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    check-cast v0, Landroid/view/SurfaceView;

    .line 63
    .line 64
    if-eqz v0, :cond_3

    .line 65
    .line 66
    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    .line 67
    .line 68
    .line 69
    move-result-object v2

    .line 70
    :cond_3
    invoke-virtual {p1, v2}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    .line 71
    .line 72
    .line 73
    :cond_4
    :goto_1
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method public final 〇0〇O0088o()V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇0O:Z

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getDisplay()Landroid/view/Display;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    iget-object v1, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->o0:Lcom/google/android/camera/CameraViewImpl;

    .line 12
    .line 13
    if-eqz v1, :cond_1

    .line 14
    .line 15
    iget-object v2, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇080OO8〇0:Lcom/google/android/camera/compat/view/PreviewTransformation;

    .line 16
    .line 17
    if-eqz v1, :cond_0

    .line 18
    .line 19
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    .line 20
    .line 21
    .line 22
    move-result v3

    .line 23
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 24
    .line 25
    .line 26
    move-result-object v3

    .line 27
    invoke-interface {v1, v3}, Lcom/google/android/camera/ICamera;->〇080(Ljava/lang/Integer;)I

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    goto :goto_0

    .line 32
    :cond_0
    const/4 v1, 0x0

    .line 33
    :goto_0
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    invoke-virtual {v2, v1, v0}, Lcom/google/android/camera/compat/view/PreviewTransformation;->〇8o8o〇(II)V

    .line 38
    .line 39
    .line 40
    :cond_1
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public final 〇80〇808〇O()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇08O〇00〇o:Lcom/google/android/camera/compat/view/SurfaceRequest;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->OO:Lcom/google/android/camera/size/CameraSize;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->O8o08O8O:Lcom/google/android/camera/compat/view/PreviewViewImplementation;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public final 〇8o8o〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇OOo8〇0:Lcom/google/android/camera/compat/view/DeferrableSurface;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/google/android/camera/compat/view/DeferrableSurface;->〇o〇()V

    .line 7
    .line 8
    .line 9
    iput-object v1, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇OOo8〇0:Lcom/google/android/camera/compat/view/DeferrableSurface;

    .line 10
    .line 11
    :cond_0
    iput-object v1, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->〇08O〇00〇o:Lcom/google/android/camera/compat/view/SurfaceRequest;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
.end method

.method public final 〇O8o08O()V
    .locals 1
    .annotation build Landroidx/annotation/MainThread;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/view/CameraCompatPreviewView;->O8o08O8O:Lcom/google/android/camera/compat/view/PreviewViewImplementation;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/camera/compat/view/PreviewViewImplementation;->Oooo8o0〇()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

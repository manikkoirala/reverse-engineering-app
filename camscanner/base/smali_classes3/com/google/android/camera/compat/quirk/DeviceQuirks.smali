.class public final Lcom/google/android/camera/compat/quirk/DeviceQuirks;
.super Ljava/lang/Object;
.source "DeviceQuirks.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080:Lcom/google/android/camera/compat/quirk/DeviceQuirks;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static 〇o00〇〇Oo:Lcom/google/android/camera/compat/quirk/Quirks;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/google/android/camera/compat/quirk/DeviceQuirks;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/google/android/camera/compat/quirk/DeviceQuirks;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/google/android/camera/compat/quirk/DeviceQuirks;->〇080:Lcom/google/android/camera/compat/quirk/DeviceQuirks;

    .line 7
    .line 8
    :try_start_0
    new-instance v0, Lcom/google/android/camera/compat/quirk/Quirks;

    .line 9
    .line 10
    sget-object v1, Lcom/google/android/camera/compat/quirk/DeviceQuirksLoader;->〇080:Lcom/google/android/camera/compat/quirk/DeviceQuirksLoader$Companion;

    .line 11
    .line 12
    invoke-virtual {v1}, Lcom/google/android/camera/compat/quirk/DeviceQuirksLoader$Companion;->〇080()Ljava/util/List;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-direct {v0, v1}, Lcom/google/android/camera/compat/quirk/Quirks;-><init>(Ljava/util/List;)V

    .line 17
    .line 18
    .line 19
    sput-object v0, Lcom/google/android/camera/compat/quirk/DeviceQuirks;->〇o00〇〇Oo:Lcom/google/android/camera/compat/quirk/Quirks;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :catch_0
    move-exception v0

    .line 23
    const-string v1, "CameraX-DeviceQuirks"

    .line 24
    .line 25
    invoke-static {v1, v0}, Lcom/google/android/camera/log/CameraLog;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 26
    .line 27
    .line 28
    :goto_0
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method


# virtual methods
.method public final 〇080(Ljava/lang/Class;)Lcom/google/android/camera/compat/quirk/Quirk;
    .locals 1
    .param p1    # Ljava/lang/Class;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/camera/compat/quirk/Quirk;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    .line 1
    const-string v0, "quirkClass"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/google/android/camera/compat/quirk/DeviceQuirks;->〇o00〇〇Oo:Lcom/google/android/camera/compat/quirk/Quirks;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    invoke-virtual {v0, p1}, Lcom/google/android/camera/compat/quirk/Quirks;->〇o00〇〇Oo(Ljava/lang/Class;)Lcom/google/android/camera/compat/quirk/Quirk;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 p1, 0x0

    .line 16
    :goto_0
    return-object p1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

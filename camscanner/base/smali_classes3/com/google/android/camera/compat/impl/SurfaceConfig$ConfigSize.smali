.class public final enum Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;
.super Ljava/lang/Enum;
.source "SurfaceConfig.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/camera/compat/impl/SurfaceConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ConfigSize"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

.field public static final enum ANALYSIS:Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

.field public static final enum MAXIMUM:Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

.field public static final enum NOT_SUPPORT:Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

.field public static final enum PREVIEW:Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

.field public static final enum RECORD:Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;


# instance fields
.field private final id:I


# direct methods
.method private static final synthetic $values()[Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;
    .locals 3

    .line 1
    const/4 v0, 0x5

    .line 2
    new-array v0, v0, [Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    sget-object v2, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;->ANALYSIS:Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 6
    .line 7
    aput-object v2, v0, v1

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    sget-object v2, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;->PREVIEW:Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 11
    .line 12
    aput-object v2, v0, v1

    .line 13
    .line 14
    const/4 v1, 0x2

    .line 15
    sget-object v2, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;->RECORD:Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 16
    .line 17
    aput-object v2, v0, v1

    .line 18
    .line 19
    const/4 v1, 0x3

    .line 20
    sget-object v2, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;->MAXIMUM:Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 21
    .line 22
    aput-object v2, v0, v1

    .line 23
    .line 24
    const/4 v1, 0x4

    .line 25
    sget-object v2, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;->NOT_SUPPORT:Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 26
    .line 27
    aput-object v2, v0, v1

    .line 28
    .line 29
    return-object v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 2
    .line 3
    const-string v1, "ANALYSIS"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;-><init>(Ljava/lang/String;II)V

    .line 7
    .line 8
    .line 9
    sput-object v0, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;->ANALYSIS:Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 10
    .line 11
    new-instance v0, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 12
    .line 13
    const-string v1, "PREVIEW"

    .line 14
    .line 15
    const/4 v2, 0x1

    .line 16
    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;-><init>(Ljava/lang/String;II)V

    .line 17
    .line 18
    .line 19
    sput-object v0, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;->PREVIEW:Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 20
    .line 21
    new-instance v0, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 22
    .line 23
    const-string v1, "RECORD"

    .line 24
    .line 25
    const/4 v2, 0x2

    .line 26
    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;-><init>(Ljava/lang/String;II)V

    .line 27
    .line 28
    .line 29
    sput-object v0, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;->RECORD:Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 30
    .line 31
    new-instance v0, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 32
    .line 33
    const-string v1, "MAXIMUM"

    .line 34
    .line 35
    const/4 v2, 0x3

    .line 36
    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;-><init>(Ljava/lang/String;II)V

    .line 37
    .line 38
    .line 39
    sput-object v0, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;->MAXIMUM:Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 40
    .line 41
    new-instance v0, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 42
    .line 43
    const-string v1, "NOT_SUPPORT"

    .line 44
    .line 45
    const/4 v2, 0x4

    .line 46
    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;-><init>(Ljava/lang/String;II)V

    .line 47
    .line 48
    .line 49
    sput-object v0, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;->NOT_SUPPORT:Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 50
    .line 51
    invoke-static {}, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;->$values()[Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    sput-object v0, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;->$VALUES:[Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 56
    .line 57
    return-void
    .line 58
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput p3, p0, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;->id:I

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;
    .locals 1

    .line 1
    const-class v0, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static values()[Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;->$VALUES:[Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 2
    .line 3
    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method


# virtual methods
.method public final getId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/google/android/camera/compat/impl/SurfaceConfig$ConfigSize;->id:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

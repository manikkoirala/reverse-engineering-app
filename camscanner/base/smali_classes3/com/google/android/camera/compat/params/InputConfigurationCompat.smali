.class public final Lcom/google/android/camera/compat/params/InputConfigurationCompat;
.super Ljava/lang/Object;
.source "InputConfigurationCompat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatApi31Impl;,
        Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatImpl;,
        Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatApi23Impl;
    }
.end annotation


# instance fields
.field private final 〇080:Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatImpl;


# direct methods
.method private constructor <init>(Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatImpl;)V
    .locals 0
    .param p1    # Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatImpl;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/google/android/camera/compat/params/InputConfigurationCompat;->〇080:Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatImpl;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static 〇o00〇〇Oo(Ljava/lang/Object;)Lcom/google/android/camera/compat/params/InputConfigurationCompat;
    .locals 3
    .param p0    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p0, :cond_0

    .line 3
    .line 4
    return-object v0

    .line 5
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 6
    .line 7
    const/16 v2, 0x17

    .line 8
    .line 9
    if-ge v1, v2, :cond_1

    .line 10
    .line 11
    return-object v0

    .line 12
    :cond_1
    const/16 v0, 0x1f

    .line 13
    .line 14
    if-lt v1, v0, :cond_2

    .line 15
    .line 16
    new-instance v0, Lcom/google/android/camera/compat/params/InputConfigurationCompat;

    .line 17
    .line 18
    new-instance v1, Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatApi31Impl;

    .line 19
    .line 20
    invoke-direct {v1, p0}, Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatApi31Impl;-><init>(Ljava/lang/Object;)V

    .line 21
    .line 22
    .line 23
    invoke-direct {v0, v1}, Lcom/google/android/camera/compat/params/InputConfigurationCompat;-><init>(Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatImpl;)V

    .line 24
    .line 25
    .line 26
    return-object v0

    .line 27
    :cond_2
    new-instance v0, Lcom/google/android/camera/compat/params/InputConfigurationCompat;

    .line 28
    .line 29
    new-instance v1, Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatApi23Impl;

    .line 30
    .line 31
    invoke-direct {v1, p0}, Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatApi23Impl;-><init>(Ljava/lang/Object;)V

    .line 32
    .line 33
    .line 34
    invoke-direct {v0, v1}, Lcom/google/android/camera/compat/params/InputConfigurationCompat;-><init>(Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatImpl;)V

    .line 35
    .line 36
    .line 37
    return-object v0
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/google/android/camera/compat/params/InputConfigurationCompat;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    return p1

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/google/android/camera/compat/params/InputConfigurationCompat;->〇080:Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatImpl;

    .line 8
    .line 9
    check-cast p1, Lcom/google/android/camera/compat/params/InputConfigurationCompat;

    .line 10
    .line 11
    iget-object p1, p1, Lcom/google/android/camera/compat/params/InputConfigurationCompat;->〇080:Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatImpl;

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    return p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public hashCode()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/params/InputConfigurationCompat;->〇080:Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatImpl;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public toString()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/params/InputConfigurationCompat;->〇080:Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatImpl;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇080()Ljava/lang/Object;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/compat/params/InputConfigurationCompat;->〇080:Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatImpl;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/google/android/camera/compat/params/InputConfigurationCompat$InputConfigurationCompatImpl;->getInputConfiguration()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

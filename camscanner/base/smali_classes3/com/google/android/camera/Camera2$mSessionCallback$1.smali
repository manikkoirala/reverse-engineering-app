.class public final Lcom/google/android/camera/Camera2$mSessionCallback$1;
.super Landroid/hardware/camera2/CameraCaptureSession$StateCallback;
.source "Camera2.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/camera/Camera2;-><init>(Landroid/content/Context;Lcom/google/android/camera/CameraViewImpl$Callback;Lcom/google/android/camera/PreviewImpl;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic 〇080:Lcom/google/android/camera/Camera2;


# direct methods
.method constructor <init>(Lcom/google/android/camera/Camera2;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/camera/Camera2$mSessionCallback$1;->〇080:Lcom/google/android/camera/Camera2;

    .line 2
    .line 3
    invoke-direct {p0}, Landroid/hardware/camera2/CameraCaptureSession$StateCallback;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public onClosed(Landroid/hardware/camera2/CameraCaptureSession;)V
    .locals 2
    .param p1    # Landroid/hardware/camera2/CameraCaptureSession;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "session"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "CameraX-Camera2"

    .line 7
    .line 8
    const-string v1, "mSessionCallback, onClosed"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/google/android/camera/log/CameraLog;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/google/android/camera/Camera2$mSessionCallback$1;->〇080:Lcom/google/android/camera/Camera2;

    .line 14
    .line 15
    invoke-static {v0}, Lcom/google/android/camera/Camera2;->〇0OO8(Lcom/google/android/camera/Camera2;)Landroid/os/ConditionVariable;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/google/android/camera/Camera2$mSessionCallback$1;->〇080:Lcom/google/android/camera/Camera2;

    .line 23
    .line 24
    invoke-static {v0}, Lcom/google/android/camera/Camera2;->oOo(Lcom/google/android/camera/Camera2;)Lcom/google/android/camera/compat/CameraCaptureSessionCompat;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    if-eqz v0, :cond_1

    .line 29
    .line 30
    iget-object v0, p0, Lcom/google/android/camera/Camera2$mSessionCallback$1;->〇080:Lcom/google/android/camera/Camera2;

    .line 31
    .line 32
    invoke-static {v0}, Lcom/google/android/camera/Camera2;->oOo(Lcom/google/android/camera/Camera2;)Lcom/google/android/camera/compat/CameraCaptureSessionCompat;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    const/4 v1, 0x0

    .line 37
    if-eqz v0, :cond_0

    .line 38
    .line 39
    invoke-virtual {v0}, Lcom/google/android/camera/compat/CameraCaptureSessionCompat;->O8()Landroid/hardware/camera2/CameraCaptureSession;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    goto :goto_0

    .line 44
    :cond_0
    move-object v0, v1

    .line 45
    :goto_0
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 46
    .line 47
    .line 48
    move-result p1

    .line 49
    if-eqz p1, :cond_1

    .line 50
    .line 51
    iget-object p1, p0, Lcom/google/android/camera/Camera2$mSessionCallback$1;->〇080:Lcom/google/android/camera/Camera2;

    .line 52
    .line 53
    invoke-static {p1, v1}, Lcom/google/android/camera/Camera2;->〇o〇8(Lcom/google/android/camera/Camera2;Lcom/google/android/camera/compat/CameraCaptureSessionCompat;)V

    .line 54
    .line 55
    .line 56
    :cond_1
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public onConfigureFailed(Landroid/hardware/camera2/CameraCaptureSession;)V
    .locals 1
    .param p1    # Landroid/hardware/camera2/CameraCaptureSession;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "session"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "CameraX-Camera2"

    .line 7
    .line 8
    const-string v0, "mSessionCallback, onConfigureFailed, failed to configure capture session"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/google/android/camera/log/CameraLog;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    iget-object p1, p0, Lcom/google/android/camera/Camera2$mSessionCallback$1;->〇080:Lcom/google/android/camera/Camera2;

    .line 14
    .line 15
    invoke-static {p1}, Lcom/google/android/camera/Camera2;->〇0OO8(Lcom/google/android/camera/Camera2;)Landroid/os/ConditionVariable;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    invoke-virtual {p1}, Landroid/os/ConditionVariable;->open()V

    .line 20
    .line 21
    .line 22
    iget-object p1, p0, Lcom/google/android/camera/Camera2$mSessionCallback$1;->〇080:Lcom/google/android/camera/Camera2;

    .line 23
    .line 24
    invoke-static {p1}, Lcom/google/android/camera/Camera2;->oOo(Lcom/google/android/camera/Camera2;)Lcom/google/android/camera/compat/CameraCaptureSessionCompat;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    if-eqz p1, :cond_0

    .line 29
    .line 30
    iget-object p1, p0, Lcom/google/android/camera/Camera2$mSessionCallback$1;->〇080:Lcom/google/android/camera/Camera2;

    .line 31
    .line 32
    const/4 v0, 0x0

    .line 33
    invoke-static {p1, v0}, Lcom/google/android/camera/Camera2;->〇o〇8(Lcom/google/android/camera/Camera2;Lcom/google/android/camera/compat/CameraCaptureSessionCompat;)V

    .line 34
    .line 35
    .line 36
    :cond_0
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public onConfigured(Landroid/hardware/camera2/CameraCaptureSession;)V
    .locals 6
    .param p1    # Landroid/hardware/camera2/CameraCaptureSession;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "session"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/google/android/camera/Camera2$mSessionCallback$1;->〇080:Lcom/google/android/camera/Camera2;

    .line 7
    .line 8
    invoke-static {v0}, Lcom/google/android/camera/Camera2;->o〇8〇(Lcom/google/android/camera/Camera2;)Lcom/google/android/camera/compat/CameraDeviceCompat;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const-string v1, "CameraX-Camera2"

    .line 13
    .line 14
    if-nez v0, :cond_0

    .line 15
    .line 16
    const-string p1, "mSessionCallback, onConfigured, Camera is null"

    .line 17
    .line 18
    invoke-static {v1, p1}, Lcom/google/android/camera/log/CameraLog;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    return-void

    .line 22
    :cond_0
    iget-object v0, p0, Lcom/google/android/camera/Camera2$mSessionCallback$1;->〇080:Lcom/google/android/camera/Camera2;

    .line 23
    .line 24
    invoke-static {v0}, Lcom/google/android/camera/Camera2;->〇0OO8(Lcom/google/android/camera/Camera2;)Landroid/os/ConditionVariable;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 29
    .line 30
    .line 31
    const-string v0, "mSessionCallback, onConfigured, CameraCaptureSession created"

    .line 32
    .line 33
    invoke-static {v1, v0}, Lcom/google/android/camera/log/CameraLog;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    iget-object v0, p0, Lcom/google/android/camera/Camera2$mSessionCallback$1;->〇080:Lcom/google/android/camera/Camera2;

    .line 37
    .line 38
    invoke-static {p1}, Lcom/google/android/camera/compat/CameraCaptureSessionCompat;->Oo08(Landroid/hardware/camera2/CameraCaptureSession;)Lcom/google/android/camera/compat/CameraCaptureSessionCompat;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    invoke-static {v0, p1}, Lcom/google/android/camera/Camera2;->〇o〇8(Lcom/google/android/camera/Camera2;Lcom/google/android/camera/compat/CameraCaptureSessionCompat;)V

    .line 43
    .line 44
    .line 45
    iget-object p1, p0, Lcom/google/android/camera/Camera2$mSessionCallback$1;->〇080:Lcom/google/android/camera/Camera2;

    .line 46
    .line 47
    invoke-virtual {p1}, Lcom/google/android/camera/CameraViewImpl;->o800o8O()Lkotlinx/coroutines/CoroutineScope;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-static {}, Lcom/google/android/camera/lifecycle/CameraDispatchers;->〇080()Lkotlinx/coroutines/ExecutorCoroutineDispatcher;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    const/4 v2, 0x0

    .line 56
    new-instance v3, Lcom/google/android/camera/Camera2$mSessionCallback$1$onConfigured$1;

    .line 57
    .line 58
    iget-object p1, p0, Lcom/google/android/camera/Camera2$mSessionCallback$1;->〇080:Lcom/google/android/camera/Camera2;

    .line 59
    .line 60
    const/4 v4, 0x0

    .line 61
    invoke-direct {v3, p1, v4}, Lcom/google/android/camera/Camera2$mSessionCallback$1$onConfigured$1;-><init>(Lcom/google/android/camera/Camera2;Lkotlin/coroutines/Continuation;)V

    .line 62
    .line 63
    .line 64
    const/4 v4, 0x2

    .line 65
    const/4 v5, 0x0

    .line 66
    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 67
    .line 68
    .line 69
    return-void
    .line 70
    .line 71
.end method

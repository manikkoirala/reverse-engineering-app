.class public final Lcom/google/android/camera/util/CameraSizeUtils;
.super Ljava/lang/Object;
.source "CameraSizeUtils.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/camera/util/CameraSizeUtils$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final O8:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/camera/size/CameraSize;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final 〇080:Lcom/google/android/camera/util/CameraSizeUtils$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇o00〇〇Oo:Lcom/google/android/camera/size/CameraSize;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇o〇:Lcom/google/android/camera/size/CameraSize;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lcom/google/android/camera/util/CameraSizeUtils$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/google/android/camera/util/CameraSizeUtils$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/google/android/camera/util/CameraSizeUtils;->〇080:Lcom/google/android/camera/util/CameraSizeUtils$Companion;

    .line 8
    .line 9
    new-instance v0, Lcom/google/android/camera/size/CameraSize;

    .line 10
    .line 11
    const/16 v1, 0x780

    .line 12
    .line 13
    const/16 v2, 0x438

    .line 14
    .line 15
    invoke-direct {v0, v1, v2}, Lcom/google/android/camera/size/CameraSize;-><init>(II)V

    .line 16
    .line 17
    .line 18
    sput-object v0, Lcom/google/android/camera/util/CameraSizeUtils;->〇o00〇〇Oo:Lcom/google/android/camera/size/CameraSize;

    .line 19
    .line 20
    new-instance v0, Lcom/google/android/camera/size/CameraSize;

    .line 21
    .line 22
    const/16 v1, 0x5a0

    .line 23
    .line 24
    invoke-direct {v0, v1, v2}, Lcom/google/android/camera/size/CameraSize;-><init>(II)V

    .line 25
    .line 26
    .line 27
    sput-object v0, Lcom/google/android/camera/util/CameraSizeUtils;->〇o〇:Lcom/google/android/camera/size/CameraSize;

    .line 28
    .line 29
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    .line 30
    .line 31
    const/4 v1, 0x2

    .line 32
    invoke-direct {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(I)V

    .line 33
    .line 34
    .line 35
    sput-object v0, Lcom/google/android/camera/util/CameraSizeUtils;->O8:Ljava/util/concurrent/ConcurrentHashMap;

    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static final synthetic 〇080()Lcom/google/android/camera/size/CameraSize;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/camera/util/CameraSizeUtils;->〇o〇:Lcom/google/android/camera/size/CameraSize;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public static final synthetic 〇o00〇〇Oo()Lcom/google/android/camera/size/CameraSize;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/camera/util/CameraSizeUtils;->〇o00〇〇Oo:Lcom/google/android/camera/size/CameraSize;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public static final synthetic 〇o〇()Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/camera/util/CameraSizeUtils;->O8:Ljava/util/concurrent/ConcurrentHashMap;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

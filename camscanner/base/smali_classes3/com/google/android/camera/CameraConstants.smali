.class public final Lcom/google/android/camera/CameraConstants;
.super Ljava/lang/Object;
.source "CameraConstants.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/camera/CameraConstants$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final O8:Lcom/google/android/camera/size/AspectRatio;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final Oo08:Lcom/google/android/camera/size/AspectRatio;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o〇0:Lcom/google/android/camera/size/AspectRatio;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final 〇080:Lcom/google/android/camera/CameraConstants$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇o00〇〇Oo:Lcom/google/android/camera/size/AspectRatio;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇o〇:Lcom/google/android/camera/size/AspectRatio;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇〇888:Lcom/google/android/camera/size/AspectRatio;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    new-instance v0, Lcom/google/android/camera/CameraConstants$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/google/android/camera/CameraConstants$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/google/android/camera/CameraConstants;->〇080:Lcom/google/android/camera/CameraConstants$Companion;

    .line 8
    .line 9
    const/16 v0, 0x10

    .line 10
    .line 11
    const/16 v1, 0x9

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/google/android/camera/size/AspectRatio;->o〇0(II)Lcom/google/android/camera/size/AspectRatio;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    const-string v2, "of(16, 9)"

    .line 18
    .line 19
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    sput-object v0, Lcom/google/android/camera/CameraConstants;->〇o00〇〇Oo:Lcom/google/android/camera/size/AspectRatio;

    .line 23
    .line 24
    const/4 v0, 0x4

    .line 25
    const/4 v2, 0x3

    .line 26
    invoke-static {v0, v2}, Lcom/google/android/camera/size/AspectRatio;->o〇0(II)Lcom/google/android/camera/size/AspectRatio;

    .line 27
    .line 28
    .line 29
    move-result-object v3

    .line 30
    const-string v4, "of(4, 3)"

    .line 31
    .line 32
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    sput-object v3, Lcom/google/android/camera/CameraConstants;->〇o〇:Lcom/google/android/camera/size/AspectRatio;

    .line 36
    .line 37
    const/4 v3, 0x2

    .line 38
    const/4 v4, 0x1

    .line 39
    invoke-static {v3, v4}, Lcom/google/android/camera/size/AspectRatio;->o〇0(II)Lcom/google/android/camera/size/AspectRatio;

    .line 40
    .line 41
    .line 42
    move-result-object v3

    .line 43
    const-string v5, "of(2, 1)"

    .line 44
    .line 45
    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    sput-object v3, Lcom/google/android/camera/CameraConstants;->O8:Lcom/google/android/camera/size/AspectRatio;

    .line 49
    .line 50
    const/16 v3, 0x14

    .line 51
    .line 52
    invoke-static {v3, v1}, Lcom/google/android/camera/size/AspectRatio;->o〇0(II)Lcom/google/android/camera/size/AspectRatio;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    const-string v3, "of(20, 9)"

    .line 57
    .line 58
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    sput-object v1, Lcom/google/android/camera/CameraConstants;->Oo08:Lcom/google/android/camera/size/AspectRatio;

    .line 62
    .line 63
    invoke-static {v2, v0}, Lcom/google/android/camera/size/AspectRatio;->o〇0(II)Lcom/google/android/camera/size/AspectRatio;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    const-string v1, "of(3, 4)"

    .line 68
    .line 69
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    sput-object v0, Lcom/google/android/camera/CameraConstants;->o〇0:Lcom/google/android/camera/size/AspectRatio;

    .line 73
    .line 74
    invoke-static {v4, v4}, Lcom/google/android/camera/size/AspectRatio;->o〇0(II)Lcom/google/android/camera/size/AspectRatio;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    const-string v1, "of(1, 1)"

    .line 79
    .line 80
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    sput-object v0, Lcom/google/android/camera/CameraConstants;->〇〇888:Lcom/google/android/camera/size/AspectRatio;

    .line 84
    .line 85
    return-void
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public static final synthetic 〇080()Lcom/google/android/camera/size/AspectRatio;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/camera/CameraConstants;->〇o00〇〇Oo:Lcom/google/android/camera/size/AspectRatio;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public static final synthetic 〇o00〇〇Oo()Lcom/google/android/camera/size/AspectRatio;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/camera/CameraConstants;->Oo08:Lcom/google/android/camera/size/AspectRatio;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public static final synthetic 〇o〇()Lcom/google/android/camera/size/AspectRatio;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/camera/CameraConstants;->〇o〇:Lcom/google/android/camera/size/AspectRatio;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

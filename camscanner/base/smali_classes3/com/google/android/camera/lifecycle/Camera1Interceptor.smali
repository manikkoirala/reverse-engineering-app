.class public final Lcom/google/android/camera/lifecycle/Camera1Interceptor;
.super Lcom/google/android/camera/lifecycle/CameraInterceptor;
.source "Camera1Interceptor.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/camera/lifecycle/Camera1Interceptor$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final o〇0:Lcom/google/android/camera/lifecycle/Camera1Interceptor$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8:Landroid/hardware/Camera$CameraInfo;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private Oo08:I

.field private 〇o〇:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/google/android/camera/lifecycle/Camera1Interceptor$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/google/android/camera/lifecycle/Camera1Interceptor$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/google/android/camera/lifecycle/Camera1Interceptor;->o〇0:Lcom/google/android/camera/lifecycle/Camera1Interceptor$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/android/camera/lifecycle/CameraInterceptor;-><init>()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/google/android/camera/data/CameraApi;->〇080:Lcom/google/android/camera/data/CameraApi$Companion;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/google/android/camera/data/CameraApi$Companion;->〇080()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    iput v0, p0, Lcom/google/android/camera/lifecycle/Camera1Interceptor;->〇o〇:I

    .line 11
    .line 12
    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    .line 13
    .line 14
    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 15
    .line 16
    .line 17
    iput-object v0, p0, Lcom/google/android/camera/lifecycle/Camera1Interceptor;->O8:Landroid/hardware/Camera$CameraInfo;

    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final oO80(I)Z
    .locals 6

    .line 1
    const-string v0, "CameraX-Camera1Interceptor"

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    const/4 v2, 0x0

    .line 5
    :try_start_0
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    .line 6
    .line 7
    .line 8
    move-result v3

    .line 9
    const/4 v4, 0x0

    .line 10
    :goto_0
    if-ge v4, v3, :cond_1

    .line 11
    .line 12
    iget-object v5, p0, Lcom/google/android/camera/lifecycle/Camera1Interceptor;->O8:Landroid/hardware/Camera$CameraInfo;

    .line 13
    .line 14
    invoke-static {v4, v5}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 15
    .line 16
    .line 17
    iget-object v5, p0, Lcom/google/android/camera/lifecycle/Camera1Interceptor;->O8:Landroid/hardware/Camera$CameraInfo;

    .line 18
    .line 19
    iget v5, v5, Landroid/hardware/Camera$CameraInfo;->facing:I

    .line 20
    .line 21
    if-ne v5, p1, :cond_0

    .line 22
    .line 23
    iput v4, p0, Lcom/google/android/camera/lifecycle/Camera1Interceptor;->Oo08:I

    .line 24
    .line 25
    const-string p1, "chooseCamera, CameraId = %d"

    .line 26
    .line 27
    new-array v3, v1, [Ljava/lang/Object;

    .line 28
    .line 29
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 30
    .line 31
    .line 32
    move-result-object v4

    .line 33
    aput-object v4, v3, v2

    .line 34
    .line 35
    invoke-static {v0, p1, v3}, Lcom/google/android/camera/log/CameraLog;->oO80(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    .line 37
    .line 38
    return v1

    .line 39
    :cond_0
    add-int/lit8 v4, v4, 0x1

    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_1
    const-string p1, "chooseCamera, no camera available"

    .line 43
    .line 44
    invoke-static {v0, p1}, Lcom/google/android/camera/log/CameraLog;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    return v2

    .line 48
    :catch_0
    const-string p1, "chooseCamera error\uff0ctry 0"

    .line 49
    .line 50
    invoke-static {v0, p1}, Lcom/google/android/camera/log/CameraLog;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    iput v2, p0, Lcom/google/android/camera/lifecycle/Camera1Interceptor;->Oo08:I

    .line 54
    .line 55
    return v1
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method


# virtual methods
.method public 〇o00〇〇Oo(IILkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0
    .param p3    # Lkotlin/coroutines/Continuation;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lcom/google/android/camera/lifecycle/CameraUse;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p2}, Lcom/google/android/camera/lifecycle/Camera1Interceptor;->oO80(I)Z

    .line 2
    .line 3
    .line 4
    new-instance p1, Lcom/google/android/camera/lifecycle/CameraUse;

    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/google/android/camera/lifecycle/Camera1Interceptor;->〇o〇()I

    .line 7
    .line 8
    .line 9
    move-result p2

    .line 10
    iget p3, p0, Lcom/google/android/camera/lifecycle/Camera1Interceptor;->Oo08:I

    .line 11
    .line 12
    invoke-direct {p1, p2, p3}, Lcom/google/android/camera/lifecycle/CameraUse;-><init>(II)V

    .line 13
    .line 14
    .line 15
    return-object p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public 〇o〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/google/android/camera/lifecycle/Camera1Interceptor;->〇o〇:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

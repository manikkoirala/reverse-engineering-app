.class public final Lcom/google/android/camera/lifecycle/Camera2Interceptor;
.super Lcom/google/android/camera/lifecycle/CameraInterceptor;
.source "Camera2Interceptor.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/camera/lifecycle/Camera2Interceptor$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final Oo08:Lcom/google/android/camera/lifecycle/Camera2Interceptor$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8:I

.field private 〇o〇:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/google/android/camera/lifecycle/Camera2Interceptor$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/google/android/camera/lifecycle/Camera2Interceptor$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/google/android/camera/lifecycle/Camera2Interceptor;->Oo08:Lcom/google/android/camera/lifecycle/Camera2Interceptor$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/android/camera/lifecycle/CameraInterceptor;-><init>()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/google/android/camera/data/CameraApi;->〇080:Lcom/google/android/camera/data/CameraApi$Companion;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/google/android/camera/data/CameraApi$Companion;->〇o00〇〇Oo()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    iput v0, p0, Lcom/google/android/camera/lifecycle/Camera2Interceptor;->〇o〇:I

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
.end method


# virtual methods
.method public 〇o00〇〇Oo(IILkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 3
    .param p3    # Lkotlin/coroutines/Continuation;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lcom/google/android/camera/lifecycle/CameraUse;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lcom/google/android/camera/lifecycle/CameraInterceptor;->〇080(I)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 8
    .line 9
    const/16 v1, 0x1c

    .line 10
    .line 11
    if-lt v0, v1, :cond_0

    .line 12
    .line 13
    :try_start_0
    sget-object v0, Lcom/google/android/camera/lifecycle/Camera2CameraFactory;->〇o00〇〇Oo:Lcom/google/android/camera/lifecycle/Camera2CameraFactory;

    .line 14
    .line 15
    invoke-virtual {v0, p2}, Lcom/google/android/camera/lifecycle/CompatCameraFactory;->〇o〇(I)Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-eqz v1, :cond_0

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/google/android/camera/lifecycle/Camera2CameraFactory;->o〇0()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    iput v0, p0, Lcom/google/android/camera/lifecycle/Camera2Interceptor;->O8:I

    .line 30
    .line 31
    new-instance v0, Lcom/google/android/camera/lifecycle/CameraUse;

    .line 32
    .line 33
    invoke-virtual {p0}, Lcom/google/android/camera/lifecycle/Camera2Interceptor;->〇o〇()I

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    iget v2, p0, Lcom/google/android/camera/lifecycle/Camera2Interceptor;->O8:I

    .line 38
    .line 39
    invoke-direct {v0, v1, v2}, Lcom/google/android/camera/lifecycle/CameraUse;-><init>(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    .line 41
    .line 42
    return-object v0

    .line 43
    :catch_0
    move-exception v0

    .line 44
    const-string v1, "CameraX-Camera2Interceptor"

    .line 45
    .line 46
    const-string v2, "chooseCamera error"

    .line 47
    .line 48
    invoke-static {v1, v2, v0}, Lcom/google/android/camera/log/CameraLog;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/camera/lifecycle/CameraInterceptor;->O8(IILkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    return-object p1

    .line 56
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/camera/lifecycle/CameraInterceptor;->O8(IILkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 57
    .line 58
    .line 59
    move-result-object p1

    .line 60
    return-object p1
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public 〇o〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/google/android/camera/lifecycle/Camera2Interceptor;->〇o〇:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.class public final Lcom/google/android/camera/TextureViewImplementation$initializePreview$1$onSurfaceTextureDestroyed$1;
.super Ljava/lang/Object;
.source "TextureViewImplementation.kt"

# interfaces
.implements Lcom/google/android/camera/compat/utils/futures/FutureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/camera/TextureViewImplementation$initializePreview$1;->onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/camera/compat/utils/futures/FutureCallback<",
        "Lcom/google/android/camera/compat/view/SurfaceRequest$Result;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic 〇080:Landroid/graphics/SurfaceTexture;

.field final synthetic 〇o00〇〇Oo:Lcom/google/android/camera/TextureViewImplementation;


# direct methods
.method constructor <init>(Landroid/graphics/SurfaceTexture;Lcom/google/android/camera/TextureViewImplementation;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/camera/TextureViewImplementation$initializePreview$1$onSurfaceTextureDestroyed$1;->〇080:Landroid/graphics/SurfaceTexture;

    .line 2
    .line 3
    iput-object p2, p0, Lcom/google/android/camera/TextureViewImplementation$initializePreview$1$onSurfaceTextureDestroyed$1;->〇o00〇〇Oo:Lcom/google/android/camera/TextureViewImplementation;

    .line 4
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method


# virtual methods
.method public onFailure(Ljava/lang/Throwable;)V
    .locals 2
    .param p1    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string/jumbo v0, "t"

    .line 2
    .line 3
    .line 4
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 8
    .line 9
    const-string v1, "SurfaceReleaseFuture did not complete nicely."

    .line 10
    .line 11
    invoke-direct {v0, v1, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 12
    .line 13
    .line 14
    throw v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/google/android/camera/compat/view/SurfaceRequest$Result;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/google/android/camera/TextureViewImplementation$initializePreview$1$onSurfaceTextureDestroyed$1;->〇080(Lcom/google/android/camera/compat/view/SurfaceRequest$Result;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public 〇080(Lcom/google/android/camera/compat/view/SurfaceRequest$Result;)V
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    const/4 v1, 0x0

    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/google/android/camera/compat/view/SurfaceRequest$Result;->〇080()I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    const/4 v2, 0x3

    .line 10
    if-ne p1, v2, :cond_0

    .line 11
    .line 12
    const/4 v1, 0x1

    .line 13
    :cond_0
    xor-int/lit8 p1, v1, 0x1

    .line 14
    .line 15
    const-string v0, "Unexpected result from SurfaceRequest. Surface was provided twice."

    .line 16
    .line 17
    invoke-static {p1, v0}, Lcom/google/android/camera/compat/Preconditions;->o〇0(ZLjava/lang/String;)V

    .line 18
    .line 19
    .line 20
    const-string p1, "CameraX-TextureViewPreview"

    .line 21
    .line 22
    const-string v0, "SurfaceTexture about to manually be destroyed"

    .line 23
    .line 24
    invoke-static {p1, v0}, Lcom/google/android/camera/log/CameraLog;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    iget-object p1, p0, Lcom/google/android/camera/TextureViewImplementation$initializePreview$1$onSurfaceTextureDestroyed$1;->〇080:Landroid/graphics/SurfaceTexture;

    .line 28
    .line 29
    invoke-virtual {p1}, Landroid/graphics/SurfaceTexture;->release()V

    .line 30
    .line 31
    .line 32
    iget-object p1, p0, Lcom/google/android/camera/TextureViewImplementation$initializePreview$1$onSurfaceTextureDestroyed$1;->〇o00〇〇Oo:Lcom/google/android/camera/TextureViewImplementation;

    .line 33
    .line 34
    invoke-static {p1}, Lcom/google/android/camera/TextureViewImplementation;->OoO8(Lcom/google/android/camera/TextureViewImplementation;)Landroid/graphics/SurfaceTexture;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    if-eqz p1, :cond_1

    .line 39
    .line 40
    iget-object p1, p0, Lcom/google/android/camera/TextureViewImplementation$initializePreview$1$onSurfaceTextureDestroyed$1;->〇o00〇〇Oo:Lcom/google/android/camera/TextureViewImplementation;

    .line 41
    .line 42
    const/4 v0, 0x0

    .line 43
    invoke-static {p1, v0}, Lcom/google/android/camera/TextureViewImplementation;->〇oo〇(Lcom/google/android/camera/TextureViewImplementation;Landroid/graphics/SurfaceTexture;)V

    .line 44
    .line 45
    .line 46
    :cond_1
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.class public interface abstract Lcom/google/android/camera/ICamera;
.super Ljava/lang/Object;
.source "ICamera.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/camera/ICamera$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# virtual methods
.method public abstract O8(Lcom/google/android/camera/size/AspectRatio;)Z
    .param p1    # Lcom/google/android/camera/size/AspectRatio;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract getCameraApi()I
.end method

.method public abstract getCameraFacing()I
.end method

.method public abstract getCameraViewImpl()Lcom/google/android/camera/CameraViewImpl;
.end method

.method public abstract getFlash()I
.end method

.method public abstract getMaxZoom()F
.end method

.method public abstract getMinZoom()F
.end method

.method public abstract getSupportedAspectRatios()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/google/android/camera/size/AspectRatio;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getZoomRange()[F
.end method

.method public abstract release()V
.end method

.method public abstract setAutoFocus(Z)V
.end method

.method public abstract setCameraFacing(I)V
.end method

.method public abstract setCameraModel(Lcom/google/android/camera/data/CameraModel;)V
.end method

.method public abstract setDisplayOrientation(I)V
.end method

.method public abstract setFlash(I)V
.end method

.method public abstract setPictureSize(Lcom/google/android/camera/size/CameraSize;)V
.end method

.method public abstract setZoomRatio(F)V
.end method

.method public abstract start(I)V
.end method

.method public abstract stop()V
.end method

.method public abstract 〇080(Ljava/lang/Integer;)I
.end method

.method public abstract 〇o00〇〇Oo(I)V
.end method

.method public abstract 〇o〇()Z
.end method

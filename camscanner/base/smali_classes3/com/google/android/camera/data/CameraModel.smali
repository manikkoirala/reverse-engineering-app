.class public final Lcom/google/android/camera/data/CameraModel;
.super Ljava/lang/Object;
.source "CameraModel.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8:Lcom/google/android/camera/size/CameraSize;

.field private final 〇080:I

.field private final 〇o00〇〇Oo:F

.field private final 〇o〇:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(IFLjava/lang/Boolean;Lcom/google/android/camera/size/CameraSize;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lcom/google/android/camera/data/CameraModel;->〇080:I

    .line 3
    iput p2, p0, Lcom/google/android/camera/data/CameraModel;->〇o00〇〇Oo:F

    .line 4
    iput-object p3, p0, Lcom/google/android/camera/data/CameraModel;->〇o〇:Ljava/lang/Boolean;

    .line 5
    iput-object p4, p0, Lcom/google/android/camera/data/CameraModel;->O8:Lcom/google/android/camera/size/CameraSize;

    return-void
.end method

.method public synthetic constructor <init>(IFLjava/lang/Boolean;Lcom/google/android/camera/size/CameraSize;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    .line 6
    sget-object p1, Lcom/google/android/camera/data/Flash;->〇o〇:Lcom/google/android/camera/data/Flash$Companion;

    invoke-virtual {p1}, Lcom/google/android/camera/data/Flash$Companion;->〇o00〇〇Oo()I

    move-result p1

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    const/high16 p2, -0x40800000    # -1.0f

    :cond_1
    and-int/lit8 p6, p5, 0x4

    const/4 v0, 0x0

    if-eqz p6, :cond_2

    move-object p3, v0

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    move-object p4, v0

    .line 7
    :cond_3
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/camera/data/CameraModel;-><init>(IFLjava/lang/Boolean;Lcom/google/android/camera/size/CameraSize;)V

    return-void
.end method


# virtual methods
.method public final O8()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/google/android/camera/data/CameraModel;->〇o00〇〇Oo:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final 〇080()Ljava/lang/Boolean;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/data/CameraModel;->〇o〇:Ljava/lang/Boolean;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final 〇o00〇〇Oo()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/google/android/camera/data/CameraModel;->〇080:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final 〇o〇()Lcom/google/android/camera/size/CameraSize;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/camera/data/CameraModel;->O8:Lcom/google/android/camera/size/CameraSize;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

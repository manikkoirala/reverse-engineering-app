.class public final Lcom/google/ads/mediation/facebook/R$string;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ads/mediation/facebook/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final androidx_startup:I = 0x7f1304ea

.field public static final common_google_play_services_enable_button:I = 0x7f1305a7

.field public static final common_google_play_services_enable_text:I = 0x7f1305a8

.field public static final common_google_play_services_enable_title:I = 0x7f1305a9

.field public static final common_google_play_services_install_button:I = 0x7f1305aa

.field public static final common_google_play_services_install_text:I = 0x7f1305ab

.field public static final common_google_play_services_install_title:I = 0x7f1305ac

.field public static final common_google_play_services_notification_channel_name:I = 0x7f1305ad

.field public static final common_google_play_services_notification_ticker:I = 0x7f1305ae

.field public static final common_google_play_services_unknown_issue:I = 0x7f1305af

.field public static final common_google_play_services_unsupported_text:I = 0x7f1305b0

.field public static final common_google_play_services_update_button:I = 0x7f1305b1

.field public static final common_google_play_services_update_text:I = 0x7f1305b2

.field public static final common_google_play_services_update_title:I = 0x7f1305b3

.field public static final common_google_play_services_updating_text:I = 0x7f1305b4

.field public static final common_google_play_services_wear_update_text:I = 0x7f1305b5

.field public static final common_open_on_phone:I = 0x7f1305b6

.field public static final common_signin_button_text:I = 0x7f1305b7

.field public static final common_signin_button_text_long:I = 0x7f1305b8

.field public static final copy_toast_msg:I = 0x7f1305b9

.field public static final fallback_menu_item_copy_link:I = 0x7f131d25

.field public static final fallback_menu_item_open_in_browser:I = 0x7f131d26

.field public static final fallback_menu_item_share_link:I = 0x7f131d27

.field public static final native_body:I = 0x7f131e06

.field public static final native_headline:I = 0x7f131e07

.field public static final native_media_view:I = 0x7f131e08

.field public static final notifications_permission_confirm:I = 0x7f131e2c

.field public static final notifications_permission_decline:I = 0x7f131e2d

.field public static final notifications_permission_title:I = 0x7f131e2e

.field public static final offline_notification_text:I = 0x7f131e2f

.field public static final offline_notification_title:I = 0x7f131e30

.field public static final offline_opt_in_confirm:I = 0x7f131e31

.field public static final offline_opt_in_confirmation:I = 0x7f131e32

.field public static final offline_opt_in_decline:I = 0x7f131e33

.field public static final offline_opt_in_message:I = 0x7f131e34

.field public static final offline_opt_in_title:I = 0x7f131e35

.field public static final s1:I = 0x7f131e62

.field public static final s2:I = 0x7f131e63

.field public static final s3:I = 0x7f131e64

.field public static final s4:I = 0x7f131e65

.field public static final s5:I = 0x7f131e66

.field public static final s6:I = 0x7f131e67

.field public static final s7:I = 0x7f131e68

.field public static final status_bar_notification_info_overflow:I = 0x7f131eca

.field public static final watermark_label_prefix:I = 0x7f131f99


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

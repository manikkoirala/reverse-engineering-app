.class public final Lcom/google/ads/mediation/pangle/R$drawable;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ads/mediation/pangle/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final abc_ab_share_pack_mtrl_alpha:I = 0x7f080050

.field public static final abc_action_bar_item_background_material:I = 0x7f080051

.field public static final abc_btn_borderless_material:I = 0x7f080052

.field public static final abc_btn_check_material:I = 0x7f080053

.field public static final abc_btn_check_material_anim:I = 0x7f080054

.field public static final abc_btn_check_to_on_mtrl_000:I = 0x7f080055

.field public static final abc_btn_check_to_on_mtrl_015:I = 0x7f080056

.field public static final abc_btn_colored_material:I = 0x7f080057

.field public static final abc_btn_default_mtrl_shape:I = 0x7f080058

.field public static final abc_btn_radio_material:I = 0x7f080059

.field public static final abc_btn_radio_material_anim:I = 0x7f08005a

.field public static final abc_btn_radio_to_on_mtrl_000:I = 0x7f08005b

.field public static final abc_btn_radio_to_on_mtrl_015:I = 0x7f08005c

.field public static final abc_btn_switch_to_on_mtrl_00001:I = 0x7f08005d

.field public static final abc_btn_switch_to_on_mtrl_00012:I = 0x7f08005e

.field public static final abc_cab_background_internal_bg:I = 0x7f08005f

.field public static final abc_cab_background_top_material:I = 0x7f080060

.field public static final abc_cab_background_top_mtrl_alpha:I = 0x7f080061

.field public static final abc_control_background_material:I = 0x7f080062

.field public static final abc_dialog_material_background:I = 0x7f080063

.field public static final abc_edit_text_material:I = 0x7f080064

.field public static final abc_ic_ab_back_material:I = 0x7f080065

.field public static final abc_ic_arrow_drop_right_black_24dp:I = 0x7f080066

.field public static final abc_ic_clear_material:I = 0x7f080067

.field public static final abc_ic_commit_search_api_mtrl_alpha:I = 0x7f080068

.field public static final abc_ic_go_search_api_material:I = 0x7f080069

.field public static final abc_ic_menu_copy_mtrl_am_alpha:I = 0x7f08006a

.field public static final abc_ic_menu_cut_mtrl_alpha:I = 0x7f08006b

.field public static final abc_ic_menu_overflow_material:I = 0x7f08006c

.field public static final abc_ic_menu_paste_mtrl_am_alpha:I = 0x7f08006d

.field public static final abc_ic_menu_selectall_mtrl_alpha:I = 0x7f08006e

.field public static final abc_ic_menu_share_mtrl_alpha:I = 0x7f08006f

.field public static final abc_ic_search_api_material:I = 0x7f080070

.field public static final abc_ic_voice_search_api_material:I = 0x7f080071

.field public static final abc_item_background_holo_dark:I = 0x7f080072

.field public static final abc_item_background_holo_light:I = 0x7f080073

.field public static final abc_list_divider_material:I = 0x7f080074

.field public static final abc_list_divider_mtrl_alpha:I = 0x7f080075

.field public static final abc_list_focused_holo:I = 0x7f080076

.field public static final abc_list_longpressed_holo:I = 0x7f080077

.field public static final abc_list_pressed_holo_dark:I = 0x7f080078

.field public static final abc_list_pressed_holo_light:I = 0x7f080079

.field public static final abc_list_selector_background_transition_holo_dark:I = 0x7f08007a

.field public static final abc_list_selector_background_transition_holo_light:I = 0x7f08007b

.field public static final abc_list_selector_disabled_holo_dark:I = 0x7f08007c

.field public static final abc_list_selector_disabled_holo_light:I = 0x7f08007d

.field public static final abc_list_selector_holo_dark:I = 0x7f08007e

.field public static final abc_list_selector_holo_light:I = 0x7f08007f

.field public static final abc_menu_hardkey_panel_mtrl_mult:I = 0x7f080080

.field public static final abc_popup_background_mtrl_mult:I = 0x7f080081

.field public static final abc_ratingbar_indicator_material:I = 0x7f080082

.field public static final abc_ratingbar_material:I = 0x7f080083

.field public static final abc_ratingbar_small_material:I = 0x7f080084

.field public static final abc_scrubber_control_off_mtrl_alpha:I = 0x7f080085

.field public static final abc_scrubber_control_to_pressed_mtrl_000:I = 0x7f080086

.field public static final abc_scrubber_control_to_pressed_mtrl_005:I = 0x7f080087

.field public static final abc_scrubber_primary_mtrl_alpha:I = 0x7f080088

.field public static final abc_scrubber_track_mtrl_alpha:I = 0x7f080089

.field public static final abc_seekbar_thumb_material:I = 0x7f08008a

.field public static final abc_seekbar_tick_mark_material:I = 0x7f08008b

.field public static final abc_seekbar_track_material:I = 0x7f08008c

.field public static final abc_spinner_mtrl_am_alpha:I = 0x7f08008d

.field public static final abc_spinner_textfield_background_material:I = 0x7f08008e

.field public static final abc_star_black_48dp:I = 0x7f08008f

.field public static final abc_star_half_black_48dp:I = 0x7f080090

.field public static final abc_switch_thumb_material:I = 0x7f080091

.field public static final abc_switch_track_mtrl_alpha:I = 0x7f080092

.field public static final abc_tab_indicator_material:I = 0x7f080093

.field public static final abc_tab_indicator_mtrl_alpha:I = 0x7f080094

.field public static final abc_text_cursor_material:I = 0x7f080095

.field public static final abc_text_select_handle_left_mtrl:I = 0x7f080096

.field public static final abc_text_select_handle_middle_mtrl:I = 0x7f080097

.field public static final abc_text_select_handle_right_mtrl:I = 0x7f080098

.field public static final abc_textfield_activated_mtrl_alpha:I = 0x7f080099

.field public static final abc_textfield_default_mtrl_alpha:I = 0x7f08009a

.field public static final abc_textfield_search_activated_mtrl_alpha:I = 0x7f08009b

.field public static final abc_textfield_search_default_mtrl_alpha:I = 0x7f08009c

.field public static final abc_textfield_search_material:I = 0x7f08009d

.field public static final abc_vector_test:I = 0x7f08009e

.field public static final admob_close_button_black_circle_white_cross:I = 0x7f0800ab

.field public static final admob_close_button_white_circle_black_cross:I = 0x7f0800ac

.field public static final btn_checkbox_checked_mtrl:I = 0x7f08037f

.field public static final btn_checkbox_checked_to_unchecked_mtrl_animation:I = 0x7f080380

.field public static final btn_checkbox_unchecked_mtrl:I = 0x7f080381

.field public static final btn_checkbox_unchecked_to_checked_mtrl_animation:I = 0x7f080382

.field public static final btn_radio_off_mtrl:I = 0x7f080399

.field public static final btn_radio_off_to_on_mtrl_animation:I = 0x7f08039a

.field public static final btn_radio_on_mtrl:I = 0x7f08039b

.field public static final btn_radio_on_to_off_mtrl_animation:I = 0x7f08039c

.field public static final common_full_open_on_phone:I = 0x7f0803d9

.field public static final common_google_signin_btn_icon_dark:I = 0x7f0803da

.field public static final common_google_signin_btn_icon_dark_focused:I = 0x7f0803db

.field public static final common_google_signin_btn_icon_dark_normal:I = 0x7f0803dc

.field public static final common_google_signin_btn_icon_dark_normal_background:I = 0x7f0803dd

.field public static final common_google_signin_btn_icon_disabled:I = 0x7f0803de

.field public static final common_google_signin_btn_icon_light:I = 0x7f0803df

.field public static final common_google_signin_btn_icon_light_focused:I = 0x7f0803e0

.field public static final common_google_signin_btn_icon_light_normal:I = 0x7f0803e1

.field public static final common_google_signin_btn_icon_light_normal_background:I = 0x7f0803e2

.field public static final common_google_signin_btn_text_dark:I = 0x7f0803e3

.field public static final common_google_signin_btn_text_dark_focused:I = 0x7f0803e4

.field public static final common_google_signin_btn_text_dark_normal:I = 0x7f0803e5

.field public static final common_google_signin_btn_text_dark_normal_background:I = 0x7f0803e6

.field public static final common_google_signin_btn_text_disabled:I = 0x7f0803e7

.field public static final common_google_signin_btn_text_light:I = 0x7f0803e8

.field public static final common_google_signin_btn_text_light_focused:I = 0x7f0803e9

.field public static final common_google_signin_btn_text_light_normal:I = 0x7f0803ea

.field public static final common_google_signin_btn_text_light_normal_background:I = 0x7f0803eb

.field public static final googleg_disabled_color_18:I = 0x7f08046a

.field public static final googleg_standard_color_18:I = 0x7f08046b

.field public static final notification_action_background:I = 0x7f081002

.field public static final notification_bg:I = 0x7f081003

.field public static final notification_bg_low:I = 0x7f081004

.field public static final notification_bg_low_normal:I = 0x7f081005

.field public static final notification_bg_low_pressed:I = 0x7f081006

.field public static final notification_bg_normal:I = 0x7f081007

.field public static final notification_bg_normal_pressed:I = 0x7f081008

.field public static final notification_icon_background:I = 0x7f081009

.field public static final notification_template_icon_bg:I = 0x7f08100b

.field public static final notification_template_icon_low_bg:I = 0x7f08100c

.field public static final notification_tile_bg:I = 0x7f08100d

.field public static final notify_panel_notification_icon_bg:I = 0x7f08100e

.field public static final tooltip_frame_dark:I = 0x7f081214

.field public static final tooltip_frame_light:I = 0x7f081215

.field public static final tt_ad_arrow_backward:I = 0x7f081217

.field public static final tt_ad_arrow_backward_wrapper:I = 0x7f081218

.field public static final tt_ad_arrow_forward:I = 0x7f081219

.field public static final tt_ad_arrow_forward_wrapper:I = 0x7f08121a

.field public static final tt_ad_closed_background_300_250:I = 0x7f08121b

.field public static final tt_ad_closed_background_320_50:I = 0x7f08121c

.field public static final tt_ad_closed_logo_red:I = 0x7f08121d

.field public static final tt_ad_cover_btn_begin_bg:I = 0x7f08121e

.field public static final tt_ad_download_progress_bar_horizontal:I = 0x7f08121f

.field public static final tt_ad_feedback:I = 0x7f081220

.field public static final tt_ad_landing_loading_three_left:I = 0x7f081221

.field public static final tt_ad_landing_loading_three_mid:I = 0x7f081222

.field public static final tt_ad_landing_loading_three_right:I = 0x7f081223

.field public static final tt_ad_link:I = 0x7f081224

.field public static final tt_ad_loading_rect:I = 0x7f081225

.field public static final tt_ad_loading_three_left:I = 0x7f081226

.field public static final tt_ad_loading_three_mid:I = 0x7f081227

.field public static final tt_ad_loading_three_right:I = 0x7f081228

.field public static final tt_ad_logo:I = 0x7f081229

.field public static final tt_ad_logo_new:I = 0x7f08122a

.field public static final tt_ad_logo_reward_full:I = 0x7f08122b

.field public static final tt_ad_refresh:I = 0x7f08122c

.field public static final tt_ad_report_info_bg:I = 0x7f08122d

.field public static final tt_ad_report_info_button_bg:I = 0x7f08122e

.field public static final tt_ad_skip_btn_bg:I = 0x7f08122f

.field public static final tt_ad_skip_btn_bg2:I = 0x7f081230

.field public static final tt_ad_threedots:I = 0x7f081231

.field public static final tt_ad_xmark:I = 0x7f081232

.field public static final tt_app_open_top_bg:I = 0x7f081233

.field public static final tt_back_video:I = 0x7f081234

.field public static final tt_backup_btn_1:I = 0x7f081235

.field public static final tt_backup_btn_2:I = 0x7f081236

.field public static final tt_browser_download_selector:I = 0x7f081237

.field public static final tt_browser_progress_style:I = 0x7f081238

.field public static final tt_button_back:I = 0x7f081239

.field public static final tt_circle_solid_mian:I = 0x7f08123a

.field public static final tt_close_btn:I = 0x7f08123b

.field public static final tt_close_move_detail:I = 0x7f08123c

.field public static final tt_close_move_details_normal:I = 0x7f08123d

.field public static final tt_close_move_details_pressed:I = 0x7f08123e

.field public static final tt_custom_dialog_bg:I = 0x7f08123f

.field public static final tt_detail_video_btn_bg:I = 0x7f081240

.field public static final tt_dislike_bottom_seletor:I = 0x7f081241

.field public static final tt_dislike_cancle_bg_selector:I = 0x7f081242

.field public static final tt_dislike_dialog_bg:I = 0x7f081243

.field public static final tt_dislike_icon:I = 0x7f081244

.field public static final tt_dislike_icon2:I = 0x7f081245

.field public static final tt_dislike_middle_seletor:I = 0x7f081246

.field public static final tt_dislike_top_bg:I = 0x7f081247

.field public static final tt_dislike_top_seletor:I = 0x7f081248

.field public static final tt_download_corner_bg:I = 0x7f081249

.field public static final tt_enlarge_video:I = 0x7f08124a

.field public static final tt_forward_video:I = 0x7f08124b

.field public static final tt_full_reward_loading_progress_style:I = 0x7f08124c

.field public static final tt_interact_circle:I = 0x7f08124d

.field public static final tt_interact_four_transparent_round_rect:I = 0x7f08124e

.field public static final tt_interact_oval:I = 0x7f08124f

.field public static final tt_interact_round_rect:I = 0x7f081250

.field public static final tt_interact_round_rect_stroke:I = 0x7f081251

.field public static final tt_item_background_material:I = 0x7f081252

.field public static final tt_landingpage_loading_1_progress_style:I = 0x7f081253

.field public static final tt_landingpage_loading_text_rect:I = 0x7f081254

.field public static final tt_leftbackbutton_titlebar_photo_preview:I = 0x7f081255

.field public static final tt_leftbackicon_selector:I = 0x7f081256

.field public static final tt_leftbackicon_selector_for_dark:I = 0x7f081257

.field public static final tt_lefterbackicon_titlebar:I = 0x7f081258

.field public static final tt_lefterbackicon_titlebar_for_dark:I = 0x7f081259

.field public static final tt_lefterbackicon_titlebar_press:I = 0x7f08125a

.field public static final tt_lefterbackicon_titlebar_press_for_dark:I = 0x7f08125b

.field public static final tt_lefterbackicon_titlebar_press_wrapper:I = 0x7f08125c

.field public static final tt_mute:I = 0x7f08125d

.field public static final tt_mute_btn_bg:I = 0x7f08125e

.field public static final tt_mute_wrapper:I = 0x7f08125f

.field public static final tt_new_pause_video:I = 0x7f081260

.field public static final tt_new_pause_video_press:I = 0x7f081261

.field public static final tt_new_play_video:I = 0x7f081262

.field public static final tt_normalscreen_loading:I = 0x7f081263

.field public static final tt_pangle_ad_close_btn_bg:I = 0x7f081264

.field public static final tt_pangle_ad_close_drawable:I = 0x7f081265

.field public static final tt_pangle_ad_mute_btn_bg:I = 0x7f081266

.field public static final tt_pangle_banner_btn_bg:I = 0x7f081267

.field public static final tt_pangle_btn_bg:I = 0x7f081268

.field public static final tt_pangle_close_icon:I = 0x7f081269

.field public static final tt_pangle_logo_white:I = 0x7f08126a

.field public static final tt_pangle_star_empty_bg:I = 0x7f08126b

.field public static final tt_pangle_star_full_bg:I = 0x7f08126c

.field public static final tt_play_movebar_textpage:I = 0x7f08126d

.field public static final tt_playable_btn_bk:I = 0x7f08126e

.field public static final tt_playable_progress_style:I = 0x7f08126f

.field public static final tt_privacy_back_icon:I = 0x7f081270

.field public static final tt_privacy_bg:I = 0x7f081271

.field public static final tt_privacy_btn_bg:I = 0x7f081272

.field public static final tt_privacy_progress_style:I = 0x7f081273

.field public static final tt_privacy_webview_bg:I = 0x7f081274

.field public static final tt_refreshing_video_textpage:I = 0x7f081275

.field public static final tt_refreshing_video_textpage_normal:I = 0x7f081276

.field public static final tt_refreshing_video_textpage_pressed:I = 0x7f081277

.field public static final tt_reward_countdown_bg:I = 0x7f081278

.field public static final tt_reward_dislike_icon:I = 0x7f081279

.field public static final tt_reward_full_feedback:I = 0x7f08127a

.field public static final tt_reward_full_mute:I = 0x7f08127b

.field public static final tt_reward_full_new_bar_bg:I = 0x7f08127c

.field public static final tt_reward_full_new_bar_btn_bg:I = 0x7f08127d

.field public static final tt_reward_full_unmute:I = 0x7f08127e

.field public static final tt_reward_full_video_backup_btn_bg:I = 0x7f08127f

.field public static final tt_reward_video_download_btn_bg:I = 0x7f081280

.field public static final tt_seek_progress:I = 0x7f081281

.field public static final tt_seek_thumb:I = 0x7f081282

.field public static final tt_seek_thumb_fullscreen:I = 0x7f081283

.field public static final tt_seek_thumb_fullscreen_press:I = 0x7f081284

.field public static final tt_seek_thumb_fullscreen_selector:I = 0x7f081285

.field public static final tt_seek_thumb_normal:I = 0x7f081286

.field public static final tt_seek_thumb_press:I = 0x7f081287

.field public static final tt_shadow_btn_back:I = 0x7f081288

.field public static final tt_shadow_btn_back_withoutnight:I = 0x7f081289

.field public static final tt_shadow_fullscreen_top:I = 0x7f08128a

.field public static final tt_shadow_lefterback_titlebar:I = 0x7f08128b

.field public static final tt_shadow_lefterback_titlebar_press:I = 0x7f08128c

.field public static final tt_shadow_lefterback_titlebar_press_withoutnight:I = 0x7f08128d

.field public static final tt_shadow_lefterback_titlebar_withoutnight:I = 0x7f08128e

.field public static final tt_shrink_fullscreen:I = 0x7f08128f

.field public static final tt_shrink_video:I = 0x7f081290

.field public static final tt_skip_btn:I = 0x7f081291

.field public static final tt_skip_btn_wrapper:I = 0x7f081292

.field public static final tt_slide_up_1:I = 0x7f081293

.field public static final tt_slide_up_10:I = 0x7f081294

.field public static final tt_slide_up_11:I = 0x7f081295

.field public static final tt_slide_up_12:I = 0x7f081296

.field public static final tt_slide_up_13:I = 0x7f081297

.field public static final tt_slide_up_14:I = 0x7f081298

.field public static final tt_slide_up_15:I = 0x7f081299

.field public static final tt_slide_up_2:I = 0x7f08129a

.field public static final tt_slide_up_3:I = 0x7f08129b

.field public static final tt_slide_up_4:I = 0x7f08129c

.field public static final tt_slide_up_5:I = 0x7f08129d

.field public static final tt_slide_up_6:I = 0x7f08129e

.field public static final tt_slide_up_7:I = 0x7f08129f

.field public static final tt_slide_up_8:I = 0x7f0812a0

.field public static final tt_slide_up_9:I = 0x7f0812a1

.field public static final tt_splash_brush_bg:I = 0x7f0812a2

.field public static final tt_splash_hand:I = 0x7f0812a3

.field public static final tt_splash_hand2:I = 0x7f0812a4

.field public static final tt_splash_hand3:I = 0x7f0812a5

.field public static final tt_splash_mute:I = 0x7f0812a6

.field public static final tt_splash_rock:I = 0x7f0812a7

.field public static final tt_splash_shake_hand:I = 0x7f0812a8

.field public static final tt_splash_slide_right_bg:I = 0x7f0812a9

.field public static final tt_splash_slide_right_circle:I = 0x7f0812aa

.field public static final tt_splash_slide_up_10:I = 0x7f0812ab

.field public static final tt_splash_slide_up_arrow:I = 0x7f0812ac

.field public static final tt_splash_slide_up_bg:I = 0x7f0812ad

.field public static final tt_splash_slide_up_circle:I = 0x7f0812ae

.field public static final tt_splash_slide_up_finger:I = 0x7f0812af

.field public static final tt_splash_twist:I = 0x7f0812b0

.field public static final tt_splash_unlock_btn_bg:I = 0x7f0812b1

.field public static final tt_splash_unlock_icon_empty:I = 0x7f0812b2

.field public static final tt_splash_unlock_image_arrow:I = 0x7f0812b3

.field public static final tt_splash_unlock_image_arrow_wrapper:I = 0x7f0812b4

.field public static final tt_splash_unlock_image_go:I = 0x7f0812b5

.field public static final tt_splash_unmute:I = 0x7f0812b6

.field public static final tt_star:I = 0x7f0812b7

.field public static final tt_star_thick:I = 0x7f0812b8

.field public static final tt_stop_movebar_textpage:I = 0x7f0812b9

.field public static final tt_suggestion_logo:I = 0x7f0812ba

.field public static final tt_titlebar_close:I = 0x7f0812bb

.field public static final tt_titlebar_close_drawable:I = 0x7f0812bc

.field public static final tt_titlebar_close_for_dark:I = 0x7f0812bd

.field public static final tt_titlebar_close_press:I = 0x7f0812be

.field public static final tt_titlebar_close_press_for_dark:I = 0x7f0812bf

.field public static final tt_titlebar_close_seletor:I = 0x7f0812c0

.field public static final tt_titlebar_close_seletor_for_dark:I = 0x7f0812c1

.field public static final tt_unmute:I = 0x7f0812c2

.field public static final tt_unmute_wrapper:I = 0x7f0812c3

.field public static final tt_up_slide:I = 0x7f0812c4

.field public static final tt_user:I = 0x7f0812c5

.field public static final tt_user_info:I = 0x7f0812c6

.field public static final tt_video_black_desc_gradient:I = 0x7f0812c7

.field public static final tt_video_close:I = 0x7f0812c8

.field public static final tt_video_close_drawable:I = 0x7f0812c9

.field public static final tt_video_loading_progress_bar:I = 0x7f0812ca

.field public static final tt_video_progress:I = 0x7f0812cb

.field public static final tt_video_progress_drawable:I = 0x7f0812cc

.field public static final tt_white_lefterbackicon_titlebar:I = 0x7f0812cd

.field public static final tt_white_lefterbackicon_titlebar_press:I = 0x7f0812ce

.field public static final tt_white_righterbackicon_titlebar:I = 0x7f0812cf

.field public static final tt_white_slide_up:I = 0x7f0812d0

.field public static final tt_wriggle_union:I = 0x7f0812d1

.field public static final tt_wriggle_union_white:I = 0x7f0812d2


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public final Lkotlinx/serialization/json/JsonBuilder;
.super Ljava/lang/Object;
.source "Json.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private O8:Z

.field private OO0o〇〇:Lkotlinx/serialization/modules/SerializersModule;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO0o〇〇〇〇0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private Oo08:Z

.field private oO80:Z

.field private o〇0:Z

.field private 〇080:Z

.field private 〇80〇808〇O:Z

.field private 〇8o8o〇:Z

.field private 〇O8o08O:Z

.field private 〇o00〇〇Oo:Z

.field private 〇o〇:Z

.field private 〇〇888:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlinx/serialization/json/Json;)V
    .locals 1
    .param p1    # Lkotlinx/serialization/json/Json;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "json"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p1}, Lkotlinx/serialization/json/Json;->Oo08()Lkotlinx/serialization/json/JsonConfiguration;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {v0}, Lkotlinx/serialization/json/JsonConfiguration;->Oo08()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    iput-boolean v0, p0, Lkotlinx/serialization/json/JsonBuilder;->〇080:Z

    .line 18
    .line 19
    invoke-virtual {p1}, Lkotlinx/serialization/json/Json;->Oo08()Lkotlinx/serialization/json/JsonConfiguration;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-virtual {v0}, Lkotlinx/serialization/json/JsonConfiguration;->o〇0()Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    iput-boolean v0, p0, Lkotlinx/serialization/json/JsonBuilder;->〇o00〇〇Oo:Z

    .line 28
    .line 29
    invoke-virtual {p1}, Lkotlinx/serialization/json/Json;->Oo08()Lkotlinx/serialization/json/JsonConfiguration;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lkotlinx/serialization/json/JsonConfiguration;->〇〇888()Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    iput-boolean v0, p0, Lkotlinx/serialization/json/JsonBuilder;->〇o〇:Z

    .line 38
    .line 39
    invoke-virtual {p1}, Lkotlinx/serialization/json/Json;->Oo08()Lkotlinx/serialization/json/JsonConfiguration;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    invoke-virtual {v0}, Lkotlinx/serialization/json/JsonConfiguration;->〇O8o08O()Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    iput-boolean v0, p0, Lkotlinx/serialization/json/JsonBuilder;->O8:Z

    .line 48
    .line 49
    invoke-virtual {p1}, Lkotlinx/serialization/json/Json;->Oo08()Lkotlinx/serialization/json/JsonConfiguration;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lkotlinx/serialization/json/JsonConfiguration;->〇o00〇〇Oo()Z

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    iput-boolean v0, p0, Lkotlinx/serialization/json/JsonBuilder;->Oo08:Z

    .line 58
    .line 59
    invoke-virtual {p1}, Lkotlinx/serialization/json/Json;->Oo08()Lkotlinx/serialization/json/JsonConfiguration;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lkotlinx/serialization/json/JsonConfiguration;->oO80()Z

    .line 64
    .line 65
    .line 66
    move-result v0

    .line 67
    iput-boolean v0, p0, Lkotlinx/serialization/json/JsonBuilder;->o〇0:Z

    .line 68
    .line 69
    invoke-virtual {p1}, Lkotlinx/serialization/json/Json;->Oo08()Lkotlinx/serialization/json/JsonConfiguration;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    invoke-virtual {v0}, Lkotlinx/serialization/json/JsonConfiguration;->〇80〇808〇O()Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    iput-object v0, p0, Lkotlinx/serialization/json/JsonBuilder;->〇〇888:Ljava/lang/String;

    .line 78
    .line 79
    invoke-virtual {p1}, Lkotlinx/serialization/json/Json;->Oo08()Lkotlinx/serialization/json/JsonConfiguration;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    invoke-virtual {v0}, Lkotlinx/serialization/json/JsonConfiguration;->O8()Z

    .line 84
    .line 85
    .line 86
    move-result v0

    .line 87
    iput-boolean v0, p0, Lkotlinx/serialization/json/JsonBuilder;->oO80:Z

    .line 88
    .line 89
    invoke-virtual {p1}, Lkotlinx/serialization/json/Json;->Oo08()Lkotlinx/serialization/json/JsonConfiguration;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    invoke-virtual {v0}, Lkotlinx/serialization/json/JsonConfiguration;->〇8o8o〇()Z

    .line 94
    .line 95
    .line 96
    move-result v0

    .line 97
    iput-boolean v0, p0, Lkotlinx/serialization/json/JsonBuilder;->〇80〇808〇O:Z

    .line 98
    .line 99
    invoke-virtual {p1}, Lkotlinx/serialization/json/Json;->Oo08()Lkotlinx/serialization/json/JsonConfiguration;

    .line 100
    .line 101
    .line 102
    move-result-object v0

    .line 103
    invoke-virtual {v0}, Lkotlinx/serialization/json/JsonConfiguration;->〇o〇()Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object v0

    .line 107
    iput-object v0, p0, Lkotlinx/serialization/json/JsonBuilder;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 108
    .line 109
    invoke-virtual {p1}, Lkotlinx/serialization/json/Json;->Oo08()Lkotlinx/serialization/json/JsonConfiguration;

    .line 110
    .line 111
    .line 112
    move-result-object v0

    .line 113
    invoke-virtual {v0}, Lkotlinx/serialization/json/JsonConfiguration;->〇080()Z

    .line 114
    .line 115
    .line 116
    move-result v0

    .line 117
    iput-boolean v0, p0, Lkotlinx/serialization/json/JsonBuilder;->〇8o8o〇:Z

    .line 118
    .line 119
    invoke-virtual {p1}, Lkotlinx/serialization/json/Json;->Oo08()Lkotlinx/serialization/json/JsonConfiguration;

    .line 120
    .line 121
    .line 122
    move-result-object v0

    .line 123
    invoke-virtual {v0}, Lkotlinx/serialization/json/JsonConfiguration;->OO0o〇〇〇〇0()Z

    .line 124
    .line 125
    .line 126
    move-result v0

    .line 127
    iput-boolean v0, p0, Lkotlinx/serialization/json/JsonBuilder;->〇O8o08O:Z

    .line 128
    .line 129
    invoke-virtual {p1}, Lkotlinx/serialization/json/Json;->〇080()Lkotlinx/serialization/modules/SerializersModule;

    .line 130
    .line 131
    .line 132
    move-result-object p1

    .line 133
    iput-object p1, p0, Lkotlinx/serialization/json/JsonBuilder;->OO0o〇〇:Lkotlinx/serialization/modules/SerializersModule;

    .line 134
    .line 135
    return-void
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method


# virtual methods
.method public final O8(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lkotlinx/serialization/json/JsonBuilder;->〇080:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final Oo08(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lkotlinx/serialization/json/JsonBuilder;->〇o00〇〇Oo:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final o〇0(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lkotlinx/serialization/json/JsonBuilder;->〇o〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final 〇080()Lkotlinx/serialization/json/JsonConfiguration;
    .locals 15
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-boolean v0, p0, Lkotlinx/serialization/json/JsonBuilder;->〇80〇808〇O:Z

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    iget-object v0, p0, Lkotlinx/serialization/json/JsonBuilder;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 6
    .line 7
    const-string/jumbo v1, "type"

    .line 8
    .line 9
    .line 10
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 18
    .line 19
    const-string v1, "Class discriminator should not be specified when array polymorphism is specified"

    .line 20
    .line 21
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    throw v0

    .line 29
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lkotlinx/serialization/json/JsonBuilder;->o〇0:Z

    .line 30
    .line 31
    const-string v1, "    "

    .line 32
    .line 33
    if-nez v0, :cond_3

    .line 34
    .line 35
    iget-object v0, p0, Lkotlinx/serialization/json/JsonBuilder;->〇〇888:Ljava/lang/String;

    .line 36
    .line 37
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    if-eqz v0, :cond_2

    .line 42
    .line 43
    goto :goto_4

    .line 44
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 45
    .line 46
    const-string v1, "Indent should not be specified when default printing mode is used"

    .line 47
    .line 48
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    throw v0

    .line 56
    :cond_3
    iget-object v0, p0, Lkotlinx/serialization/json/JsonBuilder;->〇〇888:Ljava/lang/String;

    .line 57
    .line 58
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 59
    .line 60
    .line 61
    move-result v0

    .line 62
    if-nez v0, :cond_9

    .line 63
    .line 64
    iget-object v0, p0, Lkotlinx/serialization/json/JsonBuilder;->〇〇888:Ljava/lang/String;

    .line 65
    .line 66
    const/4 v1, 0x0

    .line 67
    const/4 v2, 0x0

    .line 68
    :goto_1
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 69
    .line 70
    .line 71
    move-result v3

    .line 72
    const/4 v4, 0x1

    .line 73
    if-ge v2, v3, :cond_7

    .line 74
    .line 75
    invoke-interface {v0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    .line 76
    .line 77
    .line 78
    move-result v3

    .line 79
    const/16 v5, 0x20

    .line 80
    .line 81
    if-eq v3, v5, :cond_5

    .line 82
    .line 83
    const/16 v5, 0x9

    .line 84
    .line 85
    if-eq v3, v5, :cond_5

    .line 86
    .line 87
    const/16 v5, 0xd

    .line 88
    .line 89
    if-eq v3, v5, :cond_5

    .line 90
    .line 91
    const/16 v5, 0xa

    .line 92
    .line 93
    if-ne v3, v5, :cond_4

    .line 94
    .line 95
    goto :goto_2

    .line 96
    :cond_4
    const/4 v4, 0x0

    .line 97
    :cond_5
    :goto_2
    if-nez v4, :cond_6

    .line 98
    .line 99
    goto :goto_3

    .line 100
    :cond_6
    add-int/lit8 v2, v2, 0x1

    .line 101
    .line 102
    goto :goto_1

    .line 103
    :cond_7
    const/4 v1, 0x1

    .line 104
    :goto_3
    if-eqz v1, :cond_8

    .line 105
    .line 106
    goto :goto_4

    .line 107
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    .line 108
    .line 109
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 110
    .line 111
    .line 112
    const-string v1, "Only whitespace, tab, newline and carriage return are allowed as pretty print symbols. Had "

    .line 113
    .line 114
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    iget-object v1, p0, Lkotlinx/serialization/json/JsonBuilder;->〇〇888:Ljava/lang/String;

    .line 118
    .line 119
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    .line 121
    .line 122
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 123
    .line 124
    .line 125
    move-result-object v0

    .line 126
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 127
    .line 128
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 129
    .line 130
    .line 131
    move-result-object v0

    .line 132
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 133
    .line 134
    .line 135
    throw v1

    .line 136
    :cond_9
    :goto_4
    new-instance v0, Lkotlinx/serialization/json/JsonConfiguration;

    .line 137
    .line 138
    iget-boolean v3, p0, Lkotlinx/serialization/json/JsonBuilder;->〇080:Z

    .line 139
    .line 140
    iget-boolean v4, p0, Lkotlinx/serialization/json/JsonBuilder;->〇o〇:Z

    .line 141
    .line 142
    iget-boolean v5, p0, Lkotlinx/serialization/json/JsonBuilder;->O8:Z

    .line 143
    .line 144
    iget-boolean v6, p0, Lkotlinx/serialization/json/JsonBuilder;->Oo08:Z

    .line 145
    .line 146
    iget-boolean v7, p0, Lkotlinx/serialization/json/JsonBuilder;->o〇0:Z

    .line 147
    .line 148
    iget-boolean v8, p0, Lkotlinx/serialization/json/JsonBuilder;->〇o00〇〇Oo:Z

    .line 149
    .line 150
    iget-object v9, p0, Lkotlinx/serialization/json/JsonBuilder;->〇〇888:Ljava/lang/String;

    .line 151
    .line 152
    iget-boolean v10, p0, Lkotlinx/serialization/json/JsonBuilder;->oO80:Z

    .line 153
    .line 154
    iget-boolean v11, p0, Lkotlinx/serialization/json/JsonBuilder;->〇80〇808〇O:Z

    .line 155
    .line 156
    iget-object v12, p0, Lkotlinx/serialization/json/JsonBuilder;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 157
    .line 158
    iget-boolean v13, p0, Lkotlinx/serialization/json/JsonBuilder;->〇8o8o〇:Z

    .line 159
    .line 160
    iget-boolean v14, p0, Lkotlinx/serialization/json/JsonBuilder;->〇O8o08O:Z

    .line 161
    .line 162
    move-object v2, v0

    .line 163
    invoke-direct/range {v2 .. v14}, Lkotlinx/serialization/json/JsonConfiguration;-><init>(ZZZZZZLjava/lang/String;ZZLjava/lang/String;ZZ)V

    .line 164
    .line 165
    .line 166
    return-object v0
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public final 〇o00〇〇Oo()Lkotlinx/serialization/modules/SerializersModule;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lkotlinx/serialization/json/JsonBuilder;->OO0o〇〇:Lkotlinx/serialization/modules/SerializersModule;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final 〇o〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lkotlinx/serialization/json/JsonBuilder;->Oo08:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

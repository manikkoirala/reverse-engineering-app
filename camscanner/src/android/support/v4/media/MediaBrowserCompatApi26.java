// 
// Decompiled by Procyon v0.6.0
// 

package android.support.v4.media;

import android.support.v4.media.session.MediaSessionCompat;
import android.media.browse.MediaBrowser$MediaItem;
import java.util.List;
import androidx.annotation.NonNull;
import android.media.browse.MediaBrowser$SubscriptionCallback;
import android.media.browse.MediaBrowser;
import android.os.Bundle;
import androidx.annotation.RequiresApi;

@RequiresApi(26)
class MediaBrowserCompatApi26
{
    private MediaBrowserCompatApi26() {
    }
    
    static Object createSubscriptionCallback(final SubscriptionCallback subscriptionCallback) {
        return new SubscriptionCallbackProxy(subscriptionCallback);
    }
    
    public static void subscribe(final Object o, final String s, final Bundle bundle, final Object o2) {
        \u3007o\u3007.\u3007080((MediaBrowser)o, s, bundle, (MediaBrowser$SubscriptionCallback)o2);
    }
    
    public static void unsubscribe(final Object o, final String s, final Object o2) {
        \u3007o00\u3007\u3007Oo.\u3007080((MediaBrowser)o, s, (MediaBrowser$SubscriptionCallback)o2);
    }
    
    interface SubscriptionCallback extends MediaBrowserCompatApi21.SubscriptionCallback
    {
        void onChildrenLoaded(@NonNull final String p0, final List<?> p1, @NonNull final Bundle p2);
        
        void onError(@NonNull final String p0, @NonNull final Bundle p1);
    }
    
    static class SubscriptionCallbackProxy<T extends MediaBrowserCompatApi26.SubscriptionCallback> extends MediaBrowserCompatApi21.SubscriptionCallbackProxy<T>
    {
        SubscriptionCallbackProxy(final T t) {
            super(t);
        }
        
        public void onChildrenLoaded(@NonNull final String s, final List<MediaBrowser$MediaItem> list, @NonNull final Bundle bundle) {
            MediaSessionCompat.ensureClassLoader(bundle);
            ((T)super.mSubscriptionCallback).onChildrenLoaded(s, list, bundle);
        }
        
        public void onError(@NonNull final String s, @NonNull final Bundle bundle) {
            MediaSessionCompat.ensureClassLoader(bundle);
            ((T)super.mSubscriptionCallback).onError(s, bundle);
        }
    }
}

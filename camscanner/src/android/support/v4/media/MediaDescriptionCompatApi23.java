// 
// Decompiled by Procyon v0.6.0
// 

package android.support.v4.media;

import android.media.MediaDescription$Builder;
import android.media.MediaDescription;
import android.net.Uri;
import androidx.annotation.RequiresApi;

@RequiresApi(23)
class MediaDescriptionCompatApi23
{
    private MediaDescriptionCompatApi23() {
    }
    
    public static Uri getMediaUri(final Object o) {
        return O8.\u3007080((MediaDescription)o);
    }
    
    static class Builder
    {
        private Builder() {
        }
        
        public static void setMediaUri(final Object o, final Uri uri) {
            Oo08.\u3007080((MediaDescription$Builder)o, uri);
        }
    }
}

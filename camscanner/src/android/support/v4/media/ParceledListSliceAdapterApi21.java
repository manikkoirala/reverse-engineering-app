// 
// Decompiled by Procyon v0.6.0
// 

package android.support.v4.media;

import java.lang.reflect.InvocationTargetException;
import android.media.browse.MediaBrowser$MediaItem;
import java.util.List;
import java.lang.reflect.Constructor;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
class ParceledListSliceAdapterApi21
{
    private static Constructor sConstructor;
    
    static {
        try {
            ParceledListSliceAdapterApi21.sConstructor = Class.forName("android.content.pm.ParceledListSlice").getConstructor(List.class);
            return;
        }
        catch (final NoSuchMethodException ex) {}
        catch (final ClassNotFoundException ex2) {}
        final NoSuchMethodException ex;
        ex.printStackTrace();
    }
    
    private ParceledListSliceAdapterApi21() {
    }
    
    static Object newInstance(List<MediaBrowser$MediaItem> instance) {
        try {
            instance = (InvocationTargetException)ParceledListSliceAdapterApi21.sConstructor.newInstance(instance);
            return instance;
        }
        catch (final InvocationTargetException instance) {}
        catch (final IllegalAccessException instance) {}
        catch (final InstantiationException ex) {}
        instance.printStackTrace();
        instance = null;
        return instance;
    }
}

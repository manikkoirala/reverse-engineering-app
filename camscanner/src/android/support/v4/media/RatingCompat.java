// 
// Decompiled by Procyon v0.6.0
// 

package android.support.v4.media;

import androidx.annotation.RestrictTo;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.media.Rating;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

public final class RatingCompat implements Parcelable
{
    public static final Parcelable$Creator<RatingCompat> CREATOR;
    public static final int RATING_3_STARS = 3;
    public static final int RATING_4_STARS = 4;
    public static final int RATING_5_STARS = 5;
    public static final int RATING_HEART = 1;
    public static final int RATING_NONE = 0;
    private static final float RATING_NOT_RATED = -1.0f;
    public static final int RATING_PERCENTAGE = 6;
    public static final int RATING_THUMB_UP_DOWN = 2;
    private static final String TAG = "Rating";
    private Object mRatingObj;
    private final int mRatingStyle;
    private final float mRatingValue;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<RatingCompat>() {
            public RatingCompat createFromParcel(final Parcel parcel) {
                return new RatingCompat(parcel.readInt(), parcel.readFloat());
            }
            
            public RatingCompat[] newArray(final int n) {
                return new RatingCompat[n];
            }
        };
    }
    
    RatingCompat(final int mRatingStyle, final float mRatingValue) {
        this.mRatingStyle = mRatingStyle;
        this.mRatingValue = mRatingValue;
    }
    
    public static RatingCompat fromRating(final Object mRatingObj) {
        RatingCompat ratingCompat = null;
        if (mRatingObj != null) {
            final Rating rating = (Rating)mRatingObj;
            final int ratingStyle = rating.getRatingStyle();
            if (rating.isRated()) {
                switch (ratingStyle) {
                    default: {
                        return null;
                    }
                    case 6: {
                        ratingCompat = newPercentageRating(rating.getPercentRating());
                        break;
                    }
                    case 3:
                    case 4:
                    case 5: {
                        ratingCompat = newStarRating(ratingStyle, rating.getStarRating());
                        break;
                    }
                    case 2: {
                        ratingCompat = newThumbRating(rating.isThumbUp());
                        break;
                    }
                    case 1: {
                        ratingCompat = newHeartRating(rating.hasHeart());
                        break;
                    }
                }
            }
            else {
                ratingCompat = newUnratedRating(ratingStyle);
            }
            ratingCompat.mRatingObj = mRatingObj;
        }
        return ratingCompat;
    }
    
    public static RatingCompat newHeartRating(final boolean b) {
        float n;
        if (b) {
            n = 1.0f;
        }
        else {
            n = 0.0f;
        }
        return new RatingCompat(1, n);
    }
    
    public static RatingCompat newPercentageRating(final float n) {
        if (n >= 0.0f && n <= 100.0f) {
            return new RatingCompat(6, n);
        }
        return null;
    }
    
    public static RatingCompat newStarRating(final int i, final float n) {
        float n2;
        if (i != 3) {
            if (i != 4) {
                if (i != 5) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Invalid rating style (");
                    sb.append(i);
                    sb.append(") for a star rating");
                    return null;
                }
                n2 = 5.0f;
            }
            else {
                n2 = 4.0f;
            }
        }
        else {
            n2 = 3.0f;
        }
        if (n >= 0.0f && n <= n2) {
            return new RatingCompat(i, n);
        }
        return null;
    }
    
    public static RatingCompat newThumbRating(final boolean b) {
        float n;
        if (b) {
            n = 1.0f;
        }
        else {
            n = 0.0f;
        }
        return new RatingCompat(2, n);
    }
    
    public static RatingCompat newUnratedRating(final int n) {
        switch (n) {
            default: {
                return null;
            }
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6: {
                return new RatingCompat(n, -1.0f);
            }
        }
    }
    
    public int describeContents() {
        return this.mRatingStyle;
    }
    
    public float getPercentRating() {
        if (this.mRatingStyle == 6 && this.isRated()) {
            return this.mRatingValue;
        }
        return -1.0f;
    }
    
    public Object getRating() {
        if (this.mRatingObj == null) {
            if (this.isRated()) {
                final int mRatingStyle = this.mRatingStyle;
                switch (mRatingStyle) {
                    default: {
                        return null;
                    }
                    case 6: {
                        this.mRatingObj = Rating.newPercentageRating(this.getPercentRating());
                        break;
                    }
                    case 3:
                    case 4:
                    case 5: {
                        this.mRatingObj = Rating.newStarRating(mRatingStyle, this.getStarRating());
                        break;
                    }
                    case 2: {
                        this.mRatingObj = Rating.newThumbRating(this.isThumbUp());
                        break;
                    }
                    case 1: {
                        this.mRatingObj = Rating.newHeartRating(this.hasHeart());
                        break;
                    }
                }
            }
            else {
                this.mRatingObj = Rating.newUnratedRating(this.mRatingStyle);
            }
        }
        return this.mRatingObj;
    }
    
    public int getRatingStyle() {
        return this.mRatingStyle;
    }
    
    public float getStarRating() {
        final int mRatingStyle = this.mRatingStyle;
        if (mRatingStyle == 3 || mRatingStyle == 4 || mRatingStyle == 5) {
            if (this.isRated()) {
                return this.mRatingValue;
            }
        }
        return -1.0f;
    }
    
    public boolean hasHeart() {
        final int mRatingStyle = this.mRatingStyle;
        boolean b = false;
        if (mRatingStyle != 1) {
            return false;
        }
        if (this.mRatingValue == 1.0f) {
            b = true;
        }
        return b;
    }
    
    public boolean isRated() {
        return this.mRatingValue >= 0.0f;
    }
    
    public boolean isThumbUp() {
        final int mRatingStyle = this.mRatingStyle;
        boolean b = false;
        if (mRatingStyle != 2) {
            return false;
        }
        if (this.mRatingValue == 1.0f) {
            b = true;
        }
        return b;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Rating:style=");
        sb.append(this.mRatingStyle);
        sb.append(" rating=");
        final float mRatingValue = this.mRatingValue;
        String value;
        if (mRatingValue < 0.0f) {
            value = "unrated";
        }
        else {
            value = String.valueOf(mRatingValue);
        }
        sb.append(value);
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeInt(this.mRatingStyle);
        parcel.writeFloat(this.mRatingValue);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public @interface StarStyle {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public @interface Style {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package android.support.v4.media.session;

import android.os.IInterface;
import android.os.BaseBundle;
import android.support.v4.media.RatingCompat;
import android.net.Uri;
import android.os.IBinder;
import androidx.core.app.BundleCompat;
import java.util.Iterator;
import android.os.Parcelable;
import java.util.ArrayList;
import androidx.annotation.GuardedBy;
import java.util.HashMap;
import androidx.annotation.RequiresApi;
import java.lang.ref.WeakReference;
import android.os.Looper;
import android.os.Message;
import android.os.IBinder$DeathRecipient;
import android.text.TextUtils;
import android.os.ResultReceiver;
import android.os.Handler;
import androidx.annotation.Nullable;
import android.app.PendingIntent;
import java.util.List;
import android.support.v4.media.MediaMetadataCompat;
import android.view.KeyEvent;
import android.support.v4.media.MediaDescriptionCompat;
import android.os.Bundle;
import androidx.core.app.ComponentActivity;
import android.app.Activity;
import android.os.RemoteException;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.content.Context;
import java.util.HashSet;
import androidx.annotation.RestrictTo;

public final class MediaControllerCompat
{
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final String COMMAND_ADD_QUEUE_ITEM = "android.support.v4.media.session.command.ADD_QUEUE_ITEM";
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final String COMMAND_ADD_QUEUE_ITEM_AT = "android.support.v4.media.session.command.ADD_QUEUE_ITEM_AT";
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final String COMMAND_ARGUMENT_INDEX = "android.support.v4.media.session.command.ARGUMENT_INDEX";
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final String COMMAND_ARGUMENT_MEDIA_DESCRIPTION = "android.support.v4.media.session.command.ARGUMENT_MEDIA_DESCRIPTION";
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final String COMMAND_GET_EXTRA_BINDER = "android.support.v4.media.session.command.GET_EXTRA_BINDER";
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final String COMMAND_REMOVE_QUEUE_ITEM = "android.support.v4.media.session.command.REMOVE_QUEUE_ITEM";
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final String COMMAND_REMOVE_QUEUE_ITEM_AT = "android.support.v4.media.session.command.REMOVE_QUEUE_ITEM_AT";
    static final String TAG = "MediaControllerCompat";
    private final MediaControllerImpl mImpl;
    private final HashSet<Callback> mRegisteredCallbacks;
    private final MediaSessionCompat.Token mToken;
    
    public MediaControllerCompat(final Context context, @NonNull final MediaSessionCompat.Token mToken) throws RemoteException {
        this.mRegisteredCallbacks = new HashSet<Callback>();
        if (mToken != null) {
            this.mToken = mToken;
            final int sdk_INT = Build$VERSION.SDK_INT;
            if (sdk_INT >= 24) {
                this.mImpl = (MediaControllerImpl)new MediaControllerImplApi24(context, mToken);
            }
            else if (sdk_INT >= 23) {
                this.mImpl = (MediaControllerImpl)new MediaControllerImplApi23(context, mToken);
            }
            else {
                this.mImpl = (MediaControllerImpl)new MediaControllerImplApi21(context, mToken);
            }
            return;
        }
        throw new IllegalArgumentException("sessionToken must not be null");
    }
    
    public MediaControllerCompat(final Context context, @NonNull final MediaSessionCompat mediaSessionCompat) {
        this.mRegisteredCallbacks = new HashSet<Callback>();
        if (mediaSessionCompat != null) {
            final MediaSessionCompat.Token sessionToken = mediaSessionCompat.getSessionToken();
            this.mToken = sessionToken;
            Object mImpl;
            try {
                final int sdk_INT = Build$VERSION.SDK_INT;
                if (sdk_INT >= 24) {
                    mImpl = new MediaControllerImplApi24(context, sessionToken);
                }
                else if (sdk_INT >= 23) {
                    mImpl = new MediaControllerImplApi23(context, sessionToken);
                }
                else {
                    mImpl = new MediaControllerImplApi21(context, sessionToken);
                }
            }
            catch (final RemoteException ex) {
                mImpl = null;
            }
            this.mImpl = (MediaControllerImpl)mImpl;
            return;
        }
        throw new IllegalArgumentException("session must not be null");
    }
    
    public static MediaControllerCompat getMediaController(@NonNull final Activity activity) {
        final boolean b = activity instanceof ComponentActivity;
        final MediaControllerCompat mediaControllerCompat = null;
        if (b) {
            final MediaControllerExtraData mediaControllerExtraData = ((ComponentActivity)activity).getExtraData(MediaControllerExtraData.class);
            MediaControllerCompat mediaController = mediaControllerCompat;
            if (mediaControllerExtraData != null) {
                mediaController = mediaControllerExtraData.getMediaController();
            }
            return mediaController;
        }
        final Object mediaController2 = MediaControllerCompatApi21.getMediaController(activity);
        if (mediaController2 == null) {
            return null;
        }
        final Object sessionToken = MediaControllerCompatApi21.getSessionToken(mediaController2);
        try {
            return new MediaControllerCompat((Context)activity, MediaSessionCompat.Token.fromToken(sessionToken));
        }
        catch (final RemoteException ex) {
            return null;
        }
    }
    
    public static void setMediaController(@NonNull final Activity activity, final MediaControllerCompat mediaControllerCompat) {
        if (activity instanceof ComponentActivity) {
            ((ComponentActivity)activity).putExtraData((ComponentActivity.ExtraData)new MediaControllerExtraData(mediaControllerCompat));
        }
        Object fromToken;
        if (mediaControllerCompat != null) {
            fromToken = MediaControllerCompatApi21.fromToken((Context)activity, mediaControllerCompat.getSessionToken().getToken());
        }
        else {
            fromToken = null;
        }
        MediaControllerCompatApi21.setMediaController(activity, fromToken);
    }
    
    static void validateCustomAction(final String str, final Bundle bundle) {
        if (str == null) {
            return;
        }
        if (str.equals("android.support.v4.media.session.action.FOLLOW") || str.equals("android.support.v4.media.session.action.UNFOLLOW")) {
            if (bundle == null || !((BaseBundle)bundle).containsKey("android.support.v4.media.session.ARGUMENT_MEDIA_ATTRIBUTE")) {
                final StringBuilder sb = new StringBuilder();
                sb.append("An extra field android.support.v4.media.session.ARGUMENT_MEDIA_ATTRIBUTE is required for this action ");
                sb.append(str);
                sb.append(".");
                throw new IllegalArgumentException(sb.toString());
            }
        }
    }
    
    public void addQueueItem(final MediaDescriptionCompat mediaDescriptionCompat) {
        this.mImpl.addQueueItem(mediaDescriptionCompat);
    }
    
    public void addQueueItem(final MediaDescriptionCompat mediaDescriptionCompat, final int n) {
        this.mImpl.addQueueItem(mediaDescriptionCompat, n);
    }
    
    public void adjustVolume(final int n, final int n2) {
        this.mImpl.adjustVolume(n, n2);
    }
    
    public boolean dispatchMediaButtonEvent(final KeyEvent keyEvent) {
        if (keyEvent != null) {
            return this.mImpl.dispatchMediaButtonEvent(keyEvent);
        }
        throw new IllegalArgumentException("KeyEvent may not be null");
    }
    
    public Bundle getExtras() {
        return this.mImpl.getExtras();
    }
    
    public long getFlags() {
        return this.mImpl.getFlags();
    }
    
    public Object getMediaController() {
        return this.mImpl.getMediaController();
    }
    
    public MediaMetadataCompat getMetadata() {
        return this.mImpl.getMetadata();
    }
    
    public String getPackageName() {
        return this.mImpl.getPackageName();
    }
    
    public PlaybackInfo getPlaybackInfo() {
        return this.mImpl.getPlaybackInfo();
    }
    
    public PlaybackStateCompat getPlaybackState() {
        return this.mImpl.getPlaybackState();
    }
    
    public List<MediaSessionCompat.QueueItem> getQueue() {
        return this.mImpl.getQueue();
    }
    
    public CharSequence getQueueTitle() {
        return this.mImpl.getQueueTitle();
    }
    
    public int getRatingType() {
        return this.mImpl.getRatingType();
    }
    
    public int getRepeatMode() {
        return this.mImpl.getRepeatMode();
    }
    
    public PendingIntent getSessionActivity() {
        return this.mImpl.getSessionActivity();
    }
    
    public MediaSessionCompat.Token getSessionToken() {
        return this.mToken;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public Bundle getSessionToken2Bundle() {
        return this.mToken.getSessionToken2Bundle();
    }
    
    public int getShuffleMode() {
        return this.mImpl.getShuffleMode();
    }
    
    public TransportControls getTransportControls() {
        return this.mImpl.getTransportControls();
    }
    
    public boolean isCaptioningEnabled() {
        return this.mImpl.isCaptioningEnabled();
    }
    
    public boolean isSessionReady() {
        return this.mImpl.isSessionReady();
    }
    
    public void registerCallback(@NonNull final Callback callback) {
        this.registerCallback(callback, null);
    }
    
    public void registerCallback(@NonNull final Callback e, final Handler handler) {
        if (e != null) {
            Handler handler2;
            if ((handler2 = handler) == null) {
                handler2 = new Handler();
            }
            e.setHandler(handler2);
            this.mImpl.registerCallback(e, handler2);
            this.mRegisteredCallbacks.add(e);
            return;
        }
        throw new IllegalArgumentException("callback must not be null");
    }
    
    public void removeQueueItem(final MediaDescriptionCompat mediaDescriptionCompat) {
        this.mImpl.removeQueueItem(mediaDescriptionCompat);
    }
    
    @Deprecated
    public void removeQueueItemAt(final int n) {
        final List<MediaSessionCompat.QueueItem> queue = this.getQueue();
        if (queue != null && n >= 0 && n < queue.size()) {
            final MediaSessionCompat.QueueItem queueItem = queue.get(n);
            if (queueItem != null) {
                this.removeQueueItem(queueItem.getDescription());
            }
        }
    }
    
    public void sendCommand(@NonNull final String s, final Bundle bundle, final ResultReceiver resultReceiver) {
        if (!TextUtils.isEmpty((CharSequence)s)) {
            this.mImpl.sendCommand(s, bundle, resultReceiver);
            return;
        }
        throw new IllegalArgumentException("command must neither be null nor empty");
    }
    
    public void setVolumeTo(final int n, final int n2) {
        this.mImpl.setVolumeTo(n, n2);
    }
    
    public void unregisterCallback(@NonNull final Callback o) {
        if (o != null) {
            try {
                this.mRegisteredCallbacks.remove(o);
                this.mImpl.unregisterCallback(o);
                return;
            }
            finally {
                o.setHandler(null);
            }
        }
        throw new IllegalArgumentException("callback must not be null");
    }
    
    public abstract static class Callback implements IBinder$DeathRecipient
    {
        final Object mCallbackObj;
        MessageHandler mHandler;
        IMediaControllerCallback mIControllerCallback;
        
        public Callback() {
            this.mCallbackObj = MediaControllerCompatApi21.createCallback((MediaControllerCompatApi21.Callback)new StubApi21(this));
        }
        
        public void binderDied() {
            this.postToHandler(8, null, null);
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY })
        public IMediaControllerCallback getIControllerCallback() {
            return this.mIControllerCallback;
        }
        
        public void onAudioInfoChanged(final PlaybackInfo playbackInfo) {
        }
        
        public void onCaptioningEnabledChanged(final boolean b) {
        }
        
        public void onExtrasChanged(final Bundle bundle) {
        }
        
        public void onMetadataChanged(final MediaMetadataCompat mediaMetadataCompat) {
        }
        
        public void onPlaybackStateChanged(final PlaybackStateCompat playbackStateCompat) {
        }
        
        public void onQueueChanged(final List<MediaSessionCompat.QueueItem> list) {
        }
        
        public void onQueueTitleChanged(final CharSequence charSequence) {
        }
        
        public void onRepeatModeChanged(final int n) {
        }
        
        public void onSessionDestroyed() {
        }
        
        public void onSessionEvent(final String s, final Bundle bundle) {
        }
        
        public void onSessionReady() {
        }
        
        public void onShuffleModeChanged(final int n) {
        }
        
        void postToHandler(final int n, final Object o, final Bundle data) {
            final MessageHandler mHandler = this.mHandler;
            if (mHandler != null) {
                final Message obtainMessage = mHandler.obtainMessage(n, o);
                obtainMessage.setData(data);
                obtainMessage.sendToTarget();
            }
        }
        
        void setHandler(final Handler handler) {
            if (handler == null) {
                final MessageHandler mHandler = this.mHandler;
                if (mHandler != null) {
                    mHandler.mRegistered = false;
                    mHandler.removeCallbacksAndMessages((Object)null);
                    this.mHandler = null;
                }
            }
            else {
                final MessageHandler mHandler2 = new MessageHandler(handler.getLooper());
                this.mHandler = mHandler2;
                mHandler2.mRegistered = true;
            }
        }
        
        private class MessageHandler extends Handler
        {
            private static final int MSG_DESTROYED = 8;
            private static final int MSG_EVENT = 1;
            private static final int MSG_SESSION_READY = 13;
            private static final int MSG_UPDATE_CAPTIONING_ENABLED = 11;
            private static final int MSG_UPDATE_EXTRAS = 7;
            private static final int MSG_UPDATE_METADATA = 3;
            private static final int MSG_UPDATE_PLAYBACK_STATE = 2;
            private static final int MSG_UPDATE_QUEUE = 5;
            private static final int MSG_UPDATE_QUEUE_TITLE = 6;
            private static final int MSG_UPDATE_REPEAT_MODE = 9;
            private static final int MSG_UPDATE_SHUFFLE_MODE = 12;
            private static final int MSG_UPDATE_VOLUME = 4;
            boolean mRegistered;
            final Callback this$0;
            
            MessageHandler(final Callback this$0, final Looper looper) {
                this.this$0 = this$0;
                super(looper);
                this.mRegistered = false;
            }
            
            public void handleMessage(final Message message) {
                if (!this.mRegistered) {
                    return;
                }
                switch (message.what) {
                    case 13: {
                        this.this$0.onSessionReady();
                        break;
                    }
                    case 12: {
                        this.this$0.onShuffleModeChanged((int)message.obj);
                        break;
                    }
                    case 11: {
                        this.this$0.onCaptioningEnabledChanged((boolean)message.obj);
                        break;
                    }
                    case 9: {
                        this.this$0.onRepeatModeChanged((int)message.obj);
                        break;
                    }
                    case 8: {
                        this.this$0.onSessionDestroyed();
                        break;
                    }
                    case 7: {
                        final Bundle bundle = (Bundle)message.obj;
                        MediaSessionCompat.ensureClassLoader(bundle);
                        this.this$0.onExtrasChanged(bundle);
                        break;
                    }
                    case 6: {
                        this.this$0.onQueueTitleChanged((CharSequence)message.obj);
                        break;
                    }
                    case 5: {
                        this.this$0.onQueueChanged((List<MediaSessionCompat.QueueItem>)message.obj);
                        break;
                    }
                    case 4: {
                        this.this$0.onAudioInfoChanged((PlaybackInfo)message.obj);
                        break;
                    }
                    case 3: {
                        this.this$0.onMetadataChanged((MediaMetadataCompat)message.obj);
                        break;
                    }
                    case 2: {
                        this.this$0.onPlaybackStateChanged((PlaybackStateCompat)message.obj);
                        break;
                    }
                    case 1: {
                        final Bundle data = message.getData();
                        MediaSessionCompat.ensureClassLoader(data);
                        this.this$0.onSessionEvent((String)message.obj, data);
                        break;
                    }
                }
            }
        }
        
        private static class StubApi21 implements MediaControllerCompatApi21.Callback
        {
            private final WeakReference<MediaControllerCompat.Callback> mCallback;
            
            StubApi21(final MediaControllerCompat.Callback referent) {
                this.mCallback = new WeakReference<MediaControllerCompat.Callback>(referent);
            }
            
            @Override
            public void onAudioInfoChanged(final int n, final int n2, final int n3, final int n4, final int n5) {
                final MediaControllerCompat.Callback callback = this.mCallback.get();
                if (callback != null) {
                    callback.onAudioInfoChanged(new MediaControllerCompat.PlaybackInfo(n, n2, n3, n4, n5));
                }
            }
            
            @Override
            public void onExtrasChanged(final Bundle bundle) {
                final MediaControllerCompat.Callback callback = this.mCallback.get();
                if (callback != null) {
                    callback.onExtrasChanged(bundle);
                }
            }
            
            @Override
            public void onMetadataChanged(final Object o) {
                final MediaControllerCompat.Callback callback = this.mCallback.get();
                if (callback != null) {
                    callback.onMetadataChanged(MediaMetadataCompat.fromMediaMetadata(o));
                }
            }
            
            @Override
            public void onPlaybackStateChanged(final Object o) {
                final MediaControllerCompat.Callback callback = this.mCallback.get();
                if (callback != null) {
                    if (callback.mIControllerCallback == null) {
                        callback.onPlaybackStateChanged(PlaybackStateCompat.fromPlaybackState(o));
                    }
                }
            }
            
            @Override
            public void onQueueChanged(final List<?> list) {
                final MediaControllerCompat.Callback callback = this.mCallback.get();
                if (callback != null) {
                    callback.onQueueChanged(MediaSessionCompat.QueueItem.fromQueueItemList(list));
                }
            }
            
            @Override
            public void onQueueTitleChanged(final CharSequence charSequence) {
                final MediaControllerCompat.Callback callback = this.mCallback.get();
                if (callback != null) {
                    callback.onQueueTitleChanged(charSequence);
                }
            }
            
            @Override
            public void onSessionDestroyed() {
                final MediaControllerCompat.Callback callback = this.mCallback.get();
                if (callback != null) {
                    callback.onSessionDestroyed();
                }
            }
            
            @Override
            public void onSessionEvent(final String s, final Bundle bundle) {
                final MediaControllerCompat.Callback callback = this.mCallback.get();
                if (callback != null) {
                    if (callback.mIControllerCallback == null || Build$VERSION.SDK_INT >= 23) {
                        callback.onSessionEvent(s, bundle);
                    }
                }
            }
        }
        
        private static class StubCompat extends Stub
        {
            private final WeakReference<Callback> mCallback;
            
            StubCompat(final Callback referent) {
                this.mCallback = new WeakReference<Callback>(referent);
            }
            
            public void onCaptioningEnabledChanged(final boolean b) throws RemoteException {
                final Callback callback = this.mCallback.get();
                if (callback != null) {
                    callback.postToHandler(11, b, null);
                }
            }
            
            public void onEvent(final String s, final Bundle bundle) throws RemoteException {
                final Callback callback = this.mCallback.get();
                if (callback != null) {
                    callback.postToHandler(1, s, bundle);
                }
            }
            
            public void onExtrasChanged(final Bundle bundle) throws RemoteException {
                final Callback callback = this.mCallback.get();
                if (callback != null) {
                    callback.postToHandler(7, bundle, null);
                }
            }
            
            public void onMetadataChanged(final MediaMetadataCompat mediaMetadataCompat) throws RemoteException {
                final Callback callback = this.mCallback.get();
                if (callback != null) {
                    callback.postToHandler(3, mediaMetadataCompat, null);
                }
            }
            
            public void onPlaybackStateChanged(final PlaybackStateCompat playbackStateCompat) throws RemoteException {
                final Callback callback = this.mCallback.get();
                if (callback != null) {
                    callback.postToHandler(2, playbackStateCompat, null);
                }
            }
            
            public void onQueueChanged(final List<MediaSessionCompat.QueueItem> list) throws RemoteException {
                final Callback callback = this.mCallback.get();
                if (callback != null) {
                    callback.postToHandler(5, list, null);
                }
            }
            
            public void onQueueTitleChanged(final CharSequence charSequence) throws RemoteException {
                final Callback callback = this.mCallback.get();
                if (callback != null) {
                    callback.postToHandler(6, charSequence, null);
                }
            }
            
            public void onRepeatModeChanged(final int i) throws RemoteException {
                final Callback callback = this.mCallback.get();
                if (callback != null) {
                    callback.postToHandler(9, i, null);
                }
            }
            
            public void onSessionDestroyed() throws RemoteException {
                final Callback callback = this.mCallback.get();
                if (callback != null) {
                    callback.postToHandler(8, null, null);
                }
            }
            
            public void onSessionReady() throws RemoteException {
                final Callback callback = this.mCallback.get();
                if (callback != null) {
                    callback.postToHandler(13, null, null);
                }
            }
            
            public void onShuffleModeChanged(final int i) throws RemoteException {
                final Callback callback = this.mCallback.get();
                if (callback != null) {
                    callback.postToHandler(12, i, null);
                }
            }
            
            public void onShuffleModeChangedRemoved(final boolean b) throws RemoteException {
            }
            
            public void onVolumeInfoChanged(final ParcelableVolumeInfo parcelableVolumeInfo) throws RemoteException {
                final Callback callback = this.mCallback.get();
                if (callback != null) {
                    PlaybackInfo playbackInfo;
                    if (parcelableVolumeInfo != null) {
                        playbackInfo = new PlaybackInfo(parcelableVolumeInfo.volumeType, parcelableVolumeInfo.audioStream, parcelableVolumeInfo.controlType, parcelableVolumeInfo.maxVolume, parcelableVolumeInfo.currentVolume);
                    }
                    else {
                        playbackInfo = null;
                    }
                    callback.postToHandler(4, playbackInfo, null);
                }
            }
        }
    }
    
    private static class MediaControllerExtraData extends ExtraData
    {
        private final MediaControllerCompat mMediaController;
        
        MediaControllerExtraData(final MediaControllerCompat mMediaController) {
            this.mMediaController = mMediaController;
        }
        
        MediaControllerCompat getMediaController() {
            return this.mMediaController;
        }
    }
    
    interface MediaControllerImpl
    {
        void addQueueItem(final MediaDescriptionCompat p0);
        
        void addQueueItem(final MediaDescriptionCompat p0, final int p1);
        
        void adjustVolume(final int p0, final int p1);
        
        boolean dispatchMediaButtonEvent(final KeyEvent p0);
        
        Bundle getExtras();
        
        long getFlags();
        
        Object getMediaController();
        
        MediaMetadataCompat getMetadata();
        
        String getPackageName();
        
        PlaybackInfo getPlaybackInfo();
        
        PlaybackStateCompat getPlaybackState();
        
        List<MediaSessionCompat.QueueItem> getQueue();
        
        CharSequence getQueueTitle();
        
        int getRatingType();
        
        int getRepeatMode();
        
        PendingIntent getSessionActivity();
        
        int getShuffleMode();
        
        TransportControls getTransportControls();
        
        boolean isCaptioningEnabled();
        
        boolean isSessionReady();
        
        void registerCallback(final Callback p0, final Handler p1);
        
        void removeQueueItem(final MediaDescriptionCompat p0);
        
        void sendCommand(final String p0, final Bundle p1, final ResultReceiver p2);
        
        void setVolumeTo(final int p0, final int p1);
        
        void unregisterCallback(final Callback p0);
    }
    
    @RequiresApi(21)
    static class MediaControllerImplApi21 implements MediaControllerImpl
    {
        private HashMap<Callback, ExtraCallback> mCallbackMap;
        protected final Object mControllerObj;
        final Object mLock;
        @GuardedBy("mLock")
        private final List<Callback> mPendingCallbacks;
        final MediaSessionCompat.Token mSessionToken;
        
        public MediaControllerImplApi21(final Context context, final MediaSessionCompat.Token mSessionToken) throws RemoteException {
            this.mLock = new Object();
            this.mPendingCallbacks = new ArrayList<Callback>();
            this.mCallbackMap = new HashMap<Callback, ExtraCallback>();
            this.mSessionToken = mSessionToken;
            final Object fromToken = MediaControllerCompatApi21.fromToken(context, mSessionToken.getToken());
            this.mControllerObj = fromToken;
            if (fromToken != null) {
                if (mSessionToken.getExtraBinder() == null) {
                    this.requestExtraBinder();
                }
                return;
            }
            throw new RemoteException();
        }
        
        private void requestExtraBinder() {
            this.sendCommand("android.support.v4.media.session.command.GET_EXTRA_BINDER", null, new ExtraBinderRequestResultReceiver(this));
        }
        
        @Override
        public void addQueueItem(final MediaDescriptionCompat mediaDescriptionCompat) {
            if ((this.getFlags() & 0x4L) != 0x0L) {
                final Bundle bundle = new Bundle();
                bundle.putParcelable("android.support.v4.media.session.command.ARGUMENT_MEDIA_DESCRIPTION", (Parcelable)mediaDescriptionCompat);
                this.sendCommand("android.support.v4.media.session.command.ADD_QUEUE_ITEM", bundle, null);
                return;
            }
            throw new UnsupportedOperationException("This session doesn't support queue management operations");
        }
        
        @Override
        public void addQueueItem(final MediaDescriptionCompat mediaDescriptionCompat, final int n) {
            if ((this.getFlags() & 0x4L) != 0x0L) {
                final Bundle bundle = new Bundle();
                bundle.putParcelable("android.support.v4.media.session.command.ARGUMENT_MEDIA_DESCRIPTION", (Parcelable)mediaDescriptionCompat);
                ((BaseBundle)bundle).putInt("android.support.v4.media.session.command.ARGUMENT_INDEX", n);
                this.sendCommand("android.support.v4.media.session.command.ADD_QUEUE_ITEM_AT", bundle, null);
                return;
            }
            throw new UnsupportedOperationException("This session doesn't support queue management operations");
        }
        
        @Override
        public void adjustVolume(final int n, final int n2) {
            MediaControllerCompatApi21.adjustVolume(this.mControllerObj, n, n2);
        }
        
        @Override
        public boolean dispatchMediaButtonEvent(final KeyEvent keyEvent) {
            return MediaControllerCompatApi21.dispatchMediaButtonEvent(this.mControllerObj, keyEvent);
        }
        
        @Override
        public Bundle getExtras() {
            return MediaControllerCompatApi21.getExtras(this.mControllerObj);
        }
        
        @Override
        public long getFlags() {
            return MediaControllerCompatApi21.getFlags(this.mControllerObj);
        }
        
        @Override
        public Object getMediaController() {
            return this.mControllerObj;
        }
        
        @Override
        public MediaMetadataCompat getMetadata() {
            final Object metadata = MediaControllerCompatApi21.getMetadata(this.mControllerObj);
            MediaMetadataCompat fromMediaMetadata;
            if (metadata != null) {
                fromMediaMetadata = MediaMetadataCompat.fromMediaMetadata(metadata);
            }
            else {
                fromMediaMetadata = null;
            }
            return fromMediaMetadata;
        }
        
        @Override
        public String getPackageName() {
            return MediaControllerCompatApi21.getPackageName(this.mControllerObj);
        }
        
        @Override
        public PlaybackInfo getPlaybackInfo() {
            final Object playbackInfo = MediaControllerCompatApi21.getPlaybackInfo(this.mControllerObj);
            Object o;
            if (playbackInfo != null) {
                o = new PlaybackInfo(MediaControllerCompatApi21.PlaybackInfo.getPlaybackType(playbackInfo), MediaControllerCompatApi21.PlaybackInfo.getLegacyAudioStream(playbackInfo), MediaControllerCompatApi21.PlaybackInfo.getVolumeControl(playbackInfo), MediaControllerCompatApi21.PlaybackInfo.getMaxVolume(playbackInfo), MediaControllerCompatApi21.PlaybackInfo.getCurrentVolume(playbackInfo));
            }
            else {
                o = null;
            }
            return (PlaybackInfo)o;
        }
        
        @Override
        public PlaybackStateCompat getPlaybackState() {
            if (this.mSessionToken.getExtraBinder() != null) {
                try {
                    return this.mSessionToken.getExtraBinder().getPlaybackState();
                }
                catch (final RemoteException ex) {}
            }
            final Object playbackState = MediaControllerCompatApi21.getPlaybackState(this.mControllerObj);
            PlaybackStateCompat fromPlaybackState;
            if (playbackState != null) {
                fromPlaybackState = PlaybackStateCompat.fromPlaybackState(playbackState);
            }
            else {
                fromPlaybackState = null;
            }
            return fromPlaybackState;
        }
        
        @Override
        public List<MediaSessionCompat.QueueItem> getQueue() {
            final List<Object> queue = MediaControllerCompatApi21.getQueue(this.mControllerObj);
            List<MediaSessionCompat.QueueItem> fromQueueItemList;
            if (queue != null) {
                fromQueueItemList = MediaSessionCompat.QueueItem.fromQueueItemList(queue);
            }
            else {
                fromQueueItemList = null;
            }
            return fromQueueItemList;
        }
        
        @Override
        public CharSequence getQueueTitle() {
            return MediaControllerCompatApi21.getQueueTitle(this.mControllerObj);
        }
        
        @Override
        public int getRatingType() {
            Label_0033: {
                if (Build$VERSION.SDK_INT >= 22 || this.mSessionToken.getExtraBinder() == null) {
                    break Label_0033;
                }
                try {
                    return this.mSessionToken.getExtraBinder().getRatingType();
                    return MediaControllerCompatApi21.getRatingType(this.mControllerObj);
                }
                catch (final RemoteException ex) {
                    return MediaControllerCompatApi21.getRatingType(this.mControllerObj);
                }
            }
        }
        
        @Override
        public int getRepeatMode() {
            if (this.mSessionToken.getExtraBinder() == null) {
                return -1;
            }
            try {
                return this.mSessionToken.getExtraBinder().getRepeatMode();
            }
            catch (final RemoteException ex) {
                return -1;
            }
        }
        
        @Override
        public PendingIntent getSessionActivity() {
            return MediaControllerCompatApi21.getSessionActivity(this.mControllerObj);
        }
        
        @Override
        public int getShuffleMode() {
            if (this.mSessionToken.getExtraBinder() == null) {
                return -1;
            }
            try {
                return this.mSessionToken.getExtraBinder().getShuffleMode();
            }
            catch (final RemoteException ex) {
                return -1;
            }
        }
        
        @Override
        public TransportControls getTransportControls() {
            final Object transportControls = MediaControllerCompatApi21.getTransportControls(this.mControllerObj);
            TransportControls transportControls2;
            if (transportControls != null) {
                transportControls2 = new TransportControlsApi21(transportControls);
            }
            else {
                transportControls2 = null;
            }
            return transportControls2;
        }
        
        @Override
        public boolean isCaptioningEnabled() {
            if (this.mSessionToken.getExtraBinder() == null) {
                return false;
            }
            try {
                return this.mSessionToken.getExtraBinder().isCaptioningEnabled();
            }
            catch (final RemoteException ex) {
                return false;
            }
        }
        
        @Override
        public boolean isSessionReady() {
            return this.mSessionToken.getExtraBinder() != null;
        }
        
        @GuardedBy("mLock")
        void processPendingCallbacksLocked() {
            if (this.mSessionToken.getExtraBinder() == null) {
                return;
            }
            final Iterator<Callback> iterator = this.mPendingCallbacks.iterator();
        Label_0088_Outer:
            while (true) {
                while (true) {
                    if (!iterator.hasNext()) {
                        break Label_0088;
                    }
                    final Callback key = iterator.next();
                    final ExtraCallback extraCallback = new ExtraCallback(key);
                    this.mCallbackMap.put(key, extraCallback);
                    key.mIControllerCallback = extraCallback;
                    try {
                        this.mSessionToken.getExtraBinder().registerCallbackListener(extraCallback);
                        key.postToHandler(13, null, null);
                        continue Label_0088_Outer;
                        this.mPendingCallbacks.clear();
                    }
                    catch (final RemoteException ex) {
                        continue;
                    }
                    break;
                }
            }
        }
        
        @Override
        public final void registerCallback(final Callback p0, final Handler p1) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: getfield        android/support/v4/media/session/MediaControllerCompat$MediaControllerImplApi21.mControllerObj:Ljava/lang/Object;
            //     4: aload_1        
            //     5: getfield        android/support/v4/media/session/MediaControllerCompat$Callback.mCallbackObj:Ljava/lang/Object;
            //     8: aload_2        
            //     9: invokestatic    android/support/v4/media/session/MediaControllerCompatApi21.registerCallback:(Ljava/lang/Object;Ljava/lang/Object;Landroid/os/Handler;)V
            //    12: aload_0        
            //    13: getfield        android/support/v4/media/session/MediaControllerCompat$MediaControllerImplApi21.mLock:Ljava/lang/Object;
            //    16: astore_2       
            //    17: aload_2        
            //    18: monitorenter   
            //    19: aload_0        
            //    20: getfield        android/support/v4/media/session/MediaControllerCompat$MediaControllerImplApi21.mSessionToken:Landroid/support/v4/media/session/MediaSessionCompat$Token;
            //    23: invokevirtual   android/support/v4/media/session/MediaSessionCompat$Token.getExtraBinder:()Landroid/support/v4/media/session/IMediaSession;
            //    26: ifnull          77
            //    29: new             Landroid/support/v4/media/session/MediaControllerCompat$MediaControllerImplApi21$ExtraCallback;
            //    32: astore_3       
            //    33: aload_3        
            //    34: aload_1        
            //    35: invokespecial   android/support/v4/media/session/MediaControllerCompat$MediaControllerImplApi21$ExtraCallback.<init>:(Landroid/support/v4/media/session/MediaControllerCompat$Callback;)V
            //    38: aload_0        
            //    39: getfield        android/support/v4/media/session/MediaControllerCompat$MediaControllerImplApi21.mCallbackMap:Ljava/util/HashMap;
            //    42: aload_1        
            //    43: aload_3        
            //    44: invokevirtual   java/util/HashMap.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
            //    47: pop            
            //    48: aload_1        
            //    49: aload_3        
            //    50: putfield        android/support/v4/media/session/MediaControllerCompat$Callback.mIControllerCallback:Landroid/support/v4/media/session/IMediaControllerCallback;
            //    53: aload_0        
            //    54: getfield        android/support/v4/media/session/MediaControllerCompat$MediaControllerImplApi21.mSessionToken:Landroid/support/v4/media/session/MediaSessionCompat$Token;
            //    57: invokevirtual   android/support/v4/media/session/MediaSessionCompat$Token.getExtraBinder:()Landroid/support/v4/media/session/IMediaSession;
            //    60: aload_3        
            //    61: invokeinterface android/support/v4/media/session/IMediaSession.registerCallbackListener:(Landroid/support/v4/media/session/IMediaControllerCallback;)V
            //    66: aload_1        
            //    67: bipush          13
            //    69: aconst_null    
            //    70: aconst_null    
            //    71: invokevirtual   android/support/v4/media/session/MediaControllerCompat$Callback.postToHandler:(ILjava/lang/Object;Landroid/os/Bundle;)V
            //    74: goto            93
            //    77: aload_1        
            //    78: aconst_null    
            //    79: putfield        android/support/v4/media/session/MediaControllerCompat$Callback.mIControllerCallback:Landroid/support/v4/media/session/IMediaControllerCallback;
            //    82: aload_0        
            //    83: getfield        android/support/v4/media/session/MediaControllerCompat$MediaControllerImplApi21.mPendingCallbacks:Ljava/util/List;
            //    86: aload_1        
            //    87: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
            //    92: pop            
            //    93: aload_2        
            //    94: monitorexit    
            //    95: return         
            //    96: astore_1       
            //    97: aload_2        
            //    98: monitorexit    
            //    99: aload_1        
            //   100: athrow         
            //   101: astore_1       
            //   102: goto            93
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                        
            //  -----  -----  -----  -----  ----------------------------
            //  19     53     96     101    Any
            //  53     74     101    105    Landroid/os/RemoteException;
            //  53     74     96     101    Any
            //  77     93     96     101    Any
            //  93     95     96     101    Any
            //  97     99     96     101    Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0077:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        @Override
        public void removeQueueItem(final MediaDescriptionCompat mediaDescriptionCompat) {
            if ((this.getFlags() & 0x4L) != 0x0L) {
                final Bundle bundle = new Bundle();
                bundle.putParcelable("android.support.v4.media.session.command.ARGUMENT_MEDIA_DESCRIPTION", (Parcelable)mediaDescriptionCompat);
                this.sendCommand("android.support.v4.media.session.command.REMOVE_QUEUE_ITEM", bundle, null);
                return;
            }
            throw new UnsupportedOperationException("This session doesn't support queue management operations");
        }
        
        @Override
        public void sendCommand(final String s, final Bundle bundle, final ResultReceiver resultReceiver) {
            MediaControllerCompatApi21.sendCommand(this.mControllerObj, s, bundle, resultReceiver);
        }
        
        @Override
        public void setVolumeTo(final int n, final int n2) {
            MediaControllerCompatApi21.setVolumeTo(this.mControllerObj, n, n2);
        }
        
        @Override
        public final void unregisterCallback(final Callback p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: getfield        android/support/v4/media/session/MediaControllerCompat$MediaControllerImplApi21.mControllerObj:Ljava/lang/Object;
            //     4: aload_1        
            //     5: getfield        android/support/v4/media/session/MediaControllerCompat$Callback.mCallbackObj:Ljava/lang/Object;
            //     8: invokestatic    android/support/v4/media/session/MediaControllerCompatApi21.unregisterCallback:(Ljava/lang/Object;Ljava/lang/Object;)V
            //    11: aload_0        
            //    12: getfield        android/support/v4/media/session/MediaControllerCompat$MediaControllerImplApi21.mLock:Ljava/lang/Object;
            //    15: astore_2       
            //    16: aload_2        
            //    17: monitorenter   
            //    18: aload_0        
            //    19: getfield        android/support/v4/media/session/MediaControllerCompat$MediaControllerImplApi21.mSessionToken:Landroid/support/v4/media/session/MediaSessionCompat$Token;
            //    22: invokevirtual   android/support/v4/media/session/MediaSessionCompat$Token.getExtraBinder:()Landroid/support/v4/media/session/IMediaSession;
            //    25: astore_3       
            //    26: aload_3        
            //    27: ifnull          67
            //    30: aload_0        
            //    31: getfield        android/support/v4/media/session/MediaControllerCompat$MediaControllerImplApi21.mCallbackMap:Ljava/util/HashMap;
            //    34: aload_1        
            //    35: invokevirtual   java/util/HashMap.remove:(Ljava/lang/Object;)Ljava/lang/Object;
            //    38: checkcast       Landroid/support/v4/media/session/MediaControllerCompat$MediaControllerImplApi21$ExtraCallback;
            //    41: astore_3       
            //    42: aload_3        
            //    43: ifnull          78
            //    46: aload_1        
            //    47: aconst_null    
            //    48: putfield        android/support/v4/media/session/MediaControllerCompat$Callback.mIControllerCallback:Landroid/support/v4/media/session/IMediaControllerCallback;
            //    51: aload_0        
            //    52: getfield        android/support/v4/media/session/MediaControllerCompat$MediaControllerImplApi21.mSessionToken:Landroid/support/v4/media/session/MediaSessionCompat$Token;
            //    55: invokevirtual   android/support/v4/media/session/MediaSessionCompat$Token.getExtraBinder:()Landroid/support/v4/media/session/IMediaSession;
            //    58: aload_3        
            //    59: invokeinterface android/support/v4/media/session/IMediaSession.unregisterCallbackListener:(Landroid/support/v4/media/session/IMediaControllerCallback;)V
            //    64: goto            78
            //    67: aload_0        
            //    68: getfield        android/support/v4/media/session/MediaControllerCompat$MediaControllerImplApi21.mPendingCallbacks:Ljava/util/List;
            //    71: aload_1        
            //    72: invokeinterface java/util/List.remove:(Ljava/lang/Object;)Z
            //    77: pop            
            //    78: aload_2        
            //    79: monitorexit    
            //    80: return         
            //    81: astore_1       
            //    82: aload_2        
            //    83: monitorexit    
            //    84: aload_1        
            //    85: athrow         
            //    86: astore_1       
            //    87: goto            78
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                        
            //  -----  -----  -----  -----  ----------------------------
            //  18     26     81     86     Any
            //  30     42     86     90     Landroid/os/RemoteException;
            //  30     42     81     86     Any
            //  46     64     86     90     Landroid/os/RemoteException;
            //  46     64     81     86     Any
            //  67     78     81     86     Any
            //  78     80     81     86     Any
            //  82     84     81     86     Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0067:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        private static class ExtraBinderRequestResultReceiver extends ResultReceiver
        {
            private WeakReference<MediaControllerImplApi21> mMediaControllerImpl;
            
            ExtraBinderRequestResultReceiver(final MediaControllerImplApi21 referent) {
                super((Handler)null);
                this.mMediaControllerImpl = new WeakReference<MediaControllerImplApi21>(referent);
            }
            
            protected void onReceiveResult(final int n, final Bundle bundle) {
                final MediaControllerImplApi21 mediaControllerImplApi21 = this.mMediaControllerImpl.get();
                if (mediaControllerImplApi21 != null) {
                    if (bundle != null) {
                        synchronized (mediaControllerImplApi21.mLock) {
                            mediaControllerImplApi21.mSessionToken.setExtraBinder(IMediaSession.Stub.asInterface(BundleCompat.getBinder(bundle, "android.support.v4.media.session.EXTRA_BINDER")));
                            mediaControllerImplApi21.mSessionToken.setSessionToken2Bundle(bundle.getBundle("android.support.v4.media.session.SESSION_TOKEN2_BUNDLE"));
                            mediaControllerImplApi21.processPendingCallbacksLocked();
                        }
                    }
                }
            }
        }
        
        private static class ExtraCallback extends StubCompat
        {
            ExtraCallback(final Callback callback) {
                super(callback);
            }
            
            @Override
            public void onExtrasChanged(final Bundle bundle) throws RemoteException {
                throw new AssertionError();
            }
            
            @Override
            public void onMetadataChanged(final MediaMetadataCompat mediaMetadataCompat) throws RemoteException {
                throw new AssertionError();
            }
            
            @Override
            public void onQueueChanged(final List<MediaSessionCompat.QueueItem> list) throws RemoteException {
                throw new AssertionError();
            }
            
            @Override
            public void onQueueTitleChanged(final CharSequence charSequence) throws RemoteException {
                throw new AssertionError();
            }
            
            @Override
            public void onSessionDestroyed() throws RemoteException {
                throw new AssertionError();
            }
            
            @Override
            public void onVolumeInfoChanged(final ParcelableVolumeInfo parcelableVolumeInfo) throws RemoteException {
                throw new AssertionError();
            }
        }
    }
    
    @RequiresApi(23)
    static class MediaControllerImplApi23 extends MediaControllerImplApi21
    {
        public MediaControllerImplApi23(final Context context, final MediaSessionCompat.Token token) throws RemoteException {
            super(context, token);
        }
        
        @Override
        public TransportControls getTransportControls() {
            final Object transportControls = MediaControllerCompatApi21.getTransportControls(super.mControllerObj);
            TransportControlsApi21 transportControlsApi21;
            if (transportControls != null) {
                transportControlsApi21 = new TransportControlsApi23(transportControls);
            }
            else {
                transportControlsApi21 = null;
            }
            return transportControlsApi21;
        }
    }
    
    @RequiresApi(24)
    static class MediaControllerImplApi24 extends MediaControllerImplApi23
    {
        public MediaControllerImplApi24(final Context context, final MediaSessionCompat.Token token) throws RemoteException {
            super(context, token);
        }
        
        @Override
        public TransportControls getTransportControls() {
            final Object transportControls = MediaControllerCompatApi21.getTransportControls(super.mControllerObj);
            TransportControlsApi23 transportControlsApi23;
            if (transportControls != null) {
                transportControlsApi23 = new TransportControlsApi24(transportControls);
            }
            else {
                transportControlsApi23 = null;
            }
            return transportControlsApi23;
        }
    }
    
    static class MediaControllerImplBase implements MediaControllerImpl
    {
        private IMediaSession mBinder;
        private TransportControls mTransportControls;
        
        public MediaControllerImplBase(final MediaSessionCompat.Token token) {
            this.mBinder = IMediaSession.Stub.asInterface((IBinder)token.getToken());
        }
        
        @Override
        public void addQueueItem(final MediaDescriptionCompat mediaDescriptionCompat) {
            try {
                if ((this.mBinder.getFlags() & 0x4L) != 0x0L) {
                    this.mBinder.addQueueItem(mediaDescriptionCompat);
                    return;
                }
                throw new UnsupportedOperationException("This session doesn't support queue management operations");
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void addQueueItem(final MediaDescriptionCompat mediaDescriptionCompat, final int n) {
            try {
                if ((this.mBinder.getFlags() & 0x4L) != 0x0L) {
                    this.mBinder.addQueueItemAt(mediaDescriptionCompat, n);
                    return;
                }
                throw new UnsupportedOperationException("This session doesn't support queue management operations");
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void adjustVolume(final int n, final int n2) {
            try {
                this.mBinder.adjustVolume(n, n2, null);
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public boolean dispatchMediaButtonEvent(final KeyEvent keyEvent) {
            Label_0017: {
                if (keyEvent == null) {
                    break Label_0017;
                }
                try {
                    this.mBinder.sendMediaButton(keyEvent);
                    return false;
                    throw new IllegalArgumentException("event may not be null.");
                }
                catch (final RemoteException ex) {
                    return false;
                }
            }
        }
        
        @Override
        public Bundle getExtras() {
            try {
                return this.mBinder.getExtras();
            }
            catch (final RemoteException ex) {
                return null;
            }
        }
        
        @Override
        public long getFlags() {
            try {
                return this.mBinder.getFlags();
            }
            catch (final RemoteException ex) {
                return 0L;
            }
        }
        
        @Override
        public Object getMediaController() {
            return null;
        }
        
        @Override
        public MediaMetadataCompat getMetadata() {
            try {
                return this.mBinder.getMetadata();
            }
            catch (final RemoteException ex) {
                return null;
            }
        }
        
        @Override
        public String getPackageName() {
            try {
                return this.mBinder.getPackageName();
            }
            catch (final RemoteException ex) {
                return null;
            }
        }
        
        @Override
        public PlaybackInfo getPlaybackInfo() {
            try {
                final ParcelableVolumeInfo volumeAttributes = this.mBinder.getVolumeAttributes();
                return new PlaybackInfo(volumeAttributes.volumeType, volumeAttributes.audioStream, volumeAttributes.controlType, volumeAttributes.maxVolume, volumeAttributes.currentVolume);
            }
            catch (final RemoteException ex) {
                return null;
            }
        }
        
        @Override
        public PlaybackStateCompat getPlaybackState() {
            try {
                return this.mBinder.getPlaybackState();
            }
            catch (final RemoteException ex) {
                return null;
            }
        }
        
        @Override
        public List<MediaSessionCompat.QueueItem> getQueue() {
            try {
                return this.mBinder.getQueue();
            }
            catch (final RemoteException ex) {
                return null;
            }
        }
        
        @Override
        public CharSequence getQueueTitle() {
            try {
                return this.mBinder.getQueueTitle();
            }
            catch (final RemoteException ex) {
                return null;
            }
        }
        
        @Override
        public int getRatingType() {
            try {
                return this.mBinder.getRatingType();
            }
            catch (final RemoteException ex) {
                return 0;
            }
        }
        
        @Override
        public int getRepeatMode() {
            try {
                return this.mBinder.getRepeatMode();
            }
            catch (final RemoteException ex) {
                return -1;
            }
        }
        
        @Override
        public PendingIntent getSessionActivity() {
            try {
                return this.mBinder.getLaunchPendingIntent();
            }
            catch (final RemoteException ex) {
                return null;
            }
        }
        
        @Override
        public int getShuffleMode() {
            try {
                return this.mBinder.getShuffleMode();
            }
            catch (final RemoteException ex) {
                return -1;
            }
        }
        
        @Override
        public TransportControls getTransportControls() {
            if (this.mTransportControls == null) {
                this.mTransportControls = new TransportControlsBase(this.mBinder);
            }
            return this.mTransportControls;
        }
        
        @Override
        public boolean isCaptioningEnabled() {
            try {
                return this.mBinder.isCaptioningEnabled();
            }
            catch (final RemoteException ex) {
                return false;
            }
        }
        
        @Override
        public boolean isSessionReady() {
            return true;
        }
        
        @Override
        public void registerCallback(final Callback callback, final Handler handler) {
            if (callback != null) {
                try {
                    ((IInterface)this.mBinder).asBinder().linkToDeath((IBinder$DeathRecipient)callback, 0);
                    this.mBinder.registerCallbackListener((IMediaControllerCallback)callback.mCallbackObj);
                    callback.postToHandler(13, null, null);
                }
                catch (final RemoteException ex) {
                    callback.postToHandler(8, null, null);
                }
                return;
            }
            throw new IllegalArgumentException("callback may not be null.");
        }
        
        @Override
        public void removeQueueItem(final MediaDescriptionCompat mediaDescriptionCompat) {
            try {
                if ((this.mBinder.getFlags() & 0x4L) != 0x0L) {
                    this.mBinder.removeQueueItem(mediaDescriptionCompat);
                    return;
                }
                throw new UnsupportedOperationException("This session doesn't support queue management operations");
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void sendCommand(final String s, final Bundle bundle, final ResultReceiver resultReceiver) {
            try {
                this.mBinder.sendCommand(s, bundle, new MediaSessionCompat.ResultReceiverWrapper(resultReceiver));
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void setVolumeTo(final int n, final int n2) {
            try {
                this.mBinder.setVolumeTo(n, n2, null);
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void unregisterCallback(final Callback callback) {
            Label_0038: {
                if (callback == null) {
                    break Label_0038;
                }
                try {
                    this.mBinder.unregisterCallbackListener((IMediaControllerCallback)callback.mCallbackObj);
                    ((IInterface)this.mBinder).asBinder().unlinkToDeath((IBinder$DeathRecipient)callback, 0);
                    return;
                    throw new IllegalArgumentException("callback may not be null.");
                }
                catch (final RemoteException ex) {}
            }
        }
    }
    
    public static final class PlaybackInfo
    {
        public static final int PLAYBACK_TYPE_LOCAL = 1;
        public static final int PLAYBACK_TYPE_REMOTE = 2;
        private final int mAudioStream;
        private final int mCurrentVolume;
        private final int mMaxVolume;
        private final int mPlaybackType;
        private final int mVolumeControl;
        
        PlaybackInfo(final int mPlaybackType, final int mAudioStream, final int mVolumeControl, final int mMaxVolume, final int mCurrentVolume) {
            this.mPlaybackType = mPlaybackType;
            this.mAudioStream = mAudioStream;
            this.mVolumeControl = mVolumeControl;
            this.mMaxVolume = mMaxVolume;
            this.mCurrentVolume = mCurrentVolume;
        }
        
        public int getAudioStream() {
            return this.mAudioStream;
        }
        
        public int getCurrentVolume() {
            return this.mCurrentVolume;
        }
        
        public int getMaxVolume() {
            return this.mMaxVolume;
        }
        
        public int getPlaybackType() {
            return this.mPlaybackType;
        }
        
        public int getVolumeControl() {
            return this.mVolumeControl;
        }
    }
    
    public abstract static class TransportControls
    {
        public static final String EXTRA_LEGACY_STREAM_TYPE = "android.media.session.extra.LEGACY_STREAM_TYPE";
        
        TransportControls() {
        }
        
        public abstract void fastForward();
        
        public abstract void pause();
        
        public abstract void play();
        
        public abstract void playFromMediaId(final String p0, final Bundle p1);
        
        public abstract void playFromSearch(final String p0, final Bundle p1);
        
        public abstract void playFromUri(final Uri p0, final Bundle p1);
        
        public abstract void prepare();
        
        public abstract void prepareFromMediaId(final String p0, final Bundle p1);
        
        public abstract void prepareFromSearch(final String p0, final Bundle p1);
        
        public abstract void prepareFromUri(final Uri p0, final Bundle p1);
        
        public abstract void rewind();
        
        public abstract void seekTo(final long p0);
        
        public abstract void sendCustomAction(final PlaybackStateCompat.CustomAction p0, final Bundle p1);
        
        public abstract void sendCustomAction(final String p0, final Bundle p1);
        
        public abstract void setCaptioningEnabled(final boolean p0);
        
        public abstract void setRating(final RatingCompat p0);
        
        public abstract void setRating(final RatingCompat p0, final Bundle p1);
        
        public abstract void setRepeatMode(final int p0);
        
        public abstract void setShuffleMode(final int p0);
        
        public abstract void skipToNext();
        
        public abstract void skipToPrevious();
        
        public abstract void skipToQueueItem(final long p0);
        
        public abstract void stop();
    }
    
    static class TransportControlsApi21 extends TransportControls
    {
        protected final Object mControlsObj;
        
        public TransportControlsApi21(final Object mControlsObj) {
            this.mControlsObj = mControlsObj;
        }
        
        @Override
        public void fastForward() {
            MediaControllerCompatApi21.TransportControls.fastForward(this.mControlsObj);
        }
        
        @Override
        public void pause() {
            MediaControllerCompatApi21.TransportControls.pause(this.mControlsObj);
        }
        
        @Override
        public void play() {
            MediaControllerCompatApi21.TransportControls.play(this.mControlsObj);
        }
        
        @Override
        public void playFromMediaId(final String s, final Bundle bundle) {
            MediaControllerCompatApi21.TransportControls.playFromMediaId(this.mControlsObj, s, bundle);
        }
        
        @Override
        public void playFromSearch(final String s, final Bundle bundle) {
            MediaControllerCompatApi21.TransportControls.playFromSearch(this.mControlsObj, s, bundle);
        }
        
        @Override
        public void playFromUri(final Uri uri, final Bundle bundle) {
            if (uri != null && !Uri.EMPTY.equals((Object)uri)) {
                final Bundle bundle2 = new Bundle();
                bundle2.putParcelable("android.support.v4.media.session.action.ARGUMENT_URI", (Parcelable)uri);
                bundle2.putBundle("android.support.v4.media.session.action.ARGUMENT_EXTRAS", bundle);
                this.sendCustomAction("android.support.v4.media.session.action.PLAY_FROM_URI", bundle2);
                return;
            }
            throw new IllegalArgumentException("You must specify a non-empty Uri for playFromUri.");
        }
        
        @Override
        public void prepare() {
            this.sendCustomAction("android.support.v4.media.session.action.PREPARE", null);
        }
        
        @Override
        public void prepareFromMediaId(final String s, final Bundle bundle) {
            final Bundle bundle2 = new Bundle();
            ((BaseBundle)bundle2).putString("android.support.v4.media.session.action.ARGUMENT_MEDIA_ID", s);
            bundle2.putBundle("android.support.v4.media.session.action.ARGUMENT_EXTRAS", bundle);
            this.sendCustomAction("android.support.v4.media.session.action.PREPARE_FROM_MEDIA_ID", bundle2);
        }
        
        @Override
        public void prepareFromSearch(final String s, final Bundle bundle) {
            final Bundle bundle2 = new Bundle();
            ((BaseBundle)bundle2).putString("android.support.v4.media.session.action.ARGUMENT_QUERY", s);
            bundle2.putBundle("android.support.v4.media.session.action.ARGUMENT_EXTRAS", bundle);
            this.sendCustomAction("android.support.v4.media.session.action.PREPARE_FROM_SEARCH", bundle2);
        }
        
        @Override
        public void prepareFromUri(final Uri uri, final Bundle bundle) {
            final Bundle bundle2 = new Bundle();
            bundle2.putParcelable("android.support.v4.media.session.action.ARGUMENT_URI", (Parcelable)uri);
            bundle2.putBundle("android.support.v4.media.session.action.ARGUMENT_EXTRAS", bundle);
            this.sendCustomAction("android.support.v4.media.session.action.PREPARE_FROM_URI", bundle2);
        }
        
        @Override
        public void rewind() {
            MediaControllerCompatApi21.TransportControls.rewind(this.mControlsObj);
        }
        
        @Override
        public void seekTo(final long n) {
            MediaControllerCompatApi21.TransportControls.seekTo(this.mControlsObj, n);
        }
        
        @Override
        public void sendCustomAction(final PlaybackStateCompat.CustomAction customAction, final Bundle bundle) {
            MediaControllerCompat.validateCustomAction(customAction.getAction(), bundle);
            MediaControllerCompatApi21.TransportControls.sendCustomAction(this.mControlsObj, customAction.getAction(), bundle);
        }
        
        @Override
        public void sendCustomAction(final String s, final Bundle bundle) {
            MediaControllerCompat.validateCustomAction(s, bundle);
            MediaControllerCompatApi21.TransportControls.sendCustomAction(this.mControlsObj, s, bundle);
        }
        
        @Override
        public void setCaptioningEnabled(final boolean b) {
            final Bundle bundle = new Bundle();
            bundle.putBoolean("android.support.v4.media.session.action.ARGUMENT_CAPTIONING_ENABLED", b);
            this.sendCustomAction("android.support.v4.media.session.action.SET_CAPTIONING_ENABLED", bundle);
        }
        
        @Override
        public void setRating(final RatingCompat ratingCompat) {
            final Object mControlsObj = this.mControlsObj;
            Object rating;
            if (ratingCompat != null) {
                rating = ratingCompat.getRating();
            }
            else {
                rating = null;
            }
            MediaControllerCompatApi21.TransportControls.setRating(mControlsObj, rating);
        }
        
        @Override
        public void setRating(final RatingCompat ratingCompat, final Bundle bundle) {
            final Bundle bundle2 = new Bundle();
            bundle2.putParcelable("android.support.v4.media.session.action.ARGUMENT_RATING", (Parcelable)ratingCompat);
            bundle2.putBundle("android.support.v4.media.session.action.ARGUMENT_EXTRAS", bundle);
            this.sendCustomAction("android.support.v4.media.session.action.SET_RATING", bundle2);
        }
        
        @Override
        public void setRepeatMode(final int n) {
            final Bundle bundle = new Bundle();
            ((BaseBundle)bundle).putInt("android.support.v4.media.session.action.ARGUMENT_REPEAT_MODE", n);
            this.sendCustomAction("android.support.v4.media.session.action.SET_REPEAT_MODE", bundle);
        }
        
        @Override
        public void setShuffleMode(final int n) {
            final Bundle bundle = new Bundle();
            ((BaseBundle)bundle).putInt("android.support.v4.media.session.action.ARGUMENT_SHUFFLE_MODE", n);
            this.sendCustomAction("android.support.v4.media.session.action.SET_SHUFFLE_MODE", bundle);
        }
        
        @Override
        public void skipToNext() {
            MediaControllerCompatApi21.TransportControls.skipToNext(this.mControlsObj);
        }
        
        @Override
        public void skipToPrevious() {
            MediaControllerCompatApi21.TransportControls.skipToPrevious(this.mControlsObj);
        }
        
        @Override
        public void skipToQueueItem(final long n) {
            MediaControllerCompatApi21.TransportControls.skipToQueueItem(this.mControlsObj, n);
        }
        
        @Override
        public void stop() {
            MediaControllerCompatApi21.TransportControls.stop(this.mControlsObj);
        }
    }
    
    @RequiresApi(23)
    static class TransportControlsApi23 extends TransportControlsApi21
    {
        public TransportControlsApi23(final Object o) {
            super(o);
        }
        
        @Override
        public void playFromUri(final Uri uri, final Bundle bundle) {
            MediaControllerCompatApi23.TransportControls.playFromUri(super.mControlsObj, uri, bundle);
        }
    }
    
    @RequiresApi(24)
    static class TransportControlsApi24 extends TransportControlsApi23
    {
        public TransportControlsApi24(final Object o) {
            super(o);
        }
        
        @Override
        public void prepare() {
            MediaControllerCompatApi24.TransportControls.prepare(super.mControlsObj);
        }
        
        @Override
        public void prepareFromMediaId(final String s, final Bundle bundle) {
            MediaControllerCompatApi24.TransportControls.prepareFromMediaId(super.mControlsObj, s, bundle);
        }
        
        @Override
        public void prepareFromSearch(final String s, final Bundle bundle) {
            MediaControllerCompatApi24.TransportControls.prepareFromSearch(super.mControlsObj, s, bundle);
        }
        
        @Override
        public void prepareFromUri(final Uri uri, final Bundle bundle) {
            MediaControllerCompatApi24.TransportControls.prepareFromUri(super.mControlsObj, uri, bundle);
        }
    }
    
    static class TransportControlsBase extends TransportControls
    {
        private IMediaSession mBinder;
        
        public TransportControlsBase(final IMediaSession mBinder) {
            this.mBinder = mBinder;
        }
        
        @Override
        public void fastForward() {
            try {
                this.mBinder.fastForward();
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void pause() {
            try {
                this.mBinder.pause();
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void play() {
            try {
                this.mBinder.play();
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void playFromMediaId(final String s, final Bundle bundle) {
            try {
                this.mBinder.playFromMediaId(s, bundle);
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void playFromSearch(final String s, final Bundle bundle) {
            try {
                this.mBinder.playFromSearch(s, bundle);
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void playFromUri(final Uri uri, final Bundle bundle) {
            try {
                this.mBinder.playFromUri(uri, bundle);
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void prepare() {
            try {
                this.mBinder.prepare();
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void prepareFromMediaId(final String s, final Bundle bundle) {
            try {
                this.mBinder.prepareFromMediaId(s, bundle);
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void prepareFromSearch(final String s, final Bundle bundle) {
            try {
                this.mBinder.prepareFromSearch(s, bundle);
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void prepareFromUri(final Uri uri, final Bundle bundle) {
            try {
                this.mBinder.prepareFromUri(uri, bundle);
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void rewind() {
            try {
                this.mBinder.rewind();
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void seekTo(final long n) {
            try {
                this.mBinder.seekTo(n);
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void sendCustomAction(final PlaybackStateCompat.CustomAction customAction, final Bundle bundle) {
            this.sendCustomAction(customAction.getAction(), bundle);
        }
        
        @Override
        public void sendCustomAction(final String s, final Bundle bundle) {
            MediaControllerCompat.validateCustomAction(s, bundle);
            try {
                this.mBinder.sendCustomAction(s, bundle);
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void setCaptioningEnabled(final boolean captioningEnabled) {
            try {
                this.mBinder.setCaptioningEnabled(captioningEnabled);
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void setRating(final RatingCompat ratingCompat) {
            try {
                this.mBinder.rate(ratingCompat);
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void setRating(final RatingCompat ratingCompat, final Bundle bundle) {
            try {
                this.mBinder.rateWithExtras(ratingCompat, bundle);
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void setRepeatMode(final int repeatMode) {
            try {
                this.mBinder.setRepeatMode(repeatMode);
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void setShuffleMode(final int shuffleMode) {
            try {
                this.mBinder.setShuffleMode(shuffleMode);
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void skipToNext() {
            try {
                this.mBinder.next();
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void skipToPrevious() {
            try {
                this.mBinder.previous();
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void skipToQueueItem(final long n) {
            try {
                this.mBinder.skipToQueueItem(n);
            }
            catch (final RemoteException ex) {}
        }
        
        @Override
        public void stop() {
            try {
                this.mBinder.stop();
            }
            catch (final RemoteException ex) {}
        }
    }
}

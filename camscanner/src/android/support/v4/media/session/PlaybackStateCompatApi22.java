// 
// Decompiled by Procyon v0.6.0
// 

package android.support.v4.media.session;

import java.util.Iterator;
import android.media.session.PlaybackState$CustomAction;
import android.media.session.PlaybackState$Builder;
import java.util.List;
import android.media.session.PlaybackState;
import android.os.Bundle;
import androidx.annotation.RequiresApi;

@RequiresApi(22)
class PlaybackStateCompatApi22
{
    private PlaybackStateCompatApi22() {
    }
    
    public static Bundle getExtras(final Object o) {
        return oO80.\u3007080((PlaybackState)o);
    }
    
    public static Object newInstance(final int n, final long n2, final long bufferedPosition, final float n3, final long actions, final CharSequence errorMessage, final long n4, final List<Object> list, final long activeQueueItemId, final Bundle bundle) {
        final PlaybackState$Builder playbackState$Builder = new PlaybackState$Builder();
        playbackState$Builder.setState(n, n2, n3, n4);
        playbackState$Builder.setBufferedPosition(bufferedPosition);
        playbackState$Builder.setActions(actions);
        playbackState$Builder.setErrorMessage(errorMessage);
        final Iterator<Object> iterator = (Iterator<Object>)list.iterator();
        while (iterator.hasNext()) {
            playbackState$Builder.addCustomAction((PlaybackState$CustomAction)iterator.next());
        }
        playbackState$Builder.setActiveQueueItemId(activeQueueItemId);
        \u300780\u3007808\u3007O.\u3007080(playbackState$Builder, bundle);
        return playbackState$Builder.build();
    }
}

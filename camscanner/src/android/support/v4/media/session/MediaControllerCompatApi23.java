// 
// Decompiled by Procyon v0.6.0
// 

package android.support.v4.media.session;

import android.media.session.MediaController$TransportControls;
import android.os.Bundle;
import android.net.Uri;
import androidx.annotation.RequiresApi;

@RequiresApi(23)
class MediaControllerCompatApi23
{
    private MediaControllerCompatApi23() {
    }
    
    public static class TransportControls
    {
        private TransportControls() {
        }
        
        public static void playFromUri(final Object o, final Uri uri, final Bundle bundle) {
            \u3007080.\u3007080((MediaController$TransportControls)o, uri, bundle);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package android.support.v4.media.session;

import android.net.Uri;
import android.os.Bundle;
import android.media.session.MediaController$TransportControls;
import androidx.annotation.RequiresApi;

@RequiresApi(24)
class MediaControllerCompatApi24
{
    private MediaControllerCompatApi24() {
    }
    
    public static class TransportControls
    {
        private TransportControls() {
        }
        
        public static void prepare(final Object o) {
            \u3007o\u3007.\u3007080((MediaController$TransportControls)o);
        }
        
        public static void prepareFromMediaId(final Object o, final String s, final Bundle bundle) {
            O8.\u3007080((MediaController$TransportControls)o, s, bundle);
        }
        
        public static void prepareFromSearch(final Object o, final String s, final Bundle bundle) {
            \u3007o00\u3007\u3007Oo.\u3007080((MediaController$TransportControls)o, s, bundle);
        }
        
        public static void prepareFromUri(final Object o, final Uri uri, final Bundle bundle) {
            Oo08.\u3007080((MediaController$TransportControls)o, uri, bundle);
        }
    }
}

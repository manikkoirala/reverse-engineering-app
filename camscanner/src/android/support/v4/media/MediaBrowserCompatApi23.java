// 
// Decompiled by Procyon v0.6.0
// 

package android.support.v4.media;

import android.media.browse.MediaBrowser$MediaItem;
import android.os.Parcel;
import androidx.annotation.NonNull;
import android.media.browse.MediaBrowser$ItemCallback;
import android.media.browse.MediaBrowser;
import androidx.annotation.RequiresApi;

@RequiresApi(23)
class MediaBrowserCompatApi23
{
    private MediaBrowserCompatApi23() {
    }
    
    public static Object createItemCallback(final ItemCallback itemCallback) {
        return new ItemCallbackProxy(itemCallback);
    }
    
    public static void getItem(final Object o, final String s, final Object o2) {
        \u3007080.\u3007080((MediaBrowser)o, s, (MediaBrowser$ItemCallback)o2);
    }
    
    interface ItemCallback
    {
        void onError(@NonNull final String p0);
        
        void onItemLoaded(final Parcel p0);
    }
    
    static class ItemCallbackProxy<T extends ItemCallback> extends MediaBrowser$ItemCallback
    {
        protected final T mItemCallback;
        
        public ItemCallbackProxy(final T mItemCallback) {
            this.mItemCallback = mItemCallback;
        }
        
        public void onError(@NonNull final String s) {
            this.mItemCallback.onError(s);
        }
        
        public void onItemLoaded(final MediaBrowser$MediaItem mediaBrowser$MediaItem) {
            if (mediaBrowser$MediaItem == null) {
                this.mItemCallback.onItemLoaded(null);
            }
            else {
                final Parcel obtain = Parcel.obtain();
                mediaBrowser$MediaItem.writeToParcel(obtain, 0);
                this.mItemCallback.onItemLoaded(obtain);
            }
        }
    }
}

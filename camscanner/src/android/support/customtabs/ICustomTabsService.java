// 
// Decompiled by Procyon v0.6.0
// 

package android.support.customtabs;

import android.os.Parcelable;
import android.os.Parcel;
import android.os.Binder;
import android.os.IBinder;
import java.util.List;
import android.net.Uri;
import android.os.RemoteException;
import android.os.Bundle;
import android.os.IInterface;

public interface ICustomTabsService extends IInterface
{
    Bundle extraCommand(final String p0, final Bundle p1) throws RemoteException;
    
    boolean mayLaunchUrl(final ICustomTabsCallback p0, final Uri p1, final Bundle p2, final List<Bundle> p3) throws RemoteException;
    
    boolean newSession(final ICustomTabsCallback p0) throws RemoteException;
    
    boolean newSessionWithExtras(final ICustomTabsCallback p0, final Bundle p1) throws RemoteException;
    
    int postMessage(final ICustomTabsCallback p0, final String p1, final Bundle p2) throws RemoteException;
    
    boolean receiveFile(final ICustomTabsCallback p0, final Uri p1, final int p2, final Bundle p3) throws RemoteException;
    
    boolean requestPostMessageChannel(final ICustomTabsCallback p0, final Uri p1) throws RemoteException;
    
    boolean requestPostMessageChannelWithExtras(final ICustomTabsCallback p0, final Uri p1, final Bundle p2) throws RemoteException;
    
    boolean updateVisuals(final ICustomTabsCallback p0, final Bundle p1) throws RemoteException;
    
    boolean validateRelationship(final ICustomTabsCallback p0, final int p1, final Uri p2, final Bundle p3) throws RemoteException;
    
    boolean warmup(final long p0) throws RemoteException;
    
    public static class Default implements ICustomTabsService
    {
        public IBinder asBinder() {
            return null;
        }
        
        @Override
        public Bundle extraCommand(final String s, final Bundle bundle) throws RemoteException {
            return null;
        }
        
        @Override
        public boolean mayLaunchUrl(final ICustomTabsCallback customTabsCallback, final Uri uri, final Bundle bundle, final List<Bundle> list) throws RemoteException {
            return false;
        }
        
        @Override
        public boolean newSession(final ICustomTabsCallback customTabsCallback) throws RemoteException {
            return false;
        }
        
        @Override
        public boolean newSessionWithExtras(final ICustomTabsCallback customTabsCallback, final Bundle bundle) throws RemoteException {
            return false;
        }
        
        @Override
        public int postMessage(final ICustomTabsCallback customTabsCallback, final String s, final Bundle bundle) throws RemoteException {
            return 0;
        }
        
        @Override
        public boolean receiveFile(final ICustomTabsCallback customTabsCallback, final Uri uri, final int n, final Bundle bundle) throws RemoteException {
            return false;
        }
        
        @Override
        public boolean requestPostMessageChannel(final ICustomTabsCallback customTabsCallback, final Uri uri) throws RemoteException {
            return false;
        }
        
        @Override
        public boolean requestPostMessageChannelWithExtras(final ICustomTabsCallback customTabsCallback, final Uri uri, final Bundle bundle) throws RemoteException {
            return false;
        }
        
        @Override
        public boolean updateVisuals(final ICustomTabsCallback customTabsCallback, final Bundle bundle) throws RemoteException {
            return false;
        }
        
        @Override
        public boolean validateRelationship(final ICustomTabsCallback customTabsCallback, final int n, final Uri uri, final Bundle bundle) throws RemoteException {
            return false;
        }
        
        @Override
        public boolean warmup(final long n) throws RemoteException {
            return false;
        }
    }
    
    public abstract static class Stub extends Binder implements ICustomTabsService
    {
        private static final String DESCRIPTOR = "android.support.customtabs.ICustomTabsService";
        static final int TRANSACTION_extraCommand = 5;
        static final int TRANSACTION_mayLaunchUrl = 4;
        static final int TRANSACTION_newSession = 3;
        static final int TRANSACTION_newSessionWithExtras = 10;
        static final int TRANSACTION_postMessage = 8;
        static final int TRANSACTION_receiveFile = 12;
        static final int TRANSACTION_requestPostMessageChannel = 7;
        static final int TRANSACTION_requestPostMessageChannelWithExtras = 11;
        static final int TRANSACTION_updateVisuals = 6;
        static final int TRANSACTION_validateRelationship = 9;
        static final int TRANSACTION_warmup = 2;
        public static final int o0 = 0;
        
        public Stub() {
            this.attachInterface((IInterface)this, "android.support.customtabs.ICustomTabsService");
        }
        
        public static ICustomTabsService asInterface(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("android.support.customtabs.ICustomTabsService");
            if (queryLocalInterface != null && queryLocalInterface instanceof ICustomTabsService) {
                return (ICustomTabsService)queryLocalInterface;
            }
            return new Proxy(binder);
        }
        
        public static ICustomTabsService getDefaultImpl() {
            return Proxy.sDefaultImpl;
        }
        
        public static boolean setDefaultImpl(final ICustomTabsService sDefaultImpl) {
            if (Proxy.sDefaultImpl != null) {
                throw new IllegalStateException("setDefaultImpl() called twice");
            }
            if (sDefaultImpl != null) {
                Proxy.sDefaultImpl = sDefaultImpl;
                return true;
            }
            return false;
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            if (n == 1598968902) {
                parcel2.writeString("android.support.customtabs.ICustomTabsService");
                return true;
            }
            final Bundle bundle = null;
            final Bundle bundle2 = null;
            final Bundle bundle3 = null;
            Bundle bundle4 = null;
            final Uri uri = null;
            final Bundle bundle5 = null;
            final Bundle bundle6 = null;
            final Bundle bundle7 = null;
            Bundle bundle8 = null;
            switch (n) {
                default: {
                    return super.onTransact(n, parcel, parcel2, n2);
                }
                case 12: {
                    parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                    final ICustomTabsCallback interface1 = ICustomTabsCallback.Stub.asInterface(parcel.readStrongBinder());
                    Uri uri2;
                    if (parcel.readInt() != 0) {
                        uri2 = (Uri)Uri.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        uri2 = null;
                    }
                    n = parcel.readInt();
                    if (parcel.readInt() != 0) {
                        bundle8 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    n = (this.receiveFile(interface1, uri2, n, bundle8) ? 1 : 0);
                    parcel2.writeNoException();
                    parcel2.writeInt(n);
                    return true;
                }
                case 11: {
                    parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                    final ICustomTabsCallback interface2 = ICustomTabsCallback.Stub.asInterface(parcel.readStrongBinder());
                    Uri uri3;
                    if (parcel.readInt() != 0) {
                        uri3 = (Uri)Uri.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        uri3 = null;
                    }
                    Bundle bundle9 = bundle;
                    if (parcel.readInt() != 0) {
                        bundle9 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    n = (this.requestPostMessageChannelWithExtras(interface2, uri3, bundle9) ? 1 : 0);
                    parcel2.writeNoException();
                    parcel2.writeInt(n);
                    return true;
                }
                case 10: {
                    parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                    final ICustomTabsCallback interface3 = ICustomTabsCallback.Stub.asInterface(parcel.readStrongBinder());
                    Bundle bundle10 = bundle2;
                    if (parcel.readInt() != 0) {
                        bundle10 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    n = (this.newSessionWithExtras(interface3, bundle10) ? 1 : 0);
                    parcel2.writeNoException();
                    parcel2.writeInt(n);
                    return true;
                }
                case 9: {
                    parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                    final ICustomTabsCallback interface4 = ICustomTabsCallback.Stub.asInterface(parcel.readStrongBinder());
                    n = parcel.readInt();
                    Uri uri4;
                    if (parcel.readInt() != 0) {
                        uri4 = (Uri)Uri.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        uri4 = null;
                    }
                    Bundle bundle11 = bundle3;
                    if (parcel.readInt() != 0) {
                        bundle11 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    n = (this.validateRelationship(interface4, n, uri4, bundle11) ? 1 : 0);
                    parcel2.writeNoException();
                    parcel2.writeInt(n);
                    return true;
                }
                case 8: {
                    parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                    final ICustomTabsCallback interface5 = ICustomTabsCallback.Stub.asInterface(parcel.readStrongBinder());
                    final String string = parcel.readString();
                    if (parcel.readInt() != 0) {
                        bundle4 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    n = this.postMessage(interface5, string, bundle4);
                    parcel2.writeNoException();
                    parcel2.writeInt(n);
                    return true;
                }
                case 7: {
                    parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                    final ICustomTabsCallback interface6 = ICustomTabsCallback.Stub.asInterface(parcel.readStrongBinder());
                    Uri uri5 = uri;
                    if (parcel.readInt() != 0) {
                        uri5 = (Uri)Uri.CREATOR.createFromParcel(parcel);
                    }
                    n = (this.requestPostMessageChannel(interface6, uri5) ? 1 : 0);
                    parcel2.writeNoException();
                    parcel2.writeInt(n);
                    return true;
                }
                case 6: {
                    parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                    final ICustomTabsCallback interface7 = ICustomTabsCallback.Stub.asInterface(parcel.readStrongBinder());
                    Bundle bundle12 = bundle5;
                    if (parcel.readInt() != 0) {
                        bundle12 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    n = (this.updateVisuals(interface7, bundle12) ? 1 : 0);
                    parcel2.writeNoException();
                    parcel2.writeInt(n);
                    return true;
                }
                case 5: {
                    parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                    final String string2 = parcel.readString();
                    Bundle bundle13 = bundle6;
                    if (parcel.readInt() != 0) {
                        bundle13 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    final Bundle extraCommand = this.extraCommand(string2, bundle13);
                    parcel2.writeNoException();
                    if (extraCommand != null) {
                        parcel2.writeInt(1);
                        extraCommand.writeToParcel(parcel2, 1);
                    }
                    else {
                        parcel2.writeInt(0);
                    }
                    return true;
                }
                case 4: {
                    parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                    final ICustomTabsCallback interface8 = ICustomTabsCallback.Stub.asInterface(parcel.readStrongBinder());
                    Uri uri6;
                    if (parcel.readInt() != 0) {
                        uri6 = (Uri)Uri.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        uri6 = null;
                    }
                    Bundle bundle14 = bundle7;
                    if (parcel.readInt() != 0) {
                        bundle14 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    n = (this.mayLaunchUrl(interface8, uri6, bundle14, parcel.createTypedArrayList(Bundle.CREATOR)) ? 1 : 0);
                    parcel2.writeNoException();
                    parcel2.writeInt(n);
                    return true;
                }
                case 3: {
                    parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                    n = (this.newSession(ICustomTabsCallback.Stub.asInterface(parcel.readStrongBinder())) ? 1 : 0);
                    parcel2.writeNoException();
                    parcel2.writeInt(n);
                    return true;
                }
                case 2: {
                    parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                    n = (this.warmup(parcel.readLong()) ? 1 : 0);
                    parcel2.writeNoException();
                    parcel2.writeInt(n);
                    return true;
                }
            }
        }
        
        private static class Proxy implements ICustomTabsService
        {
            public static ICustomTabsService sDefaultImpl;
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            @Override
            public Bundle extraCommand(final String s, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsService");
                    obtain.writeString(s);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (!this.mRemote.transact(5, obtain, obtain2, 0) && Stub.getDefaultImpl() != null) {
                        return Stub.getDefaultImpl().extraCommand(s, bundle);
                    }
                    obtain2.readException();
                    Bundle bundle2;
                    if (obtain2.readInt() != 0) {
                        bundle2 = (Bundle)Bundle.CREATOR.createFromParcel(obtain2);
                    }
                    else {
                        bundle2 = null;
                    }
                    return bundle2;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            public String getInterfaceDescriptor() {
                return "android.support.customtabs.ICustomTabsService";
            }
            
            @Override
            public boolean mayLaunchUrl(final ICustomTabsCallback customTabsCallback, final Uri uri, final Bundle bundle, final List<Bundle> list) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsService");
                    IBinder binder;
                    if (customTabsCallback != null) {
                        binder = ((IInterface)customTabsCallback).asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    boolean b = true;
                    if (uri != null) {
                        obtain.writeInt(1);
                        ((Parcelable)uri).writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    obtain.writeTypedList((List)list);
                    if (!this.mRemote.transact(4, obtain, obtain2, 0) && Stub.getDefaultImpl() != null) {
                        return Stub.getDefaultImpl().mayLaunchUrl(customTabsCallback, uri, bundle, list);
                    }
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        b = false;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean newSession(final ICustomTabsCallback customTabsCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsService");
                    IBinder binder;
                    if (customTabsCallback != null) {
                        binder = ((IInterface)customTabsCallback).asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    final IBinder mRemote = this.mRemote;
                    boolean b = false;
                    if (!mRemote.transact(3, obtain, obtain2, 0) && Stub.getDefaultImpl() != null) {
                        return Stub.getDefaultImpl().newSession(customTabsCallback);
                    }
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        b = true;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean newSessionWithExtras(final ICustomTabsCallback customTabsCallback, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsService");
                    IBinder binder;
                    if (customTabsCallback != null) {
                        binder = ((IInterface)customTabsCallback).asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    boolean b = true;
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (!this.mRemote.transact(10, obtain, obtain2, 0) && Stub.getDefaultImpl() != null) {
                        return Stub.getDefaultImpl().newSessionWithExtras(customTabsCallback, bundle);
                    }
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        b = false;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public int postMessage(final ICustomTabsCallback customTabsCallback, final String s, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsService");
                    IBinder binder;
                    if (customTabsCallback != null) {
                        binder = ((IInterface)customTabsCallback).asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    obtain.writeString(s);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (!this.mRemote.transact(8, obtain, obtain2, 0) && Stub.getDefaultImpl() != null) {
                        return Stub.getDefaultImpl().postMessage(customTabsCallback, s, bundle);
                    }
                    obtain2.readException();
                    return obtain2.readInt();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean receiveFile(final ICustomTabsCallback customTabsCallback, final Uri uri, int int1, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsService");
                    IBinder binder;
                    if (customTabsCallback != null) {
                        binder = ((IInterface)customTabsCallback).asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    boolean b = true;
                    if (uri != null) {
                        obtain.writeInt(1);
                        ((Parcelable)uri).writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    obtain.writeInt(int1);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (!this.mRemote.transact(12, obtain, obtain2, 0) && Stub.getDefaultImpl() != null) {
                        return Stub.getDefaultImpl().receiveFile(customTabsCallback, uri, int1, bundle);
                    }
                    obtain2.readException();
                    int1 = obtain2.readInt();
                    if (int1 == 0) {
                        b = false;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean requestPostMessageChannel(final ICustomTabsCallback customTabsCallback, final Uri uri) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsService");
                    IBinder binder;
                    if (customTabsCallback != null) {
                        binder = ((IInterface)customTabsCallback).asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    boolean b = true;
                    if (uri != null) {
                        obtain.writeInt(1);
                        ((Parcelable)uri).writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (!this.mRemote.transact(7, obtain, obtain2, 0) && Stub.getDefaultImpl() != null) {
                        return Stub.getDefaultImpl().requestPostMessageChannel(customTabsCallback, uri);
                    }
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        b = false;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean requestPostMessageChannelWithExtras(final ICustomTabsCallback customTabsCallback, final Uri uri, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsService");
                    IBinder binder;
                    if (customTabsCallback != null) {
                        binder = ((IInterface)customTabsCallback).asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    boolean b = true;
                    if (uri != null) {
                        obtain.writeInt(1);
                        ((Parcelable)uri).writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (!this.mRemote.transact(11, obtain, obtain2, 0) && Stub.getDefaultImpl() != null) {
                        return Stub.getDefaultImpl().requestPostMessageChannelWithExtras(customTabsCallback, uri, bundle);
                    }
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        b = false;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean updateVisuals(final ICustomTabsCallback customTabsCallback, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsService");
                    IBinder binder;
                    if (customTabsCallback != null) {
                        binder = ((IInterface)customTabsCallback).asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    boolean b = true;
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (!this.mRemote.transact(6, obtain, obtain2, 0) && Stub.getDefaultImpl() != null) {
                        return Stub.getDefaultImpl().updateVisuals(customTabsCallback, bundle);
                    }
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        b = false;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean validateRelationship(final ICustomTabsCallback customTabsCallback, int int1, final Uri uri, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsService");
                    IBinder binder;
                    if (customTabsCallback != null) {
                        binder = ((IInterface)customTabsCallback).asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    obtain.writeInt(int1);
                    boolean b = true;
                    if (uri != null) {
                        obtain.writeInt(1);
                        ((Parcelable)uri).writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (!this.mRemote.transact(9, obtain, obtain2, 0) && Stub.getDefaultImpl() != null) {
                        return Stub.getDefaultImpl().validateRelationship(customTabsCallback, int1, uri, bundle);
                    }
                    obtain2.readException();
                    int1 = obtain2.readInt();
                    if (int1 == 0) {
                        b = false;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean warmup(final long n) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsService");
                    obtain.writeLong(n);
                    final IBinder mRemote = this.mRemote;
                    boolean b = false;
                    if (!mRemote.transact(2, obtain, obtain2, 0) && Stub.getDefaultImpl() != null) {
                        return Stub.getDefaultImpl().warmup(n);
                    }
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        b = true;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package android.support.customtabs.trusted;

import android.os.Parcel;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.Bundle;
import android.os.IInterface;

public interface ITrustedWebActivityService extends IInterface
{
    Bundle areNotificationsEnabled(final Bundle p0) throws RemoteException;
    
    void cancelNotification(final Bundle p0) throws RemoteException;
    
    Bundle extraCommand(final String p0, final Bundle p1, final IBinder p2) throws RemoteException;
    
    Bundle getActiveNotifications() throws RemoteException;
    
    Bundle getSmallIconBitmap() throws RemoteException;
    
    int getSmallIconId() throws RemoteException;
    
    Bundle notifyNotificationWithChannel(final Bundle p0) throws RemoteException;
    
    public static class Default implements ITrustedWebActivityService
    {
        @Override
        public Bundle areNotificationsEnabled(final Bundle bundle) throws RemoteException {
            return null;
        }
        
        public IBinder asBinder() {
            return null;
        }
        
        @Override
        public void cancelNotification(final Bundle bundle) throws RemoteException {
        }
        
        @Override
        public Bundle extraCommand(final String s, final Bundle bundle, final IBinder binder) throws RemoteException {
            return null;
        }
        
        @Override
        public Bundle getActiveNotifications() throws RemoteException {
            return null;
        }
        
        @Override
        public Bundle getSmallIconBitmap() throws RemoteException {
            return null;
        }
        
        @Override
        public int getSmallIconId() throws RemoteException {
            return 0;
        }
        
        @Override
        public Bundle notifyNotificationWithChannel(final Bundle bundle) throws RemoteException {
            return null;
        }
    }
    
    public abstract static class Stub extends Binder implements ITrustedWebActivityService
    {
        private static final String DESCRIPTOR = "android.support.customtabs.trusted.ITrustedWebActivityService";
        static final int TRANSACTION_areNotificationsEnabled = 6;
        static final int TRANSACTION_cancelNotification = 3;
        static final int TRANSACTION_extraCommand = 9;
        static final int TRANSACTION_getActiveNotifications = 5;
        static final int TRANSACTION_getSmallIconBitmap = 7;
        static final int TRANSACTION_getSmallIconId = 4;
        static final int TRANSACTION_notifyNotificationWithChannel = 2;
        
        public Stub() {
            this.attachInterface((IInterface)this, "android.support.customtabs.trusted.ITrustedWebActivityService");
        }
        
        public static ITrustedWebActivityService asInterface(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("android.support.customtabs.trusted.ITrustedWebActivityService");
            if (queryLocalInterface != null && queryLocalInterface instanceof ITrustedWebActivityService) {
                return (ITrustedWebActivityService)queryLocalInterface;
            }
            return new Proxy(binder);
        }
        
        public static ITrustedWebActivityService getDefaultImpl() {
            return Proxy.sDefaultImpl;
        }
        
        public static boolean setDefaultImpl(final ITrustedWebActivityService sDefaultImpl) {
            if (Proxy.sDefaultImpl != null) {
                throw new IllegalStateException("setDefaultImpl() called twice");
            }
            if (sDefaultImpl != null) {
                Proxy.sDefaultImpl = sDefaultImpl;
                return true;
            }
            return false;
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(int smallIconId, final Parcel parcel, final Parcel parcel2, final int n) throws RemoteException {
            final Bundle bundle = null;
            final Bundle bundle2 = null;
            Bundle bundle3 = null;
            final Bundle bundle4 = null;
            if (smallIconId == 9) {
                parcel.enforceInterface("android.support.customtabs.trusted.ITrustedWebActivityService");
                final String string = parcel.readString();
                if (parcel.readInt() != 0) {
                    bundle3 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                }
                final Bundle extraCommand = this.extraCommand(string, bundle3, parcel.readStrongBinder());
                parcel2.writeNoException();
                if (extraCommand != null) {
                    parcel2.writeInt(1);
                    extraCommand.writeToParcel(parcel2, 1);
                }
                else {
                    parcel2.writeInt(0);
                }
                return true;
            }
            if (smallIconId == 1598968902) {
                parcel2.writeString("android.support.customtabs.trusted.ITrustedWebActivityService");
                return true;
            }
            switch (smallIconId) {
                default: {
                    return super.onTransact(smallIconId, parcel, parcel2, n);
                }
                case 7: {
                    parcel.enforceInterface("android.support.customtabs.trusted.ITrustedWebActivityService");
                    final Bundle smallIconBitmap = this.getSmallIconBitmap();
                    parcel2.writeNoException();
                    if (smallIconBitmap != null) {
                        parcel2.writeInt(1);
                        smallIconBitmap.writeToParcel(parcel2, 1);
                    }
                    else {
                        parcel2.writeInt(0);
                    }
                    return true;
                }
                case 6: {
                    parcel.enforceInterface("android.support.customtabs.trusted.ITrustedWebActivityService");
                    Bundle bundle5 = bundle4;
                    if (parcel.readInt() != 0) {
                        bundle5 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    final Bundle notificationsEnabled = this.areNotificationsEnabled(bundle5);
                    parcel2.writeNoException();
                    if (notificationsEnabled != null) {
                        parcel2.writeInt(1);
                        notificationsEnabled.writeToParcel(parcel2, 1);
                    }
                    else {
                        parcel2.writeInt(0);
                    }
                    return true;
                }
                case 5: {
                    parcel.enforceInterface("android.support.customtabs.trusted.ITrustedWebActivityService");
                    final Bundle activeNotifications = this.getActiveNotifications();
                    parcel2.writeNoException();
                    if (activeNotifications != null) {
                        parcel2.writeInt(1);
                        activeNotifications.writeToParcel(parcel2, 1);
                    }
                    else {
                        parcel2.writeInt(0);
                    }
                    return true;
                }
                case 4: {
                    parcel.enforceInterface("android.support.customtabs.trusted.ITrustedWebActivityService");
                    smallIconId = this.getSmallIconId();
                    parcel2.writeNoException();
                    parcel2.writeInt(smallIconId);
                    return true;
                }
                case 3: {
                    parcel.enforceInterface("android.support.customtabs.trusted.ITrustedWebActivityService");
                    Bundle bundle6 = bundle;
                    if (parcel.readInt() != 0) {
                        bundle6 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    this.cancelNotification(bundle6);
                    parcel2.writeNoException();
                    return true;
                }
                case 2: {
                    parcel.enforceInterface("android.support.customtabs.trusted.ITrustedWebActivityService");
                    Bundle bundle7 = bundle2;
                    if (parcel.readInt() != 0) {
                        bundle7 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    final Bundle notifyNotificationWithChannel = this.notifyNotificationWithChannel(bundle7);
                    parcel2.writeNoException();
                    if (notifyNotificationWithChannel != null) {
                        parcel2.writeInt(1);
                        notifyNotificationWithChannel.writeToParcel(parcel2, 1);
                    }
                    else {
                        parcel2.writeInt(0);
                    }
                    return true;
                }
            }
        }
        
        private static class Proxy implements ITrustedWebActivityService
        {
            public static ITrustedWebActivityService sDefaultImpl;
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            @Override
            public Bundle areNotificationsEnabled(Bundle notificationsEnabled) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.trusted.ITrustedWebActivityService");
                    if (notificationsEnabled != null) {
                        obtain.writeInt(1);
                        notificationsEnabled.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (!this.mRemote.transact(6, obtain, obtain2, 0) && Stub.getDefaultImpl() != null) {
                        notificationsEnabled = Stub.getDefaultImpl().areNotificationsEnabled(notificationsEnabled);
                        return notificationsEnabled;
                    }
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        notificationsEnabled = (Bundle)Bundle.CREATOR.createFromParcel(obtain2);
                    }
                    else {
                        notificationsEnabled = null;
                    }
                    return notificationsEnabled;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            @Override
            public void cancelNotification(final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.trusted.ITrustedWebActivityService");
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (!this.mRemote.transact(3, obtain, obtain2, 0) && Stub.getDefaultImpl() != null) {
                        Stub.getDefaultImpl().cancelNotification(bundle);
                        return;
                    }
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public Bundle extraCommand(final String s, final Bundle bundle, final IBinder binder) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.trusted.ITrustedWebActivityService");
                    obtain.writeString(s);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(binder);
                    if (!this.mRemote.transact(9, obtain, obtain2, 0) && Stub.getDefaultImpl() != null) {
                        return Stub.getDefaultImpl().extraCommand(s, bundle, binder);
                    }
                    obtain2.readException();
                    Bundle bundle2;
                    if (obtain2.readInt() != 0) {
                        bundle2 = (Bundle)Bundle.CREATOR.createFromParcel(obtain2);
                    }
                    else {
                        bundle2 = null;
                    }
                    return bundle2;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public Bundle getActiveNotifications() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.trusted.ITrustedWebActivityService");
                    if (!this.mRemote.transact(5, obtain, obtain2, 0) && Stub.getDefaultImpl() != null) {
                        return Stub.getDefaultImpl().getActiveNotifications();
                    }
                    obtain2.readException();
                    Bundle bundle;
                    if (obtain2.readInt() != 0) {
                        bundle = (Bundle)Bundle.CREATOR.createFromParcel(obtain2);
                    }
                    else {
                        bundle = null;
                    }
                    return bundle;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            public String getInterfaceDescriptor() {
                return "android.support.customtabs.trusted.ITrustedWebActivityService";
            }
            
            @Override
            public Bundle getSmallIconBitmap() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.trusted.ITrustedWebActivityService");
                    if (!this.mRemote.transact(7, obtain, obtain2, 0) && Stub.getDefaultImpl() != null) {
                        return Stub.getDefaultImpl().getSmallIconBitmap();
                    }
                    obtain2.readException();
                    Bundle bundle;
                    if (obtain2.readInt() != 0) {
                        bundle = (Bundle)Bundle.CREATOR.createFromParcel(obtain2);
                    }
                    else {
                        bundle = null;
                    }
                    return bundle;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public int getSmallIconId() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.trusted.ITrustedWebActivityService");
                    if (!this.mRemote.transact(4, obtain, obtain2, 0) && Stub.getDefaultImpl() != null) {
                        return Stub.getDefaultImpl().getSmallIconId();
                    }
                    obtain2.readException();
                    return obtain2.readInt();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public Bundle notifyNotificationWithChannel(Bundle notifyNotificationWithChannel) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.trusted.ITrustedWebActivityService");
                    if (notifyNotificationWithChannel != null) {
                        obtain.writeInt(1);
                        notifyNotificationWithChannel.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (!this.mRemote.transact(2, obtain, obtain2, 0) && Stub.getDefaultImpl() != null) {
                        notifyNotificationWithChannel = Stub.getDefaultImpl().notifyNotificationWithChannel(notifyNotificationWithChannel);
                        return notifyNotificationWithChannel;
                    }
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        notifyNotificationWithChannel = (Bundle)Bundle.CREATOR.createFromParcel(obtain2);
                    }
                    else {
                        notifyNotificationWithChannel = null;
                    }
                    return notifyNotificationWithChannel;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}

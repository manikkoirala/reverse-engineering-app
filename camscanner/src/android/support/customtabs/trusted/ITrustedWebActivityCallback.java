// 
// Decompiled by Procyon v0.6.0
// 

package android.support.customtabs.trusted;

import android.os.Parcel;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.Bundle;
import android.os.IInterface;

public interface ITrustedWebActivityCallback extends IInterface
{
    void onExtraCallback(final String p0, final Bundle p1) throws RemoteException;
    
    public static class Default implements ITrustedWebActivityCallback
    {
        public IBinder asBinder() {
            return null;
        }
        
        @Override
        public void onExtraCallback(final String s, final Bundle bundle) throws RemoteException {
        }
    }
    
    public abstract static class Stub extends Binder implements ITrustedWebActivityCallback
    {
        private static final String DESCRIPTOR = "android.support.customtabs.trusted.ITrustedWebActivityCallback";
        static final int TRANSACTION_onExtraCallback = 2;
        
        public Stub() {
            this.attachInterface((IInterface)this, "android.support.customtabs.trusted.ITrustedWebActivityCallback");
        }
        
        public static ITrustedWebActivityCallback asInterface(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("android.support.customtabs.trusted.ITrustedWebActivityCallback");
            if (queryLocalInterface != null && queryLocalInterface instanceof ITrustedWebActivityCallback) {
                return (ITrustedWebActivityCallback)queryLocalInterface;
            }
            return new Proxy(binder);
        }
        
        public static ITrustedWebActivityCallback getDefaultImpl() {
            return Proxy.sDefaultImpl;
        }
        
        public static boolean setDefaultImpl(final ITrustedWebActivityCallback sDefaultImpl) {
            if (Proxy.sDefaultImpl != null) {
                throw new IllegalStateException("setDefaultImpl() called twice");
            }
            if (sDefaultImpl != null) {
                Proxy.sDefaultImpl = sDefaultImpl;
                return true;
            }
            return false;
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            if (n == 2) {
                parcel.enforceInterface("android.support.customtabs.trusted.ITrustedWebActivityCallback");
                final String string = parcel.readString();
                Bundle bundle;
                if (parcel.readInt() != 0) {
                    bundle = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                }
                else {
                    bundle = null;
                }
                this.onExtraCallback(string, bundle);
                parcel2.writeNoException();
                return true;
            }
            if (n != 1598968902) {
                return super.onTransact(n, parcel, parcel2, n2);
            }
            parcel2.writeString("android.support.customtabs.trusted.ITrustedWebActivityCallback");
            return true;
        }
        
        private static class Proxy implements ITrustedWebActivityCallback
        {
            public static ITrustedWebActivityCallback sDefaultImpl;
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            public String getInterfaceDescriptor() {
                return "android.support.customtabs.trusted.ITrustedWebActivityCallback";
            }
            
            @Override
            public void onExtraCallback(final String s, final Bundle bundle) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.trusted.ITrustedWebActivityCallback");
                    obtain.writeString(s);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (!this.mRemote.transact(2, obtain, obtain2, 0) && Stub.getDefaultImpl() != null) {
                        Stub.getDefaultImpl().onExtraCallback(s, bundle);
                        return;
                    }
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}

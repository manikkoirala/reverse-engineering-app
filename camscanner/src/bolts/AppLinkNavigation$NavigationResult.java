// 
// Decompiled by Procyon v0.6.0
// 

package bolts;

public enum AppLinkNavigation$NavigationResult
{
    private static final AppLinkNavigation$NavigationResult[] $VALUES;
    
    APP("app", true), 
    FAILED("failed", false), 
    WEB("web", true);
    
    private String code;
    private boolean succeeded;
    
    private AppLinkNavigation$NavigationResult(final String code, final boolean succeeded) {
        this.code = code;
        this.succeeded = succeeded;
    }
    
    public String getCode() {
        return this.code;
    }
    
    public boolean isSucceeded() {
        return this.succeeded;
    }
}

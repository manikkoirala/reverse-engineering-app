// 
// Decompiled by Procyon v0.6.0
// 

package bolts;

public class ExecutorException extends RuntimeException
{
    public ExecutorException(final Exception cause) {
        super("An exception was thrown by an Executor", cause);
    }
}

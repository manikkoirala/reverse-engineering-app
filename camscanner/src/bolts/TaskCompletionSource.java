// 
// Decompiled by Procyon v0.6.0
// 

package bolts;

public class TaskCompletionSource<TResult>
{
    private final Task<TResult> \u3007080;
    
    public TaskCompletionSource() {
        this.\u3007080 = new Task<TResult>();
    }
    
    public boolean O8() {
        return this.\u3007080.\u3007O888o0o();
    }
    
    public boolean Oo08(final Exception ex) {
        return this.\u3007080.oo88o8O(ex);
    }
    
    public boolean o\u30070(final TResult tResult) {
        return this.\u3007080.\u3007oo\u3007(tResult);
    }
    
    public void setResult(final TResult tResult) {
        if (this.o\u30070(tResult)) {
            return;
        }
        throw new IllegalStateException("Cannot set the result of a completed task.");
    }
    
    public Task<TResult> \u3007080() {
        return this.\u3007080;
    }
    
    public void \u3007o00\u3007\u3007Oo() {
        if (this.O8()) {
            return;
        }
        throw new IllegalStateException("Cannot cancel a completed task.");
    }
    
    public void \u3007o\u3007(final Exception ex) {
        if (this.Oo08(ex)) {
            return;
        }
        throw new IllegalStateException("Cannot set the error on a completed task.");
    }
}

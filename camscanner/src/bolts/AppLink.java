// 
// Decompiled by Procyon v0.6.0
// 

package bolts;

import java.util.Collections;
import java.util.List;
import android.net.Uri;

public class AppLink
{
    private Uri \u3007080;
    private List<Target> \u3007o00\u3007\u3007Oo;
    private Uri \u3007o\u3007;
    
    public AppLink(final Uri \u3007080, final List<Target> list, final Uri \u3007o\u3007) {
        this.\u3007080 = \u3007080;
        List<Target> emptyList = list;
        if (list == null) {
            emptyList = Collections.emptyList();
        }
        this.\u3007o00\u3007\u3007Oo = emptyList;
        this.\u3007o\u3007 = \u3007o\u3007;
    }
    
    public static class Target
    {
        private final String O8;
        private final Uri \u3007080;
        private final String \u3007o00\u3007\u3007Oo;
        private final String \u3007o\u3007;
        
        public Target(final String \u3007o00\u3007\u3007Oo, final String \u3007o\u3007, final Uri \u3007080, final String o8) {
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007o\u3007 = \u3007o\u3007;
            this.\u3007080 = \u3007080;
            this.O8 = o8;
        }
    }
}

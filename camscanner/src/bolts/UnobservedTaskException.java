// 
// Decompiled by Procyon v0.6.0
// 

package bolts;

public class UnobservedTaskException extends RuntimeException
{
    public UnobservedTaskException(final Throwable cause) {
        super(cause);
    }
}

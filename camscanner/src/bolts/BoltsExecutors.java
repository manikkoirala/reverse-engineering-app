// 
// Decompiled by Procyon v0.6.0
// 

package bolts;

import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ExecutorService;

final class BoltsExecutors
{
    private static final BoltsExecutors O8;
    private final ExecutorService \u3007080;
    private final ScheduledExecutorService \u3007o00\u3007\u3007Oo;
    private final Executor \u3007o\u3007;
    
    static {
        O8 = new BoltsExecutors();
    }
    
    private BoltsExecutors() {
        ExecutorService \u3007080;
        if (!\u3007o\u3007()) {
            \u3007080 = Executors.newCachedThreadPool();
        }
        else {
            \u3007080 = AndroidExecutors.\u3007o00\u3007\u3007Oo();
        }
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = Executors.newSingleThreadScheduledExecutor();
        this.\u3007o\u3007 = new ImmediateExecutor();
    }
    
    public static ExecutorService \u3007080() {
        return BoltsExecutors.O8.\u3007080;
    }
    
    static Executor \u3007o00\u3007\u3007Oo() {
        return BoltsExecutors.O8.\u3007o\u3007;
    }
    
    private static boolean \u3007o\u3007() {
        final String property = System.getProperty("java.runtime.name");
        return property != null && property.toLowerCase(Locale.US).contains("android");
    }
    
    private static class ImmediateExecutor implements Executor
    {
        private ThreadLocal<Integer> o0;
        
        private ImmediateExecutor() {
            this.o0 = new ThreadLocal<Integer>();
        }
        
        private int \u3007080() {
            Integer value;
            if ((value = this.o0.get()) == null) {
                value = 0;
            }
            final int i = value - 1;
            if (i == 0) {
                this.o0.remove();
            }
            else {
                this.o0.set(i);
            }
            return i;
        }
        
        private int \u3007o00\u3007\u3007Oo() {
            Integer value;
            if ((value = this.o0.get()) == null) {
                value = 0;
            }
            final int i = value + 1;
            this.o0.set(i);
            return i;
        }
        
        @Override
        public void execute(final Runnable runnable) {
            Label_0018: {
                if (this.\u3007o00\u3007\u3007Oo() > 15) {
                    break Label_0018;
                }
                try {
                    runnable.run();
                    return;
                    BoltsExecutors.\u3007080().execute(runnable);
                }
                finally {
                    this.\u3007080();
                }
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package bolts;

import java.util.Iterator;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executor;

public class Task<TResult>
{
    private static Task<Boolean> OO0o\u3007\u3007;
    private static final Executor OO0o\u3007\u3007\u3007\u30070;
    private static Task<Boolean> Oooo8o0\u3007;
    public static final ExecutorService \u300780\u3007808\u3007O;
    public static final Executor \u30078o8o\u3007;
    private static Task<?> \u3007O8o08O;
    private static Task<?> \u3007\u3007808\u3007;
    private TResult O8;
    private Exception Oo08;
    private List<Continuation<TResult, Void>> oO80;
    private boolean o\u30070;
    private final Object \u3007080;
    private boolean \u3007o00\u3007\u3007Oo;
    private boolean \u3007o\u3007;
    private UnobservedErrorNotifier \u3007\u3007888;
    
    static {
        \u300780\u3007808\u3007O = BoltsExecutors.\u3007080();
        OO0o\u3007\u3007\u3007\u30070 = BoltsExecutors.\u3007o00\u3007\u3007Oo();
        \u30078o8o\u3007 = AndroidExecutors.\u3007o\u3007();
        Task.\u3007O8o08O = new Task<Object>(null);
        Task.OO0o\u3007\u3007 = new Task<Boolean>(Boolean.TRUE);
        Task.Oooo8o0\u3007 = new Task<Boolean>(Boolean.FALSE);
        Task.\u3007\u3007808\u3007 = new Task<Object>(true);
    }
    
    Task() {
        this.\u3007080 = new Object();
        this.oO80 = new ArrayList<Continuation<TResult, Void>>();
    }
    
    private Task(final TResult tResult) {
        this.\u3007080 = new Object();
        this.oO80 = new ArrayList<Continuation<TResult, Void>>();
        this.\u3007oo\u3007(tResult);
    }
    
    private Task(final boolean b) {
        this.\u3007080 = new Object();
        this.oO80 = new ArrayList<Continuation<TResult, Void>>();
        if (b) {
            this.\u3007O888o0o();
        }
        else {
            this.\u3007oo\u3007(null);
        }
    }
    
    private static <TContinuationResult, TResult> void O8(final bolts.TaskCompletionSource<TContinuationResult> taskCompletionSource, final Continuation<TResult, Task<TContinuationResult>> continuation, final Task<TResult> task, final Executor executor, final CancellationToken cancellationToken) {
        try {
            executor.execute(new Runnable(cancellationToken, taskCompletionSource, continuation, task) {
                final Task OO;
                final bolts.TaskCompletionSource o0;
                final Continuation \u3007OOo8\u30070;
                
                @Override
                public void run() {
                    try {
                        final Task task = this.\u3007OOo8\u30070.then(this.OO);
                        if (task == null) {
                            this.o0.setResult(null);
                        }
                        else {
                            task.o\u30070(new Continuation<TContinuationResult, Void>(this) {
                                final Task$15 \u3007080;
                                
                                public Void \u3007080(final Task<TContinuationResult> task) {
                                    this.\u3007080.getClass();
                                    if (task.\u3007O\u3007()) {
                                        this.\u3007080.o0.\u3007o00\u3007\u3007Oo();
                                    }
                                    else if (task.\u3007\u30078O0\u30078()) {
                                        this.\u3007080.o0.\u3007o\u3007(task.OO0o\u3007\u3007());
                                    }
                                    else {
                                        this.\u3007080.o0.setResult(task.Oooo8o0\u3007());
                                    }
                                    return null;
                                }
                            });
                        }
                    }
                    catch (final Exception ex) {
                        this.o0.\u3007o\u3007(ex);
                    }
                    catch (final CancellationException ex2) {
                        this.o0.\u3007o00\u3007\u3007Oo();
                    }
                }
            });
        }
        catch (final Exception ex) {
            taskCompletionSource.\u3007o\u3007(new ExecutorException(ex));
        }
    }
    
    public static <TResult> TaskCompletionSource OO0o\u3007\u3007\u3007\u30070() {
        return new Task().new TaskCompletionSource();
    }
    
    private static <TContinuationResult, TResult> void Oo08(final bolts.TaskCompletionSource<TContinuationResult> taskCompletionSource, final Continuation<TResult, TContinuationResult> continuation, final Task<TResult> task, final Executor executor, final CancellationToken cancellationToken) {
        try {
            executor.execute(new Runnable(cancellationToken, taskCompletionSource, continuation, task) {
                final Task OO;
                final bolts.TaskCompletionSource o0;
                final Continuation \u3007OOo8\u30070;
                
                @Override
                public void run() {
                    try {
                        this.o0.setResult(this.\u3007OOo8\u30070.then(this.OO));
                    }
                    catch (final Exception ex) {
                        this.o0.\u3007o\u3007(ex);
                    }
                    catch (final CancellationException ex2) {
                        this.o0.\u3007o00\u3007\u3007Oo();
                    }
                }
            });
        }
        catch (final Exception ex) {
            taskCompletionSource.\u3007o\u3007(new ExecutorException(ex));
        }
    }
    
    public static <TResult> Task<TResult> call(final Callable<TResult> callable) {
        return call(callable, Task.OO0o\u3007\u3007\u3007\u30070, null);
    }
    
    public static <TResult> Task<TResult> call(final Callable<TResult> callable, final CancellationToken cancellationToken) {
        return call(callable, Task.OO0o\u3007\u3007\u3007\u30070, cancellationToken);
    }
    
    public static <TResult> Task<TResult> call(final Callable<TResult> callable, final Executor executor) {
        return call(callable, executor, null);
    }
    
    public static <TResult> Task<TResult> call(final Callable<TResult> callable, final Executor executor, final CancellationToken cancellationToken) {
        final bolts.TaskCompletionSource taskCompletionSource = new bolts.TaskCompletionSource();
        try {
            executor.execute(new Runnable(cancellationToken, taskCompletionSource, callable) {
                final bolts.TaskCompletionSource o0;
                final Callable \u3007OOo8\u30070;
                
                @Override
                public void run() {
                    try {
                        this.o0.setResult(this.\u3007OOo8\u30070.call());
                    }
                    catch (final Exception ex) {
                        this.o0.\u3007o\u3007(ex);
                    }
                    catch (final CancellationException ex2) {
                        this.o0.\u3007o00\u3007\u3007Oo();
                    }
                }
            });
        }
        catch (final Exception ex) {
            taskCompletionSource.\u3007o\u3007(new ExecutorException(ex));
        }
        return taskCompletionSource.\u3007080();
    }
    
    private void o800o8O() {
        synchronized (this.\u3007080) {
            for (final Continuation continuation : this.oO80) {
                try {
                    continuation.then(this);
                    continue;
                }
                catch (final Exception cause) {
                    throw new RuntimeException(cause);
                }
                catch (final RuntimeException ex) {
                    throw ex;
                }
                break;
            }
            this.oO80 = null;
        }
    }
    
    public static <TResult> Task<TResult> \u30078o8o\u3007(final Exception ex) {
        final bolts.TaskCompletionSource taskCompletionSource = new bolts.TaskCompletionSource();
        taskCompletionSource.\u3007o\u3007(ex);
        return taskCompletionSource.\u3007080();
    }
    
    public static <TResult> Task<TResult> \u3007O8o08O(final TResult result) {
        if (result == null) {
            return (Task<TResult>)Task.\u3007O8o08O;
        }
        if (result instanceof Boolean) {
            Task<Boolean> task;
            if (result) {
                task = Task.OO0o\u3007\u3007;
            }
            else {
                task = Task.Oooo8o0\u3007;
            }
            return (Task<TResult>)task;
        }
        final bolts.TaskCompletionSource taskCompletionSource = new bolts.TaskCompletionSource();
        taskCompletionSource.setResult(result);
        return taskCompletionSource.\u3007080();
    }
    
    public static <TResult> Task<TResult> \u3007o\u3007() {
        return (Task<TResult>)Task.\u3007\u3007808\u3007;
    }
    
    public static UnobservedExceptionHandler \u3007\u3007808\u3007() {
        return null;
    }
    
    public Exception OO0o\u3007\u3007() {
        synchronized (this.\u3007080) {
            if (this.Oo08 != null) {
                this.o\u30070 = true;
                final UnobservedErrorNotifier \u3007\u3007888 = this.\u3007\u3007888;
                if (\u3007\u3007888 != null) {
                    \u3007\u3007888.\u3007080();
                    this.\u3007\u3007888 = null;
                }
            }
            return this.Oo08;
        }
    }
    
    public <TContinuationResult> Task<TContinuationResult> OoO8(final Continuation<TResult, TContinuationResult> continuation, final Executor executor, final CancellationToken cancellationToken) {
        return this.oO80((Continuation<TResult, Task<TContinuationResult>>)new Continuation<TResult, Task<TContinuationResult>>(this, cancellationToken, continuation) {
            final Continuation \u3007080;
            final Task \u3007o00\u3007\u3007Oo;
            
            public Task<TContinuationResult> \u3007080(final Task<TResult> task) {
                if (task.\u3007\u30078O0\u30078()) {
                    return Task.\u30078o8o\u3007(task.OO0o\u3007\u3007());
                }
                if (task.\u3007O\u3007()) {
                    return Task.\u3007o\u3007();
                }
                return task.o\u30070((Continuation<TResult, TContinuationResult>)this.\u3007080);
            }
        }, executor);
    }
    
    public TResult Oooo8o0\u3007() {
        synchronized (this.\u3007080) {
            return this.O8;
        }
    }
    
    public <TContinuationResult> Task<TContinuationResult> oO80(final Continuation<TResult, Task<TContinuationResult>> continuation, final Executor executor) {
        return this.\u300780\u3007808\u3007O(continuation, executor, null);
    }
    
    boolean oo88o8O(final Exception oo08) {
        synchronized (this.\u3007080) {
            if (this.\u3007o00\u3007\u3007Oo) {
                return false;
            }
            this.\u3007o00\u3007\u3007Oo = true;
            this.Oo08 = oo08;
            this.o\u30070 = false;
            this.\u3007080.notifyAll();
            this.o800o8O();
            if (!this.o\u30070) {
                \u3007\u3007808\u3007();
            }
            return true;
        }
    }
    
    public <TContinuationResult> Task<TContinuationResult> o\u30070(final Continuation<TResult, TContinuationResult> continuation) {
        return this.\u3007\u3007888(continuation, Task.OO0o\u3007\u3007\u3007\u30070, null);
    }
    
    public <TContinuationResult> Task<TContinuationResult> \u30070\u3007O0088o(final Continuation<TResult, TContinuationResult> continuation) {
        return this.OoO8(continuation, Task.OO0o\u3007\u3007\u3007\u30070, null);
    }
    
    public <TContinuationResult> Task<TContinuationResult> \u300780\u3007808\u3007O(final Continuation<TResult, Task<TContinuationResult>> continuation, final Executor executor, final CancellationToken cancellationToken) {
        final bolts.TaskCompletionSource taskCompletionSource = new bolts.TaskCompletionSource();
        synchronized (this.\u3007080) {
            final boolean \u3007o00 = this.\u3007O00();
            if (!\u3007o00) {
                this.oO80.add(new Continuation<TResult, Void>(this, taskCompletionSource, continuation, executor, cancellationToken) {
                    final Task O8;
                    final bolts.TaskCompletionSource \u3007080;
                    final Continuation \u3007o00\u3007\u3007Oo;
                    final Executor \u3007o\u3007;
                    
                    public Void \u3007080(final Task<TResult> task) {
                        O8(this.\u3007080, (Continuation<Object, Task<Object>>)this.\u3007o00\u3007\u3007Oo, task, this.\u3007o\u3007, null);
                        return null;
                    }
                });
            }
            monitorexit(this.\u3007080);
            if (\u3007o00) {
                O8(taskCompletionSource, continuation, this, executor, cancellationToken);
            }
            return taskCompletionSource.\u3007080();
        }
    }
    
    public boolean \u3007O00() {
        synchronized (this.\u3007080) {
            return this.\u3007o00\u3007\u3007Oo;
        }
    }
    
    boolean \u3007O888o0o() {
        synchronized (this.\u3007080) {
            if (this.\u3007o00\u3007\u3007Oo) {
                return false;
            }
            this.\u3007o00\u3007\u3007Oo = true;
            this.\u3007o\u3007 = true;
            this.\u3007080.notifyAll();
            this.o800o8O();
            return true;
        }
    }
    
    public boolean \u3007O\u3007() {
        synchronized (this.\u3007080) {
            return this.\u3007o\u3007;
        }
    }
    
    boolean \u3007oo\u3007(final TResult o8) {
        synchronized (this.\u3007080) {
            if (this.\u3007o00\u3007\u3007Oo) {
                return false;
            }
            this.\u3007o00\u3007\u3007Oo = true;
            this.O8 = o8;
            this.\u3007080.notifyAll();
            this.o800o8O();
            return true;
        }
    }
    
    public <TContinuationResult> Task<TContinuationResult> \u3007\u3007888(final Continuation<TResult, TContinuationResult> continuation, final Executor executor, final CancellationToken cancellationToken) {
        final bolts.TaskCompletionSource taskCompletionSource = new bolts.TaskCompletionSource();
        synchronized (this.\u3007080) {
            final boolean \u3007o00 = this.\u3007O00();
            if (!\u3007o00) {
                this.oO80.add(new Continuation<TResult, Void>(this, taskCompletionSource, continuation, executor, cancellationToken) {
                    final Task O8;
                    final bolts.TaskCompletionSource \u3007080;
                    final Continuation \u3007o00\u3007\u3007Oo;
                    final Executor \u3007o\u3007;
                    
                    public Void \u3007080(final Task<TResult> task) {
                        Oo08(this.\u3007080, (Continuation<Object, Object>)this.\u3007o00\u3007\u3007Oo, task, this.\u3007o\u3007, null);
                        return null;
                    }
                });
            }
            monitorexit(this.\u3007080);
            if (\u3007o00) {
                Oo08(taskCompletionSource, continuation, this, executor, cancellationToken);
            }
            return taskCompletionSource.\u3007080();
        }
    }
    
    public boolean \u3007\u30078O0\u30078() {
        synchronized (this.\u3007080) {
            return this.OO0o\u3007\u3007() != null;
        }
    }
    
    public class TaskCompletionSource extends bolts.TaskCompletionSource<TResult>
    {
        final Task \u3007o00\u3007\u3007Oo;
        
        TaskCompletionSource(final Task \u3007o00\u3007\u3007Oo) {
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        }
    }
    
    public interface UnobservedExceptionHandler
    {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package bolts;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ExecutorService;
import android.annotation.SuppressLint;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.Executor;

final class AndroidExecutors
{
    static final int O8;
    static final int Oo08;
    private static final AndroidExecutors \u3007o00\u3007\u3007Oo;
    private static final int \u3007o\u3007;
    private final Executor \u3007080;
    
    static {
        \u3007o00\u3007\u3007Oo = new AndroidExecutors();
        final int n = \u3007o\u3007 = Runtime.getRuntime().availableProcessors();
        O8 = n + 1;
        Oo08 = n * 2 + 1;
    }
    
    private AndroidExecutors() {
        this.\u3007080 = new UIThreadExecutor();
    }
    
    @SuppressLint({ "NewApi" })
    public static void \u3007080(final ThreadPoolExecutor threadPoolExecutor, final boolean value) {
        threadPoolExecutor.allowCoreThreadTimeOut(value);
    }
    
    public static ExecutorService \u3007o00\u3007\u3007Oo() {
        final ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(AndroidExecutors.O8, AndroidExecutors.Oo08, 1L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
        \u3007080(threadPoolExecutor, true);
        return threadPoolExecutor;
    }
    
    public static Executor \u3007o\u3007() {
        return AndroidExecutors.\u3007o00\u3007\u3007Oo.\u3007080;
    }
    
    private static class UIThreadExecutor implements Executor
    {
        @Override
        public void execute(final Runnable runnable) {
            new Handler(Looper.getMainLooper()).post(runnable);
        }
    }
}

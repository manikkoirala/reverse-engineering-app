// 
// Decompiled by Procyon v0.6.0
// 

package _COROUTINE;

import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class ArtificialStackFrames
{
    @NotNull
    public final StackTraceElement \u3007080() {
        return CoroutineDebuggingKt.\u3007080(new Exception(), _BOUNDARY.class.getSimpleName());
    }
    
    @NotNull
    public final StackTraceElement \u3007o00\u3007\u3007Oo() {
        return CoroutineDebuggingKt.\u3007080(new Exception(), _CREATION.class.getSimpleName());
    }
}

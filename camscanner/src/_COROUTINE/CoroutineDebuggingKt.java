// 
// Decompiled by Procyon v0.6.0
// 

package _COROUTINE;

import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class CoroutineDebuggingKt
{
    @NotNull
    private static final String \u3007080 = "_COROUTINE";
    
    private static final StackTraceElement \u3007o00\u3007\u3007Oo(final Throwable t, final String str) {
        final StackTraceElement stackTraceElement = t.getStackTrace()[0];
        final StringBuilder sb = new StringBuilder();
        sb.append(CoroutineDebuggingKt.\u3007080);
        sb.append('.');
        sb.append(str);
        return new StackTraceElement(sb.toString(), "_", stackTraceElement.getFileName(), stackTraceElement.getLineNumber());
    }
}

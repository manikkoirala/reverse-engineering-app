// 
// Decompiled by Procyon v0.6.0
// 

package a.a.a.a.a;

import android.os.Handler$Callback;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import com.pgl.ssdk.ces.d;
import com.pgl.ssdk.ces.a;
import java.io.ByteArrayOutputStream;
import java.net.Proxy;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import java.net.URL;
import java.util.Locale;
import android.content.Context;
import java.net.HttpURLConnection;
import android.os.Handler;
import android.os.HandlerThread;

public abstract class c
{
    private String a;
    public String b;
    private int c;
    private int d;
    private byte[] e;
    private int f;
    private byte[] g;
    private int h;
    private int i;
    private int j;
    private boolean k;
    private HandlerThread l;
    private Handler m;
    private boolean n;
    private HttpURLConnection o;
    
    public c(final Context context, final String s) {
        this.b = "";
        this.f = -1;
        this.g = null;
        this.h = 10000;
        this.i = 2;
        this.j = 0;
        this.k = false;
        this.n = false;
        this.o = null;
        String a = null;
        Label_0068: {
            if (s != null) {
                a = s;
                if (s.length() > 0) {
                    break Label_0068;
                }
            }
            a = "";
        }
        this.a = a;
    }
    
    private void a(final int n) {
        String value;
        if (n != 1) {
            if (n != 2) {
                value = "";
            }
            else {
                value = "application/octet-stream";
            }
        }
        else {
            value = "application/json; charset=utf-8";
        }
        if (value.length() > 0) {
            this.o.addRequestProperty("Content-Type", value);
        }
        final HttpURLConnection o = this.o;
        final StringBuilder sb = new StringBuilder();
        sb.append("sessionid=");
        sb.append(this.a);
        o.addRequestProperty("Cookie", sb.toString());
        try {
            final String language = Locale.getDefault().getLanguage();
            HttpURLConnection httpURLConnection;
            StringBuilder sb2;
            String str;
            if (language.equalsIgnoreCase("zh")) {
                httpURLConnection = this.o;
                sb2 = new StringBuilder();
                sb2.append(Locale.getDefault().toString());
                sb2.append(",");
                sb2.append(language);
                str = ";q=0.9";
            }
            else {
                httpURLConnection = this.o;
                sb2 = new StringBuilder();
                sb2.append(Locale.getDefault().toString());
                sb2.append(",");
                sb2.append(language);
                str = ";q=0.9,en-US;q=0.6,en;q=0.4";
            }
            sb2.append(str);
            httpURLConnection.addRequestProperty("Accept-Language", sb2.toString());
        }
        finally {}
    }
    
    private boolean a() {
        Label_0386: {
            try {
                final URL url = new URL(this.b);
                URLConnection urlConnection;
                if (this.n) {
                    HttpsURLConnection.setDefaultSSLSocketFactory(SSLContext.getInstance("TLS").getSocketFactory());
                    urlConnection = url.openConnection(Proxy.NO_PROXY);
                }
                else {
                    urlConnection = url.openConnection();
                }
                (this.o = (HttpURLConnection)urlConnection).setConnectTimeout(this.h);
                this.o.setReadTimeout(this.h);
                final int c = this.c;
                String requestMethod;
                if (c != 1) {
                    if (c != 3) {
                        if (c != 4) {
                            if (c != 5) {
                                if (c != 6) {
                                    requestMethod = "GET";
                                }
                                else {
                                    requestMethod = "TRACE";
                                }
                            }
                            else {
                                requestMethod = "HEAD";
                            }
                        }
                        else {
                            requestMethod = "DELETE";
                        }
                    }
                    else {
                        requestMethod = "PUT";
                    }
                }
                else {
                    requestMethod = "POST";
                }
                this.o.setRequestMethod(requestMethod);
                this.a(this.d);
                final byte[] e = this.e;
                if (e != null && e.length > 0) {
                    this.o.setDoOutput(true);
                    final OutputStream outputStream = this.o.getOutputStream();
                    outputStream.write(this.e);
                    outputStream.flush();
                    outputStream.close();
                }
                while (true) {
                    this.o.connect();
                    while (true) {
                        InputStream inputStream;
                        try {
                            this.f = this.o.getResponseCode();
                            inputStream = this.o.getInputStream();
                            try {
                                final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                final byte[] array = new byte[1024];
                                while (true) {
                                    final int read = inputStream.read(array, 0, 1024);
                                    if (read <= 0) {
                                        break;
                                    }
                                    byteArrayOutputStream.write(array, 0, read);
                                }
                                this.g = byteArrayOutputStream.toByteArray();
                                inputStream.close();
                            }
                            finally {}
                        }
                        finally {
                            inputStream = null;
                        }
                        if (inputStream != null) {
                            continue;
                        }
                        break;
                    }
                    break;
                }
                final HttpURLConnection o = this.o;
                boolean b = true;
                final boolean b2 = true;
                if (o != null) {
                    b = b2;
                    break Label_0386;
                }
                break Label_0386;
            }
            finally {
                try {
                    final Throwable t;
                    t.printStackTrace();
                    final HttpURLConnection o2 = this.o;
                    boolean b = false;
                    final boolean b3 = false;
                    if (o2 != null) {
                        b = b3;
                        o2.disconnect();
                        this.o = null;
                    }
                    if (b) {
                        final int f = this.f;
                        final byte[] g = this.g;
                        if (f == 200 && g != null) {
                            try {
                                if (g.length > 0) {
                                    if ((int)com.pgl.ssdk.ces.a.meta(223, (Context)null, (Object)g) == 0) {
                                        com.pgl.ssdk.ces.d.h = true;
                                    }
                                }
                            }
                            finally {
                                com.pgl.ssdk.ces.d.h = false;
                            }
                        }
                    }
                    return b;
                }
                finally {
                    final HttpURLConnection o3 = this.o;
                    if (o3 != null) {
                        o3.disconnect();
                        this.o = null;
                    }
                }
            }
        }
    }
    
    public void a(final int c, final int d, final byte[] e) {
        if (this.k) {
            return;
        }
        this.c = c;
        this.d = d;
        this.e = e;
        this.k = true;
        this.j = 0;
        ((Thread)(this.l = new HandlerThread("request"))).start();
        (this.m = new Handler(this.l.getLooper(), (Handler$Callback)new b(this))).sendEmptyMessage(1);
    }
}

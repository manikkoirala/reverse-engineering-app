// 
// Decompiled by Procyon v0.6.0
// 

package a.a.a.a.a;

import android.os.Message;
import android.os.Handler$Callback;

class b implements Handler$Callback
{
    final c o0;
    
    b(final c o0) {
        this.o0 = o0;
    }
    
    public boolean handleMessage(final Message message) {
        if (this.o0.j >= this.o0.i) {
            this.o0.k = false;
            this.o0.j = 0;
            this.o0.l.quit();
            return false;
        }
        if (this.o0.a()) {
            this.o0.k = false;
            this.o0.j = 0;
            this.o0.l.quit();
            return true;
        }
        this.o0.m.sendEmptyMessageDelayed(1, (long)(this.o0.h * 1));
        this.o0.j++;
        return false;
    }
}

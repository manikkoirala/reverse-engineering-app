// 
// Decompiled by Procyon v0.6.0
// 

package a.a.a.a.b;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class b
{
    private static final Map \u3007080;
    
    static {
        final Map map = \u3007080 = new HashMap();
        final Class<Boolean> type = Boolean.TYPE;
        map.put(Boolean.class, type);
        map.put(Byte.class, Byte.TYPE);
        map.put(Character.class, Character.TYPE);
        map.put(Short.class, Short.TYPE);
        final Class<Integer> type2 = Integer.TYPE;
        map.put(Integer.class, type2);
        final Class<Float> type3 = Float.TYPE;
        map.put(Float.class, type3);
        final Class<Long> type4 = Long.TYPE;
        map.put(Long.class, type4);
        map.put(Double.class, Double.TYPE);
        map.put(type, type);
        final Class<Byte> type5 = Byte.TYPE;
        map.put(type5, type5);
        final Class<Character> type6 = Character.TYPE;
        map.put(type6, type6);
        final Class<Short> type7 = Short.TYPE;
        map.put(type7, type7);
        map.put(type2, type2);
        map.put(type3, type3);
        map.put(type4, type4);
        final Class<Double> type8 = Double.TYPE;
        map.put(type8, type8);
    }
    
    private static Method \u3007080(final Class clazz, final String anObject, final Class... array) {
        final Method[] declaredMethods = clazz.getDeclaredMethods();
        if (anObject != null) {
            while (true) {
                for (final Method method : declaredMethods) {
                    if (method.getName().equals(anObject)) {
                        final Class<?>[] parameterTypes = method.getParameterTypes();
                        boolean b = false;
                        Label_0182: {
                            Label_0179: {
                                Label_0167: {
                                    if (parameterTypes == null) {
                                        if (array == null) {
                                            break Label_0179;
                                        }
                                        if (array.length == 0) {
                                            break Label_0179;
                                        }
                                    }
                                    else if (array == null) {
                                        if (parameterTypes.length == 0) {
                                            break Label_0179;
                                        }
                                    }
                                    else if (parameterTypes.length == array.length) {
                                        for (int j = 0; j < parameterTypes.length; ++j) {
                                            if (!parameterTypes[j].isAssignableFrom(array[j])) {
                                                final Map \u3007080 = a.a.a.a.b.b.\u3007080;
                                                if (!\u3007080.containsKey(parameterTypes[j]) || !((Class<?>)\u3007080.get(parameterTypes[j])).equals(\u3007080.get(array[j]))) {
                                                    break Label_0167;
                                                }
                                            }
                                        }
                                        break Label_0179;
                                    }
                                }
                                b = false;
                                break Label_0182;
                            }
                            b = true;
                        }
                        if (b) {
                            if (method != null) {
                                method.setAccessible(true);
                                return method;
                            }
                            if (clazz.getSuperclass() != null) {
                                return \u3007080(clazz.getSuperclass(), anObject, array);
                            }
                            throw new NoSuchMethodException();
                        }
                    }
                }
                Method method = null;
                continue;
            }
        }
        throw new NullPointerException("Method name must not be null.");
    }
    
    public static void \u3007o00\u3007\u3007Oo(final Class clazz, final String s, final Object... array) {
        final int n = 0;
        Class[] array3;
        if (array != null && array.length > 0) {
            final Class[] array2 = new Class[array.length];
            int n2 = 0;
            while (true) {
                array3 = array2;
                if (n2 >= array.length) {
                    break;
                }
                final Object o = array[n2];
                if (o != null && o instanceof a) {
                    array2[n2] = ((a)o).\u3007080;
                }
                else {
                    Class<? extends a> class1;
                    if (o == null) {
                        class1 = null;
                    }
                    else {
                        class1 = ((a)o).getClass();
                    }
                    array2[n2] = class1;
                }
                ++n2;
            }
        }
        else {
            array3 = null;
        }
        final Method \u3007080 = \u3007080(clazz, s, array3);
        Object[] args;
        if (array != null && array.length > 0) {
            final Object[] array4 = new Object[array.length];
            int n3 = n;
            while (true) {
                args = array4;
                if (n3 >= array.length) {
                    break;
                }
                final Object o2 = array[n3];
                if (o2 != null && o2 instanceof a) {
                    array4[n3] = ((a)o2).\u3007o00\u3007\u3007Oo;
                }
                else {
                    array4[n3] = o2;
                }
                ++n3;
            }
        }
        else {
            args = null;
        }
        \u3007080.invoke(null, args);
    }
    
    public static void \u3007o\u3007(final String className, final String s, final Object... array) {
        try {
            \u3007o00\u3007\u3007Oo(Class.forName(className), s, array);
        }
        catch (final Exception ex) {}
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package a.a.a.a.b;

import android.os.Message;
import android.os.Looper;
import java.lang.ref.WeakReference;
import android.os.Handler;

public class e extends Handler
{
    protected WeakReference o0;
    
    public e(final Looper looper, final d referent) {
        super(looper);
        if (referent != null) {
            this.o0 = new WeakReference((T)referent);
        }
    }
    
    public void handleMessage(final Message message) {
        final WeakReference o0 = this.o0;
        if (o0 == null) {
            return;
        }
        final d d = (d)o0.get();
        if (d != null && message != null) {
            d.a(message);
        }
    }
}

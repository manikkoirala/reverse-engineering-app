// 
// Decompiled by Procyon v0.6.0
// 

package a.a.a.a.b;

import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.io.Serializable;
import java.io.FileOutputStream;
import java.io.Closeable;
import com.pgl.ssdk.ces.e.a;
import android.os.Build;
import java.util.zip.ZipFile;
import java.io.File;
import android.content.Context;
import java.util.ArrayList;
import java.util.List;

public class c
{
    private static List \u3007080;
    
    static {
        c.\u3007080 = new ArrayList();
    }
    
    public static boolean \u3007080(Context context, final String libname) {
        synchronized (c.class) {
            if (c.\u3007080.contains(libname)) {
                return true;
            }
            try {
                System.loadLibrary(libname);
                c.\u3007080.add(libname);
            }
            catch (final UnsatisfiedLinkError absolutePath) {
                String mapLibraryName = System.mapLibraryName(libname);
                final Context context2 = null;
                if (context != null && context.getFilesDir() != null) {
                    final File file = (File)(absolutePath = new File(context.getFilesDir(), "libso"));
                    if (!file.exists()) {
                        absolutePath = file.getAbsolutePath();
                        new File((String)absolutePath).mkdirs();
                        absolutePath = file;
                    }
                }
                else {
                    absolutePath = null;
                }
                File file2;
                if (absolutePath != null) {
                    file2 = new File((File)absolutePath, mapLibraryName);
                }
                else {
                    file2 = null;
                }
                if (file2 == null) {}
                if (file2.exists()) {
                    file2.delete();
                }
                Label_0593: {
                    try {
                        final ZipFile zipFile = new ZipFile(new File(context.getApplicationInfo().sourceDir), 1);
                        try {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("lib/");
                            final String cpu_ABI = Build.CPU_ABI;
                            sb.append(cpu_ABI);
                            sb.append("/");
                            sb.append(System.mapLibraryName(libname));
                            ZipEntry entry;
                            if ((entry = zipFile.getEntry(sb.toString())) == null) {
                                int endIndex = cpu_ABI.indexOf(45);
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("lib/");
                                if (endIndex <= 0) {
                                    endIndex = cpu_ABI.length();
                                }
                                sb2.append(cpu_ABI.substring(0, endIndex));
                                sb2.append("/");
                                sb2.append(System.mapLibraryName(libname));
                                final String string = sb2.toString();
                                entry = zipFile.getEntry(string);
                                if (entry == null) {
                                    final StringBuilder sb3 = new StringBuilder();
                                    sb3.append("Library entry not found:");
                                    sb3.append(string);
                                    sb3.toString();
                                    a.a((Closeable)null);
                                    a.a((Closeable)null);
                                    a.a(zipFile);
                                    break Label_0593;
                                }
                            }
                            file2.createNewFile();
                            final InputStream inputStream = zipFile.getInputStream(entry);
                            try {
                                final FileOutputStream fileOutputStream = new FileOutputStream(file2);
                                try {
                                    final byte[] array = new byte[16384];
                                    while (true) {
                                        final int read = inputStream.read(array);
                                        if (read <= 0) {
                                            break;
                                        }
                                        fileOutputStream.write(array, 0, read);
                                    }
                                    b.\u3007o\u3007("android.os.FileUtils", file2.getAbsolutePath(), 493, -1, -1);
                                    a.a((Closeable)fileOutputStream);
                                    a.a((Closeable)inputStream);
                                    a.a(zipFile);
                                }
                                finally {}
                            }
                            finally {}
                        }
                        finally {}
                    }
                    finally {
                        mapLibraryName = null;
                        absolutePath = null;
                        context = context2;
                    }
                    try {
                        final Throwable t;
                        final Object message = t.getMessage();
                        a.a((Closeable)absolutePath);
                        a.a((Closeable)mapLibraryName);
                        a.a((ZipFile)context);
                        context = (Context)message;
                        if (context != null) {
                            return false;
                        }
                        try {
                            System.load(file2.getAbsolutePath());
                            c.\u3007080.add(libname);
                            return true;
                        }
                        finally {
                            return false;
                        }
                    }
                    finally {
                        a.a((Closeable)absolutePath);
                        a.a((Closeable)mapLibraryName);
                        a.a((ZipFile)context);
                    }
                }
            }
            finally {
                return false;
            }
        }
    }
}

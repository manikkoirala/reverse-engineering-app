// 
// Decompiled by Procyon v0.6.0
// 

package a;

import com.intsig.document.nativelib.PDFium$Bookmark;
import java.util.ArrayList;

public class c
{
    public ArrayList<c> children;
    public int destPageNum;
    public float page_offset_x;
    public float page_offset_y;
    public String title;
    
    public c(final String title, final int destPageNum, final float page_offset_x, final float page_offset_y) {
        this.title = title;
        this.destPageNum = destPageNum;
        this.page_offset_x = page_offset_x;
        this.page_offset_y = page_offset_y;
        this.children = new ArrayList<c>();
    }
    
    public void add(final PDFium$Bookmark e) {
        this.children.add((c)e);
    }
}

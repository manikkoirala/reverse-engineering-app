// 
// Decompiled by Procyon v0.6.0
// 

package a;

import android.graphics.RectF;

public class b
{
    public static final int ANNOT_CARET = 14;
    public static final int ANNOT_CIRCLE = 6;
    public static final int ANNOT_FILEATTACHMENT = 17;
    public static final int ANNOT_FREETEXT = 3;
    public static final int ANNOT_HIGHLIGHT = 9;
    public static final int ANNOT_INK = 15;
    public static final int ANNOT_LINE = 4;
    public static final int ANNOT_LINK = 2;
    public static final int ANNOT_MOVIE = 19;
    public static final int ANNOT_POLYGON = 7;
    public static final int ANNOT_POLYLINE = 8;
    public static final int ANNOT_POPUP = 16;
    public static final int ANNOT_PRINTERMARK = 22;
    public static final int ANNOT_REDACT = 28;
    public static final int ANNOT_RICHMEDIA = 26;
    public static final int ANNOT_SCREEN = 21;
    public static final int ANNOT_SOUND = 18;
    public static final int ANNOT_SQUARE = 5;
    public static final int ANNOT_SQUIGGLY = 11;
    public static final int ANNOT_STAMP = 13;
    public static final int ANNOT_STRIKEOUT = 12;
    public static final int ANNOT_TEXT = 1;
    public static final int ANNOT_THREED = 25;
    public static final int ANNOT_TRAPNET = 23;
    public static final int ANNOT_UNDERLINE = 10;
    public static final int ANNOT_UNKNOWN = 0;
    public static final int ANNOT_WATERMARK = 24;
    public static final int ANNOT_WIDGET = 20;
    public static final int ANNOT_XFAWIDGET = 27;
    public a dest;
    public int id;
    public int pageNum;
    public RectF rect;
    public String text;
    public int type;
    
    public b(final int pageNum, final int type, final int id) {
        this.pageNum = pageNum;
        this.type = type;
        this.id = id;
    }
    
    public b(final int pageNum, final int id, final String \u3007080) {
        this.pageNum = pageNum;
        final a dest = new a();
        this.dest = dest;
        dest.\u3007080 = \u3007080;
        this.id = id;
        this.type = 2;
    }
    
    public b(final int pageNum, final int id, final String text, final int \u3007o00\u3007\u3007Oo, final float \u3007o\u3007, final float o8) {
        this.pageNum = pageNum;
        this.id = id;
        final a dest = new a();
        this.dest = dest;
        this.type = 2;
        this.text = text;
        dest.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        dest.\u3007o\u3007 = \u3007o\u3007;
        dest.O8 = o8;
    }
    
    public b(final int pageNum, final int id, final String text, final String s) {
        this.pageNum = pageNum;
        this.id = id;
        this.text = text;
        this.type = 1;
    }
    
    public void addRect(final float n, final float n2, final float n3, final float n4) {
        this.rect = new RectF(n, n2, n3, n4);
    }
    
    public static final class a
    {
        public float O8;
        public String \u3007080;
        public int \u3007o00\u3007\u3007Oo;
        public float \u3007o\u3007;
        
        @Override
        public final String toString() {
            if (this.\u3007080 != null) {
                final StringBuilder \u3007080 = a.a.\u3007080("url:");
                \u3007080.append(this.\u3007080);
                return \u3007080.toString();
            }
            final StringBuilder \u300781 = a.a.\u3007080("page(");
            \u300781.append(this.\u3007o00\u3007\u3007Oo);
            \u300781.append(" ");
            \u300781.append(this.\u3007o\u3007);
            \u300781.append(",");
            \u300781.append(this.O8);
            \u300781.append(")");
            return \u300781.toString();
        }
    }
}

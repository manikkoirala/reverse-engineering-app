// 
// Decompiled by Procyon v0.6.0
// 

package a;

import android.graphics.RectF;
import java.util.ArrayList;

public class d
{
    public String link;
    public int pageNum;
    public ArrayList<RectF> regions;
    public String text;
    public float x;
    public float y;
    
    public d(final int pageNum, final float x, final float y, final String text) {
        this.text = text;
        this.pageNum = pageNum;
        this.x = x;
        this.y = y;
        this.regions = new ArrayList<RectF>();
    }
    
    public d(final String link, final String text) {
        this.link = link;
        this.text = text;
        this.regions = new ArrayList<RectF>();
        this.pageNum = -1;
    }
    
    public void addRect(final double n, final double n2, final double n3, final double n4) {
        this.regions.add(new RectF((float)n, (float)n2, (float)n3, (float)n4));
    }
    
    public boolean isUrlLink() {
        return this.link != null;
    }
}

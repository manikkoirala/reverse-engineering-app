// 
// Decompiled by Procyon v0.6.0
// 

package androidx.resourceinspection.annotation;

import androidx.annotation.RestrictTo;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.SOURCE)
@Target({ ElementType.TYPE })
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public @interface AppCompatShadowedAttributes {
}

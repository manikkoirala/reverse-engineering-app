// 
// Decompiled by Procyon v0.6.0
// 

package androidx.dynamicanimation.animation;

import androidx.annotation.RequiresApi;
import android.util.FloatProperty;

public abstract class FloatPropertyCompat<T>
{
    final String mPropertyName;
    
    public FloatPropertyCompat(final String mPropertyName) {
        this.mPropertyName = mPropertyName;
    }
    
    @RequiresApi(24)
    public static <T> FloatPropertyCompat<T> createFloatPropertyCompat(final FloatProperty<T> floatProperty) {
        return new FloatPropertyCompat<T>(\u3007080.\u3007080((FloatProperty)floatProperty), floatProperty) {
            final FloatProperty val$property;
            
            @Override
            public float getValue(final T t) {
                return (float)\u3007o00\u3007\u3007Oo.\u3007080(this.val$property, (Object)t);
            }
            
            @Override
            public void setValue(final T t, final float n) {
                \u3007o\u3007.\u3007080(this.val$property, (Object)t, n);
            }
        };
    }
    
    public abstract float getValue(final T p0);
    
    public abstract void setValue(final T p0, final float p1);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.dynamicanimation.animation;

public final class FloatValueHolder
{
    private float mValue;
    
    public FloatValueHolder() {
        this.mValue = 0.0f;
    }
    
    public FloatValueHolder(final float value) {
        this.mValue = 0.0f;
        this.setValue(value);
    }
    
    public float getValue() {
        return this.mValue;
    }
    
    public void setValue(final float mValue) {
        this.mValue = mValue;
    }
}

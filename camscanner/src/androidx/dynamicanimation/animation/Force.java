// 
// Decompiled by Procyon v0.6.0
// 

package androidx.dynamicanimation.animation;

interface Force
{
    float getAcceleration(final float p0, final float p1);
    
    boolean isAtEquilibrium(final float p0, final float p1);
}

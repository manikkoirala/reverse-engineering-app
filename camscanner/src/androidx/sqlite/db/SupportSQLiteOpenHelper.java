// 
// Decompiled by Procyon v0.6.0
// 

package androidx.sqlite.db;

import java.util.Iterator;
import android.text.TextUtils;
import android.content.Context;
import java.util.List;
import android.database.sqlite.SQLiteException;
import java.io.IOException;
import android.util.Pair;
import androidx.annotation.NonNull;
import java.io.File;
import androidx.annotation.RequiresApi;
import androidx.annotation.Nullable;
import java.io.Closeable;

public interface SupportSQLiteOpenHelper extends Closeable
{
    void close();
    
    @Nullable
    String getDatabaseName();
    
    SupportSQLiteDatabase getReadableDatabase();
    
    SupportSQLiteDatabase getWritableDatabase();
    
    @RequiresApi(api = 16)
    void setWriteAheadLoggingEnabled(final boolean p0);
    
    public abstract static class Callback
    {
        private static final String TAG = "SupportSQLite";
        public final int version;
        
        public Callback(final int version) {
            this.version = version;
        }
        
        private void deleteDatabaseFile(final String s) {
            if (s.equalsIgnoreCase(":memory:")) {
                return;
            }
            if (s.trim().length() == 0) {
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("deleting the database file: ");
            sb.append(s);
            try {
                SupportSQLiteCompat.Api16Impl.deleteDatabase(new File(s));
            }
            catch (final Exception ex) {}
        }
        
        public void onConfigure(@NonNull final SupportSQLiteDatabase supportSQLiteDatabase) {
        }
        
        public void onCorruption(@NonNull SupportSQLiteDatabase supportSQLiteDatabase) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Corruption reported by sqlite on database: ");
            sb.append(supportSQLiteDatabase.getPath());
            if (!supportSQLiteDatabase.isOpen()) {
                this.deleteDatabaseFile(supportSQLiteDatabase.getPath());
                return;
            }
            final List list = null;
            while (true) {
                try {
                    List<Pair<String, String>> attachedDbs = null;
                    Label_0134: {
                        try {
                            attachedDbs = supportSQLiteDatabase.getAttachedDbs();
                            break Label_0068;
                        }
                        finally {
                            Label_0079: {
                                break Label_0079;
                                try {
                                    supportSQLiteDatabase.close();
                                    break Label_0134;
                                    iftrue(Label_0121:)(list == null);
                                Block_11:
                                    while (true) {
                                        Label_0131: {
                                            break Label_0131;
                                            iftrue(Label_0131:)(!((Iterator)supportSQLiteDatabase).hasNext());
                                            break Block_11;
                                            Label_0121: {
                                                this.deleteDatabaseFile(supportSQLiteDatabase.getPath());
                                            }
                                        }
                                        supportSQLiteDatabase = (SupportSQLiteDatabase)list.iterator();
                                        continue;
                                    }
                                    this.deleteDatabaseFile((String)((Iterator<Pair>)supportSQLiteDatabase).next().second);
                                }
                                catch (final IOException ex) {}
                            }
                        }
                    }
                    if (attachedDbs != null) {
                        supportSQLiteDatabase = (SupportSQLiteDatabase)attachedDbs.iterator();
                        while (((Iterator)supportSQLiteDatabase).hasNext()) {
                            this.deleteDatabaseFile((String)((Iterator<Pair>)supportSQLiteDatabase).next().second);
                        }
                    }
                    else {
                        this.deleteDatabaseFile(supportSQLiteDatabase.getPath());
                    }
                }
                catch (final SQLiteException ex2) {
                    continue;
                }
                break;
            }
        }
        
        public abstract void onCreate(@NonNull final SupportSQLiteDatabase p0);
        
        public void onDowngrade(@NonNull final SupportSQLiteDatabase supportSQLiteDatabase, final int i, final int j) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Can't downgrade database from version ");
            sb.append(i);
            sb.append(" to ");
            sb.append(j);
            throw new SQLiteException(sb.toString());
        }
        
        public void onOpen(@NonNull final SupportSQLiteDatabase supportSQLiteDatabase) {
        }
        
        public abstract void onUpgrade(@NonNull final SupportSQLiteDatabase p0, final int p1, final int p2);
    }
    
    public static class Configuration
    {
        @NonNull
        public final Callback callback;
        @NonNull
        public final Context context;
        @Nullable
        public final String name;
        public final boolean useNoBackupDirectory;
        
        Configuration(@NonNull final Context context, @Nullable final String s, @NonNull final Callback callback) {
            this(context, s, callback, false);
        }
        
        Configuration(@NonNull final Context context, @Nullable final String name, @NonNull final Callback callback, final boolean useNoBackupDirectory) {
            this.context = context;
            this.name = name;
            this.callback = callback;
            this.useNoBackupDirectory = useNoBackupDirectory;
        }
        
        @NonNull
        public static Builder builder(@NonNull final Context context) {
            return new Builder(context);
        }
        
        public static class Builder
        {
            Callback mCallback;
            Context mContext;
            String mName;
            boolean mUseNoBackupDirectory;
            
            Builder(@NonNull final Context mContext) {
                this.mContext = mContext;
            }
            
            @NonNull
            public Configuration build() {
                if (this.mCallback == null) {
                    throw new IllegalArgumentException("Must set a callback to create the configuration.");
                }
                if (this.mContext == null) {
                    throw new IllegalArgumentException("Must set a non-null context to create the configuration.");
                }
                if (this.mUseNoBackupDirectory && TextUtils.isEmpty((CharSequence)this.mName)) {
                    throw new IllegalArgumentException("Must set a non-null database name to a configuration that uses the no backup directory.");
                }
                return new Configuration(this.mContext, this.mName, this.mCallback, this.mUseNoBackupDirectory);
            }
            
            @NonNull
            public Builder callback(@NonNull final Callback mCallback) {
                this.mCallback = mCallback;
                return this;
            }
            
            @NonNull
            public Builder name(@Nullable final String mName) {
                this.mName = mName;
                return this;
            }
            
            @NonNull
            public Builder noBackupDirectory(final boolean mUseNoBackupDirectory) {
                this.mUseNoBackupDirectory = mUseNoBackupDirectory;
                return this;
            }
        }
    }
    
    public interface Factory
    {
        @NonNull
        SupportSQLiteOpenHelper create(@NonNull final Configuration p0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.sqlite.db;

public interface SupportSQLiteStatement extends SupportSQLiteProgram
{
    void execute();
    
    long executeInsert();
    
    int executeUpdateDelete();
    
    long simpleQueryForLong();
    
    String simpleQueryForString();
}

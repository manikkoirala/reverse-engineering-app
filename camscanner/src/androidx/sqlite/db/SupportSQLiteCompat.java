// 
// Decompiled by Procyon v0.6.0
// 

package androidx.sqlite.db;

import android.content.ContentResolver;
import java.util.List;
import android.os.Bundle;
import android.content.Context;
import android.app.ActivityManager;
import android.net.Uri;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase$CursorFactory;
import android.database.sqlite.SQLiteDatabase;
import java.io.File;
import androidx.annotation.NonNull;
import android.os.CancellationSignal;
import androidx.annotation.RequiresApi;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public final class SupportSQLiteCompat
{
    private SupportSQLiteCompat() {
    }
    
    @RequiresApi(16)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final class Api16Impl
    {
        private Api16Impl() {
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public static void cancel(@NonNull final CancellationSignal cancellationSignal) {
            cancellationSignal.cancel();
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public static CancellationSignal createCancellationSignal() {
            return new CancellationSignal();
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public static boolean deleteDatabase(@NonNull final File file) {
            return SQLiteDatabase.deleteDatabase(file);
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public static void disableWriteAheadLogging(@NonNull final SQLiteDatabase sqLiteDatabase) {
            sqLiteDatabase.disableWriteAheadLogging();
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public static boolean isWriteAheadLoggingEnabled(@NonNull final SQLiteDatabase sqLiteDatabase) {
            return sqLiteDatabase.isWriteAheadLoggingEnabled();
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public static Cursor rawQueryWithFactory(@NonNull final SQLiteDatabase sqLiteDatabase, @NonNull final String s, @NonNull final String[] array, @NonNull final String s2, @NonNull final CancellationSignal cancellationSignal, @NonNull final SQLiteDatabase$CursorFactory sqLiteDatabase$CursorFactory) {
            return sqLiteDatabase.rawQueryWithFactory(sqLiteDatabase$CursorFactory, s, array, s2, cancellationSignal);
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public static void setForeignKeyConstraintsEnabled(@NonNull final SQLiteDatabase sqLiteDatabase, final boolean foreignKeyConstraintsEnabled) {
            sqLiteDatabase.setForeignKeyConstraintsEnabled(foreignKeyConstraintsEnabled);
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public static void setWriteAheadLoggingEnabled(@NonNull final SQLiteOpenHelper sqLiteOpenHelper, final boolean writeAheadLoggingEnabled) {
            sqLiteOpenHelper.setWriteAheadLoggingEnabled(writeAheadLoggingEnabled);
        }
    }
    
    @RequiresApi(19)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final class Api19Impl
    {
        private Api19Impl() {
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public static Uri getNotificationUri(@NonNull final Cursor cursor) {
            return cursor.getNotificationUri();
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public static boolean isLowRamDevice(@NonNull final ActivityManager activityManager) {
            return activityManager.isLowRamDevice();
        }
    }
    
    @RequiresApi(21)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final class Api21Impl
    {
        private Api21Impl() {
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public static File getNoBackupFilesDir(@NonNull final Context context) {
            return context.getNoBackupFilesDir();
        }
    }
    
    @RequiresApi(23)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final class Api23Impl
    {
        private Api23Impl() {
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public static void setExtras(@NonNull final Cursor cursor, @NonNull final Bundle bundle) {
            \u3007080.\u3007080(cursor, bundle);
        }
    }
    
    @RequiresApi(29)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final class Api29Impl
    {
        private Api29Impl() {
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public static List<Uri> getNotificationUris(@NonNull final Cursor cursor) {
            return \u3007o\u3007.\u3007080(cursor);
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public static void setNotificationUris(@NonNull final Cursor cursor, @NonNull final ContentResolver contentResolver, @NonNull final List<Uri> list) {
            \u3007o00\u3007\u3007Oo.\u3007080(cursor, contentResolver, (List)list);
        }
    }
}

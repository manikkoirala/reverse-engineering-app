// 
// Decompiled by Procyon v0.6.0
// 

package androidx.sqlite.db;

public interface SupportSQLiteQuery
{
    void bindTo(final SupportSQLiteProgram p0);
    
    int getArgCount();
    
    String getSql();
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.sqlite.db.framework;

import android.database.sqlite.SQLiteClosable;
import java.util.Iterator;
import java.util.Locale;
import android.os.CancellationSignal;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteProgram;
import android.database.sqlite.SQLiteQuery;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase$CursorFactory;
import android.database.Cursor;
import androidx.sqlite.db.SupportSQLiteQuery;
import android.content.ContentValues;
import android.util.Pair;
import java.util.List;
import android.database.SQLException;
import android.os.Build$VERSION;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.sqlite.db.SupportSQLiteCompat;
import androidx.sqlite.db.SupportSQLiteProgram;
import androidx.sqlite.db.SimpleSQLiteQuery;
import android.text.TextUtils;
import androidx.sqlite.db.SupportSQLiteStatement;
import java.io.IOException;
import android.database.sqlite.SQLiteTransactionListener;
import android.database.sqlite.SQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

class FrameworkSQLiteDatabase implements SupportSQLiteDatabase
{
    private static final String[] CONFLICT_VALUES;
    private static final String[] EMPTY_STRING_ARRAY;
    private final SQLiteDatabase mDelegate;
    
    static {
        CONFLICT_VALUES = new String[] { "", " OR ROLLBACK ", " OR ABORT ", " OR FAIL ", " OR IGNORE ", " OR REPLACE " };
        EMPTY_STRING_ARRAY = new String[0];
    }
    
    FrameworkSQLiteDatabase(final SQLiteDatabase mDelegate) {
        this.mDelegate = mDelegate;
    }
    
    @Override
    public void beginTransaction() {
        this.mDelegate.beginTransaction();
    }
    
    @Override
    public void beginTransactionNonExclusive() {
        this.mDelegate.beginTransactionNonExclusive();
    }
    
    @Override
    public void beginTransactionWithListener(final SQLiteTransactionListener sqLiteTransactionListener) {
        this.mDelegate.beginTransactionWithListener(sqLiteTransactionListener);
    }
    
    @Override
    public void beginTransactionWithListenerNonExclusive(final SQLiteTransactionListener sqLiteTransactionListener) {
        this.mDelegate.beginTransactionWithListenerNonExclusive(sqLiteTransactionListener);
    }
    
    @Override
    public void close() throws IOException {
        ((SQLiteClosable)this.mDelegate).close();
    }
    
    @Override
    public SupportSQLiteStatement compileStatement(final String s) {
        return new FrameworkSQLiteStatement(this.mDelegate.compileStatement(s));
    }
    
    @Override
    public int delete(String string, final String str, final Object[] array) {
        final StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM ");
        sb.append(string);
        if (TextUtils.isEmpty((CharSequence)str)) {
            string = "";
        }
        else {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(" WHERE ");
            sb2.append(str);
            string = sb2.toString();
        }
        sb.append(string);
        final SupportSQLiteStatement compileStatement = this.compileStatement(sb.toString());
        SimpleSQLiteQuery.bind(compileStatement, array);
        return compileStatement.executeUpdateDelete();
    }
    
    @RequiresApi(api = 16)
    @Override
    public void disableWriteAheadLogging() {
        SupportSQLiteCompat.Api16Impl.disableWriteAheadLogging(this.mDelegate);
    }
    
    @Override
    public boolean enableWriteAheadLogging() {
        return this.mDelegate.enableWriteAheadLogging();
    }
    
    @Override
    public void endTransaction() {
        this.mDelegate.endTransaction();
    }
    
    @Override
    public void execPerConnectionSQL(@NonNull final String s, @Nullable final Object[] array) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 30) {
            \u3007080.\u3007080(this.mDelegate, s, array);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("execPerConnectionSQL is not supported on a SDK version lower than 30, current version is: ");
        sb.append(sdk_INT);
        throw new UnsupportedOperationException(sb.toString());
    }
    
    @Override
    public void execSQL(final String s) throws SQLException {
        this.mDelegate.execSQL(s);
    }
    
    @Override
    public void execSQL(final String s, final Object[] array) throws SQLException {
        this.mDelegate.execSQL(s, array);
    }
    
    @Override
    public List<Pair<String, String>> getAttachedDbs() {
        return this.mDelegate.getAttachedDbs();
    }
    
    @Override
    public long getMaximumSize() {
        return this.mDelegate.getMaximumSize();
    }
    
    @Override
    public long getPageSize() {
        return this.mDelegate.getPageSize();
    }
    
    @Override
    public String getPath() {
        return this.mDelegate.getPath();
    }
    
    @Override
    public int getVersion() {
        return this.mDelegate.getVersion();
    }
    
    @Override
    public boolean inTransaction() {
        return this.mDelegate.inTransaction();
    }
    
    @Override
    public long insert(final String s, final int n, final ContentValues contentValues) throws SQLException {
        return this.mDelegate.insertWithOnConflict(s, (String)null, contentValues, n);
    }
    
    @Override
    public boolean isDatabaseIntegrityOk() {
        return this.mDelegate.isDatabaseIntegrityOk();
    }
    
    @Override
    public boolean isDbLockedByCurrentThread() {
        return this.mDelegate.isDbLockedByCurrentThread();
    }
    
    boolean isDelegate(final SQLiteDatabase sqLiteDatabase) {
        return this.mDelegate == sqLiteDatabase;
    }
    
    @Override
    public boolean isExecPerConnectionSQLSupported() {
        return Build$VERSION.SDK_INT >= 30;
    }
    
    @Override
    public boolean isOpen() {
        return this.mDelegate.isOpen();
    }
    
    @Override
    public boolean isReadOnly() {
        return this.mDelegate.isReadOnly();
    }
    
    @RequiresApi(api = 16)
    @Override
    public boolean isWriteAheadLoggingEnabled() {
        return SupportSQLiteCompat.Api16Impl.isWriteAheadLoggingEnabled(this.mDelegate);
    }
    
    @Override
    public boolean needUpgrade(final int n) {
        return this.mDelegate.needUpgrade(n);
    }
    
    @Override
    public Cursor query(final SupportSQLiteQuery supportSQLiteQuery) {
        return this.mDelegate.rawQueryWithFactory((SQLiteDatabase$CursorFactory)new SQLiteDatabase$CursorFactory(this, supportSQLiteQuery) {
            final FrameworkSQLiteDatabase this$0;
            final SupportSQLiteQuery val$supportQuery;
            
            public Cursor newCursor(final SQLiteDatabase sqLiteDatabase, final SQLiteCursorDriver sqLiteCursorDriver, final String s, final SQLiteQuery sqLiteQuery) {
                this.val$supportQuery.bindTo(new FrameworkSQLiteProgram((SQLiteProgram)sqLiteQuery));
                return (Cursor)new SQLiteCursor(sqLiteCursorDriver, s, sqLiteQuery);
            }
        }, supportSQLiteQuery.getSql(), FrameworkSQLiteDatabase.EMPTY_STRING_ARRAY, (String)null);
    }
    
    @RequiresApi(api = 16)
    @Override
    public Cursor query(final SupportSQLiteQuery supportSQLiteQuery, final CancellationSignal cancellationSignal) {
        return SupportSQLiteCompat.Api16Impl.rawQueryWithFactory(this.mDelegate, supportSQLiteQuery.getSql(), FrameworkSQLiteDatabase.EMPTY_STRING_ARRAY, null, cancellationSignal, (SQLiteDatabase$CursorFactory)new SQLiteDatabase$CursorFactory(this, supportSQLiteQuery) {
            final FrameworkSQLiteDatabase this$0;
            final SupportSQLiteQuery val$supportQuery;
            
            public Cursor newCursor(final SQLiteDatabase sqLiteDatabase, final SQLiteCursorDriver sqLiteCursorDriver, final String s, final SQLiteQuery sqLiteQuery) {
                this.val$supportQuery.bindTo(new FrameworkSQLiteProgram((SQLiteProgram)sqLiteQuery));
                return (Cursor)new SQLiteCursor(sqLiteCursorDriver, s, sqLiteQuery);
            }
        });
    }
    
    @Override
    public Cursor query(final String s) {
        return this.query(new SimpleSQLiteQuery(s));
    }
    
    @Override
    public Cursor query(final String s, final Object[] array) {
        return this.query(new SimpleSQLiteQuery(s, array));
    }
    
    @RequiresApi(api = 16)
    @Override
    public void setForeignKeyConstraintsEnabled(final boolean b) {
        SupportSQLiteCompat.Api16Impl.setForeignKeyConstraintsEnabled(this.mDelegate, b);
    }
    
    @Override
    public void setLocale(final Locale locale) {
        this.mDelegate.setLocale(locale);
    }
    
    @Override
    public void setMaxSqlCacheSize(final int maxSqlCacheSize) {
        this.mDelegate.setMaxSqlCacheSize(maxSqlCacheSize);
    }
    
    @Override
    public long setMaximumSize(final long maximumSize) {
        return this.mDelegate.setMaximumSize(maximumSize);
    }
    
    @Override
    public void setPageSize(final long pageSize) {
        this.mDelegate.setPageSize(pageSize);
    }
    
    @Override
    public void setTransactionSuccessful() {
        this.mDelegate.setTransactionSuccessful();
    }
    
    @Override
    public void setVersion(final int version) {
        this.mDelegate.setVersion(version);
    }
    
    @Override
    public int update(String s, int size, final ContentValues contentValues, final String str, final Object[] array) {
        if (contentValues != null && contentValues.size() != 0) {
            final StringBuilder sb = new StringBuilder(120);
            sb.append("UPDATE ");
            sb.append(FrameworkSQLiteDatabase.CONFLICT_VALUES[size]);
            sb.append(s);
            sb.append(" SET ");
            size = contentValues.size();
            int n;
            if (array == null) {
                n = size;
            }
            else {
                n = array.length + size;
            }
            final Object[] array2 = new Object[n];
            final Iterator iterator = contentValues.keySet().iterator();
            int n2 = 0;
            while (iterator.hasNext()) {
                final String str2 = (String)iterator.next();
                if (n2 > 0) {
                    s = ",";
                }
                else {
                    s = "";
                }
                sb.append(s);
                sb.append(str2);
                array2[n2] = contentValues.get(str2);
                sb.append("=?");
                ++n2;
            }
            if (array != null) {
                for (int i = size; i < n; ++i) {
                    array2[i] = array[i - size];
                }
            }
            if (!TextUtils.isEmpty((CharSequence)str)) {
                sb.append(" WHERE ");
                sb.append(str);
            }
            final SupportSQLiteStatement compileStatement = this.compileStatement(sb.toString());
            SimpleSQLiteQuery.bind(compileStatement, array2);
            return compileStatement.executeUpdateDelete();
        }
        throw new IllegalArgumentException("Empty values");
    }
    
    @Override
    public boolean yieldIfContendedSafely() {
        return this.mDelegate.yieldIfContendedSafely();
    }
    
    @Override
    public boolean yieldIfContendedSafely(final long n) {
        return this.mDelegate.yieldIfContendedSafely(n);
    }
}

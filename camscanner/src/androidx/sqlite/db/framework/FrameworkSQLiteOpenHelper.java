// 
// Decompiled by Procyon v0.6.0
// 

package androidx.sqlite.db.framework;

import android.database.sqlite.SQLiteDatabase$CursorFactory;
import android.database.sqlite.SQLiteDatabase;
import android.database.DatabaseErrorHandler;
import androidx.annotation.RequiresApi;
import androidx.sqlite.db.SupportSQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.io.File;
import androidx.sqlite.db.SupportSQLiteCompat;
import android.os.Build$VERSION;
import android.content.Context;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

class FrameworkSQLiteOpenHelper implements SupportSQLiteOpenHelper
{
    private final Callback mCallback;
    private final Context mContext;
    private OpenHelper mDelegate;
    private final Object mLock;
    private final String mName;
    private final boolean mUseNoBackupDirectory;
    private boolean mWriteAheadLoggingEnabled;
    
    FrameworkSQLiteOpenHelper(final Context context, final String s, final Callback callback) {
        this(context, s, callback, false);
    }
    
    FrameworkSQLiteOpenHelper(final Context mContext, final String mName, final Callback mCallback, final boolean mUseNoBackupDirectory) {
        this.mContext = mContext;
        this.mName = mName;
        this.mCallback = mCallback;
        this.mUseNoBackupDirectory = mUseNoBackupDirectory;
        this.mLock = new Object();
    }
    
    private OpenHelper getDelegate() {
        synchronized (this.mLock) {
            if (this.mDelegate == null) {
                final FrameworkSQLiteDatabase[] array = { null };
                if (Build$VERSION.SDK_INT >= 23 && this.mName != null && this.mUseNoBackupDirectory) {
                    this.mDelegate = new OpenHelper(this.mContext, new File(SupportSQLiteCompat.Api21Impl.getNoBackupFilesDir(this.mContext), this.mName).getAbsolutePath(), array, this.mCallback);
                }
                else {
                    this.mDelegate = new OpenHelper(this.mContext, this.mName, array, this.mCallback);
                }
                SupportSQLiteCompat.Api16Impl.setWriteAheadLoggingEnabled(this.mDelegate, this.mWriteAheadLoggingEnabled);
            }
            return this.mDelegate;
        }
    }
    
    @Override
    public void close() {
        this.getDelegate().close();
    }
    
    @Override
    public String getDatabaseName() {
        return this.mName;
    }
    
    @Override
    public SupportSQLiteDatabase getReadableDatabase() {
        return this.getDelegate().getReadableSupportDatabase();
    }
    
    @Override
    public SupportSQLiteDatabase getWritableDatabase() {
        return this.getDelegate().getWritableSupportDatabase();
    }
    
    @RequiresApi(api = 16)
    @Override
    public void setWriteAheadLoggingEnabled(final boolean mWriteAheadLoggingEnabled) {
        synchronized (this.mLock) {
            final OpenHelper mDelegate = this.mDelegate;
            if (mDelegate != null) {
                SupportSQLiteCompat.Api16Impl.setWriteAheadLoggingEnabled(mDelegate, mWriteAheadLoggingEnabled);
            }
            this.mWriteAheadLoggingEnabled = mWriteAheadLoggingEnabled;
        }
    }
    
    static class OpenHelper extends SQLiteOpenHelper
    {
        final Callback mCallback;
        final FrameworkSQLiteDatabase[] mDbRef;
        private boolean mMigrated;
        
        OpenHelper(final Context context, final String s, final FrameworkSQLiteDatabase[] mDbRef, final Callback mCallback) {
            super(context, s, (SQLiteDatabase$CursorFactory)null, mCallback.version, (DatabaseErrorHandler)new DatabaseErrorHandler(mCallback, mDbRef) {
                final Callback val$callback;
                final FrameworkSQLiteDatabase[] val$dbRef;
                
                public void onCorruption(final SQLiteDatabase sqLiteDatabase) {
                    this.val$callback.onCorruption(OpenHelper.getWrappedDb(this.val$dbRef, sqLiteDatabase));
                }
            });
            this.mCallback = mCallback;
            this.mDbRef = mDbRef;
        }
        
        static FrameworkSQLiteDatabase getWrappedDb(final FrameworkSQLiteDatabase[] array, final SQLiteDatabase sqLiteDatabase) {
            final FrameworkSQLiteDatabase frameworkSQLiteDatabase = array[0];
            if (frameworkSQLiteDatabase == null || !frameworkSQLiteDatabase.isDelegate(sqLiteDatabase)) {
                array[0] = new FrameworkSQLiteDatabase(sqLiteDatabase);
            }
            return array[0];
        }
        
        public void close() {
            synchronized (this) {
                super.close();
                this.mDbRef[0] = null;
            }
        }
        
        SupportSQLiteDatabase getReadableSupportDatabase() {
            synchronized (this) {
                this.mMigrated = false;
                final SQLiteDatabase readableDatabase = super.getReadableDatabase();
                if (this.mMigrated) {
                    this.close();
                    return this.getReadableSupportDatabase();
                }
                return this.getWrappedDb(readableDatabase);
            }
        }
        
        FrameworkSQLiteDatabase getWrappedDb(final SQLiteDatabase sqLiteDatabase) {
            return getWrappedDb(this.mDbRef, sqLiteDatabase);
        }
        
        SupportSQLiteDatabase getWritableSupportDatabase() {
            synchronized (this) {
                this.mMigrated = false;
                final SQLiteDatabase writableDatabase = super.getWritableDatabase();
                if (this.mMigrated) {
                    this.close();
                    return this.getWritableSupportDatabase();
                }
                return this.getWrappedDb(writableDatabase);
            }
        }
        
        public void onConfigure(final SQLiteDatabase sqLiteDatabase) {
            this.mCallback.onConfigure(this.getWrappedDb(sqLiteDatabase));
        }
        
        public void onCreate(final SQLiteDatabase sqLiteDatabase) {
            this.mCallback.onCreate(this.getWrappedDb(sqLiteDatabase));
        }
        
        public void onDowngrade(final SQLiteDatabase sqLiteDatabase, final int n, final int n2) {
            this.mMigrated = true;
            this.mCallback.onDowngrade(this.getWrappedDb(sqLiteDatabase), n, n2);
        }
        
        public void onOpen(final SQLiteDatabase sqLiteDatabase) {
            if (!this.mMigrated) {
                this.mCallback.onOpen(this.getWrappedDb(sqLiteDatabase));
            }
        }
        
        public void onUpgrade(final SQLiteDatabase sqLiteDatabase, final int n, final int n2) {
            this.mMigrated = true;
            this.mCallback.onUpgrade(this.getWrappedDb(sqLiteDatabase), n, n2);
        }
    }
}

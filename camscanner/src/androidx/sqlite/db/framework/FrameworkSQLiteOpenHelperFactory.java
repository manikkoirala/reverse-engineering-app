// 
// Decompiled by Procyon v0.6.0
// 

package androidx.sqlite.db.framework;

import androidx.annotation.NonNull;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

public final class FrameworkSQLiteOpenHelperFactory implements Factory
{
    @NonNull
    @Override
    public SupportSQLiteOpenHelper create(@NonNull final Configuration configuration) {
        return new FrameworkSQLiteOpenHelper(configuration.context, configuration.name, configuration.callback, configuration.useNoBackupDirectory);
    }
}

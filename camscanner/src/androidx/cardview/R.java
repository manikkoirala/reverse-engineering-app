// 
// Decompiled by Procyon v0.6.0
// 

package androidx.cardview;

public final class R
{
    private R() {
    }
    
    public static final class attr
    {
        public static final int cardBackgroundColor = 2130968811;
        public static final int cardCornerRadius = 2130968812;
        public static final int cardElevation = 2130968813;
        public static final int cardMaxElevation = 2130968815;
        public static final int cardPreventCornerOverlap = 2130968816;
        public static final int cardUseCompatPadding = 2130968817;
        public static final int cardViewStyle = 2130968818;
        public static final int contentPadding = 2130969000;
        public static final int contentPaddingBottom = 2130969001;
        public static final int contentPaddingLeft = 2130969003;
        public static final int contentPaddingRight = 2130969004;
        public static final int contentPaddingTop = 2130969006;
        
        private attr() {
        }
    }
    
    public static final class color
    {
        public static final int cardview_dark_background = 2131099782;
        public static final int cardview_light_background = 2131099783;
        public static final int cardview_shadow_end_color = 2131099784;
        public static final int cardview_shadow_start_color = 2131099785;
        
        private color() {
        }
    }
    
    public static final class dimen
    {
        public static final int cardview_compat_inset_shadow = 2131165411;
        public static final int cardview_default_elevation = 2131165412;
        public static final int cardview_default_radius = 2131165413;
        
        private dimen() {
        }
    }
    
    public static final class style
    {
        public static final int Base_CardView = 2132017215;
        public static final int CardView = 2132017542;
        public static final int CardView_Dark = 2132017543;
        public static final int CardView_Light = 2132017544;
        
        private style() {
        }
    }
    
    public static final class styleable
    {
        public static final int[] CardView;
        public static final int CardView_android_minHeight = 1;
        public static final int CardView_android_minWidth = 0;
        public static final int CardView_cardBackgroundColor = 2;
        public static final int CardView_cardCornerRadius = 3;
        public static final int CardView_cardElevation = 4;
        public static final int CardView_cardMaxElevation = 5;
        public static final int CardView_cardPreventCornerOverlap = 6;
        public static final int CardView_cardUseCompatPadding = 7;
        public static final int CardView_contentPadding = 8;
        public static final int CardView_contentPaddingBottom = 9;
        public static final int CardView_contentPaddingLeft = 10;
        public static final int CardView_contentPaddingRight = 11;
        public static final int CardView_contentPaddingTop = 12;
        
        static {
            CardView = new int[] { 16843071, 16843072, 2130968811, 2130968812, 2130968813, 2130968815, 2130968816, 2130968817, 2130969000, 2130969001, 2130969003, 2130969004, 2130969006 };
        }
        
        private styleable() {
        }
    }
}

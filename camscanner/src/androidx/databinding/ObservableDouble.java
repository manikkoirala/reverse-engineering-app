// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding;

import android.os.Parcel;
import android.os.Parcelable$Creator;
import java.io.Serializable;
import android.os.Parcelable;

public class ObservableDouble extends BaseObservableField implements Parcelable, Serializable
{
    public static final Parcelable$Creator<ObservableDouble> CREATOR;
    static final long serialVersionUID = 1L;
    private double mValue;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<ObservableDouble>() {
            public ObservableDouble createFromParcel(final Parcel parcel) {
                return new ObservableDouble(parcel.readDouble());
            }
            
            public ObservableDouble[] newArray(final int n) {
                return new ObservableDouble[n];
            }
        };
    }
    
    public ObservableDouble() {
    }
    
    public ObservableDouble(final double mValue) {
        this.mValue = mValue;
    }
    
    public ObservableDouble(final Observable... array) {
        super(array);
    }
    
    public int describeContents() {
        return 0;
    }
    
    public double get() {
        return this.mValue;
    }
    
    public void set(final double mValue) {
        if (mValue != this.mValue) {
            this.mValue = mValue;
            this.notifyChange();
        }
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeDouble(this.mValue);
    }
}

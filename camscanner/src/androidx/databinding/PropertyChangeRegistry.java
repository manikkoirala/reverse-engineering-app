// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding;

import androidx.annotation.NonNull;

public class PropertyChangeRegistry extends CallbackRegistry<Observable.OnPropertyChangedCallback, Observable, Void>
{
    private static final NotifierCallback<Observable.OnPropertyChangedCallback, Observable, Void> NOTIFIER_CALLBACK;
    
    static {
        NOTIFIER_CALLBACK = new NotifierCallback<Observable.OnPropertyChangedCallback, Observable, Void>() {
            public void onNotifyCallback(final Observable.OnPropertyChangedCallback onPropertyChangedCallback, final Observable observable, final int n, final Void void1) {
                onPropertyChangedCallback.onPropertyChanged(observable, n);
            }
        };
    }
    
    public PropertyChangeRegistry() {
        super(PropertyChangeRegistry.NOTIFIER_CALLBACK);
    }
    
    public void notifyChange(@NonNull final Observable observable, final int n) {
        ((CallbackRegistry<C, Observable, Void>)this).notifyCallbacks(observable, n, null);
    }
}

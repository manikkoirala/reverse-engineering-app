// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;

public class MapChangeRegistry extends CallbackRegistry<ObservableMap.OnMapChangedCallback, ObservableMap, Object>
{
    private static NotifierCallback<ObservableMap.OnMapChangedCallback, ObservableMap, Object> NOTIFIER_CALLBACK;
    
    static {
        MapChangeRegistry.NOTIFIER_CALLBACK = new NotifierCallback<ObservableMap.OnMapChangedCallback, ObservableMap, Object>() {
            public void onNotifyCallback(final ObservableMap.OnMapChangedCallback onMapChangedCallback, final ObservableMap observableMap, final int n, final Object o) {
                onMapChangedCallback.onMapChanged(observableMap, o);
            }
        };
    }
    
    public MapChangeRegistry() {
        super(MapChangeRegistry.NOTIFIER_CALLBACK);
    }
    
    public void notifyChange(@NonNull final ObservableMap observableMap, @Nullable final Object o) {
        ((CallbackRegistry<C, ObservableMap, Object>)this).notifyCallbacks(observableMap, 0, o);
    }
}

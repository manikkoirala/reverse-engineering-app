// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding;

import android.os.Parcel;
import android.os.Parcelable$Creator;
import java.io.Serializable;
import android.os.Parcelable;

public class ObservableFloat extends BaseObservableField implements Parcelable, Serializable
{
    public static final Parcelable$Creator<ObservableFloat> CREATOR;
    static final long serialVersionUID = 1L;
    private float mValue;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<ObservableFloat>() {
            public ObservableFloat createFromParcel(final Parcel parcel) {
                return new ObservableFloat(parcel.readFloat());
            }
            
            public ObservableFloat[] newArray(final int n) {
                return new ObservableFloat[n];
            }
        };
    }
    
    public ObservableFloat() {
    }
    
    public ObservableFloat(final float mValue) {
        this.mValue = mValue;
    }
    
    public ObservableFloat(final Observable... array) {
        super(array);
    }
    
    public int describeContents() {
        return 0;
    }
    
    public float get() {
        return this.mValue;
    }
    
    public void set(final float mValue) {
        if (mValue != this.mValue) {
            this.mValue = mValue;
            this.notifyChange();
        }
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeFloat(this.mValue);
    }
}

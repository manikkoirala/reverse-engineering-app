// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding;

import java.util.Collection;
import java.util.ArrayList;

public class ObservableArrayList<T> extends ArrayList<T> implements ObservableList<T>
{
    private transient ListChangeRegistry mListeners;
    
    public ObservableArrayList() {
        this.mListeners = new ListChangeRegistry();
    }
    
    private void notifyAdd(final int n, final int n2) {
        final ListChangeRegistry mListeners = this.mListeners;
        if (mListeners != null) {
            mListeners.notifyInserted(this, n, n2);
        }
    }
    
    private void notifyRemove(final int n, final int n2) {
        final ListChangeRegistry mListeners = this.mListeners;
        if (mListeners != null) {
            mListeners.notifyRemoved(this, n, n2);
        }
    }
    
    @Override
    public void add(final int index, final T element) {
        super.add(index, element);
        this.notifyAdd(index, 1);
    }
    
    @Override
    public boolean add(final T e) {
        super.add(e);
        this.notifyAdd(this.size() - 1, 1);
        return true;
    }
    
    @Override
    public boolean addAll(final int index, final Collection<? extends T> c) {
        final boolean addAll = super.addAll(index, c);
        if (addAll) {
            this.notifyAdd(index, c.size());
        }
        return addAll;
    }
    
    @Override
    public boolean addAll(final Collection<? extends T> c) {
        final int size = this.size();
        final boolean addAll = super.addAll(c);
        if (addAll) {
            this.notifyAdd(size, this.size() - size);
        }
        return addAll;
    }
    
    @Override
    public void addOnListChangedCallback(final OnListChangedCallback onListChangedCallback) {
        if (this.mListeners == null) {
            this.mListeners = new ListChangeRegistry();
        }
        ((CallbackRegistry<OnListChangedCallback, T, A>)this.mListeners).add(onListChangedCallback);
    }
    
    @Override
    public void clear() {
        final int size = this.size();
        super.clear();
        if (size != 0) {
            this.notifyRemove(0, size);
        }
    }
    
    @Override
    public T remove(final int index) {
        final T remove = super.remove(index);
        this.notifyRemove(index, 1);
        return remove;
    }
    
    @Override
    public boolean remove(final Object o) {
        final int index = this.indexOf(o);
        if (index >= 0) {
            this.remove(index);
            return true;
        }
        return false;
    }
    
    @Override
    public void removeOnListChangedCallback(final OnListChangedCallback onListChangedCallback) {
        final ListChangeRegistry mListeners = this.mListeners;
        if (mListeners != null) {
            ((CallbackRegistry<OnListChangedCallback, T, A>)mListeners).remove(onListChangedCallback);
        }
    }
    
    @Override
    protected void removeRange(final int fromIndex, final int toIndex) {
        super.removeRange(fromIndex, toIndex);
        this.notifyRemove(fromIndex, toIndex - fromIndex);
    }
    
    @Override
    public T set(final int index, final T element) {
        final T set = super.set(index, element);
        final ListChangeRegistry mListeners = this.mListeners;
        if (mListeners != null) {
            mListeners.notifyChanged(this, index, 1);
        }
        return set;
    }
}

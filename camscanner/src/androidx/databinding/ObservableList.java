// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding;

import java.util.List;

public interface ObservableList<T> extends List<T>
{
    void addOnListChangedCallback(final OnListChangedCallback<? extends ObservableList<T>> p0);
    
    void removeOnListChangedCallback(final OnListChangedCallback<? extends ObservableList<T>> p0);
    
    public abstract static class OnListChangedCallback<T extends ObservableList>
    {
        public abstract void onChanged(final T p0);
        
        public abstract void onItemRangeChanged(final T p0, final int p1, final int p2);
        
        public abstract void onItemRangeInserted(final T p0, final int p1, final int p2);
        
        public abstract void onItemRangeMoved(final T p0, final int p1, final int p2, final int p3);
        
        public abstract void onItemRangeRemoved(final T p0, final int p1, final int p2);
    }
}

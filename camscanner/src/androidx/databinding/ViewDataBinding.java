// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding;

import androidx.lifecycle.OnLifecycleEvent;
import java.lang.ref.WeakReference;
import androidx.lifecycle.Observer;
import androidx.lifecycle.LiveData;
import androidx.annotation.MainThread;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.Lifecycle;
import java.lang.ref.Reference;
import androidx.annotation.RestrictTo;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.util.SparseBooleanArray;
import java.util.List;
import android.util.SparseArray;
import android.util.LongSparseArray;
import android.util.SparseLongArray;
import android.util.SparseIntArray;
import java.util.Map;
import android.graphics.drawable.Drawable;
import android.content.res.ColorStateList;
import androidx.core.content.\u3007o00\u3007\u3007Oo;
import androidx.databinding.library.R;
import android.view.ViewGroup;
import android.text.TextUtils;
import android.os.Looper;
import android.annotation.TargetApi;
import android.os.Build$VERSION;
import android.os.Handler;
import android.view.View;
import androidx.lifecycle.LifecycleOwner;
import android.view.Choreographer$FrameCallback;
import android.view.Choreographer;
import java.lang.ref.ReferenceQueue;
import android.view.View$OnAttachStateChangeListener;
import androidx.viewbinding.ViewBinding;

public abstract class ViewDataBinding extends BaseObservable implements ViewBinding
{
    private static final int BINDING_NUMBER_START = 8;
    public static final String BINDING_TAG_PREFIX = "binding_";
    private static final CreateWeakListener CREATE_LIST_LISTENER;
    private static final CreateWeakListener CREATE_LIVE_DATA_LISTENER;
    private static final CreateWeakListener CREATE_MAP_LISTENER;
    private static final CreateWeakListener CREATE_PROPERTY_LISTENER;
    private static final int HALTED = 2;
    private static final int REBIND = 1;
    private static final CallbackRegistry.NotifierCallback<OnRebindCallback, ViewDataBinding, Void> REBIND_NOTIFIER;
    private static final int REBOUND = 3;
    private static final View$OnAttachStateChangeListener ROOT_REATTACHED_LISTENER;
    static int SDK_INT;
    private static final boolean USE_CHOREOGRAPHER;
    private static final ReferenceQueue<ViewDataBinding> sReferenceQueue;
    protected final DataBindingComponent mBindingComponent;
    private Choreographer mChoreographer;
    private ViewDataBinding mContainingBinding;
    private final Choreographer$FrameCallback mFrameCallback;
    private boolean mInLiveDataRegisterObserver;
    private boolean mIsExecutingPendingBindings;
    private LifecycleOwner mLifecycleOwner;
    private WeakListener[] mLocalFieldObservers;
    private OnStartListener mOnStartListener;
    private boolean mPendingRebind;
    private CallbackRegistry<OnRebindCallback, ViewDataBinding, Void> mRebindCallbacks;
    private boolean mRebindHalted;
    private final Runnable mRebindRunnable;
    private final View mRoot;
    private Handler mUIThreadHandler;
    
    static {
        ViewDataBinding.SDK_INT = Build$VERSION.SDK_INT;
        USE_CHOREOGRAPHER = true;
        CREATE_PROPERTY_LISTENER = (CreateWeakListener)new CreateWeakListener() {
            @Override
            public WeakListener create(final ViewDataBinding viewDataBinding, final int n) {
                return new WeakPropertyListener(viewDataBinding, n).getListener();
            }
        };
        CREATE_LIST_LISTENER = (CreateWeakListener)new CreateWeakListener() {
            @Override
            public WeakListener create(final ViewDataBinding viewDataBinding, final int n) {
                return new WeakListListener(viewDataBinding, n).getListener();
            }
        };
        CREATE_MAP_LISTENER = (CreateWeakListener)new CreateWeakListener() {
            @Override
            public WeakListener create(final ViewDataBinding viewDataBinding, final int n) {
                return new WeakMapListener(viewDataBinding, n).getListener();
            }
        };
        CREATE_LIVE_DATA_LISTENER = (CreateWeakListener)new CreateWeakListener() {
            @Override
            public WeakListener create(final ViewDataBinding viewDataBinding, final int n) {
                return new LiveDataListener(viewDataBinding, n).getListener();
            }
        };
        REBIND_NOTIFIER = new CallbackRegistry.NotifierCallback<OnRebindCallback, ViewDataBinding, Void>() {
            public void onNotifyCallback(final OnRebindCallback onRebindCallback, final ViewDataBinding viewDataBinding, final int n, final Void void1) {
                if (n != 1) {
                    if (n != 2) {
                        if (n == 3) {
                            onRebindCallback.onBound(viewDataBinding);
                        }
                    }
                    else {
                        onRebindCallback.onCanceled(viewDataBinding);
                    }
                }
                else if (!onRebindCallback.onPreBind(viewDataBinding)) {
                    viewDataBinding.mRebindHalted = true;
                }
            }
        };
        sReferenceQueue = new ReferenceQueue<ViewDataBinding>();
        ROOT_REATTACHED_LISTENER = (View$OnAttachStateChangeListener)new View$OnAttachStateChangeListener() {
            @TargetApi(19)
            public void onViewAttachedToWindow(final View view) {
                ViewDataBinding.getBinding(view).mRebindRunnable.run();
                view.removeOnAttachStateChangeListener((View$OnAttachStateChangeListener)this);
            }
            
            public void onViewDetachedFromWindow(final View view) {
            }
        };
    }
    
    protected ViewDataBinding(final DataBindingComponent mBindingComponent, final View mRoot, final int n) {
        this.mRebindRunnable = new Runnable() {
            final ViewDataBinding this$0;
            
            @Override
            public void run() {
                synchronized (this) {
                    this.this$0.mPendingRebind = false;
                    monitorexit(this);
                    processReferenceQueue();
                    if (!this.this$0.mRoot.isAttachedToWindow()) {
                        this.this$0.mRoot.removeOnAttachStateChangeListener(ViewDataBinding.ROOT_REATTACHED_LISTENER);
                        this.this$0.mRoot.addOnAttachStateChangeListener(ViewDataBinding.ROOT_REATTACHED_LISTENER);
                        return;
                    }
                    this.this$0.executePendingBindings();
                }
            }
        };
        this.mPendingRebind = false;
        this.mRebindHalted = false;
        this.mBindingComponent = mBindingComponent;
        this.mLocalFieldObservers = new WeakListener[n];
        this.mRoot = mRoot;
        if (Looper.myLooper() != null) {
            if (ViewDataBinding.USE_CHOREOGRAPHER) {
                this.mChoreographer = Choreographer.getInstance();
                this.mFrameCallback = (Choreographer$FrameCallback)new Choreographer$FrameCallback(this) {
                    final ViewDataBinding this$0;
                    
                    public void doFrame(final long n) {
                        this.this$0.mRebindRunnable.run();
                    }
                };
            }
            else {
                this.mFrameCallback = null;
                this.mUIThreadHandler = new Handler(Looper.myLooper());
            }
            return;
        }
        throw new IllegalStateException("DataBinding must be created in view's UI Thread");
    }
    
    protected ViewDataBinding(final Object o, final View view, final int n) {
        this(checkAndCastToBindingComponent(o), view, n);
    }
    
    protected static ViewDataBinding bind(final Object o, final View view, final int n) {
        return DataBindingUtil.bind(checkAndCastToBindingComponent(o), view, n);
    }
    
    private static DataBindingComponent checkAndCastToBindingComponent(final Object o) {
        if (o == null) {
            return null;
        }
        if (o instanceof DataBindingComponent) {
            return (DataBindingComponent)o;
        }
        throw new IllegalArgumentException("The provided bindingComponent parameter must be an instance of DataBindingComponent. See  https://issuetracker.google.com/issues/116541301 for details of why this parameter is not defined as DataBindingComponent");
    }
    
    private void executeBindingsInternal() {
        if (this.mIsExecutingPendingBindings) {
            this.requestRebind();
            return;
        }
        if (!this.hasPendingBindings()) {
            return;
        }
        this.mIsExecutingPendingBindings = true;
        this.mRebindHalted = false;
        final CallbackRegistry<OnRebindCallback, ViewDataBinding, Void> mRebindCallbacks = this.mRebindCallbacks;
        if (mRebindCallbacks != null) {
            mRebindCallbacks.notifyCallbacks(this, 1, null);
            if (this.mRebindHalted) {
                this.mRebindCallbacks.notifyCallbacks(this, 2, null);
            }
        }
        if (!this.mRebindHalted) {
            this.executeBindings();
            final CallbackRegistry<OnRebindCallback, ViewDataBinding, Void> mRebindCallbacks2 = this.mRebindCallbacks;
            if (mRebindCallbacks2 != null) {
                mRebindCallbacks2.notifyCallbacks(this, 3, null);
            }
        }
        this.mIsExecutingPendingBindings = false;
    }
    
    protected static void executeBindingsOn(final ViewDataBinding viewDataBinding) {
        viewDataBinding.executeBindingsInternal();
    }
    
    private static int findIncludeIndex(final String s, int i, final IncludedLayouts includedLayouts, int length) {
        final CharSequence subSequence = s.subSequence(s.indexOf(47) + 1, s.length() - 2);
        final String[] array = includedLayouts.layouts[length];
        for (length = array.length; i < length; ++i) {
            if (TextUtils.equals(subSequence, (CharSequence)array[i])) {
                return i;
            }
        }
        return -1;
    }
    
    private static int findLastMatching(final ViewGroup viewGroup, int i) {
        final String s = (String)viewGroup.getChildAt(i).getTag();
        final String substring = s.substring(0, s.length() - 1);
        final int length = substring.length();
        final int childCount = viewGroup.getChildCount();
        final int n = i + 1;
        int n2 = i;
        View child;
        String s2;
        int n3;
        for (i = n; i < childCount; ++i, n2 = n3) {
            child = viewGroup.getChildAt(i);
            if (child.getTag() instanceof String) {
                s2 = (String)child.getTag();
            }
            else {
                s2 = null;
            }
            n3 = n2;
            if (s2 != null) {
                n3 = n2;
                if (s2.startsWith(substring)) {
                    if (s2.length() == s.length() && s2.charAt(s2.length() - 1) == '0') {
                        return n2;
                    }
                    n3 = n2;
                    if (isNumeric(s2, length)) {
                        n3 = i;
                    }
                }
            }
        }
        return n2;
    }
    
    static ViewDataBinding getBinding(final View view) {
        if (view != null) {
            return (ViewDataBinding)view.getTag(R.id.dataBinding);
        }
        return null;
    }
    
    public static int getBuildSdkInt() {
        return ViewDataBinding.SDK_INT;
    }
    
    protected static int getColorFromResource(final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 23) {
            return \u3007o00\u3007\u3007Oo.\u3007080(view.getContext(), n);
        }
        return view.getResources().getColor(n);
    }
    
    protected static ColorStateList getColorStateListFromResource(final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 23) {
            return \u3007080.\u3007080(view.getContext(), n);
        }
        return view.getResources().getColorStateList(n);
    }
    
    protected static Drawable getDrawableFromResource(final View view, final int n) {
        return view.getContext().getDrawable(n);
    }
    
    protected static <K, T> T getFrom(final Map<K, T> map, final K k) {
        if (map == null) {
            return null;
        }
        return map.get(k);
    }
    
    protected static byte getFromArray(final byte[] array, final int n) {
        if (array != null && n >= 0 && n < array.length) {
            return array[n];
        }
        return 0;
    }
    
    protected static char getFromArray(final char[] array, final int n) {
        if (array != null && n >= 0 && n < array.length) {
            return array[n];
        }
        return '\0';
    }
    
    protected static double getFromArray(final double[] array, final int n) {
        if (array != null && n >= 0 && n < array.length) {
            return array[n];
        }
        return 0.0;
    }
    
    protected static float getFromArray(final float[] array, final int n) {
        if (array != null && n >= 0 && n < array.length) {
            return array[n];
        }
        return 0.0f;
    }
    
    protected static int getFromArray(final int[] array, final int n) {
        if (array != null && n >= 0 && n < array.length) {
            return array[n];
        }
        return 0;
    }
    
    protected static long getFromArray(final long[] array, final int n) {
        if (array != null && n >= 0 && n < array.length) {
            return array[n];
        }
        return 0L;
    }
    
    protected static <T> T getFromArray(final T[] array, final int n) {
        if (array != null && n >= 0 && n < array.length) {
            return array[n];
        }
        return null;
    }
    
    protected static short getFromArray(final short[] array, final int n) {
        if (array != null && n >= 0 && n < array.length) {
            return array[n];
        }
        return 0;
    }
    
    protected static boolean getFromArray(final boolean[] array, final int n) {
        return array != null && n >= 0 && n < array.length && array[n];
    }
    
    protected static int getFromList(final SparseIntArray sparseIntArray, final int n) {
        if (sparseIntArray != null && n >= 0) {
            return sparseIntArray.get(n);
        }
        return 0;
    }
    
    @TargetApi(18)
    protected static long getFromList(final SparseLongArray sparseLongArray, final int n) {
        if (sparseLongArray != null && n >= 0) {
            return sparseLongArray.get(n);
        }
        return 0L;
    }
    
    @TargetApi(16)
    protected static <T> T getFromList(final LongSparseArray<T> longSparseArray, final int n) {
        if (longSparseArray != null && n >= 0) {
            return (T)longSparseArray.get((long)n);
        }
        return null;
    }
    
    protected static <T> T getFromList(final SparseArray<T> sparseArray, final int n) {
        if (sparseArray != null && n >= 0) {
            return (T)sparseArray.get(n);
        }
        return null;
    }
    
    protected static <T> T getFromList(final androidx.collection.LongSparseArray<T> longSparseArray, final int n) {
        if (longSparseArray != null && n >= 0) {
            return longSparseArray.get(n);
        }
        return null;
    }
    
    protected static <T> T getFromList(final List<T> list, final int n) {
        if (list != null && n >= 0 && n < list.size()) {
            return list.get(n);
        }
        return null;
    }
    
    protected static boolean getFromList(final SparseBooleanArray sparseBooleanArray, final int n) {
        return sparseBooleanArray != null && n >= 0 && sparseBooleanArray.get(n);
    }
    
    private void handleFieldChange(final int n, final Object o, final int n2) {
        if (this.mInLiveDataRegisterObserver) {
            return;
        }
        if (this.onFieldChange(n, o, n2)) {
            this.requestRebind();
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected static <T extends ViewDataBinding> T inflateInternal(@NonNull final LayoutInflater layoutInflater, final int n, @Nullable final ViewGroup viewGroup, final boolean b, @Nullable final Object o) {
        return DataBindingUtil.inflate(layoutInflater, n, viewGroup, b, checkAndCastToBindingComponent(o));
    }
    
    private static boolean isNumeric(final String s, final int n) {
        final int length = s.length();
        int i = n;
        if (length == n) {
            return false;
        }
        while (i < length) {
            if (!Character.isDigit(s.charAt(i))) {
                return false;
            }
            ++i;
        }
        return true;
    }
    
    private static void mapBindings(final DataBindingComponent dataBindingComponent, final View view, final Object[] array, final IncludedLayouts includedLayouts, final SparseIntArray sparseIntArray, final boolean b) {
        if (getBinding(view) != null) {
            return;
        }
        final Object tag = view.getTag();
        String s;
        if (tag instanceof String) {
            s = (String)tag;
        }
        else {
            s = null;
        }
        int tagInt = 0;
        boolean b2 = false;
        if (b && s != null && s.startsWith("layout")) {
            int lastIndex = s.lastIndexOf(95);
            Label_0123: {
                if (lastIndex > 0) {
                    ++lastIndex;
                    if (isNumeric(s, lastIndex)) {
                        tagInt = parseTagInt(s, lastIndex);
                        if (array[tagInt] == null) {
                            array[tagInt] = view;
                        }
                        if (includedLayouts == null) {
                            tagInt = -1;
                        }
                        b2 = true;
                        break Label_0123;
                    }
                }
                tagInt = -1;
                b2 = false;
            }
        }
        else if (s != null && s.startsWith("binding_")) {
            int tagInt2 = parseTagInt(s, ViewDataBinding.BINDING_NUMBER_START);
            if (array[tagInt2] == null) {
                array[tagInt2] = view;
            }
            if (includedLayouts == null) {
                tagInt2 = -1;
            }
            tagInt = tagInt2;
            b2 = true;
        }
        else {
            b2 = false;
            tagInt = -1;
        }
        if (!b2) {
            final int id = view.getId();
            if (id > 0 && sparseIntArray != null) {
                final int value = sparseIntArray.get(id, -1);
                if (value >= 0 && array[value] == null) {
                    array[value] = view;
                }
            }
        }
        if (view instanceof ViewGroup) {
            final ViewGroup viewGroup = (ViewGroup)view;
            final int childCount = viewGroup.getChildCount();
            int i = 0;
            int n = 0;
            while (i < childCount) {
                final View child = viewGroup.getChildAt(i);
                int n6 = 0;
                int n7 = 0;
                int n8 = 0;
                Label_0508: {
                    if (tagInt >= 0 && child.getTag() instanceof String) {
                        final String s2 = (String)child.getTag();
                        if (s2.endsWith("_0") && s2.startsWith("layout") && s2.indexOf(47) > 0) {
                            final int includeIndex = findIncludeIndex(s2, n, includedLayouts, tagInt);
                            if (includeIndex >= 0) {
                                final int n2 = includedLayouts.indexes[tagInt][includeIndex];
                                final int n3 = includedLayouts.layoutIds[tagInt][includeIndex];
                                final int lastMatching = findLastMatching(viewGroup, i);
                                if (lastMatching == i) {
                                    array[n2] = DataBindingUtil.bind(dataBindingComponent, child, n3);
                                }
                                else {
                                    final int n4 = lastMatching - i + 1;
                                    final View[] array2 = new View[n4];
                                    for (int j = 0; j < n4; ++j) {
                                        array2[j] = viewGroup.getChildAt(i + j);
                                    }
                                    array[n2] = DataBindingUtil.bind(dataBindingComponent, array2, n3);
                                    i += n4 - 1;
                                }
                                final int n5 = includeIndex + 1;
                                n6 = 1;
                                n7 = i;
                                n8 = n5;
                                break Label_0508;
                            }
                        }
                    }
                    final int n9 = i;
                    n8 = n;
                    final int n10 = 0;
                    n7 = n9;
                    n6 = n10;
                }
                if (n6 == 0) {
                    mapBindings(dataBindingComponent, child, array, includedLayouts, sparseIntArray, false);
                }
                final int n11 = n7 + 1;
                n = n8;
                i = n11;
            }
        }
    }
    
    protected static Object[] mapBindings(final DataBindingComponent dataBindingComponent, final View view, final int n, final IncludedLayouts includedLayouts, final SparseIntArray sparseIntArray) {
        final Object[] array = new Object[n];
        mapBindings(dataBindingComponent, view, array, includedLayouts, sparseIntArray, true);
        return array;
    }
    
    protected static Object[] mapBindings(final DataBindingComponent dataBindingComponent, final View[] array, int i, final IncludedLayouts includedLayouts, final SparseIntArray sparseIntArray) {
        final Object[] array2 = new Object[i];
        for (i = 0; i < array.length; ++i) {
            mapBindings(dataBindingComponent, array[i], array2, includedLayouts, sparseIntArray, true);
        }
        return array2;
    }
    
    protected static byte parse(final String s, final byte b) {
        try {
            return Byte.parseByte(s);
        }
        catch (final NumberFormatException ex) {
            return b;
        }
    }
    
    protected static char parse(final String s, final char c) {
        if (s != null && !s.isEmpty()) {
            return s.charAt(0);
        }
        return c;
    }
    
    protected static double parse(final String s, final double n) {
        try {
            return Double.parseDouble(s);
        }
        catch (final NumberFormatException ex) {
            return n;
        }
    }
    
    protected static float parse(final String s, final float n) {
        try {
            return Float.parseFloat(s);
        }
        catch (final NumberFormatException ex) {
            return n;
        }
    }
    
    protected static int parse(final String s, final int n) {
        try {
            return Integer.parseInt(s);
        }
        catch (final NumberFormatException ex) {
            return n;
        }
    }
    
    protected static long parse(final String s, final long n) {
        try {
            return Long.parseLong(s);
        }
        catch (final NumberFormatException ex) {
            return n;
        }
    }
    
    protected static short parse(final String s, final short n) {
        try {
            return Short.parseShort(s);
        }
        catch (final NumberFormatException ex) {
            return n;
        }
    }
    
    protected static boolean parse(final String s, final boolean b) {
        if (s == null) {
            return b;
        }
        return Boolean.parseBoolean(s);
    }
    
    private static int parseTagInt(final String s, int i) {
        final int length = s.length();
        int n = 0;
        while (i < length) {
            n = n * 10 + (s.charAt(i) - '0');
            ++i;
        }
        return n;
    }
    
    private static void processReferenceQueue() {
        while (true) {
            final Reference<? extends ViewDataBinding> poll = ViewDataBinding.sReferenceQueue.poll();
            if (poll == null) {
                break;
            }
            if (!(poll instanceof WeakListener)) {
                continue;
            }
            ((WeakListener)poll).unregister();
        }
    }
    
    protected static byte safeUnbox(final Byte b) {
        byte byteValue;
        if (b == null) {
            byteValue = 0;
        }
        else {
            byteValue = b;
        }
        return byteValue;
    }
    
    protected static char safeUnbox(final Character c) {
        char charValue;
        if (c == null) {
            charValue = '\0';
        }
        else {
            charValue = c;
        }
        return charValue;
    }
    
    protected static double safeUnbox(final Double n) {
        double doubleValue;
        if (n == null) {
            doubleValue = 0.0;
        }
        else {
            doubleValue = n;
        }
        return doubleValue;
    }
    
    protected static float safeUnbox(final Float n) {
        float floatValue;
        if (n == null) {
            floatValue = 0.0f;
        }
        else {
            floatValue = n;
        }
        return floatValue;
    }
    
    protected static int safeUnbox(final Integer n) {
        int intValue;
        if (n == null) {
            intValue = 0;
        }
        else {
            intValue = n;
        }
        return intValue;
    }
    
    protected static long safeUnbox(final Long n) {
        long longValue;
        if (n == null) {
            longValue = 0L;
        }
        else {
            longValue = n;
        }
        return longValue;
    }
    
    protected static short safeUnbox(final Short n) {
        short shortValue;
        if (n == null) {
            shortValue = 0;
        }
        else {
            shortValue = n;
        }
        return shortValue;
    }
    
    protected static boolean safeUnbox(final Boolean b) {
        return b != null && b;
    }
    
    protected static void setBindingInverseListener(final ViewDataBinding viewDataBinding, final InverseBindingListener inverseBindingListener, final PropertyChangedInverseListener propertyChangedInverseListener) {
        if (inverseBindingListener != propertyChangedInverseListener) {
            if (inverseBindingListener != null) {
                viewDataBinding.removeOnPropertyChangedCallback((OnPropertyChangedCallback)inverseBindingListener);
            }
            if (propertyChangedInverseListener != null) {
                viewDataBinding.addOnPropertyChangedCallback(propertyChangedInverseListener);
            }
        }
    }
    
    @TargetApi(16)
    protected static <T> void setTo(final LongSparseArray<T> longSparseArray, final int n, final T t) {
        if (longSparseArray != null && n >= 0) {
            if (n < longSparseArray.size()) {
                longSparseArray.put((long)n, (Object)t);
            }
        }
    }
    
    protected static <T> void setTo(final SparseArray<T> sparseArray, final int n, final T t) {
        if (sparseArray != null && n >= 0) {
            if (n < sparseArray.size()) {
                sparseArray.put(n, (Object)t);
            }
        }
    }
    
    protected static void setTo(final SparseBooleanArray sparseBooleanArray, final int n, final boolean b) {
        if (sparseBooleanArray != null && n >= 0) {
            if (n < sparseBooleanArray.size()) {
                sparseBooleanArray.put(n, b);
            }
        }
    }
    
    protected static void setTo(final SparseIntArray sparseIntArray, final int n, final int n2) {
        if (sparseIntArray != null && n >= 0) {
            if (n < sparseIntArray.size()) {
                sparseIntArray.put(n, n2);
            }
        }
    }
    
    @TargetApi(18)
    protected static void setTo(final SparseLongArray sparseLongArray, final int n, final long n2) {
        if (sparseLongArray != null && n >= 0) {
            if (n < sparseLongArray.size()) {
                sparseLongArray.put(n, n2);
            }
        }
    }
    
    protected static <T> void setTo(final androidx.collection.LongSparseArray<T> longSparseArray, final int n, final T t) {
        if (longSparseArray != null && n >= 0) {
            if (n < longSparseArray.size()) {
                longSparseArray.put(n, t);
            }
        }
    }
    
    protected static <T> void setTo(final List<T> list, final int n, final T t) {
        if (list != null && n >= 0) {
            if (n < list.size()) {
                list.set(n, t);
            }
        }
    }
    
    protected static <K, T> void setTo(final Map<K, T> map, final K k, final T t) {
        if (map == null) {
            return;
        }
        map.put(k, t);
    }
    
    protected static void setTo(final byte[] array, final int n, final byte b) {
        if (array != null && n >= 0) {
            if (n < array.length) {
                array[n] = b;
            }
        }
    }
    
    protected static void setTo(final char[] array, final int n, final char c) {
        if (array != null && n >= 0) {
            if (n < array.length) {
                array[n] = c;
            }
        }
    }
    
    protected static void setTo(final double[] array, final int n, final double n2) {
        if (array != null && n >= 0) {
            if (n < array.length) {
                array[n] = n2;
            }
        }
    }
    
    protected static void setTo(final float[] array, final int n, final float n2) {
        if (array != null && n >= 0) {
            if (n < array.length) {
                array[n] = n2;
            }
        }
    }
    
    protected static void setTo(final int[] array, final int n, final int n2) {
        if (array != null && n >= 0) {
            if (n < array.length) {
                array[n] = n2;
            }
        }
    }
    
    protected static void setTo(final long[] array, final int n, final long n2) {
        if (array != null && n >= 0) {
            if (n < array.length) {
                array[n] = n2;
            }
        }
    }
    
    protected static <T> void setTo(final T[] array, final int n, final T t) {
        if (array != null && n >= 0) {
            if (n < array.length) {
                array[n] = t;
            }
        }
    }
    
    protected static void setTo(final short[] array, final int n, final short n2) {
        if (array != null && n >= 0) {
            if (n < array.length) {
                array[n] = n2;
            }
        }
    }
    
    protected static void setTo(final boolean[] array, final int n, final boolean b) {
        if (array != null && n >= 0) {
            if (n < array.length) {
                array[n] = b;
            }
        }
    }
    
    private boolean updateRegistration(final int n, final Object o, final CreateWeakListener createWeakListener) {
        if (o == null) {
            return this.unregisterFrom(n);
        }
        final WeakListener weakListener = this.mLocalFieldObservers[n];
        if (weakListener == null) {
            this.registerTo(n, o, createWeakListener);
            return true;
        }
        if (weakListener.getTarget() == o) {
            return false;
        }
        this.unregisterFrom(n);
        this.registerTo(n, o, createWeakListener);
        return true;
    }
    
    public void addOnRebindCallback(@NonNull final OnRebindCallback onRebindCallback) {
        if (this.mRebindCallbacks == null) {
            this.mRebindCallbacks = new CallbackRegistry<OnRebindCallback, ViewDataBinding, Void>(ViewDataBinding.REBIND_NOTIFIER);
        }
        this.mRebindCallbacks.add(onRebindCallback);
    }
    
    protected void ensureBindingComponentIsNotNull(final Class<?> clazz) {
        if (this.mBindingComponent != null) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Required DataBindingComponent is null in class ");
        sb.append(this.getClass().getSimpleName());
        sb.append(". A BindingAdapter in ");
        sb.append(clazz.getCanonicalName());
        sb.append(" is not static and requires an object to use, retrieved from the DataBindingComponent. If you don't use an inflation method taking a DataBindingComponent, use DataBindingUtil.setDefaultComponent or make all BindingAdapter methods static.");
        throw new IllegalStateException(sb.toString());
    }
    
    protected abstract void executeBindings();
    
    public void executePendingBindings() {
        final ViewDataBinding mContainingBinding = this.mContainingBinding;
        if (mContainingBinding == null) {
            this.executeBindingsInternal();
        }
        else {
            mContainingBinding.executePendingBindings();
        }
    }
    
    void forceExecuteBindings() {
        this.executeBindings();
    }
    
    @Nullable
    public LifecycleOwner getLifecycleOwner() {
        return this.mLifecycleOwner;
    }
    
    protected Object getObservedField(final int n) {
        final WeakListener weakListener = this.mLocalFieldObservers[n];
        if (weakListener == null) {
            return null;
        }
        return weakListener.getTarget();
    }
    
    @NonNull
    @Override
    public View getRoot() {
        return this.mRoot;
    }
    
    public abstract boolean hasPendingBindings();
    
    public abstract void invalidateAll();
    
    protected abstract boolean onFieldChange(final int p0, final Object p1, final int p2);
    
    protected void registerTo(final int n, final Object target, final CreateWeakListener createWeakListener) {
        if (target == null) {
            return;
        }
        WeakListener weakListener;
        if ((weakListener = this.mLocalFieldObservers[n]) == null) {
            final WeakListener create = createWeakListener.create(this, n);
            this.mLocalFieldObservers[n] = create;
            final LifecycleOwner mLifecycleOwner = this.mLifecycleOwner;
            weakListener = create;
            if (mLifecycleOwner != null) {
                create.setLifecycleOwner(mLifecycleOwner);
                weakListener = create;
            }
        }
        weakListener.setTarget(target);
    }
    
    public void removeOnRebindCallback(@NonNull final OnRebindCallback onRebindCallback) {
        final CallbackRegistry<OnRebindCallback, ViewDataBinding, Void> mRebindCallbacks = this.mRebindCallbacks;
        if (mRebindCallbacks != null) {
            mRebindCallbacks.remove(onRebindCallback);
        }
    }
    
    protected void requestRebind() {
        final ViewDataBinding mContainingBinding = this.mContainingBinding;
        if (mContainingBinding != null) {
            mContainingBinding.requestRebind();
            return;
        }
        final LifecycleOwner mLifecycleOwner = this.mLifecycleOwner;
        if (mLifecycleOwner != null && !mLifecycleOwner.getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
            return;
        }
        synchronized (this) {
            if (this.mPendingRebind) {
                return;
            }
            this.mPendingRebind = true;
            monitorexit(this);
            if (ViewDataBinding.USE_CHOREOGRAPHER) {
                this.mChoreographer.postFrameCallback(this.mFrameCallback);
            }
            else {
                this.mUIThreadHandler.post(this.mRebindRunnable);
            }
        }
    }
    
    protected void setContainedBinding(final ViewDataBinding viewDataBinding) {
        if (viewDataBinding != null) {
            viewDataBinding.mContainingBinding = this;
        }
    }
    
    @MainThread
    public void setLifecycleOwner(@Nullable final LifecycleOwner lifecycleOwner) {
        final LifecycleOwner mLifecycleOwner = this.mLifecycleOwner;
        if (mLifecycleOwner == lifecycleOwner) {
            return;
        }
        if (mLifecycleOwner != null) {
            mLifecycleOwner.getLifecycle().removeObserver(this.mOnStartListener);
        }
        if ((this.mLifecycleOwner = lifecycleOwner) != null) {
            if (this.mOnStartListener == null) {
                this.mOnStartListener = new OnStartListener(this);
            }
            lifecycleOwner.getLifecycle().addObserver(this.mOnStartListener);
        }
        for (final WeakListener weakListener : this.mLocalFieldObservers) {
            if (weakListener != null) {
                weakListener.setLifecycleOwner(lifecycleOwner);
            }
        }
    }
    
    protected void setRootTag(final View view) {
        view.setTag(R.id.dataBinding, (Object)this);
    }
    
    protected void setRootTag(final View[] array) {
        for (int length = array.length, i = 0; i < length; ++i) {
            array[i].setTag(R.id.dataBinding, (Object)this);
        }
    }
    
    public abstract boolean setVariable(final int p0, @Nullable final Object p1);
    
    public void unbind() {
        for (final WeakListener weakListener : this.mLocalFieldObservers) {
            if (weakListener != null) {
                weakListener.unregister();
            }
        }
    }
    
    protected boolean unregisterFrom(final int n) {
        final WeakListener weakListener = this.mLocalFieldObservers[n];
        return weakListener != null && weakListener.unregister();
    }
    
    protected boolean updateLiveDataRegistration(final int n, final LiveData<?> liveData) {
        this.mInLiveDataRegisterObserver = true;
        try {
            return this.updateRegistration(n, liveData, ViewDataBinding.CREATE_LIVE_DATA_LISTENER);
        }
        finally {
            this.mInLiveDataRegisterObserver = false;
        }
    }
    
    protected boolean updateRegistration(final int n, final Observable observable) {
        return this.updateRegistration(n, observable, ViewDataBinding.CREATE_PROPERTY_LISTENER);
    }
    
    protected boolean updateRegistration(final int n, final ObservableList list) {
        return this.updateRegistration(n, list, ViewDataBinding.CREATE_LIST_LISTENER);
    }
    
    protected boolean updateRegistration(final int n, final ObservableMap observableMap) {
        return this.updateRegistration(n, observableMap, ViewDataBinding.CREATE_MAP_LISTENER);
    }
    
    private interface CreateWeakListener
    {
        WeakListener create(final ViewDataBinding p0, final int p1);
    }
    
    protected static class IncludedLayouts
    {
        public final int[][] indexes;
        public final int[][] layoutIds;
        public final String[][] layouts;
        
        public IncludedLayouts(final int n) {
            this.layouts = new String[n][];
            this.indexes = new int[n][];
            this.layoutIds = new int[n][];
        }
        
        public void setIncludes(final int n, final String[] array, final int[] array2, final int[] array3) {
            this.layouts[n] = array;
            this.indexes[n] = array2;
            this.layoutIds[n] = array3;
        }
    }
    
    private static class LiveDataListener implements Observer, ObservableReference<LiveData<?>>
    {
        LifecycleOwner mLifecycleOwner;
        final WeakListener<LiveData<?>> mListener;
        
        public LiveDataListener(final ViewDataBinding viewDataBinding, final int n) {
            this.mListener = (WeakListener<LiveData<?>>)new WeakListener(viewDataBinding, n, (ObservableReference<Object>)this);
        }
        
        public void addListener(final LiveData<?> liveData) {
            final LifecycleOwner mLifecycleOwner = this.mLifecycleOwner;
            if (mLifecycleOwner != null) {
                liveData.observe(mLifecycleOwner, this);
            }
        }
        
        @Override
        public WeakListener<LiveData<?>> getListener() {
            return this.mListener;
        }
        
        @Override
        public void onChanged(@Nullable final Object o) {
            final ViewDataBinding binder = this.mListener.getBinder();
            if (binder != null) {
                final WeakListener<LiveData<?>> mListener = this.mListener;
                binder.handleFieldChange(mListener.mLocalFieldId, mListener.getTarget(), 0);
            }
        }
        
        public void removeListener(final LiveData<?> liveData) {
            liveData.removeObserver(this);
        }
        
        @Override
        public void setLifecycleOwner(final LifecycleOwner mLifecycleOwner) {
            final LiveData liveData = this.mListener.getTarget();
            if (liveData != null) {
                if (this.mLifecycleOwner != null) {
                    liveData.removeObserver(this);
                }
                if (mLifecycleOwner != null) {
                    liveData.observe(mLifecycleOwner, this);
                }
            }
            this.mLifecycleOwner = mLifecycleOwner;
        }
    }
    
    private interface ObservableReference<T>
    {
        void addListener(final T p0);
        
        WeakListener<T> getListener();
        
        void removeListener(final T p0);
        
        void setLifecycleOwner(final LifecycleOwner p0);
    }
    
    static class OnStartListener implements LifecycleObserver
    {
        final WeakReference<ViewDataBinding> mBinding;
        
        private OnStartListener(final ViewDataBinding referent) {
            this.mBinding = new WeakReference<ViewDataBinding>(referent);
        }
        
        @OnLifecycleEvent(Lifecycle.Event.ON_START)
        public void onStart() {
            final ViewDataBinding viewDataBinding = this.mBinding.get();
            if (viewDataBinding != null) {
                viewDataBinding.executePendingBindings();
            }
        }
    }
    
    protected abstract static class PropertyChangedInverseListener extends OnPropertyChangedCallback implements InverseBindingListener
    {
        final int mPropertyId;
        
        public PropertyChangedInverseListener(final int mPropertyId) {
            this.mPropertyId = mPropertyId;
        }
        
        @Override
        public void onPropertyChanged(final Observable observable, final int n) {
            if (n == this.mPropertyId || n == 0) {
                this.onChange();
            }
        }
    }
    
    private static class WeakListListener extends OnListChangedCallback implements ObservableReference<ObservableList>
    {
        final WeakListener<ObservableList> mListener;
        
        public WeakListListener(final ViewDataBinding viewDataBinding, final int n) {
            this.mListener = (WeakListener<ObservableList>)new WeakListener(viewDataBinding, n, (ObservableReference<Object>)this);
        }
        
        public void addListener(final ObservableList list) {
            list.addOnListChangedCallback((ObservableList.OnListChangedCallback)this);
        }
        
        @Override
        public WeakListener<ObservableList> getListener() {
            return this.mListener;
        }
        
        @Override
        public void onChanged(final ObservableList list) {
            final ViewDataBinding binder = this.mListener.getBinder();
            if (binder == null) {
                return;
            }
            final ObservableList list2 = this.mListener.getTarget();
            if (list2 != list) {
                return;
            }
            binder.handleFieldChange(this.mListener.mLocalFieldId, list2, 0);
        }
        
        @Override
        public void onItemRangeChanged(final ObservableList list, final int n, final int n2) {
            this.onChanged(list);
        }
        
        @Override
        public void onItemRangeInserted(final ObservableList list, final int n, final int n2) {
            this.onChanged(list);
        }
        
        @Override
        public void onItemRangeMoved(final ObservableList list, final int n, final int n2, final int n3) {
            this.onChanged(list);
        }
        
        @Override
        public void onItemRangeRemoved(final ObservableList list, final int n, final int n2) {
            this.onChanged(list);
        }
        
        public void removeListener(final ObservableList list) {
            list.removeOnListChangedCallback((ObservableList.OnListChangedCallback)this);
        }
        
        @Override
        public void setLifecycleOwner(final LifecycleOwner lifecycleOwner) {
        }
    }
    
    private static class WeakListener<T> extends WeakReference<ViewDataBinding>
    {
        protected final int mLocalFieldId;
        private final ObservableReference<T> mObservable;
        private T mTarget;
        
        public WeakListener(final ViewDataBinding referent, final int mLocalFieldId, final ObservableReference<T> mObservable) {
            super(referent, ViewDataBinding.sReferenceQueue);
            this.mLocalFieldId = mLocalFieldId;
            this.mObservable = mObservable;
        }
        
        protected ViewDataBinding getBinder() {
            final ViewDataBinding viewDataBinding = this.get();
            if (viewDataBinding == null) {
                this.unregister();
            }
            return viewDataBinding;
        }
        
        public T getTarget() {
            return this.mTarget;
        }
        
        public void setLifecycleOwner(final LifecycleOwner lifecycleOwner) {
            this.mObservable.setLifecycleOwner(lifecycleOwner);
        }
        
        public void setTarget(final T mTarget) {
            this.unregister();
            this.mTarget = mTarget;
            if (mTarget != null) {
                this.mObservable.addListener(mTarget);
            }
        }
        
        public boolean unregister() {
            final T mTarget = this.mTarget;
            boolean b;
            if (mTarget != null) {
                this.mObservable.removeListener(mTarget);
                b = true;
            }
            else {
                b = false;
            }
            this.mTarget = null;
            return b;
        }
    }
    
    private static class WeakMapListener extends OnMapChangedCallback implements ObservableReference<ObservableMap>
    {
        final WeakListener<ObservableMap> mListener;
        
        public WeakMapListener(final ViewDataBinding viewDataBinding, final int n) {
            this.mListener = (WeakListener<ObservableMap>)new WeakListener(viewDataBinding, n, (ObservableReference<Object>)this);
        }
        
        public void addListener(final ObservableMap observableMap) {
            observableMap.addOnMapChangedCallback((ObservableMap.OnMapChangedCallback)this);
        }
        
        @Override
        public WeakListener<ObservableMap> getListener() {
            return this.mListener;
        }
        
        @Override
        public void onMapChanged(final ObservableMap observableMap, final Object o) {
            final ViewDataBinding binder = this.mListener.getBinder();
            if (binder != null) {
                if (observableMap == this.mListener.getTarget()) {
                    binder.handleFieldChange(this.mListener.mLocalFieldId, observableMap, 0);
                }
            }
        }
        
        public void removeListener(final ObservableMap observableMap) {
            observableMap.removeOnMapChangedCallback((ObservableMap.OnMapChangedCallback)this);
        }
        
        @Override
        public void setLifecycleOwner(final LifecycleOwner lifecycleOwner) {
        }
    }
    
    private static class WeakPropertyListener extends OnPropertyChangedCallback implements ObservableReference<Observable>
    {
        final WeakListener<Observable> mListener;
        
        public WeakPropertyListener(final ViewDataBinding viewDataBinding, final int n) {
            this.mListener = (WeakListener<Observable>)new WeakListener(viewDataBinding, n, (ObservableReference<Object>)this);
        }
        
        public void addListener(final Observable observable) {
            observable.addOnPropertyChangedCallback((Observable.OnPropertyChangedCallback)this);
        }
        
        @Override
        public WeakListener<Observable> getListener() {
            return this.mListener;
        }
        
        @Override
        public void onPropertyChanged(final Observable observable, final int n) {
            final ViewDataBinding binder = this.mListener.getBinder();
            if (binder == null) {
                return;
            }
            if (this.mListener.getTarget() != observable) {
                return;
            }
            binder.handleFieldChange(this.mListener.mLocalFieldId, observable, n);
        }
        
        public void removeListener(final Observable observable) {
            observable.removeOnPropertyChangedCallback((Observable.OnPropertyChangedCallback)this);
        }
        
        @Override
        public void setLifecycleOwner(final LifecycleOwner lifecycleOwner) {
        }
    }
}

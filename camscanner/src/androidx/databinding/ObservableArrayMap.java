// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding;

import java.util.Iterator;
import java.util.Collection;
import androidx.collection.ArrayMap;

public class ObservableArrayMap<K, V> extends ArrayMap<K, V> implements ObservableMap<K, V>
{
    private transient MapChangeRegistry mListeners;
    
    private void notifyChange(final Object o) {
        final MapChangeRegistry mListeners = this.mListeners;
        if (mListeners != null) {
            ((CallbackRegistry<C, ObservableArrayMap, Object>)mListeners).notifyCallbacks(this, 0, o);
        }
    }
    
    @Override
    public void addOnMapChangedCallback(final OnMapChangedCallback<? extends ObservableMap<K, V>, K, V> onMapChangedCallback) {
        if (this.mListeners == null) {
            this.mListeners = new MapChangeRegistry();
        }
        ((CallbackRegistry<OnMapChangedCallback, T, A>)this.mListeners).add((OnMapChangedCallback)onMapChangedCallback);
    }
    
    @Override
    public void clear() {
        if (!this.isEmpty()) {
            super.clear();
            this.notifyChange(null);
        }
    }
    
    @Override
    public V put(final K k, final V v) {
        super.put(k, v);
        this.notifyChange(k);
        return v;
    }
    
    @Override
    public boolean removeAll(final Collection<?> collection) {
        final Iterator<?> iterator = collection.iterator();
        boolean b = false;
        while (iterator.hasNext()) {
            final int indexOfKey = this.indexOfKey(iterator.next());
            if (indexOfKey >= 0) {
                this.removeAt(indexOfKey);
                b = true;
            }
        }
        return b;
    }
    
    @Override
    public V removeAt(final int n) {
        final Object key = this.keyAt(n);
        final V remove = super.removeAt(n);
        if (remove != null) {
            this.notifyChange(key);
        }
        return remove;
    }
    
    @Override
    public void removeOnMapChangedCallback(final OnMapChangedCallback<? extends ObservableMap<K, V>, K, V> onMapChangedCallback) {
        final MapChangeRegistry mListeners = this.mListeners;
        if (mListeners != null) {
            ((CallbackRegistry<OnMapChangedCallback, T, A>)mListeners).remove((OnMapChangedCallback)onMapChangedCallback);
        }
    }
    
    @Override
    public boolean retainAll(final Collection<?> collection) {
        int i = this.size() - 1;
        boolean b = false;
        while (i >= 0) {
            if (!collection.contains(this.keyAt(i))) {
                this.removeAt(i);
                b = true;
            }
            --i;
        }
        return b;
    }
    
    @Override
    public V setValueAt(final int n, final V v) {
        final Object key = this.keyAt(n);
        final V setValue = super.setValueAt(n, v);
        this.notifyChange(key);
        return setValue;
    }
}

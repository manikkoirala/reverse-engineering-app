// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.library.baseAdapters;

import java.util.HashMap;
import android.util.SparseArray;
import androidx.databinding.ViewDataBinding;
import android.view.View;
import androidx.databinding.DataBindingComponent;
import java.util.ArrayList;
import java.util.List;
import android.util.SparseIntArray;
import androidx.databinding.DataBinderMapper;

public class DataBinderMapperImpl extends DataBinderMapper
{
    private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP;
    
    static {
        INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(0);
    }
    
    @Override
    public List<DataBinderMapper> collectDependencies() {
        return new ArrayList<DataBinderMapper>(0);
    }
    
    @Override
    public String convertBrIdToString(final int n) {
        return (String)InnerBrLookup.sKeys.get(n);
    }
    
    @Override
    public ViewDataBinding getDataBinder(final DataBindingComponent dataBindingComponent, final View view, final int n) {
        if (DataBinderMapperImpl.INTERNAL_LAYOUT_ID_LOOKUP.get(n) > 0 && view.getTag() == null) {
            throw new RuntimeException("view must have a tag");
        }
        return null;
    }
    
    @Override
    public ViewDataBinding getDataBinder(final DataBindingComponent dataBindingComponent, final View[] array, final int n) {
        if (array != null) {
            if (array.length != 0) {
                if (DataBinderMapperImpl.INTERNAL_LAYOUT_ID_LOOKUP.get(n) > 0) {
                    if (array[0].getTag() == null) {
                        throw new RuntimeException("view must have a tag");
                    }
                }
            }
        }
        return null;
    }
    
    @Override
    public int getLayoutId(final String key) {
        int intValue = 0;
        if (key == null) {
            return 0;
        }
        final Integer n = InnerLayoutIdLookup.sKeys.get(key);
        if (n != null) {
            intValue = n;
        }
        return intValue;
    }
    
    private static class InnerBrLookup
    {
        static final SparseArray<String> sKeys;
        
        static {
            (sKeys = new SparseArray(1)).put(0, (Object)"_all");
        }
    }
    
    private static class InnerLayoutIdLookup
    {
        static final HashMap<String, Integer> sKeys;
        
        static {
            sKeys = new HashMap<String, Integer>(0);
        }
    }
}

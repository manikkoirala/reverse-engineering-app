// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding;

abstract class BaseObservableField extends BaseObservable
{
    public BaseObservableField() {
    }
    
    public BaseObservableField(final Observable... array) {
        if (array != null && array.length != 0) {
            final DependencyCallback dependencyCallback = new DependencyCallback();
            for (int i = 0; i < array.length; ++i) {
                array[i].addOnPropertyChangedCallback((Observable.OnPropertyChangedCallback)dependencyCallback);
            }
        }
    }
    
    class DependencyCallback extends OnPropertyChangedCallback
    {
        final BaseObservableField this$0;
        
        DependencyCallback(final BaseObservableField this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public void onPropertyChanged(final Observable observable, final int n) {
            this.this$0.notifyChange();
        }
    }
}

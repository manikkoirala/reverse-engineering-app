// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.view.ViewStub;
import android.view.View;
import android.view.ViewStub$OnInflateListener;

public class ViewStubProxy
{
    private ViewDataBinding mContainingBinding;
    private ViewStub$OnInflateListener mOnInflateListener;
    private ViewStub$OnInflateListener mProxyListener;
    private View mRoot;
    private ViewDataBinding mViewDataBinding;
    private ViewStub mViewStub;
    
    public ViewStubProxy(@NonNull final ViewStub mViewStub) {
        final ViewStub$OnInflateListener viewStub$OnInflateListener = (ViewStub$OnInflateListener)new ViewStub$OnInflateListener() {
            final ViewStubProxy this$0;
            
            public void onInflate(final ViewStub viewStub, final View view) {
                this.this$0.mRoot = view;
                final ViewStubProxy this$0 = this.this$0;
                this$0.mViewDataBinding = DataBindingUtil.bind(this$0.mContainingBinding.mBindingComponent, view, viewStub.getLayoutResource());
                this.this$0.mViewStub = null;
                if (this.this$0.mOnInflateListener != null) {
                    this.this$0.mOnInflateListener.onInflate(viewStub, view);
                    this.this$0.mOnInflateListener = null;
                }
                this.this$0.mContainingBinding.invalidateAll();
                this.this$0.mContainingBinding.forceExecuteBindings();
            }
        };
        this.mProxyListener = (ViewStub$OnInflateListener)viewStub$OnInflateListener;
        (this.mViewStub = mViewStub).setOnInflateListener((ViewStub$OnInflateListener)viewStub$OnInflateListener);
    }
    
    @Nullable
    public ViewDataBinding getBinding() {
        return this.mViewDataBinding;
    }
    
    public View getRoot() {
        return this.mRoot;
    }
    
    @Nullable
    public ViewStub getViewStub() {
        return this.mViewStub;
    }
    
    public boolean isInflated() {
        return this.mRoot != null;
    }
    
    public void setContainingBinding(@NonNull final ViewDataBinding mContainingBinding) {
        this.mContainingBinding = mContainingBinding;
    }
    
    public void setOnInflateListener(@Nullable final ViewStub$OnInflateListener mOnInflateListener) {
        if (this.mViewStub != null) {
            this.mOnInflateListener = mOnInflateListener;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding;

import java.util.ArrayList;
import java.util.List;

public class CallbackRegistry<C, T, A> implements Cloneable
{
    private static final String TAG = "CallbackRegistry";
    private List<C> mCallbacks;
    private long mFirst64Removed;
    private int mNotificationLevel;
    private final NotifierCallback<C, T, A> mNotifier;
    private long[] mRemainderRemoved;
    
    public CallbackRegistry(final NotifierCallback<C, T, A> mNotifier) {
        this.mCallbacks = new ArrayList<C>();
        this.mFirst64Removed = 0L;
        this.mNotifier = mNotifier;
    }
    
    private boolean isRemoved(final int n) {
        final boolean b = true;
        boolean b2 = true;
        if (n < 64) {
            if ((1L << n & this.mFirst64Removed) == 0x0L) {
                b2 = false;
            }
            return b2;
        }
        final long[] mRemainderRemoved = this.mRemainderRemoved;
        if (mRemainderRemoved == null) {
            return false;
        }
        final int n2 = n / 64 - 1;
        return n2 < mRemainderRemoved.length && (1L << n % 64 & mRemainderRemoved[n2]) != 0x0L && b;
    }
    
    private void notifyCallbacks(final T t, final int n, final A a, int i, final int n2, final long n3) {
        long n4 = 1L;
        while (i < n2) {
            if ((n3 & n4) == 0x0L) {
                this.mNotifier.onNotifyCallback(this.mCallbacks.get(i), t, n, a);
            }
            n4 <<= 1;
            ++i;
        }
    }
    
    private void notifyFirst64(final T t, final int n, final A a) {
        this.notifyCallbacks(t, n, a, 0, Math.min(64, this.mCallbacks.size()), this.mFirst64Removed);
    }
    
    private void notifyRecurse(final T t, final int n, final A a) {
        final int size = this.mCallbacks.size();
        final long[] mRemainderRemoved = this.mRemainderRemoved;
        int n2;
        if (mRemainderRemoved == null) {
            n2 = -1;
        }
        else {
            n2 = mRemainderRemoved.length - 1;
        }
        this.notifyRemainder(t, n, a, n2);
        this.notifyCallbacks(t, n, a, (n2 + 2) * 64, size, 0L);
    }
    
    private void notifyRemainder(final T t, final int n, final A a, final int n2) {
        if (n2 < 0) {
            this.notifyFirst64(t, n, a);
        }
        else {
            final long n3 = this.mRemainderRemoved[n2];
            final int n4 = (n2 + 1) * 64;
            final int min = Math.min(this.mCallbacks.size(), n4 + 64);
            this.notifyRemainder(t, n, a, n2 - 1);
            this.notifyCallbacks(t, n, a, n4, min, n3);
        }
    }
    
    private void removeRemovedCallbacks(final int n, final long n2) {
        int i = n + 64 - 1;
        long n3 = Long.MIN_VALUE;
        while (i >= n) {
            if ((n2 & n3) != 0x0L) {
                this.mCallbacks.remove(i);
            }
            n3 >>>= 1;
            --i;
        }
    }
    
    private void setRemovalBit(final int n) {
        if (n < 64) {
            this.mFirst64Removed |= 1L << n;
        }
        else {
            final int n2 = n / 64 - 1;
            final long[] mRemainderRemoved = this.mRemainderRemoved;
            if (mRemainderRemoved == null) {
                this.mRemainderRemoved = new long[this.mCallbacks.size() / 64];
            }
            else if (mRemainderRemoved.length <= n2) {
                final long[] mRemainderRemoved2 = new long[this.mCallbacks.size() / 64];
                final long[] mRemainderRemoved3 = this.mRemainderRemoved;
                System.arraycopy(mRemainderRemoved3, 0, mRemainderRemoved2, 0, mRemainderRemoved3.length);
                this.mRemainderRemoved = mRemainderRemoved2;
            }
            final long[] mRemainderRemoved4 = this.mRemainderRemoved;
            mRemainderRemoved4[n2] |= 1L << n % 64;
        }
    }
    
    public void add(final C c) {
        monitorenter(this);
        if (c != null) {
            Label_0059: {
                try {
                    final int lastIndex = this.mCallbacks.lastIndexOf(c);
                    if (lastIndex < 0 || this.isRemoved(lastIndex)) {
                        this.mCallbacks.add(c);
                    }
                    monitorexit(this);
                    return;
                }
                finally {
                    break Label_0059;
                }
                throw new IllegalArgumentException("callback cannot be null");
            }
            monitorexit(this);
        }
        throw new IllegalArgumentException("callback cannot be null");
    }
    
    public void clear() {
        synchronized (this) {
            if (this.mNotificationLevel == 0) {
                this.mCallbacks.clear();
            }
            else if (!this.mCallbacks.isEmpty()) {
                for (int i = this.mCallbacks.size() - 1; i >= 0; --i) {
                    this.setRemovalBit(i);
                }
            }
        }
    }
    
    public CallbackRegistry<C, T, A> clone() {
        monitorenter(this);
        Object mCallbacks = null;
        Label_0116: {
            List<C> list;
            try {
                try {
                    final CallbackRegistry callbackRegistry = (CallbackRegistry)super.clone();
                    try {
                        callbackRegistry.mFirst64Removed = 0L;
                        callbackRegistry.mRemainderRemoved = null;
                        int n = 0;
                        callbackRegistry.mNotificationLevel = 0;
                        mCallbacks = new ArrayList<C>();
                        callbackRegistry.mCallbacks = (List<C>)mCallbacks;
                        final int size = this.mCallbacks.size();
                        while (true) {
                            mCallbacks = callbackRegistry;
                            if (n >= size) {
                                break Label_0116;
                            }
                            if (!this.isRemoved(n)) {
                                callbackRegistry.mCallbacks.add(this.mCallbacks.get(n));
                            }
                            ++n;
                        }
                    }
                    catch (final CloneNotSupportedException mCallbacks) {}
                }
                finally {}
            }
            catch (final CloneNotSupportedException mCallbacks) {
                list = null;
            }
            ((Throwable)mCallbacks).printStackTrace();
            mCallbacks = list;
        }
        monitorexit(this);
        return (CallbackRegistry<C, T, A>)mCallbacks;
        monitorexit(this);
    }
    
    public ArrayList<C> copyCallbacks() {
        synchronized (this) {
            final ArrayList<C> list = new ArrayList<C>(this.mCallbacks.size());
            for (int size = this.mCallbacks.size(), i = 0; i < size; ++i) {
                if (!this.isRemoved(i)) {
                    list.add(this.mCallbacks.get(i));
                }
            }
            return list;
        }
    }
    
    public void copyCallbacks(final List<C> list) {
        synchronized (this) {
            list.clear();
            for (int size = this.mCallbacks.size(), i = 0; i < size; ++i) {
                if (!this.isRemoved(i)) {
                    list.add(this.mCallbacks.get(i));
                }
            }
        }
    }
    
    public boolean isEmpty() {
        synchronized (this) {
            if (this.mCallbacks.isEmpty()) {
                return true;
            }
            if (this.mNotificationLevel == 0) {
                return false;
            }
            for (int size = this.mCallbacks.size(), i = 0; i < size; ++i) {
                if (!this.isRemoved(i)) {
                    return false;
                }
            }
            return true;
        }
    }
    
    public void notifyCallbacks(final T t, int i, final A a) {
        synchronized (this) {
            ++this.mNotificationLevel;
            this.notifyRecurse(t, i, a);
            i = this.mNotificationLevel - 1;
            this.mNotificationLevel = i;
            if (i == 0) {
                final long[] mRemainderRemoved = this.mRemainderRemoved;
                if (mRemainderRemoved != null) {
                    long n;
                    for (i = mRemainderRemoved.length - 1; i >= 0; --i) {
                        n = this.mRemainderRemoved[i];
                        if (n != 0L) {
                            this.removeRemovedCallbacks((i + 1) * 64, n);
                            this.mRemainderRemoved[i] = 0L;
                        }
                    }
                }
                final long mFirst64Removed = this.mFirst64Removed;
                if (mFirst64Removed != 0L) {
                    this.removeRemovedCallbacks(0, mFirst64Removed);
                    this.mFirst64Removed = 0L;
                }
            }
        }
    }
    
    public void remove(final C c) {
        synchronized (this) {
            if (this.mNotificationLevel == 0) {
                this.mCallbacks.remove(c);
            }
            else {
                final int lastIndex = this.mCallbacks.lastIndexOf(c);
                if (lastIndex >= 0) {
                    this.setRemovalBit(lastIndex);
                }
            }
        }
    }
    
    public abstract static class NotifierCallback<C, T, A>
    {
        public abstract void onNotifyCallback(final C p0, final T p1, final int p2, final A p3);
    }
}

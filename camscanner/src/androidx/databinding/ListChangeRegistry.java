// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding;

import androidx.annotation.NonNull;
import androidx.core.util.Pools;

public class ListChangeRegistry extends CallbackRegistry<ObservableList.OnListChangedCallback, ObservableList, ListChanges>
{
    private static final int ALL = 0;
    private static final int CHANGED = 1;
    private static final int INSERTED = 2;
    private static final int MOVED = 3;
    private static final NotifierCallback<ObservableList.OnListChangedCallback, ObservableList, ListChanges> NOTIFIER_CALLBACK;
    private static final int REMOVED = 4;
    private static final Pools.SynchronizedPool<ListChanges> sListChanges;
    
    static {
        sListChanges = new Pools.SynchronizedPool(10);
        NOTIFIER_CALLBACK = new NotifierCallback<ObservableList.OnListChangedCallback, ObservableList, ListChanges>() {
            public void onNotifyCallback(final ObservableList.OnListChangedCallback onListChangedCallback, final ObservableList list, final int n, final ListChanges listChanges) {
                if (n != 1) {
                    if (n != 2) {
                        if (n != 3) {
                            if (n != 4) {
                                onListChangedCallback.onChanged(list);
                            }
                            else {
                                onListChangedCallback.onItemRangeRemoved(list, listChanges.start, listChanges.count);
                            }
                        }
                        else {
                            onListChangedCallback.onItemRangeMoved(list, listChanges.start, listChanges.to, listChanges.count);
                        }
                    }
                    else {
                        onListChangedCallback.onItemRangeInserted(list, listChanges.start, listChanges.count);
                    }
                }
                else {
                    onListChangedCallback.onItemRangeChanged(list, listChanges.start, listChanges.count);
                }
            }
        };
    }
    
    public ListChangeRegistry() {
        super(ListChangeRegistry.NOTIFIER_CALLBACK);
    }
    
    private static ListChanges acquire(final int start, final int to, final int count) {
        ListChanges listChanges;
        if ((listChanges = ListChangeRegistry.sListChanges.acquire()) == null) {
            listChanges = new ListChanges();
        }
        listChanges.start = start;
        listChanges.to = to;
        listChanges.count = count;
        return listChanges;
    }
    
    @Override
    public void notifyCallbacks(@NonNull final ObservableList list, final int n, final ListChanges listChanges) {
        synchronized (this) {
            super.notifyCallbacks(list, n, listChanges);
            if (listChanges != null) {
                ListChangeRegistry.sListChanges.release(listChanges);
            }
        }
    }
    
    public void notifyChanged(@NonNull final ObservableList list) {
        this.notifyCallbacks(list, 0, null);
    }
    
    public void notifyChanged(@NonNull final ObservableList list, final int n, final int n2) {
        this.notifyCallbacks(list, 1, acquire(n, 0, n2));
    }
    
    public void notifyInserted(@NonNull final ObservableList list, final int n, final int n2) {
        this.notifyCallbacks(list, 2, acquire(n, 0, n2));
    }
    
    public void notifyMoved(@NonNull final ObservableList list, final int n, final int n2, final int n3) {
        this.notifyCallbacks(list, 3, acquire(n, n2, n3));
    }
    
    public void notifyRemoved(@NonNull final ObservableList list, final int n, final int n2) {
        this.notifyCallbacks(list, 4, acquire(n, 0, n2));
    }
    
    static class ListChanges
    {
        public int count;
        public int start;
        public int to;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target({ ElementType.TYPE })
public @interface InverseBindingMethods {
    InverseBindingMethod[] value();
}

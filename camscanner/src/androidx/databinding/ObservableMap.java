// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding;

import java.util.Map;

public interface ObservableMap<K, V> extends Map<K, V>
{
    void addOnMapChangedCallback(final OnMapChangedCallback<? extends ObservableMap<K, V>, K, V> p0);
    
    void removeOnMapChangedCallback(final OnMapChangedCallback<? extends ObservableMap<K, V>, K, V> p0);
    
    public abstract static class OnMapChangedCallback<T extends ObservableMap<K, V>, K, V>
    {
        public abstract void onMapChanged(final T p0, final K p1);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import android.widget.ProgressBar;
import androidx.databinding.BindingAdapter;
import android.widget.SeekBar$OnSeekBarChangeListener;
import androidx.databinding.InverseBindingListener;
import android.widget.SeekBar;
import androidx.databinding.InverseBindingMethod;
import androidx.databinding.InverseBindingMethods;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
@InverseBindingMethods({ @InverseBindingMethod(attribute = "android:progress", type = SeekBar.class) })
public class SeekBarBindingAdapter
{
    @BindingAdapter(requireAll = false, value = { "android:onStartTrackingTouch", "android:onStopTrackingTouch", "android:onProgressChanged", "android:progressAttrChanged" })
    public static void setOnSeekBarChangeListener(final SeekBar seekBar, final OnStartTrackingTouch onStartTrackingTouch, final OnStopTrackingTouch onStopTrackingTouch, final OnProgressChanged onProgressChanged, final InverseBindingListener inverseBindingListener) {
        if (onStartTrackingTouch == null && onStopTrackingTouch == null && onProgressChanged == null && inverseBindingListener == null) {
            seekBar.setOnSeekBarChangeListener((SeekBar$OnSeekBarChangeListener)null);
        }
        else {
            seekBar.setOnSeekBarChangeListener((SeekBar$OnSeekBarChangeListener)new SeekBar$OnSeekBarChangeListener(onProgressChanged, inverseBindingListener, onStartTrackingTouch, onStopTrackingTouch) {
                final InverseBindingListener val$attrChanged;
                final OnProgressChanged val$progressChanged;
                final OnStartTrackingTouch val$start;
                final OnStopTrackingTouch val$stop;
                
                public void onProgressChanged(final SeekBar seekBar, final int n, final boolean b) {
                    final OnProgressChanged val$progressChanged = this.val$progressChanged;
                    if (val$progressChanged != null) {
                        val$progressChanged.onProgressChanged(seekBar, n, b);
                    }
                    final InverseBindingListener val$attrChanged = this.val$attrChanged;
                    if (val$attrChanged != null) {
                        val$attrChanged.onChange();
                    }
                }
                
                public void onStartTrackingTouch(final SeekBar seekBar) {
                    final OnStartTrackingTouch val$start = this.val$start;
                    if (val$start != null) {
                        val$start.onStartTrackingTouch(seekBar);
                    }
                }
                
                public void onStopTrackingTouch(final SeekBar seekBar) {
                    final OnStopTrackingTouch val$stop = this.val$stop;
                    if (val$stop != null) {
                        val$stop.onStopTrackingTouch(seekBar);
                    }
                }
            });
        }
    }
    
    @BindingAdapter({ "android:progress" })
    public static void setProgress(final SeekBar seekBar, final int progress) {
        if (progress != ((ProgressBar)seekBar).getProgress()) {
            ((ProgressBar)seekBar).setProgress(progress);
        }
    }
    
    public interface OnProgressChanged
    {
        void onProgressChanged(final SeekBar p0, final int p1, final boolean p2);
    }
    
    public interface OnStartTrackingTouch
    {
        void onStartTrackingTouch(final SeekBar p0);
    }
    
    public interface OnStopTrackingTouch
    {
        void onStopTrackingTouch(final SeekBar p0);
    }
}

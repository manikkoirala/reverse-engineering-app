// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import androidx.databinding.InverseBindingListener;
import android.widget.CompoundButton$OnCheckedChangeListener;
import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingMethod;
import androidx.databinding.InverseBindingMethods;
import android.widget.CompoundButton;
import androidx.databinding.BindingMethod;
import androidx.databinding.BindingMethods;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
@BindingMethods({ @BindingMethod(attribute = "android:buttonTint", method = "setButtonTintList", type = CompoundButton.class), @BindingMethod(attribute = "android:onCheckedChanged", method = "setOnCheckedChangeListener", type = CompoundButton.class) })
@InverseBindingMethods({ @InverseBindingMethod(attribute = "android:checked", type = CompoundButton.class) })
public class CompoundButtonBindingAdapter
{
    @BindingAdapter({ "android:checked" })
    public static void setChecked(final CompoundButton compoundButton, final boolean checked) {
        if (compoundButton.isChecked() != checked) {
            compoundButton.setChecked(checked);
        }
    }
    
    @BindingAdapter(requireAll = false, value = { "android:onCheckedChanged", "android:checkedAttrChanged" })
    public static void setListeners(final CompoundButton compoundButton, final CompoundButton$OnCheckedChangeListener onCheckedChangeListener, final InverseBindingListener inverseBindingListener) {
        if (inverseBindingListener == null) {
            compoundButton.setOnCheckedChangeListener(onCheckedChangeListener);
        }
        else {
            compoundButton.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)new CompoundButton$OnCheckedChangeListener(onCheckedChangeListener, inverseBindingListener) {
                final InverseBindingListener val$attrChange;
                final CompoundButton$OnCheckedChangeListener val$listener;
                
                public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                    final CompoundButton$OnCheckedChangeListener val$listener = this.val$listener;
                    if (val$listener != null) {
                        val$listener.onCheckedChanged(compoundButton, b);
                    }
                    this.val$attrChange.onChange();
                }
            });
        }
    }
}

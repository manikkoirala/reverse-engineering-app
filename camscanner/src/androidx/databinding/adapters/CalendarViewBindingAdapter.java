// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import androidx.databinding.InverseBindingListener;
import android.widget.CalendarView$OnDateChangeListener;
import androidx.databinding.BindingAdapter;
import android.widget.CalendarView;
import androidx.databinding.InverseBindingMethod;
import androidx.databinding.InverseBindingMethods;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
@InverseBindingMethods({ @InverseBindingMethod(attribute = "android:date", type = CalendarView.class) })
public class CalendarViewBindingAdapter
{
    @BindingAdapter({ "android:date" })
    public static void setDate(final CalendarView calendarView, final long date) {
        if (calendarView.getDate() != date) {
            calendarView.setDate(date);
        }
    }
    
    @BindingAdapter(requireAll = false, value = { "android:onSelectedDayChange", "android:dateAttrChanged" })
    public static void setListeners(final CalendarView calendarView, final CalendarView$OnDateChangeListener onDateChangeListener, final InverseBindingListener inverseBindingListener) {
        if (inverseBindingListener == null) {
            calendarView.setOnDateChangeListener(onDateChangeListener);
        }
        else {
            calendarView.setOnDateChangeListener((CalendarView$OnDateChangeListener)new CalendarView$OnDateChangeListener(onDateChangeListener, inverseBindingListener) {
                final InverseBindingListener val$attrChange;
                final CalendarView$OnDateChangeListener val$onDayChange;
                
                public void onSelectedDayChange(final CalendarView calendarView, final int n, final int n2, final int n3) {
                    final CalendarView$OnDateChangeListener val$onDayChange = this.val$onDayChange;
                    if (val$onDayChange != null) {
                        val$onDayChange.onSelectedDayChange(calendarView, n, n2, n3);
                    }
                    this.val$attrChange.onChange();
                }
            });
        }
    }
}

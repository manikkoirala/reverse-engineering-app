// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import androidx.databinding.BindingAdapter;
import androidx.cardview.widget.CardView;
import androidx.databinding.BindingMethod;
import androidx.databinding.BindingMethods;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
@BindingMethods({ @BindingMethod(attribute = "cardCornerRadius", method = "setRadius", type = CardView.class), @BindingMethod(attribute = "cardMaxElevation", method = "setMaxCardElevation", type = CardView.class), @BindingMethod(attribute = "cardPreventCornerOverlap", method = "setPreventCornerOverlap", type = CardView.class), @BindingMethod(attribute = "cardUseCompatPadding", method = "setUseCompatPadding", type = CardView.class) })
public class CardViewBindingAdapter
{
    @BindingAdapter({ "contentPadding" })
    public static void setContentPadding(final CardView cardView, final int n) {
        cardView.setContentPadding(n, n, n, n);
    }
    
    @BindingAdapter({ "contentPaddingBottom" })
    public static void setContentPaddingBottom(final CardView cardView, final int n) {
        cardView.setContentPadding(cardView.getContentPaddingLeft(), cardView.getContentPaddingTop(), cardView.getContentPaddingRight(), n);
    }
    
    @BindingAdapter({ "contentPaddingLeft" })
    public static void setContentPaddingLeft(final CardView cardView, final int n) {
        cardView.setContentPadding(n, cardView.getContentPaddingTop(), cardView.getContentPaddingRight(), cardView.getContentPaddingBottom());
    }
    
    @BindingAdapter({ "contentPaddingRight" })
    public static void setContentPaddingRight(final CardView cardView, final int n) {
        cardView.setContentPadding(cardView.getContentPaddingLeft(), cardView.getContentPaddingTop(), n, cardView.getContentPaddingBottom());
    }
    
    @BindingAdapter({ "contentPaddingTop" })
    public static void setContentPaddingTop(final CardView cardView, final int n) {
        cardView.setContentPadding(cardView.getContentPaddingLeft(), n, cardView.getContentPaddingRight(), cardView.getContentPaddingBottom());
    }
}

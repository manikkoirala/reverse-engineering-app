// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import android.widget.AutoCompleteTextView$Validator;
import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingListener;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.AutoCompleteTextView;
import androidx.databinding.BindingMethod;
import androidx.databinding.BindingMethods;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
@BindingMethods({ @BindingMethod(attribute = "android:completionThreshold", method = "setThreshold", type = AutoCompleteTextView.class), @BindingMethod(attribute = "android:popupBackground", method = "setDropDownBackgroundDrawable", type = AutoCompleteTextView.class), @BindingMethod(attribute = "android:onDismiss", method = "setOnDismissListener", type = AutoCompleteTextView.class), @BindingMethod(attribute = "android:onItemClick", method = "setOnItemClickListener", type = AutoCompleteTextView.class) })
public class AutoCompleteTextViewBindingAdapter
{
    @BindingAdapter(requireAll = false, value = { "android:onItemSelected", "android:onNothingSelected" })
    public static void setOnItemSelectedListener(final AutoCompleteTextView autoCompleteTextView, final AdapterViewBindingAdapter.OnItemSelected onItemSelected, final AdapterViewBindingAdapter.OnNothingSelected onNothingSelected) {
        if (onItemSelected == null && onNothingSelected == null) {
            autoCompleteTextView.setOnItemSelectedListener((AdapterView$OnItemSelectedListener)null);
        }
        else {
            autoCompleteTextView.setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterViewBindingAdapter.OnItemSelectedComponentListener(onItemSelected, onNothingSelected, null));
        }
    }
    
    @BindingAdapter(requireAll = false, value = { "android:fixText", "android:isValid" })
    public static void setValidator(final AutoCompleteTextView autoCompleteTextView, final FixText fixText, final IsValid isValid) {
        if (fixText == null && isValid == null) {
            autoCompleteTextView.setValidator((AutoCompleteTextView$Validator)null);
        }
        else {
            autoCompleteTextView.setValidator((AutoCompleteTextView$Validator)new AutoCompleteTextView$Validator(isValid, fixText) {
                final FixText val$fixText;
                final IsValid val$isValid;
                
                public CharSequence fixText(final CharSequence charSequence) {
                    final FixText val$fixText = this.val$fixText;
                    CharSequence fixText = charSequence;
                    if (val$fixText != null) {
                        fixText = val$fixText.fixText(charSequence);
                    }
                    return fixText;
                }
                
                public boolean isValid(final CharSequence charSequence) {
                    final IsValid val$isValid = this.val$isValid;
                    return val$isValid == null || val$isValid.isValid(charSequence);
                }
            });
        }
    }
    
    public interface FixText
    {
        CharSequence fixText(final CharSequence p0);
    }
    
    public interface IsValid
    {
        boolean isValid(final CharSequence p0);
    }
}

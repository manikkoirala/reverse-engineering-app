// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import java.lang.ref.WeakReference;
import android.view.View;
import java.util.WeakHashMap;
import android.util.SparseArray;

public class ListenerUtil
{
    private static final SparseArray<WeakHashMap<View, WeakReference<?>>> sListeners;
    
    static {
        sListeners = new SparseArray();
    }
    
    public static <T> T getListener(final View view, final int n) {
        return (T)view.getTag(n);
    }
    
    public static <T> T trackListener(final View view, final T t, final int n) {
        final Object tag = view.getTag(n);
        view.setTag(n, (Object)t);
        return (T)tag;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import android.annotation.TargetApi;
import android.view.View$OnLongClickListener;
import android.view.View$OnLayoutChangeListener;
import androidx.databinding.library.baseAdapters.R;
import android.view.View$OnAttachStateChangeListener;
import android.view.View$OnClickListener;
import androidx.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.view.View;
import androidx.databinding.BindingMethod;
import androidx.databinding.BindingMethods;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
@BindingMethods({ @BindingMethod(attribute = "android:backgroundTint", method = "setBackgroundTintList", type = View.class), @BindingMethod(attribute = "android:fadeScrollbars", method = "setScrollbarFadingEnabled", type = View.class), @BindingMethod(attribute = "android:getOutline", method = "setOutlineProvider", type = View.class), @BindingMethod(attribute = "android:nextFocusForward", method = "setNextFocusForwardId", type = View.class), @BindingMethod(attribute = "android:nextFocusLeft", method = "setNextFocusLeftId", type = View.class), @BindingMethod(attribute = "android:nextFocusRight", method = "setNextFocusRightId", type = View.class), @BindingMethod(attribute = "android:nextFocusUp", method = "setNextFocusUpId", type = View.class), @BindingMethod(attribute = "android:nextFocusDown", method = "setNextFocusDownId", type = View.class), @BindingMethod(attribute = "android:requiresFadingEdge", method = "setVerticalFadingEdgeEnabled", type = View.class), @BindingMethod(attribute = "android:scrollbarDefaultDelayBeforeFade", method = "setScrollBarDefaultDelayBeforeFade", type = View.class), @BindingMethod(attribute = "android:scrollbarFadeDuration", method = "setScrollBarFadeDuration", type = View.class), @BindingMethod(attribute = "android:scrollbarSize", method = "setScrollBarSize", type = View.class), @BindingMethod(attribute = "android:scrollbarStyle", method = "setScrollBarStyle", type = View.class), @BindingMethod(attribute = "android:transformPivotX", method = "setPivotX", type = View.class), @BindingMethod(attribute = "android:transformPivotY", method = "setPivotY", type = View.class), @BindingMethod(attribute = "android:onDrag", method = "setOnDragListener", type = View.class), @BindingMethod(attribute = "android:onClick", method = "setOnClickListener", type = View.class), @BindingMethod(attribute = "android:onApplyWindowInsets", method = "setOnApplyWindowInsetsListener", type = View.class), @BindingMethod(attribute = "android:onCreateContextMenu", method = "setOnCreateContextMenuListener", type = View.class), @BindingMethod(attribute = "android:onFocusChange", method = "setOnFocusChangeListener", type = View.class), @BindingMethod(attribute = "android:onGenericMotion", method = "setOnGenericMotionListener", type = View.class), @BindingMethod(attribute = "android:onHover", method = "setOnHoverListener", type = View.class), @BindingMethod(attribute = "android:onKey", method = "setOnKeyListener", type = View.class), @BindingMethod(attribute = "android:onLongClick", method = "setOnLongClickListener", type = View.class), @BindingMethod(attribute = "android:onSystemUiVisibilityChange", method = "setOnSystemUiVisibilityChangeListener", type = View.class), @BindingMethod(attribute = "android:onTouch", method = "setOnTouchListener", type = View.class) })
public class ViewBindingAdapter
{
    public static final int FADING_EDGE_HORIZONTAL = 1;
    public static final int FADING_EDGE_NONE = 0;
    public static final int FADING_EDGE_VERTICAL = 2;
    
    private static int pixelsToDimensionPixelSize(final float n) {
        final int n2 = (int)(0.5f + n);
        if (n2 != 0) {
            return n2;
        }
        final float n3 = fcmpl(n, 0.0f);
        if (n3 == 0) {
            return 0;
        }
        if (n3 > 0) {
            return 1;
        }
        return -1;
    }
    
    @BindingAdapter({ "android:background" })
    public static void setBackground(final View view, final Drawable background) {
        view.setBackground(background);
    }
    
    @BindingAdapter({ "android:onClickListener", "android:clickable" })
    public static void setClickListener(final View view, final View$OnClickListener onClickListener, final boolean clickable) {
        view.setOnClickListener(onClickListener);
        view.setClickable(clickable);
    }
    
    @BindingAdapter(requireAll = false, value = { "android:onViewDetachedFromWindow", "android:onViewAttachedToWindow" })
    public static void setOnAttachStateChangeListener(final View view, final OnViewDetachedFromWindow onViewDetachedFromWindow, final OnViewAttachedToWindow onViewAttachedToWindow) {
        Object o;
        if (onViewDetachedFromWindow == null && onViewAttachedToWindow == null) {
            o = null;
        }
        else {
            o = new View$OnAttachStateChangeListener(onViewAttachedToWindow, onViewDetachedFromWindow) {
                final OnViewAttachedToWindow val$attach;
                final OnViewDetachedFromWindow val$detach;
                
                public void onViewAttachedToWindow(final View view) {
                    final OnViewAttachedToWindow val$attach = this.val$attach;
                    if (val$attach != null) {
                        val$attach.onViewAttachedToWindow(view);
                    }
                }
                
                public void onViewDetachedFromWindow(final View view) {
                    final OnViewDetachedFromWindow val$detach = this.val$detach;
                    if (val$detach != null) {
                        val$detach.onViewDetachedFromWindow(view);
                    }
                }
            };
        }
        final View$OnAttachStateChangeListener view$OnAttachStateChangeListener = ListenerUtil.trackListener(view, o, R.id.onAttachStateChangeListener);
        if (view$OnAttachStateChangeListener != null) {
            view.removeOnAttachStateChangeListener(view$OnAttachStateChangeListener);
        }
        if (o != null) {
            view.addOnAttachStateChangeListener((View$OnAttachStateChangeListener)o);
        }
    }
    
    @BindingAdapter({ "android:onClick", "android:clickable" })
    public static void setOnClick(final View view, final View$OnClickListener onClickListener, final boolean clickable) {
        view.setOnClickListener(onClickListener);
        view.setClickable(clickable);
    }
    
    @BindingAdapter({ "android:onLayoutChange" })
    public static void setOnLayoutChangeListener(final View view, final View$OnLayoutChangeListener view$OnLayoutChangeListener, final View$OnLayoutChangeListener view$OnLayoutChangeListener2) {
        if (view$OnLayoutChangeListener != null) {
            view.removeOnLayoutChangeListener(view$OnLayoutChangeListener);
        }
        if (view$OnLayoutChangeListener2 != null) {
            view.addOnLayoutChangeListener(view$OnLayoutChangeListener2);
        }
    }
    
    @BindingAdapter({ "android:onLongClick", "android:longClickable" })
    public static void setOnLongClick(final View view, final View$OnLongClickListener onLongClickListener, final boolean longClickable) {
        view.setOnLongClickListener(onLongClickListener);
        view.setLongClickable(longClickable);
    }
    
    @BindingAdapter({ "android:onLongClickListener", "android:longClickable" })
    public static void setOnLongClickListener(final View view, final View$OnLongClickListener onLongClickListener, final boolean longClickable) {
        view.setOnLongClickListener(onLongClickListener);
        view.setLongClickable(longClickable);
    }
    
    @BindingAdapter({ "android:padding" })
    public static void setPadding(final View view, final float n) {
        final int pixelsToDimensionPixelSize = pixelsToDimensionPixelSize(n);
        view.setPadding(pixelsToDimensionPixelSize, pixelsToDimensionPixelSize, pixelsToDimensionPixelSize, pixelsToDimensionPixelSize);
    }
    
    @BindingAdapter({ "android:paddingBottom" })
    public static void setPaddingBottom(final View view, final float n) {
        view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), view.getPaddingRight(), pixelsToDimensionPixelSize(n));
    }
    
    @BindingAdapter({ "android:paddingEnd" })
    public static void setPaddingEnd(final View view, final float n) {
        view.setPaddingRelative(view.getPaddingStart(), view.getPaddingTop(), pixelsToDimensionPixelSize(n), view.getPaddingBottom());
    }
    
    @BindingAdapter({ "android:paddingLeft" })
    public static void setPaddingLeft(final View view, final float n) {
        view.setPadding(pixelsToDimensionPixelSize(n), view.getPaddingTop(), view.getPaddingRight(), view.getPaddingBottom());
    }
    
    @BindingAdapter({ "android:paddingRight" })
    public static void setPaddingRight(final View view, final float n) {
        view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), pixelsToDimensionPixelSize(n), view.getPaddingBottom());
    }
    
    @BindingAdapter({ "android:paddingStart" })
    public static void setPaddingStart(final View view, final float n) {
        view.setPaddingRelative(pixelsToDimensionPixelSize(n), view.getPaddingTop(), view.getPaddingEnd(), view.getPaddingBottom());
    }
    
    @BindingAdapter({ "android:paddingTop" })
    public static void setPaddingTop(final View view, final float n) {
        view.setPadding(view.getPaddingLeft(), pixelsToDimensionPixelSize(n), view.getPaddingRight(), view.getPaddingBottom());
    }
    
    @BindingAdapter({ "android:requiresFadingEdge" })
    public static void setRequiresFadingEdge(final View view, final int n) {
        boolean horizontalFadingEdgeEnabled = false;
        final boolean verticalFadingEdgeEnabled = (n & 0x2) != 0x0;
        if ((n & 0x1) != 0x0) {
            horizontalFadingEdgeEnabled = true;
        }
        view.setVerticalFadingEdgeEnabled(verticalFadingEdgeEnabled);
        view.setHorizontalFadingEdgeEnabled(horizontalFadingEdgeEnabled);
    }
    
    @TargetApi(12)
    public interface OnViewAttachedToWindow
    {
        void onViewAttachedToWindow(final View p0);
    }
    
    @TargetApi(12)
    public interface OnViewDetachedFromWindow
    {
        void onViewDetachedFromWindow(final View p0);
    }
}

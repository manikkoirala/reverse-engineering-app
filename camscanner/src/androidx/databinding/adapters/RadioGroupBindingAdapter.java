// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import androidx.databinding.InverseBindingListener;
import android.widget.RadioGroup$OnCheckedChangeListener;
import androidx.databinding.BindingAdapter;
import android.widget.RadioGroup;
import androidx.databinding.InverseBindingMethod;
import androidx.databinding.InverseBindingMethods;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
@InverseBindingMethods({ @InverseBindingMethod(attribute = "android:checkedButton", method = "getCheckedRadioButtonId", type = RadioGroup.class) })
public class RadioGroupBindingAdapter
{
    @BindingAdapter({ "android:checkedButton" })
    public static void setCheckedButton(final RadioGroup radioGroup, final int n) {
        if (n != radioGroup.getCheckedRadioButtonId()) {
            radioGroup.check(n);
        }
    }
    
    @BindingAdapter(requireAll = false, value = { "android:onCheckedChanged", "android:checkedButtonAttrChanged" })
    public static void setListeners(final RadioGroup radioGroup, final RadioGroup$OnCheckedChangeListener onCheckedChangeListener, final InverseBindingListener inverseBindingListener) {
        if (inverseBindingListener == null) {
            radioGroup.setOnCheckedChangeListener(onCheckedChangeListener);
        }
        else {
            radioGroup.setOnCheckedChangeListener((RadioGroup$OnCheckedChangeListener)new RadioGroup$OnCheckedChangeListener(onCheckedChangeListener, inverseBindingListener) {
                final InverseBindingListener val$attrChange;
                final RadioGroup$OnCheckedChangeListener val$listener;
                
                public void onCheckedChanged(final RadioGroup radioGroup, final int n) {
                    final RadioGroup$OnCheckedChangeListener val$listener = this.val$listener;
                    if (val$listener != null) {
                        val$listener.onCheckedChanged(radioGroup, n);
                    }
                    this.val$attrChange.onChange();
                }
            });
        }
    }
}

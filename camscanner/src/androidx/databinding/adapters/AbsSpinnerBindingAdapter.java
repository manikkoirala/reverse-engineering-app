// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import android.widget.Adapter;
import android.view.View;
import android.widget.ArrayAdapter;
import androidx.databinding.BindingAdapter;
import android.widget.SpinnerAdapter;
import java.util.List;
import android.widget.AbsSpinner;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
public class AbsSpinnerBindingAdapter
{
    @BindingAdapter({ "android:entries" })
    public static <T> void setEntries(final AbsSpinner absSpinner, final List<T> list) {
        if (list != null) {
            final SpinnerAdapter adapter = absSpinner.getAdapter();
            if (adapter instanceof ObservableListAdapter) {
                ((ObservableListAdapter<T>)adapter).setList(list);
            }
            else {
                absSpinner.setAdapter((SpinnerAdapter)new ObservableListAdapter(((View)absSpinner).getContext(), (List<Object>)list, 17367048, 17367049, 0));
            }
        }
        else {
            absSpinner.setAdapter((SpinnerAdapter)null);
        }
    }
    
    @BindingAdapter({ "android:entries" })
    public static <T extends CharSequence> void setEntries(final AbsSpinner absSpinner, final T[] array) {
        if (array != null) {
            final SpinnerAdapter adapter = absSpinner.getAdapter();
            int n2;
            final int n = n2 = 1;
            Label_0071: {
                if (adapter != null) {
                    n2 = n;
                    if (((Adapter)adapter).getCount() == array.length) {
                        for (int i = 0; i < array.length; ++i) {
                            if (!array[i].equals(((Adapter)adapter).getItem(i))) {
                                n2 = n;
                                break Label_0071;
                            }
                        }
                        n2 = 0;
                    }
                }
            }
            if (n2 != 0) {
                final ArrayAdapter adapter2 = new ArrayAdapter(((View)absSpinner).getContext(), 17367048, (Object[])array);
                adapter2.setDropDownViewResource(17367049);
                absSpinner.setAdapter((SpinnerAdapter)adapter2);
            }
        }
        else {
            absSpinner.setAdapter((SpinnerAdapter)null);
        }
    }
}

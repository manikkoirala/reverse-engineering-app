// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import android.widget.ZoomControls;
import androidx.databinding.BindingMethod;
import androidx.databinding.BindingMethods;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
@BindingMethods({ @BindingMethod(attribute = "android:onZoomIn", method = "setOnZoomInClickListener", type = ZoomControls.class), @BindingMethod(attribute = "android:onZoomOut", method = "setOnZoomOutClickListener", type = ZoomControls.class) })
public class ZoomControlsBindingAdapter
{
}

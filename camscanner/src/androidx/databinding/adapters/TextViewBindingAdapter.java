// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import android.view.View;
import androidx.databinding.library.baseAdapters.R;
import android.text.Editable;
import android.text.TextWatcher;
import androidx.databinding.InverseBindingListener;
import android.text.Spanned;
import android.text.method.DialerKeyListener;
import android.text.method.TransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.InputFilter;
import android.text.InputFilter$LengthFilter;
import android.graphics.drawable.Drawable;
import android.text.method.DigitsKeyListener;
import android.widget.TextView$BufferType;
import androidx.databinding.BindingAdapter;
import android.text.method.KeyListener;
import android.text.method.TextKeyListener;
import android.text.method.TextKeyListener$Capitalize;
import androidx.databinding.InverseBindingAdapter;
import android.widget.TextView;
import androidx.databinding.BindingMethod;
import androidx.databinding.BindingMethods;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
@BindingMethods({ @BindingMethod(attribute = "android:autoLink", method = "setAutoLinkMask", type = TextView.class), @BindingMethod(attribute = "android:drawablePadding", method = "setCompoundDrawablePadding", type = TextView.class), @BindingMethod(attribute = "android:editorExtras", method = "setInputExtras", type = TextView.class), @BindingMethod(attribute = "android:inputType", method = "setRawInputType", type = TextView.class), @BindingMethod(attribute = "android:scrollHorizontally", method = "setHorizontallyScrolling", type = TextView.class), @BindingMethod(attribute = "android:textAllCaps", method = "setAllCaps", type = TextView.class), @BindingMethod(attribute = "android:textColorHighlight", method = "setHighlightColor", type = TextView.class), @BindingMethod(attribute = "android:textColorHint", method = "setHintTextColor", type = TextView.class), @BindingMethod(attribute = "android:textColorLink", method = "setLinkTextColor", type = TextView.class), @BindingMethod(attribute = "android:onEditorAction", method = "setOnEditorActionListener", type = TextView.class) })
public class TextViewBindingAdapter
{
    public static final int DECIMAL = 5;
    public static final int INTEGER = 1;
    public static final int SIGNED = 3;
    private static final String TAG = "TextViewBindingAdapters";
    
    @InverseBindingAdapter(attribute = "android:text", event = "android:textAttrChanged")
    public static String getTextString(final TextView textView) {
        return textView.getText().toString();
    }
    
    private static boolean haveContentsChanged(final CharSequence charSequence, final CharSequence charSequence2) {
        if (charSequence == null != (charSequence2 == null)) {
            return true;
        }
        if (charSequence == null) {
            return false;
        }
        final int length = charSequence.length();
        if (length != charSequence2.length()) {
            return true;
        }
        for (int i = 0; i < length; ++i) {
            if (charSequence.charAt(i) != charSequence2.charAt(i)) {
                return true;
            }
        }
        return false;
    }
    
    @BindingAdapter({ "android:autoText" })
    public static void setAutoText(final TextView textView, final boolean b) {
        final KeyListener keyListener = textView.getKeyListener();
        TextKeyListener$Capitalize textKeyListener$Capitalize = TextKeyListener$Capitalize.NONE;
        int inputType;
        if (keyListener != null) {
            inputType = keyListener.getInputType();
        }
        else {
            inputType = 0;
        }
        if ((inputType & 0x1000) != 0x0) {
            textKeyListener$Capitalize = TextKeyListener$Capitalize.CHARACTERS;
        }
        else if ((inputType & 0x2000) != 0x0) {
            textKeyListener$Capitalize = TextKeyListener$Capitalize.WORDS;
        }
        else if ((inputType & 0x4000) != 0x0) {
            textKeyListener$Capitalize = TextKeyListener$Capitalize.SENTENCES;
        }
        textView.setKeyListener((KeyListener)TextKeyListener.getInstance(b, textKeyListener$Capitalize));
    }
    
    @BindingAdapter({ "android:bufferType" })
    public static void setBufferType(final TextView textView, final TextView$BufferType textView$BufferType) {
        textView.setText(textView.getText(), textView$BufferType);
    }
    
    @BindingAdapter({ "android:capitalize" })
    public static void setCapitalize(final TextView textView, final TextKeyListener$Capitalize textKeyListener$Capitalize) {
        textView.setKeyListener((KeyListener)TextKeyListener.getInstance((textView.getKeyListener().getInputType() & 0x8000) != 0x0, textKeyListener$Capitalize));
    }
    
    @BindingAdapter({ "android:digits" })
    public static void setDigits(final TextView textView, final CharSequence charSequence) {
        if (charSequence != null) {
            textView.setKeyListener((KeyListener)DigitsKeyListener.getInstance(charSequence.toString()));
        }
        else if (textView.getKeyListener() instanceof DigitsKeyListener) {
            textView.setKeyListener((KeyListener)null);
        }
    }
    
    @BindingAdapter({ "android:drawableBottom" })
    public static void setDrawableBottom(final TextView textView, final Drawable intrinsicBounds) {
        setIntrinsicBounds(intrinsicBounds);
        final Drawable[] compoundDrawables = textView.getCompoundDrawables();
        textView.setCompoundDrawables(compoundDrawables[0], compoundDrawables[1], compoundDrawables[2], intrinsicBounds);
    }
    
    @BindingAdapter({ "android:drawableEnd" })
    public static void setDrawableEnd(final TextView textView, final Drawable intrinsicBounds) {
        setIntrinsicBounds(intrinsicBounds);
        final Drawable[] compoundDrawablesRelative = textView.getCompoundDrawablesRelative();
        textView.setCompoundDrawablesRelative(compoundDrawablesRelative[0], compoundDrawablesRelative[1], intrinsicBounds, compoundDrawablesRelative[3]);
    }
    
    @BindingAdapter({ "android:drawableLeft" })
    public static void setDrawableLeft(final TextView textView, final Drawable intrinsicBounds) {
        setIntrinsicBounds(intrinsicBounds);
        final Drawable[] compoundDrawables = textView.getCompoundDrawables();
        textView.setCompoundDrawables(intrinsicBounds, compoundDrawables[1], compoundDrawables[2], compoundDrawables[3]);
    }
    
    @BindingAdapter({ "android:drawableRight" })
    public static void setDrawableRight(final TextView textView, final Drawable intrinsicBounds) {
        setIntrinsicBounds(intrinsicBounds);
        final Drawable[] compoundDrawables = textView.getCompoundDrawables();
        textView.setCompoundDrawables(compoundDrawables[0], compoundDrawables[1], intrinsicBounds, compoundDrawables[3]);
    }
    
    @BindingAdapter({ "android:drawableStart" })
    public static void setDrawableStart(final TextView textView, final Drawable intrinsicBounds) {
        setIntrinsicBounds(intrinsicBounds);
        final Drawable[] compoundDrawablesRelative = textView.getCompoundDrawablesRelative();
        textView.setCompoundDrawablesRelative(intrinsicBounds, compoundDrawablesRelative[1], compoundDrawablesRelative[2], compoundDrawablesRelative[3]);
    }
    
    @BindingAdapter({ "android:drawableTop" })
    public static void setDrawableTop(final TextView textView, final Drawable intrinsicBounds) {
        setIntrinsicBounds(intrinsicBounds);
        final Drawable[] compoundDrawables = textView.getCompoundDrawables();
        textView.setCompoundDrawables(compoundDrawables[0], intrinsicBounds, compoundDrawables[2], compoundDrawables[3]);
    }
    
    @BindingAdapter({ "android:imeActionId" })
    public static void setImeActionLabel(final TextView textView, final int n) {
        textView.setImeActionLabel(textView.getImeActionLabel(), n);
    }
    
    @BindingAdapter({ "android:imeActionLabel" })
    public static void setImeActionLabel(final TextView textView, final CharSequence charSequence) {
        textView.setImeActionLabel(charSequence, textView.getImeActionId());
    }
    
    @BindingAdapter({ "android:inputMethod" })
    public static void setInputMethod(final TextView textView, final CharSequence obj) {
        try {
            textView.setKeyListener((KeyListener)Class.forName(obj.toString()).newInstance());
        }
        catch (final IllegalAccessException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Could not create input method: ");
            sb.append((Object)obj);
        }
        catch (final InstantiationException ex2) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Could not create input method: ");
            sb2.append((Object)obj);
        }
        catch (final ClassNotFoundException ex3) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Could not create input method: ");
            sb3.append((Object)obj);
        }
    }
    
    private static void setIntrinsicBounds(final Drawable drawable) {
        if (drawable != null) {
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        }
    }
    
    @BindingAdapter({ "android:lineSpacingExtra" })
    public static void setLineSpacingExtra(final TextView textView, final float n) {
        textView.setLineSpacing(n, textView.getLineSpacingMultiplier());
    }
    
    @BindingAdapter({ "android:lineSpacingMultiplier" })
    public static void setLineSpacingMultiplier(final TextView textView, final float n) {
        textView.setLineSpacing(textView.getLineSpacingExtra(), n);
    }
    
    @BindingAdapter({ "android:maxLength" })
    public static void setMaxLength(final TextView textView, final int n) {
        final InputFilter[] filters = textView.getFilters();
        InputFilter[] filters2 = null;
        Label_0149: {
            if (filters != null) {
                int i = 0;
                while (true) {
                    while (i < filters.length) {
                        final InputFilter inputFilter = filters[i];
                        if (inputFilter instanceof InputFilter$LengthFilter) {
                            if (((InputFilter$LengthFilter)inputFilter).getMax() != n) {
                                filters[i] = (InputFilter)new InputFilter$LengthFilter(n);
                            }
                            final boolean b = true;
                            filters2 = filters;
                            if (!b) {
                                final int n2 = filters.length + 1;
                                filters2 = new InputFilter[n2];
                                System.arraycopy(filters, 0, filters2, 0, filters.length);
                                filters2[n2 - 1] = (InputFilter)new InputFilter$LengthFilter(n);
                            }
                            break Label_0149;
                        }
                        else {
                            ++i;
                        }
                    }
                    final boolean b = false;
                    continue;
                }
            }
            filters2 = new InputFilter[] { (InputFilter)new InputFilter$LengthFilter(n) };
        }
        textView.setFilters(filters2);
    }
    
    @BindingAdapter({ "android:numeric" })
    public static void setNumeric(final TextView textView, final int n) {
        boolean b = true;
        final boolean b2 = (n & 0x3) != 0x0;
        if ((n & 0x5) == 0x0) {
            b = false;
        }
        textView.setKeyListener((KeyListener)DigitsKeyListener.getInstance(b2, b));
    }
    
    @BindingAdapter({ "android:password" })
    public static void setPassword(final TextView textView, final boolean b) {
        if (b) {
            textView.setTransformationMethod((TransformationMethod)PasswordTransformationMethod.getInstance());
        }
        else if (textView.getTransformationMethod() instanceof PasswordTransformationMethod) {
            textView.setTransformationMethod((TransformationMethod)null);
        }
    }
    
    @BindingAdapter({ "android:phoneNumber" })
    public static void setPhoneNumber(final TextView textView, final boolean b) {
        if (b) {
            textView.setKeyListener((KeyListener)DialerKeyListener.getInstance());
        }
        else if (textView.getKeyListener() instanceof DialerKeyListener) {
            textView.setKeyListener((KeyListener)null);
        }
    }
    
    @BindingAdapter({ "android:shadowColor" })
    public static void setShadowColor(final TextView textView, final int n) {
        textView.setShadowLayer(textView.getShadowRadius(), textView.getShadowDx(), textView.getShadowDy(), n);
    }
    
    @BindingAdapter({ "android:shadowDx" })
    public static void setShadowDx(final TextView textView, final float n) {
        textView.setShadowLayer(textView.getShadowRadius(), n, textView.getShadowDy(), textView.getShadowColor());
    }
    
    @BindingAdapter({ "android:shadowDy" })
    public static void setShadowDy(final TextView textView, final float n) {
        textView.setShadowLayer(textView.getShadowRadius(), textView.getShadowDx(), n, textView.getShadowColor());
    }
    
    @BindingAdapter({ "android:shadowRadius" })
    public static void setShadowRadius(final TextView textView, final float n) {
        textView.setShadowLayer(n, textView.getShadowDx(), textView.getShadowDy(), textView.getShadowColor());
    }
    
    @BindingAdapter({ "android:text" })
    public static void setText(final TextView textView, final CharSequence text) {
        final CharSequence text2 = textView.getText();
        if (text != text2) {
            if (text != null || text2.length() != 0) {
                if (text instanceof Spanned) {
                    if (text.equals(text2)) {
                        return;
                    }
                }
                else if (!haveContentsChanged(text, text2)) {
                    return;
                }
                textView.setText(text);
            }
        }
    }
    
    @BindingAdapter({ "android:textSize" })
    public static void setTextSize(final TextView textView, final float n) {
        textView.setTextSize(0, n);
    }
    
    @BindingAdapter(requireAll = false, value = { "android:beforeTextChanged", "android:onTextChanged", "android:afterTextChanged", "android:textAttrChanged" })
    public static void setTextWatcher(final TextView textView, final BeforeTextChanged beforeTextChanged, final OnTextChanged onTextChanged, final AfterTextChanged afterTextChanged, final InverseBindingListener inverseBindingListener) {
        Object o;
        if (beforeTextChanged == null && afterTextChanged == null && onTextChanged == null && inverseBindingListener == null) {
            o = null;
        }
        else {
            o = new TextWatcher(beforeTextChanged, onTextChanged, inverseBindingListener, afterTextChanged) {
                final AfterTextChanged val$after;
                final BeforeTextChanged val$before;
                final OnTextChanged val$on;
                final InverseBindingListener val$textAttrChanged;
                
                public void afterTextChanged(final Editable editable) {
                    final AfterTextChanged val$after = this.val$after;
                    if (val$after != null) {
                        val$after.afterTextChanged(editable);
                    }
                }
                
                public void beforeTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
                    final BeforeTextChanged val$before = this.val$before;
                    if (val$before != null) {
                        val$before.beforeTextChanged(charSequence, n, n2, n3);
                    }
                }
                
                public void onTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
                    final OnTextChanged val$on = this.val$on;
                    if (val$on != null) {
                        val$on.onTextChanged(charSequence, n, n2, n3);
                    }
                    final InverseBindingListener val$textAttrChanged = this.val$textAttrChanged;
                    if (val$textAttrChanged != null) {
                        val$textAttrChanged.onChange();
                    }
                }
            };
        }
        final TextWatcher textWatcher = ListenerUtil.trackListener((View)textView, o, R.id.textWatcher);
        if (textWatcher != null) {
            textView.removeTextChangedListener(textWatcher);
        }
        if (o != null) {
            textView.addTextChangedListener((TextWatcher)o);
        }
    }
    
    public interface AfterTextChanged
    {
        void afterTextChanged(final Editable p0);
    }
    
    public interface BeforeTextChanged
    {
        void beforeTextChanged(final CharSequence p0, final int p1, final int p2, final int p3);
    }
    
    public interface OnTextChanged
    {
        void onTextChanged(final CharSequence p0, final int p1, final int p2, final int p3);
    }
}

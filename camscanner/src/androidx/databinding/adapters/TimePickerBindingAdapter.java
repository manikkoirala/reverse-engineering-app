// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import androidx.databinding.InverseBindingListener;
import android.widget.TimePicker$OnTimeChangedListener;
import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;
import android.os.Build$VERSION;
import android.widget.TimePicker;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
public class TimePickerBindingAdapter
{
    @InverseBindingAdapter(attribute = "android:hour")
    public static int getHour(final TimePicker timePicker) {
        if (Build$VERSION.SDK_INT >= 23) {
            return \u3007080.\u3007080(timePicker);
        }
        final Integer currentHour = timePicker.getCurrentHour();
        if (currentHour == null) {
            return 0;
        }
        return currentHour;
    }
    
    @InverseBindingAdapter(attribute = "android:minute")
    public static int getMinute(final TimePicker timePicker) {
        if (Build$VERSION.SDK_INT >= 23) {
            return \u3007o\u3007.\u3007080(timePicker);
        }
        final Integer currentMinute = timePicker.getCurrentMinute();
        if (currentMinute == null) {
            return 0;
        }
        return currentMinute;
    }
    
    @BindingAdapter({ "android:hour" })
    public static void setHour(final TimePicker timePicker, final int i) {
        if (Build$VERSION.SDK_INT >= 23) {
            if (\u3007080.\u3007080(timePicker) != i) {
                \u3007o00\u3007\u3007Oo.\u3007080(timePicker, i);
            }
        }
        else if (timePicker.getCurrentHour() != i) {
            timePicker.setCurrentHour(Integer.valueOf(i));
        }
    }
    
    @BindingAdapter(requireAll = false, value = { "android:onTimeChanged", "android:hourAttrChanged", "android:minuteAttrChanged" })
    public static void setListeners(final TimePicker timePicker, final TimePicker$OnTimeChangedListener onTimeChangedListener, final InverseBindingListener inverseBindingListener, final InverseBindingListener inverseBindingListener2) {
        if (inverseBindingListener == null && inverseBindingListener2 == null) {
            timePicker.setOnTimeChangedListener(onTimeChangedListener);
        }
        else {
            timePicker.setOnTimeChangedListener((TimePicker$OnTimeChangedListener)new TimePicker$OnTimeChangedListener(onTimeChangedListener, inverseBindingListener, inverseBindingListener2) {
                final InverseBindingListener val$hourChange;
                final TimePicker$OnTimeChangedListener val$listener;
                final InverseBindingListener val$minuteChange;
                
                public void onTimeChanged(final TimePicker timePicker, final int n, final int n2) {
                    final TimePicker$OnTimeChangedListener val$listener = this.val$listener;
                    if (val$listener != null) {
                        val$listener.onTimeChanged(timePicker, n, n2);
                    }
                    final InverseBindingListener val$hourChange = this.val$hourChange;
                    if (val$hourChange != null) {
                        val$hourChange.onChange();
                    }
                    final InverseBindingListener val$minuteChange = this.val$minuteChange;
                    if (val$minuteChange != null) {
                        val$minuteChange.onChange();
                    }
                }
            });
        }
    }
    
    @BindingAdapter({ "android:minute" })
    public static void setMinute(final TimePicker timePicker, final int i) {
        if (Build$VERSION.SDK_INT >= 23) {
            if (\u3007o\u3007.\u3007080(timePicker) != i) {
                O8.\u3007080(timePicker, i);
            }
        }
        else if (timePicker.getCurrentMinute() != i) {
            timePicker.setCurrentHour(Integer.valueOf(i));
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import androidx.databinding.BindingAdapter;
import android.content.Context;
import androidx.appcompat.widget.SwitchCompat;
import androidx.databinding.BindingMethod;
import androidx.databinding.BindingMethods;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
@BindingMethods({ @BindingMethod(attribute = "android:thumb", method = "setThumbDrawable", type = SwitchCompat.class), @BindingMethod(attribute = "android:track", method = "setTrackDrawable", type = SwitchCompat.class) })
public class SwitchCompatBindingAdapter
{
    @BindingAdapter({ "android:switchTextAppearance" })
    public static void setSwitchTextAppearance(final SwitchCompat switchCompat, final int n) {
        switchCompat.setSwitchTextAppearance(null, n);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import android.annotation.TargetApi;
import android.widget.SearchView$OnSuggestionListener;
import androidx.databinding.BindingAdapter;
import android.widget.SearchView$OnQueryTextListener;
import android.widget.SearchView;
import androidx.databinding.BindingMethod;
import androidx.databinding.BindingMethods;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
@BindingMethods({ @BindingMethod(attribute = "android:onQueryTextFocusChange", method = "setOnQueryTextFocusChangeListener", type = SearchView.class), @BindingMethod(attribute = "android:onSearchClick", method = "setOnSearchClickListener", type = SearchView.class), @BindingMethod(attribute = "android:onClose", method = "setOnCloseListener", type = SearchView.class) })
public class SearchViewBindingAdapter
{
    @BindingAdapter(requireAll = false, value = { "android:onQueryTextSubmit", "android:onQueryTextChange" })
    public static void setOnQueryTextListener(final SearchView searchView, final OnQueryTextSubmit onQueryTextSubmit, final OnQueryTextChange onQueryTextChange) {
        if (onQueryTextSubmit == null && onQueryTextChange == null) {
            searchView.setOnQueryTextListener((SearchView$OnQueryTextListener)null);
        }
        else {
            searchView.setOnQueryTextListener((SearchView$OnQueryTextListener)new SearchView$OnQueryTextListener(onQueryTextSubmit, onQueryTextChange) {
                final OnQueryTextChange val$change;
                final OnQueryTextSubmit val$submit;
                
                public boolean onQueryTextChange(final String s) {
                    final OnQueryTextChange val$change = this.val$change;
                    return val$change != null && val$change.onQueryTextChange(s);
                }
                
                public boolean onQueryTextSubmit(final String s) {
                    final OnQueryTextSubmit val$submit = this.val$submit;
                    return val$submit != null && val$submit.onQueryTextSubmit(s);
                }
            });
        }
    }
    
    @BindingAdapter(requireAll = false, value = { "android:onSuggestionSelect", "android:onSuggestionClick" })
    public static void setOnSuggestListener(final SearchView searchView, final OnSuggestionSelect onSuggestionSelect, final OnSuggestionClick onSuggestionClick) {
        if (onSuggestionSelect == null && onSuggestionClick == null) {
            searchView.setOnSuggestionListener((SearchView$OnSuggestionListener)null);
        }
        else {
            searchView.setOnSuggestionListener((SearchView$OnSuggestionListener)new SearchView$OnSuggestionListener(onSuggestionSelect, onSuggestionClick) {
                final OnSuggestionClick val$change;
                final OnSuggestionSelect val$submit;
                
                public boolean onSuggestionClick(final int n) {
                    final OnSuggestionClick val$change = this.val$change;
                    return val$change != null && val$change.onSuggestionClick(n);
                }
                
                public boolean onSuggestionSelect(final int n) {
                    final OnSuggestionSelect val$submit = this.val$submit;
                    return val$submit != null && val$submit.onSuggestionSelect(n);
                }
            });
        }
    }
    
    @TargetApi(11)
    public interface OnQueryTextChange
    {
        boolean onQueryTextChange(final String p0);
    }
    
    @TargetApi(11)
    public interface OnQueryTextSubmit
    {
        boolean onQueryTextSubmit(final String p0);
    }
    
    @TargetApi(11)
    public interface OnSuggestionClick
    {
        boolean onSuggestionClick(final int p0);
    }
    
    @TargetApi(11)
    public interface OnSuggestionSelect
    {
        boolean onSuggestionSelect(final int p0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import androidx.databinding.BindingAdapter;
import android.view.View;
import androidx.databinding.library.baseAdapters.R;
import androidx.databinding.InverseBindingListener;
import android.widget.DatePicker$OnDateChangedListener;
import android.widget.DatePicker;
import androidx.databinding.InverseBindingMethod;
import androidx.databinding.InverseBindingMethods;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
@InverseBindingMethods({ @InverseBindingMethod(attribute = "android:year", type = DatePicker.class), @InverseBindingMethod(attribute = "android:month", type = DatePicker.class), @InverseBindingMethod(attribute = "android:day", method = "getDayOfMonth", type = DatePicker.class) })
public class DatePickerBindingAdapter
{
    @BindingAdapter(requireAll = false, value = { "android:year", "android:month", "android:day", "android:onDateChanged", "android:yearAttrChanged", "android:monthAttrChanged", "android:dayAttrChanged" })
    public static void setListeners(final DatePicker datePicker, int dayOfMonth, final int n, int onDateChanged, final DatePicker$OnDateChangedListener datePicker$OnDateChangedListener, final InverseBindingListener inverseBindingListener, final InverseBindingListener inverseBindingListener2, final InverseBindingListener inverseBindingListener3) {
        int year = dayOfMonth;
        if (dayOfMonth == 0) {
            year = datePicker.getYear();
        }
        if ((dayOfMonth = onDateChanged) == 0) {
            dayOfMonth = datePicker.getDayOfMonth();
        }
        if (inverseBindingListener == null && inverseBindingListener2 == null && inverseBindingListener3 == null) {
            datePicker.init(year, n, dayOfMonth, datePicker$OnDateChangedListener);
        }
        else {
            onDateChanged = R.id.onDateChanged;
            DateChangedListener dateChangedListener;
            if ((dateChangedListener = ListenerUtil.getListener((View)datePicker, onDateChanged)) == null) {
                dateChangedListener = new DateChangedListener();
                ListenerUtil.trackListener((View)datePicker, dateChangedListener, onDateChanged);
            }
            dateChangedListener.setListeners(datePicker$OnDateChangedListener, inverseBindingListener, inverseBindingListener2, inverseBindingListener3);
            datePicker.init(year, n, dayOfMonth, (DatePicker$OnDateChangedListener)dateChangedListener);
        }
    }
    
    private static class DateChangedListener implements DatePicker$OnDateChangedListener
    {
        InverseBindingListener mDayChanged;
        DatePicker$OnDateChangedListener mListener;
        InverseBindingListener mMonthChanged;
        InverseBindingListener mYearChanged;
        
        public void onDateChanged(final DatePicker datePicker, final int n, final int n2, final int n3) {
            final DatePicker$OnDateChangedListener mListener = this.mListener;
            if (mListener != null) {
                mListener.onDateChanged(datePicker, n, n2, n3);
            }
            final InverseBindingListener mYearChanged = this.mYearChanged;
            if (mYearChanged != null) {
                mYearChanged.onChange();
            }
            final InverseBindingListener mMonthChanged = this.mMonthChanged;
            if (mMonthChanged != null) {
                mMonthChanged.onChange();
            }
            final InverseBindingListener mDayChanged = this.mDayChanged;
            if (mDayChanged != null) {
                mDayChanged.onChange();
            }
        }
        
        public void setListeners(final DatePicker$OnDateChangedListener mListener, final InverseBindingListener mYearChanged, final InverseBindingListener mMonthChanged, final InverseBindingListener mDayChanged) {
            this.mListener = mListener;
            this.mYearChanged = mYearChanged;
            this.mMonthChanged = mMonthChanged;
            this.mDayChanged = mDayChanged;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import android.widget.Chronometer;
import androidx.databinding.BindingMethod;
import androidx.databinding.BindingMethods;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
@BindingMethods({ @BindingMethod(attribute = "android:onChronometerTick", method = "setOnChronometerTickListener", type = Chronometer.class) })
public class ChronometerBindingAdapter
{
}

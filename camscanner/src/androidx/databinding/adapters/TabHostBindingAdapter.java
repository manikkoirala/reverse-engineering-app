// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import androidx.databinding.InverseBindingListener;
import android.widget.TabHost$OnTabChangeListener;
import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;
import android.widget.TabHost;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
public class TabHostBindingAdapter
{
    @InverseBindingAdapter(attribute = "android:currentTab")
    public static int getCurrentTab(final TabHost tabHost) {
        return tabHost.getCurrentTab();
    }
    
    @InverseBindingAdapter(attribute = "android:currentTab")
    public static String getCurrentTabTag(final TabHost tabHost) {
        return tabHost.getCurrentTabTag();
    }
    
    @BindingAdapter({ "android:currentTab" })
    public static void setCurrentTab(final TabHost tabHost, final int currentTab) {
        if (tabHost.getCurrentTab() != currentTab) {
            tabHost.setCurrentTab(currentTab);
        }
    }
    
    @BindingAdapter({ "android:currentTab" })
    public static void setCurrentTabTag(final TabHost tabHost, final String currentTabByTag) {
        if (tabHost.getCurrentTabTag() != currentTabByTag) {
            tabHost.setCurrentTabByTag(currentTabByTag);
        }
    }
    
    @BindingAdapter(requireAll = false, value = { "android:onTabChanged", "android:currentTabAttrChanged" })
    public static void setListeners(final TabHost tabHost, final TabHost$OnTabChangeListener onTabChangedListener, final InverseBindingListener inverseBindingListener) {
        if (inverseBindingListener == null) {
            tabHost.setOnTabChangedListener(onTabChangedListener);
        }
        else {
            tabHost.setOnTabChangedListener((TabHost$OnTabChangeListener)new TabHost$OnTabChangeListener(onTabChangedListener, inverseBindingListener) {
                final InverseBindingListener val$attrChange;
                final TabHost$OnTabChangeListener val$listener;
                
                public void onTabChanged(final String s) {
                    final TabHost$OnTabChangeListener val$listener = this.val$listener;
                    if (val$listener != null) {
                        val$listener.onTabChanged(s);
                    }
                    this.val$attrChange.onChange();
                }
            });
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import androidx.databinding.BindingAdapter;
import android.widget.AbsListView$OnScrollListener;
import android.widget.AbsListView;
import androidx.databinding.BindingMethod;
import androidx.databinding.BindingMethods;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
@BindingMethods({ @BindingMethod(attribute = "android:listSelector", method = "setSelector", type = AbsListView.class), @BindingMethod(attribute = "android:scrollingCache", method = "setScrollingCacheEnabled", type = AbsListView.class), @BindingMethod(attribute = "android:smoothScrollbar", method = "setSmoothScrollbarEnabled", type = AbsListView.class), @BindingMethod(attribute = "android:onMovedToScrapHeap", method = "setRecyclerListener", type = AbsListView.class) })
public class AbsListViewBindingAdapter
{
    @BindingAdapter(requireAll = false, value = { "android:onScroll", "android:onScrollStateChanged" })
    public static void setOnScroll(final AbsListView absListView, final OnScroll onScroll, final OnScrollStateChanged onScrollStateChanged) {
        absListView.setOnScrollListener((AbsListView$OnScrollListener)new AbsListView$OnScrollListener(onScrollStateChanged, onScroll) {
            final OnScroll val$scrollListener;
            final OnScrollStateChanged val$scrollStateListener;
            
            public void onScroll(final AbsListView absListView, final int n, final int n2, final int n3) {
                final OnScroll val$scrollListener = this.val$scrollListener;
                if (val$scrollListener != null) {
                    val$scrollListener.onScroll(absListView, n, n2, n3);
                }
            }
            
            public void onScrollStateChanged(final AbsListView absListView, final int n) {
                final OnScrollStateChanged val$scrollStateListener = this.val$scrollStateListener;
                if (val$scrollStateListener != null) {
                    val$scrollStateListener.onScrollStateChanged(absListView, n);
                }
            }
        });
    }
    
    public interface OnScroll
    {
        void onScroll(final AbsListView p0, final int p1, final int p2, final int p3);
    }
    
    public interface OnScrollStateChanged
    {
        void onScrollStateChanged(final AbsListView p0, final int p1);
    }
}

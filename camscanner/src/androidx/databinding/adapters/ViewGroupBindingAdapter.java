// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import android.view.View;
import android.view.ViewGroup$OnHierarchyChangeListener;
import android.view.animation.Animation;
import android.view.animation.Animation$AnimationListener;
import androidx.databinding.BindingAdapter;
import android.annotation.TargetApi;
import android.animation.LayoutTransition;
import android.view.ViewGroup;
import androidx.databinding.BindingMethod;
import androidx.databinding.BindingMethods;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
@BindingMethods({ @BindingMethod(attribute = "android:alwaysDrawnWithCache", method = "setAlwaysDrawnWithCacheEnabled", type = ViewGroup.class), @BindingMethod(attribute = "android:animationCache", method = "setAnimationCacheEnabled", type = ViewGroup.class), @BindingMethod(attribute = "android:splitMotionEvents", method = "setMotionEventSplittingEnabled", type = ViewGroup.class) })
public class ViewGroupBindingAdapter
{
    @TargetApi(11)
    @BindingAdapter({ "android:animateLayoutChanges" })
    public static void setAnimateLayoutChanges(final ViewGroup viewGroup, final boolean b) {
        if (b) {
            viewGroup.setLayoutTransition(new LayoutTransition());
        }
        else {
            viewGroup.setLayoutTransition((LayoutTransition)null);
        }
    }
    
    @BindingAdapter(requireAll = false, value = { "android:onAnimationStart", "android:onAnimationEnd", "android:onAnimationRepeat" })
    public static void setListener(final ViewGroup viewGroup, final OnAnimationStart onAnimationStart, final OnAnimationEnd onAnimationEnd, final OnAnimationRepeat onAnimationRepeat) {
        if (onAnimationStart == null && onAnimationEnd == null && onAnimationRepeat == null) {
            viewGroup.setLayoutAnimationListener((Animation$AnimationListener)null);
        }
        else {
            viewGroup.setLayoutAnimationListener((Animation$AnimationListener)new Animation$AnimationListener(onAnimationStart, onAnimationEnd, onAnimationRepeat) {
                final OnAnimationEnd val$end;
                final OnAnimationRepeat val$repeat;
                final OnAnimationStart val$start;
                
                public void onAnimationEnd(final Animation animation) {
                    final OnAnimationEnd val$end = this.val$end;
                    if (val$end != null) {
                        val$end.onAnimationEnd(animation);
                    }
                }
                
                public void onAnimationRepeat(final Animation animation) {
                    final OnAnimationRepeat val$repeat = this.val$repeat;
                    if (val$repeat != null) {
                        val$repeat.onAnimationRepeat(animation);
                    }
                }
                
                public void onAnimationStart(final Animation animation) {
                    final OnAnimationStart val$start = this.val$start;
                    if (val$start != null) {
                        val$start.onAnimationStart(animation);
                    }
                }
            });
        }
    }
    
    @BindingAdapter(requireAll = false, value = { "android:onChildViewAdded", "android:onChildViewRemoved" })
    public static void setListener(final ViewGroup viewGroup, final OnChildViewAdded onChildViewAdded, final OnChildViewRemoved onChildViewRemoved) {
        if (onChildViewAdded == null && onChildViewRemoved == null) {
            viewGroup.setOnHierarchyChangeListener((ViewGroup$OnHierarchyChangeListener)null);
        }
        else {
            viewGroup.setOnHierarchyChangeListener((ViewGroup$OnHierarchyChangeListener)new ViewGroup$OnHierarchyChangeListener(onChildViewAdded, onChildViewRemoved) {
                final OnChildViewAdded val$added;
                final OnChildViewRemoved val$removed;
                
                public void onChildViewAdded(final View view, final View view2) {
                    final OnChildViewAdded val$added = this.val$added;
                    if (val$added != null) {
                        val$added.onChildViewAdded(view, view2);
                    }
                }
                
                public void onChildViewRemoved(final View view, final View view2) {
                    final OnChildViewRemoved val$removed = this.val$removed;
                    if (val$removed != null) {
                        val$removed.onChildViewRemoved(view, view2);
                    }
                }
            });
        }
    }
    
    public interface OnAnimationEnd
    {
        void onAnimationEnd(final Animation p0);
    }
    
    public interface OnAnimationRepeat
    {
        void onAnimationRepeat(final Animation p0);
    }
    
    public interface OnAnimationStart
    {
        void onAnimationStart(final Animation p0);
    }
    
    public interface OnChildViewAdded
    {
        void onChildViewAdded(final View p0, final View p1);
    }
    
    public interface OnChildViewRemoved
    {
        void onChildViewRemoved(final View p0, final View p1);
    }
}

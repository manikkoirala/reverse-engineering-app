// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import android.net.Uri;
import androidx.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import androidx.databinding.BindingMethod;
import androidx.databinding.BindingMethods;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
@BindingMethods({ @BindingMethod(attribute = "android:tint", method = "setImageTintList", type = ImageView.class), @BindingMethod(attribute = "android:tintMode", method = "setImageTintMode", type = ImageView.class) })
public class ImageViewBindingAdapter
{
    @BindingAdapter({ "android:src" })
    public static void setImageDrawable(final ImageView imageView, final Drawable imageDrawable) {
        imageView.setImageDrawable(imageDrawable);
    }
    
    @BindingAdapter({ "android:src" })
    public static void setImageUri(final ImageView imageView, final Uri imageURI) {
        imageView.setImageURI(imageURI);
    }
    
    @BindingAdapter({ "android:src" })
    public static void setImageUri(final ImageView imageView, final String s) {
        if (s == null) {
            imageView.setImageURI((Uri)null);
        }
        else {
            imageView.setImageURI(Uri.parse(s));
        }
    }
}

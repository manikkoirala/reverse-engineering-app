// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import androidx.databinding.BindingAdapter;
import android.content.Context;
import android.widget.Switch;
import androidx.databinding.BindingMethod;
import androidx.databinding.BindingMethods;
import androidx.annotation.RestrictTo;
import android.annotation.TargetApi;

@TargetApi(14)
@RestrictTo({ RestrictTo.Scope.LIBRARY })
@BindingMethods({ @BindingMethod(attribute = "android:thumb", method = "setThumbDrawable", type = Switch.class), @BindingMethod(attribute = "android:track", method = "setTrackDrawable", type = Switch.class) })
public class SwitchBindingAdapter
{
    @BindingAdapter({ "android:switchTextAppearance" })
    public static void setSwitchTextAppearance(final Switch switch1, final int n) {
        switch1.setSwitchTextAppearance((Context)null, n);
    }
}

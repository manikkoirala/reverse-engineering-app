// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import android.widget.TextView;
import android.view.ViewGroup;
import android.view.View;
import androidx.databinding.ObservableList;
import java.util.List;
import android.view.LayoutInflater;
import android.content.Context;
import androidx.annotation.RestrictTo;
import android.widget.BaseAdapter;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
class ObservableListAdapter<T> extends BaseAdapter
{
    private final Context mContext;
    private final int mDropDownResourceId;
    private final LayoutInflater mLayoutInflater;
    private List<T> mList;
    private ObservableList.OnListChangedCallback mListChangedCallback;
    private final int mResourceId;
    private final int mTextViewResourceId;
    
    public ObservableListAdapter(final Context mContext, final List<T> list, final int mResourceId, final int mDropDownResourceId, final int mTextViewResourceId) {
        this.mContext = mContext;
        this.mResourceId = mResourceId;
        this.mDropDownResourceId = mDropDownResourceId;
        this.mTextViewResourceId = mTextViewResourceId;
        LayoutInflater mLayoutInflater;
        if (mResourceId == 0) {
            mLayoutInflater = null;
        }
        else {
            mLayoutInflater = (LayoutInflater)mContext.getSystemService("layout_inflater");
        }
        this.mLayoutInflater = mLayoutInflater;
        this.setList(list);
    }
    
    public int getCount() {
        return this.mList.size();
    }
    
    public View getDropDownView(final int n, final View view, final ViewGroup viewGroup) {
        return this.getViewForResource(this.mDropDownResourceId, n, view, viewGroup);
    }
    
    public Object getItem(final int n) {
        return this.mList.get(n);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(final int n, final View view, final ViewGroup viewGroup) {
        return this.getViewForResource(this.mResourceId, n, view, viewGroup);
    }
    
    public View getViewForResource(int mTextViewResourceId, final int n, View viewById, final ViewGroup viewGroup) {
        Object inflate = viewById;
        if (viewById == null) {
            if (mTextViewResourceId == 0) {
                inflate = new TextView(this.mContext);
            }
            else {
                inflate = this.mLayoutInflater.inflate(mTextViewResourceId, viewGroup, false);
            }
        }
        mTextViewResourceId = this.mTextViewResourceId;
        if (mTextViewResourceId == 0) {
            viewById = (View)inflate;
        }
        else {
            viewById = ((View)inflate).findViewById(mTextViewResourceId);
        }
        final TextView textView = (TextView)viewById;
        final T value = this.mList.get(n);
        CharSequence value2;
        if (value instanceof CharSequence) {
            value2 = (CharSequence)value;
        }
        else {
            value2 = String.valueOf(value);
        }
        textView.setText(value2);
        return (View)inflate;
    }
    
    public void setList(final List<T> mList) {
        final List<T> mList2 = this.mList;
        if (mList2 == mList) {
            return;
        }
        if (mList2 instanceof ObservableList) {
            ((ObservableList)mList2).removeOnListChangedCallback(this.mListChangedCallback);
        }
        this.mList = mList;
        if (mList instanceof ObservableList) {
            if (this.mListChangedCallback == null) {
                this.mListChangedCallback = new ObservableList.OnListChangedCallback(this) {
                    final ObservableListAdapter this$0;
                    
                    @Override
                    public void onChanged(final ObservableList list) {
                        this.this$0.notifyDataSetChanged();
                    }
                    
                    @Override
                    public void onItemRangeChanged(final ObservableList list, final int n, final int n2) {
                        this.this$0.notifyDataSetChanged();
                    }
                    
                    @Override
                    public void onItemRangeInserted(final ObservableList list, final int n, final int n2) {
                        this.this$0.notifyDataSetChanged();
                    }
                    
                    @Override
                    public void onItemRangeMoved(final ObservableList list, final int n, final int n2, final int n3) {
                        this.this$0.notifyDataSetChanged();
                    }
                    
                    @Override
                    public void onItemRangeRemoved(final ObservableList list, final int n, final int n2) {
                        this.this$0.notifyDataSetChanged();
                    }
                };
            }
            ((ObservableList)this.mList).addOnListChangedCallback(this.mListChangedCallback);
        }
        this.notifyDataSetChanged();
    }
}

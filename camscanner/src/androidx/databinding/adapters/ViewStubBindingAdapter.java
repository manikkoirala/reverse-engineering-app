// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import androidx.databinding.BindingAdapter;
import android.view.ViewStub$OnInflateListener;
import androidx.databinding.ViewStubProxy;
import androidx.databinding.Untaggable;
import android.view.ViewStub;
import androidx.databinding.BindingMethod;
import androidx.databinding.BindingMethods;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
@BindingMethods({ @BindingMethod(attribute = "android:layout", method = "setLayoutResource", type = ViewStub.class) })
@Untaggable({ "android.view.ViewStub" })
public class ViewStubBindingAdapter
{
    @BindingAdapter({ "android:onInflate" })
    public static void setOnInflateListener(final ViewStubProxy viewStubProxy, final ViewStub$OnInflateListener onInflateListener) {
        viewStubProxy.setOnInflateListener(onInflateListener);
    }
}

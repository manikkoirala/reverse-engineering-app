// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import android.view.View;
import android.widget.Adapter;
import androidx.databinding.BindingAdapter;
import android.widget.AdapterView$OnItemSelectedListener;
import androidx.databinding.InverseBindingListener;
import androidx.databinding.InverseBindingMethod;
import androidx.databinding.InverseBindingMethods;
import android.widget.AdapterView;
import androidx.databinding.BindingMethod;
import androidx.databinding.BindingMethods;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
@BindingMethods({ @BindingMethod(attribute = "android:onItemClick", method = "setOnItemClickListener", type = AdapterView.class), @BindingMethod(attribute = "android:onItemLongClick", method = "setOnItemLongClickListener", type = AdapterView.class) })
@InverseBindingMethods({ @InverseBindingMethod(attribute = "android:selectedItemPosition", type = AdapterView.class), @InverseBindingMethod(attribute = "android:selection", event = "android:selectedItemPositionAttrChanged", method = "getSelectedItemPosition", type = AdapterView.class) })
public class AdapterViewBindingAdapter
{
    @BindingAdapter(requireAll = false, value = { "android:onItemSelected", "android:onNothingSelected", "android:selectedItemPositionAttrChanged" })
    public static void setOnItemSelectedListener(final AdapterView adapterView, final OnItemSelected onItemSelected, final OnNothingSelected onNothingSelected, final InverseBindingListener inverseBindingListener) {
        if (onItemSelected == null && onNothingSelected == null && inverseBindingListener == null) {
            adapterView.setOnItemSelectedListener((AdapterView$OnItemSelectedListener)null);
        }
        else {
            adapterView.setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new OnItemSelectedComponentListener(onItemSelected, onNothingSelected, inverseBindingListener));
        }
    }
    
    @BindingAdapter({ "android:selectedItemPosition" })
    public static void setSelectedItemPosition(final AdapterView adapterView, final int selection) {
        if (adapterView.getSelectedItemPosition() != selection) {
            adapterView.setSelection(selection);
        }
    }
    
    @BindingAdapter({ "android:selectedItemPosition", "android:adapter" })
    public static void setSelectedItemPosition(final AdapterView adapterView, final int n, final Adapter adapter) {
        if (adapter != adapterView.getAdapter()) {
            adapterView.setAdapter(adapter);
            adapterView.setSelection(n);
        }
        else if (adapterView.getSelectedItemPosition() != n) {
            adapterView.setSelection(n);
        }
    }
    
    @BindingAdapter({ "android:selection" })
    public static void setSelection(final AdapterView adapterView, final int n) {
        setSelectedItemPosition(adapterView, n);
    }
    
    @BindingAdapter({ "android:selection", "android:adapter" })
    public static void setSelection(final AdapterView adapterView, final int n, final Adapter adapter) {
        setSelectedItemPosition(adapterView, n, adapter);
    }
    
    public interface OnItemSelected
    {
        void onItemSelected(final AdapterView<?> p0, final View p1, final int p2, final long p3);
    }
    
    public static class OnItemSelectedComponentListener implements AdapterView$OnItemSelectedListener
    {
        private final InverseBindingListener mAttrChanged;
        private final OnNothingSelected mNothingSelected;
        private final OnItemSelected mSelected;
        
        public OnItemSelectedComponentListener(final OnItemSelected mSelected, final OnNothingSelected mNothingSelected, final InverseBindingListener mAttrChanged) {
            this.mSelected = mSelected;
            this.mNothingSelected = mNothingSelected;
            this.mAttrChanged = mAttrChanged;
        }
        
        public void onItemSelected(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
            final OnItemSelected mSelected = this.mSelected;
            if (mSelected != null) {
                mSelected.onItemSelected(adapterView, view, n, n2);
            }
            final InverseBindingListener mAttrChanged = this.mAttrChanged;
            if (mAttrChanged != null) {
                mAttrChanged.onChange();
            }
        }
        
        public void onNothingSelected(final AdapterView<?> adapterView) {
            final OnNothingSelected mNothingSelected = this.mNothingSelected;
            if (mNothingSelected != null) {
                mNothingSelected.onNothingSelected(adapterView);
            }
            final InverseBindingListener mAttrChanged = this.mAttrChanged;
            if (mAttrChanged != null) {
                mAttrChanged.onChange();
            }
        }
    }
    
    public interface OnNothingSelected
    {
        void onNothingSelected(final AdapterView<?> p0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingListener;
import android.widget.NumberPicker$OnValueChangeListener;
import androidx.databinding.InverseBindingMethod;
import androidx.databinding.InverseBindingMethods;
import android.widget.NumberPicker;
import androidx.databinding.BindingMethod;
import androidx.databinding.BindingMethods;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
@BindingMethods({ @BindingMethod(attribute = "android:format", method = "setFormatter", type = NumberPicker.class), @BindingMethod(attribute = "android:onScrollStateChange", method = "setOnScrollListener", type = NumberPicker.class) })
@InverseBindingMethods({ @InverseBindingMethod(attribute = "android:value", type = NumberPicker.class) })
public class NumberPickerBindingAdapter
{
    @BindingAdapter(requireAll = false, value = { "android:onValueChange", "android:valueAttrChanged" })
    public static void setListeners(final NumberPicker numberPicker, final NumberPicker$OnValueChangeListener onValueChangedListener, final InverseBindingListener inverseBindingListener) {
        if (inverseBindingListener == null) {
            numberPicker.setOnValueChangedListener(onValueChangedListener);
        }
        else {
            numberPicker.setOnValueChangedListener((NumberPicker$OnValueChangeListener)new NumberPicker$OnValueChangeListener(onValueChangedListener, inverseBindingListener) {
                final InverseBindingListener val$attrChange;
                final NumberPicker$OnValueChangeListener val$listener;
                
                public void onValueChange(final NumberPicker numberPicker, final int n, final int n2) {
                    final NumberPicker$OnValueChangeListener val$listener = this.val$listener;
                    if (val$listener != null) {
                        val$listener.onValueChange(numberPicker, n, n2);
                    }
                    this.val$attrChange.onChange();
                }
            });
        }
    }
    
    @BindingAdapter({ "android:value" })
    public static void setValue(final NumberPicker numberPicker, final int value) {
        if (numberPicker.getValue() != value) {
            numberPicker.setValue(value);
        }
    }
}

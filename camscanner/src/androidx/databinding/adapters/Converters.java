// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import android.graphics.drawable.ColorDrawable;
import androidx.databinding.BindingConversion;
import android.content.res.ColorStateList;

public class Converters
{
    @BindingConversion
    public static ColorStateList convertColorToColorStateList(final int n) {
        return ColorStateList.valueOf(n);
    }
    
    @BindingConversion
    public static ColorDrawable convertColorToDrawable(final int n) {
        return new ColorDrawable(n);
    }
}

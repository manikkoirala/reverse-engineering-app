// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingListener;
import android.widget.RatingBar$OnRatingBarChangeListener;
import android.widget.RatingBar;
import androidx.databinding.InverseBindingMethod;
import androidx.databinding.InverseBindingMethods;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
@InverseBindingMethods({ @InverseBindingMethod(attribute = "android:rating", type = RatingBar.class) })
public class RatingBarBindingAdapter
{
    @BindingAdapter(requireAll = false, value = { "android:onRatingChanged", "android:ratingAttrChanged" })
    public static void setListeners(final RatingBar ratingBar, final RatingBar$OnRatingBarChangeListener onRatingBarChangeListener, final InverseBindingListener inverseBindingListener) {
        if (inverseBindingListener == null) {
            ratingBar.setOnRatingBarChangeListener(onRatingBarChangeListener);
        }
        else {
            ratingBar.setOnRatingBarChangeListener((RatingBar$OnRatingBarChangeListener)new RatingBar$OnRatingBarChangeListener(onRatingBarChangeListener, inverseBindingListener) {
                final RatingBar$OnRatingBarChangeListener val$listener;
                final InverseBindingListener val$ratingChange;
                
                public void onRatingChanged(final RatingBar ratingBar, final float n, final boolean b) {
                    final RatingBar$OnRatingBarChangeListener val$listener = this.val$listener;
                    if (val$listener != null) {
                        val$listener.onRatingChanged(ratingBar, n, b);
                    }
                    this.val$ratingChange.onChange();
                }
            });
        }
    }
    
    @BindingAdapter({ "android:rating" })
    public static void setRating(final RatingBar ratingBar, final float rating) {
        if (ratingBar.getRating() != rating) {
            ratingBar.setRating(rating);
        }
    }
}

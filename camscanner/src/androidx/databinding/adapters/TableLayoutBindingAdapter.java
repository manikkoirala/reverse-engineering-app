// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding.adapters;

import androidx.databinding.BindingAdapter;
import android.widget.TableLayout;
import android.util.SparseBooleanArray;
import java.util.regex.Pattern;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
public class TableLayoutBindingAdapter
{
    private static final int MAX_COLUMNS = 20;
    private static Pattern sColumnPattern;
    
    static {
        TableLayoutBindingAdapter.sColumnPattern = Pattern.compile("\\s*,\\s*");
    }
    
    private static SparseBooleanArray parseColumns(final CharSequence p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   android/util/SparseBooleanArray.<init>:()V
        //     7: astore          4
        //     9: aload_0        
        //    10: ifnonnull       16
        //    13: aload           4
        //    15: areturn        
        //    16: getstatic       androidx/databinding/adapters/TableLayoutBindingAdapter.sColumnPattern:Ljava/util/regex/Pattern;
        //    19: aload_0        
        //    20: invokevirtual   java/util/regex/Pattern.split:(Ljava/lang/CharSequence;)[Ljava/lang/String;
        //    23: astore_0       
        //    24: aload_0        
        //    25: arraylength    
        //    26: istore_2       
        //    27: iconst_0       
        //    28: istore_1       
        //    29: iload_1        
        //    30: iload_2        
        //    31: if_icmpge       62
        //    34: aload_0        
        //    35: iload_1        
        //    36: aaload         
        //    37: astore          5
        //    39: aload           5
        //    41: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //    44: istore_3       
        //    45: iload_3        
        //    46: iflt            56
        //    49: aload           4
        //    51: iload_3        
        //    52: iconst_1       
        //    53: invokevirtual   android/util/SparseBooleanArray.put:(IZ)V
        //    56: iinc            1, 1
        //    59: goto            29
        //    62: aload           4
        //    64: areturn        
        //    65: astore          5
        //    67: goto            56
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                             
        //  -----  -----  -----  -----  ---------------------------------
        //  39     45     65     70     Ljava/lang/NumberFormatException;
        //  49     56     65     70     Ljava/lang/NumberFormatException;
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException: Cannot invoke "com.strobel.assembler.metadata.TypeReference.getSimpleType()" because "type" is null
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:837)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2086)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @BindingAdapter({ "android:collapseColumns" })
    public static void setCollapseColumns(final TableLayout tableLayout, final CharSequence charSequence) {
        final SparseBooleanArray columns = parseColumns(charSequence);
        for (int i = 0; i < 20; ++i) {
            final boolean value = columns.get(i, false);
            if (value != tableLayout.isColumnCollapsed(i)) {
                tableLayout.setColumnCollapsed(i, value);
            }
        }
    }
    
    @BindingAdapter({ "android:shrinkColumns" })
    public static void setShrinkColumns(final TableLayout tableLayout, final CharSequence charSequence) {
        int i = 0;
        if (charSequence != null && charSequence.length() > 0 && charSequence.charAt(0) == '*') {
            tableLayout.setShrinkAllColumns(true);
        }
        else {
            tableLayout.setShrinkAllColumns(false);
            for (SparseBooleanArray columns = parseColumns(charSequence); i < columns.size(); ++i) {
                final int key = columns.keyAt(i);
                final boolean value = columns.valueAt(i);
                if (value) {
                    tableLayout.setColumnShrinkable(key, value);
                }
            }
        }
    }
    
    @BindingAdapter({ "android:stretchColumns" })
    public static void setStretchColumns(final TableLayout tableLayout, final CharSequence charSequence) {
        int i = 0;
        if (charSequence != null && charSequence.length() > 0 && charSequence.charAt(0) == '*') {
            tableLayout.setStretchAllColumns(true);
        }
        else {
            tableLayout.setStretchAllColumns(false);
            for (SparseBooleanArray columns = parseColumns(charSequence); i < columns.size(); ++i) {
                final int key = columns.keyAt(i);
                final boolean value = columns.valueAt(i);
                if (value) {
                    tableLayout.setColumnStretchable(key, value);
                }
            }
        }
    }
}

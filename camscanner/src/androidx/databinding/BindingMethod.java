// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target({ ElementType.ANNOTATION_TYPE })
public @interface BindingMethod {
    String attribute();
    
    String method();
    
    Class type();
}

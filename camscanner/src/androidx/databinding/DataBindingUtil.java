// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewParent;
import android.view.ViewGroup;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.view.View;

public class DataBindingUtil
{
    private static DataBindingComponent sDefaultComponent;
    private static DataBinderMapper sMapper;
    
    static {
        DataBindingUtil.sMapper = (DataBinderMapper)new DataBinderMapperImpl();
        DataBindingUtil.sDefaultComponent = null;
    }
    
    private DataBindingUtil() {
    }
    
    @Nullable
    public static <T extends ViewDataBinding> T bind(@NonNull final View view) {
        return bind(view, DataBindingUtil.sDefaultComponent);
    }
    
    @Nullable
    public static <T extends ViewDataBinding> T bind(@NonNull final View view, final DataBindingComponent dataBindingComponent) {
        final ViewDataBinding binding = getBinding(view);
        if (binding != null) {
            return (T)binding;
        }
        final Object tag = view.getTag();
        if (!(tag instanceof String)) {
            throw new IllegalArgumentException("View is not a binding layout");
        }
        final int layoutId = DataBindingUtil.sMapper.getLayoutId((String)tag);
        if (layoutId != 0) {
            return (T)DataBindingUtil.sMapper.getDataBinder(dataBindingComponent, view, layoutId);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("View is not a binding layout. Tag: ");
        sb.append(tag);
        throw new IllegalArgumentException(sb.toString());
    }
    
    static <T extends ViewDataBinding> T bind(final DataBindingComponent dataBindingComponent, final View view, final int n) {
        return (T)DataBindingUtil.sMapper.getDataBinder(dataBindingComponent, view, n);
    }
    
    static <T extends ViewDataBinding> T bind(final DataBindingComponent dataBindingComponent, final View[] array, final int n) {
        return (T)DataBindingUtil.sMapper.getDataBinder(dataBindingComponent, array, n);
    }
    
    private static <T extends ViewDataBinding> T bindToAddedViews(final DataBindingComponent dataBindingComponent, final ViewGroup viewGroup, final int n, final int n2) {
        final int childCount = viewGroup.getChildCount();
        final int n3 = childCount - n;
        if (n3 == 1) {
            return (T)bind(dataBindingComponent, viewGroup.getChildAt(childCount - 1), n2);
        }
        final View[] array = new View[n3];
        for (int i = 0; i < n3; ++i) {
            array[i] = viewGroup.getChildAt(i + n);
        }
        return (T)bind(dataBindingComponent, array, n2);
    }
    
    @Nullable
    public static String convertBrIdToString(final int n) {
        return DataBindingUtil.sMapper.convertBrIdToString(n);
    }
    
    @Nullable
    public static <T extends ViewDataBinding> T findBinding(@NonNull View view) {
        while (view != null) {
            final ViewDataBinding binding = ViewDataBinding.getBinding(view);
            if (binding != null) {
                return (T)binding;
            }
            final Object tag = view.getTag();
            if (tag instanceof String) {
                final String s = (String)tag;
                if (s.startsWith("layout") && s.endsWith("_0")) {
                    final char char1 = s.charAt(6);
                    final int index = s.indexOf(47, 7);
                    final boolean b = true;
                    final boolean b2 = false;
                    int n = 0;
                    Label_0103: {
                        if (char1 == '/') {
                            if (index == -1) {
                                n = (b ? 1 : 0);
                                break Label_0103;
                            }
                        }
                        else {
                            n = (b2 ? 1 : 0);
                            if (char1 != '-') {
                                break Label_0103;
                            }
                            n = (b2 ? 1 : 0);
                            if (index == -1) {
                                break Label_0103;
                            }
                            if (s.indexOf(47, index + 1) == -1) {
                                n = (b ? 1 : 0);
                                break Label_0103;
                            }
                        }
                        n = 0;
                    }
                    if (n != 0) {
                        return null;
                    }
                }
            }
            final ViewParent parent = view.getParent();
            if (parent instanceof View) {
                view = (View)parent;
            }
            else {
                view = null;
            }
        }
        return null;
    }
    
    @Nullable
    public static <T extends ViewDataBinding> T getBinding(@NonNull final View view) {
        return (T)ViewDataBinding.getBinding(view);
    }
    
    @Nullable
    public static DataBindingComponent getDefaultComponent() {
        return DataBindingUtil.sDefaultComponent;
    }
    
    public static <T extends ViewDataBinding> T inflate(@NonNull final LayoutInflater layoutInflater, final int n, @Nullable final ViewGroup viewGroup, final boolean b) {
        return inflate(layoutInflater, n, viewGroup, b, DataBindingUtil.sDefaultComponent);
    }
    
    public static <T extends ViewDataBinding> T inflate(@NonNull final LayoutInflater layoutInflater, final int n, @Nullable final ViewGroup viewGroup, final boolean b, @Nullable final DataBindingComponent dataBindingComponent) {
        int childCount = 0;
        final boolean b2 = viewGroup != null && b;
        if (b2) {
            childCount = viewGroup.getChildCount();
        }
        final View inflate = layoutInflater.inflate(n, viewGroup, b);
        if (b2) {
            return (T)bindToAddedViews(dataBindingComponent, viewGroup, childCount, n);
        }
        return (T)bind(dataBindingComponent, inflate, n);
    }
    
    public static <T extends ViewDataBinding> T setContentView(@NonNull final Activity activity, final int n) {
        return setContentView(activity, n, DataBindingUtil.sDefaultComponent);
    }
    
    public static <T extends ViewDataBinding> T setContentView(@NonNull final Activity activity, final int contentView, @Nullable final DataBindingComponent dataBindingComponent) {
        activity.setContentView(contentView);
        return bindToAddedViews(dataBindingComponent, (ViewGroup)activity.getWindow().getDecorView().findViewById(16908290), 0, contentView);
    }
    
    public static void setDefaultComponent(@Nullable final DataBindingComponent sDefaultComponent) {
        DataBindingUtil.sDefaultComponent = sDefaultComponent;
    }
}

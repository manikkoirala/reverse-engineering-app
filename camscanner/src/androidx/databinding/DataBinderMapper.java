// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding;

import android.view.View;
import androidx.annotation.NonNull;
import java.util.Collections;
import java.util.List;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
public abstract class DataBinderMapper
{
    @NonNull
    public List<DataBinderMapper> collectDependencies() {
        return Collections.emptyList();
    }
    
    public abstract String convertBrIdToString(final int p0);
    
    public abstract ViewDataBinding getDataBinder(final DataBindingComponent p0, final View p1, final int p2);
    
    public abstract ViewDataBinding getDataBinder(final DataBindingComponent p0, final View[] p1, final int p2);
    
    public abstract int getLayoutId(final String p0);
}

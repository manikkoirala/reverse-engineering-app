// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding;

import android.os.Parcel;
import android.os.Parcelable$Creator;
import java.io.Serializable;
import android.os.Parcelable;

public class ObservableLong extends BaseObservableField implements Parcelable, Serializable
{
    public static final Parcelable$Creator<ObservableLong> CREATOR;
    static final long serialVersionUID = 1L;
    private long mValue;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<ObservableLong>() {
            public ObservableLong createFromParcel(final Parcel parcel) {
                return new ObservableLong(parcel.readLong());
            }
            
            public ObservableLong[] newArray(final int n) {
                return new ObservableLong[n];
            }
        };
    }
    
    public ObservableLong() {
    }
    
    public ObservableLong(final long mValue) {
        this.mValue = mValue;
    }
    
    public ObservableLong(final Observable... array) {
        super(array);
    }
    
    public int describeContents() {
        return 0;
    }
    
    public long get() {
        return this.mValue;
    }
    
    public void set(final long mValue) {
        if (mValue != this.mValue) {
            this.mValue = mValue;
            this.notifyChange();
        }
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeLong(this.mValue);
    }
}

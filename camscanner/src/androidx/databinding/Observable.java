// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding;

public interface Observable
{
    void addOnPropertyChangedCallback(final OnPropertyChangedCallback p0);
    
    void removeOnPropertyChangedCallback(final OnPropertyChangedCallback p0);
    
    public abstract static class OnPropertyChangedCallback
    {
        public abstract void onPropertyChanged(final Observable p0, final int p1);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding;

import androidx.annotation.NonNull;

public class BaseObservable implements Observable
{
    private transient PropertyChangeRegistry mCallbacks;
    
    @Override
    public void addOnPropertyChangedCallback(@NonNull final OnPropertyChangedCallback onPropertyChangedCallback) {
        synchronized (this) {
            if (this.mCallbacks == null) {
                this.mCallbacks = new PropertyChangeRegistry();
            }
            monitorexit(this);
            ((CallbackRegistry<OnPropertyChangedCallback, T, A>)this.mCallbacks).add(onPropertyChangedCallback);
        }
    }
    
    public void notifyChange() {
        synchronized (this) {
            final PropertyChangeRegistry mCallbacks = this.mCallbacks;
            if (mCallbacks == null) {
                return;
            }
            monitorexit(this);
            ((CallbackRegistry<C, BaseObservable, Void>)mCallbacks).notifyCallbacks(this, 0, null);
        }
    }
    
    public void notifyPropertyChanged(final int n) {
        synchronized (this) {
            final PropertyChangeRegistry mCallbacks = this.mCallbacks;
            if (mCallbacks == null) {
                return;
            }
            monitorexit(this);
            ((CallbackRegistry<C, BaseObservable, Void>)mCallbacks).notifyCallbacks(this, n, null);
        }
    }
    
    @Override
    public void removeOnPropertyChangedCallback(@NonNull final OnPropertyChangedCallback onPropertyChangedCallback) {
        synchronized (this) {
            final PropertyChangeRegistry mCallbacks = this.mCallbacks;
            if (mCallbacks == null) {
                return;
            }
            monitorexit(this);
            ((CallbackRegistry<OnPropertyChangedCallback, T, A>)mCallbacks).remove(onPropertyChangedCallback);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding;

public abstract class OnRebindCallback<T extends ViewDataBinding>
{
    public void onBound(final T t) {
    }
    
    public void onCanceled(final T t) {
    }
    
    public boolean onPreBind(final T t) {
        return true;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.databinding;

import android.view.View;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MergedDataBinderMapper extends DataBinderMapper
{
    private static final String TAG = "MergedDataBinderMapper";
    private Set<Class<? extends DataBinderMapper>> mExistingMappers;
    private List<String> mFeatureBindingMappers;
    private List<DataBinderMapper> mMappers;
    
    public MergedDataBinderMapper() {
        this.mExistingMappers = new HashSet<Class<? extends DataBinderMapper>>();
        this.mMappers = new CopyOnWriteArrayList<DataBinderMapper>();
        this.mFeatureBindingMappers = new CopyOnWriteArrayList<String>();
    }
    
    private boolean loadFeatures() {
        final Iterator<String> iterator = this.mFeatureBindingMappers.iterator();
        boolean b = false;
        while (iterator.hasNext()) {
            final String str = iterator.next();
            try {
                final Class<?> forName = Class.forName(str);
                if (!DataBinderMapper.class.isAssignableFrom(forName)) {
                    continue;
                }
                this.addMapper((DataBinderMapper)forName.newInstance());
                this.mFeatureBindingMappers.remove(str);
                b = true;
            }
            catch (final InstantiationException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("unable to add feature mapper for ");
                sb.append(str);
            }
            catch (final IllegalAccessException ex2) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("unable to add feature mapper for ");
                sb2.append(str);
            }
            catch (final ClassNotFoundException ex3) {}
        }
        return b;
    }
    
    public void addMapper(final DataBinderMapper dataBinderMapper) {
        if (this.mExistingMappers.add(dataBinderMapper.getClass())) {
            this.mMappers.add(dataBinderMapper);
            final Iterator<DataBinderMapper> iterator = dataBinderMapper.collectDependencies().iterator();
            while (iterator.hasNext()) {
                this.addMapper(iterator.next());
            }
        }
    }
    
    protected void addMapper(final String str) {
        final List<String> mFeatureBindingMappers = this.mFeatureBindingMappers;
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(".DataBinderMapperImpl");
        mFeatureBindingMappers.add(sb.toString());
    }
    
    @Override
    public String convertBrIdToString(final int n) {
        final Iterator<DataBinderMapper> iterator = this.mMappers.iterator();
        while (iterator.hasNext()) {
            final String convertBrIdToString = iterator.next().convertBrIdToString(n);
            if (convertBrIdToString != null) {
                return convertBrIdToString;
            }
        }
        if (this.loadFeatures()) {
            return this.convertBrIdToString(n);
        }
        return null;
    }
    
    @Override
    public ViewDataBinding getDataBinder(final DataBindingComponent dataBindingComponent, final View view, final int n) {
        final Iterator<DataBinderMapper> iterator = this.mMappers.iterator();
        while (iterator.hasNext()) {
            final ViewDataBinding dataBinder = iterator.next().getDataBinder(dataBindingComponent, view, n);
            if (dataBinder != null) {
                return dataBinder;
            }
        }
        if (this.loadFeatures()) {
            return this.getDataBinder(dataBindingComponent, view, n);
        }
        return null;
    }
    
    @Override
    public ViewDataBinding getDataBinder(final DataBindingComponent dataBindingComponent, final View[] array, final int n) {
        final Iterator<DataBinderMapper> iterator = this.mMappers.iterator();
        while (iterator.hasNext()) {
            final ViewDataBinding dataBinder = iterator.next().getDataBinder(dataBindingComponent, array, n);
            if (dataBinder != null) {
                return dataBinder;
            }
        }
        if (this.loadFeatures()) {
            return this.getDataBinder(dataBindingComponent, array, n);
        }
        return null;
    }
    
    @Override
    public int getLayoutId(final String s) {
        final Iterator<DataBinderMapper> iterator = this.mMappers.iterator();
        while (iterator.hasNext()) {
            final int layoutId = iterator.next().getLayoutId(s);
            if (layoutId != 0) {
                return layoutId;
            }
        }
        if (this.loadFeatures()) {
            return this.getLayoutId(s);
        }
        return 0;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.concurrent.futures;

import androidx.annotation.RestrictTo;
import java.util.concurrent.Executor;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public enum DirectExecutor implements Executor
{
    private static final DirectExecutor[] $VALUES;
    
    INSTANCE;
    
    @Override
    public void execute(final Runnable runnable) {
        runnable.run();
    }
    
    @Override
    public String toString() {
        return "DirectExecutor";
    }
}

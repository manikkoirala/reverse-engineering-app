// 
// Decompiled by Procyon v0.6.0
// 

package androidx.concurrent.futures;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeoutException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.Executor;
import androidx.annotation.NonNull;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import androidx.annotation.Nullable;
import java.util.logging.Logger;
import androidx.annotation.RestrictTo;
import com.google.common.util.concurrent.ListenableFuture;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public abstract class AbstractResolvableFuture<V> implements ListenableFuture<V>
{
    static final AtomicHelper ATOMIC_HELPER;
    static final boolean GENERATE_CANCELLATION_CAUSES;
    private static final Object NULL;
    private static final long SPIN_THRESHOLD_NANOS = 1000L;
    private static final Logger log;
    @Nullable
    volatile Listener listeners;
    @Nullable
    volatile Object value;
    @Nullable
    volatile Waiter waiters;
    
    static {
        GENERATE_CANCELLATION_CAUSES = Boolean.parseBoolean(System.getProperty("guava.concurrent.generate_cancellation_cause", "false"));
        log = Logger.getLogger(AbstractResolvableFuture.class.getName());
        SynchronizedHelper atomic_HELPER;
        try {
            final SafeAtomicHelper safeAtomicHelper = new SafeAtomicHelper(AtomicReferenceFieldUpdater.newUpdater(Waiter.class, Thread.class, "thread"), AtomicReferenceFieldUpdater.newUpdater(Waiter.class, Waiter.class, "next"), (AtomicReferenceFieldUpdater<AbstractResolvableFuture, Waiter>)AtomicReferenceFieldUpdater.newUpdater(AbstractResolvableFuture.class, Waiter.class, "waiters"), (AtomicReferenceFieldUpdater<AbstractResolvableFuture, Listener>)AtomicReferenceFieldUpdater.newUpdater(AbstractResolvableFuture.class, Listener.class, "listeners"), (AtomicReferenceFieldUpdater<AbstractResolvableFuture, Object>)AtomicReferenceFieldUpdater.newUpdater(AbstractResolvableFuture.class, Object.class, "value"));
        }
        finally {
            atomic_HELPER = new SynchronizedHelper();
        }
        ATOMIC_HELPER = (AtomicHelper)atomic_HELPER;
        final Throwable thrown;
        if (thrown != null) {
            AbstractResolvableFuture.log.log(Level.SEVERE, "SafeAtomicHelper is broken!", thrown);
        }
        NULL = new Object();
    }
    
    protected AbstractResolvableFuture() {
    }
    
    private void addDoneString(final StringBuilder sb) {
        try {
            final Object uninterruptibly = getUninterruptibly((Future<Object>)this);
            sb.append("SUCCESS, result=[");
            sb.append(this.userObjectToString(uninterruptibly));
            sb.append("]");
        }
        catch (final RuntimeException ex) {
            sb.append("UNKNOWN, cause=[");
            sb.append(ex.getClass());
            sb.append(" thrown from get()]");
        }
        catch (final CancellationException ex2) {
            sb.append("CANCELLED");
        }
        catch (final ExecutionException ex3) {
            sb.append("FAILURE, cause=[");
            sb.append(ex3.getCause());
            sb.append("]");
        }
    }
    
    private static CancellationException cancellationExceptionWithCause(@Nullable final String message, @Nullable final Throwable cause) {
        final CancellationException ex = new CancellationException(message);
        ex.initCause(cause);
        return ex;
    }
    
    @NonNull
    static <T> T checkNotNull(@Nullable final T t) {
        t.getClass();
        return t;
    }
    
    private Listener clearListeners(Listener listener) {
        Listener listeners;
        do {
            listeners = this.listeners;
        } while (!AbstractResolvableFuture.ATOMIC_HELPER.casListeners(this, listeners, Listener.TOMBSTONE));
        Listener next = listener;
        Listener next2;
        for (listener = listeners; listener != null; listener = next2) {
            next2 = listener.next;
            listener.next = next;
            next = listener;
        }
        return next;
    }
    
    static void complete(final AbstractResolvableFuture<?> abstractResolvableFuture) {
        final Listener listener = null;
        AbstractResolvableFuture<?> owner = abstractResolvableFuture;
        Listener next = listener;
    Label_0006:
        while (true) {
            owner.releaseWaiters();
            owner.afterDone();
            for (Listener clearListeners = owner.clearListeners(next); clearListeners != null; clearListeners = next) {
                next = clearListeners.next;
                final Runnable task = clearListeners.task;
                if (task instanceof SetFuture) {
                    final SetFuture setFuture = (SetFuture)task;
                    owner = setFuture.owner;
                    if (owner.value == setFuture && AbstractResolvableFuture.ATOMIC_HELPER.casValue(owner, setFuture, getFutureValue(setFuture.future))) {
                        continue Label_0006;
                    }
                }
                else {
                    executeListener(task, clearListeners.executor);
                }
            }
            break;
        }
    }
    
    private static void executeListener(final Runnable obj, final Executor obj2) {
        try {
            obj2.execute(obj);
        }
        catch (final RuntimeException thrown) {
            final Logger log = AbstractResolvableFuture.log;
            final Level severe = Level.SEVERE;
            final StringBuilder sb = new StringBuilder();
            sb.append("RuntimeException while executing runnable ");
            sb.append(obj);
            sb.append(" with executor ");
            sb.append(obj2);
            log.log(severe, sb.toString(), thrown);
        }
    }
    
    private V getDoneValue(final Object o) throws ExecutionException {
        if (o instanceof Cancellation) {
            throw cancellationExceptionWithCause("Task was cancelled.", ((Cancellation)o).cause);
        }
        if (!(o instanceof Failure)) {
            Object o2;
            if ((o2 = o) == AbstractResolvableFuture.NULL) {
                o2 = null;
            }
            return (V)o2;
        }
        throw new ExecutionException(((Failure)o).exception);
    }
    
    static Object getFutureValue(final ListenableFuture<?> obj) {
        if (obj instanceof AbstractResolvableFuture) {
            Object o2;
            final Object o = o2 = ((AbstractResolvableFuture)obj).value;
            if (o instanceof Cancellation) {
                final Cancellation cancellation = (Cancellation)o;
                o2 = o;
                if (cancellation.wasInterrupted) {
                    if (cancellation.cause != null) {
                        o2 = new Cancellation(false, cancellation.cause);
                    }
                    else {
                        o2 = Cancellation.CAUSELESS_CANCELLED;
                    }
                }
            }
            return o2;
        }
        final boolean cancelled = ((Future)obj).isCancelled();
        if ((AbstractResolvableFuture.GENERATE_CANCELLATION_CAUSES ^ true) & cancelled) {
            return Cancellation.CAUSELESS_CANCELLED;
        }
        try {
            Object o3;
            if ((o3 = getUninterruptibly((Future<Object>)obj)) == null) {
                o3 = AbstractResolvableFuture.NULL;
            }
            return o3;
        }
        catch (final CancellationException cause) {
            if (!cancelled) {
                final StringBuilder sb = new StringBuilder();
                sb.append("get() threw CancellationException, despite reporting isCancelled() == false: ");
                sb.append(obj);
                return new Failure(new IllegalArgumentException(sb.toString(), cause));
            }
            return new Cancellation(false, cause);
        }
        catch (final ExecutionException ex) {
            return new Failure(ex.getCause());
        }
        finally {
            final Throwable t;
            return new Failure(t);
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    static <V> V getUninterruptibly(final Future<V> future) throws ExecutionException {
        boolean b = false;
        try {
            return future.get();
        }
        catch (final InterruptedException ex) {
            b = true;
            return future.get();
        }
        finally {
            if (b) {
                Thread.currentThread().interrupt();
            }
        }
    }
    
    private void releaseWaiters() {
        Waiter waiter;
        do {
            waiter = this.waiters;
        } while (!AbstractResolvableFuture.ATOMIC_HELPER.casWaiters(this, waiter, Waiter.TOMBSTONE));
        while (waiter != null) {
            waiter.unpark();
            waiter = waiter.next;
        }
    }
    
    private void removeWaiter(Waiter waiters) {
        waiters.thread = null;
    Label_0005:
        while (true) {
            waiters = this.waiters;
            if (waiters == Waiter.TOMBSTONE) {
                return;
            }
            Waiter waiter = null;
            while (waiters != null) {
                final Waiter next = waiters.next;
                Waiter waiter2;
                if (waiters.thread != null) {
                    waiter2 = waiters;
                }
                else if (waiter != null) {
                    waiter.next = next;
                    waiter2 = waiter;
                    if (waiter.thread == null) {
                        continue Label_0005;
                    }
                }
                else {
                    waiter2 = waiter;
                    if (!AbstractResolvableFuture.ATOMIC_HELPER.casWaiters(this, waiters, next)) {
                        continue Label_0005;
                    }
                }
                waiters = next;
                waiter = waiter2;
            }
        }
    }
    
    private String userObjectToString(final Object obj) {
        if (obj == this) {
            return "this future";
        }
        return String.valueOf(obj);
    }
    
    public final void addListener(final Runnable runnable, final Executor executor) {
        checkNotNull(runnable);
        checkNotNull(executor);
        Listener next = this.listeners;
        if (next != Listener.TOMBSTONE) {
            final Listener listener = new Listener(runnable, executor);
            do {
                listener.next = next;
                if (AbstractResolvableFuture.ATOMIC_HELPER.casListeners(this, next, listener)) {
                    return;
                }
            } while ((next = this.listeners) != Listener.TOMBSTONE);
        }
        executeListener(runnable, executor);
    }
    
    protected void afterDone() {
    }
    
    public final boolean cancel(final boolean b) {
        Object o = this.value;
        final boolean b2 = true;
        boolean b3;
        if (o == null | o instanceof SetFuture) {
            Cancellation cancellation;
            if (AbstractResolvableFuture.GENERATE_CANCELLATION_CAUSES) {
                cancellation = new Cancellation(b, new CancellationException("Future.cancel() was called."));
            }
            else if (b) {
                cancellation = Cancellation.CAUSELESS_INTERRUPTED;
            }
            else {
                cancellation = Cancellation.CAUSELESS_CANCELLED;
            }
            b3 = false;
            AbstractResolvableFuture<? extends V> abstractResolvableFuture = (AbstractResolvableFuture<? extends V>)this;
            while (true) {
                if (AbstractResolvableFuture.ATOMIC_HELPER.casValue(abstractResolvableFuture, o, cancellation)) {
                    if (b) {
                        abstractResolvableFuture.interruptTask();
                    }
                    complete(abstractResolvableFuture);
                    b3 = b2;
                    if (!(o instanceof SetFuture)) {
                        break;
                    }
                    final ListenableFuture<? extends V> future = ((SetFuture)o).future;
                    if (!(future instanceof AbstractResolvableFuture)) {
                        ((Future)future).cancel(b);
                        b3 = b2;
                        break;
                    }
                    abstractResolvableFuture = (AbstractResolvableFuture<? extends V>)future;
                    o = abstractResolvableFuture.value;
                    final boolean b4 = o == null;
                    b3 = b2;
                    if (!(b4 | o instanceof SetFuture)) {
                        break;
                    }
                    b3 = true;
                }
                else {
                    if (!((o = abstractResolvableFuture.value) instanceof SetFuture)) {
                        break;
                    }
                    continue;
                }
            }
        }
        else {
            b3 = false;
        }
        return b3;
    }
    
    public final V get() throws InterruptedException, ExecutionException {
        if (Thread.interrupted()) {
            throw new InterruptedException();
        }
        final Object value = this.value;
        if (value != null & (value instanceof SetFuture ^ true)) {
            return this.getDoneValue(value);
        }
        Waiter next = this.waiters;
        if (next != Waiter.TOMBSTONE) {
            final Waiter waiter = new Waiter();
            do {
                waiter.setNext(next);
                if (AbstractResolvableFuture.ATOMIC_HELPER.casWaiters(this, next, waiter)) {
                    Object value2;
                    do {
                        LockSupport.park(this);
                        if (Thread.interrupted()) {
                            this.removeWaiter(waiter);
                            throw new InterruptedException();
                        }
                        value2 = this.value;
                    } while (!(value2 != null & (value2 instanceof SetFuture ^ true)));
                    return this.getDoneValue(value2);
                }
            } while ((next = this.waiters) != Waiter.TOMBSTONE);
        }
        return this.getDoneValue(this.value);
    }
    
    public final V get(long convert, final TimeUnit timeUnit) throws InterruptedException, TimeoutException, ExecutionException {
        long nanos = timeUnit.toNanos(convert);
        if (Thread.interrupted()) {
            throw new InterruptedException();
        }
        final Object value = this.value;
        if (value != null & (value instanceof SetFuture ^ true)) {
            return this.getDoneValue(value);
        }
        long n;
        if (nanos > 0L) {
            n = System.nanoTime() + nanos;
        }
        else {
            n = 0L;
        }
        long n2 = nanos;
        Label_0254: {
            if (nanos >= 1000L) {
                Waiter next = this.waiters;
                if (next != Waiter.TOMBSTONE) {
                    final Waiter waiter = new Waiter();
                    do {
                        waiter.setNext(next);
                        if (AbstractResolvableFuture.ATOMIC_HELPER.casWaiters(this, next, waiter)) {
                            do {
                                LockSupport.parkNanos(this, nanos);
                                if (Thread.interrupted()) {
                                    this.removeWaiter(waiter);
                                    throw new InterruptedException();
                                }
                                final Object value2 = this.value;
                                if (value2 != null & (value2 instanceof SetFuture ^ true)) {
                                    return this.getDoneValue(value2);
                                }
                                n2 = (nanos = n - System.nanoTime());
                            } while (n2 >= 1000L);
                            this.removeWaiter(waiter);
                            break Label_0254;
                        }
                    } while ((next = this.waiters) != Waiter.TOMBSTONE);
                }
                return this.getDoneValue(this.value);
            }
        }
        while (n2 > 0L) {
            final Object value3 = this.value;
            if (value3 != null & (value3 instanceof SetFuture ^ true)) {
                return this.getDoneValue(value3);
            }
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }
            n2 = n - System.nanoTime();
        }
        final String string = this.toString();
        final String string2 = timeUnit.toString();
        final Locale root = Locale.ROOT;
        final String lowerCase = string2.toLowerCase(root);
        final StringBuilder sb = new StringBuilder();
        sb.append("Waited ");
        sb.append(convert);
        sb.append(" ");
        sb.append(timeUnit.toString().toLowerCase(root));
        String s;
        final String str = s = sb.toString();
        if (n2 + 1000L < 0L) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append(" (plus ");
            final String string3 = sb2.toString();
            final long sourceDuration = -n2;
            convert = timeUnit.convert(sourceDuration, TimeUnit.NANOSECONDS);
            final long lng = sourceDuration - timeUnit.toNanos(convert);
            final long n3 = lcmp(convert, 0L);
            final boolean b = n3 == 0 || lng > 1000L;
            String string4 = string3;
            if (n3 > 0) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(string3);
                sb3.append(convert);
                sb3.append(" ");
                sb3.append(lowerCase);
                String s2 = sb3.toString();
                if (b) {
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append(s2);
                    sb4.append(",");
                    s2 = sb4.toString();
                }
                final StringBuilder sb5 = new StringBuilder();
                sb5.append(s2);
                sb5.append(" ");
                string4 = sb5.toString();
            }
            String string5 = string4;
            if (b) {
                final StringBuilder sb6 = new StringBuilder();
                sb6.append(string4);
                sb6.append(lng);
                sb6.append(" nanoseconds ");
                string5 = sb6.toString();
            }
            final StringBuilder sb7 = new StringBuilder();
            sb7.append(string5);
            sb7.append("delay)");
            s = sb7.toString();
        }
        if (this.isDone()) {
            final StringBuilder sb8 = new StringBuilder();
            sb8.append(s);
            sb8.append(" but future completed as timeout expired");
            throw new TimeoutException(sb8.toString());
        }
        final StringBuilder sb9 = new StringBuilder();
        sb9.append(s);
        sb9.append(" for ");
        sb9.append(string);
        throw new TimeoutException(sb9.toString());
    }
    
    protected void interruptTask() {
    }
    
    public final boolean isCancelled() {
        return this.value instanceof Cancellation;
    }
    
    public final boolean isDone() {
        final Object value = this.value;
        return (value instanceof SetFuture ^ true) & value != null;
    }
    
    final void maybePropagateCancellationTo(@Nullable final Future<?> future) {
        if (future != null & this.isCancelled()) {
            future.cancel(this.wasInterrupted());
        }
    }
    
    @Nullable
    protected String pendingToString() {
        final Object value = this.value;
        if (value instanceof SetFuture) {
            final StringBuilder sb = new StringBuilder();
            sb.append("setFuture=[");
            sb.append(this.userObjectToString(((SetFuture)value).future));
            sb.append("]");
            return sb.toString();
        }
        if (this instanceof ScheduledFuture) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("remaining delay=[");
            sb2.append(((ScheduledFuture)this).getDelay(TimeUnit.MILLISECONDS));
            sb2.append(" ms]");
            return sb2.toString();
        }
        return null;
    }
    
    protected boolean set(@Nullable final V v) {
        Object null = v;
        if (v == null) {
            null = AbstractResolvableFuture.NULL;
        }
        if (AbstractResolvableFuture.ATOMIC_HELPER.casValue(this, null, null)) {
            complete(this);
            return true;
        }
        return false;
    }
    
    protected boolean setException(final Throwable t) {
        if (AbstractResolvableFuture.ATOMIC_HELPER.casValue(this, null, new Failure(checkNotNull(t)))) {
            complete(this);
            return true;
        }
        return false;
    }
    
    protected boolean setFuture(final ListenableFuture<? extends V> listenableFuture) {
        checkNotNull(listenableFuture);
        Object o;
        if ((o = this.value) == null) {
            if (((Future)listenableFuture).isDone()) {
                if (AbstractResolvableFuture.ATOMIC_HELPER.casValue(this, null, getFutureValue(listenableFuture))) {
                    complete(this);
                    return true;
                }
                return false;
            }
            else {
                final SetFuture setFuture = new SetFuture((AbstractResolvableFuture<V>)this, (ListenableFuture<? extends V>)listenableFuture);
                if (AbstractResolvableFuture.ATOMIC_HELPER.casValue(this, null, setFuture)) {
                    try {
                        listenableFuture.addListener((Runnable)setFuture, (Executor)DirectExecutor.INSTANCE);
                    }
                    finally {
                        Failure fallback_INSTANCE = null;
                        try {
                            final Throwable t;
                            final Failure failure = new Failure(t);
                        }
                        finally {
                            fallback_INSTANCE = Failure.FALLBACK_INSTANCE;
                        }
                        AbstractResolvableFuture.ATOMIC_HELPER.casValue(this, setFuture, fallback_INSTANCE);
                    }
                    return true;
                }
                o = this.value;
            }
        }
        if (o instanceof Cancellation) {
            ((Future)listenableFuture).cancel(((Cancellation)o).wasInterrupted);
        }
        return false;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("[status=");
        if (this.isCancelled()) {
            sb.append("CANCELLED");
        }
        else if (this.isDone()) {
            this.addDoneString(sb);
        }
        else {
            String str;
            try {
                str = this.pendingToString();
            }
            catch (final RuntimeException ex) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Exception thrown from implementation: ");
                sb2.append(ex.getClass());
                str = sb2.toString();
            }
            if (str != null && !str.isEmpty()) {
                sb.append("PENDING, info=[");
                sb.append(str);
                sb.append("]");
            }
            else if (this.isDone()) {
                this.addDoneString(sb);
            }
            else {
                sb.append("PENDING");
            }
        }
        sb.append("]");
        return sb.toString();
    }
    
    protected final boolean wasInterrupted() {
        final Object value = this.value;
        return value instanceof Cancellation && ((Cancellation)value).wasInterrupted;
    }
    
    private abstract static class AtomicHelper
    {
        abstract boolean casListeners(final AbstractResolvableFuture<?> p0, final Listener p1, final Listener p2);
        
        abstract boolean casValue(final AbstractResolvableFuture<?> p0, final Object p1, final Object p2);
        
        abstract boolean casWaiters(final AbstractResolvableFuture<?> p0, final Waiter p1, final Waiter p2);
        
        abstract void putNext(final Waiter p0, final Waiter p1);
        
        abstract void putThread(final Waiter p0, final Thread p1);
    }
    
    private static final class Cancellation
    {
        static final Cancellation CAUSELESS_CANCELLED;
        static final Cancellation CAUSELESS_INTERRUPTED;
        @Nullable
        final Throwable cause;
        final boolean wasInterrupted;
        
        static {
            if (AbstractResolvableFuture.GENERATE_CANCELLATION_CAUSES) {
                CAUSELESS_CANCELLED = null;
                CAUSELESS_INTERRUPTED = null;
            }
            else {
                CAUSELESS_CANCELLED = new Cancellation(false, null);
                CAUSELESS_INTERRUPTED = new Cancellation(true, null);
            }
        }
        
        Cancellation(final boolean wasInterrupted, @Nullable final Throwable cause) {
            this.wasInterrupted = wasInterrupted;
            this.cause = cause;
        }
    }
    
    private static final class Failure
    {
        static final Failure FALLBACK_INSTANCE;
        final Throwable exception;
        
        static {
            FALLBACK_INSTANCE = new Failure(new Throwable() {
                @Override
                public Throwable fillInStackTrace() {
                    monitorenter(this);
                    monitorexit(this);
                    return this;
                }
            });
        }
        
        Failure(final Throwable t) {
            this.exception = AbstractResolvableFuture.checkNotNull(t);
        }
    }
    
    private static final class Listener
    {
        static final Listener TOMBSTONE;
        final Executor executor;
        @Nullable
        Listener next;
        final Runnable task;
        
        static {
            TOMBSTONE = new Listener(null, null);
        }
        
        Listener(final Runnable task, final Executor executor) {
            this.task = task;
            this.executor = executor;
        }
    }
    
    private static final class SafeAtomicHelper extends AtomicHelper
    {
        final AtomicReferenceFieldUpdater<AbstractResolvableFuture, Listener> listenersUpdater;
        final AtomicReferenceFieldUpdater<AbstractResolvableFuture, Object> valueUpdater;
        final AtomicReferenceFieldUpdater<Waiter, Waiter> waiterNextUpdater;
        final AtomicReferenceFieldUpdater<Waiter, Thread> waiterThreadUpdater;
        final AtomicReferenceFieldUpdater<AbstractResolvableFuture, Waiter> waitersUpdater;
        
        SafeAtomicHelper(final AtomicReferenceFieldUpdater<Waiter, Thread> waiterThreadUpdater, final AtomicReferenceFieldUpdater<Waiter, Waiter> waiterNextUpdater, final AtomicReferenceFieldUpdater<AbstractResolvableFuture, Waiter> waitersUpdater, final AtomicReferenceFieldUpdater<AbstractResolvableFuture, Listener> listenersUpdater, final AtomicReferenceFieldUpdater<AbstractResolvableFuture, Object> valueUpdater) {
            this.waiterThreadUpdater = waiterThreadUpdater;
            this.waiterNextUpdater = waiterNextUpdater;
            this.waitersUpdater = waitersUpdater;
            this.listenersUpdater = listenersUpdater;
            this.valueUpdater = valueUpdater;
        }
        
        @Override
        boolean casListeners(final AbstractResolvableFuture<?> abstractResolvableFuture, final Listener listener, final Listener listener2) {
            return \u3007080.\u3007080(this.listenersUpdater, abstractResolvableFuture, listener, listener2);
        }
        
        @Override
        boolean casValue(final AbstractResolvableFuture<?> abstractResolvableFuture, final Object o, final Object o2) {
            return \u3007080.\u3007080(this.valueUpdater, abstractResolvableFuture, o, o2);
        }
        
        @Override
        boolean casWaiters(final AbstractResolvableFuture<?> abstractResolvableFuture, final Waiter waiter, final Waiter waiter2) {
            return \u3007080.\u3007080(this.waitersUpdater, abstractResolvableFuture, waiter, waiter2);
        }
        
        @Override
        void putNext(final Waiter waiter, final Waiter waiter2) {
            this.waiterNextUpdater.lazySet(waiter, waiter2);
        }
        
        @Override
        void putThread(final Waiter waiter, final Thread thread) {
            this.waiterThreadUpdater.lazySet(waiter, thread);
        }
    }
    
    private static final class SetFuture<V> implements Runnable
    {
        final ListenableFuture<? extends V> future;
        final AbstractResolvableFuture<V> owner;
        
        SetFuture(final AbstractResolvableFuture<V> owner, final ListenableFuture<? extends V> future) {
            this.owner = owner;
            this.future = future;
        }
        
        @Override
        public void run() {
            if (this.owner.value != this) {
                return;
            }
            if (AbstractResolvableFuture.ATOMIC_HELPER.casValue(this.owner, this, AbstractResolvableFuture.getFutureValue(this.future))) {
                AbstractResolvableFuture.complete(this.owner);
            }
        }
    }
    
    private static final class SynchronizedHelper extends AtomicHelper
    {
        SynchronizedHelper() {
        }
        
        @Override
        boolean casListeners(final AbstractResolvableFuture<?> abstractResolvableFuture, final Listener listener, final Listener listeners) {
            synchronized (abstractResolvableFuture) {
                if (abstractResolvableFuture.listeners == listener) {
                    abstractResolvableFuture.listeners = listeners;
                    return true;
                }
                return false;
            }
        }
        
        @Override
        boolean casValue(final AbstractResolvableFuture<?> abstractResolvableFuture, final Object o, final Object value) {
            synchronized (abstractResolvableFuture) {
                if (abstractResolvableFuture.value == o) {
                    abstractResolvableFuture.value = value;
                    return true;
                }
                return false;
            }
        }
        
        @Override
        boolean casWaiters(final AbstractResolvableFuture<?> abstractResolvableFuture, final Waiter waiter, final Waiter waiters) {
            synchronized (abstractResolvableFuture) {
                if (abstractResolvableFuture.waiters == waiter) {
                    abstractResolvableFuture.waiters = waiters;
                    return true;
                }
                return false;
            }
        }
        
        @Override
        void putNext(final Waiter waiter, final Waiter next) {
            waiter.next = next;
        }
        
        @Override
        void putThread(final Waiter waiter, final Thread thread) {
            waiter.thread = thread;
        }
    }
    
    private static final class Waiter
    {
        static final Waiter TOMBSTONE;
        @Nullable
        volatile Waiter next;
        @Nullable
        volatile Thread thread;
        
        static {
            TOMBSTONE = new Waiter(false);
        }
        
        Waiter() {
            AbstractResolvableFuture.ATOMIC_HELPER.putThread(this, Thread.currentThread());
        }
        
        Waiter(final boolean b) {
        }
        
        void setNext(final Waiter waiter) {
            AbstractResolvableFuture.ATOMIC_HELPER.putNext(this, waiter);
        }
        
        void unpark() {
            final Thread thread = this.thread;
            if (thread != null) {
                this.thread = null;
                LockSupport.unpark(thread);
            }
        }
    }
}

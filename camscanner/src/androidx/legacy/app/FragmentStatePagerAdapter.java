// 
// Decompiled by Procyon v0.6.0
// 

package androidx.legacy.app;

import android.os.BaseBundle;
import java.util.Iterator;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;
import android.app.Fragment$SavedState;
import java.util.ArrayList;
import android.app.FragmentManager;
import android.app.Fragment;
import android.app.FragmentTransaction;
import androidx.viewpager.widget.PagerAdapter;

@Deprecated
public abstract class FragmentStatePagerAdapter extends PagerAdapter
{
    private static final boolean DEBUG = false;
    private static final String TAG = "FragStatePagerAdapter";
    private FragmentTransaction mCurTransaction;
    private Fragment mCurrentPrimaryItem;
    private final FragmentManager mFragmentManager;
    private ArrayList<Fragment> mFragments;
    private ArrayList<Fragment$SavedState> mSavedState;
    
    @Deprecated
    public FragmentStatePagerAdapter(final FragmentManager mFragmentManager) {
        this.mCurTransaction = null;
        this.mSavedState = new ArrayList<Fragment$SavedState>();
        this.mFragments = new ArrayList<Fragment>();
        this.mCurrentPrimaryItem = null;
        this.mFragmentManager = mFragmentManager;
    }
    
    @Deprecated
    @Override
    public void destroyItem(final ViewGroup viewGroup, final int n, final Object o) {
        final Fragment fragment = (Fragment)o;
        if (this.mCurTransaction == null) {
            this.mCurTransaction = this.mFragmentManager.beginTransaction();
        }
        while (this.mSavedState.size() <= n) {
            this.mSavedState.add(null);
        }
        final ArrayList<Fragment$SavedState> mSavedState = this.mSavedState;
        Fragment$SavedState saveFragmentInstanceState;
        if (fragment.isAdded()) {
            saveFragmentInstanceState = this.mFragmentManager.saveFragmentInstanceState(fragment);
        }
        else {
            saveFragmentInstanceState = null;
        }
        mSavedState.set(n, saveFragmentInstanceState);
        this.mFragments.set(n, null);
        this.mCurTransaction.remove(fragment);
    }
    
    @Deprecated
    @Override
    public void finishUpdate(final ViewGroup viewGroup) {
        final FragmentTransaction mCurTransaction = this.mCurTransaction;
        if (mCurTransaction != null) {
            mCurTransaction.commitAllowingStateLoss();
            this.mCurTransaction = null;
            this.mFragmentManager.executePendingTransactions();
        }
    }
    
    @Deprecated
    public abstract Fragment getItem(final int p0);
    
    @Deprecated
    @Override
    public Object instantiateItem(final ViewGroup viewGroup, final int index) {
        if (this.mFragments.size() > index) {
            final Fragment fragment = this.mFragments.get(index);
            if (fragment != null) {
                return fragment;
            }
        }
        if (this.mCurTransaction == null) {
            this.mCurTransaction = this.mFragmentManager.beginTransaction();
        }
        final Fragment item = this.getItem(index);
        if (this.mSavedState.size() > index) {
            final Fragment$SavedState initialSavedState = this.mSavedState.get(index);
            if (initialSavedState != null) {
                item.setInitialSavedState(initialSavedState);
            }
        }
        while (this.mFragments.size() <= index) {
            this.mFragments.add(null);
        }
        item.setMenuVisibility(false);
        FragmentCompat.setUserVisibleHint(item, false);
        this.mFragments.set(index, item);
        this.mCurTransaction.add(((View)viewGroup).getId(), item);
        return item;
    }
    
    @Deprecated
    @Override
    public boolean isViewFromObject(final View view, final Object o) {
        return ((Fragment)o).getView() == view;
    }
    
    @Deprecated
    @Override
    public void restoreState(final Parcelable parcelable, final ClassLoader classLoader) {
        if (parcelable != null) {
            final Bundle bundle = (Bundle)parcelable;
            bundle.setClassLoader(classLoader);
            final Parcelable[] parcelableArray = bundle.getParcelableArray("states");
            this.mSavedState.clear();
            this.mFragments.clear();
            if (parcelableArray != null) {
                for (int i = 0; i < parcelableArray.length; ++i) {
                    this.mSavedState.add((Fragment$SavedState)parcelableArray[i]);
                }
            }
            for (final String str : ((BaseBundle)bundle).keySet()) {
                if (str.startsWith("f")) {
                    final int int1 = Integer.parseInt(str.substring(1));
                    final Fragment fragment = this.mFragmentManager.getFragment(bundle, str);
                    if (fragment != null) {
                        while (this.mFragments.size() <= int1) {
                            this.mFragments.add(null);
                        }
                        FragmentCompat.setMenuVisibility(fragment, false);
                        this.mFragments.set(int1, fragment);
                    }
                    else {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Bad fragment at key ");
                        sb.append(str);
                    }
                }
            }
        }
    }
    
    @Deprecated
    @Override
    public Parcelable saveState() {
        Bundle bundle;
        if (this.mSavedState.size() > 0) {
            bundle = new Bundle();
            final Fragment$SavedState[] a = new Fragment$SavedState[this.mSavedState.size()];
            this.mSavedState.toArray(a);
            bundle.putParcelableArray("states", (Parcelable[])a);
        }
        else {
            bundle = null;
        }
        Bundle bundle2;
        for (int i = 0; i < this.mFragments.size(); ++i, bundle = bundle2) {
            final Fragment fragment = this.mFragments.get(i);
            bundle2 = bundle;
            if (fragment != null) {
                bundle2 = bundle;
                if (fragment.isAdded()) {
                    if ((bundle2 = bundle) == null) {
                        bundle2 = new Bundle();
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("f");
                    sb.append(i);
                    this.mFragmentManager.putFragment(bundle2, sb.toString(), fragment);
                }
            }
        }
        return (Parcelable)bundle;
    }
    
    @Deprecated
    @Override
    public void setPrimaryItem(final ViewGroup viewGroup, final int n, final Object o) {
        final Fragment mCurrentPrimaryItem = (Fragment)o;
        final Fragment mCurrentPrimaryItem2 = this.mCurrentPrimaryItem;
        if (mCurrentPrimaryItem != mCurrentPrimaryItem2) {
            if (mCurrentPrimaryItem2 != null) {
                mCurrentPrimaryItem2.setMenuVisibility(false);
                FragmentCompat.setUserVisibleHint(this.mCurrentPrimaryItem, false);
            }
            if (mCurrentPrimaryItem != null) {
                mCurrentPrimaryItem.setMenuVisibility(true);
                FragmentCompat.setUserVisibleHint(mCurrentPrimaryItem, true);
            }
            this.mCurrentPrimaryItem = mCurrentPrimaryItem;
        }
    }
    
    @Deprecated
    @Override
    public void startUpdate(final ViewGroup viewGroup) {
        if (((View)viewGroup).getId() != -1) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("ViewPager with adapter ");
        sb.append(this);
        sb.append(" requires a view id");
        throw new IllegalStateException(sb.toString());
    }
}

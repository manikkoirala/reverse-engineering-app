// 
// Decompiled by Procyon v0.6.0
// 

package androidx.legacy.app;

import android.content.Context;
import android.content.pm.PackageManager;
import android.app.Activity;
import java.util.Arrays;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.RequiresApi;
import androidx.annotation.NonNull;
import android.app.Fragment;
import androidx.annotation.RestrictTo;
import android.os.Build$VERSION;

@Deprecated
public class FragmentCompat
{
    static final FragmentCompatImpl IMPL;
    private static PermissionCompatDelegate sDelegate;
    
    static {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 24) {
            IMPL = (FragmentCompatImpl)new FragmentCompatApi24Impl();
        }
        else if (sdk_INT >= 23) {
            IMPL = (FragmentCompatImpl)new FragmentCompatApi23Impl();
        }
        else {
            IMPL = (FragmentCompatImpl)new FragmentCompatApi15Impl();
        }
    }
    
    @Deprecated
    public FragmentCompat() {
    }
    
    @Deprecated
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static PermissionCompatDelegate getPermissionCompatDelegate() {
        return FragmentCompat.sDelegate;
    }
    
    @Deprecated
    public static void requestPermissions(@NonNull final Fragment fragment, @NonNull final String[] array, final int n) {
        final PermissionCompatDelegate sDelegate = FragmentCompat.sDelegate;
        if (sDelegate != null && sDelegate.requestPermissions(fragment, array, n)) {
            return;
        }
        FragmentCompat.IMPL.requestPermissions(fragment, array, n);
    }
    
    @Deprecated
    public static void setMenuVisibility(final Fragment fragment, final boolean menuVisibility) {
        fragment.setMenuVisibility(menuVisibility);
    }
    
    @Deprecated
    public static void setPermissionCompatDelegate(final PermissionCompatDelegate sDelegate) {
        FragmentCompat.sDelegate = sDelegate;
    }
    
    @Deprecated
    public static void setUserVisibleHint(final Fragment fragment, final boolean b) {
        FragmentCompat.IMPL.setUserVisibleHint(fragment, b);
    }
    
    @Deprecated
    public static boolean shouldShowRequestPermissionRationale(@NonNull final Fragment fragment, @NonNull final String s) {
        return FragmentCompat.IMPL.shouldShowRequestPermissionRationale(fragment, s);
    }
    
    @RequiresApi(15)
    static class FragmentCompatApi15Impl extends FragmentCompatBaseImpl
    {
        @Override
        public void setUserVisibleHint(final Fragment fragment, final boolean userVisibleHint) {
            fragment.setUserVisibleHint(userVisibleHint);
        }
    }
    
    static class FragmentCompatBaseImpl implements FragmentCompatImpl
    {
        @Override
        public void requestPermissions(final Fragment fragment, final String[] array, final int n) {
            new Handler(Looper.getMainLooper()).post((Runnable)new Runnable(this, array, fragment, n) {
                final FragmentCompatBaseImpl this$0;
                final Fragment val$fragment;
                final String[] val$permissions;
                final int val$requestCode;
                
                @Override
                public void run() {
                    final int[] a = new int[this.val$permissions.length];
                    final Activity activity = this.val$fragment.getActivity();
                    if (activity != null) {
                        final PackageManager packageManager = ((Context)activity).getPackageManager();
                        final String packageName = ((Context)activity).getPackageName();
                        for (int length = this.val$permissions.length, i = 0; i < length; ++i) {
                            a[i] = packageManager.checkPermission(this.val$permissions[i], packageName);
                        }
                    }
                    else {
                        Arrays.fill(a, -1);
                    }
                    ((OnRequestPermissionsResultCallback)this.val$fragment).onRequestPermissionsResult(this.val$requestCode, this.val$permissions, a);
                }
            });
        }
        
        @Override
        public void setUserVisibleHint(final Fragment fragment, final boolean b) {
        }
        
        @Override
        public boolean shouldShowRequestPermissionRationale(final Fragment fragment, final String s) {
            return false;
        }
    }
    
    interface FragmentCompatImpl
    {
        void requestPermissions(final Fragment p0, final String[] p1, final int p2);
        
        void setUserVisibleHint(final Fragment p0, final boolean p1);
        
        boolean shouldShowRequestPermissionRationale(final Fragment p0, final String p1);
    }
    
    @RequiresApi(23)
    static class FragmentCompatApi23Impl extends FragmentCompatApi15Impl
    {
        @Override
        public void requestPermissions(final Fragment fragment, final String[] array, final int n) {
            \u3007o00\u3007\u3007Oo.\u3007080(fragment, array, n);
        }
        
        @Override
        public boolean shouldShowRequestPermissionRationale(final Fragment fragment, final String s) {
            return \u3007080.\u3007080(fragment, s);
        }
    }
    
    @RequiresApi(24)
    static class FragmentCompatApi24Impl extends FragmentCompatApi23Impl
    {
        @Override
        public void setUserVisibleHint(final Fragment fragment, final boolean userVisibleHint) {
            fragment.setUserVisibleHint(userVisibleHint);
        }
    }
    
    @Deprecated
    public interface OnRequestPermissionsResultCallback
    {
        @Deprecated
        void onRequestPermissionsResult(final int p0, @NonNull final String[] p1, @NonNull final int[] p2);
    }
    
    @Deprecated
    public interface PermissionCompatDelegate
    {
        @Deprecated
        boolean requestPermissions(final Fragment p0, final String[] p1, final int p2);
    }
}

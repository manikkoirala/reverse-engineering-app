// 
// Decompiled by Procyon v0.6.0
// 

package androidx.exifinterface.media;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.io.FilterOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInput;
import java.util.concurrent.TimeUnit;
import android.location.Location;
import java.util.regex.Matcher;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap$Config;
import android.graphics.Bitmap;
import android.annotation.SuppressLint;
import java.util.Date;
import java.text.ParsePosition;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.util.Iterator;
import java.util.Map;
import android.system.OsConstants;
import android.util.Pair;
import java.nio.ByteBuffer;
import java.io.EOFException;
import java.util.zip.CRC32;
import android.media.MediaDataSource;
import android.os.Build$VERSION;
import android.media.MediaMetadataRetriever;
import androidx.annotation.Nullable;
import java.io.OutputStream;
import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.InputStream;
import java.io.FileInputStream;
import android.system.Os;
import java.io.IOException;
import androidx.annotation.NonNull;
import java.io.File;
import java.util.TimeZone;
import java.util.Locale;
import java.util.Collection;
import java.util.Arrays;
import android.util.Log;
import java.io.FileDescriptor;
import java.nio.ByteOrder;
import java.util.Set;
import android.content.res.AssetManager$AssetInputStream;
import java.util.HashSet;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import androidx.annotation.RestrictTo;
import java.util.List;
import java.util.regex.Pattern;
import java.nio.charset.Charset;

public class ExifInterface
{
    public static final short ALTITUDE_ABOVE_SEA_LEVEL = 0;
    public static final short ALTITUDE_BELOW_SEA_LEVEL = 1;
    static final Charset ASCII;
    public static final int[] BITS_PER_SAMPLE_GREYSCALE_1;
    public static final int[] BITS_PER_SAMPLE_GREYSCALE_2;
    public static final int[] BITS_PER_SAMPLE_RGB;
    static final short BYTE_ALIGN_II = 18761;
    static final short BYTE_ALIGN_MM = 19789;
    public static final int COLOR_SPACE_S_RGB = 1;
    public static final int COLOR_SPACE_UNCALIBRATED = 65535;
    public static final short CONTRAST_HARD = 2;
    public static final short CONTRAST_NORMAL = 0;
    public static final short CONTRAST_SOFT = 1;
    public static final int DATA_DEFLATE_ZIP = 8;
    public static final int DATA_HUFFMAN_COMPRESSED = 2;
    public static final int DATA_JPEG = 6;
    public static final int DATA_JPEG_COMPRESSED = 7;
    public static final int DATA_LOSSY_JPEG = 34892;
    public static final int DATA_PACK_BITS_COMPRESSED = 32773;
    public static final int DATA_UNCOMPRESSED = 1;
    private static final Pattern DATETIME_PRIMARY_FORMAT_PATTERN;
    private static final Pattern DATETIME_SECONDARY_FORMAT_PATTERN;
    private static final int DATETIME_VALUE_STRING_LENGTH = 19;
    private static final boolean DEBUG;
    static final byte[] EXIF_ASCII_PREFIX;
    private static final ExifTag[] EXIF_POINTER_TAGS;
    static final ExifTag[][] EXIF_TAGS;
    public static final short EXPOSURE_MODE_AUTO = 0;
    public static final short EXPOSURE_MODE_AUTO_BRACKET = 2;
    public static final short EXPOSURE_MODE_MANUAL = 1;
    public static final short EXPOSURE_PROGRAM_ACTION = 6;
    public static final short EXPOSURE_PROGRAM_APERTURE_PRIORITY = 3;
    public static final short EXPOSURE_PROGRAM_CREATIVE = 5;
    public static final short EXPOSURE_PROGRAM_LANDSCAPE_MODE = 8;
    public static final short EXPOSURE_PROGRAM_MANUAL = 1;
    public static final short EXPOSURE_PROGRAM_NORMAL = 2;
    public static final short EXPOSURE_PROGRAM_NOT_DEFINED = 0;
    public static final short EXPOSURE_PROGRAM_PORTRAIT_MODE = 7;
    public static final short EXPOSURE_PROGRAM_SHUTTER_PRIORITY = 4;
    public static final short FILE_SOURCE_DSC = 3;
    public static final short FILE_SOURCE_OTHER = 0;
    public static final short FILE_SOURCE_REFLEX_SCANNER = 2;
    public static final short FILE_SOURCE_TRANSPARENT_SCANNER = 1;
    public static final short FLAG_FLASH_FIRED = 1;
    public static final short FLAG_FLASH_MODE_AUTO = 24;
    public static final short FLAG_FLASH_MODE_COMPULSORY_FIRING = 8;
    public static final short FLAG_FLASH_MODE_COMPULSORY_SUPPRESSION = 16;
    public static final short FLAG_FLASH_NO_FLASH_FUNCTION = 32;
    public static final short FLAG_FLASH_RED_EYE_SUPPORTED = 64;
    public static final short FLAG_FLASH_RETURN_LIGHT_DETECTED = 6;
    public static final short FLAG_FLASH_RETURN_LIGHT_NOT_DETECTED = 4;
    private static final List<Integer> FLIPPED_ROTATION_ORDER;
    public static final short FORMAT_CHUNKY = 1;
    public static final short FORMAT_PLANAR = 2;
    public static final short GAIN_CONTROL_HIGH_GAIN_DOWN = 4;
    public static final short GAIN_CONTROL_HIGH_GAIN_UP = 2;
    public static final short GAIN_CONTROL_LOW_GAIN_DOWN = 3;
    public static final short GAIN_CONTROL_LOW_GAIN_UP = 1;
    public static final short GAIN_CONTROL_NONE = 0;
    public static final String GPS_DIRECTION_MAGNETIC = "M";
    public static final String GPS_DIRECTION_TRUE = "T";
    public static final String GPS_DISTANCE_KILOMETERS = "K";
    public static final String GPS_DISTANCE_MILES = "M";
    public static final String GPS_DISTANCE_NAUTICAL_MILES = "N";
    public static final String GPS_MEASUREMENT_2D = "2";
    public static final String GPS_MEASUREMENT_3D = "3";
    public static final short GPS_MEASUREMENT_DIFFERENTIAL_CORRECTED = 1;
    public static final String GPS_MEASUREMENT_INTERRUPTED = "V";
    public static final String GPS_MEASUREMENT_IN_PROGRESS = "A";
    public static final short GPS_MEASUREMENT_NO_DIFFERENTIAL = 0;
    public static final String GPS_SPEED_KILOMETERS_PER_HOUR = "K";
    public static final String GPS_SPEED_KNOTS = "N";
    public static final String GPS_SPEED_MILES_PER_HOUR = "M";
    private static final Pattern GPS_TIMESTAMP_PATTERN;
    private static final byte[] HEIF_BRAND_HEIC;
    private static final byte[] HEIF_BRAND_MIF1;
    private static final byte[] HEIF_TYPE_FTYP;
    static final byte[] IDENTIFIER_EXIF_APP1;
    private static final byte[] IDENTIFIER_XMP_APP1;
    private static final ExifTag[] IFD_EXIF_TAGS;
    private static final int IFD_FORMAT_BYTE = 1;
    static final int[] IFD_FORMAT_BYTES_PER_FORMAT;
    private static final int IFD_FORMAT_DOUBLE = 12;
    private static final int IFD_FORMAT_IFD = 13;
    static final String[] IFD_FORMAT_NAMES;
    private static final int IFD_FORMAT_SBYTE = 6;
    private static final int IFD_FORMAT_SINGLE = 11;
    private static final int IFD_FORMAT_SLONG = 9;
    private static final int IFD_FORMAT_SRATIONAL = 10;
    private static final int IFD_FORMAT_SSHORT = 8;
    private static final int IFD_FORMAT_STRING = 2;
    private static final int IFD_FORMAT_ULONG = 4;
    private static final int IFD_FORMAT_UNDEFINED = 7;
    private static final int IFD_FORMAT_URATIONAL = 5;
    private static final int IFD_FORMAT_USHORT = 3;
    private static final ExifTag[] IFD_GPS_TAGS;
    private static final ExifTag[] IFD_INTEROPERABILITY_TAGS;
    private static final int IFD_OFFSET = 8;
    private static final ExifTag[] IFD_THUMBNAIL_TAGS;
    private static final ExifTag[] IFD_TIFF_TAGS;
    private static final int IFD_TYPE_EXIF = 1;
    private static final int IFD_TYPE_GPS = 2;
    private static final int IFD_TYPE_INTEROPERABILITY = 3;
    private static final int IFD_TYPE_ORF_CAMERA_SETTINGS = 7;
    private static final int IFD_TYPE_ORF_IMAGE_PROCESSING = 8;
    private static final int IFD_TYPE_ORF_MAKER_NOTE = 6;
    private static final int IFD_TYPE_PEF = 9;
    static final int IFD_TYPE_PREVIEW = 5;
    static final int IFD_TYPE_PRIMARY = 0;
    static final int IFD_TYPE_THUMBNAIL = 4;
    private static final int IMAGE_TYPE_ARW = 1;
    private static final int IMAGE_TYPE_CR2 = 2;
    private static final int IMAGE_TYPE_DNG = 3;
    private static final int IMAGE_TYPE_HEIF = 12;
    private static final int IMAGE_TYPE_JPEG = 4;
    private static final int IMAGE_TYPE_NEF = 5;
    private static final int IMAGE_TYPE_NRW = 6;
    private static final int IMAGE_TYPE_ORF = 7;
    private static final int IMAGE_TYPE_PEF = 8;
    private static final int IMAGE_TYPE_PNG = 13;
    private static final int IMAGE_TYPE_RAF = 9;
    private static final int IMAGE_TYPE_RW2 = 10;
    private static final int IMAGE_TYPE_SRW = 11;
    private static final int IMAGE_TYPE_UNKNOWN = 0;
    private static final int IMAGE_TYPE_WEBP = 14;
    private static final ExifTag JPEG_INTERCHANGE_FORMAT_LENGTH_TAG;
    private static final ExifTag JPEG_INTERCHANGE_FORMAT_TAG;
    static final byte[] JPEG_SIGNATURE;
    public static final String LATITUDE_NORTH = "N";
    public static final String LATITUDE_SOUTH = "S";
    public static final short LIGHT_SOURCE_CLOUDY_WEATHER = 10;
    public static final short LIGHT_SOURCE_COOL_WHITE_FLUORESCENT = 14;
    public static final short LIGHT_SOURCE_D50 = 23;
    public static final short LIGHT_SOURCE_D55 = 20;
    public static final short LIGHT_SOURCE_D65 = 21;
    public static final short LIGHT_SOURCE_D75 = 22;
    public static final short LIGHT_SOURCE_DAYLIGHT = 1;
    public static final short LIGHT_SOURCE_DAYLIGHT_FLUORESCENT = 12;
    public static final short LIGHT_SOURCE_DAY_WHITE_FLUORESCENT = 13;
    public static final short LIGHT_SOURCE_FINE_WEATHER = 9;
    public static final short LIGHT_SOURCE_FLASH = 4;
    public static final short LIGHT_SOURCE_FLUORESCENT = 2;
    public static final short LIGHT_SOURCE_ISO_STUDIO_TUNGSTEN = 24;
    public static final short LIGHT_SOURCE_OTHER = 255;
    public static final short LIGHT_SOURCE_SHADE = 11;
    public static final short LIGHT_SOURCE_STANDARD_LIGHT_A = 17;
    public static final short LIGHT_SOURCE_STANDARD_LIGHT_B = 18;
    public static final short LIGHT_SOURCE_STANDARD_LIGHT_C = 19;
    public static final short LIGHT_SOURCE_TUNGSTEN = 3;
    public static final short LIGHT_SOURCE_UNKNOWN = 0;
    public static final short LIGHT_SOURCE_WARM_WHITE_FLUORESCENT = 16;
    public static final short LIGHT_SOURCE_WHITE_FLUORESCENT = 15;
    public static final String LONGITUDE_EAST = "E";
    public static final String LONGITUDE_WEST = "W";
    static final byte MARKER = -1;
    static final byte MARKER_APP1 = -31;
    private static final byte MARKER_COM = -2;
    static final byte MARKER_EOI = -39;
    private static final byte MARKER_SOF0 = -64;
    private static final byte MARKER_SOF1 = -63;
    private static final byte MARKER_SOF10 = -54;
    private static final byte MARKER_SOF11 = -53;
    private static final byte MARKER_SOF13 = -51;
    private static final byte MARKER_SOF14 = -50;
    private static final byte MARKER_SOF15 = -49;
    private static final byte MARKER_SOF2 = -62;
    private static final byte MARKER_SOF3 = -61;
    private static final byte MARKER_SOF5 = -59;
    private static final byte MARKER_SOF6 = -58;
    private static final byte MARKER_SOF7 = -57;
    private static final byte MARKER_SOF9 = -55;
    private static final byte MARKER_SOI = -40;
    private static final byte MARKER_SOS = -38;
    private static final int MAX_THUMBNAIL_SIZE = 512;
    public static final short METERING_MODE_AVERAGE = 1;
    public static final short METERING_MODE_CENTER_WEIGHT_AVERAGE = 2;
    public static final short METERING_MODE_MULTI_SPOT = 4;
    public static final short METERING_MODE_OTHER = 255;
    public static final short METERING_MODE_PARTIAL = 6;
    public static final short METERING_MODE_PATTERN = 5;
    public static final short METERING_MODE_SPOT = 3;
    public static final short METERING_MODE_UNKNOWN = 0;
    private static final Pattern NON_ZERO_TIME_PATTERN;
    private static final ExifTag[] ORF_CAMERA_SETTINGS_TAGS;
    private static final ExifTag[] ORF_IMAGE_PROCESSING_TAGS;
    private static final byte[] ORF_MAKER_NOTE_HEADER_1;
    private static final int ORF_MAKER_NOTE_HEADER_1_SIZE = 8;
    private static final byte[] ORF_MAKER_NOTE_HEADER_2;
    private static final int ORF_MAKER_NOTE_HEADER_2_SIZE = 12;
    private static final ExifTag[] ORF_MAKER_NOTE_TAGS;
    private static final short ORF_SIGNATURE_1 = 20306;
    private static final short ORF_SIGNATURE_2 = 21330;
    public static final int ORIENTATION_FLIP_HORIZONTAL = 2;
    public static final int ORIENTATION_FLIP_VERTICAL = 4;
    public static final int ORIENTATION_NORMAL = 1;
    public static final int ORIENTATION_ROTATE_180 = 3;
    public static final int ORIENTATION_ROTATE_270 = 8;
    public static final int ORIENTATION_ROTATE_90 = 6;
    public static final int ORIENTATION_TRANSPOSE = 5;
    public static final int ORIENTATION_TRANSVERSE = 7;
    public static final int ORIENTATION_UNDEFINED = 0;
    public static final int ORIGINAL_RESOLUTION_IMAGE = 0;
    private static final int PEF_MAKER_NOTE_SKIP_SIZE = 6;
    private static final String PEF_SIGNATURE = "PENTAX";
    private static final ExifTag[] PEF_TAGS;
    public static final int PHOTOMETRIC_INTERPRETATION_BLACK_IS_ZERO = 1;
    public static final int PHOTOMETRIC_INTERPRETATION_RGB = 2;
    public static final int PHOTOMETRIC_INTERPRETATION_WHITE_IS_ZERO = 0;
    public static final int PHOTOMETRIC_INTERPRETATION_YCBCR = 6;
    private static final int PNG_CHUNK_CRC_BYTE_LENGTH = 4;
    private static final int PNG_CHUNK_TYPE_BYTE_LENGTH = 4;
    private static final byte[] PNG_CHUNK_TYPE_EXIF;
    private static final byte[] PNG_CHUNK_TYPE_IEND;
    private static final byte[] PNG_CHUNK_TYPE_IHDR;
    private static final byte[] PNG_SIGNATURE;
    private static final int RAF_OFFSET_TO_JPEG_IMAGE_OFFSET = 84;
    private static final String RAF_SIGNATURE = "FUJIFILMCCD-RAW";
    public static final int REDUCED_RESOLUTION_IMAGE = 1;
    public static final short RENDERED_PROCESS_CUSTOM = 1;
    public static final short RENDERED_PROCESS_NORMAL = 0;
    public static final short RESOLUTION_UNIT_CENTIMETERS = 3;
    public static final short RESOLUTION_UNIT_INCHES = 2;
    private static final List<Integer> ROTATION_ORDER;
    private static final short RW2_SIGNATURE = 85;
    public static final short SATURATION_HIGH = 0;
    public static final short SATURATION_LOW = 0;
    public static final short SATURATION_NORMAL = 0;
    public static final short SCENE_CAPTURE_TYPE_LANDSCAPE = 1;
    public static final short SCENE_CAPTURE_TYPE_NIGHT = 3;
    public static final short SCENE_CAPTURE_TYPE_PORTRAIT = 2;
    public static final short SCENE_CAPTURE_TYPE_STANDARD = 0;
    public static final short SCENE_TYPE_DIRECTLY_PHOTOGRAPHED = 1;
    public static final short SENSITIVITY_TYPE_ISO_SPEED = 3;
    public static final short SENSITIVITY_TYPE_REI = 2;
    public static final short SENSITIVITY_TYPE_REI_AND_ISO = 6;
    public static final short SENSITIVITY_TYPE_SOS = 1;
    public static final short SENSITIVITY_TYPE_SOS_AND_ISO = 5;
    public static final short SENSITIVITY_TYPE_SOS_AND_REI = 4;
    public static final short SENSITIVITY_TYPE_SOS_AND_REI_AND_ISO = 7;
    public static final short SENSITIVITY_TYPE_UNKNOWN = 0;
    public static final short SENSOR_TYPE_COLOR_SEQUENTIAL = 5;
    public static final short SENSOR_TYPE_COLOR_SEQUENTIAL_LINEAR = 8;
    public static final short SENSOR_TYPE_NOT_DEFINED = 1;
    public static final short SENSOR_TYPE_ONE_CHIP = 2;
    public static final short SENSOR_TYPE_THREE_CHIP = 4;
    public static final short SENSOR_TYPE_TRILINEAR = 7;
    public static final short SENSOR_TYPE_TWO_CHIP = 3;
    public static final short SHARPNESS_HARD = 2;
    public static final short SHARPNESS_NORMAL = 0;
    public static final short SHARPNESS_SOFT = 1;
    private static final int SIGNATURE_CHECK_SIZE = 5000;
    static final byte START_CODE = 42;
    public static final int STREAM_TYPE_EXIF_DATA_ONLY = 1;
    public static final int STREAM_TYPE_FULL_IMAGE_DATA = 0;
    public static final short SUBJECT_DISTANCE_RANGE_CLOSE_VIEW = 2;
    public static final short SUBJECT_DISTANCE_RANGE_DISTANT_VIEW = 3;
    public static final short SUBJECT_DISTANCE_RANGE_MACRO = 1;
    public static final short SUBJECT_DISTANCE_RANGE_UNKNOWN = 0;
    private static final String TAG = "ExifInterface";
    public static final String TAG_APERTURE_VALUE = "ApertureValue";
    public static final String TAG_ARTIST = "Artist";
    public static final String TAG_BITS_PER_SAMPLE = "BitsPerSample";
    public static final String TAG_BODY_SERIAL_NUMBER = "BodySerialNumber";
    public static final String TAG_BRIGHTNESS_VALUE = "BrightnessValue";
    @Deprecated
    public static final String TAG_CAMARA_OWNER_NAME = "CameraOwnerName";
    public static final String TAG_CAMERA_OWNER_NAME = "CameraOwnerName";
    public static final String TAG_CFA_PATTERN = "CFAPattern";
    public static final String TAG_COLOR_SPACE = "ColorSpace";
    public static final String TAG_COMPONENTS_CONFIGURATION = "ComponentsConfiguration";
    public static final String TAG_COMPRESSED_BITS_PER_PIXEL = "CompressedBitsPerPixel";
    public static final String TAG_COMPRESSION = "Compression";
    public static final String TAG_CONTRAST = "Contrast";
    public static final String TAG_COPYRIGHT = "Copyright";
    public static final String TAG_CUSTOM_RENDERED = "CustomRendered";
    public static final String TAG_DATETIME = "DateTime";
    public static final String TAG_DATETIME_DIGITIZED = "DateTimeDigitized";
    public static final String TAG_DATETIME_ORIGINAL = "DateTimeOriginal";
    public static final String TAG_DEFAULT_CROP_SIZE = "DefaultCropSize";
    public static final String TAG_DEVICE_SETTING_DESCRIPTION = "DeviceSettingDescription";
    public static final String TAG_DIGITAL_ZOOM_RATIO = "DigitalZoomRatio";
    public static final String TAG_DNG_VERSION = "DNGVersion";
    private static final String TAG_EXIF_IFD_POINTER = "ExifIFDPointer";
    public static final String TAG_EXIF_VERSION = "ExifVersion";
    public static final String TAG_EXPOSURE_BIAS_VALUE = "ExposureBiasValue";
    public static final String TAG_EXPOSURE_INDEX = "ExposureIndex";
    public static final String TAG_EXPOSURE_MODE = "ExposureMode";
    public static final String TAG_EXPOSURE_PROGRAM = "ExposureProgram";
    public static final String TAG_EXPOSURE_TIME = "ExposureTime";
    public static final String TAG_FILE_SOURCE = "FileSource";
    public static final String TAG_FLASH = "Flash";
    public static final String TAG_FLASHPIX_VERSION = "FlashpixVersion";
    public static final String TAG_FLASH_ENERGY = "FlashEnergy";
    public static final String TAG_FOCAL_LENGTH = "FocalLength";
    public static final String TAG_FOCAL_LENGTH_IN_35MM_FILM = "FocalLengthIn35mmFilm";
    public static final String TAG_FOCAL_PLANE_RESOLUTION_UNIT = "FocalPlaneResolutionUnit";
    public static final String TAG_FOCAL_PLANE_X_RESOLUTION = "FocalPlaneXResolution";
    public static final String TAG_FOCAL_PLANE_Y_RESOLUTION = "FocalPlaneYResolution";
    public static final String TAG_F_NUMBER = "FNumber";
    public static final String TAG_GAIN_CONTROL = "GainControl";
    public static final String TAG_GAMMA = "Gamma";
    public static final String TAG_GPS_ALTITUDE = "GPSAltitude";
    public static final String TAG_GPS_ALTITUDE_REF = "GPSAltitudeRef";
    public static final String TAG_GPS_AREA_INFORMATION = "GPSAreaInformation";
    public static final String TAG_GPS_DATESTAMP = "GPSDateStamp";
    public static final String TAG_GPS_DEST_BEARING = "GPSDestBearing";
    public static final String TAG_GPS_DEST_BEARING_REF = "GPSDestBearingRef";
    public static final String TAG_GPS_DEST_DISTANCE = "GPSDestDistance";
    public static final String TAG_GPS_DEST_DISTANCE_REF = "GPSDestDistanceRef";
    public static final String TAG_GPS_DEST_LATITUDE = "GPSDestLatitude";
    public static final String TAG_GPS_DEST_LATITUDE_REF = "GPSDestLatitudeRef";
    public static final String TAG_GPS_DEST_LONGITUDE = "GPSDestLongitude";
    public static final String TAG_GPS_DEST_LONGITUDE_REF = "GPSDestLongitudeRef";
    public static final String TAG_GPS_DIFFERENTIAL = "GPSDifferential";
    public static final String TAG_GPS_DOP = "GPSDOP";
    public static final String TAG_GPS_H_POSITIONING_ERROR = "GPSHPositioningError";
    public static final String TAG_GPS_IMG_DIRECTION = "GPSImgDirection";
    public static final String TAG_GPS_IMG_DIRECTION_REF = "GPSImgDirectionRef";
    private static final String TAG_GPS_INFO_IFD_POINTER = "GPSInfoIFDPointer";
    public static final String TAG_GPS_LATITUDE = "GPSLatitude";
    public static final String TAG_GPS_LATITUDE_REF = "GPSLatitudeRef";
    public static final String TAG_GPS_LONGITUDE = "GPSLongitude";
    public static final String TAG_GPS_LONGITUDE_REF = "GPSLongitudeRef";
    public static final String TAG_GPS_MAP_DATUM = "GPSMapDatum";
    public static final String TAG_GPS_MEASURE_MODE = "GPSMeasureMode";
    public static final String TAG_GPS_PROCESSING_METHOD = "GPSProcessingMethod";
    public static final String TAG_GPS_SATELLITES = "GPSSatellites";
    public static final String TAG_GPS_SPEED = "GPSSpeed";
    public static final String TAG_GPS_SPEED_REF = "GPSSpeedRef";
    public static final String TAG_GPS_STATUS = "GPSStatus";
    public static final String TAG_GPS_TIMESTAMP = "GPSTimeStamp";
    public static final String TAG_GPS_TRACK = "GPSTrack";
    public static final String TAG_GPS_TRACK_REF = "GPSTrackRef";
    public static final String TAG_GPS_VERSION_ID = "GPSVersionID";
    public static final String TAG_IMAGE_DESCRIPTION = "ImageDescription";
    public static final String TAG_IMAGE_LENGTH = "ImageLength";
    public static final String TAG_IMAGE_UNIQUE_ID = "ImageUniqueID";
    public static final String TAG_IMAGE_WIDTH = "ImageWidth";
    private static final String TAG_INTEROPERABILITY_IFD_POINTER = "InteroperabilityIFDPointer";
    public static final String TAG_INTEROPERABILITY_INDEX = "InteroperabilityIndex";
    public static final String TAG_ISO_SPEED = "ISOSpeed";
    public static final String TAG_ISO_SPEED_LATITUDE_YYY = "ISOSpeedLatitudeyyy";
    public static final String TAG_ISO_SPEED_LATITUDE_ZZZ = "ISOSpeedLatitudezzz";
    @Deprecated
    public static final String TAG_ISO_SPEED_RATINGS = "ISOSpeedRatings";
    public static final String TAG_JPEG_INTERCHANGE_FORMAT = "JPEGInterchangeFormat";
    public static final String TAG_JPEG_INTERCHANGE_FORMAT_LENGTH = "JPEGInterchangeFormatLength";
    public static final String TAG_LENS_MAKE = "LensMake";
    public static final String TAG_LENS_MODEL = "LensModel";
    public static final String TAG_LENS_SERIAL_NUMBER = "LensSerialNumber";
    public static final String TAG_LENS_SPECIFICATION = "LensSpecification";
    public static final String TAG_LIGHT_SOURCE = "LightSource";
    public static final String TAG_MAKE = "Make";
    public static final String TAG_MAKER_NOTE = "MakerNote";
    public static final String TAG_MAX_APERTURE_VALUE = "MaxApertureValue";
    public static final String TAG_METERING_MODE = "MeteringMode";
    public static final String TAG_MODEL = "Model";
    public static final String TAG_NEW_SUBFILE_TYPE = "NewSubfileType";
    public static final String TAG_OECF = "OECF";
    public static final String TAG_OFFSET_TIME = "OffsetTime";
    public static final String TAG_OFFSET_TIME_DIGITIZED = "OffsetTimeDigitized";
    public static final String TAG_OFFSET_TIME_ORIGINAL = "OffsetTimeOriginal";
    public static final String TAG_ORF_ASPECT_FRAME = "AspectFrame";
    private static final String TAG_ORF_CAMERA_SETTINGS_IFD_POINTER = "CameraSettingsIFDPointer";
    private static final String TAG_ORF_IMAGE_PROCESSING_IFD_POINTER = "ImageProcessingIFDPointer";
    public static final String TAG_ORF_PREVIEW_IMAGE_LENGTH = "PreviewImageLength";
    public static final String TAG_ORF_PREVIEW_IMAGE_START = "PreviewImageStart";
    public static final String TAG_ORF_THUMBNAIL_IMAGE = "ThumbnailImage";
    public static final String TAG_ORIENTATION = "Orientation";
    public static final String TAG_PHOTOGRAPHIC_SENSITIVITY = "PhotographicSensitivity";
    public static final String TAG_PHOTOMETRIC_INTERPRETATION = "PhotometricInterpretation";
    public static final String TAG_PIXEL_X_DIMENSION = "PixelXDimension";
    public static final String TAG_PIXEL_Y_DIMENSION = "PixelYDimension";
    public static final String TAG_PLANAR_CONFIGURATION = "PlanarConfiguration";
    public static final String TAG_PRIMARY_CHROMATICITIES = "PrimaryChromaticities";
    private static final ExifTag TAG_RAF_IMAGE_SIZE;
    public static final String TAG_RECOMMENDED_EXPOSURE_INDEX = "RecommendedExposureIndex";
    public static final String TAG_REFERENCE_BLACK_WHITE = "ReferenceBlackWhite";
    public static final String TAG_RELATED_SOUND_FILE = "RelatedSoundFile";
    public static final String TAG_RESOLUTION_UNIT = "ResolutionUnit";
    public static final String TAG_ROWS_PER_STRIP = "RowsPerStrip";
    public static final String TAG_RW2_ISO = "ISO";
    public static final String TAG_RW2_JPG_FROM_RAW = "JpgFromRaw";
    public static final String TAG_RW2_SENSOR_BOTTOM_BORDER = "SensorBottomBorder";
    public static final String TAG_RW2_SENSOR_LEFT_BORDER = "SensorLeftBorder";
    public static final String TAG_RW2_SENSOR_RIGHT_BORDER = "SensorRightBorder";
    public static final String TAG_RW2_SENSOR_TOP_BORDER = "SensorTopBorder";
    public static final String TAG_SAMPLES_PER_PIXEL = "SamplesPerPixel";
    public static final String TAG_SATURATION = "Saturation";
    public static final String TAG_SCENE_CAPTURE_TYPE = "SceneCaptureType";
    public static final String TAG_SCENE_TYPE = "SceneType";
    public static final String TAG_SENSING_METHOD = "SensingMethod";
    public static final String TAG_SENSITIVITY_TYPE = "SensitivityType";
    public static final String TAG_SHARPNESS = "Sharpness";
    public static final String TAG_SHUTTER_SPEED_VALUE = "ShutterSpeedValue";
    public static final String TAG_SOFTWARE = "Software";
    public static final String TAG_SPATIAL_FREQUENCY_RESPONSE = "SpatialFrequencyResponse";
    public static final String TAG_SPECTRAL_SENSITIVITY = "SpectralSensitivity";
    public static final String TAG_STANDARD_OUTPUT_SENSITIVITY = "StandardOutputSensitivity";
    public static final String TAG_STRIP_BYTE_COUNTS = "StripByteCounts";
    public static final String TAG_STRIP_OFFSETS = "StripOffsets";
    public static final String TAG_SUBFILE_TYPE = "SubfileType";
    public static final String TAG_SUBJECT_AREA = "SubjectArea";
    public static final String TAG_SUBJECT_DISTANCE = "SubjectDistance";
    public static final String TAG_SUBJECT_DISTANCE_RANGE = "SubjectDistanceRange";
    public static final String TAG_SUBJECT_LOCATION = "SubjectLocation";
    public static final String TAG_SUBSEC_TIME = "SubSecTime";
    public static final String TAG_SUBSEC_TIME_DIGITIZED = "SubSecTimeDigitized";
    public static final String TAG_SUBSEC_TIME_ORIGINAL = "SubSecTimeOriginal";
    private static final String TAG_SUB_IFD_POINTER = "SubIFDPointer";
    public static final String TAG_THUMBNAIL_IMAGE_LENGTH = "ThumbnailImageLength";
    public static final String TAG_THUMBNAIL_IMAGE_WIDTH = "ThumbnailImageWidth";
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final String TAG_THUMBNAIL_ORIENTATION = "ThumbnailOrientation";
    public static final String TAG_TRANSFER_FUNCTION = "TransferFunction";
    public static final String TAG_USER_COMMENT = "UserComment";
    public static final String TAG_WHITE_BALANCE = "WhiteBalance";
    public static final String TAG_WHITE_POINT = "WhitePoint";
    public static final String TAG_XMP = "Xmp";
    public static final String TAG_X_RESOLUTION = "XResolution";
    public static final String TAG_Y_CB_CR_COEFFICIENTS = "YCbCrCoefficients";
    public static final String TAG_Y_CB_CR_POSITIONING = "YCbCrPositioning";
    public static final String TAG_Y_CB_CR_SUB_SAMPLING = "YCbCrSubSampling";
    public static final String TAG_Y_RESOLUTION = "YResolution";
    private static final int WEBP_CHUNK_SIZE_BYTE_LENGTH = 4;
    private static final byte[] WEBP_CHUNK_TYPE_ANIM;
    private static final byte[] WEBP_CHUNK_TYPE_ANMF;
    private static final int WEBP_CHUNK_TYPE_BYTE_LENGTH = 4;
    private static final byte[] WEBP_CHUNK_TYPE_EXIF;
    private static final byte[] WEBP_CHUNK_TYPE_VP8;
    private static final byte[] WEBP_CHUNK_TYPE_VP8L;
    private static final byte[] WEBP_CHUNK_TYPE_VP8X;
    private static final int WEBP_CHUNK_TYPE_VP8X_DEFAULT_LENGTH = 10;
    private static final int WEBP_FILE_SIZE_BYTE_LENGTH = 4;
    private static final byte[] WEBP_SIGNATURE_1;
    private static final byte[] WEBP_SIGNATURE_2;
    private static final byte WEBP_VP8L_SIGNATURE = 47;
    private static final byte[] WEBP_VP8_SIGNATURE;
    @Deprecated
    public static final int WHITEBALANCE_AUTO = 0;
    @Deprecated
    public static final int WHITEBALANCE_MANUAL = 1;
    public static final short WHITE_BALANCE_AUTO = 0;
    public static final short WHITE_BALANCE_MANUAL = 1;
    public static final short Y_CB_CR_POSITIONING_CENTERED = 1;
    public static final short Y_CB_CR_POSITIONING_CO_SITED = 2;
    private static final HashMap<Integer, Integer> sExifPointerTagMap;
    private static final HashMap<Integer, ExifTag>[] sExifTagMapsForReading;
    private static final HashMap<String, ExifTag>[] sExifTagMapsForWriting;
    private static SimpleDateFormat sFormatterPrimary;
    private static SimpleDateFormat sFormatterSecondary;
    private static final HashSet<String> sTagSetForCompatibility;
    private boolean mAreThumbnailStripsConsecutive;
    private AssetManager$AssetInputStream mAssetInputStream;
    private final HashMap<String, ExifAttribute>[] mAttributes;
    private Set<Integer> mAttributesOffsets;
    private ByteOrder mExifByteOrder;
    private String mFilename;
    private boolean mHasThumbnail;
    private boolean mHasThumbnailStrips;
    private boolean mIsExifDataOnly;
    private int mMimeType;
    private boolean mModified;
    private int mOffsetToExifData;
    private int mOrfMakerNoteOffset;
    private int mOrfThumbnailLength;
    private int mOrfThumbnailOffset;
    private FileDescriptor mSeekableFileDescriptor;
    private byte[] mThumbnailBytes;
    private int mThumbnailCompression;
    private int mThumbnailLength;
    private int mThumbnailOffset;
    private boolean mXmpIsFromSeparateMarker;
    
    static {
        final Integer value = 3;
        DEBUG = Log.isLoggable("ExifInterface", 3);
        final Integer value2 = 1;
        final Integer value3 = 2;
        final Integer value4 = 8;
        ROTATION_ORDER = Arrays.asList(value2, 6, value, value4);
        final Integer value5 = 7;
        final Integer value6 = 5;
        FLIPPED_ROTATION_ORDER = Arrays.asList(value3, value5, 4, value6);
        BITS_PER_SAMPLE_RGB = new int[] { 8, 8, 8 };
        BITS_PER_SAMPLE_GREYSCALE_1 = new int[] { 4 };
        BITS_PER_SAMPLE_GREYSCALE_2 = new int[] { 8 };
        JPEG_SIGNATURE = new byte[] { -1, -40, -1 };
        HEIF_TYPE_FTYP = new byte[] { 102, 116, 121, 112 };
        HEIF_BRAND_MIF1 = new byte[] { 109, 105, 102, 49 };
        HEIF_BRAND_HEIC = new byte[] { 104, 101, 105, 99 };
        ORF_MAKER_NOTE_HEADER_1 = new byte[] { 79, 76, 89, 77, 80, 0 };
        ORF_MAKER_NOTE_HEADER_2 = new byte[] { 79, 76, 89, 77, 80, 85, 83, 0, 73, 73 };
        PNG_SIGNATURE = new byte[] { -119, 80, 78, 71, 13, 10, 26, 10 };
        PNG_CHUNK_TYPE_EXIF = new byte[] { 101, 88, 73, 102 };
        PNG_CHUNK_TYPE_IHDR = new byte[] { 73, 72, 68, 82 };
        PNG_CHUNK_TYPE_IEND = new byte[] { 73, 69, 78, 68 };
        WEBP_SIGNATURE_1 = new byte[] { 82, 73, 70, 70 };
        WEBP_SIGNATURE_2 = new byte[] { 87, 69, 66, 80 };
        WEBP_CHUNK_TYPE_EXIF = new byte[] { 69, 88, 73, 70 };
        WEBP_VP8_SIGNATURE = new byte[] { -99, 1, 42 };
        WEBP_CHUNK_TYPE_VP8X = "VP8X".getBytes(Charset.defaultCharset());
        WEBP_CHUNK_TYPE_VP8L = "VP8L".getBytes(Charset.defaultCharset());
        WEBP_CHUNK_TYPE_VP8 = "VP8 ".getBytes(Charset.defaultCharset());
        WEBP_CHUNK_TYPE_ANIM = "ANIM".getBytes(Charset.defaultCharset());
        WEBP_CHUNK_TYPE_ANMF = "ANMF".getBytes(Charset.defaultCharset());
        IFD_FORMAT_NAMES = new String[] { "", "BYTE", "STRING", "USHORT", "ULONG", "URATIONAL", "SBYTE", "UNDEFINED", "SSHORT", "SLONG", "SRATIONAL", "SINGLE", "DOUBLE", "IFD" };
        IFD_FORMAT_BYTES_PER_FORMAT = new int[] { 0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8, 1 };
        EXIF_ASCII_PREFIX = new byte[] { 65, 83, 67, 73, 73, 0, 0, 0 };
        final ExifTag[] array = IFD_TIFF_TAGS = new ExifTag[] { new ExifTag("NewSubfileType", 254, 4), new ExifTag("SubfileType", 255, 4), new ExifTag("ImageWidth", 256, 3, 4), new ExifTag("ImageLength", 257, 3, 4), new ExifTag("BitsPerSample", 258, 3), new ExifTag("Compression", 259, 3), new ExifTag("PhotometricInterpretation", 262, 3), new ExifTag("ImageDescription", 270, 2), new ExifTag("Make", 271, 2), new ExifTag("Model", 272, 2), new ExifTag("StripOffsets", 273, 3, 4), new ExifTag("Orientation", 274, 3), new ExifTag("SamplesPerPixel", 277, 3), new ExifTag("RowsPerStrip", 278, 3, 4), new ExifTag("StripByteCounts", 279, 3, 4), new ExifTag("XResolution", 282, 5), new ExifTag("YResolution", 283, 5), new ExifTag("PlanarConfiguration", 284, 3), new ExifTag("ResolutionUnit", 296, 3), new ExifTag("TransferFunction", 301, 3), new ExifTag("Software", 305, 2), new ExifTag("DateTime", 306, 2), new ExifTag("Artist", 315, 2), new ExifTag("WhitePoint", 318, 5), new ExifTag("PrimaryChromaticities", 319, 5), new ExifTag("SubIFDPointer", 330, 4), new ExifTag("JPEGInterchangeFormat", 513, 4), new ExifTag("JPEGInterchangeFormatLength", 514, 4), new ExifTag("YCbCrCoefficients", 529, 5), new ExifTag("YCbCrSubSampling", 530, 3), new ExifTag("YCbCrPositioning", 531, 3), new ExifTag("ReferenceBlackWhite", 532, 5), new ExifTag("Copyright", 33432, 2), new ExifTag("ExifIFDPointer", 34665, 4), new ExifTag("GPSInfoIFDPointer", 34853, 4), new ExifTag("SensorTopBorder", 4, 4), new ExifTag("SensorLeftBorder", 5, 4), new ExifTag("SensorBottomBorder", 6, 4), new ExifTag("SensorRightBorder", 7, 4), new ExifTag("ISO", 23, 3), new ExifTag("JpgFromRaw", 46, 7), new ExifTag("Xmp", 700, 1) };
        final ExifTag[] array2 = IFD_EXIF_TAGS = new ExifTag[] { new ExifTag("ExposureTime", 33434, 5), new ExifTag("FNumber", 33437, 5), new ExifTag("ExposureProgram", 34850, 3), new ExifTag("SpectralSensitivity", 34852, 2), new ExifTag("PhotographicSensitivity", 34855, 3), new ExifTag("OECF", 34856, 7), new ExifTag("SensitivityType", 34864, 3), new ExifTag("StandardOutputSensitivity", 34865, 4), new ExifTag("RecommendedExposureIndex", 34866, 4), new ExifTag("ISOSpeed", 34867, 4), new ExifTag("ISOSpeedLatitudeyyy", 34868, 4), new ExifTag("ISOSpeedLatitudezzz", 34869, 4), new ExifTag("ExifVersion", 36864, 2), new ExifTag("DateTimeOriginal", 36867, 2), new ExifTag("DateTimeDigitized", 36868, 2), new ExifTag("OffsetTime", 36880, 2), new ExifTag("OffsetTimeOriginal", 36881, 2), new ExifTag("OffsetTimeDigitized", 36882, 2), new ExifTag("ComponentsConfiguration", 37121, 7), new ExifTag("CompressedBitsPerPixel", 37122, 5), new ExifTag("ShutterSpeedValue", 37377, 10), new ExifTag("ApertureValue", 37378, 5), new ExifTag("BrightnessValue", 37379, 10), new ExifTag("ExposureBiasValue", 37380, 10), new ExifTag("MaxApertureValue", 37381, 5), new ExifTag("SubjectDistance", 37382, 5), new ExifTag("MeteringMode", 37383, 3), new ExifTag("LightSource", 37384, 3), new ExifTag("Flash", 37385, 3), new ExifTag("FocalLength", 37386, 5), new ExifTag("SubjectArea", 37396, 3), new ExifTag("MakerNote", 37500, 7), new ExifTag("UserComment", 37510, 7), new ExifTag("SubSecTime", 37520, 2), new ExifTag("SubSecTimeOriginal", 37521, 2), new ExifTag("SubSecTimeDigitized", 37522, 2), new ExifTag("FlashpixVersion", 40960, 7), new ExifTag("ColorSpace", 40961, 3), new ExifTag("PixelXDimension", 40962, 3, 4), new ExifTag("PixelYDimension", 40963, 3, 4), new ExifTag("RelatedSoundFile", 40964, 2), new ExifTag("InteroperabilityIFDPointer", 40965, 4), new ExifTag("FlashEnergy", 41483, 5), new ExifTag("SpatialFrequencyResponse", 41484, 7), new ExifTag("FocalPlaneXResolution", 41486, 5), new ExifTag("FocalPlaneYResolution", 41487, 5), new ExifTag("FocalPlaneResolutionUnit", 41488, 3), new ExifTag("SubjectLocation", 41492, 3), new ExifTag("ExposureIndex", 41493, 5), new ExifTag("SensingMethod", 41495, 3), new ExifTag("FileSource", 41728, 7), new ExifTag("SceneType", 41729, 7), new ExifTag("CFAPattern", 41730, 7), new ExifTag("CustomRendered", 41985, 3), new ExifTag("ExposureMode", 41986, 3), new ExifTag("WhiteBalance", 41987, 3), new ExifTag("DigitalZoomRatio", 41988, 5), new ExifTag("FocalLengthIn35mmFilm", 41989, 3), new ExifTag("SceneCaptureType", 41990, 3), new ExifTag("GainControl", 41991, 3), new ExifTag("Contrast", 41992, 3), new ExifTag("Saturation", 41993, 3), new ExifTag("Sharpness", 41994, 3), new ExifTag("DeviceSettingDescription", 41995, 7), new ExifTag("SubjectDistanceRange", 41996, 3), new ExifTag("ImageUniqueID", 42016, 2), new ExifTag("CameraOwnerName", 42032, 2), new ExifTag("BodySerialNumber", 42033, 2), new ExifTag("LensSpecification", 42034, 5), new ExifTag("LensMake", 42035, 2), new ExifTag("LensModel", 42036, 2), new ExifTag("Gamma", 42240, 5), new ExifTag("DNGVersion", 50706, 1), new ExifTag("DefaultCropSize", 50720, 3, 4) };
        final ExifTag[] array3 = IFD_GPS_TAGS = new ExifTag[] { new ExifTag("GPSVersionID", 0, 1), new ExifTag("GPSLatitudeRef", 1, 2), new ExifTag("GPSLatitude", 2, 5, 10), new ExifTag("GPSLongitudeRef", 3, 2), new ExifTag("GPSLongitude", 4, 5, 10), new ExifTag("GPSAltitudeRef", 5, 1), new ExifTag("GPSAltitude", 6, 5), new ExifTag("GPSTimeStamp", 7, 5), new ExifTag("GPSSatellites", 8, 2), new ExifTag("GPSStatus", 9, 2), new ExifTag("GPSMeasureMode", 10, 2), new ExifTag("GPSDOP", 11, 5), new ExifTag("GPSSpeedRef", 12, 2), new ExifTag("GPSSpeed", 13, 5), new ExifTag("GPSTrackRef", 14, 2), new ExifTag("GPSTrack", 15, 5), new ExifTag("GPSImgDirectionRef", 16, 2), new ExifTag("GPSImgDirection", 17, 5), new ExifTag("GPSMapDatum", 18, 2), new ExifTag("GPSDestLatitudeRef", 19, 2), new ExifTag("GPSDestLatitude", 20, 5), new ExifTag("GPSDestLongitudeRef", 21, 2), new ExifTag("GPSDestLongitude", 22, 5), new ExifTag("GPSDestBearingRef", 23, 2), new ExifTag("GPSDestBearing", 24, 5), new ExifTag("GPSDestDistanceRef", 25, 2), new ExifTag("GPSDestDistance", 26, 5), new ExifTag("GPSProcessingMethod", 27, 7), new ExifTag("GPSAreaInformation", 28, 7), new ExifTag("GPSDateStamp", 29, 2), new ExifTag("GPSDifferential", 30, 3), new ExifTag("GPSHPositioningError", 31, 5) };
        final ExifTag[] array4 = IFD_INTEROPERABILITY_TAGS = new ExifTag[] { new ExifTag("InteroperabilityIndex", 1, 2) };
        final ExifTag[] array5 = IFD_THUMBNAIL_TAGS = new ExifTag[] { new ExifTag("NewSubfileType", 254, 4), new ExifTag("SubfileType", 255, 4), new ExifTag("ThumbnailImageWidth", 256, 3, 4), new ExifTag("ThumbnailImageLength", 257, 3, 4), new ExifTag("BitsPerSample", 258, 3), new ExifTag("Compression", 259, 3), new ExifTag("PhotometricInterpretation", 262, 3), new ExifTag("ImageDescription", 270, 2), new ExifTag("Make", 271, 2), new ExifTag("Model", 272, 2), new ExifTag("StripOffsets", 273, 3, 4), new ExifTag("ThumbnailOrientation", 274, 3), new ExifTag("SamplesPerPixel", 277, 3), new ExifTag("RowsPerStrip", 278, 3, 4), new ExifTag("StripByteCounts", 279, 3, 4), new ExifTag("XResolution", 282, 5), new ExifTag("YResolution", 283, 5), new ExifTag("PlanarConfiguration", 284, 3), new ExifTag("ResolutionUnit", 296, 3), new ExifTag("TransferFunction", 301, 3), new ExifTag("Software", 305, 2), new ExifTag("DateTime", 306, 2), new ExifTag("Artist", 315, 2), new ExifTag("WhitePoint", 318, 5), new ExifTag("PrimaryChromaticities", 319, 5), new ExifTag("SubIFDPointer", 330, 4), new ExifTag("JPEGInterchangeFormat", 513, 4), new ExifTag("JPEGInterchangeFormatLength", 514, 4), new ExifTag("YCbCrCoefficients", 529, 5), new ExifTag("YCbCrSubSampling", 530, 3), new ExifTag("YCbCrPositioning", 531, 3), new ExifTag("ReferenceBlackWhite", 532, 5), new ExifTag("Copyright", 33432, 2), new ExifTag("ExifIFDPointer", 34665, 4), new ExifTag("GPSInfoIFDPointer", 34853, 4), new ExifTag("DNGVersion", 50706, 1), new ExifTag("DefaultCropSize", 50720, 3, 4) };
        TAG_RAF_IMAGE_SIZE = new ExifTag("StripOffsets", 273, 3);
        final ExifTag[][] array6 = EXIF_TAGS = new ExifTag[][] { array, array2, array3, array4, array5, array, ORF_MAKER_NOTE_TAGS = new ExifTag[] { new ExifTag("ThumbnailImage", 256, 7), new ExifTag("CameraSettingsIFDPointer", 8224, 4), new ExifTag("ImageProcessingIFDPointer", 8256, 4) }, ORF_CAMERA_SETTINGS_TAGS = new ExifTag[] { new ExifTag("PreviewImageStart", 257, 4), new ExifTag("PreviewImageLength", 258, 4) }, ORF_IMAGE_PROCESSING_TAGS = new ExifTag[] { new ExifTag("AspectFrame", 4371, 3) }, PEF_TAGS = new ExifTag[] { new ExifTag("ColorSpace", 55, 3) } };
        EXIF_POINTER_TAGS = new ExifTag[] { new ExifTag("SubIFDPointer", 330, 4), new ExifTag("ExifIFDPointer", 34665, 4), new ExifTag("GPSInfoIFDPointer", 34853, 4), new ExifTag("InteroperabilityIFDPointer", 40965, 4), new ExifTag("CameraSettingsIFDPointer", 8224, 1), new ExifTag("ImageProcessingIFDPointer", 8256, 1) };
        JPEG_INTERCHANGE_FORMAT_TAG = new ExifTag("JPEGInterchangeFormat", 513, 4);
        JPEG_INTERCHANGE_FORMAT_LENGTH_TAG = new ExifTag("JPEGInterchangeFormatLength", 514, 4);
        sExifTagMapsForReading = new HashMap[array6.length];
        sExifTagMapsForWriting = new HashMap[array6.length];
        sTagSetForCompatibility = new HashSet<String>(Arrays.asList("FNumber", "DigitalZoomRatio", "ExposureTime", "SubjectDistance", "GPSTimeStamp"));
        sExifPointerTagMap = new HashMap<Integer, Integer>();
        final Charset charset = ASCII = Charset.forName("US-ASCII");
        IDENTIFIER_EXIF_APP1 = "Exif\u0000\u0000".getBytes(charset);
        IDENTIFIER_XMP_APP1 = "http://ns.adobe.com/xap/1.0/\u0000".getBytes(charset);
        final Locale us = Locale.US;
        (ExifInterface.sFormatterPrimary = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss", us)).setTimeZone(TimeZone.getTimeZone("UTC"));
        (ExifInterface.sFormatterSecondary = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", us)).setTimeZone(TimeZone.getTimeZone("UTC"));
        int n = 0;
        while (true) {
            final ExifTag[][] exif_TAGS = ExifInterface.EXIF_TAGS;
            if (n >= exif_TAGS.length) {
                break;
            }
            ExifInterface.sExifTagMapsForReading[n] = new HashMap<Integer, ExifTag>();
            ExifInterface.sExifTagMapsForWriting[n] = new HashMap<String, ExifTag>();
            for (final ExifTag exifTag : exif_TAGS[n]) {
                ExifInterface.sExifTagMapsForReading[n].put(exifTag.number, exifTag);
                ExifInterface.sExifTagMapsForWriting[n].put(exifTag.name, exifTag);
            }
            ++n;
        }
        final HashMap<Integer, Integer> sExifPointerTagMap2 = ExifInterface.sExifPointerTagMap;
        final ExifTag[] exif_POINTER_TAGS = ExifInterface.EXIF_POINTER_TAGS;
        sExifPointerTagMap2.put(exif_POINTER_TAGS[0].number, value6);
        sExifPointerTagMap2.put(exif_POINTER_TAGS[1].number, value2);
        sExifPointerTagMap2.put(exif_POINTER_TAGS[2].number, value3);
        sExifPointerTagMap2.put(exif_POINTER_TAGS[3].number, value);
        sExifPointerTagMap2.put(exif_POINTER_TAGS[4].number, value5);
        sExifPointerTagMap2.put(exif_POINTER_TAGS[5].number, value4);
        NON_ZERO_TIME_PATTERN = Pattern.compile(".*[1-9].*");
        GPS_TIMESTAMP_PATTERN = Pattern.compile("^(\\d{2}):(\\d{2}):(\\d{2})$");
        DATETIME_PRIMARY_FORMAT_PATTERN = Pattern.compile("^(\\d{4}):(\\d{2}):(\\d{2})\\s(\\d{2}):(\\d{2}):(\\d{2})$");
        DATETIME_SECONDARY_FORMAT_PATTERN = Pattern.compile("^(\\d{4})-(\\d{2})-(\\d{2})\\s(\\d{2}):(\\d{2}):(\\d{2})$");
    }
    
    public ExifInterface(@NonNull final File file) throws IOException {
        final ExifTag[][] exif_TAGS = ExifInterface.EXIF_TAGS;
        this.mAttributes = new HashMap[exif_TAGS.length];
        this.mAttributesOffsets = new HashSet<Integer>(exif_TAGS.length);
        this.mExifByteOrder = ByteOrder.BIG_ENDIAN;
        if (file != null) {
            this.initForFilename(file.getAbsolutePath());
            return;
        }
        throw new NullPointerException("file cannot be null");
    }
    
    public ExifInterface(@NonNull FileDescriptor dup) throws IOException {
        final ExifTag[][] exif_TAGS = ExifInterface.EXIF_TAGS;
        this.mAttributes = new HashMap[exif_TAGS.length];
        this.mAttributesOffsets = new HashSet<Integer>(exif_TAGS.length);
        this.mExifByteOrder = ByteOrder.BIG_ENDIAN;
        if (dup == null) {
            throw new NullPointerException("fileDescriptor cannot be null");
        }
        this.mAssetInputStream = null;
        this.mFilename = null;
        boolean b = false;
        Closeable closeable = null;
        Label_0093: {
            if (isSeekableFD(dup)) {
                this.mSeekableFileDescriptor = dup;
                try {
                    dup = Os.dup(dup);
                    b = true;
                    break Label_0093;
                }
                catch (final Exception cause) {
                    throw new IOException("Failed to duplicate file descriptor", cause);
                }
            }
            this.mSeekableFileDescriptor = null;
            b = false;
            try {
                final FileInputStream fileInputStream = new FileInputStream(dup);
                try {
                    this.loadAttributes(fileInputStream);
                    closeQuietly(fileInputStream);
                    if (b) {
                        closeFileDescriptor(dup);
                    }
                    return;
                }
                finally {}
            }
            finally {
                closeable = null;
            }
        }
        closeQuietly(closeable);
        if (b) {
            closeFileDescriptor(dup);
        }
    }
    
    public ExifInterface(@NonNull final InputStream inputStream) throws IOException {
        this(inputStream, 0);
    }
    
    public ExifInterface(@NonNull InputStream in, int n) throws IOException {
        final ExifTag[][] exif_TAGS = ExifInterface.EXIF_TAGS;
        this.mAttributes = new HashMap[exif_TAGS.length];
        this.mAttributesOffsets = new HashSet<Integer>(exif_TAGS.length);
        this.mExifByteOrder = ByteOrder.BIG_ENDIAN;
        if (in != null) {
            this.mFilename = null;
            if (n == 1) {
                n = 1;
            }
            else {
                n = 0;
            }
            Label_0171: {
                if (n != 0) {
                    in = new BufferedInputStream(in, 5000);
                    if (!isExifDataOnly((BufferedInputStream)in)) {
                        return;
                    }
                    this.mIsExifDataOnly = true;
                    this.mAssetInputStream = null;
                    this.mSeekableFileDescriptor = null;
                }
                else if (in instanceof AssetManager$AssetInputStream) {
                    this.mAssetInputStream = (AssetManager$AssetInputStream)in;
                    this.mSeekableFileDescriptor = null;
                }
                else {
                    if (in instanceof FileInputStream) {
                        final FileInputStream fileInputStream = (FileInputStream)in;
                        if (isSeekableFD(fileInputStream.getFD())) {
                            this.mAssetInputStream = null;
                            this.mSeekableFileDescriptor = fileInputStream.getFD();
                            break Label_0171;
                        }
                    }
                    this.mAssetInputStream = null;
                    this.mSeekableFileDescriptor = null;
                }
            }
            this.loadAttributes(in);
            return;
        }
        throw new NullPointerException("inputStream cannot be null");
    }
    
    public ExifInterface(@NonNull final String s) throws IOException {
        final ExifTag[][] exif_TAGS = ExifInterface.EXIF_TAGS;
        this.mAttributes = new HashMap[exif_TAGS.length];
        this.mAttributesOffsets = new HashSet<Integer>(exif_TAGS.length);
        this.mExifByteOrder = ByteOrder.BIG_ENDIAN;
        if (s != null) {
            this.initForFilename(s);
            return;
        }
        throw new NullPointerException("filename cannot be null");
    }
    
    private void addDefaultValuesForCompatibility() {
        final String attribute = this.getAttribute("DateTimeOriginal");
        if (attribute != null && this.getAttribute("DateTime") == null) {
            this.mAttributes[0].put("DateTime", ExifAttribute.createString(attribute));
        }
        if (this.getAttribute("ImageWidth") == null) {
            this.mAttributes[0].put("ImageWidth", ExifAttribute.createULong(0L, this.mExifByteOrder));
        }
        if (this.getAttribute("ImageLength") == null) {
            this.mAttributes[0].put("ImageLength", ExifAttribute.createULong(0L, this.mExifByteOrder));
        }
        if (this.getAttribute("Orientation") == null) {
            this.mAttributes[0].put("Orientation", ExifAttribute.createULong(0L, this.mExifByteOrder));
        }
        if (this.getAttribute("LightSource") == null) {
            this.mAttributes[1].put("LightSource", ExifAttribute.createULong(0L, this.mExifByteOrder));
        }
    }
    
    private static String byteArrayToHexString(final byte[] array) {
        final StringBuilder sb = new StringBuilder(array.length * 2);
        for (int i = 0; i < array.length; ++i) {
            sb.append(String.format("%02x", array[i]));
        }
        return sb.toString();
    }
    
    private static void closeFileDescriptor(final FileDescriptor fileDescriptor) {
        try {
            Os.close(fileDescriptor);
        }
        catch (final Exception ex) {}
    }
    
    private static void closeQuietly(final Closeable closeable) {
        if (closeable == null) {
            goto Label_0016;
        }
        try {
            closeable.close();
            goto Label_0016;
        }
        catch (final RuntimeException ex) {
            throw ex;
        }
        catch (final Exception ex2) {
            goto Label_0016;
        }
    }
    
    private String convertDecimalDegree(double n) {
        final long lng = (long)n;
        n -= lng;
        final long lng2 = (long)(n * 60.0);
        final long round = Math.round((n - lng2 / 60.0) * 3600.0 * 1.0E7);
        final StringBuilder sb = new StringBuilder();
        sb.append(lng);
        sb.append("/1,");
        sb.append(lng2);
        sb.append("/1,");
        sb.append(round);
        sb.append("/10000000");
        return sb.toString();
    }
    
    private static double convertRationalLatLonToDouble(final String s, final String s2) {
        try {
            final String[] split = s.split(",", -1);
            final String[] split2 = split[0].split("/", -1);
            final double n = Double.parseDouble(split2[0].trim()) / Double.parseDouble(split2[1].trim());
            final String[] split3 = split[1].split("/", -1);
            final double n2 = Double.parseDouble(split3[0].trim()) / Double.parseDouble(split3[1].trim());
            final String[] split4 = split[2].split("/", -1);
            final double n3 = n + n2 / 60.0 + Double.parseDouble(split4[0].trim()) / Double.parseDouble(split4[1].trim()) / 3600.0;
            if (s2.equals("S") || s2.equals("W")) {
                return -n3;
            }
            if (!s2.equals("N") && !s2.equals("E")) {
                throw new IllegalArgumentException();
            }
            return n3;
        }
        catch (final NumberFormatException | ArrayIndexOutOfBoundsException ex) {
            throw new IllegalArgumentException();
        }
    }
    
    private static long[] convertToLongArray(final Object o) {
        if (o instanceof int[]) {
            final int[] array = (int[])o;
            final long[] array2 = new long[array.length];
            for (int i = 0; i < array.length; ++i) {
                array2[i] = array[i];
            }
            return array2;
        }
        if (o instanceof long[]) {
            return (long[])o;
        }
        return null;
    }
    
    private static int copy(final InputStream inputStream, final OutputStream outputStream) throws IOException {
        final byte[] array = new byte[8192];
        int n = 0;
        while (true) {
            final int read = inputStream.read(array);
            if (read == -1) {
                break;
            }
            n += read;
            outputStream.write(array, 0, read);
        }
        return n;
    }
    
    private static void copy(final InputStream inputStream, final OutputStream outputStream, int i) throws IOException {
        final byte[] array = new byte[8192];
        while (i > 0) {
            final int min = Math.min(i, 8192);
            final int read = inputStream.read(array, 0, min);
            if (read != min) {
                throw new IOException("Failed to copy the given amount of bytes from the inputstream to the output stream.");
            }
            i -= read;
            outputStream.write(array, 0, read);
        }
    }
    
    private void copyChunksUpToGivenChunkType(final ByteOrderedDataInputStream byteOrderedDataInputStream, final ByteOrderedDataOutputStream byteOrderedDataOutputStream, final byte[] array, final byte[] array2) throws IOException {
        byte[] b;
        do {
            b = new byte[4];
            if (byteOrderedDataInputStream.read(b) != 4) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Encountered invalid length while copying WebP chunks up tochunk type ");
                final Charset ascii = ExifInterface.ASCII;
                sb.append(new String(array, ascii));
                String string;
                if (array2 == null) {
                    string = "";
                }
                else {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append(" or ");
                    sb2.append(new String(array2, ascii));
                    string = sb2.toString();
                }
                sb.append(string);
                throw new IOException(sb.toString());
            }
            this.copyWebPChunk(byteOrderedDataInputStream, byteOrderedDataOutputStream, b);
        } while (!Arrays.equals(b, array) && (array2 == null || !Arrays.equals(b, array2)));
    }
    
    private void copyWebPChunk(final ByteOrderedDataInputStream byteOrderedDataInputStream, final ByteOrderedDataOutputStream byteOrderedDataOutputStream, final byte[] array) throws IOException {
        final int int1 = byteOrderedDataInputStream.readInt();
        byteOrderedDataOutputStream.write(array);
        byteOrderedDataOutputStream.writeInt(int1);
        int n = int1;
        if (int1 % 2 == 1) {
            n = int1 + 1;
        }
        copy(byteOrderedDataInputStream, byteOrderedDataOutputStream, n);
    }
    
    @Nullable
    private ExifAttribute getExifAttribute(@NonNull final String anObject) {
        if (anObject != null) {
            String key = anObject;
            if ("ISOSpeedRatings".equals(anObject)) {
                key = "PhotographicSensitivity";
            }
            for (int i = 0; i < ExifInterface.EXIF_TAGS.length; ++i) {
                final ExifAttribute exifAttribute = this.mAttributes[i].get(key);
                if (exifAttribute != null) {
                    return exifAttribute;
                }
            }
            return null;
        }
        throw new NullPointerException("tag shouldn't be null");
    }
    
    private void getHeifAttributes(final ByteOrderedDataInputStream byteOrderedDataInputStream) throws IOException {
        final MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        try {
            if (Build$VERSION.SDK_INT >= 23) {
                \u3007080.\u3007080(mediaMetadataRetriever, (MediaDataSource)new MediaDataSource(this, byteOrderedDataInputStream) {
                    long mPosition;
                    final ExifInterface this$0;
                    final ByteOrderedDataInputStream val$in;
                    
                    public void close() throws IOException {
                    }
                    
                    public long getSize() throws IOException {
                        return -1L;
                    }
                    
                    public int readAt(final long mPosition, final byte[] array, int read, final int n) throws IOException {
                        if (n == 0) {
                            return 0;
                        }
                        if (mPosition < 0L) {
                            return -1;
                        }
                        while (true) {
                            try {
                                final long mPosition2 = this.mPosition;
                                if (mPosition2 != mPosition) {
                                    if (mPosition2 >= 0L && mPosition >= mPosition2 + this.val$in.available()) {
                                        return -1;
                                    }
                                    this.val$in.seek(mPosition);
                                    this.mPosition = mPosition;
                                }
                                int available;
                                if ((available = n) > this.val$in.available()) {
                                    available = this.val$in.available();
                                }
                                read = this.val$in.read(array, read, available);
                                if (read >= 0) {
                                    this.mPosition += read;
                                    return read;
                                }
                                this.mPosition = -1L;
                                return -1;
                            }
                            catch (final IOException ex) {
                                continue;
                            }
                            break;
                        }
                    }
                });
            }
            else {
                final FileDescriptor mSeekableFileDescriptor = this.mSeekableFileDescriptor;
                if (mSeekableFileDescriptor != null) {
                    mediaMetadataRetriever.setDataSource(mSeekableFileDescriptor);
                }
                else {
                    final String mFilename = this.mFilename;
                    if (mFilename == null) {
                        return;
                    }
                    mediaMetadataRetriever.setDataSource(mFilename);
                }
            }
            final String metadata = mediaMetadataRetriever.extractMetadata(33);
            final String metadata2 = mediaMetadataRetriever.extractMetadata(34);
            final String metadata3 = mediaMetadataRetriever.extractMetadata(26);
            final String metadata4 = mediaMetadataRetriever.extractMetadata(17);
            String s;
            String s2;
            String s3;
            if ("yes".equals(metadata3)) {
                s = mediaMetadataRetriever.extractMetadata(29);
                s2 = mediaMetadataRetriever.extractMetadata(30);
                s3 = mediaMetadataRetriever.extractMetadata(31);
            }
            else if ("yes".equals(metadata4)) {
                s = mediaMetadataRetriever.extractMetadata(18);
                s2 = mediaMetadataRetriever.extractMetadata(19);
                s3 = mediaMetadataRetriever.extractMetadata(24);
            }
            else {
                s = null;
                s2 = null;
                s3 = null;
            }
            if (s != null) {
                this.mAttributes[0].put("ImageWidth", ExifAttribute.createUShort(Integer.parseInt(s), this.mExifByteOrder));
            }
            if (s2 != null) {
                this.mAttributes[0].put("ImageLength", ExifAttribute.createUShort(Integer.parseInt(s2), this.mExifByteOrder));
            }
            if (s3 != null) {
                final int int1 = Integer.parseInt(s3);
                int n;
                if (int1 != 90) {
                    if (int1 != 180) {
                        if (int1 != 270) {
                            n = 1;
                        }
                        else {
                            n = 8;
                        }
                    }
                    else {
                        n = 3;
                    }
                }
                else {
                    n = 6;
                }
                this.mAttributes[0].put("Orientation", ExifAttribute.createUShort(n, this.mExifByteOrder));
            }
            if (metadata != null && metadata2 != null) {
                final int int2 = Integer.parseInt(metadata);
                int int3 = Integer.parseInt(metadata2);
                if (int3 <= 6) {
                    throw new IOException("Invalid exif length");
                }
                byteOrderedDataInputStream.seek(int2);
                final byte[] array = new byte[6];
                if (byteOrderedDataInputStream.read(array) != 6) {
                    throw new IOException("Can't read identifier");
                }
                int3 -= 6;
                if (!Arrays.equals(array, ExifInterface.IDENTIFIER_EXIF_APP1)) {
                    throw new IOException("Invalid identifier");
                }
                final byte[] b = new byte[int3];
                if (byteOrderedDataInputStream.read(b) != int3) {
                    throw new IOException("Can't read exif");
                }
                this.mOffsetToExifData = int2 + 6;
                this.readExifSegment(b, 0);
            }
            if (ExifInterface.DEBUG) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Heif meta: ");
                sb.append(s);
                sb.append("x");
                sb.append(s2);
                sb.append(", rotation ");
                sb.append(s3);
            }
        }
        finally {
            mediaMetadataRetriever.release();
        }
    }
    
    private void getJpegAttributes(final ByteOrderedDataInputStream obj, final int n, final int n2) throws IOException {
        if (ExifInterface.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("getJpegAttributes starting with: ");
            sb.append(obj);
        }
        obj.mark(0);
        obj.setByteOrder(ByteOrder.BIG_ENDIAN);
        final byte byte1 = obj.readByte();
        if (byte1 != -1) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Invalid marker: ");
            sb2.append(Integer.toHexString(byte1 & 0xFF));
            throw new IOException(sb2.toString());
        }
        if (obj.readByte() != -40) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Invalid marker: ");
            sb3.append(Integer.toHexString(byte1 & 0xFF));
            throw new IOException(sb3.toString());
        }
        int n3 = 2;
        while (true) {
            final byte byte2 = obj.readByte();
            if (byte2 != -1) {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("Invalid marker:");
                sb4.append(Integer.toHexString(byte2 & 0xFF));
                throw new IOException(sb4.toString());
            }
            final byte byte3 = obj.readByte();
            final boolean debug = ExifInterface.DEBUG;
            if (debug) {
                final StringBuilder sb5 = new StringBuilder();
                sb5.append("Found JPEG segment indicator: ");
                sb5.append(Integer.toHexString(byte3 & 0xFF));
            }
            if (byte3 == -39 || byte3 == -38) {
                obj.setByteOrder(this.mExifByteOrder);
                return;
            }
            int n4 = obj.readUnsignedShort() - 2;
            final int n5 = n3 + 1 + 1 + 2;
            if (debug) {
                final StringBuilder sb6 = new StringBuilder();
                sb6.append("JPEG segment: ");
                sb6.append(Integer.toHexString(byte3 & 0xFF));
                sb6.append(" (length: ");
                sb6.append(n4 + 2);
                sb6.append(")");
            }
            if (n4 < 0) {
                throw new IOException("Invalid length");
            }
            int n6 = 0;
            int n7 = 0;
            Label_0699: {
                if (byte3 != -31) {
                    if (byte3 != -2) {
                        switch (byte3) {
                            default:
                                Label_0375: {
                                    switch (byte3) {
                                        default: {
                                            switch (byte3) {
                                                default: {
                                                    switch (byte3) {
                                                        default: {
                                                            n6 = n5;
                                                            n7 = n4;
                                                            break Label_0699;
                                                        }
                                                        case -51:
                                                        case -50:
                                                        case -49: {
                                                            break Label_0375;
                                                        }
                                                    }
                                                    break;
                                                }
                                                case -55:
                                                case -54:
                                                case -53: {
                                                    break Label_0375;
                                                }
                                            }
                                            break;
                                        }
                                        case -59:
                                        case -58:
                                        case -57: {
                                            break Label_0375;
                                        }
                                    }
                                    break;
                                }
                            case -64:
                            case -63:
                            case -62:
                            case -61: {
                                if (obj.skipBytes(1) == 1) {
                                    this.mAttributes[n2].put("ImageLength", ExifAttribute.createULong(obj.readUnsignedShort(), this.mExifByteOrder));
                                    this.mAttributes[n2].put("ImageWidth", ExifAttribute.createULong(obj.readUnsignedShort(), this.mExifByteOrder));
                                    n4 -= 5;
                                    n6 = n5;
                                    n7 = n4;
                                    break Label_0699;
                                }
                                throw new IOException("Invalid SOFx");
                            }
                        }
                    }
                    else {
                        final byte[] array = new byte[n4];
                        if (obj.read(array) != n4) {
                            throw new IOException("Invalid exif");
                        }
                        n6 = n5;
                        if (this.getAttribute("UserComment") == null) {
                            this.mAttributes[1].put("UserComment", ExifAttribute.createString(new String(array, ExifInterface.ASCII)));
                            n6 = n5;
                        }
                    }
                }
                else {
                    final byte[] array2 = new byte[n4];
                    obj.readFully(array2);
                    final byte[] identifier_EXIF_APP1 = ExifInterface.IDENTIFIER_EXIF_APP1;
                    if (startsWith(array2, identifier_EXIF_APP1)) {
                        final byte[] copyOfRange = Arrays.copyOfRange(array2, identifier_EXIF_APP1.length, n4);
                        this.mOffsetToExifData = n + n5 + identifier_EXIF_APP1.length;
                        this.readExifSegment(copyOfRange, n2);
                        this.setThumbnailData(new ByteOrderedDataInputStream(copyOfRange));
                    }
                    else {
                        final byte[] identifier_XMP_APP1 = ExifInterface.IDENTIFIER_XMP_APP1;
                        if (startsWith(array2, identifier_XMP_APP1)) {
                            final int length = identifier_XMP_APP1.length;
                            final byte[] copyOfRange2 = Arrays.copyOfRange(array2, identifier_XMP_APP1.length, n4);
                            if (this.getAttribute("Xmp") == null) {
                                this.mAttributes[0].put("Xmp", new ExifAttribute(1, copyOfRange2.length, n5 + length, copyOfRange2));
                                this.mXmpIsFromSeparateMarker = true;
                            }
                        }
                    }
                    n6 = n5 + n4;
                }
                n7 = 0;
            }
            if (n7 < 0) {
                throw new IOException("Invalid length");
            }
            if (obj.skipBytes(n7) != n7) {
                throw new IOException("Invalid JPEG segment");
            }
            n3 = n6 + n7;
        }
    }
    
    private int getMimeType(final BufferedInputStream bufferedInputStream) throws IOException {
        bufferedInputStream.mark(5000);
        final byte[] b = new byte[5000];
        bufferedInputStream.read(b);
        bufferedInputStream.reset();
        if (isJpegFormat(b)) {
            return 4;
        }
        if (this.isRafFormat(b)) {
            return 9;
        }
        if (this.isHeifFormat(b)) {
            return 12;
        }
        if (this.isOrfFormat(b)) {
            return 7;
        }
        if (this.isRw2Format(b)) {
            return 10;
        }
        if (this.isPngFormat(b)) {
            return 13;
        }
        if (this.isWebpFormat(b)) {
            return 14;
        }
        return 0;
    }
    
    private void getOrfAttributes(final ByteOrderedDataInputStream byteOrderedDataInputStream) throws IOException {
        this.getRawAttributes(byteOrderedDataInputStream);
        final ExifAttribute exifAttribute = this.mAttributes[1].get("MakerNote");
        if (exifAttribute != null) {
            final ByteOrderedDataInputStream byteOrderedDataInputStream2 = new ByteOrderedDataInputStream(exifAttribute.bytes);
            byteOrderedDataInputStream2.setByteOrder(this.mExifByteOrder);
            final byte[] orf_MAKER_NOTE_HEADER_1 = ExifInterface.ORF_MAKER_NOTE_HEADER_1;
            final byte[] a = new byte[orf_MAKER_NOTE_HEADER_1.length];
            byteOrderedDataInputStream2.readFully(a);
            byteOrderedDataInputStream2.seek(0L);
            final byte[] orf_MAKER_NOTE_HEADER_2 = ExifInterface.ORF_MAKER_NOTE_HEADER_2;
            final byte[] a2 = new byte[orf_MAKER_NOTE_HEADER_2.length];
            byteOrderedDataInputStream2.readFully(a2);
            if (Arrays.equals(a, orf_MAKER_NOTE_HEADER_1)) {
                byteOrderedDataInputStream2.seek(8L);
            }
            else if (Arrays.equals(a2, orf_MAKER_NOTE_HEADER_2)) {
                byteOrderedDataInputStream2.seek(12L);
            }
            this.readImageFileDirectory(byteOrderedDataInputStream2, 6);
            final ExifAttribute value = this.mAttributes[7].get("PreviewImageStart");
            final ExifAttribute value2 = this.mAttributes[7].get("PreviewImageLength");
            if (value != null && value2 != null) {
                this.mAttributes[5].put("JPEGInterchangeFormat", value);
                this.mAttributes[5].put("JPEGInterchangeFormatLength", value2);
            }
            final ExifAttribute exifAttribute2 = this.mAttributes[8].get("AspectFrame");
            if (exifAttribute2 != null) {
                final int[] a3 = (int[])exifAttribute2.getValue(this.mExifByteOrder);
                if (a3 != null && a3.length == 4) {
                    final int n = a3[2];
                    final int n2 = a3[0];
                    if (n > n2) {
                        final int n3 = a3[3];
                        final int n4 = a3[1];
                        if (n3 > n4) {
                            final int n5 = n - n2 + 1;
                            final int n6 = n3 - n4 + 1;
                            int n7;
                            int n8;
                            if ((n7 = n5) < (n8 = n6)) {
                                final int n9 = n5 + n6;
                                n8 = n9 - n6;
                                n7 = n9 - n8;
                            }
                            final ExifAttribute uShort = ExifAttribute.createUShort(n7, this.mExifByteOrder);
                            final ExifAttribute uShort2 = ExifAttribute.createUShort(n8, this.mExifByteOrder);
                            this.mAttributes[0].put("ImageWidth", uShort);
                            this.mAttributes[0].put("ImageLength", uShort2);
                        }
                    }
                }
                else {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Invalid aspect frame values. frame=");
                    sb.append(Arrays.toString(a3));
                }
            }
        }
    }
    
    private void getPngAttributes(final ByteOrderedDataInputStream obj) throws IOException {
        if (ExifInterface.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("getPngAttributes starting with: ");
            sb.append(obj);
        }
        obj.mark(0);
        obj.setByteOrder(ByteOrder.BIG_ENDIAN);
        final byte[] png_SIGNATURE = ExifInterface.PNG_SIGNATURE;
        obj.skipBytes(png_SIGNATURE.length);
        int n = png_SIGNATURE.length + 0;
        try {
            while (true) {
                int int1 = obj.readInt();
                final byte[] b = new byte[4];
                Label_0349: {
                    if (obj.read(b) != 4) {
                        break Label_0349;
                    }
                    final int mOffsetToExifData = n + 4 + 4;
                    if (mOffsetToExifData == 16 && !Arrays.equals(b, ExifInterface.PNG_CHUNK_TYPE_IHDR)) {
                        throw new IOException("Encountered invalid PNG file--IHDR chunk should appearas the first chunk");
                    }
                    if (Arrays.equals(b, ExifInterface.PNG_CHUNK_TYPE_IEND)) {
                        break;
                    }
                    if (Arrays.equals(b, ExifInterface.PNG_CHUNK_TYPE_EXIF)) {
                        final byte[] array = new byte[int1];
                        if (obj.read(array) != int1) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Failed to read given length for given PNG chunk type: ");
                            sb2.append(byteArrayToHexString(b));
                            throw new IOException(sb2.toString());
                        }
                        final int int2 = obj.readInt();
                        final CRC32 crc32 = new CRC32();
                        crc32.update(b);
                        crc32.update(array);
                        if ((int)crc32.getValue() == int2) {
                            this.mOffsetToExifData = mOffsetToExifData;
                            this.readExifSegment(array, 0);
                            this.validateImages();
                            this.setThumbnailData(new ByteOrderedDataInputStream(array));
                            break;
                        }
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("Encountered invalid CRC value for PNG-EXIF chunk.\n recorded CRC value: ");
                        sb3.append(int2);
                        sb3.append(", calculated CRC value: ");
                        sb3.append(crc32.getValue());
                        throw new IOException(sb3.toString());
                    }
                    int1 += 4;
                    try {
                        obj.skipBytes(int1);
                        n = mOffsetToExifData + int1;
                        continue;
                        throw new IOException("Encountered invalid length while parsing PNG chunktype");
                    }
                    catch (final EOFException ex) {
                        throw new IOException("Encountered corrupt PNG file.");
                    }
                }
            }
        }
        catch (final EOFException ex2) {}
    }
    
    private void getRafAttributes(final ByteOrderedDataInputStream obj) throws IOException {
        final boolean debug = ExifInterface.DEBUG;
        if (debug) {
            final StringBuilder sb = new StringBuilder();
            sb.append("getRafAttributes starting with: ");
            sb.append(obj);
        }
        obj.mark(0);
        obj.skipBytes(84);
        final byte[] array = new byte[4];
        final byte[] array2 = new byte[4];
        final byte[] array3 = new byte[4];
        obj.read(array);
        obj.read(array2);
        obj.read(array3);
        final int int1 = ByteBuffer.wrap(array).getInt();
        final int int2 = ByteBuffer.wrap(array2).getInt();
        final int int3 = ByteBuffer.wrap(array3).getInt();
        final byte[] b = new byte[int2];
        obj.seek(int1);
        obj.read(b);
        this.getJpegAttributes(new ByteOrderedDataInputStream(b), int1, 5);
        obj.seek(int3);
        obj.setByteOrder(ByteOrder.BIG_ENDIAN);
        final int int4 = obj.readInt();
        if (debug) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("numberOfDirectoryEntry: ");
            sb2.append(int4);
        }
        for (int i = 0; i < int4; ++i) {
            final int unsignedShort = obj.readUnsignedShort();
            final int unsignedShort2 = obj.readUnsignedShort();
            if (unsignedShort == ExifInterface.TAG_RAF_IMAGE_SIZE.number) {
                final short short1 = obj.readShort();
                final short short2 = obj.readShort();
                final ExifAttribute uShort = ExifAttribute.createUShort(short1, this.mExifByteOrder);
                final ExifAttribute uShort2 = ExifAttribute.createUShort(short2, this.mExifByteOrder);
                this.mAttributes[0].put("ImageLength", uShort);
                this.mAttributes[0].put("ImageWidth", uShort2);
                if (ExifInterface.DEBUG) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Updated to length: ");
                    sb3.append(short1);
                    sb3.append(", width: ");
                    sb3.append(short2);
                }
                return;
            }
            obj.skipBytes(unsignedShort2);
        }
    }
    
    private void getRawAttributes(ByteOrderedDataInputStream byteOrderedDataInputStream) throws IOException {
        this.parseTiffHeaders(byteOrderedDataInputStream, byteOrderedDataInputStream.available());
        this.readImageFileDirectory(byteOrderedDataInputStream, 0);
        this.updateImageSizeValues(byteOrderedDataInputStream, 0);
        this.updateImageSizeValues(byteOrderedDataInputStream, 5);
        this.updateImageSizeValues(byteOrderedDataInputStream, 4);
        this.validateImages();
        if (this.mMimeType == 8) {
            final ExifAttribute exifAttribute = this.mAttributes[1].get("MakerNote");
            if (exifAttribute != null) {
                byteOrderedDataInputStream = new ByteOrderedDataInputStream(exifAttribute.bytes);
                byteOrderedDataInputStream.setByteOrder(this.mExifByteOrder);
                byteOrderedDataInputStream.seek(6L);
                this.readImageFileDirectory(byteOrderedDataInputStream, 9);
                final ExifAttribute value = this.mAttributes[9].get("ColorSpace");
                if (value != null) {
                    this.mAttributes[1].put("ColorSpace", value);
                }
            }
        }
    }
    
    private void getRw2Attributes(final ByteOrderedDataInputStream obj) throws IOException {
        if (ExifInterface.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("getRw2Attributes starting with: ");
            sb.append(obj);
        }
        this.getRawAttributes(obj);
        final ExifAttribute exifAttribute = this.mAttributes[0].get("JpgFromRaw");
        if (exifAttribute != null) {
            this.getJpegAttributes(new ByteOrderedDataInputStream(exifAttribute.bytes), (int)exifAttribute.bytesOffset, 5);
        }
        final ExifAttribute value = this.mAttributes[0].get("ISO");
        final ExifAttribute exifAttribute2 = this.mAttributes[1].get("PhotographicSensitivity");
        if (value != null && exifAttribute2 == null) {
            this.mAttributes[1].put("PhotographicSensitivity", value);
        }
    }
    
    private void getStandaloneAttributes(final ByteOrderedDataInputStream byteOrderedDataInputStream) throws IOException {
        final byte[] identifier_EXIF_APP1 = ExifInterface.IDENTIFIER_EXIF_APP1;
        byteOrderedDataInputStream.skipBytes(identifier_EXIF_APP1.length);
        final byte[] array = new byte[byteOrderedDataInputStream.available()];
        byteOrderedDataInputStream.readFully(array);
        this.mOffsetToExifData = identifier_EXIF_APP1.length;
        this.readExifSegment(array, 0);
    }
    
    private void getWebpAttributes(final ByteOrderedDataInputStream obj) throws IOException {
        if (ExifInterface.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("getWebpAttributes starting with: ");
            sb.append(obj);
        }
        obj.mark(0);
        obj.setByteOrder(ByteOrder.LITTLE_ENDIAN);
        obj.skipBytes(ExifInterface.WEBP_SIGNATURE_1.length);
        final int n = obj.readInt() + 8;
        int n2 = obj.skipBytes(ExifInterface.WEBP_SIGNATURE_2.length) + 8;
        try {
            while (true) {
                final byte[] array = new byte[4];
                if (obj.read(array) != 4) {
                    throw new IOException("Encountered invalid length while parsing WebP chunktype");
                }
                final int int1 = obj.readInt();
                final int mOffsetToExifData = n2 + 4 + 4;
                if (Arrays.equals(ExifInterface.WEBP_CHUNK_TYPE_EXIF, array)) {
                    final byte[] b = new byte[int1];
                    if (obj.read(b) == int1) {
                        this.mOffsetToExifData = mOffsetToExifData;
                        this.readExifSegment(b, 0);
                        this.setThumbnailData(new ByteOrderedDataInputStream(b));
                        break;
                    }
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Failed to read given length for given PNG chunk type: ");
                    sb2.append(byteArrayToHexString(array));
                    throw new IOException(sb2.toString());
                }
                else {
                    int n3 = int1;
                    if (int1 % 2 == 1) {
                        n3 = int1 + 1;
                    }
                    final int n4 = mOffsetToExifData + n3;
                    if (n4 == n) {
                        break;
                    }
                    if (n4 > n) {
                        throw new IOException("Encountered WebP file with invalid chunk size");
                    }
                    final int skipBytes = obj.skipBytes(n3);
                    if (skipBytes != n3) {
                        throw new IOException("Encountered WebP file with invalid chunk size");
                    }
                    n2 = mOffsetToExifData + skipBytes;
                }
            }
        }
        catch (final EOFException ex) {
            throw new IOException("Encountered corrupt WebP file.");
        }
    }
    
    private static Pair<Integer, Integer> guessDataFormat(final String s) {
        final boolean contains = s.contains(",");
        int i = 1;
        final Integer value = 2;
        final Integer value2 = -1;
        if (contains) {
            final String[] split = s.split(",", -1);
            Pair guessDataFormat;
            final Pair<Integer, Integer> pair = (Pair<Integer, Integer>)(guessDataFormat = guessDataFormat(split[0]));
            if ((int)pair.first == 2) {
                return pair;
            }
            while (i < split.length) {
                final Pair<Integer, Integer> guessDataFormat2 = guessDataFormat(split[i]);
                int intValue;
                if (!((Integer)guessDataFormat2.first).equals(guessDataFormat.first) && !((Integer)guessDataFormat2.second).equals(guessDataFormat.first)) {
                    intValue = -1;
                }
                else {
                    intValue = (int)guessDataFormat.first;
                }
                int intValue2;
                if ((int)guessDataFormat.second != -1 && (((Integer)guessDataFormat2.first).equals(guessDataFormat.second) || ((Integer)guessDataFormat2.second).equals(guessDataFormat.second))) {
                    intValue2 = (int)guessDataFormat.second;
                }
                else {
                    intValue2 = -1;
                }
                if (intValue == -1 && intValue2 == -1) {
                    return (Pair<Integer, Integer>)new Pair((Object)value, (Object)value2);
                }
                if (intValue == -1) {
                    guessDataFormat = new Pair((Object)intValue2, (Object)value2);
                }
                else if (intValue2 == -1) {
                    guessDataFormat = new Pair((Object)intValue, (Object)value2);
                }
                ++i;
            }
            return (Pair<Integer, Integer>)guessDataFormat;
        }
        else {
            Label_0417: {
                if (!s.contains("/")) {
                    break Label_0417;
                }
                final String[] split2 = s.split("/", -1);
                Label_0405: {
                    if (split2.length != 2) {
                        break Label_0405;
                    }
                    try {
                        final long n = (long)Double.parseDouble(split2[0]);
                        final long n2 = (long)Double.parseDouble(split2[1]);
                        if (n < 0L || n2 < 0L) {
                            return (Pair<Integer, Integer>)new Pair((Object)10, (Object)value2);
                        }
                        if (n <= 2147483647L && n2 <= 2147483647L) {
                            return (Pair<Integer, Integer>)new Pair((Object)10, (Object)5);
                        }
                        return (Pair<Integer, Integer>)new Pair((Object)5, (Object)value2);
                        try {
                            final Long value3 = Long.parseLong(s);
                            if (value3 >= 0L && value3 <= 65535L) {
                                return (Pair<Integer, Integer>)new Pair((Object)3, (Object)4);
                            }
                            if (value3 < 0L) {
                                return (Pair<Integer, Integer>)new Pair((Object)9, (Object)value2);
                            }
                            return (Pair<Integer, Integer>)new Pair((Object)4, (Object)value2);
                        }
                        catch (final NumberFormatException ex) {
                            try {
                                Double.parseDouble(s);
                                return (Pair<Integer, Integer>)new Pair((Object)12, (Object)value2);
                            }
                            catch (final NumberFormatException ex2) {
                                return (Pair<Integer, Integer>)new Pair((Object)value, (Object)value2);
                            }
                        }
                        return (Pair<Integer, Integer>)new Pair((Object)value, (Object)value2);
                    }
                    catch (final NumberFormatException ex3) {
                        return (Pair<Integer, Integer>)new Pair((Object)value, (Object)value2);
                    }
                }
            }
        }
    }
    
    private void handleThumbnailFromJfif(final ByteOrderedDataInputStream byteOrderedDataInputStream, final HashMap hashMap) throws IOException {
        final ExifAttribute exifAttribute = hashMap.get("JPEGInterchangeFormat");
        final ExifAttribute exifAttribute2 = hashMap.get("JPEGInterchangeFormatLength");
        if (exifAttribute != null && exifAttribute2 != null) {
            final int intValue = exifAttribute.getIntValue(this.mExifByteOrder);
            final int intValue2 = exifAttribute2.getIntValue(this.mExifByteOrder);
            int n = intValue;
            if (this.mMimeType == 7) {
                n = intValue + this.mOrfMakerNoteOffset;
            }
            final int min = Math.min(intValue2, byteOrderedDataInputStream.getLength() - n);
            if (n > 0 && min > 0) {
                this.mHasThumbnail = true;
                if (this.mFilename == null && this.mAssetInputStream == null && this.mSeekableFileDescriptor == null) {
                    final byte[] array = new byte[min];
                    byteOrderedDataInputStream.skip(n);
                    byteOrderedDataInputStream.read(array);
                    this.mThumbnailBytes = array;
                }
                this.mThumbnailOffset = n;
                this.mThumbnailLength = min;
            }
            if (ExifInterface.DEBUG) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Setting thumbnail attributes with offset: ");
                sb.append(n);
                sb.append(", length: ");
                sb.append(min);
            }
        }
    }
    
    private void handleThumbnailFromStrips(final ByteOrderedDataInputStream byteOrderedDataInputStream, final HashMap hashMap) throws IOException {
        final ExifAttribute exifAttribute = hashMap.get("StripOffsets");
        final ExifAttribute exifAttribute2 = hashMap.get("StripByteCounts");
        if (exifAttribute != null && exifAttribute2 != null) {
            final long[] convertToLongArray = convertToLongArray(exifAttribute.getValue(this.mExifByteOrder));
            final long[] convertToLongArray2 = convertToLongArray(exifAttribute2.getValue(this.mExifByteOrder));
            if (convertToLongArray != null) {
                if (convertToLongArray.length != 0) {
                    if (convertToLongArray2 != null) {
                        if (convertToLongArray2.length != 0) {
                            if (convertToLongArray.length != convertToLongArray2.length) {
                                return;
                            }
                            final int length = convertToLongArray2.length;
                            long n = 0L;
                            for (int i = 0; i < length; ++i) {
                                n += convertToLongArray2[i];
                            }
                            final int mThumbnailLength = (int)n;
                            final byte[] mThumbnailBytes = new byte[mThumbnailLength];
                            this.mAreThumbnailStripsConsecutive = true;
                            this.mHasThumbnailStrips = true;
                            this.mHasThumbnail = true;
                            int j = 0;
                            int n2 = 0;
                            int n3 = 0;
                            while (j < convertToLongArray.length) {
                                final int n4 = (int)convertToLongArray[j];
                                final int k = (int)convertToLongArray2[j];
                                if (j < convertToLongArray.length - 1 && n4 + k != convertToLongArray[j + 1]) {
                                    this.mAreThumbnailStripsConsecutive = false;
                                }
                                final int l = n4 - n2;
                                if (l < 0) {
                                    return;
                                }
                                final long n5 = l;
                                if (byteOrderedDataInputStream.skip(n5) != n5) {
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append("Failed to skip ");
                                    sb.append(l);
                                    sb.append(" bytes.");
                                    return;
                                }
                                final byte[] b = new byte[k];
                                if (byteOrderedDataInputStream.read(b) != k) {
                                    final StringBuilder sb2 = new StringBuilder();
                                    sb2.append("Failed to read ");
                                    sb2.append(k);
                                    sb2.append(" bytes.");
                                    return;
                                }
                                n2 = n2 + l + k;
                                System.arraycopy(b, 0, mThumbnailBytes, n3, k);
                                n3 += k;
                                ++j;
                            }
                            this.mThumbnailBytes = mThumbnailBytes;
                            if (this.mAreThumbnailStripsConsecutive) {
                                this.mThumbnailOffset = (int)convertToLongArray[0];
                                this.mThumbnailLength = mThumbnailLength;
                            }
                        }
                    }
                }
            }
        }
    }
    
    private void initForFilename(final String s) throws IOException {
        if (s == null) {
            throw new NullPointerException("filename cannot be null");
        }
        Closeable closeable = null;
        this.mAssetInputStream = null;
        this.mFilename = s;
        try {
            final FileInputStream fileInputStream = new FileInputStream(s);
            try {
                if (isSeekableFD(fileInputStream.getFD())) {
                    this.mSeekableFileDescriptor = fileInputStream.getFD();
                }
                else {
                    this.mSeekableFileDescriptor = null;
                }
                this.loadAttributes(fileInputStream);
                closeQuietly(fileInputStream);
                return;
            }
            finally {
                closeable = fileInputStream;
            }
        }
        finally {}
        closeQuietly(closeable);
    }
    
    private static boolean isExifDataOnly(final BufferedInputStream bufferedInputStream) throws IOException {
        final byte[] identifier_EXIF_APP1 = ExifInterface.IDENTIFIER_EXIF_APP1;
        bufferedInputStream.mark(identifier_EXIF_APP1.length);
        final byte[] b = new byte[identifier_EXIF_APP1.length];
        bufferedInputStream.read(b);
        bufferedInputStream.reset();
        int n = 0;
        while (true) {
            final byte[] identifier_EXIF_APP2 = ExifInterface.IDENTIFIER_EXIF_APP1;
            if (n >= identifier_EXIF_APP2.length) {
                return true;
            }
            if (b[n] != identifier_EXIF_APP2[n]) {
                return false;
            }
            ++n;
        }
    }
    
    private boolean isHeifFormat(byte[] a) throws IOException {
        final InputStream inputStream = null;
        Object o2;
        final Object o = o2 = null;
        while (true) {
            try {
                final InputStream inputStream2;
                Label_0335: {
                    try {
                        o2 = o;
                        final ByteOrderedDataInputStream byteOrderedDataInputStream = new ByteOrderedDataInputStream(a);
                        try {
                            long n = byteOrderedDataInputStream.readInt();
                            o2 = new byte[4];
                            byteOrderedDataInputStream.read((byte[])o2);
                            if (!Arrays.equals((byte[])o2, ExifInterface.HEIF_TYPE_FTYP)) {
                                byteOrderedDataInputStream.close();
                                return false;
                            }
                            long n2;
                            if (n == 1L) {
                                final long long1 = byteOrderedDataInputStream.readLong();
                                n2 = 16L;
                                n = long1;
                                if (long1 < 16L) {
                                    byteOrderedDataInputStream.close();
                                    return false;
                                }
                            }
                            else {
                                n2 = 8L;
                            }
                            long n3 = n;
                            if (n > a.length) {
                                n3 = a.length;
                            }
                            final long n4 = n3 - n2;
                            if (n4 < 8L) {
                                byteOrderedDataInputStream.close();
                                return false;
                            }
                            a = new byte[4];
                            long n5 = 0L;
                            int n6 = 0;
                            int n7 = 0;
                            while (n5 < n4 / 4L) {
                                if (byteOrderedDataInputStream.read(a) != 4) {
                                    byteOrderedDataInputStream.close();
                                    return false;
                                }
                                int n8;
                                if (n5 == 1L) {
                                    n8 = n7;
                                }
                                else {
                                    int n9;
                                    if (Arrays.equals(a, ExifInterface.HEIF_BRAND_MIF1)) {
                                        n9 = 1;
                                    }
                                    else {
                                        final boolean equals = Arrays.equals(a, ExifInterface.HEIF_BRAND_HEIC);
                                        n9 = n6;
                                        if (equals) {
                                            n7 = 1;
                                            n9 = n6;
                                        }
                                    }
                                    n6 = n9;
                                    n8 = n7;
                                    if (n9 != 0) {
                                        n6 = n9;
                                        if ((n8 = n7) != 0) {
                                            byteOrderedDataInputStream.close();
                                            return true;
                                        }
                                    }
                                }
                                ++n5;
                                n7 = n8;
                            }
                            byteOrderedDataInputStream.close();
                            return false;
                        }
                        catch (final Exception ex) {}
                        finally {
                            o2 = byteOrderedDataInputStream;
                        }
                    }
                    finally {
                        break Label_0335;
                    }
                    final boolean debug = ExifInterface.DEBUG;
                    if (inputStream2 != null) {
                        inputStream2.close();
                    }
                    return false;
                }
                if (o2 != null) {
                    ((InputStream)o2).close();
                }
                throw inputStream2;
            }
            catch (final Exception ex2) {
                final InputStream inputStream2 = inputStream;
                continue;
            }
            break;
        }
    }
    
    private static boolean isJpegFormat(final byte[] array) throws IOException {
        int n = 0;
        while (true) {
            final byte[] jpeg_SIGNATURE = ExifInterface.JPEG_SIGNATURE;
            if (n >= jpeg_SIGNATURE.length) {
                return true;
            }
            if (array[n] != jpeg_SIGNATURE[n]) {
                return false;
            }
            ++n;
        }
    }
    
    private boolean isOrfFormat(final byte[] array) throws IOException {
        boolean b = false;
        final InputStream inputStream = null;
        final InputStream inputStream2 = null;
        InputStream inputStream3 = null;
        try {
            final ByteOrderedDataInputStream byteOrderedDataInputStream = new ByteOrderedDataInputStream(array);
            try {
                byteOrderedDataInputStream.setByteOrder(this.mExifByteOrder = this.readByteOrder(byteOrderedDataInputStream));
                final short short1 = byteOrderedDataInputStream.readShort();
                if (short1 == 20306 || short1 == 21330) {
                    b = true;
                }
                byteOrderedDataInputStream.close();
                return b;
            }
            catch (final Exception ex) {}
        }
        catch (final Exception ex2) {
            inputStream3 = inputStream;
        }
        finally {
            inputStream3 = inputStream2;
        }
        if (inputStream3 != null) {
            inputStream3.close();
        }
        return false;
    }
    
    private boolean isPngFormat(final byte[] array) throws IOException {
        int n = 0;
        while (true) {
            final byte[] png_SIGNATURE = ExifInterface.PNG_SIGNATURE;
            if (n >= png_SIGNATURE.length) {
                return true;
            }
            if (array[n] != png_SIGNATURE[n]) {
                return false;
            }
            ++n;
        }
    }
    
    private boolean isRafFormat(final byte[] array) throws IOException {
        final byte[] bytes = "FUJIFILMCCD-RAW".getBytes(Charset.defaultCharset());
        for (int i = 0; i < bytes.length; ++i) {
            if (array[i] != bytes[i]) {
                return false;
            }
        }
        return true;
    }
    
    private boolean isRw2Format(final byte[] array) throws IOException {
        boolean b = false;
        final InputStream inputStream = null;
        InputStream inputStream2 = null;
        try {
            final ByteOrderedDataInputStream byteOrderedDataInputStream = new ByteOrderedDataInputStream(array);
            try {
                byteOrderedDataInputStream.setByteOrder(this.mExifByteOrder = this.readByteOrder(byteOrderedDataInputStream));
                if (byteOrderedDataInputStream.readShort() == 85) {
                    b = true;
                }
                byteOrderedDataInputStream.close();
                return b;
            }
            catch (final Exception ex) {}
        }
        catch (final Exception ex2) {
            inputStream2 = inputStream;
        }
        if (inputStream2 != null) {
            inputStream2.close();
        }
        return false;
    }
    
    private static boolean isSeekableFD(final FileDescriptor fileDescriptor) {
        try {
            Os.lseek(fileDescriptor, 0L, OsConstants.SEEK_CUR);
            return true;
        }
        catch (final Exception ex) {
            return false;
        }
    }
    
    private boolean isSupportedDataType(final HashMap hashMap) throws IOException {
        final ExifAttribute exifAttribute = hashMap.get("BitsPerSample");
        if (exifAttribute != null) {
            final int[] a = (int[])exifAttribute.getValue(this.mExifByteOrder);
            final int[] bits_PER_SAMPLE_RGB = ExifInterface.BITS_PER_SAMPLE_RGB;
            if (Arrays.equals(bits_PER_SAMPLE_RGB, a)) {
                return true;
            }
            if (this.mMimeType == 3) {
                final ExifAttribute exifAttribute2 = hashMap.get("PhotometricInterpretation");
                if (exifAttribute2 != null) {
                    final int intValue = exifAttribute2.getIntValue(this.mExifByteOrder);
                    if ((intValue == 1 && Arrays.equals(a, ExifInterface.BITS_PER_SAMPLE_GREYSCALE_2)) || (intValue == 6 && Arrays.equals(a, bits_PER_SAMPLE_RGB))) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    private boolean isSupportedFormatForSavingAttributes() {
        final int mMimeType = this.mMimeType;
        return mMimeType == 4 || mMimeType == 13 || mMimeType == 14;
    }
    
    public static boolean isSupportedMimeType(@NonNull String lowerCase) {
        if (lowerCase == null) {
            throw new NullPointerException("mimeType shouldn't be null");
        }
        lowerCase = lowerCase.toLowerCase(Locale.ROOT);
        lowerCase.hashCode();
        final int hashCode = lowerCase.hashCode();
        int n = -1;
        switch (hashCode) {
            case 2111234748: {
                if (!lowerCase.equals("image/x-canon-cr2")) {
                    break;
                }
                n = 14;
                break;
            }
            case 2099152524: {
                if (!lowerCase.equals("image/x-nikon-nrw")) {
                    break;
                }
                n = 13;
                break;
            }
            case 2099152104: {
                if (!lowerCase.equals("image/x-nikon-nef")) {
                    break;
                }
                n = 12;
                break;
            }
            case 1378106698: {
                if (!lowerCase.equals("image/x-olympus-orf")) {
                    break;
                }
                n = 11;
                break;
            }
            case -332763809: {
                if (!lowerCase.equals("image/x-pentax-pef")) {
                    break;
                }
                n = 10;
                break;
            }
            case -879258763: {
                if (!lowerCase.equals("image/png")) {
                    break;
                }
                n = 9;
                break;
            }
            case -985160897: {
                if (!lowerCase.equals("image/x-panasonic-rw2")) {
                    break;
                }
                n = 8;
                break;
            }
            case -1423313290: {
                if (!lowerCase.equals("image/x-adobe-dng")) {
                    break;
                }
                n = 7;
                break;
            }
            case -1487018032: {
                if (!lowerCase.equals("image/webp")) {
                    break;
                }
                n = 6;
                break;
            }
            case -1487394660: {
                if (!lowerCase.equals("image/jpeg")) {
                    break;
                }
                n = 5;
                break;
            }
            case -1487464690: {
                if (!lowerCase.equals("image/heif")) {
                    break;
                }
                n = 4;
                break;
            }
            case -1487464693: {
                if (!lowerCase.equals("image/heic")) {
                    break;
                }
                n = 3;
                break;
            }
            case -1594371159: {
                if (!lowerCase.equals("image/x-sony-arw")) {
                    break;
                }
                n = 2;
                break;
            }
            case -1635437028: {
                if (!lowerCase.equals("image/x-samsung-srw")) {
                    break;
                }
                n = 1;
                break;
            }
            case -1875291391: {
                if (!lowerCase.equals("image/x-fuji-raf")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        switch (n) {
            default: {
                return false;
            }
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14: {
                return true;
            }
        }
    }
    
    private boolean isThumbnail(final HashMap hashMap) throws IOException {
        final ExifAttribute exifAttribute = hashMap.get("ImageLength");
        final ExifAttribute exifAttribute2 = hashMap.get("ImageWidth");
        if (exifAttribute != null && exifAttribute2 != null) {
            final int intValue = exifAttribute.getIntValue(this.mExifByteOrder);
            final int intValue2 = exifAttribute2.getIntValue(this.mExifByteOrder);
            if (intValue <= 512 && intValue2 <= 512) {
                return true;
            }
        }
        return false;
    }
    
    private boolean isWebpFormat(final byte[] array) throws IOException {
        int n = 0;
        while (true) {
            final byte[] webp_SIGNATURE_1 = ExifInterface.WEBP_SIGNATURE_1;
            if (n < webp_SIGNATURE_1.length) {
                if (array[n] != webp_SIGNATURE_1[n]) {
                    return false;
                }
                ++n;
            }
            else {
                int n2 = 0;
                while (true) {
                    final byte[] webp_SIGNATURE_2 = ExifInterface.WEBP_SIGNATURE_2;
                    if (n2 >= webp_SIGNATURE_2.length) {
                        return true;
                    }
                    if (array[ExifInterface.WEBP_SIGNATURE_1.length + n2 + 4] != webp_SIGNATURE_2[n2]) {
                        return false;
                    }
                    ++n2;
                }
            }
        }
    }
    
    private void loadAttributes(@NonNull final InputStream in) {
        if (in != null) {
            int i = 0;
            try {
                Label_0342: {
                    try {
                        while (i < ExifInterface.EXIF_TAGS.length) {
                            this.mAttributes[i] = new HashMap<String, ExifAttribute>();
                            ++i;
                        }
                        InputStream inputStream = in;
                        if (!this.mIsExifDataOnly) {
                            inputStream = new BufferedInputStream(in, 5000);
                            this.mMimeType = this.getMimeType((BufferedInputStream)inputStream);
                        }
                        final ByteOrderedDataInputStream thumbnailData = new ByteOrderedDataInputStream(inputStream);
                        if (!this.mIsExifDataOnly) {
                            switch (this.mMimeType) {
                                case 14: {
                                    this.getWebpAttributes(thumbnailData);
                                    this.addDefaultValuesForCompatibility();
                                    if (ExifInterface.DEBUG) {
                                        this.printAttributes();
                                    }
                                    return;
                                }
                                case 13: {
                                    this.getPngAttributes(thumbnailData);
                                    this.addDefaultValuesForCompatibility();
                                    if (ExifInterface.DEBUG) {
                                        this.printAttributes();
                                    }
                                    return;
                                }
                                case 12: {
                                    this.getHeifAttributes(thumbnailData);
                                    break;
                                }
                                case 10: {
                                    this.getRw2Attributes(thumbnailData);
                                    this.addDefaultValuesForCompatibility();
                                    if (ExifInterface.DEBUG) {
                                        this.printAttributes();
                                    }
                                    return;
                                }
                                case 9: {
                                    this.getRafAttributes(thumbnailData);
                                    this.addDefaultValuesForCompatibility();
                                    if (ExifInterface.DEBUG) {
                                        this.printAttributes();
                                    }
                                    return;
                                }
                                case 7: {
                                    this.getOrfAttributes(thumbnailData);
                                    break;
                                }
                                case 4: {
                                    this.getJpegAttributes(thumbnailData, 0, 0);
                                    this.addDefaultValuesForCompatibility();
                                    if (ExifInterface.DEBUG) {
                                        this.printAttributes();
                                    }
                                    return;
                                }
                                case 0:
                                case 1:
                                case 2:
                                case 3:
                                case 5:
                                case 6:
                                case 8:
                                case 11: {
                                    this.getRawAttributes(thumbnailData);
                                    break;
                                }
                            }
                        }
                        else {
                            this.getStandaloneAttributes(thumbnailData);
                        }
                        thumbnailData.seek(this.mOffsetToExifData);
                        this.setThumbnailData(thumbnailData);
                        this.addDefaultValuesForCompatibility();
                        if (ExifInterface.DEBUG) {
                            break Label_0342;
                        }
                        return;
                    }
                    finally {
                        this.addDefaultValuesForCompatibility();
                        if (ExifInterface.DEBUG) {
                            this.printAttributes();
                        }
                        return;
                        this.printAttributes();
                    }
                }
            }
            catch (final IOException ex) {}
        }
        throw new NullPointerException("inputstream shouldn't be null");
    }
    
    private static Long parseDateTime(@Nullable final String p0, @Nullable final String p1, @Nullable final String p2) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: ifnull          263
        //     4: getstatic       androidx/exifinterface/media/ExifInterface.NON_ZERO_TIME_PATTERN:Ljava/util/regex/Pattern;
        //     7: aload_0        
        //     8: invokevirtual   java/util/regex/Pattern.matcher:(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
        //    11: invokevirtual   java/util/regex/Matcher.matches:()Z
        //    14: ifne            20
        //    17: goto            263
        //    20: new             Ljava/text/ParsePosition;
        //    23: dup            
        //    24: iconst_0       
        //    25: invokespecial   java/text/ParsePosition.<init>:(I)V
        //    28: astore          13
        //    30: getstatic       androidx/exifinterface/media/ExifInterface.sFormatterPrimary:Ljava/text/SimpleDateFormat;
        //    33: aload_0        
        //    34: aload           13
        //    36: invokevirtual   java/text/SimpleDateFormat.parse:(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/util/Date;
        //    39: astore          12
        //    41: aload           12
        //    43: astore          11
        //    45: aload           12
        //    47: ifnonnull       69
        //    50: getstatic       androidx/exifinterface/media/ExifInterface.sFormatterSecondary:Ljava/text/SimpleDateFormat;
        //    53: aload_0        
        //    54: aload           13
        //    56: invokevirtual   java/text/SimpleDateFormat.parse:(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/util/Date;
        //    59: astore_0       
        //    60: aload_0        
        //    61: astore          11
        //    63: aload_0        
        //    64: ifnonnull       69
        //    67: aconst_null    
        //    68: areturn        
        //    69: aload           11
        //    71: invokevirtual   java/util/Date.getTime:()J
        //    74: lstore          9
        //    76: lload           9
        //    78: lstore          7
        //    80: aload_2        
        //    81: ifnull          216
        //    84: iconst_1       
        //    85: istore_3       
        //    86: aload_2        
        //    87: iconst_0       
        //    88: iconst_1       
        //    89: invokevirtual   java/lang/String.substring:(II)Ljava/lang/String;
        //    92: astore_0       
        //    93: aload_2        
        //    94: iconst_1       
        //    95: iconst_3       
        //    96: invokevirtual   java/lang/String.substring:(II)Ljava/lang/String;
        //    99: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   102: istore          4
        //   104: aload_2        
        //   105: iconst_4       
        //   106: bipush          6
        //   108: invokevirtual   java/lang/String.substring:(II)Ljava/lang/String;
        //   111: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   114: istore          5
        //   116: ldc_w           "+"
        //   119: aload_0        
        //   120: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   123: istore          6
        //   125: iload           6
        //   127: ifne            144
        //   130: lload           9
        //   132: lstore          7
        //   134: ldc_w           "-"
        //   137: aload_0        
        //   138: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   141: ifeq            216
        //   144: lload           9
        //   146: lstore          7
        //   148: ldc_w           ":"
        //   151: aload_2        
        //   152: iconst_3       
        //   153: iconst_4       
        //   154: invokevirtual   java/lang/String.substring:(II)Ljava/lang/String;
        //   157: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   160: ifeq            216
        //   163: lload           9
        //   165: lstore          7
        //   167: iload           4
        //   169: bipush          14
        //   171: if_icmpgt       216
        //   174: ldc_w           "-"
        //   177: aload_0        
        //   178: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   181: istore          6
        //   183: iload           6
        //   185: ifeq            191
        //   188: goto            193
        //   191: iconst_m1      
        //   192: istore_3       
        //   193: lload           9
        //   195: iload           4
        //   197: bipush          60
        //   199: imul           
        //   200: iload           5
        //   202: iadd           
        //   203: bipush          60
        //   205: imul           
        //   206: sipush          1000
        //   209: imul           
        //   210: iload_3        
        //   211: imul           
        //   212: i2l            
        //   213: ladd           
        //   214: lstore          7
        //   216: lload           7
        //   218: lstore          9
        //   220: aload_1        
        //   221: ifnull          257
        //   224: aload_1        
        //   225: invokestatic    java/lang/Long.parseLong:(Ljava/lang/String;)J
        //   228: lstore          9
        //   230: lload           9
        //   232: ldc2_w          1000
        //   235: lcmp           
        //   236: ifle            250
        //   239: lload           9
        //   241: ldc2_w          10
        //   244: ldiv           
        //   245: lstore          9
        //   247: goto            230
        //   250: lload           7
        //   252: lload           9
        //   254: ladd           
        //   255: lstore          9
        //   257: lload           9
        //   259: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //   262: areturn        
        //   263: aconst_null    
        //   264: areturn        
        //   265: astore_0       
        //   266: goto            263
        //   269: astore_0       
        //   270: lload           7
        //   272: lstore          9
        //   274: goto            257
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  30     41     265    269    Ljava/lang/IllegalArgumentException;
        //  50     60     265    269    Ljava/lang/IllegalArgumentException;
        //  69     76     265    269    Ljava/lang/IllegalArgumentException;
        //  86     125    265    269    Ljava/lang/IllegalArgumentException;
        //  134    144    265    269    Ljava/lang/IllegalArgumentException;
        //  148    163    265    269    Ljava/lang/IllegalArgumentException;
        //  174    183    265    269    Ljava/lang/IllegalArgumentException;
        //  224    230    269    277    Ljava/lang/NumberFormatException;
        //  224    230    265    269    Ljava/lang/IllegalArgumentException;
        //  239    247    269    277    Ljava/lang/NumberFormatException;
        //  239    247    265    269    Ljava/lang/IllegalArgumentException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0230:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void parseTiffHeaders(final ByteOrderedDataInputStream byteOrderedDataInputStream, int i) throws IOException {
        byteOrderedDataInputStream.setByteOrder(this.mExifByteOrder = this.readByteOrder(byteOrderedDataInputStream));
        final int unsignedShort = byteOrderedDataInputStream.readUnsignedShort();
        final int mMimeType = this.mMimeType;
        if (mMimeType != 7 && mMimeType != 10 && unsignedShort != 42) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid start code: ");
            sb.append(Integer.toHexString(unsignedShort));
            throw new IOException(sb.toString());
        }
        final int int1 = byteOrderedDataInputStream.readInt();
        if (int1 < 8 || int1 >= i) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Invalid first Ifd offset: ");
            sb2.append(int1);
            throw new IOException(sb2.toString());
        }
        i = int1 - 8;
        if (i > 0 && byteOrderedDataInputStream.skipBytes(i) != i) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Couldn't jump to first Ifd: ");
            sb3.append(i);
            throw new IOException(sb3.toString());
        }
    }
    
    private void printAttributes() {
        for (int i = 0; i < this.mAttributes.length; ++i) {
            final StringBuilder sb = new StringBuilder();
            sb.append("The size of tag group[");
            sb.append(i);
            sb.append("]: ");
            sb.append(this.mAttributes[i].size());
            for (final Map.Entry<K, ExifAttribute> entry : this.mAttributes[i].entrySet()) {
                final ExifAttribute exifAttribute = entry.getValue();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("tagName: ");
                sb2.append((String)entry.getKey());
                sb2.append(", tagType: ");
                sb2.append(exifAttribute.toString());
                sb2.append(", tagValue: '");
                sb2.append(exifAttribute.getStringValue(this.mExifByteOrder));
                sb2.append("'");
            }
        }
    }
    
    private ByteOrder readByteOrder(final ByteOrderedDataInputStream byteOrderedDataInputStream) throws IOException {
        final short short1 = byteOrderedDataInputStream.readShort();
        if (short1 == 18761) {
            return ByteOrder.LITTLE_ENDIAN;
        }
        if (short1 == 19789) {
            return ByteOrder.BIG_ENDIAN;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid byte order: ");
        sb.append(Integer.toHexString(short1));
        throw new IOException(sb.toString());
    }
    
    private void readExifSegment(final byte[] array, final int n) throws IOException {
        final ByteOrderedDataInputStream byteOrderedDataInputStream = new ByteOrderedDataInputStream(array);
        this.parseTiffHeaders(byteOrderedDataInputStream, array.length);
        this.readImageFileDirectory(byteOrderedDataInputStream, n);
    }
    
    private void readImageFileDirectory(final ByteOrderedDataInputStream byteOrderedDataInputStream, int int1) throws IOException {
        this.mAttributesOffsets.add(byteOrderedDataInputStream.mPosition);
        if (byteOrderedDataInputStream.mPosition + 2 > byteOrderedDataInputStream.mLength) {
            return;
        }
        final short short1 = byteOrderedDataInputStream.readShort();
        if (ExifInterface.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("numberOfDirectoryEntry: ");
            sb.append(short1);
        }
        if (byteOrderedDataInputStream.mPosition + short1 * 12 <= byteOrderedDataInputStream.mLength) {
            if (short1 > 0) {
                for (short n = 0; n < short1; ++n) {
                    final int unsignedShort = byteOrderedDataInputStream.readUnsignedShort();
                    final int unsignedShort2 = byteOrderedDataInputStream.readUnsignedShort();
                    final int int2 = byteOrderedDataInputStream.readInt();
                    final long n2 = byteOrderedDataInputStream.peek() + 4L;
                    final ExifTag exifTag = ExifInterface.sExifTagMapsForReading[int1].get(unsignedShort);
                    final boolean debug = ExifInterface.DEBUG;
                    if (debug) {
                        String name;
                        if (exifTag != null) {
                            name = exifTag.name;
                        }
                        else {
                            name = null;
                        }
                        String.format("ifdType: %d, tagNumber: %d, tagName: %s, dataFormat: %d, numberOfComponents: %d", int1, unsignedShort, name, unsignedShort2, int2);
                    }
                    int primaryFormat = 0;
                    long lng = 0L;
                    int n3 = 0;
                    Label_0487: {
                        Label_0262: {
                            if (exifTag == null) {
                                if (debug) {
                                    final StringBuilder sb2 = new StringBuilder();
                                    sb2.append("Skip the tag entry since tag number is not defined: ");
                                    sb2.append(unsignedShort);
                                }
                            }
                            else {
                                if (unsignedShort2 > 0) {
                                    final int[] ifd_FORMAT_BYTES_PER_FORMAT = ExifInterface.IFD_FORMAT_BYTES_PER_FORMAT;
                                    if (unsignedShort2 < ifd_FORMAT_BYTES_PER_FORMAT.length) {
                                        if (!exifTag.isFormatCompatible(unsignedShort2)) {
                                            if (debug) {
                                                final StringBuilder sb3 = new StringBuilder();
                                                sb3.append("Skip the tag entry since data format (");
                                                sb3.append(ExifInterface.IFD_FORMAT_NAMES[unsignedShort2]);
                                                sb3.append(") is unexpected for tag: ");
                                                sb3.append(exifTag.name);
                                            }
                                            break Label_0262;
                                        }
                                        else {
                                            if ((primaryFormat = unsignedShort2) == 7) {
                                                primaryFormat = exifTag.primaryFormat;
                                            }
                                            lng = int2 * (long)ifd_FORMAT_BYTES_PER_FORMAT[primaryFormat];
                                            if (lng >= 0L && lng <= 2147483647L) {
                                                n3 = 1;
                                                break Label_0487;
                                            }
                                            if (debug) {
                                                final StringBuilder sb4 = new StringBuilder();
                                                sb4.append("Skip the tag entry since the number of components is invalid: ");
                                                sb4.append(int2);
                                            }
                                            n3 = 0;
                                            break Label_0487;
                                        }
                                    }
                                }
                                if (debug) {
                                    final StringBuilder sb5 = new StringBuilder();
                                    sb5.append("Skip the tag entry since data format is invalid: ");
                                    sb5.append(unsignedShort2);
                                }
                            }
                        }
                        final int n4 = 0;
                        lng = 0L;
                        primaryFormat = unsignedShort2;
                        n3 = n4;
                    }
                    if (n3 == 0) {
                        byteOrderedDataInputStream.seek(n2);
                    }
                    else {
                        if (lng > 4L) {
                            final int int3 = byteOrderedDataInputStream.readInt();
                            if (debug) {
                                final StringBuilder sb6 = new StringBuilder();
                                sb6.append("seek to data offset: ");
                                sb6.append(int3);
                            }
                            if (this.mMimeType == 7) {
                                if ("MakerNote".equals(exifTag.name)) {
                                    this.mOrfMakerNoteOffset = int3;
                                }
                                else if (int1 == 6 && "ThumbnailImage".equals(exifTag.name)) {
                                    this.mOrfThumbnailOffset = int3;
                                    this.mOrfThumbnailLength = int2;
                                    final ExifAttribute uShort = ExifAttribute.createUShort(6, this.mExifByteOrder);
                                    final ExifAttribute uLong = ExifAttribute.createULong(this.mOrfThumbnailOffset, this.mExifByteOrder);
                                    final ExifAttribute uLong2 = ExifAttribute.createULong(this.mOrfThumbnailLength, this.mExifByteOrder);
                                    this.mAttributes[4].put("Compression", uShort);
                                    this.mAttributes[4].put("JPEGInterchangeFormat", uLong);
                                    this.mAttributes[4].put("JPEGInterchangeFormatLength", uLong2);
                                }
                            }
                            final long n5 = int3;
                            if (n5 + lng > byteOrderedDataInputStream.mLength) {
                                if (debug) {
                                    final StringBuilder sb7 = new StringBuilder();
                                    sb7.append("Skip the tag entry since data offset is invalid: ");
                                    sb7.append(int3);
                                }
                                byteOrderedDataInputStream.seek(n2);
                                continue;
                            }
                            byteOrderedDataInputStream.seek(n5);
                        }
                        final Integer n6 = ExifInterface.sExifPointerTagMap.get(unsignedShort);
                        if (debug) {
                            final StringBuilder sb8 = new StringBuilder();
                            sb8.append("nextIfdType: ");
                            sb8.append(n6);
                            sb8.append(" byteCount: ");
                            sb8.append(lng);
                        }
                        if (n6 != null) {
                            long unsignedInt = 0L;
                            Label_0908: {
                                int n7;
                                if (primaryFormat != 3) {
                                    if (primaryFormat == 4) {
                                        unsignedInt = byteOrderedDataInputStream.readUnsignedInt();
                                        break Label_0908;
                                    }
                                    if (primaryFormat != 8) {
                                        if (primaryFormat != 9 && primaryFormat != 13) {
                                            unsignedInt = -1L;
                                            break Label_0908;
                                        }
                                        n7 = byteOrderedDataInputStream.readInt();
                                    }
                                    else {
                                        n7 = byteOrderedDataInputStream.readShort();
                                    }
                                }
                                else {
                                    n7 = byteOrderedDataInputStream.readUnsignedShort();
                                }
                                unsignedInt = n7;
                            }
                            if (debug) {
                                String.format("Offset: %d, tagName: %s", unsignedInt, exifTag.name);
                            }
                            if (unsignedInt > 0L && unsignedInt < byteOrderedDataInputStream.mLength) {
                                if (!this.mAttributesOffsets.contains((int)unsignedInt)) {
                                    byteOrderedDataInputStream.seek(unsignedInt);
                                    this.readImageFileDirectory(byteOrderedDataInputStream, n6);
                                }
                                else if (debug) {
                                    final StringBuilder sb9 = new StringBuilder();
                                    sb9.append("Skip jump into the IFD since it has already been read: IfdType ");
                                    sb9.append(n6);
                                    sb9.append(" (at ");
                                    sb9.append(unsignedInt);
                                    sb9.append(")");
                                }
                            }
                            else if (debug) {
                                final StringBuilder sb10 = new StringBuilder();
                                sb10.append("Skip jump into the IFD since its offset is invalid: ");
                                sb10.append(unsignedInt);
                            }
                            byteOrderedDataInputStream.seek(n2);
                        }
                        else {
                            final int peek = byteOrderedDataInputStream.peek();
                            final int mOffsetToExifData = this.mOffsetToExifData;
                            final byte[] array = new byte[(int)lng];
                            byteOrderedDataInputStream.readFully(array);
                            final ExifAttribute value = new ExifAttribute(primaryFormat, int2, peek + mOffsetToExifData, array);
                            this.mAttributes[int1].put(exifTag.name, value);
                            if ("DNGVersion".equals(exifTag.name)) {
                                this.mMimeType = 3;
                            }
                            if ((("Make".equals(exifTag.name) || "Model".equals(exifTag.name)) && value.getStringValue(this.mExifByteOrder).contains("PENTAX")) || ("Compression".equals(exifTag.name) && value.getIntValue(this.mExifByteOrder) == 65535)) {
                                this.mMimeType = 8;
                            }
                            if (byteOrderedDataInputStream.peek() != n2) {
                                byteOrderedDataInputStream.seek(n2);
                            }
                        }
                    }
                }
                if (byteOrderedDataInputStream.peek() + 4 <= byteOrderedDataInputStream.mLength) {
                    int1 = byteOrderedDataInputStream.readInt();
                    final boolean debug2 = ExifInterface.DEBUG;
                    if (debug2) {
                        String.format("nextIfdOffset: %d", int1);
                    }
                    final long n8 = int1;
                    if (n8 > 0L && int1 < byteOrderedDataInputStream.mLength) {
                        if (!this.mAttributesOffsets.contains(int1)) {
                            byteOrderedDataInputStream.seek(n8);
                            if (this.mAttributes[4].isEmpty()) {
                                this.readImageFileDirectory(byteOrderedDataInputStream, 4);
                            }
                            else if (this.mAttributes[5].isEmpty()) {
                                this.readImageFileDirectory(byteOrderedDataInputStream, 5);
                            }
                        }
                        else if (debug2) {
                            final StringBuilder sb11 = new StringBuilder();
                            sb11.append("Stop reading file since re-reading an IFD may cause an infinite loop: ");
                            sb11.append(int1);
                        }
                    }
                    else if (debug2) {
                        final StringBuilder sb12 = new StringBuilder();
                        sb12.append("Stop reading file since a wrong offset may cause an infinite loop: ");
                        sb12.append(int1);
                    }
                }
            }
        }
    }
    
    private void removeAttribute(final String key) {
        for (int i = 0; i < ExifInterface.EXIF_TAGS.length; ++i) {
            this.mAttributes[i].remove(key);
        }
    }
    
    private void retrieveJpegImageSize(final ByteOrderedDataInputStream byteOrderedDataInputStream, final int n) throws IOException {
        final ExifAttribute exifAttribute = this.mAttributes[n].get("ImageLength");
        final ExifAttribute exifAttribute2 = this.mAttributes[n].get("ImageWidth");
        if (exifAttribute == null || exifAttribute2 == null) {
            final ExifAttribute exifAttribute3 = this.mAttributes[n].get("JPEGInterchangeFormat");
            final ExifAttribute exifAttribute4 = this.mAttributes[n].get("JPEGInterchangeFormatLength");
            if (exifAttribute3 != null && exifAttribute4 != null) {
                final int intValue = exifAttribute3.getIntValue(this.mExifByteOrder);
                final int intValue2 = exifAttribute3.getIntValue(this.mExifByteOrder);
                byteOrderedDataInputStream.seek(intValue);
                final byte[] b = new byte[intValue2];
                byteOrderedDataInputStream.read(b);
                this.getJpegAttributes(new ByteOrderedDataInputStream(b), intValue, n);
            }
        }
    }
    
    private void saveJpegAttributes(final InputStream inputStream, final OutputStream obj) throws IOException {
        if (ExifInterface.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("saveJpegAttributes starting with (inputStream: ");
            sb.append(inputStream);
            sb.append(", outputStream: ");
            sb.append(obj);
            sb.append(")");
        }
        final DataInputStream dataInputStream = new DataInputStream(inputStream);
        final ByteOrderedDataOutputStream byteOrderedDataOutputStream = new ByteOrderedDataOutputStream(obj, ByteOrder.BIG_ENDIAN);
        if (dataInputStream.readByte() != -1) {
            throw new IOException("Invalid marker");
        }
        byteOrderedDataOutputStream.writeByte(-1);
        if (dataInputStream.readByte() == -40) {
            byteOrderedDataOutputStream.writeByte(-40);
            ExifAttribute value;
            if (this.getAttribute("Xmp") != null && this.mXmpIsFromSeparateMarker) {
                value = this.mAttributes[0].remove("Xmp");
            }
            else {
                value = null;
            }
            byteOrderedDataOutputStream.writeByte(-1);
            byteOrderedDataOutputStream.writeByte(-31);
            this.writeExifSegment(byteOrderedDataOutputStream);
            if (value != null) {
                this.mAttributes[0].put("Xmp", value);
            }
            final byte[] array = new byte[4096];
            while (dataInputStream.readByte() == -1) {
                final byte byte1 = dataInputStream.readByte();
                if (byte1 == -39 || byte1 == -38) {
                    byteOrderedDataOutputStream.writeByte(-1);
                    byteOrderedDataOutputStream.writeByte(byte1);
                    copy(dataInputStream, byteOrderedDataOutputStream);
                    return;
                }
                if (byte1 != -31) {
                    byteOrderedDataOutputStream.writeByte(-1);
                    byteOrderedDataOutputStream.writeByte(byte1);
                    int i = dataInputStream.readUnsignedShort();
                    byteOrderedDataOutputStream.writeUnsignedShort(i);
                    i -= 2;
                    if (i < 0) {
                        throw new IOException("Invalid length");
                    }
                    while (i > 0) {
                        final int read = dataInputStream.read(array, 0, Math.min(i, 4096));
                        if (read < 0) {
                            break;
                        }
                        byteOrderedDataOutputStream.write(array, 0, read);
                        i -= read;
                    }
                }
                else {
                    final int n = dataInputStream.readUnsignedShort() - 2;
                    if (n < 0) {
                        throw new IOException("Invalid length");
                    }
                    final byte[] array2 = new byte[6];
                    if (n >= 6) {
                        if (dataInputStream.read(array2) != 6) {
                            throw new IOException("Invalid exif");
                        }
                        if (Arrays.equals(array2, ExifInterface.IDENTIFIER_EXIF_APP1)) {
                            final int n2 = n - 6;
                            if (dataInputStream.skipBytes(n2) == n2) {
                                continue;
                            }
                            throw new IOException("Invalid length");
                        }
                    }
                    byteOrderedDataOutputStream.writeByte(-1);
                    byteOrderedDataOutputStream.writeByte(byte1);
                    byteOrderedDataOutputStream.writeUnsignedShort(n + 2);
                    int j;
                    if ((j = n) >= 6) {
                        j = n - 6;
                        byteOrderedDataOutputStream.write(array2);
                    }
                    while (j > 0) {
                        final int read2 = dataInputStream.read(array, 0, Math.min(j, 4096));
                        if (read2 < 0) {
                            break;
                        }
                        byteOrderedDataOutputStream.write(array, 0, read2);
                        j -= read2;
                    }
                }
            }
            throw new IOException("Invalid marker");
        }
        throw new IOException("Invalid marker");
    }
    
    private void savePngAttributes(InputStream inputStream, OutputStream obj) throws IOException {
        if (ExifInterface.DEBUG) {
            final StringBuilder sb = new StringBuilder();
            sb.append("savePngAttributes starting with (inputStream: ");
            sb.append(inputStream);
            sb.append(", outputStream: ");
            sb.append(obj);
            sb.append(")");
        }
        final DataInputStream dataInputStream = new DataInputStream(inputStream);
        final ByteOrder big_ENDIAN = ByteOrder.BIG_ENDIAN;
        final ByteOrderedDataOutputStream byteOrderedDataOutputStream = new ByteOrderedDataOutputStream(obj, big_ENDIAN);
        final byte[] png_SIGNATURE = ExifInterface.PNG_SIGNATURE;
        copy(dataInputStream, byteOrderedDataOutputStream, png_SIGNATURE.length);
        final int mOffsetToExifData = this.mOffsetToExifData;
        if (mOffsetToExifData == 0) {
            final int int1 = dataInputStream.readInt();
            byteOrderedDataOutputStream.writeInt(int1);
            copy(dataInputStream, byteOrderedDataOutputStream, int1 + 4 + 4);
        }
        else {
            copy(dataInputStream, byteOrderedDataOutputStream, mOffsetToExifData - png_SIGNATURE.length - 4 - 4);
            dataInputStream.skipBytes(dataInputStream.readInt() + 4 + 4);
        }
        inputStream = null;
        Throwable t;
        try {
            obj = new ByteArrayOutputStream();
            try {
                final ByteOrderedDataOutputStream byteOrderedDataOutputStream2 = new ByteOrderedDataOutputStream(obj, big_ENDIAN);
                this.writeExifSegment(byteOrderedDataOutputStream2);
                final byte[] byteArray = ((ByteArrayOutputStream)byteOrderedDataOutputStream2.mOutputStream).toByteArray();
                byteOrderedDataOutputStream.write(byteArray);
                final CRC32 crc32 = new CRC32();
                crc32.update(byteArray, 4, byteArray.length - 4);
                byteOrderedDataOutputStream.writeInt((int)crc32.getValue());
                closeQuietly(obj);
                copy(dataInputStream, byteOrderedDataOutputStream);
                return;
            }
            finally {}
        }
        finally {
            final Throwable t2;
            t = t2;
        }
        closeQuietly(inputStream);
        throw t;
    }
    
    private void saveWebpAttributes(final InputStream obj, OutputStream obj2) throws IOException {
        Object cause = null;
        if (ExifInterface.DEBUG) {
            cause = new StringBuilder();
            ((StringBuilder)cause).append("saveWebpAttributes starting with (inputStream: ");
            ((StringBuilder)cause).append(obj);
            ((StringBuilder)cause).append(", outputStream: ");
            ((StringBuilder)cause).append(obj2);
            ((StringBuilder)cause).append(")");
        }
        final ByteOrder little_ENDIAN = ByteOrder.LITTLE_ENDIAN;
        final ByteOrderedDataInputStream byteOrderedDataInputStream = new ByteOrderedDataInputStream(obj, little_ENDIAN);
        final ByteOrderedDataOutputStream out = new ByteOrderedDataOutputStream(obj2, little_ENDIAN);
        final byte[] webp_SIGNATURE_1 = ExifInterface.WEBP_SIGNATURE_1;
        copy(byteOrderedDataInputStream, out, webp_SIGNATURE_1.length);
        final byte[] webp_SIGNATURE_2 = ExifInterface.WEBP_SIGNATURE_2;
        byteOrderedDataInputStream.skipBytes(webp_SIGNATURE_2.length + 4);
        byte[] a = null;
        final OutputStream outputStream = obj2 = null;
        try {
            try {
                cause = new(java.io.ByteArrayOutputStream.class)();
                obj2 = outputStream;
                new ByteArrayOutputStream();
                try {
                    obj2 = new ByteOrderedDataOutputStream((OutputStream)cause, little_ENDIAN);
                    final int mOffsetToExifData = this.mOffsetToExifData;
                    if (mOffsetToExifData != 0) {
                        copy(byteOrderedDataInputStream, obj2, mOffsetToExifData - (webp_SIGNATURE_1.length + 4 + webp_SIGNATURE_2.length) - 4 - 4);
                        byteOrderedDataInputStream.skipBytes(4);
                        byteOrderedDataInputStream.skipBytes(byteOrderedDataInputStream.readInt());
                        this.writeExifSegment((ByteOrderedDataOutputStream)obj2);
                    }
                    else {
                        a = new byte[4];
                        if (byteOrderedDataInputStream.read(a) != 4) {
                            throw new IOException("Encountered invalid length while parsing WebP chunk type");
                        }
                        final byte[] webp_CHUNK_TYPE_VP8X = ExifInterface.WEBP_CHUNK_TYPE_VP8X;
                        final boolean equals = Arrays.equals(a, webp_CHUNK_TYPE_VP8X);
                        final int n = 0;
                        if (equals) {
                            final int int1 = byteOrderedDataInputStream.readInt();
                            int n2;
                            if (int1 % 2 == 1) {
                                n2 = int1 + 1;
                            }
                            else {
                                n2 = int1;
                            }
                            a = new byte[n2];
                            byteOrderedDataInputStream.read(a);
                            final byte b = (byte)(0x8 | a[0]);
                            a[0] = b;
                            int n3 = n;
                            if ((b >> 1 & 0x1) == 0x1) {
                                n3 = 1;
                            }
                            ((ByteOrderedDataOutputStream)obj2).write(webp_CHUNK_TYPE_VP8X);
                            ((ByteOrderedDataOutputStream)obj2).writeInt(int1);
                            ((ByteOrderedDataOutputStream)obj2).write(a);
                            if (n3 != 0) {
                                this.copyChunksUpToGivenChunkType(byteOrderedDataInputStream, (ByteOrderedDataOutputStream)obj2, ExifInterface.WEBP_CHUNK_TYPE_ANIM, null);
                                while (true) {
                                    a = new byte[4];
                                    obj.read(a);
                                    if (!Arrays.equals(a, ExifInterface.WEBP_CHUNK_TYPE_ANMF)) {
                                        break;
                                    }
                                    this.copyWebPChunk(byteOrderedDataInputStream, (ByteOrderedDataOutputStream)obj2, a);
                                }
                                this.writeExifSegment((ByteOrderedDataOutputStream)obj2);
                            }
                            else {
                                this.copyChunksUpToGivenChunkType(byteOrderedDataInputStream, (ByteOrderedDataOutputStream)obj2, ExifInterface.WEBP_CHUNK_TYPE_VP8, ExifInterface.WEBP_CHUNK_TYPE_VP8L);
                                this.writeExifSegment((ByteOrderedDataOutputStream)obj2);
                            }
                        }
                        else {
                            final byte[] webp_CHUNK_TYPE_VP8 = ExifInterface.WEBP_CHUNK_TYPE_VP8;
                            if (Arrays.equals(a, webp_CHUNK_TYPE_VP8) || Arrays.equals(a, ExifInterface.WEBP_CHUNK_TYPE_VP8L)) {
                                final int int2 = byteOrderedDataInputStream.readInt();
                                int n4;
                                if (int2 % 2 == 1) {
                                    n4 = int2 + 1;
                                }
                                else {
                                    n4 = int2;
                                }
                                final byte[] b2 = new byte[3];
                                int n5 = 0;
                                int n6 = 0;
                                int n7 = 0;
                                int n8 = 0;
                                Label_0656: {
                                    if (Arrays.equals(a, webp_CHUNK_TYPE_VP8)) {
                                        byteOrderedDataInputStream.read(b2);
                                        final byte[] array = new byte[3];
                                        if (byteOrderedDataInputStream.read(array) != 3 || !Arrays.equals(ExifInterface.WEBP_VP8_SIGNATURE, array)) {
                                            throw new IOException("Encountered error while checking VP8 signature");
                                        }
                                        n5 = byteOrderedDataInputStream.readInt();
                                        n6 = n5 << 18 >> 18;
                                        n7 = n5 << 2 >> 18;
                                        n4 -= 10;
                                    }
                                    else if (Arrays.equals(a, ExifInterface.WEBP_CHUNK_TYPE_VP8L)) {
                                        if (byteOrderedDataInputStream.readByte() == 47) {
                                            n5 = byteOrderedDataInputStream.readInt();
                                            n8 = (n5 & 0x8);
                                            n4 -= 5;
                                            n7 = (n5 << 4 >> 18) + 1;
                                            n6 = (n5 << 18 >> 18) + 1;
                                            break Label_0656;
                                        }
                                        throw new IOException("Encountered error while checking VP8L signature");
                                    }
                                    else {
                                        n5 = 0;
                                        n6 = 0;
                                        n7 = 0;
                                    }
                                    n8 = 0;
                                }
                                ((ByteOrderedDataOutputStream)obj2).write(webp_CHUNK_TYPE_VP8X);
                                ((ByteOrderedDataOutputStream)obj2).writeInt(10);
                                final byte[] array2 = new byte[10];
                                final byte b3 = (byte)(array2[0] | 0x8);
                                array2[0] = b3;
                                array2[0] = (byte)(b3 | n8 << 4);
                                --n6;
                                --n7;
                                array2[4] = (byte)n6;
                                array2[5] = (byte)(n6 >> 8);
                                array2[6] = (byte)(n6 >> 16);
                                array2[7] = (byte)n7;
                                array2[8] = (byte)(n7 >> 8);
                                array2[9] = (byte)(n7 >> 16);
                                ((ByteOrderedDataOutputStream)obj2).write(array2);
                                ((ByteOrderedDataOutputStream)obj2).write(a);
                                ((ByteOrderedDataOutputStream)obj2).writeInt(int2);
                                if (Arrays.equals(a, webp_CHUNK_TYPE_VP8)) {
                                    ((ByteOrderedDataOutputStream)obj2).write(b2);
                                    ((ByteOrderedDataOutputStream)obj2).write(ExifInterface.WEBP_VP8_SIGNATURE);
                                    ((ByteOrderedDataOutputStream)obj2).writeInt(n5);
                                }
                                else if (Arrays.equals(a, ExifInterface.WEBP_CHUNK_TYPE_VP8L)) {
                                    obj2.write(47);
                                    ((ByteOrderedDataOutputStream)obj2).writeInt(n5);
                                }
                                copy(byteOrderedDataInputStream, obj2, n4);
                                this.writeExifSegment((ByteOrderedDataOutputStream)obj2);
                            }
                        }
                    }
                    copy(byteOrderedDataInputStream, obj2);
                    final int size = ((ByteArrayOutputStream)cause).size();
                    final byte[] webp_SIGNATURE_3 = ExifInterface.WEBP_SIGNATURE_2;
                    out.writeInt(size + webp_SIGNATURE_3.length);
                    out.write(webp_SIGNATURE_3);
                    ((ByteArrayOutputStream)cause).writeTo(out);
                    closeQuietly((Closeable)cause);
                    return;
                }
                catch (final Exception ex) {
                    obj2 = (OutputStream)cause;
                    cause = ex;
                }
                finally {
                    obj2 = (OutputStream)cause;
                }
            }
            finally {}
        }
        catch (final Exception cause) {}
        throw new IOException("Failed to save WebP file", (Throwable)cause);
        closeQuietly(obj2);
    }
    
    private void setThumbnailData(final ByteOrderedDataInputStream byteOrderedDataInputStream) throws IOException {
        final HashMap<String, ExifAttribute> hashMap = this.mAttributes[4];
        final ExifAttribute exifAttribute = hashMap.get("Compression");
        if (exifAttribute != null) {
            final int intValue = exifAttribute.getIntValue(this.mExifByteOrder);
            if ((this.mThumbnailCompression = intValue) != 1) {
                if (intValue == 6) {
                    this.handleThumbnailFromJfif(byteOrderedDataInputStream, hashMap);
                    return;
                }
                if (intValue != 7) {
                    return;
                }
            }
            if (this.isSupportedDataType(hashMap)) {
                this.handleThumbnailFromStrips(byteOrderedDataInputStream, hashMap);
            }
        }
        else {
            this.mThumbnailCompression = 6;
            this.handleThumbnailFromJfif(byteOrderedDataInputStream, hashMap);
        }
    }
    
    private static boolean startsWith(final byte[] array, final byte[] array2) {
        if (array == null || array2 == null) {
            return false;
        }
        if (array.length < array2.length) {
            return false;
        }
        for (int i = 0; i < array2.length; ++i) {
            if (array[i] != array2[i]) {
                return false;
            }
        }
        return true;
    }
    
    private void swapBasedOnImageSize(final int n, final int n2) throws IOException {
        if (!this.mAttributes[n].isEmpty()) {
            if (!this.mAttributes[n2].isEmpty()) {
                final ExifAttribute exifAttribute = this.mAttributes[n].get("ImageLength");
                final ExifAttribute exifAttribute2 = this.mAttributes[n].get("ImageWidth");
                final ExifAttribute exifAttribute3 = this.mAttributes[n2].get("ImageLength");
                final ExifAttribute exifAttribute4 = this.mAttributes[n2].get("ImageWidth");
                if (exifAttribute != null) {
                    if (exifAttribute2 != null) {
                        if (exifAttribute3 != null) {
                            if (exifAttribute4 != null) {
                                final int intValue = exifAttribute.getIntValue(this.mExifByteOrder);
                                final int intValue2 = exifAttribute2.getIntValue(this.mExifByteOrder);
                                final int intValue3 = exifAttribute3.getIntValue(this.mExifByteOrder);
                                final int intValue4 = exifAttribute4.getIntValue(this.mExifByteOrder);
                                if (intValue < intValue3 && intValue2 < intValue4) {
                                    final HashMap<String, ExifAttribute>[] mAttributes = this.mAttributes;
                                    final HashMap<String, ExifAttribute> hashMap = mAttributes[n];
                                    mAttributes[n] = mAttributes[n2];
                                    mAttributes[n2] = hashMap;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    private void updateImageSizeValues(final ByteOrderedDataInputStream byteOrderedDataInputStream, final int n) throws IOException {
        final ExifAttribute exifAttribute = this.mAttributes[n].get("DefaultCropSize");
        final ExifAttribute exifAttribute2 = this.mAttributes[n].get("SensorTopBorder");
        final ExifAttribute exifAttribute3 = this.mAttributes[n].get("SensorLeftBorder");
        final ExifAttribute exifAttribute4 = this.mAttributes[n].get("SensorBottomBorder");
        final ExifAttribute exifAttribute5 = this.mAttributes[n].get("SensorRightBorder");
        if (exifAttribute != null) {
            ExifAttribute value;
            ExifAttribute value2;
            if (exifAttribute.format == 5) {
                final Rational[] a = (Rational[])exifAttribute.getValue(this.mExifByteOrder);
                if (a == null || a.length != 2) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Invalid crop size values. cropSize=");
                    sb.append(Arrays.toString(a));
                    return;
                }
                value = ExifAttribute.createURational(a[0], this.mExifByteOrder);
                value2 = ExifAttribute.createURational(a[1], this.mExifByteOrder);
            }
            else {
                final int[] a2 = (int[])exifAttribute.getValue(this.mExifByteOrder);
                if (a2 == null || a2.length != 2) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Invalid crop size values. cropSize=");
                    sb2.append(Arrays.toString(a2));
                    return;
                }
                value = ExifAttribute.createUShort(a2[0], this.mExifByteOrder);
                value2 = ExifAttribute.createUShort(a2[1], this.mExifByteOrder);
            }
            this.mAttributes[n].put("ImageWidth", value);
            this.mAttributes[n].put("ImageLength", value2);
        }
        else if (exifAttribute2 != null && exifAttribute3 != null && exifAttribute4 != null && exifAttribute5 != null) {
            final int intValue = exifAttribute2.getIntValue(this.mExifByteOrder);
            final int intValue2 = exifAttribute4.getIntValue(this.mExifByteOrder);
            final int intValue3 = exifAttribute5.getIntValue(this.mExifByteOrder);
            final int intValue4 = exifAttribute3.getIntValue(this.mExifByteOrder);
            if (intValue2 > intValue && intValue3 > intValue4) {
                final ExifAttribute uShort = ExifAttribute.createUShort(intValue2 - intValue, this.mExifByteOrder);
                final ExifAttribute uShort2 = ExifAttribute.createUShort(intValue3 - intValue4, this.mExifByteOrder);
                this.mAttributes[n].put("ImageLength", uShort);
                this.mAttributes[n].put("ImageWidth", uShort2);
            }
        }
        else {
            this.retrieveJpegImageSize(byteOrderedDataInputStream, n);
        }
    }
    
    private void validateImages() throws IOException {
        this.swapBasedOnImageSize(0, 5);
        this.swapBasedOnImageSize(0, 4);
        this.swapBasedOnImageSize(5, 4);
        final ExifAttribute value = this.mAttributes[1].get("PixelXDimension");
        final ExifAttribute value2 = this.mAttributes[1].get("PixelYDimension");
        if (value != null && value2 != null) {
            this.mAttributes[0].put("ImageWidth", value);
            this.mAttributes[0].put("ImageLength", value2);
        }
        if (this.mAttributes[4].isEmpty() && this.isThumbnail(this.mAttributes[5])) {
            final HashMap<String, ExifAttribute>[] mAttributes = this.mAttributes;
            mAttributes[4] = mAttributes[5];
            mAttributes[5] = new HashMap<String, ExifAttribute>();
        }
        this.isThumbnail(this.mAttributes[4]);
    }
    
    private int writeExifSegment(final ByteOrderedDataOutputStream byteOrderedDataOutputStream) throws IOException {
        final ExifTag[][] exif_TAGS = ExifInterface.EXIF_TAGS;
        final int[] array = new int[exif_TAGS.length];
        final int[] array2 = new int[exif_TAGS.length];
        final ExifTag[] exif_POINTER_TAGS = ExifInterface.EXIF_POINTER_TAGS;
        for (int length = exif_POINTER_TAGS.length, i = 0; i < length; ++i) {
            this.removeAttribute(exif_POINTER_TAGS[i].name);
        }
        this.removeAttribute(ExifInterface.JPEG_INTERCHANGE_FORMAT_TAG.name);
        this.removeAttribute(ExifInterface.JPEG_INTERCHANGE_FORMAT_LENGTH_TAG.name);
        for (int j = 0; j < ExifInterface.EXIF_TAGS.length; ++j) {
            final Object[] array3 = this.mAttributes[j].entrySet().toArray();
            for (int length2 = array3.length, k = 0; k < length2; ++k) {
                final Map.Entry entry = (Map.Entry)array3[k];
                if (entry.getValue() == null) {
                    this.mAttributes[j].remove(entry.getKey());
                }
            }
        }
        if (!this.mAttributes[1].isEmpty()) {
            this.mAttributes[0].put(ExifInterface.EXIF_POINTER_TAGS[1].name, ExifAttribute.createULong(0L, this.mExifByteOrder));
        }
        if (!this.mAttributes[2].isEmpty()) {
            this.mAttributes[0].put(ExifInterface.EXIF_POINTER_TAGS[2].name, ExifAttribute.createULong(0L, this.mExifByteOrder));
        }
        if (!this.mAttributes[3].isEmpty()) {
            this.mAttributes[1].put(ExifInterface.EXIF_POINTER_TAGS[3].name, ExifAttribute.createULong(0L, this.mExifByteOrder));
        }
        if (this.mHasThumbnail) {
            this.mAttributes[4].put(ExifInterface.JPEG_INTERCHANGE_FORMAT_TAG.name, ExifAttribute.createULong(0L, this.mExifByteOrder));
            this.mAttributes[4].put(ExifInterface.JPEG_INTERCHANGE_FORMAT_LENGTH_TAG.name, ExifAttribute.createULong(this.mThumbnailLength, this.mExifByteOrder));
        }
        for (int l = 0; l < ExifInterface.EXIF_TAGS.length; ++l) {
            final Iterator<Map.Entry<String, ExifAttribute>> iterator = this.mAttributes[l].entrySet().iterator();
            int n = 0;
            while (iterator.hasNext()) {
                final int size = ((Map.Entry<K, ExifAttribute>)iterator.next()).getValue().size();
                if (size > 4) {
                    n += size;
                }
            }
            array2[l] += n;
        }
        int mThumbnailOffset = 8;
        int n3;
        for (int n2 = 0; n2 < ExifInterface.EXIF_TAGS.length; ++n2, mThumbnailOffset = n3) {
            n3 = mThumbnailOffset;
            if (!this.mAttributes[n2].isEmpty()) {
                array[n2] = mThumbnailOffset;
                n3 = mThumbnailOffset + (this.mAttributes[n2].size() * 12 + 2 + 4 + array2[n2]);
            }
        }
        int n4 = mThumbnailOffset;
        if (this.mHasThumbnail) {
            this.mAttributes[4].put(ExifInterface.JPEG_INTERCHANGE_FORMAT_TAG.name, ExifAttribute.createULong(mThumbnailOffset, this.mExifByteOrder));
            this.mThumbnailOffset = mThumbnailOffset;
            n4 = mThumbnailOffset + this.mThumbnailLength;
        }
        int m = n4;
        if (this.mMimeType == 4) {
            m = n4 + 8;
        }
        if (ExifInterface.DEBUG) {
            for (int i2 = 0; i2 < ExifInterface.EXIF_TAGS.length; ++i2) {
                String.format("index: %d, offsets: %d, tag count: %d, data sizes: %d, total size: %d", i2, array[i2], this.mAttributes[i2].size(), array2[i2], m);
            }
        }
        if (!this.mAttributes[1].isEmpty()) {
            this.mAttributes[0].put(ExifInterface.EXIF_POINTER_TAGS[1].name, ExifAttribute.createULong(array[1], this.mExifByteOrder));
        }
        if (!this.mAttributes[2].isEmpty()) {
            this.mAttributes[0].put(ExifInterface.EXIF_POINTER_TAGS[2].name, ExifAttribute.createULong(array[2], this.mExifByteOrder));
        }
        if (!this.mAttributes[3].isEmpty()) {
            this.mAttributes[1].put(ExifInterface.EXIF_POINTER_TAGS[3].name, ExifAttribute.createULong(array[3], this.mExifByteOrder));
        }
        final int mMimeType = this.mMimeType;
        if (mMimeType != 4) {
            if (mMimeType != 13) {
                if (mMimeType == 14) {
                    byteOrderedDataOutputStream.write(ExifInterface.WEBP_CHUNK_TYPE_EXIF);
                    byteOrderedDataOutputStream.writeInt(m);
                }
            }
            else {
                byteOrderedDataOutputStream.writeInt(m);
                byteOrderedDataOutputStream.write(ExifInterface.PNG_CHUNK_TYPE_EXIF);
            }
        }
        else {
            byteOrderedDataOutputStream.writeUnsignedShort(m);
            byteOrderedDataOutputStream.write(ExifInterface.IDENTIFIER_EXIF_APP1);
        }
        short n5;
        if (this.mExifByteOrder == ByteOrder.BIG_ENDIAN) {
            n5 = 19789;
        }
        else {
            n5 = 18761;
        }
        byteOrderedDataOutputStream.writeShort(n5);
        byteOrderedDataOutputStream.setByteOrder(this.mExifByteOrder);
        byteOrderedDataOutputStream.writeUnsignedShort(42);
        byteOrderedDataOutputStream.writeUnsignedInt(8L);
        for (int n6 = 0; n6 < ExifInterface.EXIF_TAGS.length; ++n6) {
            if (!this.mAttributes[n6].isEmpty()) {
                byteOrderedDataOutputStream.writeUnsignedShort(this.mAttributes[n6].size());
                int n7 = array[n6] + 2 + this.mAttributes[n6].size() * 12 + 4;
                for (final Map.Entry<Object, V> entry2 : this.mAttributes[n6].entrySet()) {
                    final int number = ExifInterface.sExifTagMapsForWriting[n6].get(entry2.getKey()).number;
                    final ExifAttribute exifAttribute = (ExifAttribute)entry2.getValue();
                    int size2 = exifAttribute.size();
                    byteOrderedDataOutputStream.writeUnsignedShort(number);
                    byteOrderedDataOutputStream.writeUnsignedShort(exifAttribute.format);
                    byteOrderedDataOutputStream.writeInt(exifAttribute.numberOfComponents);
                    if (size2 > 4) {
                        byteOrderedDataOutputStream.writeUnsignedInt(n7);
                        n7 += size2;
                    }
                    else {
                        byteOrderedDataOutputStream.write(exifAttribute.bytes);
                        if (size2 >= 4) {
                            continue;
                        }
                        while (size2 < 4) {
                            byteOrderedDataOutputStream.writeByte(0);
                            ++size2;
                        }
                    }
                }
                if (n6 == 0 && !this.mAttributes[4].isEmpty()) {
                    byteOrderedDataOutputStream.writeUnsignedInt(array[4]);
                }
                else {
                    byteOrderedDataOutputStream.writeUnsignedInt(0L);
                }
                final Iterator<Map.Entry<String, ExifAttribute>> iterator3 = this.mAttributes[n6].entrySet().iterator();
                while (iterator3.hasNext()) {
                    final byte[] bytes = ((Map.Entry<K, ExifAttribute>)iterator3.next()).getValue().bytes;
                    if (bytes.length > 4) {
                        byteOrderedDataOutputStream.write(bytes, 0, bytes.length);
                    }
                }
            }
        }
        if (this.mHasThumbnail) {
            byteOrderedDataOutputStream.write(this.getThumbnailBytes());
        }
        if (this.mMimeType == 14 && m % 2 == 1) {
            byteOrderedDataOutputStream.writeByte(0);
        }
        byteOrderedDataOutputStream.setByteOrder(ByteOrder.BIG_ENDIAN);
        return m;
    }
    
    public void flipHorizontally() {
        int i = 1;
        while (true) {
            switch (this.getAttributeInt("Orientation", 1)) {
                default: {
                    i = 0;
                }
                case 2: {
                    this.setAttribute("Orientation", Integer.toString(i));
                    return;
                }
                case 8: {
                    i = 7;
                    continue;
                }
                case 7: {
                    i = 8;
                    continue;
                }
                case 6: {
                    i = 5;
                    continue;
                }
                case 5: {
                    i = 6;
                    continue;
                }
                case 4: {
                    i = 3;
                    continue;
                }
                case 3: {
                    i = 4;
                    continue;
                }
                case 1: {
                    i = 2;
                    continue;
                }
            }
            break;
        }
    }
    
    public void flipVertically() {
        int i = 1;
        while (true) {
            switch (this.getAttributeInt("Orientation", 1)) {
                default: {
                    i = 0;
                }
                case 4: {
                    this.setAttribute("Orientation", Integer.toString(i));
                    return;
                }
                case 8: {
                    i = 5;
                    continue;
                }
                case 7: {
                    i = 6;
                    continue;
                }
                case 6: {
                    i = 7;
                    continue;
                }
                case 5: {
                    i = 8;
                    continue;
                }
                case 3: {
                    i = 2;
                    continue;
                }
                case 2: {
                    i = 3;
                    continue;
                }
                case 1: {
                    i = 4;
                    continue;
                }
            }
            break;
        }
    }
    
    public double getAltitude(final double n) {
        final double attributeDouble = this.getAttributeDouble("GPSAltitude", -1.0);
        int n2 = -1;
        final int attributeInt = this.getAttributeInt("GPSAltitudeRef", -1);
        if (attributeDouble >= 0.0 && attributeInt >= 0) {
            if (attributeInt != 1) {
                n2 = 1;
            }
            return attributeDouble * n2;
        }
        return n;
    }
    
    @Nullable
    public String getAttribute(@NonNull String string) {
        Label_0251: {
            if (string == null) {
                break Label_0251;
            }
            final ExifAttribute exifAttribute = this.getExifAttribute(string);
            Label_0249: {
                if (exifAttribute == null) {
                    break Label_0249;
                }
                if (!ExifInterface.sTagSetForCompatibility.contains(string)) {
                    return exifAttribute.getStringValue(this.mExifByteOrder);
                }
                if (string.equals("GPSTimeStamp")) {
                    final int format = exifAttribute.format;
                    if (format != 5 && format != 10) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("GPS Timestamp format is not rational. format=");
                        sb.append(exifAttribute.format);
                        return null;
                    }
                    final Rational[] a = (Rational[])exifAttribute.getValue(this.mExifByteOrder);
                    if (a != null && a.length == 3) {
                        final Rational rational = a[0];
                        final int i = (int)(rational.numerator / (float)rational.denominator);
                        final Rational rational2 = a[1];
                        final int j = (int)(rational2.numerator / (float)rational2.denominator);
                        final Rational rational3 = a[2];
                        return String.format("%02d:%02d:%02d", i, j, (int)(rational3.numerator / (float)rational3.denominator));
                    }
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Invalid GPS Timestamp array. array=");
                    sb2.append(Arrays.toString(a));
                    return null;
                }
                try {
                    string = Double.toString(exifAttribute.getDoubleValue(this.mExifByteOrder));
                    return string;
                    return null;
                    throw new NullPointerException("tag shouldn't be null");
                }
                catch (final NumberFormatException ex) {
                    return null;
                }
            }
        }
    }
    
    @Nullable
    public byte[] getAttributeBytes(@NonNull final String s) {
        if (s == null) {
            throw new NullPointerException("tag shouldn't be null");
        }
        final ExifAttribute exifAttribute = this.getExifAttribute(s);
        if (exifAttribute != null) {
            return exifAttribute.bytes;
        }
        return null;
    }
    
    public double getAttributeDouble(@NonNull final String s, final double n) {
        if (s != null) {
            final ExifAttribute exifAttribute = this.getExifAttribute(s);
            if (exifAttribute == null) {
                return n;
            }
            try {
                return exifAttribute.getDoubleValue(this.mExifByteOrder);
            }
            catch (final NumberFormatException ex) {
                return n;
            }
        }
        throw new NullPointerException("tag shouldn't be null");
    }
    
    public int getAttributeInt(@NonNull final String s, final int n) {
        if (s != null) {
            final ExifAttribute exifAttribute = this.getExifAttribute(s);
            if (exifAttribute == null) {
                return n;
            }
            try {
                return exifAttribute.getIntValue(this.mExifByteOrder);
            }
            catch (final NumberFormatException ex) {
                return n;
            }
        }
        throw new NullPointerException("tag shouldn't be null");
    }
    
    @Nullable
    public long[] getAttributeRange(@NonNull final String s) {
        if (s == null) {
            throw new NullPointerException("tag shouldn't be null");
        }
        if (this.mModified) {
            throw new IllegalStateException("The underlying file has been modified since being parsed");
        }
        final ExifAttribute exifAttribute = this.getExifAttribute(s);
        if (exifAttribute != null) {
            return new long[] { exifAttribute.bytesOffset, exifAttribute.bytes.length };
        }
        return null;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public Long getDateTime() {
        return parseDateTime(this.getAttribute("DateTime"), this.getAttribute("SubSecTime"), this.getAttribute("OffsetTime"));
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public Long getDateTimeDigitized() {
        return parseDateTime(this.getAttribute("DateTimeDigitized"), this.getAttribute("SubSecTimeDigitized"), this.getAttribute("OffsetTimeDigitized"));
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public Long getDateTimeOriginal() {
        return parseDateTime(this.getAttribute("DateTimeOriginal"), this.getAttribute("SubSecTimeOriginal"), this.getAttribute("OffsetTimeOriginal"));
    }
    
    @SuppressLint({ "AutoBoxing" })
    @Nullable
    public Long getGpsDateTime() {
        final String attribute = this.getAttribute("GPSDateStamp");
        final String attribute2 = this.getAttribute("GPSTimeStamp");
        Label_0160: {
            if (attribute == null || attribute2 == null) {
                break Label_0160;
            }
            final Pattern non_ZERO_TIME_PATTERN = ExifInterface.NON_ZERO_TIME_PATTERN;
            if (!non_ZERO_TIME_PATTERN.matcher(attribute).matches() && !non_ZERO_TIME_PATTERN.matcher(attribute2).matches()) {
                break Label_0160;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append(attribute);
            sb.append(' ');
            sb.append(attribute2);
            final String string = sb.toString();
            final ParsePosition parsePosition = new ParsePosition(0);
            try {
                Date date;
                if ((date = ExifInterface.sFormatterPrimary.parse(string, parsePosition)) == null && (date = ExifInterface.sFormatterSecondary.parse(string, parsePosition)) == null) {
                    return null;
                }
                return date.getTime();
                return null;
            }
            catch (final IllegalArgumentException ex) {
                return null;
            }
        }
    }
    
    @Deprecated
    public boolean getLatLong(final float[] array) {
        final double[] latLong = this.getLatLong();
        if (latLong == null) {
            return false;
        }
        array[0] = (float)latLong[0];
        array[1] = (float)latLong[1];
        return true;
    }
    
    @Nullable
    public double[] getLatLong() {
        final String attribute = this.getAttribute("GPSLatitude");
        final String attribute2 = this.getAttribute("GPSLatitudeRef");
        final String attribute3 = this.getAttribute("GPSLongitude");
        final String attribute4 = this.getAttribute("GPSLongitudeRef");
        if (attribute != null && attribute2 != null && attribute3 != null && attribute4 != null) {
            try {
                return new double[] { convertRationalLatLonToDouble(attribute, attribute2), convertRationalLatLonToDouble(attribute3, attribute4) };
            }
            catch (final IllegalArgumentException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Latitude/longitude values are not parsable. ");
                sb.append(String.format("latValue=%s, latRef=%s, lngValue=%s, lngRef=%s", attribute, attribute2, attribute3, attribute4));
            }
        }
        return null;
    }
    
    public int getRotationDegrees() {
        switch (this.getAttributeInt("Orientation", 1)) {
            default: {
                return 0;
            }
            case 6:
            case 7: {
                return 90;
            }
            case 5:
            case 8: {
                return 270;
            }
            case 3:
            case 4: {
                return 180;
            }
        }
    }
    
    @Nullable
    public byte[] getThumbnail() {
        final int mThumbnailCompression = this.mThumbnailCompression;
        if (mThumbnailCompression != 6 && mThumbnailCompression != 7) {
            return null;
        }
        return this.getThumbnailBytes();
    }
    
    @Nullable
    public Bitmap getThumbnailBitmap() {
        if (!this.mHasThumbnail) {
            return null;
        }
        if (this.mThumbnailBytes == null) {
            this.mThumbnailBytes = this.getThumbnailBytes();
        }
        final int mThumbnailCompression = this.mThumbnailCompression;
        if (mThumbnailCompression != 6 && mThumbnailCompression != 7) {
            if (mThumbnailCompression == 1) {
                final int n = this.mThumbnailBytes.length / 3;
                final int[] array = new int[n];
                for (int i = 0; i < n; ++i) {
                    final byte[] mThumbnailBytes = this.mThumbnailBytes;
                    final int n2 = i * 3;
                    array[i] = (mThumbnailBytes[n2] << 16) + 0 + (mThumbnailBytes[n2 + 1] << 8) + mThumbnailBytes[n2 + 2];
                }
                final ExifAttribute exifAttribute = this.mAttributes[4].get("ImageLength");
                final ExifAttribute exifAttribute2 = this.mAttributes[4].get("ImageWidth");
                if (exifAttribute != null && exifAttribute2 != null) {
                    return Bitmap.createBitmap(array, exifAttribute2.getIntValue(this.mExifByteOrder), exifAttribute.getIntValue(this.mExifByteOrder), Bitmap$Config.ARGB_8888);
                }
            }
            return null;
        }
        return BitmapFactory.decodeByteArray(this.mThumbnailBytes, 0, this.mThumbnailLength);
    }
    
    @Nullable
    public byte[] getThumbnailBytes() {
        if (!this.mHasThumbnail) {
            return null;
        }
        final byte[] mThumbnailBytes = this.mThumbnailBytes;
        if (mThumbnailBytes != null) {
            return mThumbnailBytes;
        }
        try {
            Object mAssetInputStream = this.mAssetInputStream;
            while (true) {
                Label_0084: {
                    if (mAssetInputStream == null) {
                        break Label_0084;
                    }
                    try {
                        if (((InputStream)mAssetInputStream).markSupported()) {
                            ((InputStream)mAssetInputStream).reset();
                        }
                        closeQuietly((Closeable)mAssetInputStream);
                        return null;
                    }
                    catch (final Exception ex) {}
                    finally {
                        final FileDescriptor dup = null;
                        goto Label_0283;
                    }
                }
                if (this.mFilename != null) {
                    mAssetInputStream = new FileInputStream(this.mFilename);
                    continue;
                }
                break;
            }
            final FileDescriptor dup = Os.dup(this.mSeekableFileDescriptor);
            try {
                Os.lseek(dup, 0L, OsConstants.SEEK_SET);
                mAssetInputStream = new FileInputStream(dup);
                try {
                    if (((InputStream)mAssetInputStream).skip(this.mThumbnailOffset + this.mOffsetToExifData) != this.mThumbnailOffset + this.mOffsetToExifData) {
                        throw new IOException("Corrupted image");
                    }
                    final byte[] array = new byte[this.mThumbnailLength];
                    if (((InputStream)mAssetInputStream).read(array) == this.mThumbnailLength) {
                        this.mThumbnailBytes = array;
                        closeQuietly((Closeable)mAssetInputStream);
                        if (dup != null) {
                            closeFileDescriptor(dup);
                        }
                        return array;
                    }
                    throw new IOException("Corrupted image");
                }
                catch (final Exception ex2) {}
            }
            catch (final Exception ex3) {}
        }
        catch (final Exception ex4) {}
    }
    
    @Nullable
    public long[] getThumbnailRange() {
        if (this.mModified) {
            throw new IllegalStateException("The underlying file has been modified since being parsed");
        }
        if (!this.mHasThumbnail) {
            return null;
        }
        if (this.mHasThumbnailStrips && !this.mAreThumbnailStripsConsecutive) {
            return null;
        }
        return new long[] { this.mThumbnailOffset + this.mOffsetToExifData, this.mThumbnailLength };
    }
    
    public boolean hasAttribute(@NonNull final String s) {
        return this.getExifAttribute(s) != null;
    }
    
    public boolean hasThumbnail() {
        return this.mHasThumbnail;
    }
    
    public boolean isFlipped() {
        final int attributeInt = this.getAttributeInt("Orientation", 1);
        return attributeInt == 2 || attributeInt == 7 || attributeInt == 4 || attributeInt == 5;
    }
    
    public boolean isThumbnailCompressed() {
        if (!this.mHasThumbnail) {
            return false;
        }
        final int mThumbnailCompression = this.mThumbnailCompression;
        return mThumbnailCompression == 6 || mThumbnailCompression == 7;
    }
    
    public void resetOrientation() {
        this.setAttribute("Orientation", Integer.toString(1));
    }
    
    public void rotate(int n) {
        if (n % 90 == 0) {
            final int attributeInt = this.getAttributeInt("Orientation", 1);
            final List<Integer> rotation_ORDER = ExifInterface.ROTATION_ORDER;
            final boolean contains = rotation_ORDER.contains(attributeInt);
            final int n2 = 0;
            final int n3 = 0;
            final int n4 = 0;
            int i;
            if (contains) {
                final int n5 = (rotation_ORDER.indexOf(attributeInt) + n / 90) % 4;
                n = n4;
                if (n5 < 0) {
                    n = 4;
                }
                i = rotation_ORDER.get(n5 + n);
            }
            else {
                final List<Integer> flipped_ROTATION_ORDER = ExifInterface.FLIPPED_ROTATION_ORDER;
                i = n3;
                if (flipped_ROTATION_ORDER.contains(attributeInt)) {
                    final int n6 = (flipped_ROTATION_ORDER.indexOf(attributeInt) + n / 90) % 4;
                    n = n2;
                    if (n6 < 0) {
                        n = 4;
                    }
                    i = (int)flipped_ROTATION_ORDER.get(n6 + n);
                }
            }
            this.setAttribute("Orientation", Integer.toString(i));
            return;
        }
        throw new IllegalArgumentException("degree should be a multiple of 90");
    }
    
    public void saveAttributes() throws IOException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokespecial   androidx/exifinterface/media/ExifInterface.isSupportedFormatForSavingAttributes:()Z
        //     4: ifeq            947
        //     7: aload_0        
        //     8: getfield        androidx/exifinterface/media/ExifInterface.mSeekableFileDescriptor:Ljava/io/FileDescriptor;
        //    11: ifnonnull       35
        //    14: aload_0        
        //    15: getfield        androidx/exifinterface/media/ExifInterface.mFilename:Ljava/lang/String;
        //    18: ifnull          24
        //    21: goto            35
        //    24: new             Ljava/io/IOException;
        //    27: dup            
        //    28: ldc_w           "ExifInterface does not support saving attributes for the current input."
        //    31: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //    34: athrow         
        //    35: aload_0        
        //    36: iconst_1       
        //    37: putfield        androidx/exifinterface/media/ExifInterface.mModified:Z
        //    40: aload_0        
        //    41: aload_0        
        //    42: invokevirtual   androidx/exifinterface/media/ExifInterface.getThumbnail:()[B
        //    45: putfield        androidx/exifinterface/media/ExifInterface.mThumbnailBytes:[B
        //    48: aconst_null    
        //    49: astore          9
        //    51: aconst_null    
        //    52: astore          5
        //    54: aconst_null    
        //    55: astore          6
        //    57: aconst_null    
        //    58: astore          7
        //    60: ldc_w           "temp"
        //    63: ldc_w           "tmp"
        //    66: invokestatic    java/io/File.createTempFile:(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
        //    69: astore          13
        //    71: aload_0        
        //    72: getfield        androidx/exifinterface/media/ExifInterface.mFilename:Ljava/lang/String;
        //    75: ifnull          99
        //    78: new             Ljava/io/FileInputStream;
        //    81: astore          8
        //    83: aload           8
        //    85: aload_0        
        //    86: getfield        androidx/exifinterface/media/ExifInterface.mFilename:Ljava/lang/String;
        //    89: invokespecial   java/io/FileInputStream.<init>:(Ljava/lang/String;)V
        //    92: aload           8
        //    94: astore          5
        //    96: goto            128
        //    99: aload_0        
        //   100: getfield        androidx/exifinterface/media/ExifInterface.mSeekableFileDescriptor:Ljava/io/FileDescriptor;
        //   103: lconst_0       
        //   104: getstatic       android/system/OsConstants.SEEK_SET:I
        //   107: invokestatic    android/system/Os.lseek:(Ljava/io/FileDescriptor;JI)J
        //   110: pop2           
        //   111: new             Ljava/io/FileInputStream;
        //   114: dup            
        //   115: aload_0        
        //   116: getfield        androidx/exifinterface/media/ExifInterface.mSeekableFileDescriptor:Ljava/io/FileDescriptor;
        //   119: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/FileDescriptor;)V
        //   122: astore          8
        //   124: aload           8
        //   126: astore          5
        //   128: new             Ljava/io/FileOutputStream;
        //   131: astore          6
        //   133: aload           6
        //   135: aload           13
        //   137: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/File;)V
        //   140: aload           5
        //   142: aload           6
        //   144: invokestatic    androidx/exifinterface/media/ExifInterface.copy:(Ljava/io/InputStream;Ljava/io/OutputStream;)I
        //   147: pop            
        //   148: aload           5
        //   150: invokestatic    androidx/exifinterface/media/ExifInterface.closeQuietly:(Ljava/io/Closeable;)V
        //   153: aload           6
        //   155: invokestatic    androidx/exifinterface/media/ExifInterface.closeQuietly:(Ljava/io/Closeable;)V
        //   158: iconst_0       
        //   159: istore_3       
        //   160: iconst_0       
        //   161: istore_1       
        //   162: iconst_0       
        //   163: istore          4
        //   165: iconst_0       
        //   166: istore_2       
        //   167: new             Ljava/io/FileInputStream;
        //   170: astore          5
        //   172: aload           5
        //   174: aload           13
        //   176: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
        //   179: aload_0        
        //   180: getfield        androidx/exifinterface/media/ExifInterface.mFilename:Ljava/lang/String;
        //   183: ifnull          203
        //   186: new             Ljava/io/FileOutputStream;
        //   189: astore          7
        //   191: aload           7
        //   193: aload_0        
        //   194: getfield        androidx/exifinterface/media/ExifInterface.mFilename:Ljava/lang/String;
        //   197: invokespecial   java/io/FileOutputStream.<init>:(Ljava/lang/String;)V
        //   200: goto            228
        //   203: aload_0        
        //   204: getfield        androidx/exifinterface/media/ExifInterface.mSeekableFileDescriptor:Ljava/io/FileDescriptor;
        //   207: lconst_0       
        //   208: getstatic       android/system/OsConstants.SEEK_SET:I
        //   211: invokestatic    android/system/Os.lseek:(Ljava/io/FileDescriptor;JI)J
        //   214: pop2           
        //   215: new             Ljava/io/FileOutputStream;
        //   218: dup            
        //   219: aload_0        
        //   220: getfield        androidx/exifinterface/media/ExifInterface.mSeekableFileDescriptor:Ljava/io/FileDescriptor;
        //   223: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/FileDescriptor;)V
        //   226: astore          7
        //   228: new             Ljava/io/BufferedInputStream;
        //   231: astore          8
        //   233: aload           8
        //   235: aload           5
        //   237: invokespecial   java/io/BufferedInputStream.<init>:(Ljava/io/InputStream;)V
        //   240: new             Ljava/io/BufferedOutputStream;
        //   243: astore          11
        //   245: aload           11
        //   247: aload           7
        //   249: invokespecial   java/io/BufferedOutputStream.<init>:(Ljava/io/OutputStream;)V
        //   252: iload_3        
        //   253: istore_1       
        //   254: aload           8
        //   256: astore          6
        //   258: aload           11
        //   260: astore          10
        //   262: aload_0        
        //   263: getfield        androidx/exifinterface/media/ExifInterface.mMimeType:I
        //   266: istore          4
        //   268: iload           4
        //   270: iconst_4       
        //   271: if_icmpne       295
        //   274: iload_3        
        //   275: istore_1       
        //   276: aload           8
        //   278: astore          6
        //   280: aload           11
        //   282: astore          10
        //   284: aload_0        
        //   285: aload           8
        //   287: aload           11
        //   289: invokespecial   androidx/exifinterface/media/ExifInterface.saveJpegAttributes:(Ljava/io/InputStream;Ljava/io/OutputStream;)V
        //   292: goto            348
        //   295: iload           4
        //   297: bipush          13
        //   299: if_icmpne       323
        //   302: iload_3        
        //   303: istore_1       
        //   304: aload           8
        //   306: astore          6
        //   308: aload           11
        //   310: astore          10
        //   312: aload_0        
        //   313: aload           8
        //   315: aload           11
        //   317: invokespecial   androidx/exifinterface/media/ExifInterface.savePngAttributes:(Ljava/io/InputStream;Ljava/io/OutputStream;)V
        //   320: goto            348
        //   323: iload           4
        //   325: bipush          14
        //   327: if_icmpne       348
        //   330: iload_3        
        //   331: istore_1       
        //   332: aload           8
        //   334: astore          6
        //   336: aload           11
        //   338: astore          10
        //   340: aload_0        
        //   341: aload           8
        //   343: aload           11
        //   345: invokespecial   androidx/exifinterface/media/ExifInterface.saveWebpAttributes:(Ljava/io/InputStream;Ljava/io/OutputStream;)V
        //   348: aload           8
        //   350: invokestatic    androidx/exifinterface/media/ExifInterface.closeQuietly:(Ljava/io/Closeable;)V
        //   353: aload           11
        //   355: invokestatic    androidx/exifinterface/media/ExifInterface.closeQuietly:(Ljava/io/Closeable;)V
        //   358: aload           13
        //   360: invokevirtual   java/io/File.delete:()Z
        //   363: pop            
        //   364: aload_0        
        //   365: aconst_null    
        //   366: putfield        androidx/exifinterface/media/ExifInterface.mThumbnailBytes:[B
        //   369: return         
        //   370: astore          9
        //   372: aload           5
        //   374: astore          6
        //   376: aload           7
        //   378: astore          5
        //   380: aload           11
        //   382: astore          7
        //   384: goto            489
        //   387: astore          5
        //   389: aconst_null    
        //   390: astore          10
        //   392: aload           8
        //   394: astore          6
        //   396: goto            824
        //   399: astore          9
        //   401: aconst_null    
        //   402: astore          10
        //   404: aload           5
        //   406: astore          6
        //   408: aload           7
        //   410: astore          5
        //   412: aload           10
        //   414: astore          7
        //   416: goto            489
        //   419: astore          9
        //   421: aconst_null    
        //   422: astore          10
        //   424: aconst_null    
        //   425: astore          8
        //   427: aload           5
        //   429: astore          6
        //   431: aload           7
        //   433: astore          5
        //   435: aload           10
        //   437: astore          7
        //   439: goto            489
        //   442: astore          7
        //   444: aload           5
        //   446: astore          6
        //   448: aload           7
        //   450: astore          5
        //   452: goto            476
        //   455: astore          5
        //   457: aconst_null    
        //   458: astore          10
        //   460: aload           9
        //   462: astore          6
        //   464: iload           4
        //   466: istore_1       
        //   467: goto            824
        //   470: astore          5
        //   472: aload           7
        //   474: astore          6
        //   476: aconst_null    
        //   477: astore          7
        //   479: aconst_null    
        //   480: astore          8
        //   482: aload           5
        //   484: astore          9
        //   486: aconst_null    
        //   487: astore          5
        //   489: new             Ljava/io/FileInputStream;
        //   492: astore          11
        //   494: aload           11
        //   496: aload           13
        //   498: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
        //   501: aload           5
        //   503: astore          6
        //   505: aload           5
        //   507: astore          10
        //   509: aload_0        
        //   510: getfield        androidx/exifinterface/media/ExifInterface.mFilename:Ljava/lang/String;
        //   513: ifnonnull       573
        //   516: aload           5
        //   518: astore          6
        //   520: aload           5
        //   522: astore          10
        //   524: aload_0        
        //   525: getfield        androidx/exifinterface/media/ExifInterface.mSeekableFileDescriptor:Ljava/io/FileDescriptor;
        //   528: lconst_0       
        //   529: getstatic       android/system/OsConstants.SEEK_SET:I
        //   532: invokestatic    android/system/Os.lseek:(Ljava/io/FileDescriptor;JI)J
        //   535: pop2           
        //   536: aload           5
        //   538: astore          6
        //   540: aload           5
        //   542: astore          10
        //   544: new             Ljava/io/FileOutputStream;
        //   547: astore          12
        //   549: aload           5
        //   551: astore          6
        //   553: aload           5
        //   555: astore          10
        //   557: aload           12
        //   559: aload_0        
        //   560: getfield        androidx/exifinterface/media/ExifInterface.mSeekableFileDescriptor:Ljava/io/FileDescriptor;
        //   563: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/FileDescriptor;)V
        //   566: aload           12
        //   568: astore          5
        //   570: goto            594
        //   573: aload           5
        //   575: astore          6
        //   577: aload           5
        //   579: astore          10
        //   581: new             Ljava/io/FileOutputStream;
        //   584: dup            
        //   585: aload_0        
        //   586: getfield        androidx/exifinterface/media/ExifInterface.mFilename:Ljava/lang/String;
        //   589: invokespecial   java/io/FileOutputStream.<init>:(Ljava/lang/String;)V
        //   592: astore          5
        //   594: aload           5
        //   596: astore          6
        //   598: aload           5
        //   600: astore          10
        //   602: aload           11
        //   604: aload           5
        //   606: invokestatic    androidx/exifinterface/media/ExifInterface.copy:(Ljava/io/InputStream;Ljava/io/OutputStream;)I
        //   609: pop            
        //   610: iload_3        
        //   611: istore_1       
        //   612: aload           8
        //   614: astore          6
        //   616: aload           7
        //   618: astore          10
        //   620: aload           11
        //   622: invokestatic    androidx/exifinterface/media/ExifInterface.closeQuietly:(Ljava/io/Closeable;)V
        //   625: iload_3        
        //   626: istore_1       
        //   627: aload           8
        //   629: astore          6
        //   631: aload           7
        //   633: astore          10
        //   635: aload           5
        //   637: invokestatic    androidx/exifinterface/media/ExifInterface.closeQuietly:(Ljava/io/Closeable;)V
        //   640: iload_3        
        //   641: istore_1       
        //   642: aload           8
        //   644: astore          6
        //   646: aload           7
        //   648: astore          10
        //   650: new             Ljava/io/IOException;
        //   653: astore          5
        //   655: iload_3        
        //   656: istore_1       
        //   657: aload           8
        //   659: astore          6
        //   661: aload           7
        //   663: astore          10
        //   665: aload           5
        //   667: ldc_w           "Failed to save new file"
        //   670: aload           9
        //   672: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   675: iload_3        
        //   676: istore_1       
        //   677: aload           8
        //   679: astore          6
        //   681: aload           7
        //   683: astore          10
        //   685: aload           5
        //   687: athrow         
        //   688: astore          9
        //   690: aload           6
        //   692: astore          5
        //   694: goto            779
        //   697: astore          9
        //   699: aload           10
        //   701: astore          5
        //   703: aload           11
        //   705: astore          6
        //   707: goto            721
        //   710: astore          9
        //   712: aload           6
        //   714: astore          11
        //   716: goto            779
        //   719: astore          9
        //   721: new             Ljava/io/IOException;
        //   724: astore          10
        //   726: new             Ljava/lang/StringBuilder;
        //   729: astore          11
        //   731: aload           11
        //   733: invokespecial   java/lang/StringBuilder.<init>:()V
        //   736: aload           11
        //   738: ldc_w           "Failed to save new file. Original file is stored in "
        //   741: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   744: pop            
        //   745: aload           11
        //   747: aload           13
        //   749: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   752: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   755: pop            
        //   756: aload           10
        //   758: aload           11
        //   760: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   763: aload           9
        //   765: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   768: aload           10
        //   770: athrow         
        //   771: astore          9
        //   773: iconst_1       
        //   774: istore_2       
        //   775: aload           6
        //   777: astore          11
        //   779: iload_2        
        //   780: istore_1       
        //   781: aload           8
        //   783: astore          6
        //   785: aload           7
        //   787: astore          10
        //   789: aload           11
        //   791: invokestatic    androidx/exifinterface/media/ExifInterface.closeQuietly:(Ljava/io/Closeable;)V
        //   794: iload_2        
        //   795: istore_1       
        //   796: aload           8
        //   798: astore          6
        //   800: aload           7
        //   802: astore          10
        //   804: aload           5
        //   806: invokestatic    androidx/exifinterface/media/ExifInterface.closeQuietly:(Ljava/io/Closeable;)V
        //   809: iload_2        
        //   810: istore_1       
        //   811: aload           8
        //   813: astore          6
        //   815: aload           7
        //   817: astore          10
        //   819: aload           9
        //   821: athrow         
        //   822: astore          5
        //   824: aload           6
        //   826: invokestatic    androidx/exifinterface/media/ExifInterface.closeQuietly:(Ljava/io/Closeable;)V
        //   829: aload           10
        //   831: invokestatic    androidx/exifinterface/media/ExifInterface.closeQuietly:(Ljava/io/Closeable;)V
        //   834: iload_1        
        //   835: ifne            844
        //   838: aload           13
        //   840: invokevirtual   java/io/File.delete:()Z
        //   843: pop            
        //   844: aload           5
        //   846: athrow         
        //   847: astore          7
        //   849: goto            862
        //   852: astore          7
        //   854: goto            882
        //   857: astore          7
        //   859: aconst_null    
        //   860: astore          6
        //   862: aload           5
        //   864: astore          8
        //   866: aload           7
        //   868: astore          5
        //   870: aload           8
        //   872: astore          7
        //   874: goto            934
        //   877: astore          7
        //   879: aconst_null    
        //   880: astore          6
        //   882: goto            906
        //   885: astore          5
        //   887: aconst_null    
        //   888: astore          8
        //   890: aload           6
        //   892: astore          7
        //   894: aload           8
        //   896: astore          6
        //   898: goto            934
        //   901: astore          7
        //   903: aconst_null    
        //   904: astore          6
        //   906: new             Ljava/io/IOException;
        //   909: astore          8
        //   911: aload           8
        //   913: ldc_w           "Failed to copy original file to temp file"
        //   916: aload           7
        //   918: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   921: aload           8
        //   923: athrow         
        //   924: astore          8
        //   926: aload           5
        //   928: astore          7
        //   930: aload           8
        //   932: astore          5
        //   934: aload           7
        //   936: invokestatic    androidx/exifinterface/media/ExifInterface.closeQuietly:(Ljava/io/Closeable;)V
        //   939: aload           6
        //   941: invokestatic    androidx/exifinterface/media/ExifInterface.closeQuietly:(Ljava/io/Closeable;)V
        //   944: aload           5
        //   946: athrow         
        //   947: new             Ljava/io/IOException;
        //   950: dup            
        //   951: ldc_w           "ExifInterface only supports saving attributes on JPEG, PNG, or WebP formats."
        //   954: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //   957: athrow         
        //    Exceptions:
        //  throws java.io.IOException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  60     92     901    906    Ljava/lang/Exception;
        //  60     92     885    901    Any
        //  99     124    901    906    Ljava/lang/Exception;
        //  99     124    885    901    Any
        //  128    140    877    882    Ljava/lang/Exception;
        //  128    140    857    862    Any
        //  140    148    852    857    Ljava/lang/Exception;
        //  140    148    847    852    Any
        //  167    179    470    476    Ljava/lang/Exception;
        //  167    179    455    470    Any
        //  179    200    442    455    Ljava/lang/Exception;
        //  179    200    455    470    Any
        //  203    228    442    455    Ljava/lang/Exception;
        //  203    228    455    470    Any
        //  228    240    419    442    Ljava/lang/Exception;
        //  228    240    455    470    Any
        //  240    252    399    419    Ljava/lang/Exception;
        //  240    252    387    399    Any
        //  262    268    370    387    Ljava/lang/Exception;
        //  262    268    822    824    Any
        //  284    292    370    387    Ljava/lang/Exception;
        //  284    292    822    824    Any
        //  312    320    370    387    Ljava/lang/Exception;
        //  312    320    822    824    Any
        //  340    348    370    387    Ljava/lang/Exception;
        //  340    348    822    824    Any
        //  489    501    719    721    Ljava/lang/Exception;
        //  489    501    710    719    Any
        //  509    516    697    710    Ljava/lang/Exception;
        //  509    516    688    697    Any
        //  524    536    697    710    Ljava/lang/Exception;
        //  524    536    688    697    Any
        //  544    549    697    710    Ljava/lang/Exception;
        //  544    549    688    697    Any
        //  557    566    697    710    Ljava/lang/Exception;
        //  557    566    688    697    Any
        //  581    594    697    710    Ljava/lang/Exception;
        //  581    594    688    697    Any
        //  602    610    697    710    Ljava/lang/Exception;
        //  602    610    688    697    Any
        //  620    625    822    824    Any
        //  635    640    822    824    Any
        //  650    655    822    824    Any
        //  665    675    822    824    Any
        //  685    688    822    824    Any
        //  721    771    771    779    Any
        //  789    794    822    824    Any
        //  804    809    822    824    Any
        //  819    822    822    824    Any
        //  906    924    924    934    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException
        //     at java.base/java.util.Collections$1.remove(Collections.java:4820)
        //     at java.base/java.util.AbstractCollection.removeAll(AbstractCollection.java:369)
        //     at com.strobel.decompiler.ast.AstBuilder.convertLocalVariables(AstBuilder.java:3018)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2501)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void setAltitude(final double a) {
        String s;
        if (a >= 0.0) {
            s = "0";
        }
        else {
            s = "1";
        }
        this.setAttribute("GPSAltitude", new Rational(Math.abs(a)).toString());
        this.setAttribute("GPSAltitudeRef", s);
    }
    
    public void setAttribute(@NonNull String anObject, @Nullable String s) {
        final String str = s;
        if (anObject != null) {
            String replaceAll = null;
            Label_0161: {
                if (!"DateTime".equals(anObject) && !"DateTimeOriginal".equals(anObject)) {
                    replaceAll = str;
                    if (!"DateTimeDigitized".equals(anObject)) {
                        break Label_0161;
                    }
                }
                if ((replaceAll = str) != null) {
                    final boolean find = ExifInterface.DATETIME_PRIMARY_FORMAT_PATTERN.matcher(str).find();
                    final boolean find2 = ExifInterface.DATETIME_SECONDARY_FORMAT_PATTERN.matcher(str).find();
                    if (s.length() != 19 || (!find && !find2)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Invalid value for ");
                        sb.append(anObject);
                        sb.append(" : ");
                        sb.append(str);
                        return;
                    }
                    replaceAll = str;
                    if (find2) {
                        replaceAll = str.replaceAll("-", ":");
                    }
                }
            }
            s = anObject;
            if ("ISOSpeedRatings".equals(anObject)) {
                s = "PhotographicSensitivity";
            }
            if ((anObject = replaceAll) != null) {
                anObject = replaceAll;
                if (ExifInterface.sTagSetForCompatibility.contains(s)) {
                    if (s.equals("GPSTimeStamp")) {
                        final Matcher matcher = ExifInterface.GPS_TIMESTAMP_PATTERN.matcher(replaceAll);
                        if (!matcher.find()) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Invalid value for ");
                            sb2.append(s);
                            sb2.append(" : ");
                            sb2.append(replaceAll);
                            return;
                        }
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append(Integer.parseInt(matcher.group(1)));
                        sb3.append("/1,");
                        sb3.append(Integer.parseInt(matcher.group(2)));
                        sb3.append("/1,");
                        sb3.append(Integer.parseInt(matcher.group(3)));
                        sb3.append("/1");
                        anObject = sb3.toString();
                    }
                    else {
                        try {
                            anObject = new Rational(Double.parseDouble(replaceAll)).toString();
                        }
                        catch (final NumberFormatException ex) {
                            final StringBuilder sb4 = new StringBuilder();
                            sb4.append("Invalid value for ");
                            sb4.append(s);
                            sb4.append(" : ");
                            sb4.append(replaceAll);
                            return;
                        }
                    }
                }
            }
            for (int i = 0; i < ExifInterface.EXIF_TAGS.length; ++i) {
                if (i != 4 || this.mHasThumbnail) {
                    final ExifTag exifTag = ExifInterface.sExifTagMapsForWriting[i].get(s);
                    if (exifTag != null) {
                        if (anObject == null) {
                            this.mAttributes[i].remove(s);
                        }
                        else {
                            final Pair<Integer, Integer> guessDataFormat = guessDataFormat(anObject);
                            int j;
                            if (exifTag.primaryFormat != (int)guessDataFormat.first && exifTag.primaryFormat != (int)guessDataFormat.second) {
                                final int secondaryFormat = exifTag.secondaryFormat;
                                if (secondaryFormat != -1 && (secondaryFormat == (int)guessDataFormat.first || exifTag.secondaryFormat == (int)guessDataFormat.second)) {
                                    j = exifTag.secondaryFormat;
                                }
                                else {
                                    j = exifTag.primaryFormat;
                                    if (j != 1 && j != 7 && j != 2) {
                                        if (ExifInterface.DEBUG) {
                                            final StringBuilder sb5 = new StringBuilder();
                                            sb5.append("Given tag (");
                                            sb5.append(s);
                                            sb5.append(") value didn't match with one of expected formats: ");
                                            final String[] ifd_FORMAT_NAMES = ExifInterface.IFD_FORMAT_NAMES;
                                            sb5.append(ifd_FORMAT_NAMES[exifTag.primaryFormat]);
                                            final int secondaryFormat2 = exifTag.secondaryFormat;
                                            final String s2 = "";
                                            String string;
                                            if (secondaryFormat2 == -1) {
                                                string = "";
                                            }
                                            else {
                                                final StringBuilder sb6 = new StringBuilder();
                                                sb6.append(", ");
                                                sb6.append(ifd_FORMAT_NAMES[exifTag.secondaryFormat]);
                                                string = sb6.toString();
                                            }
                                            sb5.append(string);
                                            sb5.append(" (guess: ");
                                            sb5.append(ifd_FORMAT_NAMES[(int)guessDataFormat.first]);
                                            String string2;
                                            if ((int)guessDataFormat.second == -1) {
                                                string2 = s2;
                                            }
                                            else {
                                                final StringBuilder sb7 = new StringBuilder();
                                                sb7.append(", ");
                                                sb7.append(ifd_FORMAT_NAMES[(int)guessDataFormat.second]);
                                                string2 = sb7.toString();
                                            }
                                            sb5.append(string2);
                                            sb5.append(")");
                                        }
                                        continue;
                                    }
                                }
                            }
                            else {
                                j = exifTag.primaryFormat;
                            }
                            switch (j) {
                                default: {
                                    if (ExifInterface.DEBUG) {
                                        final StringBuilder sb8 = new StringBuilder();
                                        sb8.append("Data format isn't one of expected formats: ");
                                        sb8.append(j);
                                        break;
                                    }
                                    break;
                                }
                                case 12: {
                                    final String[] split = anObject.split(",", -1);
                                    final double[] array = new double[split.length];
                                    for (int k = 0; k < split.length; ++k) {
                                        array[k] = Double.parseDouble(split[k]);
                                    }
                                    this.mAttributes[i].put(s, ExifAttribute.createDouble(array, this.mExifByteOrder));
                                    break;
                                }
                                case 10: {
                                    final String[] split2 = anObject.split(",", -1);
                                    final Rational[] array2 = new Rational[split2.length];
                                    for (int l = 0; l < split2.length; ++l) {
                                        final String[] split3 = split2[l].split("/", -1);
                                        array2[l] = new Rational((long)Double.parseDouble(split3[0]), (long)Double.parseDouble(split3[1]));
                                    }
                                    this.mAttributes[i].put(s, ExifAttribute.createSRational(array2, this.mExifByteOrder));
                                    break;
                                }
                                case 9: {
                                    final String[] split4 = anObject.split(",", -1);
                                    final int[] array3 = new int[split4.length];
                                    for (int n = 0; n < split4.length; ++n) {
                                        array3[n] = Integer.parseInt(split4[n]);
                                    }
                                    this.mAttributes[i].put(s, ExifAttribute.createSLong(array3, this.mExifByteOrder));
                                    break;
                                }
                                case 5: {
                                    final String[] split5 = anObject.split(",", -1);
                                    final Rational[] array4 = new Rational[split5.length];
                                    for (int n2 = 0; n2 < split5.length; ++n2) {
                                        final String[] split6 = split5[n2].split("/", -1);
                                        array4[n2] = new Rational((long)Double.parseDouble(split6[0]), (long)Double.parseDouble(split6[1]));
                                    }
                                    this.mAttributes[i].put(s, ExifAttribute.createURational(array4, this.mExifByteOrder));
                                    break;
                                }
                                case 4: {
                                    final String[] split7 = anObject.split(",", -1);
                                    final long[] array5 = new long[split7.length];
                                    for (int n3 = 0; n3 < split7.length; ++n3) {
                                        array5[n3] = Long.parseLong(split7[n3]);
                                    }
                                    this.mAttributes[i].put(s, ExifAttribute.createULong(array5, this.mExifByteOrder));
                                    break;
                                }
                                case 3: {
                                    final String[] split8 = anObject.split(",", -1);
                                    final int[] array6 = new int[split8.length];
                                    for (int n4 = 0; n4 < split8.length; ++n4) {
                                        array6[n4] = Integer.parseInt(split8[n4]);
                                    }
                                    this.mAttributes[i].put(s, ExifAttribute.createUShort(array6, this.mExifByteOrder));
                                    break;
                                }
                                case 2:
                                case 7: {
                                    this.mAttributes[i].put(s, ExifAttribute.createString(anObject));
                                    break;
                                }
                                case 1: {
                                    this.mAttributes[i].put(s, ExifAttribute.createByte(anObject));
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            return;
        }
        throw new NullPointerException("tag shouldn't be null");
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public void setDateTime(@NonNull final Long n) {
        final long longValue = n;
        this.setAttribute("DateTime", ExifInterface.sFormatterPrimary.format(new Date(n)));
        this.setAttribute("SubSecTime", Long.toString(longValue % 1000L));
    }
    
    public void setGpsInfo(final Location location) {
        if (location == null) {
            return;
        }
        this.setAttribute("GPSProcessingMethod", location.getProvider());
        this.setLatLong(location.getLatitude(), location.getLongitude());
        this.setAltitude(location.getAltitude());
        this.setAttribute("GPSSpeedRef", "K");
        this.setAttribute("GPSSpeed", new Rational(location.getSpeed() * TimeUnit.HOURS.toSeconds(1L) / 1000.0f).toString());
        final String[] split = ExifInterface.sFormatterPrimary.format(new Date(location.getTime())).split("\\s+", -1);
        this.setAttribute("GPSDateStamp", split[0]);
        this.setAttribute("GPSTimeStamp", split[1]);
    }
    
    public void setLatLong(final double a, final double d) {
        if (a < -90.0 || a > 90.0 || Double.isNaN(a)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Latitude value ");
            sb.append(a);
            sb.append(" is not valid.");
            throw new IllegalArgumentException(sb.toString());
        }
        if (d >= -180.0 && d <= 180.0 && !Double.isNaN(d)) {
            String s;
            if (a >= 0.0) {
                s = "N";
            }
            else {
                s = "S";
            }
            this.setAttribute("GPSLatitudeRef", s);
            this.setAttribute("GPSLatitude", this.convertDecimalDegree(Math.abs(a)));
            String s2;
            if (d >= 0.0) {
                s2 = "E";
            }
            else {
                s2 = "W";
            }
            this.setAttribute("GPSLongitudeRef", s2);
            this.setAttribute("GPSLongitude", this.convertDecimalDegree(Math.abs(d)));
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Longitude value ");
        sb2.append(d);
        sb2.append(" is not valid.");
        throw new IllegalArgumentException(sb2.toString());
    }
    
    private static class ByteOrderedDataInputStream extends InputStream implements DataInput
    {
        private static final ByteOrder BIG_ENDIAN;
        private static final ByteOrder LITTLE_ENDIAN;
        private ByteOrder mByteOrder;
        private DataInputStream mDataInputStream;
        final int mLength;
        int mPosition;
        
        static {
            LITTLE_ENDIAN = ByteOrder.LITTLE_ENDIAN;
            BIG_ENDIAN = ByteOrder.BIG_ENDIAN;
        }
        
        public ByteOrderedDataInputStream(final InputStream inputStream) throws IOException {
            this(inputStream, ByteOrder.BIG_ENDIAN);
        }
        
        ByteOrderedDataInputStream(final InputStream in, final ByteOrder mByteOrder) throws IOException {
            this.mByteOrder = ByteOrder.BIG_ENDIAN;
            final DataInputStream mDataInputStream = new DataInputStream(in);
            this.mDataInputStream = mDataInputStream;
            final int available = mDataInputStream.available();
            this.mLength = available;
            this.mPosition = 0;
            this.mDataInputStream.mark(available);
            this.mByteOrder = mByteOrder;
        }
        
        public ByteOrderedDataInputStream(final byte[] buf) throws IOException {
            this(new ByteArrayInputStream(buf));
        }
        
        @Override
        public int available() throws IOException {
            return this.mDataInputStream.available();
        }
        
        public int getLength() {
            return this.mLength;
        }
        
        @Override
        public void mark(final int readlimit) {
            synchronized (this) {
                this.mDataInputStream.mark(readlimit);
            }
        }
        
        public int peek() {
            return this.mPosition;
        }
        
        @Override
        public int read() throws IOException {
            ++this.mPosition;
            return this.mDataInputStream.read();
        }
        
        @Override
        public int read(final byte[] b, int read, final int len) throws IOException {
            read = this.mDataInputStream.read(b, read, len);
            this.mPosition += read;
            return read;
        }
        
        @Override
        public boolean readBoolean() throws IOException {
            ++this.mPosition;
            return this.mDataInputStream.readBoolean();
        }
        
        @Override
        public byte readByte() throws IOException {
            final int mPosition = this.mPosition + 1;
            this.mPosition = mPosition;
            if (mPosition > this.mLength) {
                throw new EOFException();
            }
            final int read = this.mDataInputStream.read();
            if (read >= 0) {
                return (byte)read;
            }
            throw new EOFException();
        }
        
        @Override
        public char readChar() throws IOException {
            this.mPosition += 2;
            return this.mDataInputStream.readChar();
        }
        
        @Override
        public double readDouble() throws IOException {
            return Double.longBitsToDouble(this.readLong());
        }
        
        @Override
        public float readFloat() throws IOException {
            return Float.intBitsToFloat(this.readInt());
        }
        
        @Override
        public void readFully(final byte[] b) throws IOException {
            final int mPosition = this.mPosition + b.length;
            this.mPosition = mPosition;
            if (mPosition > this.mLength) {
                throw new EOFException();
            }
            if (this.mDataInputStream.read(b, 0, b.length) == b.length) {
                return;
            }
            throw new IOException("Couldn't read up to the length of buffer");
        }
        
        @Override
        public void readFully(final byte[] b, final int off, final int len) throws IOException {
            final int mPosition = this.mPosition + len;
            this.mPosition = mPosition;
            if (mPosition > this.mLength) {
                throw new EOFException();
            }
            if (this.mDataInputStream.read(b, off, len) == len) {
                return;
            }
            throw new IOException("Couldn't read up to the length of buffer");
        }
        
        @Override
        public int readInt() throws IOException {
            final int mPosition = this.mPosition + 4;
            this.mPosition = mPosition;
            if (mPosition > this.mLength) {
                throw new EOFException();
            }
            final int read = this.mDataInputStream.read();
            final int read2 = this.mDataInputStream.read();
            final int read3 = this.mDataInputStream.read();
            final int read4 = this.mDataInputStream.read();
            if ((read | read2 | read3 | read4) < 0) {
                throw new EOFException();
            }
            final ByteOrder mByteOrder = this.mByteOrder;
            if (mByteOrder == ByteOrderedDataInputStream.LITTLE_ENDIAN) {
                return (read4 << 24) + (read3 << 16) + (read2 << 8) + read;
            }
            if (mByteOrder == ByteOrderedDataInputStream.BIG_ENDIAN) {
                return (read << 24) + (read2 << 16) + (read3 << 8) + read4;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid byte order: ");
            sb.append(this.mByteOrder);
            throw new IOException(sb.toString());
        }
        
        @Override
        public String readLine() throws IOException {
            return null;
        }
        
        @Override
        public long readLong() throws IOException {
            final int mPosition = this.mPosition + 8;
            this.mPosition = mPosition;
            if (mPosition > this.mLength) {
                throw new EOFException();
            }
            final int read = this.mDataInputStream.read();
            final int read2 = this.mDataInputStream.read();
            final int read3 = this.mDataInputStream.read();
            final int read4 = this.mDataInputStream.read();
            final int read5 = this.mDataInputStream.read();
            final int read6 = this.mDataInputStream.read();
            final int read7 = this.mDataInputStream.read();
            final int read8 = this.mDataInputStream.read();
            if ((read | read2 | read3 | read4 | read5 | read6 | read7 | read8) < 0) {
                throw new EOFException();
            }
            final ByteOrder mByteOrder = this.mByteOrder;
            if (mByteOrder == ByteOrderedDataInputStream.LITTLE_ENDIAN) {
                return ((long)read8 << 56) + ((long)read7 << 48) + ((long)read6 << 40) + ((long)read5 << 32) + ((long)read4 << 24) + ((long)read3 << 16) + ((long)read2 << 8) + read;
            }
            if (mByteOrder == ByteOrderedDataInputStream.BIG_ENDIAN) {
                return ((long)read << 56) + ((long)read2 << 48) + ((long)read3 << 40) + ((long)read4 << 32) + ((long)read5 << 24) + ((long)read6 << 16) + ((long)read7 << 8) + read8;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid byte order: ");
            sb.append(this.mByteOrder);
            throw new IOException(sb.toString());
        }
        
        @Override
        public short readShort() throws IOException {
            final int mPosition = this.mPosition + 2;
            this.mPosition = mPosition;
            if (mPosition > this.mLength) {
                throw new EOFException();
            }
            final int read = this.mDataInputStream.read();
            final int read2 = this.mDataInputStream.read();
            if ((read | read2) < 0) {
                throw new EOFException();
            }
            final ByteOrder mByteOrder = this.mByteOrder;
            if (mByteOrder == ByteOrderedDataInputStream.LITTLE_ENDIAN) {
                return (short)((read2 << 8) + read);
            }
            if (mByteOrder == ByteOrderedDataInputStream.BIG_ENDIAN) {
                return (short)((read << 8) + read2);
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid byte order: ");
            sb.append(this.mByteOrder);
            throw new IOException(sb.toString());
        }
        
        @Override
        public String readUTF() throws IOException {
            this.mPosition += 2;
            return this.mDataInputStream.readUTF();
        }
        
        @Override
        public int readUnsignedByte() throws IOException {
            ++this.mPosition;
            return this.mDataInputStream.readUnsignedByte();
        }
        
        public long readUnsignedInt() throws IOException {
            return (long)this.readInt() & 0xFFFFFFFFL;
        }
        
        @Override
        public int readUnsignedShort() throws IOException {
            final int mPosition = this.mPosition + 2;
            this.mPosition = mPosition;
            if (mPosition > this.mLength) {
                throw new EOFException();
            }
            final int read = this.mDataInputStream.read();
            final int read2 = this.mDataInputStream.read();
            if ((read | read2) < 0) {
                throw new EOFException();
            }
            final ByteOrder mByteOrder = this.mByteOrder;
            if (mByteOrder == ByteOrderedDataInputStream.LITTLE_ENDIAN) {
                return (read2 << 8) + read;
            }
            if (mByteOrder == ByteOrderedDataInputStream.BIG_ENDIAN) {
                return (read << 8) + read2;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid byte order: ");
            sb.append(this.mByteOrder);
            throw new IOException(sb.toString());
        }
        
        public void seek(long n) throws IOException {
            final int mPosition = this.mPosition;
            if (mPosition > n) {
                this.mPosition = 0;
                this.mDataInputStream.reset();
                this.mDataInputStream.mark(this.mLength);
            }
            else {
                n -= mPosition;
            }
            final int n2 = (int)n;
            if (this.skipBytes(n2) == n2) {
                return;
            }
            throw new IOException("Couldn't seek up to the byteCount");
        }
        
        public void setByteOrder(final ByteOrder mByteOrder) {
            this.mByteOrder = mByteOrder;
        }
        
        @Override
        public int skipBytes(int i) throws IOException {
            int min;
            for (min = Math.min(i, this.mLength - this.mPosition), i = 0; i < min; i += this.mDataInputStream.skipBytes(min - i)) {}
            this.mPosition += i;
            return i;
        }
    }
    
    private static class ByteOrderedDataOutputStream extends FilterOutputStream
    {
        private ByteOrder mByteOrder;
        final OutputStream mOutputStream;
        
        public ByteOrderedDataOutputStream(final OutputStream outputStream, final ByteOrder mByteOrder) {
            super(outputStream);
            this.mOutputStream = outputStream;
            this.mByteOrder = mByteOrder;
        }
        
        public void setByteOrder(final ByteOrder mByteOrder) {
            this.mByteOrder = mByteOrder;
        }
        
        @Override
        public void write(final byte[] b) throws IOException {
            this.mOutputStream.write(b);
        }
        
        @Override
        public void write(final byte[] b, final int off, final int len) throws IOException {
            this.mOutputStream.write(b, off, len);
        }
        
        public void writeByte(final int n) throws IOException {
            this.mOutputStream.write(n);
        }
        
        public void writeInt(final int n) throws IOException {
            final ByteOrder mByteOrder = this.mByteOrder;
            if (mByteOrder == ByteOrder.LITTLE_ENDIAN) {
                this.mOutputStream.write(n >>> 0 & 0xFF);
                this.mOutputStream.write(n >>> 8 & 0xFF);
                this.mOutputStream.write(n >>> 16 & 0xFF);
                this.mOutputStream.write(n >>> 24 & 0xFF);
            }
            else if (mByteOrder == ByteOrder.BIG_ENDIAN) {
                this.mOutputStream.write(n >>> 24 & 0xFF);
                this.mOutputStream.write(n >>> 16 & 0xFF);
                this.mOutputStream.write(n >>> 8 & 0xFF);
                this.mOutputStream.write(n >>> 0 & 0xFF);
            }
        }
        
        public void writeShort(final short n) throws IOException {
            final ByteOrder mByteOrder = this.mByteOrder;
            if (mByteOrder == ByteOrder.LITTLE_ENDIAN) {
                this.mOutputStream.write(n >>> 0 & 0xFF);
                this.mOutputStream.write(n >>> 8 & 0xFF);
            }
            else if (mByteOrder == ByteOrder.BIG_ENDIAN) {
                this.mOutputStream.write(n >>> 8 & 0xFF);
                this.mOutputStream.write(n >>> 0 & 0xFF);
            }
        }
        
        public void writeUnsignedInt(final long n) throws IOException {
            this.writeInt((int)n);
        }
        
        public void writeUnsignedShort(final int n) throws IOException {
            this.writeShort((short)n);
        }
    }
    
    private static class ExifAttribute
    {
        public static final long BYTES_OFFSET_UNKNOWN = -1L;
        public final byte[] bytes;
        public final long bytesOffset;
        public final int format;
        public final int numberOfComponents;
        
        ExifAttribute(final int format, final int numberOfComponents, final long bytesOffset, final byte[] bytes) {
            this.format = format;
            this.numberOfComponents = numberOfComponents;
            this.bytesOffset = bytesOffset;
            this.bytes = bytes;
        }
        
        ExifAttribute(final int n, final int n2, final byte[] array) {
            this(n, n2, -1L, array);
        }
        
        public static ExifAttribute createByte(final String s) {
            if (s.length() == 1 && s.charAt(0) >= '0' && s.charAt(0) <= '1') {
                return new ExifAttribute(1, 1, new byte[] { (byte)(s.charAt(0) - '0') });
            }
            final byte[] bytes = s.getBytes(ExifInterface.ASCII);
            return new ExifAttribute(1, bytes.length, bytes);
        }
        
        public static ExifAttribute createDouble(final double n, final ByteOrder byteOrder) {
            return createDouble(new double[] { n }, byteOrder);
        }
        
        public static ExifAttribute createDouble(final double[] array, final ByteOrder bo) {
            final ByteBuffer wrap = ByteBuffer.wrap(new byte[ExifInterface.IFD_FORMAT_BYTES_PER_FORMAT[12] * array.length]);
            wrap.order(bo);
            for (int length = array.length, i = 0; i < length; ++i) {
                wrap.putDouble(array[i]);
            }
            return new ExifAttribute(12, array.length, wrap.array());
        }
        
        public static ExifAttribute createSLong(final int n, final ByteOrder byteOrder) {
            return createSLong(new int[] { n }, byteOrder);
        }
        
        public static ExifAttribute createSLong(final int[] array, final ByteOrder bo) {
            final ByteBuffer wrap = ByteBuffer.wrap(new byte[ExifInterface.IFD_FORMAT_BYTES_PER_FORMAT[9] * array.length]);
            wrap.order(bo);
            for (int length = array.length, i = 0; i < length; ++i) {
                wrap.putInt(array[i]);
            }
            return new ExifAttribute(9, array.length, wrap.array());
        }
        
        public static ExifAttribute createSRational(final Rational rational, final ByteOrder byteOrder) {
            return createSRational(new Rational[] { rational }, byteOrder);
        }
        
        public static ExifAttribute createSRational(final Rational[] array, final ByteOrder bo) {
            final ByteBuffer wrap = ByteBuffer.wrap(new byte[ExifInterface.IFD_FORMAT_BYTES_PER_FORMAT[10] * array.length]);
            wrap.order(bo);
            for (final Rational rational : array) {
                wrap.putInt((int)rational.numerator);
                wrap.putInt((int)rational.denominator);
            }
            return new ExifAttribute(10, array.length, wrap.array());
        }
        
        public static ExifAttribute createString(final String str) {
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append('\0');
            final byte[] bytes = sb.toString().getBytes(ExifInterface.ASCII);
            return new ExifAttribute(2, bytes.length, bytes);
        }
        
        public static ExifAttribute createULong(final long n, final ByteOrder byteOrder) {
            return createULong(new long[] { n }, byteOrder);
        }
        
        public static ExifAttribute createULong(final long[] array, final ByteOrder bo) {
            final ByteBuffer wrap = ByteBuffer.wrap(new byte[ExifInterface.IFD_FORMAT_BYTES_PER_FORMAT[4] * array.length]);
            wrap.order(bo);
            for (int length = array.length, i = 0; i < length; ++i) {
                wrap.putInt((int)array[i]);
            }
            return new ExifAttribute(4, array.length, wrap.array());
        }
        
        public static ExifAttribute createURational(final Rational rational, final ByteOrder byteOrder) {
            return createURational(new Rational[] { rational }, byteOrder);
        }
        
        public static ExifAttribute createURational(final Rational[] array, final ByteOrder bo) {
            final ByteBuffer wrap = ByteBuffer.wrap(new byte[ExifInterface.IFD_FORMAT_BYTES_PER_FORMAT[5] * array.length]);
            wrap.order(bo);
            for (final Rational rational : array) {
                wrap.putInt((int)rational.numerator);
                wrap.putInt((int)rational.denominator);
            }
            return new ExifAttribute(5, array.length, wrap.array());
        }
        
        public static ExifAttribute createUShort(final int n, final ByteOrder byteOrder) {
            return createUShort(new int[] { n }, byteOrder);
        }
        
        public static ExifAttribute createUShort(final int[] array, final ByteOrder bo) {
            final ByteBuffer wrap = ByteBuffer.wrap(new byte[ExifInterface.IFD_FORMAT_BYTES_PER_FORMAT[3] * array.length]);
            wrap.order(bo);
            for (int length = array.length, i = 0; i < length; ++i) {
                wrap.putShort((short)array[i]);
            }
            return new ExifAttribute(3, array.length, wrap.array());
        }
        
        public double getDoubleValue(final ByteOrder byteOrder) {
            final Object value = this.getValue(byteOrder);
            if (value == null) {
                throw new NumberFormatException("NULL can't be converted to a double value");
            }
            if (value instanceof String) {
                return Double.parseDouble((String)value);
            }
            if (value instanceof long[]) {
                final long[] array = (long[])value;
                if (array.length == 1) {
                    return (double)array[0];
                }
                throw new NumberFormatException("There are more than one component");
            }
            else if (value instanceof int[]) {
                final int[] array2 = (int[])value;
                if (array2.length == 1) {
                    return array2[0];
                }
                throw new NumberFormatException("There are more than one component");
            }
            else if (value instanceof double[]) {
                final double[] array3 = (double[])value;
                if (array3.length == 1) {
                    return array3[0];
                }
                throw new NumberFormatException("There are more than one component");
            }
            else {
                if (!(value instanceof Rational[])) {
                    throw new NumberFormatException("Couldn't find a double value");
                }
                final Rational[] array4 = (Rational[])value;
                if (array4.length == 1) {
                    return array4[0].calculate();
                }
                throw new NumberFormatException("There are more than one component");
            }
        }
        
        public int getIntValue(final ByteOrder byteOrder) {
            final Object value = this.getValue(byteOrder);
            if (value == null) {
                throw new NumberFormatException("NULL can't be converted to a integer value");
            }
            if (value instanceof String) {
                return Integer.parseInt((String)value);
            }
            if (value instanceof long[]) {
                final long[] array = (long[])value;
                if (array.length == 1) {
                    return (int)array[0];
                }
                throw new NumberFormatException("There are more than one component");
            }
            else {
                if (!(value instanceof int[])) {
                    throw new NumberFormatException("Couldn't find a integer value");
                }
                final int[] array2 = (int[])value;
                if (array2.length == 1) {
                    return array2[0];
                }
                throw new NumberFormatException("There are more than one component");
            }
        }
        
        public String getStringValue(final ByteOrder byteOrder) {
            final Object value = this.getValue(byteOrder);
            if (value == null) {
                return null;
            }
            if (value instanceof String) {
                return (String)value;
            }
            final StringBuilder sb = new StringBuilder();
            final boolean b = value instanceof long[];
            final int n = 0;
            final int n2 = 0;
            int i = 0;
            final int n3 = 0;
            if (b) {
                final long[] array = (long[])value;
                int n4;
                for (int j = n3; j < array.length; j = n4) {
                    sb.append(array[j]);
                    n4 = j + 1;
                    if ((j = n4) != array.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            }
            if (value instanceof int[]) {
                final int[] array2 = (int[])value;
                int n5;
                for (int k = n; k < array2.length; k = n5) {
                    sb.append(array2[k]);
                    n5 = k + 1;
                    if ((k = n5) != array2.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            }
            if (value instanceof double[]) {
                final double[] array3 = (double[])value;
                int n6;
                for (int l = n2; l < array3.length; l = n6) {
                    sb.append(array3[l]);
                    n6 = l + 1;
                    if ((l = n6) != array3.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            }
            if (value instanceof Rational[]) {
                int n7;
                for (Rational[] array4 = (Rational[])value; i < array4.length; i = n7) {
                    sb.append(array4[i].numerator);
                    sb.append('/');
                    sb.append(array4[i].denominator);
                    n7 = i + 1;
                    if ((i = n7) != array4.length) {
                        sb.append(",");
                    }
                }
                return sb.toString();
            }
            return null;
        }
        
        Object getValue(final ByteOrder p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     3: new             Landroidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream;
            //     6: astore          13
            //     8: aload           13
            //    10: aload_0        
            //    11: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.bytes:[B
            //    14: invokespecial   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.<init>:([B)V
            //    17: aload           13
            //    19: aload_1        
            //    20: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.setByteOrder:(Ljava/nio/ByteOrder;)V
            //    23: aload_0        
            //    24: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.format:I
            //    27: istore          12
            //    29: iconst_0       
            //    30: istore          9
            //    32: iconst_0       
            //    33: istore_2       
            //    34: iconst_0       
            //    35: istore          10
            //    37: iconst_0       
            //    38: istore          8
            //    40: iconst_0       
            //    41: istore          11
            //    43: iconst_0       
            //    44: istore          6
            //    46: iconst_0       
            //    47: istore          7
            //    49: iconst_0       
            //    50: istore          4
            //    52: iconst_0       
            //    53: istore_3       
            //    54: iconst_1       
            //    55: istore          5
            //    57: iload           12
            //    59: tableswitch {
            //                2: 593
            //                3: 460
            //                4: 421
            //                5: 382
            //                6: 330
            //                7: 593
            //                8: 460
            //                9: 291
            //               10: 252
            //               11: 201
            //               12: 161
            //               13: 123
            //          default: 120
            //        }
            //   120: goto            662
            //   123: aload_0        
            //   124: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   127: newarray        D
            //   129: astore_1       
            //   130: iload_3        
            //   131: istore_2       
            //   132: iload_2        
            //   133: aload_0        
            //   134: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   137: if_icmpge       154
            //   140: aload_1        
            //   141: iload_2        
            //   142: aload           13
            //   144: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.readDouble:()D
            //   147: dastore        
            //   148: iinc            2, 1
            //   151: goto            132
            //   154: aload           13
            //   156: invokevirtual   java/io/InputStream.close:()V
            //   159: aload_1        
            //   160: areturn        
            //   161: aload_0        
            //   162: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   165: newarray        D
            //   167: astore_1       
            //   168: iload           9
            //   170: istore_2       
            //   171: iload_2        
            //   172: aload_0        
            //   173: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   176: if_icmpge       194
            //   179: aload_1        
            //   180: iload_2        
            //   181: aload           13
            //   183: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.readFloat:()F
            //   186: f2d            
            //   187: dastore        
            //   188: iinc            2, 1
            //   191: goto            171
            //   194: aload           13
            //   196: invokevirtual   java/io/InputStream.close:()V
            //   199: aload_1        
            //   200: areturn        
            //   201: aload_0        
            //   202: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   205: anewarray       Landroidx/exifinterface/media/ExifInterface$Rational;
            //   208: astore_1       
            //   209: iload_2        
            //   210: aload_0        
            //   211: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   214: if_icmpge       245
            //   217: aload_1        
            //   218: iload_2        
            //   219: new             Landroidx/exifinterface/media/ExifInterface$Rational;
            //   222: dup            
            //   223: aload           13
            //   225: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.readInt:()I
            //   228: i2l            
            //   229: aload           13
            //   231: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.readInt:()I
            //   234: i2l            
            //   235: invokespecial   androidx/exifinterface/media/ExifInterface$Rational.<init>:(JJ)V
            //   238: aastore        
            //   239: iinc            2, 1
            //   242: goto            209
            //   245: aload           13
            //   247: invokevirtual   java/io/InputStream.close:()V
            //   250: aload_1        
            //   251: areturn        
            //   252: aload_0        
            //   253: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   256: newarray        I
            //   258: astore_1       
            //   259: iload           10
            //   261: istore_2       
            //   262: iload_2        
            //   263: aload_0        
            //   264: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   267: if_icmpge       284
            //   270: aload_1        
            //   271: iload_2        
            //   272: aload           13
            //   274: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.readInt:()I
            //   277: iastore        
            //   278: iinc            2, 1
            //   281: goto            262
            //   284: aload           13
            //   286: invokevirtual   java/io/InputStream.close:()V
            //   289: aload_1        
            //   290: areturn        
            //   291: aload_0        
            //   292: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   295: newarray        I
            //   297: astore_1       
            //   298: iload           8
            //   300: istore_2       
            //   301: iload_2        
            //   302: aload_0        
            //   303: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   306: if_icmpge       323
            //   309: aload_1        
            //   310: iload_2        
            //   311: aload           13
            //   313: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.readShort:()S
            //   316: iastore        
            //   317: iinc            2, 1
            //   320: goto            301
            //   323: aload           13
            //   325: invokevirtual   java/io/InputStream.close:()V
            //   328: aload_1        
            //   329: areturn        
            //   330: aload_0        
            //   331: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   334: anewarray       Landroidx/exifinterface/media/ExifInterface$Rational;
            //   337: astore_1       
            //   338: iload           11
            //   340: istore_2       
            //   341: iload_2        
            //   342: aload_0        
            //   343: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   346: if_icmpge       375
            //   349: aload_1        
            //   350: iload_2        
            //   351: new             Landroidx/exifinterface/media/ExifInterface$Rational;
            //   354: dup            
            //   355: aload           13
            //   357: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.readUnsignedInt:()J
            //   360: aload           13
            //   362: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.readUnsignedInt:()J
            //   365: invokespecial   androidx/exifinterface/media/ExifInterface$Rational.<init>:(JJ)V
            //   368: aastore        
            //   369: iinc            2, 1
            //   372: goto            341
            //   375: aload           13
            //   377: invokevirtual   java/io/InputStream.close:()V
            //   380: aload_1        
            //   381: areturn        
            //   382: aload_0        
            //   383: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   386: newarray        J
            //   388: astore_1       
            //   389: iload           6
            //   391: istore_2       
            //   392: iload_2        
            //   393: aload_0        
            //   394: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   397: if_icmpge       414
            //   400: aload_1        
            //   401: iload_2        
            //   402: aload           13
            //   404: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.readUnsignedInt:()J
            //   407: lastore        
            //   408: iinc            2, 1
            //   411: goto            392
            //   414: aload           13
            //   416: invokevirtual   java/io/InputStream.close:()V
            //   419: aload_1        
            //   420: areturn        
            //   421: aload_0        
            //   422: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   425: newarray        I
            //   427: astore_1       
            //   428: iload           7
            //   430: istore_2       
            //   431: iload_2        
            //   432: aload_0        
            //   433: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   436: if_icmpge       453
            //   439: aload_1        
            //   440: iload_2        
            //   441: aload           13
            //   443: invokevirtual   androidx/exifinterface/media/ExifInterface$ByteOrderedDataInputStream.readUnsignedShort:()I
            //   446: iastore        
            //   447: iinc            2, 1
            //   450: goto            431
            //   453: aload           13
            //   455: invokevirtual   java/io/InputStream.close:()V
            //   458: aload_1        
            //   459: areturn        
            //   460: iload           4
            //   462: istore_2       
            //   463: aload_0        
            //   464: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   467: getstatic       androidx/exifinterface/media/ExifInterface.EXIF_ASCII_PREFIX:[B
            //   470: arraylength    
            //   471: if_icmplt       522
            //   474: iconst_0       
            //   475: istore_2       
            //   476: getstatic       androidx/exifinterface/media/ExifInterface.EXIF_ASCII_PREFIX:[B
            //   479: astore_1       
            //   480: iload           5
            //   482: istore_3       
            //   483: iload_2        
            //   484: aload_1        
            //   485: arraylength    
            //   486: if_icmpge       512
            //   489: aload_0        
            //   490: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.bytes:[B
            //   493: iload_2        
            //   494: baload         
            //   495: aload_1        
            //   496: iload_2        
            //   497: baload         
            //   498: if_icmpeq       506
            //   501: iconst_0       
            //   502: istore_3       
            //   503: goto            512
            //   506: iinc            2, 1
            //   509: goto            476
            //   512: iload           4
            //   514: istore_2       
            //   515: iload_3        
            //   516: ifeq            522
            //   519: aload_1        
            //   520: arraylength    
            //   521: istore_2       
            //   522: new             Ljava/lang/StringBuilder;
            //   525: astore_1       
            //   526: aload_1        
            //   527: invokespecial   java/lang/StringBuilder.<init>:()V
            //   530: iload_2        
            //   531: aload_0        
            //   532: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.numberOfComponents:I
            //   535: if_icmpge       581
            //   538: aload_0        
            //   539: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.bytes:[B
            //   542: iload_2        
            //   543: baload         
            //   544: istore_3       
            //   545: iload_3        
            //   546: ifne            552
            //   549: goto            581
            //   552: iload_3        
            //   553: bipush          32
            //   555: if_icmplt       568
            //   558: aload_1        
            //   559: iload_3        
            //   560: i2c            
            //   561: invokevirtual   java/lang/StringBuilder.append:(C)Ljava/lang/StringBuilder;
            //   564: pop            
            //   565: goto            575
            //   568: aload_1        
            //   569: bipush          63
            //   571: invokevirtual   java/lang/StringBuilder.append:(C)Ljava/lang/StringBuilder;
            //   574: pop            
            //   575: iinc            2, 1
            //   578: goto            530
            //   581: aload_1        
            //   582: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //   585: astore_1       
            //   586: aload           13
            //   588: invokevirtual   java/io/InputStream.close:()V
            //   591: aload_1        
            //   592: areturn        
            //   593: aload_0        
            //   594: getfield        androidx/exifinterface/media/ExifInterface$ExifAttribute.bytes:[B
            //   597: astore_1       
            //   598: aload_1        
            //   599: arraylength    
            //   600: iconst_1       
            //   601: if_icmpne       643
            //   604: aload_1        
            //   605: iconst_0       
            //   606: baload         
            //   607: istore_2       
            //   608: iload_2        
            //   609: iflt            643
            //   612: iload_2        
            //   613: iconst_1       
            //   614: if_icmpgt       643
            //   617: new             Ljava/lang/String;
            //   620: dup            
            //   621: iconst_1       
            //   622: newarray        C
            //   624: dup            
            //   625: iconst_0       
            //   626: iload_2        
            //   627: bipush          48
            //   629: iadd           
            //   630: i2c            
            //   631: castore        
            //   632: invokespecial   java/lang/String.<init>:([C)V
            //   635: astore_1       
            //   636: aload           13
            //   638: invokevirtual   java/io/InputStream.close:()V
            //   641: aload_1        
            //   642: areturn        
            //   643: new             Ljava/lang/String;
            //   646: dup            
            //   647: aload_1        
            //   648: getstatic       androidx/exifinterface/media/ExifInterface.ASCII:Ljava/nio/charset/Charset;
            //   651: invokespecial   java/lang/String.<init>:([BLjava/nio/charset/Charset;)V
            //   654: astore_1       
            //   655: aload           13
            //   657: invokevirtual   java/io/InputStream.close:()V
            //   660: aload_1        
            //   661: areturn        
            //   662: aload           13
            //   664: invokevirtual   java/io/InputStream.close:()V
            //   667: aconst_null    
            //   668: areturn        
            //   669: astore_1       
            //   670: goto            678
            //   673: astore_1       
            //   674: aload           14
            //   676: astore          13
            //   678: aload           13
            //   680: ifnull          688
            //   683: aload           13
            //   685: invokevirtual   java/io/InputStream.close:()V
            //   688: aload_1        
            //   689: athrow         
            //   690: astore_1       
            //   691: aconst_null    
            //   692: astore          13
            //   694: aload           13
            //   696: ifnull          704
            //   699: aload           13
            //   701: invokevirtual   java/io/InputStream.close:()V
            //   704: aconst_null    
            //   705: areturn        
            //   706: astore_1       
            //   707: goto            694
            //   710: astore          13
            //   712: goto            159
            //   715: astore          13
            //   717: goto            199
            //   720: astore          13
            //   722: goto            250
            //   725: astore          13
            //   727: goto            289
            //   730: astore          13
            //   732: goto            328
            //   735: astore          13
            //   737: goto            380
            //   740: astore          13
            //   742: goto            419
            //   745: astore          13
            //   747: goto            458
            //   750: astore          13
            //   752: goto            591
            //   755: astore          13
            //   757: goto            641
            //   760: astore          13
            //   762: goto            660
            //   765: astore_1       
            //   766: goto            667
            //   769: astore          13
            //   771: goto            688
            //   774: astore_1       
            //   775: goto            704
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                 
            //  -----  -----  -----  -----  ---------------------
            //  3      17     690    694    Ljava/io/IOException;
            //  3      17     673    678    Any
            //  17     29     706    710    Ljava/io/IOException;
            //  17     29     669    673    Any
            //  123    130    706    710    Ljava/io/IOException;
            //  123    130    669    673    Any
            //  132    148    706    710    Ljava/io/IOException;
            //  132    148    669    673    Any
            //  154    159    710    715    Ljava/io/IOException;
            //  161    168    706    710    Ljava/io/IOException;
            //  161    168    669    673    Any
            //  171    188    706    710    Ljava/io/IOException;
            //  171    188    669    673    Any
            //  194    199    715    720    Ljava/io/IOException;
            //  201    209    706    710    Ljava/io/IOException;
            //  201    209    669    673    Any
            //  209    239    706    710    Ljava/io/IOException;
            //  209    239    669    673    Any
            //  245    250    720    725    Ljava/io/IOException;
            //  252    259    706    710    Ljava/io/IOException;
            //  252    259    669    673    Any
            //  262    278    706    710    Ljava/io/IOException;
            //  262    278    669    673    Any
            //  284    289    725    730    Ljava/io/IOException;
            //  291    298    706    710    Ljava/io/IOException;
            //  291    298    669    673    Any
            //  301    317    706    710    Ljava/io/IOException;
            //  301    317    669    673    Any
            //  323    328    730    735    Ljava/io/IOException;
            //  330    338    706    710    Ljava/io/IOException;
            //  330    338    669    673    Any
            //  341    369    706    710    Ljava/io/IOException;
            //  341    369    669    673    Any
            //  375    380    735    740    Ljava/io/IOException;
            //  382    389    706    710    Ljava/io/IOException;
            //  382    389    669    673    Any
            //  392    408    706    710    Ljava/io/IOException;
            //  392    408    669    673    Any
            //  414    419    740    745    Ljava/io/IOException;
            //  421    428    706    710    Ljava/io/IOException;
            //  421    428    669    673    Any
            //  431    447    706    710    Ljava/io/IOException;
            //  431    447    669    673    Any
            //  453    458    745    750    Ljava/io/IOException;
            //  463    474    706    710    Ljava/io/IOException;
            //  463    474    669    673    Any
            //  476    480    706    710    Ljava/io/IOException;
            //  476    480    669    673    Any
            //  483    501    706    710    Ljava/io/IOException;
            //  483    501    669    673    Any
            //  519    522    706    710    Ljava/io/IOException;
            //  519    522    669    673    Any
            //  522    530    706    710    Ljava/io/IOException;
            //  522    530    669    673    Any
            //  530    545    706    710    Ljava/io/IOException;
            //  530    545    669    673    Any
            //  558    565    706    710    Ljava/io/IOException;
            //  558    565    669    673    Any
            //  568    575    706    710    Ljava/io/IOException;
            //  568    575    669    673    Any
            //  581    586    706    710    Ljava/io/IOException;
            //  581    586    669    673    Any
            //  586    591    750    755    Ljava/io/IOException;
            //  593    604    706    710    Ljava/io/IOException;
            //  593    604    669    673    Any
            //  617    636    706    710    Ljava/io/IOException;
            //  617    636    669    673    Any
            //  636    641    755    760    Ljava/io/IOException;
            //  643    655    706    710    Ljava/io/IOException;
            //  643    655    669    673    Any
            //  655    660    760    765    Ljava/io/IOException;
            //  662    667    765    769    Ljava/io/IOException;
            //  683    688    769    774    Ljava/io/IOException;
            //  699    704    774    778    Ljava/io/IOException;
            // 
            // The error that occurred was:
            // 
            // java.lang.IndexOutOfBoundsException: Index 388 out of bounds for length 388
            //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
            //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
            //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
            //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
            //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
            //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
            //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        public int size() {
            return ExifInterface.IFD_FORMAT_BYTES_PER_FORMAT[this.format] * this.numberOfComponents;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("(");
            sb.append(ExifInterface.IFD_FORMAT_NAMES[this.format]);
            sb.append(", data length:");
            sb.append(this.bytes.length);
            sb.append(")");
            return sb.toString();
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface ExifStreamType {
    }
    
    static class ExifTag
    {
        public final String name;
        public final int number;
        public final int primaryFormat;
        public final int secondaryFormat;
        
        ExifTag(final String name, final int number, final int primaryFormat) {
            this.name = name;
            this.number = number;
            this.primaryFormat = primaryFormat;
            this.secondaryFormat = -1;
        }
        
        ExifTag(final String name, final int number, final int primaryFormat, final int secondaryFormat) {
            this.name = name;
            this.number = number;
            this.primaryFormat = primaryFormat;
            this.secondaryFormat = secondaryFormat;
        }
        
        boolean isFormatCompatible(final int n) {
            final int primaryFormat = this.primaryFormat;
            if (primaryFormat != 7) {
                if (n != 7) {
                    if (primaryFormat != n) {
                        final int secondaryFormat = this.secondaryFormat;
                        if (secondaryFormat != n) {
                            return ((primaryFormat == 4 || secondaryFormat == 4) && n == 3) || ((primaryFormat == 9 || secondaryFormat == 9) && n == 8) || ((primaryFormat == 12 || secondaryFormat == 12) && n == 11);
                        }
                    }
                }
            }
            return true;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface IfdType {
    }
    
    private static class Rational
    {
        public final long denominator;
        public final long numerator;
        
        Rational(final double n) {
            this((long)(n * 10000.0), 10000L);
        }
        
        Rational(final long numerator, final long denominator) {
            if (denominator == 0L) {
                this.numerator = 0L;
                this.denominator = 1L;
                return;
            }
            this.numerator = numerator;
            this.denominator = denominator;
        }
        
        public double calculate() {
            return this.numerator / (double)this.denominator;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.numerator);
            sb.append("/");
            sb.append(this.denominator);
            return sb.toString();
        }
    }
}

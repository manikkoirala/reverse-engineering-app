// 
// Decompiled by Procyon v0.6.0
// 

package androidx.vectordrawable.graphics.drawable;

import android.animation.ValueAnimator;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff$Mode;
import java.util.Collection;
import android.animation.AnimatorListenerAdapter;
import android.content.res.TypedArray;
import androidx.core.content.res.TypedArrayUtils;
import android.graphics.Region;
import android.graphics.Rect;
import android.graphics.drawable.Drawable$ConstantState;
import android.graphics.ColorFilter;
import android.graphics.Canvas;
import androidx.core.graphics.drawable.DrawableCompat;
import android.animation.TypeEvaluator;
import android.animation.ObjectAnimator;
import android.animation.AnimatorSet;
import androidx.collection.ArrayMap;
import android.animation.Animator;
import androidx.annotation.RequiresApi;
import android.content.res.Resources$Theme;
import android.util.AttributeSet;
import android.content.res.XmlResourceParser;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParser;
import android.util.Xml;
import androidx.core.content.res.ResourcesCompat;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.os.Build$VERSION;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;
import android.content.res.Resources;
import android.content.Context;
import android.graphics.drawable.Drawable$Callback;
import android.animation.ArgbEvaluator;
import android.animation.Animator$AnimatorListener;
import java.util.ArrayList;

public class AnimatedVectorDrawableCompat extends VectorDrawableCommon implements Animatable2Compat
{
    private static final String ANIMATED_VECTOR = "animated-vector";
    private static final boolean DBG_ANIMATION_VECTOR_DRAWABLE = false;
    private static final String LOGTAG = "AnimatedVDCompat";
    private static final String TARGET = "target";
    private AnimatedVectorDrawableCompatState mAnimatedVectorState;
    ArrayList<AnimationCallback> mAnimationCallbacks;
    private Animator$AnimatorListener mAnimatorListener;
    private ArgbEvaluator mArgbEvaluator;
    AnimatedVectorDrawableDelegateState mCachedConstantStateDelegate;
    final Drawable$Callback mCallback;
    private Context mContext;
    
    AnimatedVectorDrawableCompat() {
        this(null, null, null);
    }
    
    private AnimatedVectorDrawableCompat(@Nullable final Context context) {
        this(context, null, null);
    }
    
    private AnimatedVectorDrawableCompat(@Nullable final Context mContext, @Nullable final AnimatedVectorDrawableCompatState mAnimatedVectorState, @Nullable final Resources resources) {
        this.mArgbEvaluator = null;
        this.mAnimatorListener = null;
        this.mAnimationCallbacks = null;
        final Drawable$Callback mCallback = (Drawable$Callback)new Drawable$Callback() {
            final AnimatedVectorDrawableCompat this$0;
            
            public void invalidateDrawable(final Drawable drawable) {
                this.this$0.invalidateSelf();
            }
            
            public void scheduleDrawable(final Drawable drawable, final Runnable runnable, final long n) {
                this.this$0.scheduleSelf(runnable, n);
            }
            
            public void unscheduleDrawable(final Drawable drawable, final Runnable runnable) {
                this.this$0.unscheduleSelf(runnable);
            }
        };
        this.mCallback = (Drawable$Callback)mCallback;
        this.mContext = mContext;
        if (mAnimatedVectorState != null) {
            this.mAnimatedVectorState = mAnimatedVectorState;
        }
        else {
            this.mAnimatedVectorState = new AnimatedVectorDrawableCompatState(mContext, mAnimatedVectorState, (Drawable$Callback)mCallback, resources);
        }
    }
    
    public static void clearAnimationCallbacks(final Drawable drawable) {
        if (!(drawable instanceof Animatable)) {
            return;
        }
        if (Build$VERSION.SDK_INT >= 24) {
            \u3007o00\u3007\u3007Oo.\u3007080((AnimatedVectorDrawable)drawable);
        }
        else {
            ((AnimatedVectorDrawableCompat)drawable).clearAnimationCallbacks();
        }
    }
    
    @Nullable
    public static AnimatedVectorDrawableCompat create(@NonNull final Context context, @DrawableRes int next) {
        if (Build$VERSION.SDK_INT >= 24) {
            final AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat(context);
            (animatedVectorDrawableCompat.mDelegateDrawable = ResourcesCompat.getDrawable(context.getResources(), next, context.getTheme())).setCallback(animatedVectorDrawableCompat.mCallback);
            animatedVectorDrawableCompat.mCachedConstantStateDelegate = new AnimatedVectorDrawableDelegateState(animatedVectorDrawableCompat.mDelegateDrawable.getConstantState());
            return animatedVectorDrawableCompat;
        }
        final Resources resources = context.getResources();
        try {
            final XmlResourceParser xml = resources.getXml(next);
            final AttributeSet attributeSet = Xml.asAttributeSet((XmlPullParser)xml);
            do {
                next = ((XmlPullParser)xml).next();
            } while (next != 2 && next != 1);
            if (next == 2) {
                return createFromXmlInner(context, context.getResources(), (XmlPullParser)xml, attributeSet, context.getTheme());
            }
            throw new XmlPullParserException("No start tag found");
        }
        catch (final XmlPullParserException | IOException ex) {
            return null;
        }
    }
    
    public static AnimatedVectorDrawableCompat createFromXmlInner(final Context context, final Resources resources, final XmlPullParser xmlPullParser, final AttributeSet set, final Resources$Theme resources$Theme) throws XmlPullParserException, IOException {
        final AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat(context);
        animatedVectorDrawableCompat.inflate(resources, xmlPullParser, set, resources$Theme);
        return animatedVectorDrawableCompat;
    }
    
    public static void registerAnimationCallback(final Drawable drawable, final AnimationCallback animationCallback) {
        if (drawable != null) {
            if (animationCallback != null) {
                if (!(drawable instanceof Animatable)) {
                    return;
                }
                if (Build$VERSION.SDK_INT >= 24) {
                    registerPlatformCallback((AnimatedVectorDrawable)drawable, animationCallback);
                }
                else {
                    ((AnimatedVectorDrawableCompat)drawable).registerAnimationCallback(animationCallback);
                }
            }
        }
    }
    
    @RequiresApi(23)
    private static void registerPlatformCallback(@NonNull final AnimatedVectorDrawable animatedVectorDrawable, @NonNull final AnimationCallback animationCallback) {
        \u3007080.\u3007080(animatedVectorDrawable, animationCallback.getPlatformCallback());
    }
    
    private void removeAnimatorSetListener() {
        final Animator$AnimatorListener mAnimatorListener = this.mAnimatorListener;
        if (mAnimatorListener != null) {
            ((Animator)this.mAnimatedVectorState.mAnimatorSet).removeListener(mAnimatorListener);
            this.mAnimatorListener = null;
        }
    }
    
    private void setupAnimatorsForTarget(final String s, final Animator e) {
        e.setTarget(this.mAnimatedVectorState.mVectorDrawable.getTargetByName(s));
        final AnimatedVectorDrawableCompatState mAnimatedVectorState = this.mAnimatedVectorState;
        if (mAnimatedVectorState.mAnimators == null) {
            mAnimatedVectorState.mAnimators = new ArrayList<Animator>();
            this.mAnimatedVectorState.mTargetNameMap = new ArrayMap<Animator, String>();
        }
        this.mAnimatedVectorState.mAnimators.add(e);
        this.mAnimatedVectorState.mTargetNameMap.put(e, s);
    }
    
    private void setupColorAnimator(final Animator animator) {
        if (animator instanceof AnimatorSet) {
            final ArrayList childAnimations = ((AnimatorSet)animator).getChildAnimations();
            if (childAnimations != null) {
                for (int i = 0; i < childAnimations.size(); ++i) {
                    this.setupColorAnimator((Animator)childAnimations.get(i));
                }
            }
        }
        if (animator instanceof ObjectAnimator) {
            final ObjectAnimator objectAnimator = (ObjectAnimator)animator;
            final String propertyName = objectAnimator.getPropertyName();
            if ("fillColor".equals(propertyName) || "strokeColor".equals(propertyName)) {
                if (this.mArgbEvaluator == null) {
                    this.mArgbEvaluator = new ArgbEvaluator();
                }
                ((ValueAnimator)objectAnimator).setEvaluator((TypeEvaluator)this.mArgbEvaluator);
            }
        }
    }
    
    public static boolean unregisterAnimationCallback(final Drawable drawable, final AnimationCallback animationCallback) {
        if (drawable == null || animationCallback == null) {
            return false;
        }
        if (!(drawable instanceof Animatable)) {
            return false;
        }
        if (Build$VERSION.SDK_INT >= 24) {
            return unregisterPlatformCallback((AnimatedVectorDrawable)drawable, animationCallback);
        }
        return ((AnimatedVectorDrawableCompat)drawable).unregisterAnimationCallback(animationCallback);
    }
    
    @RequiresApi(23)
    private static boolean unregisterPlatformCallback(final AnimatedVectorDrawable animatedVectorDrawable, final AnimationCallback animationCallback) {
        return \u3007o\u3007.\u3007080(animatedVectorDrawable, animationCallback.getPlatformCallback());
    }
    
    @Override
    public void applyTheme(final Resources$Theme resources$Theme) {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            DrawableCompat.applyTheme(mDelegateDrawable, resources$Theme);
        }
    }
    
    public boolean canApplyTheme() {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        return mDelegateDrawable != null && DrawableCompat.canApplyTheme(mDelegateDrawable);
    }
    
    @Override
    public void clearAnimationCallbacks() {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            \u3007o00\u3007\u3007Oo.\u3007080((AnimatedVectorDrawable)mDelegateDrawable);
            return;
        }
        this.removeAnimatorSetListener();
        final ArrayList<AnimationCallback> mAnimationCallbacks = this.mAnimationCallbacks;
        if (mAnimationCallbacks == null) {
            return;
        }
        mAnimationCallbacks.clear();
    }
    
    public void draw(final Canvas canvas) {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            mDelegateDrawable.draw(canvas);
            return;
        }
        this.mAnimatedVectorState.mVectorDrawable.draw(canvas);
        if (this.mAnimatedVectorState.mAnimatorSet.isStarted()) {
            this.invalidateSelf();
        }
    }
    
    public int getAlpha() {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            return DrawableCompat.getAlpha(mDelegateDrawable);
        }
        return this.mAnimatedVectorState.mVectorDrawable.getAlpha();
    }
    
    public int getChangingConfigurations() {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            return mDelegateDrawable.getChangingConfigurations();
        }
        return super.getChangingConfigurations() | this.mAnimatedVectorState.mChangingConfigurations;
    }
    
    public ColorFilter getColorFilter() {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            return DrawableCompat.getColorFilter(mDelegateDrawable);
        }
        return this.mAnimatedVectorState.mVectorDrawable.getColorFilter();
    }
    
    public Drawable$ConstantState getConstantState() {
        if (super.mDelegateDrawable != null && Build$VERSION.SDK_INT >= 24) {
            return new AnimatedVectorDrawableDelegateState(super.mDelegateDrawable.getConstantState());
        }
        return null;
    }
    
    public int getIntrinsicHeight() {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            return mDelegateDrawable.getIntrinsicHeight();
        }
        return this.mAnimatedVectorState.mVectorDrawable.getIntrinsicHeight();
    }
    
    public int getIntrinsicWidth() {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            return mDelegateDrawable.getIntrinsicWidth();
        }
        return this.mAnimatedVectorState.mVectorDrawable.getIntrinsicWidth();
    }
    
    public int getOpacity() {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            return mDelegateDrawable.getOpacity();
        }
        return this.mAnimatedVectorState.mVectorDrawable.getOpacity();
    }
    
    public void inflate(final Resources resources, final XmlPullParser xmlPullParser, final AttributeSet set) throws XmlPullParserException, IOException {
        this.inflate(resources, xmlPullParser, set, null);
    }
    
    public void inflate(final Resources resources, final XmlPullParser xmlPullParser, final AttributeSet set, final Resources$Theme resources$Theme) throws XmlPullParserException, IOException {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            DrawableCompat.inflate(mDelegateDrawable, resources, xmlPullParser, set, resources$Theme);
            return;
        }
        for (int n = xmlPullParser.getEventType(), depth = xmlPullParser.getDepth(); n != 1 && (xmlPullParser.getDepth() >= depth + 1 || n != 3); n = xmlPullParser.next()) {
            if (n == 2) {
                final String name = xmlPullParser.getName();
                if ("animated-vector".equals(name)) {
                    final TypedArray obtainAttributes = TypedArrayUtils.obtainAttributes(resources, resources$Theme, set, AndroidResources.STYLEABLE_ANIMATED_VECTOR_DRAWABLE);
                    final int resourceId = obtainAttributes.getResourceId(0, 0);
                    if (resourceId != 0) {
                        final VectorDrawableCompat create = VectorDrawableCompat.create(resources, resourceId, resources$Theme);
                        create.setAllowCaching(false);
                        create.setCallback(this.mCallback);
                        final VectorDrawableCompat mVectorDrawable = this.mAnimatedVectorState.mVectorDrawable;
                        if (mVectorDrawable != null) {
                            mVectorDrawable.setCallback((Drawable$Callback)null);
                        }
                        this.mAnimatedVectorState.mVectorDrawable = create;
                    }
                    obtainAttributes.recycle();
                }
                else if ("target".equals(name)) {
                    final TypedArray obtainAttributes2 = resources.obtainAttributes(set, AndroidResources.STYLEABLE_ANIMATED_VECTOR_DRAWABLE_TARGET);
                    final String string = obtainAttributes2.getString(0);
                    final int resourceId2 = obtainAttributes2.getResourceId(1, 0);
                    if (resourceId2 != 0) {
                        final Context mContext = this.mContext;
                        if (mContext == null) {
                            obtainAttributes2.recycle();
                            throw new IllegalStateException("Context can't be null when inflating animators");
                        }
                        this.setupAnimatorsForTarget(string, AnimatorInflaterCompat.loadAnimator(mContext, resourceId2));
                    }
                    obtainAttributes2.recycle();
                }
            }
        }
        this.mAnimatedVectorState.setupAnimatorSet();
    }
    
    public boolean isAutoMirrored() {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            return DrawableCompat.isAutoMirrored(mDelegateDrawable);
        }
        return this.mAnimatedVectorState.mVectorDrawable.isAutoMirrored();
    }
    
    public boolean isRunning() {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            return ((AnimatedVectorDrawable)mDelegateDrawable).isRunning();
        }
        return this.mAnimatedVectorState.mAnimatorSet.isRunning();
    }
    
    public boolean isStateful() {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            return mDelegateDrawable.isStateful();
        }
        return this.mAnimatedVectorState.mVectorDrawable.isStateful();
    }
    
    public Drawable mutate() {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            mDelegateDrawable.mutate();
        }
        return this;
    }
    
    @Override
    protected void onBoundsChange(final Rect rect) {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            mDelegateDrawable.setBounds(rect);
            return;
        }
        this.mAnimatedVectorState.mVectorDrawable.setBounds(rect);
    }
    
    @Override
    protected boolean onLevelChange(final int n) {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            return mDelegateDrawable.setLevel(n);
        }
        return this.mAnimatedVectorState.mVectorDrawable.setLevel(n);
    }
    
    protected boolean onStateChange(final int[] array) {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            return mDelegateDrawable.setState(array);
        }
        return this.mAnimatedVectorState.mVectorDrawable.setState(array);
    }
    
    @Override
    public void registerAnimationCallback(@NonNull final AnimationCallback animationCallback) {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            registerPlatformCallback((AnimatedVectorDrawable)mDelegateDrawable, animationCallback);
            return;
        }
        if (animationCallback == null) {
            return;
        }
        if (this.mAnimationCallbacks == null) {
            this.mAnimationCallbacks = new ArrayList<AnimationCallback>();
        }
        if (this.mAnimationCallbacks.contains(animationCallback)) {
            return;
        }
        this.mAnimationCallbacks.add(animationCallback);
        if (this.mAnimatorListener == null) {
            this.mAnimatorListener = (Animator$AnimatorListener)new AnimatorListenerAdapter(this) {
                final AnimatedVectorDrawableCompat this$0;
                
                public void onAnimationEnd(final Animator animator) {
                    final ArrayList list = new ArrayList((Collection<? extends E>)this.this$0.mAnimationCallbacks);
                    for (int size = list.size(), i = 0; i < size; ++i) {
                        ((AnimationCallback)list.get(i)).onAnimationEnd(this.this$0);
                    }
                }
                
                public void onAnimationStart(final Animator animator) {
                    final ArrayList list = new ArrayList((Collection<? extends E>)this.this$0.mAnimationCallbacks);
                    for (int size = list.size(), i = 0; i < size; ++i) {
                        ((AnimationCallback)list.get(i)).onAnimationStart(this.this$0);
                    }
                }
            };
        }
        ((Animator)this.mAnimatedVectorState.mAnimatorSet).addListener(this.mAnimatorListener);
    }
    
    public void setAlpha(final int n) {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            mDelegateDrawable.setAlpha(n);
            return;
        }
        this.mAnimatedVectorState.mVectorDrawable.setAlpha(n);
    }
    
    public void setAutoMirrored(final boolean autoMirrored) {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            DrawableCompat.setAutoMirrored(mDelegateDrawable, autoMirrored);
            return;
        }
        this.mAnimatedVectorState.mVectorDrawable.setAutoMirrored(autoMirrored);
    }
    
    public void setColorFilter(final ColorFilter colorFilter) {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            mDelegateDrawable.setColorFilter(colorFilter);
            return;
        }
        this.mAnimatedVectorState.mVectorDrawable.setColorFilter(colorFilter);
    }
    
    public void setTint(final int tint) {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            DrawableCompat.setTint(mDelegateDrawable, tint);
            return;
        }
        this.mAnimatedVectorState.mVectorDrawable.setTint(tint);
    }
    
    public void setTintList(final ColorStateList tintList) {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            DrawableCompat.setTintList(mDelegateDrawable, tintList);
            return;
        }
        this.mAnimatedVectorState.mVectorDrawable.setTintList(tintList);
    }
    
    public void setTintMode(final PorterDuff$Mode tintMode) {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            DrawableCompat.setTintMode(mDelegateDrawable, tintMode);
            return;
        }
        this.mAnimatedVectorState.mVectorDrawable.setTintMode(tintMode);
    }
    
    public boolean setVisible(final boolean b, final boolean b2) {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            return mDelegateDrawable.setVisible(b, b2);
        }
        this.mAnimatedVectorState.mVectorDrawable.setVisible(b, b2);
        return super.setVisible(b, b2);
    }
    
    public void start() {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            ((AnimatedVectorDrawable)mDelegateDrawable).start();
            return;
        }
        if (this.mAnimatedVectorState.mAnimatorSet.isStarted()) {
            return;
        }
        this.mAnimatedVectorState.mAnimatorSet.start();
        this.invalidateSelf();
    }
    
    public void stop() {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            ((AnimatedVectorDrawable)mDelegateDrawable).stop();
            return;
        }
        this.mAnimatedVectorState.mAnimatorSet.end();
    }
    
    @Override
    public boolean unregisterAnimationCallback(@NonNull final AnimationCallback o) {
        final Drawable mDelegateDrawable = super.mDelegateDrawable;
        if (mDelegateDrawable != null) {
            unregisterPlatformCallback((AnimatedVectorDrawable)mDelegateDrawable, o);
        }
        final ArrayList<AnimationCallback> mAnimationCallbacks = this.mAnimationCallbacks;
        if (mAnimationCallbacks != null && o != null) {
            final boolean remove = mAnimationCallbacks.remove(o);
            if (this.mAnimationCallbacks.size() == 0) {
                this.removeAnimatorSetListener();
            }
            return remove;
        }
        return false;
    }
    
    private static class AnimatedVectorDrawableCompatState extends Drawable$ConstantState
    {
        AnimatorSet mAnimatorSet;
        ArrayList<Animator> mAnimators;
        int mChangingConfigurations;
        ArrayMap<Animator, String> mTargetNameMap;
        VectorDrawableCompat mVectorDrawable;
        
        public AnimatedVectorDrawableCompatState(final Context context, final AnimatedVectorDrawableCompatState animatedVectorDrawableCompatState, final Drawable$Callback callback, final Resources resources) {
            if (animatedVectorDrawableCompatState != null) {
                this.mChangingConfigurations = animatedVectorDrawableCompatState.mChangingConfigurations;
                final VectorDrawableCompat mVectorDrawable = animatedVectorDrawableCompatState.mVectorDrawable;
                int i = 0;
                if (mVectorDrawable != null) {
                    final Drawable$ConstantState constantState = mVectorDrawable.getConstantState();
                    if (resources != null) {
                        this.mVectorDrawable = (VectorDrawableCompat)constantState.newDrawable(resources);
                    }
                    else {
                        this.mVectorDrawable = (VectorDrawableCompat)constantState.newDrawable();
                    }
                    (this.mVectorDrawable = (VectorDrawableCompat)this.mVectorDrawable.mutate()).setCallback(callback);
                    this.mVectorDrawable.setBounds(animatedVectorDrawableCompatState.mVectorDrawable.getBounds());
                    this.mVectorDrawable.setAllowCaching(false);
                }
                final ArrayList<Animator> mAnimators = animatedVectorDrawableCompatState.mAnimators;
                if (mAnimators != null) {
                    final int size = mAnimators.size();
                    this.mAnimators = new ArrayList<Animator>(size);
                    this.mTargetNameMap = new ArrayMap<Animator, String>(size);
                    while (i < size) {
                        final Animator animator = animatedVectorDrawableCompatState.mAnimators.get(i);
                        final Animator clone = animator.clone();
                        final String s = animatedVectorDrawableCompatState.mTargetNameMap.get(animator);
                        clone.setTarget(this.mVectorDrawable.getTargetByName(s));
                        this.mAnimators.add(clone);
                        this.mTargetNameMap.put(clone, s);
                        ++i;
                    }
                    this.setupAnimatorSet();
                }
            }
        }
        
        public int getChangingConfigurations() {
            return this.mChangingConfigurations;
        }
        
        public Drawable newDrawable() {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }
        
        public Drawable newDrawable(final Resources resources) {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }
        
        public void setupAnimatorSet() {
            if (this.mAnimatorSet == null) {
                this.mAnimatorSet = new AnimatorSet();
            }
            this.mAnimatorSet.playTogether((Collection)this.mAnimators);
        }
    }
    
    @RequiresApi(24)
    private static class AnimatedVectorDrawableDelegateState extends Drawable$ConstantState
    {
        private final Drawable$ConstantState mDelegateState;
        
        public AnimatedVectorDrawableDelegateState(final Drawable$ConstantState mDelegateState) {
            this.mDelegateState = mDelegateState;
        }
        
        public boolean canApplyTheme() {
            return this.mDelegateState.canApplyTheme();
        }
        
        public int getChangingConfigurations() {
            return this.mDelegateState.getChangingConfigurations();
        }
        
        public Drawable newDrawable() {
            final AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat();
            (animatedVectorDrawableCompat.mDelegateDrawable = this.mDelegateState.newDrawable()).setCallback(animatedVectorDrawableCompat.mCallback);
            return animatedVectorDrawableCompat;
        }
        
        public Drawable newDrawable(final Resources resources) {
            final AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat();
            (animatedVectorDrawableCompat.mDelegateDrawable = this.mDelegateState.newDrawable(resources)).setCallback(animatedVectorDrawableCompat.mCallback);
            return animatedVectorDrawableCompat;
        }
        
        public Drawable newDrawable(final Resources resources, final Resources$Theme resources$Theme) {
            final AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat();
            (animatedVectorDrawableCompat.mDelegateDrawable = this.mDelegateState.newDrawable(resources, resources$Theme)).setCallback(animatedVectorDrawableCompat.mCallback);
            return animatedVectorDrawableCompat;
        }
    }
}

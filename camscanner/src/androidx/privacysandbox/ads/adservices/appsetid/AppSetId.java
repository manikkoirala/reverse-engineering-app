// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.appsetid;

import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class AppSetId
{
    @NotNull
    public static final Companion Companion;
    public static final int SCOPE_APP = 1;
    public static final int SCOPE_DEVELOPER = 2;
    @NotNull
    private final String id;
    private final int scope;
    
    static {
        Companion = new Companion(null);
    }
    
    public AppSetId(@NotNull final String id, final int scope) {
        Intrinsics.checkNotNullParameter((Object)id, "id");
        this.id = id;
        this.scope = scope;
        int n = 1;
        if (scope != 1) {
            if (scope == 2) {
                n = n;
            }
            else {
                n = 0;
            }
        }
        if (n != 0) {
            return;
        }
        throw new IllegalArgumentException("Scope undefined.".toString());
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof AppSetId)) {
            return false;
        }
        final String id = this.id;
        final AppSetId appSetId = (AppSetId)o;
        if (!Intrinsics.\u3007o\u3007((Object)id, (Object)appSetId.id) || this.scope != appSetId.scope) {
            b = false;
        }
        return b;
    }
    
    @NotNull
    public final String getId() {
        return this.id;
    }
    
    public final int getScope() {
        return this.scope;
    }
    
    @Override
    public int hashCode() {
        return this.id.hashCode() * 31 + this.scope;
    }
    
    @NotNull
    @Override
    public String toString() {
        String str;
        if (this.scope == 1) {
            str = "SCOPE_APP";
        }
        else {
            str = "SCOPE_DEVELOPER";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("AppSetId: id=");
        sb.append(this.id);
        sb.append(", scope=");
        sb.append(str);
        return sb.toString();
    }
    
    @Metadata
    public static final class Companion
    {
        private Companion() {
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.appsetid;

import androidx.privacysandbox.ads.adservices.internal.AdServicesInfo;
import androidx.annotation.DoNotInline;
import kotlin.ResultKt;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import android.os.OutcomeReceiver;
import java.util.concurrent.Executor;
import androidx.core.os.OutcomeReceiverKt;
import androidx.privacysandbox.ads.adservices.adid.\u3007o00\u3007\u3007Oo;
import kotlinx.coroutines.CancellableContinuationImpl;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import androidx.appcompat.widget.oo88o8O;
import kotlin.jvm.internal.Intrinsics;
import androidx.annotation.RequiresExtension;
import kotlin.coroutines.Continuation;
import android.annotation.SuppressLint;
import android.content.Context;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public abstract class AppSetIdManager
{
    @NotNull
    public static final Companion Companion;
    
    static {
        Companion = new Companion(null);
    }
    
    @SuppressLint({ "NewApi", "ClassVerificationFailure" })
    public static final AppSetIdManager obtain(@NotNull final Context context) {
        return AppSetIdManager.Companion.obtain(context);
    }
    
    public abstract Object getAppSetId(@NotNull final Continuation<? super AppSetId> p0);
    
    @Metadata
    @SuppressLint({ "ClassVerificationFailure", "NewApi" })
    @RequiresExtension(extension = 1000000, version = 4)
    private static final class Api33Ext4Impl extends AppSetIdManager
    {
        @NotNull
        private final android.adservices.appsetid.AppSetIdManager mAppSetIdManager;
        
        public Api33Ext4Impl(@NotNull final android.adservices.appsetid.AppSetIdManager mAppSetIdManager) {
            Intrinsics.checkNotNullParameter((Object)mAppSetIdManager, "mAppSetIdManager");
            this.mAppSetIdManager = mAppSetIdManager;
        }
        
        public Api33Ext4Impl(@NotNull final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            final Object \u3007080 = oo88o8O.\u3007080(context, (Class)android.adservices.appsetid.AppSetIdManager.class);
            Intrinsics.checkNotNullExpressionValue(\u3007080, "context.getSystemService\u2026:class.java\n            )");
            this((android.adservices.appsetid.AppSetIdManager)\u3007080);
        }
        
        public static final /* synthetic */ android.adservices.appsetid.AppSetIdManager access$getMAppSetIdManager$p(final Api33Ext4Impl api33Ext4Impl) {
            return api33Ext4Impl.mAppSetIdManager;
        }
        
        private final AppSetId convertResponse(final android.adservices.appsetid.AppSetId appSetId) {
            if (appSetId.getScope() == 1) {
                final String id = appSetId.getId();
                Intrinsics.checkNotNullExpressionValue((Object)id, "response.id");
                return new AppSetId(id, 1);
            }
            final String id2 = appSetId.getId();
            Intrinsics.checkNotNullExpressionValue((Object)id2, "response.id");
            return new AppSetId(id2, 2);
        }
        
        private final Object getAppSetIdAsyncInternal(final Continuation<? super android.adservices.appsetid.AppSetId> continuation) {
            final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.\u3007o\u3007((Continuation)continuation), 1);
            cancellableContinuationImpl.O8ooOoo\u3007();
            access$getMAppSetIdManager$p(this).getAppSetId((Executor)new \u3007o00\u3007\u3007Oo(), (OutcomeReceiver)OutcomeReceiverKt.asOutcomeReceiver((kotlin.coroutines.Continuation<? super Object>)cancellableContinuationImpl));
            final Object o\u3007O8\u3007\u3007o = cancellableContinuationImpl.o\u3007O8\u3007\u3007o();
            if (o\u3007O8\u3007\u3007o == IntrinsicsKt.O8()) {
                DebugProbesKt.\u3007o\u3007((Continuation)continuation);
            }
            return o\u3007O8\u3007\u3007o;
        }
        
        @DoNotInline
        @Override
        public Object getAppSetId(@NotNull final Continuation<? super AppSetId> continuation) {
            Object o = null;
            Label_0047: {
                if (continuation instanceof AppSetIdManager$Api33Ext4Impl$getAppSetId.AppSetIdManager$Api33Ext4Impl$getAppSetId$1) {
                    final AppSetIdManager$Api33Ext4Impl$getAppSetId.AppSetIdManager$Api33Ext4Impl$getAppSetId$1 appSetIdManager$Api33Ext4Impl$getAppSetId$1 = (AppSetIdManager$Api33Ext4Impl$getAppSetId.AppSetIdManager$Api33Ext4Impl$getAppSetId$1)continuation;
                    final int label = appSetIdManager$Api33Ext4Impl$getAppSetId$1.label;
                    if ((label & Integer.MIN_VALUE) != 0x0) {
                        appSetIdManager$Api33Ext4Impl$getAppSetId$1.label = label + Integer.MIN_VALUE;
                        o = appSetIdManager$Api33Ext4Impl$getAppSetId$1;
                        break Label_0047;
                    }
                }
                o = new AppSetIdManager$Api33Ext4Impl$getAppSetId.AppSetIdManager$Api33Ext4Impl$getAppSetId$1(this, (Continuation)continuation);
            }
            final Object result = ((AppSetIdManager$Api33Ext4Impl$getAppSetId.AppSetIdManager$Api33Ext4Impl$getAppSetId$1)o).result;
            final Object o2 = IntrinsicsKt.O8();
            final int label2 = ((AppSetIdManager$Api33Ext4Impl$getAppSetId.AppSetIdManager$Api33Ext4Impl$getAppSetId$1)o).label;
            Api33Ext4Impl api33Ext4Impl;
            Object appSetIdAsyncInternal;
            if (label2 != 0) {
                if (label2 != 1) {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                api33Ext4Impl = (Api33Ext4Impl)((AppSetIdManager$Api33Ext4Impl$getAppSetId.AppSetIdManager$Api33Ext4Impl$getAppSetId$1)o).L$0;
                ResultKt.\u3007o00\u3007\u3007Oo(result);
                appSetIdAsyncInternal = result;
            }
            else {
                ResultKt.\u3007o00\u3007\u3007Oo(result);
                ((AppSetIdManager$Api33Ext4Impl$getAppSetId.AppSetIdManager$Api33Ext4Impl$getAppSetId$1)o).L$0 = this;
                ((AppSetIdManager$Api33Ext4Impl$getAppSetId.AppSetIdManager$Api33Ext4Impl$getAppSetId$1)o).label = 1;
                appSetIdAsyncInternal = this.getAppSetIdAsyncInternal((Continuation<? super android.adservices.appsetid.AppSetId>)o);
                if (appSetIdAsyncInternal == o2) {
                    return o2;
                }
                api33Ext4Impl = this;
            }
            return api33Ext4Impl.convertResponse((android.adservices.appsetid.AppSetId)appSetIdAsyncInternal);
        }
    }
    
    @Metadata
    public static final class Companion
    {
        private Companion() {
        }
        
        @SuppressLint({ "NewApi", "ClassVerificationFailure" })
        public final AppSetIdManager obtain(@NotNull final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Api33Ext4Impl api33Ext4Impl;
            if (AdServicesInfo.INSTANCE.version() >= 4) {
                api33Ext4Impl = new Api33Ext4Impl(context);
            }
            else {
                api33Ext4Impl = null;
            }
            return api33Ext4Impl;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.common;

import kotlin.jvm.internal.Intrinsics;
import android.net.Uri;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class AdData
{
    @NotNull
    private final String metadata;
    @NotNull
    private final Uri renderUri;
    
    public AdData(@NotNull final Uri renderUri, @NotNull final String metadata) {
        Intrinsics.checkNotNullParameter((Object)renderUri, "renderUri");
        Intrinsics.checkNotNullParameter((Object)metadata, "metadata");
        this.renderUri = renderUri;
        this.metadata = metadata;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof AdData)) {
            return false;
        }
        final Uri renderUri = this.renderUri;
        final AdData adData = (AdData)o;
        if (!Intrinsics.\u3007o\u3007((Object)renderUri, (Object)adData.renderUri) || !Intrinsics.\u3007o\u3007((Object)this.metadata, (Object)adData.metadata)) {
            b = false;
        }
        return b;
    }
    
    @NotNull
    public final String getMetadata() {
        return this.metadata;
    }
    
    @NotNull
    public final Uri getRenderUri() {
        return this.renderUri;
    }
    
    @Override
    public int hashCode() {
        return this.renderUri.hashCode() * 31 + this.metadata.hashCode();
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("AdData: renderUri=");
        sb.append(this.renderUri);
        sb.append(", metadata='");
        sb.append(this.metadata);
        sb.append('\'');
        return sb.toString();
    }
}

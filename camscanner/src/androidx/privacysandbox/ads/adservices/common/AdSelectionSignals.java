// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.common;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class AdSelectionSignals
{
    @NotNull
    private final String signals;
    
    public AdSelectionSignals(@NotNull final String signals) {
        Intrinsics.checkNotNullParameter((Object)signals, "signals");
        this.signals = signals;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof AdSelectionSignals && Intrinsics.\u3007o\u3007((Object)this.signals, (Object)((AdSelectionSignals)o).signals));
    }
    
    @NotNull
    public final String getSignals() {
        return this.signals;
    }
    
    @Override
    public int hashCode() {
        return this.signals.hashCode();
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("AdSelectionSignals: ");
        sb.append(this.signals);
        return sb.toString();
    }
}

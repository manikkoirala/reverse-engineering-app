// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.common;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class AdTechIdentifier
{
    @NotNull
    private final String identifier;
    
    public AdTechIdentifier(@NotNull final String identifier) {
        Intrinsics.checkNotNullParameter((Object)identifier, "identifier");
        this.identifier = identifier;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof AdTechIdentifier && Intrinsics.\u3007o\u3007((Object)this.identifier, (Object)((AdTechIdentifier)o).identifier));
    }
    
    @NotNull
    public final String getIdentifier() {
        return this.identifier;
    }
    
    @Override
    public int hashCode() {
        return this.identifier.hashCode();
    }
    
    @NotNull
    @Override
    public String toString() {
        return String.valueOf(this.identifier);
    }
}

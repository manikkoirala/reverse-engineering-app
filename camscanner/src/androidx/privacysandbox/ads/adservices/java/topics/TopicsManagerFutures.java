// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.java.topics;

import androidx.annotation.DoNotInline;
import androidx.privacysandbox.ads.adservices.java.internal.CoroutineAdapterKt;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.BuildersKt;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineScopeKt;
import kotlinx.coroutines.Dispatchers;
import kotlin.jvm.internal.Intrinsics;
import androidx.privacysandbox.ads.adservices.topics.TopicsManager;
import androidx.annotation.RequiresPermission;
import androidx.privacysandbox.ads.adservices.topics.GetTopicsResponse;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.privacysandbox.ads.adservices.topics.GetTopicsRequest;
import android.content.Context;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public abstract class TopicsManagerFutures
{
    @NotNull
    public static final Companion Companion;
    
    static {
        Companion = new Companion(null);
    }
    
    public static final TopicsManagerFutures from(@NotNull final Context context) {
        return TopicsManagerFutures.Companion.from(context);
    }
    
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_TOPICS")
    @NotNull
    public abstract ListenableFuture<GetTopicsResponse> getTopicsAsync(@NotNull final GetTopicsRequest p0);
    
    @Metadata
    private static final class Api33Ext4JavaImpl extends TopicsManagerFutures
    {
        @NotNull
        private final TopicsManager mTopicsManager;
        
        public Api33Ext4JavaImpl(@NotNull final TopicsManager mTopicsManager) {
            Intrinsics.checkNotNullParameter((Object)mTopicsManager, "mTopicsManager");
            this.mTopicsManager = mTopicsManager;
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_ADSERVICES_TOPICS")
        @NotNull
        @Override
        public ListenableFuture<GetTopicsResponse> getTopicsAsync(@NotNull final GetTopicsRequest getTopicsRequest) {
            Intrinsics.checkNotNullParameter((Object)getTopicsRequest, "request");
            return (ListenableFuture<GetTopicsResponse>)CoroutineAdapterKt.asListenableFuture$default(BuildersKt.\u3007o00\u3007\u3007Oo(CoroutineScopeKt.\u3007080((CoroutineContext)Dispatchers.\u3007o\u3007()), (CoroutineContext)null, (CoroutineStart)null, (Function2)new TopicsManagerFutures$Api33Ext4JavaImpl$getTopicsAsync.TopicsManagerFutures$Api33Ext4JavaImpl$getTopicsAsync$1(this, getTopicsRequest, (Continuation)null), 3, (Object)null), null, 1, null);
        }
    }
    
    @Metadata
    public static final class Companion
    {
        private Companion() {
        }
        
        public final TopicsManagerFutures from(@NotNull final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            final TopicsManager obtain = TopicsManager.Companion.obtain(context);
            TopicsManagerFutures topicsManagerFutures;
            if (obtain != null) {
                topicsManagerFutures = new Api33Ext4JavaImpl(obtain);
            }
            else {
                topicsManagerFutures = null;
            }
            return topicsManagerFutures;
        }
    }
}

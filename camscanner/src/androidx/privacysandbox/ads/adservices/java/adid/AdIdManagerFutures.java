// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.java.adid;

import androidx.annotation.DoNotInline;
import androidx.privacysandbox.ads.adservices.java.internal.CoroutineAdapterKt;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.BuildersKt;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineScopeKt;
import kotlinx.coroutines.Dispatchers;
import kotlin.jvm.internal.Intrinsics;
import androidx.privacysandbox.ads.adservices.adid.AdIdManager;
import androidx.annotation.RequiresPermission;
import androidx.privacysandbox.ads.adservices.adid.AdId;
import com.google.common.util.concurrent.ListenableFuture;
import android.content.Context;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public abstract class AdIdManagerFutures
{
    @NotNull
    public static final Companion Companion;
    
    static {
        Companion = new Companion(null);
    }
    
    public static final AdIdManagerFutures from(@NotNull final Context context) {
        return AdIdManagerFutures.Companion.from(context);
    }
    
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_AD_ID")
    @NotNull
    public abstract ListenableFuture<AdId> getAdIdAsync();
    
    @Metadata
    private static final class Api33Ext4JavaImpl extends AdIdManagerFutures
    {
        @NotNull
        private final AdIdManager mAdIdManager;
        
        public Api33Ext4JavaImpl(@NotNull final AdIdManager mAdIdManager) {
            Intrinsics.checkNotNullParameter((Object)mAdIdManager, "mAdIdManager");
            this.mAdIdManager = mAdIdManager;
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_ADSERVICES_AD_ID")
        @NotNull
        @Override
        public ListenableFuture<AdId> getAdIdAsync() {
            return (ListenableFuture<AdId>)CoroutineAdapterKt.asListenableFuture$default(BuildersKt.\u3007o00\u3007\u3007Oo(CoroutineScopeKt.\u3007080((CoroutineContext)Dispatchers.\u3007080()), (CoroutineContext)null, (CoroutineStart)null, (Function2)new AdIdManagerFutures$Api33Ext4JavaImpl$getAdIdAsync.AdIdManagerFutures$Api33Ext4JavaImpl$getAdIdAsync$1(this, (Continuation)null), 3, (Object)null), null, 1, null);
        }
    }
    
    @Metadata
    public static final class Companion
    {
        private Companion() {
        }
        
        public final AdIdManagerFutures from(@NotNull final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            final AdIdManager obtain = AdIdManager.Companion.obtain(context);
            AdIdManagerFutures adIdManagerFutures;
            if (obtain != null) {
                adIdManagerFutures = new Api33Ext4JavaImpl(obtain);
            }
            else {
                adIdManagerFutures = null;
            }
            return adIdManagerFutures;
        }
    }
}

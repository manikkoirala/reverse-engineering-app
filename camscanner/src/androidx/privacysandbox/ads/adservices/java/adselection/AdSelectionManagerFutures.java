// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.java.adselection;

import androidx.annotation.DoNotInline;
import androidx.privacysandbox.ads.adservices.java.internal.CoroutineAdapterKt;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.BuildersKt;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineScopeKt;
import kotlinx.coroutines.Dispatchers;
import kotlin.jvm.internal.Intrinsics;
import androidx.privacysandbox.ads.adservices.adselection.AdSelectionManager;
import androidx.privacysandbox.ads.adservices.adselection.AdSelectionOutcome;
import androidx.privacysandbox.ads.adservices.adselection.AdSelectionConfig;
import androidx.annotation.RequiresPermission;
import kotlin.Unit;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.privacysandbox.ads.adservices.adselection.ReportImpressionRequest;
import android.content.Context;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public abstract class AdSelectionManagerFutures
{
    @NotNull
    public static final Companion Companion;
    
    static {
        Companion = new Companion(null);
    }
    
    public static final AdSelectionManagerFutures from(@NotNull final Context context) {
        return AdSelectionManagerFutures.Companion.from(context);
    }
    
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_CUSTOM_AUDIENCE")
    @NotNull
    public abstract ListenableFuture<Unit> reportImpressionAsync(@NotNull final ReportImpressionRequest p0);
    
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_CUSTOM_AUDIENCE")
    @NotNull
    public abstract ListenableFuture<AdSelectionOutcome> selectAdsAsync(@NotNull final AdSelectionConfig p0);
    
    @Metadata
    private static final class Api33Ext4JavaImpl extends AdSelectionManagerFutures
    {
        private final AdSelectionManager mAdSelectionManager;
        
        public Api33Ext4JavaImpl(final AdSelectionManager mAdSelectionManager) {
            this.mAdSelectionManager = mAdSelectionManager;
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_ADSERVICES_CUSTOM_AUDIENCE")
        @NotNull
        @Override
        public ListenableFuture<Unit> reportImpressionAsync(@NotNull final ReportImpressionRequest reportImpressionRequest) {
            Intrinsics.checkNotNullParameter((Object)reportImpressionRequest, "reportImpressionRequest");
            return (ListenableFuture<Unit>)CoroutineAdapterKt.asListenableFuture$default(BuildersKt.\u3007o00\u3007\u3007Oo(CoroutineScopeKt.\u3007080((CoroutineContext)Dispatchers.\u3007080()), (CoroutineContext)null, (CoroutineStart)null, (Function2)new AdSelectionManagerFutures$Api33Ext4JavaImpl$reportImpressionAsync.AdSelectionManagerFutures$Api33Ext4JavaImpl$reportImpressionAsync$1(this, reportImpressionRequest, (Continuation)null), 3, (Object)null), null, 1, null);
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_ADSERVICES_CUSTOM_AUDIENCE")
        @NotNull
        @Override
        public ListenableFuture<AdSelectionOutcome> selectAdsAsync(@NotNull final AdSelectionConfig adSelectionConfig) {
            Intrinsics.checkNotNullParameter((Object)adSelectionConfig, "adSelectionConfig");
            return (ListenableFuture<AdSelectionOutcome>)CoroutineAdapterKt.asListenableFuture$default(BuildersKt.\u3007o00\u3007\u3007Oo(CoroutineScopeKt.\u3007080((CoroutineContext)Dispatchers.\u3007080()), (CoroutineContext)null, (CoroutineStart)null, (Function2)new AdSelectionManagerFutures$Api33Ext4JavaImpl$selectAdsAsync.AdSelectionManagerFutures$Api33Ext4JavaImpl$selectAdsAsync$1(this, adSelectionConfig, (Continuation)null), 3, (Object)null), null, 1, null);
        }
    }
    
    @Metadata
    public static final class Companion
    {
        private Companion() {
        }
        
        public final AdSelectionManagerFutures from(@NotNull final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            final AdSelectionManager obtain = AdSelectionManager.Companion.obtain(context);
            AdSelectionManagerFutures adSelectionManagerFutures;
            if (obtain != null) {
                adSelectionManagerFutures = new Api33Ext4JavaImpl(obtain);
            }
            else {
                adSelectionManagerFutures = null;
            }
            return adSelectionManagerFutures;
        }
    }
}

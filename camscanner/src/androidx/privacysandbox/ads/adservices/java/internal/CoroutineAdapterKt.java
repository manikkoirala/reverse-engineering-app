// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.java.internal;

import kotlinx.coroutines.Job;
import kotlin.jvm.functions.Function1;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import kotlin.jvm.internal.Intrinsics;
import com.google.common.util.concurrent.ListenableFuture;
import org.jetbrains.annotations.NotNull;
import kotlinx.coroutines.Deferred;
import kotlin.Metadata;

@Metadata
public final class CoroutineAdapterKt
{
    @NotNull
    public static final <T> ListenableFuture<T> asListenableFuture(@NotNull final Deferred<? extends T> deferred, final Object o) {
        Intrinsics.checkNotNullParameter((Object)deferred, "<this>");
        final com.google.common.util.concurrent.ListenableFuture<T> future = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<T>)new \u3007080(deferred, o));
        Intrinsics.checkNotNullExpressionValue((Object)future, "getFuture { completer ->\u2026        }\n    }\n    tag\n}");
        return future;
    }
    
    private static final Object asListenableFuture$lambda$0(final Deferred deferred, final Object o, final CallbackToFutureAdapter.Completer completer) {
        Intrinsics.checkNotNullParameter((Object)deferred, "$this_asListenableFuture");
        Intrinsics.checkNotNullParameter((Object)completer, "completer");
        ((Job)deferred).\u3007\u3007808\u3007((Function1)new CoroutineAdapterKt$asListenableFuture$1.CoroutineAdapterKt$asListenableFuture$1$1(completer, deferred));
        return o;
    }
}

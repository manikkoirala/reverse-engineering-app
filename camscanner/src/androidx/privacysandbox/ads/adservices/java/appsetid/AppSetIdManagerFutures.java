// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.java.appsetid;

import androidx.annotation.DoNotInline;
import androidx.privacysandbox.ads.adservices.java.internal.CoroutineAdapterKt;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.BuildersKt;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineScopeKt;
import kotlinx.coroutines.Dispatchers;
import kotlin.jvm.internal.Intrinsics;
import androidx.privacysandbox.ads.adservices.appsetid.AppSetIdManager;
import androidx.privacysandbox.ads.adservices.appsetid.AppSetId;
import com.google.common.util.concurrent.ListenableFuture;
import android.content.Context;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public abstract class AppSetIdManagerFutures
{
    @NotNull
    public static final Companion Companion;
    
    static {
        Companion = new Companion(null);
    }
    
    public static final AppSetIdManagerFutures from(@NotNull final Context context) {
        return AppSetIdManagerFutures.Companion.from(context);
    }
    
    @NotNull
    public abstract ListenableFuture<AppSetId> getAppSetIdAsync();
    
    @Metadata
    private static final class Api33Ext4JavaImpl extends AppSetIdManagerFutures
    {
        @NotNull
        private final AppSetIdManager mAppSetIdManager;
        
        public Api33Ext4JavaImpl(@NotNull final AppSetIdManager mAppSetIdManager) {
            Intrinsics.checkNotNullParameter((Object)mAppSetIdManager, "mAppSetIdManager");
            this.mAppSetIdManager = mAppSetIdManager;
        }
        
        @DoNotInline
        @NotNull
        @Override
        public ListenableFuture<AppSetId> getAppSetIdAsync() {
            return (ListenableFuture<AppSetId>)CoroutineAdapterKt.asListenableFuture$default(BuildersKt.\u3007o00\u3007\u3007Oo(CoroutineScopeKt.\u3007080((CoroutineContext)Dispatchers.\u3007080()), (CoroutineContext)null, (CoroutineStart)null, (Function2)new AppSetIdManagerFutures$Api33Ext4JavaImpl$getAppSetIdAsync.AppSetIdManagerFutures$Api33Ext4JavaImpl$getAppSetIdAsync$1(this, (Continuation)null), 3, (Object)null), null, 1, null);
        }
    }
    
    @Metadata
    public static final class Companion
    {
        private Companion() {
        }
        
        public final AppSetIdManagerFutures from(@NotNull final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            final AppSetIdManager obtain = AppSetIdManager.Companion.obtain(context);
            AppSetIdManagerFutures appSetIdManagerFutures;
            if (obtain != null) {
                appSetIdManagerFutures = new Api33Ext4JavaImpl(obtain);
            }
            else {
                appSetIdManagerFutures = null;
            }
            return appSetIdManagerFutures;
        }
    }
}

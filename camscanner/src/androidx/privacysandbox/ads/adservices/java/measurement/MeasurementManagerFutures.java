// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.java.measurement;

import androidx.annotation.DoNotInline;
import androidx.privacysandbox.ads.adservices.java.internal.CoroutineAdapterKt;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.BuildersKt;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineScopeKt;
import kotlinx.coroutines.Dispatchers;
import kotlin.jvm.internal.Intrinsics;
import androidx.privacysandbox.ads.adservices.measurement.MeasurementManager;
import androidx.privacysandbox.ads.adservices.measurement.WebTriggerRegistrationRequest;
import androidx.privacysandbox.ads.adservices.measurement.WebSourceRegistrationRequest;
import android.view.InputEvent;
import android.net.Uri;
import androidx.annotation.RequiresPermission;
import kotlin.Unit;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.privacysandbox.ads.adservices.measurement.DeletionRequest;
import android.content.Context;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public abstract class MeasurementManagerFutures
{
    @NotNull
    public static final Companion Companion;
    
    static {
        Companion = new Companion(null);
    }
    
    public static final MeasurementManagerFutures from(@NotNull final Context context) {
        return MeasurementManagerFutures.Companion.from(context);
    }
    
    @NotNull
    public abstract ListenableFuture<Unit> deleteRegistrationsAsync(@NotNull final DeletionRequest p0);
    
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_ATTRIBUTION")
    @NotNull
    public abstract ListenableFuture<Integer> getMeasurementApiStatusAsync();
    
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_ATTRIBUTION")
    @NotNull
    public abstract ListenableFuture<Unit> registerSourceAsync(@NotNull final Uri p0, final InputEvent p1);
    
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_ATTRIBUTION")
    @NotNull
    public abstract ListenableFuture<Unit> registerTriggerAsync(@NotNull final Uri p0);
    
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_ATTRIBUTION")
    @NotNull
    public abstract ListenableFuture<Unit> registerWebSourceAsync(@NotNull final WebSourceRegistrationRequest p0);
    
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_ATTRIBUTION")
    @NotNull
    public abstract ListenableFuture<Unit> registerWebTriggerAsync(@NotNull final WebTriggerRegistrationRequest p0);
    
    @Metadata
    private static final class Api33Ext5JavaImpl extends MeasurementManagerFutures
    {
        @NotNull
        private final MeasurementManager mMeasurementManager;
        
        public Api33Ext5JavaImpl(@NotNull final MeasurementManager mMeasurementManager) {
            Intrinsics.checkNotNullParameter((Object)mMeasurementManager, "mMeasurementManager");
            this.mMeasurementManager = mMeasurementManager;
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_ADSERVICES_ATTRIBUTION")
        @NotNull
        @Override
        public ListenableFuture<Unit> deleteRegistrationsAsync(@NotNull final DeletionRequest deletionRequest) {
            Intrinsics.checkNotNullParameter((Object)deletionRequest, "deletionRequest");
            return (ListenableFuture<Unit>)CoroutineAdapterKt.asListenableFuture$default(BuildersKt.\u3007o00\u3007\u3007Oo(CoroutineScopeKt.\u3007080((CoroutineContext)Dispatchers.\u3007080()), (CoroutineContext)null, (CoroutineStart)null, (Function2)new MeasurementManagerFutures$Api33Ext5JavaImpl$deleteRegistrationsAsync.MeasurementManagerFutures$Api33Ext5JavaImpl$deleteRegistrationsAsync$1(this, deletionRequest, (Continuation)null), 3, (Object)null), null, 1, null);
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_ADSERVICES_ATTRIBUTION")
        @NotNull
        @Override
        public ListenableFuture<Integer> getMeasurementApiStatusAsync() {
            return (ListenableFuture<Integer>)CoroutineAdapterKt.asListenableFuture$default(BuildersKt.\u3007o00\u3007\u3007Oo(CoroutineScopeKt.\u3007080((CoroutineContext)Dispatchers.\u3007080()), (CoroutineContext)null, (CoroutineStart)null, (Function2)new MeasurementManagerFutures$Api33Ext5JavaImpl$getMeasurementApiStatusAsync.MeasurementManagerFutures$Api33Ext5JavaImpl$getMeasurementApiStatusAsync$1(this, (Continuation)null), 3, (Object)null), null, 1, null);
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_ADSERVICES_ATTRIBUTION")
        @NotNull
        @Override
        public ListenableFuture<Unit> registerSourceAsync(@NotNull final Uri uri, final InputEvent inputEvent) {
            Intrinsics.checkNotNullParameter((Object)uri, "attributionSource");
            return (ListenableFuture<Unit>)CoroutineAdapterKt.asListenableFuture$default(BuildersKt.\u3007o00\u3007\u3007Oo(CoroutineScopeKt.\u3007080((CoroutineContext)Dispatchers.\u3007080()), (CoroutineContext)null, (CoroutineStart)null, (Function2)new MeasurementManagerFutures$Api33Ext5JavaImpl$registerSourceAsync.MeasurementManagerFutures$Api33Ext5JavaImpl$registerSourceAsync$1(this, uri, inputEvent, (Continuation)null), 3, (Object)null), null, 1, null);
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_ADSERVICES_ATTRIBUTION")
        @NotNull
        @Override
        public ListenableFuture<Unit> registerTriggerAsync(@NotNull final Uri uri) {
            Intrinsics.checkNotNullParameter((Object)uri, "trigger");
            return (ListenableFuture<Unit>)CoroutineAdapterKt.asListenableFuture$default(BuildersKt.\u3007o00\u3007\u3007Oo(CoroutineScopeKt.\u3007080((CoroutineContext)Dispatchers.\u3007080()), (CoroutineContext)null, (CoroutineStart)null, (Function2)new MeasurementManagerFutures$Api33Ext5JavaImpl$registerTriggerAsync.MeasurementManagerFutures$Api33Ext5JavaImpl$registerTriggerAsync$1(this, uri, (Continuation)null), 3, (Object)null), null, 1, null);
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_ADSERVICES_ATTRIBUTION")
        @NotNull
        @Override
        public ListenableFuture<Unit> registerWebSourceAsync(@NotNull final WebSourceRegistrationRequest webSourceRegistrationRequest) {
            Intrinsics.checkNotNullParameter((Object)webSourceRegistrationRequest, "request");
            return (ListenableFuture<Unit>)CoroutineAdapterKt.asListenableFuture$default(BuildersKt.\u3007o00\u3007\u3007Oo(CoroutineScopeKt.\u3007080((CoroutineContext)Dispatchers.\u3007080()), (CoroutineContext)null, (CoroutineStart)null, (Function2)new MeasurementManagerFutures$Api33Ext5JavaImpl$registerWebSourceAsync.MeasurementManagerFutures$Api33Ext5JavaImpl$registerWebSourceAsync$1(this, webSourceRegistrationRequest, (Continuation)null), 3, (Object)null), null, 1, null);
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_ADSERVICES_ATTRIBUTION")
        @NotNull
        @Override
        public ListenableFuture<Unit> registerWebTriggerAsync(@NotNull final WebTriggerRegistrationRequest webTriggerRegistrationRequest) {
            Intrinsics.checkNotNullParameter((Object)webTriggerRegistrationRequest, "request");
            return (ListenableFuture<Unit>)CoroutineAdapterKt.asListenableFuture$default(BuildersKt.\u3007o00\u3007\u3007Oo(CoroutineScopeKt.\u3007080((CoroutineContext)Dispatchers.\u3007080()), (CoroutineContext)null, (CoroutineStart)null, (Function2)new MeasurementManagerFutures$Api33Ext5JavaImpl$registerWebTriggerAsync.MeasurementManagerFutures$Api33Ext5JavaImpl$registerWebTriggerAsync$1(this, webTriggerRegistrationRequest, (Continuation)null), 3, (Object)null), null, 1, null);
        }
    }
    
    @Metadata
    public static final class Companion
    {
        private Companion() {
        }
        
        public final MeasurementManagerFutures from(@NotNull final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            final MeasurementManager obtain = MeasurementManager.Companion.obtain(context);
            MeasurementManagerFutures measurementManagerFutures;
            if (obtain != null) {
                measurementManagerFutures = new Api33Ext5JavaImpl(obtain);
            }
            else {
                measurementManagerFutures = null;
            }
            return measurementManagerFutures;
        }
    }
}

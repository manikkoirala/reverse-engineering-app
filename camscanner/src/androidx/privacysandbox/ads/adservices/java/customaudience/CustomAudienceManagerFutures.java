// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.java.customaudience;

import androidx.annotation.DoNotInline;
import androidx.privacysandbox.ads.adservices.java.internal.CoroutineAdapterKt;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.BuildersKt;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineScopeKt;
import kotlinx.coroutines.Dispatchers;
import kotlin.jvm.internal.Intrinsics;
import androidx.privacysandbox.ads.adservices.customaudience.CustomAudienceManager;
import androidx.privacysandbox.ads.adservices.customaudience.LeaveCustomAudienceRequest;
import androidx.annotation.RequiresPermission;
import kotlin.Unit;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.privacysandbox.ads.adservices.customaudience.JoinCustomAudienceRequest;
import android.content.Context;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public abstract class CustomAudienceManagerFutures
{
    @NotNull
    public static final Companion Companion;
    
    static {
        Companion = new Companion(null);
    }
    
    public static final CustomAudienceManagerFutures from(@NotNull final Context context) {
        return CustomAudienceManagerFutures.Companion.from(context);
    }
    
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_CUSTOM_AUDIENCE")
    @NotNull
    public abstract ListenableFuture<Unit> joinCustomAudienceAsync(@NotNull final JoinCustomAudienceRequest p0);
    
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_CUSTOM_AUDIENCE")
    @NotNull
    public abstract ListenableFuture<Unit> leaveCustomAudienceAsync(@NotNull final LeaveCustomAudienceRequest p0);
    
    @Metadata
    private static final class Api33Ext4JavaImpl extends CustomAudienceManagerFutures
    {
        private final CustomAudienceManager mCustomAudienceManager;
        
        public Api33Ext4JavaImpl(final CustomAudienceManager mCustomAudienceManager) {
            this.mCustomAudienceManager = mCustomAudienceManager;
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_ADSERVICES_CUSTOM_AUDIENCE")
        @NotNull
        @Override
        public ListenableFuture<Unit> joinCustomAudienceAsync(@NotNull final JoinCustomAudienceRequest joinCustomAudienceRequest) {
            Intrinsics.checkNotNullParameter((Object)joinCustomAudienceRequest, "request");
            return (ListenableFuture<Unit>)CoroutineAdapterKt.asListenableFuture$default(BuildersKt.\u3007o00\u3007\u3007Oo(CoroutineScopeKt.\u3007080((CoroutineContext)Dispatchers.\u3007080()), (CoroutineContext)null, (CoroutineStart)null, (Function2)new CustomAudienceManagerFutures$Api33Ext4JavaImpl$joinCustomAudienceAsync.CustomAudienceManagerFutures$Api33Ext4JavaImpl$joinCustomAudienceAsync$1(this, joinCustomAudienceRequest, (Continuation)null), 3, (Object)null), null, 1, null);
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_ADSERVICES_CUSTOM_AUDIENCE")
        @NotNull
        @Override
        public ListenableFuture<Unit> leaveCustomAudienceAsync(@NotNull final LeaveCustomAudienceRequest leaveCustomAudienceRequest) {
            Intrinsics.checkNotNullParameter((Object)leaveCustomAudienceRequest, "request");
            return (ListenableFuture<Unit>)CoroutineAdapterKt.asListenableFuture$default(BuildersKt.\u3007o00\u3007\u3007Oo(CoroutineScopeKt.\u3007080((CoroutineContext)Dispatchers.\u3007080()), (CoroutineContext)null, (CoroutineStart)null, (Function2)new CustomAudienceManagerFutures$Api33Ext4JavaImpl$leaveCustomAudienceAsync.CustomAudienceManagerFutures$Api33Ext4JavaImpl$leaveCustomAudienceAsync$1(this, leaveCustomAudienceRequest, (Continuation)null), 3, (Object)null), null, 1, null);
        }
    }
    
    @Metadata
    public static final class Companion
    {
        private Companion() {
        }
        
        public final CustomAudienceManagerFutures from(@NotNull final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            final CustomAudienceManager obtain = CustomAudienceManager.Companion.obtain(context);
            CustomAudienceManagerFutures customAudienceManagerFutures;
            if (obtain != null) {
                customAudienceManagerFutures = new Api33Ext4JavaImpl(obtain);
            }
            else {
                customAudienceManagerFutures = null;
            }
            return customAudienceManagerFutures;
        }
    }
}

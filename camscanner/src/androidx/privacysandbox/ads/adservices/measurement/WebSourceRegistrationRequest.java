// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.measurement;

import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import android.view.InputEvent;
import android.net.Uri;
import androidx.annotation.RequiresApi;
import kotlin.Metadata;

@Metadata
@RequiresApi(33)
public final class WebSourceRegistrationRequest
{
    private final Uri appDestination;
    private final InputEvent inputEvent;
    @NotNull
    private final Uri topOriginUri;
    private final Uri verifiedDestination;
    private final Uri webDestination;
    @NotNull
    private final List<WebSourceParams> webSourceParams;
    
    public WebSourceRegistrationRequest(@NotNull final List<WebSourceParams> webSourceParams, @NotNull final Uri topOriginUri, final InputEvent inputEvent, final Uri appDestination, final Uri webDestination, final Uri verifiedDestination) {
        Intrinsics.checkNotNullParameter((Object)webSourceParams, "webSourceParams");
        Intrinsics.checkNotNullParameter((Object)topOriginUri, "topOriginUri");
        this.webSourceParams = webSourceParams;
        this.topOriginUri = topOriginUri;
        this.inputEvent = inputEvent;
        this.appDestination = appDestination;
        this.webDestination = webDestination;
        this.verifiedDestination = verifiedDestination;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof WebSourceRegistrationRequest)) {
            return false;
        }
        final List<WebSourceParams> webSourceParams = this.webSourceParams;
        final WebSourceRegistrationRequest webSourceRegistrationRequest = (WebSourceRegistrationRequest)o;
        if (!Intrinsics.\u3007o\u3007((Object)webSourceParams, (Object)webSourceRegistrationRequest.webSourceParams) || !Intrinsics.\u3007o\u3007((Object)this.webDestination, (Object)webSourceRegistrationRequest.webDestination) || !Intrinsics.\u3007o\u3007((Object)this.appDestination, (Object)webSourceRegistrationRequest.appDestination) || !Intrinsics.\u3007o\u3007((Object)this.topOriginUri, (Object)webSourceRegistrationRequest.topOriginUri) || !Intrinsics.\u3007o\u3007((Object)this.inputEvent, (Object)webSourceRegistrationRequest.inputEvent) || !Intrinsics.\u3007o\u3007((Object)this.verifiedDestination, (Object)webSourceRegistrationRequest.verifiedDestination)) {
            b = false;
        }
        return b;
    }
    
    public final Uri getAppDestination() {
        return this.appDestination;
    }
    
    public final InputEvent getInputEvent() {
        return this.inputEvent;
    }
    
    @NotNull
    public final Uri getTopOriginUri() {
        return this.topOriginUri;
    }
    
    public final Uri getVerifiedDestination() {
        return this.verifiedDestination;
    }
    
    public final Uri getWebDestination() {
        return this.webDestination;
    }
    
    @NotNull
    public final List<WebSourceParams> getWebSourceParams() {
        return this.webSourceParams;
    }
    
    @Override
    public int hashCode() {
        final int n = this.webSourceParams.hashCode() * 31 + this.topOriginUri.hashCode();
        final InputEvent inputEvent = this.inputEvent;
        int n2 = n;
        if (inputEvent != null) {
            n2 = n * 31 + inputEvent.hashCode();
        }
        final Uri appDestination = this.appDestination;
        int n3 = n2;
        if (appDestination != null) {
            n3 = n2 * 31 + appDestination.hashCode();
        }
        final Uri webDestination = this.webDestination;
        int n4 = n3;
        if (webDestination != null) {
            n4 = n3 * 31 + webDestination.hashCode();
        }
        final int n5 = n4 * 31 + this.topOriginUri.hashCode();
        final InputEvent inputEvent2 = this.inputEvent;
        int n6 = n5;
        if (inputEvent2 != null) {
            n6 = n5 * 31 + inputEvent2.hashCode();
        }
        final Uri verifiedDestination = this.verifiedDestination;
        int n7 = n6;
        if (verifiedDestination != null) {
            n7 = n6 * 31 + verifiedDestination.hashCode();
        }
        return n7;
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("WebSourceParams=[");
        sb.append(this.webSourceParams);
        sb.append("], TopOriginUri=");
        sb.append(this.topOriginUri);
        sb.append(", InputEvent=");
        sb.append(this.inputEvent);
        sb.append(", AppDestination=");
        sb.append(this.appDestination);
        sb.append(", WebDestination=");
        sb.append(this.webDestination);
        sb.append(", VerifiedDestination=");
        sb.append(this.verifiedDestination);
        final String string = sb.toString();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("WebSourceRegistrationRequest { ");
        sb2.append(string);
        sb2.append(" }");
        return sb2.toString();
    }
    
    @Metadata
    public static final class Builder
    {
        private Uri appDestination;
        private InputEvent inputEvent;
        @NotNull
        private final Uri topOriginUri;
        private Uri verifiedDestination;
        private Uri webDestination;
        @NotNull
        private final List<WebSourceParams> webSourceParams;
        
        public Builder(@NotNull final List<WebSourceParams> webSourceParams, @NotNull final Uri topOriginUri) {
            Intrinsics.checkNotNullParameter((Object)webSourceParams, "webSourceParams");
            Intrinsics.checkNotNullParameter((Object)topOriginUri, "topOriginUri");
            this.webSourceParams = webSourceParams;
            this.topOriginUri = topOriginUri;
        }
        
        @NotNull
        public final WebSourceRegistrationRequest build() {
            return new WebSourceRegistrationRequest(this.webSourceParams, this.topOriginUri, this.inputEvent, this.appDestination, this.webDestination, this.verifiedDestination);
        }
        
        @NotNull
        public final Builder setAppDestination(final Uri appDestination) {
            this.appDestination = appDestination;
            return this;
        }
        
        @NotNull
        public final Builder setInputEvent(@NotNull final InputEvent inputEvent) {
            Intrinsics.checkNotNullParameter((Object)inputEvent, "inputEvent");
            this.inputEvent = inputEvent;
            return this;
        }
        
        @NotNull
        public final Builder setVerifiedDestination(final Uri verifiedDestination) {
            this.verifiedDestination = verifiedDestination;
            return this;
        }
        
        @NotNull
        public final Builder setWebDestination(final Uri webDestination) {
            this.webDestination = webDestination;
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.measurement;

import androidx.privacysandbox.ads.adservices.adid.\u3007080;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.net.Uri;
import androidx.annotation.RequiresApi;
import kotlin.Metadata;

@Metadata
@RequiresApi(33)
public final class WebTriggerParams
{
    private final boolean debugKeyAllowed;
    @NotNull
    private final Uri registrationUri;
    
    public WebTriggerParams(@NotNull final Uri registrationUri, final boolean debugKeyAllowed) {
        Intrinsics.checkNotNullParameter((Object)registrationUri, "registrationUri");
        this.registrationUri = registrationUri;
        this.debugKeyAllowed = debugKeyAllowed;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof WebTriggerParams)) {
            return false;
        }
        final Uri registrationUri = this.registrationUri;
        final WebTriggerParams webTriggerParams = (WebTriggerParams)o;
        if (!Intrinsics.\u3007o\u3007((Object)registrationUri, (Object)webTriggerParams.registrationUri) || this.debugKeyAllowed != webTriggerParams.debugKeyAllowed) {
            b = false;
        }
        return b;
    }
    
    public final boolean getDebugKeyAllowed() {
        return this.debugKeyAllowed;
    }
    
    @NotNull
    public final Uri getRegistrationUri() {
        return this.registrationUri;
    }
    
    @Override
    public int hashCode() {
        return this.registrationUri.hashCode() * 31 + \u3007080.\u3007080(this.debugKeyAllowed);
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("WebTriggerParams { RegistrationUri=");
        sb.append(this.registrationUri);
        sb.append(", DebugKeyAllowed=");
        sb.append(this.debugKeyAllowed);
        sb.append(" }");
        return sb.toString();
    }
}

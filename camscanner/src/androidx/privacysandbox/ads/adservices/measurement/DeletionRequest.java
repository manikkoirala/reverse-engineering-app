// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.measurement;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.HashSet;
import java.util.Collection;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import java.time.Instant;
import android.net.Uri;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import androidx.annotation.RequiresApi;
import kotlin.Metadata;

@Metadata
@RequiresApi(33)
public final class DeletionRequest
{
    @NotNull
    public static final Companion Companion;
    public static final int DELETION_MODE_ALL = 0;
    public static final int DELETION_MODE_EXCLUDE_INTERNAL_DATA = 1;
    public static final int MATCH_BEHAVIOR_DELETE = 0;
    public static final int MATCH_BEHAVIOR_PRESERVE = 1;
    private final int deletionMode;
    @NotNull
    private final List<Uri> domainUris;
    @NotNull
    private final Instant end;
    private final int matchBehavior;
    @NotNull
    private final List<Uri> originUris;
    @NotNull
    private final Instant start;
    
    static {
        Companion = new Companion(null);
    }
    
    public DeletionRequest(final int deletionMode, final int matchBehavior, @NotNull final Instant start, @NotNull final Instant end, @NotNull final List<? extends Uri> domainUris, @NotNull final List<? extends Uri> originUris) {
        Intrinsics.checkNotNullParameter((Object)start, "start");
        Intrinsics.checkNotNullParameter((Object)end, "end");
        Intrinsics.checkNotNullParameter((Object)domainUris, "domainUris");
        Intrinsics.checkNotNullParameter((Object)originUris, "originUris");
        this.deletionMode = deletionMode;
        this.matchBehavior = matchBehavior;
        this.start = start;
        this.end = end;
        this.domainUris = (List<Uri>)domainUris;
        this.originUris = (List<Uri>)originUris;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof DeletionRequest)) {
            return false;
        }
        final int deletionMode = this.deletionMode;
        final DeletionRequest deletionRequest = (DeletionRequest)o;
        if (deletionMode != deletionRequest.deletionMode || !Intrinsics.\u3007o\u3007((Object)new HashSet(this.domainUris), (Object)new HashSet(deletionRequest.domainUris)) || !Intrinsics.\u3007o\u3007((Object)new HashSet(this.originUris), (Object)new HashSet(deletionRequest.originUris)) || !Intrinsics.\u3007o\u3007((Object)this.start, (Object)deletionRequest.start) || !Intrinsics.\u3007o\u3007((Object)this.end, (Object)deletionRequest.end) || this.matchBehavior != deletionRequest.matchBehavior) {
            b = false;
        }
        return b;
    }
    
    public final int getDeletionMode() {
        return this.deletionMode;
    }
    
    @NotNull
    public final List<Uri> getDomainUris() {
        return this.domainUris;
    }
    
    @NotNull
    public final Instant getEnd() {
        return this.end;
    }
    
    public final int getMatchBehavior() {
        return this.matchBehavior;
    }
    
    @NotNull
    public final List<Uri> getOriginUris() {
        return this.originUris;
    }
    
    @NotNull
    public final Instant getStart() {
        return this.start;
    }
    
    @Override
    public int hashCode() {
        return ((((this.deletionMode * 31 + this.domainUris.hashCode()) * 31 + this.originUris.hashCode()) * 31 + \u3007080.\u3007080(this.start)) * 31 + \u3007080.\u3007080(this.end)) * 31 + this.matchBehavior;
    }
    
    @NotNull
    @Override
    public String toString() {
        String str;
        if (this.deletionMode == 0) {
            str = "DELETION_MODE_ALL";
        }
        else {
            str = "DELETION_MODE_EXCLUDE_INTERNAL_DATA";
        }
        String str2;
        if (this.matchBehavior == 0) {
            str2 = "MATCH_BEHAVIOR_DELETE";
        }
        else {
            str2 = "MATCH_BEHAVIOR_PRESERVE";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("DeletionRequest { DeletionMode=");
        sb.append(str);
        sb.append(", MatchBehavior=");
        sb.append(str2);
        sb.append(", Start=");
        sb.append(this.start);
        sb.append(", End=");
        sb.append(this.end);
        sb.append(", DomainUris=");
        sb.append(this.domainUris);
        sb.append(", OriginUris=");
        sb.append(this.originUris);
        sb.append(" }");
        return sb.toString();
    }
    
    @Metadata
    @RequiresApi(33)
    public static final class Builder
    {
        private final int deletionMode;
        @NotNull
        private List<? extends Uri> domainUris;
        @NotNull
        private Instant end;
        private final int matchBehavior;
        @NotNull
        private List<? extends Uri> originUris;
        @NotNull
        private Instant start;
        
        public Builder(final int deletionMode, final int matchBehavior) {
            this.deletionMode = deletionMode;
            this.matchBehavior = matchBehavior;
            final Instant \u3007080 = \u3007o00\u3007\u3007Oo.\u3007080();
            Intrinsics.checkNotNullExpressionValue((Object)\u3007080, "MIN");
            this.start = \u3007080;
            final Instant \u300781 = \u3007o\u3007.\u3007080();
            Intrinsics.checkNotNullExpressionValue((Object)\u300781, "MAX");
            this.end = \u300781;
            this.domainUris = CollectionsKt.\u300780\u3007808\u3007O();
            this.originUris = CollectionsKt.\u300780\u3007808\u3007O();
        }
        
        @NotNull
        public final DeletionRequest build() {
            return new DeletionRequest(this.deletionMode, this.matchBehavior, this.start, this.end, this.domainUris, this.originUris);
        }
        
        @NotNull
        public final Builder setDomainUris(@NotNull final List<? extends Uri> domainUris) {
            Intrinsics.checkNotNullParameter((Object)domainUris, "domainUris");
            this.domainUris = domainUris;
            return this;
        }
        
        @NotNull
        public final Builder setEnd(@NotNull final Instant end) {
            Intrinsics.checkNotNullParameter((Object)end, "end");
            this.end = end;
            return this;
        }
        
        @NotNull
        public final Builder setOriginUris(@NotNull final List<? extends Uri> originUris) {
            Intrinsics.checkNotNullParameter((Object)originUris, "originUris");
            this.originUris = originUris;
            return this;
        }
        
        @NotNull
        public final Builder setStart(@NotNull final Instant start) {
            Intrinsics.checkNotNullParameter((Object)start, "start");
            this.start = start;
            return this;
        }
    }
    
    @Metadata
    public static final class Companion
    {
        private Companion() {
        }
        
        @Retention(RetentionPolicy.SOURCE)
        @Metadata
        public @interface DeletionMode {
        }
        
        @Retention(RetentionPolicy.SOURCE)
        @Metadata
        public @interface MatchBehavior {
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.measurement;

import kotlin.jvm.internal.Intrinsics;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import android.net.Uri;
import androidx.annotation.RequiresApi;
import kotlin.Metadata;

@Metadata
@RequiresApi(33)
public final class WebTriggerRegistrationRequest
{
    @NotNull
    private final Uri destination;
    @NotNull
    private final List<WebTriggerParams> webTriggerParams;
    
    public WebTriggerRegistrationRequest(@NotNull final List<WebTriggerParams> webTriggerParams, @NotNull final Uri destination) {
        Intrinsics.checkNotNullParameter((Object)webTriggerParams, "webTriggerParams");
        Intrinsics.checkNotNullParameter((Object)destination, "destination");
        this.webTriggerParams = webTriggerParams;
        this.destination = destination;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof WebTriggerRegistrationRequest)) {
            return false;
        }
        final List<WebTriggerParams> webTriggerParams = this.webTriggerParams;
        final WebTriggerRegistrationRequest webTriggerRegistrationRequest = (WebTriggerRegistrationRequest)o;
        if (!Intrinsics.\u3007o\u3007((Object)webTriggerParams, (Object)webTriggerRegistrationRequest.webTriggerParams) || !Intrinsics.\u3007o\u3007((Object)this.destination, (Object)webTriggerRegistrationRequest.destination)) {
            b = false;
        }
        return b;
    }
    
    @NotNull
    public final Uri getDestination() {
        return this.destination;
    }
    
    @NotNull
    public final List<WebTriggerParams> getWebTriggerParams() {
        return this.webTriggerParams;
    }
    
    @Override
    public int hashCode() {
        return this.webTriggerParams.hashCode() * 31 + this.destination.hashCode();
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("WebTriggerRegistrationRequest { WebTriggerParams=");
        sb.append(this.webTriggerParams);
        sb.append(", Destination=");
        sb.append(this.destination);
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.measurement;

import androidx.privacysandbox.ads.adservices.internal.AdServicesInfo;
import androidx.annotation.DoNotInline;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import android.os.OutcomeReceiver;
import java.util.concurrent.Executor;
import androidx.core.os.OutcomeReceiverKt;
import androidx.privacysandbox.ads.adservices.adid.\u3007o00\u3007\u3007Oo;
import kotlinx.coroutines.CancellableContinuationImpl;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import android.adservices.measurement.WebTriggerRegistrationRequest$Builder;
import android.adservices.measurement.WebTriggerParams$Builder;
import android.adservices.measurement.WebSourceRegistrationRequest$Builder;
import java.util.Iterator;
import android.adservices.measurement.WebSourceParams$Builder;
import java.util.ArrayList;
import java.util.List;
import android.adservices.measurement.DeletionRequest$Builder;
import androidx.appcompat.widget.oo88o8O;
import kotlin.jvm.internal.Intrinsics;
import androidx.annotation.RequiresExtension;
import android.view.InputEvent;
import android.net.Uri;
import androidx.annotation.RequiresPermission;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import android.annotation.SuppressLint;
import android.content.Context;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public abstract class MeasurementManager
{
    @NotNull
    public static final Companion Companion;
    public static final int MEASUREMENT_API_STATE_DISABLED = 0;
    public static final int MEASUREMENT_API_STATE_ENABLED = 1;
    
    static {
        Companion = new Companion(null);
    }
    
    @SuppressLint({ "NewApi", "ClassVerificationFailure" })
    public static final MeasurementManager obtain(@NotNull final Context context) {
        return MeasurementManager.Companion.obtain(context);
    }
    
    public abstract Object deleteRegistrations(@NotNull final DeletionRequest p0, @NotNull final Continuation<? super Unit> p1);
    
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_ATTRIBUTION")
    public abstract Object getMeasurementApiStatus(@NotNull final Continuation<? super Integer> p0);
    
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_ATTRIBUTION")
    public abstract Object registerSource(@NotNull final Uri p0, final InputEvent p1, @NotNull final Continuation<? super Unit> p2);
    
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_ATTRIBUTION")
    public abstract Object registerTrigger(@NotNull final Uri p0, @NotNull final Continuation<? super Unit> p1);
    
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_ATTRIBUTION")
    public abstract Object registerWebSource(@NotNull final WebSourceRegistrationRequest p0, @NotNull final Continuation<? super Unit> p1);
    
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_ATTRIBUTION")
    public abstract Object registerWebTrigger(@NotNull final WebTriggerRegistrationRequest p0, @NotNull final Continuation<? super Unit> p1);
    
    @Metadata
    @SuppressLint({ "NewApi", "ClassVerificationFailure" })
    @RequiresExtension(extension = 1000000, version = 5)
    private static final class Api33Ext5Impl extends MeasurementManager
    {
        @NotNull
        private final android.adservices.measurement.MeasurementManager mMeasurementManager;
        
        public Api33Ext5Impl(@NotNull final android.adservices.measurement.MeasurementManager mMeasurementManager) {
            Intrinsics.checkNotNullParameter((Object)mMeasurementManager, "mMeasurementManager");
            this.mMeasurementManager = mMeasurementManager;
        }
        
        public Api33Ext5Impl(@NotNull final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            final Object \u3007080 = oo88o8O.\u3007080(context, (Class)android.adservices.measurement.MeasurementManager.class);
            Intrinsics.checkNotNullExpressionValue(\u3007080, "context.getSystemService\u2026:class.java\n            )");
            this((android.adservices.measurement.MeasurementManager)\u3007080);
        }
        
        public static final /* synthetic */ android.adservices.measurement.MeasurementManager access$getMMeasurementManager$p(final Api33Ext5Impl api33Ext5Impl) {
            return api33Ext5Impl.mMeasurementManager;
        }
        
        private final android.adservices.measurement.DeletionRequest convertDeletionRequest(final DeletionRequest deletionRequest) {
            final android.adservices.measurement.DeletionRequest build = new DeletionRequest$Builder().setDeletionMode(deletionRequest.getDeletionMode()).setMatchBehavior(deletionRequest.getMatchBehavior()).setStart(deletionRequest.getStart()).setEnd(deletionRequest.getEnd()).setDomainUris((List)deletionRequest.getDomainUris()).setOriginUris((List)deletionRequest.getOriginUris()).build();
            Intrinsics.checkNotNullExpressionValue((Object)build, "Builder()\n              \u2026\n                .build()");
            return build;
        }
        
        private final List<android.adservices.measurement.WebSourceParams> convertWebSourceParams(final List<WebSourceParams> list) {
            final ArrayList list2 = new ArrayList();
            for (final WebSourceParams webSourceParams : list) {
                final android.adservices.measurement.WebSourceParams build = new WebSourceParams$Builder(webSourceParams.getRegistrationUri()).setDebugKeyAllowed(webSourceParams.getDebugKeyAllowed()).build();
                Intrinsics.checkNotNullExpressionValue((Object)build, "Builder(param.registrati\u2026                 .build()");
                list2.add(build);
            }
            return list2;
        }
        
        private final android.adservices.measurement.WebSourceRegistrationRequest convertWebSourceRequest(final WebSourceRegistrationRequest webSourceRegistrationRequest) {
            final android.adservices.measurement.WebSourceRegistrationRequest build = new WebSourceRegistrationRequest$Builder((List)this.convertWebSourceParams(webSourceRegistrationRequest.getWebSourceParams()), webSourceRegistrationRequest.getTopOriginUri()).setWebDestination(webSourceRegistrationRequest.getWebDestination()).setAppDestination(webSourceRegistrationRequest.getAppDestination()).setInputEvent(webSourceRegistrationRequest.getInputEvent()).setVerifiedDestination(webSourceRegistrationRequest.getVerifiedDestination()).build();
            Intrinsics.checkNotNullExpressionValue((Object)build, "Builder(\n               \u2026\n                .build()");
            return build;
        }
        
        private final List<android.adservices.measurement.WebTriggerParams> convertWebTriggerParams(final List<WebTriggerParams> list) {
            final ArrayList list2 = new ArrayList();
            for (final WebTriggerParams webTriggerParams : list) {
                final android.adservices.measurement.WebTriggerParams build = new WebTriggerParams$Builder(webTriggerParams.getRegistrationUri()).setDebugKeyAllowed(webTriggerParams.getDebugKeyAllowed()).build();
                Intrinsics.checkNotNullExpressionValue((Object)build, "Builder(param.registrati\u2026                 .build()");
                list2.add(build);
            }
            return list2;
        }
        
        private final android.adservices.measurement.WebTriggerRegistrationRequest convertWebTriggerRequest(final WebTriggerRegistrationRequest webTriggerRegistrationRequest) {
            final android.adservices.measurement.WebTriggerRegistrationRequest build = new WebTriggerRegistrationRequest$Builder((List)this.convertWebTriggerParams(webTriggerRegistrationRequest.getWebTriggerParams()), webTriggerRegistrationRequest.getDestination()).build();
            Intrinsics.checkNotNullExpressionValue((Object)build, "Builder(\n               \u2026\n                .build()");
            return build;
        }
        
        @DoNotInline
        @Override
        public Object deleteRegistrations(@NotNull final DeletionRequest deletionRequest, @NotNull final Continuation<? super Unit> continuation) {
            final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.\u3007o\u3007((Continuation)continuation), 1);
            cancellableContinuationImpl.O8ooOoo\u3007();
            access$getMMeasurementManager$p(this).deleteRegistrations(this.convertDeletionRequest(deletionRequest), (Executor)new \u3007o00\u3007\u3007Oo(), (OutcomeReceiver)OutcomeReceiverKt.asOutcomeReceiver((kotlin.coroutines.Continuation<? super Object>)cancellableContinuationImpl));
            final Object o\u3007O8\u3007\u3007o = cancellableContinuationImpl.o\u3007O8\u3007\u3007o();
            if (o\u3007O8\u3007\u3007o == IntrinsicsKt.O8()) {
                DebugProbesKt.\u3007o\u3007((Continuation)continuation);
            }
            if (o\u3007O8\u3007\u3007o == IntrinsicsKt.O8()) {
                return o\u3007O8\u3007\u3007o;
            }
            return Unit.\u3007080;
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_ADSERVICES_ATTRIBUTION")
        @Override
        public Object getMeasurementApiStatus(@NotNull final Continuation<? super Integer> continuation) {
            final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.\u3007o\u3007((Continuation)continuation), 1);
            cancellableContinuationImpl.O8ooOoo\u3007();
            access$getMMeasurementManager$p(this).getMeasurementApiStatus((Executor)new \u3007o00\u3007\u3007Oo(), (OutcomeReceiver)OutcomeReceiverKt.asOutcomeReceiver((kotlin.coroutines.Continuation<? super Object>)cancellableContinuationImpl));
            final Object o\u3007O8\u3007\u3007o = cancellableContinuationImpl.o\u3007O8\u3007\u3007o();
            if (o\u3007O8\u3007\u3007o == IntrinsicsKt.O8()) {
                DebugProbesKt.\u3007o\u3007((Continuation)continuation);
            }
            return o\u3007O8\u3007\u3007o;
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_ADSERVICES_ATTRIBUTION")
        @Override
        public Object registerSource(@NotNull final Uri uri, final InputEvent inputEvent, @NotNull final Continuation<? super Unit> continuation) {
            final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.\u3007o\u3007((Continuation)continuation), 1);
            cancellableContinuationImpl.O8ooOoo\u3007();
            access$getMMeasurementManager$p(this).registerSource(uri, inputEvent, (Executor)new \u3007o00\u3007\u3007Oo(), (OutcomeReceiver)OutcomeReceiverKt.asOutcomeReceiver((kotlin.coroutines.Continuation<? super Object>)cancellableContinuationImpl));
            final Object o\u3007O8\u3007\u3007o = cancellableContinuationImpl.o\u3007O8\u3007\u3007o();
            if (o\u3007O8\u3007\u3007o == IntrinsicsKt.O8()) {
                DebugProbesKt.\u3007o\u3007((Continuation)continuation);
            }
            if (o\u3007O8\u3007\u3007o == IntrinsicsKt.O8()) {
                return o\u3007O8\u3007\u3007o;
            }
            return Unit.\u3007080;
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_ADSERVICES_ATTRIBUTION")
        @Override
        public Object registerTrigger(@NotNull final Uri uri, @NotNull final Continuation<? super Unit> continuation) {
            final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.\u3007o\u3007((Continuation)continuation), 1);
            cancellableContinuationImpl.O8ooOoo\u3007();
            access$getMMeasurementManager$p(this).registerTrigger(uri, (Executor)new \u3007o00\u3007\u3007Oo(), (OutcomeReceiver)OutcomeReceiverKt.asOutcomeReceiver((kotlin.coroutines.Continuation<? super Object>)cancellableContinuationImpl));
            final Object o\u3007O8\u3007\u3007o = cancellableContinuationImpl.o\u3007O8\u3007\u3007o();
            if (o\u3007O8\u3007\u3007o == IntrinsicsKt.O8()) {
                DebugProbesKt.\u3007o\u3007((Continuation)continuation);
            }
            if (o\u3007O8\u3007\u3007o == IntrinsicsKt.O8()) {
                return o\u3007O8\u3007\u3007o;
            }
            return Unit.\u3007080;
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_ADSERVICES_ATTRIBUTION")
        @Override
        public Object registerWebSource(@NotNull final WebSourceRegistrationRequest webSourceRegistrationRequest, @NotNull final Continuation<? super Unit> continuation) {
            final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.\u3007o\u3007((Continuation)continuation), 1);
            cancellableContinuationImpl.O8ooOoo\u3007();
            access$getMMeasurementManager$p(this).registerWebSource(this.convertWebSourceRequest(webSourceRegistrationRequest), (Executor)new \u3007o00\u3007\u3007Oo(), (OutcomeReceiver)OutcomeReceiverKt.asOutcomeReceiver((kotlin.coroutines.Continuation<? super Object>)cancellableContinuationImpl));
            final Object o\u3007O8\u3007\u3007o = cancellableContinuationImpl.o\u3007O8\u3007\u3007o();
            if (o\u3007O8\u3007\u3007o == IntrinsicsKt.O8()) {
                DebugProbesKt.\u3007o\u3007((Continuation)continuation);
            }
            if (o\u3007O8\u3007\u3007o == IntrinsicsKt.O8()) {
                return o\u3007O8\u3007\u3007o;
            }
            return Unit.\u3007080;
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_ADSERVICES_ATTRIBUTION")
        @Override
        public Object registerWebTrigger(@NotNull final WebTriggerRegistrationRequest webTriggerRegistrationRequest, @NotNull final Continuation<? super Unit> continuation) {
            final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.\u3007o\u3007((Continuation)continuation), 1);
            cancellableContinuationImpl.O8ooOoo\u3007();
            access$getMMeasurementManager$p(this).registerWebTrigger(this.convertWebTriggerRequest(webTriggerRegistrationRequest), (Executor)new \u3007o00\u3007\u3007Oo(), (OutcomeReceiver)OutcomeReceiverKt.asOutcomeReceiver((kotlin.coroutines.Continuation<? super Object>)cancellableContinuationImpl));
            final Object o\u3007O8\u3007\u3007o = cancellableContinuationImpl.o\u3007O8\u3007\u3007o();
            if (o\u3007O8\u3007\u3007o == IntrinsicsKt.O8()) {
                DebugProbesKt.\u3007o\u3007((Continuation)continuation);
            }
            if (o\u3007O8\u3007\u3007o == IntrinsicsKt.O8()) {
                return o\u3007O8\u3007\u3007o;
            }
            return Unit.\u3007080;
        }
    }
    
    @Metadata
    public static final class Companion
    {
        private Companion() {
        }
        
        @SuppressLint({ "NewApi", "ClassVerificationFailure" })
        public final MeasurementManager obtain(@NotNull final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            final StringBuilder sb = new StringBuilder();
            sb.append("AdServicesInfo.version=");
            final AdServicesInfo instance = AdServicesInfo.INSTANCE;
            sb.append(instance.version());
            Api33Ext5Impl api33Ext5Impl;
            if (instance.version() >= 5) {
                api33Ext5Impl = new Api33Ext5Impl(context);
            }
            else {
                api33Ext5Impl = null;
            }
            return api33Ext5Impl;
        }
    }
}

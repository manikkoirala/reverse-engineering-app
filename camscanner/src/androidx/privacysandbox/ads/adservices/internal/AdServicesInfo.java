// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.internal;

import androidx.annotation.DoNotInline;
import android.os.ext.SdkExtensions;
import androidx.annotation.RequiresApi;
import android.os.Build$VERSION;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class AdServicesInfo
{
    @NotNull
    public static final AdServicesInfo INSTANCE;
    
    static {
        INSTANCE = new AdServicesInfo();
    }
    
    private AdServicesInfo() {
    }
    
    public final int version() {
        int adServicesVersion;
        if (Build$VERSION.SDK_INT >= 30) {
            adServicesVersion = Extensions30Impl.INSTANCE.getAdServicesVersion();
        }
        else {
            adServicesVersion = 0;
        }
        return adServicesVersion;
    }
    
    @Metadata
    @RequiresApi(30)
    private static final class Extensions30Impl
    {
        @NotNull
        public static final Extensions30Impl INSTANCE;
        
        static {
            INSTANCE = new Extensions30Impl();
        }
        
        @DoNotInline
        public final int getAdServicesVersion() {
            return SdkExtensions.getExtensionVersion(1000000);
        }
    }
}

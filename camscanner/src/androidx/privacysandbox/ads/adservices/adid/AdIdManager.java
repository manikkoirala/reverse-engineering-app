// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.adid;

import androidx.privacysandbox.ads.adservices.internal.AdServicesInfo;
import androidx.annotation.DoNotInline;
import kotlin.ResultKt;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import android.os.OutcomeReceiver;
import java.util.concurrent.Executor;
import androidx.core.os.OutcomeReceiverKt;
import kotlinx.coroutines.CancellableContinuationImpl;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import androidx.appcompat.widget.oo88o8O;
import kotlin.jvm.internal.Intrinsics;
import androidx.annotation.RequiresExtension;
import androidx.annotation.RequiresPermission;
import kotlin.coroutines.Continuation;
import android.annotation.SuppressLint;
import android.content.Context;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public abstract class AdIdManager
{
    @NotNull
    public static final Companion Companion;
    
    static {
        Companion = new Companion(null);
    }
    
    @SuppressLint({ "NewApi", "ClassVerificationFailure" })
    public static final AdIdManager obtain(@NotNull final Context context) {
        return AdIdManager.Companion.obtain(context);
    }
    
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_AD_ID")
    public abstract Object getAdId(@NotNull final Continuation<? super AdId> p0);
    
    @Metadata
    @SuppressLint({ "ClassVerificationFailure", "NewApi" })
    @RequiresExtension(extension = 1000000, version = 4)
    private static final class Api33Ext4Impl extends AdIdManager
    {
        @NotNull
        private final android.adservices.adid.AdIdManager mAdIdManager;
        
        public Api33Ext4Impl(@NotNull final android.adservices.adid.AdIdManager mAdIdManager) {
            Intrinsics.checkNotNullParameter((Object)mAdIdManager, "mAdIdManager");
            this.mAdIdManager = mAdIdManager;
        }
        
        public Api33Ext4Impl(@NotNull final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            final Object \u3007080 = oo88o8O.\u3007080(context, (Class)android.adservices.adid.AdIdManager.class);
            Intrinsics.checkNotNullExpressionValue(\u3007080, "context.getSystemService\u2026:class.java\n            )");
            this((android.adservices.adid.AdIdManager)\u3007080);
        }
        
        public static final /* synthetic */ android.adservices.adid.AdIdManager access$getMAdIdManager$p(final Api33Ext4Impl api33Ext4Impl) {
            return api33Ext4Impl.mAdIdManager;
        }
        
        private final AdId convertResponse(final android.adservices.adid.AdId adId) {
            final String adId2 = adId.getAdId();
            Intrinsics.checkNotNullExpressionValue((Object)adId2, "response.adId");
            return new AdId(adId2, adId.isLimitAdTrackingEnabled());
        }
        
        @RequiresPermission("android.permission.ACCESS_ADSERVICES_AD_ID")
        private final Object getAdIdAsyncInternal(final Continuation<? super android.adservices.adid.AdId> continuation) {
            final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.\u3007o\u3007((Continuation)continuation), 1);
            cancellableContinuationImpl.O8ooOoo\u3007();
            access$getMAdIdManager$p(this).getAdId((Executor)new \u3007o00\u3007\u3007Oo(), (OutcomeReceiver)OutcomeReceiverKt.asOutcomeReceiver((kotlin.coroutines.Continuation<? super Object>)cancellableContinuationImpl));
            final Object o\u3007O8\u3007\u3007o = cancellableContinuationImpl.o\u3007O8\u3007\u3007o();
            if (o\u3007O8\u3007\u3007o == IntrinsicsKt.O8()) {
                DebugProbesKt.\u3007o\u3007((Continuation)continuation);
            }
            return o\u3007O8\u3007\u3007o;
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_ADSERVICES_AD_ID")
        @Override
        public Object getAdId(@NotNull final Continuation<? super AdId> continuation) {
            Object o = null;
            Label_0047: {
                if (continuation instanceof AdIdManager$Api33Ext4Impl$getAdId.AdIdManager$Api33Ext4Impl$getAdId$1) {
                    final AdIdManager$Api33Ext4Impl$getAdId.AdIdManager$Api33Ext4Impl$getAdId$1 adIdManager$Api33Ext4Impl$getAdId$1 = (AdIdManager$Api33Ext4Impl$getAdId.AdIdManager$Api33Ext4Impl$getAdId$1)continuation;
                    final int label = adIdManager$Api33Ext4Impl$getAdId$1.label;
                    if ((label & Integer.MIN_VALUE) != 0x0) {
                        adIdManager$Api33Ext4Impl$getAdId$1.label = label + Integer.MIN_VALUE;
                        o = adIdManager$Api33Ext4Impl$getAdId$1;
                        break Label_0047;
                    }
                }
                o = new AdIdManager$Api33Ext4Impl$getAdId.AdIdManager$Api33Ext4Impl$getAdId$1(this, (Continuation)continuation);
            }
            final Object result = ((AdIdManager$Api33Ext4Impl$getAdId.AdIdManager$Api33Ext4Impl$getAdId$1)o).result;
            final Object o2 = IntrinsicsKt.O8();
            final int label2 = ((AdIdManager$Api33Ext4Impl$getAdId.AdIdManager$Api33Ext4Impl$getAdId$1)o).label;
            Api33Ext4Impl api33Ext4Impl;
            Object adIdAsyncInternal;
            if (label2 != 0) {
                if (label2 != 1) {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                api33Ext4Impl = (Api33Ext4Impl)((AdIdManager$Api33Ext4Impl$getAdId.AdIdManager$Api33Ext4Impl$getAdId$1)o).L$0;
                ResultKt.\u3007o00\u3007\u3007Oo(result);
                adIdAsyncInternal = result;
            }
            else {
                ResultKt.\u3007o00\u3007\u3007Oo(result);
                ((AdIdManager$Api33Ext4Impl$getAdId.AdIdManager$Api33Ext4Impl$getAdId$1)o).L$0 = this;
                ((AdIdManager$Api33Ext4Impl$getAdId.AdIdManager$Api33Ext4Impl$getAdId$1)o).label = 1;
                adIdAsyncInternal = this.getAdIdAsyncInternal((Continuation<? super android.adservices.adid.AdId>)o);
                if (adIdAsyncInternal == o2) {
                    return o2;
                }
                api33Ext4Impl = this;
            }
            return api33Ext4Impl.convertResponse((android.adservices.adid.AdId)adIdAsyncInternal);
        }
    }
    
    @Metadata
    public static final class Companion
    {
        private Companion() {
        }
        
        @SuppressLint({ "NewApi", "ClassVerificationFailure" })
        public final AdIdManager obtain(@NotNull final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Api33Ext4Impl api33Ext4Impl;
            if (AdServicesInfo.INSTANCE.version() >= 4) {
                api33Ext4Impl = new Api33Ext4Impl(context);
            }
            else {
                api33Ext4Impl = null;
            }
            return api33Ext4Impl;
        }
    }
}

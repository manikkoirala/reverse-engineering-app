// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.adid;

import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class AdId
{
    @NotNull
    private final String adId;
    private final boolean isLimitAdTrackingEnabled;
    
    public AdId(@NotNull final String adId, final boolean isLimitAdTrackingEnabled) {
        Intrinsics.checkNotNullParameter((Object)adId, "adId");
        this.adId = adId;
        this.isLimitAdTrackingEnabled = isLimitAdTrackingEnabled;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof AdId)) {
            return false;
        }
        final String adId = this.adId;
        final AdId adId2 = (AdId)o;
        if (!Intrinsics.\u3007o\u3007((Object)adId, (Object)adId2.adId) || this.isLimitAdTrackingEnabled != adId2.isLimitAdTrackingEnabled) {
            b = false;
        }
        return b;
    }
    
    @NotNull
    public final String getAdId() {
        return this.adId;
    }
    
    @Override
    public int hashCode() {
        return this.adId.hashCode() * 31 + \u3007080.\u3007080(this.isLimitAdTrackingEnabled);
    }
    
    public final boolean isLimitAdTrackingEnabled() {
        return this.isLimitAdTrackingEnabled;
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("AdId: adId=");
        sb.append(this.adId);
        sb.append(", isLimitAdTrackingEnabled=");
        sb.append(this.isLimitAdTrackingEnabled);
        return sb.toString();
    }
}

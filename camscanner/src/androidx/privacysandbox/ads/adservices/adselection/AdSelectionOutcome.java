// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.adselection;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.net.Uri;
import kotlin.Metadata;

@Metadata
public final class AdSelectionOutcome
{
    private final long adSelectionId;
    @NotNull
    private final Uri renderUri;
    
    public AdSelectionOutcome(final long adSelectionId, @NotNull final Uri renderUri) {
        Intrinsics.checkNotNullParameter((Object)renderUri, "renderUri");
        this.adSelectionId = adSelectionId;
        this.renderUri = renderUri;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof AdSelectionOutcome)) {
            return false;
        }
        final long adSelectionId = this.adSelectionId;
        final AdSelectionOutcome adSelectionOutcome = (AdSelectionOutcome)o;
        if (adSelectionId != adSelectionOutcome.adSelectionId || !Intrinsics.\u3007o\u3007((Object)this.renderUri, (Object)adSelectionOutcome.renderUri)) {
            b = false;
        }
        return b;
    }
    
    public final long getAdSelectionId() {
        return this.adSelectionId;
    }
    
    @NotNull
    public final Uri getRenderUri() {
        return this.renderUri;
    }
    
    @Override
    public int hashCode() {
        return \u3007080.\u3007080(this.adSelectionId) * 31 + this.renderUri.hashCode();
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("AdSelectionOutcome: adSelectionId=");
        sb.append(this.adSelectionId);
        sb.append(", renderUri=");
        sb.append(this.renderUri);
        return sb.toString();
    }
}

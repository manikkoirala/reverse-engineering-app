// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.adselection;

import androidx.privacysandbox.ads.adservices.internal.AdServicesInfo;
import kotlin.ResultKt;
import androidx.annotation.DoNotInline;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import android.os.OutcomeReceiver;
import java.util.concurrent.Executor;
import androidx.core.os.OutcomeReceiverKt;
import androidx.privacysandbox.ads.adservices.adid.\u3007o00\u3007\u3007Oo;
import kotlinx.coroutines.CancellableContinuationImpl;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import android.net.Uri;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ArrayList;
import androidx.privacysandbox.ads.adservices.common.AdSelectionSignals;
import java.util.Map;
import android.adservices.common.AdTechIdentifier;
import java.util.List;
import android.adservices.adselection.AdSelectionConfig$Builder;
import androidx.appcompat.widget.oo88o8O;
import kotlin.jvm.internal.Intrinsics;
import androidx.annotation.RequiresExtension;
import androidx.annotation.RequiresPermission;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import android.annotation.SuppressLint;
import android.content.Context;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public abstract class AdSelectionManager
{
    @NotNull
    public static final Companion Companion;
    
    static {
        Companion = new Companion(null);
    }
    
    @SuppressLint({ "NewApi", "ClassVerificationFailure" })
    public static final AdSelectionManager obtain(@NotNull final Context context) {
        return AdSelectionManager.Companion.obtain(context);
    }
    
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_CUSTOM_AUDIENCE")
    public abstract Object reportImpression(@NotNull final ReportImpressionRequest p0, @NotNull final Continuation<? super Unit> p1);
    
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_CUSTOM_AUDIENCE")
    public abstract Object selectAds(@NotNull final AdSelectionConfig p0, @NotNull final Continuation<? super AdSelectionOutcome> p1);
    
    @Metadata
    @SuppressLint({ "NewApi", "ClassVerificationFailure" })
    @RequiresExtension(extension = 1000000, version = 4)
    private static final class Api33Ext4Impl extends AdSelectionManager
    {
        @NotNull
        private final android.adservices.adselection.AdSelectionManager mAdSelectionManager;
        
        public Api33Ext4Impl(@NotNull final android.adservices.adselection.AdSelectionManager mAdSelectionManager) {
            Intrinsics.checkNotNullParameter((Object)mAdSelectionManager, "mAdSelectionManager");
            this.mAdSelectionManager = mAdSelectionManager;
        }
        
        public Api33Ext4Impl(@NotNull final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            final Object \u3007080 = oo88o8O.\u3007080(context, (Class)android.adservices.adselection.AdSelectionManager.class);
            Intrinsics.checkNotNullExpressionValue(\u3007080, "context.getSystemService\u2026:class.java\n            )");
            this((android.adservices.adselection.AdSelectionManager)\u3007080);
        }
        
        public static final /* synthetic */ android.adservices.adselection.AdSelectionManager access$getMAdSelectionManager$p(final Api33Ext4Impl api33Ext4Impl) {
            return api33Ext4Impl.mAdSelectionManager;
        }
        
        private final android.adservices.adselection.AdSelectionConfig convertAdSelectionConfig(final AdSelectionConfig adSelectionConfig) {
            final android.adservices.adselection.AdSelectionConfig build = new AdSelectionConfig$Builder().setAdSelectionSignals(this.convertAdSelectionSignals(adSelectionConfig.getAdSelectionSignals())).setCustomAudienceBuyers((List)this.convertBuyers(adSelectionConfig.getCustomAudienceBuyers())).setDecisionLogicUri(adSelectionConfig.getDecisionLogicUri()).setSeller(AdTechIdentifier.fromString(adSelectionConfig.getSeller().getIdentifier())).setPerBuyerSignals((Map)this.convertPerBuyerSignals(adSelectionConfig.getPerBuyerSignals())).setSellerSignals(this.convertAdSelectionSignals(adSelectionConfig.getSellerSignals())).setTrustedScoringSignalsUri(adSelectionConfig.getTrustedScoringSignalsUri()).build();
            Intrinsics.checkNotNullExpressionValue((Object)build, "Builder()\n              \u2026\n                .build()");
            return build;
        }
        
        private final android.adservices.common.AdSelectionSignals convertAdSelectionSignals(final AdSelectionSignals adSelectionSignals) {
            final android.adservices.common.AdSelectionSignals fromString = android.adservices.common.AdSelectionSignals.fromString(adSelectionSignals.getSignals());
            Intrinsics.checkNotNullExpressionValue((Object)fromString, "fromString(request.signals)");
            return fromString;
        }
        
        private final List<AdTechIdentifier> convertBuyers(final List<androidx.privacysandbox.ads.adservices.common.AdTechIdentifier> list) {
            final ArrayList list2 = new ArrayList();
            final Iterator<androidx.privacysandbox.ads.adservices.common.AdTechIdentifier> iterator = list.iterator();
            while (iterator.hasNext()) {
                final AdTechIdentifier fromString = AdTechIdentifier.fromString(iterator.next().getIdentifier());
                Intrinsics.checkNotNullExpressionValue((Object)fromString, "fromString(buyer.identifier)");
                list2.add(fromString);
            }
            return list2;
        }
        
        private final Map<AdTechIdentifier, android.adservices.common.AdSelectionSignals> convertPerBuyerSignals(final Map<androidx.privacysandbox.ads.adservices.common.AdTechIdentifier, AdSelectionSignals> map) {
            final HashMap hashMap = new HashMap();
            for (final androidx.privacysandbox.ads.adservices.common.AdTechIdentifier adTechIdentifier : map.keySet()) {
                final AdTechIdentifier fromString = AdTechIdentifier.fromString(adTechIdentifier.getIdentifier());
                Intrinsics.checkNotNullExpressionValue((Object)fromString, "fromString(key.identifier)");
                android.adservices.common.AdSelectionSignals convertAdSelectionSignals;
                if (map.get(adTechIdentifier) != null) {
                    final AdSelectionSignals value = map.get(adTechIdentifier);
                    Intrinsics.Oo08((Object)value);
                    convertAdSelectionSignals = this.convertAdSelectionSignals(value);
                }
                else {
                    convertAdSelectionSignals = null;
                }
                hashMap.put(fromString, convertAdSelectionSignals);
            }
            return hashMap;
        }
        
        private final android.adservices.adselection.ReportImpressionRequest convertReportImpressionRequest(final ReportImpressionRequest reportImpressionRequest) {
            return new android.adservices.adselection.ReportImpressionRequest(reportImpressionRequest.getAdSelectionId(), this.convertAdSelectionConfig(reportImpressionRequest.getAdSelectionConfig()));
        }
        
        private final AdSelectionOutcome convertResponse(final android.adservices.adselection.AdSelectionOutcome adSelectionOutcome) {
            final long adSelectionId = adSelectionOutcome.getAdSelectionId();
            final Uri renderUri = adSelectionOutcome.getRenderUri();
            Intrinsics.checkNotNullExpressionValue((Object)renderUri, "response.renderUri");
            return new AdSelectionOutcome(adSelectionId, renderUri);
        }
        
        @RequiresPermission("android.permission.ACCESS_ADSERVICES_CUSTOM_AUDIENCE")
        private final Object selectAdsInternal(final android.adservices.adselection.AdSelectionConfig adSelectionConfig, final Continuation<? super android.adservices.adselection.AdSelectionOutcome> continuation) {
            final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.\u3007o\u3007((Continuation)continuation), 1);
            cancellableContinuationImpl.O8ooOoo\u3007();
            access$getMAdSelectionManager$p(this).selectAds(adSelectionConfig, (Executor)new \u3007o00\u3007\u3007Oo(), (OutcomeReceiver)OutcomeReceiverKt.asOutcomeReceiver((kotlin.coroutines.Continuation<? super Object>)cancellableContinuationImpl));
            final Object o\u3007O8\u3007\u3007o = cancellableContinuationImpl.o\u3007O8\u3007\u3007o();
            if (o\u3007O8\u3007\u3007o == IntrinsicsKt.O8()) {
                DebugProbesKt.\u3007o\u3007((Continuation)continuation);
            }
            return o\u3007O8\u3007\u3007o;
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_ADSERVICES_CUSTOM_AUDIENCE")
        @Override
        public Object reportImpression(@NotNull final ReportImpressionRequest reportImpressionRequest, @NotNull final Continuation<? super Unit> continuation) {
            final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.\u3007o\u3007((Continuation)continuation), 1);
            cancellableContinuationImpl.O8ooOoo\u3007();
            access$getMAdSelectionManager$p(this).reportImpression(this.convertReportImpressionRequest(reportImpressionRequest), (Executor)new \u3007o00\u3007\u3007Oo(), (OutcomeReceiver)OutcomeReceiverKt.asOutcomeReceiver((kotlin.coroutines.Continuation<? super Object>)cancellableContinuationImpl));
            final Object o\u3007O8\u3007\u3007o = cancellableContinuationImpl.o\u3007O8\u3007\u3007o();
            if (o\u3007O8\u3007\u3007o == IntrinsicsKt.O8()) {
                DebugProbesKt.\u3007o\u3007((Continuation)continuation);
            }
            if (o\u3007O8\u3007\u3007o == IntrinsicsKt.O8()) {
                return o\u3007O8\u3007\u3007o;
            }
            return Unit.\u3007080;
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_ADSERVICES_CUSTOM_AUDIENCE")
        @Override
        public Object selectAds(@NotNull final AdSelectionConfig adSelectionConfig, @NotNull final Continuation<? super AdSelectionOutcome> continuation) {
            Object o = null;
            Label_0053: {
                if (continuation instanceof AdSelectionManager$Api33Ext4Impl$selectAds.AdSelectionManager$Api33Ext4Impl$selectAds$1) {
                    final AdSelectionManager$Api33Ext4Impl$selectAds.AdSelectionManager$Api33Ext4Impl$selectAds$1 adSelectionManager$Api33Ext4Impl$selectAds$1 = (AdSelectionManager$Api33Ext4Impl$selectAds.AdSelectionManager$Api33Ext4Impl$selectAds$1)continuation;
                    final int label = adSelectionManager$Api33Ext4Impl$selectAds$1.label;
                    if ((label & Integer.MIN_VALUE) != 0x0) {
                        adSelectionManager$Api33Ext4Impl$selectAds$1.label = label + Integer.MIN_VALUE;
                        o = adSelectionManager$Api33Ext4Impl$selectAds$1;
                        break Label_0053;
                    }
                }
                o = new AdSelectionManager$Api33Ext4Impl$selectAds.AdSelectionManager$Api33Ext4Impl$selectAds$1(this, (Continuation)continuation);
            }
            final Object result = ((AdSelectionManager$Api33Ext4Impl$selectAds.AdSelectionManager$Api33Ext4Impl$selectAds$1)o).result;
            final Object o2 = IntrinsicsKt.O8();
            final int label2 = ((AdSelectionManager$Api33Ext4Impl$selectAds.AdSelectionManager$Api33Ext4Impl$selectAds$1)o).label;
            Api33Ext4Impl api33Ext4Impl;
            Object selectAdsInternal;
            if (label2 != 0) {
                if (label2 != 1) {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                api33Ext4Impl = (Api33Ext4Impl)((AdSelectionManager$Api33Ext4Impl$selectAds.AdSelectionManager$Api33Ext4Impl$selectAds$1)o).L$0;
                ResultKt.\u3007o00\u3007\u3007Oo(result);
                selectAdsInternal = result;
            }
            else {
                ResultKt.\u3007o00\u3007\u3007Oo(result);
                final android.adservices.adselection.AdSelectionConfig convertAdSelectionConfig = this.convertAdSelectionConfig(adSelectionConfig);
                ((AdSelectionManager$Api33Ext4Impl$selectAds.AdSelectionManager$Api33Ext4Impl$selectAds$1)o).L$0 = this;
                ((AdSelectionManager$Api33Ext4Impl$selectAds.AdSelectionManager$Api33Ext4Impl$selectAds$1)o).label = 1;
                selectAdsInternal = this.selectAdsInternal(convertAdSelectionConfig, (Continuation<? super android.adservices.adselection.AdSelectionOutcome>)o);
                if (selectAdsInternal == o2) {
                    return o2;
                }
                api33Ext4Impl = this;
            }
            return api33Ext4Impl.convertResponse((android.adservices.adselection.AdSelectionOutcome)selectAdsInternal);
        }
    }
    
    @Metadata
    public static final class Companion
    {
        private Companion() {
        }
        
        @SuppressLint({ "NewApi", "ClassVerificationFailure" })
        public final AdSelectionManager obtain(@NotNull final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Api33Ext4Impl api33Ext4Impl;
            if (AdServicesInfo.INSTANCE.version() >= 4) {
                api33Ext4Impl = new Api33Ext4Impl(context);
            }
            else {
                api33Ext4Impl = null;
            }
            return api33Ext4Impl;
        }
    }
}

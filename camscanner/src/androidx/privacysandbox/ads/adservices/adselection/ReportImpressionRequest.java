// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.adselection;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class ReportImpressionRequest
{
    @NotNull
    private final AdSelectionConfig adSelectionConfig;
    private final long adSelectionId;
    
    public ReportImpressionRequest(final long adSelectionId, @NotNull final AdSelectionConfig adSelectionConfig) {
        Intrinsics.checkNotNullParameter((Object)adSelectionConfig, "adSelectionConfig");
        this.adSelectionId = adSelectionId;
        this.adSelectionConfig = adSelectionConfig;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof ReportImpressionRequest)) {
            return false;
        }
        final long adSelectionId = this.adSelectionId;
        final ReportImpressionRequest reportImpressionRequest = (ReportImpressionRequest)o;
        if (adSelectionId != reportImpressionRequest.adSelectionId || !Intrinsics.\u3007o\u3007((Object)this.adSelectionConfig, (Object)reportImpressionRequest.adSelectionConfig)) {
            b = false;
        }
        return b;
    }
    
    @NotNull
    public final AdSelectionConfig getAdSelectionConfig() {
        return this.adSelectionConfig;
    }
    
    public final long getAdSelectionId() {
        return this.adSelectionId;
    }
    
    @Override
    public int hashCode() {
        return \u3007080.\u3007080(this.adSelectionId) * 31 + this.adSelectionConfig.hashCode();
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ReportImpressionRequest: adSelectionId=");
        sb.append(this.adSelectionId);
        sb.append(", adSelectionConfig=");
        sb.append(this.adSelectionConfig);
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.adselection;

import kotlin.jvm.internal.Intrinsics;
import java.util.Map;
import android.net.Uri;
import androidx.privacysandbox.ads.adservices.common.AdTechIdentifier;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import androidx.privacysandbox.ads.adservices.common.AdSelectionSignals;
import kotlin.Metadata;

@Metadata
public final class AdSelectionConfig
{
    @NotNull
    private final AdSelectionSignals adSelectionSignals;
    @NotNull
    private final List<AdTechIdentifier> customAudienceBuyers;
    @NotNull
    private final Uri decisionLogicUri;
    @NotNull
    private final Map<AdTechIdentifier, AdSelectionSignals> perBuyerSignals;
    @NotNull
    private final AdTechIdentifier seller;
    @NotNull
    private final AdSelectionSignals sellerSignals;
    @NotNull
    private final Uri trustedScoringSignalsUri;
    
    public AdSelectionConfig(@NotNull final AdTechIdentifier seller, @NotNull final Uri decisionLogicUri, @NotNull final List<AdTechIdentifier> customAudienceBuyers, @NotNull final AdSelectionSignals adSelectionSignals, @NotNull final AdSelectionSignals sellerSignals, @NotNull final Map<AdTechIdentifier, AdSelectionSignals> perBuyerSignals, @NotNull final Uri trustedScoringSignalsUri) {
        Intrinsics.checkNotNullParameter((Object)seller, "seller");
        Intrinsics.checkNotNullParameter((Object)decisionLogicUri, "decisionLogicUri");
        Intrinsics.checkNotNullParameter((Object)customAudienceBuyers, "customAudienceBuyers");
        Intrinsics.checkNotNullParameter((Object)adSelectionSignals, "adSelectionSignals");
        Intrinsics.checkNotNullParameter((Object)sellerSignals, "sellerSignals");
        Intrinsics.checkNotNullParameter((Object)perBuyerSignals, "perBuyerSignals");
        Intrinsics.checkNotNullParameter((Object)trustedScoringSignalsUri, "trustedScoringSignalsUri");
        this.seller = seller;
        this.decisionLogicUri = decisionLogicUri;
        this.customAudienceBuyers = customAudienceBuyers;
        this.adSelectionSignals = adSelectionSignals;
        this.sellerSignals = sellerSignals;
        this.perBuyerSignals = perBuyerSignals;
        this.trustedScoringSignalsUri = trustedScoringSignalsUri;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof AdSelectionConfig)) {
            return false;
        }
        final AdTechIdentifier seller = this.seller;
        final AdSelectionConfig adSelectionConfig = (AdSelectionConfig)o;
        if (!Intrinsics.\u3007o\u3007((Object)seller, (Object)adSelectionConfig.seller) || !Intrinsics.\u3007o\u3007((Object)this.decisionLogicUri, (Object)adSelectionConfig.decisionLogicUri) || !Intrinsics.\u3007o\u3007((Object)this.customAudienceBuyers, (Object)adSelectionConfig.customAudienceBuyers) || !Intrinsics.\u3007o\u3007((Object)this.adSelectionSignals, (Object)adSelectionConfig.adSelectionSignals) || !Intrinsics.\u3007o\u3007((Object)this.sellerSignals, (Object)adSelectionConfig.sellerSignals) || !Intrinsics.\u3007o\u3007((Object)this.perBuyerSignals, (Object)adSelectionConfig.perBuyerSignals) || !Intrinsics.\u3007o\u3007((Object)this.trustedScoringSignalsUri, (Object)adSelectionConfig.trustedScoringSignalsUri)) {
            b = false;
        }
        return b;
    }
    
    @NotNull
    public final AdSelectionSignals getAdSelectionSignals() {
        return this.adSelectionSignals;
    }
    
    @NotNull
    public final List<AdTechIdentifier> getCustomAudienceBuyers() {
        return this.customAudienceBuyers;
    }
    
    @NotNull
    public final Uri getDecisionLogicUri() {
        return this.decisionLogicUri;
    }
    
    @NotNull
    public final Map<AdTechIdentifier, AdSelectionSignals> getPerBuyerSignals() {
        return this.perBuyerSignals;
    }
    
    @NotNull
    public final AdTechIdentifier getSeller() {
        return this.seller;
    }
    
    @NotNull
    public final AdSelectionSignals getSellerSignals() {
        return this.sellerSignals;
    }
    
    @NotNull
    public final Uri getTrustedScoringSignalsUri() {
        return this.trustedScoringSignalsUri;
    }
    
    @Override
    public int hashCode() {
        return (((((this.seller.hashCode() * 31 + this.decisionLogicUri.hashCode()) * 31 + this.customAudienceBuyers.hashCode()) * 31 + this.adSelectionSignals.hashCode()) * 31 + this.sellerSignals.hashCode()) * 31 + this.perBuyerSignals.hashCode()) * 31 + this.trustedScoringSignalsUri.hashCode();
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("AdSelectionConfig: seller=");
        sb.append(this.seller);
        sb.append(", decisionLogicUri='");
        sb.append(this.decisionLogicUri);
        sb.append("', customAudienceBuyers=");
        sb.append(this.customAudienceBuyers);
        sb.append(", adSelectionSignals=");
        sb.append(this.adSelectionSignals);
        sb.append(", sellerSignals=");
        sb.append(this.sellerSignals);
        sb.append(", perBuyerSignals=");
        sb.append(this.perBuyerSignals);
        sb.append(", trustedScoringSignalsUri=");
        sb.append(this.trustedScoringSignalsUri);
        return sb.toString();
    }
}

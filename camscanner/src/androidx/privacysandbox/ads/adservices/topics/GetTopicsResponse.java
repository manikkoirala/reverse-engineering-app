// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.topics;

import java.util.Objects;
import java.util.HashSet;
import java.util.Collection;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import java.util.List;
import kotlin.Metadata;

@Metadata
public final class GetTopicsResponse
{
    @NotNull
    private final List<Topic> topics;
    
    public GetTopicsResponse(@NotNull final List<Topic> topics) {
        Intrinsics.checkNotNullParameter((Object)topics, "topics");
        this.topics = topics;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GetTopicsResponse)) {
            return false;
        }
        final int size = this.topics.size();
        final GetTopicsResponse getTopicsResponse = (GetTopicsResponse)o;
        return size == getTopicsResponse.topics.size() && Intrinsics.\u3007o\u3007((Object)new HashSet(this.topics), (Object)new HashSet(getTopicsResponse.topics));
    }
    
    @NotNull
    public final List<Topic> getTopics() {
        return this.topics;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(this.topics);
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Topics=");
        sb.append(this.topics);
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.topics;

import androidx.appcompat.widget.oo88o8O;
import android.adservices.topics.TopicsManager;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.content.Context;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresExtension;
import android.annotation.SuppressLint;
import kotlin.Metadata;

@Metadata
@SuppressLint({ "NewApi", "ClassVerificationFailure" })
@RequiresExtension(extension = 1000000, version = 4)
@RestrictTo({ RestrictTo.Scope.LIBRARY })
public final class TopicsManagerApi33Ext4Impl extends TopicsManagerImplCommon
{
    public TopicsManagerApi33Ext4Impl(@NotNull final Context context) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        final Object \u3007080 = oo88o8O.\u3007080(context, (Class)android.adservices.topics.TopicsManager.class);
        Intrinsics.checkNotNullExpressionValue(\u3007080, "context.getSystemService\u2026opicsManager::class.java)");
        super((android.adservices.topics.TopicsManager)\u3007080);
    }
}

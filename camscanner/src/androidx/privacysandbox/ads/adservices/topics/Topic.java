// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.topics;

import org.jetbrains.annotations.NotNull;
import androidx.privacysandbox.ads.adservices.adselection.\u3007080;
import kotlin.Metadata;

@Metadata
public final class Topic
{
    private final long modelVersion;
    private final long taxonomyVersion;
    private final int topicId;
    
    public Topic(final long taxonomyVersion, final long modelVersion, final int topicId) {
        this.taxonomyVersion = taxonomyVersion;
        this.modelVersion = modelVersion;
        this.topicId = topicId;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof Topic)) {
            return false;
        }
        final long taxonomyVersion = this.taxonomyVersion;
        final Topic topic = (Topic)o;
        if (taxonomyVersion != topic.taxonomyVersion || this.modelVersion != topic.modelVersion || this.topicId != topic.topicId) {
            b = false;
        }
        return b;
    }
    
    public final long getModelVersion() {
        return this.modelVersion;
    }
    
    public final long getTaxonomyVersion() {
        return this.taxonomyVersion;
    }
    
    public final int getTopicId() {
        return this.topicId;
    }
    
    @Override
    public int hashCode() {
        return (\u3007080.\u3007080(this.taxonomyVersion) * 31 + \u3007080.\u3007080(this.modelVersion)) * 31 + this.topicId;
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("TaxonomyVersion=");
        sb.append(this.taxonomyVersion);
        sb.append(", ModelVersion=");
        sb.append(this.modelVersion);
        sb.append(", TopicCode=");
        sb.append(this.topicId);
        sb.append(" }");
        final String string = sb.toString();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Topic { ");
        sb2.append(string);
        return sb2.toString();
    }
}

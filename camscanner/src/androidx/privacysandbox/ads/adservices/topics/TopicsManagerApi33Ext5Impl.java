// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.topics;

import android.adservices.topics.GetTopicsRequest$Builder;
import androidx.appcompat.widget.oo88o8O;
import android.adservices.topics.TopicsManager;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.content.Context;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresExtension;
import android.annotation.SuppressLint;
import kotlin.Metadata;

@Metadata
@SuppressLint({ "NewApi", "ClassVerificationFailure" })
@RequiresExtension(extension = 1000000, version = 5)
@RestrictTo({ RestrictTo.Scope.LIBRARY })
public final class TopicsManagerApi33Ext5Impl extends TopicsManagerImplCommon
{
    public TopicsManagerApi33Ext5Impl(@NotNull final Context context) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        final Object \u3007080 = oo88o8O.\u3007080(context, (Class)android.adservices.topics.TopicsManager.class);
        Intrinsics.checkNotNullExpressionValue(\u3007080, "context.getSystemService\u2026opicsManager::class.java)");
        super((android.adservices.topics.TopicsManager)\u3007080);
    }
    
    @NotNull
    @Override
    public android.adservices.topics.GetTopicsRequest convertRequest$ads_adservices_release(@NotNull final GetTopicsRequest getTopicsRequest) {
        Intrinsics.checkNotNullParameter((Object)getTopicsRequest, "request");
        final android.adservices.topics.GetTopicsRequest build = new GetTopicsRequest$Builder().setAdsSdkName(getTopicsRequest.getAdsSdkName()).setShouldRecordObservation(getTopicsRequest.shouldRecordObservation()).build();
        Intrinsics.checkNotNullExpressionValue((Object)build, "Builder()\n            .s\u2026ion)\n            .build()");
        return build;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.topics;

import androidx.privacysandbox.ads.adservices.adid.\u3007080;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class GetTopicsRequest
{
    @NotNull
    private final String adsSdkName;
    private final boolean shouldRecordObservation;
    
    public GetTopicsRequest() {
        this(null, false, 3, null);
    }
    
    public GetTopicsRequest(@NotNull final String adsSdkName, final boolean shouldRecordObservation) {
        Intrinsics.checkNotNullParameter((Object)adsSdkName, "adsSdkName");
        this.adsSdkName = adsSdkName;
        this.shouldRecordObservation = shouldRecordObservation;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof GetTopicsRequest)) {
            return false;
        }
        final String adsSdkName = this.adsSdkName;
        final GetTopicsRequest getTopicsRequest = (GetTopicsRequest)o;
        if (!Intrinsics.\u3007o\u3007((Object)adsSdkName, (Object)getTopicsRequest.adsSdkName) || this.shouldRecordObservation != getTopicsRequest.shouldRecordObservation) {
            b = false;
        }
        return b;
    }
    
    @NotNull
    public final String getAdsSdkName() {
        return this.adsSdkName;
    }
    
    @Override
    public int hashCode() {
        return this.adsSdkName.hashCode() * 31 + \u3007080.\u3007080(this.shouldRecordObservation);
    }
    
    public final boolean shouldRecordObservation() {
        return this.shouldRecordObservation;
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("GetTopicsRequest: adsSdkName=");
        sb.append(this.adsSdkName);
        sb.append(", shouldRecordObservation=");
        sb.append(this.shouldRecordObservation);
        return sb.toString();
    }
    
    @Metadata
    public static final class Builder
    {
        @NotNull
        private String adsSdkName;
        private boolean shouldRecordObservation;
        
        public Builder() {
            this.adsSdkName = "";
            this.shouldRecordObservation = true;
        }
        
        @NotNull
        public final GetTopicsRequest build() {
            if (this.adsSdkName.length() > 0) {
                return new GetTopicsRequest(this.adsSdkName, this.shouldRecordObservation);
            }
            throw new IllegalStateException("adsSdkName must be set".toString());
        }
        
        @NotNull
        public final Builder setAdsSdkName(@NotNull final String adsSdkName) {
            Intrinsics.checkNotNullParameter((Object)adsSdkName, "adsSdkName");
            this.adsSdkName = adsSdkName;
            return this;
        }
        
        @NotNull
        public final Builder setShouldRecordObservation(final boolean shouldRecordObservation) {
            this.shouldRecordObservation = shouldRecordObservation;
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.topics;

import java.util.Iterator;
import java.util.List;
import android.adservices.topics.Topic;
import java.util.ArrayList;
import android.adservices.topics.GetTopicsRequest$Builder;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import android.os.OutcomeReceiver;
import java.util.concurrent.Executor;
import androidx.core.os.OutcomeReceiverKt;
import androidx.privacysandbox.ads.adservices.adid.\u3007o00\u3007\u3007Oo;
import kotlinx.coroutines.CancellableContinuationImpl;
import androidx.annotation.RequiresPermission;
import androidx.annotation.DoNotInline;
import kotlin.ResultKt;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import android.adservices.topics.GetTopicsResponse;
import kotlin.coroutines.Continuation;
import android.adservices.topics.GetTopicsRequest;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresExtension;
import android.annotation.SuppressLint;
import kotlin.Metadata;

@Metadata
@SuppressLint({ "NewApi" })
@RequiresExtension(extension = 1000000, version = 4)
@RestrictTo({ RestrictTo.Scope.LIBRARY })
public class TopicsManagerImplCommon extends TopicsManager
{
    @NotNull
    private final android.adservices.topics.TopicsManager mTopicsManager;
    
    public TopicsManagerImplCommon(@NotNull final android.adservices.topics.TopicsManager mTopicsManager) {
        Intrinsics.checkNotNullParameter((Object)mTopicsManager, "mTopicsManager");
        this.mTopicsManager = mTopicsManager;
    }
    
    public static final /* synthetic */ android.adservices.topics.TopicsManager access$getMTopicsManager$p(final TopicsManagerImplCommon topicsManagerImplCommon) {
        return topicsManagerImplCommon.mTopicsManager;
    }
    
    @DoNotInline
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_TOPICS")
    static /* synthetic */ Object getTopics$suspendImpl(TopicsManagerImplCommon l$0, final androidx.privacysandbox.ads.adservices.topics.GetTopicsRequest getTopicsRequest, final Continuation<? super androidx.privacysandbox.ads.adservices.topics.GetTopicsResponse> continuation) {
        Object o = null;
        Label_0051: {
            if (continuation instanceof TopicsManagerImplCommon$getTopics.TopicsManagerImplCommon$getTopics$1) {
                final TopicsManagerImplCommon$getTopics.TopicsManagerImplCommon$getTopics$1 topicsManagerImplCommon$getTopics$1 = (TopicsManagerImplCommon$getTopics.TopicsManagerImplCommon$getTopics$1)continuation;
                final int label = topicsManagerImplCommon$getTopics$1.label;
                if ((label & Integer.MIN_VALUE) != 0x0) {
                    topicsManagerImplCommon$getTopics$1.label = label + Integer.MIN_VALUE;
                    o = topicsManagerImplCommon$getTopics$1;
                    break Label_0051;
                }
            }
            o = new TopicsManagerImplCommon$getTopics.TopicsManagerImplCommon$getTopics$1(l$0, (Continuation)continuation);
        }
        final Object result = ((TopicsManagerImplCommon$getTopics.TopicsManagerImplCommon$getTopics$1)o).result;
        final Object o2 = IntrinsicsKt.O8();
        final int label2 = ((TopicsManagerImplCommon$getTopics.TopicsManagerImplCommon$getTopics$1)o).label;
        Object topicsAsyncInternal;
        if (label2 != 0) {
            if (label2 != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            l$0 = (TopicsManagerImplCommon)((TopicsManagerImplCommon$getTopics.TopicsManagerImplCommon$getTopics$1)o).L$0;
            ResultKt.\u3007o00\u3007\u3007Oo(result);
            topicsAsyncInternal = result;
        }
        else {
            ResultKt.\u3007o00\u3007\u3007Oo(result);
            final GetTopicsRequest convertRequest$ads_adservices_release = l$0.convertRequest$ads_adservices_release(getTopicsRequest);
            ((TopicsManagerImplCommon$getTopics.TopicsManagerImplCommon$getTopics$1)o).L$0 = l$0;
            ((TopicsManagerImplCommon$getTopics.TopicsManagerImplCommon$getTopics$1)o).label = 1;
            if ((topicsAsyncInternal = l$0.getTopicsAsyncInternal(convertRequest$ads_adservices_release, (Continuation<? super GetTopicsResponse>)o)) == o2) {
                return o2;
            }
        }
        return l$0.convertResponse$ads_adservices_release((GetTopicsResponse)topicsAsyncInternal);
    }
    
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_TOPICS")
    private final Object getTopicsAsyncInternal(final GetTopicsRequest getTopicsRequest, final Continuation<? super GetTopicsResponse> continuation) {
        final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.\u3007o\u3007((Continuation)continuation), 1);
        cancellableContinuationImpl.O8ooOoo\u3007();
        access$getMTopicsManager$p(this).getTopics(getTopicsRequest, (Executor)new \u3007o00\u3007\u3007Oo(), (OutcomeReceiver)OutcomeReceiverKt.asOutcomeReceiver((kotlin.coroutines.Continuation<? super Object>)cancellableContinuationImpl));
        final Object o\u3007O8\u3007\u3007o = cancellableContinuationImpl.o\u3007O8\u3007\u3007o();
        if (o\u3007O8\u3007\u3007o == IntrinsicsKt.O8()) {
            DebugProbesKt.\u3007o\u3007((Continuation)continuation);
        }
        return o\u3007O8\u3007\u3007o;
    }
    
    @NotNull
    public GetTopicsRequest convertRequest$ads_adservices_release(@NotNull final androidx.privacysandbox.ads.adservices.topics.GetTopicsRequest getTopicsRequest) {
        Intrinsics.checkNotNullParameter((Object)getTopicsRequest, "request");
        final GetTopicsRequest build = new GetTopicsRequest$Builder().setAdsSdkName(getTopicsRequest.getAdsSdkName()).build();
        Intrinsics.checkNotNullExpressionValue((Object)build, "Builder()\n            .s\u2026ame)\n            .build()");
        return build;
    }
    
    @NotNull
    public final androidx.privacysandbox.ads.adservices.topics.GetTopicsResponse convertResponse$ads_adservices_release(@NotNull final GetTopicsResponse getTopicsResponse) {
        Intrinsics.checkNotNullParameter((Object)getTopicsResponse, "response");
        final ArrayList list = new ArrayList();
        for (final Topic topic : getTopicsResponse.getTopics()) {
            list.add(new androidx.privacysandbox.ads.adservices.topics.Topic(topic.getTaxonomyVersion(), topic.getModelVersion(), topic.getTopicId()));
        }
        return new androidx.privacysandbox.ads.adservices.topics.GetTopicsResponse(list);
    }
    
    @DoNotInline
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_TOPICS")
    @Override
    public Object getTopics(@NotNull final androidx.privacysandbox.ads.adservices.topics.GetTopicsRequest getTopicsRequest, @NotNull final Continuation<? super androidx.privacysandbox.ads.adservices.topics.GetTopicsResponse> continuation) {
        return getTopics$suspendImpl(this, getTopicsRequest, continuation);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.topics;

import androidx.privacysandbox.ads.adservices.internal.AdServicesInfo;
import kotlin.jvm.internal.Intrinsics;
import androidx.annotation.RequiresPermission;
import kotlin.coroutines.Continuation;
import android.annotation.SuppressLint;
import android.content.Context;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public abstract class TopicsManager
{
    @NotNull
    public static final Companion Companion;
    
    static {
        Companion = new Companion(null);
    }
    
    @SuppressLint({ "NewApi", "ClassVerificationFailure" })
    public static final TopicsManager obtain(@NotNull final Context context) {
        return TopicsManager.Companion.obtain(context);
    }
    
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_TOPICS")
    public abstract Object getTopics(@NotNull final GetTopicsRequest p0, @NotNull final Continuation<? super GetTopicsResponse> p1);
    
    @Metadata
    public static final class Companion
    {
        private Companion() {
        }
        
        @SuppressLint({ "NewApi", "ClassVerificationFailure" })
        public final TopicsManager obtain(@NotNull final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            final AdServicesInfo instance = AdServicesInfo.INSTANCE;
            TopicsManagerImplCommon topicsManagerImplCommon;
            if (instance.version() >= 5) {
                topicsManagerImplCommon = new TopicsManagerApi33Ext5Impl(context);
            }
            else if (instance.version() == 4) {
                topicsManagerImplCommon = new TopicsManagerApi33Ext4Impl(context);
            }
            else {
                topicsManagerImplCommon = null;
            }
            return topicsManagerImplCommon;
        }
    }
}

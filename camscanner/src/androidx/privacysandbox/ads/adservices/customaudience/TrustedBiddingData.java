// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.customaudience;

import kotlin.jvm.internal.Intrinsics;
import android.net.Uri;
import org.jetbrains.annotations.NotNull;
import java.util.List;
import kotlin.Metadata;

@Metadata
public final class TrustedBiddingData
{
    @NotNull
    private final List<String> trustedBiddingKeys;
    @NotNull
    private final Uri trustedBiddingUri;
    
    public TrustedBiddingData(@NotNull final Uri trustedBiddingUri, @NotNull final List<String> trustedBiddingKeys) {
        Intrinsics.checkNotNullParameter((Object)trustedBiddingUri, "trustedBiddingUri");
        Intrinsics.checkNotNullParameter((Object)trustedBiddingKeys, "trustedBiddingKeys");
        this.trustedBiddingUri = trustedBiddingUri;
        this.trustedBiddingKeys = trustedBiddingKeys;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof TrustedBiddingData)) {
            return false;
        }
        final Uri trustedBiddingUri = this.trustedBiddingUri;
        final TrustedBiddingData trustedBiddingData = (TrustedBiddingData)o;
        if (!Intrinsics.\u3007o\u3007((Object)trustedBiddingUri, (Object)trustedBiddingData.trustedBiddingUri) || !Intrinsics.\u3007o\u3007((Object)this.trustedBiddingKeys, (Object)trustedBiddingData.trustedBiddingKeys)) {
            b = false;
        }
        return b;
    }
    
    @NotNull
    public final List<String> getTrustedBiddingKeys() {
        return this.trustedBiddingKeys;
    }
    
    @NotNull
    public final Uri getTrustedBiddingUri() {
        return this.trustedBiddingUri;
    }
    
    @Override
    public int hashCode() {
        return this.trustedBiddingUri.hashCode() * 31 + this.trustedBiddingKeys.hashCode();
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("TrustedBiddingData: trustedBiddingUri=");
        sb.append(this.trustedBiddingUri);
        sb.append(" trustedBiddingKeys=");
        sb.append(this.trustedBiddingKeys);
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.customaudience;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class JoinCustomAudienceRequest
{
    @NotNull
    private final CustomAudience customAudience;
    
    public JoinCustomAudienceRequest(@NotNull final CustomAudience customAudience) {
        Intrinsics.checkNotNullParameter((Object)customAudience, "customAudience");
        this.customAudience = customAudience;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof JoinCustomAudienceRequest && Intrinsics.\u3007o\u3007((Object)this.customAudience, (Object)((JoinCustomAudienceRequest)o).customAudience));
    }
    
    @NotNull
    public final CustomAudience getCustomAudience() {
        return this.customAudience;
    }
    
    @Override
    public int hashCode() {
        return this.customAudience.hashCode();
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("JoinCustomAudience: customAudience=");
        sb.append(this.customAudience);
        return sb.toString();
    }
}

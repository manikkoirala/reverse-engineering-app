// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.customaudience;

import androidx.privacysandbox.ads.adservices.internal.AdServicesInfo;
import androidx.annotation.DoNotInline;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import android.os.OutcomeReceiver;
import java.util.concurrent.Executor;
import androidx.core.os.OutcomeReceiverKt;
import androidx.privacysandbox.ads.adservices.adid.\u3007o00\u3007\u3007Oo;
import kotlinx.coroutines.CancellableContinuationImpl;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import android.adservices.customaudience.TrustedBiddingData$Builder;
import android.adservices.customaudience.LeaveCustomAudienceRequest$Builder;
import android.adservices.customaudience.JoinCustomAudienceRequest$Builder;
import android.adservices.customaudience.CustomAudience$Builder;
import androidx.privacysandbox.ads.adservices.common.AdSelectionSignals;
import androidx.privacysandbox.ads.adservices.common.AdTechIdentifier;
import java.util.Iterator;
import android.adservices.common.AdData$Builder;
import java.util.ArrayList;
import androidx.privacysandbox.ads.adservices.common.AdData;
import java.util.List;
import androidx.appcompat.widget.oo88o8O;
import kotlin.jvm.internal.Intrinsics;
import androidx.annotation.RequiresExtension;
import androidx.annotation.RequiresPermission;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import android.annotation.SuppressLint;
import android.content.Context;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public abstract class CustomAudienceManager
{
    @NotNull
    public static final Companion Companion;
    
    static {
        Companion = new Companion(null);
    }
    
    @SuppressLint({ "NewApi", "ClassVerificationFailure" })
    public static final CustomAudienceManager obtain(@NotNull final Context context) {
        return CustomAudienceManager.Companion.obtain(context);
    }
    
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_CUSTOM_AUDIENCE")
    public abstract Object joinCustomAudience(@NotNull final JoinCustomAudienceRequest p0, @NotNull final Continuation<? super Unit> p1);
    
    @RequiresPermission("android.permission.ACCESS_ADSERVICES_CUSTOM_AUDIENCE")
    public abstract Object leaveCustomAudience(@NotNull final LeaveCustomAudienceRequest p0, @NotNull final Continuation<? super Unit> p1);
    
    @Metadata
    @SuppressLint({ "ClassVerificationFailure", "NewApi" })
    @RequiresExtension(extension = 1000000, version = 4)
    private static final class Api33Ext4Impl extends CustomAudienceManager
    {
        @NotNull
        private final android.adservices.customaudience.CustomAudienceManager customAudienceManager;
        
        public Api33Ext4Impl(@NotNull final android.adservices.customaudience.CustomAudienceManager customAudienceManager) {
            Intrinsics.checkNotNullParameter((Object)customAudienceManager, "customAudienceManager");
            this.customAudienceManager = customAudienceManager;
        }
        
        public Api33Ext4Impl(@NotNull final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            final Object \u3007080 = oo88o8O.\u3007080(context, (Class)android.adservices.customaudience.CustomAudienceManager.class);
            Intrinsics.checkNotNullExpressionValue(\u3007080, "context.getSystemService\u2026:class.java\n            )");
            this((android.adservices.customaudience.CustomAudienceManager)\u3007080);
        }
        
        public static final /* synthetic */ android.adservices.customaudience.CustomAudienceManager access$getCustomAudienceManager$p(final Api33Ext4Impl api33Ext4Impl) {
            return api33Ext4Impl.customAudienceManager;
        }
        
        private final List<android.adservices.common.AdData> convertAdData(final List<AdData> list) {
            final ArrayList list2 = new ArrayList();
            for (final AdData adData : list) {
                final android.adservices.common.AdData build = new AdData$Builder().setMetadata(adData.getMetadata()).setRenderUri(adData.getRenderUri()).build();
                Intrinsics.checkNotNullExpressionValue((Object)build, "Builder()\n              \u2026                 .build()");
                list2.add(build);
            }
            return list2;
        }
        
        private final android.adservices.common.AdTechIdentifier convertAdTechIdentifier(final AdTechIdentifier adTechIdentifier) {
            final android.adservices.common.AdTechIdentifier fromString = android.adservices.common.AdTechIdentifier.fromString(adTechIdentifier.getIdentifier());
            Intrinsics.checkNotNullExpressionValue((Object)fromString, "fromString(input.identifier)");
            return fromString;
        }
        
        private final android.adservices.common.AdSelectionSignals convertBiddingSignals(final AdSelectionSignals adSelectionSignals) {
            if (adSelectionSignals == null) {
                return null;
            }
            return android.adservices.common.AdSelectionSignals.fromString(adSelectionSignals.getSignals());
        }
        
        private final android.adservices.customaudience.CustomAudience convertCustomAudience(final CustomAudience customAudience) {
            final android.adservices.customaudience.CustomAudience build = new CustomAudience$Builder().setActivationTime(customAudience.getActivationTime()).setAds((List)this.convertAdData(customAudience.getAds())).setBiddingLogicUri(customAudience.getBiddingLogicUri()).setBuyer(this.convertAdTechIdentifier(customAudience.getBuyer())).setDailyUpdateUri(customAudience.getDailyUpdateUri()).setExpirationTime(customAudience.getExpirationTime()).setName(customAudience.getName()).setTrustedBiddingData(this.convertTrustedSignals(customAudience.getTrustedBiddingSignals())).setUserBiddingSignals(this.convertBiddingSignals(customAudience.getUserBiddingSignals())).build();
            Intrinsics.checkNotNullExpressionValue((Object)build, "Builder()\n              \u2026\n                .build()");
            return build;
        }
        
        private final android.adservices.customaudience.JoinCustomAudienceRequest convertJoinRequest(final JoinCustomAudienceRequest joinCustomAudienceRequest) {
            final android.adservices.customaudience.JoinCustomAudienceRequest build = new JoinCustomAudienceRequest$Builder().setCustomAudience(this.convertCustomAudience(joinCustomAudienceRequest.getCustomAudience())).build();
            Intrinsics.checkNotNullExpressionValue((Object)build, "Builder()\n              \u2026\n                .build()");
            return build;
        }
        
        private final android.adservices.customaudience.LeaveCustomAudienceRequest convertLeaveRequest(final LeaveCustomAudienceRequest leaveCustomAudienceRequest) {
            final android.adservices.customaudience.LeaveCustomAudienceRequest build = new LeaveCustomAudienceRequest$Builder().setBuyer(this.convertAdTechIdentifier(leaveCustomAudienceRequest.getBuyer())).setName(leaveCustomAudienceRequest.getName()).build();
            Intrinsics.checkNotNullExpressionValue((Object)build, "Builder()\n              \u2026\n                .build()");
            return build;
        }
        
        private final android.adservices.customaudience.TrustedBiddingData convertTrustedSignals(final TrustedBiddingData trustedBiddingData) {
            if (trustedBiddingData == null) {
                return null;
            }
            return new TrustedBiddingData$Builder().setTrustedBiddingKeys((List)trustedBiddingData.getTrustedBiddingKeys()).setTrustedBiddingUri(trustedBiddingData.getTrustedBiddingUri()).build();
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_ADSERVICES_CUSTOM_AUDIENCE")
        @Override
        public Object joinCustomAudience(@NotNull final JoinCustomAudienceRequest joinCustomAudienceRequest, @NotNull final Continuation<? super Unit> continuation) {
            final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.\u3007o\u3007((Continuation)continuation), 1);
            cancellableContinuationImpl.O8ooOoo\u3007();
            access$getCustomAudienceManager$p(this).joinCustomAudience(this.convertJoinRequest(joinCustomAudienceRequest), (Executor)new \u3007o00\u3007\u3007Oo(), (OutcomeReceiver)OutcomeReceiverKt.asOutcomeReceiver((kotlin.coroutines.Continuation<? super Object>)cancellableContinuationImpl));
            final Object o\u3007O8\u3007\u3007o = cancellableContinuationImpl.o\u3007O8\u3007\u3007o();
            if (o\u3007O8\u3007\u3007o == IntrinsicsKt.O8()) {
                DebugProbesKt.\u3007o\u3007((Continuation)continuation);
            }
            if (o\u3007O8\u3007\u3007o == IntrinsicsKt.O8()) {
                return o\u3007O8\u3007\u3007o;
            }
            return Unit.\u3007080;
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_ADSERVICES_CUSTOM_AUDIENCE")
        @Override
        public Object leaveCustomAudience(@NotNull final LeaveCustomAudienceRequest leaveCustomAudienceRequest, @NotNull final Continuation<? super Unit> continuation) {
            final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.\u3007o\u3007((Continuation)continuation), 1);
            cancellableContinuationImpl.O8ooOoo\u3007();
            access$getCustomAudienceManager$p(this).leaveCustomAudience(this.convertLeaveRequest(leaveCustomAudienceRequest), (Executor)new \u3007o00\u3007\u3007Oo(), (OutcomeReceiver)OutcomeReceiverKt.asOutcomeReceiver((kotlin.coroutines.Continuation<? super Object>)cancellableContinuationImpl));
            final Object o\u3007O8\u3007\u3007o = cancellableContinuationImpl.o\u3007O8\u3007\u3007o();
            if (o\u3007O8\u3007\u3007o == IntrinsicsKt.O8()) {
                DebugProbesKt.\u3007o\u3007((Continuation)continuation);
            }
            if (o\u3007O8\u3007\u3007o == IntrinsicsKt.O8()) {
                return o\u3007O8\u3007\u3007o;
            }
            return Unit.\u3007080;
        }
    }
    
    @Metadata
    public static final class Companion
    {
        private Companion() {
        }
        
        @SuppressLint({ "NewApi", "ClassVerificationFailure" })
        public final CustomAudienceManager obtain(@NotNull final Context context) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Api33Ext4Impl api33Ext4Impl;
            if (AdServicesInfo.INSTANCE.version() >= 4) {
                api33Ext4Impl = new Api33Ext4Impl(context);
            }
            else {
                api33Ext4Impl = null;
            }
            return api33Ext4Impl;
        }
    }
}

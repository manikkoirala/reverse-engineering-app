// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.customaudience;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import androidx.privacysandbox.ads.adservices.common.AdTechIdentifier;
import kotlin.Metadata;

@Metadata
public final class LeaveCustomAudienceRequest
{
    @NotNull
    private final AdTechIdentifier buyer;
    @NotNull
    private final String name;
    
    public LeaveCustomAudienceRequest(@NotNull final AdTechIdentifier buyer, @NotNull final String name) {
        Intrinsics.checkNotNullParameter((Object)buyer, "buyer");
        Intrinsics.checkNotNullParameter((Object)name, "name");
        this.buyer = buyer;
        this.name = name;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof LeaveCustomAudienceRequest)) {
            return false;
        }
        final AdTechIdentifier buyer = this.buyer;
        final LeaveCustomAudienceRequest leaveCustomAudienceRequest = (LeaveCustomAudienceRequest)o;
        if (!Intrinsics.\u3007o\u3007((Object)buyer, (Object)leaveCustomAudienceRequest.buyer) || !Intrinsics.\u3007o\u3007((Object)this.name, (Object)leaveCustomAudienceRequest.name)) {
            b = false;
        }
        return b;
    }
    
    @NotNull
    public final AdTechIdentifier getBuyer() {
        return this.buyer;
    }
    
    @NotNull
    public final String getName() {
        return this.name;
    }
    
    @Override
    public int hashCode() {
        return this.buyer.hashCode() * 31 + this.name.hashCode();
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("LeaveCustomAudience: buyer=");
        sb.append(this.buyer);
        sb.append(", name=");
        sb.append(this.name);
        return sb.toString();
    }
}

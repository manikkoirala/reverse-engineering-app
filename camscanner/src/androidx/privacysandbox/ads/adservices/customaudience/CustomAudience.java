// 
// Decompiled by Procyon v0.6.0
// 

package androidx.privacysandbox.ads.adservices.customaudience;

import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import androidx.privacysandbox.ads.adservices.common.AdSelectionSignals;
import androidx.privacysandbox.ads.adservices.common.AdTechIdentifier;
import android.net.Uri;
import org.jetbrains.annotations.NotNull;
import androidx.privacysandbox.ads.adservices.common.AdData;
import java.util.List;
import java.time.Instant;
import kotlin.Metadata;

@Metadata
public final class CustomAudience
{
    private final Instant activationTime;
    @NotNull
    private final List<AdData> ads;
    @NotNull
    private final Uri biddingLogicUri;
    @NotNull
    private final AdTechIdentifier buyer;
    @NotNull
    private final Uri dailyUpdateUri;
    private final Instant expirationTime;
    @NotNull
    private final String name;
    private final TrustedBiddingData trustedBiddingSignals;
    private final AdSelectionSignals userBiddingSignals;
    
    public CustomAudience(@NotNull final AdTechIdentifier buyer, @NotNull final String name, @NotNull final Uri dailyUpdateUri, @NotNull final Uri biddingLogicUri, @NotNull final List<AdData> ads, final Instant activationTime, final Instant expirationTime, final AdSelectionSignals userBiddingSignals, final TrustedBiddingData trustedBiddingSignals) {
        Intrinsics.checkNotNullParameter((Object)buyer, "buyer");
        Intrinsics.checkNotNullParameter((Object)name, "name");
        Intrinsics.checkNotNullParameter((Object)dailyUpdateUri, "dailyUpdateUri");
        Intrinsics.checkNotNullParameter((Object)biddingLogicUri, "biddingLogicUri");
        Intrinsics.checkNotNullParameter((Object)ads, "ads");
        this.buyer = buyer;
        this.name = name;
        this.dailyUpdateUri = dailyUpdateUri;
        this.biddingLogicUri = biddingLogicUri;
        this.ads = ads;
        this.activationTime = activationTime;
        this.expirationTime = expirationTime;
        this.userBiddingSignals = userBiddingSignals;
        this.trustedBiddingSignals = trustedBiddingSignals;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof CustomAudience)) {
            return false;
        }
        final AdTechIdentifier buyer = this.buyer;
        final CustomAudience customAudience = (CustomAudience)o;
        if (!Intrinsics.\u3007o\u3007((Object)buyer, (Object)customAudience.buyer) || !Intrinsics.\u3007o\u3007((Object)this.name, (Object)customAudience.name) || !Intrinsics.\u3007o\u3007((Object)this.activationTime, (Object)customAudience.activationTime) || !Intrinsics.\u3007o\u3007((Object)this.expirationTime, (Object)customAudience.expirationTime) || !Intrinsics.\u3007o\u3007((Object)this.dailyUpdateUri, (Object)customAudience.dailyUpdateUri) || !Intrinsics.\u3007o\u3007((Object)this.userBiddingSignals, (Object)customAudience.userBiddingSignals) || !Intrinsics.\u3007o\u3007((Object)this.trustedBiddingSignals, (Object)customAudience.trustedBiddingSignals) || !Intrinsics.\u3007o\u3007((Object)this.ads, (Object)customAudience.ads)) {
            b = false;
        }
        return b;
    }
    
    public final Instant getActivationTime() {
        return this.activationTime;
    }
    
    @NotNull
    public final List<AdData> getAds() {
        return this.ads;
    }
    
    @NotNull
    public final Uri getBiddingLogicUri() {
        return this.biddingLogicUri;
    }
    
    @NotNull
    public final AdTechIdentifier getBuyer() {
        return this.buyer;
    }
    
    @NotNull
    public final Uri getDailyUpdateUri() {
        return this.dailyUpdateUri;
    }
    
    public final Instant getExpirationTime() {
        return this.expirationTime;
    }
    
    @NotNull
    public final String getName() {
        return this.name;
    }
    
    public final TrustedBiddingData getTrustedBiddingSignals() {
        return this.trustedBiddingSignals;
    }
    
    public final AdSelectionSignals getUserBiddingSignals() {
        return this.userBiddingSignals;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.buyer.hashCode();
        final int hashCode2 = this.name.hashCode();
        final Instant activationTime = this.activationTime;
        int hashCode3 = 0;
        int hashCode4;
        if (activationTime != null) {
            hashCode4 = activationTime.hashCode();
        }
        else {
            hashCode4 = 0;
        }
        final Instant expirationTime = this.expirationTime;
        int hashCode5;
        if (expirationTime != null) {
            hashCode5 = expirationTime.hashCode();
        }
        else {
            hashCode5 = 0;
        }
        final int hashCode6 = this.dailyUpdateUri.hashCode();
        final AdSelectionSignals userBiddingSignals = this.userBiddingSignals;
        int hashCode7;
        if (userBiddingSignals != null) {
            hashCode7 = userBiddingSignals.hashCode();
        }
        else {
            hashCode7 = 0;
        }
        final TrustedBiddingData trustedBiddingSignals = this.trustedBiddingSignals;
        if (trustedBiddingSignals != null) {
            hashCode3 = trustedBiddingSignals.hashCode();
        }
        return (((((((hashCode * 31 + hashCode2) * 31 + hashCode4) * 31 + hashCode5) * 31 + hashCode6) * 31 + hashCode7) * 31 + hashCode3) * 31 + this.biddingLogicUri.hashCode()) * 31 + this.ads.hashCode();
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("CustomAudience: buyer=");
        sb.append(this.biddingLogicUri);
        sb.append(", activationTime=");
        sb.append(this.activationTime);
        sb.append(", expirationTime=");
        sb.append(this.expirationTime);
        sb.append(", dailyUpdateUri=");
        sb.append(this.dailyUpdateUri);
        sb.append(", userBiddingSignals=");
        sb.append(this.userBiddingSignals);
        sb.append(", trustedBiddingSignals=");
        sb.append(this.trustedBiddingSignals);
        sb.append(", biddingLogicUri=");
        sb.append(this.biddingLogicUri);
        sb.append(", ads=");
        sb.append(this.ads);
        return sb.toString();
    }
    
    @Metadata
    public static final class Builder
    {
        private Instant activationTime;
        @NotNull
        private List<AdData> ads;
        @NotNull
        private Uri biddingLogicUri;
        @NotNull
        private AdTechIdentifier buyer;
        @NotNull
        private Uri dailyUpdateUri;
        private Instant expirationTime;
        @NotNull
        private String name;
        private TrustedBiddingData trustedBiddingData;
        private AdSelectionSignals userBiddingSignals;
        
        public Builder(@NotNull final AdTechIdentifier buyer, @NotNull final String name, @NotNull final Uri dailyUpdateUri, @NotNull final Uri biddingLogicUri, @NotNull final List<AdData> ads) {
            Intrinsics.checkNotNullParameter((Object)buyer, "buyer");
            Intrinsics.checkNotNullParameter((Object)name, "name");
            Intrinsics.checkNotNullParameter((Object)dailyUpdateUri, "dailyUpdateUri");
            Intrinsics.checkNotNullParameter((Object)biddingLogicUri, "biddingLogicUri");
            Intrinsics.checkNotNullParameter((Object)ads, "ads");
            this.buyer = buyer;
            this.name = name;
            this.dailyUpdateUri = dailyUpdateUri;
            this.biddingLogicUri = biddingLogicUri;
            this.ads = ads;
        }
        
        @NotNull
        public final CustomAudience build() {
            return new CustomAudience(this.buyer, this.name, this.dailyUpdateUri, this.biddingLogicUri, this.ads, this.activationTime, this.expirationTime, this.userBiddingSignals, this.trustedBiddingData);
        }
        
        @NotNull
        public final Builder setActivationTime(@NotNull final Instant activationTime) {
            Intrinsics.checkNotNullParameter((Object)activationTime, "activationTime");
            this.activationTime = activationTime;
            return this;
        }
        
        @NotNull
        public final Builder setAds(@NotNull final List<AdData> ads) {
            Intrinsics.checkNotNullParameter((Object)ads, "ads");
            this.ads = ads;
            return this;
        }
        
        @NotNull
        public final Builder setBiddingLogicUri(@NotNull final Uri biddingLogicUri) {
            Intrinsics.checkNotNullParameter((Object)biddingLogicUri, "biddingLogicUri");
            this.biddingLogicUri = biddingLogicUri;
            return this;
        }
        
        @NotNull
        public final Builder setBuyer(@NotNull final AdTechIdentifier buyer) {
            Intrinsics.checkNotNullParameter((Object)buyer, "buyer");
            this.buyer = buyer;
            return this;
        }
        
        @NotNull
        public final Builder setDailyUpdateUri(@NotNull final Uri dailyUpdateUri) {
            Intrinsics.checkNotNullParameter((Object)dailyUpdateUri, "dailyUpdateUri");
            this.dailyUpdateUri = dailyUpdateUri;
            return this;
        }
        
        @NotNull
        public final Builder setExpirationTime(@NotNull final Instant expirationTime) {
            Intrinsics.checkNotNullParameter((Object)expirationTime, "expirationTime");
            this.expirationTime = expirationTime;
            return this;
        }
        
        @NotNull
        public final Builder setName(@NotNull final String name) {
            Intrinsics.checkNotNullParameter((Object)name, "name");
            this.name = name;
            return this;
        }
        
        @NotNull
        public final Builder setTrustedBiddingData(@NotNull final TrustedBiddingData trustedBiddingData) {
            Intrinsics.checkNotNullParameter((Object)trustedBiddingData, "trustedBiddingSignals");
            this.trustedBiddingData = trustedBiddingData;
            return this;
        }
        
        @NotNull
        public final Builder setUserBiddingSignals(@NotNull final AdSelectionSignals userBiddingSignals) {
            Intrinsics.checkNotNullParameter((Object)userBiddingSignals, "userBiddingSignals");
            this.userBiddingSignals = userBiddingSignals;
            return this;
        }
    }
}

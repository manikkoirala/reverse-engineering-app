// 
// Decompiled by Procyon v0.6.0
// 

package androidx.startup;

import java.util.List;
import androidx.annotation.NonNull;
import android.content.Context;

public interface Initializer<T>
{
    @NonNull
    T create(@NonNull final Context p0);
    
    @NonNull
    List<Class<? extends Initializer<?>>> dependencies();
}

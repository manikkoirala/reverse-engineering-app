// 
// Decompiled by Procyon v0.6.0
// 

package androidx.startup;

import android.database.Cursor;
import android.content.Context;
import android.content.ContentValues;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.net.Uri;
import android.content.ContentProvider;

public class InitializationProvider extends ContentProvider
{
    public final int delete(@NonNull final Uri uri, @Nullable final String s, @Nullable final String[] array) {
        throw new IllegalStateException("Not allowed.");
    }
    
    @Nullable
    public final String getType(@NonNull final Uri uri) {
        throw new IllegalStateException("Not allowed.");
    }
    
    @Nullable
    public final Uri insert(@NonNull final Uri uri, @Nullable final ContentValues contentValues) {
        throw new IllegalStateException("Not allowed.");
    }
    
    public final boolean onCreate() {
        final Context context = this.getContext();
        if (context != null) {
            if (context.getApplicationContext() != null) {
                AppInitializer.getInstance(context).discoverAndInitialize();
            }
            return true;
        }
        throw new StartupException("Context cannot be null");
    }
    
    @Nullable
    public final Cursor query(@NonNull final Uri uri, @Nullable final String[] array, @Nullable final String s, @Nullable final String[] array2, @Nullable final String s2) {
        throw new IllegalStateException("Not allowed.");
    }
    
    public final int update(@NonNull final Uri uri, @Nullable final ContentValues contentValues, @Nullable final String s, @Nullable final String[] array) {
        throw new IllegalStateException("Not allowed.");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.startup;

import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
public final class StartupException extends RuntimeException
{
    public StartupException(@NonNull final String message) {
        super(message);
    }
    
    public StartupException(@NonNull final String message, @NonNull final Throwable cause) {
        super(message, cause);
    }
    
    public StartupException(@NonNull final Throwable cause) {
        super(cause);
    }
}

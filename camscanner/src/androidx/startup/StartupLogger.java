// 
// Decompiled by Procyon v0.6.0
// 

package androidx.startup;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
public final class StartupLogger
{
    static final boolean DEBUG = false;
    private static final String TAG = "StartupLogger";
    
    private StartupLogger() {
    }
    
    public static void e(@NonNull final String s, @Nullable final Throwable t) {
    }
    
    public static void i(@NonNull final String s) {
    }
    
    public static void w(@NonNull final String s) {
    }
}

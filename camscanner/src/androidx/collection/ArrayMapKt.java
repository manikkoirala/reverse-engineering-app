// 
// Decompiled by Procyon v0.6.0
// 

package androidx.collection;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Pair;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class ArrayMapKt
{
    @NotNull
    public static final <K, V> ArrayMap<K, V> arrayMapOf() {
        return new ArrayMap<K, V>();
    }
    
    @NotNull
    public static final <K, V> ArrayMap<K, V> arrayMapOf(@NotNull final Pair<? extends K, ? extends V>... array) {
        Intrinsics.\u3007\u3007888((Object)array, "pairs");
        final ArrayMap arrayMap = new ArrayMap(array.length);
        for (final Pair<? extends K, ? extends V> pair : array) {
            arrayMap.put(pair.getFirst(), pair.getSecond());
        }
        return arrayMap;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.collection;

import java.util.Iterator;
import kotlin.collections.LongIterator;
import kotlin.jvm.functions.Function0;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class LongSparseArrayKt
{
    public static final <T> boolean contains(@NotNull final LongSparseArray<T> longSparseArray, final long n) {
        Intrinsics.\u3007\u3007888((Object)longSparseArray, "receiver$0");
        return longSparseArray.containsKey(n);
    }
    
    public static final <T> void forEach(@NotNull final LongSparseArray<T> longSparseArray, @NotNull final Function2<? super Long, ? super T, Unit> function2) {
        Intrinsics.\u3007\u3007888((Object)longSparseArray, "receiver$0");
        Intrinsics.\u3007\u3007888((Object)function2, "action");
        for (int size = longSparseArray.size(), i = 0; i < size; ++i) {
            function2.invoke((Object)longSparseArray.keyAt(i), longSparseArray.valueAt(i));
        }
    }
    
    public static final <T> T getOrDefault(@NotNull final LongSparseArray<T> longSparseArray, final long n, final T t) {
        Intrinsics.\u3007\u3007888((Object)longSparseArray, "receiver$0");
        return longSparseArray.get(n, t);
    }
    
    public static final <T> T getOrElse(@NotNull final LongSparseArray<T> longSparseArray, final long n, @NotNull final Function0<? extends T> function0) {
        Intrinsics.\u3007\u3007888((Object)longSparseArray, "receiver$0");
        Intrinsics.\u3007\u3007888((Object)function0, "defaultValue");
        Object o = longSparseArray.get(n);
        if (o == null) {
            o = function0.invoke();
        }
        return (T)o;
    }
    
    public static final <T> int getSize(@NotNull final LongSparseArray<T> longSparseArray) {
        Intrinsics.\u3007\u3007888((Object)longSparseArray, "receiver$0");
        return longSparseArray.size();
    }
    
    public static final <T> boolean isNotEmpty(@NotNull final LongSparseArray<T> longSparseArray) {
        Intrinsics.\u3007\u3007888((Object)longSparseArray, "receiver$0");
        return longSparseArray.isEmpty() ^ true;
    }
    
    @NotNull
    public static final <T> LongIterator keyIterator(@NotNull final LongSparseArray<T> longSparseArray) {
        Intrinsics.\u3007\u3007888((Object)longSparseArray, "receiver$0");
        return (LongIterator)new LongSparseArrayKt$keyIterator.LongSparseArrayKt$keyIterator$1((LongSparseArray)longSparseArray);
    }
    
    @NotNull
    public static final <T> LongSparseArray<T> plus(@NotNull final LongSparseArray<T> longSparseArray, @NotNull final LongSparseArray<T> longSparseArray2) {
        Intrinsics.\u3007\u3007888((Object)longSparseArray, "receiver$0");
        Intrinsics.\u3007\u3007888((Object)longSparseArray2, "other");
        final LongSparseArray longSparseArray3 = new LongSparseArray(longSparseArray.size() + longSparseArray2.size());
        longSparseArray3.putAll(longSparseArray);
        longSparseArray3.putAll(longSparseArray2);
        return longSparseArray3;
    }
    
    public static final <T> boolean remove(@NotNull final LongSparseArray<T> longSparseArray, final long n, final T t) {
        Intrinsics.\u3007\u3007888((Object)longSparseArray, "receiver$0");
        return longSparseArray.remove(n, t);
    }
    
    public static final <T> void set(@NotNull final LongSparseArray<T> longSparseArray, final long n, final T t) {
        Intrinsics.\u3007\u3007888((Object)longSparseArray, "receiver$0");
        longSparseArray.put(n, t);
    }
    
    @NotNull
    public static final <T> Iterator<T> valueIterator(@NotNull final LongSparseArray<T> longSparseArray) {
        Intrinsics.\u3007\u3007888((Object)longSparseArray, "receiver$0");
        return (Iterator<T>)new LongSparseArrayKt$valueIterator.LongSparseArrayKt$valueIterator$1((LongSparseArray)longSparseArray);
    }
}

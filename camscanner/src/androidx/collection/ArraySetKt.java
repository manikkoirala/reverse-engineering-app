// 
// Decompiled by Procyon v0.6.0
// 

package androidx.collection;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class ArraySetKt
{
    @NotNull
    public static final <T> ArraySet<T> arraySetOf() {
        return new ArraySet<T>();
    }
    
    @NotNull
    public static final <T> ArraySet<T> arraySetOf(@NotNull final T... array) {
        Intrinsics.\u3007\u3007888((Object)array, "values");
        final ArraySet set = new ArraySet(array.length);
        for (int length = array.length, i = 0; i < length; ++i) {
            set.add(array[i]);
        }
        return set;
    }
}

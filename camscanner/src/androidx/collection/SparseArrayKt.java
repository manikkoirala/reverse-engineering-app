// 
// Decompiled by Procyon v0.6.0
// 

package androidx.collection;

import java.util.Iterator;
import kotlin.collections.IntIterator;
import kotlin.jvm.functions.Function0;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class SparseArrayKt
{
    public static final <T> boolean contains(@NotNull final SparseArrayCompat<T> sparseArrayCompat, final int n) {
        Intrinsics.\u3007\u3007888((Object)sparseArrayCompat, "receiver$0");
        return sparseArrayCompat.containsKey(n);
    }
    
    public static final <T> void forEach(@NotNull final SparseArrayCompat<T> sparseArrayCompat, @NotNull final Function2<? super Integer, ? super T, Unit> function2) {
        Intrinsics.\u3007\u3007888((Object)sparseArrayCompat, "receiver$0");
        Intrinsics.\u3007\u3007888((Object)function2, "action");
        for (int size = sparseArrayCompat.size(), i = 0; i < size; ++i) {
            function2.invoke((Object)sparseArrayCompat.keyAt(i), sparseArrayCompat.valueAt(i));
        }
    }
    
    public static final <T> T getOrDefault(@NotNull final SparseArrayCompat<T> sparseArrayCompat, final int n, final T t) {
        Intrinsics.\u3007\u3007888((Object)sparseArrayCompat, "receiver$0");
        return sparseArrayCompat.get(n, t);
    }
    
    public static final <T> T getOrElse(@NotNull final SparseArrayCompat<T> sparseArrayCompat, final int n, @NotNull final Function0<? extends T> function0) {
        Intrinsics.\u3007\u3007888((Object)sparseArrayCompat, "receiver$0");
        Intrinsics.\u3007\u3007888((Object)function0, "defaultValue");
        Object o = sparseArrayCompat.get(n);
        if (o == null) {
            o = function0.invoke();
        }
        return (T)o;
    }
    
    public static final <T> int getSize(@NotNull final SparseArrayCompat<T> sparseArrayCompat) {
        Intrinsics.\u3007\u3007888((Object)sparseArrayCompat, "receiver$0");
        return sparseArrayCompat.size();
    }
    
    public static final <T> boolean isNotEmpty(@NotNull final SparseArrayCompat<T> sparseArrayCompat) {
        Intrinsics.\u3007\u3007888((Object)sparseArrayCompat, "receiver$0");
        return sparseArrayCompat.isEmpty() ^ true;
    }
    
    @NotNull
    public static final <T> IntIterator keyIterator(@NotNull final SparseArrayCompat<T> sparseArrayCompat) {
        Intrinsics.\u3007\u3007888((Object)sparseArrayCompat, "receiver$0");
        return (IntIterator)new SparseArrayKt$keyIterator.SparseArrayKt$keyIterator$1((SparseArrayCompat)sparseArrayCompat);
    }
    
    @NotNull
    public static final <T> SparseArrayCompat<T> plus(@NotNull final SparseArrayCompat<T> sparseArrayCompat, @NotNull final SparseArrayCompat<T> sparseArrayCompat2) {
        Intrinsics.\u3007\u3007888((Object)sparseArrayCompat, "receiver$0");
        Intrinsics.\u3007\u3007888((Object)sparseArrayCompat2, "other");
        final SparseArrayCompat sparseArrayCompat3 = new SparseArrayCompat(sparseArrayCompat.size() + sparseArrayCompat2.size());
        sparseArrayCompat3.putAll(sparseArrayCompat);
        sparseArrayCompat3.putAll(sparseArrayCompat2);
        return sparseArrayCompat3;
    }
    
    public static final <T> boolean remove(@NotNull final SparseArrayCompat<T> sparseArrayCompat, final int n, final T t) {
        Intrinsics.\u3007\u3007888((Object)sparseArrayCompat, "receiver$0");
        return sparseArrayCompat.remove(n, t);
    }
    
    public static final <T> void set(@NotNull final SparseArrayCompat<T> sparseArrayCompat, final int n, final T t) {
        Intrinsics.\u3007\u3007888((Object)sparseArrayCompat, "receiver$0");
        sparseArrayCompat.put(n, t);
    }
    
    @NotNull
    public static final <T> Iterator<T> valueIterator(@NotNull final SparseArrayCompat<T> sparseArrayCompat) {
        Intrinsics.\u3007\u3007888((Object)sparseArrayCompat, "receiver$0");
        return (Iterator<T>)new SparseArrayKt$valueIterator.SparseArrayKt$valueIterator$1((SparseArrayCompat)sparseArrayCompat);
    }
}

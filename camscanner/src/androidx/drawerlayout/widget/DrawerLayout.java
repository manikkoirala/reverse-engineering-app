// 
// Decompiled by Procyon v0.6.0
// 

package androidx.drawerlayout.widget;

import android.view.accessibility.AccessibilityRecord;
import android.os.Parcel;
import android.os.Parcelable$ClassLoaderCreator;
import android.os.Parcelable$Creator;
import androidx.customview.view.AbsSavedState;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.ColorInt;
import androidx.core.content.ContextCompat;
import androidx.annotation.DrawableRes;
import androidx.annotation.RestrictTo;
import android.os.Parcelable;
import android.annotation.SuppressLint;
import android.view.View$MeasureSpec;
import androidx.core.graphics.Insets;
import androidx.core.view.WindowInsetsCompat;
import android.view.KeyEvent;
import android.view.ViewGroup$MarginLayoutParams;
import androidx.core.view.GravityCompat;
import android.graphics.Canvas;
import android.os.SystemClock;
import android.view.ViewGroup$LayoutParams;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import android.view.MotionEvent;
import android.content.res.TypedArray;
import android.view.WindowInsets;
import android.view.View$OnApplyWindowInsetsListener;
import androidx.core.view.AccessibilityDelegateCompat;
import androidx.core.view.ViewCompat;
import androidx.drawerlayout.R;
import android.util.AttributeSet;
import androidx.annotation.NonNull;
import android.content.Context;
import android.os.Build$VERSION;
import android.graphics.drawable.Drawable;
import android.graphics.Paint;
import android.view.View;
import java.util.ArrayList;
import java.util.List;
import androidx.annotation.Nullable;
import androidx.customview.widget.ViewDragHelper;
import android.graphics.Matrix;
import android.graphics.Rect;
import androidx.core.view.accessibility.AccessibilityViewCommand;
import androidx.customview.widget.Openable;
import android.view.ViewGroup;

public class DrawerLayout extends ViewGroup implements Openable
{
    private static final String ACCESSIBILITY_CLASS_NAME = "androidx.drawerlayout.widget.DrawerLayout";
    private static final boolean ALLOW_EDGE_LOCK = false;
    static final boolean CAN_HIDE_DESCENDANTS;
    private static final boolean CHILDREN_DISALLOW_INTERCEPT = true;
    private static final int DEFAULT_SCRIM_COLOR = -1728053248;
    static final int[] LAYOUT_ATTRS;
    public static final int LOCK_MODE_LOCKED_CLOSED = 1;
    public static final int LOCK_MODE_LOCKED_OPEN = 2;
    public static final int LOCK_MODE_UNDEFINED = 3;
    public static final int LOCK_MODE_UNLOCKED = 0;
    private static final int MIN_DRAWER_MARGIN = 64;
    private static final int MIN_FLING_VELOCITY = 400;
    private static final int PEEK_DELAY = 160;
    private static final boolean SET_DRAWER_SHADOW_FROM_ELEVATION;
    public static final int STATE_DRAGGING = 1;
    public static final int STATE_IDLE = 0;
    public static final int STATE_SETTLING = 2;
    private static final String TAG = "DrawerLayout";
    private static final int[] THEME_ATTRS;
    private static final float TOUCH_SLOP_SENSITIVITY = 1.0f;
    private static boolean sEdgeSizeUsingSystemGestureInsets;
    private final AccessibilityViewCommand mActionDismiss;
    private final ChildAccessibilityDelegate mChildAccessibilityDelegate;
    private Rect mChildHitRect;
    private Matrix mChildInvertedMatrix;
    private boolean mChildrenCanceledTouch;
    private boolean mDrawStatusBarBackground;
    private float mDrawerElevation;
    private int mDrawerState;
    private boolean mFirstLayout;
    private boolean mInLayout;
    private float mInitialMotionX;
    private float mInitialMotionY;
    private Object mLastInsets;
    private final ViewDragCallback mLeftCallback;
    private final ViewDragHelper mLeftDragger;
    @Nullable
    private DrawerListener mListener;
    private List<DrawerListener> mListeners;
    private int mLockModeEnd;
    private int mLockModeLeft;
    private int mLockModeRight;
    private int mLockModeStart;
    private int mMinDrawerMargin;
    private final ArrayList<View> mNonDrawerViews;
    private final ViewDragCallback mRightCallback;
    private final ViewDragHelper mRightDragger;
    private int mScrimColor;
    private float mScrimOpacity;
    private Paint mScrimPaint;
    private Drawable mShadowEnd;
    private Drawable mShadowLeft;
    private Drawable mShadowLeftResolved;
    private Drawable mShadowRight;
    private Drawable mShadowRightResolved;
    private Drawable mShadowStart;
    private Drawable mStatusBarBackground;
    private CharSequence mTitleLeft;
    private CharSequence mTitleRight;
    
    static {
        boolean sEdgeSizeUsingSystemGestureInsets = true;
        THEME_ATTRS = new int[] { 16843828 };
        LAYOUT_ATTRS = new int[] { 16842931 };
        final int sdk_INT = Build$VERSION.SDK_INT;
        CAN_HIDE_DESCENDANTS = true;
        SET_DRAWER_SHADOW_FROM_ELEVATION = true;
        if (sdk_INT < 29) {
            sEdgeSizeUsingSystemGestureInsets = false;
        }
        DrawerLayout.sEdgeSizeUsingSystemGestureInsets = sEdgeSizeUsingSystemGestureInsets;
    }
    
    public DrawerLayout(@NonNull final Context context) {
        this(context, null);
    }
    
    public DrawerLayout(@NonNull final Context context, @Nullable final AttributeSet set) {
        this(context, set, R.attr.drawerLayoutStyle);
    }
    
    public DrawerLayout(@NonNull final Context context, @Nullable AttributeSet obtainStyledAttributes, int drawerLayout_elevation) {
        super(context, obtainStyledAttributes, drawerLayout_elevation);
        this.mChildAccessibilityDelegate = new ChildAccessibilityDelegate();
        this.mScrimColor = -1728053248;
        this.mScrimPaint = new Paint();
        this.mFirstLayout = true;
        this.mLockModeLeft = 3;
        this.mLockModeRight = 3;
        this.mLockModeStart = 3;
        this.mLockModeEnd = 3;
        this.mShadowStart = null;
        this.mShadowEnd = null;
        this.mShadowLeft = null;
        this.mShadowRight = null;
        this.mActionDismiss = new AccessibilityViewCommand() {
            final DrawerLayout this$0;
            
            @Override
            public boolean perform(@NonNull final View view, @Nullable final CommandArguments commandArguments) {
                if (this.this$0.isDrawerOpen(view) && this.this$0.getDrawerLockMode(view) != 2) {
                    this.this$0.closeDrawer(view);
                    return true;
                }
                return false;
            }
        };
        this.setDescendantFocusability(262144);
        final float density = ((View)this).getResources().getDisplayMetrics().density;
        this.mMinDrawerMargin = (int)(64.0f * density + 0.5f);
        final float n = density * 400.0f;
        final ViewDragCallback mLeftCallback = new ViewDragCallback(3);
        this.mLeftCallback = mLeftCallback;
        final ViewDragCallback mRightCallback = new ViewDragCallback(5);
        this.mRightCallback = mRightCallback;
        final ViewDragHelper create = ViewDragHelper.create(this, 1.0f, (ViewDragHelper.Callback)mLeftCallback);
        (this.mLeftDragger = create).setEdgeTrackingEnabled(1);
        create.setMinVelocity(n);
        mLeftCallback.setDragger(create);
        final ViewDragHelper create2 = ViewDragHelper.create(this, 1.0f, (ViewDragHelper.Callback)mRightCallback);
        (this.mRightDragger = create2).setEdgeTrackingEnabled(2);
        create2.setMinVelocity(n);
        mRightCallback.setDragger(create2);
        ((View)this).setFocusableInTouchMode(true);
        ViewCompat.setImportantForAccessibility((View)this, 1);
        ViewCompat.setAccessibilityDelegate((View)this, new AccessibilityDelegate());
        this.setMotionEventSplittingEnabled(false);
        if (ViewCompat.getFitsSystemWindows((View)this)) {
            ((View)this).setOnApplyWindowInsetsListener((View$OnApplyWindowInsetsListener)new View$OnApplyWindowInsetsListener(this) {
                final DrawerLayout this$0;
                
                public WindowInsets onApplyWindowInsets(final View view, final WindowInsets windowInsets) {
                    ((DrawerLayout)view).setChildInsets(windowInsets, windowInsets.getSystemWindowInsetTop() > 0);
                    return windowInsets.consumeSystemWindowInsets();
                }
            });
            ((View)this).setSystemUiVisibility(1280);
            final TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(DrawerLayout.THEME_ATTRS);
            try {
                this.mStatusBarBackground = obtainStyledAttributes2.getDrawable(0);
            }
            finally {
                obtainStyledAttributes2.recycle();
            }
        }
        obtainStyledAttributes = (AttributeSet)context.obtainStyledAttributes(obtainStyledAttributes, R.styleable.DrawerLayout, drawerLayout_elevation, 0);
        try {
            drawerLayout_elevation = R.styleable.DrawerLayout_elevation;
            if (((TypedArray)obtainStyledAttributes).hasValue(drawerLayout_elevation)) {
                this.mDrawerElevation = ((TypedArray)obtainStyledAttributes).getDimension(drawerLayout_elevation, 0.0f);
            }
            else {
                this.mDrawerElevation = ((View)this).getResources().getDimension(R.dimen.def_drawer_elevation);
            }
            ((TypedArray)obtainStyledAttributes).recycle();
            this.mNonDrawerViews = new ArrayList<View>();
        }
        finally {
            ((TypedArray)obtainStyledAttributes).recycle();
        }
    }
    
    private boolean dispatchTransformedGenericPointerEvent(MotionEvent transformedMotionEvent, final View view) {
        boolean b;
        if (!view.getMatrix().isIdentity()) {
            transformedMotionEvent = this.getTransformedMotionEvent(transformedMotionEvent, view);
            b = view.dispatchGenericMotionEvent(transformedMotionEvent);
            transformedMotionEvent.recycle();
        }
        else {
            final float n = (float)(((View)this).getScrollX() - view.getLeft());
            final float n2 = (float)(((View)this).getScrollY() - view.getTop());
            transformedMotionEvent.offsetLocation(n, n2);
            b = view.dispatchGenericMotionEvent(transformedMotionEvent);
            transformedMotionEvent.offsetLocation(-n, -n2);
        }
        return b;
    }
    
    private MotionEvent getTransformedMotionEvent(MotionEvent obtain, final View view) {
        final float n = (float)(((View)this).getScrollX() - view.getLeft());
        final float n2 = (float)(((View)this).getScrollY() - view.getTop());
        obtain = MotionEvent.obtain(obtain);
        obtain.offsetLocation(n, n2);
        final Matrix matrix = view.getMatrix();
        if (!matrix.isIdentity()) {
            if (this.mChildInvertedMatrix == null) {
                this.mChildInvertedMatrix = new Matrix();
            }
            matrix.invert(this.mChildInvertedMatrix);
            obtain.transform(this.mChildInvertedMatrix);
        }
        return obtain;
    }
    
    static String gravityToString(final int i) {
        if ((i & 0x3) == 0x3) {
            return "LEFT";
        }
        if ((i & 0x5) == 0x5) {
            return "RIGHT";
        }
        return Integer.toHexString(i);
    }
    
    private static boolean hasOpaqueBackground(final View view) {
        final Drawable background = view.getBackground();
        boolean b = false;
        if (background != null) {
            b = b;
            if (background.getOpacity() == -1) {
                b = true;
            }
        }
        return b;
    }
    
    private boolean hasPeekingDrawer() {
        for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            if (((LayoutParams)this.getChildAt(i).getLayoutParams()).isPeeking) {
                return true;
            }
        }
        return false;
    }
    
    private boolean hasVisibleDrawer() {
        return this.findVisibleDrawer() != null;
    }
    
    static boolean includeChildForAccessibility(final View view) {
        return ViewCompat.getImportantForAccessibility(view) != 4 && ViewCompat.getImportantForAccessibility(view) != 2;
    }
    
    private boolean isInBoundsOfChild(final float n, final float n2, final View view) {
        if (this.mChildHitRect == null) {
            this.mChildHitRect = new Rect();
        }
        view.getHitRect(this.mChildHitRect);
        return this.mChildHitRect.contains((int)n, (int)n2);
    }
    
    private void mirror(final Drawable drawable, final int n) {
        if (drawable != null && DrawableCompat.isAutoMirrored(drawable)) {
            DrawableCompat.setLayoutDirection(drawable, n);
        }
    }
    
    private Drawable resolveLeftShadow() {
        final int layoutDirection = ViewCompat.getLayoutDirection((View)this);
        if (layoutDirection == 0) {
            final Drawable mShadowStart = this.mShadowStart;
            if (mShadowStart != null) {
                this.mirror(mShadowStart, layoutDirection);
                return this.mShadowStart;
            }
        }
        else {
            final Drawable mShadowEnd = this.mShadowEnd;
            if (mShadowEnd != null) {
                this.mirror(mShadowEnd, layoutDirection);
                return this.mShadowEnd;
            }
        }
        return this.mShadowLeft;
    }
    
    private Drawable resolveRightShadow() {
        final int layoutDirection = ViewCompat.getLayoutDirection((View)this);
        if (layoutDirection == 0) {
            final Drawable mShadowEnd = this.mShadowEnd;
            if (mShadowEnd != null) {
                this.mirror(mShadowEnd, layoutDirection);
                return this.mShadowEnd;
            }
        }
        else {
            final Drawable mShadowStart = this.mShadowStart;
            if (mShadowStart != null) {
                this.mirror(mShadowStart, layoutDirection);
                return this.mShadowStart;
            }
        }
        return this.mShadowRight;
    }
    
    private void resolveShadowDrawables() {
        if (DrawerLayout.SET_DRAWER_SHADOW_FROM_ELEVATION) {
            return;
        }
        this.mShadowLeftResolved = this.resolveLeftShadow();
        this.mShadowRightResolved = this.resolveRightShadow();
    }
    
    private void updateChildAccessibilityAction(final View view) {
        final AccessibilityNodeInfoCompat.AccessibilityActionCompat action_DISMISS = AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_DISMISS;
        ViewCompat.removeAccessibilityAction(view, action_DISMISS.getId());
        if (this.isDrawerOpen(view) && this.getDrawerLockMode(view) != 2) {
            ViewCompat.replaceAccessibilityAction(view, action_DISMISS, null, this.mActionDismiss);
        }
    }
    
    private void updateChildrenImportantForAccessibility(final View view, final boolean b) {
        for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            if ((!b && !this.isDrawerView(child)) || (b && child == view)) {
                ViewCompat.setImportantForAccessibility(child, 1);
            }
            else {
                ViewCompat.setImportantForAccessibility(child, 4);
            }
        }
    }
    
    public void addDrawerListener(@NonNull final DrawerListener drawerListener) {
        if (drawerListener == null) {
            return;
        }
        if (this.mListeners == null) {
            this.mListeners = new ArrayList<DrawerListener>();
        }
        this.mListeners.add(drawerListener);
    }
    
    public void addFocusables(final ArrayList<View> list, final int n, final int n2) {
        if (this.getDescendantFocusability() == 393216) {
            return;
        }
        final int childCount = this.getChildCount();
        final int n3 = 0;
        int i = 0;
        boolean b = false;
        while (i < childCount) {
            final View child = this.getChildAt(i);
            if (this.isDrawerView(child)) {
                if (this.isDrawerOpen(child)) {
                    child.addFocusables((ArrayList)list, n, n2);
                    b = true;
                }
            }
            else {
                this.mNonDrawerViews.add(child);
            }
            ++i;
        }
        if (!b) {
            for (int size = this.mNonDrawerViews.size(), j = n3; j < size; ++j) {
                final View view = this.mNonDrawerViews.get(j);
                if (view.getVisibility() == 0) {
                    view.addFocusables((ArrayList)list, n, n2);
                }
            }
        }
        this.mNonDrawerViews.clear();
    }
    
    public void addView(final View view, final int n, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        super.addView(view, n, viewGroup$LayoutParams);
        if (this.findOpenDrawer() == null && !this.isDrawerView(view)) {
            ViewCompat.setImportantForAccessibility(view, 1);
        }
        else {
            ViewCompat.setImportantForAccessibility(view, 4);
        }
        if (!DrawerLayout.CAN_HIDE_DESCENDANTS) {
            ViewCompat.setAccessibilityDelegate(view, this.mChildAccessibilityDelegate);
        }
    }
    
    void cancelChildViewTouch() {
        if (!this.mChildrenCanceledTouch) {
            final long uptimeMillis = SystemClock.uptimeMillis();
            final MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
            for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
                this.getChildAt(i).dispatchTouchEvent(obtain);
            }
            obtain.recycle();
            this.mChildrenCanceledTouch = true;
        }
    }
    
    boolean checkDrawerViewAbsoluteGravity(final View view, final int n) {
        return (this.getDrawerViewAbsoluteGravity(view) & n) == n;
    }
    
    protected boolean checkLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        return viewGroup$LayoutParams instanceof LayoutParams && super.checkLayoutParams(viewGroup$LayoutParams);
    }
    
    public void close() {
        this.closeDrawer(8388611);
    }
    
    public void closeDrawer(final int n) {
        this.closeDrawer(n, true);
    }
    
    public void closeDrawer(final int n, final boolean b) {
        final View drawerWithGravity = this.findDrawerWithGravity(n);
        if (drawerWithGravity != null) {
            this.closeDrawer(drawerWithGravity, b);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("No drawer view found with gravity ");
        sb.append(gravityToString(n));
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void closeDrawer(@NonNull final View view) {
        this.closeDrawer(view, true);
    }
    
    public void closeDrawer(@NonNull final View obj, final boolean b) {
        if (this.isDrawerView(obj)) {
            final LayoutParams layoutParams = (LayoutParams)obj.getLayoutParams();
            if (this.mFirstLayout) {
                layoutParams.onScreen = 0.0f;
                layoutParams.openState = 0;
            }
            else if (b) {
                layoutParams.openState |= 0x4;
                if (this.checkDrawerViewAbsoluteGravity(obj, 3)) {
                    this.mLeftDragger.smoothSlideViewTo(obj, -obj.getWidth(), obj.getTop());
                }
                else {
                    this.mRightDragger.smoothSlideViewTo(obj, ((View)this).getWidth(), obj.getTop());
                }
            }
            else {
                this.moveDrawerToOffset(obj, 0.0f);
                this.updateDrawerState(0, obj);
                obj.setVisibility(4);
            }
            ((View)this).invalidate();
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("View ");
        sb.append(obj);
        sb.append(" is not a sliding drawer");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void closeDrawers() {
        this.closeDrawers(false);
    }
    
    void closeDrawers(final boolean b) {
        final int childCount = this.getChildCount();
        int i = 0;
        int n = 0;
        while (i < childCount) {
            final View child = this.getChildAt(i);
            final LayoutParams layoutParams = (LayoutParams)child.getLayoutParams();
            int n2 = n;
            if (this.isDrawerView(child)) {
                if (b && !layoutParams.isPeeking) {
                    n2 = n;
                }
                else {
                    final int width = child.getWidth();
                    boolean b2;
                    if (this.checkDrawerViewAbsoluteGravity(child, 3)) {
                        b2 = this.mLeftDragger.smoothSlideViewTo(child, -width, child.getTop());
                    }
                    else {
                        b2 = this.mRightDragger.smoothSlideViewTo(child, ((View)this).getWidth(), child.getTop());
                    }
                    n2 = (n | (b2 ? 1 : 0));
                    layoutParams.isPeeking = false;
                }
            }
            ++i;
            n = n2;
        }
        this.mLeftCallback.removeCallbacks();
        this.mRightCallback.removeCallbacks();
        if (n != 0) {
            ((View)this).invalidate();
        }
    }
    
    public void computeScroll() {
        final int childCount = this.getChildCount();
        float max = 0.0f;
        for (int i = 0; i < childCount; ++i) {
            max = Math.max(max, ((LayoutParams)this.getChildAt(i).getLayoutParams()).onScreen);
        }
        this.mScrimOpacity = max;
        final boolean continueSettling = this.mLeftDragger.continueSettling(true);
        final boolean continueSettling2 = this.mRightDragger.continueSettling(true);
        if (continueSettling || continueSettling2) {
            ViewCompat.postInvalidateOnAnimation((View)this);
        }
    }
    
    public boolean dispatchGenericMotionEvent(final MotionEvent motionEvent) {
        if ((motionEvent.getSource() & 0x2) != 0x0 && motionEvent.getAction() != 10 && this.mScrimOpacity > 0.0f) {
            int i = this.getChildCount();
            if (i != 0) {
                final float x = motionEvent.getX();
                final float y = motionEvent.getY();
                --i;
                while (i >= 0) {
                    final View child = this.getChildAt(i);
                    if (this.isInBoundsOfChild(x, y, child)) {
                        if (!this.isContentView(child)) {
                            if (this.dispatchTransformedGenericPointerEvent(motionEvent, child)) {
                                return true;
                            }
                        }
                    }
                    --i;
                }
            }
            return false;
        }
        return super.dispatchGenericMotionEvent(motionEvent);
    }
    
    void dispatchOnDrawerClosed(View rootView) {
        final LayoutParams layoutParams = (LayoutParams)rootView.getLayoutParams();
        if ((layoutParams.openState & 0x1) == 0x1) {
            layoutParams.openState = 0;
            final List<DrawerListener> mListeners = this.mListeners;
            if (mListeners != null) {
                for (int i = mListeners.size() - 1; i >= 0; --i) {
                    this.mListeners.get(i).onDrawerClosed(rootView);
                }
            }
            this.updateChildrenImportantForAccessibility(rootView, false);
            this.updateChildAccessibilityAction(rootView);
            if (((View)this).hasWindowFocus()) {
                rootView = ((View)this).getRootView();
                if (rootView != null) {
                    rootView.sendAccessibilityEvent(32);
                }
            }
        }
    }
    
    void dispatchOnDrawerOpened(final View view) {
        final LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if ((layoutParams.openState & 0x1) == 0x0) {
            layoutParams.openState = 1;
            final List<DrawerListener> mListeners = this.mListeners;
            if (mListeners != null) {
                for (int i = mListeners.size() - 1; i >= 0; --i) {
                    this.mListeners.get(i).onDrawerOpened(view);
                }
            }
            this.updateChildrenImportantForAccessibility(view, true);
            this.updateChildAccessibilityAction(view);
            if (((View)this).hasWindowFocus()) {
                ((View)this).sendAccessibilityEvent(32);
            }
        }
    }
    
    void dispatchOnDrawerSlide(final View view, final float n) {
        final List<DrawerListener> mListeners = this.mListeners;
        if (mListeners != null) {
            for (int i = mListeners.size() - 1; i >= 0; --i) {
                this.mListeners.get(i).onDrawerSlide(view, n);
            }
        }
    }
    
    protected boolean drawChild(final Canvas canvas, final View view, final long n) {
        final int height = ((View)this).getHeight();
        final boolean contentView = this.isContentView(view);
        int width = ((View)this).getWidth();
        final int save = canvas.save();
        int n2 = 0;
        int n3 = width;
        if (contentView) {
            final int childCount = this.getChildCount();
            int i = 0;
            n2 = 0;
            while (i < childCount) {
                final View child = this.getChildAt(i);
                int n4 = width;
                int n5 = n2;
                if (child != view) {
                    n4 = width;
                    n5 = n2;
                    if (child.getVisibility() == 0) {
                        n4 = width;
                        n5 = n2;
                        if (hasOpaqueBackground(child)) {
                            n4 = width;
                            n5 = n2;
                            if (this.isDrawerView(child)) {
                                if (child.getHeight() < height) {
                                    n4 = width;
                                    n5 = n2;
                                }
                                else if (this.checkDrawerViewAbsoluteGravity(child, 3)) {
                                    final int right = child.getRight();
                                    n4 = width;
                                    if (right > (n5 = n2)) {
                                        n5 = right;
                                        n4 = width;
                                    }
                                }
                                else {
                                    final int left = child.getLeft();
                                    n4 = width;
                                    n5 = n2;
                                    if (left < width) {
                                        n4 = left;
                                        n5 = n2;
                                    }
                                }
                            }
                        }
                    }
                }
                ++i;
                width = n4;
                n2 = n5;
            }
            canvas.clipRect(n2, 0, width, ((View)this).getHeight());
            n3 = width;
        }
        final boolean drawChild = super.drawChild(canvas, view, n);
        canvas.restoreToCount(save);
        final float mScrimOpacity = this.mScrimOpacity;
        if (mScrimOpacity > 0.0f && contentView) {
            final int mScrimColor = this.mScrimColor;
            this.mScrimPaint.setColor((mScrimColor & 0xFFFFFF) | (int)(((0xFF000000 & mScrimColor) >>> 24) * mScrimOpacity) << 24);
            canvas.drawRect((float)n2, 0.0f, (float)n3, (float)((View)this).getHeight(), this.mScrimPaint);
        }
        else if (this.mShadowLeftResolved != null && this.checkDrawerViewAbsoluteGravity(view, 3)) {
            final int intrinsicWidth = this.mShadowLeftResolved.getIntrinsicWidth();
            final int right2 = view.getRight();
            final float max = Math.max(0.0f, Math.min(right2 / (float)this.mLeftDragger.getEdgeSize(), 1.0f));
            this.mShadowLeftResolved.setBounds(right2, view.getTop(), intrinsicWidth + right2, view.getBottom());
            this.mShadowLeftResolved.setAlpha((int)(max * 255.0f));
            this.mShadowLeftResolved.draw(canvas);
        }
        else if (this.mShadowRightResolved != null && this.checkDrawerViewAbsoluteGravity(view, 5)) {
            final int intrinsicWidth2 = this.mShadowRightResolved.getIntrinsicWidth();
            final int left2 = view.getLeft();
            final float max2 = Math.max(0.0f, Math.min((((View)this).getWidth() - left2) / (float)this.mRightDragger.getEdgeSize(), 1.0f));
            this.mShadowRightResolved.setBounds(left2 - intrinsicWidth2, view.getTop(), left2, view.getBottom());
            this.mShadowRightResolved.setAlpha((int)(max2 * 255.0f));
            this.mShadowRightResolved.draw(canvas);
        }
        return drawChild;
    }
    
    View findDrawerWithGravity(int i) {
        final int absoluteGravity = GravityCompat.getAbsoluteGravity(i, ViewCompat.getLayoutDirection((View)this));
        int childCount;
        View child;
        for (childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            child = this.getChildAt(i);
            if ((this.getDrawerViewAbsoluteGravity(child) & 0x7) == (absoluteGravity & 0x7)) {
                return child;
            }
        }
        return null;
    }
    
    View findOpenDrawer() {
        for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            if ((((LayoutParams)child.getLayoutParams()).openState & 0x1) == 0x1) {
                return child;
            }
        }
        return null;
    }
    
    View findVisibleDrawer() {
        for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            if (this.isDrawerView(child) && this.isDrawerVisible(child)) {
                return child;
            }
        }
        return null;
    }
    
    protected ViewGroup$LayoutParams generateDefaultLayoutParams() {
        return (ViewGroup$LayoutParams)new LayoutParams(-1, -1);
    }
    
    public ViewGroup$LayoutParams generateLayoutParams(final AttributeSet set) {
        return (ViewGroup$LayoutParams)new LayoutParams(((View)this).getContext(), set);
    }
    
    protected ViewGroup$LayoutParams generateLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        LayoutParams layoutParams;
        if (viewGroup$LayoutParams instanceof LayoutParams) {
            layoutParams = new LayoutParams((LayoutParams)viewGroup$LayoutParams);
        }
        else if (viewGroup$LayoutParams instanceof ViewGroup$MarginLayoutParams) {
            layoutParams = new LayoutParams((ViewGroup$MarginLayoutParams)viewGroup$LayoutParams);
        }
        else {
            layoutParams = new LayoutParams(viewGroup$LayoutParams);
        }
        return (ViewGroup$LayoutParams)layoutParams;
    }
    
    public float getDrawerElevation() {
        if (DrawerLayout.SET_DRAWER_SHADOW_FROM_ELEVATION) {
            return this.mDrawerElevation;
        }
        return 0.0f;
    }
    
    public int getDrawerLockMode(int n) {
        final int layoutDirection = ViewCompat.getLayoutDirection((View)this);
        if (n != 3) {
            if (n != 5) {
                if (n != 8388611) {
                    if (n == 8388613) {
                        n = this.mLockModeEnd;
                        if (n != 3) {
                            return n;
                        }
                        if (layoutDirection == 0) {
                            n = this.mLockModeRight;
                        }
                        else {
                            n = this.mLockModeLeft;
                        }
                        if (n != 3) {
                            return n;
                        }
                    }
                }
                else {
                    n = this.mLockModeStart;
                    if (n != 3) {
                        return n;
                    }
                    if (layoutDirection == 0) {
                        n = this.mLockModeLeft;
                    }
                    else {
                        n = this.mLockModeRight;
                    }
                    if (n != 3) {
                        return n;
                    }
                }
            }
            else {
                n = this.mLockModeRight;
                if (n != 3) {
                    return n;
                }
                if (layoutDirection == 0) {
                    n = this.mLockModeEnd;
                }
                else {
                    n = this.mLockModeStart;
                }
                if (n != 3) {
                    return n;
                }
            }
        }
        else {
            n = this.mLockModeLeft;
            if (n != 3) {
                return n;
            }
            if (layoutDirection == 0) {
                n = this.mLockModeStart;
            }
            else {
                n = this.mLockModeEnd;
            }
            if (n != 3) {
                return n;
            }
        }
        return 0;
    }
    
    public int getDrawerLockMode(@NonNull final View obj) {
        if (this.isDrawerView(obj)) {
            return this.getDrawerLockMode(((LayoutParams)obj.getLayoutParams()).gravity);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("View ");
        sb.append(obj);
        sb.append(" is not a drawer");
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Nullable
    public CharSequence getDrawerTitle(int absoluteGravity) {
        absoluteGravity = GravityCompat.getAbsoluteGravity(absoluteGravity, ViewCompat.getLayoutDirection((View)this));
        if (absoluteGravity == 3) {
            return this.mTitleLeft;
        }
        if (absoluteGravity == 5) {
            return this.mTitleRight;
        }
        return null;
    }
    
    int getDrawerViewAbsoluteGravity(final View view) {
        return GravityCompat.getAbsoluteGravity(((LayoutParams)view.getLayoutParams()).gravity, ViewCompat.getLayoutDirection((View)this));
    }
    
    float getDrawerViewOffset(final View view) {
        return ((LayoutParams)view.getLayoutParams()).onScreen;
    }
    
    @Nullable
    public Drawable getStatusBarBackgroundDrawable() {
        return this.mStatusBarBackground;
    }
    
    boolean isContentView(final View view) {
        return ((LayoutParams)view.getLayoutParams()).gravity == 0;
    }
    
    public boolean isDrawerOpen(final int n) {
        final View drawerWithGravity = this.findDrawerWithGravity(n);
        return drawerWithGravity != null && this.isDrawerOpen(drawerWithGravity);
    }
    
    public boolean isDrawerOpen(@NonNull final View obj) {
        if (this.isDrawerView(obj)) {
            final int openState = ((LayoutParams)obj.getLayoutParams()).openState;
            boolean b = true;
            if ((openState & 0x1) != 0x1) {
                b = false;
            }
            return b;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("View ");
        sb.append(obj);
        sb.append(" is not a drawer");
        throw new IllegalArgumentException(sb.toString());
    }
    
    boolean isDrawerView(final View view) {
        final int absoluteGravity = GravityCompat.getAbsoluteGravity(((LayoutParams)view.getLayoutParams()).gravity, ViewCompat.getLayoutDirection(view));
        return (absoluteGravity & 0x3) != 0x0 || (absoluteGravity & 0x5) != 0x0;
    }
    
    public boolean isDrawerVisible(final int n) {
        final View drawerWithGravity = this.findDrawerWithGravity(n);
        return drawerWithGravity != null && this.isDrawerVisible(drawerWithGravity);
    }
    
    public boolean isDrawerVisible(@NonNull final View obj) {
        if (this.isDrawerView(obj)) {
            return ((LayoutParams)obj.getLayoutParams()).onScreen > 0.0f;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("View ");
        sb.append(obj);
        sb.append(" is not a drawer");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public boolean isOpen() {
        return this.isDrawerOpen(8388611);
    }
    
    void moveDrawerToOffset(final View view, final float n) {
        final float drawerViewOffset = this.getDrawerViewOffset(view);
        final float n2 = (float)view.getWidth();
        int n3 = (int)(n2 * n) - (int)(drawerViewOffset * n2);
        if (!this.checkDrawerViewAbsoluteGravity(view, 3)) {
            n3 = -n3;
        }
        view.offsetLeftAndRight(n3);
        this.setDrawerViewOffset(view, n);
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.mFirstLayout = true;
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mFirstLayout = true;
    }
    
    public void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        if (this.mDrawStatusBarBackground && this.mStatusBarBackground != null) {
            final Object mLastInsets = this.mLastInsets;
            int systemWindowInsetTop;
            if (mLastInsets != null) {
                systemWindowInsetTop = ((WindowInsets)mLastInsets).getSystemWindowInsetTop();
            }
            else {
                systemWindowInsetTop = 0;
            }
            if (systemWindowInsetTop > 0) {
                this.mStatusBarBackground.setBounds(0, 0, ((View)this).getWidth(), systemWindowInsetTop);
                this.mStatusBarBackground.draw(canvas);
            }
        }
    }
    
    public boolean onInterceptTouchEvent(final MotionEvent motionEvent) {
        final int actionMasked = motionEvent.getActionMasked();
        final boolean shouldInterceptTouchEvent = this.mLeftDragger.shouldInterceptTouchEvent(motionEvent);
        final boolean shouldInterceptTouchEvent2 = this.mRightDragger.shouldInterceptTouchEvent(motionEvent);
        final boolean b = true;
        boolean b2;
        if (actionMasked != 0) {
            Label_0093: {
                if (actionMasked != 1) {
                    if (actionMasked != 2) {
                        if (actionMasked != 3) {
                            break Label_0093;
                        }
                    }
                    else {
                        if (this.mLeftDragger.checkTouchSlop(3)) {
                            this.mLeftCallback.removeCallbacks();
                            this.mRightCallback.removeCallbacks();
                        }
                        break Label_0093;
                    }
                }
                this.closeDrawers(true);
                this.mChildrenCanceledTouch = false;
            }
            b2 = false;
        }
        else {
            final float x = motionEvent.getX();
            final float y = motionEvent.getY();
            this.mInitialMotionX = x;
            this.mInitialMotionY = y;
            Label_0161: {
                if (this.mScrimOpacity > 0.0f) {
                    final View topChildUnder = this.mLeftDragger.findTopChildUnder((int)x, (int)y);
                    if (topChildUnder != null && this.isContentView(topChildUnder)) {
                        b2 = true;
                        break Label_0161;
                    }
                }
                b2 = false;
            }
            this.mChildrenCanceledTouch = false;
        }
        boolean b3 = b;
        if (!(shouldInterceptTouchEvent | shouldInterceptTouchEvent2)) {
            b3 = b;
            if (!b2) {
                b3 = b;
                if (!this.hasPeekingDrawer()) {
                    b3 = (this.mChildrenCanceledTouch && b);
                }
            }
        }
        return b3;
    }
    
    public boolean onKeyDown(final int n, final KeyEvent keyEvent) {
        if (n == 4 && this.hasVisibleDrawer()) {
            keyEvent.startTracking();
            return true;
        }
        return super.onKeyDown(n, keyEvent);
    }
    
    public boolean onKeyUp(final int n, final KeyEvent keyEvent) {
        if (n == 4) {
            final View visibleDrawer = this.findVisibleDrawer();
            if (visibleDrawer != null && this.getDrawerLockMode(visibleDrawer) == 0) {
                this.closeDrawers();
            }
            return visibleDrawer != null;
        }
        return super.onKeyUp(n, keyEvent);
    }
    
    protected void onLayout(final boolean b, int visibility, final int n, int i, final int n2) {
        this.mInLayout = true;
        final int n3 = i - visibility;
        int childCount;
        View child;
        LayoutParams layoutParams;
        int measuredWidth;
        int measuredHeight;
        float n4;
        int n5;
        float n6;
        float n7;
        boolean b2;
        int n8;
        int n9;
        int bottomMargin;
        for (childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            child = this.getChildAt(i);
            if (child.getVisibility() != 8) {
                layoutParams = (LayoutParams)child.getLayoutParams();
                if (this.isContentView(child)) {
                    visibility = layoutParams.leftMargin;
                    child.layout(visibility, layoutParams.topMargin, child.getMeasuredWidth() + visibility, layoutParams.topMargin + child.getMeasuredHeight());
                }
                else {
                    measuredWidth = child.getMeasuredWidth();
                    measuredHeight = child.getMeasuredHeight();
                    if (this.checkDrawerViewAbsoluteGravity(child, 3)) {
                        visibility = -measuredWidth;
                        n4 = (float)measuredWidth;
                        n5 = visibility + (int)(layoutParams.onScreen * n4);
                        n6 = (measuredWidth + n5) / n4;
                    }
                    else {
                        n7 = (float)measuredWidth;
                        n5 = n3 - (int)(layoutParams.onScreen * n7);
                        n6 = (n3 - n5) / n7;
                    }
                    b2 = (n6 != layoutParams.onScreen);
                    visibility = (layoutParams.gravity & 0x70);
                    if (visibility != 16) {
                        if (visibility != 80) {
                            visibility = layoutParams.topMargin;
                            child.layout(n5, visibility, measuredWidth + n5, measuredHeight + visibility);
                        }
                        else {
                            visibility = n2 - n;
                            child.layout(n5, visibility - layoutParams.bottomMargin - child.getMeasuredHeight(), measuredWidth + n5, visibility - layoutParams.bottomMargin);
                        }
                    }
                    else {
                        n8 = n2 - n;
                        n9 = (n8 - measuredHeight) / 2;
                        visibility = layoutParams.topMargin;
                        if (n9 >= visibility) {
                            bottomMargin = layoutParams.bottomMargin;
                            visibility = n9;
                            if (n9 + measuredHeight > n8 - bottomMargin) {
                                visibility = n8 - bottomMargin - measuredHeight;
                            }
                        }
                        child.layout(n5, visibility, measuredWidth + n5, measuredHeight + visibility);
                    }
                    if (b2) {
                        this.setDrawerViewOffset(child, n6);
                    }
                    if (layoutParams.onScreen > 0.0f) {
                        visibility = 0;
                    }
                    else {
                        visibility = 4;
                    }
                    if (child.getVisibility() != visibility) {
                        child.setVisibility(visibility);
                    }
                }
            }
        }
        if (DrawerLayout.sEdgeSizeUsingSystemGestureInsets) {
            final WindowInsets rootWindowInsets = this.getRootWindowInsets();
            if (rootWindowInsets != null) {
                final Insets systemGestureInsets = WindowInsetsCompat.toWindowInsetsCompat(rootWindowInsets).getSystemGestureInsets();
                final ViewDragHelper mLeftDragger = this.mLeftDragger;
                mLeftDragger.setEdgeSize(Math.max(mLeftDragger.getDefaultEdgeSize(), systemGestureInsets.left));
                final ViewDragHelper mRightDragger = this.mRightDragger;
                mRightDragger.setEdgeSize(Math.max(mRightDragger.getDefaultEdgeSize(), systemGestureInsets.right));
            }
        }
        this.mInLayout = false;
        this.mFirstLayout = false;
    }
    
    @SuppressLint({ "WrongConstant" })
    protected void onMeasure(final int n, final int n2) {
        final int mode = View$MeasureSpec.getMode(n);
        final int mode2 = View$MeasureSpec.getMode(n2);
        int size = View$MeasureSpec.getSize(n);
        final int size2 = View$MeasureSpec.getSize(n2);
        int n3 = 0;
        int n4 = 0;
        Label_0087: {
            if (mode == 1073741824) {
                n3 = size;
                n4 = size2;
                if (mode2 == 1073741824) {
                    break Label_0087;
                }
            }
            if (!((View)this).isInEditMode()) {
                throw new IllegalArgumentException("DrawerLayout must be measured with MeasureSpec.EXACTLY.");
            }
            if (mode == 0) {
                size = 300;
            }
            n3 = size;
            n4 = size2;
            if (mode2 == 0) {
                n4 = 300;
                n3 = size;
            }
        }
        ((View)this).setMeasuredDimension(n3, n4);
        final boolean b = this.mLastInsets != null && ViewCompat.getFitsSystemWindows((View)this);
        final int layoutDirection = ViewCompat.getLayoutDirection((View)this);
        final int childCount = this.getChildCount();
        int i = 0;
        int n5 = 0;
        int n6 = 0;
        while (i < childCount) {
            final View child = this.getChildAt(i);
            if (child.getVisibility() != 8) {
                final LayoutParams layoutParams = (LayoutParams)child.getLayoutParams();
                if (b) {
                    final int absoluteGravity = GravityCompat.getAbsoluteGravity(layoutParams.gravity, layoutDirection);
                    if (ViewCompat.getFitsSystemWindows(child)) {
                        final WindowInsets windowInsets = (WindowInsets)this.mLastInsets;
                        WindowInsets windowInsets2;
                        if (absoluteGravity == 3) {
                            windowInsets2 = windowInsets.replaceSystemWindowInsets(windowInsets.getSystemWindowInsetLeft(), windowInsets.getSystemWindowInsetTop(), 0, windowInsets.getSystemWindowInsetBottom());
                        }
                        else {
                            windowInsets2 = windowInsets;
                            if (absoluteGravity == 5) {
                                windowInsets2 = windowInsets.replaceSystemWindowInsets(0, windowInsets.getSystemWindowInsetTop(), windowInsets.getSystemWindowInsetRight(), windowInsets.getSystemWindowInsetBottom());
                            }
                        }
                        child.dispatchApplyWindowInsets(windowInsets2);
                    }
                    else {
                        final WindowInsets windowInsets3 = (WindowInsets)this.mLastInsets;
                        WindowInsets windowInsets4;
                        if (absoluteGravity == 3) {
                            windowInsets4 = windowInsets3.replaceSystemWindowInsets(windowInsets3.getSystemWindowInsetLeft(), windowInsets3.getSystemWindowInsetTop(), 0, windowInsets3.getSystemWindowInsetBottom());
                        }
                        else {
                            windowInsets4 = windowInsets3;
                            if (absoluteGravity == 5) {
                                windowInsets4 = windowInsets3.replaceSystemWindowInsets(0, windowInsets3.getSystemWindowInsetTop(), windowInsets3.getSystemWindowInsetRight(), windowInsets3.getSystemWindowInsetBottom());
                            }
                        }
                        layoutParams.leftMargin = windowInsets4.getSystemWindowInsetLeft();
                        layoutParams.topMargin = windowInsets4.getSystemWindowInsetTop();
                        layoutParams.rightMargin = windowInsets4.getSystemWindowInsetRight();
                        layoutParams.bottomMargin = windowInsets4.getSystemWindowInsetBottom();
                    }
                }
                if (this.isContentView(child)) {
                    child.measure(View$MeasureSpec.makeMeasureSpec(n3 - layoutParams.leftMargin - layoutParams.rightMargin, 1073741824), View$MeasureSpec.makeMeasureSpec(n4 - layoutParams.topMargin - layoutParams.bottomMargin, 1073741824));
                }
                else {
                    if (!this.isDrawerView(child)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Child ");
                        sb.append(child);
                        sb.append(" at index ");
                        sb.append(i);
                        sb.append(" does not have a valid layout_gravity - must be Gravity.LEFT, Gravity.RIGHT or Gravity.NO_GRAVITY");
                        throw new IllegalStateException(sb.toString());
                    }
                    if (DrawerLayout.SET_DRAWER_SHADOW_FROM_ELEVATION) {
                        final float elevation = ViewCompat.getElevation(child);
                        final float mDrawerElevation = this.mDrawerElevation;
                        if (elevation != mDrawerElevation) {
                            ViewCompat.setElevation(child, mDrawerElevation);
                        }
                    }
                    final int n7 = this.getDrawerViewAbsoluteGravity(child) & 0x7;
                    final boolean b2 = n7 == 3;
                    if ((b2 && n5 != 0) || (!b2 && n6 != 0)) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Child drawer has absolute gravity ");
                        sb2.append(gravityToString(n7));
                        sb2.append(" but this ");
                        sb2.append("DrawerLayout");
                        sb2.append(" already has a drawer view along that edge");
                        throw new IllegalStateException(sb2.toString());
                    }
                    if (b2) {
                        n5 = 1;
                    }
                    else {
                        n6 = 1;
                    }
                    child.measure(ViewGroup.getChildMeasureSpec(n, this.mMinDrawerMargin + layoutParams.leftMargin + layoutParams.rightMargin, layoutParams.width), ViewGroup.getChildMeasureSpec(n2, layoutParams.topMargin + layoutParams.bottomMargin, layoutParams.height));
                }
            }
            ++i;
        }
    }
    
    protected void onRestoreInstanceState(final Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        final SavedState savedState = (SavedState)parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        final int openDrawerGravity = savedState.openDrawerGravity;
        if (openDrawerGravity != 0) {
            final View drawerWithGravity = this.findDrawerWithGravity(openDrawerGravity);
            if (drawerWithGravity != null) {
                this.openDrawer(drawerWithGravity);
            }
        }
        final int lockModeLeft = savedState.lockModeLeft;
        if (lockModeLeft != 3) {
            this.setDrawerLockMode(lockModeLeft, 3);
        }
        final int lockModeRight = savedState.lockModeRight;
        if (lockModeRight != 3) {
            this.setDrawerLockMode(lockModeRight, 5);
        }
        final int lockModeStart = savedState.lockModeStart;
        if (lockModeStart != 3) {
            this.setDrawerLockMode(lockModeStart, 8388611);
        }
        final int lockModeEnd = savedState.lockModeEnd;
        if (lockModeEnd != 3) {
            this.setDrawerLockMode(lockModeEnd, 8388613);
        }
    }
    
    public void onRtlPropertiesChanged(final int n) {
        this.resolveShadowDrawables();
    }
    
    protected Parcelable onSaveInstanceState() {
        final SavedState savedState = new SavedState(super.onSaveInstanceState());
        for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            final LayoutParams layoutParams = (LayoutParams)this.getChildAt(i).getLayoutParams();
            final int openState = layoutParams.openState;
            boolean b = true;
            final boolean b2 = openState == 1;
            if (openState != 2) {
                b = false;
            }
            if (b2 || b) {
                savedState.openDrawerGravity = layoutParams.gravity;
                break;
            }
        }
        savedState.lockModeLeft = this.mLockModeLeft;
        savedState.lockModeRight = this.mLockModeRight;
        savedState.lockModeStart = this.mLockModeStart;
        savedState.lockModeEnd = this.mLockModeEnd;
        return (Parcelable)savedState;
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        this.mLeftDragger.processTouchEvent(motionEvent);
        this.mRightDragger.processTouchEvent(motionEvent);
        final int n = motionEvent.getAction() & 0xFF;
        boolean b = false;
        if (n != 0) {
            if (n != 1) {
                if (n == 3) {
                    this.closeDrawers(true);
                    this.mChildrenCanceledTouch = false;
                }
            }
            else {
                final float x = motionEvent.getX();
                final float y = motionEvent.getY();
                final View topChildUnder = this.mLeftDragger.findTopChildUnder((int)x, (int)y);
                Label_0157: {
                    if (topChildUnder != null && this.isContentView(topChildUnder)) {
                        final float n2 = x - this.mInitialMotionX;
                        final float n3 = y - this.mInitialMotionY;
                        final int touchSlop = this.mLeftDragger.getTouchSlop();
                        if (n2 * n2 + n3 * n3 < touchSlop * touchSlop) {
                            final View openDrawer = this.findOpenDrawer();
                            if (openDrawer != null && this.getDrawerLockMode(openDrawer) != 2) {
                                break Label_0157;
                            }
                        }
                    }
                    b = true;
                }
                this.closeDrawers(b);
            }
        }
        else {
            final float x2 = motionEvent.getX();
            final float y2 = motionEvent.getY();
            this.mInitialMotionX = x2;
            this.mInitialMotionY = y2;
            this.mChildrenCanceledTouch = false;
        }
        return true;
    }
    
    public void open() {
        this.openDrawer(8388611);
    }
    
    public void openDrawer(final int n) {
        this.openDrawer(n, true);
    }
    
    public void openDrawer(final int n, final boolean b) {
        final View drawerWithGravity = this.findDrawerWithGravity(n);
        if (drawerWithGravity != null) {
            this.openDrawer(drawerWithGravity, b);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("No drawer view found with gravity ");
        sb.append(gravityToString(n));
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void openDrawer(@NonNull final View view) {
        this.openDrawer(view, true);
    }
    
    public void openDrawer(@NonNull final View obj, final boolean b) {
        if (this.isDrawerView(obj)) {
            final LayoutParams layoutParams = (LayoutParams)obj.getLayoutParams();
            if (this.mFirstLayout) {
                layoutParams.onScreen = 1.0f;
                layoutParams.openState = 1;
                this.updateChildrenImportantForAccessibility(obj, true);
                this.updateChildAccessibilityAction(obj);
            }
            else if (b) {
                layoutParams.openState |= 0x2;
                if (this.checkDrawerViewAbsoluteGravity(obj, 3)) {
                    this.mLeftDragger.smoothSlideViewTo(obj, 0, obj.getTop());
                }
                else {
                    this.mRightDragger.smoothSlideViewTo(obj, ((View)this).getWidth() - obj.getWidth(), obj.getTop());
                }
            }
            else {
                this.moveDrawerToOffset(obj, 1.0f);
                this.updateDrawerState(0, obj);
                obj.setVisibility(0);
            }
            ((View)this).invalidate();
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("View ");
        sb.append(obj);
        sb.append(" is not a sliding drawer");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void removeDrawerListener(@NonNull final DrawerListener drawerListener) {
        if (drawerListener == null) {
            return;
        }
        final List<DrawerListener> mListeners = this.mListeners;
        if (mListeners == null) {
            return;
        }
        mListeners.remove(drawerListener);
    }
    
    public void requestDisallowInterceptTouchEvent(final boolean b) {
        super.requestDisallowInterceptTouchEvent(b);
        if (b) {
            this.closeDrawers(true);
        }
    }
    
    public void requestLayout() {
        if (!this.mInLayout) {
            super.requestLayout();
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void setChildInsets(final Object mLastInsets, final boolean mDrawStatusBarBackground) {
        this.mLastInsets = mLastInsets;
        this.mDrawStatusBarBackground = mDrawStatusBarBackground;
        ((View)this).setWillNotDraw(!mDrawStatusBarBackground && ((View)this).getBackground() == null);
        this.requestLayout();
    }
    
    public void setDrawerElevation(final float mDrawerElevation) {
        this.mDrawerElevation = mDrawerElevation;
        for (int i = 0; i < this.getChildCount(); ++i) {
            final View child = this.getChildAt(i);
            if (this.isDrawerView(child)) {
                ViewCompat.setElevation(child, this.mDrawerElevation);
            }
        }
    }
    
    @Deprecated
    public void setDrawerListener(final DrawerListener mListener) {
        final DrawerListener mListener2 = this.mListener;
        if (mListener2 != null) {
            this.removeDrawerListener(mListener2);
        }
        if (mListener != null) {
            this.addDrawerListener(mListener);
        }
        this.mListener = mListener;
    }
    
    public void setDrawerLockMode(final int n) {
        this.setDrawerLockMode(n, 3);
        this.setDrawerLockMode(n, 5);
    }
    
    public void setDrawerLockMode(final int n, final int n2) {
        final int absoluteGravity = GravityCompat.getAbsoluteGravity(n2, ViewCompat.getLayoutDirection((View)this));
        if (n2 != 3) {
            if (n2 != 5) {
                if (n2 != 8388611) {
                    if (n2 == 8388613) {
                        this.mLockModeEnd = n;
                    }
                }
                else {
                    this.mLockModeStart = n;
                }
            }
            else {
                this.mLockModeRight = n;
            }
        }
        else {
            this.mLockModeLeft = n;
        }
        if (n != 0) {
            ViewDragHelper viewDragHelper;
            if (absoluteGravity == 3) {
                viewDragHelper = this.mLeftDragger;
            }
            else {
                viewDragHelper = this.mRightDragger;
            }
            viewDragHelper.cancel();
        }
        if (n != 1) {
            if (n == 2) {
                final View drawerWithGravity = this.findDrawerWithGravity(absoluteGravity);
                if (drawerWithGravity != null) {
                    this.openDrawer(drawerWithGravity);
                }
            }
        }
        else {
            final View drawerWithGravity2 = this.findDrawerWithGravity(absoluteGravity);
            if (drawerWithGravity2 != null) {
                this.closeDrawer(drawerWithGravity2);
            }
        }
    }
    
    public void setDrawerLockMode(final int n, @NonNull final View obj) {
        if (this.isDrawerView(obj)) {
            this.setDrawerLockMode(n, ((LayoutParams)obj.getLayoutParams()).gravity);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("View ");
        sb.append(obj);
        sb.append(" is not a drawer with appropriate layout_gravity");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void setDrawerShadow(@DrawableRes final int n, final int n2) {
        this.setDrawerShadow(ContextCompat.getDrawable(((View)this).getContext(), n), n2);
    }
    
    public void setDrawerShadow(final Drawable drawable, final int n) {
        if (DrawerLayout.SET_DRAWER_SHADOW_FROM_ELEVATION) {
            return;
        }
        if ((n & 0x800003) == 0x800003) {
            this.mShadowStart = drawable;
        }
        else if ((n & 0x800005) == 0x800005) {
            this.mShadowEnd = drawable;
        }
        else if ((n & 0x3) == 0x3) {
            this.mShadowLeft = drawable;
        }
        else {
            if ((n & 0x5) != 0x5) {
                return;
            }
            this.mShadowRight = drawable;
        }
        this.resolveShadowDrawables();
        ((View)this).invalidate();
    }
    
    public void setDrawerTitle(int absoluteGravity, @Nullable final CharSequence charSequence) {
        absoluteGravity = GravityCompat.getAbsoluteGravity(absoluteGravity, ViewCompat.getLayoutDirection((View)this));
        if (absoluteGravity == 3) {
            this.mTitleLeft = charSequence;
        }
        else if (absoluteGravity == 5) {
            this.mTitleRight = charSequence;
        }
    }
    
    void setDrawerViewOffset(final View view, final float onScreen) {
        final LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        if (onScreen == layoutParams.onScreen) {
            return;
        }
        this.dispatchOnDrawerSlide(view, layoutParams.onScreen = onScreen);
    }
    
    public void setScrimColor(@ColorInt final int mScrimColor) {
        this.mScrimColor = mScrimColor;
        ((View)this).invalidate();
    }
    
    public void setStatusBarBackground(final int n) {
        Drawable drawable;
        if (n != 0) {
            drawable = ContextCompat.getDrawable(((View)this).getContext(), n);
        }
        else {
            drawable = null;
        }
        this.mStatusBarBackground = drawable;
        ((View)this).invalidate();
    }
    
    public void setStatusBarBackground(@Nullable final Drawable mStatusBarBackground) {
        this.mStatusBarBackground = mStatusBarBackground;
        ((View)this).invalidate();
    }
    
    public void setStatusBarBackgroundColor(@ColorInt final int n) {
        this.mStatusBarBackground = (Drawable)new ColorDrawable(n);
        ((View)this).invalidate();
    }
    
    void updateDrawerState(int i, final View view) {
        final int viewDragState = this.mLeftDragger.getViewDragState();
        final int viewDragState2 = this.mRightDragger.getViewDragState();
        int mDrawerState;
        if (viewDragState != 1 && viewDragState2 != 1) {
            final int n = mDrawerState = 2;
            if (viewDragState != 2) {
                if (viewDragState2 == 2) {
                    mDrawerState = n;
                }
                else {
                    mDrawerState = 0;
                }
            }
        }
        else {
            mDrawerState = 1;
        }
        if (view != null && i == 0) {
            final float onScreen = ((LayoutParams)view.getLayoutParams()).onScreen;
            if (onScreen == 0.0f) {
                this.dispatchOnDrawerClosed(view);
            }
            else if (onScreen == 1.0f) {
                this.dispatchOnDrawerOpened(view);
            }
        }
        if (mDrawerState != this.mDrawerState) {
            this.mDrawerState = mDrawerState;
            final List<DrawerListener> mListeners = this.mListeners;
            if (mListeners != null) {
                for (i = mListeners.size() - 1; i >= 0; --i) {
                    this.mListeners.get(i).onDrawerStateChanged(mDrawerState);
                }
            }
        }
    }
    
    class AccessibilityDelegate extends AccessibilityDelegateCompat
    {
        private final Rect mTmpRect;
        final DrawerLayout this$0;
        
        AccessibilityDelegate(final DrawerLayout this$0) {
            this.this$0 = this$0;
            this.mTmpRect = new Rect();
        }
        
        private void addChildrenForAccessibility(final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat, final ViewGroup viewGroup) {
            for (int childCount = viewGroup.getChildCount(), i = 0; i < childCount; ++i) {
                final View child = viewGroup.getChildAt(i);
                if (DrawerLayout.includeChildForAccessibility(child)) {
                    accessibilityNodeInfoCompat.addChild(child);
                }
            }
        }
        
        private void copyNodeInfoNoChildren(final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat, final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat2) {
            final Rect mTmpRect = this.mTmpRect;
            accessibilityNodeInfoCompat2.getBoundsInScreen(mTmpRect);
            accessibilityNodeInfoCompat.setBoundsInScreen(mTmpRect);
            accessibilityNodeInfoCompat.setVisibleToUser(accessibilityNodeInfoCompat2.isVisibleToUser());
            accessibilityNodeInfoCompat.setPackageName(accessibilityNodeInfoCompat2.getPackageName());
            accessibilityNodeInfoCompat.setClassName(accessibilityNodeInfoCompat2.getClassName());
            accessibilityNodeInfoCompat.setContentDescription(accessibilityNodeInfoCompat2.getContentDescription());
            accessibilityNodeInfoCompat.setEnabled(accessibilityNodeInfoCompat2.isEnabled());
            accessibilityNodeInfoCompat.setFocused(accessibilityNodeInfoCompat2.isFocused());
            accessibilityNodeInfoCompat.setAccessibilityFocused(accessibilityNodeInfoCompat2.isAccessibilityFocused());
            accessibilityNodeInfoCompat.setSelected(accessibilityNodeInfoCompat2.isSelected());
            accessibilityNodeInfoCompat.addAction(accessibilityNodeInfoCompat2.getActions());
        }
        
        @Override
        public boolean dispatchPopulateAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
            if (accessibilityEvent.getEventType() == 32) {
                final List text = ((AccessibilityRecord)accessibilityEvent).getText();
                final View visibleDrawer = this.this$0.findVisibleDrawer();
                if (visibleDrawer != null) {
                    final CharSequence drawerTitle = this.this$0.getDrawerTitle(this.this$0.getDrawerViewAbsoluteGravity(visibleDrawer));
                    if (drawerTitle != null) {
                        text.add(drawerTitle);
                    }
                }
                return true;
            }
            return super.dispatchPopulateAccessibilityEvent(view, accessibilityEvent);
        }
        
        @Override
        public void onInitializeAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(view, accessibilityEvent);
            ((AccessibilityRecord)accessibilityEvent).setClassName((CharSequence)"androidx.drawerlayout.widget.DrawerLayout");
        }
        
        @Override
        public void onInitializeAccessibilityNodeInfo(final View source, final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            if (DrawerLayout.CAN_HIDE_DESCENDANTS) {
                super.onInitializeAccessibilityNodeInfo(source, accessibilityNodeInfoCompat);
            }
            else {
                final AccessibilityNodeInfoCompat obtain = AccessibilityNodeInfoCompat.obtain(accessibilityNodeInfoCompat);
                super.onInitializeAccessibilityNodeInfo(source, obtain);
                accessibilityNodeInfoCompat.setSource(source);
                final ViewParent parentForAccessibility = ViewCompat.getParentForAccessibility(source);
                if (parentForAccessibility instanceof View) {
                    accessibilityNodeInfoCompat.setParent((View)parentForAccessibility);
                }
                this.copyNodeInfoNoChildren(accessibilityNodeInfoCompat, obtain);
                obtain.recycle();
                this.addChildrenForAccessibility(accessibilityNodeInfoCompat, (ViewGroup)source);
            }
            accessibilityNodeInfoCompat.setClassName("androidx.drawerlayout.widget.DrawerLayout");
            accessibilityNodeInfoCompat.setFocusable(false);
            accessibilityNodeInfoCompat.setFocused(false);
            accessibilityNodeInfoCompat.removeAction(AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_FOCUS);
            accessibilityNodeInfoCompat.removeAction(AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_CLEAR_FOCUS);
        }
        
        @Override
        public boolean onRequestSendAccessibilityEvent(final ViewGroup viewGroup, final View view, final AccessibilityEvent accessibilityEvent) {
            return (DrawerLayout.CAN_HIDE_DESCENDANTS || DrawerLayout.includeChildForAccessibility(view)) && super.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
        }
    }
    
    static final class ChildAccessibilityDelegate extends AccessibilityDelegateCompat
    {
        @Override
        public void onInitializeAccessibilityNodeInfo(final View view, final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            super.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat);
            if (!DrawerLayout.includeChildForAccessibility(view)) {
                accessibilityNodeInfoCompat.setParent(null);
            }
        }
    }
    
    public interface DrawerListener
    {
        void onDrawerClosed(@NonNull final View p0);
        
        void onDrawerOpened(@NonNull final View p0);
        
        void onDrawerSlide(@NonNull final View p0, final float p1);
        
        void onDrawerStateChanged(final int p0);
    }
    
    public static class LayoutParams extends ViewGroup$MarginLayoutParams
    {
        private static final int FLAG_IS_CLOSING = 4;
        private static final int FLAG_IS_OPENED = 1;
        private static final int FLAG_IS_OPENING = 2;
        public int gravity;
        boolean isPeeking;
        float onScreen;
        int openState;
        
        public LayoutParams(final int n, final int n2) {
            super(n, n2);
            this.gravity = 0;
        }
        
        public LayoutParams(final int n, final int n2, final int gravity) {
            this(n, n2);
            this.gravity = gravity;
        }
        
        public LayoutParams(@NonNull final Context context, @Nullable final AttributeSet set) {
            super(context, set);
            this.gravity = 0;
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, DrawerLayout.LAYOUT_ATTRS);
            this.gravity = obtainStyledAttributes.getInt(0, 0);
            obtainStyledAttributes.recycle();
        }
        
        public LayoutParams(@NonNull final ViewGroup$LayoutParams viewGroup$LayoutParams) {
            super(viewGroup$LayoutParams);
            this.gravity = 0;
        }
        
        public LayoutParams(@NonNull final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            super(viewGroup$MarginLayoutParams);
            this.gravity = 0;
        }
        
        public LayoutParams(@NonNull final LayoutParams layoutParams) {
            super((ViewGroup$MarginLayoutParams)layoutParams);
            this.gravity = 0;
            this.gravity = layoutParams.gravity;
        }
    }
    
    protected static class SavedState extends AbsSavedState
    {
        public static final Parcelable$Creator<SavedState> CREATOR;
        int lockModeEnd;
        int lockModeLeft;
        int lockModeRight;
        int lockModeStart;
        int openDrawerGravity;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$ClassLoaderCreator<SavedState>() {
                public SavedState createFromParcel(final Parcel parcel) {
                    return new SavedState(parcel, null);
                }
                
                public SavedState createFromParcel(final Parcel parcel, final ClassLoader classLoader) {
                    return new SavedState(parcel, classLoader);
                }
                
                public SavedState[] newArray(final int n) {
                    return new SavedState[n];
                }
            };
        }
        
        public SavedState(@NonNull final Parcel parcel, @Nullable final ClassLoader classLoader) {
            super(parcel, classLoader);
            this.openDrawerGravity = 0;
            this.openDrawerGravity = parcel.readInt();
            this.lockModeLeft = parcel.readInt();
            this.lockModeRight = parcel.readInt();
            this.lockModeStart = parcel.readInt();
            this.lockModeEnd = parcel.readInt();
        }
        
        public SavedState(@NonNull final Parcelable parcelable) {
            super(parcelable);
            this.openDrawerGravity = 0;
        }
        
        @Override
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeInt(this.openDrawerGravity);
            parcel.writeInt(this.lockModeLeft);
            parcel.writeInt(this.lockModeRight);
            parcel.writeInt(this.lockModeStart);
            parcel.writeInt(this.lockModeEnd);
        }
    }
    
    public abstract static class SimpleDrawerListener implements DrawerListener
    {
        @Override
        public void onDrawerClosed(final View view) {
        }
        
        @Override
        public void onDrawerOpened(final View view) {
        }
        
        @Override
        public void onDrawerSlide(final View view, final float n) {
        }
        
        @Override
        public void onDrawerStateChanged(final int n) {
        }
    }
    
    private class ViewDragCallback extends Callback
    {
        private final int mAbsGravity;
        private ViewDragHelper mDragger;
        private final Runnable mPeekRunnable;
        final DrawerLayout this$0;
        
        ViewDragCallback(final DrawerLayout this$0, final int mAbsGravity) {
            this.this$0 = this$0;
            this.mPeekRunnable = new Runnable() {
                final ViewDragCallback this$1;
                
                @Override
                public void run() {
                    this.this$1.peekDrawer();
                }
            };
            this.mAbsGravity = mAbsGravity;
        }
        
        private void closeOtherDrawer() {
            final int mAbsGravity = this.mAbsGravity;
            int n = 3;
            if (mAbsGravity == 3) {
                n = 5;
            }
            final View drawerWithGravity = this.this$0.findDrawerWithGravity(n);
            if (drawerWithGravity != null) {
                this.this$0.closeDrawer(drawerWithGravity);
            }
        }
        
        @Override
        public int clampViewPositionHorizontal(final View view, final int n, int width) {
            if (this.this$0.checkDrawerViewAbsoluteGravity(view, 3)) {
                return Math.max(-view.getWidth(), Math.min(n, 0));
            }
            width = ((View)this.this$0).getWidth();
            return Math.max(width - view.getWidth(), Math.min(n, width));
        }
        
        @Override
        public int clampViewPositionVertical(final View view, final int n, final int n2) {
            return view.getTop();
        }
        
        @Override
        public int getViewHorizontalDragRange(final View view) {
            int width;
            if (this.this$0.isDrawerView(view)) {
                width = view.getWidth();
            }
            else {
                width = 0;
            }
            return width;
        }
        
        @Override
        public void onEdgeDragStarted(final int n, final int n2) {
            View view;
            if ((n & 0x1) == 0x1) {
                view = this.this$0.findDrawerWithGravity(3);
            }
            else {
                view = this.this$0.findDrawerWithGravity(5);
            }
            if (view != null && this.this$0.getDrawerLockMode(view) == 0) {
                this.mDragger.captureChildView(view, n2);
            }
        }
        
        @Override
        public boolean onEdgeLock(final int n) {
            return false;
        }
        
        @Override
        public void onEdgeTouched(final int n, final int n2) {
            ((View)this.this$0).postDelayed(this.mPeekRunnable, 160L);
        }
        
        @Override
        public void onViewCaptured(final View view, final int n) {
            ((LayoutParams)view.getLayoutParams()).isPeeking = false;
            this.closeOtherDrawer();
        }
        
        @Override
        public void onViewDragStateChanged(final int n) {
            this.this$0.updateDrawerState(n, this.mDragger.getCapturedView());
        }
        
        @Override
        public void onViewPositionChanged(final View view, int visibility, int width, final int n, final int n2) {
            width = view.getWidth();
            float n3;
            if (this.this$0.checkDrawerViewAbsoluteGravity(view, 3)) {
                n3 = (float)(visibility + width);
            }
            else {
                n3 = (float)(((View)this.this$0).getWidth() - visibility);
            }
            final float n4 = n3 / width;
            this.this$0.setDrawerViewOffset(view, n4);
            if (n4 == 0.0f) {
                visibility = 4;
            }
            else {
                visibility = 0;
            }
            view.setVisibility(visibility);
            ((View)this.this$0).invalidate();
        }
        
        @Override
        public void onViewReleased(final View view, final float n, float drawerViewOffset) {
            drawerViewOffset = this.this$0.getDrawerViewOffset(view);
            final int width = view.getWidth();
            int n3 = 0;
            Label_0109: {
                if (this.this$0.checkDrawerViewAbsoluteGravity(view, 3)) {
                    final float n2 = fcmpl(n, 0.0f);
                    if (n2 <= 0 && (n2 != 0 || drawerViewOffset <= 0.5f)) {
                        n3 = -width;
                    }
                    else {
                        n3 = 0;
                    }
                }
                else {
                    final int width2 = ((View)this.this$0).getWidth();
                    if (n >= 0.0f) {
                        n3 = width2;
                        if (n != 0.0f) {
                            break Label_0109;
                        }
                        n3 = width2;
                        if (drawerViewOffset <= 0.5f) {
                            break Label_0109;
                        }
                    }
                    n3 = width2 - width;
                }
            }
            this.mDragger.settleCapturedViewAt(n3, view.getTop());
            ((View)this.this$0).invalidate();
        }
        
        void peekDrawer() {
            final int edgeSize = this.mDragger.getEdgeSize();
            final int mAbsGravity = this.mAbsGravity;
            int n = 0;
            final boolean b = mAbsGravity == 3;
            View view;
            int n2;
            if (b) {
                view = this.this$0.findDrawerWithGravity(3);
                if (view != null) {
                    n = -view.getWidth();
                }
                n2 = n + edgeSize;
            }
            else {
                view = this.this$0.findDrawerWithGravity(5);
                n2 = ((View)this.this$0).getWidth() - edgeSize;
            }
            if (view != null && ((b && view.getLeft() < n2) || (!b && view.getLeft() > n2)) && this.this$0.getDrawerLockMode(view) == 0) {
                final LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
                this.mDragger.smoothSlideViewTo(view, n2, view.getTop());
                layoutParams.isPeeking = true;
                ((View)this.this$0).invalidate();
                this.closeOtherDrawer();
                this.this$0.cancelChildViewTouch();
            }
        }
        
        public void removeCallbacks() {
            ((View)this.this$0).removeCallbacks(this.mPeekRunnable);
        }
        
        public void setDragger(final ViewDragHelper mDragger) {
            this.mDragger = mDragger;
        }
        
        @Override
        public boolean tryCaptureView(final View view, final int n) {
            return this.this$0.isDrawerView(view) && this.this$0.checkDrawerViewAbsoluteGravity(view, this.mAbsGravity) && this.this$0.getDrawerLockMode(view) == 0;
        }
    }
}

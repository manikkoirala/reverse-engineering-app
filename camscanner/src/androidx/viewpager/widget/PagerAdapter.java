// 
// Decompiled by Procyon v0.6.0
// 

package androidx.viewpager.widget;

import android.database.Observable;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import android.view.View;
import android.database.DataSetObserver;
import android.database.DataSetObservable;

public abstract class PagerAdapter
{
    public static final int POSITION_NONE = -2;
    public static final int POSITION_UNCHANGED = -1;
    private final DataSetObservable mObservable;
    private DataSetObserver mViewPagerObserver;
    
    public PagerAdapter() {
        this.mObservable = new DataSetObservable();
    }
    
    @Deprecated
    public void destroyItem(@NonNull final View view, final int n, @NonNull final Object o) {
        throw new UnsupportedOperationException("Required method destroyItem was not overridden");
    }
    
    public void destroyItem(@NonNull final ViewGroup viewGroup, final int n, @NonNull final Object o) {
        this.destroyItem((View)viewGroup, n, o);
    }
    
    @Deprecated
    public void finishUpdate(@NonNull final View view) {
    }
    
    public void finishUpdate(@NonNull final ViewGroup viewGroup) {
        this.finishUpdate((View)viewGroup);
    }
    
    public abstract int getCount();
    
    public int getItemPosition(@NonNull final Object o) {
        return -1;
    }
    
    @Nullable
    public CharSequence getPageTitle(final int n) {
        return null;
    }
    
    public float getPageWidth(final int n) {
        return 1.0f;
    }
    
    @Deprecated
    @NonNull
    public Object instantiateItem(@NonNull final View view, final int n) {
        throw new UnsupportedOperationException("Required method instantiateItem was not overridden");
    }
    
    @NonNull
    public Object instantiateItem(@NonNull final ViewGroup viewGroup, final int n) {
        return this.instantiateItem((View)viewGroup, n);
    }
    
    public abstract boolean isViewFromObject(@NonNull final View p0, @NonNull final Object p1);
    
    public void notifyDataSetChanged() {
        synchronized (this) {
            final DataSetObserver mViewPagerObserver = this.mViewPagerObserver;
            if (mViewPagerObserver != null) {
                mViewPagerObserver.onChanged();
            }
            monitorexit(this);
            this.mObservable.notifyChanged();
        }
    }
    
    public void registerDataSetObserver(@NonNull final DataSetObserver dataSetObserver) {
        ((Observable)this.mObservable).registerObserver((Object)dataSetObserver);
    }
    
    public void restoreState(@Nullable final Parcelable parcelable, @Nullable final ClassLoader classLoader) {
    }
    
    @Nullable
    public Parcelable saveState() {
        return null;
    }
    
    @Deprecated
    public void setPrimaryItem(@NonNull final View view, final int n, @NonNull final Object o) {
    }
    
    public void setPrimaryItem(@NonNull final ViewGroup viewGroup, final int n, @NonNull final Object o) {
        this.setPrimaryItem((View)viewGroup, n, o);
    }
    
    void setViewPagerObserver(final DataSetObserver mViewPagerObserver) {
        synchronized (this) {
            this.mViewPagerObserver = mViewPagerObserver;
        }
    }
    
    @Deprecated
    public void startUpdate(@NonNull final View view) {
    }
    
    public void startUpdate(@NonNull final ViewGroup viewGroup) {
        this.startUpdate((View)viewGroup);
    }
    
    public void unregisterDataSetObserver(@NonNull final DataSetObserver dataSetObserver) {
        ((Observable)this.mObservable).unregisterObserver((Object)dataSetObserver);
    }
}

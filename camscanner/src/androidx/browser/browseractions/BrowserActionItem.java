// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.browseractions;

import androidx.annotation.RestrictTo;
import androidx.annotation.NonNull;
import android.net.Uri;
import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import android.app.PendingIntent;

@Deprecated
public class BrowserActionItem
{
    @Nullable
    private final PendingIntent mAction;
    @DrawableRes
    private int mIconId;
    @Nullable
    private Uri mIconUri;
    @Nullable
    private Runnable mRunnableAction;
    private final String mTitle;
    
    public BrowserActionItem(@NonNull final String s, @NonNull final PendingIntent pendingIntent) {
        this(s, pendingIntent, 0);
    }
    
    public BrowserActionItem(@NonNull final String mTitle, @NonNull final PendingIntent mAction, @DrawableRes final int mIconId) {
        this.mTitle = mTitle;
        this.mAction = mAction;
        this.mIconId = mIconId;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public BrowserActionItem(@NonNull final String mTitle, @NonNull final PendingIntent mAction, @NonNull final Uri mIconUri) {
        this.mTitle = mTitle;
        this.mAction = mAction;
        this.mIconUri = mIconUri;
    }
    
    BrowserActionItem(@NonNull final String mTitle, @NonNull final Runnable mRunnableAction) {
        this.mTitle = mTitle;
        this.mAction = null;
        this.mRunnableAction = mRunnableAction;
    }
    
    @NonNull
    public PendingIntent getAction() {
        final PendingIntent mAction = this.mAction;
        if (mAction != null) {
            return mAction;
        }
        throw new IllegalStateException("Can't call getAction on BrowserActionItem with null action.");
    }
    
    public int getIconId() {
        return this.mIconId;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public Uri getIconUri() {
        return this.mIconUri;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    Runnable getRunnableAction() {
        return this.mRunnableAction;
    }
    
    @NonNull
    public String getTitle() {
        return this.mTitle;
    }
}

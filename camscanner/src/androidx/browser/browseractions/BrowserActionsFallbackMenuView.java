// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.browseractions;

import android.view.View;
import android.view.View$MeasureSpec;
import androidx.browser.R;
import android.util.AttributeSet;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.annotation.RestrictTo;
import android.widget.LinearLayout;

@Deprecated
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public class BrowserActionsFallbackMenuView extends LinearLayout
{
    private final int mBrowserActionsMenuMaxWidthPx;
    private final int mBrowserActionsMenuMinPaddingPx;
    
    public BrowserActionsFallbackMenuView(@NonNull final Context context, @NonNull final AttributeSet set) {
        super(context, set);
        this.mBrowserActionsMenuMinPaddingPx = ((View)this).getResources().getDimensionPixelOffset(R.dimen.browser_actions_context_menu_min_padding);
        this.mBrowserActionsMenuMaxWidthPx = ((View)this).getResources().getDimensionPixelOffset(R.dimen.browser_actions_context_menu_max_width);
    }
    
    protected void onMeasure(final int n, final int n2) {
        super.onMeasure(View$MeasureSpec.makeMeasureSpec(Math.min(((View)this).getResources().getDisplayMetrics().widthPixels - this.mBrowserActionsMenuMinPaddingPx * 2, this.mBrowserActionsMenuMaxWidthPx), 1073741824), n2);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser;

public final class R
{
    private R() {
    }
    
    public static final class color
    {
        public static final int browser_actions_bg_grey = 2131099752;
        public static final int browser_actions_divider_color = 2131099753;
        public static final int browser_actions_text_color = 2131099754;
        public static final int browser_actions_title_color = 2131099755;
        
        private color() {
        }
    }
    
    public static final class dimen
    {
        public static final int browser_actions_context_menu_max_width = 2131165348;
        public static final int browser_actions_context_menu_min_padding = 2131165349;
        
        private dimen() {
        }
    }
    
    public static final class id
    {
        public static final int browser_actions_header_text = 2131362307;
        public static final int browser_actions_menu_item_icon = 2131362308;
        public static final int browser_actions_menu_item_text = 2131362309;
        public static final int browser_actions_menu_items = 2131362310;
        public static final int browser_actions_menu_view = 2131362311;
        
        private id() {
        }
    }
    
    public static final class layout
    {
        public static final int browser_actions_context_menu_page = 2131558673;
        public static final int browser_actions_context_menu_row = 2131558674;
        
        private layout() {
        }
    }
    
    public static final class string
    {
        public static final int copy_toast_msg = 2131953081;
        public static final int fallback_menu_item_copy_link = 2131959077;
        public static final int fallback_menu_item_open_in_browser = 2131959078;
        public static final int fallback_menu_item_share_link = 2131959079;
        
        private string() {
        }
    }
    
    public static final class xml
    {
        public static final int image_share_filepaths = 2132148239;
        
        private xml() {
        }
    }
}

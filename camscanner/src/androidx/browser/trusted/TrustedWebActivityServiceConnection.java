// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import android.os.IInterface;
import android.os.BaseBundle;
import android.os.IBinder;
import android.app.Notification;
import android.graphics.Bitmap;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;
import android.os.Parcelable;
import android.os.RemoteException;
import android.support.customtabs.trusted.ITrustedWebActivityCallback;
import androidx.annotation.Nullable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.support.customtabs.trusted.ITrustedWebActivityService;
import android.content.ComponentName;

public final class TrustedWebActivityServiceConnection
{
    private static final String KEY_ACTIVE_NOTIFICATIONS = "android.support.customtabs.trusted.ACTIVE_NOTIFICATIONS";
    private static final String KEY_CHANNEL_NAME = "android.support.customtabs.trusted.CHANNEL_NAME";
    private static final String KEY_NOTIFICATION = "android.support.customtabs.trusted.NOTIFICATION";
    private static final String KEY_NOTIFICATION_SUCCESS = "android.support.customtabs.trusted.NOTIFICATION_SUCCESS";
    private static final String KEY_PLATFORM_ID = "android.support.customtabs.trusted.PLATFORM_ID";
    private static final String KEY_PLATFORM_TAG = "android.support.customtabs.trusted.PLATFORM_TAG";
    private final ComponentName mComponentName;
    private final ITrustedWebActivityService mService;
    
    TrustedWebActivityServiceConnection(@NonNull final ITrustedWebActivityService mService, @NonNull final ComponentName mComponentName) {
        this.mService = mService;
        this.mComponentName = mComponentName;
    }
    
    static void ensureBundleContains(final Bundle bundle, final String str) {
        if (((BaseBundle)bundle).containsKey(str)) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Bundle must contain ");
        sb.append(str);
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Nullable
    private static ITrustedWebActivityCallback wrapCallback(@Nullable final TrustedWebActivityCallback trustedWebActivityCallback) {
        if (trustedWebActivityCallback == null) {
            return null;
        }
        return new ITrustedWebActivityCallback.Stub(trustedWebActivityCallback) {
            final TrustedWebActivityCallback val$callback;
            
            public void onExtraCallback(final String s, final Bundle bundle) throws RemoteException {
                this.val$callback.onExtraCallback(s, bundle);
            }
        };
    }
    
    public boolean areNotificationsEnabled(@NonNull final String s) throws RemoteException {
        return ResultArgs.fromBundle(this.mService.areNotificationsEnabled(new NotificationsEnabledArgs(s).toBundle())).success;
    }
    
    public void cancel(@NonNull final String s, final int n) throws RemoteException {
        this.mService.cancelNotification(new CancelNotificationArgs(s, n).toBundle());
    }
    
    @NonNull
    @RequiresApi(23)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public Parcelable[] getActiveNotifications() throws RemoteException {
        return ActiveNotificationsArgs.fromBundle(this.mService.getActiveNotifications()).notifications;
    }
    
    @NonNull
    public ComponentName getComponentName() {
        return this.mComponentName;
    }
    
    @Nullable
    public Bitmap getSmallIconBitmap() throws RemoteException {
        return (Bitmap)this.mService.getSmallIconBitmap().getParcelable("android.support.customtabs.trusted.SMALL_ICON_BITMAP");
    }
    
    public int getSmallIconId() throws RemoteException {
        return this.mService.getSmallIconId();
    }
    
    public boolean notify(@NonNull final String s, final int n, @NonNull final Notification notification, @NonNull final String s2) throws RemoteException {
        return ResultArgs.fromBundle(this.mService.notifyNotificationWithChannel(new NotifyNotificationArgs(s, n, notification, s2).toBundle())).success;
    }
    
    @Nullable
    public Bundle sendExtraCommand(@NonNull final String s, @NonNull final Bundle bundle, @Nullable final TrustedWebActivityCallback trustedWebActivityCallback) throws RemoteException {
        final ITrustedWebActivityCallback wrapCallback = wrapCallback(trustedWebActivityCallback);
        IBinder binder;
        if (wrapCallback == null) {
            binder = null;
        }
        else {
            binder = ((IInterface)wrapCallback).asBinder();
        }
        return this.mService.extraCommand(s, bundle, binder);
    }
    
    static class ActiveNotificationsArgs
    {
        public final Parcelable[] notifications;
        
        ActiveNotificationsArgs(final Parcelable[] notifications) {
            this.notifications = notifications;
        }
        
        public static ActiveNotificationsArgs fromBundle(final Bundle bundle) {
            TrustedWebActivityServiceConnection.ensureBundleContains(bundle, "android.support.customtabs.trusted.ACTIVE_NOTIFICATIONS");
            return new ActiveNotificationsArgs(bundle.getParcelableArray("android.support.customtabs.trusted.ACTIVE_NOTIFICATIONS"));
        }
        
        public Bundle toBundle() {
            final Bundle bundle = new Bundle();
            bundle.putParcelableArray("android.support.customtabs.trusted.ACTIVE_NOTIFICATIONS", this.notifications);
            return bundle;
        }
    }
    
    static class CancelNotificationArgs
    {
        public final int platformId;
        public final String platformTag;
        
        CancelNotificationArgs(final String platformTag, final int platformId) {
            this.platformTag = platformTag;
            this.platformId = platformId;
        }
        
        public static CancelNotificationArgs fromBundle(final Bundle bundle) {
            TrustedWebActivityServiceConnection.ensureBundleContains(bundle, "android.support.customtabs.trusted.PLATFORM_TAG");
            TrustedWebActivityServiceConnection.ensureBundleContains(bundle, "android.support.customtabs.trusted.PLATFORM_ID");
            return new CancelNotificationArgs(((BaseBundle)bundle).getString("android.support.customtabs.trusted.PLATFORM_TAG"), ((BaseBundle)bundle).getInt("android.support.customtabs.trusted.PLATFORM_ID"));
        }
        
        public Bundle toBundle() {
            final Bundle bundle = new Bundle();
            ((BaseBundle)bundle).putString("android.support.customtabs.trusted.PLATFORM_TAG", this.platformTag);
            ((BaseBundle)bundle).putInt("android.support.customtabs.trusted.PLATFORM_ID", this.platformId);
            return bundle;
        }
    }
    
    static class NotificationsEnabledArgs
    {
        public final String channelName;
        
        NotificationsEnabledArgs(final String channelName) {
            this.channelName = channelName;
        }
        
        public static NotificationsEnabledArgs fromBundle(final Bundle bundle) {
            TrustedWebActivityServiceConnection.ensureBundleContains(bundle, "android.support.customtabs.trusted.CHANNEL_NAME");
            return new NotificationsEnabledArgs(((BaseBundle)bundle).getString("android.support.customtabs.trusted.CHANNEL_NAME"));
        }
        
        public Bundle toBundle() {
            final Bundle bundle = new Bundle();
            ((BaseBundle)bundle).putString("android.support.customtabs.trusted.CHANNEL_NAME", this.channelName);
            return bundle;
        }
    }
    
    static class NotifyNotificationArgs
    {
        public final String channelName;
        public final Notification notification;
        public final int platformId;
        public final String platformTag;
        
        NotifyNotificationArgs(final String platformTag, final int platformId, final Notification notification, final String channelName) {
            this.platformTag = platformTag;
            this.platformId = platformId;
            this.notification = notification;
            this.channelName = channelName;
        }
        
        public static NotifyNotificationArgs fromBundle(final Bundle bundle) {
            TrustedWebActivityServiceConnection.ensureBundleContains(bundle, "android.support.customtabs.trusted.PLATFORM_TAG");
            TrustedWebActivityServiceConnection.ensureBundleContains(bundle, "android.support.customtabs.trusted.PLATFORM_ID");
            TrustedWebActivityServiceConnection.ensureBundleContains(bundle, "android.support.customtabs.trusted.NOTIFICATION");
            TrustedWebActivityServiceConnection.ensureBundleContains(bundle, "android.support.customtabs.trusted.CHANNEL_NAME");
            return new NotifyNotificationArgs(((BaseBundle)bundle).getString("android.support.customtabs.trusted.PLATFORM_TAG"), ((BaseBundle)bundle).getInt("android.support.customtabs.trusted.PLATFORM_ID"), (Notification)bundle.getParcelable("android.support.customtabs.trusted.NOTIFICATION"), ((BaseBundle)bundle).getString("android.support.customtabs.trusted.CHANNEL_NAME"));
        }
        
        public Bundle toBundle() {
            final Bundle bundle = new Bundle();
            ((BaseBundle)bundle).putString("android.support.customtabs.trusted.PLATFORM_TAG", this.platformTag);
            ((BaseBundle)bundle).putInt("android.support.customtabs.trusted.PLATFORM_ID", this.platformId);
            bundle.putParcelable("android.support.customtabs.trusted.NOTIFICATION", (Parcelable)this.notification);
            ((BaseBundle)bundle).putString("android.support.customtabs.trusted.CHANNEL_NAME", this.channelName);
            return bundle;
        }
    }
    
    static class ResultArgs
    {
        public final boolean success;
        
        ResultArgs(final boolean success) {
            this.success = success;
        }
        
        public static ResultArgs fromBundle(final Bundle bundle) {
            TrustedWebActivityServiceConnection.ensureBundleContains(bundle, "android.support.customtabs.trusted.NOTIFICATION_SUCCESS");
            return new ResultArgs(bundle.getBoolean("android.support.customtabs.trusted.NOTIFICATION_SUCCESS"));
        }
        
        public Bundle toBundle() {
            final Bundle bundle = new Bundle();
            bundle.putBoolean("android.support.customtabs.trusted.NOTIFICATION_SUCCESS", this.success);
            return bundle;
        }
    }
}

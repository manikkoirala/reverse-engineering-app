// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import androidx.annotation.RequiresApi;
import androidx.annotation.NonNull;
import android.os.Parcelable;
import android.app.NotificationManager;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
public class NotificationApiHelperForM
{
    private NotificationApiHelperForM() {
    }
    
    @NonNull
    @RequiresApi(23)
    static Parcelable[] getActiveNotifications(final NotificationManager notificationManager) {
        return (Parcelable[])\u3007o00\u3007\u3007Oo.\u3007080(notificationManager);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import android.os.RemoteException;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.os.IBinder;
import androidx.annotation.NonNull;
import android.support.customtabs.trusted.ITrustedWebActivityCallback;

public class TrustedWebActivityCallbackRemote
{
    private final ITrustedWebActivityCallback mCallbackBinder;
    
    private TrustedWebActivityCallbackRemote(@NonNull final ITrustedWebActivityCallback mCallbackBinder) {
        this.mCallbackBinder = mCallbackBinder;
    }
    
    @Nullable
    static TrustedWebActivityCallbackRemote fromBinder(@Nullable final IBinder binder) {
        ITrustedWebActivityCallback interface1;
        if (binder == null) {
            interface1 = null;
        }
        else {
            interface1 = ITrustedWebActivityCallback.Stub.asInterface(binder);
        }
        if (interface1 == null) {
            return null;
        }
        return new TrustedWebActivityCallbackRemote(interface1);
    }
    
    public void runExtraCallback(@NonNull final String s, @NonNull final Bundle bundle) throws RemoteException {
        this.mCallbackBinder.onExtraCallback(s, bundle);
    }
}

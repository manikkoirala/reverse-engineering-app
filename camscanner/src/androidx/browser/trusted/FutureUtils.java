// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import androidx.concurrent.futures.ResolvableFuture;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.annotation.NonNull;

class FutureUtils
{
    private FutureUtils() {
    }
    
    @NonNull
    static <T> ListenableFuture<T> immediateFailedFuture(@NonNull final Throwable exception) {
        final ResolvableFuture<Object> create = ResolvableFuture.create();
        create.setException(exception);
        return (ListenableFuture<T>)create;
    }
}

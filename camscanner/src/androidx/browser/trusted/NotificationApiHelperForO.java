// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import androidx.annotation.RequiresApi;
import androidx.annotation.Nullable;
import android.app.Notification$Builder;
import android.app.NotificationChannel;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
class NotificationApiHelperForO
{
    private NotificationApiHelperForO() {
    }
    
    @Nullable
    @RequiresApi(26)
    static Notification copyNotificationOntoChannel(final Context context, final NotificationManager notificationManager, final Notification notification, final String s, final String s2) {
        \u3007o\u3007.\u3007080(notificationManager, new NotificationChannel(s, (CharSequence)s2, 3));
        if (Oo08.\u3007080(O8.\u3007080(notificationManager, s)) == 0) {
            return null;
        }
        final Notification$Builder \u3007080 = o\u30070.\u3007080(context, notification);
        \u3007\u3007888.\u3007080(\u3007080, s);
        return \u3007080.build();
    }
    
    @RequiresApi(26)
    static boolean isChannelEnabled(final NotificationManager notificationManager, final String s) {
        final NotificationChannel \u3007080 = O8.\u3007080(notificationManager, s);
        return \u3007080 == null || Oo08.\u3007080(\u3007080) != 0;
    }
}

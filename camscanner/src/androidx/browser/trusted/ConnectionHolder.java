// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import android.support.customtabs.trusted.ITrustedWebActivityService;
import android.os.IBinder;
import android.content.ComponentName;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.Iterator;
import java.util.ArrayList;
import androidx.annotation.MainThread;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.content.ServiceConnection;

class ConnectionHolder implements ServiceConnection
{
    private static final int STATE_AWAITING_CONNECTION = 0;
    private static final int STATE_CANCELLED = 3;
    private static final int STATE_CONNECTED = 1;
    private static final int STATE_DISCONNECTED = 2;
    @Nullable
    private Exception mCancellationException;
    @NonNull
    private final Runnable mCloseRunnable;
    @NonNull
    private List<CallbackToFutureAdapter.Completer<TrustedWebActivityServiceConnection>> mCompleters;
    @Nullable
    private TrustedWebActivityServiceConnection mService;
    private int mState;
    @NonNull
    private final WrapperFactory mWrapperFactory;
    
    @MainThread
    ConnectionHolder(@NonNull final Runnable runnable) {
        this(runnable, new WrapperFactory());
    }
    
    @MainThread
    ConnectionHolder(@NonNull final Runnable mCloseRunnable, @NonNull final WrapperFactory mWrapperFactory) {
        this.mState = 0;
        this.mCompleters = new ArrayList<CallbackToFutureAdapter.Completer<TrustedWebActivityServiceConnection>>();
        this.mCloseRunnable = mCloseRunnable;
        this.mWrapperFactory = mWrapperFactory;
    }
    
    @MainThread
    public void cancel(@NonNull final Exception ex) {
        final Iterator<CallbackToFutureAdapter.Completer<TrustedWebActivityServiceConnection>> iterator = this.mCompleters.iterator();
        while (iterator.hasNext()) {
            ((CallbackToFutureAdapter.Completer)iterator.next()).setException(ex);
        }
        this.mCompleters.clear();
        this.mCloseRunnable.run();
        this.mState = 3;
        this.mCancellationException = ex;
    }
    
    @MainThread
    @NonNull
    public ListenableFuture<TrustedWebActivityServiceConnection> getServiceWrapper() {
        return CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<TrustedWebActivityServiceConnection>)new \u3007080(this));
    }
    
    @MainThread
    public void onServiceConnected(final ComponentName componentName, final IBinder binder) {
        this.mService = this.mWrapperFactory.create(componentName, binder);
        final Iterator<CallbackToFutureAdapter.Completer<TrustedWebActivityServiceConnection>> iterator = this.mCompleters.iterator();
        while (iterator.hasNext()) {
            ((CallbackToFutureAdapter.Completer<TrustedWebActivityServiceConnection>)iterator.next()).set(this.mService);
        }
        this.mCompleters.clear();
        this.mState = 1;
    }
    
    @MainThread
    public void onServiceDisconnected(final ComponentName componentName) {
        this.mService = null;
        this.mCloseRunnable.run();
        this.mState = 2;
    }
    
    static class WrapperFactory
    {
        @NonNull
        TrustedWebActivityServiceConnection create(final ComponentName componentName, final IBinder binder) {
            return new TrustedWebActivityServiceConnection(ITrustedWebActivityService.Stub.asInterface(binder), componentName);
        }
    }
}

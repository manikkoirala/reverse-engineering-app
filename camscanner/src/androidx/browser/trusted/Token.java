// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import androidx.annotation.Nullable;
import java.util.List;
import java.io.IOException;
import android.content.pm.PackageManager;
import androidx.annotation.NonNull;

public final class Token
{
    private static final String TAG = "Token";
    @NonNull
    private final TokenContents mContents;
    
    private Token(@NonNull final TokenContents mContents) {
        this.mContents = mContents;
    }
    
    @Nullable
    public static Token create(@NonNull final String s, @NonNull final PackageManager packageManager) {
        final List<byte[]> fingerprintsForPackage = PackageIdentityUtils.getFingerprintsForPackage(s, packageManager);
        if (fingerprintsForPackage == null) {
            return null;
        }
        try {
            return new Token(TokenContents.create(s, fingerprintsForPackage));
        }
        catch (final IOException ex) {
            return null;
        }
    }
    
    @NonNull
    public static Token deserialize(@NonNull final byte[] array) {
        return new Token(TokenContents.deserialize(array));
    }
    
    public boolean matches(@NonNull final String s, @NonNull final PackageManager packageManager) {
        return PackageIdentityUtils.packageMatchesToken(s, packageManager, this.mContents);
    }
    
    @NonNull
    public byte[] serialize() {
        return this.mContents.serialize();
    }
}

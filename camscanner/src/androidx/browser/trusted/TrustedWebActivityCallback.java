// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import androidx.annotation.Nullable;
import android.os.Bundle;
import androidx.annotation.NonNull;

public abstract class TrustedWebActivityCallback
{
    public abstract void onExtraCallback(@NonNull final String p0, @Nullable final Bundle p1);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import java.io.InputStream;
import java.io.DataInputStream;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.io.DataOutputStream;
import java.io.ByteArrayOutputStream;
import java.util.Comparator;
import java.util.Collections;
import java.io.IOException;
import java.util.Iterator;
import java.util.Arrays;
import java.util.ArrayList;
import androidx.annotation.Nullable;
import java.util.List;
import androidx.annotation.NonNull;

final class TokenContents
{
    @NonNull
    private final byte[] mContents;
    @Nullable
    private List<byte[]> mFingerprints;
    @Nullable
    private String mPackageName;
    
    private TokenContents(@NonNull final byte[] mContents) {
        this.mContents = mContents;
    }
    
    private TokenContents(@NonNull final byte[] mContents, @NonNull final String mPackageName, @NonNull final List<byte[]> list) {
        this.mContents = mContents;
        this.mPackageName = mPackageName;
        this.mFingerprints = new ArrayList<byte[]>(list.size());
        for (final byte[] original : list) {
            this.mFingerprints.add(Arrays.copyOf(original, original.length));
        }
    }
    
    private static int compareByteArrays(final byte[] array, final byte[] array2) {
        if (array == array2) {
            return 0;
        }
        if (array == null) {
            return -1;
        }
        if (array2 == null) {
            return 1;
        }
        for (int i = 0; i < Math.min(array.length, array2.length); ++i) {
            final byte b = array[i];
            final byte b2 = array2[i];
            if (b != b2) {
                return b - b2;
            }
        }
        if (array.length != array2.length) {
            return array.length - array2.length;
        }
        return 0;
    }
    
    @NonNull
    static TokenContents create(final String s, final List<byte[]> list) throws IOException {
        return new TokenContents(createToken(s, list), s, list);
    }
    
    @NonNull
    private static byte[] createToken(@NonNull final String str, @NonNull final List<byte[]> list) throws IOException {
        Collections.sort((List<Object>)list, new OO0o\u3007\u3007());
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final DataOutputStream dataOutputStream = new DataOutputStream(out);
        dataOutputStream.writeUTF(str);
        dataOutputStream.writeInt(list.size());
        for (final byte[] b : list) {
            dataOutputStream.writeInt(b.length);
            dataOutputStream.write(b);
        }
        dataOutputStream.flush();
        return out.toByteArray();
    }
    
    @NonNull
    static TokenContents deserialize(@NonNull final byte[] array) {
        return new TokenContents(array);
    }
    
    private void parseIfNeeded() throws IOException {
        if (this.mPackageName != null) {
            return;
        }
        final DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(this.mContents));
        this.mPackageName = dataInputStream.readUTF();
        final int int1 = dataInputStream.readInt();
        this.mFingerprints = new ArrayList<byte[]>(int1);
        for (int i = 0; i < int1; ++i) {
            final int int2 = dataInputStream.readInt();
            final byte[] b = new byte[int2];
            if (dataInputStream.read(b) != int2) {
                throw new IllegalStateException("Could not read fingerprint");
            }
            this.mFingerprints.add(b);
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o != null && TokenContents.class == o.getClass() && Arrays.equals(this.mContents, ((TokenContents)o).mContents));
    }
    
    @NonNull
    public byte[] getFingerprint(final int n) throws IOException {
        this.parseIfNeeded();
        final List<byte[]> mFingerprints = this.mFingerprints;
        if (mFingerprints != null) {
            return Arrays.copyOf(mFingerprints.get(n), this.mFingerprints.get(n).length);
        }
        throw new IllegalStateException();
    }
    
    public int getFingerprintCount() throws IOException {
        this.parseIfNeeded();
        final List<byte[]> mFingerprints = this.mFingerprints;
        if (mFingerprints != null) {
            return mFingerprints.size();
        }
        throw new IllegalStateException();
    }
    
    @NonNull
    public String getPackageName() throws IOException {
        this.parseIfNeeded();
        final String mPackageName = this.mPackageName;
        if (mPackageName != null) {
            return mPackageName;
        }
        throw new IllegalStateException();
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(this.mContents);
    }
    
    @NonNull
    public byte[] serialize() {
        final byte[] mContents = this.mContents;
        return Arrays.copyOf(mContents, mContents.length);
    }
}

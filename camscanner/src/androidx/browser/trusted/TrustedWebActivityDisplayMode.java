// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import android.os.BaseBundle;
import androidx.annotation.NonNull;
import android.os.Bundle;

public interface TrustedWebActivityDisplayMode
{
    public static final String KEY_ID = "androidx.browser.trusted.displaymode.KEY_ID";
    
    @NonNull
    Bundle toBundle();
    
    public static class DefaultMode implements TrustedWebActivityDisplayMode
    {
        private static final int ID = 0;
        
        @NonNull
        @Override
        public Bundle toBundle() {
            final Bundle bundle = new Bundle();
            ((BaseBundle)bundle).putInt("androidx.browser.trusted.displaymode.KEY_ID", 0);
            return bundle;
        }
    }
    
    public static class ImmersiveMode implements TrustedWebActivityDisplayMode
    {
        private static final int ID = 1;
        public static final String KEY_CUTOUT_MODE = "androidx.browser.trusted.displaymode.KEY_CUTOUT_MODE";
        public static final String KEY_STICKY = "androidx.browser.trusted.displaymode.KEY_STICKY";
        private final boolean mIsSticky;
        private final int mLayoutInDisplayCutoutMode;
        
        public ImmersiveMode(final boolean mIsSticky, final int mLayoutInDisplayCutoutMode) {
            this.mIsSticky = mIsSticky;
            this.mLayoutInDisplayCutoutMode = mLayoutInDisplayCutoutMode;
        }
        
        @NonNull
        static TrustedWebActivityDisplayMode fromBundle(@NonNull final Bundle bundle) {
            return new ImmersiveMode(bundle.getBoolean("androidx.browser.trusted.displaymode.KEY_STICKY"), ((BaseBundle)bundle).getInt("androidx.browser.trusted.displaymode.KEY_CUTOUT_MODE"));
        }
        
        public boolean isSticky() {
            return this.mIsSticky;
        }
        
        public int layoutInDisplayCutoutMode() {
            return this.mLayoutInDisplayCutoutMode;
        }
        
        @NonNull
        @Override
        public Bundle toBundle() {
            final Bundle bundle = new Bundle();
            ((BaseBundle)bundle).putInt("androidx.browser.trusted.displaymode.KEY_ID", 1);
            bundle.putBoolean("androidx.browser.trusted.displaymode.KEY_STICKY", this.mIsSticky);
            ((BaseBundle)bundle).putInt("androidx.browser.trusted.displaymode.KEY_CUTOUT_MODE", this.mLayoutInDisplayCutoutMode);
            return bundle;
        }
    }
}

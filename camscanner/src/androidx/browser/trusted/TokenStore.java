// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import androidx.annotation.WorkerThread;
import androidx.annotation.Nullable;
import androidx.annotation.BinderThread;

public interface TokenStore
{
    @BinderThread
    @Nullable
    Token load();
    
    @WorkerThread
    void store(@Nullable final Token p0);
}

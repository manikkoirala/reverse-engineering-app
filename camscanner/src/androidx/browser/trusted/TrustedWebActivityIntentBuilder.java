// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import androidx.annotation.ColorInt;
import androidx.browser.customtabs.CustomTabColorSchemeParams;
import android.content.Intent;
import java.util.Collections;
import java.io.Serializable;
import java.util.Collection;
import java.util.ArrayList;
import androidx.browser.customtabs.CustomTabsSession;
import android.net.Uri;
import android.os.Bundle;
import androidx.browser.trusted.sharing.ShareTarget;
import androidx.browser.trusted.sharing.ShareData;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;
import android.annotation.SuppressLint;

public class TrustedWebActivityIntentBuilder
{
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_ADDITIONAL_TRUSTED_ORIGINS = "android.support.customtabs.extra.ADDITIONAL_TRUSTED_ORIGINS";
    public static final String EXTRA_DISPLAY_MODE = "androidx.browser.trusted.extra.DISPLAY_MODE";
    public static final String EXTRA_SCREEN_ORIENTATION = "androidx.browser.trusted.extra.SCREEN_ORIENTATION";
    public static final String EXTRA_SHARE_DATA = "androidx.browser.trusted.extra.SHARE_DATA";
    public static final String EXTRA_SHARE_TARGET = "androidx.browser.trusted.extra.SHARE_TARGET";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_SPLASH_SCREEN_PARAMS = "androidx.browser.trusted.EXTRA_SPLASH_SCREEN_PARAMS";
    @Nullable
    private List<String> mAdditionalTrustedOrigins;
    @NonNull
    private TrustedWebActivityDisplayMode mDisplayMode;
    @NonNull
    private final CustomTabsIntent.Builder mIntentBuilder;
    private int mScreenOrientation;
    @Nullable
    private ShareData mShareData;
    @Nullable
    private ShareTarget mShareTarget;
    @Nullable
    private Bundle mSplashScreenParams;
    @NonNull
    private final Uri mUri;
    
    public TrustedWebActivityIntentBuilder(@NonNull final Uri mUri) {
        this.mIntentBuilder = new CustomTabsIntent.Builder();
        this.mDisplayMode = new TrustedWebActivityDisplayMode.DefaultMode();
        this.mScreenOrientation = 0;
        this.mUri = mUri;
    }
    
    @NonNull
    public TrustedWebActivityIntent build(@NonNull final CustomTabsSession session) {
        if (session != null) {
            this.mIntentBuilder.setSession(session);
            final Intent intent = this.mIntentBuilder.build().intent;
            intent.setData(this.mUri);
            intent.putExtra("android.support.customtabs.extra.LAUNCH_AS_TRUSTED_WEB_ACTIVITY", true);
            if (this.mAdditionalTrustedOrigins != null) {
                intent.putExtra("android.support.customtabs.extra.ADDITIONAL_TRUSTED_ORIGINS", (Serializable)new ArrayList<Object>(this.mAdditionalTrustedOrigins));
            }
            final Bundle mSplashScreenParams = this.mSplashScreenParams;
            if (mSplashScreenParams != null) {
                intent.putExtra("androidx.browser.trusted.EXTRA_SPLASH_SCREEN_PARAMS", mSplashScreenParams);
            }
            final List<Object> emptyList = (List<Object>)Collections.emptyList();
            final ShareTarget mShareTarget = this.mShareTarget;
            List<Object> list = emptyList;
            if (mShareTarget != null) {
                list = emptyList;
                if (this.mShareData != null) {
                    intent.putExtra("androidx.browser.trusted.extra.SHARE_TARGET", mShareTarget.toBundle());
                    intent.putExtra("androidx.browser.trusted.extra.SHARE_DATA", this.mShareData.toBundle());
                    final List<Uri> uris = this.mShareData.uris;
                    list = emptyList;
                    if (uris != null) {
                        list = (List<Object>)uris;
                    }
                }
            }
            intent.putExtra("androidx.browser.trusted.extra.DISPLAY_MODE", this.mDisplayMode.toBundle());
            intent.putExtra("androidx.browser.trusted.extra.SCREEN_ORIENTATION", this.mScreenOrientation);
            return new TrustedWebActivityIntent(intent, (List<Uri>)list);
        }
        throw new NullPointerException("CustomTabsSession is required for launching a TWA");
    }
    
    @NonNull
    public CustomTabsIntent buildCustomTabsIntent() {
        return this.mIntentBuilder.build();
    }
    
    @NonNull
    public TrustedWebActivityDisplayMode getDisplayMode() {
        return this.mDisplayMode;
    }
    
    @NonNull
    public Uri getUri() {
        return this.mUri;
    }
    
    @NonNull
    public TrustedWebActivityIntentBuilder setAdditionalTrustedOrigins(@NonNull final List<String> mAdditionalTrustedOrigins) {
        this.mAdditionalTrustedOrigins = mAdditionalTrustedOrigins;
        return this;
    }
    
    @NonNull
    public TrustedWebActivityIntentBuilder setColorScheme(final int colorScheme) {
        this.mIntentBuilder.setColorScheme(colorScheme);
        return this;
    }
    
    @NonNull
    public TrustedWebActivityIntentBuilder setColorSchemeParams(final int n, @NonNull final CustomTabColorSchemeParams customTabColorSchemeParams) {
        this.mIntentBuilder.setColorSchemeParams(n, customTabColorSchemeParams);
        return this;
    }
    
    @NonNull
    public TrustedWebActivityIntentBuilder setDefaultColorSchemeParams(@NonNull final CustomTabColorSchemeParams defaultColorSchemeParams) {
        this.mIntentBuilder.setDefaultColorSchemeParams(defaultColorSchemeParams);
        return this;
    }
    
    @NonNull
    public TrustedWebActivityIntentBuilder setDisplayMode(@NonNull final TrustedWebActivityDisplayMode mDisplayMode) {
        this.mDisplayMode = mDisplayMode;
        return this;
    }
    
    @NonNull
    public TrustedWebActivityIntentBuilder setNavigationBarColor(@ColorInt final int navigationBarColor) {
        this.mIntentBuilder.setNavigationBarColor(navigationBarColor);
        return this;
    }
    
    @NonNull
    public TrustedWebActivityIntentBuilder setNavigationBarDividerColor(@ColorInt final int navigationBarDividerColor) {
        this.mIntentBuilder.setNavigationBarDividerColor(navigationBarDividerColor);
        return this;
    }
    
    @NonNull
    public TrustedWebActivityIntentBuilder setScreenOrientation(final int mScreenOrientation) {
        this.mScreenOrientation = mScreenOrientation;
        return this;
    }
    
    @NonNull
    public TrustedWebActivityIntentBuilder setShareParams(@NonNull final ShareTarget mShareTarget, @NonNull final ShareData mShareData) {
        this.mShareTarget = mShareTarget;
        this.mShareData = mShareData;
        return this;
    }
    
    @NonNull
    public TrustedWebActivityIntentBuilder setSplashScreenParams(@NonNull final Bundle mSplashScreenParams) {
        this.mSplashScreenParams = mSplashScreenParams;
        return this;
    }
    
    @NonNull
    public TrustedWebActivityIntentBuilder setToolbarColor(@ColorInt final int toolbarColor) {
        this.mIntentBuilder.setToolbarColor(toolbarColor);
        return this;
    }
}

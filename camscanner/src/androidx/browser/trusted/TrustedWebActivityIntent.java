// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import android.os.Bundle;
import androidx.core.content.ContextCompat;
import java.util.Iterator;
import android.content.Context;
import android.net.Uri;
import java.util.List;
import androidx.annotation.NonNull;
import android.content.Intent;

public final class TrustedWebActivityIntent
{
    @NonNull
    private final Intent mIntent;
    @NonNull
    private final List<Uri> mSharedFileUris;
    
    TrustedWebActivityIntent(@NonNull final Intent mIntent, @NonNull final List<Uri> mSharedFileUris) {
        this.mIntent = mIntent;
        this.mSharedFileUris = mSharedFileUris;
    }
    
    private void grantUriPermissionToProvider(final Context context) {
        final Iterator<Uri> iterator = this.mSharedFileUris.iterator();
        while (iterator.hasNext()) {
            context.grantUriPermission(this.mIntent.getPackage(), (Uri)iterator.next(), 1);
        }
    }
    
    @NonNull
    public Intent getIntent() {
        return this.mIntent;
    }
    
    public void launchTrustedWebActivity(@NonNull final Context context) {
        this.grantUriPermissionToProvider(context);
        ContextCompat.startActivity(context, this.mIntent, null);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted;

import android.annotation.SuppressLint;
import android.content.pm.SigningInfo;
import android.content.pm.PackageInfo;
import java.util.ArrayList;
import androidx.annotation.RequiresApi;
import java.io.IOException;
import android.os.Build$VERSION;
import android.content.pm.PackageManager$NameNotFoundException;
import java.util.List;
import android.content.pm.PackageManager;
import androidx.annotation.Nullable;
import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;
import android.content.pm.Signature;

class PackageIdentityUtils
{
    private static final String TAG = "PackageIdentity";
    
    private PackageIdentityUtils() {
    }
    
    @Nullable
    static byte[] getCertificateSHA256Fingerprint(final Signature signature) {
        try {
            return MessageDigest.getInstance("SHA256").digest(signature.toByteArray());
        }
        catch (final NoSuchAlgorithmException ex) {
            return null;
        }
    }
    
    @Nullable
    static List<byte[]> getFingerprintsForPackage(final String s, final PackageManager packageManager) {
        try {
            return getImpl().getFingerprintsForPackage(s, packageManager);
        }
        catch (final PackageManager$NameNotFoundException ex) {
            return null;
        }
    }
    
    private static SignaturesCompat getImpl() {
        if (Build$VERSION.SDK_INT >= 28) {
            return (SignaturesCompat)new Api28Implementation();
        }
        return (SignaturesCompat)new Pre28Implementation();
    }
    
    static boolean packageMatchesToken(final String s, final PackageManager packageManager, final TokenContents tokenContents) {
        try {
            return getImpl().packageMatchesToken(s, packageManager, tokenContents);
        }
        catch (final IOException | PackageManager$NameNotFoundException ex) {
            return false;
        }
    }
    
    @RequiresApi(28)
    static class Api28Implementation implements SignaturesCompat
    {
        @Nullable
        @Override
        public List<byte[]> getFingerprintsForPackage(final String s, final PackageManager packageManager) throws PackageManager$NameNotFoundException {
            final PackageInfo packageInfo = packageManager.getPackageInfo(s, 134217728);
            final ArrayList list = new ArrayList();
            final SigningInfo \u3007080 = oO80.\u3007080(packageInfo);
            final boolean \u300781 = \u300780\u3007808\u3007O.\u3007080(\u3007080);
            int i = 0;
            if (\u300781) {
                for (Signature[] \u300782 = OO0o\u3007\u3007\u3007\u30070.\u3007080(\u3007080); i < \u300782.length; ++i) {
                    list.add(PackageIdentityUtils.getCertificateSHA256Fingerprint(\u300782[i]));
                }
            }
            else {
                list.add(PackageIdentityUtils.getCertificateSHA256Fingerprint(\u30078o8o\u3007.\u3007080(\u3007080)[0]));
            }
            return list;
        }
        
        @Override
        public boolean packageMatchesToken(final String anObject, final PackageManager packageManager, final TokenContents tokenContents) throws PackageManager$NameNotFoundException, IOException {
            if (!tokenContents.getPackageName().equals(anObject)) {
                return false;
            }
            final List<byte[]> fingerprintsForPackage = this.getFingerprintsForPackage(anObject, packageManager);
            if (fingerprintsForPackage == null) {
                return false;
            }
            if (fingerprintsForPackage.size() == 1) {
                return \u3007O8o08O.\u3007080(packageManager, anObject, tokenContents.getFingerprint(0), 1);
            }
            return tokenContents.equals(TokenContents.create(anObject, fingerprintsForPackage));
        }
    }
    
    static class Pre28Implementation implements SignaturesCompat
    {
        @SuppressLint({ "PackageManagerGetSignatures" })
        @Nullable
        @Override
        public List<byte[]> getFingerprintsForPackage(final String s, final PackageManager packageManager) throws PackageManager$NameNotFoundException {
            final PackageInfo packageInfo = packageManager.getPackageInfo(s, 64);
            final ArrayList list = new ArrayList(packageInfo.signatures.length);
            final Signature[] signatures = packageInfo.signatures;
            for (int length = signatures.length, i = 0; i < length; ++i) {
                final byte[] certificateSHA256Fingerprint = PackageIdentityUtils.getCertificateSHA256Fingerprint(signatures[i]);
                if (certificateSHA256Fingerprint == null) {
                    return null;
                }
                list.add((Object)certificateSHA256Fingerprint);
            }
            return (List<byte[]>)list;
        }
        
        @Override
        public boolean packageMatchesToken(final String s, final PackageManager packageManager, final TokenContents tokenContents) throws IOException, PackageManager$NameNotFoundException {
            if (!s.equals(tokenContents.getPackageName())) {
                return false;
            }
            final List<byte[]> fingerprintsForPackage = this.getFingerprintsForPackage(s, packageManager);
            return fingerprintsForPackage != null && tokenContents.equals(TokenContents.create(s, fingerprintsForPackage));
        }
    }
    
    interface SignaturesCompat
    {
        @Nullable
        List<byte[]> getFingerprintsForPackage(final String p0, final PackageManager p1) throws PackageManager$NameNotFoundException;
        
        boolean packageMatchesToken(final String p0, final PackageManager p1, final TokenContents p2) throws IOException, PackageManager$NameNotFoundException;
    }
}

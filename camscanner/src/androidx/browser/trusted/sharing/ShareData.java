// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.trusted.sharing;

import android.os.BaseBundle;
import java.util.Collection;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import android.os.Bundle;
import android.net.Uri;
import java.util.List;
import androidx.annotation.Nullable;

public final class ShareData
{
    public static final String KEY_TEXT = "androidx.browser.trusted.sharing.KEY_TEXT";
    public static final String KEY_TITLE = "androidx.browser.trusted.sharing.KEY_TITLE";
    public static final String KEY_URIS = "androidx.browser.trusted.sharing.KEY_URIS";
    @Nullable
    public final String text;
    @Nullable
    public final String title;
    @Nullable
    public final List<Uri> uris;
    
    public ShareData(@Nullable final String title, @Nullable final String text, @Nullable final List<Uri> uris) {
        this.title = title;
        this.text = text;
        this.uris = uris;
    }
    
    @NonNull
    public static ShareData fromBundle(@NonNull final Bundle bundle) {
        return new ShareData(((BaseBundle)bundle).getString("androidx.browser.trusted.sharing.KEY_TITLE"), ((BaseBundle)bundle).getString("androidx.browser.trusted.sharing.KEY_TEXT"), bundle.getParcelableArrayList("androidx.browser.trusted.sharing.KEY_URIS"));
    }
    
    @NonNull
    public Bundle toBundle() {
        final Bundle bundle = new Bundle();
        ((BaseBundle)bundle).putString("androidx.browser.trusted.sharing.KEY_TITLE", this.title);
        ((BaseBundle)bundle).putString("androidx.browser.trusted.sharing.KEY_TEXT", this.text);
        if (this.uris != null) {
            bundle.putParcelableArrayList("androidx.browser.trusted.sharing.KEY_URIS", new ArrayList((Collection<? extends E>)this.uris));
        }
        return bundle;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.customtabs;

import android.os.BaseBundle;
import android.os.IInterface;
import androidx.annotation.RestrictTo;
import android.widget.RemoteViews;
import android.graphics.Bitmap;
import android.os.RemoteException;
import java.util.List;
import android.net.Uri;
import android.os.IBinder;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.NonNull;
import android.os.Parcelable;
import android.os.Bundle;
import android.support.customtabs.ICustomTabsService;
import androidx.annotation.Nullable;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.support.customtabs.ICustomTabsCallback;

public final class CustomTabsSession
{
    private static final String TAG = "CustomTabsSession";
    private final ICustomTabsCallback mCallback;
    private final ComponentName mComponentName;
    @Nullable
    private final PendingIntent mId;
    private final Object mLock;
    private final ICustomTabsService mService;
    
    CustomTabsSession(final ICustomTabsService mService, final ICustomTabsCallback mCallback, final ComponentName mComponentName, @Nullable final PendingIntent mId) {
        this.mLock = new Object();
        this.mService = mService;
        this.mCallback = mCallback;
        this.mComponentName = mComponentName;
        this.mId = mId;
    }
    
    private void addIdToBundle(final Bundle bundle) {
        final PendingIntent mId = this.mId;
        if (mId != null) {
            bundle.putParcelable("android.support.customtabs.extra.SESSION_ID", (Parcelable)mId);
        }
    }
    
    private Bundle createBundleWithId(@Nullable final Bundle bundle) {
        final Bundle bundle2 = new Bundle();
        if (bundle != null) {
            bundle2.putAll(bundle);
        }
        this.addIdToBundle(bundle2);
        return bundle2;
    }
    
    @NonNull
    @VisibleForTesting
    public static CustomTabsSession createMockSessionForTesting(@NonNull final ComponentName componentName) {
        return new CustomTabsSession(new MockSession(), new CustomTabsSessionToken.MockCallback(), componentName, null);
    }
    
    IBinder getBinder() {
        return ((IInterface)this.mCallback).asBinder();
    }
    
    ComponentName getComponentName() {
        return this.mComponentName;
    }
    
    @Nullable
    PendingIntent getId() {
        return this.mId;
    }
    
    public boolean mayLaunchUrl(@Nullable final Uri uri, @Nullable Bundle bundleWithId, @Nullable final List<Bundle> list) {
        bundleWithId = this.createBundleWithId(bundleWithId);
        try {
            return this.mService.mayLaunchUrl(this.mCallback, uri, bundleWithId, list);
        }
        catch (final RemoteException ex) {
            return false;
        }
    }
    
    public int postMessage(@NonNull final String s, @Nullable Bundle mLock) {
        final Bundle bundleWithId = this.createBundleWithId(mLock);
        mLock = (Bundle)this.mLock;
        monitorenter(mLock);
        try {
            try {
                final int postMessage = this.mService.postMessage(this.mCallback, s, bundleWithId);
                monitorexit(mLock);
                return postMessage;
            }
            finally {
                monitorexit(mLock);
            }
        }
        catch (final RemoteException ex) {}
    }
    
    public boolean receiveFile(@NonNull final Uri uri, final int n, @Nullable Bundle bundleWithId) {
        bundleWithId = this.createBundleWithId(bundleWithId);
        try {
            return this.mService.receiveFile(this.mCallback, uri, n, bundleWithId);
        }
        catch (final RemoteException ex) {
            return false;
        }
    }
    
    public boolean requestPostMessageChannel(@NonNull final Uri uri) {
        try {
            if (this.mId != null) {
                return this.mService.requestPostMessageChannelWithExtras(this.mCallback, uri, this.createBundleWithId(null));
            }
            return this.mService.requestPostMessageChannel(this.mCallback, uri);
        }
        catch (final RemoteException ex) {
            return false;
        }
    }
    
    public boolean setActionButton(@NonNull final Bitmap bitmap, @NonNull final String s) {
        final Bundle bundle = new Bundle();
        bundle.putParcelable("android.support.customtabs.customaction.ICON", (Parcelable)bitmap);
        ((BaseBundle)bundle).putString("android.support.customtabs.customaction.DESCRIPTION", s);
        final Bundle bundle2 = new Bundle();
        bundle2.putBundle("android.support.customtabs.extra.ACTION_BUTTON_BUNDLE", bundle);
        this.addIdToBundle(bundle);
        try {
            return this.mService.updateVisuals(this.mCallback, bundle2);
        }
        catch (final RemoteException ex) {
            return false;
        }
    }
    
    public boolean setSecondaryToolbarViews(@Nullable final RemoteViews remoteViews, @Nullable final int[] array, @Nullable final PendingIntent pendingIntent) {
        final Bundle bundle = new Bundle();
        bundle.putParcelable("android.support.customtabs.extra.EXTRA_REMOTEVIEWS", (Parcelable)remoteViews);
        ((BaseBundle)bundle).putIntArray("android.support.customtabs.extra.EXTRA_REMOTEVIEWS_VIEW_IDS", array);
        bundle.putParcelable("android.support.customtabs.extra.EXTRA_REMOTEVIEWS_PENDINGINTENT", (Parcelable)pendingIntent);
        this.addIdToBundle(bundle);
        try {
            return this.mService.updateVisuals(this.mCallback, bundle);
        }
        catch (final RemoteException ex) {
            return false;
        }
    }
    
    @Deprecated
    public boolean setToolbarItem(final int n, @NonNull final Bitmap bitmap, @NonNull final String s) {
        final Bundle bundle = new Bundle();
        ((BaseBundle)bundle).putInt("android.support.customtabs.customaction.ID", n);
        bundle.putParcelable("android.support.customtabs.customaction.ICON", (Parcelable)bitmap);
        ((BaseBundle)bundle).putString("android.support.customtabs.customaction.DESCRIPTION", s);
        final Bundle bundle2 = new Bundle();
        bundle2.putBundle("android.support.customtabs.extra.ACTION_BUTTON_BUNDLE", bundle);
        this.addIdToBundle(bundle2);
        try {
            return this.mService.updateVisuals(this.mCallback, bundle2);
        }
        catch (final RemoteException ex) {
            return false;
        }
    }
    
    public boolean validateRelationship(final int n, @NonNull final Uri uri, @Nullable Bundle bundleWithId) {
        if (n < 1) {
            return false;
        }
        if (n > 2) {
            return false;
        }
        bundleWithId = this.createBundleWithId(bundleWithId);
        try {
            return this.mService.validateRelationship(this.mCallback, n, uri, bundleWithId);
        }
        catch (final RemoteException ex) {
            return false;
        }
    }
    
    static class MockSession extends Stub
    {
        public Bundle extraCommand(final String s, final Bundle bundle) throws RemoteException {
            return null;
        }
        
        public boolean mayLaunchUrl(final ICustomTabsCallback customTabsCallback, final Uri uri, final Bundle bundle, final List<Bundle> list) throws RemoteException {
            return false;
        }
        
        public boolean newSession(final ICustomTabsCallback customTabsCallback) throws RemoteException {
            return false;
        }
        
        public boolean newSessionWithExtras(final ICustomTabsCallback customTabsCallback, final Bundle bundle) throws RemoteException {
            return false;
        }
        
        public int postMessage(final ICustomTabsCallback customTabsCallback, final String s, final Bundle bundle) throws RemoteException {
            return 0;
        }
        
        public boolean receiveFile(final ICustomTabsCallback customTabsCallback, final Uri uri, final int n, final Bundle bundle) throws RemoteException {
            return false;
        }
        
        public boolean requestPostMessageChannel(final ICustomTabsCallback customTabsCallback, final Uri uri) throws RemoteException {
            return false;
        }
        
        public boolean requestPostMessageChannelWithExtras(final ICustomTabsCallback customTabsCallback, final Uri uri, final Bundle bundle) throws RemoteException {
            return false;
        }
        
        public boolean updateVisuals(final ICustomTabsCallback customTabsCallback, final Bundle bundle) throws RemoteException {
            return false;
        }
        
        public boolean validateRelationship(final ICustomTabsCallback customTabsCallback, final int n, final Uri uri, final Bundle bundle) throws RemoteException {
            return false;
        }
        
        public boolean warmup(final long n) throws RemoteException {
            return false;
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static class PendingSession
    {
        @Nullable
        private final CustomTabsCallback mCallback;
        @Nullable
        private final PendingIntent mId;
        
        PendingSession(@Nullable final CustomTabsCallback mCallback, @Nullable final PendingIntent mId) {
            this.mCallback = mCallback;
            this.mId = mId;
        }
        
        @Nullable
        CustomTabsCallback getCallback() {
            return this.mCallback;
        }
        
        @Nullable
        PendingIntent getId() {
            return this.mId;
        }
    }
}

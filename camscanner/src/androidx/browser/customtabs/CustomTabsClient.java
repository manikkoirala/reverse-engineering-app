// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.customtabs;

import android.os.Parcelable;
import androidx.annotation.RestrictTo;
import java.util.Iterator;
import android.content.pm.ResolveInfo;
import android.content.pm.PackageManager;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import android.app.PendingIntent;
import android.net.Uri;
import android.os.RemoteException;
import android.os.Bundle;
import android.os.Looper;
import android.os.Handler;
import android.support.customtabs.ICustomTabsCallback;
import android.content.ServiceConnection;
import android.text.TextUtils;
import android.content.Intent;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.content.ComponentName;
import android.support.customtabs.ICustomTabsService;
import android.content.Context;

public class CustomTabsClient
{
    private static final String TAG = "CustomTabsClient";
    private final Context mApplicationContext;
    private final ICustomTabsService mService;
    private final ComponentName mServiceComponentName;
    
    CustomTabsClient(final ICustomTabsService mService, final ComponentName mServiceComponentName, final Context mApplicationContext) {
        this.mService = mService;
        this.mServiceComponentName = mServiceComponentName;
        this.mApplicationContext = mApplicationContext;
    }
    
    public static boolean bindCustomTabsService(@NonNull final Context context, @Nullable final String package1, @NonNull final CustomTabsServiceConnection customTabsServiceConnection) {
        customTabsServiceConnection.setApplicationContext(context.getApplicationContext());
        final Intent intent = new Intent("android.support.customtabs.action.CustomTabsService");
        if (!TextUtils.isEmpty((CharSequence)package1)) {
            intent.setPackage(package1);
        }
        return context.bindService(intent, (ServiceConnection)customTabsServiceConnection, 33);
    }
    
    public static boolean bindCustomTabsServicePreservePriority(@NonNull final Context context, @Nullable final String package1, @NonNull final CustomTabsServiceConnection customTabsServiceConnection) {
        customTabsServiceConnection.setApplicationContext(context.getApplicationContext());
        final Intent intent = new Intent("android.support.customtabs.action.CustomTabsService");
        if (!TextUtils.isEmpty((CharSequence)package1)) {
            intent.setPackage(package1);
        }
        return context.bindService(intent, (ServiceConnection)customTabsServiceConnection, 1);
    }
    
    public static boolean connectAndInitialize(@NonNull Context applicationContext, @NonNull final String s) {
        if (s == null) {
            return false;
        }
        applicationContext = applicationContext.getApplicationContext();
        final CustomTabsServiceConnection customTabsServiceConnection = new CustomTabsServiceConnection(applicationContext) {
            final Context val$applicationContext;
            
            @Override
            public final void onCustomTabsServiceConnected(@NonNull final ComponentName componentName, @NonNull final CustomTabsClient customTabsClient) {
                customTabsClient.warmup(0L);
                this.val$applicationContext.unbindService((ServiceConnection)this);
            }
            
            public void onServiceDisconnected(final ComponentName componentName) {
            }
        };
        try {
            return bindCustomTabsService(applicationContext, s, customTabsServiceConnection);
        }
        catch (final SecurityException ex) {
            return false;
        }
    }
    
    private ICustomTabsCallback.Stub createCallbackWrapper(@Nullable final CustomTabsCallback customTabsCallback) {
        return new ICustomTabsCallback.Stub(this, customTabsCallback) {
            private Handler mHandler = new Handler(Looper.getMainLooper());
            final CustomTabsClient this$0;
            final CustomTabsCallback val$callback;
            
            public void extraCallback(final String s, final Bundle bundle) throws RemoteException {
                if (this.val$callback == null) {
                    return;
                }
                this.mHandler.post((Runnable)new Runnable(this, s, bundle) {
                    final CustomTabsClient$2 this$1;
                    final Bundle val$args;
                    final String val$callbackName;
                    
                    @Override
                    public void run() {
                        this.this$1.val$callback.extraCallback(this.val$callbackName, this.val$args);
                    }
                });
            }
            
            public Bundle extraCallbackWithResult(@NonNull final String s, @Nullable final Bundle bundle) throws RemoteException {
                final CustomTabsCallback val$callback = this.val$callback;
                if (val$callback == null) {
                    return null;
                }
                return val$callback.extraCallbackWithResult(s, bundle);
            }
            
            public void onMessageChannelReady(final Bundle bundle) throws RemoteException {
                if (this.val$callback == null) {
                    return;
                }
                this.mHandler.post((Runnable)new Runnable(this, bundle) {
                    final CustomTabsClient$2 this$1;
                    final Bundle val$extras;
                    
                    @Override
                    public void run() {
                        this.this$1.val$callback.onMessageChannelReady(this.val$extras);
                    }
                });
            }
            
            public void onNavigationEvent(final int n, final Bundle bundle) {
                if (this.val$callback == null) {
                    return;
                }
                this.mHandler.post((Runnable)new Runnable(this, n, bundle) {
                    final CustomTabsClient$2 this$1;
                    final Bundle val$extras;
                    final int val$navigationEvent;
                    
                    @Override
                    public void run() {
                        this.this$1.val$callback.onNavigationEvent(this.val$navigationEvent, this.val$extras);
                    }
                });
            }
            
            public void onPostMessage(final String s, final Bundle bundle) throws RemoteException {
                if (this.val$callback == null) {
                    return;
                }
                this.mHandler.post((Runnable)new Runnable(this, s, bundle) {
                    final CustomTabsClient$2 this$1;
                    final Bundle val$extras;
                    final String val$message;
                    
                    @Override
                    public void run() {
                        this.this$1.val$callback.onPostMessage(this.val$message, this.val$extras);
                    }
                });
            }
            
            public void onRelationshipValidationResult(final int n, final Uri uri, final boolean b, @Nullable final Bundle bundle) throws RemoteException {
                if (this.val$callback == null) {
                    return;
                }
                this.mHandler.post((Runnable)new Runnable(this, n, uri, b, bundle) {
                    final CustomTabsClient$2 this$1;
                    final Bundle val$extras;
                    final int val$relation;
                    final Uri val$requestedOrigin;
                    final boolean val$result;
                    
                    @Override
                    public void run() {
                        this.this$1.val$callback.onRelationshipValidationResult(this.val$relation, this.val$requestedOrigin, this.val$result, this.val$extras);
                    }
                });
            }
        };
    }
    
    private static PendingIntent createSessionId(final Context context, final int n) {
        return PendingIntent.getActivity(context, n, new Intent(), 67108864);
    }
    
    @Nullable
    public static String getPackageName(@NonNull final Context context, @Nullable final List<String> list) {
        return getPackageName(context, list, false);
    }
    
    @Nullable
    public static String getPackageName(@NonNull final Context context, @Nullable final List<String> list, final boolean b) {
        final PackageManager packageManager = context.getPackageManager();
        ArrayList list2;
        if (list == null) {
            list2 = new ArrayList();
        }
        else {
            list2 = (ArrayList)list;
        }
        final Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("http://"));
        ArrayList list3 = list2;
        if (!b) {
            final ResolveInfo resolveActivity = packageManager.resolveActivity(intent, 0);
            list3 = list2;
            if (resolveActivity != null) {
                final String packageName = resolveActivity.activityInfo.packageName;
                list3 = new ArrayList<String>(list2.size() + 1);
                list3.add(packageName);
                if (list != null) {
                    list3.addAll(list);
                }
            }
        }
        final Intent intent2 = new Intent("android.support.customtabs.action.CustomTabsService");
        for (final String package1 : list3) {
            intent2.setPackage(package1);
            if (packageManager.resolveService(intent2, 0) != null) {
                return package1;
            }
        }
        return null;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static CustomTabsSession.PendingSession newPendingSession(@NonNull final Context context, @Nullable final CustomTabsCallback customTabsCallback, final int n) {
        return new CustomTabsSession.PendingSession(customTabsCallback, createSessionId(context, n));
    }
    
    @Nullable
    private CustomTabsSession newSessionInternal(@Nullable CustomTabsCallback customTabsCallback, @Nullable final PendingIntent pendingIntent) {
        final ICustomTabsCallback.Stub callbackWrapper = this.createCallbackWrapper(customTabsCallback);
        customTabsCallback = null;
        Label_0048: {
            if (pendingIntent == null) {
                break Label_0048;
            }
            try {
                final Bundle bundle = new Bundle();
                bundle.putParcelable("android.support.customtabs.extra.SESSION_ID", (Parcelable)pendingIntent);
                boolean b = this.mService.newSessionWithExtras(callbackWrapper, bundle);
                while (true) {
                    if (!b) {
                        return null;
                    }
                    customTabsCallback = (CustomTabsCallback)new CustomTabsSession(this.mService, callbackWrapper, this.mServiceComponentName, pendingIntent);
                    return (CustomTabsSession)customTabsCallback;
                    b = this.mService.newSession(callbackWrapper);
                    continue;
                }
            }
            catch (final RemoteException ex) {
                return (CustomTabsSession)customTabsCallback;
            }
        }
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public CustomTabsSession attachSession(@NonNull final CustomTabsSession.PendingSession pendingSession) {
        return this.newSessionInternal(pendingSession.getCallback(), pendingSession.getId());
    }
    
    @Nullable
    public Bundle extraCommand(@NonNull final String s, @Nullable final Bundle bundle) {
        try {
            return this.mService.extraCommand(s, bundle);
        }
        catch (final RemoteException ex) {
            return null;
        }
    }
    
    @Nullable
    public CustomTabsSession newSession(@Nullable final CustomTabsCallback customTabsCallback) {
        return this.newSessionInternal(customTabsCallback, null);
    }
    
    @Nullable
    public CustomTabsSession newSession(@Nullable final CustomTabsCallback customTabsCallback, final int n) {
        return this.newSessionInternal(customTabsCallback, createSessionId(this.mApplicationContext, n));
    }
    
    public boolean warmup(final long n) {
        try {
            return this.mService.warmup(n);
        }
        catch (final RemoteException ex) {
            return false;
        }
    }
}

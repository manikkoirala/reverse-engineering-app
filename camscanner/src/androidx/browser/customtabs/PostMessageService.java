// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.customtabs;

import android.os.IBinder;
import android.content.Intent;
import android.os.RemoteException;
import androidx.annotation.Nullable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.support.customtabs.ICustomTabsCallback;
import android.support.customtabs.IPostMessageService;
import android.app.Service;

public class PostMessageService extends Service
{
    private IPostMessageService.Stub mBinder;
    
    public PostMessageService() {
        this.mBinder = new IPostMessageService.Stub() {
            final PostMessageService this$0;
            
            public void onMessageChannelReady(@NonNull final ICustomTabsCallback customTabsCallback, @Nullable final Bundle bundle) throws RemoteException {
                customTabsCallback.onMessageChannelReady(bundle);
            }
            
            public void onPostMessage(@NonNull final ICustomTabsCallback customTabsCallback, @NonNull final String s, @Nullable final Bundle bundle) throws RemoteException {
                customTabsCallback.onPostMessage(s, bundle);
            }
        };
    }
    
    @NonNull
    public IBinder onBind(@Nullable final Intent intent) {
        return (IBinder)this.mBinder;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.customtabs;

import androidx.annotation.Nullable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
public interface PostMessageBackend
{
    void onDisconnectChannel(@NonNull final Context p0);
    
    boolean onNotifyMessageChannelReady(@Nullable final Bundle p0);
    
    boolean onPostMessage(@NonNull final String p0, @Nullable final Bundle p1);
}

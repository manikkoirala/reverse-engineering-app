// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.customtabs;

import android.content.ComponentName;
import android.content.Intent;
import androidx.annotation.RestrictTo;
import android.content.Context;
import android.os.RemoteException;
import android.os.Bundle;
import android.os.IBinder;
import androidx.annotation.NonNull;
import android.support.customtabs.ICustomTabsCallback;
import android.support.customtabs.IPostMessageService;
import androidx.annotation.Nullable;
import android.content.ServiceConnection;

public abstract class PostMessageServiceConnection implements PostMessageBackend, ServiceConnection
{
    private static final String TAG = "PostMessageServConn";
    private final Object mLock;
    private boolean mMessageChannelCreated;
    @Nullable
    private String mPackageName;
    @Nullable
    private IPostMessageService mService;
    private final ICustomTabsCallback mSessionBinder;
    
    public PostMessageServiceConnection(@NonNull final CustomTabsSessionToken customTabsSessionToken) {
        this.mLock = new Object();
        final IBinder callbackBinder = customTabsSessionToken.getCallbackBinder();
        if (callbackBinder != null) {
            this.mSessionBinder = ICustomTabsCallback.Stub.asInterface(callbackBinder);
            return;
        }
        throw new IllegalArgumentException("Provided session must have binder.");
    }
    
    private boolean isBoundToService() {
        return this.mService != null;
    }
    
    private boolean notifyMessageChannelReadyInternal(@Nullable final Bundle bundle) {
        if (this.mService == null) {
            return false;
        }
        final Object mLock = this.mLock;
        monitorenter(mLock);
        try {
            try {
                this.mService.onMessageChannelReady(this.mSessionBinder, bundle);
                monitorexit(mLock);
                return true;
            }
            finally {
                monitorexit(mLock);
            }
        }
        catch (final RemoteException ex) {}
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public boolean bindSessionToPostMessageService(@NonNull final Context context) {
        final String mPackageName = this.mPackageName;
        if (mPackageName != null) {
            return this.bindSessionToPostMessageService(context, mPackageName);
        }
        throw new IllegalStateException("setPackageName must be called before bindSessionToPostMessageService.");
    }
    
    public boolean bindSessionToPostMessageService(@NonNull final Context context, @NonNull final String s) {
        final Intent intent = new Intent();
        intent.setClassName(s, PostMessageService.class.getName());
        return context.bindService(intent, (ServiceConnection)this, 1);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public void cleanup(@NonNull final Context context) {
        if (this.isBoundToService()) {
            this.unbindFromContext(context);
        }
    }
    
    public final boolean notifyMessageChannelReady(@Nullable final Bundle bundle) {
        this.mMessageChannelCreated = true;
        return this.notifyMessageChannelReadyInternal(bundle);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    @Override
    public void onDisconnectChannel(@NonNull final Context context) {
        this.unbindFromContext(context);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    @Override
    public final boolean onNotifyMessageChannelReady(@Nullable final Bundle bundle) {
        return this.notifyMessageChannelReady(bundle);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    @Override
    public final boolean onPostMessage(@NonNull final String s, @Nullable final Bundle bundle) {
        return this.postMessage(s, bundle);
    }
    
    public void onPostMessageServiceConnected() {
        if (this.mMessageChannelCreated) {
            this.notifyMessageChannelReadyInternal(null);
        }
    }
    
    public void onPostMessageServiceDisconnected() {
    }
    
    public final void onServiceConnected(@NonNull final ComponentName componentName, @NonNull final IBinder binder) {
        this.mService = IPostMessageService.Stub.asInterface(binder);
        this.onPostMessageServiceConnected();
    }
    
    public final void onServiceDisconnected(@NonNull final ComponentName componentName) {
        this.mService = null;
        this.onPostMessageServiceDisconnected();
    }
    
    public final boolean postMessage(@NonNull final String s, @Nullable final Bundle bundle) {
        if (this.mService == null) {
            return false;
        }
        final Object mLock = this.mLock;
        monitorenter(mLock);
        try {
            try {
                this.mService.onPostMessage(this.mSessionBinder, s, bundle);
                monitorexit(mLock);
                return true;
            }
            finally {
                monitorexit(mLock);
            }
        }
        catch (final RemoteException ex) {}
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public void setPackageName(@NonNull final String mPackageName) {
        this.mPackageName = mPackageName;
    }
    
    public void unbindFromContext(@NonNull final Context context) {
        if (this.isBoundToService()) {
            context.unbindService((ServiceConnection)this);
            this.mService = null;
        }
    }
}

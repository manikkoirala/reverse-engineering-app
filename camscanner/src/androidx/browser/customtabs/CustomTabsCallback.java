// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.customtabs;

import android.net.Uri;
import androidx.annotation.Nullable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;

public class CustomTabsCallback
{
    public static final int NAVIGATION_ABORTED = 4;
    public static final int NAVIGATION_FAILED = 3;
    public static final int NAVIGATION_FINISHED = 2;
    public static final int NAVIGATION_STARTED = 1;
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final String ONLINE_EXTRAS_KEY = "online";
    public static final int TAB_HIDDEN = 6;
    public static final int TAB_SHOWN = 5;
    
    public void extraCallback(@NonNull final String s, @Nullable final Bundle bundle) {
    }
    
    @Nullable
    public Bundle extraCallbackWithResult(@NonNull final String s, @Nullable final Bundle bundle) {
        return null;
    }
    
    public void onMessageChannelReady(@Nullable final Bundle bundle) {
    }
    
    public void onNavigationEvent(final int n, @Nullable final Bundle bundle) {
    }
    
    public void onPostMessage(@NonNull final String s, @Nullable final Bundle bundle) {
    }
    
    public void onRelationshipValidationResult(final int n, @NonNull final Uri uri, final boolean b, @Nullable final Bundle bundle) {
    }
}

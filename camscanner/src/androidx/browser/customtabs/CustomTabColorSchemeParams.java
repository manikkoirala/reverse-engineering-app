// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.customtabs;

import android.os.BaseBundle;
import androidx.annotation.NonNull;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.annotation.ColorInt;

public final class CustomTabColorSchemeParams
{
    @ColorInt
    @Nullable
    public final Integer navigationBarColor;
    @ColorInt
    @Nullable
    public final Integer navigationBarDividerColor;
    @ColorInt
    @Nullable
    public final Integer secondaryToolbarColor;
    @ColorInt
    @Nullable
    public final Integer toolbarColor;
    
    CustomTabColorSchemeParams(@ColorInt @Nullable final Integer toolbarColor, @ColorInt @Nullable final Integer secondaryToolbarColor, @ColorInt @Nullable final Integer navigationBarColor, @ColorInt @Nullable final Integer navigationBarDividerColor) {
        this.toolbarColor = toolbarColor;
        this.secondaryToolbarColor = secondaryToolbarColor;
        this.navigationBarColor = navigationBarColor;
        this.navigationBarDividerColor = navigationBarDividerColor;
    }
    
    @NonNull
    static CustomTabColorSchemeParams fromBundle(@Nullable final Bundle bundle) {
        Bundle bundle2 = bundle;
        if (bundle == null) {
            bundle2 = new Bundle(0);
        }
        return new CustomTabColorSchemeParams((Integer)((BaseBundle)bundle2).get("android.support.customtabs.extra.TOOLBAR_COLOR"), (Integer)((BaseBundle)bundle2).get("android.support.customtabs.extra.SECONDARY_TOOLBAR_COLOR"), (Integer)((BaseBundle)bundle2).get("androidx.browser.customtabs.extra.NAVIGATION_BAR_COLOR"), (Integer)((BaseBundle)bundle2).get("androidx.browser.customtabs.extra.NAVIGATION_BAR_DIVIDER_COLOR"));
    }
    
    @NonNull
    Bundle toBundle() {
        final Bundle bundle = new Bundle();
        final Integer toolbarColor = this.toolbarColor;
        if (toolbarColor != null) {
            ((BaseBundle)bundle).putInt("android.support.customtabs.extra.TOOLBAR_COLOR", (int)toolbarColor);
        }
        final Integer secondaryToolbarColor = this.secondaryToolbarColor;
        if (secondaryToolbarColor != null) {
            ((BaseBundle)bundle).putInt("android.support.customtabs.extra.SECONDARY_TOOLBAR_COLOR", (int)secondaryToolbarColor);
        }
        final Integer navigationBarColor = this.navigationBarColor;
        if (navigationBarColor != null) {
            ((BaseBundle)bundle).putInt("androidx.browser.customtabs.extra.NAVIGATION_BAR_COLOR", (int)navigationBarColor);
        }
        final Integer navigationBarDividerColor = this.navigationBarDividerColor;
        if (navigationBarDividerColor != null) {
            ((BaseBundle)bundle).putInt("androidx.browser.customtabs.extra.NAVIGATION_BAR_DIVIDER_COLOR", (int)navigationBarDividerColor);
        }
        return bundle;
    }
    
    @NonNull
    CustomTabColorSchemeParams withDefaults(@NonNull final CustomTabColorSchemeParams customTabColorSchemeParams) {
        Integer n;
        if ((n = this.toolbarColor) == null) {
            n = customTabColorSchemeParams.toolbarColor;
        }
        Integer n2;
        if ((n2 = this.secondaryToolbarColor) == null) {
            n2 = customTabColorSchemeParams.secondaryToolbarColor;
        }
        Integer n3;
        if ((n3 = this.navigationBarColor) == null) {
            n3 = customTabColorSchemeParams.navigationBarColor;
        }
        Integer n4;
        if ((n4 = this.navigationBarDividerColor) == null) {
            n4 = customTabColorSchemeParams.navigationBarDividerColor;
        }
        return new CustomTabColorSchemeParams(n, n2, n3, n4);
    }
    
    public static final class Builder
    {
        @ColorInt
        @Nullable
        private Integer mNavigationBarColor;
        @ColorInt
        @Nullable
        private Integer mNavigationBarDividerColor;
        @ColorInt
        @Nullable
        private Integer mSecondaryToolbarColor;
        @ColorInt
        @Nullable
        private Integer mToolbarColor;
        
        @NonNull
        public CustomTabColorSchemeParams build() {
            return new CustomTabColorSchemeParams(this.mToolbarColor, this.mSecondaryToolbarColor, this.mNavigationBarColor, this.mNavigationBarDividerColor);
        }
        
        @NonNull
        public Builder setNavigationBarColor(@ColorInt final int n) {
            this.mNavigationBarColor = (n | 0xFF000000);
            return this;
        }
        
        @NonNull
        public Builder setNavigationBarDividerColor(@ColorInt final int i) {
            this.mNavigationBarDividerColor = i;
            return this;
        }
        
        @NonNull
        public Builder setSecondaryToolbarColor(@ColorInt final int i) {
            this.mSecondaryToolbarColor = i;
            return this;
        }
        
        @NonNull
        public Builder setToolbarColor(@ColorInt final int n) {
            this.mToolbarColor = (n | 0xFF000000);
            return this;
        }
    }
}

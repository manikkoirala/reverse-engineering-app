// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.customtabs;

import android.os.IInterface;
import androidx.annotation.RestrictTo;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.content.Intent;
import java.util.NoSuchElementException;
import java.util.List;
import android.net.Uri;
import android.os.RemoteException;
import androidx.annotation.NonNull;
import android.support.customtabs.ICustomTabsCallback;
import android.app.PendingIntent;
import androidx.annotation.Nullable;
import android.os.Bundle;
import android.os.IBinder$DeathRecipient;
import android.os.IBinder;
import androidx.collection.SimpleArrayMap;
import android.support.customtabs.ICustomTabsService;
import android.app.Service;

public abstract class CustomTabsService extends Service
{
    public static final String ACTION_CUSTOM_TABS_CONNECTION = "android.support.customtabs.action.CustomTabsService";
    public static final String CATEGORY_COLOR_SCHEME_CUSTOMIZATION = "androidx.browser.customtabs.category.ColorSchemeCustomization";
    public static final String CATEGORY_NAVBAR_COLOR_CUSTOMIZATION = "androidx.browser.customtabs.category.NavBarColorCustomization";
    public static final String CATEGORY_TRUSTED_WEB_ACTIVITY_IMMERSIVE_MODE = "androidx.browser.trusted.category.ImmersiveMode";
    public static final String CATEGORY_WEB_SHARE_TARGET_V2 = "androidx.browser.trusted.category.WebShareTargetV2";
    public static final int FILE_PURPOSE_TRUSTED_WEB_ACTIVITY_SPLASH_IMAGE = 1;
    public static final String KEY_SUCCESS = "androidx.browser.customtabs.SUCCESS";
    public static final String KEY_URL = "android.support.customtabs.otherurls.URL";
    public static final int RELATION_HANDLE_ALL_URLS = 2;
    public static final int RELATION_USE_AS_ORIGIN = 1;
    public static final int RESULT_FAILURE_DISALLOWED = -1;
    public static final int RESULT_FAILURE_MESSAGING_ERROR = -3;
    public static final int RESULT_FAILURE_REMOTE_ERROR = -2;
    public static final int RESULT_SUCCESS = 0;
    public static final String TRUSTED_WEB_ACTIVITY_CATEGORY = "androidx.browser.trusted.category.TrustedWebActivities";
    private ICustomTabsService.Stub mBinder;
    final SimpleArrayMap<IBinder, IBinder$DeathRecipient> mDeathRecipientMap;
    
    public CustomTabsService() {
        this.mDeathRecipientMap = new SimpleArrayMap<IBinder, IBinder$DeathRecipient>();
        this.mBinder = new ICustomTabsService.Stub() {
            final CustomTabsService this$0;
            
            @Nullable
            private PendingIntent getSessionIdFromBundle(@Nullable final Bundle bundle) {
                if (bundle == null) {
                    return null;
                }
                final PendingIntent pendingIntent = (PendingIntent)bundle.getParcelable("android.support.customtabs.extra.SESSION_ID");
                bundle.remove("android.support.customtabs.extra.SESSION_ID");
                return pendingIntent;
            }
            
            private boolean newSessionInternal(@NonNull final ICustomTabsCallback customTabsCallback, @Nullable final PendingIntent pendingIntent) {
                final CustomTabsSessionToken customTabsSessionToken = new CustomTabsSessionToken(customTabsCallback, pendingIntent);
                try {
                    final \u3007080 \u3007080 = new \u3007080(this, customTabsSessionToken);
                    synchronized (this.this$0.mDeathRecipientMap) {
                        ((IInterface)customTabsCallback).asBinder().linkToDeath((IBinder$DeathRecipient)\u3007080, 0);
                        this.this$0.mDeathRecipientMap.put(((IInterface)customTabsCallback).asBinder(), (IBinder$DeathRecipient)\u3007080);
                        monitorexit(this.this$0.mDeathRecipientMap);
                        return this.this$0.newSession(customTabsSessionToken);
                    }
                }
                catch (final RemoteException ex) {
                    return false;
                }
            }
            
            public Bundle extraCommand(@NonNull final String s, @Nullable final Bundle bundle) {
                return this.this$0.extraCommand(s, bundle);
            }
            
            public boolean mayLaunchUrl(@Nullable final ICustomTabsCallback customTabsCallback, @Nullable final Uri uri, @Nullable final Bundle bundle, @Nullable final List<Bundle> list) {
                return this.this$0.mayLaunchUrl(new CustomTabsSessionToken(customTabsCallback, this.getSessionIdFromBundle(bundle)), uri, bundle, list);
            }
            
            public boolean newSession(@NonNull final ICustomTabsCallback customTabsCallback) {
                return this.newSessionInternal(customTabsCallback, null);
            }
            
            public boolean newSessionWithExtras(@NonNull final ICustomTabsCallback customTabsCallback, @Nullable final Bundle bundle) {
                return this.newSessionInternal(customTabsCallback, this.getSessionIdFromBundle(bundle));
            }
            
            public int postMessage(@NonNull final ICustomTabsCallback customTabsCallback, @NonNull final String s, @Nullable final Bundle bundle) {
                return this.this$0.postMessage(new CustomTabsSessionToken(customTabsCallback, this.getSessionIdFromBundle(bundle)), s, bundle);
            }
            
            public boolean receiveFile(@NonNull final ICustomTabsCallback customTabsCallback, @NonNull final Uri uri, final int n, @Nullable final Bundle bundle) {
                return this.this$0.receiveFile(new CustomTabsSessionToken(customTabsCallback, this.getSessionIdFromBundle(bundle)), uri, n, bundle);
            }
            
            public boolean requestPostMessageChannel(@NonNull final ICustomTabsCallback customTabsCallback, @NonNull final Uri uri) {
                return this.this$0.requestPostMessageChannel(new CustomTabsSessionToken(customTabsCallback, null), uri);
            }
            
            public boolean requestPostMessageChannelWithExtras(@NonNull final ICustomTabsCallback customTabsCallback, @NonNull final Uri uri, @NonNull final Bundle bundle) {
                return this.this$0.requestPostMessageChannel(new CustomTabsSessionToken(customTabsCallback, this.getSessionIdFromBundle(bundle)), uri);
            }
            
            public boolean updateVisuals(@NonNull final ICustomTabsCallback customTabsCallback, @Nullable final Bundle bundle) {
                return this.this$0.updateVisuals(new CustomTabsSessionToken(customTabsCallback, this.getSessionIdFromBundle(bundle)), bundle);
            }
            
            public boolean validateRelationship(@NonNull final ICustomTabsCallback customTabsCallback, final int n, @NonNull final Uri uri, @Nullable final Bundle bundle) {
                return this.this$0.validateRelationship(new CustomTabsSessionToken(customTabsCallback, this.getSessionIdFromBundle(bundle)), n, uri, bundle);
            }
            
            public boolean warmup(final long n) {
                return this.this$0.warmup(n);
            }
        };
    }
    
    protected boolean cleanUpSession(@NonNull final CustomTabsSessionToken customTabsSessionToken) {
        try {
            synchronized (this.mDeathRecipientMap) {
                final IBinder callbackBinder = customTabsSessionToken.getCallbackBinder();
                if (callbackBinder == null) {
                    return false;
                }
                callbackBinder.unlinkToDeath((IBinder$DeathRecipient)this.mDeathRecipientMap.get(callbackBinder), 0);
                this.mDeathRecipientMap.remove(callbackBinder);
                return true;
            }
        }
        catch (final NoSuchElementException ex) {
            return false;
        }
    }
    
    @Nullable
    protected abstract Bundle extraCommand(@NonNull final String p0, @Nullable final Bundle p1);
    
    protected abstract boolean mayLaunchUrl(@NonNull final CustomTabsSessionToken p0, @Nullable final Uri p1, @Nullable final Bundle p2, @Nullable final List<Bundle> p3);
    
    protected abstract boolean newSession(@NonNull final CustomTabsSessionToken p0);
    
    @NonNull
    public IBinder onBind(@Nullable final Intent intent) {
        return (IBinder)this.mBinder;
    }
    
    protected abstract int postMessage(@NonNull final CustomTabsSessionToken p0, @NonNull final String p1, @Nullable final Bundle p2);
    
    protected abstract boolean receiveFile(@NonNull final CustomTabsSessionToken p0, @NonNull final Uri p1, final int p2, @Nullable final Bundle p3);
    
    protected abstract boolean requestPostMessageChannel(@NonNull final CustomTabsSessionToken p0, @NonNull final Uri p1);
    
    protected abstract boolean updateVisuals(@NonNull final CustomTabsSessionToken p0, @Nullable final Bundle p1);
    
    protected abstract boolean validateRelationship(@NonNull final CustomTabsSessionToken p0, final int p1, @NonNull final Uri p2, @Nullable final Bundle p3);
    
    protected abstract boolean warmup(final long p0);
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface FilePurpose {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface Relation {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface Result {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.customtabs;

import androidx.annotation.WorkerThread;
import androidx.core.content.FileProvider;
import java.io.File;
import android.app.PendingIntent;
import android.os.Parcelable;
import android.os.Bundle;
import androidx.core.app.BundleCompat;
import android.net.Uri;
import android.content.IntentFilter;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.annotation.RestrictTo;

public class TrustedWebUtils
{
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final String ACTION_MANAGE_TRUSTED_WEB_ACTIVITY_DATA = "android.support.customtabs.action.ACTION_MANAGE_TRUSTED_WEB_ACTIVITY_DATA";
    public static final String EXTRA_LAUNCH_AS_TRUSTED_WEB_ACTIVITY = "android.support.customtabs.extra.LAUNCH_AS_TRUSTED_WEB_ACTIVITY";
    
    private TrustedWebUtils() {
    }
    
    public static boolean areSplashScreensSupported(@NonNull final Context context, @NonNull final String package1, @NonNull final String s) {
        final ResolveInfo resolveService = context.getPackageManager().resolveService(new Intent().setAction("android.support.customtabs.action.CustomTabsService").setPackage(package1), 64);
        if (resolveService != null) {
            final IntentFilter filter = resolveService.filter;
            if (filter != null) {
                return filter.hasCategory(s);
            }
        }
        return false;
    }
    
    @Deprecated
    public static void launchAsTrustedWebActivity(@NonNull final Context context, @NonNull final CustomTabsIntent customTabsIntent, @NonNull final Uri uri) {
        if (BundleCompat.getBinder(customTabsIntent.intent.getExtras(), "android.support.customtabs.extra.SESSION") != null) {
            customTabsIntent.intent.putExtra("android.support.customtabs.extra.LAUNCH_AS_TRUSTED_WEB_ACTIVITY", true);
            customTabsIntent.launchUrl(context, uri);
            return;
        }
        throw new IllegalArgumentException("Given CustomTabsIntent should be associated with a valid CustomTabsSession");
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static void launchBrowserSiteSettings(@NonNull final Context context, @NonNull final CustomTabsSession customTabsSession, @NonNull final Uri data) {
        final Intent intent = new Intent("android.support.customtabs.action.ACTION_MANAGE_TRUSTED_WEB_ACTIVITY_DATA");
        intent.setPackage(customTabsSession.getComponentName().getPackageName());
        intent.setData(data);
        final Bundle bundle = new Bundle();
        BundleCompat.putBinder(bundle, "android.support.customtabs.extra.SESSION", customTabsSession.getBinder());
        intent.putExtras(bundle);
        final PendingIntent id = customTabsSession.getId();
        if (id != null) {
            intent.putExtra("android.support.customtabs.extra.SESSION_ID", (Parcelable)id);
        }
        context.startActivity(intent);
    }
    
    @WorkerThread
    public static boolean transferSplashImage(@NonNull final Context context, @NonNull final File file, @NonNull final String s, @NonNull final String s2, @NonNull final CustomTabsSession customTabsSession) {
        final Uri uriForFile = FileProvider.getUriForFile(context, s, file);
        context.grantUriPermission(s2, uriForFile, 1);
        return customTabsSession.receiveFile(uriForFile, 1, null);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.browser.customtabs;

import android.support.customtabs.ICustomTabsService;
import android.os.IBinder;
import androidx.annotation.NonNull;
import android.content.ComponentName;
import androidx.annotation.RestrictTo;
import androidx.annotation.Nullable;
import android.content.Context;
import android.content.ServiceConnection;

public abstract class CustomTabsServiceConnection implements ServiceConnection
{
    @Nullable
    private Context mApplicationContext;
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    Context getApplicationContext() {
        return this.mApplicationContext;
    }
    
    public abstract void onCustomTabsServiceConnected(@NonNull final ComponentName p0, @NonNull final CustomTabsClient p1);
    
    public final void onServiceConnected(@NonNull final ComponentName componentName, @NonNull final IBinder binder) {
        if (this.mApplicationContext != null) {
            this.onCustomTabsServiceConnected(componentName, new CustomTabsClient(this, ICustomTabsService.Stub.asInterface(binder), componentName, this.mApplicationContext) {
                final CustomTabsServiceConnection this$0;
            });
            return;
        }
        throw new IllegalStateException("Custom Tabs Service connected before an applicationcontext has been provided.");
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    void setApplicationContext(@NonNull final Context mApplicationContext) {
        this.mApplicationContext = mApplicationContext;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment;

public final class R
{
    private R() {
    }
    
    public static final class anim
    {
        public static final int fragment_fast_out_extra_slow_in = 2130772011;
        
        private anim() {
        }
    }
    
    public static final class animator
    {
        public static final int fragment_close_enter = 2130837507;
        public static final int fragment_close_exit = 2130837508;
        public static final int fragment_fade_enter = 2130837509;
        public static final int fragment_fade_exit = 2130837510;
        public static final int fragment_open_enter = 2130837511;
        public static final int fragment_open_exit = 2130837512;
        
        private animator() {
        }
    }
    
    public static final class id
    {
        public static final int fragment_container_view_tag = 2131363443;
        public static final int special_effects_controller_view_tag = 2131366162;
        public static final int visible_removing_fragment_view_tag = 2131368581;
        
        private id() {
        }
    }
    
    public static final class styleable
    {
        public static final int[] Fragment;
        public static final int[] FragmentContainerView;
        public static final int FragmentContainerView_android_name = 0;
        public static final int FragmentContainerView_android_tag = 1;
        public static final int Fragment_android_id = 1;
        public static final int Fragment_android_name = 0;
        public static final int Fragment_android_tag = 2;
        
        static {
            Fragment = new int[] { 16842755, 16842960, 16842961 };
            FragmentContainerView = new int[] { 16842755, 16842961 };
        }
        
        private styleable() {
        }
    }
}

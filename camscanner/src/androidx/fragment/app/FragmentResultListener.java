// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.os.Bundle;
import androidx.annotation.NonNull;

public interface FragmentResultListener
{
    void onFragmentResult(@NonNull final String p0, @NonNull final Bundle p1);
}

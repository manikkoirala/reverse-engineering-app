// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import androidx.lifecycle.LifecycleOwner;
import android.os.Bundle;
import androidx.annotation.NonNull;

public interface FragmentResultOwner
{
    void clearFragmentResult(@NonNull final String p0);
    
    void clearFragmentResultListener(@NonNull final String p0);
    
    void setFragmentResult(@NonNull final String p0, @NonNull final Bundle p1);
    
    void setFragmentResultListener(@NonNull final String p0, @NonNull final LifecycleOwner p1, @NonNull final FragmentResultListener p2);
}

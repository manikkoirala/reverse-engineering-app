// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.content.res.TypedArray;
import android.view.View$OnAttachStateChangeListener;
import androidx.fragment.app.strictmode.FragmentStrictMode;
import android.view.ViewGroup;
import androidx.fragment.R;
import android.util.AttributeSet;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
import android.view.LayoutInflater$Factory2;

class FragmentLayoutInflaterFactory implements LayoutInflater$Factory2
{
    private static final String TAG = "FragmentManager";
    final FragmentManager mFragmentManager;
    
    FragmentLayoutInflaterFactory(final FragmentManager mFragmentManager) {
        this.mFragmentManager = mFragmentManager;
    }
    
    @Nullable
    public View onCreateView(@Nullable View mView, @NonNull final String s, @NonNull final Context context, @NonNull final AttributeSet set) {
        if (FragmentContainerView.class.getName().equals(s)) {
            return (View)new FragmentContainerView(context, set, this.mFragmentManager);
        }
        final boolean equals = "fragment".equals(s);
        Fragment fragmentById = null;
        if (!equals) {
            return null;
        }
        final String attributeValue = set.getAttributeValue((String)null, "class");
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.Fragment);
        String string;
        if ((string = attributeValue) == null) {
            string = obtainStyledAttributes.getString(R.styleable.Fragment_android_name);
        }
        final int resourceId = obtainStyledAttributes.getResourceId(R.styleable.Fragment_android_id, -1);
        final String string2 = obtainStyledAttributes.getString(R.styleable.Fragment_android_tag);
        obtainStyledAttributes.recycle();
        if (string == null || !FragmentFactory.isFragmentClass(context.getClassLoader(), string)) {
            return null;
        }
        int id;
        if (mView != null) {
            id = mView.getId();
        }
        else {
            id = 0;
        }
        if (id == -1 && resourceId == -1 && string2 == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append(set.getPositionDescription());
            sb.append(": Must specify unique android:id, android:tag, or have a parent with an id for ");
            sb.append(string);
            throw new IllegalArgumentException(sb.toString());
        }
        if (resourceId != -1) {
            fragmentById = this.mFragmentManager.findFragmentById(resourceId);
        }
        Fragment fragmentByTag;
        if ((fragmentByTag = fragmentById) == null) {
            fragmentByTag = fragmentById;
            if (string2 != null) {
                fragmentByTag = this.mFragmentManager.findFragmentByTag(string2);
            }
        }
        Fragment fragmentById2;
        if ((fragmentById2 = fragmentByTag) == null) {
            fragmentById2 = fragmentByTag;
            if (id != -1) {
                fragmentById2 = this.mFragmentManager.findFragmentById(id);
            }
        }
        Fragment fragment;
        FragmentStateManager fragmentStateManager;
        if (fragmentById2 == null) {
            final Fragment instantiate = this.mFragmentManager.getFragmentFactory().instantiate(context.getClassLoader(), string);
            instantiate.mFromLayout = true;
            int mFragmentId;
            if (resourceId != 0) {
                mFragmentId = resourceId;
            }
            else {
                mFragmentId = id;
            }
            instantiate.mFragmentId = mFragmentId;
            instantiate.mContainerId = id;
            instantiate.mTag = string2;
            instantiate.mInLayout = true;
            final FragmentManager mFragmentManager = this.mFragmentManager;
            instantiate.mFragmentManager = mFragmentManager;
            instantiate.mHost = mFragmentManager.getHost();
            instantiate.onInflate(this.mFragmentManager.getHost().getContext(), set, instantiate.mSavedFragmentState);
            final FragmentStateManager addFragment = this.mFragmentManager.addFragment(instantiate);
            fragment = instantiate;
            fragmentStateManager = addFragment;
            if (FragmentManager.isLoggingEnabled(2)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Fragment ");
                sb2.append(instantiate);
                sb2.append(" has been inflated via the <fragment> tag: id=0x");
                sb2.append(Integer.toHexString(resourceId));
                fragment = instantiate;
                fragmentStateManager = addFragment;
            }
        }
        else {
            if (fragmentById2.mInLayout) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(set.getPositionDescription());
                sb3.append(": Duplicate id 0x");
                sb3.append(Integer.toHexString(resourceId));
                sb3.append(", tag ");
                sb3.append(string2);
                sb3.append(", or parent id 0x");
                sb3.append(Integer.toHexString(id));
                sb3.append(" with another fragment for ");
                sb3.append(string);
                throw new IllegalArgumentException(sb3.toString());
            }
            fragmentById2.mInLayout = true;
            final FragmentManager mFragmentManager2 = this.mFragmentManager;
            fragmentById2.mFragmentManager = mFragmentManager2;
            fragmentById2.mHost = mFragmentManager2.getHost();
            fragmentById2.onInflate(this.mFragmentManager.getHost().getContext(), set, fragmentById2.mSavedFragmentState);
            final FragmentStateManager orGetFragmentStateManager = this.mFragmentManager.createOrGetFragmentStateManager(fragmentById2);
            fragment = fragmentById2;
            fragmentStateManager = orGetFragmentStateManager;
            if (FragmentManager.isLoggingEnabled(2)) {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("Retained Fragment ");
                sb4.append(fragmentById2);
                sb4.append(" has been re-attached via the <fragment> tag: id=0x");
                sb4.append(Integer.toHexString(resourceId));
                fragmentStateManager = orGetFragmentStateManager;
                fragment = fragmentById2;
            }
        }
        final ViewGroup mContainer = (ViewGroup)mView;
        FragmentStrictMode.onFragmentTagUsage(fragment, mContainer);
        fragment.mContainer = mContainer;
        fragmentStateManager.moveToExpectedState();
        fragmentStateManager.ensureInflatedView();
        mView = fragment.mView;
        if (mView != null) {
            if (resourceId != 0) {
                mView.setId(resourceId);
            }
            if (fragment.mView.getTag() == null) {
                fragment.mView.setTag((Object)string2);
            }
            fragment.mView.addOnAttachStateChangeListener((View$OnAttachStateChangeListener)new View$OnAttachStateChangeListener(this, fragmentStateManager) {
                final FragmentLayoutInflaterFactory this$0;
                final FragmentStateManager val$fragmentStateManager;
                
                public void onViewAttachedToWindow(final View view) {
                    final Fragment fragment = this.val$fragmentStateManager.getFragment();
                    this.val$fragmentStateManager.moveToExpectedState();
                    SpecialEffectsController.getOrCreateController((ViewGroup)fragment.mView.getParent(), this.this$0.mFragmentManager).forceCompleteAllOperations();
                }
                
                public void onViewDetachedFromWindow(final View view) {
                }
            });
            return fragment.mView;
        }
        final StringBuilder sb5 = new StringBuilder();
        sb5.append("Fragment ");
        sb5.append(string);
        sb5.append(" did not create a view.");
        throw new IllegalStateException(sb5.toString());
    }
    
    @Nullable
    public View onCreateView(@NonNull final String s, @NonNull final Context context, @NonNull final AttributeSet set) {
        return this.onCreateView(null, s, context, set);
    }
}

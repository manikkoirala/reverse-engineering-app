// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import androidx.annotation.Nullable;
import android.os.Parcelable;
import android.view.View;
import androidx.lifecycle.Lifecycle;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

@Deprecated
public abstract class FragmentPagerAdapter extends PagerAdapter
{
    public static final int BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT = 1;
    @Deprecated
    public static final int BEHAVIOR_SET_USER_VISIBLE_HINT = 0;
    private static final boolean DEBUG = false;
    private static final String TAG = "FragmentPagerAdapter";
    private final int mBehavior;
    private FragmentTransaction mCurTransaction;
    private Fragment mCurrentPrimaryItem;
    private boolean mExecutingFinishUpdate;
    private final FragmentManager mFragmentManager;
    
    @Deprecated
    public FragmentPagerAdapter(@NonNull final FragmentManager fragmentManager) {
        this(fragmentManager, 0);
    }
    
    public FragmentPagerAdapter(@NonNull final FragmentManager mFragmentManager, final int mBehavior) {
        this.mCurTransaction = null;
        this.mCurrentPrimaryItem = null;
        this.mFragmentManager = mFragmentManager;
        this.mBehavior = mBehavior;
    }
    
    private static String makeFragmentName(final int i, final long lng) {
        final StringBuilder sb = new StringBuilder();
        sb.append("android:switcher:");
        sb.append(i);
        sb.append(":");
        sb.append(lng);
        return sb.toString();
    }
    
    @Override
    public void destroyItem(@NonNull final ViewGroup viewGroup, final int n, @NonNull final Object o) {
        final Fragment fragment = (Fragment)o;
        if (this.mCurTransaction == null) {
            this.mCurTransaction = this.mFragmentManager.beginTransaction();
        }
        this.mCurTransaction.detach(fragment);
        if (fragment.equals(this.mCurrentPrimaryItem)) {
            this.mCurrentPrimaryItem = null;
        }
    }
    
    @Override
    public void finishUpdate(@NonNull final ViewGroup viewGroup) {
        final FragmentTransaction mCurTransaction = this.mCurTransaction;
        if (mCurTransaction != null) {
            if (!this.mExecutingFinishUpdate) {
                try {
                    this.mExecutingFinishUpdate = true;
                    mCurTransaction.commitNowAllowingStateLoss();
                }
                finally {
                    this.mExecutingFinishUpdate = false;
                }
            }
            this.mCurTransaction = null;
        }
    }
    
    @NonNull
    public abstract Fragment getItem(final int p0);
    
    public long getItemId(final int n) {
        return n;
    }
    
    @NonNull
    @Override
    public Object instantiateItem(@NonNull final ViewGroup viewGroup, final int n) {
        if (this.mCurTransaction == null) {
            this.mCurTransaction = this.mFragmentManager.beginTransaction();
        }
        final long itemId = this.getItemId(n);
        final Fragment fragmentByTag = this.mFragmentManager.findFragmentByTag(makeFragmentName(((View)viewGroup).getId(), itemId));
        Fragment fragment;
        if (fragmentByTag != null) {
            this.mCurTransaction.attach(fragmentByTag);
            fragment = fragmentByTag;
        }
        else {
            final Fragment item = this.getItem(n);
            this.mCurTransaction.add(((View)viewGroup).getId(), item, makeFragmentName(((View)viewGroup).getId(), itemId));
            fragment = item;
        }
        if (fragment != this.mCurrentPrimaryItem) {
            fragment.setMenuVisibility(false);
            if (this.mBehavior == 1) {
                this.mCurTransaction.setMaxLifecycle(fragment, Lifecycle.State.STARTED);
            }
            else {
                fragment.setUserVisibleHint(false);
            }
        }
        return fragment;
    }
    
    @Override
    public boolean isViewFromObject(@NonNull final View view, @NonNull final Object o) {
        return ((Fragment)o).getView() == view;
    }
    
    @Override
    public void restoreState(@Nullable final Parcelable parcelable, @Nullable final ClassLoader classLoader) {
    }
    
    @Nullable
    @Override
    public Parcelable saveState() {
        return null;
    }
    
    @Override
    public void setPrimaryItem(@NonNull final ViewGroup viewGroup, final int n, @NonNull final Object o) {
        final Fragment mCurrentPrimaryItem = (Fragment)o;
        final Fragment mCurrentPrimaryItem2 = this.mCurrentPrimaryItem;
        if (mCurrentPrimaryItem != mCurrentPrimaryItem2) {
            if (mCurrentPrimaryItem2 != null) {
                mCurrentPrimaryItem2.setMenuVisibility(false);
                if (this.mBehavior == 1) {
                    if (this.mCurTransaction == null) {
                        this.mCurTransaction = this.mFragmentManager.beginTransaction();
                    }
                    this.mCurTransaction.setMaxLifecycle(this.mCurrentPrimaryItem, Lifecycle.State.STARTED);
                }
                else {
                    this.mCurrentPrimaryItem.setUserVisibleHint(false);
                }
            }
            mCurrentPrimaryItem.setMenuVisibility(true);
            if (this.mBehavior == 1) {
                if (this.mCurTransaction == null) {
                    this.mCurTransaction = this.mFragmentManager.beginTransaction();
                }
                this.mCurTransaction.setMaxLifecycle(mCurrentPrimaryItem, Lifecycle.State.RESUMED);
            }
            else {
                mCurrentPrimaryItem.setUserVisibleHint(true);
            }
            this.mCurrentPrimaryItem = mCurrentPrimaryItem;
        }
    }
    
    @Override
    public void startUpdate(@NonNull final ViewGroup viewGroup) {
        if (((View)viewGroup).getId() != -1) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("ViewPager with adapter ");
        sb.append(this);
        sb.append(" requires a view id");
        throw new IllegalStateException(sb.toString());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import androidx.lifecycle.ViewModelStore;
import java.util.Collection;
import androidx.annotation.Nullable;
import java.util.Map;

@Deprecated
public class FragmentManagerNonConfig
{
    @Nullable
    private final Map<String, FragmentManagerNonConfig> mChildNonConfigs;
    @Nullable
    private final Collection<Fragment> mFragments;
    @Nullable
    private final Map<String, ViewModelStore> mViewModelStores;
    
    FragmentManagerNonConfig(@Nullable final Collection<Fragment> mFragments, @Nullable final Map<String, FragmentManagerNonConfig> mChildNonConfigs, @Nullable final Map<String, ViewModelStore> mViewModelStores) {
        this.mFragments = mFragments;
        this.mChildNonConfigs = mChildNonConfigs;
        this.mViewModelStores = mViewModelStores;
    }
    
    @Nullable
    Map<String, FragmentManagerNonConfig> getChildNonConfigs() {
        return this.mChildNonConfigs;
    }
    
    @Nullable
    Collection<Fragment> getFragments() {
        return this.mFragments;
    }
    
    @Nullable
    Map<String, ViewModelStore> getViewModelStores() {
        return this.mViewModelStores;
    }
    
    boolean isRetaining(final Fragment fragment) {
        final Collection<Fragment> mFragments = this.mFragments;
        return mFragments != null && mFragments.contains(fragment);
    }
}

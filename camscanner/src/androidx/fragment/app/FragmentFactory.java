// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import java.lang.reflect.InvocationTargetException;
import androidx.annotation.NonNull;
import androidx.collection.SimpleArrayMap;

public class FragmentFactory
{
    private static final SimpleArrayMap<ClassLoader, SimpleArrayMap<String, Class<?>>> sClassCacheMap;
    
    static {
        sClassCacheMap = new SimpleArrayMap<ClassLoader, SimpleArrayMap<String, Class<?>>>();
    }
    
    static boolean isFragmentClass(@NonNull final ClassLoader classLoader, @NonNull final String s) {
        try {
            return Fragment.class.isAssignableFrom(loadClass(classLoader, s));
        }
        catch (final ClassNotFoundException ex) {
            return false;
        }
    }
    
    @NonNull
    private static Class<?> loadClass(@NonNull final ClassLoader loader, @NonNull final String name) throws ClassNotFoundException {
        final SimpleArrayMap<ClassLoader, SimpleArrayMap<String, Class<?>>> sClassCacheMap = FragmentFactory.sClassCacheMap;
        SimpleArrayMap simpleArrayMap;
        if ((simpleArrayMap = sClassCacheMap.get(loader)) == null) {
            simpleArrayMap = new SimpleArrayMap();
            sClassCacheMap.put(loader, simpleArrayMap);
        }
        Class<?> forName;
        if ((forName = (Class)simpleArrayMap.get(name)) == null) {
            forName = Class.forName(name, false, loader);
            simpleArrayMap.put(name, forName);
        }
        return forName;
    }
    
    @NonNull
    public static Class<? extends Fragment> loadFragmentClass(@NonNull final ClassLoader classLoader, @NonNull final String s) {
        try {
            return (Class<? extends Fragment>)loadClass(classLoader, s);
        }
        catch (final ClassCastException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unable to instantiate fragment ");
            sb.append(s);
            sb.append(": make sure class is a valid subclass of Fragment");
            throw new Fragment.InstantiationException(sb.toString(), ex);
        }
        catch (final ClassNotFoundException ex2) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Unable to instantiate fragment ");
            sb2.append(s);
            sb2.append(": make sure class name exists");
            throw new Fragment.InstantiationException(sb2.toString(), ex2);
        }
    }
    
    @NonNull
    public Fragment instantiate(@NonNull final ClassLoader classLoader, @NonNull final String s) {
        try {
            return (Fragment)loadFragmentClass(classLoader, s).getConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
        }
        catch (final InvocationTargetException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unable to instantiate fragment ");
            sb.append(s);
            sb.append(": calling Fragment constructor caused an exception");
            throw new Fragment.InstantiationException(sb.toString(), ex);
        }
        catch (final NoSuchMethodException ex2) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Unable to instantiate fragment ");
            sb2.append(s);
            sb2.append(": could not find Fragment constructor");
            throw new Fragment.InstantiationException(sb2.toString(), ex2);
        }
        catch (final IllegalAccessException ex3) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Unable to instantiate fragment ");
            sb3.append(s);
            sb3.append(": make sure class name exists, is public, and has an empty constructor that is public");
            throw new Fragment.InstantiationException(sb3.toString(), ex3);
        }
        catch (final InstantiationException ex4) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Unable to instantiate fragment ");
            sb4.append(s);
            sb4.append(": make sure class name exists, is public, and has an empty constructor that is public");
            throw new Fragment.InstantiationException(sb4.toString(), ex4);
        }
    }
}

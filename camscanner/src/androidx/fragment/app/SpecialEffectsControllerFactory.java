// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import androidx.annotation.NonNull;
import android.view.ViewGroup;

interface SpecialEffectsControllerFactory
{
    @NonNull
    SpecialEffectsController createController(@NonNull final ViewGroup p0);
}

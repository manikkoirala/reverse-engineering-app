// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.animation.LayoutTransition;
import java.util.Collection;
import java.util.Iterator;
import android.graphics.Canvas;
import androidx.annotation.RequiresApi;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import android.view.WindowInsets;
import android.view.ViewGroup$LayoutParams;
import android.view.ViewGroup;
import android.os.Bundle;
import android.content.res.TypedArray;
import androidx.fragment.R;
import kotlin.jvm.internal.DefaultConstructorMarker;
import android.util.AttributeSet;
import java.util.ArrayList;
import kotlin.jvm.internal.Intrinsics;
import android.content.Context;
import org.jetbrains.annotations.NotNull;
import android.view.View;
import java.util.List;
import android.view.View$OnApplyWindowInsetsListener;
import kotlin.Metadata;
import android.widget.FrameLayout;

@Metadata
public final class FragmentContainerView extends FrameLayout
{
    private View$OnApplyWindowInsetsListener applyWindowInsetsListener;
    @NotNull
    private final List<View> disappearingFragmentChildren;
    private boolean drawDisappearingViewsFirst;
    @NotNull
    private final List<View> transitioningFragmentViews;
    
    public FragmentContainerView(@NotNull final Context context) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        super(context);
        this.disappearingFragmentChildren = new ArrayList<View>();
        this.transitioningFragmentViews = new ArrayList<View>();
        this.drawDisappearingViewsFirst = true;
    }
    
    public FragmentContainerView(@NotNull final Context context, final AttributeSet set) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        this(context, set, 0, 4, null);
    }
    
    public FragmentContainerView(@NotNull final Context context, final AttributeSet set, final int n) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        super(context, set, n);
        this.disappearingFragmentChildren = new ArrayList<View>();
        this.transitioningFragmentViews = new ArrayList<View>();
        this.drawDisappearingViewsFirst = true;
        if (set != null) {
            final String classAttribute = set.getClassAttribute();
            final int[] fragmentContainerView = R.styleable.FragmentContainerView;
            Intrinsics.checkNotNullExpressionValue((Object)fragmentContainerView, "FragmentContainerView");
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, fragmentContainerView, 0, 0);
            String string;
            String str;
            if (classAttribute == null) {
                string = obtainStyledAttributes.getString(R.styleable.FragmentContainerView_android_name);
                str = "android:name";
            }
            else {
                str = "class";
                string = classAttribute;
            }
            obtainStyledAttributes.recycle();
            if (string != null) {
                if (!((View)this).isInEditMode()) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("FragmentContainerView must be within a FragmentActivity to use ");
                    sb.append(str);
                    sb.append("=\"");
                    sb.append(string);
                    sb.append('\"');
                    throw new UnsupportedOperationException(sb.toString());
                }
            }
        }
    }
    
    public FragmentContainerView(@NotNull final Context context, @NotNull final AttributeSet set, @NotNull final FragmentManager fragmentManager) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        Intrinsics.checkNotNullParameter((Object)set, "attrs");
        Intrinsics.checkNotNullParameter((Object)fragmentManager, "fm");
        super(context, set);
        this.disappearingFragmentChildren = new ArrayList<View>();
        this.transitioningFragmentViews = new ArrayList<View>();
        this.drawDisappearingViewsFirst = true;
        final String classAttribute = set.getClassAttribute();
        final int[] fragmentContainerView = R.styleable.FragmentContainerView;
        Intrinsics.checkNotNullExpressionValue((Object)fragmentContainerView, "FragmentContainerView");
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, fragmentContainerView, 0, 0);
        String string = classAttribute;
        if (classAttribute == null) {
            string = obtainStyledAttributes.getString(R.styleable.FragmentContainerView_android_name);
        }
        final String string2 = obtainStyledAttributes.getString(R.styleable.FragmentContainerView_android_tag);
        obtainStyledAttributes.recycle();
        final int id = ((View)this).getId();
        final Fragment fragmentById = fragmentManager.findFragmentById(id);
        if (string != null && fragmentById == null) {
            if (id == -1) {
                String string3;
                if (string2 != null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(" with tag ");
                    sb.append(string2);
                    string3 = sb.toString();
                }
                else {
                    string3 = "";
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("FragmentContainerView must have an android:id to add Fragment ");
                sb2.append(string);
                sb2.append(string3);
                throw new IllegalStateException(sb2.toString());
            }
            final Fragment instantiate = fragmentManager.getFragmentFactory().instantiate(context.getClassLoader(), string);
            Intrinsics.checkNotNullExpressionValue((Object)instantiate, "fm.fragmentFactory.insta\u2026ontext.classLoader, name)");
            instantiate.onInflate(context, set, null);
            fragmentManager.beginTransaction().setReorderingAllowed(true).add((ViewGroup)this, instantiate, string2).commitNowAllowingStateLoss();
        }
        fragmentManager.onContainerAvailable(this);
    }
    
    private final void addDisappearingFragmentView(final View view) {
        if (this.transitioningFragmentViews.contains(view)) {
            this.disappearingFragmentChildren.add(view);
        }
    }
    
    public void addView(@NotNull final View obj, final int n, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        Intrinsics.checkNotNullParameter((Object)obj, "child");
        if (FragmentManager.getViewFragment(obj) != null) {
            super.addView(obj, n, viewGroup$LayoutParams);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Views added to a FragmentContainerView must be associated with a Fragment. View ");
        sb.append(obj);
        sb.append(" is not associated with a Fragment.");
        throw new IllegalStateException(sb.toString().toString());
    }
    
    @RequiresApi(20)
    @NotNull
    public WindowInsets dispatchApplyWindowInsets(@NotNull final WindowInsets windowInsets) {
        Intrinsics.checkNotNullParameter((Object)windowInsets, "insets");
        final WindowInsetsCompat windowInsetsCompat = WindowInsetsCompat.toWindowInsetsCompat(windowInsets);
        Intrinsics.checkNotNullExpressionValue((Object)windowInsetsCompat, "toWindowInsetsCompat(insets)");
        final View$OnApplyWindowInsetsListener applyWindowInsetsListener = this.applyWindowInsetsListener;
        WindowInsetsCompat windowInsetsCompat2;
        if (applyWindowInsetsListener != null) {
            final Api20Impl instance = Api20Impl.INSTANCE;
            Intrinsics.Oo08((Object)applyWindowInsetsListener);
            windowInsetsCompat2 = WindowInsetsCompat.toWindowInsetsCompat(instance.onApplyWindowInsets(applyWindowInsetsListener, (View)this, windowInsets));
        }
        else {
            windowInsetsCompat2 = ViewCompat.onApplyWindowInsets((View)this, windowInsetsCompat);
        }
        Intrinsics.checkNotNullExpressionValue((Object)windowInsetsCompat2, "if (applyWindowInsetsLis\u2026, insetsCompat)\n        }");
        if (!windowInsetsCompat2.isConsumed()) {
            for (int childCount = ((ViewGroup)this).getChildCount(), i = 0; i < childCount; ++i) {
                ViewCompat.dispatchApplyWindowInsets(((ViewGroup)this).getChildAt(i), windowInsetsCompat2);
            }
        }
        return windowInsets;
    }
    
    protected void dispatchDraw(@NotNull final Canvas canvas) {
        Intrinsics.checkNotNullParameter((Object)canvas, "canvas");
        if (this.drawDisappearingViewsFirst) {
            final Iterator iterator = this.disappearingFragmentChildren.iterator();
            while (iterator.hasNext()) {
                super.drawChild(canvas, (View)iterator.next(), ((View)this).getDrawingTime());
            }
        }
        super.dispatchDraw(canvas);
    }
    
    protected boolean drawChild(@NotNull final Canvas canvas, @NotNull final View view, final long n) {
        Intrinsics.checkNotNullParameter((Object)canvas, "canvas");
        Intrinsics.checkNotNullParameter((Object)view, "child");
        return (!this.drawDisappearingViewsFirst || !(this.disappearingFragmentChildren.isEmpty() ^ true) || !this.disappearingFragmentChildren.contains(view)) && super.drawChild(canvas, view, n);
    }
    
    public void endViewTransition(@NotNull final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "view");
        this.transitioningFragmentViews.remove(view);
        if (this.disappearingFragmentChildren.remove(view)) {
            this.drawDisappearingViewsFirst = true;
        }
        super.endViewTransition(view);
    }
    
    public final <F extends Fragment> F getFragment() {
        return (F)FragmentManager.findFragmentManager((View)this).findFragmentById(((View)this).getId());
    }
    
    @RequiresApi(20)
    @NotNull
    public WindowInsets onApplyWindowInsets(@NotNull final WindowInsets windowInsets) {
        Intrinsics.checkNotNullParameter((Object)windowInsets, "insets");
        return windowInsets;
    }
    
    public void removeAllViewsInLayout() {
        for (int n = ((ViewGroup)this).getChildCount() - 1; -1 < n; --n) {
            final View child = ((ViewGroup)this).getChildAt(n);
            Intrinsics.checkNotNullExpressionValue((Object)child, "view");
            this.addDisappearingFragmentView(child);
        }
        super.removeAllViewsInLayout();
    }
    
    public void removeView(@NotNull final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "view");
        this.addDisappearingFragmentView(view);
        super.removeView(view);
    }
    
    public void removeViewAt(final int n) {
        final View child = ((ViewGroup)this).getChildAt(n);
        Intrinsics.checkNotNullExpressionValue((Object)child, "view");
        this.addDisappearingFragmentView(child);
        super.removeViewAt(n);
    }
    
    public void removeViewInLayout(@NotNull final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "view");
        this.addDisappearingFragmentView(view);
        super.removeViewInLayout(view);
    }
    
    public void removeViews(final int n, final int n2) {
        for (int i = n; i < n + n2; ++i) {
            final View child = ((ViewGroup)this).getChildAt(i);
            Intrinsics.checkNotNullExpressionValue((Object)child, "view");
            this.addDisappearingFragmentView(child);
        }
        super.removeViews(n, n2);
    }
    
    public void removeViewsInLayout(final int n, final int n2) {
        for (int i = n; i < n + n2; ++i) {
            final View child = ((ViewGroup)this).getChildAt(i);
            Intrinsics.checkNotNullExpressionValue((Object)child, "view");
            this.addDisappearingFragmentView(child);
        }
        super.removeViewsInLayout(n, n2);
    }
    
    public final void setDrawDisappearingViewsLast(final boolean drawDisappearingViewsFirst) {
        this.drawDisappearingViewsFirst = drawDisappearingViewsFirst;
    }
    
    public void setLayoutTransition(final LayoutTransition layoutTransition) {
        throw new UnsupportedOperationException("FragmentContainerView does not support Layout Transitions or animateLayoutChanges=\"true\".");
    }
    
    public void setOnApplyWindowInsetsListener(@NotNull final View$OnApplyWindowInsetsListener applyWindowInsetsListener) {
        Intrinsics.checkNotNullParameter((Object)applyWindowInsetsListener, "listener");
        this.applyWindowInsetsListener = applyWindowInsetsListener;
    }
    
    public void startViewTransition(@NotNull final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "view");
        if (view.getParent() == this) {
            this.transitioningFragmentViews.add(view);
        }
        super.startViewTransition(view);
    }
    
    @Metadata
    @RequiresApi(20)
    public static final class Api20Impl
    {
        @NotNull
        public static final Api20Impl INSTANCE;
        
        static {
            INSTANCE = new Api20Impl();
        }
        
        private Api20Impl() {
        }
        
        @NotNull
        public final WindowInsets onApplyWindowInsets(@NotNull final View$OnApplyWindowInsetsListener view$OnApplyWindowInsetsListener, @NotNull final View view, @NotNull final WindowInsets windowInsets) {
            Intrinsics.checkNotNullParameter((Object)view$OnApplyWindowInsetsListener, "onApplyWindowInsetsListener");
            Intrinsics.checkNotNullParameter((Object)view, "v");
            Intrinsics.checkNotNullParameter((Object)windowInsets, "insets");
            final WindowInsets onApplyWindowInsets = view$OnApplyWindowInsetsListener.onApplyWindowInsets(view, windowInsets);
            Intrinsics.checkNotNullExpressionValue((Object)onApplyWindowInsets, "onApplyWindowInsetsListe\u2026lyWindowInsets(v, insets)");
            return onApplyWindowInsets;
        }
    }
}

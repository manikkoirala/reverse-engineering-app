// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.view.View;
import androidx.annotation.IdRes;
import androidx.annotation.Nullable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.content.Context;

public abstract class FragmentContainer
{
    @Deprecated
    @NonNull
    public Fragment instantiate(@NonNull final Context context, @NonNull final String s, @Nullable final Bundle bundle) {
        return Fragment.instantiate(context, s, bundle);
    }
    
    @Nullable
    public abstract View onFindViewById(@IdRes final int p0);
    
    public abstract boolean onHasView();
}

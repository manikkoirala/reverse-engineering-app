// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import java.util.ArrayList;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.collection.SimpleArrayMap;
import androidx.lifecycle.ViewModelStore;
import java.util.Map;
import java.util.Collection;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.content.Context;
import android.view.View;
import androidx.loader.app.LoaderManager;
import android.annotation.SuppressLint;
import java.util.List;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import android.view.MenuInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.content.res.Configuration;
import androidx.annotation.Nullable;
import androidx.core.util.Preconditions;
import androidx.annotation.NonNull;

public class FragmentController
{
    private final FragmentHostCallback<?> mHost;
    
    private FragmentController(final FragmentHostCallback<?> mHost) {
        this.mHost = mHost;
    }
    
    @NonNull
    public static FragmentController createController(@NonNull final FragmentHostCallback<?> fragmentHostCallback) {
        return new FragmentController(Preconditions.checkNotNull(fragmentHostCallback, "callbacks == null"));
    }
    
    public void attachHost(@Nullable final Fragment fragment) {
        final FragmentHostCallback<?> mHost = this.mHost;
        mHost.mFragmentManager.attachController(mHost, mHost, fragment);
    }
    
    public void dispatchActivityCreated() {
        this.mHost.mFragmentManager.dispatchActivityCreated();
    }
    
    @Deprecated
    public void dispatchConfigurationChanged(@NonNull final Configuration configuration) {
        this.mHost.mFragmentManager.dispatchConfigurationChanged(configuration, true);
    }
    
    public boolean dispatchContextItemSelected(@NonNull final MenuItem menuItem) {
        return this.mHost.mFragmentManager.dispatchContextItemSelected(menuItem);
    }
    
    public void dispatchCreate() {
        this.mHost.mFragmentManager.dispatchCreate();
    }
    
    @Deprecated
    public boolean dispatchCreateOptionsMenu(@NonNull final Menu menu, @NonNull final MenuInflater menuInflater) {
        return this.mHost.mFragmentManager.dispatchCreateOptionsMenu(menu, menuInflater);
    }
    
    public void dispatchDestroy() {
        this.mHost.mFragmentManager.dispatchDestroy();
    }
    
    public void dispatchDestroyView() {
        this.mHost.mFragmentManager.dispatchDestroyView();
    }
    
    @Deprecated
    public void dispatchLowMemory() {
        this.mHost.mFragmentManager.dispatchLowMemory(true);
    }
    
    @Deprecated
    public void dispatchMultiWindowModeChanged(final boolean b) {
        this.mHost.mFragmentManager.dispatchMultiWindowModeChanged(b, true);
    }
    
    @Deprecated
    public boolean dispatchOptionsItemSelected(@NonNull final MenuItem menuItem) {
        return this.mHost.mFragmentManager.dispatchOptionsItemSelected(menuItem);
    }
    
    @Deprecated
    public void dispatchOptionsMenuClosed(@NonNull final Menu menu) {
        this.mHost.mFragmentManager.dispatchOptionsMenuClosed(menu);
    }
    
    public void dispatchPause() {
        this.mHost.mFragmentManager.dispatchPause();
    }
    
    @Deprecated
    public void dispatchPictureInPictureModeChanged(final boolean b) {
        this.mHost.mFragmentManager.dispatchPictureInPictureModeChanged(b, true);
    }
    
    @Deprecated
    public boolean dispatchPrepareOptionsMenu(@NonNull final Menu menu) {
        return this.mHost.mFragmentManager.dispatchPrepareOptionsMenu(menu);
    }
    
    @Deprecated
    public void dispatchReallyStop() {
    }
    
    public void dispatchResume() {
        this.mHost.mFragmentManager.dispatchResume();
    }
    
    public void dispatchStart() {
        this.mHost.mFragmentManager.dispatchStart();
    }
    
    public void dispatchStop() {
        this.mHost.mFragmentManager.dispatchStop();
    }
    
    @Deprecated
    public void doLoaderDestroy() {
    }
    
    @Deprecated
    public void doLoaderRetain() {
    }
    
    @Deprecated
    public void doLoaderStart() {
    }
    
    @Deprecated
    public void doLoaderStop(final boolean b) {
    }
    
    @Deprecated
    public void dumpLoaders(@NonNull final String s, @Nullable final FileDescriptor fileDescriptor, @NonNull final PrintWriter printWriter, @Nullable final String[] array) {
    }
    
    public boolean execPendingActions() {
        return this.mHost.mFragmentManager.execPendingActions(true);
    }
    
    @Nullable
    public Fragment findFragmentByWho(@NonNull final String s) {
        return this.mHost.mFragmentManager.findFragmentByWho(s);
    }
    
    @NonNull
    public List<Fragment> getActiveFragments(@SuppressLint({ "UnknownNullness" }) final List<Fragment> list) {
        return this.mHost.mFragmentManager.getActiveFragments();
    }
    
    public int getActiveFragmentsCount() {
        return this.mHost.mFragmentManager.getActiveFragmentCount();
    }
    
    @NonNull
    public FragmentManager getSupportFragmentManager() {
        return this.mHost.mFragmentManager;
    }
    
    @Deprecated
    @SuppressLint({ "UnknownNullness" })
    public LoaderManager getSupportLoaderManager() {
        throw new UnsupportedOperationException("Loaders are managed separately from FragmentController, use LoaderManager.getInstance() to obtain a LoaderManager.");
    }
    
    public void noteStateNotSaved() {
        this.mHost.mFragmentManager.noteStateNotSaved();
    }
    
    @Nullable
    public View onCreateView(@Nullable final View view, @NonNull final String s, @NonNull final Context context, @NonNull final AttributeSet set) {
        return this.mHost.mFragmentManager.getLayoutInflaterFactory().onCreateView(view, s, context, set);
    }
    
    @Deprecated
    public void reportLoaderStart() {
    }
    
    @Deprecated
    public void restoreAllState(@Nullable final Parcelable parcelable, @Nullable final FragmentManagerNonConfig fragmentManagerNonConfig) {
        this.mHost.mFragmentManager.restoreAllState(parcelable, fragmentManagerNonConfig);
    }
    
    @Deprecated
    public void restoreAllState(@Nullable final Parcelable parcelable, @Nullable final List<Fragment> list) {
        this.mHost.mFragmentManager.restoreAllState(parcelable, new FragmentManagerNonConfig(list, null, null));
    }
    
    @Deprecated
    public void restoreLoaderNonConfig(@SuppressLint({ "UnknownNullness" }) final SimpleArrayMap<String, LoaderManager> simpleArrayMap) {
    }
    
    @Deprecated
    public void restoreSaveState(@Nullable final Parcelable parcelable) {
        final FragmentHostCallback<?> mHost = this.mHost;
        if (mHost instanceof ViewModelStoreOwner) {
            mHost.mFragmentManager.restoreSaveState(parcelable);
            return;
        }
        throw new IllegalStateException("Your FragmentHostCallback must implement ViewModelStoreOwner to call restoreSaveState(). Call restoreAllState()  if you're still using retainNestedNonConfig().");
    }
    
    @Deprecated
    @Nullable
    public SimpleArrayMap<String, LoaderManager> retainLoaderNonConfig() {
        return null;
    }
    
    @Deprecated
    @Nullable
    public FragmentManagerNonConfig retainNestedNonConfig() {
        return this.mHost.mFragmentManager.retainNonConfig();
    }
    
    @Deprecated
    @Nullable
    public List<Fragment> retainNonConfig() {
        final FragmentManagerNonConfig retainNonConfig = this.mHost.mFragmentManager.retainNonConfig();
        ArrayList list;
        if (retainNonConfig != null && retainNonConfig.getFragments() != null) {
            list = new ArrayList(retainNonConfig.getFragments());
        }
        else {
            list = null;
        }
        return list;
    }
    
    @Deprecated
    @Nullable
    public Parcelable saveAllState() {
        return this.mHost.mFragmentManager.saveAllState();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app.strictmode;

import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import androidx.fragment.app.Fragment;
import kotlin.Metadata;

@Metadata
public abstract class Violation extends RuntimeException
{
    @NotNull
    private final Fragment fragment;
    
    public Violation(@NotNull final Fragment fragment, final String message) {
        Intrinsics.checkNotNullParameter((Object)fragment, "fragment");
        super(message);
        this.fragment = fragment;
    }
    
    @NotNull
    public final Fragment getFragment() {
        return this.fragment;
    }
}

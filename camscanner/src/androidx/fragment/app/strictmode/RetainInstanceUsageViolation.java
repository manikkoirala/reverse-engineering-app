// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app.strictmode;

import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import androidx.fragment.app.Fragment;
import kotlin.Metadata;

@Metadata
public abstract class RetainInstanceUsageViolation extends Violation
{
    public RetainInstanceUsageViolation(@NotNull final Fragment fragment, final String s) {
        Intrinsics.checkNotNullParameter((Object)fragment, "fragment");
        super(fragment, s);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app.strictmode;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import androidx.fragment.app.Fragment;
import kotlin.Metadata;

@Metadata
public final class SetUserVisibleHintViolation extends Violation
{
    private final boolean isVisibleToUser;
    
    public SetUserVisibleHintViolation(@NotNull final Fragment obj, final boolean b) {
        Intrinsics.checkNotNullParameter((Object)obj, "fragment");
        final StringBuilder sb = new StringBuilder();
        sb.append("Attempting to set user visible hint to ");
        sb.append(b);
        sb.append(" for fragment ");
        sb.append(obj);
        super(obj, sb.toString());
        this.isVisibleToUser = b;
    }
    
    public final boolean isVisibleToUser() {
        return this.isVisibleToUser;
    }
}

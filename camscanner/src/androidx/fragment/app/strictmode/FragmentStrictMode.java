// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app.strictmode;

import android.annotation.SuppressLint;
import java.util.LinkedHashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import kotlin.collections.MapsKt;
import kotlin.collections.SetsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import java.util.Map;
import androidx.annotation.VisibleForTesting;
import kotlin.collections.CollectionsKt;
import java.util.Set;
import android.os.Handler;
import android.os.Looper;
import android.view.ViewGroup;
import androidx.annotation.RestrictTo;
import \u3007o\u3007.\u300780\u3007808\u3007O;
import \u3007o\u3007.oO80;
import androidx.fragment.app.FragmentManager;
import kotlin.jvm.internal.Intrinsics;
import androidx.fragment.app.Fragment;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class FragmentStrictMode
{
    @NotNull
    public static final FragmentStrictMode INSTANCE;
    @NotNull
    private static final String TAG = "FragmentStrictMode";
    @NotNull
    private static Policy defaultPolicy;
    
    static {
        INSTANCE = new FragmentStrictMode();
        FragmentStrictMode.defaultPolicy = Policy.LAX;
    }
    
    private FragmentStrictMode() {
    }
    
    private final Policy getNearestPolicy(Fragment parentFragment) {
        while (parentFragment != null) {
            if (parentFragment.isAdded()) {
                final FragmentManager parentFragmentManager = parentFragment.getParentFragmentManager();
                Intrinsics.checkNotNullExpressionValue((Object)parentFragmentManager, "declaringFragment.parentFragmentManager");
                if (parentFragmentManager.getStrictModePolicy() != null) {
                    final Policy strictModePolicy = parentFragmentManager.getStrictModePolicy();
                    Intrinsics.Oo08((Object)strictModePolicy);
                    return strictModePolicy;
                }
            }
            parentFragment = parentFragment.getParentFragment();
        }
        return FragmentStrictMode.defaultPolicy;
    }
    
    private final void handlePolicyViolation(final Policy policy, final Violation violation) {
        final Fragment fragment = violation.getFragment();
        final String name = fragment.getClass().getName();
        if (policy.getFlags$fragment_release().contains(Flag.PENALTY_LOG)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Policy violation in ");
            sb.append(name);
        }
        if (policy.getListener$fragment_release() != null) {
            this.runOnHostThread(fragment, new oO80(policy, violation));
        }
        if (policy.getFlags$fragment_release().contains(Flag.PENALTY_DEATH)) {
            this.runOnHostThread(fragment, new \u300780\u3007808\u3007O(name, violation));
        }
    }
    
    private static final void handlePolicyViolation$lambda-0(final Policy policy, final Violation violation) {
        Intrinsics.checkNotNullParameter((Object)policy, "$policy");
        Intrinsics.checkNotNullParameter((Object)violation, "$violation");
        policy.getListener$fragment_release().onViolation(violation);
    }
    
    private static final void handlePolicyViolation$lambda-1(final String str, final Violation violation) {
        Intrinsics.checkNotNullParameter((Object)violation, "$violation");
        final StringBuilder sb = new StringBuilder();
        sb.append("Policy violation with PENALTY_DEATH in ");
        sb.append(str);
        throw violation;
    }
    
    private final void logIfDebuggingEnabled(final Violation violation) {
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("StrictMode violation in ");
            sb.append(violation.getFragment().getClass().getName());
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final void onFragmentReuse(@NotNull final Fragment fragment, @NotNull final String s) {
        Intrinsics.checkNotNullParameter((Object)fragment, "fragment");
        Intrinsics.checkNotNullParameter((Object)s, "previousFragmentId");
        final FragmentReuseViolation fragmentReuseViolation = new FragmentReuseViolation(fragment, s);
        final FragmentStrictMode instance = FragmentStrictMode.INSTANCE;
        instance.logIfDebuggingEnabled(fragmentReuseViolation);
        final Policy nearestPolicy = instance.getNearestPolicy(fragment);
        if (nearestPolicy.getFlags$fragment_release().contains(Flag.DETECT_FRAGMENT_REUSE) && instance.shouldHandlePolicyViolation(nearestPolicy, fragment.getClass(), fragmentReuseViolation.getClass())) {
            instance.handlePolicyViolation(nearestPolicy, fragmentReuseViolation);
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final void onFragmentTagUsage(@NotNull final Fragment fragment, final ViewGroup viewGroup) {
        Intrinsics.checkNotNullParameter((Object)fragment, "fragment");
        final FragmentTagUsageViolation fragmentTagUsageViolation = new FragmentTagUsageViolation(fragment, viewGroup);
        final FragmentStrictMode instance = FragmentStrictMode.INSTANCE;
        instance.logIfDebuggingEnabled(fragmentTagUsageViolation);
        final Policy nearestPolicy = instance.getNearestPolicy(fragment);
        if (nearestPolicy.getFlags$fragment_release().contains(Flag.DETECT_FRAGMENT_TAG_USAGE) && instance.shouldHandlePolicyViolation(nearestPolicy, fragment.getClass(), fragmentTagUsageViolation.getClass())) {
            instance.handlePolicyViolation(nearestPolicy, fragmentTagUsageViolation);
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final void onGetRetainInstanceUsage(@NotNull final Fragment fragment) {
        Intrinsics.checkNotNullParameter((Object)fragment, "fragment");
        final GetRetainInstanceUsageViolation getRetainInstanceUsageViolation = new GetRetainInstanceUsageViolation(fragment);
        final FragmentStrictMode instance = FragmentStrictMode.INSTANCE;
        instance.logIfDebuggingEnabled(getRetainInstanceUsageViolation);
        final Policy nearestPolicy = instance.getNearestPolicy(fragment);
        if (nearestPolicy.getFlags$fragment_release().contains(Flag.DETECT_RETAIN_INSTANCE_USAGE) && instance.shouldHandlePolicyViolation(nearestPolicy, fragment.getClass(), getRetainInstanceUsageViolation.getClass())) {
            instance.handlePolicyViolation(nearestPolicy, getRetainInstanceUsageViolation);
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final void onGetTargetFragmentRequestCodeUsage(@NotNull final Fragment fragment) {
        Intrinsics.checkNotNullParameter((Object)fragment, "fragment");
        final GetTargetFragmentRequestCodeUsageViolation getTargetFragmentRequestCodeUsageViolation = new GetTargetFragmentRequestCodeUsageViolation(fragment);
        final FragmentStrictMode instance = FragmentStrictMode.INSTANCE;
        instance.logIfDebuggingEnabled(getTargetFragmentRequestCodeUsageViolation);
        final Policy nearestPolicy = instance.getNearestPolicy(fragment);
        if (nearestPolicy.getFlags$fragment_release().contains(Flag.DETECT_TARGET_FRAGMENT_USAGE) && instance.shouldHandlePolicyViolation(nearestPolicy, fragment.getClass(), getTargetFragmentRequestCodeUsageViolation.getClass())) {
            instance.handlePolicyViolation(nearestPolicy, getTargetFragmentRequestCodeUsageViolation);
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final void onGetTargetFragmentUsage(@NotNull final Fragment fragment) {
        Intrinsics.checkNotNullParameter((Object)fragment, "fragment");
        final GetTargetFragmentUsageViolation getTargetFragmentUsageViolation = new GetTargetFragmentUsageViolation(fragment);
        final FragmentStrictMode instance = FragmentStrictMode.INSTANCE;
        instance.logIfDebuggingEnabled(getTargetFragmentUsageViolation);
        final Policy nearestPolicy = instance.getNearestPolicy(fragment);
        if (nearestPolicy.getFlags$fragment_release().contains(Flag.DETECT_TARGET_FRAGMENT_USAGE) && instance.shouldHandlePolicyViolation(nearestPolicy, fragment.getClass(), getTargetFragmentUsageViolation.getClass())) {
            instance.handlePolicyViolation(nearestPolicy, getTargetFragmentUsageViolation);
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final void onSetRetainInstanceUsage(@NotNull final Fragment fragment) {
        Intrinsics.checkNotNullParameter((Object)fragment, "fragment");
        final SetRetainInstanceUsageViolation setRetainInstanceUsageViolation = new SetRetainInstanceUsageViolation(fragment);
        final FragmentStrictMode instance = FragmentStrictMode.INSTANCE;
        instance.logIfDebuggingEnabled(setRetainInstanceUsageViolation);
        final Policy nearestPolicy = instance.getNearestPolicy(fragment);
        if (nearestPolicy.getFlags$fragment_release().contains(Flag.DETECT_RETAIN_INSTANCE_USAGE) && instance.shouldHandlePolicyViolation(nearestPolicy, fragment.getClass(), setRetainInstanceUsageViolation.getClass())) {
            instance.handlePolicyViolation(nearestPolicy, setRetainInstanceUsageViolation);
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final void onSetTargetFragmentUsage(@NotNull final Fragment fragment, @NotNull final Fragment fragment2, final int n) {
        Intrinsics.checkNotNullParameter((Object)fragment, "violatingFragment");
        Intrinsics.checkNotNullParameter((Object)fragment2, "targetFragment");
        final SetTargetFragmentUsageViolation setTargetFragmentUsageViolation = new SetTargetFragmentUsageViolation(fragment, fragment2, n);
        final FragmentStrictMode instance = FragmentStrictMode.INSTANCE;
        instance.logIfDebuggingEnabled(setTargetFragmentUsageViolation);
        final Policy nearestPolicy = instance.getNearestPolicy(fragment);
        if (nearestPolicy.getFlags$fragment_release().contains(Flag.DETECT_TARGET_FRAGMENT_USAGE) && instance.shouldHandlePolicyViolation(nearestPolicy, fragment.getClass(), setTargetFragmentUsageViolation.getClass())) {
            instance.handlePolicyViolation(nearestPolicy, setTargetFragmentUsageViolation);
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final void onSetUserVisibleHint(@NotNull final Fragment fragment, final boolean b) {
        Intrinsics.checkNotNullParameter((Object)fragment, "fragment");
        final SetUserVisibleHintViolation setUserVisibleHintViolation = new SetUserVisibleHintViolation(fragment, b);
        final FragmentStrictMode instance = FragmentStrictMode.INSTANCE;
        instance.logIfDebuggingEnabled(setUserVisibleHintViolation);
        final Policy nearestPolicy = instance.getNearestPolicy(fragment);
        if (nearestPolicy.getFlags$fragment_release().contains(Flag.DETECT_SET_USER_VISIBLE_HINT) && instance.shouldHandlePolicyViolation(nearestPolicy, fragment.getClass(), setUserVisibleHintViolation.getClass())) {
            instance.handlePolicyViolation(nearestPolicy, setUserVisibleHintViolation);
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final void onWrongFragmentContainer(@NotNull final Fragment fragment, @NotNull final ViewGroup viewGroup) {
        Intrinsics.checkNotNullParameter((Object)fragment, "fragment");
        Intrinsics.checkNotNullParameter((Object)viewGroup, "container");
        final WrongFragmentContainerViolation wrongFragmentContainerViolation = new WrongFragmentContainerViolation(fragment, viewGroup);
        final FragmentStrictMode instance = FragmentStrictMode.INSTANCE;
        instance.logIfDebuggingEnabled(wrongFragmentContainerViolation);
        final Policy nearestPolicy = instance.getNearestPolicy(fragment);
        if (nearestPolicy.getFlags$fragment_release().contains(Flag.DETECT_WRONG_FRAGMENT_CONTAINER) && instance.shouldHandlePolicyViolation(nearestPolicy, fragment.getClass(), wrongFragmentContainerViolation.getClass())) {
            instance.handlePolicyViolation(nearestPolicy, wrongFragmentContainerViolation);
        }
    }
    
    private final void runOnHostThread(final Fragment fragment, final Runnable runnable) {
        if (fragment.isAdded()) {
            final Handler handler = fragment.getParentFragmentManager().getHost().getHandler();
            Intrinsics.checkNotNullExpressionValue((Object)handler, "fragment.parentFragmentManager.host.handler");
            if (Intrinsics.\u3007o\u3007((Object)handler.getLooper(), (Object)Looper.myLooper())) {
                runnable.run();
            }
            else {
                handler.post(runnable);
            }
        }
        else {
            runnable.run();
        }
    }
    
    private final boolean shouldHandlePolicyViolation(final Policy policy, final Class<? extends Fragment> clazz, final Class<? extends Violation> clazz2) {
        final Set set = policy.getMAllowedViolations$fragment_release().get(clazz.getName());
        return set == null || ((Intrinsics.\u3007o\u3007((Object)clazz2.getSuperclass(), (Object)Violation.class) || !CollectionsKt.o8((Iterable)set, (Object)clazz2.getSuperclass())) && (set.contains(clazz2) ^ true));
    }
    
    @NotNull
    public final Policy getDefaultPolicy() {
        return FragmentStrictMode.defaultPolicy;
    }
    
    @VisibleForTesting
    public final void onPolicyViolation(@NotNull final Violation violation) {
        Intrinsics.checkNotNullParameter((Object)violation, "violation");
        this.logIfDebuggingEnabled(violation);
        final Fragment fragment = violation.getFragment();
        final Policy nearestPolicy = this.getNearestPolicy(fragment);
        if (this.shouldHandlePolicyViolation(nearestPolicy, fragment.getClass(), violation.getClass())) {
            this.handlePolicyViolation(nearestPolicy, violation);
        }
    }
    
    public final void setDefaultPolicy(@NotNull final Policy defaultPolicy) {
        Intrinsics.checkNotNullParameter((Object)defaultPolicy, "<set-?>");
        FragmentStrictMode.defaultPolicy = defaultPolicy;
    }
    
    @Metadata
    public enum Flag
    {
        private static final Flag[] $VALUES;
        
        DETECT_FRAGMENT_REUSE, 
        DETECT_FRAGMENT_TAG_USAGE, 
        DETECT_RETAIN_INSTANCE_USAGE, 
        DETECT_SET_USER_VISIBLE_HINT, 
        DETECT_TARGET_FRAGMENT_USAGE, 
        DETECT_WRONG_FRAGMENT_CONTAINER, 
        PENALTY_DEATH, 
        PENALTY_LOG;
        
        private static final /* synthetic */ Flag[] $values() {
            return new Flag[] { Flag.PENALTY_LOG, Flag.PENALTY_DEATH, Flag.DETECT_FRAGMENT_REUSE, Flag.DETECT_FRAGMENT_TAG_USAGE, Flag.DETECT_RETAIN_INSTANCE_USAGE, Flag.DETECT_SET_USER_VISIBLE_HINT, Flag.DETECT_TARGET_FRAGMENT_USAGE, Flag.DETECT_WRONG_FRAGMENT_CONTAINER };
        }
        
        static {
            $VALUES = $values();
        }
    }
    
    @Metadata
    public interface OnViolationListener
    {
        void onViolation(@NotNull final Violation p0);
    }
    
    @Metadata
    public static final class Policy
    {
        @NotNull
        public static final Companion Companion;
        @NotNull
        public static final Policy LAX;
        @NotNull
        private final Set<Flag> flags;
        private final OnViolationListener listener;
        @NotNull
        private final Map<String, Set<Class<? extends Violation>>> mAllowedViolations;
        
        static {
            Companion = new Companion(null);
            LAX = new Policy(SetsKt.O8(), null, MapsKt.\u3007\u3007888());
        }
        
        public Policy(@NotNull final Set<? extends Flag> flags, final OnViolationListener listener, @NotNull final Map<String, ? extends Set<Class<? extends Violation>>> map) {
            Intrinsics.checkNotNullParameter((Object)flags, "flags");
            Intrinsics.checkNotNullParameter((Object)map, "allowedViolations");
            this.flags = (Set<Flag>)flags;
            this.listener = listener;
            final LinkedHashMap mAllowedViolations = new LinkedHashMap();
            for (final Map.Entry entry : map.entrySet()) {
                mAllowedViolations.put(entry.getKey(), entry.getValue());
            }
            this.mAllowedViolations = mAllowedViolations;
        }
        
        @NotNull
        public final Set<Flag> getFlags$fragment_release() {
            return this.flags;
        }
        
        public final OnViolationListener getListener$fragment_release() {
            return this.listener;
        }
        
        @NotNull
        public final Map<String, Set<Class<? extends Violation>>> getMAllowedViolations$fragment_release() {
            return this.mAllowedViolations;
        }
        
        @Metadata
        public static final class Builder
        {
            @NotNull
            private final Set<Flag> flags;
            private OnViolationListener listener;
            @NotNull
            private final Map<String, Set<Class<? extends Violation>>> mAllowedViolations;
            
            public Builder() {
                this.flags = new LinkedHashSet<Flag>();
                this.mAllowedViolations = new LinkedHashMap<String, Set<Class<? extends Violation>>>();
            }
            
            @SuppressLint({ "BuilderSetStyle" })
            @NotNull
            public final Builder allowViolation(@NotNull final Class<? extends Fragment> clazz, @NotNull final Class<? extends Violation> clazz2) {
                Intrinsics.checkNotNullParameter((Object)clazz, "fragmentClass");
                Intrinsics.checkNotNullParameter((Object)clazz2, "violationClass");
                final String name = clazz.getName();
                Intrinsics.checkNotNullExpressionValue((Object)name, "fragmentClassString");
                return this.allowViolation(name, clazz2);
            }
            
            @SuppressLint({ "BuilderSetStyle" })
            @NotNull
            public final Builder allowViolation(@NotNull final String s, @NotNull final Class<? extends Violation> clazz) {
                Intrinsics.checkNotNullParameter((Object)s, "fragmentClass");
                Intrinsics.checkNotNullParameter((Object)clazz, "violationClass");
                Set set;
                if ((set = this.mAllowedViolations.get(s)) == null) {
                    set = new LinkedHashSet();
                }
                set.add(clazz);
                this.mAllowedViolations.put(s, set);
                return this;
            }
            
            @NotNull
            public final Policy build() {
                if (this.listener == null && !this.flags.contains(Flag.PENALTY_DEATH)) {
                    this.penaltyLog();
                }
                return new Policy(this.flags, this.listener, this.mAllowedViolations);
            }
            
            @SuppressLint({ "BuilderSetStyle" })
            @NotNull
            public final Builder detectFragmentReuse() {
                this.flags.add(Flag.DETECT_FRAGMENT_REUSE);
                return this;
            }
            
            @SuppressLint({ "BuilderSetStyle" })
            @NotNull
            public final Builder detectFragmentTagUsage() {
                this.flags.add(Flag.DETECT_FRAGMENT_TAG_USAGE);
                return this;
            }
            
            @SuppressLint({ "BuilderSetStyle" })
            @NotNull
            public final Builder detectRetainInstanceUsage() {
                this.flags.add(Flag.DETECT_RETAIN_INSTANCE_USAGE);
                return this;
            }
            
            @SuppressLint({ "BuilderSetStyle" })
            @NotNull
            public final Builder detectSetUserVisibleHint() {
                this.flags.add(Flag.DETECT_SET_USER_VISIBLE_HINT);
                return this;
            }
            
            @SuppressLint({ "BuilderSetStyle" })
            @NotNull
            public final Builder detectTargetFragmentUsage() {
                this.flags.add(Flag.DETECT_TARGET_FRAGMENT_USAGE);
                return this;
            }
            
            @SuppressLint({ "BuilderSetStyle" })
            @NotNull
            public final Builder detectWrongFragmentContainer() {
                this.flags.add(Flag.DETECT_WRONG_FRAGMENT_CONTAINER);
                return this;
            }
            
            @SuppressLint({ "BuilderSetStyle" })
            @NotNull
            public final Builder penaltyDeath() {
                this.flags.add(Flag.PENALTY_DEATH);
                return this;
            }
            
            @SuppressLint({ "BuilderSetStyle" })
            @NotNull
            public final Builder penaltyListener(@NotNull final OnViolationListener listener) {
                Intrinsics.checkNotNullParameter((Object)listener, "listener");
                this.listener = listener;
                return this;
            }
            
            @SuppressLint({ "BuilderSetStyle" })
            @NotNull
            public final Builder penaltyLog() {
                this.flags.add(Flag.PENALTY_LOG);
                return this;
            }
        }
        
        @Metadata
        public static final class Companion
        {
            private Companion() {
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app.strictmode;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import androidx.fragment.app.Fragment;
import kotlin.Metadata;

@Metadata
public final class GetTargetFragmentRequestCodeUsageViolation extends TargetFragmentUsageViolation
{
    public GetTargetFragmentRequestCodeUsageViolation(@NotNull final Fragment obj) {
        Intrinsics.checkNotNullParameter((Object)obj, "fragment");
        final StringBuilder sb = new StringBuilder();
        sb.append("Attempting to get target request code from fragment ");
        sb.append(obj);
        super(obj, sb.toString());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app.strictmode;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import androidx.fragment.app.Fragment;
import android.view.ViewGroup;
import kotlin.Metadata;

@Metadata
public final class FragmentTagUsageViolation extends Violation
{
    private final ViewGroup parentContainer;
    
    public FragmentTagUsageViolation(@NotNull final Fragment obj, final ViewGroup viewGroup) {
        Intrinsics.checkNotNullParameter((Object)obj, "fragment");
        final StringBuilder sb = new StringBuilder();
        sb.append("Attempting to use <fragment> tag to add fragment ");
        sb.append(obj);
        sb.append(" to container ");
        sb.append(viewGroup);
        super(obj, sb.toString());
        this.parentContainer = viewGroup;
    }
    
    public final ViewGroup getParentContainer() {
        return this.parentContainer;
    }
}

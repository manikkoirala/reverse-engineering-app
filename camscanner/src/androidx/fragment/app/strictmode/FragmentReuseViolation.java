// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app.strictmode;

import kotlin.jvm.internal.Intrinsics;
import androidx.fragment.app.Fragment;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class FragmentReuseViolation extends Violation
{
    @NotNull
    private final String previousFragmentId;
    
    public FragmentReuseViolation(@NotNull final Fragment obj, @NotNull final String s) {
        Intrinsics.checkNotNullParameter((Object)obj, "fragment");
        Intrinsics.checkNotNullParameter((Object)s, "previousFragmentId");
        final StringBuilder sb = new StringBuilder();
        sb.append("Attempting to reuse fragment ");
        sb.append(obj);
        sb.append(" with previous ID ");
        sb.append(s);
        super(obj, sb.toString());
        this.previousFragmentId = s;
    }
    
    @NotNull
    public final String getPreviousFragmentId() {
        return this.previousFragmentId;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app.strictmode;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import androidx.fragment.app.Fragment;
import kotlin.Metadata;

@Metadata
public final class SetTargetFragmentUsageViolation extends TargetFragmentUsageViolation
{
    private final int requestCode;
    @NotNull
    private final Fragment targetFragment;
    
    public SetTargetFragmentUsageViolation(@NotNull final Fragment obj, @NotNull final Fragment fragment, final int n) {
        Intrinsics.checkNotNullParameter((Object)obj, "fragment");
        Intrinsics.checkNotNullParameter((Object)fragment, "targetFragment");
        final StringBuilder sb = new StringBuilder();
        sb.append("Attempting to set target fragment ");
        sb.append(fragment);
        sb.append(" with request code ");
        sb.append(n);
        sb.append(" for fragment ");
        sb.append(obj);
        super(obj, sb.toString());
        this.targetFragment = fragment;
        this.requestCode = n;
    }
    
    public final int getRequestCode() {
        return this.requestCode;
    }
    
    @NotNull
    public final Fragment getTargetFragment() {
        return this.targetFragment;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app.strictmode;

import kotlin.jvm.internal.Intrinsics;
import androidx.fragment.app.Fragment;
import org.jetbrains.annotations.NotNull;
import android.view.ViewGroup;
import kotlin.Metadata;

@Metadata
public final class WrongFragmentContainerViolation extends Violation
{
    @NotNull
    private final ViewGroup container;
    
    public WrongFragmentContainerViolation(@NotNull final Fragment obj, @NotNull final ViewGroup viewGroup) {
        Intrinsics.checkNotNullParameter((Object)obj, "fragment");
        Intrinsics.checkNotNullParameter((Object)viewGroup, "container");
        final StringBuilder sb = new StringBuilder();
        sb.append("Attempting to add fragment ");
        sb.append(obj);
        sb.append(" to container ");
        sb.append(viewGroup);
        sb.append(" which is not a FragmentContainerView");
        super(obj, sb.toString());
        this.container = viewGroup;
    }
    
    @NotNull
    public final ViewGroup getContainer() {
        return this.container;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app.strictmode;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import androidx.fragment.app.Fragment;
import kotlin.Metadata;

@Metadata
public final class SetRetainInstanceUsageViolation extends RetainInstanceUsageViolation
{
    public SetRetainInstanceUsageViolation(@NotNull final Fragment obj) {
        Intrinsics.checkNotNullParameter((Object)obj, "fragment");
        final StringBuilder sb = new StringBuilder();
        sb.append("Attempting to set retain instance for fragment ");
        sb.append(obj);
        super(obj, sb.toString());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import kotlin.LazyKt;
import kotlin.LazyThreadSafetyMode;
import androidx.lifecycle.ViewModelLazy;
import org.jetbrains.annotations.NotNull;
import androidx.annotation.MainThread;
import androidx.lifecycle.viewmodel.CreationExtras;
import androidx.lifecycle.ViewModelStore;
import kotlin.reflect.KClass;
import kotlin.jvm.internal.Reflection;
import kotlin.jvm.internal.Intrinsics;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import kotlin.jvm.functions.Function0;
import androidx.lifecycle.ViewModelStoreOwner;
import kotlin.Lazy;
import kotlin.Metadata;

@Metadata
public final class FragmentViewModelLazyKt
{
    @MainThread
    @NotNull
    public static final <VM extends ViewModel> Lazy<VM> createViewModelLazy(@NotNull final Fragment fragment, @NotNull final KClass<VM> kClass, @NotNull final Function0<? extends ViewModelStore> function0, @NotNull final Function0<? extends CreationExtras> function2, final Function0<? extends ViewModelProvider.Factory> function3) {
        Intrinsics.checkNotNullParameter((Object)fragment, "<this>");
        Intrinsics.checkNotNullParameter((Object)kClass, "viewModelClass");
        Intrinsics.checkNotNullParameter((Object)function0, "storeProducer");
        Intrinsics.checkNotNullParameter((Object)function2, "extrasProducer");
        Object o = function3;
        if (function3 == null) {
            o = new FragmentViewModelLazyKt$createViewModelLazy$factoryPromise.FragmentViewModelLazyKt$createViewModelLazy$factoryPromise$1(fragment);
        }
        return (Lazy<VM>)new ViewModelLazy((kotlin.reflect.KClass<ViewModel>)kClass, function0, (Function0<? extends ViewModelProvider.Factory>)o, function2);
    }
    
    private static final ViewModelStoreOwner viewModels$lambda-0(final Lazy<? extends ViewModelStoreOwner> lazy) {
        return (ViewModelStoreOwner)lazy.getValue();
    }
    
    private static final ViewModelStoreOwner viewModels$lambda-1(final Lazy<? extends ViewModelStoreOwner> lazy) {
        return (ViewModelStoreOwner)lazy.getValue();
    }
}

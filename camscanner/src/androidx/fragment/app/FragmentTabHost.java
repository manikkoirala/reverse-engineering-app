// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.view.AbsSavedState;
import android.view.ViewGroup;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import android.view.View$BaseSavedState;
import android.annotation.SuppressLint;
import android.os.Parcelable;
import android.widget.TabHost$TabContentFactory;
import android.os.Bundle;
import android.widget.TabHost$TabSpec;
import android.content.res.TypedArray;
import android.widget.LinearLayout$LayoutParams;
import android.widget.TabWidget;
import android.view.ViewGroup$LayoutParams;
import android.view.View;
import android.widget.FrameLayout$LayoutParams;
import android.widget.LinearLayout;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import androidx.annotation.NonNull;
import java.util.ArrayList;
import android.widget.FrameLayout;
import android.content.Context;
import android.widget.TabHost$OnTabChangeListener;
import android.widget.TabHost;

@Deprecated
public class FragmentTabHost extends TabHost implements TabHost$OnTabChangeListener
{
    private boolean mAttached;
    private int mContainerId;
    private Context mContext;
    private FragmentManager mFragmentManager;
    private TabInfo mLastTab;
    private TabHost$OnTabChangeListener mOnTabChangeListener;
    private FrameLayout mRealTabContent;
    private final ArrayList<TabInfo> mTabs;
    
    @Deprecated
    public FragmentTabHost(@NonNull final Context context) {
        super(context, (AttributeSet)null);
        this.mTabs = new ArrayList<TabInfo>();
        this.initFragmentTabHost(context, null);
    }
    
    @Deprecated
    public FragmentTabHost(@NonNull final Context context, @Nullable final AttributeSet set) {
        super(context, set);
        this.mTabs = new ArrayList<TabInfo>();
        this.initFragmentTabHost(context, set);
    }
    
    @Nullable
    private FragmentTransaction doTabChanged(@Nullable final String s, @Nullable final FragmentTransaction fragmentTransaction) {
        final TabInfo tabInfoForTag = this.getTabInfoForTag(s);
        FragmentTransaction beginTransaction = fragmentTransaction;
        if (this.mLastTab != tabInfoForTag) {
            if ((beginTransaction = fragmentTransaction) == null) {
                beginTransaction = this.mFragmentManager.beginTransaction();
            }
            final TabInfo mLastTab = this.mLastTab;
            if (mLastTab != null) {
                final Fragment fragment = mLastTab.fragment;
                if (fragment != null) {
                    beginTransaction.detach(fragment);
                }
            }
            if (tabInfoForTag != null) {
                final Fragment fragment2 = tabInfoForTag.fragment;
                if (fragment2 == null) {
                    (tabInfoForTag.fragment = this.mFragmentManager.getFragmentFactory().instantiate(this.mContext.getClassLoader(), tabInfoForTag.clss.getName())).setArguments(tabInfoForTag.args);
                    beginTransaction.add(this.mContainerId, tabInfoForTag.fragment, tabInfoForTag.tag);
                }
                else {
                    beginTransaction.attach(fragment2);
                }
            }
            this.mLastTab = tabInfoForTag;
        }
        return beginTransaction;
    }
    
    private void ensureContent() {
        if (this.mRealTabContent == null && (this.mRealTabContent = (FrameLayout)((View)this).findViewById(this.mContainerId)) == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("No tab content FrameLayout found for id ");
            sb.append(this.mContainerId);
            throw new IllegalStateException(sb.toString());
        }
    }
    
    private void ensureHierarchy(final Context context) {
        if (((View)this).findViewById(16908307) == null) {
            final LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setOrientation(1);
            ((ViewGroup)this).addView((View)linearLayout, (ViewGroup$LayoutParams)new FrameLayout$LayoutParams(-1, -1));
            final TabWidget tabWidget = new TabWidget(context);
            ((View)tabWidget).setId(16908307);
            ((LinearLayout)tabWidget).setOrientation(0);
            ((ViewGroup)linearLayout).addView((View)tabWidget, (ViewGroup$LayoutParams)new LinearLayout$LayoutParams(-1, -2, 0.0f));
            final FrameLayout frameLayout = new FrameLayout(context);
            ((View)frameLayout).setId(16908305);
            ((ViewGroup)linearLayout).addView((View)frameLayout, (ViewGroup$LayoutParams)new LinearLayout$LayoutParams(0, 0, 0.0f));
            final FrameLayout mRealTabContent = new FrameLayout(context);
            ((View)(this.mRealTabContent = mRealTabContent)).setId(this.mContainerId);
            ((ViewGroup)linearLayout).addView((View)mRealTabContent, (ViewGroup$LayoutParams)new LinearLayout$LayoutParams(-1, 0, 1.0f));
        }
    }
    
    @Nullable
    private TabInfo getTabInfoForTag(final String anObject) {
        for (int size = this.mTabs.size(), i = 0; i < size; ++i) {
            final TabInfo tabInfo = this.mTabs.get(i);
            if (tabInfo.tag.equals(anObject)) {
                return tabInfo;
            }
        }
        return null;
    }
    
    private void initFragmentTabHost(final Context context, final AttributeSet set) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, new int[] { 16842995 }, 0, 0);
        this.mContainerId = obtainStyledAttributes.getResourceId(0, 0);
        obtainStyledAttributes.recycle();
        super.setOnTabChangedListener((TabHost$OnTabChangeListener)this);
    }
    
    @Deprecated
    public void addTab(@NonNull final TabHost$TabSpec tabHost$TabSpec, @NonNull final Class<?> clazz, @Nullable final Bundle bundle) {
        tabHost$TabSpec.setContent((TabHost$TabContentFactory)new DummyTabFactory(this.mContext));
        final String tag = tabHost$TabSpec.getTag();
        final TabInfo e = new TabInfo(tag, clazz, bundle);
        if (this.mAttached) {
            final Fragment fragmentByTag = this.mFragmentManager.findFragmentByTag(tag);
            if ((e.fragment = fragmentByTag) != null && !fragmentByTag.isDetached()) {
                final FragmentTransaction beginTransaction = this.mFragmentManager.beginTransaction();
                beginTransaction.detach(e.fragment);
                beginTransaction.commit();
            }
        }
        this.mTabs.add(e);
        this.addTab(tabHost$TabSpec);
    }
    
    @Deprecated
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        final String currentTabTag = this.getCurrentTabTag();
        final int size = this.mTabs.size();
        FragmentTransaction fragmentTransaction = null;
        FragmentTransaction beginTransaction;
        for (int i = 0; i < size; ++i, fragmentTransaction = beginTransaction) {
            final TabInfo mLastTab = this.mTabs.get(i);
            final Fragment fragmentByTag = this.mFragmentManager.findFragmentByTag(mLastTab.tag);
            mLastTab.fragment = fragmentByTag;
            beginTransaction = fragmentTransaction;
            if (fragmentByTag != null) {
                beginTransaction = fragmentTransaction;
                if (!fragmentByTag.isDetached()) {
                    if (mLastTab.tag.equals(currentTabTag)) {
                        this.mLastTab = mLastTab;
                        beginTransaction = fragmentTransaction;
                    }
                    else {
                        if ((beginTransaction = fragmentTransaction) == null) {
                            beginTransaction = this.mFragmentManager.beginTransaction();
                        }
                        beginTransaction.detach(mLastTab.fragment);
                    }
                }
            }
        }
        this.mAttached = true;
        final FragmentTransaction doTabChanged = this.doTabChanged(currentTabTag, fragmentTransaction);
        if (doTabChanged != null) {
            doTabChanged.commit();
            this.mFragmentManager.executePendingTransactions();
        }
    }
    
    @Deprecated
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mAttached = false;
    }
    
    @Deprecated
    protected void onRestoreInstanceState(@SuppressLint({ "UnknownNullness" }) final Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        final SavedState savedState = (SavedState)parcelable;
        super.onRestoreInstanceState(((AbsSavedState)savedState).getSuperState());
        this.setCurrentTabByTag(savedState.curTab);
    }
    
    @Deprecated
    @NonNull
    protected Parcelable onSaveInstanceState() {
        final SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.curTab = this.getCurrentTabTag();
        return (Parcelable)savedState;
    }
    
    @Deprecated
    public void onTabChanged(@Nullable final String s) {
        if (this.mAttached) {
            final FragmentTransaction doTabChanged = this.doTabChanged(s, null);
            if (doTabChanged != null) {
                doTabChanged.commit();
            }
        }
        final TabHost$OnTabChangeListener mOnTabChangeListener = this.mOnTabChangeListener;
        if (mOnTabChangeListener != null) {
            mOnTabChangeListener.onTabChanged(s);
        }
    }
    
    @Deprecated
    public void setOnTabChangedListener(@Nullable final TabHost$OnTabChangeListener mOnTabChangeListener) {
        this.mOnTabChangeListener = mOnTabChangeListener;
    }
    
    @Deprecated
    public void setup() {
        throw new IllegalStateException("Must call setup() that takes a Context and FragmentManager");
    }
    
    @Deprecated
    public void setup(@NonNull final Context mContext, @NonNull final FragmentManager mFragmentManager) {
        this.ensureHierarchy(mContext);
        super.setup();
        this.mContext = mContext;
        this.mFragmentManager = mFragmentManager;
        this.ensureContent();
    }
    
    @Deprecated
    public void setup(@NonNull final Context mContext, @NonNull final FragmentManager mFragmentManager, final int n) {
        this.ensureHierarchy(mContext);
        super.setup();
        this.mContext = mContext;
        this.mFragmentManager = mFragmentManager;
        this.mContainerId = n;
        this.ensureContent();
        ((View)this.mRealTabContent).setId(n);
        if (((View)this).getId() == -1) {
            ((View)this).setId(16908306);
        }
    }
    
    static class DummyTabFactory implements TabHost$TabContentFactory
    {
        private final Context mContext;
        
        public DummyTabFactory(final Context mContext) {
            this.mContext = mContext;
        }
        
        public View createTabContent(final String s) {
            final View view = new View(this.mContext);
            view.setMinimumWidth(0);
            view.setMinimumHeight(0);
            return view;
        }
    }
    
    static class SavedState extends View$BaseSavedState
    {
        public static final Parcelable$Creator<SavedState> CREATOR;
        String curTab;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$Creator<SavedState>() {
                public SavedState createFromParcel(final Parcel parcel) {
                    return new SavedState(parcel);
                }
                
                public SavedState[] newArray(final int n) {
                    return new SavedState[n];
                }
            };
        }
        
        SavedState(final Parcel parcel) {
            super(parcel);
            this.curTab = parcel.readString();
        }
        
        SavedState(final Parcelable parcelable) {
            super(parcelable);
        }
        
        @NonNull
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("FragmentTabHost.SavedState{");
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            sb.append(" curTab=");
            sb.append(this.curTab);
            sb.append("}");
            return sb.toString();
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeString(this.curTab);
        }
    }
    
    static final class TabInfo
    {
        @Nullable
        final Bundle args;
        @NonNull
        final Class<?> clss;
        Fragment fragment;
        @NonNull
        final String tag;
        
        TabInfo(@NonNull final String tag, @NonNull final Class<?> clss, @Nullable final Bundle args) {
            this.tag = tag;
            this.clss = clss;
            this.args = args;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import java.util.List;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.IdRes;
import java.io.PrintWriter;
import androidx.annotation.Nullable;
import java.io.FileDescriptor;
import java.util.Iterator;
import java.util.Collection;
import java.util.Collections;
import androidx.annotation.NonNull;
import java.util.ArrayList;
import java.util.HashMap;

class FragmentStore
{
    private static final String TAG = "FragmentManager";
    private final HashMap<String, FragmentStateManager> mActive;
    private final ArrayList<Fragment> mAdded;
    private FragmentManagerViewModel mNonConfig;
    private final HashMap<String, FragmentState> mSavedState;
    
    FragmentStore() {
        this.mAdded = new ArrayList<Fragment>();
        this.mActive = new HashMap<String, FragmentStateManager>();
        this.mSavedState = new HashMap<String, FragmentState>();
    }
    
    void addFragment(@NonNull final Fragment obj) {
        if (!this.mAdded.contains(obj)) {
            synchronized (this.mAdded) {
                this.mAdded.add(obj);
                monitorexit(this.mAdded);
                obj.mAdded = true;
                return;
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment already added: ");
        sb.append(obj);
        throw new IllegalStateException(sb.toString());
    }
    
    void burpActive() {
        this.mActive.values().removeAll(Collections.singleton((Object)null));
    }
    
    boolean containsActiveFragment(@NonNull final String key) {
        return this.mActive.get(key) != null;
    }
    
    void dispatchStateChange(final int fragmentManagerState) {
        for (final FragmentStateManager fragmentStateManager : this.mActive.values()) {
            if (fragmentStateManager != null) {
                fragmentStateManager.setFragmentManagerState(fragmentManagerState);
            }
        }
    }
    
    void dump(@NonNull final String s, @Nullable final FileDescriptor fileDescriptor, @NonNull final PrintWriter printWriter, @Nullable final String[] array) {
        final StringBuilder sb = new StringBuilder();
        sb.append(s);
        sb.append("    ");
        final String string = sb.toString();
        if (!this.mActive.isEmpty()) {
            printWriter.print(s);
            printWriter.println("Active Fragments:");
            for (final FragmentStateManager fragmentStateManager : this.mActive.values()) {
                printWriter.print(s);
                if (fragmentStateManager != null) {
                    final Fragment fragment = fragmentStateManager.getFragment();
                    printWriter.println(fragment);
                    fragment.dump(string, fileDescriptor, printWriter, array);
                }
                else {
                    printWriter.println("null");
                }
            }
        }
        final int size = this.mAdded.size();
        if (size > 0) {
            printWriter.print(s);
            printWriter.println("Added Fragments:");
            for (int i = 0; i < size; ++i) {
                final Fragment fragment2 = this.mAdded.get(i);
                printWriter.print(s);
                printWriter.print("  #");
                printWriter.print(i);
                printWriter.print(": ");
                printWriter.println(fragment2.toString());
            }
        }
    }
    
    @Nullable
    Fragment findActiveFragment(@NonNull final String key) {
        final FragmentStateManager fragmentStateManager = this.mActive.get(key);
        if (fragmentStateManager != null) {
            return fragmentStateManager.getFragment();
        }
        return null;
    }
    
    @Nullable
    Fragment findFragmentById(@IdRes final int n) {
        for (int i = this.mAdded.size() - 1; i >= 0; --i) {
            final Fragment fragment = this.mAdded.get(i);
            if (fragment != null && fragment.mFragmentId == n) {
                return fragment;
            }
        }
        for (final FragmentStateManager fragmentStateManager : this.mActive.values()) {
            if (fragmentStateManager != null) {
                final Fragment fragment2 = fragmentStateManager.getFragment();
                if (fragment2.mFragmentId == n) {
                    return fragment2;
                }
                continue;
            }
        }
        return null;
    }
    
    @Nullable
    Fragment findFragmentByTag(@Nullable final String s) {
        if (s != null) {
            for (int i = this.mAdded.size() - 1; i >= 0; --i) {
                final Fragment fragment = this.mAdded.get(i);
                if (fragment != null && s.equals(fragment.mTag)) {
                    return fragment;
                }
            }
        }
        if (s != null) {
            for (final FragmentStateManager fragmentStateManager : this.mActive.values()) {
                if (fragmentStateManager != null) {
                    final Fragment fragment2 = fragmentStateManager.getFragment();
                    if (s.equals(fragment2.mTag)) {
                        return fragment2;
                    }
                    continue;
                }
            }
        }
        return null;
    }
    
    @Nullable
    Fragment findFragmentByWho(@NonNull final String s) {
        for (final FragmentStateManager fragmentStateManager : this.mActive.values()) {
            if (fragmentStateManager != null) {
                final Fragment fragmentByWho = fragmentStateManager.getFragment().findFragmentByWho(s);
                if (fragmentByWho != null) {
                    return fragmentByWho;
                }
                continue;
            }
        }
        return null;
    }
    
    int findFragmentIndexInContainer(@NonNull Fragment o) {
        final ViewGroup mContainer = o.mContainer;
        if (mContainer == null) {
            return -1;
        }
        final int index = this.mAdded.indexOf(o);
        int index2 = index - 1;
        int index3;
        while (true) {
            index3 = index;
            if (index2 < 0) {
                break;
            }
            o = this.mAdded.get(index2);
            if (o.mContainer == mContainer) {
                final View mView = o.mView;
                if (mView != null) {
                    return mContainer.indexOfChild(mView) + 1;
                }
            }
            --index2;
        }
        while (++index3 < this.mAdded.size()) {
            o = this.mAdded.get(index3);
            if (o.mContainer == mContainer) {
                final View mView2 = o.mView;
                if (mView2 != null) {
                    return mContainer.indexOfChild(mView2);
                }
                continue;
            }
        }
        return -1;
    }
    
    int getActiveFragmentCount() {
        return this.mActive.size();
    }
    
    @NonNull
    List<FragmentStateManager> getActiveFragmentStateManagers() {
        final ArrayList list = new ArrayList();
        for (final FragmentStateManager e : this.mActive.values()) {
            if (e != null) {
                list.add(e);
            }
        }
        return list;
    }
    
    @NonNull
    List<Fragment> getActiveFragments() {
        final ArrayList list = new ArrayList();
        for (final FragmentStateManager fragmentStateManager : this.mActive.values()) {
            if (fragmentStateManager != null) {
                list.add(fragmentStateManager.getFragment());
            }
            else {
                list.add(null);
            }
        }
        return list;
    }
    
    @NonNull
    ArrayList<FragmentState> getAllSavedState() {
        return new ArrayList<FragmentState>(this.mSavedState.values());
    }
    
    @Nullable
    FragmentStateManager getFragmentStateManager(@NonNull final String key) {
        return this.mActive.get(key);
    }
    
    @NonNull
    List<Fragment> getFragments() {
        if (this.mAdded.isEmpty()) {
            return Collections.emptyList();
        }
        synchronized (this.mAdded) {
            return new ArrayList<Fragment>(this.mAdded);
        }
    }
    
    FragmentManagerViewModel getNonConfig() {
        return this.mNonConfig;
    }
    
    @Nullable
    FragmentState getSavedState(@NonNull final String key) {
        return this.mSavedState.get(key);
    }
    
    void makeActive(@NonNull final FragmentStateManager value) {
        final Fragment fragment = value.getFragment();
        if (this.containsActiveFragment(fragment.mWho)) {
            return;
        }
        this.mActive.put(fragment.mWho, value);
        if (fragment.mRetainInstanceChangedWhileDetached) {
            if (fragment.mRetainInstance) {
                this.mNonConfig.addRetainedFragment(fragment);
            }
            else {
                this.mNonConfig.removeRetainedFragment(fragment);
            }
            fragment.mRetainInstanceChangedWhileDetached = false;
        }
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Added fragment to active set ");
            sb.append(fragment);
        }
    }
    
    void makeInactive(@NonNull final FragmentStateManager fragmentStateManager) {
        final Fragment fragment = fragmentStateManager.getFragment();
        if (fragment.mRetainInstance) {
            this.mNonConfig.removeRetainedFragment(fragment);
        }
        if (this.mActive.put(fragment.mWho, null) == null) {
            return;
        }
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Removed fragment from active set ");
            sb.append(fragment);
        }
    }
    
    void moveToExpectedState() {
        final Iterator<Fragment> iterator = this.mAdded.iterator();
        while (iterator.hasNext()) {
            final FragmentStateManager fragmentStateManager = this.mActive.get(iterator.next().mWho);
            if (fragmentStateManager != null) {
                fragmentStateManager.moveToExpectedState();
            }
        }
        for (final FragmentStateManager fragmentStateManager2 : this.mActive.values()) {
            if (fragmentStateManager2 != null) {
                fragmentStateManager2.moveToExpectedState();
                final Fragment fragment = fragmentStateManager2.getFragment();
                if (!fragment.mRemoving || fragment.isInBackStack()) {
                    continue;
                }
                if (fragment.mBeingSaved && !this.mSavedState.containsKey(fragment.mWho)) {
                    fragmentStateManager2.saveState();
                }
                this.makeInactive(fragmentStateManager2);
            }
        }
    }
    
    void removeFragment(@NonNull final Fragment o) {
        synchronized (this.mAdded) {
            this.mAdded.remove(o);
            monitorexit(this.mAdded);
            o.mAdded = false;
        }
    }
    
    void resetActiveFragments() {
        this.mActive.clear();
    }
    
    void restoreAddedFragments(@Nullable final List<String> list) {
        this.mAdded.clear();
        if (list != null) {
            for (final String s : list) {
                final Fragment activeFragment = this.findActiveFragment(s);
                if (activeFragment == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("No instantiated fragment for (");
                    sb.append(s);
                    sb.append(")");
                    throw new IllegalStateException(sb.toString());
                }
                if (FragmentManager.isLoggingEnabled(2)) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("restoreSaveState: added (");
                    sb2.append(s);
                    sb2.append("): ");
                    sb2.append(activeFragment);
                }
                this.addFragment(activeFragment);
            }
        }
    }
    
    void restoreSaveState(@NonNull final ArrayList<FragmentState> list) {
        this.mSavedState.clear();
        for (final FragmentState value : list) {
            this.mSavedState.put(value.mWho, value);
        }
    }
    
    @NonNull
    ArrayList<String> saveActiveFragments() {
        final ArrayList list = new ArrayList(this.mActive.size());
        for (final FragmentStateManager fragmentStateManager : this.mActive.values()) {
            if (fragmentStateManager != null) {
                final Fragment fragment = fragmentStateManager.getFragment();
                fragmentStateManager.saveState();
                list.add(fragment.mWho);
                if (!FragmentManager.isLoggingEnabled(2)) {
                    continue;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Saved state of ");
                sb.append(fragment);
                sb.append(": ");
                sb.append(fragment.mSavedFragmentState);
            }
        }
        return list;
    }
    
    @Nullable
    ArrayList<String> saveAddedFragments() {
        synchronized (this.mAdded) {
            if (this.mAdded.isEmpty()) {
                return null;
            }
            final ArrayList<String> list = new ArrayList<String>(this.mAdded.size());
            for (final Fragment obj : this.mAdded) {
                list.add(obj.mWho);
                if (FragmentManager.isLoggingEnabled(2)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("saveAllState: adding fragment (");
                    sb.append(obj.mWho);
                    sb.append("): ");
                    sb.append(obj);
                }
            }
            return list;
        }
    }
    
    void setNonConfig(@NonNull final FragmentManagerViewModel mNonConfig) {
        this.mNonConfig = mNonConfig;
    }
    
    @Nullable
    FragmentState setSavedState(@NonNull final String s, @Nullable final FragmentState value) {
        if (value != null) {
            return this.mSavedState.put(s, value);
        }
        return this.mSavedState.remove(s);
    }
}

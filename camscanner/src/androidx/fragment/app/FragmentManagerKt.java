// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class FragmentManagerKt
{
    public static final void commit(@NotNull final FragmentManager fragmentManager, final boolean b, @NotNull final Function1<? super FragmentTransaction, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)fragmentManager, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "body");
        final FragmentTransaction beginTransaction = fragmentManager.beginTransaction();
        Intrinsics.checkNotNullExpressionValue((Object)beginTransaction, "beginTransaction()");
        function1.invoke((Object)beginTransaction);
        if (b) {
            beginTransaction.commitAllowingStateLoss();
        }
        else {
            beginTransaction.commit();
        }
    }
    
    public static final void commitNow(@NotNull final FragmentManager fragmentManager, final boolean b, @NotNull final Function1<? super FragmentTransaction, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)fragmentManager, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "body");
        final FragmentTransaction beginTransaction = fragmentManager.beginTransaction();
        Intrinsics.checkNotNullExpressionValue((Object)beginTransaction, "beginTransaction()");
        function1.invoke((Object)beginTransaction);
        if (b) {
            beginTransaction.commitNowAllowingStateLoss();
        }
        else {
            beginTransaction.commitNow();
        }
    }
    
    public static final void transaction(@NotNull final FragmentManager fragmentManager, final boolean b, final boolean b2, @NotNull final Function1<? super FragmentTransaction, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)fragmentManager, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "body");
        final FragmentTransaction beginTransaction = fragmentManager.beginTransaction();
        Intrinsics.checkNotNullExpressionValue((Object)beginTransaction, "beginTransaction()");
        function1.invoke((Object)beginTransaction);
        if (b) {
            if (b2) {
                beginTransaction.commitNowAllowingStateLoss();
            }
            else {
                beginTransaction.commitNow();
            }
        }
        else if (b2) {
            beginTransaction.commitAllowingStateLoss();
        }
        else {
            beginTransaction.commit();
        }
    }
}

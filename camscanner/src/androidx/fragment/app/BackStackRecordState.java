// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import java.util.List;
import java.util.Map;
import androidx.lifecycle.Lifecycle;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.os.Parcel;
import java.util.ArrayList;
import android.os.Parcelable$Creator;
import android.annotation.SuppressLint;
import android.os.Parcelable;

@SuppressLint({ "BanParcelableUsage" })
final class BackStackRecordState implements Parcelable
{
    public static final Parcelable$Creator<BackStackRecordState> CREATOR;
    private static final String TAG = "FragmentManager";
    final int mBreadCrumbShortTitleRes;
    final CharSequence mBreadCrumbShortTitleText;
    final int mBreadCrumbTitleRes;
    final CharSequence mBreadCrumbTitleText;
    final int[] mCurrentMaxLifecycleStates;
    final ArrayList<String> mFragmentWhos;
    final int mIndex;
    final String mName;
    final int[] mOldMaxLifecycleStates;
    final int[] mOps;
    final boolean mReorderingAllowed;
    final ArrayList<String> mSharedElementSourceNames;
    final ArrayList<String> mSharedElementTargetNames;
    final int mTransition;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<BackStackRecordState>() {
            public BackStackRecordState createFromParcel(final Parcel parcel) {
                return new BackStackRecordState(parcel);
            }
            
            public BackStackRecordState[] newArray(final int n) {
                return new BackStackRecordState[n];
            }
        };
    }
    
    BackStackRecordState(final Parcel parcel) {
        this.mOps = parcel.createIntArray();
        this.mFragmentWhos = parcel.createStringArrayList();
        this.mOldMaxLifecycleStates = parcel.createIntArray();
        this.mCurrentMaxLifecycleStates = parcel.createIntArray();
        this.mTransition = parcel.readInt();
        this.mName = parcel.readString();
        this.mIndex = parcel.readInt();
        this.mBreadCrumbTitleRes = parcel.readInt();
        this.mBreadCrumbTitleText = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.mBreadCrumbShortTitleRes = parcel.readInt();
        this.mBreadCrumbShortTitleText = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.mSharedElementSourceNames = parcel.createStringArrayList();
        this.mSharedElementTargetNames = parcel.createStringArrayList();
        this.mReorderingAllowed = (parcel.readInt() != 0);
    }
    
    BackStackRecordState(final BackStackRecord backStackRecord) {
        final int size = backStackRecord.mOps.size();
        this.mOps = new int[size * 6];
        if (backStackRecord.mAddToBackStack) {
            this.mFragmentWhos = new ArrayList<String>(size);
            this.mOldMaxLifecycleStates = new int[size];
            this.mCurrentMaxLifecycleStates = new int[size];
            for (int i = 0, n = 0; i < size; ++i, ++n) {
                final FragmentTransaction.Op op = backStackRecord.mOps.get(i);
                final int[] mOps = this.mOps;
                final int n2 = n + 1;
                mOps[n] = op.mCmd;
                final ArrayList<String> mFragmentWhos = this.mFragmentWhos;
                final Fragment mFragment = op.mFragment;
                String mWho;
                if (mFragment != null) {
                    mWho = mFragment.mWho;
                }
                else {
                    mWho = null;
                }
                mFragmentWhos.add(mWho);
                final int[] mOps2 = this.mOps;
                final int n3 = n2 + 1;
                mOps2[n2] = (op.mFromExpandedOp ? 1 : 0);
                final int n4 = n3 + 1;
                mOps2[n3] = op.mEnterAnim;
                final int n5 = n4 + 1;
                mOps2[n4] = op.mExitAnim;
                n = n5 + 1;
                mOps2[n5] = op.mPopEnterAnim;
                mOps2[n] = op.mPopExitAnim;
                this.mOldMaxLifecycleStates[i] = op.mOldMaxState.ordinal();
                this.mCurrentMaxLifecycleStates[i] = op.mCurrentMaxState.ordinal();
            }
            this.mTransition = backStackRecord.mTransition;
            this.mName = backStackRecord.mName;
            this.mIndex = backStackRecord.mIndex;
            this.mBreadCrumbTitleRes = backStackRecord.mBreadCrumbTitleRes;
            this.mBreadCrumbTitleText = backStackRecord.mBreadCrumbTitleText;
            this.mBreadCrumbShortTitleRes = backStackRecord.mBreadCrumbShortTitleRes;
            this.mBreadCrumbShortTitleText = backStackRecord.mBreadCrumbShortTitleText;
            this.mSharedElementSourceNames = backStackRecord.mSharedElementSourceNames;
            this.mSharedElementTargetNames = backStackRecord.mSharedElementTargetNames;
            this.mReorderingAllowed = backStackRecord.mReorderingAllowed;
            return;
        }
        throw new IllegalStateException("Not on back stack");
    }
    
    private void fillInBackStackRecord(@NonNull final BackStackRecord obj) {
        int n = 0;
        int i = 0;
        while (true) {
            final int length = this.mOps.length;
            boolean mFromExpandedOp = true;
            if (n >= length) {
                break;
            }
            final FragmentTransaction.Op op = new FragmentTransaction.Op();
            final int[] mOps = this.mOps;
            final int n2 = n + 1;
            op.mCmd = mOps[n];
            if (FragmentManager.isLoggingEnabled(2)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Instantiate ");
                sb.append(obj);
                sb.append(" op #");
                sb.append(i);
                sb.append(" base fragment #");
                sb.append(this.mOps[n2]);
            }
            op.mOldMaxState = Lifecycle.State.values()[this.mOldMaxLifecycleStates[i]];
            op.mCurrentMaxState = Lifecycle.State.values()[this.mCurrentMaxLifecycleStates[i]];
            final int[] mOps2 = this.mOps;
            final int n3 = n2 + 1;
            if (mOps2[n2] == 0) {
                mFromExpandedOp = false;
            }
            op.mFromExpandedOp = mFromExpandedOp;
            final int n4 = n3 + 1;
            final int n5 = mOps2[n3];
            op.mEnterAnim = n5;
            final int n6 = n4 + 1;
            final int n7 = mOps2[n4];
            op.mExitAnim = n7;
            final int n8 = n6 + 1;
            final int n9 = mOps2[n6];
            op.mPopEnterAnim = n9;
            final int n10 = mOps2[n8];
            op.mPopExitAnim = n10;
            obj.mEnterAnim = n5;
            obj.mExitAnim = n7;
            obj.mPopEnterAnim = n9;
            obj.mPopExitAnim = n10;
            obj.addOp(op);
            ++i;
            n = n8 + 1;
        }
        obj.mTransition = this.mTransition;
        obj.mName = this.mName;
        obj.mAddToBackStack = true;
        obj.mBreadCrumbTitleRes = this.mBreadCrumbTitleRes;
        obj.mBreadCrumbTitleText = this.mBreadCrumbTitleText;
        obj.mBreadCrumbShortTitleRes = this.mBreadCrumbShortTitleRes;
        obj.mBreadCrumbShortTitleText = this.mBreadCrumbShortTitleText;
        obj.mSharedElementSourceNames = this.mSharedElementSourceNames;
        obj.mSharedElementTargetNames = this.mSharedElementTargetNames;
        obj.mReorderingAllowed = this.mReorderingAllowed;
    }
    
    public int describeContents() {
        return 0;
    }
    
    @NonNull
    public BackStackRecord instantiate(@NonNull final FragmentManager fragmentManager) {
        final BackStackRecord backStackRecord = new BackStackRecord(fragmentManager);
        this.fillInBackStackRecord(backStackRecord);
        backStackRecord.mIndex = this.mIndex;
        for (int i = 0; i < this.mFragmentWhos.size(); ++i) {
            final String s = this.mFragmentWhos.get(i);
            if (s != null) {
                ((FragmentTransaction.Op)backStackRecord.mOps.get(i)).mFragment = fragmentManager.findActiveFragment(s);
            }
        }
        backStackRecord.bumpBackStackNesting(1);
        return backStackRecord;
    }
    
    @NonNull
    public BackStackRecord instantiate(@NonNull final FragmentManager fragmentManager, @NonNull final Map<String, Fragment> map) {
        final BackStackRecord backStackRecord = new BackStackRecord(fragmentManager);
        this.fillInBackStackRecord(backStackRecord);
        for (int i = 0; i < this.mFragmentWhos.size(); ++i) {
            final String str = this.mFragmentWhos.get(i);
            if (str != null) {
                final Fragment mFragment = map.get(str);
                if (mFragment == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Restoring FragmentTransaction ");
                    sb.append(this.mName);
                    sb.append(" failed due to missing saved state for Fragment (");
                    sb.append(str);
                    sb.append(")");
                    throw new IllegalStateException(sb.toString());
                }
                ((FragmentTransaction.Op)backStackRecord.mOps.get(i)).mFragment = mFragment;
            }
        }
        return backStackRecord;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeIntArray(this.mOps);
        parcel.writeStringList((List)this.mFragmentWhos);
        parcel.writeIntArray(this.mOldMaxLifecycleStates);
        parcel.writeIntArray(this.mCurrentMaxLifecycleStates);
        parcel.writeInt(this.mTransition);
        parcel.writeString(this.mName);
        parcel.writeInt(this.mIndex);
        parcel.writeInt(this.mBreadCrumbTitleRes);
        TextUtils.writeToParcel(this.mBreadCrumbTitleText, parcel, 0);
        parcel.writeInt(this.mBreadCrumbShortTitleRes);
        TextUtils.writeToParcel(this.mBreadCrumbShortTitleText, parcel, 0);
        parcel.writeStringList((List)this.mSharedElementSourceNames);
        parcel.writeStringList((List)this.mSharedElementTargetNames);
        parcel.writeInt((int)(this.mReorderingAllowed ? 1 : 0));
    }
}

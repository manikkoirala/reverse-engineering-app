// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.os.BaseBundle;
import androidx.annotation.MainThread;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import androidx.annotation.StringRes;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleEventObserver;
import androidx.lifecycle.Lifecycle;
import android.content.IntentSender$SendIntentException;
import android.content.IntentSender;
import androidx.lifecycle.ViewModelStore;
import android.view.LayoutInflater$Factory2;
import java.util.List;
import androidx.annotation.IdRes;
import androidx.activity.result.ActivityResultRegistry;
import androidx.core.view.MenuHost;
import androidx.core.app.OnPictureInPictureModeChangedProvider;
import androidx.core.app.OnMultiWindowModeChangedProvider;
import androidx.core.content.OnTrimMemoryProvider;
import androidx.core.content.OnConfigurationChangedProvider;
import android.annotation.SuppressLint;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.activity.result.ActivityResultRegistryOwner;
import android.os.Parcelable;
import androidx.savedstate.SavedStateRegistry;
import androidx.savedstate.SavedStateRegistryOwner;
import androidx.lifecycle.LifecycleOwner;
import androidx.activity.OnBackPressedDispatcherOwner;
import java.io.FileDescriptor;
import java.io.Writer;
import java.io.PrintWriter;
import android.util.Log;
import androidx.fragment.R;
import android.view.ViewParent;
import android.content.Context;
import android.content.ContextWrapper;
import android.view.View;
import java.util.Collection;
import android.os.Looper;
import java.util.HashSet;
import java.util.Set;
import java.util.Iterator;
import android.app.Activity;
import androidx.lifecycle.ViewModelStoreOwner;
import android.view.ViewGroup;
import android.view.MenuItem;
import android.view.MenuInflater;
import androidx.annotation.NonNull;
import android.view.Menu;
import java.util.Collections;
import java.util.HashMap;
import androidx.fragment.app.strictmode.FragmentStrictMode;
import androidx.activity.result.IntentSenderRequest;
import android.content.Intent;
import android.os.Bundle;
import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.Nullable;
import androidx.core.app.PictureInPictureModeChangedInfo;
import androidx.core.app.MultiWindowModeChangedInfo;
import android.content.res.Configuration;
import androidx.core.util.Consumer;
import androidx.activity.OnBackPressedDispatcher;
import androidx.activity.OnBackPressedCallback;
import java.util.concurrent.CopyOnWriteArrayList;
import androidx.core.view.MenuProvider;
import java.util.ArrayDeque;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.ArrayList;
import androidx.annotation.RestrictTo;

public abstract class FragmentManager implements FragmentResultOwner
{
    private static boolean DEBUG = false;
    private static final String EXTRA_CREATED_FILLIN_INTENT = "androidx.fragment.extra.ACTIVITY_OPTIONS_BUNDLE";
    static final String FRAGMENT_MANAGER_STATE_TAG = "state";
    static final String FRAGMENT_NAME_PREFIX = "fragment_";
    static final String FRAGMENT_STATE_TAG = "state";
    public static final int POP_BACK_STACK_INCLUSIVE = 1;
    static final String RESULT_NAME_PREFIX = "result_";
    static final String SAVED_STATE_TAG = "android:support:fragments";
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final String TAG = "FragmentManager";
    ArrayList<BackStackRecord> mBackStack;
    private ArrayList<OnBackStackChangedListener> mBackStackChangeListeners;
    private final AtomicInteger mBackStackIndex;
    private final Map<String, BackStackState> mBackStackStates;
    private FragmentContainer mContainer;
    private ArrayList<Fragment> mCreatedMenus;
    int mCurState;
    private SpecialEffectsControllerFactory mDefaultSpecialEffectsControllerFactory;
    private boolean mDestroyed;
    private Runnable mExecCommit;
    private boolean mExecutingActions;
    private FragmentFactory mFragmentFactory;
    private final FragmentStore mFragmentStore;
    private boolean mHavePendingDeferredStart;
    private FragmentHostCallback<?> mHost;
    private FragmentFactory mHostFragmentFactory;
    ArrayDeque<LaunchedFragmentInfo> mLaunchedFragments;
    private final FragmentLayoutInflaterFactory mLayoutInflaterFactory;
    private final FragmentLifecycleCallbacksDispatcher mLifecycleCallbacksDispatcher;
    private final MenuProvider mMenuProvider;
    private boolean mNeedMenuInvalidate;
    private FragmentManagerViewModel mNonConfig;
    private final CopyOnWriteArrayList<FragmentOnAttachListener> mOnAttachListeners;
    private final OnBackPressedCallback mOnBackPressedCallback;
    private OnBackPressedDispatcher mOnBackPressedDispatcher;
    private final Consumer<Configuration> mOnConfigurationChangedListener;
    private final Consumer<MultiWindowModeChangedInfo> mOnMultiWindowModeChangedListener;
    private final Consumer<PictureInPictureModeChangedInfo> mOnPictureInPictureModeChangedListener;
    private final Consumer<Integer> mOnTrimMemoryListener;
    private Fragment mParent;
    private final ArrayList<OpGenerator> mPendingActions;
    @Nullable
    Fragment mPrimaryNav;
    private ActivityResultLauncher<String[]> mRequestPermissions;
    private final Map<String, LifecycleAwareResultListener> mResultListeners;
    private final Map<String, Bundle> mResults;
    private SpecialEffectsControllerFactory mSpecialEffectsControllerFactory;
    private ActivityResultLauncher<Intent> mStartActivityForResult;
    private ActivityResultLauncher<IntentSenderRequest> mStartIntentSenderForResult;
    private boolean mStateSaved;
    private boolean mStopped;
    private FragmentStrictMode.Policy mStrictModePolicy;
    private ArrayList<Fragment> mTmpAddedFragments;
    private ArrayList<Boolean> mTmpIsPop;
    private ArrayList<BackStackRecord> mTmpRecords;
    
    public FragmentManager() {
        this.mPendingActions = new ArrayList<OpGenerator>();
        this.mFragmentStore = new FragmentStore();
        this.mLayoutInflaterFactory = new FragmentLayoutInflaterFactory(this);
        this.mOnBackPressedCallback = new OnBackPressedCallback(false) {
            final FragmentManager this$0;
            
            @Override
            public void handleOnBackPressed() {
                this.this$0.handleOnBackPressed();
            }
        };
        this.mBackStackIndex = new AtomicInteger();
        this.mBackStackStates = Collections.synchronizedMap(new HashMap<String, BackStackState>());
        this.mResults = Collections.synchronizedMap(new HashMap<String, Bundle>());
        this.mResultListeners = Collections.synchronizedMap(new HashMap<String, LifecycleAwareResultListener>());
        this.mLifecycleCallbacksDispatcher = new FragmentLifecycleCallbacksDispatcher(this);
        this.mOnAttachListeners = new CopyOnWriteArrayList<FragmentOnAttachListener>();
        this.mOnConfigurationChangedListener = new o\u30070(this);
        this.mOnTrimMemoryListener = new \u3007\u3007888(this);
        this.mOnMultiWindowModeChangedListener = new oO80(this);
        this.mOnPictureInPictureModeChangedListener = new \u300780\u3007808\u3007O(this);
        this.mMenuProvider = new MenuProvider() {
            final FragmentManager this$0;
            
            @Override
            public void onCreateMenu(@NonNull final Menu menu, @NonNull final MenuInflater menuInflater) {
                this.this$0.dispatchCreateOptionsMenu(menu, menuInflater);
            }
            
            @Override
            public void onMenuClosed(@NonNull final Menu menu) {
                this.this$0.dispatchOptionsMenuClosed(menu);
            }
            
            @Override
            public boolean onMenuItemSelected(@NonNull final MenuItem menuItem) {
                return this.this$0.dispatchOptionsItemSelected(menuItem);
            }
            
            @Override
            public void onPrepareMenu(@NonNull final Menu menu) {
                this.this$0.dispatchPrepareOptionsMenu(menu);
            }
        };
        this.mCurState = -1;
        this.mFragmentFactory = null;
        this.mHostFragmentFactory = new FragmentFactory() {
            final FragmentManager this$0;
            
            @NonNull
            @Override
            public Fragment instantiate(@NonNull final ClassLoader classLoader, @NonNull final String s) {
                return this.this$0.getHost().instantiate(this.this$0.getHost().getContext(), s, null);
            }
        };
        this.mSpecialEffectsControllerFactory = null;
        this.mDefaultSpecialEffectsControllerFactory = new SpecialEffectsControllerFactory() {
            final FragmentManager this$0;
            
            @NonNull
            @Override
            public SpecialEffectsController createController(@NonNull final ViewGroup viewGroup) {
                return new DefaultSpecialEffectsController(viewGroup);
            }
        };
        this.mLaunchedFragments = new ArrayDeque<LaunchedFragmentInfo>();
        this.mExecCommit = new Runnable() {
            final FragmentManager this$0;
            
            @Override
            public void run() {
                this.this$0.execPendingActions(true);
            }
        };
    }
    
    private void checkStateLoss() {
        if (!this.isStateSaved()) {
            return;
        }
        throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
    }
    
    private void cleanupExec() {
        this.mExecutingActions = false;
        this.mTmpIsPop.clear();
        this.mTmpRecords.clear();
    }
    
    private void clearBackStackStateViewModels() {
        final FragmentHostCallback<?> mHost = this.mHost;
        boolean cleared;
        if (mHost instanceof ViewModelStoreOwner) {
            cleared = this.mFragmentStore.getNonConfig().isCleared();
        }
        else {
            cleared = (!(mHost.getContext() instanceof Activity) || (((Activity)this.mHost.getContext()).isChangingConfigurations() ^ true));
        }
        if (cleared) {
            final Iterator<BackStackState> iterator = this.mBackStackStates.values().iterator();
            while (iterator.hasNext()) {
                final Iterator<String> iterator2 = iterator.next().mFragments.iterator();
                while (iterator2.hasNext()) {
                    this.mFragmentStore.getNonConfig().clearNonConfigState(iterator2.next());
                }
            }
        }
    }
    
    private Set<SpecialEffectsController> collectAllSpecialEffectsController() {
        final HashSet set = new HashSet();
        final Iterator<FragmentStateManager> iterator = this.mFragmentStore.getActiveFragmentStateManagers().iterator();
        while (iterator.hasNext()) {
            final ViewGroup mContainer = iterator.next().getFragment().mContainer;
            if (mContainer != null) {
                set.add(SpecialEffectsController.getOrCreateController(mContainer, this.getSpecialEffectsControllerFactory()));
            }
        }
        return set;
    }
    
    private Set<SpecialEffectsController> collectChangedControllers(@NonNull final ArrayList<BackStackRecord> list, int i, final int n) {
        final HashSet set = new HashSet();
        while (i < n) {
            final Iterator<FragmentTransaction.Op> iterator = list.get(i).mOps.iterator();
            while (iterator.hasNext()) {
                final Fragment mFragment = ((FragmentTransaction.Op)iterator.next()).mFragment;
                if (mFragment != null) {
                    final ViewGroup mContainer = mFragment.mContainer;
                    if (mContainer == null) {
                        continue;
                    }
                    set.add(SpecialEffectsController.getOrCreateController(mContainer, this));
                }
            }
            ++i;
        }
        return set;
    }
    
    private void dispatchParentPrimaryNavigationFragmentChanged(@Nullable final Fragment fragment) {
        if (fragment != null && fragment.equals(this.findActiveFragment(fragment.mWho))) {
            fragment.performPrimaryNavigationFragmentChanged();
        }
    }
    
    private void dispatchStateChange(final int n) {
        try {
            this.mExecutingActions = true;
            this.mFragmentStore.dispatchStateChange(n);
            this.moveToState(n, false);
            final Iterator<SpecialEffectsController> iterator = this.collectAllSpecialEffectsController().iterator();
            while (iterator.hasNext()) {
                iterator.next().forceCompleteAllOperations();
            }
            this.mExecutingActions = false;
            this.execPendingActions(true);
        }
        finally {
            this.mExecutingActions = false;
        }
    }
    
    private void doPendingDeferredStart() {
        if (this.mHavePendingDeferredStart) {
            this.mHavePendingDeferredStart = false;
            this.startPendingDeferredFragments();
        }
    }
    
    @Deprecated
    public static void enableDebugLogging(final boolean debug) {
        FragmentManager.DEBUG = debug;
    }
    
    private void endAnimatingAwayFragments() {
        final Iterator<SpecialEffectsController> iterator = this.collectAllSpecialEffectsController().iterator();
        while (iterator.hasNext()) {
            iterator.next().forceCompleteAllOperations();
        }
    }
    
    private void ensureExecReady(final boolean b) {
        if (this.mExecutingActions) {
            throw new IllegalStateException("FragmentManager is already executing transactions");
        }
        if (this.mHost == null) {
            if (this.mDestroyed) {
                throw new IllegalStateException("FragmentManager has been destroyed");
            }
            throw new IllegalStateException("FragmentManager has not been attached to a host.");
        }
        else {
            if (Looper.myLooper() == this.mHost.getHandler().getLooper()) {
                if (!b) {
                    this.checkStateLoss();
                }
                if (this.mTmpRecords == null) {
                    this.mTmpRecords = new ArrayList<BackStackRecord>();
                    this.mTmpIsPop = new ArrayList<Boolean>();
                }
                return;
            }
            throw new IllegalStateException("Must be called from main thread of fragment host");
        }
    }
    
    private static void executeOps(@NonNull final ArrayList<BackStackRecord> list, @NonNull final ArrayList<Boolean> list2, int i, final int n) {
        while (i < n) {
            final BackStackRecord backStackRecord = list.get(i);
            if (list2.get(i)) {
                backStackRecord.bumpBackStackNesting(-1);
                backStackRecord.executePopOps();
            }
            else {
                backStackRecord.bumpBackStackNesting(1);
                backStackRecord.executeOps();
            }
            ++i;
        }
    }
    
    private void executeOpsTogether(@NonNull final ArrayList<BackStackRecord> list, @NonNull final ArrayList<Boolean> list2, final int index, final int n) {
        final boolean mReorderingAllowed = list.get(index).mReorderingAllowed;
        final ArrayList<Fragment> mTmpAddedFragments = this.mTmpAddedFragments;
        if (mTmpAddedFragments == null) {
            this.mTmpAddedFragments = new ArrayList<Fragment>();
        }
        else {
            mTmpAddedFragments.clear();
        }
        this.mTmpAddedFragments.addAll(this.mFragmentStore.getFragments());
        Fragment fragment = this.getPrimaryNavigationFragment();
        int i = index;
        int n2 = 0;
        while (i < n) {
            final BackStackRecord backStackRecord = list.get(i);
            if (!list2.get(i)) {
                fragment = backStackRecord.expandOps(this.mTmpAddedFragments, fragment);
            }
            else {
                fragment = backStackRecord.trackAddedFragmentsInPop(this.mTmpAddedFragments, fragment);
            }
            if (n2 == 0 && !backStackRecord.mAddToBackStack) {
                n2 = 0;
            }
            else {
                n2 = 1;
            }
            ++i;
        }
        this.mTmpAddedFragments.clear();
        if (!mReorderingAllowed && this.mCurState >= 1) {
            for (int j = index; j < n; ++j) {
                final Iterator<FragmentTransaction.Op> iterator = list.get(j).mOps.iterator();
                while (iterator.hasNext()) {
                    final Fragment mFragment = ((FragmentTransaction.Op)iterator.next()).mFragment;
                    if (mFragment != null && mFragment.mFragmentManager != null) {
                        this.mFragmentStore.makeActive(this.createOrGetFragmentStateManager(mFragment));
                    }
                }
            }
        }
        executeOps(list, list2, index, n);
        final boolean booleanValue = list2.get(n - 1);
        for (int k = index; k < n; ++k) {
            final BackStackRecord backStackRecord2 = list.get(k);
            if (booleanValue) {
                for (int l = backStackRecord2.mOps.size() - 1; l >= 0; --l) {
                    final Fragment mFragment2 = ((FragmentTransaction.Op)backStackRecord2.mOps.get(l)).mFragment;
                    if (mFragment2 != null) {
                        this.createOrGetFragmentStateManager(mFragment2).moveToExpectedState();
                    }
                }
            }
            else {
                final Iterator<FragmentTransaction.Op> iterator2 = backStackRecord2.mOps.iterator();
                while (iterator2.hasNext()) {
                    final Fragment mFragment3 = ((FragmentTransaction.Op)iterator2.next()).mFragment;
                    if (mFragment3 != null) {
                        this.createOrGetFragmentStateManager(mFragment3).moveToExpectedState();
                    }
                }
            }
        }
        this.moveToState(this.mCurState, true);
        final Iterator<SpecialEffectsController> iterator3 = this.collectChangedControllers(list, index, n).iterator();
        int n3;
        while (true) {
            n3 = index;
            if (!iterator3.hasNext()) {
                break;
            }
            final SpecialEffectsController specialEffectsController = iterator3.next();
            specialEffectsController.updateOperationDirection(booleanValue);
            specialEffectsController.markPostponedState();
            specialEffectsController.executePendingOperations();
        }
        while (n3 < n) {
            final BackStackRecord backStackRecord3 = list.get(n3);
            if (list2.get(n3) && backStackRecord3.mIndex >= 0) {
                backStackRecord3.mIndex = -1;
            }
            backStackRecord3.runOnCommitRunnables();
            ++n3;
        }
        if (n2 != 0) {
            this.reportBackStackChanged();
        }
    }
    
    private int findBackStackIndex(@Nullable final String s, final int n, final boolean b) {
        final ArrayList<BackStackRecord> mBackStack = this.mBackStack;
        if (mBackStack == null || mBackStack.isEmpty()) {
            return -1;
        }
        if (s == null && n < 0) {
            if (b) {
                return 0;
            }
            return this.mBackStack.size() - 1;
        }
        else {
            int i;
            for (i = this.mBackStack.size() - 1; i >= 0; --i) {
                final BackStackRecord backStackRecord = this.mBackStack.get(i);
                if (s != null && s.equals(backStackRecord.getName())) {
                    break;
                }
                if (n >= 0 && n == backStackRecord.mIndex) {
                    break;
                }
            }
            if (i < 0) {
                return i;
            }
            int n2;
            if (b) {
                while ((n2 = i) > 0) {
                    final BackStackRecord backStackRecord2 = this.mBackStack.get(i - 1);
                    if (s == null || !s.equals(backStackRecord2.getName())) {
                        n2 = i;
                        if (n < 0) {
                            break;
                        }
                        n2 = i;
                        if (n != backStackRecord2.mIndex) {
                            break;
                        }
                    }
                    --i;
                }
            }
            else {
                if (i == this.mBackStack.size() - 1) {
                    return -1;
                }
                n2 = i + 1;
            }
            return n2;
        }
    }
    
    @NonNull
    public static <F extends Fragment> F findFragment(@NonNull final View obj) {
        final Fragment viewFragment = findViewFragment(obj);
        if (viewFragment != null) {
            return (F)viewFragment;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("View ");
        sb.append(obj);
        sb.append(" does not have a Fragment set");
        throw new IllegalStateException(sb.toString());
    }
    
    @NonNull
    static FragmentManager findFragmentManager(@NonNull final View view) {
        final Fragment viewFragment = findViewFragment(view);
        if (viewFragment == null) {
            Context context = view.getContext();
            while (true) {
                while (context instanceof ContextWrapper) {
                    if (context instanceof FragmentActivity) {
                        final FragmentActivity fragmentActivity = (FragmentActivity)context;
                        if (fragmentActivity != null) {
                            return fragmentActivity.getSupportFragmentManager();
                        }
                        final StringBuilder sb = new StringBuilder();
                        sb.append("View ");
                        sb.append(view);
                        sb.append(" is not within a subclass of FragmentActivity.");
                        throw new IllegalStateException(sb.toString());
                    }
                    else {
                        context = ((ContextWrapper)context).getBaseContext();
                    }
                }
                final FragmentActivity fragmentActivity = null;
                continue;
            }
        }
        if (!viewFragment.isAdded()) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("The Fragment ");
            sb2.append(viewFragment);
            sb2.append(" that owns View ");
            sb2.append(view);
            sb2.append(" has already been destroyed. Nested fragments should always use the child FragmentManager.");
            throw new IllegalStateException(sb2.toString());
        }
        return viewFragment.getChildFragmentManager();
    }
    
    @Nullable
    private static Fragment findViewFragment(@NonNull View view) {
        while (view != null) {
            final Fragment viewFragment = getViewFragment(view);
            if (viewFragment != null) {
                return viewFragment;
            }
            final ViewParent parent = view.getParent();
            if (parent instanceof View) {
                view = (View)parent;
            }
            else {
                view = null;
            }
        }
        return null;
    }
    
    private void forcePostponedTransactions() {
        final Iterator<SpecialEffectsController> iterator = this.collectAllSpecialEffectsController().iterator();
        while (iterator.hasNext()) {
            iterator.next().forcePostponedExecutePendingOperations();
        }
    }
    
    private boolean generateOpsForPendingActions(@NonNull final ArrayList<BackStackRecord> list, @NonNull final ArrayList<Boolean> list2) {
        synchronized (this.mPendingActions) {
            final boolean empty = this.mPendingActions.isEmpty();
            int i = 0;
            if (empty) {
                return false;
            }
            try {
                final int size = this.mPendingActions.size();
                boolean b = false;
                while (i < size) {
                    b |= this.mPendingActions.get(i).generateOps(list, list2);
                    ++i;
                }
                return b;
            }
            finally {
                this.mPendingActions.clear();
                this.mHost.getHandler().removeCallbacks(this.mExecCommit);
            }
        }
    }
    
    @NonNull
    private FragmentManagerViewModel getChildNonConfig(@NonNull final Fragment fragment) {
        return this.mNonConfig.getChildNonConfig(fragment);
    }
    
    private ViewGroup getFragmentContainer(@NonNull final Fragment fragment) {
        final ViewGroup mContainer = fragment.mContainer;
        if (mContainer != null) {
            return mContainer;
        }
        if (fragment.mContainerId <= 0) {
            return null;
        }
        if (this.mContainer.onHasView()) {
            final View onFindViewById = this.mContainer.onFindViewById(fragment.mContainerId);
            if (onFindViewById instanceof ViewGroup) {
                return (ViewGroup)onFindViewById;
            }
        }
        return null;
    }
    
    @Nullable
    static Fragment getViewFragment(@NonNull final View view) {
        final Object tag = view.getTag(R.id.fragment_container_view_tag);
        if (tag instanceof Fragment) {
            return (Fragment)tag;
        }
        return null;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static boolean isLoggingEnabled(final int n) {
        return FragmentManager.DEBUG || Log.isLoggable("FragmentManager", n);
    }
    
    private boolean isMenuAvailable(@NonNull final Fragment fragment) {
        return (fragment.mHasMenu && fragment.mMenuVisible) || fragment.mChildFragmentManager.checkForMenus();
    }
    
    private boolean isParentAdded() {
        final Fragment mParent = this.mParent;
        boolean b = true;
        if (mParent == null) {
            return true;
        }
        if (!mParent.isAdded() || !this.mParent.getParentFragmentManager().isParentAdded()) {
            b = false;
        }
        return b;
    }
    
    private boolean popBackStackImmediate(@Nullable final String s, final int n, final int n2) {
        this.execPendingActions(false);
        this.ensureExecReady(true);
        final Fragment mPrimaryNav = this.mPrimaryNav;
        if (mPrimaryNav != null && n < 0 && s == null && mPrimaryNav.getChildFragmentManager().popBackStackImmediate()) {
            return true;
        }
        final boolean popBackStackState = this.popBackStackState(this.mTmpRecords, this.mTmpIsPop, s, n, n2);
        if (popBackStackState) {
            this.mExecutingActions = true;
            try {
                this.removeRedundantOperationsAndExecute(this.mTmpRecords, this.mTmpIsPop);
            }
            finally {
                this.cleanupExec();
            }
        }
        this.updateOnBackPressedCallbackEnabled();
        this.doPendingDeferredStart();
        this.mFragmentStore.burpActive();
        return popBackStackState;
    }
    
    private void removeRedundantOperationsAndExecute(@NonNull final ArrayList<BackStackRecord> list, @NonNull final ArrayList<Boolean> list2) {
        if (list.isEmpty()) {
            return;
        }
        if (list.size() == list2.size()) {
            final int size = list.size();
            int i = 0;
            int n = 0;
            while (i < size) {
                int n2 = i;
                int n3 = n;
                if (!((BackStackRecord)list.get(i)).mReorderingAllowed) {
                    if (n != i) {
                        this.executeOpsTogether(list, list2, n, i);
                    }
                    int n4 = n3 = i + 1;
                    if (list2.get(i)) {
                        while ((n3 = n4) < size) {
                            n3 = n4;
                            if (!(boolean)list2.get(n4)) {
                                break;
                            }
                            n3 = n4;
                            if (((BackStackRecord)list.get(n4)).mReorderingAllowed) {
                                break;
                            }
                            ++n4;
                        }
                    }
                    this.executeOpsTogether(list, list2, i, n3);
                    n2 = n3 - 1;
                }
                i = n2 + 1;
                n = n3;
            }
            if (n != size) {
                this.executeOpsTogether(list, list2, n, size);
            }
            return;
        }
        throw new IllegalStateException("Internal error with the back stack records");
    }
    
    private void reportBackStackChanged() {
        if (this.mBackStackChangeListeners != null) {
            for (int i = 0; i < this.mBackStackChangeListeners.size(); ++i) {
                this.mBackStackChangeListeners.get(i).onBackStackChanged();
            }
        }
    }
    
    static int reverseTransit(final int n) {
        int n2 = 8194;
        if (n != 4097) {
            if (n != 8194) {
                n2 = 4100;
                if (n != 8197) {
                    if (n != 4099) {
                        if (n != 4100) {
                            n2 = 0;
                        }
                        else {
                            n2 = 8197;
                        }
                    }
                    else {
                        n2 = 4099;
                    }
                }
            }
            else {
                n2 = 4097;
            }
        }
        return n2;
    }
    
    private void setVisibleRemovingFragment(@NonNull final Fragment fragment) {
        final ViewGroup fragmentContainer = this.getFragmentContainer(fragment);
        if (fragmentContainer != null && fragment.getEnterAnim() + fragment.getExitAnim() + fragment.getPopEnterAnim() + fragment.getPopExitAnim() > 0) {
            final int visible_removing_fragment_view_tag = R.id.visible_removing_fragment_view_tag;
            if (((View)fragmentContainer).getTag(visible_removing_fragment_view_tag) == null) {
                ((View)fragmentContainer).setTag(visible_removing_fragment_view_tag, (Object)fragment);
            }
            ((Fragment)((View)fragmentContainer).getTag(visible_removing_fragment_view_tag)).setPopDirection(fragment.getPopDirection());
        }
    }
    
    private void startPendingDeferredFragments() {
        final Iterator<FragmentStateManager> iterator = this.mFragmentStore.getActiveFragmentStateManagers().iterator();
        while (iterator.hasNext()) {
            this.performPendingDeferredStart(iterator.next());
        }
    }
    
    private void throwException(final RuntimeException ex) {
        ex.getMessage();
        final PrintWriter printWriter = new PrintWriter(new LogWriter("FragmentManager"));
        final FragmentHostCallback<?> mHost = this.mHost;
        Label_0047: {
            if (mHost == null) {
                break Label_0047;
            }
            try {
                mHost.onDump("  ", null, printWriter, new String[0]);
                throw ex;
                this.dump("  ", null, printWriter, new String[0]);
                throw ex;
            }
            catch (final Exception ex2) {
                throw ex;
            }
        }
    }
    
    private void updateOnBackPressedCallbackEnabled() {
        synchronized (this.mPendingActions) {
            final boolean empty = this.mPendingActions.isEmpty();
            boolean enabled = true;
            if (!empty) {
                this.mOnBackPressedCallback.setEnabled(true);
                return;
            }
            monitorexit(this.mPendingActions);
            final OnBackPressedCallback mOnBackPressedCallback = this.mOnBackPressedCallback;
            if (this.getBackStackEntryCount() <= 0 || !this.isPrimaryNavigation(this.mParent)) {
                enabled = false;
            }
            mOnBackPressedCallback.setEnabled(enabled);
        }
    }
    
    void addBackStackState(final BackStackRecord e) {
        if (this.mBackStack == null) {
            this.mBackStack = new ArrayList<BackStackRecord>();
        }
        this.mBackStack.add(e);
    }
    
    FragmentStateManager addFragment(@NonNull final Fragment obj) {
        final String mPreviousWho = obj.mPreviousWho;
        if (mPreviousWho != null) {
            FragmentStrictMode.onFragmentReuse(obj, mPreviousWho);
        }
        if (isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("add: ");
            sb.append(obj);
        }
        final FragmentStateManager orGetFragmentStateManager = this.createOrGetFragmentStateManager(obj);
        obj.mFragmentManager = this;
        this.mFragmentStore.makeActive(orGetFragmentStateManager);
        if (!obj.mDetached) {
            this.mFragmentStore.addFragment(obj);
            obj.mRemoving = false;
            if (obj.mView == null) {
                obj.mHiddenChanged = false;
            }
            if (this.isMenuAvailable(obj)) {
                this.mNeedMenuInvalidate = true;
            }
        }
        return orGetFragmentStateManager;
    }
    
    public void addFragmentOnAttachListener(@NonNull final FragmentOnAttachListener e) {
        this.mOnAttachListeners.add(e);
    }
    
    public void addOnBackStackChangedListener(@NonNull final OnBackStackChangedListener e) {
        if (this.mBackStackChangeListeners == null) {
            this.mBackStackChangeListeners = new ArrayList<OnBackStackChangedListener>();
        }
        this.mBackStackChangeListeners.add(e);
    }
    
    void addRetainedFragment(@NonNull final Fragment fragment) {
        this.mNonConfig.addRetainedFragment(fragment);
    }
    
    int allocBackStackIndex() {
        return this.mBackStackIndex.getAndIncrement();
    }
    
    @SuppressLint({ "SyntheticAccessor" })
    void attachController(@NonNull final FragmentHostCallback<?> mHost, @NonNull final FragmentContainer mContainer, @Nullable final Fragment mParent) {
        if (this.mHost == null) {
            this.mHost = mHost;
            this.mContainer = mContainer;
            if ((this.mParent = mParent) != null) {
                this.addFragmentOnAttachListener(new FragmentOnAttachListener(this, mParent) {
                    final FragmentManager this$0;
                    final Fragment val$parent;
                    
                    @Override
                    public void onAttachFragment(@NonNull final FragmentManager fragmentManager, @NonNull final Fragment fragment) {
                        this.val$parent.onAttachFragment(fragment);
                    }
                });
            }
            else if (mHost instanceof FragmentOnAttachListener) {
                this.addFragmentOnAttachListener((FragmentOnAttachListener)mHost);
            }
            if (this.mParent != null) {
                this.updateOnBackPressedCallbackEnabled();
            }
            if (mHost instanceof OnBackPressedDispatcherOwner) {
                Object o = mHost;
                final OnBackPressedDispatcher onBackPressedDispatcher = ((OnBackPressedDispatcherOwner)o).getOnBackPressedDispatcher();
                this.mOnBackPressedDispatcher = onBackPressedDispatcher;
                if (mParent != null) {
                    o = mParent;
                }
                onBackPressedDispatcher.addCallback((LifecycleOwner)o, this.mOnBackPressedCallback);
            }
            if (mParent != null) {
                this.mNonConfig = mParent.mFragmentManager.getChildNonConfig(mParent);
            }
            else if (mHost instanceof ViewModelStoreOwner) {
                this.mNonConfig = FragmentManagerViewModel.getInstance(((ViewModelStoreOwner)mHost).getViewModelStore());
            }
            else {
                this.mNonConfig = new FragmentManagerViewModel(false);
            }
            this.mNonConfig.setIsStateSaved(this.isStateSaved());
            this.mFragmentStore.setNonConfig(this.mNonConfig);
            final FragmentHostCallback<?> mHost2 = this.mHost;
            if (mHost2 instanceof SavedStateRegistryOwner && mParent == null) {
                final SavedStateRegistry savedStateRegistry = ((SavedStateRegistryOwner)mHost2).getSavedStateRegistry();
                savedStateRegistry.registerSavedStateProvider("android:support:fragments", (SavedStateRegistry.SavedStateProvider)new OO0o\u3007\u3007\u3007\u30070(this));
                final Bundle consumeRestoredStateForKey = savedStateRegistry.consumeRestoredStateForKey("android:support:fragments");
                if (consumeRestoredStateForKey != null) {
                    this.restoreSaveStateInternal((Parcelable)consumeRestoredStateForKey);
                }
            }
            final FragmentHostCallback<?> mHost3 = this.mHost;
            if (mHost3 instanceof ActivityResultRegistryOwner) {
                final ActivityResultRegistry activityResultRegistry = ((ActivityResultRegistryOwner)mHost3).getActivityResultRegistry();
                String string;
                if (mParent != null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(mParent.mWho);
                    sb.append(":");
                    string = sb.toString();
                }
                else {
                    string = "";
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("FragmentManager:");
                sb2.append(string);
                final String string2 = sb2.toString();
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(string2);
                sb3.append("StartActivityForResult");
                this.mStartActivityForResult = activityResultRegistry.register(sb3.toString(), (ActivityResultContract<Intent, Object>)new ActivityResultContracts.StartActivityForResult(), (ActivityResultCallback<Object>)new ActivityResultCallback<ActivityResult>(this) {
                    final FragmentManager this$0;
                    
                    @Override
                    public void onActivityResult(final ActivityResult activityResult) {
                        final LaunchedFragmentInfo launchedFragmentInfo = this.this$0.mLaunchedFragments.pollFirst();
                        if (launchedFragmentInfo == null) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("No Activities were started for result for ");
                            sb.append(this);
                            return;
                        }
                        final String mWho = launchedFragmentInfo.mWho;
                        final int mRequestCode = launchedFragmentInfo.mRequestCode;
                        final Fragment fragmentByWho = this.this$0.mFragmentStore.findFragmentByWho(mWho);
                        if (fragmentByWho == null) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Activity result delivered for unknown Fragment ");
                            sb2.append(mWho);
                            return;
                        }
                        fragmentByWho.onActivityResult(mRequestCode, activityResult.getResultCode(), activityResult.getData());
                    }
                });
                final StringBuilder sb4 = new StringBuilder();
                sb4.append(string2);
                sb4.append("StartIntentSenderForResult");
                this.mStartIntentSenderForResult = activityResultRegistry.register(sb4.toString(), (ActivityResultContract<IntentSenderRequest, Object>)new FragmentIntentSenderContract(), (ActivityResultCallback<Object>)new ActivityResultCallback<ActivityResult>(this) {
                    final FragmentManager this$0;
                    
                    @Override
                    public void onActivityResult(final ActivityResult activityResult) {
                        final LaunchedFragmentInfo launchedFragmentInfo = this.this$0.mLaunchedFragments.pollFirst();
                        if (launchedFragmentInfo == null) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("No IntentSenders were started for ");
                            sb.append(this);
                            return;
                        }
                        final String mWho = launchedFragmentInfo.mWho;
                        final int mRequestCode = launchedFragmentInfo.mRequestCode;
                        final Fragment fragmentByWho = this.this$0.mFragmentStore.findFragmentByWho(mWho);
                        if (fragmentByWho == null) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Intent Sender result delivered for unknown Fragment ");
                            sb2.append(mWho);
                            return;
                        }
                        fragmentByWho.onActivityResult(mRequestCode, activityResult.getResultCode(), activityResult.getData());
                    }
                });
                final StringBuilder sb5 = new StringBuilder();
                sb5.append(string2);
                sb5.append("RequestPermissions");
                this.mRequestPermissions = activityResultRegistry.register(sb5.toString(), (ActivityResultContract<String[], Object>)new ActivityResultContracts.RequestMultiplePermissions(), (ActivityResultCallback<Object>)new ActivityResultCallback<Map<String, Boolean>>(this) {
                    final FragmentManager this$0;
                    
                    @SuppressLint({ "SyntheticAccessor" })
                    @Override
                    public void onActivityResult(final Map<String, Boolean> map) {
                        final String[] array = map.keySet().toArray(new String[0]);
                        final ArrayList list = new ArrayList((Collection<? extends E>)map.values());
                        final int[] array2 = new int[list.size()];
                        for (int i = 0; i < list.size(); ++i) {
                            int n;
                            if (list.get(i)) {
                                n = 0;
                            }
                            else {
                                n = -1;
                            }
                            array2[i] = n;
                        }
                        final LaunchedFragmentInfo launchedFragmentInfo = this.this$0.mLaunchedFragments.pollFirst();
                        if (launchedFragmentInfo == null) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("No permissions were requested for ");
                            sb.append(this);
                            return;
                        }
                        final String mWho = launchedFragmentInfo.mWho;
                        final int mRequestCode = launchedFragmentInfo.mRequestCode;
                        final Fragment fragmentByWho = this.this$0.mFragmentStore.findFragmentByWho(mWho);
                        if (fragmentByWho == null) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Permission request result delivered for unknown Fragment ");
                            sb2.append(mWho);
                            return;
                        }
                        fragmentByWho.onRequestPermissionsResult(mRequestCode, array, array2);
                    }
                });
            }
            final FragmentHostCallback<?> mHost4 = this.mHost;
            if (mHost4 instanceof OnConfigurationChangedProvider) {
                ((OnConfigurationChangedProvider)mHost4).addOnConfigurationChangedListener(this.mOnConfigurationChangedListener);
            }
            final FragmentHostCallback<?> mHost5 = this.mHost;
            if (mHost5 instanceof OnTrimMemoryProvider) {
                ((OnTrimMemoryProvider)mHost5).addOnTrimMemoryListener(this.mOnTrimMemoryListener);
            }
            final FragmentHostCallback<?> mHost6 = this.mHost;
            if (mHost6 instanceof OnMultiWindowModeChangedProvider) {
                ((OnMultiWindowModeChangedProvider)mHost6).addOnMultiWindowModeChangedListener(this.mOnMultiWindowModeChangedListener);
            }
            final FragmentHostCallback<?> mHost7 = this.mHost;
            if (mHost7 instanceof OnPictureInPictureModeChangedProvider) {
                ((OnPictureInPictureModeChangedProvider)mHost7).addOnPictureInPictureModeChangedListener(this.mOnPictureInPictureModeChangedListener);
            }
            final FragmentHostCallback<?> mHost8 = this.mHost;
            if (mHost8 instanceof MenuHost && mParent == null) {
                ((MenuHost)mHost8).addMenuProvider(this.mMenuProvider);
            }
            return;
        }
        throw new IllegalStateException("Already attached");
    }
    
    void attachFragment(@NonNull final Fragment fragment) {
        if (isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("attach: ");
            sb.append(fragment);
        }
        if (fragment.mDetached) {
            fragment.mDetached = false;
            if (!fragment.mAdded) {
                this.mFragmentStore.addFragment(fragment);
                if (isLoggingEnabled(2)) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("add from attach: ");
                    sb2.append(fragment);
                }
                if (this.isMenuAvailable(fragment)) {
                    this.mNeedMenuInvalidate = true;
                }
            }
        }
    }
    
    @NonNull
    public FragmentTransaction beginTransaction() {
        return new BackStackRecord(this);
    }
    
    boolean checkForMenus() {
        final Iterator<Fragment> iterator = this.mFragmentStore.getActiveFragments().iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final Fragment fragment = iterator.next();
            int menuAvailable = n;
            if (fragment != null) {
                menuAvailable = (this.isMenuAvailable(fragment) ? 1 : 0);
            }
            if ((n = menuAvailable) != 0) {
                return true;
            }
        }
        return false;
    }
    
    public void clearBackStack(@NonNull final String s) {
        this.enqueueAction((OpGenerator)new ClearBackStackState(s), false);
    }
    
    boolean clearBackStackState(@NonNull final ArrayList<BackStackRecord> list, @NonNull final ArrayList<Boolean> list2, @NonNull final String s) {
        return this.restoreBackStackState(list, list2, s) && this.popBackStackState(list, list2, s, -1, 1);
    }
    
    @Override
    public final void clearFragmentResult(@NonNull final String str) {
        this.mResults.remove(str);
        if (isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Clearing fragment result with key ");
            sb.append(str);
        }
    }
    
    @Override
    public final void clearFragmentResultListener(@NonNull final String str) {
        final LifecycleAwareResultListener lifecycleAwareResultListener = this.mResultListeners.remove(str);
        if (lifecycleAwareResultListener != null) {
            lifecycleAwareResultListener.removeObserver();
        }
        if (isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Clearing FragmentResultListener for key ");
            sb.append(str);
        }
    }
    
    @NonNull
    FragmentStateManager createOrGetFragmentStateManager(@NonNull final Fragment fragment) {
        final FragmentStateManager fragmentStateManager = this.mFragmentStore.getFragmentStateManager(fragment.mWho);
        if (fragmentStateManager != null) {
            return fragmentStateManager;
        }
        final FragmentStateManager fragmentStateManager2 = new FragmentStateManager(this.mLifecycleCallbacksDispatcher, this.mFragmentStore, fragment);
        fragmentStateManager2.restoreState(this.mHost.getContext().getClassLoader());
        fragmentStateManager2.setFragmentManagerState(this.mCurState);
        return fragmentStateManager2;
    }
    
    void detachFragment(@NonNull final Fragment visibleRemovingFragment) {
        if (isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("detach: ");
            sb.append(visibleRemovingFragment);
        }
        if (!visibleRemovingFragment.mDetached) {
            visibleRemovingFragment.mDetached = true;
            if (visibleRemovingFragment.mAdded) {
                if (isLoggingEnabled(2)) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("remove from detach: ");
                    sb2.append(visibleRemovingFragment);
                }
                this.mFragmentStore.removeFragment(visibleRemovingFragment);
                if (this.isMenuAvailable(visibleRemovingFragment)) {
                    this.mNeedMenuInvalidate = true;
                }
                this.setVisibleRemovingFragment(visibleRemovingFragment);
            }
        }
    }
    
    void dispatchActivityCreated() {
        this.mStateSaved = false;
        this.mStopped = false;
        this.mNonConfig.setIsStateSaved(false);
        this.dispatchStateChange(4);
    }
    
    void dispatchAttach() {
        this.mStateSaved = false;
        this.mStopped = false;
        this.mNonConfig.setIsStateSaved(false);
        this.dispatchStateChange(0);
    }
    
    void dispatchConfigurationChanged(@NonNull final Configuration configuration, final boolean b) {
        if (b && this.mHost instanceof OnConfigurationChangedProvider) {
            this.throwException(new IllegalStateException("Do not call dispatchConfigurationChanged() on host. Host implements OnConfigurationChangedProvider and automatically dispatches configuration changes to fragments."));
        }
        for (final Fragment fragment : this.mFragmentStore.getFragments()) {
            if (fragment != null) {
                fragment.performConfigurationChanged(configuration);
                if (!b) {
                    continue;
                }
                fragment.mChildFragmentManager.dispatchConfigurationChanged(configuration, true);
            }
        }
    }
    
    boolean dispatchContextItemSelected(@NonNull final MenuItem menuItem) {
        if (this.mCurState < 1) {
            return false;
        }
        for (final Fragment fragment : this.mFragmentStore.getFragments()) {
            if (fragment != null && fragment.performContextItemSelected(menuItem)) {
                return true;
            }
        }
        return false;
    }
    
    void dispatchCreate() {
        this.mStateSaved = false;
        this.mStopped = false;
        this.mNonConfig.setIsStateSaved(false);
        this.dispatchStateChange(1);
    }
    
    boolean dispatchCreateOptionsMenu(@NonNull final Menu menu, @NonNull final MenuInflater menuInflater) {
        final int mCurState = this.mCurState;
        int i = 0;
        if (mCurState < 1) {
            return false;
        }
        final Iterator<Fragment> iterator = this.mFragmentStore.getFragments().iterator();
        ArrayList mCreatedMenus = null;
        boolean b = false;
        while (iterator.hasNext()) {
            final Fragment e = iterator.next();
            if (e != null && this.isParentMenuVisible(e) && e.performCreateOptionsMenu(menu, menuInflater)) {
                ArrayList list;
                if ((list = mCreatedMenus) == null) {
                    list = new ArrayList();
                }
                list.add(e);
                b = true;
                mCreatedMenus = list;
            }
        }
        if (this.mCreatedMenus != null) {
            while (i < this.mCreatedMenus.size()) {
                final Fragment o = this.mCreatedMenus.get(i);
                if (mCreatedMenus == null || !mCreatedMenus.contains(o)) {
                    o.onDestroyOptionsMenu();
                }
                ++i;
            }
        }
        this.mCreatedMenus = mCreatedMenus;
        return b;
    }
    
    void dispatchDestroy() {
        this.execPendingActions(this.mDestroyed = true);
        this.endAnimatingAwayFragments();
        this.clearBackStackStateViewModels();
        this.dispatchStateChange(-1);
        final FragmentHostCallback<?> mHost = this.mHost;
        if (mHost instanceof OnTrimMemoryProvider) {
            ((OnTrimMemoryProvider)mHost).removeOnTrimMemoryListener(this.mOnTrimMemoryListener);
        }
        final FragmentHostCallback<?> mHost2 = this.mHost;
        if (mHost2 instanceof OnConfigurationChangedProvider) {
            ((OnConfigurationChangedProvider)mHost2).removeOnConfigurationChangedListener(this.mOnConfigurationChangedListener);
        }
        final FragmentHostCallback<?> mHost3 = this.mHost;
        if (mHost3 instanceof OnMultiWindowModeChangedProvider) {
            ((OnMultiWindowModeChangedProvider)mHost3).removeOnMultiWindowModeChangedListener(this.mOnMultiWindowModeChangedListener);
        }
        final FragmentHostCallback<?> mHost4 = this.mHost;
        if (mHost4 instanceof OnPictureInPictureModeChangedProvider) {
            ((OnPictureInPictureModeChangedProvider)mHost4).removeOnPictureInPictureModeChangedListener(this.mOnPictureInPictureModeChangedListener);
        }
        final FragmentHostCallback<?> mHost5 = this.mHost;
        if (mHost5 instanceof MenuHost) {
            ((MenuHost)mHost5).removeMenuProvider(this.mMenuProvider);
        }
        this.mHost = null;
        this.mContainer = null;
        this.mParent = null;
        if (this.mOnBackPressedDispatcher != null) {
            this.mOnBackPressedCallback.remove();
            this.mOnBackPressedDispatcher = null;
        }
        final ActivityResultLauncher<Intent> mStartActivityForResult = this.mStartActivityForResult;
        if (mStartActivityForResult != null) {
            mStartActivityForResult.unregister();
            this.mStartIntentSenderForResult.unregister();
            this.mRequestPermissions.unregister();
        }
    }
    
    void dispatchDestroyView() {
        this.dispatchStateChange(1);
    }
    
    void dispatchLowMemory(final boolean b) {
        if (b && this.mHost instanceof OnTrimMemoryProvider) {
            this.throwException(new IllegalStateException("Do not call dispatchLowMemory() on host. Host implements OnTrimMemoryProvider and automatically dispatches low memory callbacks to fragments."));
        }
        for (final Fragment fragment : this.mFragmentStore.getFragments()) {
            if (fragment != null) {
                fragment.performLowMemory();
                if (!b) {
                    continue;
                }
                fragment.mChildFragmentManager.dispatchLowMemory(true);
            }
        }
    }
    
    void dispatchMultiWindowModeChanged(final boolean b, final boolean b2) {
        if (b2 && this.mHost instanceof OnMultiWindowModeChangedProvider) {
            this.throwException(new IllegalStateException("Do not call dispatchMultiWindowModeChanged() on host. Host implements OnMultiWindowModeChangedProvider and automatically dispatches multi-window mode changes to fragments."));
        }
        for (final Fragment fragment : this.mFragmentStore.getFragments()) {
            if (fragment != null) {
                fragment.performMultiWindowModeChanged(b);
                if (!b2) {
                    continue;
                }
                fragment.mChildFragmentManager.dispatchMultiWindowModeChanged(b, true);
            }
        }
    }
    
    void dispatchOnAttachFragment(@NonNull final Fragment fragment) {
        final Iterator<FragmentOnAttachListener> iterator = this.mOnAttachListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().onAttachFragment(this, fragment);
        }
    }
    
    void dispatchOnHiddenChanged() {
        for (final Fragment fragment : this.mFragmentStore.getActiveFragments()) {
            if (fragment != null) {
                fragment.onHiddenChanged(fragment.isHidden());
                fragment.mChildFragmentManager.dispatchOnHiddenChanged();
            }
        }
    }
    
    boolean dispatchOptionsItemSelected(@NonNull final MenuItem menuItem) {
        if (this.mCurState < 1) {
            return false;
        }
        for (final Fragment fragment : this.mFragmentStore.getFragments()) {
            if (fragment != null && fragment.performOptionsItemSelected(menuItem)) {
                return true;
            }
        }
        return false;
    }
    
    void dispatchOptionsMenuClosed(@NonNull final Menu menu) {
        if (this.mCurState < 1) {
            return;
        }
        for (final Fragment fragment : this.mFragmentStore.getFragments()) {
            if (fragment != null) {
                fragment.performOptionsMenuClosed(menu);
            }
        }
    }
    
    void dispatchPause() {
        this.dispatchStateChange(5);
    }
    
    void dispatchPictureInPictureModeChanged(final boolean b, final boolean b2) {
        if (b2 && this.mHost instanceof OnPictureInPictureModeChangedProvider) {
            this.throwException(new IllegalStateException("Do not call dispatchPictureInPictureModeChanged() on host. Host implements OnPictureInPictureModeChangedProvider and automatically dispatches picture-in-picture mode changes to fragments."));
        }
        for (final Fragment fragment : this.mFragmentStore.getFragments()) {
            if (fragment != null) {
                fragment.performPictureInPictureModeChanged(b);
                if (!b2) {
                    continue;
                }
                fragment.mChildFragmentManager.dispatchPictureInPictureModeChanged(b, true);
            }
        }
    }
    
    boolean dispatchPrepareOptionsMenu(@NonNull final Menu menu) {
        final int mCurState = this.mCurState;
        boolean b = false;
        if (mCurState < 1) {
            return false;
        }
        for (final Fragment fragment : this.mFragmentStore.getFragments()) {
            if (fragment != null && this.isParentMenuVisible(fragment) && fragment.performPrepareOptionsMenu(menu)) {
                b = true;
            }
        }
        return b;
    }
    
    void dispatchPrimaryNavigationFragmentChanged() {
        this.updateOnBackPressedCallbackEnabled();
        this.dispatchParentPrimaryNavigationFragmentChanged(this.mPrimaryNav);
    }
    
    void dispatchResume() {
        this.mStateSaved = false;
        this.mStopped = false;
        this.mNonConfig.setIsStateSaved(false);
        this.dispatchStateChange(7);
    }
    
    void dispatchStart() {
        this.mStateSaved = false;
        this.mStopped = false;
        this.mNonConfig.setIsStateSaved(false);
        this.dispatchStateChange(5);
    }
    
    void dispatchStop() {
        this.mStopped = true;
        this.mNonConfig.setIsStateSaved(true);
        this.dispatchStateChange(4);
    }
    
    void dispatchViewCreated() {
        this.dispatchStateChange(2);
    }
    
    public void dump(@NonNull final String s, @Nullable final FileDescriptor fileDescriptor, @NonNull final PrintWriter printWriter, @Nullable final String[] array) {
        final StringBuilder sb = new StringBuilder();
        sb.append(s);
        sb.append("    ");
        final String string = sb.toString();
        this.mFragmentStore.dump(s, fileDescriptor, printWriter, array);
        final ArrayList<Fragment> mCreatedMenus = this.mCreatedMenus;
        final int n = 0;
        if (mCreatedMenus != null) {
            final int size = mCreatedMenus.size();
            if (size > 0) {
                printWriter.print(s);
                printWriter.println("Fragments Created Menus:");
                for (int i = 0; i < size; ++i) {
                    final Fragment fragment = this.mCreatedMenus.get(i);
                    printWriter.print(s);
                    printWriter.print("  #");
                    printWriter.print(i);
                    printWriter.print(": ");
                    printWriter.println(fragment.toString());
                }
            }
        }
        final ArrayList<BackStackRecord> mBackStack = this.mBackStack;
        if (mBackStack != null) {
            final int size2 = mBackStack.size();
            if (size2 > 0) {
                printWriter.print(s);
                printWriter.println("Back Stack:");
                for (int j = 0; j < size2; ++j) {
                    final BackStackRecord backStackRecord = this.mBackStack.get(j);
                    printWriter.print(s);
                    printWriter.print("  #");
                    printWriter.print(j);
                    printWriter.print(": ");
                    printWriter.println(backStackRecord.toString());
                    backStackRecord.dump(string, printWriter);
                }
            }
        }
        printWriter.print(s);
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Back Stack Index: ");
        sb2.append(this.mBackStackIndex.get());
        printWriter.println(sb2.toString());
        synchronized (this.mPendingActions) {
            final int size3 = this.mPendingActions.size();
            if (size3 > 0) {
                printWriter.print(s);
                printWriter.println("Pending Actions:");
                for (int k = n; k < size3; ++k) {
                    final OpGenerator x = this.mPendingActions.get(k);
                    printWriter.print(s);
                    printWriter.print("  #");
                    printWriter.print(k);
                    printWriter.print(": ");
                    printWriter.println(x);
                }
            }
            monitorexit(this.mPendingActions);
            printWriter.print(s);
            printWriter.println("FragmentManager misc state:");
            printWriter.print(s);
            printWriter.print("  mHost=");
            printWriter.println(this.mHost);
            printWriter.print(s);
            printWriter.print("  mContainer=");
            printWriter.println(this.mContainer);
            if (this.mParent != null) {
                printWriter.print(s);
                printWriter.print("  mParent=");
                printWriter.println(this.mParent);
            }
            printWriter.print(s);
            printWriter.print("  mCurState=");
            printWriter.print(this.mCurState);
            printWriter.print(" mStateSaved=");
            printWriter.print(this.mStateSaved);
            printWriter.print(" mStopped=");
            printWriter.print(this.mStopped);
            printWriter.print(" mDestroyed=");
            printWriter.println(this.mDestroyed);
            if (this.mNeedMenuInvalidate) {
                printWriter.print(s);
                printWriter.print("  mNeedMenuInvalidate=");
                printWriter.println(this.mNeedMenuInvalidate);
            }
        }
    }
    
    void enqueueAction(@NonNull final OpGenerator e, final boolean b) {
        if (!b) {
            if (this.mHost == null) {
                if (this.mDestroyed) {
                    throw new IllegalStateException("FragmentManager has been destroyed");
                }
                throw new IllegalStateException("FragmentManager has not been attached to a host.");
            }
            else {
                this.checkStateLoss();
            }
        }
        synchronized (this.mPendingActions) {
            if (this.mHost != null) {
                this.mPendingActions.add(e);
                this.scheduleCommit();
                return;
            }
            if (b) {
                return;
            }
            throw new IllegalStateException("Activity has been destroyed");
        }
    }
    
    boolean execPendingActions(boolean b) {
        this.ensureExecReady(b);
        b = false;
        while (this.generateOpsForPendingActions(this.mTmpRecords, this.mTmpIsPop)) {
            b = true;
            this.mExecutingActions = true;
            try {
                this.removeRedundantOperationsAndExecute(this.mTmpRecords, this.mTmpIsPop);
                continue;
            }
            finally {
                this.cleanupExec();
            }
            break;
        }
        this.updateOnBackPressedCallbackEnabled();
        this.doPendingDeferredStart();
        this.mFragmentStore.burpActive();
        return b;
    }
    
    void execSingleAction(@NonNull final OpGenerator opGenerator, final boolean b) {
        if (b && (this.mHost == null || this.mDestroyed)) {
            return;
        }
        this.ensureExecReady(b);
        if (opGenerator.generateOps(this.mTmpRecords, this.mTmpIsPop)) {
            this.mExecutingActions = true;
            try {
                this.removeRedundantOperationsAndExecute(this.mTmpRecords, this.mTmpIsPop);
            }
            finally {
                this.cleanupExec();
            }
        }
        this.updateOnBackPressedCallbackEnabled();
        this.doPendingDeferredStart();
        this.mFragmentStore.burpActive();
    }
    
    public boolean executePendingTransactions() {
        final boolean execPendingActions = this.execPendingActions(true);
        this.forcePostponedTransactions();
        return execPendingActions;
    }
    
    @Nullable
    Fragment findActiveFragment(@NonNull final String s) {
        return this.mFragmentStore.findActiveFragment(s);
    }
    
    @Nullable
    public Fragment findFragmentById(@IdRes final int n) {
        return this.mFragmentStore.findFragmentById(n);
    }
    
    @Nullable
    public Fragment findFragmentByTag(@Nullable final String s) {
        return this.mFragmentStore.findFragmentByTag(s);
    }
    
    Fragment findFragmentByWho(@NonNull final String s) {
        return this.mFragmentStore.findFragmentByWho(s);
    }
    
    int getActiveFragmentCount() {
        return this.mFragmentStore.getActiveFragmentCount();
    }
    
    @NonNull
    List<Fragment> getActiveFragments() {
        return this.mFragmentStore.getActiveFragments();
    }
    
    @NonNull
    public BackStackEntry getBackStackEntryAt(final int index) {
        return (BackStackEntry)this.mBackStack.get(index);
    }
    
    public int getBackStackEntryCount() {
        final ArrayList<BackStackRecord> mBackStack = this.mBackStack;
        int size;
        if (mBackStack != null) {
            size = mBackStack.size();
        }
        else {
            size = 0;
        }
        return size;
    }
    
    @NonNull
    FragmentContainer getContainer() {
        return this.mContainer;
    }
    
    @Nullable
    public Fragment getFragment(@NonNull final Bundle bundle, @NonNull final String str) {
        final String string = ((BaseBundle)bundle).getString(str);
        if (string == null) {
            return null;
        }
        final Fragment activeFragment = this.findActiveFragment(string);
        if (activeFragment == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Fragment no longer exists for key ");
            sb.append(str);
            sb.append(": unique id ");
            sb.append(string);
            this.throwException(new IllegalStateException(sb.toString()));
        }
        return activeFragment;
    }
    
    @NonNull
    public FragmentFactory getFragmentFactory() {
        final FragmentFactory mFragmentFactory = this.mFragmentFactory;
        if (mFragmentFactory != null) {
            return mFragmentFactory;
        }
        final Fragment mParent = this.mParent;
        if (mParent != null) {
            return mParent.mFragmentManager.getFragmentFactory();
        }
        return this.mHostFragmentFactory;
    }
    
    @NonNull
    FragmentStore getFragmentStore() {
        return this.mFragmentStore;
    }
    
    @NonNull
    public List<Fragment> getFragments() {
        return this.mFragmentStore.getFragments();
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public FragmentHostCallback<?> getHost() {
        return this.mHost;
    }
    
    @NonNull
    LayoutInflater$Factory2 getLayoutInflaterFactory() {
        return (LayoutInflater$Factory2)this.mLayoutInflaterFactory;
    }
    
    @NonNull
    FragmentLifecycleCallbacksDispatcher getLifecycleCallbacksDispatcher() {
        return this.mLifecycleCallbacksDispatcher;
    }
    
    @Nullable
    Fragment getParent() {
        return this.mParent;
    }
    
    @Nullable
    public Fragment getPrimaryNavigationFragment() {
        return this.mPrimaryNav;
    }
    
    @NonNull
    SpecialEffectsControllerFactory getSpecialEffectsControllerFactory() {
        final SpecialEffectsControllerFactory mSpecialEffectsControllerFactory = this.mSpecialEffectsControllerFactory;
        if (mSpecialEffectsControllerFactory != null) {
            return mSpecialEffectsControllerFactory;
        }
        final Fragment mParent = this.mParent;
        if (mParent != null) {
            return mParent.mFragmentManager.getSpecialEffectsControllerFactory();
        }
        return this.mDefaultSpecialEffectsControllerFactory;
    }
    
    @Nullable
    public FragmentStrictMode.Policy getStrictModePolicy() {
        return this.mStrictModePolicy;
    }
    
    @NonNull
    ViewModelStore getViewModelStore(@NonNull final Fragment fragment) {
        return this.mNonConfig.getViewModelStore(fragment);
    }
    
    void handleOnBackPressed() {
        this.execPendingActions(true);
        if (this.mOnBackPressedCallback.isEnabled()) {
            this.popBackStackImmediate();
        }
        else {
            this.mOnBackPressedDispatcher.onBackPressed();
        }
    }
    
    void hideFragment(@NonNull final Fragment fragment) {
        if (isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("hide: ");
            sb.append(fragment);
        }
        if (!fragment.mHidden) {
            fragment.mHidden = true;
            fragment.mHiddenChanged ^= true;
            this.setVisibleRemovingFragment(fragment);
        }
    }
    
    void invalidateMenuForFragment(@NonNull final Fragment fragment) {
        if (fragment.mAdded && this.isMenuAvailable(fragment)) {
            this.mNeedMenuInvalidate = true;
        }
    }
    
    public boolean isDestroyed() {
        return this.mDestroyed;
    }
    
    boolean isParentHidden(@Nullable final Fragment fragment) {
        return fragment != null && fragment.isHidden();
    }
    
    boolean isParentMenuVisible(@Nullable final Fragment fragment) {
        return fragment == null || fragment.isMenuVisible();
    }
    
    boolean isPrimaryNavigation(@Nullable final Fragment fragment) {
        boolean b = true;
        if (fragment == null) {
            return true;
        }
        final FragmentManager mFragmentManager = fragment.mFragmentManager;
        if (!fragment.equals(mFragmentManager.getPrimaryNavigationFragment()) || !this.isPrimaryNavigation(mFragmentManager.mParent)) {
            b = false;
        }
        return b;
    }
    
    boolean isStateAtLeast(final int n) {
        return this.mCurState >= n;
    }
    
    public boolean isStateSaved() {
        return this.mStateSaved || this.mStopped;
    }
    
    void launchRequestPermissions(@NonNull final Fragment fragment, @NonNull final String[] array, final int n) {
        if (this.mRequestPermissions != null) {
            this.mLaunchedFragments.addLast(new LaunchedFragmentInfo(fragment.mWho, n));
            this.mRequestPermissions.launch(array);
        }
        else {
            this.mHost.onRequestPermissionsFromFragment(fragment, array, n);
        }
    }
    
    void launchStartActivityForResult(@NonNull final Fragment fragment, @SuppressLint({ "UnknownNullness" }) final Intent intent, final int n, @Nullable final Bundle bundle) {
        if (this.mStartActivityForResult != null) {
            this.mLaunchedFragments.addLast(new LaunchedFragmentInfo(fragment.mWho, n));
            if (intent != null && bundle != null) {
                intent.putExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE", bundle);
            }
            this.mStartActivityForResult.launch(intent);
        }
        else {
            this.mHost.onStartActivityFromFragment(fragment, intent, n, bundle);
        }
    }
    
    void launchStartIntentSenderForResult(@NonNull final Fragment fragment, @SuppressLint({ "UnknownNullness" }) final IntentSender intentSender, final int n, @Nullable Intent intent, final int n2, final int n3, final int n4, @Nullable final Bundle obj) throws IntentSender$SendIntentException {
        if (this.mStartIntentSenderForResult != null) {
            if (obj != null) {
                if (intent == null) {
                    intent = new Intent();
                    intent.putExtra("androidx.fragment.extra.ACTIVITY_OPTIONS_BUNDLE", true);
                }
                if (isLoggingEnabled(2)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("ActivityOptions ");
                    sb.append(obj);
                    sb.append(" were added to fillInIntent ");
                    sb.append(intent);
                    sb.append(" for fragment ");
                    sb.append(fragment);
                }
                intent.putExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE", obj);
            }
            final IntentSenderRequest build = new IntentSenderRequest.Builder(intentSender).setFillInIntent(intent).setFlags(n3, n2).build();
            this.mLaunchedFragments.addLast(new LaunchedFragmentInfo(fragment.mWho, n));
            if (isLoggingEnabled(2)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Fragment ");
                sb2.append(fragment);
                sb2.append("is launching an IntentSender for result ");
            }
            this.mStartIntentSenderForResult.launch(build);
        }
        else {
            this.mHost.onStartIntentSenderFromFragment(fragment, intentSender, n, intent, n2, n3, n4, obj);
        }
    }
    
    void moveToState(final int mCurState, final boolean b) {
        if (this.mHost == null && mCurState != -1) {
            throw new IllegalStateException("No activity");
        }
        if (!b && mCurState == this.mCurState) {
            return;
        }
        this.mCurState = mCurState;
        this.mFragmentStore.moveToExpectedState();
        this.startPendingDeferredFragments();
        if (this.mNeedMenuInvalidate) {
            final FragmentHostCallback<?> mHost = this.mHost;
            if (mHost != null && this.mCurState == 7) {
                mHost.onSupportInvalidateOptionsMenu();
                this.mNeedMenuInvalidate = false;
            }
        }
    }
    
    void noteStateNotSaved() {
        if (this.mHost == null) {
            return;
        }
        this.mStateSaved = false;
        this.mStopped = false;
        this.mNonConfig.setIsStateSaved(false);
        for (final Fragment fragment : this.mFragmentStore.getFragments()) {
            if (fragment != null) {
                fragment.noteStateNotSaved();
            }
        }
    }
    
    void onContainerAvailable(@NonNull final FragmentContainerView mContainer) {
        for (final FragmentStateManager fragmentStateManager : this.mFragmentStore.getActiveFragmentStateManagers()) {
            final Fragment fragment = fragmentStateManager.getFragment();
            if (fragment.mContainerId == ((View)mContainer).getId()) {
                final View mView = fragment.mView;
                if (mView == null || mView.getParent() != null) {
                    continue;
                }
                fragment.mContainer = (ViewGroup)mContainer;
                fragmentStateManager.addViewToContainer();
            }
        }
    }
    
    @Deprecated
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public FragmentTransaction openTransaction() {
        return this.beginTransaction();
    }
    
    void performPendingDeferredStart(@NonNull final FragmentStateManager fragmentStateManager) {
        final Fragment fragment = fragmentStateManager.getFragment();
        if (fragment.mDeferStart) {
            if (this.mExecutingActions) {
                this.mHavePendingDeferredStart = true;
                return;
            }
            fragment.mDeferStart = false;
            fragmentStateManager.moveToExpectedState();
        }
    }
    
    public void popBackStack() {
        this.enqueueAction((OpGenerator)new PopBackStackState(null, -1, 0), false);
    }
    
    public void popBackStack(final int n, final int n2) {
        this.popBackStack(n, n2, false);
    }
    
    void popBackStack(final int i, final int n, final boolean b) {
        if (i >= 0) {
            this.enqueueAction((OpGenerator)new PopBackStackState(null, i, n), b);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Bad id: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void popBackStack(@Nullable final String s, final int n) {
        this.enqueueAction((OpGenerator)new PopBackStackState(s, -1, n), false);
    }
    
    public boolean popBackStackImmediate() {
        return this.popBackStackImmediate(null, -1, 0);
    }
    
    public boolean popBackStackImmediate(final int i, final int n) {
        if (i >= 0) {
            return this.popBackStackImmediate(null, i, n);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Bad id: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public boolean popBackStackImmediate(@Nullable final String s, final int n) {
        return this.popBackStackImmediate(s, -1, n);
    }
    
    boolean popBackStackState(@NonNull final ArrayList<BackStackRecord> list, @NonNull final ArrayList<Boolean> list2, @Nullable final String s, int i, int backStackIndex) {
        backStackIndex = this.findBackStackIndex(s, i, (backStackIndex & 0x1) != 0x0);
        if (backStackIndex < 0) {
            return false;
        }
        for (i = this.mBackStack.size() - 1; i >= backStackIndex; --i) {
            list.add(this.mBackStack.remove(i));
            list2.add(Boolean.TRUE);
        }
        return true;
    }
    
    public void putFragment(@NonNull final Bundle bundle, @NonNull final String s, @NonNull final Fragment obj) {
        if (obj.mFragmentManager != this) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Fragment ");
            sb.append(obj);
            sb.append(" is not currently in the FragmentManager");
            this.throwException(new IllegalStateException(sb.toString()));
        }
        ((BaseBundle)bundle).putString(s, obj.mWho);
    }
    
    public void registerFragmentLifecycleCallbacks(@NonNull final FragmentLifecycleCallbacks fragmentLifecycleCallbacks, final boolean b) {
        this.mLifecycleCallbacksDispatcher.registerFragmentLifecycleCallbacks(fragmentLifecycleCallbacks, b);
    }
    
    void removeFragment(@NonNull final Fragment fragment) {
        if (isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("remove: ");
            sb.append(fragment);
            sb.append(" nesting=");
            sb.append(fragment.mBackStackNesting);
        }
        final boolean inBackStack = fragment.isInBackStack();
        if (!fragment.mDetached || (inBackStack ^ true)) {
            this.mFragmentStore.removeFragment(fragment);
            if (this.isMenuAvailable(fragment)) {
                this.mNeedMenuInvalidate = true;
            }
            fragment.mRemoving = true;
            this.setVisibleRemovingFragment(fragment);
        }
    }
    
    public void removeFragmentOnAttachListener(@NonNull final FragmentOnAttachListener o) {
        this.mOnAttachListeners.remove(o);
    }
    
    public void removeOnBackStackChangedListener(@NonNull final OnBackStackChangedListener o) {
        final ArrayList<OnBackStackChangedListener> mBackStackChangeListeners = this.mBackStackChangeListeners;
        if (mBackStackChangeListeners != null) {
            mBackStackChangeListeners.remove(o);
        }
    }
    
    void removeRetainedFragment(@NonNull final Fragment fragment) {
        this.mNonConfig.removeRetainedFragment(fragment);
    }
    
    void restoreAllState(@Nullable final Parcelable parcelable, @Nullable final FragmentManagerNonConfig fragmentManagerNonConfig) {
        if (this.mHost instanceof ViewModelStoreOwner) {
            this.throwException(new IllegalStateException("You must use restoreSaveState when your FragmentHostCallback implements ViewModelStoreOwner"));
        }
        this.mNonConfig.restoreFromSnapshot(fragmentManagerNonConfig);
        this.restoreSaveStateInternal(parcelable);
    }
    
    public void restoreBackStack(@NonNull final String s) {
        this.enqueueAction((OpGenerator)new RestoreBackStackState(s), false);
    }
    
    boolean restoreBackStackState(@NonNull final ArrayList<BackStackRecord> list, @NonNull final ArrayList<Boolean> list2, @NonNull final String s) {
        final BackStackState backStackState = this.mBackStackStates.remove(s);
        if (backStackState == null) {
            return false;
        }
        final HashMap hashMap = new HashMap();
        for (final BackStackRecord backStackRecord : list) {
            if (backStackRecord.mBeingSaved) {
                final Iterator<FragmentTransaction.Op> iterator2 = backStackRecord.mOps.iterator();
                while (iterator2.hasNext()) {
                    final Fragment mFragment = ((FragmentTransaction.Op)iterator2.next()).mFragment;
                    if (mFragment != null) {
                        hashMap.put(mFragment.mWho, mFragment);
                    }
                }
            }
        }
        final Iterator<BackStackRecord> iterator3 = backStackState.instantiate(this, hashMap).iterator();
        boolean b = false;
    Label_0134:
        while (true) {
            b = false;
            while (iterator3.hasNext()) {
                if (!iterator3.next().generateOps(list, list2) && !b) {
                    continue Label_0134;
                }
                b = true;
            }
            break;
        }
        return b;
    }
    
    void restoreSaveState(@Nullable final Parcelable parcelable) {
        if (this.mHost instanceof SavedStateRegistryOwner) {
            this.throwException(new IllegalStateException("You cannot use restoreSaveState when your FragmentHostCallback implements SavedStateRegistryOwner."));
        }
        this.restoreSaveStateInternal(parcelable);
    }
    
    void restoreSaveStateInternal(@Nullable final Parcelable parcelable) {
        if (parcelable == null) {
            return;
        }
        final Bundle bundle = (Bundle)parcelable;
        for (final String s : ((BaseBundle)bundle).keySet()) {
            if (s.startsWith("result_")) {
                final Bundle bundle2 = bundle.getBundle(s);
                if (bundle2 == null) {
                    continue;
                }
                bundle2.setClassLoader(this.mHost.getContext().getClassLoader());
                this.mResults.put(s.substring(7), bundle2);
            }
        }
        final ArrayList<FragmentState> list = new ArrayList<FragmentState>();
        for (final String s2 : ((BaseBundle)bundle).keySet()) {
            if (s2.startsWith("fragment_")) {
                final Bundle bundle3 = bundle.getBundle(s2);
                if (bundle3 == null) {
                    continue;
                }
                bundle3.setClassLoader(this.mHost.getContext().getClassLoader());
                list.add((FragmentState)bundle3.getParcelable("state"));
            }
        }
        this.mFragmentStore.restoreSaveState(list);
        final FragmentManagerState fragmentManagerState = (FragmentManagerState)bundle.getParcelable("state");
        if (fragmentManagerState == null) {
            return;
        }
        this.mFragmentStore.resetActiveFragments();
        final Iterator<String> iterator3 = fragmentManagerState.mActive.iterator();
        while (iterator3.hasNext()) {
            final FragmentState setSavedState = this.mFragmentStore.setSavedState(iterator3.next(), null);
            if (setSavedState != null) {
                final Fragment retainedFragmentByWho = this.mNonConfig.findRetainedFragmentByWho(setSavedState.mWho);
                FragmentStateManager fragmentStateManager;
                if (retainedFragmentByWho != null) {
                    if (isLoggingEnabled(2)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("restoreSaveState: re-attaching retained ");
                        sb.append(retainedFragmentByWho);
                    }
                    fragmentStateManager = new FragmentStateManager(this.mLifecycleCallbacksDispatcher, this.mFragmentStore, retainedFragmentByWho, setSavedState);
                }
                else {
                    fragmentStateManager = new FragmentStateManager(this.mLifecycleCallbacksDispatcher, this.mFragmentStore, this.mHost.getContext().getClassLoader(), this.getFragmentFactory(), setSavedState);
                }
                final Fragment fragment = fragmentStateManager.getFragment();
                fragment.mFragmentManager = this;
                if (isLoggingEnabled(2)) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("restoreSaveState: active (");
                    sb2.append(fragment.mWho);
                    sb2.append("): ");
                    sb2.append(fragment);
                }
                fragmentStateManager.restoreState(this.mHost.getContext().getClassLoader());
                this.mFragmentStore.makeActive(fragmentStateManager);
                fragmentStateManager.setFragmentManagerState(this.mCurState);
            }
        }
        for (final Fragment obj : this.mNonConfig.getRetainedFragments()) {
            if (!this.mFragmentStore.containsActiveFragment(obj.mWho)) {
                if (isLoggingEnabled(2)) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Discarding retained Fragment ");
                    sb3.append(obj);
                    sb3.append(" that was not found in the set of active Fragments ");
                    sb3.append(fragmentManagerState.mActive);
                }
                this.mNonConfig.removeRetainedFragment(obj);
                obj.mFragmentManager = this;
                final FragmentStateManager fragmentStateManager2 = new FragmentStateManager(this.mLifecycleCallbacksDispatcher, this.mFragmentStore, obj);
                fragmentStateManager2.setFragmentManagerState(1);
                fragmentStateManager2.moveToExpectedState();
                obj.mRemoving = true;
                fragmentStateManager2.moveToExpectedState();
            }
        }
        this.mFragmentStore.restoreAddedFragments(fragmentManagerState.mAdded);
        final BackStackRecordState[] mBackStack = fragmentManagerState.mBackStack;
        final int n = 0;
        if (mBackStack != null) {
            this.mBackStack = new ArrayList<BackStackRecord>(fragmentManagerState.mBackStack.length);
            int i = 0;
            while (true) {
                final BackStackRecordState[] mBackStack2 = fragmentManagerState.mBackStack;
                if (i >= mBackStack2.length) {
                    break;
                }
                final BackStackRecord instantiate = mBackStack2[i].instantiate(this);
                if (isLoggingEnabled(2)) {
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("restoreAllState: back stack #");
                    sb4.append(i);
                    sb4.append(" (index ");
                    sb4.append(instantiate.mIndex);
                    sb4.append("): ");
                    sb4.append(instantiate);
                    final PrintWriter printWriter = new PrintWriter(new LogWriter("FragmentManager"));
                    instantiate.dump("  ", printWriter, false);
                    printWriter.close();
                }
                this.mBackStack.add(instantiate);
                ++i;
            }
        }
        else {
            this.mBackStack = null;
        }
        this.mBackStackIndex.set(fragmentManagerState.mBackStackIndex);
        final String mPrimaryNavActiveWho = fragmentManagerState.mPrimaryNavActiveWho;
        if (mPrimaryNavActiveWho != null) {
            this.dispatchParentPrimaryNavigationFragmentChanged(this.mPrimaryNav = this.findActiveFragment(mPrimaryNavActiveWho));
        }
        final ArrayList<String> mBackStackStateKeys = fragmentManagerState.mBackStackStateKeys;
        if (mBackStackStateKeys != null) {
            for (int j = n; j < mBackStackStateKeys.size(); ++j) {
                this.mBackStackStates.put((String)mBackStackStateKeys.get(j), fragmentManagerState.mBackStackStates.get(j));
            }
        }
        this.mLaunchedFragments = new ArrayDeque<LaunchedFragmentInfo>(fragmentManagerState.mLaunchedFragments);
    }
    
    @Deprecated
    FragmentManagerNonConfig retainNonConfig() {
        if (this.mHost instanceof ViewModelStoreOwner) {
            this.throwException(new IllegalStateException("You cannot use retainNonConfig when your FragmentHostCallback implements ViewModelStoreOwner."));
        }
        return this.mNonConfig.getSnapshot();
    }
    
    Parcelable saveAllState() {
        if (this.mHost instanceof SavedStateRegistryOwner) {
            this.throwException(new IllegalStateException("You cannot use saveAllState when your FragmentHostCallback implements SavedStateRegistryOwner."));
        }
        Object saveAllStateInternal;
        if (((BaseBundle)(saveAllStateInternal = this.saveAllStateInternal())).isEmpty()) {
            saveAllStateInternal = null;
        }
        return (Parcelable)saveAllStateInternal;
    }
    
    @NonNull
    Bundle saveAllStateInternal() {
        final Bundle bundle = new Bundle();
        this.forcePostponedTransactions();
        this.endAnimatingAwayFragments();
        this.execPendingActions(true);
        this.mStateSaved = true;
        this.mNonConfig.setIsStateSaved(true);
        final ArrayList<String> saveActiveFragments = this.mFragmentStore.saveActiveFragments();
        final ArrayList<FragmentState> allSavedState = this.mFragmentStore.getAllSavedState();
        if (allSavedState.isEmpty()) {
            isLoggingEnabled(2);
        }
        else {
            final ArrayList<String> saveAddedFragments = this.mFragmentStore.saveAddedFragments();
            final ArrayList<BackStackRecord> mBackStack = this.mBackStack;
            BackStackRecordState[] mBackStack2 = null;
            Label_0193: {
                if (mBackStack != null) {
                    final int size = mBackStack.size();
                    if (size > 0) {
                        final BackStackRecordState[] array = new BackStackRecordState[size];
                        int index = 0;
                        while (true) {
                            mBackStack2 = array;
                            if (index >= size) {
                                break Label_0193;
                            }
                            array[index] = new BackStackRecordState(this.mBackStack.get(index));
                            if (isLoggingEnabled(2)) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("saveAllState: adding back stack #");
                                sb.append(index);
                                sb.append(": ");
                                sb.append(this.mBackStack.get(index));
                            }
                            ++index;
                        }
                    }
                }
                mBackStack2 = null;
            }
            final FragmentManagerState fragmentManagerState = new FragmentManagerState();
            fragmentManagerState.mActive = saveActiveFragments;
            fragmentManagerState.mAdded = saveAddedFragments;
            fragmentManagerState.mBackStack = mBackStack2;
            fragmentManagerState.mBackStackIndex = this.mBackStackIndex.get();
            final Fragment mPrimaryNav = this.mPrimaryNav;
            if (mPrimaryNav != null) {
                fragmentManagerState.mPrimaryNavActiveWho = mPrimaryNav.mWho;
            }
            fragmentManagerState.mBackStackStateKeys.addAll(this.mBackStackStates.keySet());
            fragmentManagerState.mBackStackStates.addAll(this.mBackStackStates.values());
            fragmentManagerState.mLaunchedFragments = new ArrayList<LaunchedFragmentInfo>((Collection<? extends LaunchedFragmentInfo>)this.mLaunchedFragments);
            bundle.putParcelable("state", (Parcelable)fragmentManagerState);
            for (final String str : this.mResults.keySet()) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("result_");
                sb2.append(str);
                bundle.putBundle(sb2.toString(), (Bundle)this.mResults.get(str));
            }
            for (final FragmentState fragmentState : allSavedState) {
                final Bundle bundle2 = new Bundle();
                bundle2.putParcelable("state", (Parcelable)fragmentState);
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("fragment_");
                sb3.append(fragmentState.mWho);
                bundle.putBundle(sb3.toString(), bundle2);
            }
        }
        return bundle;
    }
    
    public void saveBackStack(@NonNull final String s) {
        this.enqueueAction((OpGenerator)new SaveBackStackState(s), false);
    }
    
    boolean saveBackStackState(@NonNull final ArrayList<BackStackRecord> list, @NonNull final ArrayList<Boolean> list2, @NonNull final String str) {
        final int backStackIndex = this.findBackStackIndex(str, -1, true);
        if (backStackIndex < 0) {
            return false;
        }
        for (int i = backStackIndex; i < this.mBackStack.size(); ++i) {
            final BackStackRecord obj = this.mBackStack.get(i);
            if (!obj.mReorderingAllowed) {
                final StringBuilder sb = new StringBuilder();
                sb.append("saveBackStack(\"");
                sb.append(str);
                sb.append("\") included FragmentTransactions must use setReorderingAllowed(true) to ensure that the back stack can be restored as an atomic operation. Found ");
                sb.append(obj);
                sb.append(" that did not use setReorderingAllowed(true).");
                this.throwException(new IllegalArgumentException(sb.toString()));
            }
        }
        final HashSet<Fragment> c = new HashSet<Fragment>();
        for (int j = backStackIndex; j < this.mBackStack.size(); ++j) {
            final BackStackRecord obj2 = this.mBackStack.get(j);
            final HashSet<Fragment> obj3 = new HashSet<Fragment>();
            final HashSet<Fragment> c2 = new HashSet<Fragment>();
            for (final FragmentTransaction.Op op : obj2.mOps) {
                final Fragment mFragment = op.mFragment;
                if (mFragment == null) {
                    continue;
                }
                Label_0281: {
                    if (op.mFromExpandedOp) {
                        final int mCmd = op.mCmd;
                        if (mCmd != 1 && mCmd != 2 && mCmd != 8) {
                            break Label_0281;
                        }
                    }
                    c.add(mFragment);
                    obj3.add(mFragment);
                }
                final int mCmd2 = op.mCmd;
                if (mCmd2 != 1 && mCmd2 != 2) {
                    continue;
                }
                c2.add(mFragment);
            }
            obj3.removeAll(c2);
            if (!obj3.isEmpty()) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("saveBackStack(\"");
                sb2.append(str);
                sb2.append("\") must be self contained and not reference fragments from non-saved FragmentTransactions. Found reference to fragment");
                String str2;
                if (obj3.size() == 1) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append(" ");
                    sb3.append(obj3.iterator().next());
                    str2 = sb3.toString();
                }
                else {
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("s ");
                    sb4.append(obj3);
                    str2 = sb4.toString();
                }
                sb2.append(str2);
                sb2.append(" in ");
                sb2.append(obj2);
                sb2.append(" that were previously added to the FragmentManager through a separate FragmentTransaction.");
                this.throwException(new IllegalArgumentException(sb2.toString()));
            }
        }
        final ArrayDeque arrayDeque = new ArrayDeque(c);
        while (!arrayDeque.isEmpty()) {
            final Fragment fragment = (Fragment)arrayDeque.removeFirst();
            if (fragment.mRetainInstance) {
                final StringBuilder sb5 = new StringBuilder();
                sb5.append("saveBackStack(\"");
                sb5.append(str);
                sb5.append("\") must not contain retained fragments. Found ");
                String str3;
                if (c.contains(fragment)) {
                    str3 = "direct reference to retained ";
                }
                else {
                    str3 = "retained child ";
                }
                sb5.append(str3);
                sb5.append("fragment ");
                sb5.append(fragment);
                this.throwException(new IllegalArgumentException(sb5.toString()));
            }
            for (final Fragment e : fragment.mChildFragmentManager.getActiveFragments()) {
                if (e != null) {
                    arrayDeque.addLast((Object)e);
                }
            }
        }
        final ArrayList<String> list3 = new ArrayList<String>();
        final Iterator<Fragment> iterator3 = c.iterator();
        while (iterator3.hasNext()) {
            list3.add(iterator3.next().mWho);
        }
        final ArrayList list4 = new ArrayList<BackStackRecordState>(this.mBackStack.size() - backStackIndex);
        for (int k = backStackIndex; k < this.mBackStack.size(); ++k) {
            list4.add(null);
        }
        final BackStackState backStackState = new BackStackState(list3, (List<BackStackRecordState>)list4);
        for (int l = this.mBackStack.size() - 1; l >= backStackIndex; --l) {
            final BackStackRecord e2 = this.mBackStack.remove(l);
            final BackStackRecord backStackRecord = new BackStackRecord(e2);
            backStackRecord.collapseOps();
            list4.set(l - backStackIndex, new BackStackRecordState(backStackRecord));
            e2.mBeingSaved = true;
            list.add(e2);
            list2.add(Boolean.TRUE);
        }
        this.mBackStackStates.put(str, backStackState);
        return true;
    }
    
    @Nullable
    public Fragment.SavedState saveFragmentInstanceState(@NonNull final Fragment obj) {
        final FragmentStateManager fragmentStateManager = this.mFragmentStore.getFragmentStateManager(obj.mWho);
        if (fragmentStateManager == null || !fragmentStateManager.getFragment().equals(obj)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Fragment ");
            sb.append(obj);
            sb.append(" is not currently in the FragmentManager");
            this.throwException(new IllegalStateException(sb.toString()));
        }
        return fragmentStateManager.saveInstanceState();
    }
    
    void scheduleCommit() {
        synchronized (this.mPendingActions) {
            final int size = this.mPendingActions.size();
            boolean b = true;
            if (size != 1) {
                b = false;
            }
            if (b) {
                this.mHost.getHandler().removeCallbacks(this.mExecCommit);
                this.mHost.getHandler().post(this.mExecCommit);
                this.updateOnBackPressedCallbackEnabled();
            }
        }
    }
    
    void setExitAnimationOrder(@NonNull final Fragment fragment, final boolean b) {
        final ViewGroup fragmentContainer = this.getFragmentContainer(fragment);
        if (fragmentContainer != null && fragmentContainer instanceof FragmentContainerView) {
            ((FragmentContainerView)fragmentContainer).setDrawDisappearingViewsLast(b ^ true);
        }
    }
    
    public void setFragmentFactory(@NonNull final FragmentFactory mFragmentFactory) {
        this.mFragmentFactory = mFragmentFactory;
    }
    
    @Override
    public final void setFragmentResult(@NonNull final String str, @NonNull final Bundle obj) {
        final LifecycleAwareResultListener lifecycleAwareResultListener = this.mResultListeners.get(str);
        if (lifecycleAwareResultListener != null && lifecycleAwareResultListener.isAtLeast(Lifecycle.State.STARTED)) {
            lifecycleAwareResultListener.onFragmentResult(str, obj);
        }
        else {
            this.mResults.put(str, obj);
        }
        if (isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Setting fragment result with key ");
            sb.append(str);
            sb.append(" and result ");
            sb.append(obj);
        }
    }
    
    @SuppressLint({ "SyntheticAccessor" })
    @Override
    public final void setFragmentResultListener(@NonNull final String str, @NonNull final LifecycleOwner lifecycleOwner, @NonNull final FragmentResultListener obj) {
        final Lifecycle lifecycle = lifecycleOwner.getLifecycle();
        if (lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
            return;
        }
        final LifecycleEventObserver lifecycleEventObserver = new LifecycleEventObserver(this, str, obj, lifecycle) {
            final FragmentManager this$0;
            final Lifecycle val$lifecycle;
            final FragmentResultListener val$listener;
            final String val$requestKey;
            
            @Override
            public void onStateChanged(@NonNull final LifecycleOwner lifecycleOwner, @NonNull final Lifecycle.Event event) {
                if (event == Lifecycle.Event.ON_START) {
                    final Bundle bundle = this.this$0.mResults.get(this.val$requestKey);
                    if (bundle != null) {
                        this.val$listener.onFragmentResult(this.val$requestKey, bundle);
                        this.this$0.clearFragmentResult(this.val$requestKey);
                    }
                }
                if (event == Lifecycle.Event.ON_DESTROY) {
                    this.val$lifecycle.removeObserver(this);
                    this.this$0.mResultListeners.remove(this.val$requestKey);
                }
            }
        };
        lifecycle.addObserver(lifecycleEventObserver);
        final LifecycleAwareResultListener lifecycleAwareResultListener = this.mResultListeners.put(str, new LifecycleAwareResultListener(lifecycle, obj, lifecycleEventObserver));
        if (lifecycleAwareResultListener != null) {
            lifecycleAwareResultListener.removeObserver();
        }
        if (isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Setting FragmentResultListener with key ");
            sb.append(str);
            sb.append(" lifecycleOwner ");
            sb.append(lifecycle);
            sb.append(" and listener ");
            sb.append(obj);
        }
    }
    
    void setMaxLifecycle(@NonNull final Fragment obj, @NonNull final Lifecycle.State mMaxState) {
        if (obj.equals(this.findActiveFragment(obj.mWho)) && (obj.mHost == null || obj.mFragmentManager == this)) {
            obj.mMaxState = mMaxState;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fragment ");
        sb.append(obj);
        sb.append(" is not an active fragment of FragmentManager ");
        sb.append(this);
        throw new IllegalArgumentException(sb.toString());
    }
    
    void setPrimaryNavigationFragment(@Nullable final Fragment fragment) {
        Label_0085: {
            if (fragment != null) {
                if (fragment.equals(this.findActiveFragment(fragment.mWho))) {
                    if (fragment.mHost == null) {
                        break Label_0085;
                    }
                    if (fragment.mFragmentManager == this) {
                        break Label_0085;
                    }
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Fragment ");
                sb.append(fragment);
                sb.append(" is not an active fragment of FragmentManager ");
                sb.append(this);
                throw new IllegalArgumentException(sb.toString());
            }
        }
        final Fragment mPrimaryNav = this.mPrimaryNav;
        this.mPrimaryNav = fragment;
        this.dispatchParentPrimaryNavigationFragmentChanged(mPrimaryNav);
        this.dispatchParentPrimaryNavigationFragmentChanged(this.mPrimaryNav);
    }
    
    void setSpecialEffectsControllerFactory(@NonNull final SpecialEffectsControllerFactory mSpecialEffectsControllerFactory) {
        this.mSpecialEffectsControllerFactory = mSpecialEffectsControllerFactory;
    }
    
    public void setStrictModePolicy(@Nullable final FragmentStrictMode.Policy mStrictModePolicy) {
        this.mStrictModePolicy = mStrictModePolicy;
    }
    
    void showFragment(@NonNull final Fragment obj) {
        if (isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("show: ");
            sb.append(obj);
        }
        if (obj.mHidden) {
            obj.mHidden = false;
            obj.mHiddenChanged ^= true;
        }
    }
    
    @NonNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(128);
        sb.append("FragmentManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        final Fragment mParent = this.mParent;
        if (mParent != null) {
            sb.append(mParent.getClass().getSimpleName());
            sb.append("{");
            sb.append(Integer.toHexString(System.identityHashCode(this.mParent)));
            sb.append("}");
        }
        else {
            final FragmentHostCallback<?> mHost = this.mHost;
            if (mHost != null) {
                sb.append(mHost.getClass().getSimpleName());
                sb.append("{");
                sb.append(Integer.toHexString(System.identityHashCode(this.mHost)));
                sb.append("}");
            }
            else {
                sb.append("null");
            }
        }
        sb.append("}}");
        return sb.toString();
    }
    
    public void unregisterFragmentLifecycleCallbacks(@NonNull final FragmentLifecycleCallbacks fragmentLifecycleCallbacks) {
        this.mLifecycleCallbacksDispatcher.unregisterFragmentLifecycleCallbacks(fragmentLifecycleCallbacks);
    }
    
    public interface BackStackEntry
    {
        @Deprecated
        @Nullable
        CharSequence getBreadCrumbShortTitle();
        
        @Deprecated
        @StringRes
        int getBreadCrumbShortTitleRes();
        
        @Deprecated
        @Nullable
        CharSequence getBreadCrumbTitle();
        
        @Deprecated
        @StringRes
        int getBreadCrumbTitleRes();
        
        int getId();
        
        @Nullable
        String getName();
    }
    
    private class ClearBackStackState implements OpGenerator
    {
        private final String mName;
        final FragmentManager this$0;
        
        ClearBackStackState(@NonNull final FragmentManager this$0, final String mName) {
            this.this$0 = this$0;
            this.mName = mName;
        }
        
        @Override
        public boolean generateOps(@NonNull final ArrayList<BackStackRecord> list, @NonNull final ArrayList<Boolean> list2) {
            return this.this$0.clearBackStackState(list, list2, this.mName);
        }
    }
    
    interface OpGenerator
    {
        boolean generateOps(@NonNull final ArrayList<BackStackRecord> p0, @NonNull final ArrayList<Boolean> p1);
    }
    
    static class FragmentIntentSenderContract extends ActivityResultContract<IntentSenderRequest, ActivityResult>
    {
        @NonNull
        @Override
        public Intent createIntent(@NonNull final Context context, final IntentSenderRequest intentSenderRequest) {
            final Intent obj = new Intent("androidx.activity.result.contract.action.INTENT_SENDER_REQUEST");
            final Intent fillInIntent = intentSenderRequest.getFillInIntent();
            IntentSenderRequest build = intentSenderRequest;
            if (fillInIntent != null) {
                final Bundle bundleExtra = fillInIntent.getBundleExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE");
                build = intentSenderRequest;
                if (bundleExtra != null) {
                    obj.putExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE", bundleExtra);
                    fillInIntent.removeExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE");
                    build = intentSenderRequest;
                    if (fillInIntent.getBooleanExtra("androidx.fragment.extra.ACTIVITY_OPTIONS_BUNDLE", false)) {
                        build = new IntentSenderRequest.Builder(intentSenderRequest.getIntentSender()).setFillInIntent(null).setFlags(intentSenderRequest.getFlagsValues(), intentSenderRequest.getFlagsMask()).build();
                    }
                }
            }
            obj.putExtra("androidx.activity.result.contract.extra.INTENT_SENDER_REQUEST", (Parcelable)build);
            if (FragmentManager.isLoggingEnabled(2)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("CreateIntent created the following intent: ");
                sb.append(obj);
            }
            return obj;
        }
        
        @NonNull
        @Override
        public ActivityResult parseResult(final int n, @Nullable final Intent intent) {
            return new ActivityResult(n, intent);
        }
    }
    
    public abstract static class FragmentLifecycleCallbacks
    {
        @Deprecated
        public void onFragmentActivityCreated(@NonNull final FragmentManager fragmentManager, @NonNull final Fragment fragment, @Nullable final Bundle bundle) {
        }
        
        public void onFragmentAttached(@NonNull final FragmentManager fragmentManager, @NonNull final Fragment fragment, @NonNull final Context context) {
        }
        
        public void onFragmentCreated(@NonNull final FragmentManager fragmentManager, @NonNull final Fragment fragment, @Nullable final Bundle bundle) {
        }
        
        public void onFragmentDestroyed(@NonNull final FragmentManager fragmentManager, @NonNull final Fragment fragment) {
        }
        
        public void onFragmentDetached(@NonNull final FragmentManager fragmentManager, @NonNull final Fragment fragment) {
        }
        
        public void onFragmentPaused(@NonNull final FragmentManager fragmentManager, @NonNull final Fragment fragment) {
        }
        
        public void onFragmentPreAttached(@NonNull final FragmentManager fragmentManager, @NonNull final Fragment fragment, @NonNull final Context context) {
        }
        
        public void onFragmentPreCreated(@NonNull final FragmentManager fragmentManager, @NonNull final Fragment fragment, @Nullable final Bundle bundle) {
        }
        
        public void onFragmentResumed(@NonNull final FragmentManager fragmentManager, @NonNull final Fragment fragment) {
        }
        
        public void onFragmentSaveInstanceState(@NonNull final FragmentManager fragmentManager, @NonNull final Fragment fragment, @NonNull final Bundle bundle) {
        }
        
        public void onFragmentStarted(@NonNull final FragmentManager fragmentManager, @NonNull final Fragment fragment) {
        }
        
        public void onFragmentStopped(@NonNull final FragmentManager fragmentManager, @NonNull final Fragment fragment) {
        }
        
        public void onFragmentViewCreated(@NonNull final FragmentManager fragmentManager, @NonNull final Fragment fragment, @NonNull final View view, @Nullable final Bundle bundle) {
        }
        
        public void onFragmentViewDestroyed(@NonNull final FragmentManager fragmentManager, @NonNull final Fragment fragment) {
        }
    }
    
    @SuppressLint({ "BanParcelableUsage" })
    static class LaunchedFragmentInfo implements Parcelable
    {
        public static final Parcelable$Creator<LaunchedFragmentInfo> CREATOR;
        int mRequestCode;
        String mWho;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$Creator<LaunchedFragmentInfo>() {
                public LaunchedFragmentInfo createFromParcel(final Parcel parcel) {
                    return new LaunchedFragmentInfo(parcel);
                }
                
                public LaunchedFragmentInfo[] newArray(final int n) {
                    return new LaunchedFragmentInfo[n];
                }
            };
        }
        
        LaunchedFragmentInfo(@NonNull final Parcel parcel) {
            this.mWho = parcel.readString();
            this.mRequestCode = parcel.readInt();
        }
        
        LaunchedFragmentInfo(@NonNull final String mWho, final int mRequestCode) {
            this.mWho = mWho;
            this.mRequestCode = mRequestCode;
        }
        
        public int describeContents() {
            return 0;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            parcel.writeString(this.mWho);
            parcel.writeInt(this.mRequestCode);
        }
    }
    
    private static class LifecycleAwareResultListener implements FragmentResultListener
    {
        private final Lifecycle mLifecycle;
        private final FragmentResultListener mListener;
        private final LifecycleEventObserver mObserver;
        
        LifecycleAwareResultListener(@NonNull final Lifecycle mLifecycle, @NonNull final FragmentResultListener mListener, @NonNull final LifecycleEventObserver mObserver) {
            this.mLifecycle = mLifecycle;
            this.mListener = mListener;
            this.mObserver = mObserver;
        }
        
        public boolean isAtLeast(final Lifecycle.State state) {
            return this.mLifecycle.getCurrentState().isAtLeast(state);
        }
        
        @Override
        public void onFragmentResult(@NonNull final String s, @NonNull final Bundle bundle) {
            this.mListener.onFragmentResult(s, bundle);
        }
        
        public void removeObserver() {
            this.mLifecycle.removeObserver(this.mObserver);
        }
    }
    
    public interface OnBackStackChangedListener
    {
        @MainThread
        void onBackStackChanged();
    }
    
    private class PopBackStackState implements OpGenerator
    {
        final int mFlags;
        final int mId;
        final String mName;
        final FragmentManager this$0;
        
        PopBackStackState(@Nullable final FragmentManager this$0, final String mName, final int mId, final int mFlags) {
            this.this$0 = this$0;
            this.mName = mName;
            this.mId = mId;
            this.mFlags = mFlags;
        }
        
        @Override
        public boolean generateOps(@NonNull final ArrayList<BackStackRecord> list, @NonNull final ArrayList<Boolean> list2) {
            final Fragment mPrimaryNav = this.this$0.mPrimaryNav;
            return (mPrimaryNav == null || this.mId >= 0 || this.mName != null || !mPrimaryNav.getChildFragmentManager().popBackStackImmediate()) && this.this$0.popBackStackState(list, list2, this.mName, this.mId, this.mFlags);
        }
    }
    
    private class RestoreBackStackState implements OpGenerator
    {
        private final String mName;
        final FragmentManager this$0;
        
        RestoreBackStackState(@NonNull final FragmentManager this$0, final String mName) {
            this.this$0 = this$0;
            this.mName = mName;
        }
        
        @Override
        public boolean generateOps(@NonNull final ArrayList<BackStackRecord> list, @NonNull final ArrayList<Boolean> list2) {
            return this.this$0.restoreBackStackState(list, list2, this.mName);
        }
    }
    
    private class SaveBackStackState implements OpGenerator
    {
        private final String mName;
        final FragmentManager this$0;
        
        SaveBackStackState(@NonNull final FragmentManager this$0, final String mName) {
            this.this$0 = this$0;
            this.mName = mName;
        }
        
        @Override
        public boolean generateOps(@NonNull final ArrayList<BackStackRecord> list, @NonNull final ArrayList<Boolean> list2) {
            return this.this$0.saveBackStackState(list, list2, this.mName);
        }
    }
}

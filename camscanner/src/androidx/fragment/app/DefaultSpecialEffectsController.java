// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import androidx.core.view.ViewGroupCompat;
import androidx.core.app.SharedElementCallback;
import androidx.core.view.OneShotPreDrawListener;
import androidx.core.view.ViewCompat;
import java.util.Collection;
import androidx.collection.ArrayMap;
import android.graphics.Rect;
import java.util.HashMap;
import androidx.annotation.Nullable;
import java.util.Iterator;
import android.content.Context;
import android.view.animation.Animation$AnimationListener;
import androidx.core.util.Preconditions;
import android.view.animation.Animation;
import androidx.core.os.CancellationSignal;
import android.animation.Animator$AnimatorListener;
import android.animation.Animator;
import android.view.View;
import android.animation.AnimatorListenerAdapter;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

class DefaultSpecialEffectsController extends SpecialEffectsController
{
    DefaultSpecialEffectsController(@NonNull final ViewGroup viewGroup) {
        super(viewGroup);
    }
    
    private void startAnimations(@NonNull final List<AnimationInfo> list, @NonNull final List<Operation> list2, final boolean b, @NonNull final Map<Operation, Boolean> map) {
        final ViewGroup container = this.getContainer();
        final Context context = ((View)container).getContext();
        final ArrayList list3 = new ArrayList();
        final Iterator<AnimationInfo> iterator = list.iterator();
        boolean b2 = false;
        while (iterator.hasNext()) {
            final AnimationInfo e = iterator.next();
            if (((SpecialEffectsInfo)e).isVisibilityUnchanged()) {
                ((SpecialEffectsInfo)e).completeSpecialEffect();
            }
            else {
                final FragmentAnim.AnimationOrAnimator animation = e.getAnimation(context);
                if (animation == null) {
                    ((SpecialEffectsInfo)e).completeSpecialEffect();
                }
                else {
                    final Animator animator = animation.animator;
                    if (animator == null) {
                        list3.add(e);
                    }
                    else {
                        final Operation operation = ((SpecialEffectsInfo)e).getOperation();
                        final Fragment fragment = operation.getFragment();
                        if (Boolean.TRUE.equals(map.get(operation))) {
                            if (FragmentManager.isLoggingEnabled(2)) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Ignoring Animator set on ");
                                sb.append(fragment);
                                sb.append(" as this Fragment was involved in a Transition.");
                            }
                            ((SpecialEffectsInfo)e).completeSpecialEffect();
                        }
                        else {
                            final boolean b3 = operation.getFinalState() == State.GONE;
                            if (b3) {
                                list2.remove(operation);
                            }
                            final View mView = fragment.mView;
                            container.startViewTransition(mView);
                            animator.addListener((Animator$AnimatorListener)new AnimatorListenerAdapter(this, container, mView, b3, operation, e) {
                                final DefaultSpecialEffectsController this$0;
                                final AnimationInfo val$animationInfo;
                                final ViewGroup val$container;
                                final boolean val$isHideOperation;
                                final Operation val$operation;
                                final View val$viewToAnimate;
                                
                                public void onAnimationEnd(final Animator animator) {
                                    this.val$container.endViewTransition(this.val$viewToAnimate);
                                    if (this.val$isHideOperation) {
                                        this.val$operation.getFinalState().applyState(this.val$viewToAnimate);
                                    }
                                    ((SpecialEffectsInfo)this.val$animationInfo).completeSpecialEffect();
                                    if (FragmentManager.isLoggingEnabled(2)) {
                                        final StringBuilder sb = new StringBuilder();
                                        sb.append("Animator from operation ");
                                        sb.append(this.val$operation);
                                        sb.append(" has ended.");
                                    }
                                }
                            });
                            animator.setTarget((Object)mView);
                            animator.start();
                            if (FragmentManager.isLoggingEnabled(2)) {
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("Animator from operation ");
                                sb2.append(operation);
                                sb2.append(" has started.");
                            }
                            ((SpecialEffectsInfo)e).getSignal().setOnCancelListener((CancellationSignal.OnCancelListener)new CancellationSignal.OnCancelListener(this, animator, operation) {
                                final DefaultSpecialEffectsController this$0;
                                final Animator val$animator;
                                final Operation val$operation;
                                
                                @Override
                                public void onCancel() {
                                    this.val$animator.end();
                                    if (FragmentManager.isLoggingEnabled(2)) {
                                        final StringBuilder sb = new StringBuilder();
                                        sb.append("Animator from operation ");
                                        sb.append(this.val$operation);
                                        sb.append(" has been canceled.");
                                    }
                                }
                            });
                            b2 = true;
                        }
                    }
                }
            }
        }
        for (final AnimationInfo animationInfo : list3) {
            final Operation operation2 = ((SpecialEffectsInfo)animationInfo).getOperation();
            final Fragment fragment2 = operation2.getFragment();
            if (b) {
                if (FragmentManager.isLoggingEnabled(2)) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Ignoring Animation set on ");
                    sb3.append(fragment2);
                    sb3.append(" as Animations cannot run alongside Transitions.");
                }
                ((SpecialEffectsInfo)animationInfo).completeSpecialEffect();
            }
            else if (b2) {
                if (FragmentManager.isLoggingEnabled(2)) {
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("Ignoring Animation set on ");
                    sb4.append(fragment2);
                    sb4.append(" as Animations cannot run alongside Animators.");
                }
                ((SpecialEffectsInfo)animationInfo).completeSpecialEffect();
            }
            else {
                final View mView2 = fragment2.mView;
                final Animation animation2 = Preconditions.checkNotNull(Preconditions.checkNotNull(animationInfo.getAnimation(context)).animation);
                if (operation2.getFinalState() != State.REMOVED) {
                    mView2.startAnimation(animation2);
                    ((SpecialEffectsInfo)animationInfo).completeSpecialEffect();
                }
                else {
                    container.startViewTransition(mView2);
                    final FragmentAnim.EndViewTransitionAnimation endViewTransitionAnimation = new FragmentAnim.EndViewTransitionAnimation(animation2, container, mView2);
                    ((Animation)endViewTransitionAnimation).setAnimationListener((Animation$AnimationListener)new Animation$AnimationListener(this, operation2, container, mView2, animationInfo) {
                        final DefaultSpecialEffectsController this$0;
                        final AnimationInfo val$animationInfo;
                        final ViewGroup val$container;
                        final Operation val$operation;
                        final View val$viewToAnimate;
                        
                        public void onAnimationEnd(final Animation animation) {
                            ((View)this.val$container).post((Runnable)new Runnable(this) {
                                final DefaultSpecialEffectsController$4 this$1;
                                
                                @Override
                                public void run() {
                                    final Animation$AnimationListener this$1 = (Animation$AnimationListener)this.this$1;
                                    this$1.val$container.endViewTransition(this$1.val$viewToAnimate);
                                    ((SpecialEffectsInfo)this.this$1.val$animationInfo).completeSpecialEffect();
                                }
                            });
                            if (FragmentManager.isLoggingEnabled(2)) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Animation from operation ");
                                sb.append(this.val$operation);
                                sb.append(" has ended.");
                            }
                        }
                        
                        public void onAnimationRepeat(final Animation animation) {
                        }
                        
                        public void onAnimationStart(final Animation animation) {
                            if (FragmentManager.isLoggingEnabled(2)) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Animation from operation ");
                                sb.append(this.val$operation);
                                sb.append(" has reached onAnimationStart.");
                            }
                        }
                    });
                    mView2.startAnimation((Animation)endViewTransitionAnimation);
                    if (FragmentManager.isLoggingEnabled(2)) {
                        final StringBuilder sb5 = new StringBuilder();
                        sb5.append("Animation from operation ");
                        sb5.append(operation2);
                        sb5.append(" has started.");
                    }
                }
                ((SpecialEffectsInfo)animationInfo).getSignal().setOnCancelListener((CancellationSignal.OnCancelListener)new CancellationSignal.OnCancelListener(this, mView2, container, animationInfo, operation2) {
                    final DefaultSpecialEffectsController this$0;
                    final AnimationInfo val$animationInfo;
                    final ViewGroup val$container;
                    final Operation val$operation;
                    final View val$viewToAnimate;
                    
                    @Override
                    public void onCancel() {
                        this.val$viewToAnimate.clearAnimation();
                        this.val$container.endViewTransition(this.val$viewToAnimate);
                        ((SpecialEffectsInfo)this.val$animationInfo).completeSpecialEffect();
                        if (FragmentManager.isLoggingEnabled(2)) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Animation from operation ");
                            sb.append(this.val$operation);
                            sb.append(" has been cancelled.");
                        }
                    }
                });
            }
        }
    }
    
    @NonNull
    private Map<Operation, Boolean> startTransitions(@NonNull final List<TransitionInfo> list, @NonNull final List<Operation> list2, final boolean b, @Nullable final Operation operation, @Nullable final Operation operation2) {
        DefaultSpecialEffectsController defaultSpecialEffectsController = this;
        Operation obj = operation;
        Operation obj2 = operation2;
        HashMap hashMap = new HashMap();
        final Iterator<TransitionInfo> iterator = list.iterator();
        FragmentTransitionImpl fragmentTransitionImpl = null;
        while (iterator.hasNext()) {
            final TransitionInfo transitionInfo = iterator.next();
            if (((SpecialEffectsInfo)transitionInfo).isVisibilityUnchanged()) {
                continue;
            }
            final FragmentTransitionImpl handlingImpl = transitionInfo.getHandlingImpl();
            if (fragmentTransitionImpl == null) {
                fragmentTransitionImpl = handlingImpl;
            }
            else {
                if (handlingImpl == null) {
                    continue;
                }
                if (fragmentTransitionImpl == handlingImpl) {
                    continue;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Mixing framework transitions and AndroidX transitions is not allowed. Fragment ");
                sb.append(((SpecialEffectsInfo)transitionInfo).getOperation().getFragment());
                sb.append(" returned Transition ");
                sb.append(transitionInfo.getTransition());
                sb.append(" which uses a different Transition  type than other Fragments.");
                throw new IllegalArgumentException(sb.toString());
            }
        }
        if (fragmentTransitionImpl == null) {
            for (final TransitionInfo transitionInfo2 : list) {
                hashMap.put(((SpecialEffectsInfo)transitionInfo2).getOperation(), Boolean.FALSE);
                ((SpecialEffectsInfo)transitionInfo2).completeSpecialEffect();
            }
            return hashMap;
        }
        final View view = new View(((View)this.getContainer()).getContext());
        final Rect rect = new Rect();
        ArrayList list3 = new ArrayList();
        final ArrayList list4 = new ArrayList();
        final ArrayMap arrayMap = new ArrayMap();
        final Iterator<TransitionInfo> iterator3 = list.iterator();
        Object o = null;
        final View view2 = null;
        boolean b2 = false;
        final FragmentTransitionImpl fragmentTransitionImpl2 = fragmentTransitionImpl;
        View view3 = view2;
        while (iterator3.hasNext()) {
            final TransitionInfo transitionInfo3 = iterator3.next();
            Object o2;
            Operation operation5;
            DefaultSpecialEffectsController defaultSpecialEffectsController3;
            HashMap hashMap3;
            Operation operation6;
            if (transitionInfo3.hasSharedElementTransition() && obj != null && obj2 != null) {
                final Object wrapTransitionInSet = fragmentTransitionImpl2.wrapTransitionInSet(fragmentTransitionImpl2.cloneTransition(transitionInfo3.getSharedElementTransition()));
                final ArrayList<String> sharedElementSourceNames = operation2.getFragment().getSharedElementSourceNames();
                final ArrayList<String> sharedElementSourceNames2 = operation.getFragment().getSharedElementSourceNames();
                final ArrayList<String> sharedElementTargetNames = operation.getFragment().getSharedElementTargetNames();
                for (int i = 0; i < sharedElementTargetNames.size(); ++i) {
                    final int index = sharedElementSourceNames.indexOf(sharedElementTargetNames.get(i));
                    if (index != -1) {
                        sharedElementSourceNames.set(index, sharedElementSourceNames2.get(i));
                    }
                }
                final ArrayList<String> sharedElementTargetNames2 = operation2.getFragment().getSharedElementTargetNames();
                SharedElementCallback sharedElementCallback;
                SharedElementCallback sharedElementCallback2;
                if (!b) {
                    sharedElementCallback = operation.getFragment().getExitTransitionCallback();
                    sharedElementCallback2 = operation2.getFragment().getEnterTransitionCallback();
                }
                else {
                    sharedElementCallback = operation.getFragment().getEnterTransitionCallback();
                    sharedElementCallback2 = operation2.getFragment().getExitTransitionCallback();
                }
                for (int size = sharedElementSourceNames.size(), j = 0; j < size; ++j) {
                    arrayMap.put(sharedElementSourceNames.get(j), sharedElementTargetNames2.get(j));
                }
                if (FragmentManager.isLoggingEnabled(2)) {
                    for (final String str : sharedElementTargetNames2) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Name: ");
                        sb2.append(str);
                    }
                    for (final String str2 : sharedElementSourceNames) {
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("Name: ");
                        sb3.append(str2);
                    }
                }
                final ArrayMap<Object, View> arrayMap2 = new ArrayMap<Object, View>();
                defaultSpecialEffectsController.findNamedViews((Map<String, View>)arrayMap2, operation.getFragment().mView);
                arrayMap2.retainAll(sharedElementSourceNames);
                if (sharedElementCallback != null) {
                    if (FragmentManager.isLoggingEnabled(2)) {
                        final StringBuilder sb4 = new StringBuilder();
                        sb4.append("Executing exit callback for operation ");
                        sb4.append(obj);
                    }
                    sharedElementCallback.onMapSharedElements(sharedElementSourceNames, (Map<String, View>)arrayMap2);
                    for (int k = sharedElementSourceNames.size() - 1; k >= 0; --k) {
                        final String s = sharedElementSourceNames.get(k);
                        final View view4 = arrayMap2.get(s);
                        if (view4 == null) {
                            arrayMap.remove(s);
                        }
                        else if (!s.equals(ViewCompat.getTransitionName(view4))) {
                            arrayMap.put(ViewCompat.getTransitionName(view4), arrayMap.remove(s));
                        }
                    }
                }
                else {
                    arrayMap.retainAll(arrayMap2.keySet());
                }
                final ArrayMap arrayMap3 = new ArrayMap();
                defaultSpecialEffectsController.findNamedViews(arrayMap3, operation2.getFragment().mView);
                arrayMap3.retainAll(sharedElementTargetNames2);
                arrayMap3.retainAll(arrayMap.values());
                if (sharedElementCallback2 != null) {
                    if (FragmentManager.isLoggingEnabled(2)) {
                        final StringBuilder sb5 = new StringBuilder();
                        sb5.append("Executing enter callback for operation ");
                        sb5.append(obj2);
                    }
                    sharedElementCallback2.onMapSharedElements(sharedElementTargetNames2, arrayMap3);
                    for (int l = sharedElementTargetNames2.size() - 1; l >= 0; --l) {
                        final String s2 = sharedElementTargetNames2.get(l);
                        final View view5 = (View)arrayMap3.get(s2);
                        if (view5 == null) {
                            final String keyForValue = FragmentTransition.findKeyForValue(arrayMap, s2);
                            if (keyForValue != null) {
                                arrayMap.remove(keyForValue);
                            }
                        }
                        else if (!s2.equals(ViewCompat.getTransitionName(view5))) {
                            final String keyForValue2 = FragmentTransition.findKeyForValue(arrayMap, s2);
                            if (keyForValue2 != null) {
                                arrayMap.put(keyForValue2, ViewCompat.getTransitionName(view5));
                            }
                        }
                    }
                }
                else {
                    FragmentTransition.retainValues(arrayMap, arrayMap3);
                }
                defaultSpecialEffectsController.retainMatchingViews((ArrayMap<String, View>)arrayMap2, arrayMap.keySet());
                defaultSpecialEffectsController.retainMatchingViews(arrayMap3, arrayMap.values());
                if (arrayMap.isEmpty()) {
                    list3.clear();
                    list4.clear();
                    final DefaultSpecialEffectsController defaultSpecialEffectsController2 = defaultSpecialEffectsController;
                    final Operation operation3 = obj;
                    final HashMap hashMap2 = hashMap;
                    o2 = null;
                    final Operation operation4 = obj2;
                    operation5 = operation3;
                    defaultSpecialEffectsController3 = defaultSpecialEffectsController2;
                    hashMap3 = hashMap2;
                    operation6 = operation4;
                }
                else {
                    FragmentTransition.callSharedElementStartEnd(operation2.getFragment(), operation.getFragment(), b, (ArrayMap<String, View>)arrayMap2, true);
                    OneShotPreDrawListener.add((View)this.getContainer(), new Runnable(this, operation2, operation, b, arrayMap3) {
                        final DefaultSpecialEffectsController this$0;
                        final Operation val$firstOut;
                        final boolean val$isPop;
                        final Operation val$lastIn;
                        final ArrayMap val$lastInViews;
                        
                        @Override
                        public void run() {
                            FragmentTransition.callSharedElementStartEnd(this.val$lastIn.getFragment(), this.val$firstOut.getFragment(), this.val$isPop, this.val$lastInViews, false);
                        }
                    });
                    list3.addAll(arrayMap2.values());
                    if (!sharedElementSourceNames.isEmpty()) {
                        view3 = (View)arrayMap2.get(sharedElementSourceNames.get(0));
                        fragmentTransitionImpl2.setEpicenter(wrapTransitionInSet, view3);
                    }
                    list4.addAll(arrayMap3.values());
                    if (!sharedElementTargetNames2.isEmpty()) {
                        final View view6 = (View)arrayMap3.get(sharedElementTargetNames2.get(0));
                        if (view6 != null) {
                            OneShotPreDrawListener.add((View)this.getContainer(), new Runnable(this, fragmentTransitionImpl2, view6, rect) {
                                final DefaultSpecialEffectsController this$0;
                                final FragmentTransitionImpl val$impl;
                                final Rect val$lastInEpicenterRect;
                                final View val$lastInEpicenterView;
                                
                                @Override
                                public void run() {
                                    this.val$impl.getBoundsOnScreen(this.val$lastInEpicenterView, this.val$lastInEpicenterRect);
                                }
                            });
                            b2 = true;
                        }
                    }
                    fragmentTransitionImpl2.setSharedElementTargets(wrapTransitionInSet, view, list3);
                    fragmentTransitionImpl2.scheduleRemoveTargets(wrapTransitionInSet, null, null, null, null, wrapTransitionInSet, list4);
                    final Boolean true = Boolean.TRUE;
                    operation5 = operation;
                    final HashMap hashMap4 = hashMap;
                    hashMap4.put(operation5, true);
                    operation6 = operation2;
                    hashMap4.put(operation6, true);
                    o2 = wrapTransitionInSet;
                    defaultSpecialEffectsController3 = this;
                    hashMap3 = hashMap4;
                }
            }
            else {
                final DefaultSpecialEffectsController defaultSpecialEffectsController4 = defaultSpecialEffectsController;
                final Operation operation7 = obj;
                operation6 = obj2;
                hashMap3 = hashMap;
                defaultSpecialEffectsController3 = defaultSpecialEffectsController4;
                operation5 = operation7;
                o2 = o;
            }
            final ArrayList list5 = list3;
            final Operation operation8 = operation6;
            o = o2;
            defaultSpecialEffectsController = defaultSpecialEffectsController3;
            obj = operation5;
            obj2 = operation8;
            hashMap = hashMap3;
            list3 = list5;
        }
        final View view7 = view3;
        final ArrayList c = list4;
        final ArrayList list6 = list3;
        final View view8 = view;
        final ArrayList list7 = new ArrayList();
        final Iterator<TransitionInfo> iterator6 = list.iterator();
        final Object o3 = null;
        Object mergeTransitionsTogether = null;
        final View view9 = view7;
        final Operation operation9 = obj2;
        Object mergeTransitionsTogether2 = o3;
        final ArrayList c2 = list6;
        while (iterator6.hasNext()) {
            final TransitionInfo transitionInfo4 = iterator6.next();
            if (((SpecialEffectsInfo)transitionInfo4).isVisibilityUnchanged()) {
                hashMap.put(((SpecialEffectsInfo)transitionInfo4).getOperation(), Boolean.FALSE);
                ((SpecialEffectsInfo)transitionInfo4).completeSpecialEffect();
            }
            else {
                final Object cloneTransition = fragmentTransitionImpl2.cloneTransition(transitionInfo4.getTransition());
                final Operation operation10 = ((SpecialEffectsInfo)transitionInfo4).getOperation();
                final boolean b3 = o != null && (operation10 == obj || operation10 == operation9);
                if (cloneTransition == null) {
                    if (b3) {
                        continue;
                    }
                    hashMap.put(operation10, Boolean.FALSE);
                    ((SpecialEffectsInfo)transitionInfo4).completeSpecialEffect();
                }
                else {
                    final ArrayList list8 = new ArrayList();
                    defaultSpecialEffectsController.captureTransitioningViews(list8, operation10.getFragment().mView);
                    if (b3) {
                        if (operation10 == obj) {
                            list8.removeAll(c2);
                        }
                        else {
                            list8.removeAll(c);
                        }
                    }
                    if (list8.isEmpty()) {
                        fragmentTransitionImpl2.addTarget(cloneTransition, view8);
                    }
                    else {
                        fragmentTransitionImpl2.addTargets(cloneTransition, list8);
                        fragmentTransitionImpl2.scheduleRemoveTargets(cloneTransition, cloneTransition, list8, null, null, null, null);
                        if (operation10.getFinalState() == State.GONE) {
                            list2.remove(operation10);
                            final ArrayList list9 = new ArrayList(list8);
                            list9.remove(operation10.getFragment().mView);
                            fragmentTransitionImpl2.scheduleHideFragmentView(cloneTransition, operation10.getFragment().mView, (ArrayList<View>)list9);
                            OneShotPreDrawListener.add((View)this.getContainer(), new Runnable(defaultSpecialEffectsController, list8) {
                                final DefaultSpecialEffectsController this$0;
                                final ArrayList val$transitioningViews;
                                
                                @Override
                                public void run() {
                                    FragmentTransition.setViewVisibility(this.val$transitioningViews, 4);
                                }
                            });
                        }
                    }
                    if (operation10.getFinalState() == State.VISIBLE) {
                        list7.addAll(list8);
                        if (b2) {
                            fragmentTransitionImpl2.setEpicenter(cloneTransition, rect);
                        }
                    }
                    else {
                        fragmentTransitionImpl2.setEpicenter(cloneTransition, view9);
                    }
                    hashMap.put(operation10, Boolean.TRUE);
                    if (transitionInfo4.isOverlapAllowed()) {
                        mergeTransitionsTogether = fragmentTransitionImpl2.mergeTransitionsTogether(mergeTransitionsTogether, cloneTransition, null);
                    }
                    else {
                        mergeTransitionsTogether2 = fragmentTransitionImpl2.mergeTransitionsTogether(mergeTransitionsTogether2, cloneTransition, null);
                    }
                }
            }
        }
        final Object mergeTransitionsInSequence = fragmentTransitionImpl2.mergeTransitionsInSequence(mergeTransitionsTogether, mergeTransitionsTogether2, o);
        if (mergeTransitionsInSequence == null) {
            return hashMap;
        }
        for (final TransitionInfo transitionInfo5 : list) {
            if (((SpecialEffectsInfo)transitionInfo5).isVisibilityUnchanged()) {
                continue;
            }
            final Object transition = transitionInfo5.getTransition();
            final Operation operation11 = ((SpecialEffectsInfo)transitionInfo5).getOperation();
            final boolean b4 = o != null && (operation11 == obj || operation11 == operation9);
            if (transition == null && !b4) {
                continue;
            }
            if (!ViewCompat.isLaidOut((View)this.getContainer())) {
                if (FragmentManager.isLoggingEnabled(2)) {
                    final StringBuilder sb6 = new StringBuilder();
                    sb6.append("SpecialEffectsController: Container ");
                    sb6.append(this.getContainer());
                    sb6.append(" has not been laid out. Completing operation ");
                    sb6.append(operation11);
                }
                ((SpecialEffectsInfo)transitionInfo5).completeSpecialEffect();
            }
            else {
                fragmentTransitionImpl2.setListenerForTransitionEnd(((SpecialEffectsInfo)transitionInfo5).getOperation().getFragment(), mergeTransitionsInSequence, ((SpecialEffectsInfo)transitionInfo5).getSignal(), new Runnable(defaultSpecialEffectsController, transitionInfo5, operation11) {
                    final DefaultSpecialEffectsController this$0;
                    final Operation val$operation;
                    final TransitionInfo val$transitionInfo;
                    
                    @Override
                    public void run() {
                        ((SpecialEffectsInfo)this.val$transitionInfo).completeSpecialEffect();
                        if (FragmentManager.isLoggingEnabled(2)) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Transition for operation ");
                            sb.append(this.val$operation);
                            sb.append("has completed");
                        }
                    }
                });
            }
        }
        if (!ViewCompat.isLaidOut((View)this.getContainer())) {
            return hashMap;
        }
        FragmentTransition.setViewVisibility(list7, 4);
        final ArrayList<String> prepareSetNameOverridesReordered = fragmentTransitionImpl2.prepareSetNameOverridesReordered(c);
        if (FragmentManager.isLoggingEnabled(2)) {
            for (final View obj3 : c2) {
                final StringBuilder sb7 = new StringBuilder();
                sb7.append("View: ");
                sb7.append(obj3);
                sb7.append(" Name: ");
                sb7.append(ViewCompat.getTransitionName(obj3));
            }
            for (final View obj4 : c) {
                final StringBuilder sb8 = new StringBuilder();
                sb8.append("View: ");
                sb8.append(obj4);
                sb8.append(" Name: ");
                sb8.append(ViewCompat.getTransitionName(obj4));
            }
        }
        fragmentTransitionImpl2.beginDelayedTransition(this.getContainer(), mergeTransitionsInSequence);
        fragmentTransitionImpl2.setNameOverridesReordered((View)this.getContainer(), c2, c, prepareSetNameOverridesReordered, arrayMap);
        FragmentTransition.setViewVisibility(list7, 0);
        fragmentTransitionImpl2.swapSharedElementTargets(o, c2, c);
        return hashMap;
    }
    
    private void syncAnimations(@NonNull final List<Operation> list) {
        final Fragment fragment = list.get(list.size() - 1).getFragment();
        for (final Operation operation : list) {
            operation.getFragment().mAnimationInfo.mEnterAnim = fragment.mAnimationInfo.mEnterAnim;
            operation.getFragment().mAnimationInfo.mExitAnim = fragment.mAnimationInfo.mExitAnim;
            operation.getFragment().mAnimationInfo.mPopEnterAnim = fragment.mAnimationInfo.mPopEnterAnim;
            operation.getFragment().mAnimationInfo.mPopExitAnim = fragment.mAnimationInfo.mPopExitAnim;
        }
    }
    
    void applyContainerChanges(@NonNull final Operation operation) {
        operation.getFinalState().applyState(operation.getFragment().mView);
    }
    
    void captureTransitioningViews(final ArrayList<View> list, View child) {
        if (child instanceof ViewGroup) {
            final ViewGroup e = (ViewGroup)child;
            if (ViewGroupCompat.isTransitionGroup(e)) {
                if (!list.contains(child)) {
                    list.add((View)e);
                }
            }
            else {
                for (int childCount = e.getChildCount(), i = 0; i < childCount; ++i) {
                    child = e.getChildAt(i);
                    if (child.getVisibility() == 0) {
                        this.captureTransitioningViews(list, child);
                    }
                }
            }
        }
        else if (!list.contains(child)) {
            list.add(child);
        }
    }
    
    @Override
    void executeOperations(@NonNull final List<Operation> c, final boolean b) {
        final Iterator<Operation> iterator = c.iterator();
        Operation operation = null;
        Operation operation2 = null;
        while (iterator.hasNext()) {
            final Operation operation3 = iterator.next();
            final Operation.State from = Operation.State.from(operation3.getFragment().mView);
            final int n = DefaultSpecialEffectsController$10.$SwitchMap$androidx$fragment$app$SpecialEffectsController$Operation$State[operation3.getFinalState().ordinal()];
            if (n != 1 && n != 2 && n != 3) {
                if (n != 4) {
                    continue;
                }
                if (from == State.VISIBLE) {
                    continue;
                }
                operation2 = operation3;
            }
            else {
                if (from != State.VISIBLE || operation != null) {
                    continue;
                }
                operation = operation3;
            }
        }
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Executing operations from ");
            sb.append(operation);
            sb.append(" to ");
            sb.append(operation2);
        }
        final ArrayList<AnimationInfo> list = new ArrayList<AnimationInfo>();
        final ArrayList<TransitionInfo> list2 = new ArrayList<TransitionInfo>();
        final ArrayList<Operation> list3 = new ArrayList<Operation>(c);
        this.syncAnimations(c);
        for (final Operation operation4 : c) {
            final CancellationSignal cancellationSignal = new CancellationSignal();
            operation4.markStartedSpecialEffect(cancellationSignal);
            list.add(new AnimationInfo(operation4, cancellationSignal, b));
            final CancellationSignal cancellationSignal2 = new CancellationSignal();
            operation4.markStartedSpecialEffect(cancellationSignal2);
            boolean b2 = false;
            Label_0309: {
                if (b) {
                    if (operation4 != operation) {
                        break Label_0309;
                    }
                }
                else if (operation4 != operation2) {
                    break Label_0309;
                }
                b2 = true;
            }
            list2.add(new TransitionInfo(operation4, cancellationSignal2, b, b2));
            operation4.addCompletionListener(new Runnable(this, list3, operation4) {
                final DefaultSpecialEffectsController this$0;
                final List val$awaitingContainerChanges;
                final Operation val$operation;
                
                @Override
                public void run() {
                    if (this.val$awaitingContainerChanges.contains(this.val$operation)) {
                        this.val$awaitingContainerChanges.remove(this.val$operation);
                        this.this$0.applyContainerChanges(this.val$operation);
                    }
                }
            });
        }
        final Map<Operation, Boolean> startTransitions = this.startTransitions(list2, list3, b, operation, operation2);
        this.startAnimations(list, list3, startTransitions.containsValue(Boolean.TRUE), startTransitions);
        final Iterator<Object> iterator3 = list3.iterator();
        while (iterator3.hasNext()) {
            this.applyContainerChanges(iterator3.next());
        }
        list3.clear();
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Completed executing operations from ");
            sb2.append(operation);
            sb2.append(" to ");
            sb2.append(operation2);
        }
    }
    
    void findNamedViews(final Map<String, View> map, @NonNull View child) {
        final String transitionName = ViewCompat.getTransitionName(child);
        if (transitionName != null) {
            map.put(transitionName, child);
        }
        if (child instanceof ViewGroup) {
            final ViewGroup viewGroup = (ViewGroup)child;
            for (int childCount = viewGroup.getChildCount(), i = 0; i < childCount; ++i) {
                child = viewGroup.getChildAt(i);
                if (child.getVisibility() == 0) {
                    this.findNamedViews(map, child);
                }
            }
        }
    }
    
    void retainMatchingViews(@NonNull final ArrayMap<String, View> arrayMap, @NonNull final Collection<String> collection) {
        final Iterator<Map.Entry<String, View>> iterator = arrayMap.entrySet().iterator();
        while (iterator.hasNext()) {
            if (!collection.contains(ViewCompat.getTransitionName(((Map.Entry<K, View>)iterator.next()).getValue()))) {
                iterator.remove();
            }
        }
    }
    
    private static class AnimationInfo extends SpecialEffectsInfo
    {
        @Nullable
        private FragmentAnim.AnimationOrAnimator mAnimation;
        private boolean mIsPop;
        private boolean mLoadedAnim;
        
        AnimationInfo(@NonNull final Operation operation, @NonNull final CancellationSignal cancellationSignal, final boolean mIsPop) {
            super(operation, cancellationSignal);
            this.mLoadedAnim = false;
            this.mIsPop = mIsPop;
        }
        
        @Nullable
        FragmentAnim.AnimationOrAnimator getAnimation(@NonNull final Context context) {
            if (this.mLoadedAnim) {
                return this.mAnimation;
            }
            final FragmentAnim.AnimationOrAnimator loadAnimation = FragmentAnim.loadAnimation(context, ((SpecialEffectsInfo)this).getOperation().getFragment(), ((SpecialEffectsInfo)this).getOperation().getFinalState() == State.VISIBLE, this.mIsPop);
            this.mAnimation = loadAnimation;
            this.mLoadedAnim = true;
            return loadAnimation;
        }
    }
    
    private static class SpecialEffectsInfo
    {
        @NonNull
        private final Operation mOperation;
        @NonNull
        private final CancellationSignal mSignal;
        
        SpecialEffectsInfo(@NonNull final Operation mOperation, @NonNull final CancellationSignal mSignal) {
            this.mOperation = mOperation;
            this.mSignal = mSignal;
        }
        
        void completeSpecialEffect() {
            this.mOperation.completeSpecialEffect(this.mSignal);
        }
        
        @NonNull
        Operation getOperation() {
            return this.mOperation;
        }
        
        @NonNull
        CancellationSignal getSignal() {
            return this.mSignal;
        }
        
        boolean isVisibilityUnchanged() {
            final Operation.State from = Operation.State.from(this.mOperation.getFragment().mView);
            final Operation.State finalState = this.mOperation.getFinalState();
            if (from != finalState) {
                final Operation.State visible = State.VISIBLE;
                if (from == visible || finalState == visible) {
                    return false;
                }
            }
            return true;
        }
    }
    
    private static class TransitionInfo extends SpecialEffectsInfo
    {
        private final boolean mOverlapAllowed;
        @Nullable
        private final Object mSharedElementTransition;
        @Nullable
        private final Object mTransition;
        
        TransitionInfo(@NonNull final Operation operation, @NonNull final CancellationSignal cancellationSignal, final boolean b, final boolean b2) {
            super(operation, cancellationSignal);
            if (operation.getFinalState() == State.VISIBLE) {
                Object mTransition;
                if (b) {
                    mTransition = operation.getFragment().getReenterTransition();
                }
                else {
                    mTransition = operation.getFragment().getEnterTransition();
                }
                this.mTransition = mTransition;
                boolean mOverlapAllowed;
                if (b) {
                    mOverlapAllowed = operation.getFragment().getAllowReturnTransitionOverlap();
                }
                else {
                    mOverlapAllowed = operation.getFragment().getAllowEnterTransitionOverlap();
                }
                this.mOverlapAllowed = mOverlapAllowed;
            }
            else {
                Object mTransition2;
                if (b) {
                    mTransition2 = operation.getFragment().getReturnTransition();
                }
                else {
                    mTransition2 = operation.getFragment().getExitTransition();
                }
                this.mTransition = mTransition2;
                this.mOverlapAllowed = true;
            }
            if (b2) {
                if (b) {
                    this.mSharedElementTransition = operation.getFragment().getSharedElementReturnTransition();
                }
                else {
                    this.mSharedElementTransition = operation.getFragment().getSharedElementEnterTransition();
                }
            }
            else {
                this.mSharedElementTransition = null;
            }
        }
        
        @Nullable
        private FragmentTransitionImpl getHandlingImpl(final Object obj) {
            if (obj == null) {
                return null;
            }
            final FragmentTransitionImpl platform_IMPL = FragmentTransition.PLATFORM_IMPL;
            if (platform_IMPL != null && platform_IMPL.canHandle(obj)) {
                return platform_IMPL;
            }
            final FragmentTransitionImpl support_IMPL = FragmentTransition.SUPPORT_IMPL;
            if (support_IMPL != null && support_IMPL.canHandle(obj)) {
                return support_IMPL;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Transition ");
            sb.append(obj);
            sb.append(" for fragment ");
            sb.append(((SpecialEffectsInfo)this).getOperation().getFragment());
            sb.append(" is not a valid framework Transition or AndroidX Transition");
            throw new IllegalArgumentException(sb.toString());
        }
        
        @Nullable
        FragmentTransitionImpl getHandlingImpl() {
            final FragmentTransitionImpl handlingImpl = this.getHandlingImpl(this.mTransition);
            FragmentTransitionImpl handlingImpl2 = this.getHandlingImpl(this.mSharedElementTransition);
            if (handlingImpl != null && handlingImpl2 != null && handlingImpl != handlingImpl2) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Mixing framework transitions and AndroidX transitions is not allowed. Fragment ");
                sb.append(((SpecialEffectsInfo)this).getOperation().getFragment());
                sb.append(" returned Transition ");
                sb.append(this.mTransition);
                sb.append(" which uses a different Transition  type than its shared element transition ");
                sb.append(this.mSharedElementTransition);
                throw new IllegalArgumentException(sb.toString());
            }
            if (handlingImpl != null) {
                handlingImpl2 = handlingImpl;
            }
            return handlingImpl2;
        }
        
        @Nullable
        public Object getSharedElementTransition() {
            return this.mSharedElementTransition;
        }
        
        @Nullable
        Object getTransition() {
            return this.mTransition;
        }
        
        public boolean hasSharedElementTransition() {
            return this.mSharedElementTransition != null;
        }
        
        boolean isOverlapAllowed() {
            return this.mOverlapAllowed;
        }
    }
}

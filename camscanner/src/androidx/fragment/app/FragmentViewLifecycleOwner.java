// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import androidx.annotation.Nullable;
import android.os.Bundle;
import androidx.lifecycle.LifecycleOwner;
import androidx.savedstate.SavedStateRegistry;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.SavedStateViewModelFactory;
import androidx.annotation.CallSuper;
import android.content.Context;
import androidx.lifecycle.SavedStateHandleSupport;
import androidx.lifecycle.viewmodel.MutableCreationExtras;
import android.app.Application;
import android.content.ContextWrapper;
import androidx.lifecycle.viewmodel.CreationExtras;
import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelStore;
import androidx.savedstate.SavedStateRegistryController;
import androidx.lifecycle.LifecycleRegistry;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.savedstate.SavedStateRegistryOwner;
import androidx.lifecycle.HasDefaultViewModelProviderFactory;

class FragmentViewLifecycleOwner implements HasDefaultViewModelProviderFactory, SavedStateRegistryOwner, ViewModelStoreOwner
{
    private ViewModelProvider.Factory mDefaultFactory;
    private final Fragment mFragment;
    private LifecycleRegistry mLifecycleRegistry;
    private SavedStateRegistryController mSavedStateRegistryController;
    private final ViewModelStore mViewModelStore;
    
    FragmentViewLifecycleOwner(@NonNull final Fragment mFragment, @NonNull final ViewModelStore mViewModelStore) {
        this.mLifecycleRegistry = null;
        this.mSavedStateRegistryController = null;
        this.mFragment = mFragment;
        this.mViewModelStore = mViewModelStore;
    }
    
    @CallSuper
    @NonNull
    @Override
    public CreationExtras getDefaultViewModelCreationExtras() {
        while (true) {
            for (Context context = this.mFragment.requireContext().getApplicationContext(); context instanceof ContextWrapper; context = ((ContextWrapper)context).getBaseContext()) {
                if (context instanceof Application) {
                    final Application application = (Application)context;
                    final MutableCreationExtras mutableCreationExtras = new MutableCreationExtras();
                    if (application != null) {
                        mutableCreationExtras.set(ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY, application);
                    }
                    mutableCreationExtras.set(SavedStateHandleSupport.SAVED_STATE_REGISTRY_OWNER_KEY, this.mFragment);
                    mutableCreationExtras.set(SavedStateHandleSupport.VIEW_MODEL_STORE_OWNER_KEY, this);
                    if (this.mFragment.getArguments() != null) {
                        mutableCreationExtras.set(SavedStateHandleSupport.DEFAULT_ARGS_KEY, this.mFragment.getArguments());
                    }
                    return mutableCreationExtras;
                }
            }
            final Application application = null;
            continue;
        }
    }
    
    @NonNull
    @Override
    public ViewModelProvider.Factory getDefaultViewModelProviderFactory() {
        final ViewModelProvider.Factory defaultViewModelProviderFactory = this.mFragment.getDefaultViewModelProviderFactory();
        if (!defaultViewModelProviderFactory.equals(this.mFragment.mDefaultFactory)) {
            return this.mDefaultFactory = defaultViewModelProviderFactory;
        }
        if (this.mDefaultFactory == null) {
            while (true) {
                for (Context context = this.mFragment.requireContext().getApplicationContext(); context instanceof ContextWrapper; context = ((ContextWrapper)context).getBaseContext()) {
                    if (context instanceof Application) {
                        final Application application = (Application)context;
                        final Fragment mFragment = this.mFragment;
                        this.mDefaultFactory = new SavedStateViewModelFactory(application, mFragment, mFragment.getArguments());
                        return this.mDefaultFactory;
                    }
                }
                final Application application = null;
                continue;
            }
        }
        return this.mDefaultFactory;
    }
    
    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        this.initialize();
        return this.mLifecycleRegistry;
    }
    
    @NonNull
    @Override
    public SavedStateRegistry getSavedStateRegistry() {
        this.initialize();
        return this.mSavedStateRegistryController.getSavedStateRegistry();
    }
    
    @NonNull
    @Override
    public ViewModelStore getViewModelStore() {
        this.initialize();
        return this.mViewModelStore;
    }
    
    void handleLifecycleEvent(@NonNull final Lifecycle.Event event) {
        this.mLifecycleRegistry.handleLifecycleEvent(event);
    }
    
    void initialize() {
        if (this.mLifecycleRegistry == null) {
            this.mLifecycleRegistry = new LifecycleRegistry(this);
            (this.mSavedStateRegistryController = SavedStateRegistryController.create(this)).performAttach();
        }
    }
    
    boolean isInitialized() {
        return this.mLifecycleRegistry != null;
    }
    
    void performRestore(@Nullable final Bundle bundle) {
        this.mSavedStateRegistryController.performRestore(bundle);
    }
    
    void performSave(@NonNull final Bundle bundle) {
        this.mSavedStateRegistryController.performSave(bundle);
    }
    
    void setCurrentState(@NonNull final Lifecycle.State currentState) {
        this.mLifecycleRegistry.setCurrentState(currentState);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.os.Parcelable;
import android.os.BaseBundle;
import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleOwner;
import java.util.Iterator;
import android.app.Activity;
import androidx.lifecycle.ViewModelStoreOwner;
import android.view.LayoutInflater;
import android.view.View$OnAttachStateChangeListener;
import androidx.core.view.ViewCompat;
import androidx.fragment.R;
import androidx.fragment.app.strictmode.FragmentStrictMode;
import android.content.res.Resources$NotFoundException;
import android.view.ViewGroup;
import android.util.SparseArray;
import android.view.ViewParent;
import android.view.View;
import android.os.Bundle;
import androidx.annotation.NonNull;

class FragmentStateManager
{
    private static final String TAG = "FragmentManager";
    private static final String TARGET_REQUEST_CODE_STATE_TAG = "android:target_req_state";
    private static final String TARGET_STATE_TAG = "android:target_state";
    private static final String USER_VISIBLE_HINT_TAG = "android:user_visible_hint";
    private static final String VIEW_REGISTRY_STATE_TAG = "android:view_registry_state";
    private static final String VIEW_STATE_TAG = "android:view_state";
    private final FragmentLifecycleCallbacksDispatcher mDispatcher;
    @NonNull
    private final Fragment mFragment;
    private int mFragmentManagerState;
    private final FragmentStore mFragmentStore;
    private boolean mMovingToState;
    
    FragmentStateManager(@NonNull final FragmentLifecycleCallbacksDispatcher mDispatcher, @NonNull final FragmentStore mFragmentStore, @NonNull final Fragment mFragment) {
        this.mMovingToState = false;
        this.mFragmentManagerState = -1;
        this.mDispatcher = mDispatcher;
        this.mFragmentStore = mFragmentStore;
        this.mFragment = mFragment;
    }
    
    FragmentStateManager(@NonNull final FragmentLifecycleCallbacksDispatcher mDispatcher, @NonNull final FragmentStore mFragmentStore, @NonNull final Fragment mFragment, @NonNull final FragmentState fragmentState) {
        this.mMovingToState = false;
        this.mFragmentManagerState = -1;
        this.mDispatcher = mDispatcher;
        this.mFragmentStore = mFragmentStore;
        this.mFragment = mFragment;
        mFragment.mSavedViewState = null;
        mFragment.mSavedViewRegistryState = null;
        mFragment.mBackStackNesting = 0;
        mFragment.mInLayout = false;
        mFragment.mAdded = false;
        final Fragment mTarget = mFragment.mTarget;
        String mWho;
        if (mTarget != null) {
            mWho = mTarget.mWho;
        }
        else {
            mWho = null;
        }
        mFragment.mTargetWho = mWho;
        mFragment.mTarget = null;
        final Bundle mSavedFragmentState = fragmentState.mSavedFragmentState;
        if (mSavedFragmentState != null) {
            mFragment.mSavedFragmentState = mSavedFragmentState;
        }
        else {
            mFragment.mSavedFragmentState = new Bundle();
        }
    }
    
    FragmentStateManager(@NonNull final FragmentLifecycleCallbacksDispatcher mDispatcher, @NonNull final FragmentStore mFragmentStore, @NonNull final ClassLoader classLoader, @NonNull final FragmentFactory fragmentFactory, @NonNull final FragmentState fragmentState) {
        this.mMovingToState = false;
        this.mFragmentManagerState = -1;
        this.mDispatcher = mDispatcher;
        this.mFragmentStore = mFragmentStore;
        final Fragment instantiate = fragmentState.instantiate(fragmentFactory, classLoader);
        this.mFragment = instantiate;
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Instantiated fragment ");
            sb.append(instantiate);
        }
    }
    
    private boolean isFragmentViewChild(@NonNull final View view) {
        if (view == this.mFragment.mView) {
            return true;
        }
        for (ViewParent viewParent = view.getParent(); viewParent != null; viewParent = viewParent.getParent()) {
            if (viewParent == this.mFragment.mView) {
                return true;
            }
        }
        return false;
    }
    
    private Bundle saveBasicState() {
        final Bundle bundle = new Bundle();
        this.mFragment.performSaveInstanceState(bundle);
        this.mDispatcher.dispatchOnFragmentSaveInstanceState(this.mFragment, bundle, false);
        Bundle bundle2 = bundle;
        if (((BaseBundle)bundle).isEmpty()) {
            bundle2 = null;
        }
        if (this.mFragment.mView != null) {
            this.saveViewState();
        }
        Bundle bundle3 = bundle2;
        if (this.mFragment.mSavedViewState != null) {
            if ((bundle3 = bundle2) == null) {
                bundle3 = new Bundle();
            }
            bundle3.putSparseParcelableArray("android:view_state", (SparseArray)this.mFragment.mSavedViewState);
        }
        Bundle bundle4 = bundle3;
        if (this.mFragment.mSavedViewRegistryState != null) {
            if ((bundle4 = bundle3) == null) {
                bundle4 = new Bundle();
            }
            bundle4.putBundle("android:view_registry_state", this.mFragment.mSavedViewRegistryState);
        }
        Bundle bundle5 = bundle4;
        if (!this.mFragment.mUserVisibleHint) {
            if ((bundle5 = bundle4) == null) {
                bundle5 = new Bundle();
            }
            bundle5.putBoolean("android:user_visible_hint", this.mFragment.mUserVisibleHint);
        }
        return bundle5;
    }
    
    void activityCreated() {
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("moveto ACTIVITY_CREATED: ");
            sb.append(this.mFragment);
        }
        final Fragment mFragment = this.mFragment;
        mFragment.performActivityCreated(mFragment.mSavedFragmentState);
        final FragmentLifecycleCallbacksDispatcher mDispatcher = this.mDispatcher;
        final Fragment mFragment2 = this.mFragment;
        mDispatcher.dispatchOnFragmentActivityCreated(mFragment2, mFragment2.mSavedFragmentState, false);
    }
    
    void addViewToContainer() {
        final int fragmentIndexInContainer = this.mFragmentStore.findFragmentIndexInContainer(this.mFragment);
        final Fragment mFragment = this.mFragment;
        mFragment.mContainer.addView(mFragment.mView, fragmentIndexInContainer);
    }
    
    void attach() {
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("moveto ATTACHED: ");
            sb.append(this.mFragment);
        }
        final Fragment mFragment = this.mFragment;
        final Fragment mTarget = mFragment.mTarget;
        FragmentStateManager fragmentStateManager = null;
        if (mTarget != null) {
            fragmentStateManager = this.mFragmentStore.getFragmentStateManager(mTarget.mWho);
            if (fragmentStateManager == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Fragment ");
                sb2.append(this.mFragment);
                sb2.append(" declared target fragment ");
                sb2.append(this.mFragment.mTarget);
                sb2.append(" that does not belong to this FragmentManager!");
                throw new IllegalStateException(sb2.toString());
            }
            final Fragment mFragment2 = this.mFragment;
            mFragment2.mTargetWho = mFragment2.mTarget.mWho;
            mFragment2.mTarget = null;
        }
        else {
            final String mTargetWho = mFragment.mTargetWho;
            if (mTargetWho != null) {
                fragmentStateManager = this.mFragmentStore.getFragmentStateManager(mTargetWho);
                if (fragmentStateManager == null) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Fragment ");
                    sb3.append(this.mFragment);
                    sb3.append(" declared target fragment ");
                    sb3.append(this.mFragment.mTargetWho);
                    sb3.append(" that does not belong to this FragmentManager!");
                    throw new IllegalStateException(sb3.toString());
                }
            }
        }
        if (fragmentStateManager != null) {
            fragmentStateManager.moveToExpectedState();
        }
        final Fragment mFragment3 = this.mFragment;
        mFragment3.mHost = mFragment3.mFragmentManager.getHost();
        final Fragment mFragment4 = this.mFragment;
        mFragment4.mParentFragment = mFragment4.mFragmentManager.getParent();
        this.mDispatcher.dispatchOnFragmentPreAttached(this.mFragment, false);
        this.mFragment.performAttach();
        this.mDispatcher.dispatchOnFragmentAttached(this.mFragment, false);
    }
    
    int computeExpectedState() {
        final Fragment mFragment = this.mFragment;
        if (mFragment.mFragmentManager == null) {
            return mFragment.mState;
        }
        final int mFragmentManagerState = this.mFragmentManagerState;
        final int n = FragmentStateManager$2.$SwitchMap$androidx$lifecycle$Lifecycle$State[mFragment.mMaxState.ordinal()];
        int n2 = mFragmentManagerState;
        if (n != 1) {
            if (n != 2) {
                if (n != 3) {
                    if (n != 4) {
                        n2 = Math.min(mFragmentManagerState, -1);
                    }
                    else {
                        n2 = Math.min(mFragmentManagerState, 0);
                    }
                }
                else {
                    n2 = Math.min(mFragmentManagerState, 1);
                }
            }
            else {
                n2 = Math.min(mFragmentManagerState, 5);
            }
        }
        final Fragment mFragment2 = this.mFragment;
        int a = n2;
        if (mFragment2.mFromLayout) {
            if (mFragment2.mInLayout) {
                final int max = Math.max(this.mFragmentManagerState, 2);
                final View mView = this.mFragment.mView;
                a = max;
                if (mView != null) {
                    a = max;
                    if (mView.getParent() == null) {
                        a = Math.min(max, 2);
                    }
                }
            }
            else if (this.mFragmentManagerState < 4) {
                a = Math.min(n2, mFragment2.mState);
            }
            else {
                a = Math.min(n2, 1);
            }
        }
        int min = a;
        if (!this.mFragment.mAdded) {
            min = Math.min(a, 1);
        }
        final Fragment mFragment3 = this.mFragment;
        final ViewGroup mContainer = mFragment3.mContainer;
        Enum<SpecialEffectsController.Operation.LifecycleImpact> awaitingCompletionLifecycleImpact;
        if (mContainer != null) {
            awaitingCompletionLifecycleImpact = SpecialEffectsController.getOrCreateController(mContainer, mFragment3.getParentFragmentManager()).getAwaitingCompletionLifecycleImpact(this);
        }
        else {
            awaitingCompletionLifecycleImpact = null;
        }
        int a2;
        if (awaitingCompletionLifecycleImpact == SpecialEffectsController.Operation.LifecycleImpact.ADDING) {
            a2 = Math.min(min, 6);
        }
        else if (awaitingCompletionLifecycleImpact == SpecialEffectsController.Operation.LifecycleImpact.REMOVING) {
            a2 = Math.max(min, 3);
        }
        else {
            final Fragment mFragment4 = this.mFragment;
            a2 = min;
            if (mFragment4.mRemoving) {
                if (mFragment4.isInBackStack()) {
                    a2 = Math.min(min, 1);
                }
                else {
                    a2 = Math.min(min, -1);
                }
            }
        }
        final Fragment mFragment5 = this.mFragment;
        int min2 = a2;
        if (mFragment5.mDeferStart) {
            min2 = a2;
            if (mFragment5.mState < 5) {
                min2 = Math.min(a2, 4);
            }
        }
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("computeExpectedState() of ");
            sb.append(min2);
            sb.append(" for ");
            sb.append(this.mFragment);
        }
        return min2;
    }
    
    void create() {
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("moveto CREATED: ");
            sb.append(this.mFragment);
        }
        final Fragment mFragment = this.mFragment;
        if (!mFragment.mIsCreated) {
            this.mDispatcher.dispatchOnFragmentPreCreated(mFragment, mFragment.mSavedFragmentState, false);
            final Fragment mFragment2 = this.mFragment;
            mFragment2.performCreate(mFragment2.mSavedFragmentState);
            final FragmentLifecycleCallbacksDispatcher mDispatcher = this.mDispatcher;
            final Fragment mFragment3 = this.mFragment;
            mDispatcher.dispatchOnFragmentCreated(mFragment3, mFragment3.mSavedFragmentState, false);
        }
        else {
            mFragment.restoreChildFragmentState(mFragment.mSavedFragmentState);
            this.mFragment.mState = 1;
        }
    }
    
    void createView() {
        if (this.mFragment.mFromLayout) {
            return;
        }
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("moveto CREATE_VIEW: ");
            sb.append(this.mFragment);
        }
        final Fragment mFragment = this.mFragment;
        final LayoutInflater performGetLayoutInflater = mFragment.performGetLayoutInflater(mFragment.mSavedFragmentState);
        final Fragment mFragment2 = this.mFragment;
        ViewGroup mContainer = mFragment2.mContainer;
        if (mContainer == null) {
            final int mContainerId = mFragment2.mContainerId;
            if (mContainerId != 0) {
                if (mContainerId == -1) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Cannot create fragment ");
                    sb2.append(this.mFragment);
                    sb2.append(" for a container view with no id");
                    throw new IllegalArgumentException(sb2.toString());
                }
                final ViewGroup viewGroup = (ViewGroup)mFragment2.mFragmentManager.getContainer().onFindViewById(this.mFragment.mContainerId);
                if (viewGroup == null) {
                    final Fragment mFragment3 = this.mFragment;
                    if (!mFragment3.mRestored) {
                        String resourceName;
                        try {
                            resourceName = mFragment3.getResources().getResourceName(this.mFragment.mContainerId);
                        }
                        catch (final Resources$NotFoundException ex) {
                            resourceName = "unknown";
                        }
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("No view found for id 0x");
                        sb3.append(Integer.toHexString(this.mFragment.mContainerId));
                        sb3.append(" (");
                        sb3.append(resourceName);
                        sb3.append(") for fragment ");
                        sb3.append(this.mFragment);
                        throw new IllegalArgumentException(sb3.toString());
                    }
                    mContainer = viewGroup;
                }
                else {
                    mContainer = viewGroup;
                    if (!(viewGroup instanceof FragmentContainerView)) {
                        FragmentStrictMode.onWrongFragmentContainer(this.mFragment, viewGroup);
                        mContainer = viewGroup;
                    }
                }
            }
            else {
                mContainer = null;
            }
        }
        final Fragment mFragment4 = this.mFragment;
        mFragment4.performCreateView(performGetLayoutInflater, mFragment4.mContainer = mContainer, mFragment4.mSavedFragmentState);
        final View mView = this.mFragment.mView;
        if (mView != null) {
            mView.setSaveFromParentEnabled(false);
            final Fragment mFragment5 = this.mFragment;
            mFragment5.mView.setTag(R.id.fragment_container_view_tag, (Object)mFragment5);
            if (mContainer != null) {
                this.addViewToContainer();
            }
            final Fragment mFragment6 = this.mFragment;
            if (mFragment6.mHidden) {
                mFragment6.mView.setVisibility(8);
            }
            if (ViewCompat.isAttachedToWindow(this.mFragment.mView)) {
                ViewCompat.requestApplyInsets(this.mFragment.mView);
            }
            else {
                final View mView2 = this.mFragment.mView;
                mView2.addOnAttachStateChangeListener((View$OnAttachStateChangeListener)new View$OnAttachStateChangeListener(this, mView2) {
                    final FragmentStateManager this$0;
                    final View val$fragmentView;
                    
                    public void onViewAttachedToWindow(final View view) {
                        this.val$fragmentView.removeOnAttachStateChangeListener((View$OnAttachStateChangeListener)this);
                        ViewCompat.requestApplyInsets(this.val$fragmentView);
                    }
                    
                    public void onViewDetachedFromWindow(final View view) {
                    }
                });
            }
            this.mFragment.performViewCreated();
            final FragmentLifecycleCallbacksDispatcher mDispatcher = this.mDispatcher;
            final Fragment mFragment7 = this.mFragment;
            mDispatcher.dispatchOnFragmentViewCreated(mFragment7, mFragment7.mView, mFragment7.mSavedFragmentState, false);
            final int visibility = this.mFragment.mView.getVisibility();
            this.mFragment.setPostOnViewCreatedAlpha(this.mFragment.mView.getAlpha());
            final Fragment mFragment8 = this.mFragment;
            if (mFragment8.mContainer != null && visibility == 0) {
                final View focus = mFragment8.mView.findFocus();
                if (focus != null) {
                    this.mFragment.setFocusedView(focus);
                    if (FragmentManager.isLoggingEnabled(2)) {
                        final StringBuilder sb4 = new StringBuilder();
                        sb4.append("requestFocus: Saved focused view ");
                        sb4.append(focus);
                        sb4.append(" for Fragment ");
                        sb4.append(this.mFragment);
                    }
                }
                this.mFragment.mView.setAlpha(0.0f);
            }
        }
        this.mFragment.mState = 2;
    }
    
    void destroy() {
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("movefrom CREATED: ");
            sb.append(this.mFragment);
        }
        final Fragment mFragment = this.mFragment;
        final boolean mRemoving = mFragment.mRemoving;
        boolean cleared = true;
        final boolean b = mRemoving && !mFragment.isInBackStack();
        if (b) {
            final Fragment mFragment2 = this.mFragment;
            if (!mFragment2.mBeingSaved) {
                this.mFragmentStore.setSavedState(mFragment2.mWho, null);
            }
        }
        if (b || this.mFragmentStore.getNonConfig().shouldDestroy(this.mFragment)) {
            final FragmentHostCallback<?> mHost = this.mFragment.mHost;
            if (mHost instanceof ViewModelStoreOwner) {
                cleared = this.mFragmentStore.getNonConfig().isCleared();
            }
            else if (mHost.getContext() instanceof Activity) {
                cleared = (true ^ ((Activity)mHost.getContext()).isChangingConfigurations());
            }
            if ((b && !this.mFragment.mBeingSaved) || cleared) {
                this.mFragmentStore.getNonConfig().clearNonConfigState(this.mFragment);
            }
            this.mFragment.performDestroy();
            this.mDispatcher.dispatchOnFragmentDestroyed(this.mFragment, false);
            for (final FragmentStateManager fragmentStateManager : this.mFragmentStore.getActiveFragmentStateManagers()) {
                if (fragmentStateManager != null) {
                    final Fragment fragment = fragmentStateManager.getFragment();
                    if (!this.mFragment.mWho.equals(fragment.mTargetWho)) {
                        continue;
                    }
                    fragment.mTarget = this.mFragment;
                    fragment.mTargetWho = null;
                }
            }
            final Fragment mFragment3 = this.mFragment;
            final String mTargetWho = mFragment3.mTargetWho;
            if (mTargetWho != null) {
                mFragment3.mTarget = this.mFragmentStore.findActiveFragment(mTargetWho);
            }
            this.mFragmentStore.makeInactive(this);
        }
        else {
            final String mTargetWho2 = this.mFragment.mTargetWho;
            if (mTargetWho2 != null) {
                final Fragment activeFragment = this.mFragmentStore.findActiveFragment(mTargetWho2);
                if (activeFragment != null && activeFragment.mRetainInstance) {
                    this.mFragment.mTarget = activeFragment;
                }
            }
            this.mFragment.mState = 0;
        }
    }
    
    void destroyFragmentView() {
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("movefrom CREATE_VIEW: ");
            sb.append(this.mFragment);
        }
        final Fragment mFragment = this.mFragment;
        final ViewGroup mContainer = mFragment.mContainer;
        if (mContainer != null) {
            final View mView = mFragment.mView;
            if (mView != null) {
                mContainer.removeView(mView);
            }
        }
        this.mFragment.performDestroyView();
        this.mDispatcher.dispatchOnFragmentViewDestroyed(this.mFragment, false);
        final Fragment mFragment2 = this.mFragment;
        mFragment2.mContainer = null;
        mFragment2.mView = null;
        mFragment2.mViewLifecycleOwner = null;
        mFragment2.mViewLifecycleOwnerLiveData.setValue(null);
        this.mFragment.mInLayout = false;
    }
    
    void detach() {
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("movefrom ATTACHED: ");
            sb.append(this.mFragment);
        }
        this.mFragment.performDetach();
        final FragmentLifecycleCallbacksDispatcher mDispatcher = this.mDispatcher;
        final Fragment mFragment = this.mFragment;
        final int n = 0;
        mDispatcher.dispatchOnFragmentDetached(mFragment, false);
        final Fragment mFragment2 = this.mFragment;
        mFragment2.mState = -1;
        mFragment2.mHost = null;
        mFragment2.mParentFragment = null;
        mFragment2.mFragmentManager = null;
        int n2 = n;
        if (mFragment2.mRemoving) {
            n2 = n;
            if (!mFragment2.isInBackStack()) {
                n2 = 1;
            }
        }
        if (n2 != 0 || this.mFragmentStore.getNonConfig().shouldDestroy(this.mFragment)) {
            if (FragmentManager.isLoggingEnabled(3)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("initState called for fragment: ");
                sb2.append(this.mFragment);
            }
            this.mFragment.initState();
        }
    }
    
    void ensureInflatedView() {
        final Fragment mFragment = this.mFragment;
        if (mFragment.mFromLayout && mFragment.mInLayout && !mFragment.mPerformedCreateView) {
            if (FragmentManager.isLoggingEnabled(3)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("moveto CREATE_VIEW: ");
                sb.append(this.mFragment);
            }
            final Fragment mFragment2 = this.mFragment;
            mFragment2.performCreateView(mFragment2.performGetLayoutInflater(mFragment2.mSavedFragmentState), null, this.mFragment.mSavedFragmentState);
            final View mView = this.mFragment.mView;
            if (mView != null) {
                mView.setSaveFromParentEnabled(false);
                final Fragment mFragment3 = this.mFragment;
                mFragment3.mView.setTag(R.id.fragment_container_view_tag, (Object)mFragment3);
                final Fragment mFragment4 = this.mFragment;
                if (mFragment4.mHidden) {
                    mFragment4.mView.setVisibility(8);
                }
                this.mFragment.performViewCreated();
                final FragmentLifecycleCallbacksDispatcher mDispatcher = this.mDispatcher;
                final Fragment mFragment5 = this.mFragment;
                mDispatcher.dispatchOnFragmentViewCreated(mFragment5, mFragment5.mView, mFragment5.mSavedFragmentState, false);
                this.mFragment.mState = 2;
            }
        }
    }
    
    @NonNull
    Fragment getFragment() {
        return this.mFragment;
    }
    
    void moveToExpectedState() {
        if (this.mMovingToState) {
            if (FragmentManager.isLoggingEnabled(2)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Ignoring re-entrant call to moveToExpectedState() for ");
                sb.append(this.getFragment());
            }
            return;
        }
        try {
            this.mMovingToState = true;
            boolean b = false;
            Fragment mFragment;
            int mState;
            while (true) {
                final int computeExpectedState = this.computeExpectedState();
                mFragment = this.mFragment;
                mState = mFragment.mState;
                if (computeExpectedState == mState) {
                    break;
                }
                if (computeExpectedState > mState) {
                    switch (mState + 1) {
                        case 7: {
                            this.resume();
                            break;
                        }
                        case 6: {
                            mFragment.mState = 6;
                            break;
                        }
                        case 5: {
                            this.start();
                            break;
                        }
                        case 4: {
                            if (mFragment.mView != null) {
                                final ViewGroup mContainer = mFragment.mContainer;
                                if (mContainer != null) {
                                    SpecialEffectsController.getOrCreateController(mContainer, mFragment.getParentFragmentManager()).enqueueAdd(SpecialEffectsController.Operation.State.from(this.mFragment.mView.getVisibility()), this);
                                }
                            }
                            this.mFragment.mState = 4;
                            break;
                        }
                        case 3: {
                            this.activityCreated();
                            break;
                        }
                        case 2: {
                            this.ensureInflatedView();
                            this.createView();
                            break;
                        }
                        case 1: {
                            this.create();
                            break;
                        }
                        case 0: {
                            this.attach();
                            break;
                        }
                    }
                }
                else {
                    switch (mState - 1) {
                        case 6: {
                            this.pause();
                            break;
                        }
                        case 5: {
                            mFragment.mState = 5;
                            break;
                        }
                        case 4: {
                            this.stop();
                            break;
                        }
                        case 3: {
                            if (FragmentManager.isLoggingEnabled(3)) {
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("movefrom ACTIVITY_CREATED: ");
                                sb2.append(this.mFragment);
                            }
                            final Fragment mFragment2 = this.mFragment;
                            if (mFragment2.mBeingSaved) {
                                this.saveState();
                            }
                            else if (mFragment2.mView != null && mFragment2.mSavedViewState == null) {
                                this.saveViewState();
                            }
                            final Fragment mFragment3 = this.mFragment;
                            if (mFragment3.mView != null) {
                                final ViewGroup mContainer2 = mFragment3.mContainer;
                                if (mContainer2 != null) {
                                    SpecialEffectsController.getOrCreateController(mContainer2, mFragment3.getParentFragmentManager()).enqueueRemove(this);
                                }
                            }
                            this.mFragment.mState = 3;
                            break;
                        }
                        case 2: {
                            mFragment.mInLayout = false;
                            mFragment.mState = 2;
                            break;
                        }
                        case 1: {
                            this.destroyFragmentView();
                            this.mFragment.mState = 1;
                            break;
                        }
                        case 0: {
                            if (mFragment.mBeingSaved && this.mFragmentStore.getSavedState(mFragment.mWho) == null) {
                                this.saveState();
                            }
                            this.destroy();
                            break;
                        }
                        case -1: {
                            this.detach();
                            break;
                        }
                    }
                }
                b = true;
            }
            if (!b && mState == -1 && mFragment.mRemoving && !mFragment.isInBackStack() && !this.mFragment.mBeingSaved) {
                if (FragmentManager.isLoggingEnabled(3)) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Cleaning up state of never attached fragment: ");
                    sb3.append(this.mFragment);
                }
                this.mFragmentStore.getNonConfig().clearNonConfigState(this.mFragment);
                this.mFragmentStore.makeInactive(this);
                if (FragmentManager.isLoggingEnabled(3)) {
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("initState called for fragment: ");
                    sb4.append(this.mFragment);
                }
                this.mFragment.initState();
            }
            final Fragment mFragment4 = this.mFragment;
            if (mFragment4.mHiddenChanged) {
                if (mFragment4.mView != null) {
                    final ViewGroup mContainer3 = mFragment4.mContainer;
                    if (mContainer3 != null) {
                        final SpecialEffectsController orCreateController = SpecialEffectsController.getOrCreateController(mContainer3, mFragment4.getParentFragmentManager());
                        if (this.mFragment.mHidden) {
                            orCreateController.enqueueHide(this);
                        }
                        else {
                            orCreateController.enqueueShow(this);
                        }
                    }
                }
                final Fragment mFragment5 = this.mFragment;
                final FragmentManager mFragmentManager = mFragment5.mFragmentManager;
                if (mFragmentManager != null) {
                    mFragmentManager.invalidateMenuForFragment(mFragment5);
                }
                final Fragment mFragment6 = this.mFragment;
                mFragment6.mHiddenChanged = false;
                mFragment6.onHiddenChanged(mFragment6.mHidden);
                this.mFragment.mChildFragmentManager.dispatchOnHiddenChanged();
            }
        }
        finally {
            this.mMovingToState = false;
        }
    }
    
    void pause() {
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("movefrom RESUMED: ");
            sb.append(this.mFragment);
        }
        this.mFragment.performPause();
        this.mDispatcher.dispatchOnFragmentPaused(this.mFragment, false);
    }
    
    void restoreState(@NonNull final ClassLoader classLoader) {
        final Bundle mSavedFragmentState = this.mFragment.mSavedFragmentState;
        if (mSavedFragmentState == null) {
            return;
        }
        mSavedFragmentState.setClassLoader(classLoader);
        final Fragment mFragment = this.mFragment;
        mFragment.mSavedViewState = (SparseArray<Parcelable>)mFragment.mSavedFragmentState.getSparseParcelableArray("android:view_state");
        final Fragment mFragment2 = this.mFragment;
        mFragment2.mSavedViewRegistryState = mFragment2.mSavedFragmentState.getBundle("android:view_registry_state");
        final Fragment mFragment3 = this.mFragment;
        mFragment3.mTargetWho = ((BaseBundle)mFragment3.mSavedFragmentState).getString("android:target_state");
        final Fragment mFragment4 = this.mFragment;
        if (mFragment4.mTargetWho != null) {
            mFragment4.mTargetRequestCode = ((BaseBundle)mFragment4.mSavedFragmentState).getInt("android:target_req_state", 0);
        }
        final Fragment mFragment5 = this.mFragment;
        final Boolean mSavedUserVisibleHint = mFragment5.mSavedUserVisibleHint;
        if (mSavedUserVisibleHint != null) {
            mFragment5.mUserVisibleHint = mSavedUserVisibleHint;
            this.mFragment.mSavedUserVisibleHint = null;
        }
        else {
            mFragment5.mUserVisibleHint = mFragment5.mSavedFragmentState.getBoolean("android:user_visible_hint", true);
        }
        final Fragment mFragment6 = this.mFragment;
        if (!mFragment6.mUserVisibleHint) {
            mFragment6.mDeferStart = true;
        }
    }
    
    void resume() {
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("moveto RESUMED: ");
            sb.append(this.mFragment);
        }
        final View focusedView = this.mFragment.getFocusedView();
        if (focusedView != null && this.isFragmentViewChild(focusedView)) {
            final boolean requestFocus = focusedView.requestFocus();
            if (FragmentManager.isLoggingEnabled(2)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("requestFocus: Restoring focused view ");
                sb2.append(focusedView);
                sb2.append(" ");
                String str;
                if (requestFocus) {
                    str = "succeeded";
                }
                else {
                    str = "failed";
                }
                sb2.append(str);
                sb2.append(" on Fragment ");
                sb2.append(this.mFragment);
                sb2.append(" resulting in focused view ");
                sb2.append(this.mFragment.mView.findFocus());
            }
        }
        this.mFragment.setFocusedView(null);
        this.mFragment.performResume();
        this.mDispatcher.dispatchOnFragmentResumed(this.mFragment, false);
        final Fragment mFragment = this.mFragment;
        mFragment.mSavedFragmentState = null;
        mFragment.mSavedViewState = null;
        mFragment.mSavedViewRegistryState = null;
    }
    
    @Nullable
    Fragment.SavedState saveInstanceState() {
        final int mState = this.mFragment.mState;
        Object o = null;
        if (mState > -1) {
            final Bundle saveBasicState = this.saveBasicState();
            o = o;
            if (saveBasicState != null) {
                o = new Fragment.SavedState(saveBasicState);
            }
        }
        return (Fragment.SavedState)o;
    }
    
    void saveState() {
        final FragmentState fragmentState = new FragmentState(this.mFragment);
        final Fragment mFragment = this.mFragment;
        if (mFragment.mState > -1 && fragmentState.mSavedFragmentState == null) {
            final Bundle saveBasicState = this.saveBasicState();
            fragmentState.mSavedFragmentState = saveBasicState;
            if (this.mFragment.mTargetWho != null) {
                if (saveBasicState == null) {
                    fragmentState.mSavedFragmentState = new Bundle();
                }
                ((BaseBundle)fragmentState.mSavedFragmentState).putString("android:target_state", this.mFragment.mTargetWho);
                final int mTargetRequestCode = this.mFragment.mTargetRequestCode;
                if (mTargetRequestCode != 0) {
                    ((BaseBundle)fragmentState.mSavedFragmentState).putInt("android:target_req_state", mTargetRequestCode);
                }
            }
        }
        else {
            fragmentState.mSavedFragmentState = mFragment.mSavedFragmentState;
        }
        this.mFragmentStore.setSavedState(this.mFragment.mWho, fragmentState);
    }
    
    void saveViewState() {
        if (this.mFragment.mView == null) {
            return;
        }
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Saving view state for fragment ");
            sb.append(this.mFragment);
            sb.append(" with view ");
            sb.append(this.mFragment.mView);
        }
        final SparseArray mSavedViewState = new SparseArray();
        this.mFragment.mView.saveHierarchyState(mSavedViewState);
        if (mSavedViewState.size() > 0) {
            this.mFragment.mSavedViewState = (SparseArray<Parcelable>)mSavedViewState;
        }
        final Bundle mSavedViewRegistryState = new Bundle();
        this.mFragment.mViewLifecycleOwner.performSave(mSavedViewRegistryState);
        if (!((BaseBundle)mSavedViewRegistryState).isEmpty()) {
            this.mFragment.mSavedViewRegistryState = mSavedViewRegistryState;
        }
    }
    
    void setFragmentManagerState(final int mFragmentManagerState) {
        this.mFragmentManagerState = mFragmentManagerState;
    }
    
    void start() {
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("moveto STARTED: ");
            sb.append(this.mFragment);
        }
        this.mFragment.performStart();
        this.mDispatcher.dispatchOnFragmentStarted(this.mFragment, false);
    }
    
    void stop() {
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("movefrom STARTED: ");
            sb.append(this.mFragment);
        }
        this.mFragment.performStop();
        this.mDispatcher.dispatchOnFragmentStopped(this.mFragment, false);
    }
}

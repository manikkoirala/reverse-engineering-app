// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import androidx.annotation.CallSuper;
import java.util.HashSet;
import java.util.Collection;
import android.view.View;
import androidx.core.view.ViewCompat;
import java.util.List;
import androidx.fragment.R;
import androidx.annotation.Nullable;
import java.util.Iterator;
import androidx.core.os.CancellationSignal;
import androidx.annotation.NonNull;
import java.util.ArrayList;
import android.view.ViewGroup;

abstract class SpecialEffectsController
{
    private final ViewGroup mContainer;
    boolean mIsContainerPostponed;
    boolean mOperationDirectionIsPop;
    final ArrayList<Operation> mPendingOperations;
    final ArrayList<Operation> mRunningOperations;
    
    SpecialEffectsController(@NonNull final ViewGroup mContainer) {
        this.mPendingOperations = new ArrayList<Operation>();
        this.mRunningOperations = new ArrayList<Operation>();
        this.mOperationDirectionIsPop = false;
        this.mIsContainerPostponed = false;
        this.mContainer = mContainer;
    }
    
    private void enqueue(@NonNull final State state, @NonNull final LifecycleImpact lifecycleImpact, @NonNull final FragmentStateManager fragmentStateManager) {
        synchronized (this.mPendingOperations) {
            final CancellationSignal cancellationSignal = new CancellationSignal();
            final Operation pendingOperation = this.findPendingOperation(fragmentStateManager.getFragment());
            if (pendingOperation != null) {
                pendingOperation.mergeWith(state, lifecycleImpact);
                return;
            }
            final FragmentStateManagerOperation e = new FragmentStateManagerOperation(state, lifecycleImpact, fragmentStateManager, cancellationSignal);
            this.mPendingOperations.add((Operation)e);
            ((Operation)e).addCompletionListener(new Runnable(this, e) {
                final SpecialEffectsController this$0;
                final FragmentStateManagerOperation val$operation;
                
                @Override
                public void run() {
                    if (this.this$0.mPendingOperations.contains(this.val$operation)) {
                        ((Operation)this.val$operation).getFinalState().applyState(((Operation)this.val$operation).getFragment().mView);
                    }
                }
            });
            ((Operation)e).addCompletionListener(new Runnable(this, e) {
                final SpecialEffectsController this$0;
                final FragmentStateManagerOperation val$operation;
                
                @Override
                public void run() {
                    this.this$0.mPendingOperations.remove(this.val$operation);
                    this.this$0.mRunningOperations.remove(this.val$operation);
                }
            });
        }
    }
    
    @Nullable
    private Operation findPendingOperation(@NonNull final Fragment fragment) {
        for (final Operation operation : this.mPendingOperations) {
            if (operation.getFragment().equals(fragment) && !operation.isCanceled()) {
                return operation;
            }
        }
        return null;
    }
    
    @Nullable
    private Operation findRunningOperation(@NonNull final Fragment fragment) {
        for (final Operation operation : this.mRunningOperations) {
            if (operation.getFragment().equals(fragment) && !operation.isCanceled()) {
                return operation;
            }
        }
        return null;
    }
    
    @NonNull
    static SpecialEffectsController getOrCreateController(@NonNull final ViewGroup viewGroup, @NonNull final FragmentManager fragmentManager) {
        return getOrCreateController(viewGroup, fragmentManager.getSpecialEffectsControllerFactory());
    }
    
    @NonNull
    static SpecialEffectsController getOrCreateController(@NonNull final ViewGroup viewGroup, @NonNull final SpecialEffectsControllerFactory specialEffectsControllerFactory) {
        final int special_effects_controller_view_tag = R.id.special_effects_controller_view_tag;
        final Object tag = ((View)viewGroup).getTag(special_effects_controller_view_tag);
        if (tag instanceof SpecialEffectsController) {
            return (SpecialEffectsController)tag;
        }
        final SpecialEffectsController controller = specialEffectsControllerFactory.createController(viewGroup);
        ((View)viewGroup).setTag(special_effects_controller_view_tag, (Object)controller);
        return controller;
    }
    
    private void updateFinalState() {
        for (final Operation operation : this.mPendingOperations) {
            if (operation.getLifecycleImpact() == LifecycleImpact.ADDING) {
                operation.mergeWith(State.from(operation.getFragment().requireView().getVisibility()), LifecycleImpact.NONE);
            }
        }
    }
    
    void enqueueAdd(@NonNull final State state, @NonNull final FragmentStateManager fragmentStateManager) {
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("SpecialEffectsController: Enqueuing add operation for fragment ");
            sb.append(fragmentStateManager.getFragment());
        }
        this.enqueue(state, LifecycleImpact.ADDING, fragmentStateManager);
    }
    
    void enqueueHide(@NonNull final FragmentStateManager fragmentStateManager) {
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("SpecialEffectsController: Enqueuing hide operation for fragment ");
            sb.append(fragmentStateManager.getFragment());
        }
        this.enqueue(State.GONE, LifecycleImpact.NONE, fragmentStateManager);
    }
    
    void enqueueRemove(@NonNull final FragmentStateManager fragmentStateManager) {
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("SpecialEffectsController: Enqueuing remove operation for fragment ");
            sb.append(fragmentStateManager.getFragment());
        }
        this.enqueue(State.REMOVED, LifecycleImpact.REMOVING, fragmentStateManager);
    }
    
    void enqueueShow(@NonNull final FragmentStateManager fragmentStateManager) {
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("SpecialEffectsController: Enqueuing show operation for fragment ");
            sb.append(fragmentStateManager.getFragment());
        }
        this.enqueue(State.VISIBLE, LifecycleImpact.NONE, fragmentStateManager);
    }
    
    abstract void executeOperations(@NonNull final List<Operation> p0, final boolean p1);
    
    void executePendingOperations() {
        if (this.mIsContainerPostponed) {
            return;
        }
        if (!ViewCompat.isAttachedToWindow((View)this.mContainer)) {
            this.forceCompleteAllOperations();
            this.mOperationDirectionIsPop = false;
            return;
        }
        synchronized (this.mPendingOperations) {
            if (!this.mPendingOperations.isEmpty()) {
                final ArrayList list = new ArrayList(this.mRunningOperations);
                this.mRunningOperations.clear();
                for (final Operation operation : list) {
                    if (FragmentManager.isLoggingEnabled(2)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("SpecialEffectsController: Cancelling operation ");
                        sb.append(operation);
                    }
                    operation.cancel();
                    if (!operation.isComplete()) {
                        this.mRunningOperations.add(operation);
                    }
                }
                this.updateFinalState();
                final ArrayList<Operation> c = new ArrayList<Operation>(this.mPendingOperations);
                this.mPendingOperations.clear();
                this.mRunningOperations.addAll(c);
                FragmentManager.isLoggingEnabled(2);
                final Iterator<Operation> iterator2 = c.iterator();
                while (iterator2.hasNext()) {
                    iterator2.next().onStart();
                }
                this.executeOperations(c, this.mOperationDirectionIsPop);
                this.mOperationDirectionIsPop = false;
                FragmentManager.isLoggingEnabled(2);
            }
        }
    }
    
    void forceCompleteAllOperations() {
        FragmentManager.isLoggingEnabled(2);
        final boolean attachedToWindow = ViewCompat.isAttachedToWindow((View)this.mContainer);
        synchronized (this.mPendingOperations) {
            this.updateFinalState();
            final Iterator<Operation> iterator = this.mPendingOperations.iterator();
            while (iterator.hasNext()) {
                iterator.next().onStart();
            }
            for (final Operation obj : new ArrayList(this.mRunningOperations)) {
                if (FragmentManager.isLoggingEnabled(2)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("SpecialEffectsController: ");
                    String string;
                    if (attachedToWindow) {
                        string = "";
                    }
                    else {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Container ");
                        sb2.append(this.mContainer);
                        sb2.append(" is not attached to window. ");
                        string = sb2.toString();
                    }
                    sb.append(string);
                    sb.append("Cancelling running operation ");
                    sb.append(obj);
                }
                obj.cancel();
            }
            for (final Operation obj2 : new ArrayList(this.mPendingOperations)) {
                if (FragmentManager.isLoggingEnabled(2)) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("SpecialEffectsController: ");
                    String string2;
                    if (attachedToWindow) {
                        string2 = "";
                    }
                    else {
                        final StringBuilder sb4 = new StringBuilder();
                        sb4.append("Container ");
                        sb4.append(this.mContainer);
                        sb4.append(" is not attached to window. ");
                        string2 = sb4.toString();
                    }
                    sb3.append(string2);
                    sb3.append("Cancelling pending operation ");
                    sb3.append(obj2);
                }
                obj2.cancel();
            }
        }
    }
    
    void forcePostponedExecutePendingOperations() {
        if (this.mIsContainerPostponed) {
            FragmentManager.isLoggingEnabled(2);
            this.mIsContainerPostponed = false;
            this.executePendingOperations();
        }
    }
    
    @Nullable
    LifecycleImpact getAwaitingCompletionLifecycleImpact(@NonNull final FragmentStateManager fragmentStateManager) {
        final Operation pendingOperation = this.findPendingOperation(fragmentStateManager.getFragment());
        Enum<LifecycleImpact> lifecycleImpact;
        if (pendingOperation != null) {
            lifecycleImpact = pendingOperation.getLifecycleImpact();
        }
        else {
            lifecycleImpact = null;
        }
        final Operation runningOperation = this.findRunningOperation(fragmentStateManager.getFragment());
        if (runningOperation != null && (lifecycleImpact == null || lifecycleImpact == LifecycleImpact.NONE)) {
            return runningOperation.getLifecycleImpact();
        }
        return (LifecycleImpact)lifecycleImpact;
    }
    
    @NonNull
    public ViewGroup getContainer() {
        return this.mContainer;
    }
    
    void markPostponedState() {
        synchronized (this.mPendingOperations) {
            this.updateFinalState();
            this.mIsContainerPostponed = false;
            for (int i = this.mPendingOperations.size() - 1; i >= 0; --i) {
                final Operation operation = this.mPendingOperations.get(i);
                final State from = State.from(operation.getFragment().mView);
                final State finalState = operation.getFinalState();
                final State visible = State.VISIBLE;
                if (finalState == visible && from != visible) {
                    this.mIsContainerPostponed = operation.getFragment().isPostponed();
                    break;
                }
            }
        }
    }
    
    void updateOperationDirection(final boolean mOperationDirectionIsPop) {
        this.mOperationDirectionIsPop = mOperationDirectionIsPop;
    }
    
    private static class FragmentStateManagerOperation extends Operation
    {
        @NonNull
        private final FragmentStateManager mFragmentStateManager;
        
        FragmentStateManagerOperation(@NonNull final State state, @NonNull final LifecycleImpact lifecycleImpact, @NonNull final FragmentStateManager mFragmentStateManager, @NonNull final CancellationSignal cancellationSignal) {
            super(state, lifecycleImpact, mFragmentStateManager.getFragment(), cancellationSignal);
            this.mFragmentStateManager = mFragmentStateManager;
        }
        
        @Override
        public void complete() {
            super.complete();
            this.mFragmentStateManager.moveToExpectedState();
        }
        
        @Override
        void onStart() {
            if (((Operation)this).getLifecycleImpact() == LifecycleImpact.ADDING) {
                final Fragment fragment = this.mFragmentStateManager.getFragment();
                final View focus = fragment.mView.findFocus();
                if (focus != null) {
                    fragment.setFocusedView(focus);
                    if (FragmentManager.isLoggingEnabled(2)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("requestFocus: Saved focused view ");
                        sb.append(focus);
                        sb.append(" for Fragment ");
                        sb.append(fragment);
                    }
                }
                final View requireView = ((Operation)this).getFragment().requireView();
                if (requireView.getParent() == null) {
                    this.mFragmentStateManager.addViewToContainer();
                    requireView.setAlpha(0.0f);
                }
                if (requireView.getAlpha() == 0.0f && requireView.getVisibility() == 0) {
                    requireView.setVisibility(4);
                }
                requireView.setAlpha(fragment.getPostOnViewCreatedAlpha());
            }
            else if (((Operation)this).getLifecycleImpact() == LifecycleImpact.REMOVING) {
                final Fragment fragment2 = this.mFragmentStateManager.getFragment();
                final View requireView2 = fragment2.requireView();
                if (FragmentManager.isLoggingEnabled(2)) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Clearing focus ");
                    sb2.append(requireView2.findFocus());
                    sb2.append(" on view ");
                    sb2.append(requireView2);
                    sb2.append(" for Fragment ");
                    sb2.append(fragment2);
                }
                requireView2.clearFocus();
            }
        }
    }
    
    static class Operation
    {
        @NonNull
        private final List<Runnable> mCompletionListeners;
        @NonNull
        private State mFinalState;
        @NonNull
        private final Fragment mFragment;
        private boolean mIsCanceled;
        private boolean mIsComplete;
        @NonNull
        private LifecycleImpact mLifecycleImpact;
        @NonNull
        private final HashSet<CancellationSignal> mSpecialEffectsSignals;
        
        Operation(@NonNull final State mFinalState, @NonNull final LifecycleImpact mLifecycleImpact, @NonNull final Fragment mFragment, @NonNull final CancellationSignal cancellationSignal) {
            this.mCompletionListeners = new ArrayList<Runnable>();
            this.mSpecialEffectsSignals = new HashSet<CancellationSignal>();
            this.mIsCanceled = false;
            this.mIsComplete = false;
            this.mFinalState = mFinalState;
            this.mLifecycleImpact = mLifecycleImpact;
            this.mFragment = mFragment;
            cancellationSignal.setOnCancelListener((CancellationSignal.OnCancelListener)new CancellationSignal.OnCancelListener(this) {
                final Operation this$0;
                
                @Override
                public void onCancel() {
                    this.this$0.cancel();
                }
            });
        }
        
        final void addCompletionListener(@NonNull final Runnable runnable) {
            this.mCompletionListeners.add(runnable);
        }
        
        final void cancel() {
            if (this.isCanceled()) {
                return;
            }
            this.mIsCanceled = true;
            if (this.mSpecialEffectsSignals.isEmpty()) {
                this.complete();
            }
            else {
                final Iterator iterator = new ArrayList(this.mSpecialEffectsSignals).iterator();
                while (iterator.hasNext()) {
                    ((CancellationSignal)iterator.next()).cancel();
                }
            }
        }
        
        @CallSuper
        public void complete() {
            if (this.mIsComplete) {
                return;
            }
            if (FragmentManager.isLoggingEnabled(2)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("SpecialEffectsController: ");
                sb.append(this);
                sb.append(" has called complete.");
            }
            this.mIsComplete = true;
            final Iterator<Runnable> iterator = this.mCompletionListeners.iterator();
            while (iterator.hasNext()) {
                iterator.next().run();
            }
        }
        
        public final void completeSpecialEffect(@NonNull final CancellationSignal o) {
            if (this.mSpecialEffectsSignals.remove(o) && this.mSpecialEffectsSignals.isEmpty()) {
                this.complete();
            }
        }
        
        @NonNull
        public State getFinalState() {
            return this.mFinalState;
        }
        
        @NonNull
        public final Fragment getFragment() {
            return this.mFragment;
        }
        
        @NonNull
        LifecycleImpact getLifecycleImpact() {
            return this.mLifecycleImpact;
        }
        
        final boolean isCanceled() {
            return this.mIsCanceled;
        }
        
        final boolean isComplete() {
            return this.mIsComplete;
        }
        
        public final void markStartedSpecialEffect(@NonNull final CancellationSignal e) {
            this.onStart();
            this.mSpecialEffectsSignals.add(e);
        }
        
        final void mergeWith(@NonNull final State state, @NonNull final LifecycleImpact lifecycleImpact) {
            final int n = SpecialEffectsController$3.$SwitchMap$androidx$fragment$app$SpecialEffectsController$Operation$LifecycleImpact[lifecycleImpact.ordinal()];
            if (n != 1) {
                if (n != 2) {
                    if (n == 3) {
                        if (this.mFinalState != State.REMOVED) {
                            if (FragmentManager.isLoggingEnabled(2)) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("SpecialEffectsController: For fragment ");
                                sb.append(this.mFragment);
                                sb.append(" mFinalState = ");
                                sb.append(this.mFinalState);
                                sb.append(" -> ");
                                sb.append(state);
                                sb.append(". ");
                            }
                            this.mFinalState = state;
                        }
                    }
                }
                else {
                    if (FragmentManager.isLoggingEnabled(2)) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("SpecialEffectsController: For fragment ");
                        sb2.append(this.mFragment);
                        sb2.append(" mFinalState = ");
                        sb2.append(this.mFinalState);
                        sb2.append(" -> REMOVED. mLifecycleImpact  = ");
                        sb2.append(this.mLifecycleImpact);
                        sb2.append(" to REMOVING.");
                    }
                    this.mFinalState = State.REMOVED;
                    this.mLifecycleImpact = LifecycleImpact.REMOVING;
                }
            }
            else if (this.mFinalState == State.REMOVED) {
                if (FragmentManager.isLoggingEnabled(2)) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("SpecialEffectsController: For fragment ");
                    sb3.append(this.mFragment);
                    sb3.append(" mFinalState = REMOVED -> VISIBLE. mLifecycleImpact = ");
                    sb3.append(this.mLifecycleImpact);
                    sb3.append(" to ADDING.");
                }
                this.mFinalState = State.VISIBLE;
                this.mLifecycleImpact = LifecycleImpact.ADDING;
            }
        }
        
        void onStart() {
        }
        
        @NonNull
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Operation ");
            sb.append("{");
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            sb.append("} ");
            sb.append("{");
            sb.append("mFinalState = ");
            sb.append(this.mFinalState);
            sb.append("} ");
            sb.append("{");
            sb.append("mLifecycleImpact = ");
            sb.append(this.mLifecycleImpact);
            sb.append("} ");
            sb.append("{");
            sb.append("mFragment = ");
            sb.append(this.mFragment);
            sb.append("}");
            return sb.toString();
        }
        
        enum LifecycleImpact
        {
            private static final LifecycleImpact[] $VALUES;
            
            ADDING, 
            NONE, 
            REMOVING;
        }
        
        enum State
        {
            private static final State[] $VALUES;
            
            GONE, 
            INVISIBLE, 
            REMOVED, 
            VISIBLE;
            
            @NonNull
            static State from(final int i) {
                if (i == 0) {
                    return State.VISIBLE;
                }
                if (i == 4) {
                    return State.INVISIBLE;
                }
                if (i == 8) {
                    return State.GONE;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown visibility ");
                sb.append(i);
                throw new IllegalArgumentException(sb.toString());
            }
            
            @NonNull
            static State from(@NonNull final View view) {
                if (view.getAlpha() == 0.0f && view.getVisibility() == 0) {
                    return State.INVISIBLE;
                }
                return from(view.getVisibility());
            }
            
            void applyState(@NonNull final View view) {
                final int n = SpecialEffectsController$3.$SwitchMap$androidx$fragment$app$SpecialEffectsController$Operation$State[this.ordinal()];
                if (n != 1) {
                    if (n != 2) {
                        if (n != 3) {
                            if (n == 4) {
                                if (FragmentManager.isLoggingEnabled(2)) {
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append("SpecialEffectsController: Setting view ");
                                    sb.append(view);
                                    sb.append(" to INVISIBLE");
                                }
                                view.setVisibility(4);
                            }
                        }
                        else {
                            if (FragmentManager.isLoggingEnabled(2)) {
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("SpecialEffectsController: Setting view ");
                                sb2.append(view);
                                sb2.append(" to GONE");
                            }
                            view.setVisibility(8);
                        }
                    }
                    else {
                        if (FragmentManager.isLoggingEnabled(2)) {
                            final StringBuilder sb3 = new StringBuilder();
                            sb3.append("SpecialEffectsController: Setting view ");
                            sb3.append(view);
                            sb3.append(" to VISIBLE");
                        }
                        view.setVisibility(0);
                    }
                }
                else {
                    final ViewGroup obj = (ViewGroup)view.getParent();
                    if (obj != null) {
                        if (FragmentManager.isLoggingEnabled(2)) {
                            final StringBuilder sb4 = new StringBuilder();
                            sb4.append("SpecialEffectsController: Removing view ");
                            sb4.append(view);
                            sb4.append(" from container ");
                            sb4.append(obj);
                        }
                        obj.removeView(view);
                    }
                }
            }
        }
    }
}

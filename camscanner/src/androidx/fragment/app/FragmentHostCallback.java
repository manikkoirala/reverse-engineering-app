// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.content.IntentSender$SendIntentException;
import androidx.core.app.ActivityCompat;
import android.content.IntentSender;
import androidx.core.content.ContextCompat;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import androidx.annotation.RestrictTo;
import androidx.core.util.Preconditions;
import android.os.Handler;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.annotation.Nullable;
import android.app.Activity;

public abstract class FragmentHostCallback<E> extends FragmentContainer
{
    @Nullable
    private final Activity mActivity;
    @NonNull
    private final Context mContext;
    final FragmentManager mFragmentManager;
    @NonNull
    private final Handler mHandler;
    private final int mWindowAnimations;
    
    FragmentHostCallback(@Nullable final Activity mActivity, @NonNull final Context context, @NonNull final Handler handler, final int mWindowAnimations) {
        this.mFragmentManager = new FragmentManagerImpl();
        this.mActivity = mActivity;
        this.mContext = Preconditions.checkNotNull(context, "context == null");
        this.mHandler = Preconditions.checkNotNull(handler, "handler == null");
        this.mWindowAnimations = mWindowAnimations;
    }
    
    public FragmentHostCallback(@NonNull final Context context, @NonNull final Handler handler, final int n) {
        Activity activity;
        if (context instanceof Activity) {
            activity = (Activity)context;
        }
        else {
            activity = null;
        }
        this(activity, context, handler, n);
    }
    
    FragmentHostCallback(@NonNull final FragmentActivity fragmentActivity) {
        this(fragmentActivity, (Context)fragmentActivity, new Handler(), 0);
    }
    
    @Nullable
    Activity getActivity() {
        return this.mActivity;
    }
    
    @NonNull
    Context getContext() {
        return this.mContext;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public Handler getHandler() {
        return this.mHandler;
    }
    
    public void onDump(@NonNull final String s, @Nullable final FileDescriptor fileDescriptor, @NonNull final PrintWriter printWriter, @Nullable final String[] array) {
    }
    
    @Nullable
    @Override
    public View onFindViewById(final int n) {
        return null;
    }
    
    @Nullable
    public abstract E onGetHost();
    
    @NonNull
    public LayoutInflater onGetLayoutInflater() {
        return LayoutInflater.from(this.mContext);
    }
    
    public int onGetWindowAnimations() {
        return this.mWindowAnimations;
    }
    
    @Override
    public boolean onHasView() {
        return true;
    }
    
    public boolean onHasWindowAnimations() {
        return true;
    }
    
    @Deprecated
    public void onRequestPermissionsFromFragment(@NonNull final Fragment fragment, @NonNull final String[] array, final int n) {
    }
    
    public boolean onShouldSaveFragmentState(@NonNull final Fragment fragment) {
        return true;
    }
    
    public boolean onShouldShowRequestPermissionRationale(@NonNull final String s) {
        return false;
    }
    
    public void onStartActivityFromFragment(@NonNull final Fragment fragment, @SuppressLint({ "UnknownNullness" }) final Intent intent, final int n) {
        this.onStartActivityFromFragment(fragment, intent, n, null);
    }
    
    public void onStartActivityFromFragment(@NonNull final Fragment fragment, @SuppressLint({ "UnknownNullness" }) final Intent intent, final int n, @Nullable final Bundle bundle) {
        if (n == -1) {
            ContextCompat.startActivity(this.mContext, intent, bundle);
            return;
        }
        throw new IllegalStateException("Starting activity with a requestCode requires a FragmentActivity host");
    }
    
    @Deprecated
    public void onStartIntentSenderFromFragment(@NonNull final Fragment fragment, @SuppressLint({ "UnknownNullness" }) final IntentSender intentSender, final int n, @Nullable final Intent intent, final int n2, final int n3, final int n4, @Nullable final Bundle bundle) throws IntentSender$SendIntentException {
        if (n == -1) {
            ActivityCompat.startIntentSenderForResult(this.mActivity, intentSender, n, intent, n2, n3, n4, bundle);
            return;
        }
        throw new IllegalStateException("Starting intent sender with a requestCode requires a FragmentActivity host");
    }
    
    public void onSupportInvalidateOptionsMenu() {
    }
}

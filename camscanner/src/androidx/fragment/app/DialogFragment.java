// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import android.os.BaseBundle;
import androidx.annotation.RestrictTo;
import android.view.Window;
import android.view.ViewGroup;
import androidx.savedstate.SavedStateRegistryOwner;
import androidx.savedstate.ViewTreeSavedStateRegistryOwner;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.lifecycle.ViewTreeViewModelStoreOwner;
import androidx.lifecycle.ViewTreeLifecycleOwner;
import android.view.LayoutInflater;
import androidx.activity.ComponentDialog;
import androidx.annotation.StyleRes;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import android.content.Context;
import android.app.Activity;
import android.os.Bundle;
import android.os.Looper;
import androidx.annotation.LayoutRes;
import android.view.View;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import android.os.Handler;
import androidx.annotation.Nullable;
import android.app.Dialog;
import android.content.DialogInterface$OnDismissListener;
import android.content.DialogInterface$OnCancelListener;

public class DialogFragment extends Fragment implements DialogInterface$OnCancelListener, DialogInterface$OnDismissListener
{
    private static final String SAVED_BACK_STACK_ID = "android:backStackId";
    private static final String SAVED_CANCELABLE = "android:cancelable";
    private static final String SAVED_DIALOG_STATE_TAG = "android:savedDialogState";
    private static final String SAVED_INTERNAL_DIALOG_SHOWING = "android:dialogShowing";
    private static final String SAVED_SHOWS_DIALOG = "android:showsDialog";
    private static final String SAVED_STYLE = "android:style";
    private static final String SAVED_THEME = "android:theme";
    public static final int STYLE_NORMAL = 0;
    public static final int STYLE_NO_FRAME = 2;
    public static final int STYLE_NO_INPUT = 3;
    public static final int STYLE_NO_TITLE = 1;
    private int mBackStackId;
    private boolean mCancelable;
    private boolean mCreatingDialog;
    @Nullable
    private Dialog mDialog;
    private boolean mDialogCreated;
    private Runnable mDismissRunnable;
    private boolean mDismissed;
    private Handler mHandler;
    private Observer<LifecycleOwner> mObserver;
    private DialogInterface$OnCancelListener mOnCancelListener;
    private DialogInterface$OnDismissListener mOnDismissListener;
    private boolean mShownByMe;
    private boolean mShowsDialog;
    private int mStyle;
    private int mTheme;
    private boolean mViewDestroyed;
    
    public DialogFragment() {
        this.mDismissRunnable = new Runnable() {
            final DialogFragment this$0;
            
            @SuppressLint({ "SyntheticAccessor" })
            @Override
            public void run() {
                this.this$0.mOnDismissListener.onDismiss((DialogInterface)this.this$0.mDialog);
            }
        };
        this.mOnCancelListener = (DialogInterface$OnCancelListener)new DialogInterface$OnCancelListener() {
            final DialogFragment this$0;
            
            @SuppressLint({ "SyntheticAccessor" })
            public void onCancel(@Nullable final DialogInterface dialogInterface) {
                if (this.this$0.mDialog != null) {
                    final DialogFragment this$0 = this.this$0;
                    this$0.onCancel((DialogInterface)this$0.mDialog);
                }
            }
        };
        this.mOnDismissListener = (DialogInterface$OnDismissListener)new DialogInterface$OnDismissListener() {
            final DialogFragment this$0;
            
            @SuppressLint({ "SyntheticAccessor" })
            public void onDismiss(@Nullable final DialogInterface dialogInterface) {
                if (this.this$0.mDialog != null) {
                    final DialogFragment this$0 = this.this$0;
                    this$0.onDismiss((DialogInterface)this$0.mDialog);
                }
            }
        };
        this.mStyle = 0;
        this.mTheme = 0;
        this.mCancelable = true;
        this.mShowsDialog = true;
        this.mBackStackId = -1;
        this.mObserver = new Observer<LifecycleOwner>() {
            final DialogFragment this$0;
            
            @SuppressLint({ "SyntheticAccessor" })
            @Override
            public void onChanged(final LifecycleOwner lifecycleOwner) {
                if (lifecycleOwner != null && this.this$0.mShowsDialog) {
                    final View requireView = this.this$0.requireView();
                    if (requireView.getParent() != null) {
                        throw new IllegalStateException("DialogFragment can not be attached to a container view");
                    }
                    if (this.this$0.mDialog != null) {
                        if (FragmentManager.isLoggingEnabled(3)) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("DialogFragment ");
                            sb.append(this);
                            sb.append(" setting the content view on ");
                            sb.append(this.this$0.mDialog);
                        }
                        this.this$0.mDialog.setContentView(requireView);
                    }
                }
            }
        };
        this.mDialogCreated = false;
    }
    
    public DialogFragment(@LayoutRes final int n) {
        super(n);
        this.mDismissRunnable = new Runnable() {
            final DialogFragment this$0;
            
            @SuppressLint({ "SyntheticAccessor" })
            @Override
            public void run() {
                this.this$0.mOnDismissListener.onDismiss((DialogInterface)this.this$0.mDialog);
            }
        };
        this.mOnCancelListener = (DialogInterface$OnCancelListener)new DialogInterface$OnCancelListener() {
            final DialogFragment this$0;
            
            @SuppressLint({ "SyntheticAccessor" })
            public void onCancel(@Nullable final DialogInterface dialogInterface) {
                if (this.this$0.mDialog != null) {
                    final DialogFragment this$0 = this.this$0;
                    this$0.onCancel((DialogInterface)this$0.mDialog);
                }
            }
        };
        this.mOnDismissListener = (DialogInterface$OnDismissListener)new DialogInterface$OnDismissListener() {
            final DialogFragment this$0;
            
            @SuppressLint({ "SyntheticAccessor" })
            public void onDismiss(@Nullable final DialogInterface dialogInterface) {
                if (this.this$0.mDialog != null) {
                    final DialogFragment this$0 = this.this$0;
                    this$0.onDismiss((DialogInterface)this$0.mDialog);
                }
            }
        };
        this.mStyle = 0;
        this.mTheme = 0;
        this.mCancelable = true;
        this.mShowsDialog = true;
        this.mBackStackId = -1;
        this.mObserver = new Observer<LifecycleOwner>() {
            final DialogFragment this$0;
            
            @SuppressLint({ "SyntheticAccessor" })
            @Override
            public void onChanged(final LifecycleOwner lifecycleOwner) {
                if (lifecycleOwner != null && this.this$0.mShowsDialog) {
                    final View requireView = this.this$0.requireView();
                    if (requireView.getParent() != null) {
                        throw new IllegalStateException("DialogFragment can not be attached to a container view");
                    }
                    if (this.this$0.mDialog != null) {
                        if (FragmentManager.isLoggingEnabled(3)) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("DialogFragment ");
                            sb.append(this);
                            sb.append(" setting the content view on ");
                            sb.append(this.this$0.mDialog);
                        }
                        this.this$0.mDialog.setContentView(requireView);
                    }
                }
            }
        };
        this.mDialogCreated = false;
    }
    
    private void dismissInternal(final boolean b, final boolean b2, final boolean b3) {
        if (this.mDismissed) {
            return;
        }
        this.mDismissed = true;
        this.mShownByMe = false;
        final Dialog mDialog = this.mDialog;
        if (mDialog != null) {
            mDialog.setOnDismissListener((DialogInterface$OnDismissListener)null);
            this.mDialog.dismiss();
            if (!b2) {
                if (Looper.myLooper() == this.mHandler.getLooper()) {
                    this.onDismiss((DialogInterface)this.mDialog);
                }
                else {
                    this.mHandler.post(this.mDismissRunnable);
                }
            }
        }
        this.mViewDestroyed = true;
        if (this.mBackStackId >= 0) {
            if (b3) {
                this.getParentFragmentManager().popBackStackImmediate(this.mBackStackId, 1);
            }
            else {
                this.getParentFragmentManager().popBackStack(this.mBackStackId, 1, b);
            }
            this.mBackStackId = -1;
        }
        else {
            final FragmentTransaction beginTransaction = this.getParentFragmentManager().beginTransaction();
            beginTransaction.setReorderingAllowed(true);
            beginTransaction.remove(this);
            if (b3) {
                beginTransaction.commitNow();
            }
            else if (b) {
                beginTransaction.commitAllowingStateLoss();
            }
            else {
                beginTransaction.commit();
            }
        }
    }
    
    private void prepareDialog(@Nullable final Bundle bundle) {
        if (!this.mShowsDialog) {
            return;
        }
        if (!this.mDialogCreated) {
            try {
                this.mCreatingDialog = true;
                final Dialog onCreateDialog = this.onCreateDialog(bundle);
                this.mDialog = onCreateDialog;
                if (this.mShowsDialog) {
                    this.setupDialog(onCreateDialog, this.mStyle);
                    final Context context = this.getContext();
                    if (context instanceof Activity) {
                        this.mDialog.setOwnerActivity((Activity)context);
                    }
                    this.mDialog.setCancelable(this.mCancelable);
                    this.mDialog.setOnCancelListener(this.mOnCancelListener);
                    this.mDialog.setOnDismissListener(this.mOnDismissListener);
                    this.mDialogCreated = true;
                }
                else {
                    this.mDialog = null;
                }
            }
            finally {
                this.mCreatingDialog = false;
            }
        }
    }
    
    @NonNull
    @Override
    FragmentContainer createFragmentContainer() {
        return new FragmentContainer(this, super.createFragmentContainer()) {
            final DialogFragment this$0;
            final FragmentContainer val$fragmentContainer;
            
            @Nullable
            @Override
            public View onFindViewById(final int n) {
                if (this.val$fragmentContainer.onHasView()) {
                    return this.val$fragmentContainer.onFindViewById(n);
                }
                return this.this$0.onFindViewById(n);
            }
            
            @Override
            public boolean onHasView() {
                return this.val$fragmentContainer.onHasView() || this.this$0.onHasView();
            }
        };
    }
    
    public void dismiss() {
        this.dismissInternal(false, false, false);
    }
    
    public void dismissAllowingStateLoss() {
        this.dismissInternal(true, false, false);
    }
    
    @MainThread
    public void dismissNow() {
        this.dismissInternal(false, false, true);
    }
    
    @Nullable
    public Dialog getDialog() {
        return this.mDialog;
    }
    
    public boolean getShowsDialog() {
        return this.mShowsDialog;
    }
    
    @StyleRes
    public int getTheme() {
        return this.mTheme;
    }
    
    public boolean isCancelable() {
        return this.mCancelable;
    }
    
    @Deprecated
    @MainThread
    @Override
    public void onActivityCreated(@Nullable final Bundle bundle) {
        super.onActivityCreated(bundle);
    }
    
    @MainThread
    @Override
    public void onAttach(@NonNull final Context context) {
        super.onAttach(context);
        this.getViewLifecycleOwnerLiveData().observeForever(this.mObserver);
        if (!this.mShownByMe) {
            this.mDismissed = false;
        }
    }
    
    public void onCancel(@NonNull final DialogInterface dialogInterface) {
    }
    
    @MainThread
    @Override
    public void onCreate(@Nullable final Bundle bundle) {
        super.onCreate(bundle);
        this.mHandler = new Handler();
        this.mShowsDialog = (super.mContainerId == 0);
        if (bundle != null) {
            this.mStyle = ((BaseBundle)bundle).getInt("android:style", 0);
            this.mTheme = ((BaseBundle)bundle).getInt("android:theme", 0);
            this.mCancelable = bundle.getBoolean("android:cancelable", true);
            this.mShowsDialog = bundle.getBoolean("android:showsDialog", this.mShowsDialog);
            this.mBackStackId = ((BaseBundle)bundle).getInt("android:backStackId", -1);
        }
    }
    
    @MainThread
    @NonNull
    public Dialog onCreateDialog(@Nullable final Bundle bundle) {
        if (FragmentManager.isLoggingEnabled(3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("onCreateDialog called for DialogFragment ");
            sb.append(this);
        }
        return new ComponentDialog(this.requireContext(), this.getTheme());
    }
    
    @MainThread
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        final Dialog mDialog = this.mDialog;
        if (mDialog != null) {
            this.mViewDestroyed = true;
            mDialog.setOnDismissListener((DialogInterface$OnDismissListener)null);
            this.mDialog.dismiss();
            if (!this.mDismissed) {
                this.onDismiss((DialogInterface)this.mDialog);
            }
            this.mDialog = null;
            this.mDialogCreated = false;
        }
    }
    
    @MainThread
    @Override
    public void onDetach() {
        super.onDetach();
        if (!this.mShownByMe && !this.mDismissed) {
            this.mDismissed = true;
        }
        this.getViewLifecycleOwnerLiveData().removeObserver(this.mObserver);
    }
    
    public void onDismiss(@NonNull final DialogInterface dialogInterface) {
        if (!this.mViewDestroyed) {
            if (FragmentManager.isLoggingEnabled(3)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("onDismiss called for DialogFragment ");
                sb.append(this);
            }
            this.dismissInternal(true, true, false);
        }
    }
    
    @Nullable
    View onFindViewById(final int n) {
        final Dialog mDialog = this.mDialog;
        if (mDialog != null) {
            return mDialog.findViewById(n);
        }
        return null;
    }
    
    @NonNull
    @Override
    public LayoutInflater onGetLayoutInflater(@Nullable final Bundle bundle) {
        final LayoutInflater onGetLayoutInflater = super.onGetLayoutInflater(bundle);
        if (this.mShowsDialog && !this.mCreatingDialog) {
            this.prepareDialog(bundle);
            if (FragmentManager.isLoggingEnabled(2)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("get layout inflater for DialogFragment ");
                sb.append(this);
                sb.append(" from dialog context");
            }
            final Dialog mDialog = this.mDialog;
            LayoutInflater cloneInContext = onGetLayoutInflater;
            if (mDialog != null) {
                cloneInContext = onGetLayoutInflater.cloneInContext(mDialog.getContext());
            }
            return cloneInContext;
        }
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("getting layout inflater for DialogFragment ");
            sb2.append(this);
            final String string = sb2.toString();
            if (!this.mShowsDialog) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("mShowsDialog = false: ");
                sb3.append(string);
            }
            else {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("mCreatingDialog = true: ");
                sb4.append(string);
            }
        }
        return onGetLayoutInflater;
    }
    
    boolean onHasView() {
        return this.mDialogCreated;
    }
    
    @MainThread
    @Override
    public void onSaveInstanceState(@NonNull final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        final Dialog mDialog = this.mDialog;
        if (mDialog != null) {
            final Bundle onSaveInstanceState = mDialog.onSaveInstanceState();
            onSaveInstanceState.putBoolean("android:dialogShowing", false);
            bundle.putBundle("android:savedDialogState", onSaveInstanceState);
        }
        final int mStyle = this.mStyle;
        if (mStyle != 0) {
            ((BaseBundle)bundle).putInt("android:style", mStyle);
        }
        final int mTheme = this.mTheme;
        if (mTheme != 0) {
            ((BaseBundle)bundle).putInt("android:theme", mTheme);
        }
        final boolean mCancelable = this.mCancelable;
        if (!mCancelable) {
            bundle.putBoolean("android:cancelable", mCancelable);
        }
        final boolean mShowsDialog = this.mShowsDialog;
        if (!mShowsDialog) {
            bundle.putBoolean("android:showsDialog", mShowsDialog);
        }
        final int mBackStackId = this.mBackStackId;
        if (mBackStackId != -1) {
            ((BaseBundle)bundle).putInt("android:backStackId", mBackStackId);
        }
    }
    
    @MainThread
    @Override
    public void onStart() {
        super.onStart();
        final Dialog mDialog = this.mDialog;
        if (mDialog != null) {
            this.mViewDestroyed = false;
            mDialog.show();
            final View decorView = this.mDialog.getWindow().getDecorView();
            ViewTreeLifecycleOwner.set(decorView, this);
            ViewTreeViewModelStoreOwner.set(decorView, this);
            ViewTreeSavedStateRegistryOwner.set(decorView, this);
        }
    }
    
    @MainThread
    @Override
    public void onStop() {
        super.onStop();
        final Dialog mDialog = this.mDialog;
        if (mDialog != null) {
            mDialog.hide();
        }
    }
    
    @MainThread
    @Override
    public void onViewStateRestored(@Nullable Bundle bundle) {
        super.onViewStateRestored(bundle);
        if (this.mDialog != null && bundle != null) {
            bundle = bundle.getBundle("android:savedDialogState");
            if (bundle != null) {
                this.mDialog.onRestoreInstanceState(bundle);
            }
        }
    }
    
    @Override
    void performCreateView(@NonNull final LayoutInflater layoutInflater, @Nullable final ViewGroup viewGroup, @Nullable final Bundle bundle) {
        super.performCreateView(layoutInflater, viewGroup, bundle);
        if (super.mView == null && this.mDialog != null && bundle != null) {
            final Bundle bundle2 = bundle.getBundle("android:savedDialogState");
            if (bundle2 != null) {
                this.mDialog.onRestoreInstanceState(bundle2);
            }
        }
    }
    
    @NonNull
    public final Dialog requireDialog() {
        final Dialog dialog = this.getDialog();
        if (dialog != null) {
            return dialog;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("DialogFragment ");
        sb.append(this);
        sb.append(" does not have a Dialog.");
        throw new IllegalStateException(sb.toString());
    }
    
    public void setCancelable(final boolean b) {
        this.mCancelable = b;
        final Dialog mDialog = this.mDialog;
        if (mDialog != null) {
            mDialog.setCancelable(b);
        }
    }
    
    public void setShowsDialog(final boolean mShowsDialog) {
        this.mShowsDialog = mShowsDialog;
    }
    
    public void setStyle(final int n, @StyleRes final int n2) {
        if (FragmentManager.isLoggingEnabled(2)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Setting style and theme for DialogFragment ");
            sb.append(this);
            sb.append(" to ");
            sb.append(n);
            sb.append(", ");
            sb.append(n2);
        }
        this.mStyle = n;
        if (n == 2 || n == 3) {
            this.mTheme = 16973913;
        }
        if (n2 != 0) {
            this.mTheme = n2;
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void setupDialog(@NonNull final Dialog dialog, final int n) {
        if (n != 1 && n != 2) {
            if (n != 3) {
                return;
            }
            final Window window = dialog.getWindow();
            if (window != null) {
                window.addFlags(24);
            }
        }
        dialog.requestWindowFeature(1);
    }
    
    public int show(@NonNull final FragmentTransaction fragmentTransaction, @Nullable final String s) {
        this.mDismissed = false;
        this.mShownByMe = true;
        fragmentTransaction.add(this, s);
        this.mViewDestroyed = false;
        return this.mBackStackId = fragmentTransaction.commit();
    }
    
    public void show(@NonNull final FragmentManager fragmentManager, @Nullable final String s) {
        this.mDismissed = false;
        this.mShownByMe = true;
        final FragmentTransaction beginTransaction = fragmentManager.beginTransaction();
        beginTransaction.setReorderingAllowed(true);
        beginTransaction.add(this, s);
        beginTransaction.commit();
    }
    
    public void showNow(@NonNull final FragmentManager fragmentManager, @Nullable final String s) {
        this.mDismissed = false;
        this.mShownByMe = true;
        final FragmentTransaction beginTransaction = fragmentManager.beginTransaction();
        beginTransaction.setReorderingAllowed(true);
        beginTransaction.add(this, s);
        beginTransaction.commitNow();
    }
}

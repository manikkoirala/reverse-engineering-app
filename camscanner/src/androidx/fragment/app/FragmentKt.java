// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import androidx.lifecycle.LifecycleOwner;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import android.os.Bundle;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class FragmentKt
{
    public static final void clearFragmentResult(@NotNull final Fragment fragment, @NotNull final String s) {
        Intrinsics.checkNotNullParameter((Object)fragment, "<this>");
        Intrinsics.checkNotNullParameter((Object)s, "requestKey");
        fragment.getParentFragmentManager().clearFragmentResult(s);
    }
    
    public static final void clearFragmentResultListener(@NotNull final Fragment fragment, @NotNull final String s) {
        Intrinsics.checkNotNullParameter((Object)fragment, "<this>");
        Intrinsics.checkNotNullParameter((Object)s, "requestKey");
        fragment.getParentFragmentManager().clearFragmentResultListener(s);
    }
    
    public static final void setFragmentResult(@NotNull final Fragment fragment, @NotNull final String s, @NotNull final Bundle bundle) {
        Intrinsics.checkNotNullParameter((Object)fragment, "<this>");
        Intrinsics.checkNotNullParameter((Object)s, "requestKey");
        Intrinsics.checkNotNullParameter((Object)bundle, "result");
        fragment.getParentFragmentManager().setFragmentResult(s, bundle);
    }
    
    public static final void setFragmentResultListener(@NotNull final Fragment fragment, @NotNull final String s, @NotNull final Function2<? super String, ? super Bundle, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)fragment, "<this>");
        Intrinsics.checkNotNullParameter((Object)s, "requestKey");
        Intrinsics.checkNotNullParameter((Object)function2, "listener");
        fragment.getParentFragmentManager().setFragmentResultListener(s, fragment, new Oo08(function2));
    }
    
    private static final void setFragmentResultListener$lambda-0(final Function2 function2, final String s, final Bundle bundle) {
        Intrinsics.checkNotNullParameter((Object)function2, "$tmp0");
        Intrinsics.checkNotNullParameter((Object)s, "p0");
        Intrinsics.checkNotNullParameter((Object)bundle, "p1");
        function2.invoke((Object)s, (Object)bundle);
    }
}

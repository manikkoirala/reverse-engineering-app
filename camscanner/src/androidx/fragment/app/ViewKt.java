// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.view.View;
import kotlin.Metadata;

@Metadata
public final class ViewKt
{
    @NotNull
    public static final <F extends Fragment> F findFragment(@NotNull final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        final Fragment fragment = FragmentManager.findFragment(view);
        Intrinsics.checkNotNullExpressionValue((Object)fragment, "findFragment(this)");
        return (F)fragment;
    }
}

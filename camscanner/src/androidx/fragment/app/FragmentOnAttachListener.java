// 
// Decompiled by Procyon v0.6.0
// 

package androidx.fragment.app;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;

public interface FragmentOnAttachListener
{
    @MainThread
    void onAttachFragment(@NonNull final FragmentManager p0, @NonNull final Fragment p1);
}

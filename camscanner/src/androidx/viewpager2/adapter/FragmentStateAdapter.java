// 
// Decompiled by Procyon v0.6.0
// 

package androidx.viewpager2.adapter;

import android.os.BaseBundle;
import androidx.viewpager2.widget.ViewPager2;
import android.os.Parcelable;
import androidx.fragment.app.FragmentTransaction;
import android.view.View$OnLayoutChangeListener;
import androidx.core.view.ViewCompat;
import androidx.annotation.CallSuper;
import androidx.core.util.Preconditions;
import java.util.Iterator;
import androidx.collection.ArraySet;
import android.view.ViewGroup;
import androidx.annotation.Nullable;
import android.os.Bundle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleEventObserver;
import android.os.Handler;
import android.os.Looper;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.view.View;
import androidx.fragment.app.FragmentActivity;
import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.fragment.app.Fragment;
import androidx.collection.LongSparseArray;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

public abstract class FragmentStateAdapter extends Adapter<FragmentViewHolder> implements StatefulAdapter
{
    private static final long GRACE_WINDOW_TIME_MS = 10000L;
    private static final String KEY_PREFIX_FRAGMENT = "f#";
    private static final String KEY_PREFIX_STATE = "s#";
    final FragmentManager mFragmentManager;
    private FragmentMaxLifecycleEnforcer mFragmentMaxLifecycleEnforcer;
    final LongSparseArray<Fragment> mFragments;
    private boolean mHasStaleFragments;
    boolean mIsInGracePeriod;
    private final LongSparseArray<Integer> mItemIdToViewHolder;
    final Lifecycle mLifecycle;
    private final LongSparseArray<Fragment.SavedState> mSavedStates;
    
    public FragmentStateAdapter(@NonNull final Fragment fragment) {
        this(fragment.getChildFragmentManager(), fragment.getLifecycle());
    }
    
    public FragmentStateAdapter(@NonNull final FragmentActivity fragmentActivity) {
        this(fragmentActivity.getSupportFragmentManager(), fragmentActivity.getLifecycle());
    }
    
    public FragmentStateAdapter(@NonNull final FragmentManager mFragmentManager, @NonNull final Lifecycle mLifecycle) {
        this.mFragments = new LongSparseArray<Fragment>();
        this.mSavedStates = new LongSparseArray<Fragment.SavedState>();
        this.mItemIdToViewHolder = new LongSparseArray<Integer>();
        this.mIsInGracePeriod = false;
        this.mHasStaleFragments = false;
        this.mFragmentManager = mFragmentManager;
        this.mLifecycle = mLifecycle;
        super.setHasStableIds(true);
    }
    
    @NonNull
    private static String createKey(@NonNull final String str, final long lng) {
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(lng);
        return sb.toString();
    }
    
    private void ensureFragment(final int n) {
        final long itemId = this.getItemId(n);
        if (!this.mFragments.containsKey(itemId)) {
            final Fragment fragment = this.createFragment(n);
            fragment.setInitialSavedState(this.mSavedStates.get(itemId));
            this.mFragments.put(itemId, fragment);
        }
    }
    
    private boolean isFragmentViewBound(final long n) {
        final boolean containsKey = this.mItemIdToViewHolder.containsKey(n);
        boolean b = true;
        if (containsKey) {
            return true;
        }
        final Fragment fragment = this.mFragments.get(n);
        if (fragment == null) {
            return false;
        }
        final View view = fragment.getView();
        if (view == null) {
            return false;
        }
        if (view.getParent() == null) {
            b = false;
        }
        return b;
    }
    
    private static boolean isValidKey(@NonNull final String s, @NonNull final String prefix) {
        return s.startsWith(prefix) && s.length() > prefix.length();
    }
    
    private Long itemForViewHolder(final int n) {
        Long n2 = null;
        Long value;
        for (int i = 0; i < this.mItemIdToViewHolder.size(); ++i, n2 = value) {
            value = n2;
            if (this.mItemIdToViewHolder.valueAt(i) == n) {
                if (n2 != null) {
                    throw new IllegalStateException("Design assumption violated: a ViewHolder can only be bound to one item at a time.");
                }
                value = this.mItemIdToViewHolder.keyAt(i);
            }
        }
        return n2;
    }
    
    private static long parseIdFromKey(@NonNull final String s, @NonNull final String s2) {
        return Long.parseLong(s.substring(s2.length()));
    }
    
    private void removeFragment(final long n) {
        final Fragment fragment = this.mFragments.get(n);
        if (fragment == null) {
            return;
        }
        if (fragment.getView() != null) {
            final ViewParent parent = fragment.getView().getParent();
            if (parent != null) {
                ((ViewGroup)parent).removeAllViews();
            }
        }
        if (!this.containsItem(n)) {
            this.mSavedStates.remove(n);
        }
        if (!fragment.isAdded()) {
            this.mFragments.remove(n);
            return;
        }
        if (this.shouldDelayFragmentTransactions()) {
            this.mHasStaleFragments = true;
            return;
        }
        if (fragment.isAdded() && this.containsItem(n)) {
            this.mSavedStates.put(n, this.mFragmentManager.saveFragmentInstanceState(fragment));
        }
        this.mFragmentManager.beginTransaction().remove(fragment).commitNow();
        this.mFragments.remove(n);
    }
    
    private void scheduleGracePeriodEnd() {
        final Handler handler = new Handler(Looper.getMainLooper());
        final Runnable runnable = new Runnable(this) {
            final FragmentStateAdapter this$0;
            
            @Override
            public void run() {
                final FragmentStateAdapter this$0 = this.this$0;
                this$0.mIsInGracePeriod = false;
                this$0.gcFragments();
            }
        };
        this.mLifecycle.addObserver(new LifecycleEventObserver(this, handler, runnable) {
            final FragmentStateAdapter this$0;
            final Handler val$handler;
            final Runnable val$runnable;
            
            @Override
            public void onStateChanged(@NonNull final LifecycleOwner lifecycleOwner, @NonNull final Lifecycle.Event event) {
                if (event == Lifecycle.Event.ON_DESTROY) {
                    this.val$handler.removeCallbacks(this.val$runnable);
                    lifecycleOwner.getLifecycle().removeObserver(this);
                }
            }
        });
        handler.postDelayed((Runnable)runnable, 10000L);
    }
    
    private void scheduleViewAttach(final Fragment fragment, @NonNull final FrameLayout frameLayout) {
        this.mFragmentManager.registerFragmentLifecycleCallbacks((FragmentManager.FragmentLifecycleCallbacks)new FragmentManager.FragmentLifecycleCallbacks(this, fragment, frameLayout) {
            final FragmentStateAdapter this$0;
            final FrameLayout val$container;
            final Fragment val$fragment;
            
            @Override
            public void onFragmentViewCreated(@NonNull final FragmentManager fragmentManager, @NonNull final Fragment fragment, @NonNull final View view, @Nullable final Bundle bundle) {
                if (fragment == this.val$fragment) {
                    fragmentManager.unregisterFragmentLifecycleCallbacks((FragmentManager.FragmentLifecycleCallbacks)this);
                    this.this$0.addViewToContainer(view, this.val$container);
                }
            }
        }, false);
    }
    
    void addViewToContainer(@NonNull final View view, @NonNull final FrameLayout frameLayout) {
        if (((ViewGroup)frameLayout).getChildCount() > 1) {
            throw new IllegalStateException("Design assumption violated.");
        }
        if (view.getParent() == frameLayout) {
            return;
        }
        if (((ViewGroup)frameLayout).getChildCount() > 0) {
            ((ViewGroup)frameLayout).removeAllViews();
        }
        if (view.getParent() != null) {
            ((ViewGroup)view.getParent()).removeView(view);
        }
        ((ViewGroup)frameLayout).addView(view);
    }
    
    public boolean containsItem(final long n) {
        return n >= 0L && n < ((RecyclerView.Adapter)this).getItemCount();
    }
    
    @NonNull
    public abstract Fragment createFragment(final int p0);
    
    void gcFragments() {
        if (this.mHasStaleFragments) {
            if (!this.shouldDelayFragmentTransactions()) {
                final ArraySet set = new ArraySet();
                final int n = 0;
                for (int i = 0; i < this.mFragments.size(); ++i) {
                    final long key = this.mFragments.keyAt(i);
                    if (!this.containsItem(key)) {
                        set.add(key);
                        this.mItemIdToViewHolder.remove(key);
                    }
                }
                if (!this.mIsInGracePeriod) {
                    this.mHasStaleFragments = false;
                    for (int j = n; j < this.mFragments.size(); ++j) {
                        final long key2 = this.mFragments.keyAt(j);
                        if (!this.isFragmentViewBound(key2)) {
                            set.add(key2);
                        }
                    }
                }
                final Iterator iterator = set.iterator();
                while (iterator.hasNext()) {
                    this.removeFragment((long)iterator.next());
                }
            }
        }
    }
    
    @Override
    public long getItemId(final int n) {
        return n;
    }
    
    @CallSuper
    @Override
    public void onAttachedToRecyclerView(@NonNull final RecyclerView recyclerView) {
        Preconditions.checkArgument(this.mFragmentMaxLifecycleEnforcer == null);
        (this.mFragmentMaxLifecycleEnforcer = new FragmentMaxLifecycleEnforcer()).register(recyclerView);
    }
    
    public final void onBindViewHolder(@NonNull final FragmentViewHolder fragmentViewHolder, final int n) {
        final long itemId = ((RecyclerView.ViewHolder)fragmentViewHolder).getItemId();
        final int id = ((View)fragmentViewHolder.getContainer()).getId();
        final Long itemForViewHolder = this.itemForViewHolder(id);
        if (itemForViewHolder != null && itemForViewHolder != itemId) {
            this.removeFragment(itemForViewHolder);
            this.mItemIdToViewHolder.remove(itemForViewHolder);
        }
        this.mItemIdToViewHolder.put(itemId, id);
        this.ensureFragment(n);
        final FrameLayout container = fragmentViewHolder.getContainer();
        if (ViewCompat.isAttachedToWindow((View)container)) {
            if (((View)container).getParent() != null) {
                throw new IllegalStateException("Design assumption violated.");
            }
            ((View)container).addOnLayoutChangeListener((View$OnLayoutChangeListener)new View$OnLayoutChangeListener(this, container, fragmentViewHolder) {
                final FragmentStateAdapter this$0;
                final FrameLayout val$container;
                final FragmentViewHolder val$holder;
                
                public void onLayoutChange(final View view, final int n, final int n2, final int n3, final int n4, final int n5, final int n6, final int n7, final int n8) {
                    if (((View)this.val$container).getParent() != null) {
                        ((View)this.val$container).removeOnLayoutChangeListener((View$OnLayoutChangeListener)this);
                        this.this$0.placeFragmentInViewHolder(this.val$holder);
                    }
                }
            });
        }
        this.gcFragments();
    }
    
    @NonNull
    public final FragmentViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, final int n) {
        return FragmentViewHolder.create(viewGroup);
    }
    
    @CallSuper
    @Override
    public void onDetachedFromRecyclerView(@NonNull final RecyclerView recyclerView) {
        this.mFragmentMaxLifecycleEnforcer.unregister(recyclerView);
        this.mFragmentMaxLifecycleEnforcer = null;
    }
    
    public final boolean onFailedToRecycleView(@NonNull final FragmentViewHolder fragmentViewHolder) {
        return true;
    }
    
    public final void onViewAttachedToWindow(@NonNull final FragmentViewHolder fragmentViewHolder) {
        this.placeFragmentInViewHolder(fragmentViewHolder);
        this.gcFragments();
    }
    
    public final void onViewRecycled(@NonNull final FragmentViewHolder fragmentViewHolder) {
        final Long itemForViewHolder = this.itemForViewHolder(((View)fragmentViewHolder.getContainer()).getId());
        if (itemForViewHolder != null) {
            this.removeFragment(itemForViewHolder);
            this.mItemIdToViewHolder.remove(itemForViewHolder);
        }
    }
    
    void placeFragmentInViewHolder(@NonNull final FragmentViewHolder fragmentViewHolder) {
        final Fragment fragment = this.mFragments.get(((RecyclerView.ViewHolder)fragmentViewHolder).getItemId());
        if (fragment == null) {
            throw new IllegalStateException("Design assumption violated.");
        }
        final FrameLayout container = fragmentViewHolder.getContainer();
        final View view = fragment.getView();
        if (!fragment.isAdded() && view != null) {
            throw new IllegalStateException("Design assumption violated.");
        }
        if (fragment.isAdded() && view == null) {
            this.scheduleViewAttach(fragment, container);
            return;
        }
        if (fragment.isAdded() && view.getParent() != null) {
            if (view.getParent() != container) {
                this.addViewToContainer(view, container);
            }
            return;
        }
        if (fragment.isAdded()) {
            this.addViewToContainer(view, container);
            return;
        }
        if (!this.shouldDelayFragmentTransactions()) {
            this.scheduleViewAttach(fragment, container);
            final FragmentTransaction beginTransaction = this.mFragmentManager.beginTransaction();
            final StringBuilder sb = new StringBuilder();
            sb.append("f");
            sb.append(((RecyclerView.ViewHolder)fragmentViewHolder).getItemId());
            beginTransaction.add(fragment, sb.toString()).setMaxLifecycle(fragment, Lifecycle.State.STARTED).commitNow();
            this.mFragmentMaxLifecycleEnforcer.updateFragmentMaxLifecycle(false);
        }
        else {
            if (this.mFragmentManager.isDestroyed()) {
                return;
            }
            this.mLifecycle.addObserver(new LifecycleEventObserver(this, fragmentViewHolder) {
                final FragmentStateAdapter this$0;
                final FragmentViewHolder val$holder;
                
                @Override
                public void onStateChanged(@NonNull final LifecycleOwner lifecycleOwner, @NonNull final Lifecycle.Event event) {
                    if (this.this$0.shouldDelayFragmentTransactions()) {
                        return;
                    }
                    lifecycleOwner.getLifecycle().removeObserver(this);
                    if (ViewCompat.isAttachedToWindow((View)this.val$holder.getContainer())) {
                        this.this$0.placeFragmentInViewHolder(this.val$holder);
                    }
                }
            });
        }
    }
    
    @Override
    public final void restoreState(@NonNull final Parcelable parcelable) {
        if (this.mSavedStates.isEmpty() && this.mFragments.isEmpty()) {
            final Bundle bundle = (Bundle)parcelable;
            if (bundle.getClassLoader() == null) {
                bundle.setClassLoader(this.getClass().getClassLoader());
            }
            for (final String str : ((BaseBundle)bundle).keySet()) {
                if (isValidKey(str, "f#")) {
                    this.mFragments.put(parseIdFromKey(str, "f#"), this.mFragmentManager.getFragment(bundle, str));
                }
                else {
                    if (!isValidKey(str, "s#")) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Unexpected key in savedState: ");
                        sb.append(str);
                        throw new IllegalArgumentException(sb.toString());
                    }
                    final long idFromKey = parseIdFromKey(str, "s#");
                    final Fragment.SavedState savedState = (Fragment.SavedState)bundle.getParcelable(str);
                    if (!this.containsItem(idFromKey)) {
                        continue;
                    }
                    this.mSavedStates.put(idFromKey, savedState);
                }
            }
            if (!this.mFragments.isEmpty()) {
                this.mHasStaleFragments = true;
                this.mIsInGracePeriod = true;
                this.gcFragments();
                this.scheduleGracePeriodEnd();
            }
            return;
        }
        throw new IllegalStateException("Expected the adapter to be 'fresh' while restoring state.");
    }
    
    @NonNull
    @Override
    public final Parcelable saveState() {
        final Bundle bundle = new Bundle(this.mFragments.size() + this.mSavedStates.size());
        final int n = 0;
        int n2 = 0;
        int i;
        while (true) {
            i = n;
            if (n2 >= this.mFragments.size()) {
                break;
            }
            final long key = this.mFragments.keyAt(n2);
            final Fragment fragment = this.mFragments.get(key);
            if (fragment != null && fragment.isAdded()) {
                this.mFragmentManager.putFragment(bundle, createKey("f#", key), fragment);
            }
            ++n2;
        }
        while (i < this.mSavedStates.size()) {
            final long key2 = this.mSavedStates.keyAt(i);
            if (this.containsItem(key2)) {
                bundle.putParcelable(createKey("s#", key2), (Parcelable)this.mSavedStates.get(key2));
            }
            ++i;
        }
        return (Parcelable)bundle;
    }
    
    @Override
    public final void setHasStableIds(final boolean b) {
        throw new UnsupportedOperationException("Stable Ids are required for the adapter to function properly, and the adapter takes care of setting the flag.");
    }
    
    boolean shouldDelayFragmentTransactions() {
        return this.mFragmentManager.isStateSaved();
    }
    
    private abstract static class DataSetChangeObserver extends AdapterDataObserver
    {
        @Override
        public abstract void onChanged();
        
        @Override
        public final void onItemRangeChanged(final int n, final int n2) {
            this.onChanged();
        }
        
        @Override
        public final void onItemRangeChanged(final int n, final int n2, @Nullable final Object o) {
            this.onChanged();
        }
        
        @Override
        public final void onItemRangeInserted(final int n, final int n2) {
            this.onChanged();
        }
        
        @Override
        public final void onItemRangeMoved(final int n, final int n2, final int n3) {
            this.onChanged();
        }
        
        @Override
        public final void onItemRangeRemoved(final int n, final int n2) {
            this.onChanged();
        }
    }
    
    class FragmentMaxLifecycleEnforcer
    {
        private AdapterDataObserver mDataObserver;
        private LifecycleEventObserver mLifecycleObserver;
        private ViewPager2.OnPageChangeCallback mPageChangeCallback;
        private long mPrimaryItemId;
        private ViewPager2 mViewPager;
        final FragmentStateAdapter this$0;
        
        FragmentMaxLifecycleEnforcer(final FragmentStateAdapter this$0) {
            this.this$0 = this$0;
            this.mPrimaryItemId = -1L;
        }
        
        @NonNull
        private ViewPager2 inferViewPager(@NonNull final RecyclerView recyclerView) {
            final ViewParent parent = ((View)recyclerView).getParent();
            if (parent instanceof ViewPager2) {
                return (ViewPager2)parent;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Expected ViewPager2 instance. Got: ");
            sb.append(parent);
            throw new IllegalStateException(sb.toString());
        }
        
        void register(@NonNull final RecyclerView recyclerView) {
            this.mViewPager = this.inferViewPager(recyclerView);
            final ViewPager2.OnPageChangeCallback mPageChangeCallback = new ViewPager2.OnPageChangeCallback(this) {
                final FragmentMaxLifecycleEnforcer this$1;
                
                @Override
                public void onPageScrollStateChanged(final int n) {
                    this.this$1.updateFragmentMaxLifecycle(false);
                }
                
                @Override
                public void onPageSelected(final int n) {
                    this.this$1.updateFragmentMaxLifecycle(false);
                }
            };
            this.mPageChangeCallback = mPageChangeCallback;
            this.mViewPager.registerOnPageChangeCallback((ViewPager2.OnPageChangeCallback)mPageChangeCallback);
            final DataSetChangeObserver mDataObserver = new DataSetChangeObserver(this) {
                final FragmentMaxLifecycleEnforcer this$1;
                
                @Override
                public void onChanged() {
                    this.this$1.updateFragmentMaxLifecycle(true);
                }
            };
            this.mDataObserver = mDataObserver;
            ((RecyclerView.Adapter)this.this$0).registerAdapterDataObserver(mDataObserver);
            final LifecycleEventObserver mLifecycleObserver = new LifecycleEventObserver(this) {
                final FragmentMaxLifecycleEnforcer this$1;
                
                @Override
                public void onStateChanged(@NonNull final LifecycleOwner lifecycleOwner, @NonNull final Lifecycle.Event event) {
                    this.this$1.updateFragmentMaxLifecycle(false);
                }
            };
            this.mLifecycleObserver = mLifecycleObserver;
            this.this$0.mLifecycle.addObserver(mLifecycleObserver);
        }
        
        void unregister(@NonNull final RecyclerView recyclerView) {
            this.inferViewPager(recyclerView).unregisterOnPageChangeCallback(this.mPageChangeCallback);
            ((RecyclerView.Adapter)this.this$0).unregisterAdapterDataObserver(this.mDataObserver);
            this.this$0.mLifecycle.removeObserver(this.mLifecycleObserver);
            this.mViewPager = null;
        }
        
        void updateFragmentMaxLifecycle(final boolean b) {
            if (this.this$0.shouldDelayFragmentTransactions()) {
                return;
            }
            if (this.mViewPager.getScrollState() != 0) {
                return;
            }
            if (!this.this$0.mFragments.isEmpty()) {
                if (((RecyclerView.Adapter)this.this$0).getItemCount() != 0) {
                    final int currentItem = this.mViewPager.getCurrentItem();
                    if (currentItem >= ((RecyclerView.Adapter)this.this$0).getItemCount()) {
                        return;
                    }
                    final long itemId = this.this$0.getItemId(currentItem);
                    if (itemId == this.mPrimaryItemId && !b) {
                        return;
                    }
                    final Fragment fragment = this.this$0.mFragments.get(itemId);
                    if (fragment != null) {
                        if (fragment.isAdded()) {
                            this.mPrimaryItemId = itemId;
                            final FragmentTransaction beginTransaction = this.this$0.mFragmentManager.beginTransaction();
                            Fragment fragment2 = null;
                            for (int i = 0; i < this.this$0.mFragments.size(); ++i) {
                                final long key = this.this$0.mFragments.keyAt(i);
                                final Fragment fragment3 = this.this$0.mFragments.valueAt(i);
                                if (fragment3.isAdded()) {
                                    if (key != this.mPrimaryItemId) {
                                        beginTransaction.setMaxLifecycle(fragment3, Lifecycle.State.STARTED);
                                    }
                                    else {
                                        fragment2 = fragment3;
                                    }
                                    fragment3.setMenuVisibility(key == this.mPrimaryItemId);
                                }
                            }
                            if (fragment2 != null) {
                                beginTransaction.setMaxLifecycle(fragment2, Lifecycle.State.RESUMED);
                            }
                            if (!beginTransaction.isEmpty()) {
                                beginTransaction.commitNow();
                            }
                        }
                    }
                }
            }
        }
    }
}

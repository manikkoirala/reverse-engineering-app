// 
// Decompiled by Procyon v0.6.0
// 

package androidx.viewpager2.adapter;

import androidx.core.view.ViewCompat;
import android.view.ViewGroup$LayoutParams;
import android.view.ViewGroup;
import android.view.View;
import androidx.annotation.NonNull;
import android.widget.FrameLayout;
import androidx.recyclerview.widget.RecyclerView;

public final class FragmentViewHolder extends ViewHolder
{
    private FragmentViewHolder(@NonNull final FrameLayout frameLayout) {
        super((View)frameLayout);
    }
    
    @NonNull
    static FragmentViewHolder create(@NonNull final ViewGroup viewGroup) {
        final FrameLayout frameLayout = new FrameLayout(((View)viewGroup).getContext());
        ((View)frameLayout).setLayoutParams(new ViewGroup$LayoutParams(-1, -1));
        ((View)frameLayout).setId(ViewCompat.generateViewId());
        ((View)frameLayout).setSaveEnabled(false);
        return new FragmentViewHolder(frameLayout);
    }
    
    @NonNull
    FrameLayout getContainer() {
        return (FrameLayout)super.itemView;
    }
}

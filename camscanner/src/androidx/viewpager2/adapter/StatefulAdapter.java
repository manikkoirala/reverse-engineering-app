// 
// Decompiled by Procyon v0.6.0
// 

package androidx.viewpager2.adapter;

import androidx.annotation.NonNull;
import android.os.Parcelable;

public interface StatefulAdapter
{
    void restoreState(@NonNull final Parcelable p0);
    
    @NonNull
    Parcelable saveState();
}

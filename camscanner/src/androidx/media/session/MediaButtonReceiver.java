// 
// Decompiled by Procyon v0.6.0
// 

package androidx.media.session;

import android.os.RemoteException;
import android.support.v4.media.session.MediaControllerCompat;
import android.content.BroadcastReceiver$PendingResult;
import android.os.Bundle;
import android.support.v4.media.MediaBrowserCompat;
import androidx.core.content.\u3007\u3007888;
import android.os.Build$VERSION;
import android.support.v4.media.session.MediaSessionCompat;
import android.content.pm.ServiceInfo;
import android.content.pm.PackageManager;
import androidx.annotation.RestrictTo;
import android.content.pm.ActivityInfo;
import java.util.List;
import android.content.pm.ResolveInfo;
import android.os.Parcelable;
import android.view.KeyEvent;
import android.content.Intent;
import android.support.v4.media.session.PlaybackStateCompat;
import android.content.ComponentName;
import android.app.PendingIntent;
import android.content.Context;
import android.content.BroadcastReceiver;

public class MediaButtonReceiver extends BroadcastReceiver
{
    private static final String TAG = "MediaButtonReceiver";
    
    public static PendingIntent buildMediaButtonPendingIntent(final Context context, final long n) {
        final ComponentName mediaButtonReceiverComponent = getMediaButtonReceiverComponent(context);
        if (mediaButtonReceiverComponent == null) {
            return null;
        }
        return buildMediaButtonPendingIntent(context, mediaButtonReceiverComponent, n);
    }
    
    public static PendingIntent buildMediaButtonPendingIntent(final Context context, final ComponentName component, final long lng) {
        if (component == null) {
            return null;
        }
        final int keyCode = PlaybackStateCompat.toKeyCode(lng);
        if (keyCode == 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot build a media button pending intent with the given action: ");
            sb.append(lng);
            return null;
        }
        final Intent intent = new Intent("android.intent.action.MEDIA_BUTTON");
        intent.setComponent(component);
        intent.putExtra("android.intent.extra.KEY_EVENT", (Parcelable)new KeyEvent(0, keyCode));
        return PendingIntent.getBroadcast(context, keyCode, intent, 0);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static ComponentName getMediaButtonReceiverComponent(final Context context) {
        final Intent intent = new Intent("android.intent.action.MEDIA_BUTTON");
        intent.setPackage(context.getPackageName());
        final List queryBroadcastReceivers = context.getPackageManager().queryBroadcastReceivers(intent, 0);
        if (queryBroadcastReceivers.size() == 1) {
            final ActivityInfo activityInfo = queryBroadcastReceivers.get(0).activityInfo;
            return new ComponentName(activityInfo.packageName, activityInfo.name);
        }
        queryBroadcastReceivers.size();
        return null;
    }
    
    private static ComponentName getServiceComponentByAction(final Context context, final String str) {
        final PackageManager packageManager = context.getPackageManager();
        final Intent intent = new Intent(str);
        intent.setPackage(context.getPackageName());
        final List queryIntentServices = packageManager.queryIntentServices(intent, 0);
        if (queryIntentServices.size() == 1) {
            final ServiceInfo serviceInfo = queryIntentServices.get(0).serviceInfo;
            return new ComponentName(serviceInfo.packageName, serviceInfo.name);
        }
        if (queryIntentServices.isEmpty()) {
            return null;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected 1 service that handles ");
        sb.append(str);
        sb.append(", found ");
        sb.append(queryIntentServices.size());
        throw new IllegalStateException(sb.toString());
    }
    
    public static KeyEvent handleIntent(final MediaSessionCompat mediaSessionCompat, final Intent intent) {
        if (mediaSessionCompat != null && intent != null && "android.intent.action.MEDIA_BUTTON".equals(intent.getAction()) && intent.hasExtra("android.intent.extra.KEY_EVENT")) {
            final KeyEvent keyEvent = (KeyEvent)intent.getParcelableExtra("android.intent.extra.KEY_EVENT");
            mediaSessionCompat.getController().dispatchMediaButtonEvent(keyEvent);
            return keyEvent;
        }
        return null;
    }
    
    private static void startForegroundService(final Context context, final Intent intent) {
        if (Build$VERSION.SDK_INT >= 26) {
            \u3007\u3007888.\u3007080(context, intent);
        }
        else {
            context.startService(intent);
        }
    }
    
    public void onReceive(Context applicationContext, final Intent obj) {
        if (obj == null || !"android.intent.action.MEDIA_BUTTON".equals(obj.getAction()) || !obj.hasExtra("android.intent.extra.KEY_EVENT")) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Ignore unsupported intent: ");
            sb.append(obj);
            return;
        }
        final ComponentName serviceComponentByAction = getServiceComponentByAction(applicationContext, "android.intent.action.MEDIA_BUTTON");
        if (serviceComponentByAction != null) {
            obj.setComponent(serviceComponentByAction);
            startForegroundService(applicationContext, obj);
            return;
        }
        final ComponentName serviceComponentByAction2 = getServiceComponentByAction(applicationContext, "android.media.browse.MediaBrowserService");
        if (serviceComponentByAction2 != null) {
            final BroadcastReceiver$PendingResult goAsync = this.goAsync();
            applicationContext = applicationContext.getApplicationContext();
            final MediaButtonConnectionCallback mediaButtonConnectionCallback = new MediaButtonConnectionCallback(applicationContext, obj, goAsync);
            final MediaBrowserCompat mediaBrowser = new MediaBrowserCompat(applicationContext, serviceComponentByAction2, (MediaBrowserCompat.ConnectionCallback)mediaButtonConnectionCallback, null);
            mediaButtonConnectionCallback.setMediaBrowser(mediaBrowser);
            mediaBrowser.connect();
            return;
        }
        throw new IllegalStateException("Could not find any Service that handles android.intent.action.MEDIA_BUTTON or implements a media browser service.");
    }
    
    private static class MediaButtonConnectionCallback extends ConnectionCallback
    {
        private final Context mContext;
        private final Intent mIntent;
        private MediaBrowserCompat mMediaBrowser;
        private final BroadcastReceiver$PendingResult mPendingResult;
        
        MediaButtonConnectionCallback(final Context mContext, final Intent mIntent, final BroadcastReceiver$PendingResult mPendingResult) {
            this.mContext = mContext;
            this.mIntent = mIntent;
            this.mPendingResult = mPendingResult;
        }
        
        private void finish() {
            this.mMediaBrowser.disconnect();
            this.mPendingResult.finish();
        }
        
        @Override
        public void onConnected() {
            while (true) {
                try {
                    new MediaControllerCompat(this.mContext, this.mMediaBrowser.getSessionToken()).dispatchMediaButtonEvent((KeyEvent)this.mIntent.getParcelableExtra("android.intent.extra.KEY_EVENT"));
                    this.finish();
                }
                catch (final RemoteException ex) {
                    continue;
                }
                break;
            }
        }
        
        @Override
        public void onConnectionFailed() {
            this.finish();
        }
        
        @Override
        public void onConnectionSuspended() {
            this.finish();
        }
        
        void setMediaBrowser(final MediaBrowserCompat mMediaBrowser) {
            this.mMediaBrowser = mMediaBrowser;
        }
    }
}

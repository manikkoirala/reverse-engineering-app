// 
// Decompiled by Procyon v0.6.0
// 

package androidx.media.app;

import androidx.annotation.RequiresApi;
import android.media.session.MediaSession$Token;
import android.os.Parcelable;
import android.os.Bundle;
import android.app.Notification;
import android.support.v4.media.session.MediaSessionCompat;
import android.app.PendingIntent;
import androidx.annotation.RestrictTo;
import android.app.Notification$Style;
import android.app.Notification$MediaStyle;
import android.app.Notification$DecoratedMediaCustomViewStyle;
import android.os.Build$VERSION;
import androidx.core.app.NotificationBuilderWithBuilderAccessor;
import androidx.media.R;
import android.widget.RemoteViews;

public class NotificationCompat
{
    private NotificationCompat() {
    }
    
    public static class DecoratedMediaCustomViewStyle extends MediaStyle
    {
        private void setBackgroundColor(final RemoteViews remoteViews) {
            int n;
            if (super.mBuilder.getColor() != 0) {
                n = super.mBuilder.getColor();
            }
            else {
                n = super.mBuilder.mContext.getResources().getColor(R.color.notification_material_background_media_default_color);
            }
            remoteViews.setInt(R.id.status_bar_latest_event_content, "setBackgroundColor", n);
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        @Override
        public void apply(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            if (Build$VERSION.SDK_INT >= 24) {
                notificationBuilderWithBuilderAccessor.getBuilder().setStyle((Notification$Style)((MediaStyle)this).fillInMediaStyle((Notification$MediaStyle)new Notification$DecoratedMediaCustomViewStyle()));
            }
            else {
                super.apply(notificationBuilderWithBuilderAccessor);
            }
        }
        
        @Override
        int getBigContentViewLayoutResource(int n) {
            if (n <= 3) {
                n = R.layout.notification_template_big_media_narrow_custom;
            }
            else {
                n = R.layout.notification_template_big_media_custom;
            }
            return n;
        }
        
        @Override
        int getContentViewLayoutResource() {
            int n;
            if (super.mBuilder.getContentView() != null) {
                n = R.layout.notification_template_media_custom;
            }
            else {
                n = super.getContentViewLayoutResource();
            }
            return n;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        @Override
        public RemoteViews makeBigContentView(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            if (Build$VERSION.SDK_INT >= 24) {
                return null;
            }
            RemoteViews remoteViews;
            if (super.mBuilder.getBigContentView() != null) {
                remoteViews = super.mBuilder.getBigContentView();
            }
            else {
                remoteViews = super.mBuilder.getContentView();
            }
            if (remoteViews == null) {
                return null;
            }
            final RemoteViews generateBigContentView = ((MediaStyle)this).generateBigContentView();
            ((Style)this).buildIntoRemoteViews(generateBigContentView, remoteViews);
            this.setBackgroundColor(generateBigContentView);
            return generateBigContentView;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        @Override
        public RemoteViews makeContentView(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            if (Build$VERSION.SDK_INT >= 24) {
                return null;
            }
            final RemoteViews contentView = super.mBuilder.getContentView();
            final boolean b = true;
            final boolean b2 = contentView != null;
            int n = b ? 1 : 0;
            if (!b2) {
                if (super.mBuilder.getBigContentView() != null) {
                    n = (b ? 1 : 0);
                }
                else {
                    n = 0;
                }
            }
            if (n != 0) {
                final RemoteViews generateContentView = ((MediaStyle)this).generateContentView();
                if (b2) {
                    ((Style)this).buildIntoRemoteViews(generateContentView, super.mBuilder.getContentView());
                }
                this.setBackgroundColor(generateContentView);
                return generateContentView;
            }
            return null;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        @Override
        public RemoteViews makeHeadsUpContentView(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            if (Build$VERSION.SDK_INT >= 24) {
                return null;
            }
            RemoteViews remoteViews;
            if (super.mBuilder.getHeadsUpContentView() != null) {
                remoteViews = super.mBuilder.getHeadsUpContentView();
            }
            else {
                remoteViews = super.mBuilder.getContentView();
            }
            if (remoteViews == null) {
                return null;
            }
            final RemoteViews generateBigContentView = ((MediaStyle)this).generateBigContentView();
            ((Style)this).buildIntoRemoteViews(generateBigContentView, remoteViews);
            this.setBackgroundColor(generateBigContentView);
            return generateBigContentView;
        }
    }
    
    public static class MediaStyle extends Style
    {
        private static final int MAX_MEDIA_BUTTONS = 5;
        private static final int MAX_MEDIA_BUTTONS_IN_COMPACT = 3;
        int[] mActionsToShowInCompact;
        PendingIntent mCancelButtonIntent;
        boolean mShowCancelButton;
        MediaSessionCompat.Token mToken;
        
        public MediaStyle() {
            this.mActionsToShowInCompact = null;
        }
        
        public MediaStyle(final NotificationCompat.Builder builder) {
            this.mActionsToShowInCompact = null;
            ((Style)this).setBuilder(builder);
        }
        
        private RemoteViews generateMediaActionButton(final Action action) {
            final boolean b = action.getActionIntent() == null;
            final RemoteViews remoteViews = new RemoteViews(super.mBuilder.mContext.getPackageName(), R.layout.notification_media_action);
            final int action2 = R.id.action0;
            remoteViews.setImageViewResource(action2, action.getIcon());
            if (!b) {
                remoteViews.setOnClickPendingIntent(action2, action.getActionIntent());
            }
            remoteViews.setContentDescription(action2, action.getTitle());
            return remoteViews;
        }
        
        public static MediaSessionCompat.Token getMediaSession(final Notification notification) {
            final Bundle extras = NotificationCompat.getExtras(notification);
            if (extras != null) {
                final Parcelable parcelable = extras.getParcelable("android.mediaSession");
                if (parcelable != null) {
                    return MediaSessionCompat.Token.fromToken(parcelable);
                }
            }
            return null;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        @Override
        public void apply(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            notificationBuilderWithBuilderAccessor.getBuilder().setStyle((Notification$Style)this.fillInMediaStyle(new Notification$MediaStyle()));
        }
        
        @RequiresApi(21)
        Notification$MediaStyle fillInMediaStyle(final Notification$MediaStyle notification$MediaStyle) {
            final int[] mActionsToShowInCompact = this.mActionsToShowInCompact;
            if (mActionsToShowInCompact != null) {
                notification$MediaStyle.setShowActionsInCompactView(mActionsToShowInCompact);
            }
            final MediaSessionCompat.Token mToken = this.mToken;
            if (mToken != null) {
                notification$MediaStyle.setMediaSession((MediaSession$Token)mToken.getToken());
            }
            return notification$MediaStyle;
        }
        
        RemoteViews generateBigContentView() {
            final int min = Math.min(super.mBuilder.mActions.size(), 5);
            final RemoteViews applyStandardTemplate = ((Style)this).applyStandardTemplate(false, this.getBigContentViewLayoutResource(min), false);
            applyStandardTemplate.removeAllViews(R.id.media_actions);
            if (min > 0) {
                for (int i = 0; i < min; ++i) {
                    applyStandardTemplate.addView(R.id.media_actions, this.generateMediaActionButton(super.mBuilder.mActions.get(i)));
                }
            }
            if (this.mShowCancelButton) {
                final int cancel_action = R.id.cancel_action;
                applyStandardTemplate.setViewVisibility(cancel_action, 0);
                applyStandardTemplate.setInt(cancel_action, "setAlpha", super.mBuilder.mContext.getResources().getInteger(R.integer.cancel_button_image_alpha));
                applyStandardTemplate.setOnClickPendingIntent(cancel_action, this.mCancelButtonIntent);
            }
            else {
                applyStandardTemplate.setViewVisibility(R.id.cancel_action, 8);
            }
            return applyStandardTemplate;
        }
        
        RemoteViews generateContentView() {
            final RemoteViews applyStandardTemplate = ((Style)this).applyStandardTemplate(false, this.getContentViewLayoutResource(), true);
            final int size = super.mBuilder.mActions.size();
            final int[] mActionsToShowInCompact = this.mActionsToShowInCompact;
            int min;
            if (mActionsToShowInCompact == null) {
                min = 0;
            }
            else {
                min = Math.min(mActionsToShowInCompact.length, 3);
            }
            applyStandardTemplate.removeAllViews(R.id.media_actions);
            if (min > 0) {
                for (int i = 0; i < min; ++i) {
                    if (i >= size) {
                        throw new IllegalArgumentException(String.format("setShowActionsInCompactView: action %d out of bounds (max %d)", i, size - 1));
                    }
                    applyStandardTemplate.addView(R.id.media_actions, this.generateMediaActionButton(super.mBuilder.mActions.get(this.mActionsToShowInCompact[i])));
                }
            }
            if (this.mShowCancelButton) {
                applyStandardTemplate.setViewVisibility(R.id.end_padder, 8);
                final int cancel_action = R.id.cancel_action;
                applyStandardTemplate.setViewVisibility(cancel_action, 0);
                applyStandardTemplate.setOnClickPendingIntent(cancel_action, this.mCancelButtonIntent);
                applyStandardTemplate.setInt(cancel_action, "setAlpha", super.mBuilder.mContext.getResources().getInteger(R.integer.cancel_button_image_alpha));
            }
            else {
                applyStandardTemplate.setViewVisibility(R.id.end_padder, 0);
                applyStandardTemplate.setViewVisibility(R.id.cancel_action, 8);
            }
            return applyStandardTemplate;
        }
        
        int getBigContentViewLayoutResource(int n) {
            if (n <= 3) {
                n = R.layout.notification_template_big_media_narrow;
            }
            else {
                n = R.layout.notification_template_big_media;
            }
            return n;
        }
        
        int getContentViewLayoutResource() {
            return R.layout.notification_template_media;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        @Override
        public RemoteViews makeBigContentView(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            return null;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        @Override
        public RemoteViews makeContentView(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            return null;
        }
        
        public MediaStyle setCancelButtonIntent(final PendingIntent mCancelButtonIntent) {
            this.mCancelButtonIntent = mCancelButtonIntent;
            return this;
        }
        
        public MediaStyle setMediaSession(final MediaSessionCompat.Token mToken) {
            this.mToken = mToken;
            return this;
        }
        
        public MediaStyle setShowActionsInCompactView(final int... mActionsToShowInCompact) {
            this.mActionsToShowInCompact = mActionsToShowInCompact;
            return this;
        }
        
        public MediaStyle setShowCancelButton(final boolean b) {
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.media;

import android.os.BaseBundle;
import android.os.Bundle;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class MediaBrowserCompatUtils
{
    private MediaBrowserCompatUtils() {
    }
    
    public static boolean areSameOptions(final Bundle bundle, final Bundle bundle2) {
        final boolean b = true;
        final boolean b2 = true;
        boolean b3 = true;
        if (bundle == bundle2) {
            return true;
        }
        if (bundle == null) {
            if (((BaseBundle)bundle2).getInt("android.media.browse.extra.PAGE", -1) != -1 || ((BaseBundle)bundle2).getInt("android.media.browse.extra.PAGE_SIZE", -1) != -1) {
                b3 = false;
            }
            return b3;
        }
        if (bundle2 == null) {
            return ((BaseBundle)bundle).getInt("android.media.browse.extra.PAGE", -1) == -1 && ((BaseBundle)bundle).getInt("android.media.browse.extra.PAGE_SIZE", -1) == -1 && b;
        }
        return ((BaseBundle)bundle).getInt("android.media.browse.extra.PAGE", -1) == ((BaseBundle)bundle2).getInt("android.media.browse.extra.PAGE", -1) && ((BaseBundle)bundle).getInt("android.media.browse.extra.PAGE_SIZE", -1) == ((BaseBundle)bundle2).getInt("android.media.browse.extra.PAGE_SIZE", -1) && b2;
    }
    
    public static boolean hasDuplicatedItems(final Bundle bundle, final Bundle bundle2) {
        int int1;
        if (bundle == null) {
            int1 = -1;
        }
        else {
            int1 = ((BaseBundle)bundle).getInt("android.media.browse.extra.PAGE", -1);
        }
        int int2;
        if (bundle2 == null) {
            int2 = -1;
        }
        else {
            int2 = ((BaseBundle)bundle2).getInt("android.media.browse.extra.PAGE", -1);
        }
        int int3;
        if (bundle == null) {
            int3 = -1;
        }
        else {
            int3 = ((BaseBundle)bundle).getInt("android.media.browse.extra.PAGE_SIZE", -1);
        }
        int int4;
        if (bundle2 == null) {
            int4 = -1;
        }
        else {
            int4 = ((BaseBundle)bundle2).getInt("android.media.browse.extra.PAGE_SIZE", -1);
        }
        final int n = Integer.MAX_VALUE;
        boolean b = true;
        int n2;
        int n3;
        if (int1 != -1 && int3 != -1) {
            n2 = int1 * int3;
            n3 = int3 + n2 - 1;
        }
        else {
            n3 = Integer.MAX_VALUE;
            n2 = 0;
        }
        int n5;
        int n6;
        if (int2 != -1 && int4 != -1) {
            final int n4 = int2 * int4;
            n5 = int4 + n4 - 1;
            n6 = n4;
        }
        else {
            n6 = 0;
            n5 = n;
        }
        if (n3 < n6 || n5 < n2) {
            b = false;
        }
        return b;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.multidex;

import java.util.ListIterator;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.zip.ZipFile;
import java.lang.reflect.Constructor;
import dalvik.system.DexFile;
import java.util.StringTokenizer;
import java.util.List;
import dalvik.system.BaseDexClassLoader;
import android.content.pm.ApplicationInfo;
import java.util.Arrays;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.io.IOException;
import android.content.Context;
import java.lang.reflect.Method;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.io.File;
import java.util.Set;

public final class MultiDex
{
    private static final String CODE_CACHE_NAME = "code_cache";
    private static final String CODE_CACHE_SECONDARY_FOLDER_NAME = "secondary-dexes";
    private static final boolean IS_VM_MULTIDEX_CAPABLE;
    private static final int MAX_SUPPORTED_SDK_VERSION = 20;
    private static final int MIN_SDK_VERSION = 4;
    private static final String NO_KEY_PREFIX = "";
    private static final String OLD_SECONDARY_FOLDER_NAME = "secondary-dexes";
    static final String TAG = "MultiDex";
    private static final int VM_WITH_MULTIDEX_VERSION_MAJOR = 2;
    private static final int VM_WITH_MULTIDEX_VERSION_MINOR = 1;
    private static final Set<File> installedApk;
    
    static {
        installedApk = new HashSet<File>();
        IS_VM_MULTIDEX_CAPABLE = isVMMultidexCapable(System.getProperty("java.vm.version"));
    }
    
    private MultiDex() {
    }
    
    private static void clearOldDexDir(final Context context) throws Exception {
        final File file = new File(context.getFilesDir(), "secondary-dexes");
        if (file.isDirectory()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Clearing old secondary dex dir (");
            sb.append(file.getPath());
            sb.append(").");
            final File[] listFiles = file.listFiles();
            if (listFiles == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Failed to list secondary dex dir content (");
                sb2.append(file.getPath());
                sb2.append(").");
                return;
            }
            for (final File file2 : listFiles) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Trying to delete old file ");
                sb3.append(file2.getPath());
                sb3.append(" of size ");
                sb3.append(file2.length());
                if (!file2.delete()) {
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("Failed to delete old file ");
                    sb4.append(file2.getPath());
                }
                else {
                    final StringBuilder sb5 = new StringBuilder();
                    sb5.append("Deleted old file ");
                    sb5.append(file2.getPath());
                }
            }
            if (!file.delete()) {
                final StringBuilder sb6 = new StringBuilder();
                sb6.append("Failed to delete secondary dex dir ");
                sb6.append(file.getPath());
            }
            else {
                final StringBuilder sb7 = new StringBuilder();
                sb7.append("Deleted old secondary dex dir ");
                sb7.append(file.getPath());
            }
        }
    }
    
    private static void doInstallation(final Context p0, final File p1, final File p2, final String p3, final String p4, final boolean p5) throws IOException, IllegalArgumentException, IllegalAccessException, NoSuchFieldException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException, InstantiationException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: astore          7
        //     5: aload           7
        //     7: monitorenter   
        //     8: aload           7
        //    10: aload_1        
        //    11: invokeinterface java/util/Set.contains:(Ljava/lang/Object;)Z
        //    16: ifeq            23
        //    19: aload           7
        //    21: monitorexit    
        //    22: return         
        //    23: aload           7
        //    25: aload_1        
        //    26: invokeinterface java/util/Set.add:(Ljava/lang/Object;)Z
        //    31: pop            
        //    32: getstatic       android/os/Build$VERSION.SDK_INT:I
        //    35: istore          6
        //    37: new             Ljava/lang/StringBuilder;
        //    40: astore          8
        //    42: aload           8
        //    44: invokespecial   java/lang/StringBuilder.<init>:()V
        //    47: aload           8
        //    49: ldc             "MultiDex is not guaranteed to work in SDK version "
        //    51: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    54: pop            
        //    55: aload           8
        //    57: iload           6
        //    59: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //    62: pop            
        //    63: aload           8
        //    65: ldc             ": SDK version higher than "
        //    67: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    70: pop            
        //    71: aload           8
        //    73: bipush          20
        //    75: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //    78: pop            
        //    79: aload           8
        //    81: ldc             " should be backed by "
        //    83: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    86: pop            
        //    87: aload           8
        //    89: ldc             "runtime with built-in multidex capabilty but it's not the "
        //    91: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    94: pop            
        //    95: aload           8
        //    97: ldc             "case here: java.vm.version=\""
        //    99: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   102: pop            
        //   103: aload           8
        //   105: ldc             "java.vm.version"
        //   107: invokestatic    java/lang/System.getProperty:(Ljava/lang/String;)Ljava/lang/String;
        //   110: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   113: pop            
        //   114: aload           8
        //   116: ldc             "\""
        //   118: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   121: pop            
        //   122: aload_0        
        //   123: invokestatic    androidx/multidex/MultiDex.getDexClassloader:(Landroid/content/Context;)Ljava/lang/ClassLoader;
        //   126: astore          8
        //   128: aload           8
        //   130: ifnonnull       137
        //   133: aload           7
        //   135: monitorexit    
        //   136: return         
        //   137: aload_0        
        //   138: invokestatic    androidx/multidex/MultiDex.clearOldDexDir:(Landroid/content/Context;)V
        //   141: aload_0        
        //   142: aload_2        
        //   143: aload_3        
        //   144: invokestatic    androidx/multidex/MultiDex.getDexDir:(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
        //   147: astore_3       
        //   148: new             Landroidx/multidex/MultiDexExtractor;
        //   151: astore_2       
        //   152: aload_2        
        //   153: aload_1        
        //   154: aload_3        
        //   155: invokespecial   androidx/multidex/MultiDexExtractor.<init>:(Ljava/io/File;Ljava/io/File;)V
        //   158: aload_2        
        //   159: aload_0        
        //   160: aload           4
        //   162: iconst_0       
        //   163: invokevirtual   androidx/multidex/MultiDexExtractor.load:(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;
        //   166: astore_1       
        //   167: aload           8
        //   169: aload_3        
        //   170: aload_1        
        //   171: invokestatic    androidx/multidex/MultiDex.installSecondaryDexes:(Ljava/lang/ClassLoader;Ljava/io/File;Ljava/util/List;)V
        //   174: goto            197
        //   177: astore_1       
        //   178: iload           5
        //   180: ifeq            217
        //   183: aload           8
        //   185: aload_3        
        //   186: aload_2        
        //   187: aload_0        
        //   188: aload           4
        //   190: iconst_1       
        //   191: invokevirtual   androidx/multidex/MultiDexExtractor.load:(Landroid/content/Context;Ljava/lang/String;Z)Ljava/util/List;
        //   194: invokestatic    androidx/multidex/MultiDex.installSecondaryDexes:(Ljava/lang/ClassLoader;Ljava/io/File;Ljava/util/List;)V
        //   197: aload_2        
        //   198: invokevirtual   androidx/multidex/MultiDexExtractor.close:()V
        //   201: aconst_null    
        //   202: astore_0       
        //   203: goto            207
        //   206: astore_0       
        //   207: aload_0        
        //   208: ifnonnull       215
        //   211: aload           7
        //   213: monitorexit    
        //   214: return         
        //   215: aload_0        
        //   216: athrow         
        //   217: aload_1        
        //   218: athrow         
        //   219: astore_0       
        //   220: aload_2        
        //   221: invokevirtual   androidx/multidex/MultiDexExtractor.close:()V
        //   224: aload_0        
        //   225: athrow         
        //   226: astore_0       
        //   227: aload           7
        //   229: monitorexit    
        //   230: aload_0        
        //   231: athrow         
        //   232: astore          9
        //   234: goto            141
        //   237: astore_1       
        //   238: goto            224
        //    Exceptions:
        //  throws java.io.IOException
        //  throws java.lang.IllegalArgumentException
        //  throws java.lang.IllegalAccessException
        //  throws java.lang.NoSuchFieldException
        //  throws java.lang.reflect.InvocationTargetException
        //  throws java.lang.NoSuchMethodException
        //  throws java.lang.SecurityException
        //  throws java.lang.ClassNotFoundException
        //  throws java.lang.InstantiationException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  8      22     226    232    Any
        //  23     128    226    232    Any
        //  133    136    226    232    Any
        //  137    141    232    237    Any
        //  141    158    226    232    Any
        //  158    167    219    226    Any
        //  167    174    177    197    Ljava/io/IOException;
        //  167    174    219    226    Any
        //  183    197    219    226    Any
        //  197    201    206    207    Ljava/io/IOException;
        //  197    201    226    232    Any
        //  211    214    226    232    Any
        //  215    217    226    232    Any
        //  217    219    219    226    Any
        //  220    224    237    241    Ljava/io/IOException;
        //  220    224    226    232    Any
        //  224    226    226    232    Any
        //  227    230    226    232    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 130 out of bounds for length 130
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static void expandFieldArray(final Object o, final String s, final Object[] array) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        final Field field = findField(o, s);
        final Object[] array2 = (Object[])field.get(o);
        final Object[] value = (Object[])Array.newInstance(array2.getClass().getComponentType(), array2.length + array.length);
        System.arraycopy(array2, 0, value, 0, array2.length);
        System.arraycopy(array, 0, value, array2.length, array.length);
        field.set(o, value);
    }
    
    private static Field findField(final Object o, final String s) throws NoSuchFieldException {
        Class<?> clazz = o.getClass();
        while (clazz != null) {
            try {
                final Field declaredField = clazz.getDeclaredField(s);
                if (!declaredField.isAccessible()) {
                    declaredField.setAccessible(true);
                }
                return declaredField;
            }
            catch (final NoSuchFieldException ex) {
                clazz = clazz.getSuperclass();
                continue;
            }
            break;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Field ");
        sb.append(s);
        sb.append(" not found in ");
        sb.append(o.getClass());
        throw new NoSuchFieldException(sb.toString());
    }
    
    private static Method findMethod(final Object o, final String s, final Class<?>... array) throws NoSuchMethodException {
        Class<?> clazz = o.getClass();
        while (clazz != null) {
            try {
                final Method declaredMethod = clazz.getDeclaredMethod(s, array);
                if (!declaredMethod.isAccessible()) {
                    declaredMethod.setAccessible(true);
                }
                return declaredMethod;
            }
            catch (final NoSuchMethodException ex) {
                clazz = clazz.getSuperclass();
                continue;
            }
            break;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Method ");
        sb.append(s);
        sb.append(" with parameters ");
        sb.append(Arrays.asList(array));
        sb.append(" not found in ");
        sb.append(o.getClass());
        throw new NoSuchMethodException(sb.toString());
    }
    
    private static ApplicationInfo getApplicationInfo(final Context context) {
        try {
            return context.getApplicationInfo();
        }
        catch (final RuntimeException ex) {
            return null;
        }
    }
    
    private static ClassLoader getDexClassloader(final Context context) {
        try {
            final ClassLoader classLoader = context.getClassLoader();
            if (classLoader instanceof BaseDexClassLoader) {
                return classLoader;
            }
            return null;
        }
        catch (final RuntimeException ex) {
            return null;
        }
    }
    
    private static File getDexDir(Context parent, File parent2, final String child) throws IOException {
        parent2 = new File(parent2, "code_cache");
        try {
            mkdirChecked(parent2);
            parent = (Context)parent2;
        }
        catch (final IOException ex) {
            parent = (Context)new File(parent.getFilesDir(), "code_cache");
            mkdirChecked((File)parent);
        }
        final File file = new File((File)parent, child);
        mkdirChecked(file);
        return file;
    }
    
    public static void install(final Context context) {
        if (MultiDex.IS_VM_MULTIDEX_CAPABLE) {
            return;
        }
        try {
            final ApplicationInfo applicationInfo = getApplicationInfo(context);
            if (applicationInfo == null) {
                return;
            }
            doInstallation(context, new File(applicationInfo.sourceDir), new File(applicationInfo.dataDir), "secondary-dexes", "", true);
        }
        catch (final Exception ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("MultiDex installation failed (");
            sb.append(ex.getMessage());
            sb.append(").");
            throw new RuntimeException(sb.toString());
        }
    }
    
    public static void installInstrumentation(final Context context, final Context context2) {
        if (MultiDex.IS_VM_MULTIDEX_CAPABLE) {
            return;
        }
        try {
            final ApplicationInfo applicationInfo = getApplicationInfo(context);
            if (applicationInfo == null) {
                return;
            }
            final ApplicationInfo applicationInfo2 = getApplicationInfo(context2);
            if (applicationInfo2 == null) {
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append(context.getPackageName());
            sb.append(".");
            final String string = sb.toString();
            final File file = new File(applicationInfo2.dataDir);
            final File file2 = new File(applicationInfo.sourceDir);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(string);
            sb2.append("secondary-dexes");
            doInstallation(context2, file2, file, sb2.toString(), string, false);
            doInstallation(context2, new File(applicationInfo2.sourceDir), file, "secondary-dexes", "", false);
        }
        catch (final Exception ex) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("MultiDex installation failed (");
            sb3.append(ex.getMessage());
            sb3.append(").");
            throw new RuntimeException(sb3.toString());
        }
    }
    
    private static void installSecondaryDexes(final ClassLoader classLoader, final File file, final List<? extends File> list) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, InvocationTargetException, NoSuchMethodException, IOException, SecurityException, ClassNotFoundException, InstantiationException {
        if (!list.isEmpty()) {
            V19.install(classLoader, list, file);
        }
    }
    
    static boolean isVMMultidexCapable(String str) {
        boolean b2;
        final boolean b = b2 = false;
        Label_0123: {
            if (str != null) {
                final StringTokenizer stringTokenizer = new StringTokenizer(str, ".");
                final boolean hasMoreTokens = stringTokenizer.hasMoreTokens();
                String nextToken = null;
                String nextToken2;
                if (hasMoreTokens) {
                    nextToken2 = stringTokenizer.nextToken();
                }
                else {
                    nextToken2 = null;
                }
                if (stringTokenizer.hasMoreTokens()) {
                    nextToken = stringTokenizer.nextToken();
                }
                b2 = b;
                if (nextToken2 != null) {
                    b2 = b;
                    if (nextToken != null) {
                        try {
                            final int int1 = Integer.parseInt(nextToken2);
                            final int int2 = Integer.parseInt(nextToken);
                            if (int1 <= 2) {
                                b2 = b;
                                if (int1 != 2) {
                                    break Label_0123;
                                }
                                b2 = b;
                                if (int2 < 1) {
                                    break Label_0123;
                                }
                            }
                            b2 = true;
                        }
                        catch (final NumberFormatException ex) {
                            b2 = b;
                        }
                    }
                }
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("VM with version ");
        sb.append(str);
        if (b2) {
            str = " has multidex support";
        }
        else {
            str = " does not have multidex support";
        }
        sb.append(str);
        return b2;
    }
    
    private static void mkdirChecked(final File file) throws IOException {
        file.mkdir();
        if (!file.isDirectory()) {
            final File parentFile = file.getParentFile();
            if (parentFile == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to create dir ");
                sb.append(file.getPath());
                sb.append(". Parent file is null.");
            }
            else {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Failed to create dir ");
                sb2.append(file.getPath());
                sb2.append(". parent file is a dir ");
                sb2.append(parentFile.isDirectory());
                sb2.append(", a file ");
                sb2.append(parentFile.isFile());
                sb2.append(", exists ");
                sb2.append(parentFile.exists());
                sb2.append(", readable ");
                sb2.append(parentFile.canRead());
                sb2.append(", writable ");
                sb2.append(parentFile.canWrite());
            }
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Failed to create directory ");
            sb3.append(file.getPath());
            throw new IOException(sb3.toString());
        }
    }
    
    private static final class V14
    {
        private static final int EXTRACTED_SUFFIX_LENGTH = 4;
        private final ElementConstructor elementConstructor;
        
        private V14() throws ClassNotFoundException, SecurityException, NoSuchMethodException {
            final Class<?> forName = Class.forName("dalvik.system.DexPathList$Element");
            ElementConstructor elementConstructor;
            try {
                elementConstructor = new ICSElementConstructor(forName);
            }
            catch (final NoSuchMethodException ex) {
                try {
                    elementConstructor = new JBMR11ElementConstructor(forName);
                }
                catch (final NoSuchMethodException ex2) {
                    elementConstructor = new JBMR2ElementConstructor(forName);
                }
            }
            this.elementConstructor = elementConstructor;
        }
        
        static void install(ClassLoader value, List<? extends File> dexElements) throws IOException, SecurityException, IllegalArgumentException, ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchFieldException {
            value = (ClassLoader)findField(value, "pathList").get(value);
            dexElements = new V14().makeDexElements((List<? extends File>)dexElements);
            try {
                expandFieldArray(value, "dexElements", dexElements);
            }
            catch (final NoSuchFieldException ex) {
                expandFieldArray(value, "pathElements", dexElements);
            }
        }
        
        private Object[] makeDexElements(final List<? extends File> list) throws IOException, SecurityException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
            final int size = list.size();
            final Object[] array = new Object[size];
            for (int i = 0; i < size; ++i) {
                final File file = list.get(i);
                array[i] = this.elementConstructor.newInstance(file, DexFile.loadDex(file.getPath(), optimizedPathFor(file), 0));
            }
            return array;
        }
        
        private static String optimizedPathFor(final File file) {
            final File parentFile = file.getParentFile();
            final String name = file.getName();
            final StringBuilder sb = new StringBuilder();
            sb.append(name.substring(0, name.length() - V14.EXTRACTED_SUFFIX_LENGTH));
            sb.append(".dex");
            return new File(parentFile, sb.toString()).getPath();
        }
        
        private interface ElementConstructor
        {
            Object newInstance(final File p0, final DexFile p1) throws IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException, IOException;
        }
        
        private static class ICSElementConstructor implements ElementConstructor
        {
            private final Constructor<?> elementConstructor;
            
            ICSElementConstructor(final Class<?> clazz) throws SecurityException, NoSuchMethodException {
                (this.elementConstructor = clazz.getConstructor(File.class, ZipFile.class, DexFile.class)).setAccessible(true);
            }
            
            @Override
            public Object newInstance(final File file, final DexFile dexFile) throws IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException, IOException {
                return this.elementConstructor.newInstance(file, new ZipFile(file), dexFile);
            }
        }
        
        private static class JBMR11ElementConstructor implements ElementConstructor
        {
            private final Constructor<?> elementConstructor;
            
            JBMR11ElementConstructor(final Class<?> clazz) throws SecurityException, NoSuchMethodException {
                (this.elementConstructor = clazz.getConstructor(File.class, File.class, DexFile.class)).setAccessible(true);
            }
            
            @Override
            public Object newInstance(final File file, final DexFile dexFile) throws IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
                return this.elementConstructor.newInstance(file, file, dexFile);
            }
        }
        
        private static class JBMR2ElementConstructor implements ElementConstructor
        {
            private final Constructor<?> elementConstructor;
            
            JBMR2ElementConstructor(final Class<?> clazz) throws SecurityException, NoSuchMethodException {
                (this.elementConstructor = clazz.getConstructor(File.class, Boolean.TYPE, File.class, DexFile.class)).setAccessible(true);
            }
            
            @Override
            public Object newInstance(final File file, final DexFile dexFile) throws IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
                return this.elementConstructor.newInstance(file, Boolean.FALSE, file, dexFile);
            }
        }
    }
    
    private static final class V19
    {
        static void install(final ClassLoader obj, final List<? extends File> c, final File file) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, InvocationTargetException, NoSuchMethodException, IOException {
            final Object value = findField(obj, "pathList").get(obj);
            final ArrayList list = new ArrayList();
            expandFieldArray(value, "dexElements", makeDexElements(value, new ArrayList<File>(c), file, list));
            if (list.size() > 0) {
                for (IOException ex : list) {}
                final Field access$000 = findField(value, "dexElementsSuppressedExceptions");
                final IOException[] array = (IOException[])access$000.get(value);
                IOException[] array2;
                if (array == null) {
                    array2 = list.toArray(new IOException[list.size()]);
                }
                else {
                    array2 = new IOException[list.size() + array.length];
                    list.toArray(array2);
                    System.arraycopy(array, 0, array2, list.size(), array.length);
                }
                access$000.set(value, array2);
                final IOException ex2 = new IOException("I/O exception during makeDexElement");
                ex2.initCause(list.get(0));
                throw ex2;
            }
        }
        
        private static Object[] makeDexElements(final Object obj, final ArrayList<File> list, final File file, final ArrayList<IOException> list2) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
            return (Object[])findMethod(obj, "makeDexElements", (Class<?>[])new Class[] { ArrayList.class, File.class, ArrayList.class }).invoke(obj, list, file, list2);
        }
    }
    
    private static final class V4
    {
        static void install(final ClassLoader classLoader, final List<? extends File> list) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, IOException {
            final int size = list.size();
            final Field access$000 = findField(classLoader, "path");
            final StringBuilder sb = new StringBuilder((String)access$000.get(classLoader));
            final String[] array = new String[size];
            final File[] array2 = new File[size];
            final ZipFile[] array3 = new ZipFile[size];
            final DexFile[] array4 = new DexFile[size];
            final ListIterator listIterator = list.listIterator();
            while (listIterator.hasNext()) {
                final File file = (File)listIterator.next();
                final String absolutePath = file.getAbsolutePath();
                sb.append(':');
                sb.append(absolutePath);
                final int previousIndex = listIterator.previousIndex();
                array[previousIndex] = absolutePath;
                array2[previousIndex] = file;
                array3[previousIndex] = new ZipFile(file);
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(absolutePath);
                sb2.append(".dex");
                array4[previousIndex] = DexFile.loadDex(absolutePath, sb2.toString(), 0);
            }
            access$000.set(classLoader, sb.toString());
            expandFieldArray(classLoader, "mPaths", array);
            expandFieldArray(classLoader, "mFiles", array2);
            expandFieldArray(classLoader, "mZips", array3);
            expandFieldArray(classLoader, "mDexs", array4);
        }
    }
}

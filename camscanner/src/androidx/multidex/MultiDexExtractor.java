// 
// Decompiled by Procyon v0.6.0
// 

package androidx.multidex;

import java.io.InputStream;
import java.util.Iterator;
import android.content.SharedPreferences$Editor;
import java.util.ArrayList;
import java.util.List;
import android.content.SharedPreferences;
import android.content.Context;
import java.io.FileNotFoundException;
import java.util.zip.ZipOutputStream;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.io.FileFilter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.io.File;
import java.nio.channels.FileLock;
import java.io.Closeable;

final class MultiDexExtractor implements Closeable
{
    private static final int BUFFER_SIZE = 16384;
    private static final String DEX_PREFIX = "classes";
    static final String DEX_SUFFIX = ".dex";
    private static final String EXTRACTED_NAME_EXT = ".classes";
    static final String EXTRACTED_SUFFIX = ".zip";
    private static final String KEY_CRC = "crc";
    private static final String KEY_DEX_CRC = "dex.crc.";
    private static final String KEY_DEX_NUMBER = "dex.number";
    private static final String KEY_DEX_TIME = "dex.time.";
    private static final String KEY_TIME_STAMP = "timestamp";
    private static final String LOCK_FILENAME = "MultiDex.lock";
    private static final int MAX_EXTRACT_ATTEMPTS = 3;
    private static final long NO_VALUE = -1L;
    private static final String PREFS_FILE = "multidex.version";
    private static final String TAG = "MultiDex";
    private final FileLock cacheLock;
    private final File dexDir;
    private final FileChannel lockChannel;
    private final RandomAccessFile lockRaf;
    private final File sourceApk;
    private final long sourceCrc;
    
    MultiDexExtractor(File error, final File file) throws IOException {
        final StringBuilder sb = new StringBuilder();
        sb.append("MultiDexExtractor(");
        sb.append(((File)error).getPath());
        sb.append(", ");
        sb.append(file.getPath());
        sb.append(")");
        this.sourceApk = (File)error;
        this.dexDir = file;
        this.sourceCrc = getZipCrc((File)error);
        error = (Error)new File(file, "MultiDex.lock");
        final RandomAccessFile lockRaf = new RandomAccessFile((File)error, "rw");
        this.lockRaf = lockRaf;
        try {
            final FileChannel channel = lockRaf.getChannel();
            this.lockChannel = channel;
            try {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Blocking on lock ");
                sb2.append(((File)error).getPath());
                this.cacheLock = channel.lock();
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(((File)error).getPath());
                sb3.append(" locked");
                return;
            }
            catch (final Error error) {}
            catch (final RuntimeException error) {}
            catch (final IOException ex) {}
            closeQuietly(this.lockChannel);
            throw error;
        }
        catch (final Error error) {}
        catch (final RuntimeException error) {}
        catch (final IOException ex2) {}
        closeQuietly(this.lockRaf);
        throw error;
    }
    
    private void clearDexDir() {
        final File[] listFiles = this.dexDir.listFiles(new FileFilter(this) {
            final MultiDexExtractor this$0;
            
            @Override
            public boolean accept(final File file) {
                return file.getName().equals("MultiDex.lock") ^ true;
            }
        });
        if (listFiles == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to list secondary dex dir content (");
            sb.append(this.dexDir.getPath());
            sb.append(").");
            return;
        }
        for (final File file : listFiles) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Trying to delete old file ");
            sb2.append(file.getPath());
            sb2.append(" of size ");
            sb2.append(file.length());
            if (!file.delete()) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Failed to delete old file ");
                sb3.append(file.getPath());
            }
            else {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("Deleted old file ");
                sb4.append(file.getPath());
            }
        }
    }
    
    private static void closeQuietly(final Closeable closeable) {
        try {
            closeable.close();
        }
        catch (final IOException ex) {}
    }
    
    private static void extract(ZipFile inputStream, final ZipEntry entry, final File dest, String tempFile) throws IOException, FileNotFoundException {
        inputStream = (ZipFile)inputStream.getInputStream(entry);
        final StringBuilder sb = new StringBuilder();
        sb.append("tmp-");
        sb.append(tempFile);
        tempFile = (String)File.createTempFile(sb.toString(), ".zip", dest.getParentFile());
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Extracting ");
        sb2.append(((File)tempFile).getPath());
        try {
            Object o = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream((File)tempFile)));
            try {
                final ZipEntry e = new ZipEntry("classes.dex");
                e.setTime(entry.getTime());
                ((ZipOutputStream)o).putNextEntry(e);
                final byte[] b = new byte[16384];
                for (int i = ((InputStream)inputStream).read(b); i != -1; i = ((InputStream)inputStream).read(b)) {
                    ((ZipOutputStream)o).write(b, 0, i);
                }
                ((ZipOutputStream)o).closeEntry();
                ((ZipOutputStream)o).close();
                if (!((File)tempFile).setReadOnly()) {
                    o = new StringBuilder();
                    ((StringBuilder)o).append("Failed to mark readonly \"");
                    ((StringBuilder)o).append(((File)tempFile).getAbsolutePath());
                    ((StringBuilder)o).append("\" (tmp of \"");
                    ((StringBuilder)o).append(dest.getAbsolutePath());
                    ((StringBuilder)o).append("\")");
                    throw new IOException(((StringBuilder)o).toString());
                }
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Renaming to ");
                sb3.append(dest.getPath());
                if (((File)tempFile).renameTo(dest)) {
                    return;
                }
                o = new StringBuilder();
                ((StringBuilder)o).append("Failed to rename \"");
                ((StringBuilder)o).append(((File)tempFile).getAbsolutePath());
                ((StringBuilder)o).append("\" to \"");
                ((StringBuilder)o).append(dest.getAbsolutePath());
                ((StringBuilder)o).append("\"");
                throw new IOException(((StringBuilder)o).toString());
            }
            finally {
                ((ZipOutputStream)o).close();
            }
        }
        finally {
            closeQuietly(inputStream);
            ((File)tempFile).delete();
        }
    }
    
    private static SharedPreferences getMultiDexPreferences(final Context context) {
        return context.getSharedPreferences("multidex.version", 4);
    }
    
    private static long getTimeStamp(final File file) {
        long lastModified;
        final long n = lastModified = file.lastModified();
        if (n == -1L) {
            lastModified = n - 1L;
        }
        return lastModified;
    }
    
    private static long getZipCrc(final File file) throws IOException {
        long zipCrc;
        final long n = zipCrc = ZipUtil.getZipCrc(file);
        if (n == -1L) {
            zipCrc = n - 1L;
        }
        return zipCrc;
    }
    
    private static boolean isModified(final Context context, final File file, final long n, final String s) {
        final SharedPreferences multiDexPreferences = getMultiDexPreferences(context);
        final StringBuilder sb = new StringBuilder();
        sb.append(s);
        sb.append("timestamp");
        if (multiDexPreferences.getLong(sb.toString(), -1L) == getTimeStamp(file)) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(s);
            sb2.append("crc");
            if (multiDexPreferences.getLong(sb2.toString(), -1L) == n) {
                return false;
            }
        }
        return true;
    }
    
    private List<ExtractedDex> loadExistingExtractions(final Context context, final String s) throws IOException {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.sourceApk.getName());
        sb.append(".classes");
        final String string = sb.toString();
        final SharedPreferences multiDexPreferences = getMultiDexPreferences(context);
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(s);
        sb2.append("dex.number");
        final int int1 = multiDexPreferences.getInt(sb2.toString(), 1);
        final ArrayList list = new ArrayList(int1 - 1);
        for (int i = 2; i <= int1; ++i) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(string);
            sb3.append(i);
            sb3.append(".zip");
            final ExtractedDex obj = new ExtractedDex(this.dexDir, sb3.toString());
            if (!obj.isFile()) {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("Missing extracted secondary dex file '");
                sb4.append(obj.getPath());
                sb4.append("'");
                throw new IOException(sb4.toString());
            }
            obj.crc = getZipCrc(obj);
            final StringBuilder sb5 = new StringBuilder();
            sb5.append(s);
            sb5.append("dex.crc.");
            sb5.append(i);
            final long long1 = multiDexPreferences.getLong(sb5.toString(), -1L);
            final StringBuilder sb6 = new StringBuilder();
            sb6.append(s);
            sb6.append("dex.time.");
            sb6.append(i);
            final long long2 = multiDexPreferences.getLong(sb6.toString(), -1L);
            final long lastModified = obj.lastModified();
            if (long2 != lastModified || long1 != obj.crc) {
                final StringBuilder sb7 = new StringBuilder();
                sb7.append("Invalid extracted dex: ");
                sb7.append(obj);
                sb7.append(" (key \"");
                sb7.append(s);
                sb7.append("\"), expected modification time: ");
                sb7.append(long2);
                sb7.append(", modification time: ");
                sb7.append(lastModified);
                sb7.append(", expected crc: ");
                sb7.append(long1);
                sb7.append(", file crc: ");
                sb7.append(obj.crc);
                throw new IOException(sb7.toString());
            }
            list.add((Object)obj);
        }
        return (List<ExtractedDex>)list;
    }
    
    private List<ExtractedDex> performExtractions() throws IOException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   java/lang/StringBuilder.<init>:()V
        //     7: astore          6
        //     9: aload           6
        //    11: aload_0        
        //    12: getfield        androidx/multidex/MultiDexExtractor.sourceApk:Ljava/io/File;
        //    15: invokevirtual   java/io/File.getName:()Ljava/lang/String;
        //    18: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    21: pop            
        //    22: aload           6
        //    24: ldc             ".classes"
        //    26: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    29: pop            
        //    30: aload           6
        //    32: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    35: astore          10
        //    37: aload_0        
        //    38: invokespecial   androidx/multidex/MultiDexExtractor.clearDexDir:()V
        //    41: new             Ljava/util/ArrayList;
        //    44: dup            
        //    45: invokespecial   java/util/ArrayList.<init>:()V
        //    48: astore          9
        //    50: new             Ljava/util/zip/ZipFile;
        //    53: dup            
        //    54: aload_0        
        //    55: getfield        androidx/multidex/MultiDexExtractor.sourceApk:Ljava/io/File;
        //    58: invokespecial   java/util/zip/ZipFile.<init>:(Ljava/io/File;)V
        //    61: astore          8
        //    63: new             Ljava/lang/StringBuilder;
        //    66: astore          6
        //    68: aload           6
        //    70: invokespecial   java/lang/StringBuilder.<init>:()V
        //    73: aload           6
        //    75: ldc             "classes"
        //    77: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    80: pop            
        //    81: iconst_2       
        //    82: istore_2       
        //    83: aload           6
        //    85: iconst_2       
        //    86: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //    89: pop            
        //    90: aload           6
        //    92: ldc             ".dex"
        //    94: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    97: pop            
        //    98: aload           8
        //   100: aload           6
        //   102: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   105: invokevirtual   java/util/zip/ZipFile.getEntry:(Ljava/lang/String;)Ljava/util/zip/ZipEntry;
        //   108: astore          6
        //   110: aload           6
        //   112: ifnull          598
        //   115: new             Ljava/lang/StringBuilder;
        //   118: astore          7
        //   120: aload           7
        //   122: invokespecial   java/lang/StringBuilder.<init>:()V
        //   125: aload           7
        //   127: aload           10
        //   129: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   132: pop            
        //   133: aload           7
        //   135: iload_2        
        //   136: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   139: pop            
        //   140: aload           7
        //   142: ldc             ".zip"
        //   144: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   147: pop            
        //   148: aload           7
        //   150: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   153: astore          7
        //   155: new             Landroidx/multidex/MultiDexExtractor$ExtractedDex;
        //   158: astore          11
        //   160: aload           11
        //   162: aload_0        
        //   163: getfield        androidx/multidex/MultiDexExtractor.dexDir:Ljava/io/File;
        //   166: aload           7
        //   168: invokespecial   androidx/multidex/MultiDexExtractor$ExtractedDex.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //   171: aload           9
        //   173: aload           11
        //   175: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   180: pop            
        //   181: new             Ljava/lang/StringBuilder;
        //   184: astore          7
        //   186: aload           7
        //   188: invokespecial   java/lang/StringBuilder.<init>:()V
        //   191: aload           7
        //   193: ldc_w           "Extraction is needed for file "
        //   196: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   199: pop            
        //   200: aload           7
        //   202: aload           11
        //   204: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   207: pop            
        //   208: iconst_0       
        //   209: istore          4
        //   211: iconst_0       
        //   212: istore_3       
        //   213: iload           4
        //   215: iconst_3       
        //   216: if_icmpge       471
        //   219: iload_3        
        //   220: ifne            471
        //   223: iload           4
        //   225: iconst_1       
        //   226: iadd           
        //   227: istore          5
        //   229: aload           8
        //   231: aload           6
        //   233: aload           11
        //   235: aload           10
        //   237: invokestatic    androidx/multidex/MultiDexExtractor.extract:(Ljava/util/zip/ZipFile;Ljava/util/zip/ZipEntry;Ljava/io/File;Ljava/lang/String;)V
        //   240: aload           11
        //   242: aload           11
        //   244: invokestatic    androidx/multidex/MultiDexExtractor.getZipCrc:(Ljava/io/File;)J
        //   247: putfield        androidx/multidex/MultiDexExtractor$ExtractedDex.crc:J
        //   250: iconst_1       
        //   251: istore_1       
        //   252: goto            289
        //   255: astore          7
        //   257: new             Ljava/lang/StringBuilder;
        //   260: astore          7
        //   262: aload           7
        //   264: invokespecial   java/lang/StringBuilder.<init>:()V
        //   267: aload           7
        //   269: ldc_w           "Failed to read crc from "
        //   272: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   275: pop            
        //   276: aload           7
        //   278: aload           11
        //   280: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   283: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   286: pop            
        //   287: iconst_0       
        //   288: istore_1       
        //   289: new             Ljava/lang/StringBuilder;
        //   292: astore          12
        //   294: aload           12
        //   296: invokespecial   java/lang/StringBuilder.<init>:()V
        //   299: aload           12
        //   301: ldc_w           "Extraction "
        //   304: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   307: pop            
        //   308: iload_1        
        //   309: ifeq            320
        //   312: ldc_w           "succeeded"
        //   315: astore          7
        //   317: goto            325
        //   320: ldc_w           "failed"
        //   323: astore          7
        //   325: aload           12
        //   327: aload           7
        //   329: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   332: pop            
        //   333: aload           12
        //   335: ldc_w           " '"
        //   338: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   341: pop            
        //   342: aload           12
        //   344: aload           11
        //   346: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   349: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   352: pop            
        //   353: aload           12
        //   355: ldc_w           "': length "
        //   358: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   361: pop            
        //   362: aload           12
        //   364: aload           11
        //   366: invokevirtual   java/io/File.length:()J
        //   369: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   372: pop            
        //   373: aload           12
        //   375: ldc_w           " - crc: "
        //   378: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   381: pop            
        //   382: aload           12
        //   384: aload           11
        //   386: getfield        androidx/multidex/MultiDexExtractor$ExtractedDex.crc:J
        //   389: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   392: pop            
        //   393: iload           5
        //   395: istore          4
        //   397: iload_1        
        //   398: istore_3       
        //   399: iload_1        
        //   400: ifne            213
        //   403: aload           11
        //   405: invokevirtual   java/io/File.delete:()Z
        //   408: pop            
        //   409: iload           5
        //   411: istore          4
        //   413: iload_1        
        //   414: istore_3       
        //   415: aload           11
        //   417: invokevirtual   java/io/File.exists:()Z
        //   420: ifeq            213
        //   423: new             Ljava/lang/StringBuilder;
        //   426: astore          7
        //   428: aload           7
        //   430: invokespecial   java/lang/StringBuilder.<init>:()V
        //   433: aload           7
        //   435: ldc_w           "Failed to delete corrupted secondary dex '"
        //   438: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   441: pop            
        //   442: aload           7
        //   444: aload           11
        //   446: invokevirtual   java/io/File.getPath:()Ljava/lang/String;
        //   449: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   452: pop            
        //   453: aload           7
        //   455: ldc_w           "'"
        //   458: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   461: pop            
        //   462: iload           5
        //   464: istore          4
        //   466: iload_1        
        //   467: istore_3       
        //   468: goto            213
        //   471: iload_3        
        //   472: ifeq            526
        //   475: iinc            2, 1
        //   478: new             Ljava/lang/StringBuilder;
        //   481: astore          6
        //   483: aload           6
        //   485: invokespecial   java/lang/StringBuilder.<init>:()V
        //   488: aload           6
        //   490: ldc             "classes"
        //   492: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   495: pop            
        //   496: aload           6
        //   498: iload_2        
        //   499: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   502: pop            
        //   503: aload           6
        //   505: ldc             ".dex"
        //   507: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   510: pop            
        //   511: aload           8
        //   513: aload           6
        //   515: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   518: invokevirtual   java/util/zip/ZipFile.getEntry:(Ljava/lang/String;)Ljava/util/zip/ZipEntry;
        //   521: astore          6
        //   523: goto            110
        //   526: new             Ljava/io/IOException;
        //   529: astore          7
        //   531: new             Ljava/lang/StringBuilder;
        //   534: astore          6
        //   536: aload           6
        //   538: invokespecial   java/lang/StringBuilder.<init>:()V
        //   541: aload           6
        //   543: ldc_w           "Could not create zip file "
        //   546: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   549: pop            
        //   550: aload           6
        //   552: aload           11
        //   554: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   557: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   560: pop            
        //   561: aload           6
        //   563: ldc_w           " for secondary dex ("
        //   566: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   569: pop            
        //   570: aload           6
        //   572: iload_2        
        //   573: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   576: pop            
        //   577: aload           6
        //   579: ldc             ")"
        //   581: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   584: pop            
        //   585: aload           7
        //   587: aload           6
        //   589: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   592: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //   595: aload           7
        //   597: athrow         
        //   598: aload           8
        //   600: invokevirtual   java/util/zip/ZipFile.close:()V
        //   603: aload           9
        //   605: areturn        
        //   606: astore          6
        //   608: aload           8
        //   610: invokevirtual   java/util/zip/ZipFile.close:()V
        //   613: aload           6
        //   615: athrow         
        //   616: astore          6
        //   618: goto            603
        //   621: astore          7
        //   623: goto            613
        //    Exceptions:
        //  throws java.io.IOException
        //    Signature:
        //  ()Ljava/util/List<Landroidx/multidex/MultiDexExtractor$ExtractedDex;>;
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  63     81     606    616    Any
        //  83     110    606    616    Any
        //  115    208    606    616    Any
        //  229    240    606    616    Any
        //  240    250    255    289    Ljava/io/IOException;
        //  240    250    606    616    Any
        //  257    287    606    616    Any
        //  289    308    606    616    Any
        //  325    393    606    616    Any
        //  403    409    606    616    Any
        //  415    462    606    616    Any
        //  478    523    606    616    Any
        //  526    598    606    616    Any
        //  598    603    616    621    Ljava/io/IOException;
        //  608    613    621    626    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 292 out of bounds for length 292
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static void putStoredApkInfo(final Context context, final String str, final long n, final long n2, final List<ExtractedDex> list) {
        final SharedPreferences$Editor edit = getMultiDexPreferences(context).edit();
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("timestamp");
        edit.putLong(sb.toString(), n);
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append("crc");
        edit.putLong(sb2.toString(), n2);
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(str);
        sb3.append("dex.number");
        edit.putInt(sb3.toString(), list.size() + 1);
        final Iterator iterator = list.iterator();
        int n3 = 2;
        while (iterator.hasNext()) {
            final ExtractedDex extractedDex = (ExtractedDex)iterator.next();
            final StringBuilder sb4 = new StringBuilder();
            sb4.append(str);
            sb4.append("dex.crc.");
            sb4.append(n3);
            edit.putLong(sb4.toString(), extractedDex.crc);
            final StringBuilder sb5 = new StringBuilder();
            sb5.append(str);
            sb5.append("dex.time.");
            sb5.append(n3);
            edit.putLong(sb5.toString(), extractedDex.lastModified());
            ++n3;
        }
        edit.commit();
    }
    
    @Override
    public void close() throws IOException {
        this.cacheLock.release();
        this.lockChannel.close();
        this.lockRaf.close();
    }
    
    List<? extends File> load(Context loadExistingExtractions, final String str, final boolean b) throws IOException {
        final StringBuilder sb = new StringBuilder();
        sb.append("MultiDexExtractor.load(");
        sb.append(this.sourceApk.getPath());
        sb.append(", ");
        sb.append(b);
        sb.append(", ");
        sb.append(str);
        sb.append(")");
        if (this.cacheLock.isValid()) {
            if (!b && !isModified(loadExistingExtractions, this.sourceApk, this.sourceCrc, str)) {
                try {
                    loadExistingExtractions = (Context)this.loadExistingExtractions(loadExistingExtractions, str);
                }
                catch (final IOException ex) {
                    final Object performExtractions = this.performExtractions();
                    putStoredApkInfo(loadExistingExtractions, str, getTimeStamp(this.sourceApk), this.sourceCrc, (List<ExtractedDex>)performExtractions);
                    loadExistingExtractions = (Context)performExtractions;
                }
            }
            else {
                final Object performExtractions2 = this.performExtractions();
                putStoredApkInfo(loadExistingExtractions, str, getTimeStamp(this.sourceApk), this.sourceCrc, (List<ExtractedDex>)performExtractions2);
                loadExistingExtractions = (Context)performExtractions2;
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("load found ");
            sb2.append(((List)loadExistingExtractions).size());
            sb2.append(" secondary dex files");
            return (List<? extends File>)loadExistingExtractions;
        }
        throw new IllegalStateException("MultiDexExtractor was closed");
    }
    
    private static class ExtractedDex extends File
    {
        public long crc;
        
        public ExtractedDex(final File parent, final String child) {
            super(parent, child);
            this.crc = -1L;
        }
    }
}

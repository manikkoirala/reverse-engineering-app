// 
// Decompiled by Procyon v0.6.0
// 

package androidx.swiperefreshlayout.widget;

import android.graphics.drawable.shapes.RectShape;
import android.graphics.Canvas;
import android.graphics.Shader;
import android.graphics.Shader$TileMode;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import androidx.core.content.ContextCompat;
import android.graphics.drawable.Drawable;
import android.view.View;
import androidx.core.view.ViewCompat;
import android.graphics.drawable.shapes.Shape;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.content.Context;
import android.view.animation.Animation$AnimationListener;
import android.widget.ImageView;

class CircleImageView extends ImageView
{
    private static final int FILL_SHADOW_COLOR = 1023410176;
    private static final int KEY_SHADOW_COLOR = 503316480;
    private static final int SHADOW_ELEVATION = 4;
    private static final float SHADOW_RADIUS = 3.5f;
    private static final float X_OFFSET = 0.0f;
    private static final float Y_OFFSET = 1.75f;
    private Animation$AnimationListener mListener;
    int mShadowRadius;
    
    CircleImageView(final Context context, final int color) {
        super(context);
        final float density = ((View)this).getContext().getResources().getDisplayMetrics().density;
        final int n = (int)(1.75f * density);
        final int n2 = (int)(0.0f * density);
        this.mShadowRadius = (int)(3.5f * density);
        ShapeDrawable shapeDrawable;
        if (this.elevationSupported()) {
            shapeDrawable = new ShapeDrawable((Shape)new OvalShape());
            ViewCompat.setElevation((View)this, density * 4.0f);
        }
        else {
            shapeDrawable = new ShapeDrawable((Shape)new OvalShadow(this.mShadowRadius));
            ((View)this).setLayerType(1, shapeDrawable.getPaint());
            shapeDrawable.getPaint().setShadowLayer((float)this.mShadowRadius, (float)n2, (float)n, 503316480);
            final int mShadowRadius = this.mShadowRadius;
            ((View)this).setPadding(mShadowRadius, mShadowRadius, mShadowRadius, mShadowRadius);
        }
        shapeDrawable.getPaint().setColor(color);
        ViewCompat.setBackground((View)this, (Drawable)shapeDrawable);
    }
    
    private boolean elevationSupported() {
        return true;
    }
    
    public void onAnimationEnd() {
        super.onAnimationEnd();
        final Animation$AnimationListener mListener = this.mListener;
        if (mListener != null) {
            mListener.onAnimationEnd(((View)this).getAnimation());
        }
    }
    
    public void onAnimationStart() {
        super.onAnimationStart();
        final Animation$AnimationListener mListener = this.mListener;
        if (mListener != null) {
            mListener.onAnimationStart(((View)this).getAnimation());
        }
    }
    
    protected void onMeasure(final int n, final int n2) {
        super.onMeasure(n, n2);
        if (!this.elevationSupported()) {
            ((View)this).setMeasuredDimension(((View)this).getMeasuredWidth() + this.mShadowRadius * 2, ((View)this).getMeasuredHeight() + this.mShadowRadius * 2);
        }
    }
    
    public void setAnimationListener(final Animation$AnimationListener mListener) {
        this.mListener = mListener;
    }
    
    public void setBackgroundColor(final int color) {
        if (((View)this).getBackground() instanceof ShapeDrawable) {
            ((ShapeDrawable)((View)this).getBackground()).getPaint().setColor(color);
        }
    }
    
    public void setBackgroundColorRes(final int n) {
        this.setBackgroundColor(ContextCompat.getColor(((View)this).getContext(), n));
    }
    
    private class OvalShadow extends OvalShape
    {
        private RadialGradient mRadialGradient;
        private Paint mShadowPaint;
        final CircleImageView this$0;
        
        OvalShadow(final CircleImageView this$0, final int mShadowRadius) {
            this.this$0 = this$0;
            this.mShadowPaint = new Paint();
            this$0.mShadowRadius = mShadowRadius;
            this.updateRadialGradient((int)((RectShape)this).rect().width());
        }
        
        private void updateRadialGradient(final int n) {
            final float n2 = (float)(n / 2);
            final RadialGradient radialGradient = new RadialGradient(n2, n2, (float)this.this$0.mShadowRadius, new int[] { 1023410176, 0 }, (float[])null, Shader$TileMode.CLAMP);
            this.mRadialGradient = radialGradient;
            this.mShadowPaint.setShader((Shader)radialGradient);
        }
        
        public void draw(final Canvas canvas, final Paint paint) {
            final int width = ((View)this.this$0).getWidth();
            final int height = ((View)this.this$0).getHeight();
            final int n = width / 2;
            final float n2 = (float)n;
            final float n3 = (float)(height / 2);
            canvas.drawCircle(n2, n3, n2, this.mShadowPaint);
            canvas.drawCircle(n2, n3, (float)(n - this.this$0.mShadowRadius), paint);
        }
        
        protected void onResize(final float n, final float n2) {
            super.onResize(n, n2);
            this.updateRadialGradient((int)n);
        }
    }
}

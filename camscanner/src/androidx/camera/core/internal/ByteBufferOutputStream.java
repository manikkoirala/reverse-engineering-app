// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal;

import java.io.IOException;
import java.io.EOFException;
import androidx.annotation.NonNull;
import java.nio.ByteBuffer;
import androidx.annotation.RequiresApi;
import java.io.OutputStream;

@RequiresApi(21)
public final class ByteBufferOutputStream extends OutputStream
{
    private final ByteBuffer mByteBuffer;
    
    public ByteBufferOutputStream(@NonNull final ByteBuffer mByteBuffer) {
        this.mByteBuffer = mByteBuffer;
    }
    
    @Override
    public void write(final int n) throws IOException {
        if (this.mByteBuffer.hasRemaining()) {
            this.mByteBuffer.put((byte)n);
            return;
        }
        throw new EOFException("Output ByteBuffer has no bytes remaining.");
    }
    
    @Override
    public void write(final byte[] src, final int offset, final int length) throws IOException {
        src.getClass();
        if (offset >= 0 && offset <= src.length && length >= 0) {
            final int n = offset + length;
            if (n <= src.length && n >= 0) {
                if (length == 0) {
                    return;
                }
                if (this.mByteBuffer.remaining() >= length) {
                    this.mByteBuffer.put(src, offset, length);
                    return;
                }
                throw new EOFException("Output ByteBuffer has insufficient bytes remaining.");
            }
        }
        throw new IndexOutOfBoundsException();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal;

final class AutoValue_ImmutableZoomState extends ImmutableZoomState
{
    private final float linearZoom;
    private final float maxZoomRatio;
    private final float minZoomRatio;
    private final float zoomRatio;
    
    AutoValue_ImmutableZoomState(final float zoomRatio, final float maxZoomRatio, final float minZoomRatio, final float linearZoom) {
        this.zoomRatio = zoomRatio;
        this.maxZoomRatio = maxZoomRatio;
        this.minZoomRatio = minZoomRatio;
        this.linearZoom = linearZoom;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof ImmutableZoomState) {
            final ImmutableZoomState immutableZoomState = (ImmutableZoomState)o;
            if (Float.floatToIntBits(this.zoomRatio) != Float.floatToIntBits(immutableZoomState.getZoomRatio()) || Float.floatToIntBits(this.maxZoomRatio) != Float.floatToIntBits(immutableZoomState.getMaxZoomRatio()) || Float.floatToIntBits(this.minZoomRatio) != Float.floatToIntBits(immutableZoomState.getMinZoomRatio()) || Float.floatToIntBits(this.linearZoom) != Float.floatToIntBits(immutableZoomState.getLinearZoom())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public float getLinearZoom() {
        return this.linearZoom;
    }
    
    @Override
    public float getMaxZoomRatio() {
        return this.maxZoomRatio;
    }
    
    @Override
    public float getMinZoomRatio() {
        return this.minZoomRatio;
    }
    
    @Override
    public float getZoomRatio() {
        return this.zoomRatio;
    }
    
    @Override
    public int hashCode() {
        return (((Float.floatToIntBits(this.zoomRatio) ^ 0xF4243) * 1000003 ^ Float.floatToIntBits(this.maxZoomRatio)) * 1000003 ^ Float.floatToIntBits(this.minZoomRatio)) * 1000003 ^ Float.floatToIntBits(this.linearZoom);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ImmutableZoomState{zoomRatio=");
        sb.append(this.zoomRatio);
        sb.append(", maxZoomRatio=");
        sb.append(this.maxZoomRatio);
        sb.append(", minZoomRatio=");
        sb.append(this.minZoomRatio);
        sb.append(", linearZoom=");
        sb.append(this.linearZoom);
        sb.append("}");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal.utils;

import android.graphics.YuvImage;
import java.io.IOException;
import java.io.OutputStream;
import android.graphics.Bitmap$CompressFormat;
import java.io.ByteArrayOutputStream;
import android.graphics.BitmapFactory$Options;
import android.graphics.BitmapRegionDecoder;
import androidx.annotation.IntRange;
import java.nio.ByteBuffer;
import java.nio.Buffer;
import android.graphics.Bitmap$Config;
import androidx.core.util.Preconditions;
import android.graphics.Bitmap;
import androidx.camera.core.ImageProxy;
import android.graphics.RectF;
import android.graphics.Matrix;
import androidx.annotation.Nullable;
import androidx.camera.core.Logger;
import android.graphics.Rect;
import android.util.Rational;
import androidx.annotation.NonNull;
import android.util.Size;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class ImageUtil
{
    public static final int DEFAULT_RGBA_PIXEL_STRIDE = 4;
    private static final String TAG = "ImageUtil";
    
    private ImageUtil() {
    }
    
    @Nullable
    public static Rect computeCropRectFromAspectRatio(@NonNull final Size size, @NonNull final Rational rational) {
        if (!isAspectRatioValid(rational)) {
            Logger.w("ImageUtil", "Invalid view ratio.");
            return null;
        }
        int width = size.getWidth();
        int height = size.getHeight();
        final float n = (float)width;
        final float n2 = (float)height;
        final float n3 = n / n2;
        final int numerator = rational.getNumerator();
        final int denominator = rational.getDenominator();
        final float floatValue = rational.floatValue();
        int n4 = 0;
        int n5;
        if (floatValue > n3) {
            final int round = Math.round(n / numerator * denominator);
            n5 = (height - round) / 2;
            height = round;
        }
        else {
            final int round2 = Math.round(n2 / denominator * numerator);
            n4 = (width - round2) / 2;
            width = round2;
            n5 = 0;
        }
        return new Rect(n4, n5, width + n4, height + n5);
    }
    
    @NonNull
    public static Rect computeCropRectFromDispatchInfo(@NonNull Rect rect, final int n, @NonNull final Size size, final int n2) {
        final Matrix matrix = new Matrix();
        matrix.setRotate((float)(n2 - n));
        final float[] sizeToVertexes = sizeToVertexes(size);
        matrix.mapPoints(sizeToVertexes);
        matrix.postTranslate(-min(sizeToVertexes[0], sizeToVertexes[2], sizeToVertexes[4], sizeToVertexes[6]), -min(sizeToVertexes[1], sizeToVertexes[3], sizeToVertexes[5], sizeToVertexes[7]));
        matrix.invert(matrix);
        final RectF rectF = new RectF();
        matrix.mapRect(rectF, new RectF(rect));
        rectF.sort();
        rect = new Rect();
        rectF.round(rect);
        return rect;
    }
    
    @NonNull
    public static Bitmap createBitmapFromPlane(@NonNull final ImageProxy.PlaneProxy[] array, final int n, final int n2) {
        final int length = array.length;
        final boolean b = true;
        Preconditions.checkArgument(length == 1, (Object)"Expect a single plane");
        Preconditions.checkArgument(array[0].getPixelStride() == 4, (Object)"Expect pixelStride=4");
        Preconditions.checkArgument(array[0].getRowStride() == n * 4 && b, (Object)"Expect rowStride=width*4");
        final Bitmap bitmap = Bitmap.createBitmap(n, n2, Bitmap$Config.ARGB_8888);
        bitmap.copyPixelsFromBuffer((Buffer)array[0].getBuffer());
        return bitmap;
    }
    
    @NonNull
    public static ByteBuffer createDirectByteBuffer(@NonNull final Bitmap bitmap) {
        Preconditions.checkArgument(bitmap.getConfig() == Bitmap$Config.ARGB_8888, (Object)"Only accept Bitmap with ARGB_8888 format for now.");
        final ByteBuffer allocateDirect = ByteBuffer.allocateDirect(bitmap.getAllocationByteCount());
        bitmap.copyPixelsToBuffer((Buffer)allocateDirect);
        allocateDirect.rewind();
        return allocateDirect;
    }
    
    @NonNull
    private static byte[] cropJpegByteArray(@NonNull final byte[] array, @NonNull final Rect rect, @IntRange(from = 1L, to = 100L) final int n) throws CodecFailedException {
        try {
            final BitmapRegionDecoder instance = BitmapRegionDecoder.newInstance(array, 0, array.length, false);
            final Bitmap decodeRegion = instance.decodeRegion(rect, new BitmapFactory$Options());
            instance.recycle();
            if (decodeRegion == null) {
                throw new CodecFailedException("Decode byte array failed.", FailureType.DECODE_FAILED);
            }
            final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            if (decodeRegion.compress(Bitmap$CompressFormat.JPEG, n, (OutputStream)byteArrayOutputStream)) {
                decodeRegion.recycle();
                return byteArrayOutputStream.toByteArray();
            }
            throw new CodecFailedException("Encode bitmap failed.", FailureType.ENCODE_FAILED);
        }
        catch (final IOException ex) {
            throw new CodecFailedException("Decode byte array failed.", FailureType.DECODE_FAILED);
        }
        catch (final IllegalArgumentException obj) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Decode byte array failed with illegal argument.");
            sb.append(obj);
            throw new CodecFailedException(sb.toString(), FailureType.DECODE_FAILED);
        }
    }
    
    @NonNull
    public static Rational getRotatedAspectRatio(@IntRange(from = 0L, to = 359L) final int n, @NonNull final Rational rational) {
        if (n != 90 && n != 270) {
            return new Rational(rational.getNumerator(), rational.getDenominator());
        }
        return inverseRational(rational);
    }
    
    private static Rational inverseRational(@Nullable final Rational rational) {
        if (rational == null) {
            return rational;
        }
        return new Rational(rational.getDenominator(), rational.getNumerator());
    }
    
    public static boolean isAspectRatioValid(@Nullable final Rational rational) {
        return rational != null && rational.floatValue() > 0.0f && !rational.isNaN();
    }
    
    public static boolean isAspectRatioValid(@NonNull final Size size, @Nullable final Rational rational) {
        return rational != null && rational.floatValue() > 0.0f && isCropAspectRatioHasEffect(size, rational) && !rational.isNaN();
    }
    
    private static boolean isCropAspectRatioHasEffect(@NonNull final Size size, @NonNull final Rational rational) {
        final int width = size.getWidth();
        final int height = size.getHeight();
        final int numerator = rational.getNumerator();
        final int denominator = rational.getDenominator();
        final float n = (float)width;
        final float n2 = (float)numerator;
        final float n3 = n / n2;
        final float n4 = (float)denominator;
        return height != Math.round(n3 * n4) || width != Math.round(height / n4 * n2);
    }
    
    @NonNull
    public static byte[] jpegImageToJpegByteArray(@NonNull final ImageProxy imageProxy) {
        if (imageProxy.getFormat() == 256) {
            final ByteBuffer buffer = imageProxy.getPlanes()[0].getBuffer();
            final byte[] dst = new byte[buffer.capacity()];
            buffer.rewind();
            buffer.get(dst);
            return dst;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Incorrect image format of the input image proxy: ");
        sb.append(imageProxy.getFormat());
        throw new IllegalArgumentException(sb.toString());
    }
    
    @NonNull
    public static byte[] jpegImageToJpegByteArray(@NonNull final ImageProxy imageProxy, @NonNull final Rect rect, @IntRange(from = 1L, to = 100L) final int n) throws CodecFailedException {
        if (imageProxy.getFormat() == 256) {
            return cropJpegByteArray(jpegImageToJpegByteArray(imageProxy), rect, n);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Incorrect image format of the input image proxy: ");
        sb.append(imageProxy.getFormat());
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static float min(final float a, final float b, final float a2, final float b2) {
        return Math.min(Math.min(a, b), Math.min(a2, b2));
    }
    
    private static byte[] nv21ToJpeg(@NonNull final byte[] array, final int n, final int n2, @Nullable final Rect rect, @IntRange(from = 1L, to = 100L) final int n3) throws CodecFailedException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final YuvImage yuvImage = new YuvImage(array, 17, n, n2, (int[])null);
        Rect rect2 = rect;
        if (rect == null) {
            rect2 = new Rect(0, 0, n, n2);
        }
        if (yuvImage.compressToJpeg(rect2, n3, (OutputStream)byteArrayOutputStream)) {
            return byteArrayOutputStream.toByteArray();
        }
        throw new CodecFailedException("YuvImage failed to encode jpeg.", FailureType.ENCODE_FAILED);
    }
    
    public static boolean shouldCropImage(final int n, final int n2, final int n3, final int n4) {
        return n != n3 || n2 != n4;
    }
    
    public static boolean shouldCropImage(@NonNull final ImageProxy imageProxy) {
        return shouldCropImage(imageProxy.getWidth(), imageProxy.getHeight(), imageProxy.getCropRect().width(), imageProxy.getCropRect().height());
    }
    
    @NonNull
    public static float[] sizeToVertexes(@NonNull final Size size) {
        return new float[] { 0.0f, 0.0f, (float)size.getWidth(), 0.0f, (float)size.getWidth(), (float)size.getHeight(), 0.0f, (float)size.getHeight() };
    }
    
    @NonNull
    public static byte[] yuvImageToJpegByteArray(@NonNull final ImageProxy imageProxy, @Nullable final Rect rect, @IntRange(from = 1L, to = 100L) final int n) throws CodecFailedException {
        if (imageProxy.getFormat() == 35) {
            return nv21ToJpeg(yuv_420_888toNv21(imageProxy), imageProxy.getWidth(), imageProxy.getHeight(), rect, n);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Incorrect image format of the input image proxy: ");
        sb.append(imageProxy.getFormat());
        throw new IllegalArgumentException(sb.toString());
    }
    
    @NonNull
    public static byte[] yuv_420_888toNv21(@NonNull final ImageProxy imageProxy) {
        final ImageProxy.PlaneProxy planeProxy = imageProxy.getPlanes()[0];
        final ImageProxy.PlaneProxy planeProxy2 = imageProxy.getPlanes()[1];
        final ImageProxy.PlaneProxy planeProxy3 = imageProxy.getPlanes()[2];
        final ByteBuffer buffer = planeProxy.getBuffer();
        final ByteBuffer buffer2 = planeProxy2.getBuffer();
        final ByteBuffer buffer3 = planeProxy3.getBuffer();
        buffer.rewind();
        buffer2.rewind();
        buffer3.rewind();
        final int remaining = buffer.remaining();
        final byte[] dst = new byte[imageProxy.getWidth() * imageProxy.getHeight() / 2 + remaining];
        int i = 0;
        int offset = 0;
        while (i < imageProxy.getHeight()) {
            buffer.get(dst, offset, imageProxy.getWidth());
            offset += imageProxy.getWidth();
            buffer.position(Math.min(remaining, buffer.position() - imageProxy.getWidth() + planeProxy.getRowStride()));
            ++i;
        }
        final int n = imageProxy.getHeight() / 2;
        final int n2 = imageProxy.getWidth() / 2;
        final int rowStride = planeProxy3.getRowStride();
        final int rowStride2 = planeProxy2.getRowStride();
        final int pixelStride = planeProxy3.getPixelStride();
        final int pixelStride2 = planeProxy2.getPixelStride();
        final byte[] dst2 = new byte[rowStride];
        final byte[] dst3 = new byte[rowStride2];
        int j = 0;
        int n3 = offset;
        while (j < n) {
            buffer3.get(dst2, 0, Math.min(rowStride, buffer3.remaining()));
            buffer2.get(dst3, 0, Math.min(rowStride2, buffer2.remaining()));
            int k = 0;
            int n4 = 0;
            int n5 = 0;
            while (k < n2) {
                final int n6 = n3 + 1;
                dst[n3] = dst2[n4];
                n3 = n6 + 1;
                dst[n6] = dst3[n5];
                n4 += pixelStride;
                n5 += pixelStride2;
                ++k;
            }
            ++j;
        }
        return dst;
    }
    
    public static final class CodecFailedException extends Exception
    {
        private FailureType mFailureType;
        
        CodecFailedException(@NonNull final String message) {
            super(message);
            this.mFailureType = FailureType.UNKNOWN;
        }
        
        CodecFailedException(@NonNull final String message, @NonNull final FailureType mFailureType) {
            super(message);
            this.mFailureType = mFailureType;
        }
        
        @NonNull
        public FailureType getFailureType() {
            return this.mFailureType;
        }
        
        public enum FailureType
        {
            private static final FailureType[] $VALUES;
            
            DECODE_FAILED, 
            ENCODE_FAILED, 
            UNKNOWN;
        }
    }
}

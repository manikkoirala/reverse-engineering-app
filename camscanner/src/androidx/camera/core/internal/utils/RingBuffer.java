// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal.utils;

import androidx.annotation.NonNull;

public interface RingBuffer<T>
{
    @NonNull
    T dequeue();
    
    void enqueue(@NonNull final T p0);
    
    int getMaxCapacity();
    
    boolean isEmpty();
    
    public interface OnRemoveCallback<T>
    {
        void onRemove(@NonNull final T p0);
    }
}

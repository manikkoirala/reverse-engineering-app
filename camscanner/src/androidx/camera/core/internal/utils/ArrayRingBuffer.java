// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal.utils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.GuardedBy;
import java.util.ArrayDeque;

public class ArrayRingBuffer<T> implements RingBuffer<T>
{
    private static final String TAG = "ZslRingBuffer";
    @GuardedBy("mLock")
    private final ArrayDeque<T> mBuffer;
    private final Object mLock;
    @Nullable
    final OnRemoveCallback<T> mOnRemoveCallback;
    private final int mRingBufferCapacity;
    
    public ArrayRingBuffer(final int n) {
        this(n, null);
    }
    
    public ArrayRingBuffer(final int n, @Nullable final OnRemoveCallback<T> mOnRemoveCallback) {
        this.mLock = new Object();
        this.mRingBufferCapacity = n;
        this.mBuffer = new ArrayDeque<T>(n);
        this.mOnRemoveCallback = mOnRemoveCallback;
    }
    
    @NonNull
    @Override
    public T dequeue() {
        synchronized (this.mLock) {
            return this.mBuffer.removeLast();
        }
    }
    
    @Override
    public void enqueue(@NonNull final T e) {
        synchronized (this.mLock) {
            T dequeue;
            if (this.mBuffer.size() >= this.mRingBufferCapacity) {
                dequeue = this.dequeue();
            }
            else {
                dequeue = null;
            }
            this.mBuffer.addFirst(e);
            monitorexit(this.mLock);
            final OnRemoveCallback<T> mOnRemoveCallback = this.mOnRemoveCallback;
            if (mOnRemoveCallback != null && dequeue != null) {
                mOnRemoveCallback.onRemove(dequeue);
            }
        }
    }
    
    @Override
    public int getMaxCapacity() {
        return this.mRingBufferCapacity;
    }
    
    @Override
    public boolean isEmpty() {
        synchronized (this.mLock) {
            return this.mBuffer.isEmpty();
        }
    }
}

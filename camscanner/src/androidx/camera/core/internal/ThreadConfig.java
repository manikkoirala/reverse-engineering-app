// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;
import androidx.camera.core.impl.Config;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.ReadableConfig;

@RequiresApi(21)
public interface ThreadConfig extends ReadableConfig
{
    public static final Option<Executor> OPTION_BACKGROUND_EXECUTOR = Config.Option.create("camerax.core.thread.backgroundExecutor", Executor.class);
    
    @NonNull
    Executor getBackgroundExecutor();
    
    @Nullable
    Executor getBackgroundExecutor(@Nullable final Executor p0);
    
    public interface Builder<B>
    {
        @NonNull
        B setBackgroundExecutor(@NonNull final Executor p0);
    }
}

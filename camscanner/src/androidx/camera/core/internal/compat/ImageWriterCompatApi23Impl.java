// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal.compat;

import androidx.camera.core.internal.oO80;
import androidx.annotation.IntRange;
import android.view.Surface;
import androidx.camera.core.internal.\u3007\u3007888;
import android.media.Image;
import androidx.annotation.NonNull;
import androidx.camera.camera2.internal.\u30070OO8;
import android.media.ImageWriter;
import androidx.annotation.RequiresApi;

@RequiresApi(23)
final class ImageWriterCompatApi23Impl
{
    private ImageWriterCompatApi23Impl() {
    }
    
    static void close(final ImageWriter imageWriter) {
        \u30070OO8.\u3007080(imageWriter);
    }
    
    @NonNull
    static Image dequeueInputImage(@NonNull final ImageWriter imageWriter) {
        return \u3007\u3007888.\u3007080(imageWriter);
    }
    
    @NonNull
    static ImageWriter newInstance(@NonNull final Surface surface, @IntRange(from = 1L) final int n) {
        return \u3007080.\u3007080(surface, n);
    }
    
    static void queueInputImage(@NonNull final ImageWriter imageWriter, @NonNull final Image image) {
        oO80.\u3007080(imageWriter, image);
    }
}

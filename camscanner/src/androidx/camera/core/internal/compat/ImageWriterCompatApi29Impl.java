// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal.compat;

import android.media.ImageWriter;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import android.view.Surface;
import androidx.annotation.RequiresApi;

@RequiresApi(29)
final class ImageWriterCompatApi29Impl
{
    private ImageWriterCompatApi29Impl() {
    }
    
    @NonNull
    static ImageWriter newInstance(@NonNull final Surface surface, @IntRange(from = 1L) final int n, final int n2) {
        return \u3007o00\u3007\u3007Oo.\u3007080(surface, n, n2);
    }
}

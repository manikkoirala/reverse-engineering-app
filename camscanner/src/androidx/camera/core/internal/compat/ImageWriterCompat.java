// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal.compat;

import androidx.annotation.IntRange;
import android.view.Surface;
import android.media.Image;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.media.ImageWriter;
import androidx.annotation.RequiresApi;

@RequiresApi(23)
public final class ImageWriterCompat
{
    private ImageWriterCompat() {
    }
    
    public static void close(@NonNull final ImageWriter imageWriter) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 23) {
            ImageWriterCompatApi23Impl.close(imageWriter);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unable to call close() on API ");
        sb.append(sdk_INT);
        sb.append(". Version 23 or higher required.");
        throw new RuntimeException(sb.toString());
    }
    
    @NonNull
    public static Image dequeueInputImage(@NonNull final ImageWriter imageWriter) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 23) {
            return ImageWriterCompatApi23Impl.dequeueInputImage(imageWriter);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unable to call dequeueInputImage() on API ");
        sb.append(sdk_INT);
        sb.append(". Version 23 or higher required.");
        throw new RuntimeException(sb.toString());
    }
    
    @NonNull
    public static ImageWriter newInstance(@NonNull final Surface surface, @IntRange(from = 1L) final int n) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 23) {
            return ImageWriterCompatApi23Impl.newInstance(surface, n);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unable to call newInstance(Surface, int) on API ");
        sb.append(sdk_INT);
        sb.append(". Version 23 or higher required.");
        throw new RuntimeException(sb.toString());
    }
    
    @NonNull
    public static ImageWriter newInstance(@NonNull final Surface surface, @IntRange(from = 1L) final int n, final int n2) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 29) {
            return ImageWriterCompatApi29Impl.newInstance(surface, n, n2);
        }
        if (sdk_INT >= 26) {
            return ImageWriterCompatApi26Impl.newInstance(surface, n, n2);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unable to call newInstance(Surface, int, int) on API ");
        sb.append(sdk_INT);
        sb.append(". Version 26 or higher required.");
        throw new RuntimeException(sb.toString());
    }
    
    public static void queueInputImage(@NonNull final ImageWriter imageWriter, @NonNull final Image image) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 23) {
            ImageWriterCompatApi23Impl.queueInputImage(imageWriter, image);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unable to call queueInputImage() on API ");
        sb.append(sdk_INT);
        sb.append(". Version 23 or higher required.");
        throw new RuntimeException(sb.toString());
    }
}

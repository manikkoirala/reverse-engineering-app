// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal.compat.quirk;

import androidx.annotation.NonNull;
import java.util.ArrayList;
import androidx.camera.core.impl.Quirk;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class DeviceQuirksLoader
{
    private DeviceQuirksLoader() {
    }
    
    @NonNull
    static List<Quirk> loadQuirks() {
        final ArrayList list = new ArrayList();
        if (ImageCaptureRotationOptionQuirk.load()) {
            list.add(new ImageCaptureRotationOptionQuirk());
        }
        if (SurfaceOrderQuirk.load()) {
            list.add(new SurfaceOrderQuirk());
        }
        return list;
    }
}

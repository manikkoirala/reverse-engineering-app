// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal.compat.quirk;

import androidx.camera.core.impl.CaptureConfig;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.Config;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.Quirk;

@RequiresApi(21)
public final class ImageCaptureRotationOptionQuirk implements Quirk
{
    private static boolean isHonor9X() {
        return "HONOR".equalsIgnoreCase(Build.BRAND) && "STK-LX1".equalsIgnoreCase(Build.MODEL);
    }
    
    private static boolean isHuaweiMate20Lite() {
        return "HUAWEI".equalsIgnoreCase(Build.BRAND) && "SNE-LX1".equalsIgnoreCase(Build.MODEL);
    }
    
    static boolean load() {
        return isHuaweiMate20Lite() || isHonor9X();
    }
    
    public boolean isSupported(@NonNull final Config.Option<?> option) {
        return option != CaptureConfig.OPTION_ROTATION;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal.compat.quirk;

import androidx.annotation.Nullable;
import androidx.camera.core.impl.Quirk;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.Quirks;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class DeviceQuirks
{
    @NonNull
    private static final Quirks QUIRKS;
    
    static {
        QUIRKS = new Quirks(DeviceQuirksLoader.loadQuirks());
    }
    
    private DeviceQuirks() {
    }
    
    @Nullable
    public static <T extends Quirk> T get(@NonNull final Class<T> clazz) {
        return DeviceQuirks.QUIRKS.get(clazz);
    }
}

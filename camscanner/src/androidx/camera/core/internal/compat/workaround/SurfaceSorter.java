// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal.compat.workaround;

import java.util.Comparator;
import java.util.Collections;
import java.util.List;
import androidx.camera.core.impl.SessionConfig;
import androidx.camera.core.Preview;
import androidx.camera.core.VideoCapture;
import android.media.MediaCodec;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.DeferrableSurface;
import androidx.camera.core.internal.compat.quirk.DeviceQuirks;
import androidx.camera.core.internal.compat.quirk.SurfaceOrderQuirk;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class SurfaceSorter
{
    private static final int PRIORITY_MEDIA_CODEC_SURFACE = 2;
    private static final int PRIORITY_OTHERS = 1;
    private static final int PRIORITY_PREVIEW_SURFACE = 0;
    private final boolean mHasQuirk;
    
    public SurfaceSorter() {
        this.mHasQuirk = (DeviceQuirks.get(SurfaceOrderQuirk.class) != null);
    }
    
    private int getSurfacePriority(@NonNull final DeferrableSurface deferrableSurface) {
        if (deferrableSurface.getContainerClass() == MediaCodec.class || deferrableSurface.getContainerClass() == VideoCapture.class) {
            return 2;
        }
        if (deferrableSurface.getContainerClass() == Preview.class) {
            return 0;
        }
        return 1;
    }
    
    public void sort(@NonNull final List<SessionConfig.OutputConfig> list) {
        if (!this.mHasQuirk) {
            return;
        }
        Collections.sort((List<Object>)list, new \u3007080(this));
    }
}

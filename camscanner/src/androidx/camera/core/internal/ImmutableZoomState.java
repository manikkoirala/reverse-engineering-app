// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.camera.core.ZoomState;

@RequiresApi(21)
public abstract class ImmutableZoomState implements ZoomState
{
    @NonNull
    public static ZoomState create(final float n, final float n2, final float n3, final float n4) {
        return new AutoValue_ImmutableZoomState(n, n2, n3, n4);
    }
    
    @NonNull
    public static ZoomState create(@NonNull final ZoomState zoomState) {
        return new AutoValue_ImmutableZoomState(zoomState.getZoomRatio(), zoomState.getMaxZoomRatio(), zoomState.getMinZoomRatio(), zoomState.getLinearZoom());
    }
    
    @Override
    public abstract float getLinearZoom();
    
    @Override
    public abstract float getMaxZoomRatio();
    
    @Override
    public abstract float getMinZoomRatio();
    
    @Override
    public abstract float getZoomRatio();
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal;

import android.annotation.SuppressLint;
import java.util.Iterator;
import androidx.camera.core.internal.utils.ImageUtil;
import android.graphics.Matrix$ScaleToFit;
import android.graphics.Matrix;
import java.util.HashMap;
import android.graphics.RectF;
import androidx.core.util.Preconditions;
import android.util.Size;
import androidx.camera.core.UseCase;
import java.util.Map;
import androidx.annotation.IntRange;
import android.util.Rational;
import androidx.annotation.NonNull;
import android.graphics.Rect;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class ViewPorts
{
    private ViewPorts() {
    }
    
    @NonNull
    public static Map<UseCase, Rect> calculateViewPortRects(@NonNull final Rect rect, final boolean b, @NonNull final Rational rational, @IntRange(from = 0L, to = 359L) final int n, final int n2, final int n3, @NonNull final Map<UseCase, Size> map) {
        Preconditions.checkArgument(rect.width() > 0 && rect.height() > 0, (Object)"Cannot compute viewport crop rects zero sized sensor rect.");
        final RectF rectF = new RectF(rect);
        final HashMap hashMap = new HashMap();
        final RectF rectF2 = new RectF(rect);
        for (final Map.Entry<K, Size> entry : map.entrySet()) {
            final Matrix matrix = new Matrix();
            final RectF rectF3 = new RectF(0.0f, 0.0f, (float)entry.getValue().getWidth(), (float)entry.getValue().getHeight());
            matrix.setRectToRect(rectF3, rectF, Matrix$ScaleToFit.CENTER);
            hashMap.put(entry.getKey(), matrix);
            final RectF rectF4 = new RectF();
            matrix.mapRect(rectF4, rectF3);
            rectF2.intersect(rectF4);
        }
        final RectF scaledRect = getScaledRect(rectF2, ImageUtil.getRotatedAspectRatio(n, rational), n2, b, n3, n);
        final HashMap hashMap2 = new HashMap();
        final RectF rectF5 = new RectF();
        final Matrix matrix2 = new Matrix();
        for (final Map.Entry<K, Matrix> entry2 : hashMap.entrySet()) {
            entry2.getValue().invert(matrix2);
            matrix2.mapRect(rectF5, scaledRect);
            final Rect rect2 = new Rect();
            rectF5.round(rect2);
            hashMap2.put(entry2.getKey(), rect2);
        }
        return hashMap2;
    }
    
    private static RectF correctStartOrEnd(final boolean b, @IntRange(from = 0L, to = 359L) final int i, final RectF rectF, final RectF rectF2) {
        final int n = 1;
        final boolean b2 = i == 0 && !b;
        final boolean b3 = i == 90 && b;
        if (b2 || b3) {
            return rectF2;
        }
        final boolean b4 = i == 0 && b;
        final boolean b5 = i == 270 && !b;
        if (b4 || b5) {
            return flipHorizontally(rectF2, rectF.centerX());
        }
        final boolean b6 = i == 90 && !b;
        final boolean b7 = i == 180 && b;
        if (b6 || b7) {
            return flipVertically(rectF2, rectF.centerY());
        }
        final boolean b8 = i == 180 && !b;
        int n2;
        if (i == 270 && b) {
            n2 = n;
        }
        else {
            n2 = 0;
        }
        if (!b8 && n2 == 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid argument: mirrored ");
            sb.append(b);
            sb.append(" rotation ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        }
        return flipHorizontally(flipVertically(rectF2, rectF.centerY()), rectF.centerX());
    }
    
    private static RectF flipHorizontally(final RectF rectF, final float n) {
        return new RectF(flipX(rectF.right, n), rectF.top, flipX(rectF.left, n), rectF.bottom);
    }
    
    private static RectF flipVertically(final RectF rectF, final float n) {
        return new RectF(rectF.left, flipY(rectF.bottom, n), rectF.right, flipY(rectF.top, n));
    }
    
    private static float flipX(final float n, final float n2) {
        return n2 + n2 - n;
    }
    
    private static float flipY(final float n, final float n2) {
        return n2 + n2 - n;
    }
    
    @SuppressLint({ "SwitchIntDef" })
    @NonNull
    public static RectF getScaledRect(@NonNull final RectF rectF, @NonNull final Rational rational, final int i, final boolean b, final int n, @IntRange(from = 0L, to = 359L) final int n2) {
        if (i == 3) {
            return rectF;
        }
        final Matrix matrix = new Matrix();
        final RectF rectF2 = new RectF(0.0f, 0.0f, (float)rational.getNumerator(), (float)rational.getDenominator());
        if (i != 0) {
            if (i != 1) {
                if (i != 2) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unexpected scale type: ");
                    sb.append(i);
                    throw new IllegalStateException(sb.toString());
                }
                matrix.setRectToRect(rectF2, rectF, Matrix$ScaleToFit.END);
            }
            else {
                matrix.setRectToRect(rectF2, rectF, Matrix$ScaleToFit.CENTER);
            }
        }
        else {
            matrix.setRectToRect(rectF2, rectF, Matrix$ScaleToFit.START);
        }
        final RectF rectF3 = new RectF();
        matrix.mapRect(rectF3, rectF2);
        return correctStartOrEnd(shouldMirrorStartAndEnd(b, n), n2, rectF, rectF3);
    }
    
    private static boolean shouldMirrorStartAndEnd(final boolean b, int n) {
        final int n2 = 1;
        if (n == 1) {
            n = n2;
        }
        else {
            n = 0;
        }
        return ((b ? 1 : 0) ^ n) != 0x0;
    }
}

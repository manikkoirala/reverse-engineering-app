// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal;

import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.Config;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.ReadableConfig;

@RequiresApi(21)
public interface TargetConfig<T> extends ReadableConfig
{
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final Option<Class<?>> OPTION_TARGET_CLASS = Config.Option.create("camerax.core.target.class", Class.class);
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final Option<String> OPTION_TARGET_NAME = Config.Option.create("camerax.core.target.name", String.class);
    
    @NonNull
    Class<T> getTargetClass();
    
    @Nullable
    Class<T> getTargetClass(@Nullable final Class<T> p0);
    
    @NonNull
    String getTargetName();
    
    @Nullable
    String getTargetName(@Nullable final String p0);
    
    public interface Builder<T, B>
    {
        @NonNull
        B setTargetClass(@NonNull final Class<T> p0);
        
        @NonNull
        B setTargetName(@NonNull final String p0);
    }
}

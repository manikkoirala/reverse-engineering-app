// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal;

import androidx.camera.core.impl.ImageProxyBundle;
import android.util.Size;
import androidx.camera.core.internal.compat.ImageWriterCompat;
import androidx.core.util.Preconditions;
import android.view.Surface;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.camera2.internal.\u30070OO8;
import androidx.camera.core.Logger;
import androidx.annotation.IntRange;
import android.media.ImageWriter;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.annotation.GuardedBy;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import android.graphics.Rect;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.CaptureProcessor;

@RequiresApi(26)
public class YuvToJpegProcessor implements CaptureProcessor
{
    private static final String TAG = "YuvToJpegProcessor";
    private static final Rect UNINITIALIZED_RECT;
    @GuardedBy("mLock")
    CallbackToFutureAdapter.Completer<Void> mCloseCompleter;
    @GuardedBy("mLock")
    private ListenableFuture<Void> mCloseFuture;
    @GuardedBy("mLock")
    private boolean mClosed;
    @GuardedBy("mLock")
    private Rect mImageRect;
    @GuardedBy("mLock")
    private ImageWriter mImageWriter;
    private final Object mLock;
    private final int mMaxImages;
    @GuardedBy("mLock")
    private int mProcessingImages;
    @GuardedBy("mLock")
    @IntRange(from = 0L, to = 100L)
    private int mQuality;
    @GuardedBy("mLock")
    private int mRotationDegrees;
    
    static {
        UNINITIALIZED_RECT = new Rect(0, 0, 0, 0);
    }
    
    public YuvToJpegProcessor(@IntRange(from = 0L, to = 100L) final int mQuality, final int mMaxImages) {
        this.mLock = new Object();
        this.mRotationDegrees = 0;
        this.mClosed = false;
        this.mProcessingImages = 0;
        this.mImageRect = YuvToJpegProcessor.UNINITIALIZED_RECT;
        this.mQuality = mQuality;
        this.mMaxImages = mMaxImages;
    }
    
    @Override
    public void close() {
        synchronized (this.mLock) {
            if (this.mClosed) {
                return;
            }
            this.mClosed = true;
            Object mCloseCompleter;
            if (this.mProcessingImages == 0 && this.mImageWriter != null) {
                Logger.d("YuvToJpegProcessor", "No processing in progress. Closing immediately.");
                \u30070OO8.\u3007080(this.mImageWriter);
                mCloseCompleter = this.mCloseCompleter;
            }
            else {
                Logger.d("YuvToJpegProcessor", "close() called while processing. Will close after completion.");
                mCloseCompleter = null;
            }
            monitorexit(this.mLock);
            if (mCloseCompleter != null) {
                ((CallbackToFutureAdapter.Completer<Object>)mCloseCompleter).set(null);
            }
        }
    }
    
    @NonNull
    @Override
    public ListenableFuture<Void> getCloseFuture() {
        synchronized (this.mLock) {
            com.google.common.util.concurrent.ListenableFuture<Void> listenableFuture;
            if (this.mClosed && this.mProcessingImages == 0) {
                listenableFuture = Futures.immediateFuture((Void)null);
            }
            else {
                if (this.mCloseFuture == null) {
                    this.mCloseFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new \u300780\u3007808\u3007O(this));
                }
                listenableFuture = Futures.nonCancellationPropagating(this.mCloseFuture);
            }
            return listenableFuture;
        }
    }
    
    @Override
    public void onOutputSurface(@NonNull final Surface surface, final int n) {
        Preconditions.checkState(n == 256, "YuvToJpegProcessor only supports JPEG output format.");
        synchronized (this.mLock) {
            if (!this.mClosed) {
                if (this.mImageWriter != null) {
                    throw new IllegalStateException("Output surface already set.");
                }
                this.mImageWriter = ImageWriterCompat.newInstance(surface, this.mMaxImages, n);
            }
            else {
                Logger.w("YuvToJpegProcessor", "Cannot set output surface. Processor is closed.");
            }
        }
    }
    
    @Override
    public void onResolutionUpdate(@NonNull final Size size) {
        synchronized (this.mLock) {
            this.mImageRect = new Rect(0, 0, size.getWidth(), size.getHeight());
        }
    }
    
    @Override
    public void process(@NonNull final ImageProxyBundle p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokeinterface androidx/camera/core/impl/ImageProxyBundle.getCaptureIds:()Ljava/util/List;
        //     6: astore          11
        //     8: aload           11
        //    10: invokeinterface java/util/List.size:()I
        //    15: istore_2       
        //    16: iconst_0       
        //    17: istore          4
        //    19: iconst_0       
        //    20: istore          6
        //    22: iconst_0       
        //    23: istore          5
        //    25: iconst_0       
        //    26: istore          7
        //    28: iload_2        
        //    29: iconst_1       
        //    30: if_icmpne       39
        //    33: iconst_1       
        //    34: istore          9
        //    36: goto            42
        //    39: iconst_0       
        //    40: istore          9
        //    42: new             Ljava/lang/StringBuilder;
        //    45: dup            
        //    46: invokespecial   java/lang/StringBuilder.<init>:()V
        //    49: astore          10
        //    51: aload           10
        //    53: ldc             "Processing image bundle have single capture id, but found "
        //    55: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    58: pop            
        //    59: aload           10
        //    61: aload           11
        //    63: invokeinterface java/util/List.size:()I
        //    68: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //    71: pop            
        //    72: iload           9
        //    74: aload           10
        //    76: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    79: invokestatic    androidx/core/util/Preconditions.checkArgument:(ZLjava/lang/Object;)V
        //    82: aload_1        
        //    83: aload           11
        //    85: iconst_0       
        //    86: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //    91: checkcast       Ljava/lang/Integer;
        //    94: invokevirtual   java/lang/Integer.intValue:()I
        //    97: invokeinterface androidx/camera/core/impl/ImageProxyBundle.getImageProxy:(I)Lcom/google/common/util/concurrent/ListenableFuture;
        //   102: astore_1       
        //   103: aload_1        
        //   104: invokeinterface java/util/concurrent/Future.isDone:()Z
        //   109: invokestatic    androidx/core/util/Preconditions.checkArgument:(Z)V
        //   112: aload_0        
        //   113: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mLock:Ljava/lang/Object;
        //   116: astore          11
        //   118: aload           11
        //   120: monitorenter   
        //   121: aload_0        
        //   122: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mImageWriter:Landroid/media/ImageWriter;
        //   125: astore          15
        //   127: aload_0        
        //   128: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mClosed:Z
        //   131: ifne            139
        //   134: iconst_1       
        //   135: istore_2       
        //   136: goto            141
        //   139: iconst_0       
        //   140: istore_2       
        //   141: aload_0        
        //   142: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mImageRect:Landroid/graphics/Rect;
        //   145: astore          10
        //   147: iload_2        
        //   148: ifeq            161
        //   151: aload_0        
        //   152: aload_0        
        //   153: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mProcessingImages:I
        //   156: iconst_1       
        //   157: iadd           
        //   158: putfield        androidx/camera/core/internal/YuvToJpegProcessor.mProcessingImages:I
        //   161: aload_0        
        //   162: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mQuality:I
        //   165: istore_3       
        //   166: aload_0        
        //   167: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mRotationDegrees:I
        //   170: istore          8
        //   172: aload           11
        //   174: monitorexit    
        //   175: aload_1        
        //   176: invokeinterface java/util/concurrent/Future.get:()Ljava/lang/Object;
        //   181: checkcast       Landroidx/camera/core/ImageProxy;
        //   184: astore          13
        //   186: iload_2        
        //   187: ifne            299
        //   190: ldc             "YuvToJpegProcessor"
        //   192: ldc             "Image enqueued for processing on closed processor."
        //   194: invokestatic    androidx/camera/core/Logger.w:(Ljava/lang/String;Ljava/lang/String;)V
        //   197: aload           13
        //   199: invokeinterface androidx/camera/core/ImageProxy.close:()V
        //   204: aload_0        
        //   205: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mLock:Ljava/lang/Object;
        //   208: astore_1       
        //   209: aload_1        
        //   210: monitorenter   
        //   211: iload           7
        //   213: istore_3       
        //   214: iload_2        
        //   215: ifeq            257
        //   218: aload_0        
        //   219: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mProcessingImages:I
        //   222: istore_2       
        //   223: aload_0        
        //   224: iload_2        
        //   225: iconst_1       
        //   226: isub           
        //   227: putfield        androidx/camera/core/internal/YuvToJpegProcessor.mProcessingImages:I
        //   230: iload           7
        //   232: istore_3       
        //   233: iload_2        
        //   234: ifne            257
        //   237: iload           7
        //   239: istore_3       
        //   240: aload_0        
        //   241: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mClosed:Z
        //   244: ifeq            257
        //   247: iconst_1       
        //   248: istore_3       
        //   249: goto            257
        //   252: astore          10
        //   254: goto            294
        //   257: aload_0        
        //   258: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mCloseCompleter:Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;
        //   261: astore          10
        //   263: aload_1        
        //   264: monitorexit    
        //   265: iload_3        
        //   266: ifeq            293
        //   269: aload           15
        //   271: invokestatic    androidx/camera/camera2/internal/\u30070OO8.\u3007080:(Landroid/media/ImageWriter;)V
        //   274: ldc             "YuvToJpegProcessor"
        //   276: ldc             "Closed after completion of last image processed."
        //   278: invokestatic    androidx/camera/core/Logger.d:(Ljava/lang/String;Ljava/lang/String;)V
        //   281: aload           10
        //   283: ifnull          293
        //   286: aload           10
        //   288: aconst_null    
        //   289: invokevirtual   androidx/concurrent/futures/CallbackToFutureAdapter$Completer.set:(Ljava/lang/Object;)Z
        //   292: pop            
        //   293: return         
        //   294: aload_1        
        //   295: monitorexit    
        //   296: aload           10
        //   298: athrow         
        //   299: aload           15
        //   301: invokestatic    androidx/camera/core/internal/\u3007\u3007888.\u3007080:(Landroid/media/ImageWriter;)Landroid/media/Image;
        //   304: astore          11
        //   306: aload           13
        //   308: astore          14
        //   310: aload           11
        //   312: astore          12
        //   314: aload_1        
        //   315: invokeinterface java/util/concurrent/Future.get:()Ljava/lang/Object;
        //   320: checkcast       Landroidx/camera/core/ImageProxy;
        //   323: astore_1       
        //   324: aload_1        
        //   325: invokeinterface androidx/camera/core/ImageProxy.getFormat:()I
        //   330: bipush          35
        //   332: if_icmpne       341
        //   335: iconst_1       
        //   336: istore          9
        //   338: goto            344
        //   341: iconst_0       
        //   342: istore          9
        //   344: iload           9
        //   346: ldc             "Input image is not expected YUV_420_888 image format"
        //   348: invokestatic    androidx/core/util/Preconditions.checkState:(ZLjava/lang/String;)V
        //   351: aload_1        
        //   352: invokestatic    androidx/camera/core/internal/utils/ImageUtil.yuv_420_888toNv21:(Landroidx/camera/core/ImageProxy;)[B
        //   355: astore          13
        //   357: new             Landroid/graphics/YuvImage;
        //   360: astore          12
        //   362: aload           12
        //   364: aload           13
        //   366: bipush          17
        //   368: aload_1        
        //   369: invokeinterface androidx/camera/core/ImageProxy.getWidth:()I
        //   374: aload_1        
        //   375: invokeinterface androidx/camera/core/ImageProxy.getHeight:()I
        //   380: aconst_null    
        //   381: invokespecial   android/graphics/YuvImage.<init>:([BIII[I)V
        //   384: aload           11
        //   386: invokevirtual   android/media/Image.getPlanes:()[Landroid/media/Image$Plane;
        //   389: iconst_0       
        //   390: aaload         
        //   391: invokevirtual   android/media/Image$Plane.getBuffer:()Ljava/nio/ByteBuffer;
        //   394: astore          14
        //   396: aload           14
        //   398: invokevirtual   java/nio/Buffer.position:()I
        //   401: istore          7
        //   403: new             Landroidx/camera/core/impl/utils/ExifOutputStream;
        //   406: astore          16
        //   408: new             Landroidx/camera/core/internal/ByteBufferOutputStream;
        //   411: astore          13
        //   413: aload           13
        //   415: aload           14
        //   417: invokespecial   androidx/camera/core/internal/ByteBufferOutputStream.<init>:(Ljava/nio/ByteBuffer;)V
        //   420: aload           16
        //   422: aload           13
        //   424: aload_1        
        //   425: iload           8
        //   427: invokestatic    androidx/camera/core/impl/utils/ExifData.create:(Landroidx/camera/core/ImageProxy;I)Landroidx/camera/core/impl/utils/ExifData;
        //   430: invokespecial   androidx/camera/core/impl/utils/ExifOutputStream.<init>:(Ljava/io/OutputStream;Landroidx/camera/core/impl/utils/ExifData;)V
        //   433: aload           12
        //   435: aload           10
        //   437: iload_3        
        //   438: aload           16
        //   440: invokevirtual   android/graphics/YuvImage.compressToJpeg:(Landroid/graphics/Rect;ILjava/io/OutputStream;)Z
        //   443: pop            
        //   444: aload_1        
        //   445: invokeinterface androidx/camera/core/ImageProxy.close:()V
        //   450: aload           14
        //   452: aload           14
        //   454: invokevirtual   java/nio/Buffer.position:()I
        //   457: invokevirtual   java/nio/ByteBuffer.limit:(I)Ljava/nio/Buffer;
        //   460: pop            
        //   461: aload           14
        //   463: iload           7
        //   465: invokevirtual   java/nio/ByteBuffer.position:(I)Ljava/nio/Buffer;
        //   468: pop            
        //   469: aload           15
        //   471: aload           11
        //   473: invokestatic    androidx/camera/core/internal/oO80.\u3007080:(Landroid/media/ImageWriter;Landroid/media/Image;)V
        //   476: aload_0        
        //   477: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mLock:Ljava/lang/Object;
        //   480: astore          10
        //   482: aload           10
        //   484: monitorenter   
        //   485: iload           4
        //   487: istore_3       
        //   488: iload_2        
        //   489: ifeq            530
        //   492: aload_0        
        //   493: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mProcessingImages:I
        //   496: istore_2       
        //   497: aload_0        
        //   498: iload_2        
        //   499: iconst_1       
        //   500: isub           
        //   501: putfield        androidx/camera/core/internal/YuvToJpegProcessor.mProcessingImages:I
        //   504: iload           4
        //   506: istore_3       
        //   507: iload_2        
        //   508: ifne            530
        //   511: iload           4
        //   513: istore_3       
        //   514: aload_0        
        //   515: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mClosed:Z
        //   518: ifeq            530
        //   521: iconst_1       
        //   522: istore_3       
        //   523: goto            530
        //   526: astore_1       
        //   527: goto            567
        //   530: aload_0        
        //   531: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mCloseCompleter:Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;
        //   534: astore_1       
        //   535: aload           10
        //   537: monitorexit    
        //   538: iload_3        
        //   539: ifeq            998
        //   542: aload           15
        //   544: invokestatic    androidx/camera/camera2/internal/\u30070OO8.\u3007080:(Landroid/media/ImageWriter;)V
        //   547: ldc             "YuvToJpegProcessor"
        //   549: ldc             "Closed after completion of last image processed."
        //   551: invokestatic    androidx/camera/core/Logger.d:(Ljava/lang/String;Ljava/lang/String;)V
        //   554: aload_1        
        //   555: ifnull          998
        //   558: aload_1        
        //   559: aconst_null    
        //   560: invokevirtual   androidx/concurrent/futures/CallbackToFutureAdapter$Completer.set:(Ljava/lang/Object;)Z
        //   563: pop            
        //   564: goto            998
        //   567: aload           10
        //   569: monitorexit    
        //   570: aload_1        
        //   571: athrow         
        //   572: astore_1       
        //   573: aconst_null    
        //   574: astore          10
        //   576: aload           11
        //   578: astore          12
        //   580: goto            768
        //   583: astore          10
        //   585: aconst_null    
        //   586: astore_1       
        //   587: goto            657
        //   590: astore          12
        //   592: aload_1        
        //   593: astore          10
        //   595: aload           12
        //   597: astore_1       
        //   598: aload           11
        //   600: astore          12
        //   602: goto            768
        //   605: astore          10
        //   607: goto            657
        //   610: astore          10
        //   612: aload           13
        //   614: astore_1       
        //   615: goto            657
        //   618: astore_1       
        //   619: aconst_null    
        //   620: astore          12
        //   622: aload           13
        //   624: astore          10
        //   626: goto            768
        //   629: astore          10
        //   631: aconst_null    
        //   632: astore          11
        //   634: aload           13
        //   636: astore_1       
        //   637: goto            657
        //   640: astore_1       
        //   641: aconst_null    
        //   642: astore          10
        //   644: aconst_null    
        //   645: astore          12
        //   647: goto            768
        //   650: astore          10
        //   652: aconst_null    
        //   653: astore_1       
        //   654: aconst_null    
        //   655: astore          11
        //   657: aload           11
        //   659: astore          12
        //   661: iload_2        
        //   662: ifeq            888
        //   665: aload_1        
        //   666: astore          14
        //   668: aload           11
        //   670: astore          12
        //   672: ldc             "YuvToJpegProcessor"
        //   674: ldc_w           "Failed to process YUV -> JPEG"
        //   677: aload           10
        //   679: invokestatic    androidx/camera/core/Logger.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   682: aload_1        
        //   683: astore          14
        //   685: aload           11
        //   687: astore          12
        //   689: aload           15
        //   691: invokestatic    androidx/camera/core/internal/\u3007\u3007888.\u3007080:(Landroid/media/ImageWriter;)Landroid/media/Image;
        //   694: astore          10
        //   696: aload_1        
        //   697: astore          14
        //   699: aload           10
        //   701: astore          12
        //   703: aload           10
        //   705: invokevirtual   android/media/Image.getPlanes:()[Landroid/media/Image$Plane;
        //   708: iconst_0       
        //   709: aaload         
        //   710: invokevirtual   android/media/Image$Plane.getBuffer:()Ljava/nio/ByteBuffer;
        //   713: astore          11
        //   715: aload_1        
        //   716: astore          14
        //   718: aload           10
        //   720: astore          12
        //   722: aload           11
        //   724: invokevirtual   java/nio/ByteBuffer.rewind:()Ljava/nio/Buffer;
        //   727: pop            
        //   728: aload_1        
        //   729: astore          14
        //   731: aload           10
        //   733: astore          12
        //   735: aload           11
        //   737: iconst_0       
        //   738: invokevirtual   java/nio/ByteBuffer.limit:(I)Ljava/nio/Buffer;
        //   741: pop            
        //   742: aload_1        
        //   743: astore          14
        //   745: aload           10
        //   747: astore          12
        //   749: aload           15
        //   751: aload           10
        //   753: invokestatic    androidx/camera/core/internal/oO80.\u3007080:(Landroid/media/ImageWriter;Landroid/media/Image;)V
        //   756: aload           10
        //   758: astore          12
        //   760: goto            888
        //   763: astore_1       
        //   764: aload           14
        //   766: astore          10
        //   768: aload_0        
        //   769: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mLock:Ljava/lang/Object;
        //   772: astore          11
        //   774: aload           11
        //   776: monitorenter   
        //   777: iload           6
        //   779: istore_3       
        //   780: iload_2        
        //   781: ifeq            822
        //   784: aload_0        
        //   785: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mProcessingImages:I
        //   788: istore_2       
        //   789: aload_0        
        //   790: iload_2        
        //   791: iconst_1       
        //   792: isub           
        //   793: putfield        androidx/camera/core/internal/YuvToJpegProcessor.mProcessingImages:I
        //   796: iload           6
        //   798: istore_3       
        //   799: iload_2        
        //   800: ifne            822
        //   803: iload           6
        //   805: istore_3       
        //   806: aload_0        
        //   807: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mClosed:Z
        //   810: ifeq            822
        //   813: iconst_1       
        //   814: istore_3       
        //   815: goto            822
        //   818: astore_1       
        //   819: goto            883
        //   822: aload_0        
        //   823: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mCloseCompleter:Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;
        //   826: astore          13
        //   828: aload           11
        //   830: monitorexit    
        //   831: aload           12
        //   833: ifnull          841
        //   836: aload           12
        //   838: invokevirtual   android/media/Image.close:()V
        //   841: aload           10
        //   843: ifnull          853
        //   846: aload           10
        //   848: invokeinterface androidx/camera/core/ImageProxy.close:()V
        //   853: iload_3        
        //   854: ifeq            881
        //   857: aload           15
        //   859: invokestatic    androidx/camera/camera2/internal/\u30070OO8.\u3007080:(Landroid/media/ImageWriter;)V
        //   862: ldc             "YuvToJpegProcessor"
        //   864: ldc             "Closed after completion of last image processed."
        //   866: invokestatic    androidx/camera/core/Logger.d:(Ljava/lang/String;Ljava/lang/String;)V
        //   869: aload           13
        //   871: ifnull          881
        //   874: aload           13
        //   876: aconst_null    
        //   877: invokevirtual   androidx/concurrent/futures/CallbackToFutureAdapter$Completer.set:(Ljava/lang/Object;)Z
        //   880: pop            
        //   881: aload_1        
        //   882: athrow         
        //   883: aload           11
        //   885: monitorexit    
        //   886: aload_1        
        //   887: athrow         
        //   888: aload_0        
        //   889: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mLock:Ljava/lang/Object;
        //   892: astore          11
        //   894: aload           11
        //   896: monitorenter   
        //   897: iload           5
        //   899: istore_3       
        //   900: iload_2        
        //   901: ifeq            942
        //   904: aload_0        
        //   905: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mProcessingImages:I
        //   908: istore_2       
        //   909: aload_0        
        //   910: iload_2        
        //   911: iconst_1       
        //   912: isub           
        //   913: putfield        androidx/camera/core/internal/YuvToJpegProcessor.mProcessingImages:I
        //   916: iload           5
        //   918: istore_3       
        //   919: iload_2        
        //   920: ifne            942
        //   923: iload           5
        //   925: istore_3       
        //   926: aload_0        
        //   927: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mClosed:Z
        //   930: ifeq            942
        //   933: iconst_1       
        //   934: istore_3       
        //   935: goto            942
        //   938: astore_1       
        //   939: goto            999
        //   942: aload_0        
        //   943: getfield        androidx/camera/core/internal/YuvToJpegProcessor.mCloseCompleter:Landroidx/concurrent/futures/CallbackToFutureAdapter$Completer;
        //   946: astore          10
        //   948: aload           11
        //   950: monitorexit    
        //   951: aload           12
        //   953: ifnull          961
        //   956: aload           12
        //   958: invokevirtual   android/media/Image.close:()V
        //   961: aload_1        
        //   962: ifnull          971
        //   965: aload_1        
        //   966: invokeinterface androidx/camera/core/ImageProxy.close:()V
        //   971: iload_3        
        //   972: ifeq            998
        //   975: aload           15
        //   977: invokestatic    androidx/camera/camera2/internal/\u30070OO8.\u3007080:(Landroid/media/ImageWriter;)V
        //   980: ldc             "YuvToJpegProcessor"
        //   982: ldc             "Closed after completion of last image processed."
        //   984: invokestatic    androidx/camera/core/Logger.d:(Ljava/lang/String;Ljava/lang/String;)V
        //   987: aload           10
        //   989: ifnull          998
        //   992: aload           10
        //   994: astore_1       
        //   995: goto            558
        //   998: return         
        //   999: aload           11
        //  1001: monitorexit    
        //  1002: aload_1        
        //  1003: athrow         
        //  1004: astore_1       
        //  1005: aload           11
        //  1007: monitorexit    
        //  1008: aload_1        
        //  1009: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  121    134    1004   1010   Any
        //  141    147    1004   1010   Any
        //  151    161    1004   1010   Any
        //  161    175    1004   1010   Any
        //  175    186    650    657    Ljava/lang/Exception;
        //  175    186    640    650    Any
        //  190    204    629    640    Ljava/lang/Exception;
        //  190    204    618    629    Any
        //  218    230    252    299    Any
        //  240    247    252    299    Any
        //  257    265    252    299    Any
        //  294    296    252    299    Any
        //  299    306    629    640    Ljava/lang/Exception;
        //  299    306    618    629    Any
        //  314    324    610    618    Ljava/lang/Exception;
        //  314    324    763    768    Any
        //  324    335    605    610    Ljava/lang/Exception;
        //  324    335    590    605    Any
        //  344    450    605    610    Ljava/lang/Exception;
        //  344    450    590    605    Any
        //  450    476    583    590    Ljava/lang/Exception;
        //  450    476    572    583    Any
        //  492    504    526    572    Any
        //  514    521    526    572    Any
        //  530    538    526    572    Any
        //  567    570    526    572    Any
        //  672    682    763    768    Any
        //  689    696    763    768    Any
        //  703    715    763    768    Any
        //  722    728    763    768    Any
        //  735    742    763    768    Any
        //  749    756    763    768    Any
        //  784    796    818    888    Any
        //  806    813    818    888    Any
        //  822    831    818    888    Any
        //  883    886    818    888    Any
        //  904    916    938    1004   Any
        //  926    933    938    1004   Any
        //  942    951    938    1004   Any
        //  999    1002   938    1004   Any
        //  1005   1008   1004   1010   Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0341:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void setJpegQuality(@IntRange(from = 0L, to = 100L) final int mQuality) {
        synchronized (this.mLock) {
            this.mQuality = mQuality;
        }
    }
    
    public void setRotationDegrees(final int mRotationDegrees) {
        synchronized (this.mLock) {
            this.mRotationDegrees = mRotationDegrees;
        }
    }
}

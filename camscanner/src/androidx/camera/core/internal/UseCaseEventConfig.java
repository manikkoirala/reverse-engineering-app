// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.internal;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.camera.core.UseCase;
import androidx.camera.core.impl.Config;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.ReadableConfig;

@RequiresApi(21)
public interface UseCaseEventConfig extends ReadableConfig
{
    public static final Option<UseCase.EventCallback> OPTION_USE_CASE_EVENT_CALLBACK = Config.Option.create("camerax.core.useCaseEventCallback", UseCase.EventCallback.class);
    
    @NonNull
    UseCase.EventCallback getUseCaseEventCallback();
    
    @Nullable
    UseCase.EventCallback getUseCaseEventCallback(@Nullable final UseCase.EventCallback p0);
    
    public interface Builder<B>
    {
        @NonNull
        B setUseCaseEventCallback(@NonNull final UseCase.EventCallback p0);
    }
}

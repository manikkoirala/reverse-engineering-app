// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.camera.core.impl.utils.ExifData;
import android.graphics.Matrix;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.TagBundle;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public abstract class ImmutableImageInfo implements ImageInfo
{
    @NonNull
    public static ImageInfo create(@NonNull final TagBundle tagBundle, final long n, final int n2, @NonNull final Matrix matrix) {
        return new AutoValue_ImmutableImageInfo(tagBundle, n, n2, matrix);
    }
    
    @Override
    public abstract int getRotationDegrees();
    
    @NonNull
    @Override
    public abstract Matrix getSensorToBufferTransformMatrix();
    
    @NonNull
    @Override
    public abstract TagBundle getTagBundle();
    
    @Override
    public abstract long getTimestamp();
    
    @Override
    public void populateExifData(@NonNull final ExifData.Builder builder) {
        builder.setOrientationDegrees(this.getRotationDegrees());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class InitializationException extends Exception
{
    public InitializationException(@Nullable final String message) {
        super(message);
    }
    
    public InitializationException(@Nullable final String message, @Nullable final Throwable cause) {
        super(message, cause);
    }
    
    public InitializationException(@Nullable final Throwable cause) {
        super(cause);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.RestrictTo;
import androidx.annotation.FloatRange;
import androidx.annotation.NonNull;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface CameraControl
{
    @NonNull
    ListenableFuture<Void> cancelFocusAndMetering();
    
    @NonNull
    ListenableFuture<Void> enableTorch(final boolean p0);
    
    @NonNull
    ListenableFuture<Integer> setExposureCompensationIndex(final int p0);
    
    @NonNull
    ListenableFuture<Void> setLinearZoom(@FloatRange(from = 0.0, to = 1.0) final float p0);
    
    @NonNull
    ListenableFuture<Void> setZoomRatio(final float p0);
    
    @NonNull
    ListenableFuture<FocusMeteringResult> startFocusAndMetering(@NonNull final FocusMeteringAction p0);
    
    public static final class OperationCanceledException extends Exception
    {
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public OperationCanceledException(@NonNull final String message) {
            super(message);
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public OperationCanceledException(@NonNull final String message, @NonNull final Throwable cause) {
            super(message, cause);
        }
    }
}

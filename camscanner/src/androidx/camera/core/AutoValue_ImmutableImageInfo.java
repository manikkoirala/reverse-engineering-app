// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.NonNull;
import androidx.camera.core.impl.TagBundle;
import android.graphics.Matrix;

final class AutoValue_ImmutableImageInfo extends ImmutableImageInfo
{
    private final int rotationDegrees;
    private final Matrix sensorToBufferTransformMatrix;
    private final TagBundle tagBundle;
    private final long timestamp;
    
    AutoValue_ImmutableImageInfo(final TagBundle tagBundle, final long timestamp, final int rotationDegrees, final Matrix sensorToBufferTransformMatrix) {
        if (tagBundle == null) {
            throw new NullPointerException("Null tagBundle");
        }
        this.tagBundle = tagBundle;
        this.timestamp = timestamp;
        this.rotationDegrees = rotationDegrees;
        if (sensorToBufferTransformMatrix != null) {
            this.sensorToBufferTransformMatrix = sensorToBufferTransformMatrix;
            return;
        }
        throw new NullPointerException("Null sensorToBufferTransformMatrix");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof ImmutableImageInfo) {
            final ImmutableImageInfo immutableImageInfo = (ImmutableImageInfo)o;
            if (!this.tagBundle.equals(immutableImageInfo.getTagBundle()) || this.timestamp != immutableImageInfo.getTimestamp() || this.rotationDegrees != immutableImageInfo.getRotationDegrees() || !this.sensorToBufferTransformMatrix.equals((Object)immutableImageInfo.getSensorToBufferTransformMatrix())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int getRotationDegrees() {
        return this.rotationDegrees;
    }
    
    @NonNull
    @Override
    public Matrix getSensorToBufferTransformMatrix() {
        return this.sensorToBufferTransformMatrix;
    }
    
    @NonNull
    @Override
    public TagBundle getTagBundle() {
        return this.tagBundle;
    }
    
    @Override
    public long getTimestamp() {
        return this.timestamp;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.tagBundle.hashCode();
        final long timestamp = this.timestamp;
        return (((hashCode ^ 0xF4243) * 1000003 ^ (int)(timestamp ^ timestamp >>> 32)) * 1000003 ^ this.rotationDegrees) * 1000003 ^ this.sensorToBufferTransformMatrix.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ImmutableImageInfo{tagBundle=");
        sb.append(this.tagBundle);
        sb.append(", timestamp=");
        sb.append(this.timestamp);
        sb.append(", rotationDegrees=");
        sb.append(this.rotationDegrees);
        sb.append(", sensorToBufferTransformMatrix=");
        sb.append(this.sensorToBufferTransformMatrix);
        sb.append("}");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.RestrictTo;
import androidx.annotation.Nullable;
import android.util.Rational;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class MeteringPoint
{
    private float mNormalizedX;
    private float mNormalizedY;
    private float mSize;
    @Nullable
    private Rational mSurfaceAspectRatio;
    
    MeteringPoint(final float mNormalizedX, final float mNormalizedY, final float mSize, @Nullable final Rational mSurfaceAspectRatio) {
        this.mNormalizedX = mNormalizedX;
        this.mNormalizedY = mNormalizedY;
        this.mSize = mSize;
        this.mSurfaceAspectRatio = mSurfaceAspectRatio;
    }
    
    public float getSize() {
        return this.mSize;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public Rational getSurfaceAspectRatio() {
        return this.mSurfaceAspectRatio;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public float getX() {
        return this.mNormalizedX;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public float getY() {
        return this.mNormalizedY;
    }
}

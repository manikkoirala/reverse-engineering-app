// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.NonNull;
import android.util.Size;
import android.graphics.Rect;

final class AutoValue_ResolutionInfo extends ResolutionInfo
{
    private final Rect cropRect;
    private final Size resolution;
    private final int rotationDegrees;
    
    AutoValue_ResolutionInfo(final Size resolution, final Rect cropRect, final int rotationDegrees) {
        if (resolution == null) {
            throw new NullPointerException("Null resolution");
        }
        this.resolution = resolution;
        if (cropRect != null) {
            this.cropRect = cropRect;
            this.rotationDegrees = rotationDegrees;
            return;
        }
        throw new NullPointerException("Null cropRect");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof ResolutionInfo) {
            final ResolutionInfo resolutionInfo = (ResolutionInfo)o;
            if (!this.resolution.equals((Object)resolutionInfo.getResolution()) || !this.cropRect.equals((Object)resolutionInfo.getCropRect()) || this.rotationDegrees != resolutionInfo.getRotationDegrees()) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @NonNull
    @Override
    public Rect getCropRect() {
        return this.cropRect;
    }
    
    @NonNull
    @Override
    public Size getResolution() {
        return this.resolution;
    }
    
    @Override
    public int getRotationDegrees() {
        return this.rotationDegrees;
    }
    
    @Override
    public int hashCode() {
        return ((this.resolution.hashCode() ^ 0xF4243) * 1000003 ^ this.cropRect.hashCode()) * 1000003 ^ this.rotationDegrees;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ResolutionInfo{resolution=");
        sb.append(this.resolution);
        sb.append(", cropRect=");
        sb.append(this.cropRect);
        sb.append(", rotationDegrees=");
        sb.append(this.rotationDegrees);
        sb.append("}");
        return sb.toString();
    }
}

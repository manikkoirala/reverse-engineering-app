// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.Nullable;
import androidx.annotation.AnyThread;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public interface ImageProcessor
{
    @NonNull
    Response process(@NonNull final Request p0);
    
    public interface Request
    {
        @AnyThread
        @NonNull
        List<ImageProxy> getInputImages();
        
        @AnyThread
        int getOutputFormat();
    }
    
    public interface Response
    {
        @AnyThread
        @Nullable
        ImageProxy getOutputImage();
    }
}

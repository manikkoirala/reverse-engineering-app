// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.Collections;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class CaptureConfig
{
    public static final Config.Option<Integer> OPTION_JPEG_QUALITY;
    public static final Config.Option<Integer> OPTION_ROTATION;
    public static final int TEMPLATE_TYPE_NONE = -1;
    final List<CameraCaptureCallback> mCameraCaptureCallbacks;
    @Nullable
    private final CameraCaptureResult mCameraCaptureResult;
    final Config mImplementationOptions;
    final List<DeferrableSurface> mSurfaces;
    @NonNull
    private final TagBundle mTagBundle;
    final int mTemplateType;
    private final boolean mUseRepeatingSurface;
    
    static {
        OPTION_ROTATION = (Config.Option)Config.Option.create("camerax.core.captureConfig.rotation", Integer.TYPE);
        OPTION_JPEG_QUALITY = (Config.Option)Config.Option.create("camerax.core.captureConfig.jpegQuality", Integer.class);
    }
    
    CaptureConfig(final List<DeferrableSurface> mSurfaces, final Config mImplementationOptions, final int mTemplateType, final List<CameraCaptureCallback> list, final boolean mUseRepeatingSurface, @NonNull final TagBundle mTagBundle, @Nullable final CameraCaptureResult mCameraCaptureResult) {
        this.mSurfaces = mSurfaces;
        this.mImplementationOptions = mImplementationOptions;
        this.mTemplateType = mTemplateType;
        this.mCameraCaptureCallbacks = Collections.unmodifiableList((List<? extends CameraCaptureCallback>)list);
        this.mUseRepeatingSurface = mUseRepeatingSurface;
        this.mTagBundle = mTagBundle;
        this.mCameraCaptureResult = mCameraCaptureResult;
    }
    
    @NonNull
    public static CaptureConfig defaultEmptyCaptureConfig() {
        return new Builder().build();
    }
    
    @NonNull
    public List<CameraCaptureCallback> getCameraCaptureCallbacks() {
        return this.mCameraCaptureCallbacks;
    }
    
    @Nullable
    public CameraCaptureResult getCameraCaptureResult() {
        return this.mCameraCaptureResult;
    }
    
    @NonNull
    public Config getImplementationOptions() {
        return this.mImplementationOptions;
    }
    
    @NonNull
    public List<DeferrableSurface> getSurfaces() {
        return Collections.unmodifiableList((List<? extends DeferrableSurface>)this.mSurfaces);
    }
    
    @NonNull
    public TagBundle getTagBundle() {
        return this.mTagBundle;
    }
    
    public int getTemplateType() {
        return this.mTemplateType;
    }
    
    public boolean isUseRepeatingSurface() {
        return this.mUseRepeatingSurface;
    }
    
    public static final class Builder
    {
        private List<CameraCaptureCallback> mCameraCaptureCallbacks;
        @Nullable
        private CameraCaptureResult mCameraCaptureResult;
        private MutableConfig mImplementationOptions;
        private MutableTagBundle mMutableTagBundle;
        private final Set<DeferrableSurface> mSurfaces;
        private int mTemplateType;
        private boolean mUseRepeatingSurface;
        
        public Builder() {
            this.mSurfaces = new HashSet<DeferrableSurface>();
            this.mImplementationOptions = MutableOptionsBundle.create();
            this.mTemplateType = -1;
            this.mCameraCaptureCallbacks = new ArrayList<CameraCaptureCallback>();
            this.mUseRepeatingSurface = false;
            this.mMutableTagBundle = MutableTagBundle.create();
        }
        
        private Builder(final CaptureConfig captureConfig) {
            final HashSet mSurfaces = new HashSet();
            this.mSurfaces = mSurfaces;
            this.mImplementationOptions = MutableOptionsBundle.create();
            this.mTemplateType = -1;
            this.mCameraCaptureCallbacks = new ArrayList<CameraCaptureCallback>();
            this.mUseRepeatingSurface = false;
            this.mMutableTagBundle = MutableTagBundle.create();
            mSurfaces.addAll(captureConfig.mSurfaces);
            this.mImplementationOptions = MutableOptionsBundle.from(captureConfig.mImplementationOptions);
            this.mTemplateType = captureConfig.mTemplateType;
            this.mCameraCaptureCallbacks.addAll(captureConfig.getCameraCaptureCallbacks());
            this.mUseRepeatingSurface = captureConfig.isUseRepeatingSurface();
            this.mMutableTagBundle = MutableTagBundle.from(captureConfig.getTagBundle());
        }
        
        @NonNull
        public static Builder createFrom(@NonNull final UseCaseConfig<?> useCaseConfig) {
            final OptionUnpacker captureOptionUnpacker = useCaseConfig.getCaptureOptionUnpacker(null);
            if (captureOptionUnpacker != null) {
                final Builder builder = new Builder();
                captureOptionUnpacker.unpack(useCaseConfig, builder);
                return builder;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Implementation is missing option unpacker for ");
            sb.append(useCaseConfig.getTargetName(useCaseConfig.toString()));
            throw new IllegalStateException(sb.toString());
        }
        
        @NonNull
        public static Builder from(@NonNull final CaptureConfig captureConfig) {
            return new Builder(captureConfig);
        }
        
        public void addAllCameraCaptureCallbacks(@NonNull final Collection<CameraCaptureCallback> collection) {
            final Iterator<CameraCaptureCallback> iterator = collection.iterator();
            while (iterator.hasNext()) {
                this.addCameraCaptureCallback(iterator.next());
            }
        }
        
        public void addAllTags(@NonNull final TagBundle tagBundle) {
            this.mMutableTagBundle.addTagBundle(tagBundle);
        }
        
        public void addCameraCaptureCallback(@NonNull final CameraCaptureCallback cameraCaptureCallback) {
            if (this.mCameraCaptureCallbacks.contains(cameraCaptureCallback)) {
                return;
            }
            this.mCameraCaptureCallbacks.add(cameraCaptureCallback);
        }
        
        public <T> void addImplementationOption(@NonNull final Config.Option<T> option, @NonNull final T t) {
            this.mImplementationOptions.insertOption(option, t);
        }
        
        public void addImplementationOptions(@NonNull final Config config) {
            for (final Config.Option<MultiValueSet> option : config.listOptions()) {
                final MultiValueSet retrieveOption = this.mImplementationOptions.retrieveOption(option, null);
                final ValueT retrieveOption2 = config.retrieveOption((Config.Option<ValueT>)option);
                if (retrieveOption instanceof MultiValueSet) {
                    retrieveOption.addAll(((MultiValueSet)retrieveOption2).getAllItems());
                }
                else {
                    MultiValueSet clone = (MultiValueSet)retrieveOption2;
                    if (retrieveOption2 instanceof MultiValueSet) {
                        clone = ((MultiValueSet)retrieveOption2).clone();
                    }
                    this.mImplementationOptions.insertOption(option, config.getOptionPriority(option), clone);
                }
            }
        }
        
        public void addSurface(@NonNull final DeferrableSurface deferrableSurface) {
            this.mSurfaces.add(deferrableSurface);
        }
        
        public void addTag(@NonNull final String s, @NonNull final Object o) {
            this.mMutableTagBundle.putTag(s, o);
        }
        
        @NonNull
        public CaptureConfig build() {
            return new CaptureConfig(new ArrayList<DeferrableSurface>(this.mSurfaces), OptionsBundle.from(this.mImplementationOptions), this.mTemplateType, this.mCameraCaptureCallbacks, this.mUseRepeatingSurface, TagBundle.from(this.mMutableTagBundle), this.mCameraCaptureResult);
        }
        
        public void clearSurfaces() {
            this.mSurfaces.clear();
        }
        
        @NonNull
        public Config getImplementationOptions() {
            return this.mImplementationOptions;
        }
        
        @NonNull
        public Set<DeferrableSurface> getSurfaces() {
            return this.mSurfaces;
        }
        
        @Nullable
        public Object getTag(@NonNull final String s) {
            return this.mMutableTagBundle.getTag(s);
        }
        
        public int getTemplateType() {
            return this.mTemplateType;
        }
        
        public boolean isUseRepeatingSurface() {
            return this.mUseRepeatingSurface;
        }
        
        public boolean removeCameraCaptureCallback(@NonNull final CameraCaptureCallback cameraCaptureCallback) {
            return this.mCameraCaptureCallbacks.remove(cameraCaptureCallback);
        }
        
        public void removeSurface(@NonNull final DeferrableSurface deferrableSurface) {
            this.mSurfaces.remove(deferrableSurface);
        }
        
        public void setCameraCaptureResult(@NonNull final CameraCaptureResult mCameraCaptureResult) {
            this.mCameraCaptureResult = mCameraCaptureResult;
        }
        
        public void setImplementationOptions(@NonNull final Config config) {
            this.mImplementationOptions = MutableOptionsBundle.from(config);
        }
        
        public void setTemplateType(final int mTemplateType) {
            this.mTemplateType = mTemplateType;
        }
        
        public void setUseRepeatingSurface(final boolean mUseRepeatingSurface) {
            this.mUseRepeatingSurface = mUseRepeatingSurface;
        }
    }
    
    public interface OptionUnpacker
    {
        void unpack(@NonNull final UseCaseConfig<?> p0, @NonNull final Builder p1);
    }
}

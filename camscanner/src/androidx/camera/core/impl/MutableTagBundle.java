// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.Iterator;
import androidx.annotation.NonNull;
import android.util.ArrayMap;
import java.util.Map;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class MutableTagBundle extends TagBundle
{
    private MutableTagBundle(final Map<String, Object> map) {
        super(map);
    }
    
    @NonNull
    public static MutableTagBundle create() {
        return new MutableTagBundle((Map<String, Object>)new ArrayMap());
    }
    
    @NonNull
    public static MutableTagBundle from(@NonNull final TagBundle tagBundle) {
        final ArrayMap arrayMap = new ArrayMap();
        for (final String s : tagBundle.listKeys()) {
            ((Map<String, Object>)arrayMap).put(s, tagBundle.getTag(s));
        }
        return new MutableTagBundle((Map<String, Object>)arrayMap);
    }
    
    public void addTagBundle(@NonNull final TagBundle tagBundle) {
        final Map<String, Object> mTagMap = super.mTagMap;
        if (mTagMap != null) {
            final Map<String, Object> mTagMap2 = tagBundle.mTagMap;
            if (mTagMap2 != null) {
                mTagMap.putAll(mTagMap2);
            }
        }
    }
    
    public void putTag(@NonNull final String s, @NonNull final Object o) {
        super.mTagMap.put(s, o);
    }
}

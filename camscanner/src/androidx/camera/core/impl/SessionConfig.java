// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.Logger;
import java.util.Arrays;
import androidx.camera.core.internal.compat.workaround.SurfaceSorter;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.Iterator;
import androidx.annotation.NonNull;
import java.util.ArrayList;
import java.util.Collections;
import android.hardware.camera2.CameraCaptureSession$StateCallback;
import androidx.annotation.Nullable;
import android.hardware.camera2.params.InputConfiguration;
import android.hardware.camera2.CameraDevice$StateCallback;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class SessionConfig
{
    private final List<CameraDevice$StateCallback> mDeviceStateCallbacks;
    private final List<ErrorListener> mErrorListeners;
    @Nullable
    private InputConfiguration mInputConfiguration;
    private final List<OutputConfig> mOutputConfigs;
    private final CaptureConfig mRepeatingCaptureConfig;
    private final List<CameraCaptureSession$StateCallback> mSessionStateCallbacks;
    private final List<CameraCaptureCallback> mSingleCameraCaptureCallbacks;
    
    SessionConfig(final List<OutputConfig> mOutputConfigs, final List<CameraDevice$StateCallback> list, final List<CameraCaptureSession$StateCallback> list2, final List<CameraCaptureCallback> list3, final List<ErrorListener> list4, final CaptureConfig mRepeatingCaptureConfig, @Nullable final InputConfiguration mInputConfiguration) {
        this.mOutputConfigs = mOutputConfigs;
        this.mDeviceStateCallbacks = Collections.unmodifiableList((List<? extends CameraDevice$StateCallback>)list);
        this.mSessionStateCallbacks = Collections.unmodifiableList((List<? extends CameraCaptureSession$StateCallback>)list2);
        this.mSingleCameraCaptureCallbacks = Collections.unmodifiableList((List<? extends CameraCaptureCallback>)list3);
        this.mErrorListeners = Collections.unmodifiableList((List<? extends ErrorListener>)list4);
        this.mRepeatingCaptureConfig = mRepeatingCaptureConfig;
        this.mInputConfiguration = mInputConfiguration;
    }
    
    @NonNull
    public static SessionConfig defaultEmptySessionConfig() {
        return new SessionConfig(new ArrayList<OutputConfig>(), new ArrayList<CameraDevice$StateCallback>(0), new ArrayList<CameraCaptureSession$StateCallback>(0), new ArrayList<CameraCaptureCallback>(0), new ArrayList<ErrorListener>(0), new CaptureConfig.Builder().build(), null);
    }
    
    @NonNull
    public List<CameraDevice$StateCallback> getDeviceStateCallbacks() {
        return this.mDeviceStateCallbacks;
    }
    
    @NonNull
    public List<ErrorListener> getErrorListeners() {
        return this.mErrorListeners;
    }
    
    @NonNull
    public Config getImplementationOptions() {
        return this.mRepeatingCaptureConfig.getImplementationOptions();
    }
    
    @Nullable
    public InputConfiguration getInputConfiguration() {
        return this.mInputConfiguration;
    }
    
    @NonNull
    public List<OutputConfig> getOutputConfigs() {
        return this.mOutputConfigs;
    }
    
    @NonNull
    public List<CameraCaptureCallback> getRepeatingCameraCaptureCallbacks() {
        return this.mRepeatingCaptureConfig.getCameraCaptureCallbacks();
    }
    
    @NonNull
    public CaptureConfig getRepeatingCaptureConfig() {
        return this.mRepeatingCaptureConfig;
    }
    
    @NonNull
    public List<CameraCaptureSession$StateCallback> getSessionStateCallbacks() {
        return this.mSessionStateCallbacks;
    }
    
    @NonNull
    public List<CameraCaptureCallback> getSingleCameraCaptureCallbacks() {
        return this.mSingleCameraCaptureCallbacks;
    }
    
    @NonNull
    public List<DeferrableSurface> getSurfaces() {
        final ArrayList list = new ArrayList();
        for (final OutputConfig outputConfig : this.mOutputConfigs) {
            list.add(outputConfig.getSurface());
            final Iterator<DeferrableSurface> iterator2 = outputConfig.getSharedSurfaces().iterator();
            while (iterator2.hasNext()) {
                list.add(iterator2.next());
            }
        }
        return (List<DeferrableSurface>)Collections.unmodifiableList((List<?>)list);
    }
    
    public int getTemplateType() {
        return this.mRepeatingCaptureConfig.getTemplateType();
    }
    
    static class BaseBuilder
    {
        final CaptureConfig.Builder mCaptureConfigBuilder;
        final List<CameraDevice$StateCallback> mDeviceStateCallbacks;
        final List<ErrorListener> mErrorListeners;
        @Nullable
        InputConfiguration mInputConfiguration;
        final Set<OutputConfig> mOutputConfigs;
        final List<CameraCaptureSession$StateCallback> mSessionStateCallbacks;
        final List<CameraCaptureCallback> mSingleCameraCaptureCallbacks;
        
        BaseBuilder() {
            this.mOutputConfigs = new LinkedHashSet<OutputConfig>();
            this.mCaptureConfigBuilder = new CaptureConfig.Builder();
            this.mDeviceStateCallbacks = new ArrayList<CameraDevice$StateCallback>();
            this.mSessionStateCallbacks = new ArrayList<CameraCaptureSession$StateCallback>();
            this.mErrorListeners = new ArrayList<ErrorListener>();
            this.mSingleCameraCaptureCallbacks = new ArrayList<CameraCaptureCallback>();
        }
    }
    
    public static class Builder extends BaseBuilder
    {
        @NonNull
        public static Builder createFrom(@NonNull final UseCaseConfig<?> useCaseConfig) {
            final OptionUnpacker sessionOptionUnpacker = useCaseConfig.getSessionOptionUnpacker(null);
            if (sessionOptionUnpacker != null) {
                final Builder builder = new Builder();
                sessionOptionUnpacker.unpack(useCaseConfig, builder);
                return builder;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Implementation is missing option unpacker for ");
            sb.append(useCaseConfig.getTargetName(useCaseConfig.toString()));
            throw new IllegalStateException(sb.toString());
        }
        
        @NonNull
        public Builder addAllCameraCaptureCallbacks(@NonNull final Collection<CameraCaptureCallback> collection) {
            for (final CameraCaptureCallback cameraCaptureCallback : collection) {
                super.mCaptureConfigBuilder.addCameraCaptureCallback(cameraCaptureCallback);
                if (!super.mSingleCameraCaptureCallbacks.contains(cameraCaptureCallback)) {
                    super.mSingleCameraCaptureCallbacks.add(cameraCaptureCallback);
                }
            }
            return this;
        }
        
        @NonNull
        public Builder addAllDeviceStateCallbacks(@NonNull final Collection<CameraDevice$StateCallback> collection) {
            final Iterator<CameraDevice$StateCallback> iterator = collection.iterator();
            while (iterator.hasNext()) {
                this.addDeviceStateCallback(iterator.next());
            }
            return this;
        }
        
        @NonNull
        public Builder addAllRepeatingCameraCaptureCallbacks(@NonNull final Collection<CameraCaptureCallback> collection) {
            super.mCaptureConfigBuilder.addAllCameraCaptureCallbacks(collection);
            return this;
        }
        
        @NonNull
        public Builder addAllSessionStateCallbacks(@NonNull final List<CameraCaptureSession$StateCallback> list) {
            final Iterator<CameraCaptureSession$StateCallback> iterator = list.iterator();
            while (iterator.hasNext()) {
                this.addSessionStateCallback(iterator.next());
            }
            return this;
        }
        
        @NonNull
        public Builder addCameraCaptureCallback(@NonNull final CameraCaptureCallback cameraCaptureCallback) {
            super.mCaptureConfigBuilder.addCameraCaptureCallback(cameraCaptureCallback);
            if (!super.mSingleCameraCaptureCallbacks.contains(cameraCaptureCallback)) {
                super.mSingleCameraCaptureCallbacks.add(cameraCaptureCallback);
            }
            return this;
        }
        
        @NonNull
        public Builder addDeviceStateCallback(@NonNull final CameraDevice$StateCallback cameraDevice$StateCallback) {
            if (super.mDeviceStateCallbacks.contains(cameraDevice$StateCallback)) {
                return this;
            }
            super.mDeviceStateCallbacks.add(cameraDevice$StateCallback);
            return this;
        }
        
        @NonNull
        public Builder addErrorListener(@NonNull final ErrorListener errorListener) {
            super.mErrorListeners.add(errorListener);
            return this;
        }
        
        @NonNull
        public Builder addImplementationOptions(@NonNull final Config config) {
            super.mCaptureConfigBuilder.addImplementationOptions(config);
            return this;
        }
        
        @NonNull
        public Builder addNonRepeatingSurface(@NonNull final DeferrableSurface deferrableSurface) {
            super.mOutputConfigs.add(OutputConfig.builder(deferrableSurface).build());
            return this;
        }
        
        @NonNull
        public Builder addOutputConfig(@NonNull final OutputConfig outputConfig) {
            super.mOutputConfigs.add(outputConfig);
            super.mCaptureConfigBuilder.addSurface(outputConfig.getSurface());
            final Iterator<DeferrableSurface> iterator = outputConfig.getSharedSurfaces().iterator();
            while (iterator.hasNext()) {
                super.mCaptureConfigBuilder.addSurface(iterator.next());
            }
            return this;
        }
        
        @NonNull
        public Builder addRepeatingCameraCaptureCallback(@NonNull final CameraCaptureCallback cameraCaptureCallback) {
            super.mCaptureConfigBuilder.addCameraCaptureCallback(cameraCaptureCallback);
            return this;
        }
        
        @NonNull
        public Builder addSessionStateCallback(@NonNull final CameraCaptureSession$StateCallback cameraCaptureSession$StateCallback) {
            if (super.mSessionStateCallbacks.contains(cameraCaptureSession$StateCallback)) {
                return this;
            }
            super.mSessionStateCallbacks.add(cameraCaptureSession$StateCallback);
            return this;
        }
        
        @NonNull
        public Builder addSurface(@NonNull final DeferrableSurface deferrableSurface) {
            super.mOutputConfigs.add(OutputConfig.builder(deferrableSurface).build());
            super.mCaptureConfigBuilder.addSurface(deferrableSurface);
            return this;
        }
        
        @NonNull
        public Builder addTag(@NonNull final String s, @NonNull final Object o) {
            super.mCaptureConfigBuilder.addTag(s, o);
            return this;
        }
        
        @NonNull
        public SessionConfig build() {
            return new SessionConfig(new ArrayList<OutputConfig>(super.mOutputConfigs), super.mDeviceStateCallbacks, super.mSessionStateCallbacks, super.mSingleCameraCaptureCallbacks, super.mErrorListeners, super.mCaptureConfigBuilder.build(), super.mInputConfiguration);
        }
        
        @NonNull
        public Builder clearSurfaces() {
            super.mOutputConfigs.clear();
            super.mCaptureConfigBuilder.clearSurfaces();
            return this;
        }
        
        @NonNull
        public List<CameraCaptureCallback> getSingleCameraCaptureCallbacks() {
            return Collections.unmodifiableList((List<? extends CameraCaptureCallback>)super.mSingleCameraCaptureCallbacks);
        }
        
        public boolean removeCameraCaptureCallback(@NonNull final CameraCaptureCallback cameraCaptureCallback) {
            final boolean removeCameraCaptureCallback = super.mCaptureConfigBuilder.removeCameraCaptureCallback(cameraCaptureCallback);
            final boolean remove = super.mSingleCameraCaptureCallbacks.remove(cameraCaptureCallback);
            return removeCameraCaptureCallback || remove;
        }
        
        @NonNull
        public Builder removeSurface(@NonNull final DeferrableSurface obj) {
            while (true) {
                for (final Object o : super.mOutputConfigs) {
                    if (((OutputConfig)o).getSurface().equals(obj)) {
                        if (o != null) {
                            super.mOutputConfigs.remove(o);
                        }
                        super.mCaptureConfigBuilder.removeSurface(obj);
                        return this;
                    }
                }
                Object o = null;
                continue;
            }
        }
        
        @NonNull
        public Builder setImplementationOptions(@NonNull final Config implementationOptions) {
            super.mCaptureConfigBuilder.setImplementationOptions(implementationOptions);
            return this;
        }
        
        @NonNull
        public Builder setInputConfiguration(@Nullable final InputConfiguration mInputConfiguration) {
            super.mInputConfiguration = mInputConfiguration;
            return this;
        }
        
        @NonNull
        public Builder setTemplateType(final int templateType) {
            super.mCaptureConfigBuilder.setTemplateType(templateType);
            return this;
        }
    }
    
    public interface ErrorListener
    {
        void onError(@NonNull final SessionConfig p0, @NonNull final SessionError p1);
    }
    
    public interface OptionUnpacker
    {
        void unpack(@NonNull final UseCaseConfig<?> p0, @NonNull final SessionConfig.Builder p1);
    }
    
    public abstract static class OutputConfig
    {
        public static final int SURFACE_GROUP_ID_NONE = -1;
        
        @NonNull
        public static Builder builder(@NonNull final DeferrableSurface surface) {
            return new AutoValue_SessionConfig_OutputConfig.Builder().setSurface(surface).setSharedSurfaces(Collections.emptyList()).setPhysicalCameraId(null).setSurfaceGroupId(-1);
        }
        
        @Nullable
        public abstract String getPhysicalCameraId();
        
        @NonNull
        public abstract List<DeferrableSurface> getSharedSurfaces();
        
        @NonNull
        public abstract DeferrableSurface getSurface();
        
        public abstract int getSurfaceGroupId();
        
        public abstract static class Builder
        {
            @NonNull
            public abstract OutputConfig build();
            
            @NonNull
            public abstract Builder setPhysicalCameraId(@Nullable final String p0);
            
            @NonNull
            public abstract Builder setSharedSurfaces(@NonNull final List<DeferrableSurface> p0);
            
            @NonNull
            public abstract Builder setSurface(@NonNull final DeferrableSurface p0);
            
            @NonNull
            public abstract Builder setSurfaceGroupId(final int p0);
        }
    }
    
    public enum SessionError
    {
        private static final SessionError[] $VALUES;
        
        SESSION_ERROR_SURFACE_NEEDS_RESET, 
        SESSION_ERROR_UNKNOWN;
    }
    
    public static final class ValidatingBuilder extends BaseBuilder
    {
        private static final List<Integer> SUPPORTED_TEMPLATE_PRIORITY;
        private static final String TAG = "ValidatingBuilder";
        private final SurfaceSorter mSurfaceSorter;
        private boolean mTemplateSet;
        private boolean mValid;
        
        static {
            SUPPORTED_TEMPLATE_PRIORITY = Arrays.asList(1, 5, 3);
        }
        
        public ValidatingBuilder() {
            this.mSurfaceSorter = new SurfaceSorter();
            this.mValid = true;
            this.mTemplateSet = false;
        }
        
        private List<DeferrableSurface> getSurfaces() {
            final ArrayList list = new ArrayList();
            for (final OutputConfig outputConfig : super.mOutputConfigs) {
                list.add(outputConfig.getSurface());
                final Iterator<DeferrableSurface> iterator2 = outputConfig.getSharedSurfaces().iterator();
                while (iterator2.hasNext()) {
                    list.add(iterator2.next());
                }
            }
            return list;
        }
        
        private int selectTemplateType(int i, final int j) {
            final List<Integer> supported_TEMPLATE_PRIORITY = ValidatingBuilder.SUPPORTED_TEMPLATE_PRIORITY;
            if (supported_TEMPLATE_PRIORITY.indexOf(i) < supported_TEMPLATE_PRIORITY.indexOf(j)) {
                i = j;
            }
            return i;
        }
        
        public void add(@NonNull final SessionConfig sessionConfig) {
            final CaptureConfig repeatingCaptureConfig = sessionConfig.getRepeatingCaptureConfig();
            if (repeatingCaptureConfig.getTemplateType() != -1) {
                this.mTemplateSet = true;
                super.mCaptureConfigBuilder.setTemplateType(this.selectTemplateType(repeatingCaptureConfig.getTemplateType(), super.mCaptureConfigBuilder.getTemplateType()));
            }
            super.mCaptureConfigBuilder.addAllTags(sessionConfig.getRepeatingCaptureConfig().getTagBundle());
            super.mDeviceStateCallbacks.addAll(sessionConfig.getDeviceStateCallbacks());
            super.mSessionStateCallbacks.addAll(sessionConfig.getSessionStateCallbacks());
            super.mCaptureConfigBuilder.addAllCameraCaptureCallbacks(sessionConfig.getRepeatingCameraCaptureCallbacks());
            super.mSingleCameraCaptureCallbacks.addAll(sessionConfig.getSingleCameraCaptureCallbacks());
            super.mErrorListeners.addAll(sessionConfig.getErrorListeners());
            if (sessionConfig.getInputConfiguration() != null) {
                super.mInputConfiguration = sessionConfig.getInputConfiguration();
            }
            super.mOutputConfigs.addAll(sessionConfig.getOutputConfigs());
            super.mCaptureConfigBuilder.getSurfaces().addAll(repeatingCaptureConfig.getSurfaces());
            if (!this.getSurfaces().containsAll(super.mCaptureConfigBuilder.getSurfaces())) {
                Logger.d("ValidatingBuilder", "Invalid configuration due to capture request surfaces are not a subset of surfaces");
                this.mValid = false;
            }
            super.mCaptureConfigBuilder.addImplementationOptions(repeatingCaptureConfig.getImplementationOptions());
        }
        
        public <T> void addImplementationOption(@NonNull final Config.Option<T> option, @NonNull final T t) {
            super.mCaptureConfigBuilder.addImplementationOption(option, t);
        }
        
        @NonNull
        public SessionConfig build() {
            if (this.mValid) {
                final ArrayList list = new ArrayList(super.mOutputConfigs);
                this.mSurfaceSorter.sort(list);
                return new SessionConfig(list, super.mDeviceStateCallbacks, super.mSessionStateCallbacks, super.mSingleCameraCaptureCallbacks, super.mErrorListeners, super.mCaptureConfigBuilder.build(), super.mInputConfiguration);
            }
            throw new IllegalArgumentException("Unsupported session configuration combination");
        }
        
        public void clearSurfaces() {
            super.mOutputConfigs.clear();
            super.mCaptureConfigBuilder.clearSurfaces();
        }
        
        public boolean isValid() {
            return this.mTemplateSet && this.mValid;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.Nullable;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface Observable<T>
{
    void addObserver(@NonNull final Executor p0, @NonNull final Observer<? super T> p1);
    
    @NonNull
    ListenableFuture<T> fetchData();
    
    void removeObserver(@NonNull final Observer<? super T> p0);
    
    public interface Observer<T>
    {
        void onError(@NonNull final Throwable p0);
        
        void onNewData(@Nullable final T p0);
    }
}

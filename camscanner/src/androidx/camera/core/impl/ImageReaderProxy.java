// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.RestrictTo;
import java.util.concurrent.Executor;
import androidx.annotation.NonNull;
import android.view.Surface;
import androidx.annotation.Nullable;
import androidx.camera.core.ImageProxy;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface ImageReaderProxy
{
    @Nullable
    ImageProxy acquireLatestImage();
    
    @Nullable
    ImageProxy acquireNextImage();
    
    void clearOnImageAvailableListener();
    
    void close();
    
    int getHeight();
    
    int getImageFormat();
    
    int getMaxImages();
    
    @Nullable
    Surface getSurface();
    
    int getWidth();
    
    void setOnImageAvailableListener(@NonNull final OnImageAvailableListener p0, @NonNull final Executor p1);
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public interface OnImageAvailableListener
    {
        void onImageAvailable(@NonNull final ImageReaderProxy p0);
    }
}

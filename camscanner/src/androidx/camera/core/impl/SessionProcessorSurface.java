// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.impl.utils.futures.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.annotation.NonNull;
import android.view.Surface;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class SessionProcessorSurface extends DeferrableSurface
{
    private final int mOutputConfigId;
    private final Surface mSurface;
    
    public SessionProcessorSurface(@NonNull final Surface mSurface, final int mOutputConfigId) {
        this.mSurface = mSurface;
        this.mOutputConfigId = mOutputConfigId;
    }
    
    public int getOutputConfigId() {
        return this.mOutputConfigId;
    }
    
    @NonNull
    public ListenableFuture<Surface> provideSurface() {
        return Futures.immediateFuture(this.mSurface);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.NonNull;
import java.util.HashMap;
import androidx.annotation.GuardedBy;
import java.util.Map;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class ExtendedCameraConfigProviderStore
{
    @GuardedBy("LOCK")
    private static final Map<Object, CameraConfigProvider> CAMERA_CONFIG_PROVIDERS;
    private static final Object LOCK;
    
    static {
        LOCK = new Object();
        CAMERA_CONFIG_PROVIDERS = new HashMap<Object, CameraConfigProvider>();
    }
    
    private ExtendedCameraConfigProviderStore() {
    }
    
    public static void addConfig(@NonNull final Object o, @NonNull final CameraConfigProvider cameraConfigProvider) {
        synchronized (ExtendedCameraConfigProviderStore.LOCK) {
            ExtendedCameraConfigProviderStore.CAMERA_CONFIG_PROVIDERS.put(o, cameraConfigProvider);
        }
    }
    
    @NonNull
    public static CameraConfigProvider getConfigProvider(@NonNull final Object o) {
        synchronized (ExtendedCameraConfigProviderStore.LOCK) {
            final CameraConfigProvider cameraConfigProvider = ExtendedCameraConfigProviderStore.CAMERA_CONFIG_PROVIDERS.get(o);
            monitorexit(ExtendedCameraConfigProviderStore.LOCK);
            CameraConfigProvider empty = cameraConfigProvider;
            if (cameraConfigProvider == null) {
                empty = CameraConfigProvider.EMPTY;
            }
            return empty;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface MutableConfig extends Config
{
     <ValueT> void insertOption(@NonNull final Option<ValueT> p0, @NonNull final OptionPriority p1, @Nullable final ValueT p2);
    
     <ValueT> void insertOption(@NonNull final Option<ValueT> p0, @Nullable final ValueT p1);
    
    @Nullable
     <ValueT> ValueT removeOption(@NonNull final Option<ValueT> p0);
}

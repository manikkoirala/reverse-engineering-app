// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.Iterator;
import android.util.ArrayMap;
import androidx.annotation.NonNull;
import java.util.Map;
import java.util.TreeMap;
import java.util.Comparator;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class OptionsBundle implements Config
{
    private static final OptionsBundle EMPTY_BUNDLE;
    protected static final Comparator<Option<?>> ID_COMPARE;
    protected final TreeMap<Option<?>, Map<OptionPriority, Object>> mOptions;
    
    static {
        EMPTY_BUNDLE = new OptionsBundle(new TreeMap<Option<?>, Map<OptionPriority, Object>>(ID_COMPARE = new \u30070000OOO()));
    }
    
    OptionsBundle(final TreeMap<Option<?>, Map<OptionPriority, Object>> mOptions) {
        this.mOptions = mOptions;
    }
    
    @NonNull
    public static OptionsBundle emptyBundle() {
        return OptionsBundle.EMPTY_BUNDLE;
    }
    
    @NonNull
    public static OptionsBundle from(@NonNull final Config config) {
        if (OptionsBundle.class.equals(config.getClass())) {
            return (OptionsBundle)config;
        }
        final TreeMap treeMap = new TreeMap((Comparator<? super K>)OptionsBundle.ID_COMPARE);
        for (final Option<?> key : config.listOptions()) {
            final Set<OptionPriority> priorities = config.getPriorities(key);
            final ArrayMap value = new ArrayMap();
            for (final OptionPriority optionPriority : priorities) {
                ((Map<OptionPriority, Object>)value).put(optionPriority, config.retrieveOptionWithPriority(key, optionPriority));
            }
            treeMap.put(key, value);
        }
        return new OptionsBundle(treeMap);
    }
    
    @Override
    public boolean containsOption(@NonNull final Option<?> key) {
        return this.mOptions.containsKey(key);
    }
    
    @Override
    public void findOptions(@NonNull final String prefix, @NonNull final OptionMatcher optionMatcher) {
        for (final Map.Entry<Option, V> entry : this.mOptions.tailMap(Option.create(prefix, Void.class)).entrySet()) {
            if (!entry.getKey().getId().startsWith(prefix)) {
                break;
            }
            if (!optionMatcher.onOptionMatched(entry.getKey())) {
                break;
            }
        }
    }
    
    @NonNull
    @Override
    public OptionPriority getOptionPriority(@NonNull final Option<?> option) {
        final Map map = this.mOptions.get(option);
        if (map != null) {
            return (OptionPriority)Collections.min((Collection<?>)map.keySet());
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Option does not exist: ");
        sb.append(option);
        throw new IllegalArgumentException(sb.toString());
    }
    
    @NonNull
    @Override
    public Set<OptionPriority> getPriorities(@NonNull final Option<?> key) {
        final Map map = this.mOptions.get(key);
        if (map == null) {
            return Collections.emptySet();
        }
        return (Set<OptionPriority>)Collections.unmodifiableSet((Set<?>)map.keySet());
    }
    
    @NonNull
    @Override
    public Set<Option<?>> listOptions() {
        return Collections.unmodifiableSet((Set<? extends Option<?>>)this.mOptions.keySet());
    }
    
    @Nullable
    @Override
    public <ValueT> ValueT retrieveOption(@NonNull final Option<ValueT> option) {
        final Map map = this.mOptions.get(option);
        if (map != null) {
            return (ValueT)map.get(Collections.min((Collection<?>)map.keySet()));
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Option does not exist: ");
        sb.append(option);
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Nullable
    @Override
    public <ValueT> ValueT retrieveOption(@NonNull final Option<ValueT> option, @Nullable final ValueT valueT) {
        try {
            return this.retrieveOption(option);
        }
        catch (final IllegalArgumentException ex) {
            return valueT;
        }
    }
    
    @Nullable
    @Override
    public <ValueT> ValueT retrieveOptionWithPriority(@NonNull final Option<ValueT> obj, @NonNull final OptionPriority obj2) {
        final Map map = this.mOptions.get(obj);
        if (map == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Option does not exist: ");
            sb.append(obj);
            throw new IllegalArgumentException(sb.toString());
        }
        if (map.containsKey(obj2)) {
            return (ValueT)map.get(obj2);
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Option does not exist: ");
        sb2.append(obj);
        sb2.append(" with priority=");
        sb2.append(obj2);
        throw new IllegalArgumentException(sb2.toString());
    }
}

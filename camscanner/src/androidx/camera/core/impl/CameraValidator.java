// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import android.content.pm.PackageManager;
import android.os.Build;
import androidx.camera.core.Logger;
import androidx.annotation.Nullable;
import androidx.camera.core.CameraSelector;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class CameraValidator
{
    private static final String TAG = "CameraValidator";
    
    private CameraValidator() {
    }
    
    public static void validateCameras(@NonNull final Context context, @NonNull final CameraRepository cameraRepository, @Nullable final CameraSelector cameraSelector) throws CameraIdListIncorrectException {
        Integer lensFacing = null;
        Label_0038: {
            if (cameraSelector != null) {
                try {
                    if ((lensFacing = cameraSelector.getLensFacing()) == null) {
                        Logger.w("CameraValidator", "No lens facing info in the availableCamerasSelector, don't verify the camera lens facing.");
                        return;
                    }
                    break Label_0038;
                }
                catch (final IllegalStateException ex) {
                    Logger.e("CameraValidator", "Cannot get lens facing from the availableCamerasSelector don't verify the camera lens facing.", ex);
                    return;
                }
            }
            lensFacing = null;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Verifying camera lens facing on ");
        sb.append(Build.DEVICE);
        sb.append(", lensFacingInteger: ");
        sb.append(lensFacing);
        Logger.d("CameraValidator", sb.toString());
        final PackageManager packageManager = context.getPackageManager();
        try {
            if (packageManager.hasSystemFeature("android.hardware.camera") && (cameraSelector == null || lensFacing == 1)) {
                CameraSelector.DEFAULT_BACK_CAMERA.select(cameraRepository.getCameras());
            }
            if (packageManager.hasSystemFeature("android.hardware.camera.front") && (cameraSelector == null || lensFacing == 0)) {
                CameraSelector.DEFAULT_FRONT_CAMERA.select(cameraRepository.getCameras());
            }
        }
        catch (final IllegalArgumentException ex2) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Camera LensFacing verification failed, existing cameras: ");
            sb2.append(cameraRepository.getCameras());
            Logger.e("CameraValidator", sb2.toString());
            throw new CameraIdListIncorrectException("Expected camera missing from device.", ex2);
        }
    }
    
    public static class CameraIdListIncorrectException extends Exception
    {
        public CameraIdListIncorrectException(@Nullable final String message, @Nullable final Throwable cause) {
            super(message, cause);
        }
    }
}

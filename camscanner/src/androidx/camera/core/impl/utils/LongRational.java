// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
final class LongRational
{
    private final long mDenominator;
    private final long mNumerator;
    
    LongRational(final double n) {
        this((long)(n * 10000.0), 10000L);
    }
    
    LongRational(final long mNumerator, final long mDenominator) {
        this.mNumerator = mNumerator;
        this.mDenominator = mDenominator;
    }
    
    long getDenominator() {
        return this.mDenominator;
    }
    
    long getNumerator() {
        return this.mNumerator;
    }
    
    double toDouble() {
        return this.mNumerator / (double)this.mDenominator;
    }
    
    @NonNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.mNumerator);
        sb.append("/");
        sb.append(this.mDenominator);
        return sb.toString();
    }
}

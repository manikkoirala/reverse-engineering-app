// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils;

import androidx.camera.core.Logger;
import androidx.annotation.VisibleForTesting;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import android.location.Location;
import androidx.annotation.Nullable;
import java.nio.ByteBuffer;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import androidx.camera.core.ImageProxy;
import java.io.IOException;
import androidx.annotation.NonNull;
import java.io.File;
import java.util.Date;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Locale;
import androidx.exifinterface.media.ExifInterface;
import java.text.SimpleDateFormat;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class Exif
{
    private static final List<String> ALL_EXIF_TAGS;
    private static final ThreadLocal<SimpleDateFormat> DATETIME_FORMAT;
    private static final ThreadLocal<SimpleDateFormat> DATE_FORMAT;
    private static final List<String> DO_NOT_COPY_EXIF_TAGS;
    public static final long INVALID_TIMESTAMP = -1L;
    private static final String KILOMETERS_PER_HOUR = "K";
    private static final String KNOTS = "N";
    private static final String MILES_PER_HOUR = "M";
    private static final String TAG = "Exif";
    private static final ThreadLocal<SimpleDateFormat> TIME_FORMAT;
    private final ExifInterface mExifInterface;
    private boolean mRemoveTimestamp;
    
    static {
        DATE_FORMAT = new ThreadLocal<SimpleDateFormat>() {
            public SimpleDateFormat initialValue() {
                return new SimpleDateFormat("yyyy:MM:dd", Locale.US);
            }
        };
        TIME_FORMAT = new ThreadLocal<SimpleDateFormat>() {
            public SimpleDateFormat initialValue() {
                return new SimpleDateFormat("HH:mm:ss", Locale.US);
            }
        };
        DATETIME_FORMAT = new ThreadLocal<SimpleDateFormat>() {
            public SimpleDateFormat initialValue() {
                return new SimpleDateFormat("yyyy:MM:dd HH:mm:ss", Locale.US);
            }
        };
        ALL_EXIF_TAGS = getAllExifTags();
        DO_NOT_COPY_EXIF_TAGS = Arrays.asList("ImageWidth", "ImageLength", "PixelXDimension", "PixelYDimension", "Compression", "JPEGInterchangeFormat", "JPEGInterchangeFormatLength", "ThumbnailImageLength", "ThumbnailImageWidth", "ThumbnailOrientation");
    }
    
    private Exif(final ExifInterface mExifInterface) {
        this.mRemoveTimestamp = false;
        this.mExifInterface = mExifInterface;
    }
    
    private void attachLastModifiedTimestamp() {
        final long currentTimeMillis = System.currentTimeMillis();
        final String convertToExifDateTime = convertToExifDateTime(currentTimeMillis);
        this.mExifInterface.setAttribute("DateTime", convertToExifDateTime);
        try {
            this.mExifInterface.setAttribute("SubSecTime", Long.toString(currentTimeMillis - convertFromExifDateTime(convertToExifDateTime).getTime()));
        }
        catch (final ParseException ex) {}
    }
    
    private static Date convertFromExifDate(final String source) throws ParseException {
        return Exif.DATE_FORMAT.get().parse(source);
    }
    
    private static Date convertFromExifDateTime(final String source) throws ParseException {
        return Exif.DATETIME_FORMAT.get().parse(source);
    }
    
    private static Date convertFromExifTime(final String source) throws ParseException {
        return Exif.TIME_FORMAT.get().parse(source);
    }
    
    private static String convertToExifDateTime(final long date) {
        return Exif.DATETIME_FORMAT.get().format(new Date(date));
    }
    
    @NonNull
    public static Exif createFromFile(@NonNull final File file) throws IOException {
        return createFromFileString(file.toString());
    }
    
    @NonNull
    public static Exif createFromFileString(@NonNull final String s) throws IOException {
        return new Exif(new ExifInterface(s));
    }
    
    @NonNull
    public static Exif createFromImageProxy(@NonNull final ImageProxy imageProxy) throws IOException {
        final ByteBuffer buffer = imageProxy.getPlanes()[0].getBuffer();
        buffer.rewind();
        final byte[] array = new byte[buffer.capacity()];
        buffer.get(array);
        return createFromInputStream(new ByteArrayInputStream(array));
    }
    
    @NonNull
    public static Exif createFromInputStream(@NonNull final InputStream inputStream) throws IOException {
        return new Exif(new ExifInterface(inputStream));
    }
    
    @NonNull
    public static List<String> getAllExifTags() {
        return Arrays.asList("ImageWidth", "ImageLength", "BitsPerSample", "Compression", "PhotometricInterpretation", "Orientation", "SamplesPerPixel", "PlanarConfiguration", "YCbCrSubSampling", "YCbCrPositioning", "XResolution", "YResolution", "ResolutionUnit", "StripOffsets", "RowsPerStrip", "StripByteCounts", "JPEGInterchangeFormat", "JPEGInterchangeFormatLength", "TransferFunction", "WhitePoint", "PrimaryChromaticities", "YCbCrCoefficients", "ReferenceBlackWhite", "DateTime", "ImageDescription", "Make", "Model", "Software", "Artist", "Copyright", "ExifVersion", "FlashpixVersion", "ColorSpace", "Gamma", "PixelXDimension", "PixelYDimension", "ComponentsConfiguration", "CompressedBitsPerPixel", "MakerNote", "UserComment", "RelatedSoundFile", "DateTimeOriginal", "DateTimeDigitized", "OffsetTime", "OffsetTimeOriginal", "OffsetTimeDigitized", "SubSecTime", "SubSecTimeOriginal", "SubSecTimeDigitized", "ExposureTime", "FNumber", "ExposureProgram", "SpectralSensitivity", "PhotographicSensitivity", "OECF", "SensitivityType", "StandardOutputSensitivity", "RecommendedExposureIndex", "ISOSpeed", "ISOSpeedLatitudeyyy", "ISOSpeedLatitudezzz", "ShutterSpeedValue", "ApertureValue", "BrightnessValue", "ExposureBiasValue", "MaxApertureValue", "SubjectDistance", "MeteringMode", "LightSource", "Flash", "SubjectArea", "FocalLength", "FlashEnergy", "SpatialFrequencyResponse", "FocalPlaneXResolution", "FocalPlaneYResolution", "FocalPlaneResolutionUnit", "SubjectLocation", "ExposureIndex", "SensingMethod", "FileSource", "SceneType", "CFAPattern", "CustomRendered", "ExposureMode", "WhiteBalance", "DigitalZoomRatio", "FocalLengthIn35mmFilm", "SceneCaptureType", "GainControl", "Contrast", "Saturation", "Sharpness", "DeviceSettingDescription", "SubjectDistanceRange", "ImageUniqueID", "CameraOwnerName", "BodySerialNumber", "LensSpecification", "LensMake", "LensModel", "LensSerialNumber", "GPSVersionID", "GPSLatitudeRef", "GPSLatitude", "GPSLongitudeRef", "GPSLongitude", "GPSAltitudeRef", "GPSAltitude", "GPSTimeStamp", "GPSSatellites", "GPSStatus", "GPSMeasureMode", "GPSDOP", "GPSSpeedRef", "GPSSpeed", "GPSTrackRef", "GPSTrack", "GPSImgDirectionRef", "GPSImgDirection", "GPSMapDatum", "GPSDestLatitudeRef", "GPSDestLatitude", "GPSDestLongitudeRef", "GPSDestLongitude", "GPSDestBearingRef", "GPSDestBearing", "GPSDestDistanceRef", "GPSDestDistance", "GPSProcessingMethod", "GPSAreaInformation", "GPSDateStamp", "GPSDifferential", "GPSHPositioningError", "InteroperabilityIndex", "ThumbnailImageLength", "ThumbnailImageWidth", "ThumbnailOrientation", "DNGVersion", "DefaultCropSize", "ThumbnailImage", "PreviewImageStart", "PreviewImageLength", "AspectFrame", "SensorBottomBorder", "SensorLeftBorder", "SensorRightBorder", "SensorTopBorder", "ISO", "JpgFromRaw", "Xmp", "NewSubfileType", "SubfileType");
    }
    
    private long parseTimestamp(@Nullable final String s) {
        final long n = -1L;
        if (s == null) {
            return -1L;
        }
        try {
            return convertFromExifDateTime(s).getTime();
        }
        catch (final ParseException ex) {
            return n;
        }
    }
    
    private long parseTimestamp(@Nullable final String str, @Nullable final String str2) {
        if (str == null && str2 == null) {
            return -1L;
        }
        if (str2 == null) {
            try {
                return convertFromExifDate(str).getTime();
            }
            catch (final ParseException ex) {
                return -1L;
            }
        }
        if (str == null) {
            try {
                return convertFromExifTime(str2).getTime();
            }
            catch (final ParseException ex2) {
                return -1L;
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" ");
        sb.append(str2);
        return this.parseTimestamp(sb.toString());
    }
    
    public void attachLocation(@NonNull final Location gpsInfo) {
        this.mExifInterface.setGpsInfo(gpsInfo);
    }
    
    public void attachTimestamp() {
        final long currentTimeMillis = System.currentTimeMillis();
        final String convertToExifDateTime = convertToExifDateTime(currentTimeMillis);
        this.mExifInterface.setAttribute("DateTimeOriginal", convertToExifDateTime);
        this.mExifInterface.setAttribute("DateTimeDigitized", convertToExifDateTime);
        while (true) {
            try {
                final String string = Long.toString(currentTimeMillis - convertFromExifDateTime(convertToExifDateTime).getTime());
                this.mExifInterface.setAttribute("SubSecTimeOriginal", string);
                this.mExifInterface.setAttribute("SubSecTimeDigitized", string);
                this.mRemoveTimestamp = false;
            }
            catch (final ParseException ex) {
                continue;
            }
            break;
        }
    }
    
    public void copyToCroppedImage(@NonNull final Exif exif) {
        final ArrayList list = new ArrayList((Collection<? extends E>)Exif.ALL_EXIF_TAGS);
        list.removeAll(Exif.DO_NOT_COPY_EXIF_TAGS);
        for (final String s : list) {
            final String attribute = this.mExifInterface.getAttribute(s);
            final String attribute2 = exif.mExifInterface.getAttribute(s);
            if (attribute != null && !attribute.equals(attribute2)) {
                exif.mExifInterface.setAttribute(s, attribute);
            }
        }
    }
    
    public void flipHorizontally() {
        int i = 0;
        switch (this.getOrientation()) {
            default: {
                i = 2;
                break;
            }
            case 8: {
                i = 7;
                break;
            }
            case 7: {
                i = 8;
                break;
            }
            case 6: {
                i = 5;
                break;
            }
            case 5: {
                i = 6;
                break;
            }
            case 4: {
                i = 3;
                break;
            }
            case 3: {
                i = 4;
                break;
            }
            case 2: {
                i = 1;
                break;
            }
        }
        this.mExifInterface.setAttribute("Orientation", String.valueOf(i));
    }
    
    public void flipVertically() {
        int i = 0;
        switch (this.getOrientation()) {
            default: {
                i = 4;
                break;
            }
            case 8: {
                i = 5;
                break;
            }
            case 7: {
                i = 6;
                break;
            }
            case 6: {
                i = 7;
                break;
            }
            case 5: {
                i = 8;
                break;
            }
            case 4: {
                i = 1;
                break;
            }
            case 3: {
                i = 2;
                break;
            }
            case 2: {
                i = 3;
                break;
            }
        }
        this.mExifInterface.setAttribute("Orientation", String.valueOf(i));
    }
    
    @Nullable
    public String getDescription() {
        return this.mExifInterface.getAttribute("ImageDescription");
    }
    
    @NonNull
    @VisibleForTesting
    public ExifInterface getExifInterface() {
        return this.mExifInterface;
    }
    
    public int getHeight() {
        return this.mExifInterface.getAttributeInt("ImageLength", 0);
    }
    
    public long getLastModifiedTimestamp() {
        final long timestamp = this.parseTimestamp(this.mExifInterface.getAttribute("DateTime"));
        if (timestamp == -1L) {
            return -1L;
        }
        final String attribute = this.mExifInterface.getAttribute("SubSecTime");
        long n = timestamp;
        if (attribute == null) {
            return n;
        }
        try {
            long long1;
            for (long1 = Long.parseLong(attribute); long1 > 1000L; long1 /= 10L) {}
            n = timestamp + long1;
            return n;
        }
        catch (final NumberFormatException ex) {
            n = timestamp;
            return n;
        }
    }
    
    @Nullable
    public Location getLocation() {
        final String attribute = this.mExifInterface.getAttribute("GPSProcessingMethod");
        final double[] latLong = this.mExifInterface.getLatLong();
        final double altitude = this.mExifInterface.getAltitude(0.0);
        final double attributeDouble = this.mExifInterface.getAttributeDouble("GPSSpeed", 0.0);
        String attribute2;
        if ((attribute2 = this.mExifInterface.getAttribute("GPSSpeedRef")) == null) {
            attribute2 = "K";
        }
        final long timestamp = this.parseTimestamp(this.mExifInterface.getAttribute("GPSDateStamp"), this.mExifInterface.getAttribute("GPSTimeStamp"));
        if (latLong == null) {
            return null;
        }
        String tag;
        if ((tag = attribute) == null) {
            tag = Exif.TAG;
        }
        final Location location = new Location(tag);
        location.setLatitude(latLong[0]);
        location.setLongitude(latLong[1]);
        if (altitude != 0.0) {
            location.setAltitude(altitude);
        }
        if (attributeDouble != 0.0) {
            final int hashCode = attribute2.hashCode();
            int n = 0;
            Label_0243: {
                if (hashCode != 75) {
                    if (hashCode != 77) {
                        if (hashCode == 78) {
                            if (attribute2.equals("N")) {
                                n = 1;
                                break Label_0243;
                            }
                        }
                    }
                    else if (attribute2.equals("M")) {
                        n = 0;
                        break Label_0243;
                    }
                }
                else if (attribute2.equals("K")) {
                    n = 2;
                    break Label_0243;
                }
                n = -1;
            }
            double n2;
            if (n != 0) {
                if (n != 1) {
                    n2 = Speed.fromKilometersPerHour(attributeDouble).toMetersPerSecond();
                }
                else {
                    n2 = Speed.fromKnots(attributeDouble).toMetersPerSecond();
                }
            }
            else {
                n2 = Speed.fromMilesPerHour(attributeDouble).toMetersPerSecond();
            }
            location.setSpeed((float)n2);
        }
        if (timestamp != -1L) {
            location.setTime(timestamp);
        }
        return location;
    }
    
    public int getOrientation() {
        return this.mExifInterface.getAttributeInt("Orientation", 0);
    }
    
    public int getRotation() {
        switch (this.getOrientation()) {
            default: {
                return 0;
            }
            case 8: {
                return 270;
            }
            case 6:
            case 7: {
                return 90;
            }
            case 5: {
                return 270;
            }
            case 3:
            case 4: {
                return 180;
            }
        }
    }
    
    public long getTimestamp() {
        final long timestamp = this.parseTimestamp(this.mExifInterface.getAttribute("DateTimeOriginal"));
        if (timestamp == -1L) {
            return -1L;
        }
        final String attribute = this.mExifInterface.getAttribute("SubSecTimeOriginal");
        long n = timestamp;
        if (attribute == null) {
            return n;
        }
        try {
            long long1;
            for (long1 = Long.parseLong(attribute); long1 > 1000L; long1 /= 10L) {}
            n = timestamp + long1;
            return n;
        }
        catch (final NumberFormatException ex) {
            n = timestamp;
            return n;
        }
    }
    
    public int getWidth() {
        return this.mExifInterface.getAttributeInt("ImageWidth", 0);
    }
    
    public boolean isFlippedHorizontally() {
        return this.getOrientation() == 2;
    }
    
    public boolean isFlippedVertically() {
        final int orientation = this.getOrientation();
        return orientation == 4 || orientation == 5 || orientation == 7;
    }
    
    public void removeLocation() {
        this.mExifInterface.setAttribute("GPSProcessingMethod", null);
        this.mExifInterface.setAttribute("GPSLatitude", null);
        this.mExifInterface.setAttribute("GPSLatitudeRef", null);
        this.mExifInterface.setAttribute("GPSLongitude", null);
        this.mExifInterface.setAttribute("GPSLongitudeRef", null);
        this.mExifInterface.setAttribute("GPSAltitude", null);
        this.mExifInterface.setAttribute("GPSAltitudeRef", null);
        this.mExifInterface.setAttribute("GPSSpeed", null);
        this.mExifInterface.setAttribute("GPSSpeedRef", null);
        this.mExifInterface.setAttribute("GPSDateStamp", null);
        this.mExifInterface.setAttribute("GPSTimeStamp", null);
    }
    
    public void removeTimestamp() {
        this.mExifInterface.setAttribute("DateTime", null);
        this.mExifInterface.setAttribute("DateTimeOriginal", null);
        this.mExifInterface.setAttribute("DateTimeDigitized", null);
        this.mExifInterface.setAttribute("SubSecTime", null);
        this.mExifInterface.setAttribute("SubSecTimeOriginal", null);
        this.mExifInterface.setAttribute("SubSecTimeDigitized", null);
        this.mRemoveTimestamp = true;
    }
    
    public void rotate(int orientation) {
        if (orientation % 90 != 0) {
            Logger.w(Exif.TAG, String.format(Locale.US, "Can only rotate in right angles (eg. 0, 90, 180, 270). %d is unsupported.", orientation));
            this.mExifInterface.setAttribute("Orientation", String.valueOf(0));
            return;
        }
        int n = orientation % 360;
        orientation = this.getOrientation();
        int i;
        int j;
        while (true) {
            i = orientation;
            j = n;
            if (n >= 0) {
                break;
            }
            n += 90;
            switch (orientation) {
                default: {
                    orientation = 8;
                    continue;
                }
                case 7: {
                    orientation = 2;
                    continue;
                }
                case 6: {
                    orientation = 1;
                    continue;
                }
                case 5: {
                    orientation = 4;
                    continue;
                }
                case 4: {
                    orientation = 7;
                    continue;
                }
                case 3:
                case 8: {
                    orientation = 6;
                    continue;
                }
                case 2: {
                    orientation = 5;
                    continue;
                }
            }
        }
        while (j > 0) {
            j -= 90;
            switch (i) {
                default: {
                    i = 6;
                    continue;
                }
                case 8: {
                    i = 1;
                    continue;
                }
                case 7: {
                    i = 4;
                    continue;
                }
                case 6: {
                    i = 3;
                    continue;
                }
                case 5: {
                    i = 2;
                    continue;
                }
                case 4: {
                    i = 5;
                    continue;
                }
                case 3: {
                    i = 8;
                    continue;
                }
                case 2: {
                    i = 7;
                    continue;
                }
            }
        }
        this.mExifInterface.setAttribute("Orientation", String.valueOf(i));
    }
    
    public void save() throws IOException {
        if (!this.mRemoveTimestamp) {
            this.attachLastModifiedTimestamp();
        }
        this.mExifInterface.saveAttributes();
    }
    
    public void setDescription(@Nullable final String s) {
        this.mExifInterface.setAttribute("ImageDescription", s);
    }
    
    public void setOrientation(final int i) {
        this.mExifInterface.setAttribute("Orientation", String.valueOf(i));
    }
    
    @Override
    public String toString() {
        return String.format(Locale.ENGLISH, "Exif{width=%s, height=%s, rotation=%d, isFlippedVertically=%s, isFlippedHorizontally=%s, location=%s, timestamp=%s, description=%s}", this.getWidth(), this.getHeight(), this.getRotation(), this.isFlippedVertically(), this.isFlippedHorizontally(), this.getLocation(), this.getTimestamp(), this.getDescription());
    }
    
    private static final class Speed
    {
        static Converter fromKilometersPerHour(final double n) {
            return new Converter(n * 0.621371);
        }
        
        static Converter fromKnots(final double n) {
            return new Converter(n * 1.15078);
        }
        
        static Converter fromMilesPerHour(final double n) {
            return new Converter(n);
        }
        
        static final class Converter
        {
            final double mMph;
            
            Converter(final double mMph) {
                this.mMph = mMph;
            }
            
            double toMetersPerSecond() {
                return this.mMph / 2.23694;
            }
        }
    }
}

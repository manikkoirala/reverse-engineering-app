// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils.futures;

import androidx.annotation.NonNull;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

@FunctionalInterface
@RequiresApi(21)
public interface AsyncFunction<I, O>
{
    @NonNull
    ListenableFuture<O> apply(@Nullable final I p0) throws Exception;
}

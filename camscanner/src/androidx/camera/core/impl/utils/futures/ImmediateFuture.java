// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils.futures;

import java.util.concurrent.Delayed;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import androidx.annotation.Nullable;
import java.util.concurrent.ExecutionException;
import androidx.camera.core.Logger;
import androidx.core.util.Preconditions;
import java.util.concurrent.Executor;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import com.google.common.util.concurrent.ListenableFuture;

@RequiresApi(21)
abstract class ImmediateFuture<V> implements ListenableFuture<V>
{
    private static final String TAG = "ImmediateFuture";
    
    public static <V> ListenableFuture<V> nullFuture() {
        return (ListenableFuture<V>)ImmediateSuccessfulFuture.NULL_FUTURE;
    }
    
    public void addListener(@NonNull final Runnable obj, @NonNull final Executor obj2) {
        Preconditions.checkNotNull(obj);
        Preconditions.checkNotNull(obj2);
        try {
            obj2.execute(obj);
        }
        catch (final RuntimeException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Experienced RuntimeException while attempting to notify ");
            sb.append(obj);
            sb.append(" on Executor ");
            sb.append(obj2);
            Logger.e("ImmediateFuture", sb.toString(), ex);
        }
    }
    
    public boolean cancel(final boolean b) {
        return false;
    }
    
    @Nullable
    public abstract V get() throws ExecutionException;
    
    @Nullable
    public V get(final long n, @NonNull final TimeUnit timeUnit) throws ExecutionException {
        Preconditions.checkNotNull(timeUnit);
        return this.get();
    }
    
    public boolean isCancelled() {
        return false;
    }
    
    public boolean isDone() {
        return true;
    }
    
    static class ImmediateFailedFuture<V> extends ImmediateFuture<V>
    {
        @NonNull
        private final Throwable mCause;
        
        ImmediateFailedFuture(@NonNull final Throwable mCause) {
            this.mCause = mCause;
        }
        
        @Nullable
        @Override
        public V get() throws ExecutionException {
            throw new ExecutionException(this.mCause);
        }
        
        @NonNull
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(super.toString());
            sb.append("[status=FAILURE, cause=[");
            sb.append(this.mCause);
            sb.append("]]");
            return sb.toString();
        }
    }
    
    static final class ImmediateFailedScheduledFuture<V> extends ImmediateFailedFuture<V> implements ScheduledFuture<V>
    {
        ImmediateFailedScheduledFuture(@NonNull final Throwable t) {
            super(t);
        }
        
        public int compareTo(@NonNull final Delayed delayed) {
            return -1;
        }
        
        @Override
        public long getDelay(@NonNull final TimeUnit timeUnit) {
            return 0L;
        }
    }
    
    static final class ImmediateSuccessfulFuture<V> extends ImmediateFuture<V>
    {
        static final ImmediateFuture<Object> NULL_FUTURE;
        @Nullable
        private final V mValue;
        
        static {
            NULL_FUTURE = new ImmediateSuccessfulFuture<Object>(null);
        }
        
        ImmediateSuccessfulFuture(@Nullable final V mValue) {
            this.mValue = mValue;
        }
        
        @Nullable
        @Override
        public V get() {
            return this.mValue;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(super.toString());
            sb.append("[status=SUCCESS, result=[");
            sb.append(this.mValue);
            sb.append("]]");
            return sb.toString();
        }
    }
}

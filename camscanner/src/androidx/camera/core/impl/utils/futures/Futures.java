// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils.futures;

import androidx.concurrent.futures.CallbackToFutureAdapter;
import java.util.concurrent.ScheduledFuture;
import androidx.annotation.Nullable;
import java.util.concurrent.ExecutionException;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import java.util.ArrayList;
import java.util.List;
import java.util.Collection;
import java.util.concurrent.Future;
import androidx.core.util.Preconditions;
import java.util.concurrent.Executor;
import androidx.annotation.NonNull;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.arch.core.util.Function;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class Futures
{
    private static final Function<?, ?> IDENTITY_FUNCTION;
    
    static {
        IDENTITY_FUNCTION = new Function<Object, Object>() {
            @Override
            public Object apply(final Object o) {
                return o;
            }
        };
    }
    
    private Futures() {
    }
    
    public static <V> void addCallback(@NonNull final ListenableFuture<V> listenableFuture, @NonNull final FutureCallback<? super V> futureCallback, @NonNull final Executor executor) {
        Preconditions.checkNotNull(futureCallback);
        listenableFuture.addListener((Runnable)new CallbackListener<Object>((Future<?>)listenableFuture, futureCallback), executor);
    }
    
    @NonNull
    public static <V> ListenableFuture<List<V>> allAsList(@NonNull final Collection<? extends ListenableFuture<? extends V>> c) {
        return (ListenableFuture<List<V>>)new ListFuture(new ArrayList<com.google.common.util.concurrent.ListenableFuture<?>>(c), true, CameraXExecutors.directExecutor());
    }
    
    @Nullable
    public static <V> V getDone(@NonNull final Future<V> obj) throws ExecutionException {
        final boolean done = obj.isDone();
        final StringBuilder sb = new StringBuilder();
        sb.append("Future was expected to be done, ");
        sb.append(obj);
        Preconditions.checkState(done, sb.toString());
        return (V)getUninterruptibly((Future<Object>)obj);
    }
    
    @Nullable
    public static <V> V getUninterruptibly(@NonNull final Future<V> future) throws ExecutionException {
        boolean b = false;
        try {
            return future.get();
        }
        catch (final InterruptedException ex) {
            b = true;
            return future.get();
        }
        finally {
            if (b) {
                Thread.currentThread().interrupt();
            }
        }
    }
    
    @NonNull
    public static <V> ListenableFuture<V> immediateFailedFuture(@NonNull final Throwable t) {
        return (ListenableFuture<V>)new ImmediateFuture.ImmediateFailedFuture(t);
    }
    
    @NonNull
    public static <V> ScheduledFuture<V> immediateFailedScheduledFuture(@NonNull final Throwable t) {
        return new ImmediateFuture.ImmediateFailedScheduledFuture<V>(t);
    }
    
    @NonNull
    public static <V> ListenableFuture<V> immediateFuture(@Nullable final V v) {
        if (v == null) {
            return ImmediateFuture.nullFuture();
        }
        return (ListenableFuture<V>)new ImmediateFuture.ImmediateSuccessfulFuture(v);
    }
    
    @NonNull
    public static <V> ListenableFuture<V> nonCancellationPropagating(@NonNull final ListenableFuture<V> listenableFuture) {
        Preconditions.checkNotNull(listenableFuture);
        if (((Future)listenableFuture).isDone()) {
            return listenableFuture;
        }
        return CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<V>)new \u3007080(listenableFuture));
    }
    
    public static <V> void propagate(@NonNull final ListenableFuture<V> listenableFuture, @NonNull final CallbackToFutureAdapter.Completer<V> completer) {
        propagateTransform((com.google.common.util.concurrent.ListenableFuture<Object>)listenableFuture, (Function<? super Object, ? extends V>)Futures.IDENTITY_FUNCTION, completer, CameraXExecutors.directExecutor());
    }
    
    public static <I, O> void propagateTransform(@NonNull final ListenableFuture<I> listenableFuture, @NonNull final Function<? super I, ? extends O> function, @NonNull final CallbackToFutureAdapter.Completer<O> completer, @NonNull final Executor executor) {
        propagateTransform(true, (com.google.common.util.concurrent.ListenableFuture<Object>)listenableFuture, (Function<? super Object, ? extends O>)function, completer, executor);
    }
    
    private static <I, O> void propagateTransform(final boolean b, @NonNull final ListenableFuture<I> listenableFuture, @NonNull final Function<? super I, ? extends O> function, @NonNull final CallbackToFutureAdapter.Completer<O> completer, @NonNull final Executor executor) {
        Preconditions.checkNotNull(listenableFuture);
        Preconditions.checkNotNull(function);
        Preconditions.checkNotNull(completer);
        Preconditions.checkNotNull(executor);
        addCallback((com.google.common.util.concurrent.ListenableFuture<Object>)listenableFuture, (FutureCallback<? super Object>)new FutureCallback<I>(completer, function) {
            final CallbackToFutureAdapter.Completer val$completer;
            final Function val$function;
            
            @Override
            public void onFailure(@NonNull final Throwable exception) {
                this.val$completer.setException(exception);
            }
            
            @Override
            public void onSuccess(@Nullable final I n) {
                try {
                    this.val$completer.set(this.val$function.apply(n));
                }
                finally {
                    final Throwable exception;
                    this.val$completer.setException(exception);
                }
            }
        }, executor);
        if (b) {
            completer.addCancellationListener(new Runnable(listenableFuture) {
                final ListenableFuture val$input;
                
                @Override
                public void run() {
                    ((Future)this.val$input).cancel(true);
                }
            }, CameraXExecutors.directExecutor());
        }
    }
    
    @NonNull
    public static <V> ListenableFuture<List<V>> successfulAsList(@NonNull final Collection<? extends ListenableFuture<? extends V>> c) {
        return (ListenableFuture<List<V>>)new ListFuture(new ArrayList<com.google.common.util.concurrent.ListenableFuture<?>>(c), false, CameraXExecutors.directExecutor());
    }
    
    @NonNull
    public static <I, O> ListenableFuture<O> transform(@NonNull final ListenableFuture<I> listenableFuture, @NonNull final Function<? super I, ? extends O> function, @NonNull final Executor executor) {
        Preconditions.checkNotNull(function);
        return transformAsync((com.google.common.util.concurrent.ListenableFuture<Object>)listenableFuture, (AsyncFunction<? super Object, ? extends O>)new AsyncFunction<I, O>(function) {
            final Function val$function;
            
            @NonNull
            @Override
            public ListenableFuture<O> apply(final I n) {
                return Futures.immediateFuture(this.val$function.apply(n));
            }
        }, executor);
    }
    
    @NonNull
    public static <I, O> ListenableFuture<O> transformAsync(@NonNull final ListenableFuture<I> listenableFuture, @NonNull final AsyncFunction<? super I, ? extends O> asyncFunction, @NonNull final Executor executor) {
        final ChainingListenableFuture<Object, Object> chainingListenableFuture = new ChainingListenableFuture<Object, Object>((AsyncFunction<? super Object, ?>)asyncFunction, listenableFuture);
        listenableFuture.addListener((Runnable)chainingListenableFuture, executor);
        return (ListenableFuture<O>)chainingListenableFuture;
    }
    
    private static final class CallbackListener<V> implements Runnable
    {
        final FutureCallback<? super V> mCallback;
        final Future<V> mFuture;
        
        CallbackListener(final Future<V> mFuture, final FutureCallback<? super V> mCallback) {
            this.mFuture = mFuture;
            this.mCallback = mCallback;
        }
        
        @Override
        public void run() {
            try {
                final V done = Futures.getDone(this.mFuture);
                this.mCallback.onSuccess(done);
            }
            catch (final Error done) {
                goto Label_0024;
            }
            catch (final RuntimeException ex) {}
            catch (final ExecutionException ex2) {
                final Throwable cause = ex2.getCause();
                if (cause == null) {
                    this.mCallback.onFailure(ex2);
                }
                else {
                    this.mCallback.onFailure(cause);
                }
            }
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(CallbackListener.class.getSimpleName());
            sb.append(",");
            sb.append(this.mCallback);
            return sb.toString();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils.futures;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface FutureCallback<V>
{
    void onFailure(@NonNull final Throwable p0);
    
    void onSuccess(@Nullable final V p0);
}

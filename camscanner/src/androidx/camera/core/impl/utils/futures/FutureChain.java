// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils.futures;

import java.util.concurrent.Future;
import androidx.arch.core.util.Function;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import androidx.core.util.Preconditions;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.annotation.RequiresApi;
import com.google.common.util.concurrent.ListenableFuture;

@RequiresApi(21)
public class FutureChain<V> implements ListenableFuture<V>
{
    @Nullable
    CallbackToFutureAdapter.Completer<V> mCompleter;
    @NonNull
    private final ListenableFuture<V> mDelegate;
    
    FutureChain() {
        this.mDelegate = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<V>)new CallbackToFutureAdapter.Resolver<V>(this) {
            final FutureChain this$0;
            
            @Override
            public Object attachCompleter(@NonNull final Completer<V> mCompleter) {
                Preconditions.checkState(this.this$0.mCompleter == null, "The result can only set once!");
                this.this$0.mCompleter = mCompleter;
                final StringBuilder sb = new StringBuilder();
                sb.append("FutureChain[");
                sb.append(this.this$0);
                sb.append("]");
                return sb.toString();
            }
        });
    }
    
    FutureChain(@NonNull final ListenableFuture<V> listenableFuture) {
        this.mDelegate = Preconditions.checkNotNull(listenableFuture);
    }
    
    @NonNull
    public static <V> FutureChain<V> from(@NonNull final ListenableFuture<V> listenableFuture) {
        FutureChain futureChain;
        if (listenableFuture instanceof FutureChain) {
            futureChain = (FutureChain)listenableFuture;
        }
        else {
            futureChain = new FutureChain((ListenableFuture<V>)listenableFuture);
        }
        return futureChain;
    }
    
    public final void addCallback(@NonNull final FutureCallback<? super V> futureCallback, @NonNull final Executor executor) {
        Futures.addCallback((com.google.common.util.concurrent.ListenableFuture<Object>)this, (FutureCallback<? super Object>)futureCallback, executor);
    }
    
    public void addListener(@NonNull final Runnable runnable, @NonNull final Executor executor) {
        this.mDelegate.addListener(runnable, executor);
    }
    
    public boolean cancel(final boolean b) {
        return ((Future)this.mDelegate).cancel(b);
    }
    
    @Nullable
    public V get() throws InterruptedException, ExecutionException {
        return ((Future<V>)this.mDelegate).get();
    }
    
    @Nullable
    public V get(final long n, @NonNull final TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return ((Future<V>)this.mDelegate).get(n, timeUnit);
    }
    
    public boolean isCancelled() {
        return ((Future)this.mDelegate).isCancelled();
    }
    
    public boolean isDone() {
        return ((Future)this.mDelegate).isDone();
    }
    
    boolean set(@Nullable final V v) {
        final CallbackToFutureAdapter.Completer<V> mCompleter = this.mCompleter;
        return mCompleter != null && mCompleter.set(v);
    }
    
    boolean setException(@NonNull final Throwable exception) {
        final CallbackToFutureAdapter.Completer<V> mCompleter = this.mCompleter;
        return mCompleter != null && mCompleter.setException(exception);
    }
    
    @NonNull
    public final <T> FutureChain<T> transform(@NonNull final Function<? super V, T> function, @NonNull final Executor executor) {
        return (FutureChain)Futures.transform((com.google.common.util.concurrent.ListenableFuture<Object>)this, (Function<? super Object, ?>)function, executor);
    }
    
    @NonNull
    public final <T> FutureChain<T> transformAsync(@NonNull final AsyncFunction<? super V, T> asyncFunction, @NonNull final Executor executor) {
        return (FutureChain)Futures.transformAsync((com.google.common.util.concurrent.ListenableFuture<Object>)this, (AsyncFunction<? super Object, ?>)asyncFunction, executor);
    }
}

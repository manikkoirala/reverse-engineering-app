// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils.futures;

import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.Collection;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import java.util.Iterator;
import java.util.ArrayList;
import androidx.core.util.Preconditions;
import java.util.concurrent.Executor;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.annotation.NonNull;
import java.util.concurrent.atomic.AtomicInteger;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import java.util.List;
import com.google.common.util.concurrent.ListenableFuture;

@RequiresApi(21)
class ListFuture<V> implements ListenableFuture<List<V>>
{
    private final boolean mAllMustSucceed;
    @Nullable
    List<? extends ListenableFuture<? extends V>> mFutures;
    @NonNull
    private final AtomicInteger mRemaining;
    @NonNull
    private final ListenableFuture<List<V>> mResult;
    CallbackToFutureAdapter.Completer<List<V>> mResultNotifier;
    @Nullable
    List<V> mValues;
    
    ListFuture(@NonNull final List<? extends ListenableFuture<? extends V>> list, final boolean mAllMustSucceed, @NonNull final Executor executor) {
        this.mFutures = Preconditions.checkNotNull(list);
        this.mValues = new ArrayList<V>(list.size());
        this.mAllMustSucceed = mAllMustSucceed;
        this.mRemaining = new AtomicInteger(list.size());
        this.mResult = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<List<V>>)new CallbackToFutureAdapter.Resolver<List<V>>(this) {
            final ListFuture this$0;
            
            @Override
            public Object attachCompleter(@NonNull final Completer<List<V>> mResultNotifier) {
                Preconditions.checkState(this.this$0.mResultNotifier == null, "The result can only set once!");
                this.this$0.mResultNotifier = mResultNotifier;
                final StringBuilder sb = new StringBuilder();
                sb.append("ListFuture[");
                sb.append(this);
                sb.append("]");
                return sb.toString();
            }
        });
        this.init(executor);
    }
    
    private void callAllGets() throws InterruptedException {
        final List<? extends ListenableFuture<? extends V>> mFutures = this.mFutures;
        Label_0076: {
            if (mFutures != null && !this.isDone()) {
                final Iterator<? extends ListenableFuture<? extends V>> iterator = mFutures.iterator();
                while (iterator.hasNext()) {
                    final ListenableFuture listenableFuture = (ListenableFuture)iterator.next();
                    while (!((Future)listenableFuture).isDone()) {
                        try {
                            ((Future)listenableFuture).get();
                            continue;
                        }
                        catch (final InterruptedException iterator) {
                            throw iterator;
                        }
                        catch (final Error iterator) {
                            throw iterator;
                        }
                        finally {
                            if (this.mAllMustSucceed) {
                                return;
                            }
                            continue;
                        }
                        break Label_0076;
                    }
                }
            }
        }
    }
    
    private void init(@NonNull final Executor executor) {
        this.addListener(new Runnable(this) {
            final ListFuture this$0;
            
            @Override
            public void run() {
                final ListFuture this$0 = this.this$0;
                this$0.mValues = null;
                this$0.mFutures = null;
            }
        }, CameraXExecutors.directExecutor());
        if (this.mFutures.isEmpty()) {
            this.mResultNotifier.set(new ArrayList<V>((Collection<? extends V>)this.mValues));
            return;
        }
        final int n = 0;
        for (int i = 0; i < this.mFutures.size(); ++i) {
            this.mValues.add(null);
        }
        final List<? extends ListenableFuture<? extends V>> mFutures = this.mFutures;
        for (int j = n; j < mFutures.size(); ++j) {
            final ListenableFuture listenableFuture = mFutures.get(j);
            listenableFuture.addListener((Runnable)new Runnable(this, j, listenableFuture) {
                final ListFuture this$0;
                final int val$index;
                final ListenableFuture val$listenable;
                
                @Override
                public void run() {
                    this.this$0.setOneValue(this.val$index, (Future<? extends V>)this.val$listenable);
                }
            }, executor);
        }
    }
    
    public void addListener(@NonNull final Runnable runnable, @NonNull final Executor executor) {
        this.mResult.addListener(runnable, executor);
    }
    
    public boolean cancel(final boolean b) {
        final List<? extends ListenableFuture<? extends V>> mFutures = this.mFutures;
        if (mFutures != null) {
            final Iterator<? extends ListenableFuture<? extends V>> iterator = mFutures.iterator();
            while (iterator.hasNext()) {
                ((Future)iterator.next()).cancel(b);
            }
        }
        return ((Future)this.mResult).cancel(b);
    }
    
    @Nullable
    public List<V> get() throws InterruptedException, ExecutionException {
        this.callAllGets();
        return ((Future<List<V>>)this.mResult).get();
    }
    
    public List<V> get(final long n, @NonNull final TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return ((Future<List<V>>)this.mResult).get(n, timeUnit);
    }
    
    public boolean isCancelled() {
        return ((Future)this.mResult).isCancelled();
    }
    
    public boolean isDone() {
        return ((Future)this.mResult).isDone();
    }
    
    void setOneValue(int n, @NonNull final Future<? extends V> future) {
        final List<V> mValues = this.mValues;
        if (!this.isDone()) {
            if (mValues != null) {
                final boolean b = true;
                boolean b2 = true;
                final boolean b3 = true;
                final boolean b4 = true;
                final boolean b5 = true;
                final boolean b6 = true;
                try {
                    List<V> mValues2;
                    CallbackToFutureAdapter.Completer<List<V>> completer;
                    ArrayList list;
                    List<V> mValues3;
                    List<V> mValues4 = null;
                    final RuntimeException exception;
                    List<V> mValues5;
                    List<V> mValues6;
                    List<V> mValues7;
                    final ExecutionException ex;
                    Block_27_Outer:Label_0171_Outer:Block_17_Outer:Block_23_Outer:
                    while (true) {
                        try {
                            Preconditions.checkState(future.isDone(), "Tried to set value from future which is not done");
                            mValues.set(n, Futures.getUninterruptibly((Future<V>)future));
                            n = this.mRemaining.decrementAndGet();
                            b2 = (n >= 0 && b6);
                            Preconditions.checkState(b2, "Less than 0 remaining futures");
                            if (n != 0) {
                                return;
                            }
                            mValues2 = this.mValues;
                            if (mValues2 != null) {
                                completer = this.mResultNotifier;
                                list = new ArrayList(mValues2);
                                completer.set((ArrayList)list);
                                return;
                            }
                            Label_0126: {
                                Preconditions.checkState(this.isDone());
                            }
                            return;
                        }
                        finally {
                            n = this.mRemaining.decrementAndGet();
                            Preconditions.checkState(n >= 0 && b5, "Less than 0 remaining futures");
                            if (n == 0) {
                                mValues3 = this.mValues;
                                if (mValues3 != null) {
                                    this.mResultNotifier.set(new ArrayList<V>((Collection<? extends V>)mValues3));
                                }
                                else {
                                    Preconditions.checkState(this.isDone());
                                }
                            }
                            while (true) {
                                Block_28: {
                                    while (true) {
                                        Block_16: {
                                            while (true) {
                                            Label_0171:
                                                while (true) {
                                                    while (true) {
                                                        mValues4 = this.mValues;
                                                        iftrue(Label_0126:)(mValues4 == null);
                                                        break Block_28;
                                                        b2 = false;
                                                        break Label_0171;
                                                        this.mResultNotifier.setException(exception);
                                                        n = this.mRemaining.decrementAndGet();
                                                        iftrue(Label_0240:)(n < 0);
                                                        break Block_16;
                                                        this.cancel(false);
                                                        n = this.mRemaining.decrementAndGet();
                                                        b2 = (n >= 0 && b4);
                                                        Preconditions.checkState(b2, "Less than 0 remaining futures");
                                                        iftrue(Label_0427:)(n != 0);
                                                        continue Label_0171_Outer;
                                                    }
                                                    completer = this.mResultNotifier;
                                                    list = new ArrayList(mValues5);
                                                    continue Block_27_Outer;
                                                    while (true) {
                                                        completer = this.mResultNotifier;
                                                        list = new ArrayList(mValues6);
                                                        continue Block_27_Outer;
                                                        mValues6 = this.mValues;
                                                        iftrue(Label_0126:)(mValues6 == null);
                                                        continue Block_17_Outer;
                                                    }
                                                    completer = this.mResultNotifier;
                                                    list = new ArrayList(mValues7);
                                                    continue Block_27_Outer;
                                                    Preconditions.checkState(b2, "Less than 0 remaining futures");
                                                    iftrue(Label_0427:)(n != 0);
                                                    break Label_0171;
                                                    b2 = b;
                                                    continue Label_0171;
                                                }
                                                mValues5 = this.mValues;
                                                iftrue(Label_0126:)(mValues5 == null);
                                                continue Block_17_Outer;
                                            }
                                            Label_0240: {
                                                b2 = false;
                                            }
                                        }
                                        Preconditions.checkState(b2, "Less than 0 remaining futures");
                                        iftrue(Label_0427:)(n != 0);
                                        continue Block_23_Outer;
                                    }
                                }
                                completer = this.mResultNotifier;
                                list = new ArrayList(mValues4);
                                continue Block_27_Outer;
                                Label_0427: {
                                    return;
                                }
                                mValues7 = this.mValues;
                                iftrue(Label_0126:)(mValues7 == null);
                                continue;
                            }
                            this.mResultNotifier.setException(ex.getCause());
                            n = this.mRemaining.decrementAndGet();
                            b2 = (n >= 0 && b3);
                            Preconditions.checkState(b2, "Less than 0 remaining futures");
                            iftrue(Label_0427:)(n != 0);
                        }
                        break;
                    }
                }
                catch (final Error error) {}
                catch (final RuntimeException ex2) {}
                catch (final ExecutionException ex3) {}
                catch (final CancellationException ex4) {}
            }
        }
        Preconditions.checkState(this.mAllMustSucceed, "Future was done before all dependencies completed");
    }
}

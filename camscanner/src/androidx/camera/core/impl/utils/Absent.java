// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils;

import androidx.core.util.Supplier;
import androidx.core.util.Preconditions;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
final class Absent<T> extends Optional<T>
{
    static final Absent<Object> sInstance;
    private static final long serialVersionUID = 0L;
    
    static {
        sInstance = new Absent<Object>();
    }
    
    private Absent() {
    }
    
    private Object readResolve() {
        return Absent.sInstance;
    }
    
    static <T> Optional<T> withType() {
        return (Optional<T>)Absent.sInstance;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        return o == this;
    }
    
    @NonNull
    @Override
    public T get() {
        throw new IllegalStateException("Optional.get() cannot be called on an absent value");
    }
    
    @Override
    public int hashCode() {
        return 2040732332;
    }
    
    @Override
    public boolean isPresent() {
        return false;
    }
    
    @NonNull
    @Override
    public Optional<T> or(@NonNull final Optional<? extends T> optional) {
        return Preconditions.checkNotNull((Optional<T>)optional);
    }
    
    @NonNull
    @Override
    public T or(@NonNull final Supplier<? extends T> supplier) {
        return Preconditions.checkNotNull((T)supplier.get(), "use Optional.orNull() instead of a Supplier that returns null");
    }
    
    @NonNull
    @Override
    public T or(@NonNull final T t) {
        return Preconditions.checkNotNull(t, "use Optional.orNull() instead of Optional.or(null)");
    }
    
    @Nullable
    @Override
    public T orNull() {
        return null;
    }
    
    @NonNull
    @Override
    public String toString() {
        return "Optional.absent()";
    }
}

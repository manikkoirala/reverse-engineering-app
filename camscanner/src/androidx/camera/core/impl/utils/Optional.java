// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils;

import androidx.core.util.Supplier;
import androidx.core.util.Preconditions;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import java.io.Serializable;

@RequiresApi(21)
public abstract class Optional<T> implements Serializable
{
    private static final long serialVersionUID = 0L;
    
    Optional() {
    }
    
    @NonNull
    public static <T> Optional<T> absent() {
        return Absent.withType();
    }
    
    @NonNull
    public static <T> Optional<T> fromNullable(@Nullable final T t) {
        Optional<Object> absent;
        if (t == null) {
            absent = absent();
        }
        else {
            absent = new Present<Object>(t);
        }
        return (Optional<T>)absent;
    }
    
    @NonNull
    public static <T> Optional<T> of(@NonNull final T t) {
        return new Present<T>(Preconditions.checkNotNull(t));
    }
    
    @Override
    public abstract boolean equals(@Nullable final Object p0);
    
    @NonNull
    public abstract T get();
    
    @Override
    public abstract int hashCode();
    
    public abstract boolean isPresent();
    
    @NonNull
    public abstract Optional<T> or(@NonNull final Optional<? extends T> p0);
    
    @NonNull
    public abstract T or(@NonNull final Supplier<? extends T> p0);
    
    @NonNull
    public abstract T or(@NonNull final T p0);
    
    @Nullable
    public abstract T orNull();
    
    @NonNull
    @Override
    public abstract String toString();
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteOrder;
import androidx.annotation.RequiresApi;
import java.io.FilterOutputStream;

@RequiresApi(21)
class ByteOrderedDataOutputStream extends FilterOutputStream
{
    private ByteOrder mByteOrder;
    final OutputStream mOutputStream;
    
    ByteOrderedDataOutputStream(final OutputStream outputStream, final ByteOrder mByteOrder) {
        super(outputStream);
        this.mOutputStream = outputStream;
        this.mByteOrder = mByteOrder;
    }
    
    public void setByteOrder(final ByteOrder mByteOrder) {
        this.mByteOrder = mByteOrder;
    }
    
    @Override
    public void write(final byte[] b) throws IOException {
        this.mOutputStream.write(b);
    }
    
    @Override
    public void write(final byte[] b, final int off, final int len) throws IOException {
        this.mOutputStream.write(b, off, len);
    }
    
    public void writeByte(final int n) throws IOException {
        this.mOutputStream.write(n);
    }
    
    public void writeInt(final int n) throws IOException {
        final ByteOrder mByteOrder = this.mByteOrder;
        if (mByteOrder == ByteOrder.LITTLE_ENDIAN) {
            this.mOutputStream.write(n >>> 0 & 0xFF);
            this.mOutputStream.write(n >>> 8 & 0xFF);
            this.mOutputStream.write(n >>> 16 & 0xFF);
            this.mOutputStream.write(n >>> 24 & 0xFF);
        }
        else if (mByteOrder == ByteOrder.BIG_ENDIAN) {
            this.mOutputStream.write(n >>> 24 & 0xFF);
            this.mOutputStream.write(n >>> 16 & 0xFF);
            this.mOutputStream.write(n >>> 8 & 0xFF);
            this.mOutputStream.write(n >>> 0 & 0xFF);
        }
    }
    
    public void writeShort(final short n) throws IOException {
        final ByteOrder mByteOrder = this.mByteOrder;
        if (mByteOrder == ByteOrder.LITTLE_ENDIAN) {
            this.mOutputStream.write(n >>> 0 & 0xFF);
            this.mOutputStream.write(n >>> 8 & 0xFF);
        }
        else if (mByteOrder == ByteOrder.BIG_ENDIAN) {
            this.mOutputStream.write(n >>> 8 & 0xFF);
            this.mOutputStream.write(n >>> 0 & 0xFF);
        }
    }
    
    public void writeUnsignedInt(final long n) throws IOException {
        this.writeInt((int)n);
    }
    
    public void writeUnsignedShort(final int n) throws IOException {
        this.writeShort((short)n);
    }
}

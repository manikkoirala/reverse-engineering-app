// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils;

import android.os.Looper;
import androidx.core.util.Preconditions;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class Threads
{
    private Threads() {
    }
    
    public static void checkBackgroundThread() {
        Preconditions.checkState(isBackgroundThread(), "In application's main thread");
    }
    
    public static void checkMainThread() {
        Preconditions.checkState(isMainThread(), "Not in application's main thread");
    }
    
    public static boolean isBackgroundThread() {
        return isMainThread() ^ true;
    }
    
    public static boolean isMainThread() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }
}

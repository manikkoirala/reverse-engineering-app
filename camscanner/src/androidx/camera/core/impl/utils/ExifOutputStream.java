// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils;

import java.io.IOException;
import java.util.Iterator;
import androidx.core.util.Preconditions;
import java.util.HashMap;
import java.nio.ByteOrder;
import java.util.Map;
import java.io.BufferedOutputStream;
import androidx.annotation.NonNull;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import androidx.annotation.RequiresApi;
import java.io.FilterOutputStream;

@RequiresApi(21)
public final class ExifOutputStream extends FilterOutputStream
{
    private static final short BYTE_ALIGN_II = 18761;
    private static final short BYTE_ALIGN_MM = 19789;
    private static final boolean DEBUG = false;
    private static final byte[] IDENTIFIER_EXIF_APP1;
    private static final int IFD_OFFSET = 8;
    private static final byte START_CODE = 42;
    private static final int STATE_FRAME_HEADER = 1;
    private static final int STATE_JPEG_DATA = 2;
    private static final int STATE_SOI = 0;
    private static final int STREAMBUFFER_SIZE = 65536;
    private static final String TAG = "ExifOutputStream";
    private final ByteBuffer mBuffer;
    private int mByteToCopy;
    private int mByteToSkip;
    private final ExifData mExifData;
    private final byte[] mSingleByteArray;
    private int mState;
    
    static {
        IDENTIFIER_EXIF_APP1 = "Exif\u0000\u0000".getBytes(ExifAttribute.ASCII);
    }
    
    public ExifOutputStream(@NonNull final OutputStream out, @NonNull final ExifData mExifData) {
        super(new BufferedOutputStream(out, 65536));
        this.mSingleByteArray = new byte[1];
        this.mBuffer = ByteBuffer.allocate(4);
        this.mState = 0;
        this.mExifData = mExifData;
    }
    
    private int requestByteToBuffer(int min, final byte[] src, final int offset, final int a) {
        min = Math.min(a, min - this.mBuffer.position());
        this.mBuffer.put(src, offset, min);
        return min;
    }
    
    private void writeExifSegment(@NonNull final ByteOrderedDataOutputStream byteOrderedDataOutputStream) throws IOException {
        final ExifTag[][] exif_TAGS = ExifData.EXIF_TAGS;
        final int[] array = new int[exif_TAGS.length];
        final int[] array2 = new int[exif_TAGS.length];
        for (final ExifTag exifTag : ExifData.EXIF_POINTER_TAGS) {
            for (int j = 0; j < ExifData.EXIF_TAGS.length; ++j) {
                this.mExifData.getAttributes(j).remove(exifTag.name);
            }
        }
        if (!this.mExifData.getAttributes(1).isEmpty()) {
            this.mExifData.getAttributes(0).put(ExifData.EXIF_POINTER_TAGS[1].name, ExifAttribute.createULong(0L, this.mExifData.getByteOrder()));
        }
        if (!this.mExifData.getAttributes(2).isEmpty()) {
            this.mExifData.getAttributes(0).put(ExifData.EXIF_POINTER_TAGS[2].name, ExifAttribute.createULong(0L, this.mExifData.getByteOrder()));
        }
        if (!this.mExifData.getAttributes(3).isEmpty()) {
            this.mExifData.getAttributes(1).put(ExifData.EXIF_POINTER_TAGS[3].name, ExifAttribute.createULong(0L, this.mExifData.getByteOrder()));
        }
        for (int k = 0; k < ExifData.EXIF_TAGS.length; ++k) {
            final Iterator<Map.Entry<String, ExifAttribute>> iterator = this.mExifData.getAttributes(k).entrySet().iterator();
            int n = 0;
            while (iterator.hasNext()) {
                final int size = ((Map.Entry<K, ExifAttribute>)iterator.next()).getValue().size();
                if (size > 4) {
                    n += size;
                }
            }
            array2[k] += n;
        }
        int l = 0;
        int n2 = 8;
        while (l < ExifData.EXIF_TAGS.length) {
            int n3 = n2;
            if (!this.mExifData.getAttributes(l).isEmpty()) {
                array[l] = n2;
                n3 = n2 + (this.mExifData.getAttributes(l).size() * 12 + 2 + 4 + array2[l]);
            }
            ++l;
            n2 = n3;
        }
        if (!this.mExifData.getAttributes(1).isEmpty()) {
            this.mExifData.getAttributes(0).put(ExifData.EXIF_POINTER_TAGS[1].name, ExifAttribute.createULong(array[1], this.mExifData.getByteOrder()));
        }
        if (!this.mExifData.getAttributes(2).isEmpty()) {
            this.mExifData.getAttributes(0).put(ExifData.EXIF_POINTER_TAGS[2].name, ExifAttribute.createULong(array[2], this.mExifData.getByteOrder()));
        }
        if (!this.mExifData.getAttributes(3).isEmpty()) {
            this.mExifData.getAttributes(1).put(ExifData.EXIF_POINTER_TAGS[3].name, ExifAttribute.createULong(array[3], this.mExifData.getByteOrder()));
        }
        byteOrderedDataOutputStream.writeUnsignedShort(n2 + 8);
        byteOrderedDataOutputStream.write(ExifOutputStream.IDENTIFIER_EXIF_APP1);
        short n4;
        if (this.mExifData.getByteOrder() == ByteOrder.BIG_ENDIAN) {
            n4 = 19789;
        }
        else {
            n4 = 18761;
        }
        byteOrderedDataOutputStream.writeShort(n4);
        byteOrderedDataOutputStream.setByteOrder(this.mExifData.getByteOrder());
        byteOrderedDataOutputStream.writeUnsignedShort(42);
        byteOrderedDataOutputStream.writeUnsignedInt(8L);
        for (int n5 = 0; n5 < ExifData.EXIF_TAGS.length; ++n5) {
            if (!this.mExifData.getAttributes(n5).isEmpty()) {
                byteOrderedDataOutputStream.writeUnsignedShort(this.mExifData.getAttributes(n5).size());
                int n6 = array[n5] + 2 + this.mExifData.getAttributes(n5).size() * 12 + 4;
                for (final Map.Entry<Object, V> entry : this.mExifData.getAttributes(n5).entrySet()) {
                    final ExifTag exifTag2 = (ExifTag)ExifData.Builder.sExifTagMapsForWriting.get(n5).get(entry.getKey());
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Tag not supported: ");
                    sb.append(entry.getKey());
                    sb.append(". Tag needs to be ported from ExifInterface to ExifData.");
                    final int number = Preconditions.checkNotNull(exifTag2, sb.toString()).number;
                    final ExifAttribute exifAttribute = (ExifAttribute)entry.getValue();
                    int size2 = exifAttribute.size();
                    byteOrderedDataOutputStream.writeUnsignedShort(number);
                    byteOrderedDataOutputStream.writeUnsignedShort(exifAttribute.format);
                    byteOrderedDataOutputStream.writeInt(exifAttribute.numberOfComponents);
                    if (size2 > 4) {
                        byteOrderedDataOutputStream.writeUnsignedInt(n6);
                        n6 += size2;
                    }
                    else {
                        byteOrderedDataOutputStream.write(exifAttribute.bytes);
                        if (size2 >= 4) {
                            continue;
                        }
                        while (size2 < 4) {
                            byteOrderedDataOutputStream.writeByte(0);
                            ++size2;
                        }
                    }
                }
                byteOrderedDataOutputStream.writeUnsignedInt(0L);
                final Iterator<Map.Entry<String, ExifAttribute>> iterator3 = this.mExifData.getAttributes(n5).entrySet().iterator();
                while (iterator3.hasNext()) {
                    final byte[] bytes = ((Map.Entry<K, ExifAttribute>)iterator3.next()).getValue().bytes;
                    if (bytes.length > 4) {
                        byteOrderedDataOutputStream.write(bytes, 0, bytes.length);
                    }
                }
            }
        }
        byteOrderedDataOutputStream.setByteOrder(ByteOrder.BIG_ENDIAN);
    }
    
    @Override
    public void write(final int n) throws IOException {
        final byte[] mSingleByteArray = this.mSingleByteArray;
        mSingleByteArray[0] = (byte)(n & 0xFF);
        this.write(mSingleByteArray);
    }
    
    @Override
    public void write(@NonNull final byte[] array) throws IOException {
        this.write(array, 0, array.length);
    }
    
    @Override
    public void write(@NonNull final byte[] array, int min, int n) throws IOException {
        while (true) {
            final int mByteToSkip = this.mByteToSkip;
            if ((mByteToSkip <= 0 && this.mByteToCopy <= 0 && this.mState == 2) || n <= 0) {
                if (n > 0) {
                    super.out.write(array, min, n);
                }
                return;
            }
            int off = min;
            int a = n;
            if (mByteToSkip > 0) {
                final int min2 = Math.min(n, mByteToSkip);
                a = n - min2;
                this.mByteToSkip -= min2;
                off = min + min2;
            }
            final int mByteToCopy = this.mByteToCopy;
            min = off;
            n = a;
            if (mByteToCopy > 0) {
                min = Math.min(a, mByteToCopy);
                super.out.write(array, off, min);
                n = a - min;
                this.mByteToCopy -= min;
                min += off;
            }
            if (n == 0) {
                return;
            }
            final int mState = this.mState;
            if (mState != 0) {
                if (mState != 1) {
                    continue;
                }
                final int requestByteToBuffer = this.requestByteToBuffer(4, array, min, n);
                min += requestByteToBuffer;
                n -= requestByteToBuffer;
                if (this.mBuffer.position() == 2 && this.mBuffer.getShort() == -39) {
                    super.out.write(this.mBuffer.array(), 0, 2);
                    this.mBuffer.rewind();
                }
                if (this.mBuffer.position() < 4) {
                    return;
                }
                this.mBuffer.rewind();
                final short short1 = this.mBuffer.getShort();
                if (short1 == -31) {
                    this.mByteToSkip = (this.mBuffer.getShort() & 0xFFFF) - 2;
                    this.mState = 2;
                }
                else if (!JpegHeader.isSofMarker(short1)) {
                    super.out.write(this.mBuffer.array(), 0, 4);
                    this.mByteToCopy = (this.mBuffer.getShort() & 0xFFFF) - 2;
                }
                else {
                    super.out.write(this.mBuffer.array(), 0, 4);
                    this.mState = 2;
                }
                this.mBuffer.rewind();
            }
            else {
                final int requestByteToBuffer2 = this.requestByteToBuffer(2, array, min, n);
                min += requestByteToBuffer2;
                n -= requestByteToBuffer2;
                if (this.mBuffer.position() < 2) {
                    return;
                }
                this.mBuffer.rewind();
                if (this.mBuffer.getShort() != -40) {
                    throw new IOException("Not a valid jpeg image, cannot write exif");
                }
                super.out.write(this.mBuffer.array(), 0, 2);
                this.mState = 1;
                this.mBuffer.rewind();
                final ByteOrderedDataOutputStream byteOrderedDataOutputStream = new ByteOrderedDataOutputStream(super.out, ByteOrder.BIG_ENDIAN);
                byteOrderedDataOutputStream.writeShort((short)(-31));
                this.writeExifSegment(byteOrderedDataOutputStream);
            }
        }
    }
    
    static final class JpegHeader
    {
        public static final short APP1 = -31;
        public static final short DAC = -52;
        public static final short DHT = -60;
        public static final short EOI = -39;
        public static final short JPG = -56;
        public static final short SOF0 = -64;
        public static final short SOF15 = -49;
        public static final short SOI = -40;
        
        private JpegHeader() {
        }
        
        public static boolean isSofMarker(final short n) {
            return n >= -64 && n <= -49 && n != -60 && n != -56 && n != -52;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils;

import androidx.core.util.Preconditions;
import android.util.CloseGuard;
import androidx.annotation.NonNull;
import android.os.Build$VERSION;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class CloseGuardHelper
{
    private final CloseGuardImpl mImpl;
    
    private CloseGuardHelper(final CloseGuardImpl mImpl) {
        this.mImpl = mImpl;
    }
    
    @NonNull
    public static CloseGuardHelper create() {
        if (Build$VERSION.SDK_INT >= 30) {
            return new CloseGuardHelper((CloseGuardImpl)new CloseGuardApi30Impl());
        }
        return new CloseGuardHelper((CloseGuardImpl)new CloseGuardNoOpImpl());
    }
    
    public void close() {
        this.mImpl.close();
    }
    
    public void open(@NonNull final String s) {
        this.mImpl.open(s);
    }
    
    public void warnIfOpen() {
        this.mImpl.warnIfOpen();
    }
    
    @RequiresApi(30)
    static final class CloseGuardApi30Impl implements CloseGuardImpl
    {
        private final CloseGuard mPlatformImpl;
        
        CloseGuardApi30Impl() {
            this.mPlatformImpl = new CloseGuard();
        }
        
        @Override
        public void close() {
            \u3007o00\u3007\u3007Oo.\u3007080(this.mPlatformImpl);
        }
        
        @Override
        public void open(@NonNull final String s) {
            \u3007080.\u3007080(this.mPlatformImpl, s);
        }
        
        @Override
        public void warnIfOpen() {
            \u3007o\u3007.\u3007080(this.mPlatformImpl);
        }
    }
    
    private interface CloseGuardImpl
    {
        void close();
        
        void open(@NonNull final String p0);
        
        void warnIfOpen();
    }
    
    static final class CloseGuardNoOpImpl implements CloseGuardImpl
    {
        @Override
        public void close() {
        }
        
        @Override
        public void open(@NonNull final String s) {
            Preconditions.checkNotNull(s, "CloseMethodName must not be null.");
        }
        
        @Override
        public void warnIfOpen() {
        }
    }
}

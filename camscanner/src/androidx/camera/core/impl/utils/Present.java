// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils;

import androidx.core.util.Supplier;
import androidx.core.util.Preconditions;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
final class Present<T> extends Optional<T>
{
    private static final long serialVersionUID = 0L;
    private final T mReference;
    
    Present(final T mReference) {
        this.mReference = mReference;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        return o instanceof Present && this.mReference.equals(((Present)o).mReference);
    }
    
    @NonNull
    @Override
    public T get() {
        return this.mReference;
    }
    
    @Override
    public int hashCode() {
        return this.mReference.hashCode() + 1502476572;
    }
    
    @Override
    public boolean isPresent() {
        return true;
    }
    
    @NonNull
    @Override
    public Optional<T> or(@NonNull final Optional<? extends T> optional) {
        Preconditions.checkNotNull(optional);
        return this;
    }
    
    @NonNull
    @Override
    public T or(@NonNull final Supplier<? extends T> supplier) {
        Preconditions.checkNotNull(supplier);
        return this.mReference;
    }
    
    @NonNull
    @Override
    public T or(@NonNull final T t) {
        Preconditions.checkNotNull(t, "use Optional.orNull() instead of Optional.or(null)");
        return this.mReference;
    }
    
    @Override
    public T orNull() {
        return this.mReference;
    }
    
    @NonNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Optional.of(");
        sb.append(this.mReference);
        sb.append(")");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import android.util.Size;
import java.util.Comparator;

@RequiresApi(21)
public final class CompareSizesByArea implements Comparator<Size>
{
    private boolean mReverse;
    
    public CompareSizesByArea() {
        this(false);
    }
    
    public CompareSizesByArea(final boolean mReverse) {
        this.mReverse = mReverse;
    }
    
    @Override
    public int compare(@NonNull final Size size, @NonNull final Size size2) {
        int signum = Long.signum(size.getWidth() * (long)size.getHeight() - size2.getWidth() * (long)size2.getHeight());
        if (this.mReverse) {
            signum *= -1;
        }
        return signum;
    }
}

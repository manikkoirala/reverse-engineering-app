// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils;

import androidx.annotation.DoNotInline;
import androidx.annotation.Nullable;
import android.content.ContextWrapper;
import android.app.Application;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class ContextUtil
{
    private ContextUtil() {
    }
    
    @NonNull
    public static Context getApplicationContext(@NonNull final Context context) {
        final Context applicationContext = context.getApplicationContext();
        if (Build$VERSION.SDK_INT >= 30) {
            final String attributionTag = Api30Impl.getAttributionTag(context);
            if (attributionTag != null) {
                return Api30Impl.createAttributionContext(applicationContext, attributionTag);
            }
        }
        return applicationContext;
    }
    
    @Nullable
    public static Application getApplicationFromContext(@NonNull Context context) {
        Application application;
        for (context = getApplicationContext(context); context instanceof ContextWrapper; context = getBaseContext((ContextWrapper)context)) {
            if (context instanceof Application) {
                application = (Application)context;
                return application;
            }
        }
        application = null;
        return application;
    }
    
    @NonNull
    public static Context getBaseContext(@NonNull final ContextWrapper contextWrapper) {
        final Context baseContext = contextWrapper.getBaseContext();
        if (Build$VERSION.SDK_INT >= 30) {
            final String attributionTag = Api30Impl.getAttributionTag((Context)contextWrapper);
            if (attributionTag != null) {
                return Api30Impl.createAttributionContext(baseContext, attributionTag);
            }
        }
        return baseContext;
    }
    
    @RequiresApi(30)
    private static class Api30Impl
    {
        @DoNotInline
        @NonNull
        static Context createAttributionContext(@NonNull final Context context, @Nullable final String s) {
            return Oo08.\u3007080(context, s);
        }
        
        @DoNotInline
        @Nullable
        static String getAttributionTag(@NonNull final Context context) {
            return O8.\u3007080(context);
        }
    }
}

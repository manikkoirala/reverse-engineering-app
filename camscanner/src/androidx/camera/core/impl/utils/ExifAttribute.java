// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils;

import androidx.annotation.Nullable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import androidx.annotation.NonNull;
import java.nio.charset.StandardCharsets;
import java.nio.charset.Charset;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
final class ExifAttribute
{
    static final Charset ASCII;
    public static final long BYTES_OFFSET_UNKNOWN = -1L;
    static final byte[] EXIF_ASCII_PREFIX;
    static final int IFD_FORMAT_BYTE = 1;
    static final int[] IFD_FORMAT_BYTES_PER_FORMAT;
    static final int IFD_FORMAT_DOUBLE = 12;
    static final String[] IFD_FORMAT_NAMES;
    static final int IFD_FORMAT_SBYTE = 6;
    static final int IFD_FORMAT_SINGLE = 11;
    static final int IFD_FORMAT_SLONG = 9;
    static final int IFD_FORMAT_SRATIONAL = 10;
    static final int IFD_FORMAT_SSHORT = 8;
    static final int IFD_FORMAT_STRING = 2;
    static final int IFD_FORMAT_ULONG = 4;
    static final int IFD_FORMAT_UNDEFINED = 7;
    static final int IFD_FORMAT_URATIONAL = 5;
    static final int IFD_FORMAT_USHORT = 3;
    private static final String TAG = "ExifAttribute";
    public final byte[] bytes;
    public final long bytesOffset;
    public final int format;
    public final int numberOfComponents;
    
    static {
        ASCII = StandardCharsets.US_ASCII;
        IFD_FORMAT_NAMES = new String[] { "", "BYTE", "STRING", "USHORT", "ULONG", "URATIONAL", "SBYTE", "UNDEFINED", "SSHORT", "SLONG", "SRATIONAL", "SINGLE", "DOUBLE", "IFD" };
        IFD_FORMAT_BYTES_PER_FORMAT = new int[] { 0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8, 1 };
        EXIF_ASCII_PREFIX = new byte[] { 65, 83, 67, 73, 73, 0, 0, 0 };
    }
    
    ExifAttribute(final int format, final int numberOfComponents, final long bytesOffset, final byte[] bytes) {
        this.format = format;
        this.numberOfComponents = numberOfComponents;
        this.bytesOffset = bytesOffset;
        this.bytes = bytes;
    }
    
    ExifAttribute(final int n, final int n2, final byte[] array) {
        this(n, n2, -1L, array);
    }
    
    @NonNull
    public static ExifAttribute createByte(@NonNull final String s) {
        if (s.length() == 1 && s.charAt(0) >= '0' && s.charAt(0) <= '1') {
            return new ExifAttribute(1, 1, new byte[] { (byte)(s.charAt(0) - '0') });
        }
        final byte[] bytes = s.getBytes(ExifAttribute.ASCII);
        return new ExifAttribute(1, bytes.length, bytes);
    }
    
    @NonNull
    public static ExifAttribute createDouble(final double n, @NonNull final ByteOrder byteOrder) {
        return createDouble(new double[] { n }, byteOrder);
    }
    
    @NonNull
    public static ExifAttribute createDouble(@NonNull final double[] array, @NonNull final ByteOrder bo) {
        final ByteBuffer wrap = ByteBuffer.wrap(new byte[ExifAttribute.IFD_FORMAT_BYTES_PER_FORMAT[12] * array.length]);
        wrap.order(bo);
        for (int length = array.length, i = 0; i < length; ++i) {
            wrap.putDouble(array[i]);
        }
        return new ExifAttribute(12, array.length, wrap.array());
    }
    
    @NonNull
    public static ExifAttribute createSLong(final int n, @NonNull final ByteOrder byteOrder) {
        return createSLong(new int[] { n }, byteOrder);
    }
    
    @NonNull
    public static ExifAttribute createSLong(@NonNull final int[] array, @NonNull final ByteOrder bo) {
        final ByteBuffer wrap = ByteBuffer.wrap(new byte[ExifAttribute.IFD_FORMAT_BYTES_PER_FORMAT[9] * array.length]);
        wrap.order(bo);
        for (int length = array.length, i = 0; i < length; ++i) {
            wrap.putInt(array[i]);
        }
        return new ExifAttribute(9, array.length, wrap.array());
    }
    
    @NonNull
    public static ExifAttribute createSRational(@NonNull final LongRational longRational, @NonNull final ByteOrder byteOrder) {
        return createSRational(new LongRational[] { longRational }, byteOrder);
    }
    
    @NonNull
    public static ExifAttribute createSRational(@NonNull final LongRational[] array, @NonNull final ByteOrder bo) {
        final ByteBuffer wrap = ByteBuffer.wrap(new byte[ExifAttribute.IFD_FORMAT_BYTES_PER_FORMAT[10] * array.length]);
        wrap.order(bo);
        for (final LongRational longRational : array) {
            wrap.putInt((int)longRational.getNumerator());
            wrap.putInt((int)longRational.getDenominator());
        }
        return new ExifAttribute(10, array.length, wrap.array());
    }
    
    @NonNull
    public static ExifAttribute createString(@NonNull final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append('\0');
        final byte[] bytes = sb.toString().getBytes(ExifAttribute.ASCII);
        return new ExifAttribute(2, bytes.length, bytes);
    }
    
    @NonNull
    public static ExifAttribute createULong(final long n, @NonNull final ByteOrder byteOrder) {
        return createULong(new long[] { n }, byteOrder);
    }
    
    @NonNull
    public static ExifAttribute createULong(@NonNull final long[] array, @NonNull final ByteOrder bo) {
        final ByteBuffer wrap = ByteBuffer.wrap(new byte[ExifAttribute.IFD_FORMAT_BYTES_PER_FORMAT[4] * array.length]);
        wrap.order(bo);
        for (int length = array.length, i = 0; i < length; ++i) {
            wrap.putInt((int)array[i]);
        }
        return new ExifAttribute(4, array.length, wrap.array());
    }
    
    @NonNull
    public static ExifAttribute createURational(@NonNull final LongRational longRational, @NonNull final ByteOrder byteOrder) {
        return createURational(new LongRational[] { longRational }, byteOrder);
    }
    
    @NonNull
    public static ExifAttribute createURational(@NonNull final LongRational[] array, @NonNull final ByteOrder bo) {
        final ByteBuffer wrap = ByteBuffer.wrap(new byte[ExifAttribute.IFD_FORMAT_BYTES_PER_FORMAT[5] * array.length]);
        wrap.order(bo);
        for (final LongRational longRational : array) {
            wrap.putInt((int)longRational.getNumerator());
            wrap.putInt((int)longRational.getDenominator());
        }
        return new ExifAttribute(5, array.length, wrap.array());
    }
    
    @NonNull
    public static ExifAttribute createUShort(final int n, @NonNull final ByteOrder byteOrder) {
        return createUShort(new int[] { n }, byteOrder);
    }
    
    @NonNull
    public static ExifAttribute createUShort(@NonNull final int[] array, @NonNull final ByteOrder bo) {
        final ByteBuffer wrap = ByteBuffer.wrap(new byte[ExifAttribute.IFD_FORMAT_BYTES_PER_FORMAT[3] * array.length]);
        wrap.order(bo);
        for (int length = array.length, i = 0; i < length; ++i) {
            wrap.putShort((short)array[i]);
        }
        return new ExifAttribute(3, array.length, wrap.array());
    }
    
    public double getDoubleValue(@NonNull final ByteOrder byteOrder) {
        final Object value = this.getValue(byteOrder);
        if (value == null) {
            throw new NumberFormatException("NULL can't be converted to a double value");
        }
        if (value instanceof String) {
            return Double.parseDouble((String)value);
        }
        if (value instanceof long[]) {
            final long[] array = (long[])value;
            if (array.length == 1) {
                return (double)array[0];
            }
            throw new NumberFormatException("There are more than one component");
        }
        else if (value instanceof int[]) {
            final int[] array2 = (int[])value;
            if (array2.length == 1) {
                return array2[0];
            }
            throw new NumberFormatException("There are more than one component");
        }
        else if (value instanceof double[]) {
            final double[] array3 = (double[])value;
            if (array3.length == 1) {
                return array3[0];
            }
            throw new NumberFormatException("There are more than one component");
        }
        else {
            if (!(value instanceof LongRational[])) {
                throw new NumberFormatException("Couldn't find a double value");
            }
            final LongRational[] array4 = (LongRational[])value;
            if (array4.length == 1) {
                return array4[0].toDouble();
            }
            throw new NumberFormatException("There are more than one component");
        }
    }
    
    public int getIntValue(@NonNull final ByteOrder byteOrder) {
        final Object value = this.getValue(byteOrder);
        if (value == null) {
            throw new NumberFormatException("NULL can't be converted to a integer value");
        }
        if (value instanceof String) {
            return Integer.parseInt((String)value);
        }
        if (value instanceof long[]) {
            final long[] array = (long[])value;
            if (array.length == 1) {
                return (int)array[0];
            }
            throw new NumberFormatException("There are more than one component");
        }
        else {
            if (!(value instanceof int[])) {
                throw new NumberFormatException("Couldn't find a integer value");
            }
            final int[] array2 = (int[])value;
            if (array2.length == 1) {
                return array2[0];
            }
            throw new NumberFormatException("There are more than one component");
        }
    }
    
    @Nullable
    public String getStringValue(@NonNull final ByteOrder byteOrder) {
        final Object value = this.getValue(byteOrder);
        if (value == null) {
            return null;
        }
        if (value instanceof String) {
            return (String)value;
        }
        final StringBuilder sb = new StringBuilder();
        final boolean b = value instanceof long[];
        final int n = 0;
        final int n2 = 0;
        int i = 0;
        final int n3 = 0;
        if (b) {
            final long[] array = (long[])value;
            int n4;
            for (int j = n3; j < array.length; j = n4) {
                sb.append(array[j]);
                n4 = j + 1;
                if ((j = n4) != array.length) {
                    sb.append(",");
                }
            }
            return sb.toString();
        }
        if (value instanceof int[]) {
            final int[] array2 = (int[])value;
            int n5;
            for (int k = n; k < array2.length; k = n5) {
                sb.append(array2[k]);
                n5 = k + 1;
                if ((k = n5) != array2.length) {
                    sb.append(",");
                }
            }
            return sb.toString();
        }
        if (value instanceof double[]) {
            final double[] array3 = (double[])value;
            int n6;
            for (int l = n2; l < array3.length; l = n6) {
                sb.append(array3[l]);
                n6 = l + 1;
                if ((l = n6) != array3.length) {
                    sb.append(",");
                }
            }
            return sb.toString();
        }
        if (value instanceof LongRational[]) {
            int n7;
            for (LongRational[] array4 = (LongRational[])value; i < array4.length; i = n7) {
                sb.append(array4[i].getNumerator());
                sb.append('/');
                sb.append(array4[i].getDenominator());
                n7 = i + 1;
                if ((i = n7) != array4.length) {
                    sb.append(",");
                }
            }
            return sb.toString();
        }
        return null;
    }
    
    Object getValue(final ByteOrder p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: new             Landroidx/camera/core/impl/utils/ByteOrderedDataInputStream;
        //     6: astore          14
        //     8: aload           14
        //    10: aload_0        
        //    11: getfield        androidx/camera/core/impl/utils/ExifAttribute.bytes:[B
        //    14: invokespecial   androidx/camera/core/impl/utils/ByteOrderedDataInputStream.<init>:([B)V
        //    17: aload           14
        //    19: astore          13
        //    21: aload           14
        //    23: aload_1        
        //    24: invokevirtual   androidx/camera/core/impl/utils/ByteOrderedDataInputStream.setByteOrder:(Ljava/nio/ByteOrder;)V
        //    27: aload           14
        //    29: astore          13
        //    31: aload_0        
        //    32: getfield        androidx/camera/core/impl/utils/ExifAttribute.format:I
        //    35: istore          12
        //    37: iconst_0       
        //    38: istore          7
        //    40: iconst_0       
        //    41: istore          10
        //    43: iconst_0       
        //    44: istore          8
        //    46: iconst_0       
        //    47: istore          11
        //    49: iconst_0       
        //    50: istore          6
        //    52: iconst_0       
        //    53: istore_2       
        //    54: iconst_0       
        //    55: istore_3       
        //    56: iconst_0       
        //    57: istore          4
        //    59: iconst_0       
        //    60: istore          9
        //    62: iconst_1       
        //    63: istore          5
        //    65: iload           12
        //    67: tableswitch {
        //                2: 880
        //                3: 684
        //                4: 619
        //                5: 556
        //                6: 477
        //                7: 880
        //                8: 684
        //                9: 411
        //               10: 345
        //               11: 264
        //               12: 197
        //               13: 131
        //          default: 128
        //        }
        //   128: goto            995
        //   131: aload           14
        //   133: astore          13
        //   135: aload_0        
        //   136: getfield        androidx/camera/core/impl/utils/ExifAttribute.numberOfComponents:I
        //   139: newarray        D
        //   141: astore_1       
        //   142: iload           9
        //   144: istore_2       
        //   145: aload           14
        //   147: astore          13
        //   149: iload_2        
        //   150: aload_0        
        //   151: getfield        androidx/camera/core/impl/utils/ExifAttribute.numberOfComponents:I
        //   154: if_icmpge       175
        //   157: aload           14
        //   159: astore          13
        //   161: aload_1        
        //   162: iload_2        
        //   163: aload           14
        //   165: invokevirtual   androidx/camera/core/impl/utils/ByteOrderedDataInputStream.readDouble:()D
        //   168: dastore        
        //   169: iinc            2, 1
        //   172: goto            145
        //   175: aload           14
        //   177: invokevirtual   java/io/InputStream.close:()V
        //   180: goto            195
        //   183: astore          13
        //   185: ldc             "ExifAttribute"
        //   187: ldc_w           "IOException occurred while closing InputStream"
        //   190: aload           13
        //   192: invokestatic    androidx/camera/core/Logger.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   195: aload_1        
        //   196: areturn        
        //   197: aload           14
        //   199: astore          13
        //   201: aload_0        
        //   202: getfield        androidx/camera/core/impl/utils/ExifAttribute.numberOfComponents:I
        //   205: newarray        D
        //   207: astore_1       
        //   208: iload           7
        //   210: istore_2       
        //   211: aload           14
        //   213: astore          13
        //   215: iload_2        
        //   216: aload_0        
        //   217: getfield        androidx/camera/core/impl/utils/ExifAttribute.numberOfComponents:I
        //   220: if_icmpge       242
        //   223: aload           14
        //   225: astore          13
        //   227: aload_1        
        //   228: iload_2        
        //   229: aload           14
        //   231: invokevirtual   androidx/camera/core/impl/utils/ByteOrderedDataInputStream.readFloat:()F
        //   234: f2d            
        //   235: dastore        
        //   236: iinc            2, 1
        //   239: goto            211
        //   242: aload           14
        //   244: invokevirtual   java/io/InputStream.close:()V
        //   247: goto            262
        //   250: astore          13
        //   252: ldc             "ExifAttribute"
        //   254: ldc_w           "IOException occurred while closing InputStream"
        //   257: aload           13
        //   259: invokestatic    androidx/camera/core/Logger.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   262: aload_1        
        //   263: areturn        
        //   264: aload           14
        //   266: astore          13
        //   268: aload_0        
        //   269: getfield        androidx/camera/core/impl/utils/ExifAttribute.numberOfComponents:I
        //   272: anewarray       Landroidx/camera/core/impl/utils/LongRational;
        //   275: astore_1       
        //   276: iload           10
        //   278: istore_2       
        //   279: aload           14
        //   281: astore          13
        //   283: iload_2        
        //   284: aload_0        
        //   285: getfield        androidx/camera/core/impl/utils/ExifAttribute.numberOfComponents:I
        //   288: if_icmpge       323
        //   291: aload           14
        //   293: astore          13
        //   295: aload_1        
        //   296: iload_2        
        //   297: new             Landroidx/camera/core/impl/utils/LongRational;
        //   300: dup            
        //   301: aload           14
        //   303: invokevirtual   androidx/camera/core/impl/utils/ByteOrderedDataInputStream.readInt:()I
        //   306: i2l            
        //   307: aload           14
        //   309: invokevirtual   androidx/camera/core/impl/utils/ByteOrderedDataInputStream.readInt:()I
        //   312: i2l            
        //   313: invokespecial   androidx/camera/core/impl/utils/LongRational.<init>:(JJ)V
        //   316: aastore        
        //   317: iinc            2, 1
        //   320: goto            279
        //   323: aload           14
        //   325: invokevirtual   java/io/InputStream.close:()V
        //   328: goto            343
        //   331: astore          13
        //   333: ldc             "ExifAttribute"
        //   335: ldc_w           "IOException occurred while closing InputStream"
        //   338: aload           13
        //   340: invokestatic    androidx/camera/core/Logger.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   343: aload_1        
        //   344: areturn        
        //   345: aload           14
        //   347: astore          13
        //   349: aload_0        
        //   350: getfield        androidx/camera/core/impl/utils/ExifAttribute.numberOfComponents:I
        //   353: newarray        I
        //   355: astore_1       
        //   356: iload           8
        //   358: istore_2       
        //   359: aload           14
        //   361: astore          13
        //   363: iload_2        
        //   364: aload_0        
        //   365: getfield        androidx/camera/core/impl/utils/ExifAttribute.numberOfComponents:I
        //   368: if_icmpge       389
        //   371: aload           14
        //   373: astore          13
        //   375: aload_1        
        //   376: iload_2        
        //   377: aload           14
        //   379: invokevirtual   androidx/camera/core/impl/utils/ByteOrderedDataInputStream.readInt:()I
        //   382: iastore        
        //   383: iinc            2, 1
        //   386: goto            359
        //   389: aload           14
        //   391: invokevirtual   java/io/InputStream.close:()V
        //   394: goto            409
        //   397: astore          13
        //   399: ldc             "ExifAttribute"
        //   401: ldc_w           "IOException occurred while closing InputStream"
        //   404: aload           13
        //   406: invokestatic    androidx/camera/core/Logger.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   409: aload_1        
        //   410: areturn        
        //   411: aload           14
        //   413: astore          13
        //   415: aload_0        
        //   416: getfield        androidx/camera/core/impl/utils/ExifAttribute.numberOfComponents:I
        //   419: newarray        I
        //   421: astore_1       
        //   422: iload           11
        //   424: istore_2       
        //   425: aload           14
        //   427: astore          13
        //   429: iload_2        
        //   430: aload_0        
        //   431: getfield        androidx/camera/core/impl/utils/ExifAttribute.numberOfComponents:I
        //   434: if_icmpge       455
        //   437: aload           14
        //   439: astore          13
        //   441: aload_1        
        //   442: iload_2        
        //   443: aload           14
        //   445: invokevirtual   androidx/camera/core/impl/utils/ByteOrderedDataInputStream.readShort:()S
        //   448: iastore        
        //   449: iinc            2, 1
        //   452: goto            425
        //   455: aload           14
        //   457: invokevirtual   java/io/InputStream.close:()V
        //   460: goto            475
        //   463: astore          13
        //   465: ldc             "ExifAttribute"
        //   467: ldc_w           "IOException occurred while closing InputStream"
        //   470: aload           13
        //   472: invokestatic    androidx/camera/core/Logger.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   475: aload_1        
        //   476: areturn        
        //   477: aload           14
        //   479: astore          13
        //   481: aload_0        
        //   482: getfield        androidx/camera/core/impl/utils/ExifAttribute.numberOfComponents:I
        //   485: anewarray       Landroidx/camera/core/impl/utils/LongRational;
        //   488: astore_1       
        //   489: iload           6
        //   491: istore_2       
        //   492: aload           14
        //   494: astore          13
        //   496: iload_2        
        //   497: aload_0        
        //   498: getfield        androidx/camera/core/impl/utils/ExifAttribute.numberOfComponents:I
        //   501: if_icmpge       534
        //   504: aload           14
        //   506: astore          13
        //   508: aload_1        
        //   509: iload_2        
        //   510: new             Landroidx/camera/core/impl/utils/LongRational;
        //   513: dup            
        //   514: aload           14
        //   516: invokevirtual   androidx/camera/core/impl/utils/ByteOrderedDataInputStream.readUnsignedInt:()J
        //   519: aload           14
        //   521: invokevirtual   androidx/camera/core/impl/utils/ByteOrderedDataInputStream.readUnsignedInt:()J
        //   524: invokespecial   androidx/camera/core/impl/utils/LongRational.<init>:(JJ)V
        //   527: aastore        
        //   528: iinc            2, 1
        //   531: goto            492
        //   534: aload           14
        //   536: invokevirtual   java/io/InputStream.close:()V
        //   539: goto            554
        //   542: astore          13
        //   544: ldc             "ExifAttribute"
        //   546: ldc_w           "IOException occurred while closing InputStream"
        //   549: aload           13
        //   551: invokestatic    androidx/camera/core/Logger.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   554: aload_1        
        //   555: areturn        
        //   556: aload           14
        //   558: astore          13
        //   560: aload_0        
        //   561: getfield        androidx/camera/core/impl/utils/ExifAttribute.numberOfComponents:I
        //   564: newarray        J
        //   566: astore_1       
        //   567: aload           14
        //   569: astore          13
        //   571: iload_2        
        //   572: aload_0        
        //   573: getfield        androidx/camera/core/impl/utils/ExifAttribute.numberOfComponents:I
        //   576: if_icmpge       597
        //   579: aload           14
        //   581: astore          13
        //   583: aload_1        
        //   584: iload_2        
        //   585: aload           14
        //   587: invokevirtual   androidx/camera/core/impl/utils/ByteOrderedDataInputStream.readUnsignedInt:()J
        //   590: lastore        
        //   591: iinc            2, 1
        //   594: goto            567
        //   597: aload           14
        //   599: invokevirtual   java/io/InputStream.close:()V
        //   602: goto            617
        //   605: astore          13
        //   607: ldc             "ExifAttribute"
        //   609: ldc_w           "IOException occurred while closing InputStream"
        //   612: aload           13
        //   614: invokestatic    androidx/camera/core/Logger.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   617: aload_1        
        //   618: areturn        
        //   619: aload           14
        //   621: astore          13
        //   623: aload_0        
        //   624: getfield        androidx/camera/core/impl/utils/ExifAttribute.numberOfComponents:I
        //   627: newarray        I
        //   629: astore_1       
        //   630: iload_3        
        //   631: istore_2       
        //   632: aload           14
        //   634: astore          13
        //   636: iload_2        
        //   637: aload_0        
        //   638: getfield        androidx/camera/core/impl/utils/ExifAttribute.numberOfComponents:I
        //   641: if_icmpge       662
        //   644: aload           14
        //   646: astore          13
        //   648: aload_1        
        //   649: iload_2        
        //   650: aload           14
        //   652: invokevirtual   androidx/camera/core/impl/utils/ByteOrderedDataInputStream.readUnsignedShort:()I
        //   655: iastore        
        //   656: iinc            2, 1
        //   659: goto            632
        //   662: aload           14
        //   664: invokevirtual   java/io/InputStream.close:()V
        //   667: goto            682
        //   670: astore          13
        //   672: ldc             "ExifAttribute"
        //   674: ldc_w           "IOException occurred while closing InputStream"
        //   677: aload           13
        //   679: invokestatic    androidx/camera/core/Logger.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   682: aload_1        
        //   683: areturn        
        //   684: iload           4
        //   686: istore_2       
        //   687: aload           14
        //   689: astore          13
        //   691: aload_0        
        //   692: getfield        androidx/camera/core/impl/utils/ExifAttribute.numberOfComponents:I
        //   695: getstatic       androidx/camera/core/impl/utils/ExifAttribute.EXIF_ASCII_PREFIX:[B
        //   698: arraylength    
        //   699: if_icmplt       766
        //   702: iconst_0       
        //   703: istore_2       
        //   704: aload           14
        //   706: astore          13
        //   708: getstatic       androidx/camera/core/impl/utils/ExifAttribute.EXIF_ASCII_PREFIX:[B
        //   711: astore_1       
        //   712: iload           5
        //   714: istore_3       
        //   715: aload           14
        //   717: astore          13
        //   719: iload_2        
        //   720: aload_1        
        //   721: arraylength    
        //   722: if_icmpge       752
        //   725: aload           14
        //   727: astore          13
        //   729: aload_0        
        //   730: getfield        androidx/camera/core/impl/utils/ExifAttribute.bytes:[B
        //   733: iload_2        
        //   734: baload         
        //   735: aload_1        
        //   736: iload_2        
        //   737: baload         
        //   738: if_icmpeq       746
        //   741: iconst_0       
        //   742: istore_3       
        //   743: goto            752
        //   746: iinc            2, 1
        //   749: goto            704
        //   752: iload           4
        //   754: istore_2       
        //   755: iload_3        
        //   756: ifeq            766
        //   759: aload           14
        //   761: astore          13
        //   763: aload_1        
        //   764: arraylength    
        //   765: istore_2       
        //   766: aload           14
        //   768: astore          13
        //   770: new             Ljava/lang/StringBuilder;
        //   773: astore_1       
        //   774: aload           14
        //   776: astore          13
        //   778: aload_1        
        //   779: invokespecial   java/lang/StringBuilder.<init>:()V
        //   782: aload           14
        //   784: astore          13
        //   786: iload_2        
        //   787: aload_0        
        //   788: getfield        androidx/camera/core/impl/utils/ExifAttribute.numberOfComponents:I
        //   791: if_icmpge       849
        //   794: aload           14
        //   796: astore          13
        //   798: aload_0        
        //   799: getfield        androidx/camera/core/impl/utils/ExifAttribute.bytes:[B
        //   802: iload_2        
        //   803: baload         
        //   804: istore_3       
        //   805: iload_3        
        //   806: ifne            812
        //   809: goto            849
        //   812: iload_3        
        //   813: bipush          32
        //   815: if_icmplt       832
        //   818: aload           14
        //   820: astore          13
        //   822: aload_1        
        //   823: iload_3        
        //   824: i2c            
        //   825: invokevirtual   java/lang/StringBuilder.append:(C)Ljava/lang/StringBuilder;
        //   828: pop            
        //   829: goto            843
        //   832: aload           14
        //   834: astore          13
        //   836: aload_1        
        //   837: bipush          63
        //   839: invokevirtual   java/lang/StringBuilder.append:(C)Ljava/lang/StringBuilder;
        //   842: pop            
        //   843: iinc            2, 1
        //   846: goto            782
        //   849: aload           14
        //   851: astore          13
        //   853: aload_1        
        //   854: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   857: astore_1       
        //   858: aload           14
        //   860: invokevirtual   java/io/InputStream.close:()V
        //   863: goto            878
        //   866: astore          13
        //   868: ldc             "ExifAttribute"
        //   870: ldc_w           "IOException occurred while closing InputStream"
        //   873: aload           13
        //   875: invokestatic    androidx/camera/core/Logger.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   878: aload_1        
        //   879: areturn        
        //   880: aload           14
        //   882: astore          13
        //   884: aload_0        
        //   885: getfield        androidx/camera/core/impl/utils/ExifAttribute.bytes:[B
        //   888: astore_1       
        //   889: aload           14
        //   891: astore          13
        //   893: aload_1        
        //   894: arraylength    
        //   895: iconst_1       
        //   896: if_icmpne       957
        //   899: aload_1        
        //   900: iconst_0       
        //   901: baload         
        //   902: istore_2       
        //   903: iload_2        
        //   904: iflt            957
        //   907: iload_2        
        //   908: iconst_1       
        //   909: if_icmpgt       957
        //   912: aload           14
        //   914: astore          13
        //   916: new             Ljava/lang/String;
        //   919: dup            
        //   920: iconst_1       
        //   921: newarray        C
        //   923: dup            
        //   924: iconst_0       
        //   925: iload_2        
        //   926: bipush          48
        //   928: iadd           
        //   929: i2c            
        //   930: castore        
        //   931: invokespecial   java/lang/String.<init>:([C)V
        //   934: astore_1       
        //   935: aload           14
        //   937: invokevirtual   java/io/InputStream.close:()V
        //   940: goto            955
        //   943: astore          13
        //   945: ldc             "ExifAttribute"
        //   947: ldc_w           "IOException occurred while closing InputStream"
        //   950: aload           13
        //   952: invokestatic    androidx/camera/core/Logger.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   955: aload_1        
        //   956: areturn        
        //   957: aload           14
        //   959: astore          13
        //   961: new             Ljava/lang/String;
        //   964: dup            
        //   965: aload_1        
        //   966: getstatic       androidx/camera/core/impl/utils/ExifAttribute.ASCII:Ljava/nio/charset/Charset;
        //   969: invokespecial   java/lang/String.<init>:([BLjava/nio/charset/Charset;)V
        //   972: astore_1       
        //   973: aload           14
        //   975: invokevirtual   java/io/InputStream.close:()V
        //   978: goto            993
        //   981: astore          13
        //   983: ldc             "ExifAttribute"
        //   985: ldc_w           "IOException occurred while closing InputStream"
        //   988: aload           13
        //   990: invokestatic    androidx/camera/core/Logger.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   993: aload_1        
        //   994: areturn        
        //   995: aload           14
        //   997: invokevirtual   java/io/InputStream.close:()V
        //  1000: goto            1013
        //  1003: astore_1       
        //  1004: ldc             "ExifAttribute"
        //  1006: ldc_w           "IOException occurred while closing InputStream"
        //  1009: aload_1        
        //  1010: invokestatic    androidx/camera/core/Logger.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //  1013: aconst_null    
        //  1014: areturn        
        //  1015: astore          13
        //  1017: aload           14
        //  1019: astore_1       
        //  1020: aload           13
        //  1022: astore          14
        //  1024: goto            1035
        //  1027: astore_1       
        //  1028: goto            1072
        //  1031: astore          14
        //  1033: aconst_null    
        //  1034: astore_1       
        //  1035: aload_1        
        //  1036: astore          13
        //  1038: ldc             "ExifAttribute"
        //  1040: ldc_w           "IOException occurred during reading a value"
        //  1043: aload           14
        //  1045: invokestatic    androidx/camera/core/Logger.w:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //  1048: aload_1        
        //  1049: ifnull          1069
        //  1052: aload_1        
        //  1053: invokevirtual   java/io/InputStream.close:()V
        //  1056: goto            1069
        //  1059: astore_1       
        //  1060: ldc             "ExifAttribute"
        //  1062: ldc_w           "IOException occurred while closing InputStream"
        //  1065: aload_1        
        //  1066: invokestatic    androidx/camera/core/Logger.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //  1069: aconst_null    
        //  1070: areturn        
        //  1071: astore_1       
        //  1072: aload           13
        //  1074: ifnull          1097
        //  1077: aload           13
        //  1079: invokevirtual   java/io/InputStream.close:()V
        //  1082: goto            1097
        //  1085: astore          13
        //  1087: ldc             "ExifAttribute"
        //  1089: ldc_w           "IOException occurred while closing InputStream"
        //  1092: aload           13
        //  1094: invokestatic    androidx/camera/core/Logger.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //  1097: aload_1        
        //  1098: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  3      17     1031   1035   Ljava/io/IOException;
        //  3      17     1027   1031   Any
        //  21     27     1015   1027   Ljava/io/IOException;
        //  21     27     1071   1072   Any
        //  31     37     1015   1027   Ljava/io/IOException;
        //  31     37     1071   1072   Any
        //  135    142    1015   1027   Ljava/io/IOException;
        //  135    142    1071   1072   Any
        //  149    157    1015   1027   Ljava/io/IOException;
        //  149    157    1071   1072   Any
        //  161    169    1015   1027   Ljava/io/IOException;
        //  161    169    1071   1072   Any
        //  175    180    183    195    Ljava/io/IOException;
        //  201    208    1015   1027   Ljava/io/IOException;
        //  201    208    1071   1072   Any
        //  215    223    1015   1027   Ljava/io/IOException;
        //  215    223    1071   1072   Any
        //  227    236    1015   1027   Ljava/io/IOException;
        //  227    236    1071   1072   Any
        //  242    247    250    262    Ljava/io/IOException;
        //  268    276    1015   1027   Ljava/io/IOException;
        //  268    276    1071   1072   Any
        //  283    291    1015   1027   Ljava/io/IOException;
        //  283    291    1071   1072   Any
        //  295    317    1015   1027   Ljava/io/IOException;
        //  295    317    1071   1072   Any
        //  323    328    331    343    Ljava/io/IOException;
        //  349    356    1015   1027   Ljava/io/IOException;
        //  349    356    1071   1072   Any
        //  363    371    1015   1027   Ljava/io/IOException;
        //  363    371    1071   1072   Any
        //  375    383    1015   1027   Ljava/io/IOException;
        //  375    383    1071   1072   Any
        //  389    394    397    409    Ljava/io/IOException;
        //  415    422    1015   1027   Ljava/io/IOException;
        //  415    422    1071   1072   Any
        //  429    437    1015   1027   Ljava/io/IOException;
        //  429    437    1071   1072   Any
        //  441    449    1015   1027   Ljava/io/IOException;
        //  441    449    1071   1072   Any
        //  455    460    463    475    Ljava/io/IOException;
        //  481    489    1015   1027   Ljava/io/IOException;
        //  481    489    1071   1072   Any
        //  496    504    1015   1027   Ljava/io/IOException;
        //  496    504    1071   1072   Any
        //  508    528    1015   1027   Ljava/io/IOException;
        //  508    528    1071   1072   Any
        //  534    539    542    554    Ljava/io/IOException;
        //  560    567    1015   1027   Ljava/io/IOException;
        //  560    567    1071   1072   Any
        //  571    579    1015   1027   Ljava/io/IOException;
        //  571    579    1071   1072   Any
        //  583    591    1015   1027   Ljava/io/IOException;
        //  583    591    1071   1072   Any
        //  597    602    605    617    Ljava/io/IOException;
        //  623    630    1015   1027   Ljava/io/IOException;
        //  623    630    1071   1072   Any
        //  636    644    1015   1027   Ljava/io/IOException;
        //  636    644    1071   1072   Any
        //  648    656    1015   1027   Ljava/io/IOException;
        //  648    656    1071   1072   Any
        //  662    667    670    682    Ljava/io/IOException;
        //  691    702    1015   1027   Ljava/io/IOException;
        //  691    702    1071   1072   Any
        //  708    712    1015   1027   Ljava/io/IOException;
        //  708    712    1071   1072   Any
        //  719    725    1015   1027   Ljava/io/IOException;
        //  719    725    1071   1072   Any
        //  729    741    1015   1027   Ljava/io/IOException;
        //  729    741    1071   1072   Any
        //  763    766    1015   1027   Ljava/io/IOException;
        //  763    766    1071   1072   Any
        //  770    774    1015   1027   Ljava/io/IOException;
        //  770    774    1071   1072   Any
        //  778    782    1015   1027   Ljava/io/IOException;
        //  778    782    1071   1072   Any
        //  786    794    1015   1027   Ljava/io/IOException;
        //  786    794    1071   1072   Any
        //  798    805    1015   1027   Ljava/io/IOException;
        //  798    805    1071   1072   Any
        //  822    829    1015   1027   Ljava/io/IOException;
        //  822    829    1071   1072   Any
        //  836    843    1015   1027   Ljava/io/IOException;
        //  836    843    1071   1072   Any
        //  853    858    1015   1027   Ljava/io/IOException;
        //  853    858    1071   1072   Any
        //  858    863    866    878    Ljava/io/IOException;
        //  884    889    1015   1027   Ljava/io/IOException;
        //  884    889    1071   1072   Any
        //  893    899    1015   1027   Ljava/io/IOException;
        //  893    899    1071   1072   Any
        //  916    935    1015   1027   Ljava/io/IOException;
        //  916    935    1071   1072   Any
        //  935    940    943    955    Ljava/io/IOException;
        //  961    973    1015   1027   Ljava/io/IOException;
        //  961    973    1071   1072   Any
        //  973    978    981    993    Ljava/io/IOException;
        //  995    1000   1003   1013   Ljava/io/IOException;
        //  1038   1048   1071   1072   Any
        //  1052   1056   1059   1069   Ljava/io/IOException;
        //  1077   1082   1085   1097   Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0128:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public int size() {
        return ExifAttribute.IFD_FORMAT_BYTES_PER_FORMAT[this.format] * this.numberOfComponents;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("(");
        sb.append(ExifAttribute.IFD_FORMAT_NAMES[this.format]);
        sb.append(", data length:");
        sb.append(this.bytes.length);
        sb.append(")");
        return sb.toString();
    }
}

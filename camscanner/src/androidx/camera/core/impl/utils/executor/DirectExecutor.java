// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils.executor;

import androidx.annotation.RequiresApi;
import java.util.concurrent.Executor;

@RequiresApi(21)
final class DirectExecutor implements Executor
{
    private static volatile DirectExecutor sDirectExecutor;
    
    static Executor getInstance() {
        if (DirectExecutor.sDirectExecutor != null) {
            return DirectExecutor.sDirectExecutor;
        }
        synchronized (DirectExecutor.class) {
            if (DirectExecutor.sDirectExecutor == null) {
                DirectExecutor.sDirectExecutor = new DirectExecutor();
            }
            return DirectExecutor.sDirectExecutor;
        }
    }
    
    @Override
    public void execute(final Runnable runnable) {
        runnable.run();
    }
}

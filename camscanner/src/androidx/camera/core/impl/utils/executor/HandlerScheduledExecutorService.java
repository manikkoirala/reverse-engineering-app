// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils.executor;

import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Delayed;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.RunnableScheduledFuture;
import java.util.List;
import androidx.camera.core.impl.utils.futures.Futures;
import android.os.SystemClock;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.RejectedExecutionException;
import androidx.annotation.NonNull;
import android.os.Looper;
import android.os.Handler;
import androidx.annotation.RequiresApi;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.AbstractExecutorService;

@RequiresApi(21)
final class HandlerScheduledExecutorService extends AbstractExecutorService implements ScheduledExecutorService
{
    private static ThreadLocal<ScheduledExecutorService> sThreadLocalInstance;
    private final Handler mHandler;
    
    static {
        HandlerScheduledExecutorService.sThreadLocalInstance = new ThreadLocal<ScheduledExecutorService>() {
            public ScheduledExecutorService initialValue() {
                if (Looper.myLooper() == Looper.getMainLooper()) {
                    return CameraXExecutors.mainThreadExecutor();
                }
                if (Looper.myLooper() != null) {
                    return new HandlerScheduledExecutorService(new Handler(Looper.myLooper()));
                }
                return null;
            }
        };
    }
    
    HandlerScheduledExecutorService(@NonNull final Handler mHandler) {
        this.mHandler = mHandler;
    }
    
    private RejectedExecutionException createPostFailedException() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.mHandler);
        sb.append(" is shutting down");
        return new RejectedExecutionException(sb.toString());
    }
    
    static ScheduledExecutorService currentThreadExecutor() {
        ScheduledExecutorService value;
        if ((value = HandlerScheduledExecutorService.sThreadLocalInstance.get()) == null) {
            final Looper myLooper = Looper.myLooper();
            if (myLooper == null) {
                throw new IllegalStateException("Current thread has no looper!");
            }
            value = new HandlerScheduledExecutorService(new Handler(myLooper));
            HandlerScheduledExecutorService.sThreadLocalInstance.set(value);
        }
        return value;
    }
    
    @Override
    public boolean awaitTermination(final long n, @NonNull final TimeUnit timeUnit) {
        final StringBuilder sb = new StringBuilder();
        sb.append(HandlerScheduledExecutorService.class.getSimpleName());
        sb.append(" cannot be shut down. Use Looper.quitSafely().");
        throw new UnsupportedOperationException(sb.toString());
    }
    
    @Override
    public void execute(@NonNull final Runnable runnable) {
        if (this.mHandler.post(runnable)) {
            return;
        }
        throw this.createPostFailedException();
    }
    
    @Override
    public boolean isShutdown() {
        return false;
    }
    
    @Override
    public boolean isTerminated() {
        return false;
    }
    
    @Override
    public ScheduledFuture<?> schedule(@NonNull final Runnable runnable, final long n, @NonNull final TimeUnit timeUnit) {
        return this.schedule((Callable<?>)new Callable<Void>(this, runnable) {
            final HandlerScheduledExecutorService this$0;
            final Runnable val$command;
            
            @Override
            public Void call() {
                this.val$command.run();
                return null;
            }
        }, n, timeUnit);
    }
    
    @NonNull
    @Override
    public <V> ScheduledFuture<V> schedule(@NonNull final Callable<V> callable, long sourceDuration, @NonNull final TimeUnit sourceUnit) {
        sourceDuration = SystemClock.uptimeMillis() + TimeUnit.MILLISECONDS.convert(sourceDuration, sourceUnit);
        final HandlerScheduledFuture handlerScheduledFuture = new HandlerScheduledFuture(this.mHandler, sourceDuration, (Callable<V>)callable);
        if (this.mHandler.postAtTime((Runnable)handlerScheduledFuture, sourceDuration)) {
            return handlerScheduledFuture;
        }
        return Futures.immediateFailedScheduledFuture(this.createPostFailedException());
    }
    
    @NonNull
    @Override
    public ScheduledFuture<?> scheduleAtFixedRate(@NonNull final Runnable runnable, final long n, final long n2, @NonNull final TimeUnit timeUnit) {
        final StringBuilder sb = new StringBuilder();
        sb.append(HandlerScheduledExecutorService.class.getSimpleName());
        sb.append(" does not yet support fixed-rate scheduling.");
        throw new UnsupportedOperationException(sb.toString());
    }
    
    @NonNull
    @Override
    public ScheduledFuture<?> scheduleWithFixedDelay(@NonNull final Runnable runnable, final long n, final long n2, @NonNull final TimeUnit timeUnit) {
        final StringBuilder sb = new StringBuilder();
        sb.append(HandlerScheduledExecutorService.class.getSimpleName());
        sb.append(" does not yet support fixed-delay scheduling.");
        throw new UnsupportedOperationException(sb.toString());
    }
    
    @Override
    public void shutdown() {
        final StringBuilder sb = new StringBuilder();
        sb.append(HandlerScheduledExecutorService.class.getSimpleName());
        sb.append(" cannot be shut down. Use Looper.quitSafely().");
        throw new UnsupportedOperationException(sb.toString());
    }
    
    @NonNull
    @Override
    public List<Runnable> shutdownNow() {
        final StringBuilder sb = new StringBuilder();
        sb.append(HandlerScheduledExecutorService.class.getSimpleName());
        sb.append(" cannot be shut down. Use Looper.quitSafely().");
        throw new UnsupportedOperationException(sb.toString());
    }
    
    private static class HandlerScheduledFuture<V> implements RunnableScheduledFuture<V>
    {
        final AtomicReference<CallbackToFutureAdapter.Completer<V>> mCompleter;
        private final ListenableFuture<V> mDelegate;
        private final long mRunAtMillis;
        private final Callable<V> mTask;
        
        HandlerScheduledFuture(final Handler handler, final long mRunAtMillis, final Callable<V> mTask) {
            this.mCompleter = new AtomicReference<CallbackToFutureAdapter.Completer<V>>(null);
            this.mRunAtMillis = mRunAtMillis;
            this.mTask = mTask;
            this.mDelegate = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<V>)new CallbackToFutureAdapter.Resolver<V>(this, handler, mTask) {
                final HandlerScheduledFuture this$0;
                final Handler val$handler;
                final Callable val$task;
                
                @Override
                public Object attachCompleter(@NonNull final Completer<V> newValue) throws RejectedExecutionException {
                    newValue.addCancellationListener(new Runnable(this) {
                        final HandlerScheduledExecutorService$HandlerScheduledFuture$1 this$1;
                        
                        @Override
                        public void run() {
                            if (this.this$1.this$0.mCompleter.getAndSet(null) != null) {
                                final Resolver<V> this$1 = this.this$1;
                                this$1.val$handler.removeCallbacks((Runnable)this$1.this$0);
                            }
                        }
                    }, CameraXExecutors.directExecutor());
                    this.this$0.mCompleter.set(newValue);
                    final StringBuilder sb = new StringBuilder();
                    sb.append("HandlerScheduledFuture-");
                    sb.append(this.val$task.toString());
                    return sb.toString();
                }
            });
        }
        
        @Override
        public boolean cancel(final boolean b) {
            return ((Future)this.mDelegate).cancel(b);
        }
        
        @Override
        public int compareTo(final Delayed delayed) {
            final TimeUnit milliseconds = TimeUnit.MILLISECONDS;
            return Long.compare(this.getDelay(milliseconds), delayed.getDelay(milliseconds));
        }
        
        @Override
        public V get() throws ExecutionException, InterruptedException {
            return ((Future<V>)this.mDelegate).get();
        }
        
        @Override
        public V get(final long n, @NonNull final TimeUnit timeUnit) throws ExecutionException, InterruptedException, TimeoutException {
            return ((Future<V>)this.mDelegate).get(n, timeUnit);
        }
        
        @Override
        public long getDelay(final TimeUnit timeUnit) {
            return timeUnit.convert(this.mRunAtMillis - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
        }
        
        @Override
        public boolean isCancelled() {
            return ((Future)this.mDelegate).isCancelled();
        }
        
        @Override
        public boolean isDone() {
            return ((Future)this.mDelegate).isDone();
        }
        
        @Override
        public boolean isPeriodic() {
            return false;
        }
        
        @Override
        public void run() {
            final CallbackToFutureAdapter.Completer completer = this.mCompleter.getAndSet(null);
            if (completer != null) {
                try {
                    completer.set(this.mTask.call());
                }
                catch (final Exception exception) {
                    completer.setException(exception);
                }
            }
        }
    }
}

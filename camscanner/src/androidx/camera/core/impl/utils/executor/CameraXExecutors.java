// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl.utils.executor;

import android.os.Handler;
import java.util.concurrent.ScheduledExecutorService;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class CameraXExecutors
{
    private CameraXExecutors() {
    }
    
    @NonNull
    public static Executor directExecutor() {
        return DirectExecutor.getInstance();
    }
    
    @NonNull
    public static Executor highPriorityExecutor() {
        return HighPriorityExecutor.getInstance();
    }
    
    @NonNull
    public static Executor ioExecutor() {
        return IoExecutor.getInstance();
    }
    
    public static boolean isSequentialExecutor(@NonNull final Executor executor) {
        return executor instanceof SequentialExecutor;
    }
    
    @NonNull
    public static ScheduledExecutorService mainThreadExecutor() {
        return MainThreadExecutor.getInstance();
    }
    
    @NonNull
    public static ScheduledExecutorService myLooperExecutor() {
        return HandlerScheduledExecutorService.currentThreadExecutor();
    }
    
    @NonNull
    public static ScheduledExecutorService newHandlerExecutor(@NonNull final Handler handler) {
        return new HandlerScheduledExecutorService(handler);
    }
    
    @NonNull
    public static Executor newSequentialExecutor(@NonNull final Executor executor) {
        return new SequentialExecutor(executor);
    }
}

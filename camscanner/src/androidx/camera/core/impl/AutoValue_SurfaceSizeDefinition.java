// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.NonNull;
import android.util.Size;

final class AutoValue_SurfaceSizeDefinition extends SurfaceSizeDefinition
{
    private final Size analysisSize;
    private final Size previewSize;
    private final Size recordSize;
    
    AutoValue_SurfaceSizeDefinition(final Size analysisSize, final Size previewSize, final Size recordSize) {
        if (analysisSize == null) {
            throw new NullPointerException("Null analysisSize");
        }
        this.analysisSize = analysisSize;
        if (previewSize == null) {
            throw new NullPointerException("Null previewSize");
        }
        this.previewSize = previewSize;
        if (recordSize != null) {
            this.recordSize = recordSize;
            return;
        }
        throw new NullPointerException("Null recordSize");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof SurfaceSizeDefinition) {
            final SurfaceSizeDefinition surfaceSizeDefinition = (SurfaceSizeDefinition)o;
            if (!this.analysisSize.equals((Object)surfaceSizeDefinition.getAnalysisSize()) || !this.previewSize.equals((Object)surfaceSizeDefinition.getPreviewSize()) || !this.recordSize.equals((Object)surfaceSizeDefinition.getRecordSize())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @NonNull
    @Override
    public Size getAnalysisSize() {
        return this.analysisSize;
    }
    
    @NonNull
    @Override
    public Size getPreviewSize() {
        return this.previewSize;
    }
    
    @NonNull
    @Override
    public Size getRecordSize() {
        return this.recordSize;
    }
    
    @Override
    public int hashCode() {
        return ((this.analysisSize.hashCode() ^ 0xF4243) * 1000003 ^ this.previewSize.hashCode()) * 1000003 ^ this.recordSize.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SurfaceSizeDefinition{analysisSize=");
        sb.append(this.analysisSize);
        sb.append(", previewSize=");
        sb.append(this.previewSize);
        sb.append(", recordSize=");
        sb.append(this.recordSize);
        sb.append("}");
        return sb.toString();
    }
}

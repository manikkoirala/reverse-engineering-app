// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.internal.UseCaseEventConfig;
import androidx.camera.core.internal.o\u30070;
import androidx.camera.core.UseCase;
import android.util.Range;
import androidx.camera.core.internal.TargetConfig;
import androidx.camera.core.internal.O8;
import java.util.List;
import java.util.Set;
import android.util.Size;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.internal.Oo08;
import java.util.concurrent.Executor;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.camera.core.internal.ThreadConfig;
import androidx.camera.core.VideoCapture;

@RequiresApi(21)
public final class VideoCaptureConfig implements UseCaseConfig<VideoCapture>, ImageOutputConfig, ThreadConfig
{
    public static final Option<Integer> OPTION_AUDIO_BIT_RATE;
    public static final Option<Integer> OPTION_AUDIO_CHANNEL_COUNT;
    public static final Option<Integer> OPTION_AUDIO_MIN_BUFFER_SIZE;
    public static final Option<Integer> OPTION_AUDIO_SAMPLE_RATE;
    public static final Option<Integer> OPTION_BIT_RATE;
    public static final Option<Integer> OPTION_INTRA_FRAME_INTERVAL;
    public static final Option<Integer> OPTION_VIDEO_FRAME_RATE;
    private final OptionsBundle mConfig;
    
    static {
        final Class<Integer> type = Integer.TYPE;
        OPTION_VIDEO_FRAME_RATE = (Option)Config.Option.create("camerax.core.videoCapture.recordingFrameRate", type);
        OPTION_BIT_RATE = (Option)Config.Option.create("camerax.core.videoCapture.bitRate", type);
        OPTION_INTRA_FRAME_INTERVAL = (Option)Config.Option.create("camerax.core.videoCapture.intraFrameInterval", type);
        OPTION_AUDIO_BIT_RATE = (Option)Config.Option.create("camerax.core.videoCapture.audioBitRate", type);
        OPTION_AUDIO_SAMPLE_RATE = (Option)Config.Option.create("camerax.core.videoCapture.audioSampleRate", type);
        OPTION_AUDIO_CHANNEL_COUNT = (Option)Config.Option.create("camerax.core.videoCapture.audioChannelCount", type);
        OPTION_AUDIO_MIN_BUFFER_SIZE = (Option)Config.Option.create("camerax.core.videoCapture.audioMinBufferSize", type);
    }
    
    public VideoCaptureConfig(@NonNull final OptionsBundle mConfig) {
        this.mConfig = mConfig;
    }
    
    public int getAudioBitRate() {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_AUDIO_BIT_RATE);
    }
    
    public int getAudioBitRate(final int i) {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_AUDIO_BIT_RATE, i);
    }
    
    public int getAudioChannelCount() {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_AUDIO_CHANNEL_COUNT);
    }
    
    public int getAudioChannelCount(final int i) {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_AUDIO_CHANNEL_COUNT, i);
    }
    
    public int getAudioMinBufferSize() {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_AUDIO_MIN_BUFFER_SIZE);
    }
    
    public int getAudioMinBufferSize(final int i) {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_AUDIO_MIN_BUFFER_SIZE, i);
    }
    
    public int getAudioSampleRate() {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_AUDIO_SAMPLE_RATE);
    }
    
    public int getAudioSampleRate(final int i) {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_AUDIO_SAMPLE_RATE, i);
    }
    
    public int getBitRate() {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_BIT_RATE);
    }
    
    public int getBitRate(final int i) {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_BIT_RATE, i);
    }
    
    @NonNull
    @Override
    public Config getConfig() {
        return this.mConfig;
    }
    
    public int getIFrameInterval() {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_INTRA_FRAME_INTERVAL);
    }
    
    public int getIFrameInterval(final int i) {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_INTRA_FRAME_INTERVAL, i);
    }
    
    @Override
    public int getInputFormat() {
        return 34;
    }
    
    public int getVideoFrameRate() {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_VIDEO_FRAME_RATE);
    }
    
    public int getVideoFrameRate(final int i) {
        return (int)this.retrieveOption(VideoCaptureConfig.OPTION_VIDEO_FRAME_RATE, i);
    }
    
    @Override
    public /* synthetic */ Object retrieveOption(final Option option) {
        return o\u3007\u30070\u3007.o\u30070(this, option);
    }
    
    @Override
    public /* synthetic */ Object retrieveOption(final Option option, final Object o) {
        return o\u3007\u30070\u3007.\u3007\u3007888(this, option, o);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.camera.core.AspectRatio;
import android.util.Pair;
import java.util.List;
import android.util.Size;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface ImageOutputConfig extends ReadableConfig
{
    public static final int INVALID_ROTATION = -1;
    public static final Option<Integer> OPTION_APP_TARGET_ROTATION = Config.Option.create("camerax.core.imageOutput.appTargetRotation", type);
    public static final Option<Size> OPTION_DEFAULT_RESOLUTION = Config.Option.create("camerax.core.imageOutput.defaultResolution", Size.class);
    public static final Option<Size> OPTION_MAX_RESOLUTION = Config.Option.create("camerax.core.imageOutput.maxResolution", Size.class);
    public static final Option<List<Pair<Integer, Size[]>>> OPTION_SUPPORTED_RESOLUTIONS = Config.Option.create("camerax.core.imageOutput.supportedResolutions", List.class);
    public static final Option<Integer> OPTION_TARGET_ASPECT_RATIO = Config.Option.create("camerax.core.imageOutput.targetAspectRatio", AspectRatio.class);
    public static final Option<Size> OPTION_TARGET_RESOLUTION = Config.Option.create("camerax.core.imageOutput.targetResolution", Size.class);
    public static final Option<Integer> OPTION_TARGET_ROTATION = Config.Option.create("camerax.core.imageOutput.targetRotation", type);
    public static final int ROTATION_NOT_SPECIFIED = -1;
    
    default static {
        final Class<Integer> type = Integer.TYPE;
    }
    
    int getAppTargetRotation(final int p0);
    
    @NonNull
    Size getDefaultResolution();
    
    @Nullable
    Size getDefaultResolution(@Nullable final Size p0);
    
    @NonNull
    Size getMaxResolution();
    
    @Nullable
    Size getMaxResolution(@Nullable final Size p0);
    
    @NonNull
    List<Pair<Integer, Size[]>> getSupportedResolutions();
    
    @Nullable
    List<Pair<Integer, Size[]>> getSupportedResolutions(@Nullable final List<Pair<Integer, Size[]>> p0);
    
    int getTargetAspectRatio();
    
    @NonNull
    Size getTargetResolution();
    
    @Nullable
    Size getTargetResolution(@Nullable final Size p0);
    
    int getTargetRotation();
    
    int getTargetRotation(final int p0);
    
    boolean hasTargetAspectRatio();
    
    public interface Builder<B>
    {
        @NonNull
        B setDefaultResolution(@NonNull final Size p0);
        
        @NonNull
        B setMaxResolution(@NonNull final Size p0);
        
        @NonNull
        B setSupportedResolutions(@NonNull final List<Pair<Integer, Size[]>> p0);
        
        @NonNull
        B setTargetAspectRatio(final int p0);
        
        @NonNull
        B setTargetResolution(@NonNull final Size p0);
        
        @NonNull
        B setTargetRotation(final int p0);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface OptionalRotationValue {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface RotationDegreesValue {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface RotationValue {
    }
}

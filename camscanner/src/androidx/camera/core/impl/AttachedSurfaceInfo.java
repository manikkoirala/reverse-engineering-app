// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.Nullable;
import android.util.Range;
import android.util.Size;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public abstract class AttachedSurfaceInfo
{
    AttachedSurfaceInfo() {
    }
    
    @NonNull
    public static AttachedSurfaceInfo create(@NonNull final SurfaceConfig surfaceConfig, final int n, @NonNull final Size size, @Nullable final Range<Integer> range) {
        return new AutoValue_AttachedSurfaceInfo(surfaceConfig, n, size, range);
    }
    
    public abstract int getImageFormat();
    
    @NonNull
    public abstract Size getSize();
    
    @NonNull
    public abstract SurfaceConfig getSurfaceConfig();
    
    @Nullable
    public abstract Range<Integer> getTargetFrameRate();
}

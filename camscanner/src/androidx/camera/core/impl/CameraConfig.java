// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface CameraConfig extends ReadableConfig
{
    public static final Option<Identifier> OPTION_COMPATIBILITY_ID = Config.Option.create("camerax.core.camera.compatibilityId", Identifier.class);
    public static final Option<SessionProcessor> OPTION_SESSION_PROCESSOR = Config.Option.create("camerax.core.camera.SessionProcessor", SessionProcessor.class);
    public static final Option<UseCaseConfigFactory> OPTION_USECASE_CONFIG_FACTORY = Config.Option.create("camerax.core.camera.useCaseConfigFactory", UseCaseConfigFactory.class);
    public static final Option<Integer> OPTION_USE_CASE_COMBINATION_REQUIRED_RULE = Config.Option.create("camerax.core.camera.useCaseCombinationRequiredRule", Integer.class);
    public static final Option<Boolean> OPTION_ZSL_DISABLED = Config.Option.create("camerax.core.camera.isZslDisabled", Boolean.class);
    public static final int REQUIRED_RULE_COEXISTING_PREVIEW_AND_IMAGE_CAPTURE = 1;
    public static final int REQUIRED_RULE_NONE = 0;
    
    @NonNull
    Identifier getCompatibilityId();
    
    @NonNull
    SessionProcessor getSessionProcessor();
    
    @Nullable
    SessionProcessor getSessionProcessor(@Nullable final SessionProcessor p0);
    
    int getUseCaseCombinationRequiredRule();
    
    @NonNull
    UseCaseConfigFactory getUseCaseConfigFactory();
    
    public interface Builder<B>
    {
        @NonNull
        B setCompatibilityId(@NonNull final Identifier p0);
        
        @NonNull
        B setSessionProcessor(@NonNull final SessionProcessor p0);
        
        @NonNull
        B setUseCaseCombinationRequiredRule(final int p0);
        
        @NonNull
        B setUseCaseConfigFactory(@NonNull final UseCaseConfigFactory p0);
        
        @NonNull
        B setZslDisabled(final boolean p0);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface RequiredRule {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.internal.utils.SizeUtil;
import android.util.Size;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public abstract class SurfaceConfig
{
    SurfaceConfig() {
    }
    
    @NonNull
    public static SurfaceConfig create(@NonNull final ConfigType configType, @NonNull final ConfigSize configSize) {
        return new AutoValue_SurfaceConfig(configType, configSize);
    }
    
    @NonNull
    public static ConfigType getConfigType(final int n) {
        if (n == 35) {
            return ConfigType.YUV;
        }
        if (n == 256) {
            return ConfigType.JPEG;
        }
        if (n == 32) {
            return ConfigType.RAW;
        }
        return ConfigType.PRIV;
    }
    
    @NonNull
    public static SurfaceConfig transformSurfaceConfig(int area, @NonNull final Size size, @NonNull final SurfaceSizeDefinition surfaceSizeDefinition) {
        final ConfigType configType = getConfigType(area);
        final ConfigSize vga = ConfigSize.VGA;
        area = SizeUtil.getArea(size);
        ConfigSize configSize;
        if (area <= SizeUtil.getArea(surfaceSizeDefinition.getAnalysisSize())) {
            configSize = ConfigSize.VGA;
        }
        else if (area <= SizeUtil.getArea(surfaceSizeDefinition.getPreviewSize())) {
            configSize = ConfigSize.PREVIEW;
        }
        else if (area <= SizeUtil.getArea(surfaceSizeDefinition.getRecordSize())) {
            configSize = ConfigSize.RECORD;
        }
        else {
            configSize = ConfigSize.MAXIMUM;
        }
        return create(configType, configSize);
    }
    
    @NonNull
    public abstract ConfigSize getConfigSize();
    
    @NonNull
    public abstract ConfigType getConfigType();
    
    public final boolean isSupported(@NonNull final SurfaceConfig surfaceConfig) {
        final ConfigType configType = surfaceConfig.getConfigType();
        return surfaceConfig.getConfigSize().getId() <= this.getConfigSize().getId() && configType == this.getConfigType();
    }
    
    public enum ConfigSize
    {
        private static final ConfigSize[] $VALUES;
        
        MAXIMUM(3), 
        NOT_SUPPORT(4), 
        PREVIEW(1), 
        RECORD(2), 
        VGA(0);
        
        final int mId;
        
        private ConfigSize(final int mId) {
            this.mId = mId;
        }
        
        int getId() {
            return this.mId;
        }
    }
    
    public enum ConfigType
    {
        private static final ConfigType[] $VALUES;
        
        JPEG, 
        PRIV, 
        RAW, 
        YUV;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.impl.utils.ExifData;
import android.hardware.camera2.CaptureResult;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface CameraCaptureResult
{
    @NonNull
    CameraCaptureMetaData.AeState getAeState();
    
    @NonNull
    CameraCaptureMetaData.AfMode getAfMode();
    
    @NonNull
    CameraCaptureMetaData.AfState getAfState();
    
    @NonNull
    CameraCaptureMetaData.AwbState getAwbState();
    
    @NonNull
    CaptureResult getCaptureResult();
    
    @NonNull
    CameraCaptureMetaData.FlashState getFlashState();
    
    @NonNull
    TagBundle getTagBundle();
    
    long getTimestamp();
    
    void populateExifData(@NonNull final ExifData.Builder p0);
    
    public static final class EmptyCameraCaptureResult implements CameraCaptureResult
    {
        @NonNull
        public static CameraCaptureResult create() {
            return new EmptyCameraCaptureResult();
        }
        
        @NonNull
        @Override
        public CameraCaptureMetaData.AeState getAeState() {
            return CameraCaptureMetaData.AeState.UNKNOWN;
        }
        
        @NonNull
        @Override
        public CameraCaptureMetaData.AfMode getAfMode() {
            return CameraCaptureMetaData.AfMode.UNKNOWN;
        }
        
        @NonNull
        @Override
        public CameraCaptureMetaData.AfState getAfState() {
            return CameraCaptureMetaData.AfState.UNKNOWN;
        }
        
        @NonNull
        @Override
        public CameraCaptureMetaData.AwbState getAwbState() {
            return CameraCaptureMetaData.AwbState.UNKNOWN;
        }
        
        @NonNull
        @Override
        public CameraCaptureMetaData.FlashState getFlashState() {
            return CameraCaptureMetaData.FlashState.UNKNOWN;
        }
        
        @NonNull
        @Override
        public TagBundle getTagBundle() {
            return TagBundle.emptyBundle();
        }
        
        @Override
        public long getTimestamp() {
            return -1L;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class LensFacingConverter
{
    private LensFacingConverter() {
    }
    
    @NonNull
    public static String nameOf(final int i) {
        if (i == 0) {
            return "FRONT";
        }
        if (i == 1) {
            return "BACK";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unknown lens facing ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static int valueOf(@Nullable final String str) {
        if (str == null) {
            throw new NullPointerException("name cannot be null");
        }
        if (str.equals("BACK")) {
            return 1;
        }
        if (str.equals("FRONT")) {
            return 0;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unknown len facing name ");
        sb.append(str);
        throw new IllegalArgumentException(sb.toString());
    }
    
    @NonNull
    public static Integer[] values() {
        return new Integer[] { 0, 1 };
    }
}

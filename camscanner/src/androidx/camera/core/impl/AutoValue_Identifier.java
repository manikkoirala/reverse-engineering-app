// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.NonNull;

final class AutoValue_Identifier extends Identifier
{
    private final Object value;
    
    AutoValue_Identifier(final Object value) {
        if (value != null) {
            this.value = value;
            return;
        }
        throw new NullPointerException("Null value");
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof Identifier && this.value.equals(((Identifier)o).getValue()));
    }
    
    @NonNull
    @Override
    public Object getValue() {
        return this.value;
    }
    
    @Override
    public int hashCode() {
        return this.value.hashCode() ^ 0xF4243;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Identifier{value=");
        sb.append(this.value);
        sb.append("}");
        return sb.toString();
    }
}

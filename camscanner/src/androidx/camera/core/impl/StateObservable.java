// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.concurrent.atomic.AtomicBoolean;
import androidx.camera.core.impl.utils.futures.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Executor;
import java.util.Iterator;
import java.util.Objects;
import androidx.annotation.NonNull;
import androidx.core.util.Preconditions;
import java.util.HashMap;
import androidx.annotation.Nullable;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import androidx.annotation.GuardedBy;
import java.util.concurrent.CopyOnWriteArraySet;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public abstract class StateObservable<T> implements Observable<T>
{
    private static final int INITIAL_VERSION = 0;
    private final Object mLock;
    @GuardedBy("mLock")
    private final CopyOnWriteArraySet<ObserverWrapper<T>> mNotifySet;
    private final AtomicReference<Object> mState;
    @GuardedBy("mLock")
    private boolean mUpdating;
    @GuardedBy("mLock")
    private int mVersion;
    @GuardedBy("mLock")
    private final Map<Observer<? super T>, ObserverWrapper<T>> mWrapperMap;
    
    StateObservable(@Nullable final Object initialValue, final boolean b) {
        this.mLock = new Object();
        this.mVersion = 0;
        this.mUpdating = false;
        this.mWrapperMap = new HashMap<Observer<? super T>, ObserverWrapper<T>>();
        this.mNotifySet = new CopyOnWriteArraySet<ObserverWrapper<T>>();
        if (b) {
            Preconditions.checkArgument(initialValue instanceof Throwable, (Object)"Initial errors must be Throwable");
            this.mState = new AtomicReference<Object>(ErrorWrapper.wrap((Throwable)initialValue));
        }
        else {
            this.mState = new AtomicReference<Object>(initialValue);
        }
    }
    
    @GuardedBy("mLock")
    private void removeObserverLocked(@NonNull final Observer<? super T> observer) {
        final ObserverWrapper o = this.mWrapperMap.remove(observer);
        if (o != null) {
            o.close();
            this.mNotifySet.remove(o);
        }
    }
    
    private void updateStateInternal(@Nullable final Object o) {
        synchronized (this.mLock) {
            if (Objects.equals(this.mState.getAndSet(o), o)) {
                return;
            }
            int mVersion = this.mVersion + 1;
            this.mVersion = mVersion;
            if (this.mUpdating) {
                return;
            }
            this.mUpdating = true;
            final Iterator<ObserverWrapper<T>> iterator = this.mNotifySet.iterator();
            monitorexit(this.mLock);
            while (true) {
                if (iterator.hasNext()) {
                    iterator.next().update(mVersion);
                }
                else {
                    final Object mLock = this.mLock;
                    synchronized (this.mLock) {
                        if (this.mVersion == mVersion) {
                            this.mUpdating = false;
                            return;
                        }
                        this.mNotifySet.iterator();
                        mVersion = this.mVersion;
                        continue;
                    }
                }
            }
        }
    }
    
    @Override
    public void addObserver(@NonNull final Executor executor, @NonNull final Observer<? super T> observer) {
        synchronized (this.mLock) {
            this.removeObserverLocked(observer);
            final ObserverWrapper e = new ObserverWrapper(this.mState, executor, (Observer<? super Object>)observer);
            this.mWrapperMap.put(observer, (ObserverWrapper<T>)e);
            this.mNotifySet.add((ObserverWrapper<T>)e);
            monitorexit(this.mLock);
            e.update(0);
        }
    }
    
    @NonNull
    @Override
    public ListenableFuture<T> fetchData() {
        final Object value = this.mState.get();
        if (value instanceof ErrorWrapper) {
            return (ListenableFuture<T>)Futures.immediateFailedFuture(((ErrorWrapper)value).getError());
        }
        return Futures.immediateFuture(value);
    }
    
    @Override
    public void removeObserver(@NonNull final Observer<? super T> observer) {
        synchronized (this.mLock) {
            this.removeObserverLocked(observer);
        }
    }
    
    void updateState(@Nullable final T t) {
        this.updateStateInternal(t);
    }
    
    void updateStateAsError(@NonNull final Throwable t) {
        this.updateStateInternal(ErrorWrapper.wrap(t));
    }
    
    abstract static class ErrorWrapper
    {
        @NonNull
        static ErrorWrapper wrap(@NonNull final Throwable t) {
            return (ErrorWrapper)new AutoValue_StateObservable_ErrorWrapper(t);
        }
        
        @NonNull
        public abstract Throwable getError();
    }
    
    private static final class ObserverWrapper<T> implements Runnable
    {
        private static final Object NOT_SET;
        private static final int NO_VERSION = -1;
        private final AtomicBoolean mActive;
        private final Executor mExecutor;
        private Object mLastState;
        @GuardedBy("this")
        private int mLatestSignalledVersion;
        private final Observer<? super T> mObserver;
        private final AtomicReference<Object> mStateRef;
        @GuardedBy("this")
        private boolean mWrapperUpdating;
        
        static {
            NOT_SET = new Object();
        }
        
        ObserverWrapper(@NonNull final AtomicReference<Object> mStateRef, @NonNull final Executor mExecutor, @NonNull final Observer<? super T> mObserver) {
            this.mActive = new AtomicBoolean(true);
            this.mLastState = ObserverWrapper.NOT_SET;
            this.mLatestSignalledVersion = -1;
            this.mWrapperUpdating = false;
            this.mStateRef = mStateRef;
            this.mExecutor = mExecutor;
            this.mObserver = mObserver;
        }
        
        void close() {
            this.mActive.set(false);
        }
        
        @Override
        public void run() {
            synchronized (this) {
                if (!this.mActive.get()) {
                    this.mWrapperUpdating = false;
                    return;
                }
                final Object value = this.mStateRef.get();
                int n = this.mLatestSignalledVersion;
                monitorexit(this);
                while (true) {
                    if (!Objects.equals(this.mLastState, value)) {
                        this.mLastState = value;
                        if (value instanceof ErrorWrapper) {
                            this.mObserver.onError(((ErrorWrapper)value).getError());
                        }
                        else {
                            this.mObserver.onNewData((Object)value);
                        }
                    }
                    synchronized (this) {
                        if (n != this.mLatestSignalledVersion && this.mActive.get()) {
                            this.mStateRef.get();
                            n = this.mLatestSignalledVersion;
                            continue;
                        }
                        this.mWrapperUpdating = false;
                    }
                }
            }
        }
        
        void update(final int mLatestSignalledVersion) {
            synchronized (this) {
                if (!this.mActive.get()) {
                    return;
                }
                if (mLatestSignalledVersion <= this.mLatestSignalledVersion) {
                    return;
                }
                this.mLatestSignalledVersion = mLatestSignalledVersion;
                if (this.mWrapperUpdating) {
                    return;
                }
                this.mWrapperUpdating = true;
                monitorexit(this);
                try {
                    this.mExecutor.execute(this);
                }
                finally {
                    synchronized (this) {
                        this.mWrapperUpdating = false;
                    }
                }
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.InitializationException;
import androidx.camera.core.CameraSelector;
import android.content.Context;
import androidx.annotation.Nullable;
import androidx.camera.core.CameraUnavailableException;
import androidx.annotation.NonNull;
import java.util.Set;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface CameraFactory
{
    @NonNull
    Set<String> getAvailableCameraIds();
    
    @NonNull
    CameraInternal getCamera(@NonNull final String p0) throws CameraUnavailableException;
    
    @Nullable
    Object getCameraManager();
    
    public interface Provider
    {
        @NonNull
        CameraFactory newInstance(@NonNull final Context p0, @NonNull final CameraThreadConfig p1, @Nullable final CameraSelector p2) throws InitializationException;
    }
}

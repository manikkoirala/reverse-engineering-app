// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.Collections;
import java.util.List;
import androidx.camera.core.FocusMeteringResult;
import androidx.camera.core.FocusMeteringAction;
import android.graphics.Rect;
import androidx.camera.core.impl.utils.futures.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.camera.core.CameraControl;

@RequiresApi(21)
public interface CameraControlInternal extends CameraControl
{
    public static final CameraControlInternal DEFAULT_EMPTY_INSTANCE = new CameraControlInternal() {
        @Override
        public void addInteropConfig(@NonNull final Config config) {
        }
        
        @Override
        public void addZslConfig(@NonNull final SessionConfig.Builder builder) {
        }
        
        @NonNull
        @Override
        public ListenableFuture<Void> cancelFocusAndMetering() {
            return Futures.immediateFuture((Void)null);
        }
        
        @Override
        public void clearInteropConfig() {
        }
        
        @NonNull
        @Override
        public ListenableFuture<Void> enableTorch(final boolean b) {
            return Futures.immediateFuture((Void)null);
        }
        
        @Override
        public int getFlashMode() {
            return 2;
        }
        
        @NonNull
        @Override
        public Config getInteropConfig() {
            return null;
        }
        
        @NonNull
        @Override
        public Rect getSensorRect() {
            return new Rect();
        }
        
        @NonNull
        @Override
        public SessionConfig getSessionConfig() {
            return SessionConfig.defaultEmptySessionConfig();
        }
        
        @Override
        public boolean isZslDisabledByByUserCaseConfig() {
            return false;
        }
        
        @NonNull
        @Override
        public ListenableFuture<Integer> setExposureCompensationIndex(final int n) {
            return Futures.immediateFuture(0);
        }
        
        @Override
        public void setFlashMode(final int n) {
        }
        
        @NonNull
        @Override
        public ListenableFuture<Void> setLinearZoom(final float n) {
            return Futures.immediateFuture((Void)null);
        }
        
        @NonNull
        @Override
        public ListenableFuture<Void> setZoomRatio(final float n) {
            return Futures.immediateFuture((Void)null);
        }
        
        @Override
        public void setZslDisabledByUserCaseConfig(final boolean b) {
        }
        
        @NonNull
        @Override
        public ListenableFuture<FocusMeteringResult> startFocusAndMetering(@NonNull final FocusMeteringAction focusMeteringAction) {
            return Futures.immediateFuture(FocusMeteringResult.emptyInstance());
        }
        
        @NonNull
        @Override
        public ListenableFuture<List<Void>> submitStillCaptureRequests(@NonNull final List<CaptureConfig> list, final int n, final int n2) {
            return Futures.immediateFuture(Collections.emptyList());
        }
    };
    
    void addInteropConfig(@NonNull final Config p0);
    
    void addZslConfig(@NonNull final SessionConfig.Builder p0);
    
    void clearInteropConfig();
    
    int getFlashMode();
    
    @NonNull
    Config getInteropConfig();
    
    @NonNull
    Rect getSensorRect();
    
    @NonNull
    SessionConfig getSessionConfig();
    
    boolean isZslDisabledByByUserCaseConfig();
    
    void setFlashMode(final int p0);
    
    void setZslDisabledByUserCaseConfig(final boolean p0);
    
    @NonNull
    ListenableFuture<List<Void>> submitStillCaptureRequests(@NonNull final List<CaptureConfig> p0, final int p1, final int p2);
    
    public static final class CameraControlException extends Exception
    {
        @NonNull
        private CameraCaptureFailure mCameraCaptureFailure;
        
        public CameraControlException(@NonNull final CameraCaptureFailure mCameraCaptureFailure) {
            this.mCameraCaptureFailure = mCameraCaptureFailure;
        }
        
        public CameraControlException(@NonNull final CameraCaptureFailure mCameraCaptureFailure, @NonNull final Throwable cause) {
            super(cause);
            this.mCameraCaptureFailure = mCameraCaptureFailure;
        }
        
        @NonNull
        public CameraCaptureFailure getCameraCaptureFailure() {
            return this.mCameraCaptureFailure;
        }
    }
    
    public interface ControlUpdateCallback
    {
        void onCameraControlCaptureRequests(@NonNull final List<CaptureConfig> p0);
        
        void onCameraControlUpdateSessionConfig();
    }
}

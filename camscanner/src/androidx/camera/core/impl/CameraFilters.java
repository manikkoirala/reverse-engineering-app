// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.Collections;
import java.util.List;
import androidx.camera.core.CameraFilter;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class CameraFilters
{
    public static final CameraFilter ANY;
    public static final CameraFilter NONE;
    
    static {
        ANY = new Oo08();
        NONE = new o\u30070();
    }
    
    private CameraFilters() {
    }
}

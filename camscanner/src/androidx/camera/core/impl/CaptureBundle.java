// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.Nullable;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface CaptureBundle
{
    @Nullable
    List<CaptureStage> getCaptureStages();
}

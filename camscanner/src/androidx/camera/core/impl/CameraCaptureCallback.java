// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public abstract class CameraCaptureCallback
{
    public void onCaptureCancelled() {
    }
    
    public void onCaptureCompleted(@NonNull final CameraCaptureResult cameraCaptureResult) {
    }
    
    public void onCaptureFailed(@NonNull final CameraCaptureFailure cameraCaptureFailure) {
    }
}

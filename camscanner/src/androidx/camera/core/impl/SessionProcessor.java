// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import android.hardware.camera2.CaptureResult$Key;
import java.util.Map;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.camera.core.CameraInfo;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface SessionProcessor
{
    void abortCapture(final int p0);
    
    void deInitSession();
    
    @NonNull
    SessionConfig initSession(@NonNull final CameraInfo p0, @NonNull final OutputSurface p1, @NonNull final OutputSurface p2, @Nullable final OutputSurface p3);
    
    void onCaptureSessionEnd();
    
    void onCaptureSessionStart(@NonNull final RequestProcessor p0);
    
    void setParameters(@NonNull final Config p0);
    
    int startCapture(@NonNull final CaptureCallback p0);
    
    int startRepeating(@NonNull final CaptureCallback p0);
    
    void stopRepeating();
    
    public interface CaptureCallback
    {
        void onCaptureCompleted(final long p0, final int p1, @NonNull final Map<CaptureResult$Key, Object> p2);
        
        void onCaptureFailed(final int p0);
        
        void onCaptureProcessStarted(final int p0);
        
        void onCaptureSequenceAborted(final int p0);
        
        void onCaptureSequenceCompleted(final int p0);
        
        void onCaptureStarted(final int p0, final long p1);
    }
}

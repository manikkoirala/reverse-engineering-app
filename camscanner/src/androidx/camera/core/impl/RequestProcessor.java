// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.List;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface RequestProcessor
{
    void abortCaptures();
    
    int setRepeating(@NonNull final Request p0, @NonNull final Callback p1);
    
    void stopRepeating();
    
    int submit(@NonNull final Request p0, @NonNull final Callback p1);
    
    int submit(@NonNull final List<Request> p0, @NonNull final Callback p1);
    
    public interface Callback
    {
        void onCaptureBufferLost(@NonNull final Request p0, final long p1, final int p2);
        
        void onCaptureCompleted(@NonNull final Request p0, @NonNull final CameraCaptureResult p1);
        
        void onCaptureFailed(@NonNull final Request p0, @NonNull final CameraCaptureFailure p1);
        
        void onCaptureProgressed(@NonNull final Request p0, @NonNull final CameraCaptureResult p1);
        
        void onCaptureSequenceAborted(final int p0);
        
        void onCaptureSequenceCompleted(final int p0, final long p1);
        
        void onCaptureStarted(@NonNull final Request p0, final long p1, final long p2);
    }
    
    public interface Request
    {
        @NonNull
        Config getParameters();
        
        @NonNull
        List<Integer> getTargetOutputConfigIds();
        
        int getTemplateId();
    }
}

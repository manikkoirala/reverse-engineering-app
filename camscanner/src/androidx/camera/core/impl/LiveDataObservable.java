// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.concurrent.atomic.AtomicBoolean;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;
import android.os.SystemClock;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.core.util.Preconditions;
import androidx.lifecycle.Observer;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import java.util.HashMap;
import androidx.annotation.GuardedBy;
import java.util.Map;
import androidx.lifecycle.MutableLiveData;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class LiveDataObservable<T> implements Observable<T>
{
    final MutableLiveData<Result<T>> mLiveData;
    @GuardedBy("mObservers")
    private final Map<Observer<? super T>, LiveDataObserverAdapter<T>> mObservers;
    
    public LiveDataObservable() {
        this.mLiveData = new MutableLiveData<Result<T>>();
        this.mObservers = new HashMap<Observer<? super T>, LiveDataObserverAdapter<T>>();
    }
    
    @Override
    public void addObserver(@NonNull final Executor executor, @NonNull final Observer<? super T> observer) {
        synchronized (this.mObservers) {
            final LiveDataObserverAdapter liveDataObserverAdapter = this.mObservers.get(observer);
            if (liveDataObserverAdapter != null) {
                liveDataObserverAdapter.disable();
            }
            final LiveDataObserverAdapter liveDataObserverAdapter2 = new LiveDataObserverAdapter(executor, (Observer<? super Object>)observer);
            this.mObservers.put(observer, (LiveDataObserverAdapter<T>)liveDataObserverAdapter2);
            CameraXExecutors.mainThreadExecutor().execute(new O8ooOoo\u3007(this, (LiveDataObserverAdapter)liveDataObserverAdapter, liveDataObserverAdapter2));
        }
    }
    
    @NonNull
    @Override
    public ListenableFuture<T> fetchData() {
        return CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<T>)new o\u3007O8\u3007\u3007o(this));
    }
    
    @NonNull
    public LiveData<Result<T>> getLiveData() {
        return this.mLiveData;
    }
    
    public void postError(@NonNull final Throwable t) {
        this.mLiveData.postValue(Result.fromError(t));
    }
    
    public void postValue(@Nullable final T t) {
        this.mLiveData.postValue(Result.fromValue(t));
    }
    
    @Override
    public void removeObserver(@NonNull final Observer<? super T> observer) {
        synchronized (this.mObservers) {
            final LiveDataObserverAdapter liveDataObserverAdapter = this.mObservers.remove(observer);
            if (liveDataObserverAdapter != null) {
                liveDataObserverAdapter.disable();
                CameraXExecutors.mainThreadExecutor().execute(new O\u30078O8\u3007008(this, (LiveDataObserverAdapter)liveDataObserverAdapter));
            }
        }
    }
    
    private static final class LiveDataObserverAdapter<T> implements Observer<Result<T>>
    {
        final AtomicBoolean mActive;
        final Executor mExecutor;
        final Observable.Observer<? super T> mObserver;
        
        LiveDataObserverAdapter(@NonNull final Executor mExecutor, @NonNull final Observable.Observer<? super T> mObserver) {
            this.mActive = new AtomicBoolean(true);
            this.mExecutor = mExecutor;
            this.mObserver = mObserver;
        }
        
        void disable() {
            this.mActive.set(false);
        }
        
        @Override
        public void onChanged(@NonNull final Result<T> result) {
            this.mExecutor.execute(new \u3007oOO8O8(this, result));
        }
    }
    
    public static final class Result<T>
    {
        @Nullable
        private final Throwable mError;
        @Nullable
        private final T mValue;
        
        private Result(@Nullable final T mValue, @Nullable final Throwable mError) {
            this.mValue = mValue;
            this.mError = mError;
        }
        
        static <T> Result<T> fromError(@NonNull final Throwable t) {
            return new Result<T>(null, Preconditions.checkNotNull(t));
        }
        
        static <T> Result<T> fromValue(@Nullable final T t) {
            return new Result<T>(t, null);
        }
        
        public boolean completedSuccessfully() {
            return this.mError == null;
        }
        
        @Nullable
        public Throwable getError() {
            return this.mError;
        }
        
        @Nullable
        public T getValue() {
            if (this.completedSuccessfully()) {
                return this.mValue;
            }
            throw new IllegalStateException("Result contains an error. Does not contain a value.");
        }
        
        @NonNull
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("[Result: <");
            String str;
            if (this.completedSuccessfully()) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Value: ");
                sb2.append(this.mValue);
                str = sb2.toString();
            }
            else {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Error: ");
                sb3.append(this.mError);
                str = sb3.toString();
            }
            sb.append(str);
            sb.append(">]");
            return sb.toString();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.Nullable;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.camera.core.CameraInfo;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface CameraConfigProvider
{
    public static final CameraConfigProvider EMPTY = new \u3007o\u3007();
    
    @Nullable
    CameraConfig getConfig(@NonNull final CameraInfo p0, @NonNull final Context p1);
}

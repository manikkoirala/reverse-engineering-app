// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface ImageInputConfig extends ReadableConfig
{
    public static final Option<Integer> OPTION_INPUT_FORMAT = Config.Option.create("camerax.core.imageInput.inputFormat", Integer.TYPE);
    
    int getInputFormat();
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.internal.UseCaseEventConfig;
import androidx.camera.core.internal.o\u30070;
import androidx.camera.core.UseCase;
import android.util.Range;
import androidx.camera.core.internal.TargetConfig;
import androidx.camera.core.internal.O8;
import java.util.List;
import java.util.Set;
import androidx.annotation.RestrictTo;
import androidx.annotation.Nullable;
import android.util.Size;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.internal.Oo08;
import java.util.concurrent.Executor;
import androidx.annotation.NonNull;
import androidx.camera.core.ImageReaderProxyProvider;
import androidx.annotation.RequiresApi;
import androidx.camera.core.internal.ThreadConfig;
import androidx.camera.core.ImageAnalysis;

@RequiresApi(21)
public final class ImageAnalysisConfig implements UseCaseConfig<ImageAnalysis>, ImageOutputConfig, ThreadConfig
{
    public static final Option<Integer> OPTION_BACKPRESSURE_STRATEGY;
    public static final Option<Integer> OPTION_IMAGE_QUEUE_DEPTH;
    public static final Option<ImageReaderProxyProvider> OPTION_IMAGE_READER_PROXY_PROVIDER;
    public static final Option<Boolean> OPTION_ONE_PIXEL_SHIFT_ENABLED;
    public static final Option<Integer> OPTION_OUTPUT_IMAGE_FORMAT;
    public static final Option<Boolean> OPTION_OUTPUT_IMAGE_ROTATION_ENABLED;
    private final OptionsBundle mConfig;
    
    static {
        OPTION_BACKPRESSURE_STRATEGY = (Option)Config.Option.create("camerax.core.imageAnalysis.backpressureStrategy", ImageAnalysis.BackpressureStrategy.class);
        OPTION_IMAGE_QUEUE_DEPTH = (Option)Config.Option.create("camerax.core.imageAnalysis.imageQueueDepth", Integer.TYPE);
        OPTION_IMAGE_READER_PROXY_PROVIDER = (Option)Config.Option.create("camerax.core.imageAnalysis.imageReaderProxyProvider", ImageReaderProxyProvider.class);
        OPTION_OUTPUT_IMAGE_FORMAT = (Option)Config.Option.create("camerax.core.imageAnalysis.outputImageFormat", ImageAnalysis.OutputImageFormat.class);
        OPTION_ONE_PIXEL_SHIFT_ENABLED = (Option)Config.Option.create("camerax.core.imageAnalysis.onePixelShiftEnabled", Boolean.class);
        OPTION_OUTPUT_IMAGE_ROTATION_ENABLED = (Option)Config.Option.create("camerax.core.imageAnalysis.outputImageRotationEnabled", Boolean.class);
    }
    
    public ImageAnalysisConfig(@NonNull final OptionsBundle mConfig) {
        this.mConfig = mConfig;
    }
    
    public int getBackpressureStrategy() {
        return (int)this.retrieveOption(ImageAnalysisConfig.OPTION_BACKPRESSURE_STRATEGY);
    }
    
    public int getBackpressureStrategy(final int i) {
        return (int)this.retrieveOption(ImageAnalysisConfig.OPTION_BACKPRESSURE_STRATEGY, i);
    }
    
    @NonNull
    @Override
    public Config getConfig() {
        return this.mConfig;
    }
    
    public int getImageQueueDepth() {
        return (int)this.retrieveOption(ImageAnalysisConfig.OPTION_IMAGE_QUEUE_DEPTH);
    }
    
    public int getImageQueueDepth(final int i) {
        return (int)this.retrieveOption(ImageAnalysisConfig.OPTION_IMAGE_QUEUE_DEPTH, i);
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public ImageReaderProxyProvider getImageReaderProxyProvider() {
        return (ImageReaderProxyProvider)this.retrieveOption(ImageAnalysisConfig.OPTION_IMAGE_READER_PROXY_PROVIDER, null);
    }
    
    @Override
    public int getInputFormat() {
        return 35;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public Boolean getOnePixelShiftEnabled(@Nullable final Boolean b) {
        return (Boolean)this.retrieveOption(ImageAnalysisConfig.OPTION_ONE_PIXEL_SHIFT_ENABLED, b);
    }
    
    public int getOutputImageFormat(final int i) {
        return (int)this.retrieveOption(ImageAnalysisConfig.OPTION_OUTPUT_IMAGE_FORMAT, i);
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public Boolean isOutputImageRotationEnabled(@Nullable final Boolean b) {
        return (Boolean)this.retrieveOption(ImageAnalysisConfig.OPTION_OUTPUT_IMAGE_ROTATION_ENABLED, b);
    }
    
    @Override
    public /* synthetic */ Object retrieveOption(final Option option) {
        return o\u3007\u30070\u3007.o\u30070(this, option);
    }
    
    @Override
    public /* synthetic */ Object retrieveOption(final Option option, final Object o) {
        return o\u3007\u30070\u3007.\u3007\u3007888(this, option, o);
    }
}

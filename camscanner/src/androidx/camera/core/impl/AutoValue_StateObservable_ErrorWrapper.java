// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.NonNull;

final class AutoValue_StateObservable_ErrorWrapper extends ErrorWrapper
{
    private final Throwable error;
    
    AutoValue_StateObservable_ErrorWrapper(final Throwable error) {
        if (error != null) {
            this.error = error;
            return;
        }
        throw new NullPointerException("Null error");
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof ErrorWrapper && this.error.equals(((ErrorWrapper)o).getError()));
    }
    
    @NonNull
    @Override
    public Throwable getError() {
        return this.error;
    }
    
    @Override
    public int hashCode() {
        return this.error.hashCode() ^ 0xF4243;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ErrorWrapper{error=");
        sb.append(this.error);
        sb.append("}");
        return sb.toString();
    }
}

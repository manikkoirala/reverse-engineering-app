// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class CameraCaptureMetaData
{
    private CameraCaptureMetaData() {
    }
    
    public enum AeState
    {
        private static final AeState[] $VALUES;
        
        CONVERGED, 
        FLASH_REQUIRED, 
        INACTIVE, 
        LOCKED, 
        SEARCHING, 
        UNKNOWN;
    }
    
    public enum AfMode
    {
        private static final AfMode[] $VALUES;
        
        OFF, 
        ON_CONTINUOUS_AUTO, 
        ON_MANUAL_AUTO, 
        UNKNOWN;
    }
    
    public enum AfState
    {
        private static final AfState[] $VALUES;
        
        INACTIVE, 
        LOCKED_FOCUSED, 
        LOCKED_NOT_FOCUSED, 
        PASSIVE_FOCUSED, 
        PASSIVE_NOT_FOCUSED, 
        SCANNING, 
        UNKNOWN;
    }
    
    public enum AwbState
    {
        private static final AwbState[] $VALUES;
        
        CONVERGED, 
        INACTIVE, 
        LOCKED, 
        METERING, 
        UNKNOWN;
    }
    
    public enum FlashState
    {
        private static final FlashState[] $VALUES;
        
        FIRED, 
        NONE, 
        READY, 
        UNKNOWN;
    }
}

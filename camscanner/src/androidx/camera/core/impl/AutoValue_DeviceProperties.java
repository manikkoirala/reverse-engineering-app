// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.NonNull;

final class AutoValue_DeviceProperties extends DeviceProperties
{
    private final String manufacturer;
    private final String model;
    private final int sdkVersion;
    
    AutoValue_DeviceProperties(final String manufacturer, final String model, final int sdkVersion) {
        if (manufacturer == null) {
            throw new NullPointerException("Null manufacturer");
        }
        this.manufacturer = manufacturer;
        if (model != null) {
            this.model = model;
            this.sdkVersion = sdkVersion;
            return;
        }
        throw new NullPointerException("Null model");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof DeviceProperties) {
            final DeviceProperties deviceProperties = (DeviceProperties)o;
            if (!this.manufacturer.equals(deviceProperties.manufacturer()) || !this.model.equals(deviceProperties.model()) || this.sdkVersion != deviceProperties.sdkVersion()) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return ((this.manufacturer.hashCode() ^ 0xF4243) * 1000003 ^ this.model.hashCode()) * 1000003 ^ this.sdkVersion;
    }
    
    @NonNull
    @Override
    public String manufacturer() {
        return this.manufacturer;
    }
    
    @NonNull
    @Override
    public String model() {
        return this.model;
    }
    
    @Override
    public int sdkVersion() {
        return this.sdkVersion;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DeviceProperties{manufacturer=");
        sb.append(this.manufacturer);
        sb.append(", model=");
        sb.append(this.model);
        sb.append(", sdkVersion=");
        sb.append(this.sdkVersion);
        sb.append("}");
        return sb.toString();
    }
}

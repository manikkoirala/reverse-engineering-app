// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.Nullable;
import android.os.IBinder;
import androidx.annotation.NonNull;
import android.content.Intent;
import androidx.annotation.RequiresApi;
import android.app.Service;

@RequiresApi(21)
public class MetadataHolderService extends Service
{
    private MetadataHolderService() {
    }
    
    @Nullable
    public IBinder onBind(@NonNull final Intent intent) {
        throw new UnsupportedOperationException();
    }
}

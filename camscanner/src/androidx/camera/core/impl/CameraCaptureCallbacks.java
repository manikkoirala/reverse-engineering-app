// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.Arrays;
import androidx.annotation.NonNull;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class CameraCaptureCallbacks
{
    private CameraCaptureCallbacks() {
    }
    
    @NonNull
    static CameraCaptureCallback createComboCallback(@NonNull final List<CameraCaptureCallback> list) {
        if (list.isEmpty()) {
            return createNoOpCallback();
        }
        if (list.size() == 1) {
            return list.get(0);
        }
        return new ComboCameraCaptureCallback(list);
    }
    
    @NonNull
    public static CameraCaptureCallback createComboCallback(@NonNull final CameraCaptureCallback... a) {
        return createComboCallback(Arrays.asList(a));
    }
    
    @NonNull
    public static CameraCaptureCallback createNoOpCallback() {
        return new NoOpCameraCaptureCallback();
    }
    
    public static final class ComboCameraCaptureCallback extends CameraCaptureCallback
    {
        private final List<CameraCaptureCallback> mCallbacks;
        
        ComboCameraCaptureCallback(@NonNull final List<CameraCaptureCallback> list) {
            this.mCallbacks = new ArrayList<CameraCaptureCallback>();
            for (final CameraCaptureCallback cameraCaptureCallback : list) {
                if (!(cameraCaptureCallback instanceof NoOpCameraCaptureCallback)) {
                    this.mCallbacks.add(cameraCaptureCallback);
                }
            }
        }
        
        @NonNull
        public List<CameraCaptureCallback> getCallbacks() {
            return this.mCallbacks;
        }
        
        @Override
        public void onCaptureCancelled() {
            final Iterator<CameraCaptureCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onCaptureCancelled();
            }
        }
        
        @Override
        public void onCaptureCompleted(@NonNull final CameraCaptureResult cameraCaptureResult) {
            final Iterator<CameraCaptureCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onCaptureCompleted(cameraCaptureResult);
            }
        }
        
        @Override
        public void onCaptureFailed(@NonNull final CameraCaptureFailure cameraCaptureFailure) {
            final Iterator<CameraCaptureCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onCaptureFailed(cameraCaptureFailure);
            }
        }
    }
    
    static final class NoOpCameraCaptureCallback extends CameraCaptureCallback
    {
        @Override
        public void onCaptureCompleted(@NonNull final CameraCaptureResult cameraCaptureResult) {
        }
        
        @Override
        public void onCaptureFailed(@NonNull final CameraCaptureFailure cameraCaptureFailure) {
        }
    }
}

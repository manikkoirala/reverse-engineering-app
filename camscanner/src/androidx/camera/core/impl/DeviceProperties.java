// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.NonNull;
import android.os.Build$VERSION;
import android.os.Build;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public abstract class DeviceProperties
{
    @NonNull
    public static DeviceProperties create() {
        return create(Build.MANUFACTURER, Build.MODEL, Build$VERSION.SDK_INT);
    }
    
    @NonNull
    public static DeviceProperties create(@NonNull final String s, @NonNull final String s2, final int n) {
        return new AutoValue_DeviceProperties(s, s2, n);
    }
    
    @NonNull
    public abstract String manufacturer();
    
    @NonNull
    public abstract String model();
    
    public abstract int sdkVersion();
}

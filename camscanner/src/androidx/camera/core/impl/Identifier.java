// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public abstract class Identifier
{
    @NonNull
    public static Identifier create(@NonNull final Object o) {
        return new AutoValue_Identifier(o);
    }
    
    @NonNull
    public abstract Object getValue();
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.Nullable;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class Quirks
{
    @NonNull
    private final List<Quirk> mQuirks;
    
    public Quirks(@NonNull final List<Quirk> c) {
        this.mQuirks = new ArrayList<Quirk>(c);
    }
    
    public boolean contains(@NonNull final Class<? extends Quirk> clazz) {
        final Iterator<Quirk> iterator = this.mQuirks.iterator();
        while (iterator.hasNext()) {
            if (clazz.isAssignableFrom(iterator.next().getClass())) {
                return true;
            }
        }
        return false;
    }
    
    @Nullable
    public <T extends Quirk> T get(@NonNull final Class<T> clazz) {
        for (final Quirk quirk : this.mQuirks) {
            if (quirk.getClass() == clazz) {
                return (T)quirk;
            }
        }
        return null;
    }
}

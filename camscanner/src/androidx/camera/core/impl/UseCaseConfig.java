// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.ExtendableBuilder;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.util.Range;
import androidx.camera.core.CameraSelector;
import androidx.annotation.RequiresApi;
import androidx.camera.core.internal.UseCaseEventConfig;
import androidx.camera.core.internal.TargetConfig;
import androidx.camera.core.UseCase;

@RequiresApi(21)
public interface UseCaseConfig<T extends UseCase> extends TargetConfig<T>, UseCaseEventConfig, ImageInputConfig
{
    public static final Option<CameraSelector> OPTION_CAMERA_SELECTOR = Config.Option.create("camerax.core.useCase.cameraSelector", CameraSelector.class);
    public static final Option<CaptureConfig.OptionUnpacker> OPTION_CAPTURE_CONFIG_UNPACKER = Config.Option.create("camerax.core.useCase.captureConfigUnpacker", CaptureConfig.OptionUnpacker.class);
    public static final Option<CaptureConfig> OPTION_DEFAULT_CAPTURE_CONFIG = Config.Option.create("camerax.core.useCase.defaultCaptureConfig", CaptureConfig.class);
    public static final Option<SessionConfig> OPTION_DEFAULT_SESSION_CONFIG = Config.Option.create("camerax.core.useCase.defaultSessionConfig", SessionConfig.class);
    public static final Option<SessionConfig.OptionUnpacker> OPTION_SESSION_CONFIG_UNPACKER = Config.Option.create("camerax.core.useCase.sessionConfigUnpacker", SessionConfig.OptionUnpacker.class);
    public static final Option<Integer> OPTION_SURFACE_OCCUPANCY_PRIORITY = Config.Option.create("camerax.core.useCase.surfaceOccupancyPriority", Integer.TYPE);
    public static final Option<Range<Integer>> OPTION_TARGET_FRAME_RATE = Config.Option.create("camerax.core.useCase.targetFrameRate", CameraSelector.class);
    public static final Option<Boolean> OPTION_ZSL_DISABLED = Config.Option.create("camerax.core.useCase.zslDisabled", Boolean.TYPE);
    
    @NonNull
    CameraSelector getCameraSelector();
    
    @Nullable
    CameraSelector getCameraSelector(@Nullable final CameraSelector p0);
    
    @NonNull
    CaptureConfig.OptionUnpacker getCaptureOptionUnpacker();
    
    @Nullable
    CaptureConfig.OptionUnpacker getCaptureOptionUnpacker(@Nullable final CaptureConfig.OptionUnpacker p0);
    
    @NonNull
    CaptureConfig getDefaultCaptureConfig();
    
    @Nullable
    CaptureConfig getDefaultCaptureConfig(@Nullable final CaptureConfig p0);
    
    @NonNull
    SessionConfig getDefaultSessionConfig();
    
    @Nullable
    SessionConfig getDefaultSessionConfig(@Nullable final SessionConfig p0);
    
    @NonNull
    SessionConfig.OptionUnpacker getSessionOptionUnpacker();
    
    @Nullable
    SessionConfig.OptionUnpacker getSessionOptionUnpacker(@Nullable final SessionConfig.OptionUnpacker p0);
    
    int getSurfaceOccupancyPriority();
    
    int getSurfaceOccupancyPriority(final int p0);
    
    @NonNull
    Range<Integer> getTargetFramerate();
    
    @Nullable
    Range<Integer> getTargetFramerate(@Nullable final Range<Integer> p0);
    
    boolean isZslDisabled(final boolean p0);
    
    public interface Builder<T extends UseCase, C extends UseCaseConfig<T>, B> extends TargetConfig.Builder<T, B>, ExtendableBuilder<T>, UseCaseEventConfig.Builder<B>
    {
        @NonNull
        C getUseCaseConfig();
        
        @NonNull
        B setCameraSelector(@NonNull final CameraSelector p0);
        
        @NonNull
        B setCaptureOptionUnpacker(@NonNull final CaptureConfig.OptionUnpacker p0);
        
        @NonNull
        B setDefaultCaptureConfig(@NonNull final CaptureConfig p0);
        
        @NonNull
        B setDefaultSessionConfig(@NonNull final SessionConfig p0);
        
        @NonNull
        B setSessionOptionUnpacker(@NonNull final SessionConfig.OptionUnpacker p0);
        
        @NonNull
        B setSurfaceOccupancyPriority(final int p0);
        
        @NonNull
        B setZslDisabled(final boolean p0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.Oo08;
import java.util.Iterator;
import androidx.core.util.Preconditions;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import androidx.camera.core.CameraInfo;
import java.util.List;
import androidx.annotation.RequiresApi;
import androidx.camera.core.CameraFilter;

@RequiresApi(21)
public class LensFacingCameraFilter implements CameraFilter
{
    private int mLensFacing;
    
    public LensFacingCameraFilter(final int mLensFacing) {
        this.mLensFacing = mLensFacing;
    }
    
    @NonNull
    @Override
    public List<CameraInfo> filter(@NonNull final List<CameraInfo> list) {
        final ArrayList list2 = new ArrayList();
        for (final CameraInfo cameraInfo : list) {
            Preconditions.checkArgument(cameraInfo instanceof CameraInfoInternal, (Object)"The camera info doesn't contain internal implementation.");
            final Integer lensFacing = ((CameraInfoInternal)cameraInfo).getLensFacing();
            if (lensFacing != null && lensFacing == this.mLensFacing) {
                list2.add(cameraInfo);
            }
        }
        return list2;
    }
    
    public int getLensFacing() {
        return this.mLensFacing;
    }
}

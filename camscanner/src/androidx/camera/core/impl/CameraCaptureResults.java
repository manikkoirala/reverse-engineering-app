// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.Nullable;
import androidx.camera.core.internal.CameraCaptureResultImageInfo;
import androidx.annotation.NonNull;
import androidx.camera.core.ImageInfo;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class CameraCaptureResults
{
    private CameraCaptureResults() {
    }
    
    @Nullable
    public static CameraCaptureResult retrieveCameraCaptureResult(@NonNull final ImageInfo imageInfo) {
        if (imageInfo instanceof CameraCaptureResultImageInfo) {
            return ((CameraCaptureResultImageInfo)imageInfo).getCameraCaptureResult();
        }
        return null;
    }
}

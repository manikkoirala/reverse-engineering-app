// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

final class AutoValue_Config_Option<T> extends Option<T>
{
    private final String id;
    private final Object token;
    private final Class<T> valueClass;
    
    AutoValue_Config_Option(final String id, final Class<T> valueClass, @Nullable final Object token) {
        if (id == null) {
            throw new NullPointerException("Null id");
        }
        this.id = id;
        if (valueClass != null) {
            this.valueClass = valueClass;
            this.token = token;
            return;
        }
        throw new NullPointerException("Null valueClass");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof Option) {
            final Option option = (Option)o;
            if (this.id.equals(option.getId()) && this.valueClass.equals(option.getValueClass())) {
                final Object token = this.token;
                if (token == null) {
                    if (option.getToken() == null) {
                        return b;
                    }
                }
                else if (token.equals(option.getToken())) {
                    return b;
                }
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @NonNull
    @Override
    public String getId() {
        return this.id;
    }
    
    @Nullable
    @Override
    public Object getToken() {
        return this.token;
    }
    
    @NonNull
    @Override
    public Class<T> getValueClass() {
        return this.valueClass;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.id.hashCode();
        final int hashCode2 = this.valueClass.hashCode();
        final Object token = this.token;
        int hashCode3;
        if (token == null) {
            hashCode3 = 0;
        }
        else {
            hashCode3 = token.hashCode();
        }
        return ((hashCode ^ 0xF4243) * 1000003 ^ hashCode2) * 1000003 ^ hashCode3;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Option{id=");
        sb.append(this.id);
        sb.append(", valueClass=");
        sb.append(this.valueClass);
        sb.append(", token=");
        sb.append(this.token);
        sb.append("}");
        return sb.toString();
    }
}

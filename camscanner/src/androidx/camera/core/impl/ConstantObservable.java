// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.concurrent.Future;
import java.util.concurrent.Executor;
import androidx.annotation.NonNull;
import java.util.concurrent.ExecutionException;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.annotation.Nullable;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class ConstantObservable<T> implements Observable<T>
{
    private static final ConstantObservable<Object> NULL_OBSERVABLE;
    private static final String TAG = "ConstantObservable";
    private final ListenableFuture<T> mValueFuture;
    
    static {
        NULL_OBSERVABLE = new ConstantObservable<Object>(null);
    }
    
    private ConstantObservable(@Nullable final T t) {
        this.mValueFuture = Futures.immediateFuture(t);
    }
    
    @NonNull
    public static <U> Observable<U> withValue(@Nullable final U u) {
        if (u == null) {
            return (Observable<U>)ConstantObservable.NULL_OBSERVABLE;
        }
        return new ConstantObservable<U>(u);
    }
    
    @Override
    public void addObserver(@NonNull final Executor executor, @NonNull final Observer<? super T> observer) {
        this.mValueFuture.addListener((Runnable)new \u3007\u3007808\u3007(this, observer), executor);
    }
    
    @NonNull
    @Override
    public ListenableFuture<T> fetchData() {
        return this.mValueFuture;
    }
    
    @Override
    public void removeObserver(@NonNull final Observer<? super T> observer) {
    }
}

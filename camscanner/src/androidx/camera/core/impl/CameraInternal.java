// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.Nullable;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.LinkedHashSet;
import androidx.camera.core.CameraInfo;
import androidx.camera.core.CameraControl;
import androidx.annotation.NonNull;
import java.util.Collection;
import androidx.annotation.RequiresApi;
import androidx.camera.core.UseCase;
import androidx.camera.core.Camera;

@RequiresApi(21)
public interface CameraInternal extends Camera, StateChangeCallback
{
    void attachUseCases(@NonNull final Collection<UseCase> p0);
    
    void close();
    
    void detachUseCases(@NonNull final Collection<UseCase> p0);
    
    @NonNull
    CameraControl getCameraControl();
    
    @NonNull
    CameraControlInternal getCameraControlInternal();
    
    @NonNull
    CameraInfo getCameraInfo();
    
    @NonNull
    CameraInfoInternal getCameraInfoInternal();
    
    @NonNull
    LinkedHashSet<CameraInternal> getCameraInternals();
    
    @NonNull
    Observable<State> getCameraState();
    
    @NonNull
    CameraConfig getExtendedConfig();
    
    void open();
    
    @NonNull
    ListenableFuture<Void> release();
    
    void setActiveResumingMode(final boolean p0);
    
    void setExtendedConfig(@Nullable final CameraConfig p0);
    
    public enum State
    {
        private static final State[] $VALUES;
        
        CLOSED(false), 
        CLOSING(true), 
        OPEN(true), 
        OPENING(true), 
        PENDING_OPEN(false), 
        RELEASED(false), 
        RELEASING(true);
        
        private final boolean mHoldsCameraSlot;
        
        private State(final boolean mHoldsCameraSlot) {
            this.mHoldsCameraSlot = mHoldsCameraSlot;
        }
        
        boolean holdsCameraSlot() {
            return this.mHoldsCameraSlot;
        }
    }
}

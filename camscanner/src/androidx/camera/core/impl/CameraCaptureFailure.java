// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class CameraCaptureFailure
{
    private final Reason mReason;
    
    public CameraCaptureFailure(@NonNull final Reason mReason) {
        this.mReason = mReason;
    }
    
    @NonNull
    public Reason getReason() {
        return this.mReason;
    }
    
    public enum Reason
    {
        private static final Reason[] $VALUES;
        
        ERROR;
    }
}

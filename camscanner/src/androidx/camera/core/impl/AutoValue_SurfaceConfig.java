// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.NonNull;

final class AutoValue_SurfaceConfig extends SurfaceConfig
{
    private final ConfigSize configSize;
    private final ConfigType configType;
    
    AutoValue_SurfaceConfig(final ConfigType configType, final ConfigSize configSize) {
        if (configType == null) {
            throw new NullPointerException("Null configType");
        }
        this.configType = configType;
        if (configSize != null) {
            this.configSize = configSize;
            return;
        }
        throw new NullPointerException("Null configSize");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof SurfaceConfig) {
            final SurfaceConfig surfaceConfig = (SurfaceConfig)o;
            if (!this.configType.equals(surfaceConfig.getConfigType()) || !this.configSize.equals(surfaceConfig.getConfigSize())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @NonNull
    @Override
    public ConfigSize getConfigSize() {
        return this.configSize;
    }
    
    @NonNull
    @Override
    public ConfigType getConfigType() {
        return this.configType;
    }
    
    @Override
    public int hashCode() {
        return (this.configType.hashCode() ^ 0xF4243) * 1000003 ^ this.configSize.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SurfaceConfig{configType=");
        sb.append(this.configType);
        sb.append(", configSize=");
        sb.append(this.configSize);
        sb.append("}");
        return sb.toString();
    }
}

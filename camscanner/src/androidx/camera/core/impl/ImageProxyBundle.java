// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.ImageProxy;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.annotation.NonNull;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface ImageProxyBundle
{
    @NonNull
    List<Integer> getCaptureIds();
    
    @NonNull
    ListenableFuture<ImageProxy> getImageProxy(final int p0);
}

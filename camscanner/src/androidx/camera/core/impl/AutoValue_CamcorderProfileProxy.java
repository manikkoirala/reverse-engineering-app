// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

final class AutoValue_CamcorderProfileProxy extends CamcorderProfileProxy
{
    private final int audioBitRate;
    private final int audioChannels;
    private final int audioCodec;
    private final int audioSampleRate;
    private final int duration;
    private final int fileFormat;
    private final int quality;
    private final int videoBitRate;
    private final int videoCodec;
    private final int videoFrameHeight;
    private final int videoFrameRate;
    private final int videoFrameWidth;
    
    AutoValue_CamcorderProfileProxy(final int duration, final int quality, final int fileFormat, final int videoCodec, final int videoBitRate, final int videoFrameRate, final int videoFrameWidth, final int videoFrameHeight, final int audioCodec, final int audioBitRate, final int audioSampleRate, final int audioChannels) {
        this.duration = duration;
        this.quality = quality;
        this.fileFormat = fileFormat;
        this.videoCodec = videoCodec;
        this.videoBitRate = videoBitRate;
        this.videoFrameRate = videoFrameRate;
        this.videoFrameWidth = videoFrameWidth;
        this.videoFrameHeight = videoFrameHeight;
        this.audioCodec = audioCodec;
        this.audioBitRate = audioBitRate;
        this.audioSampleRate = audioSampleRate;
        this.audioChannels = audioChannels;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof CamcorderProfileProxy) {
            final CamcorderProfileProxy camcorderProfileProxy = (CamcorderProfileProxy)o;
            if (this.duration != camcorderProfileProxy.getDuration() || this.quality != camcorderProfileProxy.getQuality() || this.fileFormat != camcorderProfileProxy.getFileFormat() || this.videoCodec != camcorderProfileProxy.getVideoCodec() || this.videoBitRate != camcorderProfileProxy.getVideoBitRate() || this.videoFrameRate != camcorderProfileProxy.getVideoFrameRate() || this.videoFrameWidth != camcorderProfileProxy.getVideoFrameWidth() || this.videoFrameHeight != camcorderProfileProxy.getVideoFrameHeight() || this.audioCodec != camcorderProfileProxy.getAudioCodec() || this.audioBitRate != camcorderProfileProxy.getAudioBitRate() || this.audioSampleRate != camcorderProfileProxy.getAudioSampleRate() || this.audioChannels != camcorderProfileProxy.getAudioChannels()) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int getAudioBitRate() {
        return this.audioBitRate;
    }
    
    @Override
    public int getAudioChannels() {
        return this.audioChannels;
    }
    
    @Override
    public int getAudioCodec() {
        return this.audioCodec;
    }
    
    @Override
    public int getAudioSampleRate() {
        return this.audioSampleRate;
    }
    
    @Override
    public int getDuration() {
        return this.duration;
    }
    
    @Override
    public int getFileFormat() {
        return this.fileFormat;
    }
    
    @Override
    public int getQuality() {
        return this.quality;
    }
    
    @Override
    public int getVideoBitRate() {
        return this.videoBitRate;
    }
    
    @Override
    public int getVideoCodec() {
        return this.videoCodec;
    }
    
    @Override
    public int getVideoFrameHeight() {
        return this.videoFrameHeight;
    }
    
    @Override
    public int getVideoFrameRate() {
        return this.videoFrameRate;
    }
    
    @Override
    public int getVideoFrameWidth() {
        return this.videoFrameWidth;
    }
    
    @Override
    public int hashCode() {
        return (((((((((((this.duration ^ 0xF4243) * 1000003 ^ this.quality) * 1000003 ^ this.fileFormat) * 1000003 ^ this.videoCodec) * 1000003 ^ this.videoBitRate) * 1000003 ^ this.videoFrameRate) * 1000003 ^ this.videoFrameWidth) * 1000003 ^ this.videoFrameHeight) * 1000003 ^ this.audioCodec) * 1000003 ^ this.audioBitRate) * 1000003 ^ this.audioSampleRate) * 1000003 ^ this.audioChannels;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("CamcorderProfileProxy{duration=");
        sb.append(this.duration);
        sb.append(", quality=");
        sb.append(this.quality);
        sb.append(", fileFormat=");
        sb.append(this.fileFormat);
        sb.append(", videoCodec=");
        sb.append(this.videoCodec);
        sb.append(", videoBitRate=");
        sb.append(this.videoBitRate);
        sb.append(", videoFrameRate=");
        sb.append(this.videoFrameRate);
        sb.append(", videoFrameWidth=");
        sb.append(this.videoFrameWidth);
        sb.append(", videoFrameHeight=");
        sb.append(this.videoFrameHeight);
        sb.append(", audioCodec=");
        sb.append(this.audioCodec);
        sb.append(", audioBitRate=");
        sb.append(this.audioBitRate);
        sb.append(", audioSampleRate=");
        sb.append(this.audioSampleRate);
        sb.append(", audioChannels=");
        sb.append(this.audioChannels);
        sb.append("}");
        return sb.toString();
    }
}

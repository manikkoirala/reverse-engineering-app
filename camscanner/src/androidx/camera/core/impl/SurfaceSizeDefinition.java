// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.NonNull;
import android.util.Size;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public abstract class SurfaceSizeDefinition
{
    SurfaceSizeDefinition() {
    }
    
    @NonNull
    public static SurfaceSizeDefinition create(@NonNull final Size size, @NonNull final Size size2, @NonNull final Size size3) {
        return new AutoValue_SurfaceSizeDefinition(size, size2, size3);
    }
    
    @NonNull
    public abstract Size getAnalysisSize();
    
    @NonNull
    public abstract Size getPreviewSize();
    
    @NonNull
    public abstract Size getRecordSize();
}

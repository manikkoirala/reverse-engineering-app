// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.impl.utils.futures.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import android.util.Size;
import androidx.annotation.NonNull;
import android.view.Surface;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class ImmediateSurface extends DeferrableSurface
{
    private final Surface mSurface;
    
    public ImmediateSurface(@NonNull final Surface mSurface) {
        this.mSurface = mSurface;
    }
    
    public ImmediateSurface(@NonNull final Surface mSurface, @NonNull final Size size, final int n) {
        super(size, n);
        this.mSurface = mSurface;
    }
    
    @NonNull
    public ListenableFuture<Surface> provideSurface() {
        return Futures.immediateFuture(this.mSurface);
    }
}

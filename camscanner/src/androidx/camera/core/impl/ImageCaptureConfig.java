// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.internal.UseCaseEventConfig;
import androidx.camera.core.internal.o\u30070;
import androidx.camera.core.UseCase;
import android.util.Range;
import androidx.camera.core.internal.TargetConfig;
import androidx.camera.core.internal.O8;
import java.util.List;
import java.util.Set;
import androidx.annotation.IntRange;
import java.util.concurrent.Executor;
import androidx.annotation.RestrictTo;
import android.util.Size;
import androidx.camera.core.CameraSelector;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.camera.core.ImageReaderProxyProvider;
import androidx.annotation.RequiresApi;
import androidx.camera.core.internal.IoConfig;
import androidx.camera.core.ImageCapture;

@RequiresApi(21)
public final class ImageCaptureConfig implements UseCaseConfig<ImageCapture>, ImageOutputConfig, IoConfig
{
    public static final Option<Integer> OPTION_BUFFER_FORMAT;
    public static final Option<CaptureBundle> OPTION_CAPTURE_BUNDLE;
    public static final Option<CaptureProcessor> OPTION_CAPTURE_PROCESSOR;
    public static final Option<Integer> OPTION_FLASH_MODE;
    public static final Option<Integer> OPTION_FLASH_TYPE;
    public static final Option<Integer> OPTION_IMAGE_CAPTURE_MODE;
    public static final Option<ImageReaderProxyProvider> OPTION_IMAGE_READER_PROXY_PROVIDER;
    public static final Option<Integer> OPTION_JPEG_COMPRESSION_QUALITY;
    public static final Option<Integer> OPTION_MAX_CAPTURE_STAGES;
    public static final Option<Boolean> OPTION_USE_SOFTWARE_JPEG_ENCODER;
    private final OptionsBundle mConfig;
    
    static {
        final Class<Integer> type = Integer.TYPE;
        OPTION_IMAGE_CAPTURE_MODE = (Option)Config.Option.create("camerax.core.imageCapture.captureMode", type);
        OPTION_FLASH_MODE = (Option)Config.Option.create("camerax.core.imageCapture.flashMode", type);
        OPTION_CAPTURE_BUNDLE = (Option)Config.Option.create("camerax.core.imageCapture.captureBundle", CaptureBundle.class);
        OPTION_CAPTURE_PROCESSOR = (Option)Config.Option.create("camerax.core.imageCapture.captureProcessor", CaptureProcessor.class);
        OPTION_BUFFER_FORMAT = (Option)Config.Option.create("camerax.core.imageCapture.bufferFormat", Integer.class);
        OPTION_MAX_CAPTURE_STAGES = (Option)Config.Option.create("camerax.core.imageCapture.maxCaptureStages", Integer.class);
        OPTION_IMAGE_READER_PROXY_PROVIDER = (Option)Config.Option.create("camerax.core.imageCapture.imageReaderProxyProvider", ImageReaderProxyProvider.class);
        OPTION_USE_SOFTWARE_JPEG_ENCODER = (Option)Config.Option.create("camerax.core.imageCapture.useSoftwareJpegEncoder", Boolean.TYPE);
        OPTION_FLASH_TYPE = (Option)Config.Option.create("camerax.core.imageCapture.flashType", type);
        OPTION_JPEG_COMPRESSION_QUALITY = (Option)Config.Option.create("camerax.core.imageCapture.jpegCompressionQuality", type);
    }
    
    public ImageCaptureConfig(@NonNull final OptionsBundle mConfig) {
        this.mConfig = mConfig;
    }
    
    @Override
    public /* synthetic */ boolean containsOption(final Option option) {
        return o\u3007\u30070\u3007.\u3007080(this, option);
    }
    
    @NonNull
    public Integer getBufferFormat() {
        return (Integer)this.retrieveOption(ImageCaptureConfig.OPTION_BUFFER_FORMAT);
    }
    
    @Nullable
    public Integer getBufferFormat(@Nullable final Integer n) {
        return (Integer)this.retrieveOption(ImageCaptureConfig.OPTION_BUFFER_FORMAT, n);
    }
    
    @NonNull
    public CaptureBundle getCaptureBundle() {
        return (CaptureBundle)this.retrieveOption(ImageCaptureConfig.OPTION_CAPTURE_BUNDLE);
    }
    
    @Nullable
    public CaptureBundle getCaptureBundle(@Nullable final CaptureBundle captureBundle) {
        return (CaptureBundle)this.retrieveOption(ImageCaptureConfig.OPTION_CAPTURE_BUNDLE, captureBundle);
    }
    
    public int getCaptureMode() {
        return (int)this.retrieveOption(ImageCaptureConfig.OPTION_IMAGE_CAPTURE_MODE);
    }
    
    @NonNull
    public CaptureProcessor getCaptureProcessor() {
        return (CaptureProcessor)this.retrieveOption(ImageCaptureConfig.OPTION_CAPTURE_PROCESSOR);
    }
    
    @Nullable
    public CaptureProcessor getCaptureProcessor(@Nullable final CaptureProcessor captureProcessor) {
        return (CaptureProcessor)this.retrieveOption(ImageCaptureConfig.OPTION_CAPTURE_PROCESSOR, captureProcessor);
    }
    
    @NonNull
    @Override
    public Config getConfig() {
        return this.mConfig;
    }
    
    public int getFlashMode() {
        return (int)this.retrieveOption(ImageCaptureConfig.OPTION_FLASH_MODE);
    }
    
    public int getFlashMode(final int i) {
        return (int)this.retrieveOption(ImageCaptureConfig.OPTION_FLASH_MODE, i);
    }
    
    public int getFlashType() {
        return (int)this.retrieveOption(ImageCaptureConfig.OPTION_FLASH_TYPE);
    }
    
    public int getFlashType(final int i) {
        return (int)this.retrieveOption(ImageCaptureConfig.OPTION_FLASH_TYPE, i);
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public ImageReaderProxyProvider getImageReaderProxyProvider() {
        return (ImageReaderProxyProvider)this.retrieveOption(ImageCaptureConfig.OPTION_IMAGE_READER_PROXY_PROVIDER, null);
    }
    
    @Override
    public int getInputFormat() {
        return (int)this.retrieveOption(ImageInputConfig.OPTION_INPUT_FORMAT);
    }
    
    @NonNull
    @Override
    public Executor getIoExecutor() {
        return (Executor)this.retrieveOption(IoConfig.OPTION_IO_EXECUTOR);
    }
    
    @Nullable
    @Override
    public Executor getIoExecutor(@Nullable final Executor executor) {
        return (Executor)this.retrieveOption(IoConfig.OPTION_IO_EXECUTOR, executor);
    }
    
    @IntRange(from = 1L, to = 100L)
    public int getJpegQuality() {
        return (int)this.retrieveOption(ImageCaptureConfig.OPTION_JPEG_COMPRESSION_QUALITY);
    }
    
    @IntRange(from = 1L, to = 100L)
    public int getJpegQuality(@IntRange(from = 1L, to = 100L) final int i) {
        return (int)this.retrieveOption(ImageCaptureConfig.OPTION_JPEG_COMPRESSION_QUALITY, i);
    }
    
    public int getMaxCaptureStages() {
        return (int)this.retrieveOption(ImageCaptureConfig.OPTION_MAX_CAPTURE_STAGES);
    }
    
    public int getMaxCaptureStages(final int i) {
        return (int)this.retrieveOption(ImageCaptureConfig.OPTION_MAX_CAPTURE_STAGES, i);
    }
    
    public boolean hasCaptureMode() {
        return this.containsOption(ImageCaptureConfig.OPTION_IMAGE_CAPTURE_MODE);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public boolean isSoftwareJpegEncoderRequested() {
        return (boolean)this.retrieveOption(ImageCaptureConfig.OPTION_USE_SOFTWARE_JPEG_ENCODER, Boolean.FALSE);
    }
    
    @Override
    public /* synthetic */ Object retrieveOption(final Option option) {
        return o\u3007\u30070\u3007.o\u30070(this, option);
    }
    
    @Override
    public /* synthetic */ Object retrieveOption(final Option option, final Object o) {
        return o\u3007\u30070\u3007.\u3007\u3007888(this, option, o);
    }
}

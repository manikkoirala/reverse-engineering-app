// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.Collections;
import java.util.ArrayList;
import java.util.Collection;
import androidx.annotation.NonNull;
import java.util.List;
import java.util.HashSet;
import java.util.Set;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public abstract class MultiValueSet<C>
{
    private Set<C> mSet;
    
    public MultiValueSet() {
        this.mSet = new HashSet<C>();
    }
    
    public void addAll(@NonNull final List<C> list) {
        this.mSet.addAll((Collection<? extends C>)list);
    }
    
    @NonNull
    public abstract MultiValueSet<C> clone();
    
    @NonNull
    public List<C> getAllItems() {
        return Collections.unmodifiableList((List<? extends C>)new ArrayList<C>((Collection<? extends C>)this.mSet));
    }
}

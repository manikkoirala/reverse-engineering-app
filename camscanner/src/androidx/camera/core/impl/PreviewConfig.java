// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.internal.UseCaseEventConfig;
import androidx.camera.core.internal.o\u30070;
import androidx.camera.core.UseCase;
import android.util.Range;
import androidx.camera.core.internal.TargetConfig;
import androidx.camera.core.internal.O8;
import java.util.List;
import java.util.Set;
import android.util.Size;
import androidx.annotation.Nullable;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.internal.Oo08;
import java.util.concurrent.Executor;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.camera.core.internal.ThreadConfig;
import androidx.camera.core.Preview;

@RequiresApi(21)
public final class PreviewConfig implements UseCaseConfig<Preview>, ImageOutputConfig, ThreadConfig
{
    public static final Option<ImageInfoProcessor> IMAGE_INFO_PROCESSOR;
    public static final Option<CaptureProcessor> OPTION_PREVIEW_CAPTURE_PROCESSOR;
    public static final Option<Boolean> OPTION_RGBA8888_SURFACE_REQUIRED;
    private final OptionsBundle mConfig;
    
    static {
        IMAGE_INFO_PROCESSOR = (Option)Config.Option.create("camerax.core.preview.imageInfoProcessor", ImageInfoProcessor.class);
        OPTION_PREVIEW_CAPTURE_PROCESSOR = (Option)Config.Option.create("camerax.core.preview.captureProcessor", CaptureProcessor.class);
        OPTION_RGBA8888_SURFACE_REQUIRED = (Option)Config.Option.create("camerax.core.preview.isRgba8888SurfaceRequired", Boolean.class);
    }
    
    public PreviewConfig(@NonNull final OptionsBundle mConfig) {
        this.mConfig = mConfig;
    }
    
    @NonNull
    public CaptureProcessor getCaptureProcessor() {
        return (CaptureProcessor)this.retrieveOption(PreviewConfig.OPTION_PREVIEW_CAPTURE_PROCESSOR);
    }
    
    @Nullable
    public CaptureProcessor getCaptureProcessor(@Nullable final CaptureProcessor captureProcessor) {
        return (CaptureProcessor)this.retrieveOption(PreviewConfig.OPTION_PREVIEW_CAPTURE_PROCESSOR, captureProcessor);
    }
    
    @NonNull
    @Override
    public Config getConfig() {
        return this.mConfig;
    }
    
    @NonNull
    ImageInfoProcessor getImageInfoProcessor() {
        return (ImageInfoProcessor)this.retrieveOption(PreviewConfig.IMAGE_INFO_PROCESSOR);
    }
    
    @Nullable
    public ImageInfoProcessor getImageInfoProcessor(@Nullable final ImageInfoProcessor imageInfoProcessor) {
        return (ImageInfoProcessor)this.retrieveOption(PreviewConfig.IMAGE_INFO_PROCESSOR, imageInfoProcessor);
    }
    
    @Override
    public int getInputFormat() {
        return (int)this.retrieveOption(ImageInputConfig.OPTION_INPUT_FORMAT);
    }
    
    public boolean isRgba8888SurfaceRequired(final boolean b) {
        return (boolean)this.retrieveOption(PreviewConfig.OPTION_RGBA8888_SURFACE_REQUIRED, b);
    }
    
    @Override
    public /* synthetic */ Object retrieveOption(final Option option) {
        return o\u3007\u30070\u3007.o\u30070(this, option);
    }
    
    @Override
    public /* synthetic */ Object retrieveOption(final Option option, final Object o) {
        return o\u3007\u30070\u3007.\u3007\u3007888(this, option, o);
    }
}

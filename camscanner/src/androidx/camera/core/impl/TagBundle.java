// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.Set;
import androidx.annotation.Nullable;
import java.util.Iterator;
import android.util.Pair;
import androidx.annotation.NonNull;
import android.util.ArrayMap;
import java.util.Map;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class TagBundle
{
    private static final String CAMERAX_USER_TAG_PREFIX = "android.hardware.camera2.CaptureRequest.setTag.CX";
    private static final TagBundle EMPTY_TAGBUNDLE;
    private static final String USER_TAG_PREFIX = "android.hardware.camera2.CaptureRequest.setTag.";
    protected final Map<String, Object> mTagMap;
    
    static {
        EMPTY_TAGBUNDLE = new TagBundle((Map<String, Object>)new ArrayMap());
    }
    
    protected TagBundle(@NonNull final Map<String, Object> mTagMap) {
        this.mTagMap = mTagMap;
    }
    
    @NonNull
    public static TagBundle create(@NonNull final Pair<String, Object> pair) {
        final ArrayMap arrayMap = new ArrayMap();
        ((Map<Object, Object>)arrayMap).put(pair.first, pair.second);
        return new TagBundle((Map<String, Object>)arrayMap);
    }
    
    @NonNull
    public static TagBundle emptyBundle() {
        return TagBundle.EMPTY_TAGBUNDLE;
    }
    
    @NonNull
    public static TagBundle from(@NonNull final TagBundle tagBundle) {
        final ArrayMap arrayMap = new ArrayMap();
        for (final String s : tagBundle.listKeys()) {
            ((Map<String, Object>)arrayMap).put(s, tagBundle.getTag(s));
        }
        return new TagBundle((Map<String, Object>)arrayMap);
    }
    
    @Nullable
    public Object getTag(@NonNull final String s) {
        return this.mTagMap.get(s);
    }
    
    @NonNull
    public Set<String> listKeys() {
        return this.mTagMap.keySet();
    }
    
    @NonNull
    @Override
    public final String toString() {
        return "android.hardware.camera2.CaptureRequest.setTag.CX";
    }
}

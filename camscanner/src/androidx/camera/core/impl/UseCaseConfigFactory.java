// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.InitializationException;
import android.content.Context;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface UseCaseConfigFactory
{
    public static final UseCaseConfigFactory EMPTY_INSTANCE = new UseCaseConfigFactory() {
        @Nullable
        @Override
        public Config getConfig(@NonNull final CaptureType captureType, final int n) {
            return null;
        }
    };
    
    @Nullable
    Config getConfig(@NonNull final CaptureType p0, final int p1);
    
    public enum CaptureType
    {
        private static final CaptureType[] $VALUES;
        
        IMAGE_ANALYSIS, 
        IMAGE_CAPTURE, 
        PREVIEW, 
        VIDEO_CAPTURE;
    }
    
    public interface Provider
    {
        @NonNull
        UseCaseConfigFactory newInstance(@NonNull final Context p0) throws InitializationException;
    }
}

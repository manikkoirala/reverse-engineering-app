// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.InitializationException;
import java.util.Set;
import android.content.Context;
import android.util.Size;
import java.util.Map;
import androidx.annotation.Nullable;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface CameraDeviceSurfaceManager
{
    boolean checkSupported(@NonNull final String p0, @Nullable final List<SurfaceConfig> p1);
    
    @NonNull
    Map<UseCaseConfig<?>, Size> getSuggestedResolutions(@NonNull final String p0, @NonNull final List<AttachedSurfaceInfo> p1, @NonNull final List<UseCaseConfig<?>> p2);
    
    @Nullable
    SurfaceConfig transformSurfaceConfig(@NonNull final String p0, final int p1, @NonNull final Size p2);
    
    public interface Provider
    {
        @NonNull
        CameraDeviceSurfaceManager newInstance(@NonNull final Context p0, @Nullable final Object p1, @NonNull final Set<String> p2) throws InitializationException;
    }
}

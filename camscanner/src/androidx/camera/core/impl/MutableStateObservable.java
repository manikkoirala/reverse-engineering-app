// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class MutableStateObservable<T> extends StateObservable<T>
{
    private MutableStateObservable(@Nullable final Object o, final boolean b) {
        super(o, b);
    }
    
    @NonNull
    public static <T> MutableStateObservable<T> withInitialError(@NonNull final Throwable t) {
        return new MutableStateObservable<T>(t, true);
    }
    
    @NonNull
    public static <T> MutableStateObservable<T> withInitialState(@Nullable final T t) {
        return new MutableStateObservable<T>(t, false);
    }
    
    public void setError(@NonNull final Throwable t) {
        this.updateStateAsError(t);
    }
    
    public void setState(@Nullable final T t) {
        this.updateState(t);
    }
}

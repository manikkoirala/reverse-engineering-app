// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import android.util.Size;
import androidx.annotation.NonNull;
import android.view.Surface;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public abstract class OutputSurface
{
    @NonNull
    public static OutputSurface create(@NonNull final Surface surface, @NonNull final Size size, final int n) {
        return new AutoValue_OutputSurface(surface, size, n);
    }
    
    public abstract int getImageFormat();
    
    @NonNull
    public abstract Size getSize();
    
    @NonNull
    public abstract Surface getSurface();
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.Nullable;
import androidx.camera.core.CameraSelector;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;
import androidx.annotation.RequiresApi;
import androidx.camera.core.CameraInfo;

@RequiresApi(21)
public interface CameraInfoInternal extends CameraInfo
{
    void addSessionCaptureCallback(@NonNull final Executor p0, @NonNull final CameraCaptureCallback p1);
    
    @NonNull
    CamcorderProfileProvider getCamcorderProfileProvider();
    
    @NonNull
    String getCameraId();
    
    @NonNull
    Quirks getCameraQuirks();
    
    @NonNull
    CameraSelector getCameraSelector();
    
    @Nullable
    Integer getLensFacing();
    
    @NonNull
    Timebase getTimebase();
    
    void removeSessionCaptureCallback(@NonNull final CameraCaptureCallback p0);
}

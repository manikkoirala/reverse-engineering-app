// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import java.util.Objects;
import java.util.Collection;
import java.util.Collections;
import androidx.annotation.Nullable;
import java.util.Set;
import java.util.Iterator;
import android.util.ArrayMap;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class MutableOptionsBundle extends OptionsBundle implements MutableConfig
{
    @NonNull
    private static final OptionPriority DEFAULT_PRIORITY;
    
    static {
        DEFAULT_PRIORITY = OptionPriority.OPTIONAL;
    }
    
    private MutableOptionsBundle(final TreeMap<Option<?>, Map<OptionPriority, Object>> treeMap) {
        super(treeMap);
    }
    
    @NonNull
    public static MutableOptionsBundle create() {
        return new MutableOptionsBundle(new TreeMap<Option<?>, Map<OptionPriority, Object>>(OptionsBundle.ID_COMPARE));
    }
    
    @NonNull
    public static MutableOptionsBundle from(@NonNull final Config config) {
        final TreeMap treeMap = new TreeMap((Comparator<? super K>)OptionsBundle.ID_COMPARE);
        for (final Config.Option<?> key : config.listOptions()) {
            final Set<Config.OptionPriority> priorities = config.getPriorities(key);
            final ArrayMap value = new ArrayMap();
            for (final OptionPriority optionPriority : priorities) {
                ((Map<OptionPriority, Object>)value).put(optionPriority, config.retrieveOptionWithPriority(key, optionPriority));
            }
            treeMap.put(key, value);
        }
        return new MutableOptionsBundle(treeMap);
    }
    
    @Override
    public <ValueT> void insertOption(@NonNull final Option<ValueT> option, @NonNull final OptionPriority obj, @Nullable final ValueT valueT) {
        final Map map = super.mOptions.get(option);
        if (map == null) {
            final ArrayMap value = new ArrayMap();
            super.mOptions.put(option, value);
            ((Map<OptionPriority, ValueT>)value).put(obj, valueT);
            return;
        }
        final OptionPriority obj2 = Collections.min((Collection<? extends OptionPriority>)map.keySet());
        if (!Objects.equals(map.get(obj2), valueT) && Oooo8o0\u3007.\u3007080(obj2, obj)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Option values conflicts: ");
            sb.append(option.getId());
            sb.append(", existing value (");
            sb.append(obj2);
            sb.append(")=");
            sb.append(map.get(obj2));
            sb.append(", conflicting (");
            sb.append(obj);
            sb.append(")=");
            sb.append(valueT);
            throw new IllegalArgumentException(sb.toString());
        }
        map.put(obj, valueT);
    }
    
    @Override
    public <ValueT> void insertOption(@NonNull final Option<ValueT> option, @Nullable final ValueT valueT) {
        this.insertOption(option, MutableOptionsBundle.DEFAULT_PRIORITY, valueT);
    }
    
    @Nullable
    @Override
    public <ValueT> ValueT removeOption(@NonNull final Option<ValueT> key) {
        return (ValueT)super.mOptions.remove(key);
    }
}

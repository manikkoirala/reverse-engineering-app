// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.camera.core.impl.utils.futures.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.Collections;
import java.util.List;
import androidx.camera.core.ImageInfo;
import androidx.annotation.NonNull;
import androidx.camera.core.ImageProxy;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class SingleImageProxyBundle implements ImageProxyBundle
{
    private final int mCaptureId;
    private final ImageProxy mImageProxy;
    
    SingleImageProxyBundle(@NonNull final ImageProxy mImageProxy, final int mCaptureId) {
        this.mCaptureId = mCaptureId;
        this.mImageProxy = mImageProxy;
    }
    
    public SingleImageProxyBundle(@NonNull final ImageProxy mImageProxy, @NonNull final String s) {
        final ImageInfo imageInfo = mImageProxy.getImageInfo();
        if (imageInfo == null) {
            throw new IllegalArgumentException("ImageProxy has no associated ImageInfo");
        }
        final Integer n = (Integer)imageInfo.getTagBundle().getTag(s);
        if (n != null) {
            this.mCaptureId = n;
            this.mImageProxy = mImageProxy;
            return;
        }
        throw new IllegalArgumentException("ImageProxy has no associated tag");
    }
    
    public void close() {
        this.mImageProxy.close();
    }
    
    @NonNull
    @Override
    public List<Integer> getCaptureIds() {
        return Collections.singletonList(this.mCaptureId);
    }
    
    @NonNull
    @Override
    public ListenableFuture<ImageProxy> getImageProxy(final int n) {
        if (n != this.mCaptureId) {
            return Futures.immediateFailedFuture(new IllegalArgumentException("Capture id does not exist in the bundle"));
        }
        return Futures.immediateFuture(this.mImageProxy);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.NonNull;
import androidx.camera.core.ImageInfo;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface ImageInfoProcessor
{
    @Nullable
    CaptureStage getCaptureStage();
    
    boolean process(@NonNull final ImageInfo p0);
}

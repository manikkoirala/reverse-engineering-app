// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.Nullable;
import java.util.Set;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface Config
{
    boolean containsOption(@NonNull final Option<?> p0);
    
    void findOptions(@NonNull final String p0, @NonNull final OptionMatcher p1);
    
    @NonNull
    OptionPriority getOptionPriority(@NonNull final Option<?> p0);
    
    @NonNull
    Set<OptionPriority> getPriorities(@NonNull final Option<?> p0);
    
    @NonNull
    Set<Option<?>> listOptions();
    
    @Nullable
     <ValueT> ValueT retrieveOption(@NonNull final Option<ValueT> p0);
    
    @Nullable
     <ValueT> ValueT retrieveOption(@NonNull final Option<ValueT> p0, @Nullable final ValueT p1);
    
    @Nullable
     <ValueT> ValueT retrieveOptionWithPriority(@NonNull final Option<ValueT> p0, @NonNull final OptionPriority p1);
    
    public abstract static class Option<T>
    {
        Option() {
        }
        
        @NonNull
        public static <T> Option<T> create(@NonNull final String s, @NonNull final Class<?> clazz) {
            return create(s, clazz, null);
        }
        
        @NonNull
        public static <T> Option<T> create(@NonNull final String s, @NonNull final Class<?> clazz, @Nullable final Object o) {
            return (Option<T>)new AutoValue_Config_Option(s, (Class<Object>)clazz, o);
        }
        
        @NonNull
        public abstract String getId();
        
        @Nullable
        public abstract Object getToken();
        
        @NonNull
        public abstract Class<T> getValueClass();
    }
    
    public interface OptionMatcher
    {
        boolean onOptionMatched(@NonNull final Option<?> p0);
    }
    
    public enum OptionPriority
    {
        private static final OptionPriority[] $VALUES;
        
        ALWAYS_OVERRIDE, 
        OPTIONAL, 
        REQUIRED;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import androidx.annotation.Nullable;
import java.util.Set;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface ReadableConfig extends Config
{
    boolean containsOption(@NonNull final Option<?> p0);
    
    void findOptions(@NonNull final String p0, @NonNull final OptionMatcher p1);
    
    @NonNull
    Config getConfig();
    
    @NonNull
    OptionPriority getOptionPriority(@NonNull final Option<?> p0);
    
    @NonNull
    Set<OptionPriority> getPriorities(@NonNull final Option<?> p0);
    
    @NonNull
    Set<Option<?>> listOptions();
    
    @Nullable
     <ValueT> ValueT retrieveOption(@NonNull final Option<ValueT> p0);
    
    @Nullable
     <ValueT> ValueT retrieveOption(@NonNull final Option<ValueT> p0, @Nullable final ValueT p1);
    
    @Nullable
     <ValueT> ValueT retrieveOptionWithPriority(@NonNull final Option<ValueT> p0, @NonNull final OptionPriority p1);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import android.os.Handler;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public abstract class CameraThreadConfig
{
    @NonNull
    public static CameraThreadConfig create(@NonNull final Executor executor, @NonNull final Handler handler) {
        return new AutoValue_CameraThreadConfig(executor, handler);
    }
    
    @NonNull
    public abstract Executor getCameraExecutor();
    
    @NonNull
    public abstract Handler getSchedulerHandler();
}

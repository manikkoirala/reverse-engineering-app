// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.impl;

import android.util.Size;
import android.view.Surface;
import androidx.annotation.NonNull;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface CaptureProcessor
{
    void close();
    
    @NonNull
    ListenableFuture<Void> getCloseFuture();
    
    void onOutputSurface(@NonNull final Surface p0, final int p1);
    
    void onResolutionUpdate(@NonNull final Size p0);
    
    void process(@NonNull final ImageProxyBundle p0);
}

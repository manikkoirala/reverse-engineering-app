// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.RestrictTo;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.core.util.Preconditions;
import androidx.annotation.NonNull;
import android.util.Rational;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class ViewPort
{
    public static final int FILL_CENTER = 1;
    public static final int FILL_END = 2;
    public static final int FILL_START = 0;
    public static final int FIT = 3;
    @NonNull
    private Rational mAspectRatio;
    private int mLayoutDirection;
    private int mRotation;
    private int mScaleType;
    
    ViewPort(final int mScaleType, @NonNull final Rational mAspectRatio, final int mRotation, final int mLayoutDirection) {
        this.mScaleType = mScaleType;
        this.mAspectRatio = mAspectRatio;
        this.mRotation = mRotation;
        this.mLayoutDirection = mLayoutDirection;
    }
    
    @NonNull
    public Rational getAspectRatio() {
        return this.mAspectRatio;
    }
    
    public int getLayoutDirection() {
        return this.mLayoutDirection;
    }
    
    public int getRotation() {
        return this.mRotation;
    }
    
    public int getScaleType() {
        return this.mScaleType;
    }
    
    public static final class Builder
    {
        private static final int DEFAULT_LAYOUT_DIRECTION = 0;
        private static final int DEFAULT_SCALE_TYPE = 1;
        private final Rational mAspectRatio;
        private int mLayoutDirection;
        private final int mRotation;
        private int mScaleType;
        
        public Builder(@NonNull final Rational mAspectRatio, final int mRotation) {
            this.mScaleType = 1;
            this.mLayoutDirection = 0;
            this.mAspectRatio = mAspectRatio;
            this.mRotation = mRotation;
        }
        
        @NonNull
        public ViewPort build() {
            Preconditions.checkNotNull(this.mAspectRatio, "The crop aspect ratio must be set.");
            return new ViewPort(this.mScaleType, this.mAspectRatio, this.mRotation, this.mLayoutDirection);
        }
        
        @NonNull
        public Builder setLayoutDirection(final int mLayoutDirection) {
            this.mLayoutDirection = mLayoutDirection;
            return this;
        }
        
        @NonNull
        public Builder setScaleType(final int mScaleType) {
            this.mScaleType = mScaleType;
            return this;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public @interface LayoutDirection {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public @interface ScaleType {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.camera.core.impl.utils.futures.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.camera.core.internal.compat.ImageWriterCompat;
import android.os.Build$VERSION;
import androidx.camera.core.impl.TagBundle;
import androidx.core.os.OperationCanceledException;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import android.graphics.Matrix$ScaleToFit;
import androidx.camera.core.impl.utils.TransformUtils;
import android.graphics.RectF;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;
import androidx.annotation.VisibleForTesting;
import java.nio.ByteBuffer;
import android.media.ImageWriter;
import androidx.annotation.Nullable;
import androidx.annotation.IntRange;
import android.graphics.Rect;
import androidx.annotation.GuardedBy;
import android.graphics.Matrix;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.ImageReaderProxy;

@RequiresApi(21)
abstract class ImageAnalysisAbstractAnalyzer implements OnImageAvailableListener
{
    private static final String TAG = "ImageAnalysisAnalyzer";
    private final Object mAnalyzerLock;
    protected boolean mIsAttached;
    private volatile boolean mOnePixelShiftEnabled;
    @GuardedBy("mAnalyzerLock")
    private Matrix mOriginalSensorToBufferTransformMatrix;
    @GuardedBy("mAnalyzerLock")
    private Rect mOriginalViewPortCropRect;
    private volatile int mOutputImageFormat;
    private volatile boolean mOutputImageRotationEnabled;
    @IntRange(from = 0L, to = 359L)
    private volatile int mPrevBufferRotationDegrees;
    @GuardedBy("mAnalyzerLock")
    @Nullable
    private SafeCloseImageReaderProxy mProcessedImageReaderProxy;
    @GuardedBy("mAnalyzerLock")
    @Nullable
    private ImageWriter mProcessedImageWriter;
    @GuardedBy("mAnalyzerLock")
    @Nullable
    @VisibleForTesting
    ByteBuffer mRGBConvertedBuffer;
    @IntRange(from = 0L, to = 359L)
    private volatile int mRelativeRotation;
    @GuardedBy("mAnalyzerLock")
    private ImageAnalysis.Analyzer mSubscribedAnalyzer;
    @GuardedBy("mAnalyzerLock")
    @Nullable
    @VisibleForTesting
    ByteBuffer mURotatedBuffer;
    @GuardedBy("mAnalyzerLock")
    private Matrix mUpdatedSensorToBufferTransformMatrix;
    @GuardedBy("mAnalyzerLock")
    private Rect mUpdatedViewPortCropRect;
    @GuardedBy("mAnalyzerLock")
    private Executor mUserExecutor;
    @GuardedBy("mAnalyzerLock")
    @Nullable
    @VisibleForTesting
    ByteBuffer mVRotatedBuffer;
    @GuardedBy("mAnalyzerLock")
    @Nullable
    @VisibleForTesting
    ByteBuffer mYRotatedBuffer;
    
    ImageAnalysisAbstractAnalyzer() {
        this.mOutputImageFormat = 1;
        this.mOriginalViewPortCropRect = new Rect();
        this.mUpdatedViewPortCropRect = new Rect();
        this.mOriginalSensorToBufferTransformMatrix = new Matrix();
        this.mUpdatedSensorToBufferTransformMatrix = new Matrix();
        this.mAnalyzerLock = new Object();
        this.mIsAttached = true;
    }
    
    @GuardedBy("mAnalyzerLock")
    private void createHelperBuffer(@NonNull final ImageProxy imageProxy) {
        if (this.mOutputImageFormat == 1) {
            if (this.mYRotatedBuffer == null) {
                this.mYRotatedBuffer = ByteBuffer.allocateDirect(imageProxy.getWidth() * imageProxy.getHeight());
            }
            this.mYRotatedBuffer.position(0);
            if (this.mURotatedBuffer == null) {
                this.mURotatedBuffer = ByteBuffer.allocateDirect(imageProxy.getWidth() * imageProxy.getHeight() / 4);
            }
            this.mURotatedBuffer.position(0);
            if (this.mVRotatedBuffer == null) {
                this.mVRotatedBuffer = ByteBuffer.allocateDirect(imageProxy.getWidth() * imageProxy.getHeight() / 4);
            }
            this.mVRotatedBuffer.position(0);
        }
        else if (this.mOutputImageFormat == 2 && this.mRGBConvertedBuffer == null) {
            this.mRGBConvertedBuffer = ByteBuffer.allocateDirect(imageProxy.getWidth() * imageProxy.getHeight() * 4);
        }
    }
    
    @NonNull
    private static SafeCloseImageReaderProxy createImageReaderProxy(int n, final int n2, int n3, final int n4, final int n5) {
        if (n3 != 90 && n3 != 270) {
            n3 = 0;
        }
        else {
            n3 = 1;
        }
        int n6;
        if (n3 != 0) {
            n6 = n2;
        }
        else {
            n6 = n;
        }
        if (n3 == 0) {
            n = n2;
        }
        return new SafeCloseImageReaderProxy(ImageReaderProxys.createIsolatedReader(n6, n, n4, n5));
    }
    
    @NonNull
    @VisibleForTesting
    static Matrix getAdditionalTransformMatrixAppliedByProcessor(final int n, final int n2, final int n3, final int n4, @IntRange(from = 0L, to = 359L) final int n5) {
        final Matrix matrix = new Matrix();
        if (n5 > 0) {
            matrix.setRectToRect(new RectF(0.0f, 0.0f, (float)n, (float)n2), TransformUtils.NORMALIZED_RECT, Matrix$ScaleToFit.FILL);
            matrix.postRotate((float)n5);
            matrix.postConcat(TransformUtils.getNormalizedToBuffer(new RectF(0.0f, 0.0f, (float)n3, (float)n4)));
        }
        return matrix;
    }
    
    @NonNull
    static Rect getUpdatedCropRect(@NonNull final Rect rect, @NonNull final Matrix matrix) {
        final RectF rectF = new RectF(rect);
        matrix.mapRect(rectF);
        final Rect rect2 = new Rect();
        rectF.round(rect2);
        return rect2;
    }
    
    @GuardedBy("mAnalyzerLock")
    private void recalculateTransformMatrixAndCropRect(final int n, final int n2, final int n3, final int n4) {
        final Matrix additionalTransformMatrixAppliedByProcessor = getAdditionalTransformMatrixAppliedByProcessor(n, n2, n3, n4, this.mRelativeRotation);
        this.mUpdatedViewPortCropRect = getUpdatedCropRect(this.mOriginalViewPortCropRect, additionalTransformMatrixAppliedByProcessor);
        this.mUpdatedSensorToBufferTransformMatrix.setConcat(this.mOriginalSensorToBufferTransformMatrix, additionalTransformMatrixAppliedByProcessor);
    }
    
    @GuardedBy("mAnalyzerLock")
    private void recreateImageReaderProxy(@NonNull final ImageProxy imageProxy, @IntRange(from = 0L, to = 359L) final int n) {
        final SafeCloseImageReaderProxy mProcessedImageReaderProxy = this.mProcessedImageReaderProxy;
        if (mProcessedImageReaderProxy == null) {
            return;
        }
        mProcessedImageReaderProxy.safeClose();
        this.mProcessedImageReaderProxy = createImageReaderProxy(imageProxy.getWidth(), imageProxy.getHeight(), n, this.mProcessedImageReaderProxy.getImageFormat(), this.mProcessedImageReaderProxy.getMaxImages());
        if (Build$VERSION.SDK_INT >= 23 && this.mOutputImageFormat == 1) {
            final ImageWriter mProcessedImageWriter = this.mProcessedImageWriter;
            if (mProcessedImageWriter != null) {
                ImageWriterCompat.close(mProcessedImageWriter);
            }
            this.mProcessedImageWriter = ImageWriterCompat.newInstance(this.mProcessedImageReaderProxy.getSurface(), this.mProcessedImageReaderProxy.getMaxImages());
        }
    }
    
    @Nullable
    abstract ImageProxy acquireImage(@NonNull final ImageReaderProxy p0);
    
    ListenableFuture<Void> analyzeImage(@NonNull final ImageProxy imageProxy) {
        final boolean mOutputImageRotationEnabled = this.mOutputImageRotationEnabled;
        boolean b = false;
        int mRelativeRotation;
        if (mOutputImageRotationEnabled) {
            mRelativeRotation = this.mRelativeRotation;
        }
        else {
            mRelativeRotation = 0;
        }
        Object o = this.mAnalyzerLock;
        synchronized (o) {
            final Executor mUserExecutor = this.mUserExecutor;
            final ImageAnalysis.Analyzer mSubscribedAnalyzer = this.mSubscribedAnalyzer;
            final boolean b2 = this.mOutputImageRotationEnabled && mRelativeRotation != this.mPrevBufferRotationDegrees;
            if (b2) {
                this.recreateImageReaderProxy(imageProxy, mRelativeRotation);
            }
            if (this.mOutputImageRotationEnabled) {
                this.createHelperBuffer(imageProxy);
            }
            final SafeCloseImageReaderProxy mProcessedImageReaderProxy = this.mProcessedImageReaderProxy;
            final ImageWriter mProcessedImageWriter = this.mProcessedImageWriter;
            final ByteBuffer mrgbConvertedBuffer = this.mRGBConvertedBuffer;
            final ByteBuffer myRotatedBuffer = this.mYRotatedBuffer;
            final ByteBuffer muRotatedBuffer = this.mURotatedBuffer;
            final ByteBuffer mvRotatedBuffer = this.mVRotatedBuffer;
            monitorexit(o);
            if (mSubscribedAnalyzer != null && mUserExecutor != null && this.mIsAttached) {
                Label_0239: {
                    if (mProcessedImageReaderProxy != null) {
                        if (this.mOutputImageFormat == 2) {
                            o = ImageProcessingUtil.convertYUVToRGB(imageProxy, mProcessedImageReaderProxy, mrgbConvertedBuffer, mRelativeRotation, this.mOnePixelShiftEnabled);
                            break Label_0239;
                        }
                        if (this.mOutputImageFormat == 1) {
                            if (this.mOnePixelShiftEnabled) {
                                ImageProcessingUtil.applyPixelShiftForYUV(imageProxy);
                            }
                            if (mProcessedImageWriter != null && myRotatedBuffer != null && muRotatedBuffer != null && mvRotatedBuffer != null) {
                                o = ImageProcessingUtil.rotateYUV(imageProxy, mProcessedImageReaderProxy, mProcessedImageWriter, myRotatedBuffer, muRotatedBuffer, mvRotatedBuffer, mRelativeRotation);
                                break Label_0239;
                            }
                        }
                    }
                    o = null;
                }
                if (o == null) {
                    b = true;
                }
                if (b) {
                    o = imageProxy;
                }
                final Rect rect = new Rect();
                final Matrix matrix = new Matrix();
                final Object mAnalyzerLock = this.mAnalyzerLock;
                monitorenter(mAnalyzerLock);
                Label_0324: {
                    if (!b2 || b) {
                        break Label_0324;
                    }
                    try {
                        this.recalculateTransformMatrixAndCropRect(imageProxy.getWidth(), imageProxy.getHeight(), ((ImageProxy)o).getWidth(), ((ImageProxy)o).getHeight());
                        this.mPrevBufferRotationDegrees = mRelativeRotation;
                        rect.set(this.mUpdatedViewPortCropRect);
                        matrix.set(this.mUpdatedSensorToBufferTransformMatrix);
                        monitorexit(mAnalyzerLock);
                        CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Object>)new \u3007O888o0o(this, mUserExecutor, imageProxy, matrix, (ImageProxy)o, rect, mSubscribedAnalyzer));
                        return;
                    }
                    finally {
                        monitorexit(mAnalyzerLock);
                    }
                }
            }
            return Futures.immediateFailedFuture(new OperationCanceledException("No analyzer or executor currently set."));
        }
    }
    
    void attach() {
        this.mIsAttached = true;
    }
    
    abstract void clearCache();
    
    void detach() {
        this.mIsAttached = false;
        this.clearCache();
    }
    
    @Override
    public void onImageAvailable(@NonNull final ImageReaderProxy imageReaderProxy) {
        try {
            final ImageProxy acquireImage = this.acquireImage(imageReaderProxy);
            if (acquireImage != null) {
                this.onValidImageAvailable(acquireImage);
            }
        }
        catch (final IllegalStateException ex) {
            Logger.e("ImageAnalysisAnalyzer", "Failed to acquire image.", ex);
        }
    }
    
    abstract void onValidImageAvailable(@NonNull final ImageProxy p0);
    
    void setAnalyzer(@Nullable final Executor mUserExecutor, @Nullable final ImageAnalysis.Analyzer mSubscribedAnalyzer) {
        if (mSubscribedAnalyzer == null) {
            this.clearCache();
        }
        synchronized (this.mAnalyzerLock) {
            this.mSubscribedAnalyzer = mSubscribedAnalyzer;
            this.mUserExecutor = mUserExecutor;
        }
    }
    
    void setOnePixelShiftEnabled(final boolean mOnePixelShiftEnabled) {
        this.mOnePixelShiftEnabled = mOnePixelShiftEnabled;
    }
    
    void setOutputImageFormat(final int mOutputImageFormat) {
        this.mOutputImageFormat = mOutputImageFormat;
    }
    
    void setOutputImageRotationEnabled(final boolean mOutputImageRotationEnabled) {
        this.mOutputImageRotationEnabled = mOutputImageRotationEnabled;
    }
    
    void setProcessedImageReaderProxy(@NonNull final SafeCloseImageReaderProxy mProcessedImageReaderProxy) {
        synchronized (this.mAnalyzerLock) {
            this.mProcessedImageReaderProxy = mProcessedImageReaderProxy;
        }
    }
    
    void setRelativeRotation(final int mRelativeRotation) {
        this.mRelativeRotation = mRelativeRotation;
    }
    
    void setSensorToBufferTransformMatrix(@NonNull final Matrix mOriginalSensorToBufferTransformMatrix) {
        synchronized (this.mAnalyzerLock) {
            this.mOriginalSensorToBufferTransformMatrix = mOriginalSensorToBufferTransformMatrix;
            this.mUpdatedSensorToBufferTransformMatrix = new Matrix(this.mOriginalSensorToBufferTransformMatrix);
        }
    }
    
    void setViewPortCropRect(@NonNull final Rect mOriginalViewPortCropRect) {
        synchronized (this.mAnalyzerLock) {
            this.mOriginalViewPortCropRect = mOriginalViewPortCropRect;
            this.mUpdatedViewPortCropRect = new Rect(this.mOriginalViewPortCropRect);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.view.Surface;
import androidx.core.util.Consumer;
import java.util.concurrent.Executor;
import androidx.annotation.NonNull;
import android.util.Size;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public interface SurfaceOutput
{
    void close();
    
    int getFormat();
    
    int getRotationDegrees();
    
    @NonNull
    Size getSize();
    
    @NonNull
    Surface getSurface(@NonNull final Executor p0, @NonNull final Consumer<Event> p1);
    
    int getTargets();
    
    void updateTransformMatrix(@NonNull final float[] p0, @NonNull final float[] p1);
    
    public abstract static class Event
    {
        public static final int EVENT_REQUEST_CLOSE = 0;
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public static Event of(final int n, @NonNull final SurfaceOutput surfaceOutput) {
            return (Event)new AutoValue_SurfaceOutput_Event(n, surfaceOutput);
        }
        
        public abstract int getEventCode();
        
        @NonNull
        public abstract SurfaceOutput getSurfaceOutput();
        
        @Retention(RetentionPolicy.SOURCE)
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public @interface EventCode {
        }
    }
    
    public enum GlTransformOptions
    {
        private static final GlTransformOptions[] $VALUES;
        
        APPLY_CROP_ROTATE_AND_MIRRORING, 
        USE_SURFACE_TEXTURE_TRANSFORM;
    }
}

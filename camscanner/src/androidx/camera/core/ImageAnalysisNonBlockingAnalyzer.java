// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.lang.ref.WeakReference;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.ImageReaderProxy;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.Nullable;
import androidx.annotation.GuardedBy;
import java.util.concurrent.Executor;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
final class ImageAnalysisNonBlockingAnalyzer extends ImageAnalysisAbstractAnalyzer
{
    final Executor mBackgroundExecutor;
    @GuardedBy("mLock")
    @Nullable
    @VisibleForTesting
    ImageProxy mCachedImage;
    private final Object mLock;
    @GuardedBy("mLock")
    @Nullable
    private CacheAnalyzingImageProxy mPostedImage;
    
    ImageAnalysisNonBlockingAnalyzer(final Executor mBackgroundExecutor) {
        this.mLock = new Object();
        this.mBackgroundExecutor = mBackgroundExecutor;
    }
    
    @Nullable
    @Override
    ImageProxy acquireImage(@NonNull final ImageReaderProxy imageReaderProxy) {
        return imageReaderProxy.acquireLatestImage();
    }
    
    void analyzeCachedImage() {
        synchronized (this.mLock) {
            this.mPostedImage = null;
            final ImageProxy mCachedImage = this.mCachedImage;
            if (mCachedImage != null) {
                this.mCachedImage = null;
                this.onValidImageAvailable(mCachedImage);
            }
        }
    }
    
    @Override
    void clearCache() {
        synchronized (this.mLock) {
            final ImageProxy mCachedImage = this.mCachedImage;
            if (mCachedImage != null) {
                mCachedImage.close();
                this.mCachedImage = null;
            }
        }
    }
    
    @Override
    void onValidImageAvailable(@NonNull final ImageProxy mCachedImage) {
        synchronized (this.mLock) {
            if (!super.mIsAttached) {
                mCachedImage.close();
                return;
            }
            if (this.mPostedImage != null) {
                if (mCachedImage.getImageInfo().getTimestamp() <= this.mPostedImage.getImageInfo().getTimestamp()) {
                    mCachedImage.close();
                }
                else {
                    final ImageProxy mCachedImage2 = this.mCachedImage;
                    if (mCachedImage2 != null) {
                        mCachedImage2.close();
                    }
                    this.mCachedImage = mCachedImage;
                }
                return;
            }
            final CacheAnalyzingImageProxy mPostedImage = new CacheAnalyzingImageProxy(mCachedImage, this);
            this.mPostedImage = mPostedImage;
            Futures.addCallback(this.analyzeImage(mPostedImage), new FutureCallback<Void>(this, mPostedImage) {
                final ImageAnalysisNonBlockingAnalyzer this$0;
                final CacheAnalyzingImageProxy val$newPostedImage;
                
                @Override
                public void onFailure(@NonNull final Throwable t) {
                    this.val$newPostedImage.close();
                }
                
                @Override
                public void onSuccess(final Void void1) {
                }
            }, CameraXExecutors.directExecutor());
        }
    }
    
    static class CacheAnalyzingImageProxy extends ForwardingImageProxy
    {
        final WeakReference<ImageAnalysisNonBlockingAnalyzer> mNonBlockingAnalyzerWeakReference;
        
        CacheAnalyzingImageProxy(@NonNull final ImageProxy imageProxy, @NonNull final ImageAnalysisNonBlockingAnalyzer referent) {
            super(imageProxy);
            this.mNonBlockingAnalyzerWeakReference = new WeakReference<ImageAnalysisNonBlockingAnalyzer>(referent);
            this.addOnImageCloseListener((OnImageCloseListener)new o\u3007O8\u3007\u3007o(this));
        }
    }
}

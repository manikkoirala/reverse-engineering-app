// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.core.util.Preconditions;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;
import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;

@RequiresApi(api = 21)
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class CameraEffect
{
    public static final int IMAGE_CAPTURE = 4;
    public static final int PREVIEW = 1;
    public static final int VIDEO_CAPTURE = 2;
    @Nullable
    private final ImageProcessor mImageProcessor;
    @NonNull
    private final Executor mProcessorExecutor;
    @Nullable
    private final SurfaceProcessor mSurfaceProcessor;
    private final int mTargets;
    
    protected CameraEffect(final int mTargets, @NonNull final Executor mProcessorExecutor, @NonNull final ImageProcessor mImageProcessor) {
        this.mTargets = mTargets;
        this.mProcessorExecutor = mProcessorExecutor;
        this.mSurfaceProcessor = null;
        this.mImageProcessor = mImageProcessor;
    }
    
    protected CameraEffect(final int mTargets, @NonNull final Executor mProcessorExecutor, @NonNull final SurfaceProcessor mSurfaceProcessor) {
        this.mTargets = mTargets;
        this.mProcessorExecutor = mProcessorExecutor;
        this.mSurfaceProcessor = mSurfaceProcessor;
        this.mImageProcessor = null;
    }
    
    @Nullable
    public ImageProcessor getImageProcessor() {
        return this.mImageProcessor;
    }
    
    @NonNull
    public Executor getProcessorExecutor() {
        return this.mProcessorExecutor;
    }
    
    @Nullable
    public SurfaceProcessor getSurfaceProcessor() {
        return this.mSurfaceProcessor;
    }
    
    public int getTargets() {
        return this.mTargets;
    }
    
    public static class Builder
    {
        @Nullable
        private ImageProcessor mImageProcessor;
        @Nullable
        private Executor mProcessorExecutor;
        @Nullable
        private SurfaceProcessor mSurfaceProcessor;
        private final int mTargets;
        
        public Builder(final int mTargets) {
            this.mTargets = mTargets;
        }
        
        @NonNull
        public CameraEffect build() {
            final Executor mProcessorExecutor = this.mProcessorExecutor;
            boolean b = true;
            Preconditions.checkState(mProcessorExecutor != null, "Must have a executor");
            final boolean b2 = this.mImageProcessor != null;
            if (this.mSurfaceProcessor == null) {
                b = false;
            }
            Preconditions.checkState(b2 ^ b, "Must have one and only one processor");
            final SurfaceProcessor mSurfaceProcessor = this.mSurfaceProcessor;
            if (mSurfaceProcessor != null) {
                return new CameraEffect(this.mTargets, this.mProcessorExecutor, mSurfaceProcessor);
            }
            return new CameraEffect(this.mTargets, this.mProcessorExecutor, this.mImageProcessor);
        }
        
        @NonNull
        public Builder setImageProcessor(@NonNull final Executor mProcessorExecutor, @NonNull final ImageProcessor mImageProcessor) {
            this.mProcessorExecutor = mProcessorExecutor;
            this.mImageProcessor = mImageProcessor;
            return this;
        }
        
        @NonNull
        public Builder setSurfaceProcessor(@NonNull final Executor mProcessorExecutor, @NonNull final SurfaceProcessor mSurfaceProcessor) {
            this.mProcessorExecutor = mProcessorExecutor;
            this.mSurfaceProcessor = mSurfaceProcessor;
            return this;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface Targets {
    }
}

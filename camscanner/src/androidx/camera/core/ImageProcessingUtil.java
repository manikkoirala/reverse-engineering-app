// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import android.media.Image;
import androidx.camera.core.internal.compat.ImageWriterCompat;
import android.os.Build$VERSION;
import android.media.ImageWriter;
import java.util.Locale;
import android.util.Log;
import androidx.annotation.IntRange;
import java.nio.ByteBuffer;
import androidx.annotation.Nullable;
import android.view.Surface;
import androidx.core.util.Preconditions;
import androidx.camera.core.impl.ImageReaderProxy;
import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public final class ImageProcessingUtil
{
    private static final String TAG = "ImageProcessingUtil";
    private static int sImageCount;
    
    static {
        System.loadLibrary("image_processing_util_jni");
    }
    
    private ImageProcessingUtil() {
    }
    
    public static boolean applyPixelShiftForYUV(@NonNull final ImageProxy imageProxy) {
        if (!isSupportedYUVFormat(imageProxy)) {
            Logger.e("ImageProcessingUtil", "Unsupported format for YUV to RGB");
            return false;
        }
        if (applyPixelShiftInternal(imageProxy) == Result.ERROR_CONVERSION) {
            Logger.e("ImageProcessingUtil", "One pixel shift for YUV failure");
            return false;
        }
        return true;
    }
    
    @NonNull
    private static Result applyPixelShiftInternal(@NonNull final ImageProxy imageProxy) {
        final int width = imageProxy.getWidth();
        final int height = imageProxy.getHeight();
        final int rowStride = imageProxy.getPlanes()[0].getRowStride();
        final int rowStride2 = imageProxy.getPlanes()[1].getRowStride();
        final int rowStride3 = imageProxy.getPlanes()[2].getRowStride();
        final int pixelStride = imageProxy.getPlanes()[0].getPixelStride();
        final int pixelStride2 = imageProxy.getPlanes()[1].getPixelStride();
        if (nativeShiftPixel(imageProxy.getPlanes()[0].getBuffer(), rowStride, imageProxy.getPlanes()[1].getBuffer(), rowStride2, imageProxy.getPlanes()[2].getBuffer(), rowStride3, pixelStride, pixelStride2, width, height, pixelStride, pixelStride2, pixelStride2) != 0) {
            return Result.ERROR_CONVERSION;
        }
        return Result.SUCCESS;
    }
    
    @Nullable
    public static ImageProxy convertJpegBytesToImage(@NonNull final ImageReaderProxy imageReaderProxy, @NonNull final byte[] array) {
        Preconditions.checkArgument(imageReaderProxy.getImageFormat() == 256);
        Preconditions.checkNotNull(array);
        final Surface surface = imageReaderProxy.getSurface();
        Preconditions.checkNotNull(surface);
        if (nativeWriteJpegToSurface(array, surface) != 0) {
            Logger.e("ImageProcessingUtil", "Failed to enqueue JPEG image.");
            return null;
        }
        final ImageProxy acquireLatestImage = imageReaderProxy.acquireLatestImage();
        if (acquireLatestImage == null) {
            Logger.e("ImageProcessingUtil", "Failed to get acquire JPEG image.");
        }
        return acquireLatestImage;
    }
    
    @Nullable
    public static ImageProxy convertYUVToRGB(@NonNull final ImageProxy imageProxy, @NonNull final ImageReaderProxy imageReaderProxy, @Nullable final ByteBuffer byteBuffer, @IntRange(from = 0L, to = 359L) final int n, final boolean b) {
        if (!isSupportedYUVFormat(imageProxy)) {
            Logger.e("ImageProcessingUtil", "Unsupported format for YUV to RGB");
            return null;
        }
        final long currentTimeMillis = System.currentTimeMillis();
        if (!isSupportedRotationDegrees(n)) {
            Logger.e("ImageProcessingUtil", "Unsupported rotation degrees for rotate RGB");
            return null;
        }
        if (convertYUVToRGBInternal(imageProxy, imageReaderProxy.getSurface(), byteBuffer, n, b) == Result.ERROR_CONVERSION) {
            Logger.e("ImageProcessingUtil", "YUV to RGB conversion failure");
            return null;
        }
        if (Log.isLoggable("MH", 3)) {
            Logger.d("ImageProcessingUtil", String.format(Locale.US, "Image processing performance profiling, duration: [%d], image count: %d", System.currentTimeMillis() - currentTimeMillis, ImageProcessingUtil.sImageCount));
            ++ImageProcessingUtil.sImageCount;
        }
        final ImageProxy acquireLatestImage = imageReaderProxy.acquireLatestImage();
        if (acquireLatestImage == null) {
            Logger.e("ImageProcessingUtil", "YUV to RGB acquireLatestImage failure");
            return null;
        }
        final SingleCloseImageProxy singleCloseImageProxy = new SingleCloseImageProxy(acquireLatestImage);
        singleCloseImageProxy.addOnImageCloseListener((ForwardingImageProxy.OnImageCloseListener)new \u30078(acquireLatestImage, imageProxy));
        return singleCloseImageProxy;
    }
    
    @NonNull
    private static Result convertYUVToRGBInternal(@NonNull final ImageProxy imageProxy, @NonNull final Surface surface, @Nullable final ByteBuffer byteBuffer, final int n, final boolean b) {
        final int width = imageProxy.getWidth();
        final int height = imageProxy.getHeight();
        final int rowStride = imageProxy.getPlanes()[0].getRowStride();
        final int rowStride2 = imageProxy.getPlanes()[1].getRowStride();
        final int rowStride3 = imageProxy.getPlanes()[2].getRowStride();
        final int pixelStride = imageProxy.getPlanes()[0].getPixelStride();
        final int pixelStride2 = imageProxy.getPlanes()[1].getPixelStride();
        int n2;
        if (b) {
            n2 = pixelStride;
        }
        else {
            n2 = 0;
        }
        int n3;
        if (b) {
            n3 = pixelStride2;
        }
        else {
            n3 = 0;
        }
        int n4;
        if (b) {
            n4 = pixelStride2;
        }
        else {
            n4 = 0;
        }
        if (nativeConvertAndroid420ToABGR(imageProxy.getPlanes()[0].getBuffer(), rowStride, imageProxy.getPlanes()[1].getBuffer(), rowStride2, imageProxy.getPlanes()[2].getBuffer(), rowStride3, pixelStride, pixelStride2, surface, byteBuffer, width, height, n2, n3, n4, n) != 0) {
            return Result.ERROR_CONVERSION;
        }
        return Result.SUCCESS;
    }
    
    private static boolean isSupportedRotationDegrees(@IntRange(from = 0L, to = 359L) final int n) {
        return n == 0 || n == 90 || n == 180 || n == 270;
    }
    
    private static boolean isSupportedYUVFormat(@NonNull final ImageProxy imageProxy) {
        return imageProxy.getFormat() == 35 && imageProxy.getPlanes().length == 3;
    }
    
    private static native int nativeConvertAndroid420ToABGR(@NonNull final ByteBuffer p0, final int p1, @NonNull final ByteBuffer p2, final int p3, @NonNull final ByteBuffer p4, final int p5, final int p6, final int p7, @NonNull final Surface p8, @Nullable final ByteBuffer p9, final int p10, final int p11, final int p12, final int p13, final int p14, final int p15);
    
    private static native int nativeRotateYUV(@NonNull final ByteBuffer p0, final int p1, @NonNull final ByteBuffer p2, final int p3, @NonNull final ByteBuffer p4, final int p5, final int p6, @NonNull final ByteBuffer p7, final int p8, final int p9, @NonNull final ByteBuffer p10, final int p11, final int p12, @NonNull final ByteBuffer p13, final int p14, final int p15, @NonNull final ByteBuffer p16, @NonNull final ByteBuffer p17, @NonNull final ByteBuffer p18, final int p19, final int p20, final int p21);
    
    private static native int nativeShiftPixel(@NonNull final ByteBuffer p0, final int p1, @NonNull final ByteBuffer p2, final int p3, @NonNull final ByteBuffer p4, final int p5, final int p6, final int p7, final int p8, final int p9, final int p10, final int p11, final int p12);
    
    private static native int nativeWriteJpegToSurface(@NonNull final byte[] p0, @NonNull final Surface p1);
    
    @Nullable
    public static ImageProxy rotateYUV(@NonNull final ImageProxy imageProxy, @NonNull final ImageReaderProxy imageReaderProxy, @NonNull final ImageWriter imageWriter, @NonNull final ByteBuffer byteBuffer, @NonNull final ByteBuffer byteBuffer2, @NonNull final ByteBuffer byteBuffer3, @IntRange(from = 0L, to = 359L) final int n) {
        if (!isSupportedYUVFormat(imageProxy)) {
            Logger.e("ImageProcessingUtil", "Unsupported format for rotate YUV");
            return null;
        }
        if (!isSupportedRotationDegrees(n)) {
            Logger.e("ImageProcessingUtil", "Unsupported rotation degrees for rotate YUV");
            return null;
        }
        final Result error_CONVERSION = Result.ERROR_CONVERSION;
        Result rotateYUVInternal;
        if (Build$VERSION.SDK_INT >= 23 && n > 0) {
            rotateYUVInternal = rotateYUVInternal(imageProxy, imageWriter, byteBuffer, byteBuffer2, byteBuffer3, n);
        }
        else {
            rotateYUVInternal = error_CONVERSION;
        }
        if (rotateYUVInternal == error_CONVERSION) {
            Logger.e("ImageProcessingUtil", "rotate YUV failure");
            return null;
        }
        final ImageProxy acquireLatestImage = imageReaderProxy.acquireLatestImage();
        if (acquireLatestImage == null) {
            Logger.e("ImageProcessingUtil", "YUV rotation acquireLatestImage failure");
            return null;
        }
        final SingleCloseImageProxy singleCloseImageProxy = new SingleCloseImageProxy(acquireLatestImage);
        singleCloseImageProxy.addOnImageCloseListener((ForwardingImageProxy.OnImageCloseListener)new O08000(acquireLatestImage, imageProxy));
        return singleCloseImageProxy;
    }
    
    @Nullable
    @RequiresApi(23)
    private static Result rotateYUVInternal(@NonNull final ImageProxy imageProxy, @NonNull final ImageWriter imageWriter, @NonNull final ByteBuffer byteBuffer, @NonNull final ByteBuffer byteBuffer2, @NonNull final ByteBuffer byteBuffer3, final int n) {
        final int width = imageProxy.getWidth();
        final int height = imageProxy.getHeight();
        final int rowStride = imageProxy.getPlanes()[0].getRowStride();
        final int rowStride2 = imageProxy.getPlanes()[1].getRowStride();
        final int rowStride3 = imageProxy.getPlanes()[2].getRowStride();
        final int pixelStride = imageProxy.getPlanes()[1].getPixelStride();
        final Image dequeueInputImage = ImageWriterCompat.dequeueInputImage(imageWriter);
        if (dequeueInputImage == null) {
            return Result.ERROR_CONVERSION;
        }
        if (nativeRotateYUV(imageProxy.getPlanes()[0].getBuffer(), rowStride, imageProxy.getPlanes()[1].getBuffer(), rowStride2, imageProxy.getPlanes()[2].getBuffer(), rowStride3, pixelStride, dequeueInputImage.getPlanes()[0].getBuffer(), dequeueInputImage.getPlanes()[0].getRowStride(), dequeueInputImage.getPlanes()[0].getPixelStride(), dequeueInputImage.getPlanes()[1].getBuffer(), dequeueInputImage.getPlanes()[1].getRowStride(), dequeueInputImage.getPlanes()[1].getPixelStride(), dequeueInputImage.getPlanes()[2].getBuffer(), dequeueInputImage.getPlanes()[2].getRowStride(), dequeueInputImage.getPlanes()[2].getPixelStride(), byteBuffer, byteBuffer2, byteBuffer3, width, height, n) != 0) {
            return Result.ERROR_CONVERSION;
        }
        ImageWriterCompat.queueInputImage(imageWriter, dequeueInputImage);
        return Result.SUCCESS;
    }
    
    enum Result
    {
        private static final Result[] $VALUES;
        
        ERROR_CONVERSION, 
        SUCCESS, 
        UNKNOWN;
    }
}

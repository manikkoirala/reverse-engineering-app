// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.NonNull;

final class AutoValue_SurfaceOutput_Event extends Event
{
    private final int eventCode;
    private final SurfaceOutput surfaceOutput;
    
    AutoValue_SurfaceOutput_Event(final int eventCode, final SurfaceOutput surfaceOutput) {
        this.eventCode = eventCode;
        if (surfaceOutput != null) {
            this.surfaceOutput = surfaceOutput;
            return;
        }
        throw new NullPointerException("Null surfaceOutput");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof Event) {
            final Event event = (Event)o;
            if (this.eventCode != event.getEventCode() || !this.surfaceOutput.equals(event.getSurfaceOutput())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int getEventCode() {
        return this.eventCode;
    }
    
    @NonNull
    @Override
    public SurfaceOutput getSurfaceOutput() {
        return this.surfaceOutput;
    }
    
    @Override
    public int hashCode() {
        return (this.eventCode ^ 0xF4243) * 1000003 ^ this.surfaceOutput.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Event{eventCode=");
        sb.append(this.eventCode);
        sb.append(", surfaceOutput=");
        sb.append(this.surfaceOutput);
        sb.append("}");
        return sb.toString();
    }
}

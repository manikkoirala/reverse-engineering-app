// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.nio.ByteBuffer;
import androidx.annotation.Nullable;
import android.graphics.Rect;
import android.media.Image$Plane;
import android.graphics.Matrix;
import androidx.camera.core.impl.TagBundle;
import androidx.annotation.NonNull;
import android.media.Image;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
final class AndroidImageProxy implements ImageProxy
{
    private final Image mImage;
    private final ImageInfo mImageInfo;
    private final PlaneProxy[] mPlanes;
    
    AndroidImageProxy(@NonNull final Image mImage) {
        this.mImage = mImage;
        final Image$Plane[] planes = mImage.getPlanes();
        if (planes != null) {
            this.mPlanes = new PlaneProxy[planes.length];
            for (int i = 0; i < planes.length; ++i) {
                this.mPlanes[i] = new PlaneProxy(planes[i]);
            }
        }
        else {
            this.mPlanes = new PlaneProxy[0];
        }
        this.mImageInfo = ImmutableImageInfo.create(TagBundle.emptyBundle(), mImage.getTimestamp(), 0, new Matrix());
    }
    
    @Override
    public void close() {
        this.mImage.close();
    }
    
    @NonNull
    @Override
    public Rect getCropRect() {
        return this.mImage.getCropRect();
    }
    
    @Override
    public int getFormat() {
        return this.mImage.getFormat();
    }
    
    @Override
    public int getHeight() {
        return this.mImage.getHeight();
    }
    
    @ExperimentalGetImage
    @Override
    public Image getImage() {
        return this.mImage;
    }
    
    @NonNull
    @Override
    public ImageInfo getImageInfo() {
        return this.mImageInfo;
    }
    
    @NonNull
    @Override
    public ImageProxy.PlaneProxy[] getPlanes() {
        return this.mPlanes;
    }
    
    @Override
    public int getWidth() {
        return this.mImage.getWidth();
    }
    
    @Override
    public void setCropRect(@Nullable final Rect cropRect) {
        this.mImage.setCropRect(cropRect);
    }
    
    private static final class PlaneProxy implements ImageProxy.PlaneProxy
    {
        private final Image$Plane mPlane;
        
        PlaneProxy(final Image$Plane mPlane) {
            this.mPlane = mPlane;
        }
        
        @NonNull
        @Override
        public ByteBuffer getBuffer() {
            return this.mPlane.getBuffer();
        }
        
        @Override
        public int getPixelStride() {
            return this.mPlane.getPixelStride();
        }
        
        @Override
        public int getRowStride() {
            return this.mPlane.getRowStride();
        }
    }
}

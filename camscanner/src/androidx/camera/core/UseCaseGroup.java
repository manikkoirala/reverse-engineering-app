// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.core.util.Preconditions;
import java.util.ArrayList;
import androidx.annotation.RestrictTo;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class UseCaseGroup
{
    @NonNull
    private final List<CameraEffect> mEffects;
    @NonNull
    private final List<UseCase> mUseCases;
    @Nullable
    private final ViewPort mViewPort;
    
    UseCaseGroup(@Nullable final ViewPort mViewPort, @NonNull final List<UseCase> mUseCases, @NonNull final List<CameraEffect> mEffects) {
        this.mViewPort = mViewPort;
        this.mUseCases = mUseCases;
        this.mEffects = mEffects;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public List<CameraEffect> getEffects() {
        return this.mEffects;
    }
    
    @NonNull
    public List<UseCase> getUseCases() {
        return this.mUseCases;
    }
    
    @Nullable
    public ViewPort getViewPort() {
        return this.mViewPort;
    }
    
    public static final class Builder
    {
        private final List<CameraEffect> mEffects;
        private final List<UseCase> mUseCases;
        private ViewPort mViewPort;
        
        public Builder() {
            this.mUseCases = new ArrayList<UseCase>();
            this.mEffects = new ArrayList<CameraEffect>();
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder addEffect(@NonNull final CameraEffect cameraEffect) {
            this.mEffects.add(cameraEffect);
            return this;
        }
        
        @NonNull
        public Builder addUseCase(@NonNull final UseCase useCase) {
            this.mUseCases.add(useCase);
            return this;
        }
        
        @NonNull
        public UseCaseGroup build() {
            Preconditions.checkArgument(this.mUseCases.isEmpty() ^ true, (Object)"UseCase must not be empty.");
            return new UseCaseGroup(this.mViewPort, this.mUseCases, this.mEffects);
        }
        
        @NonNull
        public Builder setViewPort(@NonNull final ViewPort mViewPort) {
            this.mViewPort = mViewPort;
            return this;
        }
    }
}

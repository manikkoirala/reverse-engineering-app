// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import android.os.BaseBundle;
import java.util.concurrent.RejectedExecutionException;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.content.ContentResolver;
import androidx.camera.core.impl.ConfigProvider;
import androidx.camera.core.internal.UseCaseEventConfig;
import java.util.UUID;
import android.util.Pair;
import java.util.List;
import androidx.camera.core.impl.CaptureConfig;
import androidx.camera.core.impl.OptionsBundle;
import androidx.camera.core.impl.MutableConfig;
import androidx.camera.core.internal.TargetConfig;
import androidx.camera.core.impl.MutableOptionsBundle;
import androidx.camera.core.internal.ThreadConfig;
import androidx.camera.core.impl.ImageOutputConfig;
import java.io.FileDescriptor;
import androidx.annotation.DoNotInline;
import android.location.Location;
import androidx.camera.core.impl.CameraInternal;
import androidx.core.util.Preconditions;
import android.os.Looper;
import java.util.Objects;
import androidx.camera.core.impl.ImmediateSurface;
import android.media.MediaCrypto;
import androidx.camera.core.impl.Config;
import androidx.camera.core.impl.Oooo8o0\u3007;
import androidx.camera.core.impl.UseCaseConfigFactory;
import android.media.MediaCodec$CodecException;
import android.os.Bundle;
import android.media.CamcorderProfile;
import androidx.annotation.UiThread;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.Executor;
import java.io.File;
import androidx.camera.core.internal.utils.VideoUtil;
import java.io.IOException;
import android.content.ContentValues;
import android.os.Build$VERSION;
import java.nio.ByteBuffer;
import android.media.MediaFormat;
import androidx.annotation.RequiresPermission;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import android.util.Size;
import androidx.camera.core.impl.UseCaseConfig;
import androidx.camera.core.impl.VideoCaptureConfig;
import androidx.camera.core.impl.SessionConfig;
import android.net.Uri;
import com.google.common.util.concurrent.ListenableFuture;
import android.os.ParcelFileDescriptor;
import android.media.MediaMuxer;
import androidx.annotation.VisibleForTesting;
import java.util.concurrent.atomic.AtomicBoolean;
import androidx.camera.core.impl.DeferrableSurface;
import android.view.Surface;
import androidx.annotation.GuardedBy;
import androidx.annotation.Nullable;
import android.media.AudioRecord;
import android.os.HandlerThread;
import android.os.Handler;
import androidx.annotation.NonNull;
import android.media.MediaCodec;
import android.media.MediaCodec$BufferInfo;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;

@Deprecated
@RequiresApi(21)
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public final class VideoCapture extends UseCase
{
    private static final String AUDIO_MIME_TYPE = "audio/mp4a-latm";
    private static final int[] CamcorderQuality;
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final Defaults DEFAULT_CONFIG;
    private static final int DEQUE_TIMEOUT_USEC = 10000;
    public static final int ERROR_ENCODER = 1;
    public static final int ERROR_FILE_IO = 4;
    public static final int ERROR_INVALID_CAMERA = 5;
    public static final int ERROR_MUXER = 2;
    public static final int ERROR_RECORDING_IN_PROGRESS = 3;
    public static final int ERROR_RECORDING_TOO_SHORT = 6;
    public static final int ERROR_UNKNOWN = 0;
    private static final String TAG = "VideoCapture";
    private static final String VIDEO_MIME_TYPE = "video/avc";
    private int mAudioBitRate;
    private final MediaCodec$BufferInfo mAudioBufferInfo;
    private volatile int mAudioBufferSize;
    private int mAudioChannelCount;
    @NonNull
    private MediaCodec mAudioEncoder;
    private Handler mAudioHandler;
    private HandlerThread mAudioHandlerThread;
    @Nullable
    private volatile AudioRecord mAudioRecorder;
    private int mAudioSampleRate;
    @GuardedBy("mMuxerLock")
    private int mAudioTrackIndex;
    Surface mCameraSurface;
    private DeferrableSurface mDeferrableSurface;
    private final AtomicBoolean mEndOfAudioStreamSignal;
    private final AtomicBoolean mEndOfAudioVideoSignal;
    private final AtomicBoolean mEndOfVideoStreamSignal;
    private final AtomicBoolean mIsAudioEnabled;
    @VisibleForTesting(otherwise = 2)
    public final AtomicBoolean mIsFirstAudioSampleWrite;
    @VisibleForTesting(otherwise = 2)
    public final AtomicBoolean mIsFirstVideoKeyFrameWrite;
    private volatile boolean mIsRecording;
    @GuardedBy("mMuxerLock")
    private MediaMuxer mMuxer;
    private final Object mMuxerLock;
    private final AtomicBoolean mMuxerStarted;
    private volatile ParcelFileDescriptor mParcelFileDescriptor;
    @Nullable
    private ListenableFuture<Void> mRecordingFuture;
    volatile Uri mSavedVideoUri;
    @NonNull
    private SessionConfig.Builder mSessionConfigBuilder;
    private final MediaCodec$BufferInfo mVideoBufferInfo;
    @NonNull
    MediaCodec mVideoEncoder;
    @Nullable
    private Throwable mVideoEncoderErrorMessage;
    private VideoEncoderInitStatus mVideoEncoderInitStatus;
    private Handler mVideoHandler;
    private HandlerThread mVideoHandlerThread;
    @GuardedBy("mMuxerLock")
    private int mVideoTrackIndex;
    
    static {
        DEFAULT_CONFIG = new Defaults();
        CamcorderQuality = new int[] { 8, 6, 5, 4 };
    }
    
    VideoCapture(@NonNull final VideoCaptureConfig videoCaptureConfig) {
        super(videoCaptureConfig);
        this.mVideoBufferInfo = new MediaCodec$BufferInfo();
        this.mMuxerLock = new Object();
        this.mEndOfVideoStreamSignal = new AtomicBoolean(true);
        this.mEndOfAudioStreamSignal = new AtomicBoolean(true);
        this.mEndOfAudioVideoSignal = new AtomicBoolean(true);
        this.mAudioBufferInfo = new MediaCodec$BufferInfo();
        this.mIsFirstVideoKeyFrameWrite = new AtomicBoolean(false);
        this.mIsFirstAudioSampleWrite = new AtomicBoolean(false);
        this.mRecordingFuture = null;
        this.mSessionConfigBuilder = new SessionConfig.Builder();
        this.mMuxerStarted = new AtomicBoolean(false);
        this.mIsRecording = false;
        this.mIsAudioEnabled = new AtomicBoolean(true);
        this.mVideoEncoderInitStatus = VideoEncoderInitStatus.VIDEO_ENCODER_INIT_STATUS_UNINITIALIZED;
    }
    
    @RequiresPermission("android.permission.RECORD_AUDIO")
    private AudioRecord autoConfigAudioRecordSource(final VideoCaptureConfig videoCaptureConfig) {
        int i;
        if (this.mAudioChannelCount == 1) {
            i = 16;
        }
        else {
            i = 12;
        }
        try {
            int n;
            if ((n = AudioRecord.getMinBufferSize(this.mAudioSampleRate, i, 2)) <= 0) {
                n = videoCaptureConfig.getAudioMinBufferSize();
            }
            final AudioRecord audioRecord = new AudioRecord(5, this.mAudioSampleRate, i, 2, n * 2);
            if (audioRecord.getState() == 1) {
                this.mAudioBufferSize = n;
                final StringBuilder sb = new StringBuilder();
                sb.append("source: 5 audioSampleRate: ");
                sb.append(this.mAudioSampleRate);
                sb.append(" channelConfig: ");
                sb.append(i);
                sb.append(" audioFormat: ");
                sb.append(2);
                sb.append(" bufferSize: ");
                sb.append(n);
                Logger.i("VideoCapture", sb.toString());
                return audioRecord;
            }
        }
        catch (final Exception ex) {
            Logger.e("VideoCapture", "Exception, keep trying.", ex);
        }
        return null;
    }
    
    private MediaFormat createAudioMediaFormat() {
        final MediaFormat audioFormat = MediaFormat.createAudioFormat("audio/mp4a-latm", this.mAudioSampleRate, this.mAudioChannelCount);
        audioFormat.setInteger("aac-profile", 2);
        audioFormat.setInteger("bitrate", this.mAudioBitRate);
        return audioFormat;
    }
    
    private static MediaFormat createVideoMediaFormat(final VideoCaptureConfig videoCaptureConfig, final Size size) {
        final MediaFormat videoFormat = MediaFormat.createVideoFormat("video/avc", size.getWidth(), size.getHeight());
        videoFormat.setInteger("color-format", 2130708361);
        videoFormat.setInteger("bitrate", videoCaptureConfig.getBitRate());
        videoFormat.setInteger("frame-rate", videoCaptureConfig.getVideoFrameRate());
        videoFormat.setInteger("i-frame-interval", videoCaptureConfig.getIFrameInterval());
        return videoFormat;
    }
    
    private ByteBuffer getInputBuffer(final MediaCodec mediaCodec, final int n) {
        return mediaCodec.getInputBuffer(n);
    }
    
    private ByteBuffer getOutputBuffer(final MediaCodec mediaCodec, final int n) {
        return mediaCodec.getOutputBuffer(n);
    }
    
    @NonNull
    private MediaMuxer initMediaMuxer(@NonNull final OutputFileOptions outputFileOptions) throws IOException {
        if (outputFileOptions.isSavingToFile()) {
            final File file = outputFileOptions.getFile();
            this.mSavedVideoUri = Uri.fromFile(outputFileOptions.getFile());
            return new MediaMuxer(file.getAbsolutePath(), 0);
        }
        if (outputFileOptions.isSavingToFileDescriptor()) {
            if (Build$VERSION.SDK_INT >= 26) {
                return Api26Impl.createMediaMuxer(outputFileOptions.getFileDescriptor(), 0);
            }
            throw new IllegalArgumentException("Using a FileDescriptor to record a video is only supported for Android 8.0 or above.");
        }
        else {
            if (!outputFileOptions.isSavingToMediaStore()) {
                throw new IllegalArgumentException("The OutputFileOptions should assign before recording");
            }
            ContentValues contentValues;
            if (outputFileOptions.getContentValues() != null) {
                contentValues = new ContentValues(outputFileOptions.getContentValues());
            }
            else {
                contentValues = new ContentValues();
            }
            this.mSavedVideoUri = outputFileOptions.getContentResolver().insert(outputFileOptions.getSaveCollection(), contentValues);
            if (this.mSavedVideoUri == null) {
                throw new IOException("Invalid Uri!");
            }
        }
        try {
            MediaMuxer mediaMuxer;
            if (Build$VERSION.SDK_INT < 26) {
                final String absolutePathFromUri = VideoUtil.getAbsolutePathFromUri(outputFileOptions.getContentResolver(), this.mSavedVideoUri);
                final StringBuilder sb = new StringBuilder();
                sb.append("Saved Location Path: ");
                sb.append(absolutePathFromUri);
                Logger.i("VideoCapture", sb.toString());
                mediaMuxer = new MediaMuxer(absolutePathFromUri, 0);
            }
            else {
                this.mParcelFileDescriptor = outputFileOptions.getContentResolver().openFileDescriptor(this.mSavedVideoUri, "rw");
                mediaMuxer = Api26Impl.createMediaMuxer(this.mParcelFileDescriptor.getFileDescriptor(), 0);
            }
            return mediaMuxer;
        }
        catch (final IOException ex) {
            this.mSavedVideoUri = null;
            throw ex;
        }
        throw new IOException("Invalid Uri!");
    }
    
    private void releaseAudioInputResource() {
        this.mAudioHandlerThread.quitSafely();
        final MediaCodec mAudioEncoder = this.mAudioEncoder;
        if (mAudioEncoder != null) {
            mAudioEncoder.release();
            this.mAudioEncoder = null;
        }
        if (this.mAudioRecorder != null) {
            this.mAudioRecorder.release();
            this.mAudioRecorder = null;
        }
    }
    
    @UiThread
    private void releaseCameraSurface(final boolean b) {
        final DeferrableSurface mDeferrableSurface = this.mDeferrableSurface;
        if (mDeferrableSurface == null) {
            return;
        }
        final MediaCodec mVideoEncoder = this.mVideoEncoder;
        mDeferrableSurface.close();
        this.mDeferrableSurface.getTerminationFuture().addListener((Runnable)new O\u3007Oooo\u3007\u3007(b, mVideoEncoder), (Executor)CameraXExecutors.mainThreadExecutor());
        if (b) {
            this.mVideoEncoder = null;
        }
        this.mCameraSurface = null;
        this.mDeferrableSurface = null;
    }
    
    private void releaseResources() {
        this.mVideoHandlerThread.quitSafely();
        this.releaseAudioInputResource();
        if (this.mCameraSurface != null) {
            this.releaseCameraSurface(true);
        }
    }
    
    private boolean removeRecordingResultIfNoVideoKeyFrameArrived(@NonNull final OutputFileOptions outputFileOptions) {
        final StringBuilder sb = new StringBuilder();
        sb.append("check Recording Result First Video Key Frame Write: ");
        sb.append(this.mIsFirstVideoKeyFrameWrite.get());
        Logger.i("VideoCapture", sb.toString());
        boolean b;
        if (!this.mIsFirstVideoKeyFrameWrite.get()) {
            Logger.i("VideoCapture", "The recording result has no key frame.");
            b = false;
        }
        else {
            b = true;
        }
        if (outputFileOptions.isSavingToFile()) {
            final File file = outputFileOptions.getFile();
            if (!b) {
                Logger.i("VideoCapture", "Delete file.");
                file.delete();
            }
        }
        else if (outputFileOptions.isSavingToMediaStore() && !b) {
            Logger.i("VideoCapture", "Delete file.");
            if (this.mSavedVideoUri != null) {
                outputFileOptions.getContentResolver().delete(this.mSavedVideoUri, (String)null, (String[])null);
            }
        }
        return b;
    }
    
    private void setAudioParametersByCamcorderProfile(final Size size, final String s) {
        final boolean b = false;
        int n2 = 0;
        Label_0130: {
            try {
                final int[] camcorderQuality = VideoCapture.CamcorderQuality;
                final int length = camcorderQuality.length;
                int n = 0;
                CamcorderProfile value;
                while (true) {
                    n2 = (b ? 1 : 0);
                    if (n >= length) {
                        break Label_0130;
                    }
                    final int n3 = camcorderQuality[n];
                    if (CamcorderProfile.hasProfile(Integer.parseInt(s), n3)) {
                        value = CamcorderProfile.get(Integer.parseInt(s), n3);
                        if (size.getWidth() == value.videoFrameWidth && size.getHeight() == value.videoFrameHeight) {
                            break;
                        }
                    }
                    ++n;
                }
                this.mAudioChannelCount = value.audioChannels;
                this.mAudioSampleRate = value.audioSampleRate;
                this.mAudioBitRate = value.audioBitRate;
                n2 = 1;
            }
            catch (final NumberFormatException ex) {
                Logger.i("VideoCapture", "The camera Id is not an integer because the camera may be a removable device. Use the default values for the audio related settings.");
                n2 = (b ? 1 : 0);
            }
        }
        if (n2 == 0) {
            final VideoCaptureConfig videoCaptureConfig = (VideoCaptureConfig)this.getCurrentConfig();
            this.mAudioChannelCount = videoCaptureConfig.getAudioChannelCount();
            this.mAudioSampleRate = videoCaptureConfig.getAudioSampleRate();
            this.mAudioBitRate = videoCaptureConfig.getAudioBitRate();
        }
    }
    
    private boolean writeAudioEncodedBuffer(final int n) {
        final ByteBuffer outputBuffer = this.getOutputBuffer(this.mAudioEncoder, n);
        outputBuffer.position(this.mAudioBufferInfo.offset);
        final boolean value = this.mMuxerStarted.get();
        boolean b = true;
        Label_0287: {
            if (value) {
                try {
                    final MediaCodec$BufferInfo mAudioBufferInfo = this.mAudioBufferInfo;
                    if (mAudioBufferInfo.size > 0 && mAudioBufferInfo.presentationTimeUs > 0L) {
                        synchronized (this.mMuxerLock) {
                            if (!this.mIsFirstAudioSampleWrite.get()) {
                                Logger.i("VideoCapture", "First audio sample written.");
                                this.mIsFirstAudioSampleWrite.set(true);
                            }
                            this.mMuxer.writeSampleData(this.mAudioTrackIndex, outputBuffer, this.mAudioBufferInfo);
                            break Label_0287;
                        }
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("mAudioBufferInfo size: ");
                    sb.append(this.mAudioBufferInfo.size);
                    sb.append(" presentationTimeUs: ");
                    sb.append(this.mAudioBufferInfo.presentationTimeUs);
                    Logger.i("VideoCapture", sb.toString());
                }
                catch (final Exception ex) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("audio error:size=");
                    sb2.append(this.mAudioBufferInfo.size);
                    sb2.append("/offset=");
                    sb2.append(this.mAudioBufferInfo.offset);
                    sb2.append("/timeUs=");
                    sb2.append(this.mAudioBufferInfo.presentationTimeUs);
                    Logger.e("VideoCapture", sb2.toString());
                    ex.printStackTrace();
                }
            }
        }
        this.mAudioEncoder.releaseOutputBuffer(n, false);
        if ((this.mAudioBufferInfo.flags & 0x4) == 0x0) {
            b = false;
        }
        return b;
    }
    
    private boolean writeVideoEncodedBuffer(final int n) {
        boolean b = false;
        if (n < 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Output buffer should not have negative index: ");
            sb.append(n);
            Logger.e("VideoCapture", sb.toString());
            return false;
        }
        final ByteBuffer outputBuffer = this.mVideoEncoder.getOutputBuffer(n);
        if (outputBuffer == null) {
            Logger.d("VideoCapture", "OutputBuffer was null.");
            return false;
        }
        Label_0295: {
            if (this.mMuxerStarted.get()) {
                final MediaCodec$BufferInfo mVideoBufferInfo = this.mVideoBufferInfo;
                if (mVideoBufferInfo.size > 0) {
                    outputBuffer.position(mVideoBufferInfo.offset);
                    final MediaCodec$BufferInfo mVideoBufferInfo2 = this.mVideoBufferInfo;
                    outputBuffer.limit(mVideoBufferInfo2.offset + mVideoBufferInfo2.size);
                    this.mVideoBufferInfo.presentationTimeUs = System.nanoTime() / 1000L;
                    synchronized (this.mMuxerLock) {
                        if (!this.mIsFirstVideoKeyFrameWrite.get()) {
                            if ((this.mVideoBufferInfo.flags & 0x1) != 0x0) {
                                Logger.i("VideoCapture", "First video key frame written.");
                                this.mIsFirstVideoKeyFrameWrite.set(true);
                            }
                            else {
                                final Bundle parameters = new Bundle();
                                ((BaseBundle)parameters).putInt("request-sync", 0);
                                this.mVideoEncoder.setParameters(parameters);
                            }
                        }
                        this.mMuxer.writeSampleData(this.mVideoTrackIndex, outputBuffer, this.mVideoBufferInfo);
                        break Label_0295;
                    }
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("mVideoBufferInfo.size <= 0, index ");
                sb2.append(n);
                Logger.i("VideoCapture", sb2.toString());
            }
        }
        this.mVideoEncoder.releaseOutputBuffer(n, false);
        if ((this.mVideoBufferInfo.flags & 0x4) != 0x0) {
            b = true;
        }
        return b;
    }
    
    boolean audioEncode(final OnVideoSavedCallback onVideoSavedCallback) {
        long n = 0L;
        int n2 = 0;
    Block_10:
        while (n2 == 0 && this.mIsRecording) {
            if (this.mEndOfAudioStreamSignal.get()) {
                this.mEndOfAudioStreamSignal.set(false);
                this.mIsRecording = false;
            }
            if (this.mAudioEncoder != null && this.mAudioRecorder != null) {
                int n3;
                long lng;
                try {
                    final int dequeueInputBuffer = this.mAudioEncoder.dequeueInputBuffer(-1L);
                    n3 = n2;
                    lng = n;
                    if (dequeueInputBuffer >= 0) {
                        Object o = this.getInputBuffer(this.mAudioEncoder, dequeueInputBuffer);
                        ((ByteBuffer)o).clear();
                        final int read = this.mAudioRecorder.read((ByteBuffer)o, this.mAudioBufferSize);
                        n3 = n2;
                        lng = n;
                        if (read > 0) {
                            o = this.mAudioEncoder;
                            final long n4 = System.nanoTime() / 1000L;
                            int n5;
                            if (this.mIsRecording) {
                                n5 = 0;
                            }
                            else {
                                n5 = 4;
                            }
                            ((MediaCodec)o).queueInputBuffer(dequeueInputBuffer, 0, read, n4, n5);
                            n3 = n2;
                            lng = n;
                        }
                    }
                }
                catch (final IllegalStateException ex) {
                    final Object o = new StringBuilder();
                    ((StringBuilder)o).append("audio dequeueInputBuffer IllegalStateException ");
                    ((StringBuilder)o).append(ex.getMessage());
                    Logger.i("VideoCapture", ((StringBuilder)o).toString());
                    n3 = n2;
                    lng = n;
                }
                catch (final MediaCodec$CodecException o) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("audio dequeueInputBuffer CodecException ");
                    sb.append(((Throwable)o).getMessage());
                    Logger.i("VideoCapture", sb.toString());
                    lng = n;
                    n3 = n2;
                }
                while (true) {
                    final int dequeueOutputBuffer = this.mAudioEncoder.dequeueOutputBuffer(this.mAudioBufferInfo, 0L);
                    Label_0493: {
                        if (dequeueOutputBuffer != -2) {
                            int writeAudioEncodedBuffer = n3;
                            long presentationTimeUs = lng;
                            if (dequeueOutputBuffer == -1) {
                                break Label_0493;
                            }
                            if (this.mAudioBufferInfo.presentationTimeUs > lng) {
                                writeAudioEncodedBuffer = (this.writeAudioEncodedBuffer(dequeueOutputBuffer) ? 1 : 0);
                                presentationTimeUs = this.mAudioBufferInfo.presentationTimeUs;
                                break Label_0493;
                            }
                            final Object o = new StringBuilder();
                            ((StringBuilder)o).append("Drops frame, current frame's timestamp ");
                            ((StringBuilder)o).append(this.mAudioBufferInfo.presentationTimeUs);
                            ((StringBuilder)o).append(" is earlier that last frame ");
                            ((StringBuilder)o).append(lng);
                            Logger.w("VideoCapture", ((StringBuilder)o).toString());
                            this.mAudioEncoder.releaseOutputBuffer(dequeueOutputBuffer, false);
                            writeAudioEncodedBuffer = n3;
                            presentationTimeUs = lng;
                            break Label_0493;
                        }
                        synchronized (this.mMuxerLock) {
                            final int addTrack = this.mMuxer.addTrack(this.mAudioEncoder.getOutputFormat());
                            this.mAudioTrackIndex = addTrack;
                            if (addTrack >= 0 && this.mVideoTrackIndex >= 0) {
                                Logger.i("VideoCapture", "MediaMuxer start on audio encoder thread.");
                                this.mMuxer.start();
                                this.mMuxerStarted.set(true);
                            }
                            monitorexit(this.mMuxerLock);
                            final long presentationTimeUs = lng;
                            final int writeAudioEncodedBuffer = n3;
                            n2 = writeAudioEncodedBuffer;
                            n = presentationTimeUs;
                            if (dequeueOutputBuffer < 0) {
                                break;
                            }
                            n3 = writeAudioEncodedBuffer;
                            lng = presentationTimeUs;
                            if (writeAudioEncodedBuffer != 0) {
                                n2 = writeAudioEncodedBuffer;
                                n = presentationTimeUs;
                                break;
                            }
                            continue;
                        }
                    }
                    break Block_10;
                }
            }
        }
        try {
            Logger.i("VideoCapture", "audioRecorder stop");
            this.mAudioRecorder.stop();
        }
        catch (final IllegalStateException ex2) {
            onVideoSavedCallback.onError(1, "Audio recorder stop failed!", ex2);
        }
        try {
            this.mAudioEncoder.stop();
        }
        catch (final IllegalStateException ex3) {
            onVideoSavedCallback.onError(1, "Audio encoder stop failed!", ex3);
        }
        Logger.i("VideoCapture", "Audio encode thread end");
        this.mEndOfVideoStreamSignal.set(true);
        return false;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    public UseCaseConfig<?> getDefaultConfig(final boolean b, @NonNull final UseCaseConfigFactory useCaseConfigFactory) {
        Config config2;
        final Config config = config2 = useCaseConfigFactory.getConfig(UseCaseConfigFactory.CaptureType.VIDEO_CAPTURE, 1);
        if (b) {
            config2 = Oooo8o0\u3007.\u3007o00\u3007\u3007Oo(config, VideoCapture.DEFAULT_CONFIG.getConfig());
        }
        Object useCaseConfig;
        if (config2 == null) {
            useCaseConfig = null;
        }
        else {
            useCaseConfig = this.getUseCaseConfigBuilder(config2).getUseCaseConfig();
        }
        return (UseCaseConfig<?>)useCaseConfig;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    public UseCaseConfig.Builder<?, ?, ?> getUseCaseConfigBuilder(@NonNull final Config config) {
        return Builder.fromConfig(config);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    public void onAttached() {
        this.mVideoHandlerThread = new HandlerThread("CameraX-video encoding thread");
        this.mAudioHandlerThread = new HandlerThread("CameraX-audio encoding thread");
        ((Thread)this.mVideoHandlerThread).start();
        this.mVideoHandler = new Handler(this.mVideoHandlerThread.getLooper());
        ((Thread)this.mAudioHandlerThread).start();
        this.mAudioHandler = new Handler(this.mAudioHandlerThread.getLooper());
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    public void onDetached() {
        this.stopRecording();
        final ListenableFuture<Void> mRecordingFuture = this.mRecordingFuture;
        if (mRecordingFuture != null) {
            mRecordingFuture.addListener((Runnable)new O\u3007OO(this), (Executor)CameraXExecutors.mainThreadExecutor());
        }
        else {
            this.releaseResources();
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @UiThread
    @Override
    public void onStateDetached() {
        this.stopRecording();
    }
    
    @NonNull
    @RequiresPermission("android.permission.RECORD_AUDIO")
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    protected Size onSuggestedResolutionUpdated(@NonNull final Size size) {
        if (this.mCameraSurface != null) {
            this.mVideoEncoder.stop();
            this.mVideoEncoder.release();
            this.mAudioEncoder.stop();
            this.mAudioEncoder.release();
            this.releaseCameraSurface(false);
        }
        try {
            this.mVideoEncoder = MediaCodec.createEncoderByType("video/avc");
            this.mAudioEncoder = MediaCodec.createEncoderByType("audio/mp4a-latm");
            this.setupEncoder(this.getCameraId(), size);
            this.notifyActive();
            return size;
        }
        catch (final IOException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unable to create MediaCodec due to: ");
            sb.append(ex.getCause());
            throw new IllegalStateException(sb.toString());
        }
    }
    
    public void setTargetRotation(final int targetRotationInternal) {
        this.setTargetRotationInternal(targetRotationInternal);
    }
    
    @RequiresPermission("android.permission.RECORD_AUDIO")
    @UiThread
    void setupEncoder(@NonNull final String ex, @NonNull final Size size) {
        final VideoCaptureConfig videoCaptureConfig = (VideoCaptureConfig)this.getCurrentConfig();
        this.mVideoEncoder.reset();
        this.mVideoEncoderInitStatus = VideoEncoderInitStatus.VIDEO_ENCODER_INIT_STATUS_UNINITIALIZED;
        try {
            this.mVideoEncoder.configure(createVideoMediaFormat(videoCaptureConfig, size), (Surface)null, (MediaCrypto)null, 1);
            if (this.mCameraSurface != null) {
                this.releaseCameraSurface(false);
            }
            final Surface inputSurface = this.mVideoEncoder.createInputSurface();
            this.mCameraSurface = inputSurface;
            this.mSessionConfigBuilder = SessionConfig.Builder.createFrom(videoCaptureConfig);
            final DeferrableSurface mDeferrableSurface = this.mDeferrableSurface;
            if (mDeferrableSurface != null) {
                mDeferrableSurface.close();
            }
            final ImmediateSurface mDeferrableSurface2 = new ImmediateSurface(this.mCameraSurface, size, this.getImageFormat());
            this.mDeferrableSurface = mDeferrableSurface2;
            final ListenableFuture<Void> terminationFuture = mDeferrableSurface2.getTerminationFuture();
            Objects.requireNonNull(inputSurface);
            terminationFuture.addListener((Runnable)new O\u300708(inputSurface), (Executor)CameraXExecutors.mainThreadExecutor());
            this.mSessionConfigBuilder.addNonRepeatingSurface(this.mDeferrableSurface);
            this.mSessionConfigBuilder.addErrorListener(new SessionConfig.ErrorListener(this, ex, size) {
                final VideoCapture this$0;
                final String val$cameraId;
                final Size val$resolution;
                
                @RequiresPermission("android.permission.RECORD_AUDIO")
                @Override
                public void onError(@NonNull final SessionConfig sessionConfig, @NonNull final SessionError sessionError) {
                    if (this.this$0.isCurrentCamera(this.val$cameraId)) {
                        this.this$0.setupEncoder(this.val$cameraId, this.val$resolution);
                        this.this$0.notifyReset();
                    }
                }
            });
            this.updateSessionConfig(this.mSessionConfigBuilder.build());
            this.mIsAudioEnabled.set(true);
            this.setAudioParametersByCamcorderProfile(size, (String)ex);
            this.mAudioEncoder.reset();
            this.mAudioEncoder.configure(this.createAudioMediaFormat(), (Surface)null, (MediaCrypto)null, 1);
            if (this.mAudioRecorder != null) {
                this.mAudioRecorder.release();
            }
            this.mAudioRecorder = this.autoConfigAudioRecordSource(videoCaptureConfig);
            if (this.mAudioRecorder == null) {
                Logger.e("VideoCapture", "AudioRecord object cannot initialized correctly!");
                this.mIsAudioEnabled.set(false);
            }
            synchronized (this.mMuxerLock) {
                this.mVideoTrackIndex = -1;
                this.mAudioTrackIndex = -1;
                monitorexit(this.mMuxerLock);
                this.mIsRecording = false;
            }
        }
        catch (final IllegalStateException ex) {
            goto Label_0305;
        }
        catch (final IllegalArgumentException ex2) {}
        catch (final MediaCodec$CodecException mVideoEncoderErrorMessage) {
            if (Build$VERSION.SDK_INT >= 23) {
                final int codecExceptionErrorCode = Api23Impl.getCodecExceptionErrorCode(mVideoEncoderErrorMessage);
                final String diagnosticInfo = mVideoEncoderErrorMessage.getDiagnosticInfo();
                if (codecExceptionErrorCode == 1100) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("CodecException: code: ");
                    sb.append(codecExceptionErrorCode);
                    sb.append(" diagnostic: ");
                    sb.append(diagnosticInfo);
                    Logger.i("VideoCapture", sb.toString());
                    this.mVideoEncoderInitStatus = VideoEncoderInitStatus.VIDEO_ENCODER_INIT_STATUS_INSUFFICIENT_RESOURCE;
                }
                else if (codecExceptionErrorCode == 1101) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("CodecException: code: ");
                    sb2.append(codecExceptionErrorCode);
                    sb2.append(" diagnostic: ");
                    sb2.append(diagnosticInfo);
                    Logger.i("VideoCapture", sb2.toString());
                    this.mVideoEncoderInitStatus = VideoEncoderInitStatus.VIDEO_ENCODER_INIT_STATUS_RESOURCE_RECLAIMED;
                }
            }
            else {
                this.mVideoEncoderInitStatus = VideoEncoderInitStatus.VIDEO_ENCODER_INIT_STATUS_INITIALIZED_FAILED;
            }
            this.mVideoEncoderErrorMessage = (Throwable)mVideoEncoderErrorMessage;
        }
    }
    
    @RequiresPermission("android.permission.RECORD_AUDIO")
    public void startRecording(@NonNull final OutputFileOptions outputFileOptions, @NonNull Executor executor, @NonNull OnVideoSavedCallback onVideoSavedCallback) {
        if (Looper.getMainLooper() != Looper.myLooper()) {
            CameraXExecutors.mainThreadExecutor().execute(new Ooo8\u3007\u3007(this, outputFileOptions, executor, onVideoSavedCallback));
            return;
        }
        Logger.i("VideoCapture", "startRecording");
        this.mIsFirstVideoKeyFrameWrite.set(false);
        this.mIsFirstAudioSampleWrite.set(false);
        executor = (Executor)new VideoSavedListenerWrapper(executor, onVideoSavedCallback);
        final CameraInternal camera = this.getCamera();
        if (camera == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Not bound to a Camera [");
            sb.append(this);
            sb.append("]");
            ((OnVideoSavedCallback)executor).onError(5, sb.toString(), null);
            return;
        }
        final VideoEncoderInitStatus mVideoEncoderInitStatus = this.mVideoEncoderInitStatus;
        if (mVideoEncoderInitStatus != VideoEncoderInitStatus.VIDEO_ENCODER_INIT_STATUS_INSUFFICIENT_RESOURCE && mVideoEncoderInitStatus != VideoEncoderInitStatus.VIDEO_ENCODER_INIT_STATUS_INITIALIZED_FAILED) {
            if (mVideoEncoderInitStatus != VideoEncoderInitStatus.VIDEO_ENCODER_INIT_STATUS_RESOURCE_RECLAIMED) {
                if (!this.mEndOfAudioVideoSignal.get()) {
                    ((OnVideoSavedCallback)executor).onError(3, "It is still in video recording!", null);
                    return;
                }
                if (this.mIsAudioEnabled.get()) {
                    try {
                        if (this.mAudioRecorder.getState() == 1) {
                            this.mAudioRecorder.startRecording();
                        }
                    }
                    catch (final IllegalStateException ex) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("AudioRecorder cannot start recording, disable audio.");
                        sb2.append(ex.getMessage());
                        Logger.i("VideoCapture", sb2.toString());
                        this.mIsAudioEnabled.set(false);
                        this.releaseAudioInputResource();
                    }
                    if (this.mAudioRecorder.getRecordingState() != 3) {
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("AudioRecorder startRecording failed - incorrect state: ");
                        sb3.append(this.mAudioRecorder.getRecordingState());
                        Logger.i("VideoCapture", sb3.toString());
                        this.mIsAudioEnabled.set(false);
                        this.releaseAudioInputResource();
                    }
                }
                final AtomicReference atomicReference = new AtomicReference();
                this.mRecordingFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new \u3007000O0(atomicReference));
                onVideoSavedCallback = (OnVideoSavedCallback)Preconditions.checkNotNull(atomicReference.get());
                this.mRecordingFuture.addListener((Runnable)new ooo0\u3007O88O(this), (Executor)CameraXExecutors.mainThreadExecutor());
                try {
                    Logger.i("VideoCapture", "videoEncoder start");
                    this.mVideoEncoder.start();
                    if (this.mIsAudioEnabled.get()) {
                        Logger.i("VideoCapture", "audioEncoder start");
                        this.mAudioEncoder.start();
                    }
                    try {
                        Object o = this.mMuxerLock;
                        synchronized (o) {
                            Preconditions.checkNotNull(this.mMuxer = this.initMediaMuxer(outputFileOptions));
                            this.mMuxer.setOrientationHint(this.getRelativeRotation(camera));
                            final Metadata metadata = outputFileOptions.getMetadata();
                            if (metadata != null) {
                                final Location location = metadata.location;
                                if (location != null) {
                                    this.mMuxer.setLocation((float)location.getLatitude(), (float)metadata.location.getLongitude());
                                }
                            }
                            monitorexit(o);
                            this.mEndOfVideoStreamSignal.set(false);
                            this.mEndOfAudioStreamSignal.set(false);
                            this.mEndOfAudioVideoSignal.set(false);
                            this.mIsRecording = true;
                            this.mSessionConfigBuilder.clearSurfaces();
                            this.mSessionConfigBuilder.addSurface(this.mDeferrableSurface);
                            this.updateSessionConfig(this.mSessionConfigBuilder.build());
                            this.notifyUpdated();
                            if (this.mIsAudioEnabled.get()) {
                                this.mAudioHandler.post((Runnable)new OOo8o\u3007O(this, (OnVideoSavedCallback)executor));
                            }
                            final String cameraId = this.getCameraId();
                            o = this.getAttachedSurfaceResolution();
                            this.mVideoHandler.post((Runnable)new O880oOO08(this, (OnVideoSavedCallback)executor, cameraId, (Size)o, outputFileOptions, (CallbackToFutureAdapter.Completer)onVideoSavedCallback));
                            return;
                        }
                    }
                    catch (final IOException ex2) {
                        ((CallbackToFutureAdapter.Completer<Object>)onVideoSavedCallback).set(null);
                        ((OnVideoSavedCallback)executor).onError(2, "MediaMuxer creation failed!", ex2);
                        return;
                    }
                }
                catch (final IllegalStateException ex3) {
                    ((CallbackToFutureAdapter.Completer<Object>)onVideoSavedCallback).set(null);
                    ((OnVideoSavedCallback)executor).onError(1, "Audio/Video encoder start fail", ex3);
                    return;
                }
            }
        }
        ((OnVideoSavedCallback)executor).onError(1, "Video encoder initialization failed before start recording ", this.mVideoEncoderErrorMessage);
    }
    
    public void stopRecording() {
        if (Looper.getMainLooper() != Looper.myLooper()) {
            CameraXExecutors.mainThreadExecutor().execute(new OOo0O(this));
            return;
        }
        Logger.i("VideoCapture", "stopRecording");
        this.mSessionConfigBuilder.clearSurfaces();
        this.mSessionConfigBuilder.addNonRepeatingSurface(this.mDeferrableSurface);
        this.updateSessionConfig(this.mSessionConfigBuilder.build());
        this.notifyUpdated();
        if (this.mIsRecording) {
            if (this.mIsAudioEnabled.get()) {
                this.mEndOfAudioStreamSignal.set(true);
            }
            else {
                this.mEndOfVideoStreamSignal.set(true);
            }
        }
    }
    
    boolean videoEncode(@NonNull final OnVideoSavedCallback onVideoSavedCallback, @NonNull final String s, @NonNull final Size size, @NonNull final OutputFileOptions outputFileOptions) {
        boolean writeVideoEncodedBuffer = false;
        int n = 0;
        while (!writeVideoEncodedBuffer && n == 0) {
            if (this.mEndOfVideoStreamSignal.get()) {
                this.mVideoEncoder.signalEndOfInputStream();
                this.mEndOfVideoStreamSignal.set(false);
            }
            final int dequeueOutputBuffer = this.mVideoEncoder.dequeueOutputBuffer(this.mVideoBufferInfo, 10000L);
            if (dequeueOutputBuffer == -2) {
                if (this.mMuxerStarted.get()) {
                    onVideoSavedCallback.onError(1, "Unexpected change in video encoding format.", null);
                    n = (true ? 1 : 0);
                }
                synchronized (this.mMuxerLock) {
                    this.mVideoTrackIndex = this.mMuxer.addTrack(this.mVideoEncoder.getOutputFormat());
                    if ((this.mIsAudioEnabled.get() || this.mAudioTrackIndex < 0 || this.mVideoTrackIndex < 0) && (this.mIsAudioEnabled.get() || this.mVideoTrackIndex < 0)) {
                        continue;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("MediaMuxer started on video encode thread and audio enabled: ");
                    sb.append(this.mIsAudioEnabled);
                    Logger.i("VideoCapture", sb.toString());
                    this.mMuxer.start();
                    this.mMuxerStarted.set(true);
                    continue;
                }
                break;
            }
            if (dequeueOutputBuffer == -1) {
                continue;
            }
            writeVideoEncodedBuffer = this.writeVideoEncodedBuffer(dequeueOutputBuffer);
        }
        try {
            Logger.i("VideoCapture", "videoEncoder stop");
            this.mVideoEncoder.stop();
        }
        catch (final IllegalStateException ex) {
            onVideoSavedCallback.onError(1, "Video encoder stop failed!", ex);
            n = (true ? 1 : 0);
        }
        Label_0455: {
            Label_0452: {
                try {
                    synchronized (this.mMuxerLock) {
                        if (this.mMuxer != null) {
                            if (this.mMuxerStarted.get()) {
                                Logger.i("VideoCapture", "Muxer already started");
                                this.mMuxer.stop();
                            }
                            this.mMuxer.release();
                            this.mMuxer = null;
                        }
                        monitorexit(this.mMuxerLock);
                        if (!this.removeRecordingResultIfNoVideoKeyFrameArrived(outputFileOptions)) {
                            onVideoSavedCallback.onError(6, "The file has no video key frame.", null);
                            break Label_0452;
                        }
                        break Label_0455;
                    }
                }
                catch (final IllegalStateException ex2) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("muxer stop IllegalStateException: ");
                    sb2.append(System.currentTimeMillis());
                    Logger.i("VideoCapture", sb2.toString());
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("muxer stop exception, mIsFirstVideoKeyFrameWrite: ");
                    sb3.append(this.mIsFirstVideoKeyFrameWrite.get());
                    Logger.i("VideoCapture", sb3.toString());
                    if (this.mIsFirstVideoKeyFrameWrite.get()) {
                        onVideoSavedCallback.onError(2, "Muxer stop failed!", ex2);
                    }
                    else {
                        onVideoSavedCallback.onError(6, "The file has no video key frame.", null);
                    }
                }
            }
            n = (true ? 1 : 0);
        }
        boolean b = n != 0;
        if (this.mParcelFileDescriptor != null) {
            try {
                this.mParcelFileDescriptor.close();
                this.mParcelFileDescriptor = null;
                b = (n != 0);
            }
            catch (final IOException ex3) {
                onVideoSavedCallback.onError(2, "File descriptor close failed!", ex3);
                b = true;
            }
        }
        this.mMuxerStarted.set(false);
        this.mEndOfAudioVideoSignal.set(true);
        this.mIsFirstVideoKeyFrameWrite.set(false);
        Logger.i("VideoCapture", "Video encode thread end.");
        return b;
    }
    
    @RequiresApi(23)
    private static class Api23Impl
    {
        @DoNotInline
        static int getCodecExceptionErrorCode(final MediaCodec$CodecException ex) {
            return O0OO8\u30070.\u3007080(ex);
        }
    }
    
    @RequiresApi(26)
    private static class Api26Impl
    {
        @DoNotInline
        @NonNull
        static MediaMuxer createMediaMuxer(@NonNull final FileDescriptor fileDescriptor, final int n) throws IOException {
            return new MediaMuxer(fileDescriptor, n);
        }
    }
    
    public static final class Builder implements UseCaseConfig.Builder<VideoCapture, VideoCaptureConfig, Builder>, ImageOutputConfig.Builder<Builder>, ThreadConfig.Builder<Builder>
    {
        private final MutableOptionsBundle mMutableConfig;
        
        public Builder() {
            this(MutableOptionsBundle.create());
        }
        
        private Builder(@NonNull final MutableOptionsBundle mMutableConfig) {
            this.mMutableConfig = mMutableConfig;
            final Class<?> obj = mMutableConfig.retrieveOption(TargetConfig.OPTION_TARGET_CLASS, null);
            if (obj != null && !obj.equals(VideoCapture.class)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid target class configuration for ");
                sb.append(this);
                sb.append(": ");
                sb.append(obj);
                throw new IllegalArgumentException(sb.toString());
            }
            this.setTargetClass(VideoCapture.class);
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        static Builder fromConfig(@NonNull final Config config) {
            return new Builder(MutableOptionsBundle.from(config));
        }
        
        @NonNull
        public static Builder fromConfig(@NonNull final VideoCaptureConfig videoCaptureConfig) {
            return new Builder(MutableOptionsBundle.from(videoCaptureConfig));
        }
        
        @NonNull
        @Override
        public VideoCapture build() {
            if (this.getMutableConfig().retrieveOption(ImageOutputConfig.OPTION_TARGET_ASPECT_RATIO, null) != null && this.getMutableConfig().retrieveOption(ImageOutputConfig.OPTION_TARGET_RESOLUTION, null) != null) {
                throw new IllegalArgumentException("Cannot use both setTargetResolution and setTargetAspectRatio on the same config.");
            }
            return new VideoCapture(this.getUseCaseConfig());
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        @Override
        public MutableConfig getMutableConfig() {
            return this.mMutableConfig;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public VideoCaptureConfig getUseCaseConfig() {
            return new VideoCaptureConfig(OptionsBundle.from(this.mMutableConfig));
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setAudioBitRate(final int i) {
            this.getMutableConfig().insertOption(VideoCaptureConfig.OPTION_AUDIO_BIT_RATE, i);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setAudioChannelCount(final int i) {
            this.getMutableConfig().insertOption(VideoCaptureConfig.OPTION_AUDIO_CHANNEL_COUNT, i);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setAudioMinBufferSize(final int i) {
            this.getMutableConfig().insertOption(VideoCaptureConfig.OPTION_AUDIO_MIN_BUFFER_SIZE, i);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setAudioSampleRate(final int i) {
            this.getMutableConfig().insertOption(VideoCaptureConfig.OPTION_AUDIO_SAMPLE_RATE, i);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setBackgroundExecutor(@NonNull final Executor executor) {
            this.getMutableConfig().insertOption(ThreadConfig.OPTION_BACKGROUND_EXECUTOR, executor);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setBitRate(final int i) {
            this.getMutableConfig().insertOption(VideoCaptureConfig.OPTION_BIT_RATE, i);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY })
        public Builder setCameraSelector(@NonNull final CameraSelector cameraSelector) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_CAMERA_SELECTOR, cameraSelector);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setCaptureOptionUnpacker(@NonNull final CaptureConfig.OptionUnpacker optionUnpacker) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_CAPTURE_CONFIG_UNPACKER, optionUnpacker);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setDefaultCaptureConfig(@NonNull final CaptureConfig captureConfig) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_DEFAULT_CAPTURE_CONFIG, captureConfig);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setDefaultResolution(@NonNull final Size size) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_DEFAULT_RESOLUTION, size);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setDefaultSessionConfig(@NonNull final SessionConfig sessionConfig) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_DEFAULT_SESSION_CONFIG, sessionConfig);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setIFrameInterval(final int i) {
            this.getMutableConfig().insertOption(VideoCaptureConfig.OPTION_INTRA_FRAME_INTERVAL, i);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setMaxResolution(@NonNull final Size size) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_MAX_RESOLUTION, size);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setSessionOptionUnpacker(@NonNull final SessionConfig.OptionUnpacker optionUnpacker) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_SESSION_CONFIG_UNPACKER, optionUnpacker);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setSupportedResolutions(@NonNull final List<Pair<Integer, Size[]>> list) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_SUPPORTED_RESOLUTIONS, list);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setSurfaceOccupancyPriority(final int i) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_SURFACE_OCCUPANCY_PRIORITY, i);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setTargetAspectRatio(final int i) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_TARGET_ASPECT_RATIO, i);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setTargetClass(@NonNull final Class<VideoCapture> clazz) {
            this.getMutableConfig().insertOption(TargetConfig.OPTION_TARGET_CLASS, clazz);
            if (this.getMutableConfig().retrieveOption(TargetConfig.OPTION_TARGET_NAME, null) == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append(clazz.getCanonicalName());
                sb.append("-");
                sb.append(UUID.randomUUID());
                this.setTargetName(sb.toString());
            }
            return this;
        }
        
        @NonNull
        public Builder setTargetName(@NonNull final String s) {
            this.getMutableConfig().insertOption(TargetConfig.OPTION_TARGET_NAME, s);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setTargetResolution(@NonNull final Size size) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_TARGET_RESOLUTION, size);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setTargetRotation(final int i) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_TARGET_ROTATION, i);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setUseCaseEventCallback(@NonNull final EventCallback eventCallback) {
            this.getMutableConfig().insertOption(UseCaseEventConfig.OPTION_USE_CASE_EVENT_CALLBACK, eventCallback);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setVideoFrameRate(final int i) {
            this.getMutableConfig().insertOption(VideoCaptureConfig.OPTION_VIDEO_FRAME_RATE, i);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setZslDisabled(final boolean b) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_ZSL_DISABLED, b);
            return this;
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final class Defaults implements ConfigProvider<VideoCaptureConfig>
    {
        private static final int DEFAULT_ASPECT_RATIO = 1;
        private static final int DEFAULT_AUDIO_BIT_RATE = 64000;
        private static final int DEFAULT_AUDIO_CHANNEL_COUNT = 1;
        private static final int DEFAULT_AUDIO_MIN_BUFFER_SIZE = 1024;
        private static final int DEFAULT_AUDIO_SAMPLE_RATE = 8000;
        private static final int DEFAULT_BIT_RATE = 8388608;
        private static final VideoCaptureConfig DEFAULT_CONFIG;
        private static final int DEFAULT_INTRA_FRAME_INTERVAL = 1;
        private static final Size DEFAULT_MAX_RESOLUTION;
        private static final int DEFAULT_SURFACE_OCCUPANCY_PRIORITY = 3;
        private static final int DEFAULT_VIDEO_FRAME_RATE = 30;
        
        static {
            DEFAULT_CONFIG = new VideoCapture.Builder().setVideoFrameRate(30).setBitRate(8388608).setIFrameInterval(1).setAudioBitRate(64000).setAudioSampleRate(8000).setAudioChannelCount(1).setAudioMinBufferSize(1024).setMaxResolution(DEFAULT_MAX_RESOLUTION = new Size(1920, 1080)).setSurfaceOccupancyPriority(3).setTargetAspectRatio(1).getUseCaseConfig();
        }
        
        @NonNull
        @Override
        public VideoCaptureConfig getConfig() {
            return Defaults.DEFAULT_CONFIG;
        }
    }
    
    public static final class Metadata
    {
        @Nullable
        public Location location;
    }
    
    public interface OnVideoSavedCallback
    {
        void onError(final int p0, @NonNull final String p1, @Nullable final Throwable p2);
        
        void onVideoSaved(@NonNull final OutputFileResults p0);
    }
    
    public static final class OutputFileOptions
    {
        private static final Metadata EMPTY_METADATA;
        @Nullable
        private final ContentResolver mContentResolver;
        @Nullable
        private final ContentValues mContentValues;
        @Nullable
        private final File mFile;
        @Nullable
        private final FileDescriptor mFileDescriptor;
        @Nullable
        private final Metadata mMetadata;
        @Nullable
        private final Uri mSaveCollection;
        
        static {
            EMPTY_METADATA = new Metadata();
        }
        
        OutputFileOptions(@Nullable final File mFile, @Nullable final FileDescriptor mFileDescriptor, @Nullable final ContentResolver mContentResolver, @Nullable final Uri mSaveCollection, @Nullable final ContentValues mContentValues, @Nullable final Metadata metadata) {
            this.mFile = mFile;
            this.mFileDescriptor = mFileDescriptor;
            this.mContentResolver = mContentResolver;
            this.mSaveCollection = mSaveCollection;
            this.mContentValues = mContentValues;
            Metadata empty_METADATA = metadata;
            if (metadata == null) {
                empty_METADATA = OutputFileOptions.EMPTY_METADATA;
            }
            this.mMetadata = empty_METADATA;
        }
        
        @Nullable
        ContentResolver getContentResolver() {
            return this.mContentResolver;
        }
        
        @Nullable
        ContentValues getContentValues() {
            return this.mContentValues;
        }
        
        @Nullable
        File getFile() {
            return this.mFile;
        }
        
        @Nullable
        FileDescriptor getFileDescriptor() {
            return this.mFileDescriptor;
        }
        
        @Nullable
        Metadata getMetadata() {
            return this.mMetadata;
        }
        
        @Nullable
        Uri getSaveCollection() {
            return this.mSaveCollection;
        }
        
        boolean isSavingToFile() {
            return this.getFile() != null;
        }
        
        boolean isSavingToFileDescriptor() {
            return this.getFileDescriptor() != null;
        }
        
        boolean isSavingToMediaStore() {
            return this.getSaveCollection() != null && this.getContentResolver() != null && this.getContentValues() != null;
        }
        
        public static final class Builder
        {
            @Nullable
            private ContentResolver mContentResolver;
            @Nullable
            private ContentValues mContentValues;
            @Nullable
            private File mFile;
            @Nullable
            private FileDescriptor mFileDescriptor;
            @Nullable
            private Metadata mMetadata;
            @Nullable
            private Uri mSaveCollection;
            
            public Builder(@NonNull final ContentResolver mContentResolver, @NonNull final Uri mSaveCollection, @NonNull final ContentValues mContentValues) {
                this.mContentResolver = mContentResolver;
                this.mSaveCollection = mSaveCollection;
                this.mContentValues = mContentValues;
            }
            
            public Builder(@NonNull final File mFile) {
                this.mFile = mFile;
            }
            
            public Builder(@NonNull final FileDescriptor mFileDescriptor) {
                Preconditions.checkArgument(Build$VERSION.SDK_INT >= 26, (Object)"Using a FileDescriptor to record a video is only supported for Android 8.0 or above.");
                this.mFileDescriptor = mFileDescriptor;
            }
            
            @NonNull
            public OutputFileOptions build() {
                return new OutputFileOptions(this.mFile, this.mFileDescriptor, this.mContentResolver, this.mSaveCollection, this.mContentValues, this.mMetadata);
            }
            
            @NonNull
            public Builder setMetadata(@NonNull final Metadata mMetadata) {
                this.mMetadata = mMetadata;
                return this;
            }
        }
    }
    
    public static class OutputFileResults
    {
        @Nullable
        private Uri mSavedUri;
        
        OutputFileResults(@Nullable final Uri mSavedUri) {
            this.mSavedUri = mSavedUri;
        }
        
        @Nullable
        public Uri getSavedUri() {
            return this.mSavedUri;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public @interface VideoCaptureError {
    }
    
    enum VideoEncoderInitStatus
    {
        private static final VideoEncoderInitStatus[] $VALUES;
        
        VIDEO_ENCODER_INIT_STATUS_INITIALIZED_FAILED, 
        VIDEO_ENCODER_INIT_STATUS_INSUFFICIENT_RESOURCE, 
        VIDEO_ENCODER_INIT_STATUS_RESOURCE_RECLAIMED, 
        VIDEO_ENCODER_INIT_STATUS_UNINITIALIZED;
    }
    
    private static final class VideoSavedListenerWrapper implements OnVideoSavedCallback
    {
        @NonNull
        Executor mExecutor;
        @NonNull
        OnVideoSavedCallback mOnVideoSavedCallback;
        
        VideoSavedListenerWrapper(@NonNull final Executor mExecutor, @NonNull final OnVideoSavedCallback mOnVideoSavedCallback) {
            this.mExecutor = mExecutor;
            this.mOnVideoSavedCallback = mOnVideoSavedCallback;
        }
        
        @Override
        public void onError(final int n, @NonNull final String s, @Nullable final Throwable t) {
            try {
                this.mExecutor.execute(new O0\u3007OO8(this, n, s, t));
            }
            catch (final RejectedExecutionException ex) {
                Logger.e("VideoCapture", "Unable to post to the supplied executor.");
            }
        }
        
        @Override
        public void onVideoSaved(@NonNull final OutputFileResults outputFileResults) {
            try {
                this.mExecutor.execute(new ooO\u300700O(this, outputFileResults));
            }
            catch (final RejectedExecutionException ex) {
                Logger.e("VideoCapture", "Unable to post to the supplied executor.");
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.RestrictTo;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class AspectRatio
{
    public static final int RATIO_16_9 = 1;
    public static final int RATIO_4_3 = 0;
    
    private AspectRatio() {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public @interface Ratio {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.nio.ByteBuffer;
import android.annotation.SuppressLint;
import androidx.annotation.Nullable;
import android.media.Image;
import androidx.annotation.NonNull;
import android.graphics.Rect;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface ImageProxy extends AutoCloseable
{
    void close();
    
    @NonNull
    Rect getCropRect();
    
    int getFormat();
    
    int getHeight();
    
    @Nullable
    @ExperimentalGetImage
    Image getImage();
    
    @NonNull
    ImageInfo getImageInfo();
    
    @SuppressLint({ "ArrayReturn" })
    @NonNull
    PlaneProxy[] getPlanes();
    
    int getWidth();
    
    void setCropRect(@Nullable final Rect p0);
    
    public interface PlaneProxy
    {
        @NonNull
        ByteBuffer getBuffer();
        
        int getPixelStride();
        
        int getRowStride();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class ImageCaptureException extends Exception
{
    private final int mImageCaptureError;
    
    public ImageCaptureException(final int mImageCaptureError, @NonNull final String message, @Nullable final Throwable cause) {
        super(message, cause);
        this.mImageCaptureError = mImageCaptureError;
    }
    
    public int getImageCaptureError() {
        return this.mImageCaptureError;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.camera.core.impl.ReadableConfig;
import androidx.camera.core.impl.ConfigProvider;
import androidx.camera.core.internal.UseCaseEventConfig;
import java.util.UUID;
import android.util.Pair;
import java.util.List;
import androidx.camera.core.impl.CaptureConfig;
import androidx.camera.core.impl.OptionsBundle;
import androidx.camera.core.impl.MutableConfig;
import androidx.camera.core.internal.TargetConfig;
import androidx.camera.core.impl.MutableOptionsBundle;
import androidx.camera.core.internal.ThreadConfig;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.graphics.Rect;
import android.graphics.Matrix;
import androidx.camera.core.impl.ImageOutputConfig;
import androidx.camera.core.impl.Quirk;
import androidx.camera.core.internal.compat.quirk.OnePixelShiftQuirk;
import androidx.camera.core.impl.CameraInfoInternal;
import androidx.camera.core.impl.Config;
import androidx.camera.core.impl.Oooo8o0\u3007;
import androidx.camera.core.impl.UseCaseConfigFactory;
import androidx.camera.core.impl.ImmediateSurface;
import androidx.camera.core.impl.ImageReaderProxy;
import androidx.core.util.Preconditions;
import androidx.camera.core.impl.utils.Threads;
import java.util.concurrent.Executor;
import androidx.camera.core.impl.SessionConfig;
import android.util.Size;
import androidx.camera.core.impl.CameraInternal;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.impl.UseCaseConfig;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.ImageAnalysisConfig;
import androidx.annotation.GuardedBy;
import androidx.annotation.Nullable;
import androidx.camera.core.impl.DeferrableSurface;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class ImageAnalysis extends UseCase
{
    public static final int COORDINATE_SYSTEM_ORIGINAL = 0;
    private static final int DEFAULT_BACKPRESSURE_STRATEGY = 0;
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final Defaults DEFAULT_CONFIG;
    private static final int DEFAULT_IMAGE_QUEUE_DEPTH = 6;
    private static final Boolean DEFAULT_ONE_PIXEL_SHIFT_ENABLED;
    private static final int DEFAULT_OUTPUT_IMAGE_FORMAT = 1;
    private static final boolean DEFAULT_OUTPUT_IMAGE_ROTATION_ENABLED = false;
    private static final int NON_BLOCKING_IMAGE_DEPTH = 4;
    public static final int OUTPUT_IMAGE_FORMAT_RGBA_8888 = 2;
    public static final int OUTPUT_IMAGE_FORMAT_YUV_420_888 = 1;
    public static final int STRATEGY_BLOCK_PRODUCER = 1;
    public static final int STRATEGY_KEEP_ONLY_LATEST = 0;
    private static final String TAG = "ImageAnalysis";
    private final Object mAnalysisLock;
    @Nullable
    private DeferrableSurface mDeferrableSurface;
    final ImageAnalysisAbstractAnalyzer mImageAnalysisAbstractAnalyzer;
    @GuardedBy("mAnalysisLock")
    private Analyzer mSubscribedAnalyzer;
    
    static {
        DEFAULT_CONFIG = new Defaults();
        DEFAULT_ONE_PIXEL_SHIFT_ENABLED = null;
    }
    
    ImageAnalysis(@NonNull final ImageAnalysisConfig imageAnalysisConfig) {
        super(imageAnalysisConfig);
        this.mAnalysisLock = new Object();
        if (((ImageAnalysisConfig)this.getCurrentConfig()).getBackpressureStrategy(0) == 1) {
            this.mImageAnalysisAbstractAnalyzer = new ImageAnalysisBlockingAnalyzer();
        }
        else {
            this.mImageAnalysisAbstractAnalyzer = new ImageAnalysisNonBlockingAnalyzer(imageAnalysisConfig.getBackgroundExecutor(CameraXExecutors.highPriorityExecutor()));
        }
        this.mImageAnalysisAbstractAnalyzer.setOutputImageFormat(this.getOutputImageFormat());
        this.mImageAnalysisAbstractAnalyzer.setOutputImageRotationEnabled(this.isOutputImageRotationEnabled());
    }
    
    private boolean isFlipWH(@NonNull final CameraInternal cameraInternal) {
        final boolean outputImageRotationEnabled = this.isOutputImageRotationEnabled();
        boolean b = false;
        if (outputImageRotationEnabled) {
            b = b;
            if (this.getRelativeRotation(cameraInternal) % 180 != 0) {
                b = true;
            }
        }
        return b;
    }
    
    private void tryUpdateRelativeRotation() {
        final CameraInternal camera = this.getCamera();
        if (camera != null) {
            this.mImageAnalysisAbstractAnalyzer.setRelativeRotation(this.getRelativeRotation(camera));
        }
    }
    
    public void clearAnalyzer() {
        synchronized (this.mAnalysisLock) {
            this.mImageAnalysisAbstractAnalyzer.setAnalyzer(null, null);
            if (this.mSubscribedAnalyzer != null) {
                this.notifyInactive();
            }
            this.mSubscribedAnalyzer = null;
        }
    }
    
    void clearPipeline() {
        Threads.checkMainThread();
        final DeferrableSurface mDeferrableSurface = this.mDeferrableSurface;
        if (mDeferrableSurface != null) {
            mDeferrableSurface.close();
            this.mDeferrableSurface = null;
        }
    }
    
    SessionConfig.Builder createPipeline(@NonNull final String s, @NonNull final ImageAnalysisConfig imageAnalysisConfig, @NonNull final Size size) {
        Threads.checkMainThread();
        final Executor executor = Preconditions.checkNotNull(imageAnalysisConfig.getBackgroundExecutor(CameraXExecutors.highPriorityExecutor()));
        final int backpressureStrategy = this.getBackpressureStrategy();
        final int n = 1;
        int imageQueueDepth;
        if (backpressureStrategy == 1) {
            imageQueueDepth = this.getImageQueueDepth();
        }
        else {
            imageQueueDepth = 4;
        }
        SafeCloseImageReaderProxy safeCloseImageReaderProxy;
        if (imageAnalysisConfig.getImageReaderProxyProvider() != null) {
            safeCloseImageReaderProxy = new SafeCloseImageReaderProxy(imageAnalysisConfig.getImageReaderProxyProvider().newInstance(size.getWidth(), size.getHeight(), this.getImageFormat(), imageQueueDepth, 0L));
        }
        else {
            safeCloseImageReaderProxy = new SafeCloseImageReaderProxy(ImageReaderProxys.createIsolatedReader(size.getWidth(), size.getHeight(), this.getImageFormat(), imageQueueDepth));
        }
        final boolean b = this.getCamera() != null && this.isFlipWH(this.getCamera());
        int n2;
        if (b) {
            n2 = size.getHeight();
        }
        else {
            n2 = size.getWidth();
        }
        int n3;
        if (b) {
            n3 = size.getWidth();
        }
        else {
            n3 = size.getHeight();
        }
        int n4;
        if (this.getOutputImageFormat() == 2) {
            n4 = 1;
        }
        else {
            n4 = 35;
        }
        final boolean b2 = this.getImageFormat() == 35 && this.getOutputImageFormat() == 2;
        int n5 = 0;
        Label_0275: {
            if (this.getImageFormat() == 35) {
                if (this.getCamera() != null) {
                    n5 = n;
                    if (this.getRelativeRotation(this.getCamera()) != 0) {
                        break Label_0275;
                    }
                }
                if (Boolean.TRUE.equals(this.getOnePixelShiftEnabled())) {
                    n5 = n;
                    break Label_0275;
                }
            }
            n5 = 0;
        }
        SafeCloseImageReaderProxy processedImageReaderProxy;
        if (!b2 && n5 == 0) {
            processedImageReaderProxy = null;
        }
        else {
            processedImageReaderProxy = new SafeCloseImageReaderProxy(ImageReaderProxys.createIsolatedReader(n2, n3, n4, safeCloseImageReaderProxy.getMaxImages()));
        }
        if (processedImageReaderProxy != null) {
            this.mImageAnalysisAbstractAnalyzer.setProcessedImageReaderProxy(processedImageReaderProxy);
        }
        this.tryUpdateRelativeRotation();
        safeCloseImageReaderProxy.setOnImageAvailableListener(this.mImageAnalysisAbstractAnalyzer, executor);
        final SessionConfig.Builder from = SessionConfig.Builder.createFrom(imageAnalysisConfig);
        final DeferrableSurface mDeferrableSurface = this.mDeferrableSurface;
        if (mDeferrableSurface != null) {
            mDeferrableSurface.close();
        }
        final ImmediateSurface mDeferrableSurface2 = new ImmediateSurface(safeCloseImageReaderProxy.getSurface(), size, this.getImageFormat());
        this.mDeferrableSurface = mDeferrableSurface2;
        mDeferrableSurface2.getTerminationFuture().addListener((Runnable)new \u3007\u30078O0\u30078(safeCloseImageReaderProxy, processedImageReaderProxy), (Executor)CameraXExecutors.mainThreadExecutor());
        from.addSurface(this.mDeferrableSurface);
        from.addErrorListener(new \u30070\u3007O0088o(this, s, imageAnalysisConfig, size));
        return from;
    }
    
    @Nullable
    @ExperimentalUseCaseApi
    public Executor getBackgroundExecutor() {
        return ((ImageAnalysisConfig)this.getCurrentConfig()).getBackgroundExecutor(null);
    }
    
    public int getBackpressureStrategy() {
        return ((ImageAnalysisConfig)this.getCurrentConfig()).getBackpressureStrategy(0);
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    public UseCaseConfig<?> getDefaultConfig(final boolean b, @NonNull final UseCaseConfigFactory useCaseConfigFactory) {
        Config config2;
        final Config config = config2 = useCaseConfigFactory.getConfig(UseCaseConfigFactory.CaptureType.IMAGE_ANALYSIS, 1);
        if (b) {
            config2 = Oooo8o0\u3007.\u3007o00\u3007\u3007Oo(config, ImageAnalysis.DEFAULT_CONFIG.getConfig());
        }
        Object useCaseConfig;
        if (config2 == null) {
            useCaseConfig = null;
        }
        else {
            useCaseConfig = this.getUseCaseConfigBuilder(config2).getUseCaseConfig();
        }
        return (UseCaseConfig<?>)useCaseConfig;
    }
    
    public int getImageQueueDepth() {
        return ((ImageAnalysisConfig)this.getCurrentConfig()).getImageQueueDepth(6);
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public Boolean getOnePixelShiftEnabled() {
        return ((ImageAnalysisConfig)this.getCurrentConfig()).getOnePixelShiftEnabled(ImageAnalysis.DEFAULT_ONE_PIXEL_SHIFT_ENABLED);
    }
    
    public int getOutputImageFormat() {
        return ((ImageAnalysisConfig)this.getCurrentConfig()).getOutputImageFormat(1);
    }
    
    @Nullable
    @Override
    public ResolutionInfo getResolutionInfo() {
        return super.getResolutionInfo();
    }
    
    public int getTargetRotation() {
        return this.getTargetRotationInternal();
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    public UseCaseConfig.Builder<?, ?, ?> getUseCaseConfigBuilder(@NonNull final Config config) {
        return Builder.fromConfig(config);
    }
    
    public boolean isOutputImageRotationEnabled() {
        return ((ImageAnalysisConfig)this.getCurrentConfig()).isOutputImageRotationEnabled(Boolean.FALSE);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    public void onAttached() {
        this.mImageAnalysisAbstractAnalyzer.attach();
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    public void onDetached() {
        this.clearPipeline();
        this.mImageAnalysisAbstractAnalyzer.detach();
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    protected UseCaseConfig<?> onMergeConfig(@NonNull final CameraInfoInternal cameraInfoInternal, @NonNull final UseCaseConfig.Builder<?, ?, ?> builder) {
        final Boolean onePixelShiftEnabled = this.getOnePixelShiftEnabled();
        boolean onePixelShiftEnabled2 = cameraInfoInternal.getCameraQuirks().contains(OnePixelShiftQuirk.class);
        final ImageAnalysisAbstractAnalyzer mImageAnalysisAbstractAnalyzer = this.mImageAnalysisAbstractAnalyzer;
        if (onePixelShiftEnabled != null) {
            onePixelShiftEnabled2 = onePixelShiftEnabled;
        }
        mImageAnalysisAbstractAnalyzer.setOnePixelShiftEnabled(onePixelShiftEnabled2);
        Object o = this.mAnalysisLock;
        synchronized (o) {
            final Analyzer mSubscribedAnalyzer = this.mSubscribedAnalyzer;
            Size defaultTargetResolution;
            if (mSubscribedAnalyzer != null) {
                defaultTargetResolution = mSubscribedAnalyzer.getDefaultTargetResolution();
            }
            else {
                defaultTargetResolution = null;
            }
            monitorexit(o);
            if (defaultTargetResolution != null) {
                final Object useCaseConfig = builder.getUseCaseConfig();
                o = ImageOutputConfig.OPTION_TARGET_RESOLUTION;
                if (!((ReadableConfig)useCaseConfig).containsOption((Config.Option<?>)o)) {
                    builder.getMutableConfig().insertOption((Config.Option<Size>)o, defaultTargetResolution);
                }
            }
            return (UseCaseConfig<?>)builder.getUseCaseConfig();
        }
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    protected Size onSuggestedResolutionUpdated(@NonNull final Size size) {
        this.updateSessionConfig(this.createPipeline(this.getCameraId(), (ImageAnalysisConfig)this.getCurrentConfig(), size).build());
        return size;
    }
    
    public void setAnalyzer(@NonNull final Executor executor, @NonNull final Analyzer mSubscribedAnalyzer) {
        synchronized (this.mAnalysisLock) {
            this.mImageAnalysisAbstractAnalyzer.setAnalyzer(executor, new OoO8(mSubscribedAnalyzer));
            if (this.mSubscribedAnalyzer == null) {
                this.notifyActive();
            }
            this.mSubscribedAnalyzer = mSubscribedAnalyzer;
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    public void setSensorToBufferTransformMatrix(@NonNull final Matrix matrix) {
        super.setSensorToBufferTransformMatrix(matrix);
        this.mImageAnalysisAbstractAnalyzer.setSensorToBufferTransformMatrix(matrix);
    }
    
    public void setTargetRotation(final int targetRotationInternal) {
        if (this.setTargetRotationInternal(targetRotationInternal)) {
            this.tryUpdateRelativeRotation();
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    public void setViewPortCropRect(@NonNull final Rect rect) {
        super.setViewPortCropRect(rect);
        this.mImageAnalysisAbstractAnalyzer.setViewPortCropRect(rect);
    }
    
    @NonNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ImageAnalysis:");
        sb.append(this.getName());
        return sb.toString();
    }
    
    public interface Analyzer
    {
        void analyze(@NonNull final ImageProxy p0);
        
        @Nullable
        Size getDefaultTargetResolution();
        
        int getTargetCoordinateSystem();
        
        void updateTransform(@Nullable final Matrix p0);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public @interface BackpressureStrategy {
    }
    
    public static final class Builder implements ImageOutputConfig.Builder<Builder>, ThreadConfig.Builder<Builder>, UseCaseConfig.Builder<ImageAnalysis, ImageAnalysisConfig, Builder>
    {
        private final MutableOptionsBundle mMutableConfig;
        
        public Builder() {
            this(MutableOptionsBundle.create());
        }
        
        private Builder(final MutableOptionsBundle mMutableConfig) {
            this.mMutableConfig = mMutableConfig;
            final Class<?> obj = mMutableConfig.retrieveOption(TargetConfig.OPTION_TARGET_CLASS, null);
            if (obj != null && !obj.equals(ImageAnalysis.class)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid target class configuration for ");
                sb.append(this);
                sb.append(": ");
                sb.append(obj);
                throw new IllegalArgumentException(sb.toString());
            }
            this.setTargetClass(ImageAnalysis.class);
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        static Builder fromConfig(@NonNull final Config config) {
            return new Builder(MutableOptionsBundle.from(config));
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public static Builder fromConfig(@NonNull final ImageAnalysisConfig imageAnalysisConfig) {
            return new Builder(MutableOptionsBundle.from(imageAnalysisConfig));
        }
        
        @NonNull
        @Override
        public ImageAnalysis build() {
            if (this.getMutableConfig().retrieveOption(ImageOutputConfig.OPTION_TARGET_ASPECT_RATIO, null) != null && this.getMutableConfig().retrieveOption(ImageOutputConfig.OPTION_TARGET_RESOLUTION, null) != null) {
                throw new IllegalArgumentException("Cannot use both setTargetResolution and setTargetAspectRatio on the same config.");
            }
            return new ImageAnalysis(this.getUseCaseConfig());
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        @Override
        public MutableConfig getMutableConfig() {
            return this.mMutableConfig;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public ImageAnalysisConfig getUseCaseConfig() {
            return new ImageAnalysisConfig(OptionsBundle.from(this.mMutableConfig));
        }
        
        @NonNull
        public Builder setBackgroundExecutor(@NonNull final Executor executor) {
            this.getMutableConfig().insertOption(ThreadConfig.OPTION_BACKGROUND_EXECUTOR, executor);
            return this;
        }
        
        @NonNull
        public Builder setBackpressureStrategy(final int i) {
            this.getMutableConfig().insertOption(ImageAnalysisConfig.OPTION_BACKPRESSURE_STRATEGY, i);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY })
        public Builder setCameraSelector(@NonNull final CameraSelector cameraSelector) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_CAMERA_SELECTOR, cameraSelector);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setCaptureOptionUnpacker(@NonNull final CaptureConfig.OptionUnpacker optionUnpacker) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_CAPTURE_CONFIG_UNPACKER, optionUnpacker);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setDefaultCaptureConfig(@NonNull final CaptureConfig captureConfig) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_DEFAULT_CAPTURE_CONFIG, captureConfig);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setDefaultResolution(@NonNull final Size size) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_DEFAULT_RESOLUTION, size);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setDefaultSessionConfig(@NonNull final SessionConfig sessionConfig) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_DEFAULT_SESSION_CONFIG, sessionConfig);
            return this;
        }
        
        @NonNull
        public Builder setImageQueueDepth(final int i) {
            this.getMutableConfig().insertOption(ImageAnalysisConfig.OPTION_IMAGE_QUEUE_DEPTH, i);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setImageReaderProxyProvider(@NonNull final ImageReaderProxyProvider imageReaderProxyProvider) {
            this.getMutableConfig().insertOption(ImageAnalysisConfig.OPTION_IMAGE_READER_PROXY_PROVIDER, imageReaderProxyProvider);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setMaxResolution(@NonNull final Size size) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_MAX_RESOLUTION, size);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setOnePixelShiftEnabled(final boolean b) {
            this.getMutableConfig().insertOption(ImageAnalysisConfig.OPTION_ONE_PIXEL_SHIFT_ENABLED, b);
            return this;
        }
        
        @NonNull
        public Builder setOutputImageFormat(final int i) {
            this.getMutableConfig().insertOption(ImageAnalysisConfig.OPTION_OUTPUT_IMAGE_FORMAT, i);
            return this;
        }
        
        @NonNull
        @RequiresApi(23)
        public Builder setOutputImageRotationEnabled(final boolean b) {
            this.getMutableConfig().insertOption(ImageAnalysisConfig.OPTION_OUTPUT_IMAGE_ROTATION_ENABLED, b);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setSessionOptionUnpacker(@NonNull final SessionConfig.OptionUnpacker optionUnpacker) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_SESSION_CONFIG_UNPACKER, optionUnpacker);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setSupportedResolutions(@NonNull final List<Pair<Integer, Size[]>> list) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_SUPPORTED_RESOLUTIONS, list);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setSurfaceOccupancyPriority(final int i) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_SURFACE_OCCUPANCY_PRIORITY, i);
            return this;
        }
        
        @NonNull
        public Builder setTargetAspectRatio(final int i) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_TARGET_ASPECT_RATIO, i);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setTargetClass(@NonNull final Class<ImageAnalysis> clazz) {
            this.getMutableConfig().insertOption(TargetConfig.OPTION_TARGET_CLASS, clazz);
            if (this.getMutableConfig().retrieveOption(TargetConfig.OPTION_TARGET_NAME, null) == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append(clazz.getCanonicalName());
                sb.append("-");
                sb.append(UUID.randomUUID());
                this.setTargetName(sb.toString());
            }
            return this;
        }
        
        @NonNull
        public Builder setTargetName(@NonNull final String s) {
            this.getMutableConfig().insertOption(TargetConfig.OPTION_TARGET_NAME, s);
            return this;
        }
        
        @NonNull
        public Builder setTargetResolution(@NonNull final Size size) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_TARGET_RESOLUTION, size);
            return this;
        }
        
        @NonNull
        public Builder setTargetRotation(final int i) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_TARGET_ROTATION, i);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setUseCaseEventCallback(@NonNull final EventCallback eventCallback) {
            this.getMutableConfig().insertOption(UseCaseEventConfig.OPTION_USE_CASE_EVENT_CALLBACK, eventCallback);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setZslDisabled(final boolean b) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_ZSL_DISABLED, b);
            return this;
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final class Defaults implements ConfigProvider<ImageAnalysisConfig>
    {
        private static final int DEFAULT_ASPECT_RATIO = 0;
        private static final ImageAnalysisConfig DEFAULT_CONFIG;
        private static final int DEFAULT_SURFACE_OCCUPANCY_PRIORITY = 1;
        private static final Size DEFAULT_TARGET_RESOLUTION;
        
        static {
            DEFAULT_CONFIG = new Builder().setDefaultResolution(DEFAULT_TARGET_RESOLUTION = new Size(640, 480)).setSurfaceOccupancyPriority(1).setTargetAspectRatio(0).getUseCaseConfig();
        }
        
        @NonNull
        @Override
        public ImageAnalysisConfig getConfig() {
            return Defaults.DEFAULT_CONFIG;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public @interface OutputImageFormat {
    }
}

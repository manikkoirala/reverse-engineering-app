// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.camera.core.impl.CameraFactory;
import androidx.core.util.Preconditions;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;
import androidx.annotation.NonNull;
import androidx.annotation.GuardedBy;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import androidx.annotation.RequiresApi;
import java.util.concurrent.Executor;

@RequiresApi(21)
class CameraExecutor implements Executor
{
    private static final int DEFAULT_CORE_THREADS = 1;
    private static final int DEFAULT_MAX_THREADS = 1;
    private static final String TAG = "CameraExecutor";
    private static final ThreadFactory THREAD_FACTORY;
    private final Object mExecutorLock;
    @GuardedBy("mExecutorLock")
    @NonNull
    private ThreadPoolExecutor mThreadPoolExecutor;
    
    static {
        THREAD_FACTORY = new ThreadFactory() {
            private static final String THREAD_NAME_STEM = "CameraX-core_camera_%d";
            private final AtomicInteger mThreadId = new AtomicInteger(0);
            
            @Override
            public Thread newThread(@NonNull final Runnable target) {
                final Thread thread = new Thread(target);
                thread.setName(String.format(Locale.US, "CameraX-core_camera_%d", this.mThreadId.getAndIncrement()));
                return thread;
            }
        };
    }
    
    CameraExecutor() {
        this.mExecutorLock = new Object();
        this.mThreadPoolExecutor = createExecutor();
    }
    
    private static ThreadPoolExecutor createExecutor() {
        final ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(), CameraExecutor.THREAD_FACTORY);
        threadPoolExecutor.setRejectedExecutionHandler(new O8());
        return threadPoolExecutor;
    }
    
    void deinit() {
        synchronized (this.mExecutorLock) {
            if (!this.mThreadPoolExecutor.isShutdown()) {
                this.mThreadPoolExecutor.shutdown();
            }
        }
    }
    
    @Override
    public void execute(@NonNull final Runnable command) {
        Preconditions.checkNotNull(command);
        synchronized (this.mExecutorLock) {
            this.mThreadPoolExecutor.execute(command);
        }
    }
    
    void init(@NonNull final CameraFactory cameraFactory) {
        Preconditions.checkNotNull(cameraFactory);
        synchronized (this.mExecutorLock) {
            if (this.mThreadPoolExecutor.isShutdown()) {
                this.mThreadPoolExecutor = createExecutor();
            }
            final ThreadPoolExecutor mThreadPoolExecutor = this.mThreadPoolExecutor;
            monitorexit(this.mExecutorLock);
            final int max = Math.max(1, cameraFactory.getAvailableCameraIds().size());
            mThreadPoolExecutor.setMaximumPoolSize(max);
            mThreadPoolExecutor.setCorePoolSize(max);
        }
    }
}

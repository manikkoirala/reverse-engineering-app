// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.RequiresApi;

@RequiresApi(21)
final class ImageReaderFormatRecommender
{
    private ImageReaderFormatRecommender() {
    }
    
    static FormatCombo chooseCombo() {
        return FormatCombo.create(256, 35);
    }
    
    abstract static class FormatCombo
    {
        static FormatCombo create(final int n, final int n2) {
            return (FormatCombo)new AutoValue_ImageReaderFormatRecommender_FormatCombo(n, n2);
        }
        
        abstract int imageAnalysisFormat();
        
        abstract int imageCaptureFormat();
    }
}

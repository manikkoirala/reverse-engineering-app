// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.camera.core.impl.ReadableConfig;
import java.util.concurrent.Future;
import android.net.Uri;
import java.io.OutputStream;
import java.io.File;
import android.content.ContentValues;
import android.content.ContentResolver;
import android.location.Location;
import java.util.Locale;
import java.util.ArrayDeque;
import java.util.Deque;
import java.nio.ByteBuffer;
import java.util.concurrent.RejectedExecutionException;
import java.io.IOException;
import java.io.InputStream;
import androidx.camera.core.impl.utils.Exif;
import java.io.ByteArrayInputStream;
import android.graphics.Matrix;
import java.util.concurrent.atomic.AtomicBoolean;
import androidx.camera.core.impl.ConfigProvider;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.camera.core.internal.UseCaseEventConfig;
import java.util.UUID;
import androidx.camera.core.impl.OptionsBundle;
import androidx.camera.core.internal.TargetConfig;
import androidx.camera.core.impl.MutableOptionsBundle;
import androidx.camera.core.internal.IoConfig;
import android.os.Looper;
import androidx.arch.core.util.Function;
import androidx.camera.core.impl.utils.CameraOrientationUtil;
import androidx.camera.core.impl.ImageOutputConfig;
import androidx.camera.core.impl.Quirk;
import androidx.camera.core.internal.compat.quirk.SoftwareJpegEncodingPreferredQuirk;
import androidx.camera.core.impl.CameraInfoInternal;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.ThreadFactory;
import java.util.Collection;
import java.util.ArrayList;
import androidx.annotation.VisibleForTesting;
import androidx.camera.core.impl.Oooo8o0\u3007;
import androidx.camera.core.impl.UseCaseConfigFactory;
import android.view.Surface;
import androidx.camera.camera2.internal.\u3007o\u30078;
import androidx.camera.core.impl.ImmediateSurface;
import androidx.camera.core.impl.TagBundle;
import androidx.camera.core.impl.MutableTagBundle;
import android.media.ImageReader;
import java.util.concurrent.CancellationException;
import androidx.camera.core.imagecapture.TakePictureRequest;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import java.util.Collections;
import androidx.camera.core.impl.SessionProcessor;
import androidx.camera.core.impl.ImageInputConfig;
import java.util.Iterator;
import android.util.Pair;
import androidx.camera.core.impl.utils.TransformUtils;
import androidx.annotation.IntRange;
import androidx.camera.core.impl.CameraInternal;
import androidx.camera.core.impl.CaptureStage;
import androidx.camera.core.impl.MutableConfig;
import androidx.annotation.OptIn;
import android.os.Build$VERSION;
import java.util.Objects;
import androidx.camera.core.internal.utils.ImageUtil;
import androidx.annotation.Nullable;
import android.graphics.Rect;
import androidx.camera.core.impl.utils.Threads;
import androidx.annotation.UiThread;
import androidx.camera.core.internal.YuvToJpegProcessor;
import android.util.Size;
import androidx.core.util.Preconditions;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.impl.Config;
import java.util.List;
import androidx.annotation.MainThread;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.core.impl.UseCaseConfig;
import androidx.camera.core.impl.ImageCaptureConfig;
import androidx.camera.core.imagecapture.TakePictureManager;
import androidx.camera.core.impl.SessionConfig;
import androidx.camera.core.impl.CameraCaptureCallback;
import java.util.concurrent.atomic.AtomicReference;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.camera.core.imagecapture.ImagePipeline;
import androidx.camera.core.imagecapture.ImageCaptureControl;
import androidx.annotation.GuardedBy;
import java.util.concurrent.ExecutorService;
import androidx.camera.core.impl.DeferrableSurface;
import android.util.Rational;
import androidx.camera.core.impl.ImageReaderProxy;
import androidx.camera.core.impl.CaptureProcessor;
import androidx.camera.core.impl.CaptureConfig;
import androidx.camera.core.impl.CaptureBundle;
import androidx.camera.core.internal.compat.workaround.ExifRotationAvailability;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class ImageCapture extends UseCase
{
    public static final int CAPTURE_MODE_MAXIMIZE_QUALITY = 0;
    public static final int CAPTURE_MODE_MINIMIZE_LATENCY = 1;
    @ExperimentalZeroShutterLag
    public static final int CAPTURE_MODE_ZERO_SHUTTER_LAG = 2;
    private static final int DEFAULT_CAPTURE_MODE = 1;
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final Defaults DEFAULT_CONFIG;
    private static final int DEFAULT_FLASH_MODE = 2;
    public static final int ERROR_CAMERA_CLOSED = 3;
    public static final int ERROR_CAPTURE_FAILED = 2;
    public static final int ERROR_FILE_IO = 1;
    public static final int ERROR_INVALID_CAMERA = 4;
    public static final int ERROR_UNKNOWN = 0;
    static final ExifRotationAvailability EXIF_ROTATION_AVAILABILITY;
    public static final int FLASH_MODE_AUTO = 0;
    public static final int FLASH_MODE_OFF = 2;
    public static final int FLASH_MODE_ON = 1;
    private static final int FLASH_MODE_UNKNOWN = -1;
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final int FLASH_TYPE_ONE_SHOT_FLASH = 0;
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final int FLASH_TYPE_USE_TORCH_AS_FLASH = 1;
    private static final byte JPEG_QUALITY_MAXIMIZE_QUALITY_MODE = 100;
    private static final byte JPEG_QUALITY_MINIMIZE_LATENCY_MODE = 95;
    private static final int MAX_IMAGES = 2;
    private static final String TAG = "ImageCapture";
    private CaptureBundle mCaptureBundle;
    private CaptureConfig mCaptureConfig;
    private final int mCaptureMode;
    private CaptureProcessor mCaptureProcessor;
    private final ImageReaderProxy.OnImageAvailableListener mClosingListener;
    private Rational mCropAspectRatio;
    private DeferrableSurface mDeferrableSurface;
    private ExecutorService mExecutor;
    @GuardedBy("mLockedFlashMode")
    private int mFlashMode;
    private final int mFlashType;
    private final ImageCaptureControl mImageCaptureControl;
    private ImageCaptureRequestProcessor mImageCaptureRequestProcessor;
    private ImagePipeline mImagePipeline;
    SafeCloseImageReaderProxy mImageReader;
    private ListenableFuture<Void> mImageReaderCloseFuture;
    @NonNull
    final Executor mIoExecutor;
    @GuardedBy("mLockedFlashMode")
    private final AtomicReference<Integer> mLockedFlashMode;
    private int mMaxCaptureStages;
    private CameraCaptureCallback mMetadataMatchingCaptureCallback;
    ProcessingImageReader mProcessingImageReader;
    final Executor mSequentialIoExecutor;
    SessionConfig.Builder mSessionConfigBuilder;
    private TakePictureManager mTakePictureManager;
    boolean mUseProcessingPipeline;
    private boolean mUseSoftwareJpeg;
    
    static {
        DEFAULT_CONFIG = new Defaults();
        EXIF_ROTATION_AVAILABILITY = new ExifRotationAvailability();
    }
    
    ImageCapture(@NonNull ImageCaptureConfig imageCaptureConfig) {
        super(imageCaptureConfig);
        this.mUseProcessingPipeline = false;
        this.mClosingListener = new \u300700();
        this.mLockedFlashMode = new AtomicReference<Integer>(null);
        this.mFlashMode = -1;
        this.mCropAspectRatio = null;
        this.mUseSoftwareJpeg = false;
        this.mImageReaderCloseFuture = Futures.immediateFuture((Void)null);
        this.mImageCaptureControl = new ImageCaptureControl() {
            final ImageCapture this$0;
            
            @MainThread
            @Override
            public void lockFlashMode() {
                this.this$0.lockFlashMode();
            }
            
            @MainThread
            @NonNull
            @Override
            public ListenableFuture<Void> submitStillCaptureRequests(@NonNull final List<CaptureConfig> list) {
                return this.this$0.submitStillCaptureRequest(list);
            }
            
            @MainThread
            @Override
            public void unlockFlashMode() {
                this.this$0.unlockFlashMode();
            }
        };
        imageCaptureConfig = (ImageCaptureConfig)this.getCurrentConfig();
        if (imageCaptureConfig.containsOption(ImageCaptureConfig.OPTION_IMAGE_CAPTURE_MODE)) {
            this.mCaptureMode = imageCaptureConfig.getCaptureMode();
        }
        else {
            this.mCaptureMode = 1;
        }
        this.mFlashType = imageCaptureConfig.getFlashType(0);
        final Executor mIoExecutor = Preconditions.checkNotNull(imageCaptureConfig.getIoExecutor(CameraXExecutors.ioExecutor()));
        this.mIoExecutor = mIoExecutor;
        this.mSequentialIoExecutor = CameraXExecutors.newSequentialExecutor(mIoExecutor);
    }
    
    @UiThread
    private void abortImageCaptureRequests() {
        if (this.mImageCaptureRequestProcessor != null) {
            this.mImageCaptureRequestProcessor.cancelRequests(new CameraClosedException("Camera is closed."));
        }
    }
    
    @MainThread
    private void clearPipelineWithNode() {
        Threads.checkMainThread();
        this.mImagePipeline.close();
        this.mImagePipeline = null;
        this.mTakePictureManager.abortRequests();
        this.mTakePictureManager = null;
    }
    
    @NonNull
    static Rect computeDispatchCropRect(@Nullable Rect computeCropRectFromAspectRatio, @Nullable final Rational rational, final int n, @NonNull final Size size, final int n2) {
        if (computeCropRectFromAspectRatio != null) {
            return ImageUtil.computeCropRectFromDispatchInfo(computeCropRectFromAspectRatio, n, size, n2);
        }
        if (rational != null) {
            Rational rational2 = rational;
            if (n2 % 180 != 0) {
                rational2 = new Rational(rational.getDenominator(), rational.getNumerator());
            }
            if (ImageUtil.isAspectRatioValid(size, rational2)) {
                computeCropRectFromAspectRatio = ImageUtil.computeCropRectFromAspectRatio(size, rational2);
                Objects.requireNonNull(computeCropRectFromAspectRatio);
                return computeCropRectFromAspectRatio;
            }
        }
        return new Rect(0, 0, size.getWidth(), size.getHeight());
    }
    
    @MainThread
    @OptIn(markerClass = { ExperimentalZeroShutterLag.class })
    private SessionConfig.Builder createPipelineWithNode(@NonNull final String s, @NonNull final ImageCaptureConfig imageCaptureConfig, @NonNull final Size size) {
        Threads.checkMainThread();
        final boolean b = false;
        String.format("createPipelineWithNode(cameraId: %s, resolution: %s)", s, size);
        Preconditions.checkState(this.mImagePipeline == null);
        this.mImagePipeline = new ImagePipeline(imageCaptureConfig, size);
        boolean b2 = b;
        if (this.mTakePictureManager == null) {
            b2 = true;
        }
        Preconditions.checkState(b2);
        this.mTakePictureManager = new TakePictureManager(this.mImageCaptureControl, this.mImagePipeline);
        final SessionConfig.Builder sessionConfigBuilder = this.mImagePipeline.createSessionConfigBuilder();
        if (Build$VERSION.SDK_INT >= 23 && this.getCaptureMode() == 2) {
            this.getCameraControl().addZslConfig(sessionConfigBuilder);
        }
        sessionConfigBuilder.addErrorListener(new o0ooO(this, s));
        return sessionConfigBuilder;
    }
    
    static boolean enforceSoftwareJpegConstraints(@NonNull final MutableConfig mutableConfig) {
        final Boolean true = Boolean.TRUE;
        final Config.Option<Boolean> option_USE_SOFTWARE_JPEG_ENCODER = ImageCaptureConfig.OPTION_USE_SOFTWARE_JPEG_ENCODER;
        final Boolean false = Boolean.FALSE;
        final boolean equals = true.equals(mutableConfig.retrieveOption(option_USE_SOFTWARE_JPEG_ENCODER, false));
        boolean b = false;
        final boolean b2 = false;
        if (equals) {
            final int sdk_INT = Build$VERSION.SDK_INT;
            boolean b3;
            if (sdk_INT < 26) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Software JPEG only supported on API 26+, but current API level is ");
                sb.append(sdk_INT);
                Logger.w("ImageCapture", sb.toString());
                b3 = false;
            }
            else {
                b3 = true;
            }
            final Integer n = mutableConfig.retrieveOption(ImageCaptureConfig.OPTION_BUFFER_FORMAT, null);
            if (n != null && n != 256) {
                Logger.w("ImageCapture", "Software JPEG cannot be used with non-JPEG output buffer format.");
                b3 = b2;
            }
            if (!(b = b3)) {
                Logger.w("ImageCapture", "Unable to support software JPEG. Disabling.");
                mutableConfig.insertOption(option_USE_SOFTWARE_JPEG_ENCODER, false);
                b = b3;
            }
        }
        return b;
    }
    
    private CaptureBundle getCaptureBundle(final CaptureBundle captureBundle) {
        final List<CaptureStage> captureStages = this.mCaptureBundle.getCaptureStages();
        CaptureBundle captureBundle2 = captureBundle;
        if (captureStages != null) {
            if (captureStages.isEmpty()) {
                captureBundle2 = captureBundle;
            }
            else {
                captureBundle2 = CaptureBundles.createCaptureBundle(captureStages);
            }
        }
        return captureBundle2;
    }
    
    private int getCaptureStageSize(@NonNull final ImageCaptureConfig imageCaptureConfig) {
        final CaptureBundle captureBundle = imageCaptureConfig.getCaptureBundle(null);
        if (captureBundle == null) {
            return 1;
        }
        final List<CaptureStage> captureStages = captureBundle.getCaptureStages();
        if (captureStages == null) {
            return 1;
        }
        return captureStages.size();
    }
    
    static int getError(final Throwable t) {
        if (t instanceof CameraClosedException) {
            return 3;
        }
        if (t instanceof ImageCaptureException) {
            return ((ImageCaptureException)t).getImageCaptureError();
        }
        return 0;
    }
    
    @UiThread
    private int getJpegQualityForImageCaptureRequest(@NonNull final CameraInternal cameraInternal, final boolean b) {
        int n;
        if (b) {
            final int relativeRotation = this.getRelativeRotation(cameraInternal);
            final Size attachedSurfaceResolution = this.getAttachedSurfaceResolution();
            Objects.requireNonNull(attachedSurfaceResolution);
            final Rect computeDispatchCropRect = computeDispatchCropRect(this.getViewPortCropRect(), this.mCropAspectRatio, relativeRotation, attachedSurfaceResolution, relativeRotation);
            if (ImageUtil.shouldCropImage(attachedSurfaceResolution.getWidth(), attachedSurfaceResolution.getHeight(), computeDispatchCropRect.width(), computeDispatchCropRect.height())) {
                if (this.mCaptureMode == 0) {
                    n = 100;
                }
                else {
                    n = 95;
                }
            }
            else {
                n = this.getJpegQualityInternal();
            }
        }
        else {
            n = this.getJpegQualityInternal();
        }
        return n;
    }
    
    @IntRange(from = 1L, to = 100L)
    private int getJpegQualityInternal() {
        final ImageCaptureConfig imageCaptureConfig = (ImageCaptureConfig)this.getCurrentConfig();
        if (imageCaptureConfig.containsOption(ImageCaptureConfig.OPTION_JPEG_COMPRESSION_QUALITY)) {
            return imageCaptureConfig.getJpegQuality();
        }
        final int mCaptureMode = this.mCaptureMode;
        if (mCaptureMode == 0) {
            return 100;
        }
        if (mCaptureMode != 1 && mCaptureMode != 2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("CaptureMode ");
            sb.append(this.mCaptureMode);
            sb.append(" is invalid");
            throw new IllegalStateException(sb.toString());
        }
        return 95;
    }
    
    @NonNull
    private Rect getTakePictureCropRect() {
        final Rect viewPortCropRect = this.getViewPortCropRect();
        final Size attachedSurfaceResolution = this.getAttachedSurfaceResolution();
        Objects.requireNonNull(attachedSurfaceResolution);
        if (viewPortCropRect != null) {
            return viewPortCropRect;
        }
        if (ImageUtil.isAspectRatioValid(this.mCropAspectRatio)) {
            final CameraInternal camera = this.getCamera();
            Objects.requireNonNull(camera);
            final int relativeRotation = this.getRelativeRotation(camera);
            Rational mCropAspectRatio = new Rational(this.mCropAspectRatio.getDenominator(), this.mCropAspectRatio.getNumerator());
            if (!TransformUtils.is90or270(relativeRotation)) {
                mCropAspectRatio = this.mCropAspectRatio;
            }
            final Rect computeCropRectFromAspectRatio = ImageUtil.computeCropRectFromAspectRatio(attachedSurfaceResolution, mCropAspectRatio);
            Objects.requireNonNull(computeCropRectFromAspectRatio);
            return computeCropRectFromAspectRatio;
        }
        return new Rect(0, 0, attachedSurfaceResolution.getWidth(), attachedSurfaceResolution.getHeight());
    }
    
    private static boolean isImageFormatSupported(final List<Pair<Integer, Size[]>> list, final int i) {
        if (list == null) {
            return false;
        }
        final Iterator<Pair<Integer, Size[]>> iterator = list.iterator();
        while (iterator.hasNext()) {
            if (((Integer)iterator.next().first).equals(i)) {
                return true;
            }
        }
        return false;
    }
    
    @MainThread
    private boolean isNodeEnabled() {
        Threads.checkMainThread();
        final ImageCaptureConfig imageCaptureConfig = (ImageCaptureConfig)this.getCurrentConfig();
        if (imageCaptureConfig.getImageReaderProxyProvider() != null) {
            return false;
        }
        if (this.isSessionProcessorEnabledInCurrentCamera()) {
            return false;
        }
        if (this.mCaptureProcessor != null) {
            return false;
        }
        if (this.getCaptureStageSize(imageCaptureConfig) > 1) {
            return false;
        }
        final Integer obj = (Integer)imageCaptureConfig.retrieveOption(ImageInputConfig.OPTION_INPUT_FORMAT, 256);
        Objects.requireNonNull(obj);
        return obj == 256 && this.mUseProcessingPipeline;
    }
    
    private boolean isSessionProcessorEnabledInCurrentCamera() {
        final CameraInternal camera = this.getCamera();
        boolean b = false;
        if (camera == null) {
            return false;
        }
        if (this.getCamera().getExtendedConfig().getSessionProcessor(null) != null) {
            b = true;
        }
        return b;
    }
    
    @UiThread
    private void sendImageCaptureRequest(@NonNull final Executor executor, @NonNull final OnImageCapturedCallback onImageCapturedCallback, final boolean b) {
        final CameraInternal camera = this.getCamera();
        if (camera == null) {
            executor.execute(new \u3007oOO8O8(this, onImageCapturedCallback));
            return;
        }
        final ImageCaptureRequestProcessor mImageCaptureRequestProcessor = this.mImageCaptureRequestProcessor;
        if (mImageCaptureRequestProcessor == null) {
            executor.execute(new \u30070000OOO(onImageCapturedCallback));
            return;
        }
        mImageCaptureRequestProcessor.sendRequest(new ImageCaptureRequest(this.getRelativeRotation(camera), this.getJpegQualityForImageCaptureRequest(camera, b), this.mCropAspectRatio, this.getViewPortCropRect(), this.getSensorToBufferTransformMatrix(), executor, onImageCapturedCallback));
    }
    
    private void sendInvalidCameraError(@NonNull final Executor executor, @Nullable final OnImageCapturedCallback onImageCapturedCallback, @Nullable final OnImageSavedCallback onImageSavedCallback) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Not bound to a valid Camera [");
        sb.append(this);
        sb.append("]");
        final ImageCaptureException ex = new ImageCaptureException(4, sb.toString(), null);
        if (onImageCapturedCallback != null) {
            onImageCapturedCallback.onError(ex);
        }
        else {
            if (onImageSavedCallback == null) {
                throw new IllegalArgumentException("Must have either in-memory or on-disk callback.");
            }
            onImageSavedCallback.onError(ex);
        }
    }
    
    @NonNull
    private ListenableFuture<ImageProxy> takePictureInternal(@NonNull final ImageCaptureRequest imageCaptureRequest) {
        return CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<ImageProxy>)new \u3007o(this, imageCaptureRequest));
    }
    
    @MainThread
    private void takePictureWithNode(@NonNull final Executor executor, @Nullable final OnImageCapturedCallback onImageCapturedCallback, @Nullable final OnImageSavedCallback onImageSavedCallback, @Nullable final OutputFileOptions outputFileOptions) {
        Threads.checkMainThread();
        final CameraInternal camera = this.getCamera();
        if (camera == null) {
            this.sendInvalidCameraError(executor, onImageCapturedCallback, onImageSavedCallback);
            return;
        }
        this.mTakePictureManager.offerRequest(TakePictureRequest.of(executor, onImageCapturedCallback, onImageSavedCallback, outputFileOptions, this.getTakePictureCropRect(), this.getSensorToBufferTransformMatrix(), this.getRelativeRotation(camera), this.getJpegQualityInternal(), this.getCaptureMode(), this.mSessionConfigBuilder.getSingleCameraCaptureCallbacks()));
    }
    
    private void trySetFlashModeToCameraControl() {
        synchronized (this.mLockedFlashMode) {
            if (this.mLockedFlashMode.get() != null) {
                return;
            }
            this.getCameraControl().setFlashMode(this.getFlashMode());
        }
    }
    
    @UiThread
    void clearPipeline() {
        Threads.checkMainThread();
        if (this.isNodeEnabled()) {
            this.clearPipelineWithNode();
            return;
        }
        final ImageCaptureRequestProcessor mImageCaptureRequestProcessor = this.mImageCaptureRequestProcessor;
        if (mImageCaptureRequestProcessor != null) {
            mImageCaptureRequestProcessor.cancelRequests(new CancellationException("Request is canceled."));
            this.mImageCaptureRequestProcessor = null;
        }
        final DeferrableSurface mDeferrableSurface = this.mDeferrableSurface;
        this.mDeferrableSurface = null;
        this.mImageReader = null;
        this.mProcessingImageReader = null;
        this.mImageReaderCloseFuture = Futures.immediateFuture((Void)null);
        if (mDeferrableSurface != null) {
            mDeferrableSurface.close();
        }
    }
    
    @UiThread
    SessionConfig.Builder createPipeline(@NonNull final String s, @NonNull final ImageCaptureConfig imageCaptureConfig, @NonNull final Size size) {
        Threads.checkMainThread();
        if (this.isNodeEnabled()) {
            return this.createPipelineWithNode(s, imageCaptureConfig, size);
        }
        final SessionConfig.Builder from = SessionConfig.Builder.createFrom(imageCaptureConfig);
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 23 && this.getCaptureMode() == 2) {
            this.getCameraControl().addZslConfig(from);
        }
        YuvToJpegProcessor yuvToJpegProcessor = null;
        Label_0645: {
            if (imageCaptureConfig.getImageReaderProxyProvider() != null) {
                this.mImageReader = new SafeCloseImageReaderProxy(imageCaptureConfig.getImageReaderProxyProvider().newInstance(size.getWidth(), size.getHeight(), this.getImageFormat(), 2, 0L));
                this.mMetadataMatchingCaptureCallback = new CameraCaptureCallback(this) {
                    final ImageCapture this$0;
                };
            }
            else {
                if (this.isSessionProcessorEnabledInCurrentCamera()) {
                    ImageReaderProxy build;
                    if (this.getImageFormat() == 256) {
                        build = new AndroidImageReaderProxy(ImageReader.newInstance(size.getWidth(), size.getHeight(), this.getImageFormat(), 2));
                        yuvToJpegProcessor = null;
                    }
                    else {
                        if (this.getImageFormat() != 35) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Unsupported image format:");
                            sb.append(this.getImageFormat());
                            throw new IllegalArgumentException(sb.toString());
                        }
                        if (sdk_INT < 26) {
                            throw new UnsupportedOperationException("Does not support API level < 26");
                        }
                        yuvToJpegProcessor = new YuvToJpegProcessor(this.getJpegQualityInternal(), 2);
                        final ModifiableImageReaderProxy modifiableImageReaderProxy = new ModifiableImageReaderProxy(ImageReader.newInstance(size.getWidth(), size.getHeight(), 35, 2));
                        final CaptureBundle singleDefaultCaptureBundle = CaptureBundles.singleDefaultCaptureBundle();
                        build = new ProcessingImageReader.Builder(modifiableImageReaderProxy, singleDefaultCaptureBundle, yuvToJpegProcessor).setPostProcessExecutor(this.mExecutor).setOutputFormat(256).build();
                        final MutableTagBundle create = MutableTagBundle.create();
                        create.putTag(((ProcessingImageReader)build).getTagBundleKey(), singleDefaultCaptureBundle.getCaptureStages().get(0).getId());
                        modifiableImageReaderProxy.setImageTagBundle(create);
                    }
                    this.mMetadataMatchingCaptureCallback = new CameraCaptureCallback(this) {
                        final ImageCapture this$0;
                    };
                    this.mImageReader = new SafeCloseImageReaderProxy(build);
                    break Label_0645;
                }
                CaptureProcessor mCaptureProcessor = this.mCaptureProcessor;
                if (mCaptureProcessor != null || this.mUseSoftwareJpeg) {
                    final int imageFormat = this.getImageFormat();
                    int imageFormat2 = this.getImageFormat();
                    if (this.mUseSoftwareJpeg) {
                        if (sdk_INT < 26) {
                            throw new IllegalStateException("Software JPEG only supported on API 26+");
                        }
                        Logger.i("ImageCapture", "Using software JPEG encoder.");
                        if (this.mCaptureProcessor != null) {
                            yuvToJpegProcessor = new YuvToJpegProcessor(this.getJpegQualityInternal(), this.mMaxCaptureStages);
                            mCaptureProcessor = new CaptureProcessorPipeline(this.mCaptureProcessor, this.mMaxCaptureStages, yuvToJpegProcessor, this.mExecutor);
                        }
                        else {
                            yuvToJpegProcessor = (YuvToJpegProcessor)(mCaptureProcessor = new YuvToJpegProcessor(this.getJpegQualityInternal(), this.mMaxCaptureStages));
                        }
                        imageFormat2 = 256;
                    }
                    else {
                        yuvToJpegProcessor = null;
                    }
                    final ProcessingImageReader build2 = new ProcessingImageReader.Builder(size.getWidth(), size.getHeight(), imageFormat, this.mMaxCaptureStages, this.getCaptureBundle(CaptureBundles.singleDefaultCaptureBundle()), mCaptureProcessor).setPostProcessExecutor(this.mExecutor).setOutputFormat(imageFormat2).build();
                    this.mProcessingImageReader = build2;
                    this.mMetadataMatchingCaptureCallback = build2.getCameraCaptureCallback();
                    this.mImageReader = new SafeCloseImageReaderProxy(this.mProcessingImageReader);
                    break Label_0645;
                }
                final MetadataImageReader metadataImageReader = new MetadataImageReader(size.getWidth(), size.getHeight(), this.getImageFormat(), 2);
                this.mMetadataMatchingCaptureCallback = metadataImageReader.getCameraCaptureCallback();
                this.mImageReader = new SafeCloseImageReaderProxy(metadataImageReader);
            }
            yuvToJpegProcessor = null;
        }
        final ImageCaptureRequestProcessor mImageCaptureRequestProcessor = this.mImageCaptureRequestProcessor;
        if (mImageCaptureRequestProcessor != null) {
            mImageCaptureRequestProcessor.cancelRequests(new CancellationException("Request is canceled."));
        }
        final oo\u3007 oo\u3007 = new oo\u3007(this);
        Object o;
        if (yuvToJpegProcessor == null) {
            o = null;
        }
        else {
            o = new O8\u3007o(yuvToJpegProcessor);
        }
        this.mImageCaptureRequestProcessor = new ImageCaptureRequestProcessor(2, (ImageCaptor)oo\u3007, (RequestProcessCallback)o);
        this.mImageReader.setOnImageAvailableListener(this.mClosingListener, CameraXExecutors.mainThreadExecutor());
        final DeferrableSurface mDeferrableSurface = this.mDeferrableSurface;
        if (mDeferrableSurface != null) {
            mDeferrableSurface.close();
        }
        final Surface surface = this.mImageReader.getSurface();
        Objects.requireNonNull(surface);
        this.mDeferrableSurface = new ImmediateSurface(surface, new Size(this.mImageReader.getWidth(), this.mImageReader.getHeight()), this.getImageFormat());
        final ProcessingImageReader mProcessingImageReader = this.mProcessingImageReader;
        ListenableFuture<Void> mImageReaderCloseFuture;
        if (mProcessingImageReader != null) {
            mImageReaderCloseFuture = mProcessingImageReader.getCloseFuture();
        }
        else {
            mImageReaderCloseFuture = Futures.immediateFuture((Void)null);
        }
        this.mImageReaderCloseFuture = mImageReaderCloseFuture;
        final ListenableFuture<Void> terminationFuture = this.mDeferrableSurface.getTerminationFuture();
        final SafeCloseImageReaderProxy mImageReader = this.mImageReader;
        Objects.requireNonNull(mImageReader);
        terminationFuture.addListener((Runnable)new \u3007o\u30078(mImageReader), (Executor)CameraXExecutors.mainThreadExecutor());
        from.addNonRepeatingSurface(this.mDeferrableSurface);
        from.addErrorListener(new \u300700\u30078(this, s, imageCaptureConfig, size));
        return from;
    }
    
    public int getCaptureMode() {
        return this.mCaptureMode;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    public UseCaseConfig<?> getDefaultConfig(final boolean b, @NonNull final UseCaseConfigFactory useCaseConfigFactory) {
        Config config2;
        final Config config = config2 = useCaseConfigFactory.getConfig(UseCaseConfigFactory.CaptureType.IMAGE_CAPTURE, this.getCaptureMode());
        if (b) {
            config2 = Oooo8o0\u3007.\u3007o00\u3007\u3007Oo(config, ImageCapture.DEFAULT_CONFIG.getConfig());
        }
        Object useCaseConfig;
        if (config2 == null) {
            useCaseConfig = null;
        }
        else {
            useCaseConfig = this.getUseCaseConfigBuilder(config2).getUseCaseConfig();
        }
        return (UseCaseConfig<?>)useCaseConfig;
    }
    
    public int getFlashMode() {
        synchronized (this.mLockedFlashMode) {
            int n = this.mFlashMode;
            if (n == -1) {
                n = ((ImageCaptureConfig)this.getCurrentConfig()).getFlashMode(2);
            }
            return n;
        }
    }
    
    @IntRange(from = 1L, to = 100L)
    public int getJpegQuality() {
        return this.getJpegQualityInternal();
    }
    
    @Nullable
    @Override
    public ResolutionInfo getResolutionInfo() {
        return super.getResolutionInfo();
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    protected ResolutionInfo getResolutionInfoInternal() {
        final CameraInternal camera = this.getCamera();
        final Size attachedSurfaceResolution = this.getAttachedSurfaceResolution();
        if (camera != null && attachedSurfaceResolution != null) {
            final Rect viewPortCropRect = this.getViewPortCropRect();
            final Rational mCropAspectRatio = this.mCropAspectRatio;
            Rect computeCropRectFromAspectRatio;
            if ((computeCropRectFromAspectRatio = viewPortCropRect) == null) {
                if (mCropAspectRatio != null) {
                    computeCropRectFromAspectRatio = ImageUtil.computeCropRectFromAspectRatio(attachedSurfaceResolution, mCropAspectRatio);
                }
                else {
                    computeCropRectFromAspectRatio = new Rect(0, 0, attachedSurfaceResolution.getWidth(), attachedSurfaceResolution.getHeight());
                }
            }
            final int relativeRotation = this.getRelativeRotation(camera);
            Objects.requireNonNull(computeCropRectFromAspectRatio);
            return ResolutionInfo.create(attachedSurfaceResolution, computeCropRectFromAspectRatio, relativeRotation);
        }
        return null;
    }
    
    public int getTargetRotation() {
        return this.getTargetRotationInternal();
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    public UseCaseConfig.Builder<?, ?, ?> getUseCaseConfigBuilder(@NonNull final Config config) {
        return Builder.fromConfig(config);
    }
    
    @VisibleForTesting
    boolean isProcessingPipelineEnabled() {
        return this.mImagePipeline != null && this.mTakePictureManager != null;
    }
    
    ListenableFuture<Void> issueTakePicture(@NonNull final ImageCaptureRequest imageCaptureRequest) {
        Logger.d("ImageCapture", "issueTakePicture");
        final ArrayList list = new ArrayList();
        CaptureBundle captureBundle;
        String tagBundleKey;
        if (this.mProcessingImageReader != null) {
            captureBundle = this.getCaptureBundle(CaptureBundles.singleDefaultCaptureBundle());
            if (captureBundle == null) {
                return Futures.immediateFailedFuture(new IllegalArgumentException("ImageCapture cannot set empty CaptureBundle."));
            }
            final List<CaptureStage> captureStages = captureBundle.getCaptureStages();
            if (captureStages == null) {
                return Futures.immediateFailedFuture(new IllegalArgumentException("ImageCapture has CaptureBundle with null capture stages"));
            }
            if (this.mCaptureProcessor == null && captureStages.size() > 1) {
                return Futures.immediateFailedFuture(new IllegalArgumentException("No CaptureProcessor can be found to process the images captured for multiple CaptureStages."));
            }
            if (captureStages.size() > this.mMaxCaptureStages) {
                return Futures.immediateFailedFuture(new IllegalArgumentException("ImageCapture has CaptureStages > Max CaptureStage size"));
            }
            this.mProcessingImageReader.setCaptureBundle(captureBundle);
            this.mProcessingImageReader.setOnProcessingErrorCallback(CameraXExecutors.directExecutor(), (ProcessingImageReader.OnProcessingErrorCallback)new o\u30078(imageCaptureRequest));
            tagBundleKey = this.mProcessingImageReader.getTagBundleKey();
        }
        else {
            captureBundle = this.getCaptureBundle(CaptureBundles.singleDefaultCaptureBundle());
            if (captureBundle == null) {
                return Futures.immediateFailedFuture(new IllegalArgumentException("ImageCapture cannot set empty CaptureBundle."));
            }
            final List<CaptureStage> captureStages2 = captureBundle.getCaptureStages();
            if (captureStages2 == null) {
                return Futures.immediateFailedFuture(new IllegalArgumentException("ImageCapture has CaptureBundle with null capture stages"));
            }
            if (captureStages2.size() > 1) {
                return Futures.immediateFailedFuture(new IllegalArgumentException("ImageCapture have no CaptureProcess set with CaptureBundle size > 1."));
            }
            tagBundleKey = null;
        }
        for (final CaptureStage captureStage : captureBundle.getCaptureStages()) {
            final CaptureConfig.Builder builder = new CaptureConfig.Builder();
            builder.setTemplateType(this.mCaptureConfig.getTemplateType());
            builder.addImplementationOptions(this.mCaptureConfig.getImplementationOptions());
            builder.addAllCameraCaptureCallbacks(this.mSessionConfigBuilder.getSingleCameraCaptureCallbacks());
            builder.addSurface(this.mDeferrableSurface);
            if (this.getImageFormat() == 256) {
                if (ImageCapture.EXIF_ROTATION_AVAILABILITY.isRotationOptionSupported()) {
                    builder.addImplementationOption(CaptureConfig.OPTION_ROTATION, imageCaptureRequest.mRotationDegrees);
                }
                builder.addImplementationOption(CaptureConfig.OPTION_JPEG_QUALITY, imageCaptureRequest.mJpegQuality);
            }
            builder.addImplementationOptions(captureStage.getCaptureConfig().getImplementationOptions());
            if (tagBundleKey != null) {
                builder.addTag(tagBundleKey, captureStage.getId());
            }
            builder.addCameraCaptureCallback(this.mMetadataMatchingCaptureCallback);
            list.add(builder.build());
        }
        return this.submitStillCaptureRequest(list);
    }
    
    void lockFlashMode() {
        synchronized (this.mLockedFlashMode) {
            if (this.mLockedFlashMode.get() != null) {
                return;
            }
            this.mLockedFlashMode.set(this.getFlashMode());
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    public void onAttached() {
        final ImageCaptureConfig imageCaptureConfig = (ImageCaptureConfig)this.getCurrentConfig();
        this.mCaptureConfig = CaptureConfig.Builder.createFrom(imageCaptureConfig).build();
        this.mCaptureProcessor = imageCaptureConfig.getCaptureProcessor(null);
        this.mMaxCaptureStages = imageCaptureConfig.getMaxCaptureStages(2);
        this.mCaptureBundle = imageCaptureConfig.getCaptureBundle(CaptureBundles.singleDefaultCaptureBundle());
        this.mUseSoftwareJpeg = imageCaptureConfig.isSoftwareJpegEncoderRequested();
        Preconditions.checkNotNull(this.getCamera(), "Attached camera cannot be null");
        this.mExecutor = Executors.newFixedThreadPool(1, new ThreadFactory(this) {
            private final AtomicInteger mId = new AtomicInteger(0);
            final ImageCapture this$0;
            
            @Override
            public Thread newThread(@NonNull final Runnable target) {
                final StringBuilder sb = new StringBuilder();
                sb.append("CameraX-image_capture_");
                sb.append(this.mId.getAndIncrement());
                return new Thread(target, sb.toString());
            }
        });
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    protected void onCameraControlReady() {
        this.trySetFlashModeToCameraControl();
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    public void onDetached() {
        final ListenableFuture<Void> mImageReaderCloseFuture = this.mImageReaderCloseFuture;
        this.abortImageCaptureRequests();
        this.clearPipeline();
        this.mUseSoftwareJpeg = false;
        final ExecutorService mExecutor = this.mExecutor;
        Objects.requireNonNull(mExecutor);
        mImageReaderCloseFuture.addListener((Runnable)new \u3007\u3007\u30070\u3007\u30070(mExecutor), CameraXExecutors.directExecutor());
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    protected UseCaseConfig<?> onMergeConfig(@NonNull final CameraInfoInternal cameraInfoInternal, @NonNull final UseCaseConfig.Builder<?, ?, ?> builder) {
        final Object useCaseConfig = builder.getUseCaseConfig();
        final Config.Option<CaptureProcessor> option_CAPTURE_PROCESSOR = ImageCaptureConfig.OPTION_CAPTURE_PROCESSOR;
        if (((ReadableConfig)useCaseConfig).retrieveOption(option_CAPTURE_PROCESSOR, (Object)null) != null && Build$VERSION.SDK_INT >= 29) {
            Logger.i("ImageCapture", "Requesting software JPEG due to a CaptureProcessor is set.");
            builder.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_USE_SOFTWARE_JPEG_ENCODER, Boolean.TRUE);
        }
        else if (cameraInfoInternal.getCameraQuirks().contains(SoftwareJpegEncodingPreferredQuirk.class)) {
            final Boolean false = Boolean.FALSE;
            final MutableConfig mutableConfig = builder.getMutableConfig();
            final Config.Option<Boolean> option_USE_SOFTWARE_JPEG_ENCODER = ImageCaptureConfig.OPTION_USE_SOFTWARE_JPEG_ENCODER;
            final Boolean true = Boolean.TRUE;
            if (false.equals(mutableConfig.retrieveOption(option_USE_SOFTWARE_JPEG_ENCODER, true))) {
                Logger.w("ImageCapture", "Device quirk suggests software JPEG encoder, but it has been explicitly disabled.");
            }
            else {
                Logger.i("ImageCapture", "Requesting software JPEG due to device quirk.");
                builder.getMutableConfig().insertOption(option_USE_SOFTWARE_JPEG_ENCODER, true);
            }
        }
        final boolean enforceSoftwareJpegConstraints = enforceSoftwareJpegConstraints(builder.getMutableConfig());
        final Integer n = builder.getMutableConfig().retrieveOption(ImageCaptureConfig.OPTION_BUFFER_FORMAT, null);
        final boolean b = false;
        int intValue = 35;
        if (n != null) {
            Preconditions.checkArgument(builder.getMutableConfig().retrieveOption((Config.Option<Object>)option_CAPTURE_PROCESSOR, null) == null, (Object)"Cannot set buffer format with CaptureProcessor defined.");
            final MutableConfig mutableConfig2 = builder.getMutableConfig();
            final Config.Option<Integer> option_INPUT_FORMAT = ImageInputConfig.OPTION_INPUT_FORMAT;
            if (!enforceSoftwareJpegConstraints) {
                intValue = n;
            }
            mutableConfig2.insertOption(option_INPUT_FORMAT, intValue);
        }
        else if (builder.getMutableConfig().retrieveOption((Config.Option<Object>)option_CAPTURE_PROCESSOR, null) == null && !enforceSoftwareJpegConstraints) {
            final List<Pair<Integer, Size[]>> list = builder.getMutableConfig().retrieveOption(ImageOutputConfig.OPTION_SUPPORTED_RESOLUTIONS, null);
            if (list == null) {
                builder.getMutableConfig().insertOption(ImageInputConfig.OPTION_INPUT_FORMAT, 256);
            }
            else if (isImageFormatSupported(list, 256)) {
                builder.getMutableConfig().insertOption(ImageInputConfig.OPTION_INPUT_FORMAT, 256);
            }
            else if (isImageFormatSupported(list, 35)) {
                builder.getMutableConfig().insertOption(ImageInputConfig.OPTION_INPUT_FORMAT, 35);
            }
        }
        else {
            builder.getMutableConfig().insertOption(ImageInputConfig.OPTION_INPUT_FORMAT, 35);
        }
        final Integer n2 = builder.getMutableConfig().retrieveOption(ImageCaptureConfig.OPTION_MAX_CAPTURE_STAGES, 2);
        Preconditions.checkNotNull(n2, "Maximum outstanding image count must be at least 1");
        boolean b2 = b;
        if (n2 >= 1) {
            b2 = true;
        }
        Preconditions.checkArgument(b2, (Object)"Maximum outstanding image count must be at least 1");
        return (UseCaseConfig<?>)builder.getUseCaseConfig();
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @UiThread
    @Override
    public void onStateDetached() {
        this.abortImageCaptureRequests();
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    protected Size onSuggestedResolutionUpdated(@NonNull final Size size) {
        final SessionConfig.Builder pipeline = this.createPipeline(this.getCameraId(), (ImageCaptureConfig)this.getCurrentConfig(), size);
        this.mSessionConfigBuilder = pipeline;
        this.updateSessionConfig(pipeline.build());
        this.notifyActive();
        return size;
    }
    
    public void setCropAspectRatio(@NonNull final Rational mCropAspectRatio) {
        this.mCropAspectRatio = mCropAspectRatio;
    }
    
    public void setFlashMode(final int n) {
        if (n != 0 && n != 1 && n != 2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid flash mode: ");
            sb.append(n);
            throw new IllegalArgumentException(sb.toString());
        }
        synchronized (this.mLockedFlashMode) {
            this.mFlashMode = n;
            this.trySetFlashModeToCameraControl();
        }
    }
    
    public void setTargetRotation(final int targetRotationInternal) {
        final int targetRotation = this.getTargetRotation();
        if (this.setTargetRotationInternal(targetRotationInternal) && this.mCropAspectRatio != null) {
            this.mCropAspectRatio = ImageUtil.getRotatedAspectRatio(Math.abs(CameraOrientationUtil.surfaceRotationToDegrees(targetRotationInternal) - CameraOrientationUtil.surfaceRotationToDegrees(targetRotation)), this.mCropAspectRatio);
        }
    }
    
    @MainThread
    ListenableFuture<Void> submitStillCaptureRequest(@NonNull final List<CaptureConfig> list) {
        Threads.checkMainThread();
        return Futures.transform(this.getCameraControl().submitStillCaptureRequests(list, this.mCaptureMode, this.mFlashType), (Function<? super List<Void>, ? extends Void>)new Oo8Oo00oo(), CameraXExecutors.directExecutor());
    }
    
    public void takePicture(@NonNull final OutputFileOptions outputFileOptions, @NonNull final Executor executor, @NonNull final OnImageSavedCallback onImageSavedCallback) {
        if (Looper.getMainLooper() != Looper.myLooper()) {
            CameraXExecutors.mainThreadExecutor().execute(new OOO\u3007O0(this, outputFileOptions, executor, onImageSavedCallback));
            return;
        }
        if (this.isNodeEnabled()) {
            this.takePictureWithNode(executor, null, onImageSavedCallback, outputFileOptions);
            return;
        }
        this.sendImageCaptureRequest(CameraXExecutors.mainThreadExecutor(), (OnImageCapturedCallback)new OnImageCapturedCallback(this, outputFileOptions, this.getJpegQualityInternal(), executor, new ImageSaver.OnImageSavedCallback(this, onImageSavedCallback) {
            final ImageCapture this$0;
            final ImageCapture.OnImageSavedCallback val$imageSavedCallback;
            
            @Override
            public void onError(@NonNull final SaveError saveError, @NonNull final String s, @Nullable final Throwable t) {
                boolean b;
                if (saveError == SaveError.FILE_IO_FAILED) {
                    b = true;
                }
                else {
                    b = false;
                }
                this.val$imageSavedCallback.onError(new ImageCaptureException((int)(b ? 1 : 0), s, t));
            }
            
            @Override
            public void onImageSaved(@NonNull final OutputFileResults outputFileResults) {
                this.val$imageSavedCallback.onImageSaved(outputFileResults);
            }
        }, onImageSavedCallback) {
            final ImageCapture this$0;
            final Executor val$executor;
            final OnImageSavedCallback val$imageSavedCallback;
            final ImageSaver.OnImageSavedCallback val$imageSavedCallbackWrapper;
            final OutputFileOptions val$outputFileOptions;
            final int val$outputJpegQuality;
            
            @Override
            public void onCaptureSuccess(@NonNull final ImageProxy imageProxy) {
                this.this$0.mIoExecutor.execute(new ImageSaver(imageProxy, this.val$outputFileOptions, imageProxy.getImageInfo().getRotationDegrees(), this.val$outputJpegQuality, this.val$executor, this.this$0.mSequentialIoExecutor, this.val$imageSavedCallbackWrapper));
            }
            
            @Override
            public void onError(@NonNull final ImageCaptureException ex) {
                this.val$imageSavedCallback.onError(ex);
            }
        }, true);
    }
    
    public void takePicture(@NonNull final Executor executor, @NonNull final OnImageCapturedCallback onImageCapturedCallback) {
        if (Looper.getMainLooper() != Looper.myLooper()) {
            CameraXExecutors.mainThreadExecutor().execute(new o\u3007\u30070\u3007(this, executor, onImageCapturedCallback));
            return;
        }
        if (this.isNodeEnabled()) {
            this.takePictureWithNode(executor, onImageCapturedCallback, null, null);
            return;
        }
        this.sendImageCaptureRequest(executor, onImageCapturedCallback, false);
    }
    
    @NonNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ImageCapture:");
        sb.append(this.getName());
        return sb.toString();
    }
    
    void unlockFlashMode() {
        synchronized (this.mLockedFlashMode) {
            final Integer n = this.mLockedFlashMode.getAndSet(null);
            if (n == null) {
                return;
            }
            if (n != this.getFlashMode()) {
                this.trySetFlashModeToCameraControl();
            }
        }
    }
    
    public static final class Builder implements UseCaseConfig.Builder<ImageCapture, ImageCaptureConfig, Builder>, ImageOutputConfig.Builder<Builder>, IoConfig.Builder<Builder>
    {
        private final MutableOptionsBundle mMutableConfig;
        
        public Builder() {
            this(MutableOptionsBundle.create());
        }
        
        private Builder(final MutableOptionsBundle mMutableConfig) {
            this.mMutableConfig = mMutableConfig;
            final Class<?> obj = mMutableConfig.retrieveOption(TargetConfig.OPTION_TARGET_CLASS, null);
            if (obj != null && !obj.equals(ImageCapture.class)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid target class configuration for ");
                sb.append(this);
                sb.append(": ");
                sb.append(obj);
                throw new IllegalArgumentException(sb.toString());
            }
            this.setTargetClass(ImageCapture.class);
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public static Builder fromConfig(@NonNull final Config config) {
            return new Builder(MutableOptionsBundle.from(config));
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        static Builder fromConfig(@NonNull final ImageCaptureConfig imageCaptureConfig) {
            return new Builder(MutableOptionsBundle.from(imageCaptureConfig));
        }
        
        @NonNull
        @Override
        public ImageCapture build() {
            if (this.getMutableConfig().retrieveOption(ImageOutputConfig.OPTION_TARGET_ASPECT_RATIO, null) != null && this.getMutableConfig().retrieveOption(ImageOutputConfig.OPTION_TARGET_RESOLUTION, null) != null) {
                throw new IllegalArgumentException("Cannot use both setTargetResolution and setTargetAspectRatio on the same config.");
            }
            final Integer n = this.getMutableConfig().retrieveOption(ImageCaptureConfig.OPTION_BUFFER_FORMAT, null);
            final boolean b = false;
            if (n != null) {
                Preconditions.checkArgument(this.getMutableConfig().retrieveOption(ImageCaptureConfig.OPTION_CAPTURE_PROCESSOR, null) == null, (Object)"Cannot set buffer format with CaptureProcessor defined.");
                this.getMutableConfig().insertOption(ImageInputConfig.OPTION_INPUT_FORMAT, n);
            }
            else if (this.getMutableConfig().retrieveOption(ImageCaptureConfig.OPTION_CAPTURE_PROCESSOR, null) != null) {
                this.getMutableConfig().insertOption(ImageInputConfig.OPTION_INPUT_FORMAT, 35);
            }
            else {
                this.getMutableConfig().insertOption(ImageInputConfig.OPTION_INPUT_FORMAT, 256);
            }
            final ImageCapture imageCapture = new ImageCapture(this.getUseCaseConfig());
            final Size size = this.getMutableConfig().retrieveOption(ImageOutputConfig.OPTION_TARGET_RESOLUTION, null);
            if (size != null) {
                imageCapture.setCropAspectRatio(new Rational(size.getWidth(), size.getHeight()));
            }
            final Integer n2 = this.getMutableConfig().retrieveOption(ImageCaptureConfig.OPTION_MAX_CAPTURE_STAGES, 2);
            Preconditions.checkNotNull(n2, "Maximum outstanding image count must be at least 1");
            boolean b2 = b;
            if (n2 >= 1) {
                b2 = true;
            }
            Preconditions.checkArgument(b2, (Object)"Maximum outstanding image count must be at least 1");
            Preconditions.checkNotNull((Object)this.getMutableConfig().retrieveOption((Config.Option<T>)IoConfig.OPTION_IO_EXECUTOR, (T)CameraXExecutors.ioExecutor()), "The IO executor can't be null");
            final MutableConfig mutableConfig = this.getMutableConfig();
            final Option<Integer> option_FLASH_MODE = ImageCaptureConfig.OPTION_FLASH_MODE;
            if (mutableConfig.containsOption((Config.Option<?>)option_FLASH_MODE)) {
                final Integer obj = this.getMutableConfig().retrieveOption(option_FLASH_MODE);
                if (obj != null) {
                    if (obj == 0 || obj == 1) {
                        return imageCapture;
                    }
                    if (obj == 2) {
                        return imageCapture;
                    }
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("The flash mode is not allowed to set: ");
                sb.append(obj);
                throw new IllegalArgumentException(sb.toString());
            }
            return imageCapture;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        @Override
        public MutableConfig getMutableConfig() {
            return this.mMutableConfig;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public ImageCaptureConfig getUseCaseConfig() {
            return new ImageCaptureConfig(OptionsBundle.from(this.mMutableConfig));
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setBufferFormat(final int i) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_BUFFER_FORMAT, i);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setCameraSelector(@NonNull final CameraSelector cameraSelector) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_CAMERA_SELECTOR, cameraSelector);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setCaptureBundle(@NonNull final CaptureBundle captureBundle) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_CAPTURE_BUNDLE, captureBundle);
            return this;
        }
        
        @NonNull
        public Builder setCaptureMode(final int i) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_IMAGE_CAPTURE_MODE, i);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setCaptureOptionUnpacker(@NonNull final CaptureConfig.OptionUnpacker optionUnpacker) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_CAPTURE_CONFIG_UNPACKER, optionUnpacker);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setCaptureProcessor(@NonNull final CaptureProcessor captureProcessor) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_CAPTURE_PROCESSOR, captureProcessor);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setDefaultCaptureConfig(@NonNull final CaptureConfig captureConfig) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_DEFAULT_CAPTURE_CONFIG, captureConfig);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setDefaultResolution(@NonNull final Size size) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_DEFAULT_RESOLUTION, size);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setDefaultSessionConfig(@NonNull final SessionConfig sessionConfig) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_DEFAULT_SESSION_CONFIG, sessionConfig);
            return this;
        }
        
        @NonNull
        public Builder setFlashMode(final int i) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_FLASH_MODE, i);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setFlashType(final int i) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_FLASH_TYPE, i);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setImageReaderProxyProvider(@NonNull final ImageReaderProxyProvider imageReaderProxyProvider) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_IMAGE_READER_PROXY_PROVIDER, imageReaderProxyProvider);
            return this;
        }
        
        @NonNull
        public Builder setIoExecutor(@NonNull final Executor executor) {
            this.getMutableConfig().insertOption(IoConfig.OPTION_IO_EXECUTOR, executor);
            return this;
        }
        
        @NonNull
        public Builder setJpegQuality(@IntRange(from = 1L, to = 100L) final int i) {
            Preconditions.checkArgumentInRange(i, 1, 100, "jpegQuality");
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_JPEG_COMPRESSION_QUALITY, i);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setMaxCaptureStages(final int i) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_MAX_CAPTURE_STAGES, i);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setMaxResolution(@NonNull final Size size) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_MAX_RESOLUTION, size);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setSessionOptionUnpacker(@NonNull final SessionConfig.OptionUnpacker optionUnpacker) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_SESSION_CONFIG_UNPACKER, optionUnpacker);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setSoftwareJpegEncoderRequested(final boolean b) {
            this.getMutableConfig().insertOption(ImageCaptureConfig.OPTION_USE_SOFTWARE_JPEG_ENCODER, b);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setSupportedResolutions(@NonNull final List<Pair<Integer, Size[]>> list) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_SUPPORTED_RESOLUTIONS, list);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setSurfaceOccupancyPriority(final int i) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_SURFACE_OCCUPANCY_PRIORITY, i);
            return this;
        }
        
        @NonNull
        public Builder setTargetAspectRatio(final int i) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_TARGET_ASPECT_RATIO, i);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setTargetClass(@NonNull final Class<ImageCapture> clazz) {
            this.getMutableConfig().insertOption(TargetConfig.OPTION_TARGET_CLASS, clazz);
            if (this.getMutableConfig().retrieveOption(TargetConfig.OPTION_TARGET_NAME, null) == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append(clazz.getCanonicalName());
                sb.append("-");
                sb.append(UUID.randomUUID());
                this.setTargetName(sb.toString());
            }
            return this;
        }
        
        @NonNull
        public Builder setTargetName(@NonNull final String s) {
            this.getMutableConfig().insertOption(TargetConfig.OPTION_TARGET_NAME, s);
            return this;
        }
        
        @NonNull
        public Builder setTargetResolution(@NonNull final Size size) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_TARGET_RESOLUTION, size);
            return this;
        }
        
        @NonNull
        public Builder setTargetRotation(final int i) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_TARGET_ROTATION, i);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setUseCaseEventCallback(@NonNull final EventCallback eventCallback) {
            this.getMutableConfig().insertOption(UseCaseEventConfig.OPTION_USE_CASE_EVENT_CALLBACK, eventCallback);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setZslDisabled(final boolean b) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_ZSL_DISABLED, b);
            return this;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public @interface CaptureMode {
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final class Defaults implements ConfigProvider<ImageCaptureConfig>
    {
        private static final int DEFAULT_ASPECT_RATIO = 0;
        private static final ImageCaptureConfig DEFAULT_CONFIG;
        private static final int DEFAULT_SURFACE_OCCUPANCY_PRIORITY = 4;
        
        static {
            DEFAULT_CONFIG = new ImageCapture.Builder().setSurfaceOccupancyPriority(4).setTargetAspectRatio(0).getUseCaseConfig();
        }
        
        @NonNull
        @Override
        public ImageCaptureConfig getConfig() {
            return Defaults.DEFAULT_CONFIG;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public @interface FlashMode {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public @interface FlashType {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public @interface ImageCaptureError {
    }
    
    @VisibleForTesting
    static class ImageCaptureRequest
    {
        @NonNull
        private final OnImageCapturedCallback mCallback;
        AtomicBoolean mDispatched;
        @IntRange(from = 1L, to = 100L)
        final int mJpegQuality;
        @NonNull
        private final Executor mListenerExecutor;
        final int mRotationDegrees;
        @NonNull
        private final Matrix mSensorToBufferTransformMatrix;
        private final Rational mTargetRatio;
        private final Rect mViewPortCropRect;
        
        ImageCaptureRequest(final int mRotationDegrees, @IntRange(from = 1L, to = 100L) final int mJpegQuality, final Rational mTargetRatio, @Nullable final Rect mViewPortCropRect, @NonNull final Matrix mSensorToBufferTransformMatrix, @NonNull final Executor mListenerExecutor, @NonNull final OnImageCapturedCallback mCallback) {
            boolean b = false;
            this.mDispatched = new AtomicBoolean(false);
            this.mRotationDegrees = mRotationDegrees;
            this.mJpegQuality = mJpegQuality;
            if (mTargetRatio != null) {
                Preconditions.checkArgument(mTargetRatio.isZero() ^ true, (Object)"Target ratio cannot be zero");
                if (mTargetRatio.floatValue() > 0.0f) {
                    b = true;
                }
                Preconditions.checkArgument(b, (Object)"Target ratio must be positive");
            }
            this.mTargetRatio = mTargetRatio;
            this.mViewPortCropRect = mViewPortCropRect;
            this.mSensorToBufferTransformMatrix = mSensorToBufferTransformMatrix;
            this.mListenerExecutor = mListenerExecutor;
            this.mCallback = mCallback;
        }
        
        void dispatchImage(final ImageProxy imageProxy) {
            if (!this.mDispatched.compareAndSet(false, true)) {
                imageProxy.close();
                return;
            }
            Size size = null;
            int n = 0;
            Label_0155: {
                if (ImageCapture.EXIF_ROTATION_AVAILABILITY.shouldUseExifOrientation(imageProxy)) {
                    try {
                        final ByteBuffer buffer = imageProxy.getPlanes()[0].getBuffer();
                        buffer.rewind();
                        final byte[] array = new byte[buffer.capacity()];
                        buffer.get(array);
                        final Exif fromInputStream = Exif.createFromInputStream(new ByteArrayInputStream(array));
                        buffer.rewind();
                        size = new Size(fromInputStream.getWidth(), fromInputStream.getHeight());
                        n = fromInputStream.getRotation();
                        break Label_0155;
                    }
                    catch (final IOException ex) {
                        this.notifyCallbackError(1, "Unable to parse JPEG exif", ex);
                        imageProxy.close();
                        return;
                    }
                }
                size = new Size(imageProxy.getWidth(), imageProxy.getHeight());
                n = this.mRotationDegrees;
            }
            final SettableImageProxy settableImageProxy = new SettableImageProxy(imageProxy, size, ImmutableImageInfo.create(imageProxy.getImageInfo().getTagBundle(), imageProxy.getImageInfo().getTimestamp(), n, this.mSensorToBufferTransformMatrix));
            settableImageProxy.setCropRect(ImageCapture.computeDispatchCropRect(this.mViewPortCropRect, this.mTargetRatio, this.mRotationDegrees, size, n));
            try {
                this.mListenerExecutor.execute(new \u3007\u30070o(this, settableImageProxy));
            }
            catch (final RejectedExecutionException ex2) {
                Logger.e("ImageCapture", "Unable to post to the supplied executor.");
                imageProxy.close();
            }
        }
        
        void notifyCallbackError(final int n, final String s, final Throwable t) {
            if (!this.mDispatched.compareAndSet(false, true)) {
                return;
            }
            try {
                this.mListenerExecutor.execute(new o\u30070OOo\u30070(this, n, s, t));
            }
            catch (final RejectedExecutionException ex) {
                Logger.e("ImageCapture", "Unable to post to the supplied executor.");
            }
        }
    }
    
    @VisibleForTesting
    static class ImageCaptureRequestProcessor implements OnImageCloseListener
    {
        @GuardedBy("mLock")
        ImageCaptureRequest mCurrentRequest;
        @GuardedBy("mLock")
        ListenableFuture<ImageProxy> mCurrentRequestFuture;
        @GuardedBy("mLock")
        private final ImageCaptor mImageCaptor;
        final Object mLock;
        private final int mMaxImages;
        @GuardedBy("mLock")
        int mOutstandingImages;
        @GuardedBy("mLock")
        private final Deque<ImageCaptureRequest> mPendingRequests;
        @Nullable
        private final RequestProcessCallback mRequestProcessCallback;
        
        ImageCaptureRequestProcessor(final int n, @NonNull final ImageCaptor imageCaptor) {
            this(n, imageCaptor, null);
        }
        
        ImageCaptureRequestProcessor(final int mMaxImages, @NonNull final ImageCaptor mImageCaptor, @Nullable final RequestProcessCallback mRequestProcessCallback) {
            this.mPendingRequests = new ArrayDeque<ImageCaptureRequest>();
            this.mCurrentRequest = null;
            this.mCurrentRequestFuture = null;
            this.mOutstandingImages = 0;
            this.mLock = new Object();
            this.mMaxImages = mMaxImages;
            this.mImageCaptor = mImageCaptor;
            this.mRequestProcessCallback = mRequestProcessCallback;
        }
        
        public void cancelRequests(@NonNull final Throwable t) {
            Object o = this.mLock;
            synchronized (o) {
                final ImageCaptureRequest mCurrentRequest = this.mCurrentRequest;
                this.mCurrentRequest = null;
                final ListenableFuture<ImageProxy> mCurrentRequestFuture = this.mCurrentRequestFuture;
                this.mCurrentRequestFuture = null;
                final ArrayList list = new ArrayList(this.mPendingRequests);
                this.mPendingRequests.clear();
                monitorexit(o);
                if (mCurrentRequest != null && mCurrentRequestFuture != null) {
                    mCurrentRequest.notifyCallbackError(ImageCapture.getError(t), t.getMessage(), t);
                    ((Future)mCurrentRequestFuture).cancel(true);
                }
                o = list.iterator();
                while (((Iterator)o).hasNext()) {
                    ((ImageCaptureRequest)((Iterator)o).next()).notifyCallbackError(ImageCapture.getError(t), t.getMessage(), t);
                }
            }
        }
        
        @Override
        public void onImageClose(@NonNull final ImageProxy imageProxy) {
            synchronized (this.mLock) {
                --this.mOutstandingImages;
                CameraXExecutors.mainThreadExecutor().execute(new \u300708O8o\u30070(this));
            }
        }
        
        void processNextRequest() {
            synchronized (this.mLock) {
                if (this.mCurrentRequest != null) {
                    return;
                }
                if (this.mOutstandingImages >= this.mMaxImages) {
                    Logger.w("ImageCapture", "Too many acquire images. Close image to be able to process next.");
                    return;
                }
                final ImageCaptureRequest mCurrentRequest = this.mPendingRequests.poll();
                if (mCurrentRequest == null) {
                    return;
                }
                this.mCurrentRequest = mCurrentRequest;
                final RequestProcessCallback mRequestProcessCallback = this.mRequestProcessCallback;
                if (mRequestProcessCallback != null) {
                    mRequestProcessCallback.onPreProcessRequest(mCurrentRequest);
                }
                Futures.addCallback(this.mCurrentRequestFuture = this.mImageCaptor.capture(mCurrentRequest), new FutureCallback<ImageProxy>(this, mCurrentRequest) {
                    final ImageCaptureRequestProcessor this$0;
                    final ImageCaptureRequest val$imageCaptureRequest;
                    
                    @Override
                    public void onFailure(@NonNull final Throwable t) {
                        synchronized (this.this$0.mLock) {
                            if (!(t instanceof CancellationException)) {
                                final ImageCaptureRequest val$imageCaptureRequest = this.val$imageCaptureRequest;
                                final int error = ImageCapture.getError(t);
                                String message;
                                if (t != null) {
                                    message = t.getMessage();
                                }
                                else {
                                    message = "Unknown error";
                                }
                                val$imageCaptureRequest.notifyCallbackError(error, message, t);
                            }
                            final ImageCaptureRequestProcessor this$0 = this.this$0;
                            this$0.mCurrentRequest = null;
                            this$0.mCurrentRequestFuture = null;
                            this$0.processNextRequest();
                        }
                    }
                    
                    @Override
                    public void onSuccess(@Nullable final ImageProxy imageProxy) {
                        synchronized (this.this$0.mLock) {
                            Preconditions.checkNotNull(imageProxy);
                            final SingleCloseImageProxy singleCloseImageProxy = new SingleCloseImageProxy(imageProxy);
                            singleCloseImageProxy.addOnImageCloseListener((ForwardingImageProxy.OnImageCloseListener)this.this$0);
                            final ImageCaptureRequestProcessor this$0 = this.this$0;
                            ++this$0.mOutstandingImages;
                            this.val$imageCaptureRequest.dispatchImage(singleCloseImageProxy);
                            final ImageCaptureRequestProcessor this$2 = this.this$0;
                            this$2.mCurrentRequest = null;
                            this$2.mCurrentRequestFuture = null;
                            this$2.processNextRequest();
                        }
                    }
                }, CameraXExecutors.mainThreadExecutor());
            }
        }
        
        @NonNull
        public List<ImageCaptureRequest> pullOutUnfinishedRequests() {
            synchronized (this.mLock) {
                final ArrayList list = new ArrayList(this.mPendingRequests);
                this.mPendingRequests.clear();
                final ImageCaptureRequest mCurrentRequest = this.mCurrentRequest;
                this.mCurrentRequest = null;
                if (mCurrentRequest != null) {
                    final ListenableFuture<ImageProxy> mCurrentRequestFuture = this.mCurrentRequestFuture;
                    if (mCurrentRequestFuture != null && ((Future)mCurrentRequestFuture).cancel(true)) {
                        list.add(0, mCurrentRequest);
                    }
                }
                return list;
            }
        }
        
        public void sendRequest(@NonNull final ImageCaptureRequest imageCaptureRequest) {
            synchronized (this.mLock) {
                this.mPendingRequests.offer(imageCaptureRequest);
                final Locale us = Locale.US;
                int i;
                if (this.mCurrentRequest != null) {
                    i = 1;
                }
                else {
                    i = 0;
                }
                Logger.d("ImageCapture", String.format(us, "Send image capture request [current, pending] = [%d, %d]", i, this.mPendingRequests.size()));
                this.processNextRequest();
            }
        }
        
        interface ImageCaptor
        {
            @NonNull
            ListenableFuture<ImageProxy> capture(@NonNull final ImageCaptureRequest p0);
        }
        
        interface RequestProcessCallback
        {
            void onPreProcessRequest(@NonNull final ImageCaptureRequest p0);
        }
    }
    
    public static final class Metadata
    {
        private boolean mIsReversedHorizontal;
        private boolean mIsReversedHorizontalSet;
        private boolean mIsReversedVertical;
        @Nullable
        private Location mLocation;
        
        public Metadata() {
            this.mIsReversedHorizontalSet = false;
        }
        
        @Nullable
        public Location getLocation() {
            return this.mLocation;
        }
        
        public boolean isReversedHorizontal() {
            return this.mIsReversedHorizontal;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public boolean isReversedHorizontalSet() {
            return this.mIsReversedHorizontalSet;
        }
        
        public boolean isReversedVertical() {
            return this.mIsReversedVertical;
        }
        
        public void setLocation(@Nullable final Location mLocation) {
            this.mLocation = mLocation;
        }
        
        public void setReversedHorizontal(final boolean mIsReversedHorizontal) {
            this.mIsReversedHorizontal = mIsReversedHorizontal;
            this.mIsReversedHorizontalSet = true;
        }
        
        public void setReversedVertical(final boolean mIsReversedVertical) {
            this.mIsReversedVertical = mIsReversedVertical;
        }
    }
    
    public abstract static class OnImageCapturedCallback
    {
        public void onCaptureSuccess(@NonNull final ImageProxy imageProxy) {
        }
        
        public void onError(@NonNull final ImageCaptureException ex) {
        }
    }
    
    public interface OnImageSavedCallback
    {
        void onError(@NonNull final ImageCaptureException p0);
        
        void onImageSaved(@NonNull final OutputFileResults p0);
    }
    
    public static final class OutputFileOptions
    {
        @Nullable
        private final ContentResolver mContentResolver;
        @Nullable
        private final ContentValues mContentValues;
        @Nullable
        private final File mFile;
        @NonNull
        private final Metadata mMetadata;
        @Nullable
        private final OutputStream mOutputStream;
        @Nullable
        private final Uri mSaveCollection;
        
        OutputFileOptions(@Nullable final File mFile, @Nullable final ContentResolver mContentResolver, @Nullable final Uri mSaveCollection, @Nullable final ContentValues mContentValues, @Nullable final OutputStream mOutputStream, @Nullable final Metadata metadata) {
            this.mFile = mFile;
            this.mContentResolver = mContentResolver;
            this.mSaveCollection = mSaveCollection;
            this.mContentValues = mContentValues;
            this.mOutputStream = mOutputStream;
            Metadata mMetadata = metadata;
            if (metadata == null) {
                mMetadata = new Metadata();
            }
            this.mMetadata = mMetadata;
        }
        
        @Nullable
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public ContentResolver getContentResolver() {
            return this.mContentResolver;
        }
        
        @Nullable
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public ContentValues getContentValues() {
            return this.mContentValues;
        }
        
        @Nullable
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public File getFile() {
            return this.mFile;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Metadata getMetadata() {
            return this.mMetadata;
        }
        
        @Nullable
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public OutputStream getOutputStream() {
            return this.mOutputStream;
        }
        
        @Nullable
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Uri getSaveCollection() {
            return this.mSaveCollection;
        }
        
        public static final class Builder
        {
            @Nullable
            private ContentResolver mContentResolver;
            @Nullable
            private ContentValues mContentValues;
            @Nullable
            private File mFile;
            @Nullable
            private Metadata mMetadata;
            @Nullable
            private OutputStream mOutputStream;
            @Nullable
            private Uri mSaveCollection;
            
            public Builder(@NonNull final ContentResolver mContentResolver, @NonNull final Uri mSaveCollection, @NonNull final ContentValues mContentValues) {
                this.mContentResolver = mContentResolver;
                this.mSaveCollection = mSaveCollection;
                this.mContentValues = mContentValues;
            }
            
            public Builder(@NonNull final File mFile) {
                this.mFile = mFile;
            }
            
            public Builder(@NonNull final OutputStream mOutputStream) {
                this.mOutputStream = mOutputStream;
            }
            
            @NonNull
            public OutputFileOptions build() {
                return new OutputFileOptions(this.mFile, this.mContentResolver, this.mSaveCollection, this.mContentValues, this.mOutputStream, this.mMetadata);
            }
            
            @NonNull
            public Builder setMetadata(@NonNull final Metadata mMetadata) {
                this.mMetadata = mMetadata;
                return this;
            }
        }
    }
    
    public static class OutputFileResults
    {
        @Nullable
        private final Uri mSavedUri;
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public OutputFileResults(@Nullable final Uri mSavedUri) {
            this.mSavedUri = mSavedUri;
        }
        
        @Nullable
        public Uri getSavedUri() {
            return this.mSavedUri;
        }
    }
}

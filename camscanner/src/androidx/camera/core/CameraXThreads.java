// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public final class CameraXThreads
{
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final String TAG = "CameraX-";
    
    private CameraXThreads() {
    }
}

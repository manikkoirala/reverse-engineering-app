// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.RequiresOptIn;
import androidx.annotation.RequiresApi;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.CLASS)
@RequiresApi(21)
@RequiresOptIn
public @interface ExperimentalGetImage {
}

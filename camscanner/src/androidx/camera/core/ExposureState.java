// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import android.util.Rational;
import androidx.annotation.NonNull;
import android.util.Range;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface ExposureState
{
    int getExposureCompensationIndex();
    
    @NonNull
    Range<Integer> getExposureCompensationRange();
    
    @NonNull
    Rational getExposureCompensationStep();
    
    boolean isExposureCompensationSupported();
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.camera.core.impl.DeferrableSurface;
import androidx.camera.core.internal.utils.UseCaseConfigUtil;
import androidx.annotation.CallSuper;
import java.util.Iterator;
import androidx.camera.core.internal.TargetConfig;
import androidx.camera.core.impl.MutableOptionsBundle;
import androidx.camera.core.impl.CameraInfoInternal;
import androidx.camera.core.impl.Config;
import android.annotation.SuppressLint;
import androidx.annotation.IntRange;
import java.util.Objects;
import androidx.camera.core.impl.UseCaseConfigFactory;
import androidx.core.util.Preconditions;
import androidx.camera.core.impl.CameraControlInternal;
import androidx.camera.core.impl.ImageOutputConfig;
import androidx.annotation.RestrictTo;
import java.util.HashSet;
import android.graphics.Rect;
import java.util.Set;
import android.graphics.Matrix;
import androidx.annotation.Nullable;
import androidx.camera.core.impl.UseCaseConfig;
import androidx.annotation.GuardedBy;
import androidx.camera.core.impl.CameraInternal;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.SessionConfig;
import android.util.Size;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public abstract class UseCase
{
    private Size mAttachedResolution;
    @NonNull
    private SessionConfig mAttachedSessionConfig;
    @GuardedBy("mCameraLock")
    private CameraInternal mCamera;
    @Nullable
    private UseCaseConfig<?> mCameraConfig;
    private final Object mCameraLock;
    @NonNull
    private UseCaseConfig<?> mCurrentConfig;
    @Nullable
    private UseCaseConfig<?> mExtendedConfig;
    @NonNull
    private Matrix mSensorToBufferTransformMatrix;
    private State mState;
    private final Set<StateChangeCallback> mStateChangeCallbacks;
    @NonNull
    private UseCaseConfig<?> mUseCaseConfig;
    @Nullable
    private Rect mViewPortCropRect;
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected UseCase(@NonNull final UseCaseConfig<?> useCaseConfig) {
        this.mStateChangeCallbacks = new HashSet<StateChangeCallback>();
        this.mCameraLock = new Object();
        this.mState = State.INACTIVE;
        this.mSensorToBufferTransformMatrix = new Matrix();
        this.mAttachedSessionConfig = SessionConfig.defaultEmptySessionConfig();
        this.mUseCaseConfig = useCaseConfig;
        this.mCurrentConfig = useCaseConfig;
    }
    
    private void addStateChangeCallback(@NonNull final StateChangeCallback stateChangeCallback) {
        this.mStateChangeCallbacks.add(stateChangeCallback);
    }
    
    private void removeStateChangeCallback(@NonNull final StateChangeCallback stateChangeCallback) {
        this.mStateChangeCallbacks.remove(stateChangeCallback);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected int getAppTargetRotation() {
        return ((ImageOutputConfig)this.mCurrentConfig).getAppTargetRotation(-1);
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public Size getAttachedSurfaceResolution() {
        return this.mAttachedResolution;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public CameraInternal getCamera() {
        synchronized (this.mCameraLock) {
            return this.mCamera;
        }
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected CameraControlInternal getCameraControl() {
        synchronized (this.mCameraLock) {
            final CameraInternal mCamera = this.mCamera;
            if (mCamera == null) {
                return CameraControlInternal.DEFAULT_EMPTY_INSTANCE;
            }
            return mCamera.getCameraControlInternal();
        }
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected String getCameraId() {
        final CameraInternal camera = this.getCamera();
        final StringBuilder sb = new StringBuilder();
        sb.append("No camera attached to use case: ");
        sb.append(this);
        return Preconditions.checkNotNull(camera, sb.toString()).getCameraInfoInternal().getCameraId();
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public UseCaseConfig<?> getCurrentConfig() {
        return this.mCurrentConfig;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public abstract UseCaseConfig<?> getDefaultConfig(final boolean p0, @NonNull final UseCaseConfigFactory p1);
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public int getImageFormat() {
        return this.mCurrentConfig.getInputFormat();
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public String getName() {
        final UseCaseConfig<?> mCurrentConfig = this.mCurrentConfig;
        final StringBuilder sb = new StringBuilder();
        sb.append("<UnknownUseCase-");
        sb.append(this.hashCode());
        sb.append(">");
        final String targetName = mCurrentConfig.getTargetName(sb.toString());
        Objects.requireNonNull(targetName);
        return targetName;
    }
    
    @IntRange(from = 0L, to = 359L)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected int getRelativeRotation(@NonNull final CameraInternal cameraInternal) {
        return cameraInternal.getCameraInfoInternal().getSensorRotationDegrees(this.getTargetRotationInternal());
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public ResolutionInfo getResolutionInfo() {
        return this.getResolutionInfoInternal();
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected ResolutionInfo getResolutionInfoInternal() {
        final CameraInternal camera = this.getCamera();
        final Size attachedSurfaceResolution = this.getAttachedSurfaceResolution();
        if (camera != null && attachedSurfaceResolution != null) {
            Rect viewPortCropRect;
            if ((viewPortCropRect = this.getViewPortCropRect()) == null) {
                viewPortCropRect = new Rect(0, 0, attachedSurfaceResolution.getWidth(), attachedSurfaceResolution.getHeight());
            }
            return ResolutionInfo.create(attachedSurfaceResolution, viewPortCropRect, this.getRelativeRotation(camera));
        }
        return null;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public Matrix getSensorToBufferTransformMatrix() {
        return this.mSensorToBufferTransformMatrix;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public SessionConfig getSessionConfig() {
        return this.mAttachedSessionConfig;
    }
    
    @SuppressLint({ "WrongConstant" })
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected int getTargetRotationInternal() {
        return ((ImageOutputConfig)this.mCurrentConfig).getTargetRotation(0);
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public abstract UseCaseConfig.Builder<?, ?, ?> getUseCaseConfigBuilder(@NonNull final Config p0);
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public Rect getViewPortCropRect() {
        return this.mViewPortCropRect;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected boolean isCurrentCamera(@NonNull final String a) {
        return this.getCamera() != null && Objects.equals(a, this.getCameraId());
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public UseCaseConfig<?> mergeConfigs(@NonNull final CameraInfoInternal cameraInfoInternal, @Nullable final UseCaseConfig<?> useCaseConfig, @Nullable final UseCaseConfig<?> useCaseConfig2) {
        MutableOptionsBundle mutableOptionsBundle;
        if (useCaseConfig2 != null) {
            mutableOptionsBundle = MutableOptionsBundle.from(useCaseConfig2);
            mutableOptionsBundle.removeOption(TargetConfig.OPTION_TARGET_NAME);
        }
        else {
            mutableOptionsBundle = MutableOptionsBundle.create();
        }
        for (final Config.Option option : this.mUseCaseConfig.listOptions()) {
            mutableOptionsBundle.insertOption(option, this.mUseCaseConfig.getOptionPriority(option), (Object)this.mUseCaseConfig.retrieveOption((Config.Option<ValueT>)option));
        }
        if (useCaseConfig != null) {
            for (final Config.Option option2 : useCaseConfig.listOptions()) {
                if (option2.getId().equals(TargetConfig.OPTION_TARGET_NAME.getId())) {
                    continue;
                }
                mutableOptionsBundle.insertOption(option2, useCaseConfig.getOptionPriority(option2), (ValueT)useCaseConfig.retrieveOption((Config.Option<ValueT>)option2));
            }
        }
        if (mutableOptionsBundle.containsOption(ImageOutputConfig.OPTION_TARGET_RESOLUTION)) {
            final Config.Option<Integer> option_TARGET_ASPECT_RATIO = ImageOutputConfig.OPTION_TARGET_ASPECT_RATIO;
            if (mutableOptionsBundle.containsOption(option_TARGET_ASPECT_RATIO)) {
                mutableOptionsBundle.removeOption((Config.Option<Object>)option_TARGET_ASPECT_RATIO);
            }
        }
        return this.onMergeConfig(cameraInfoInternal, this.getUseCaseConfigBuilder(mutableOptionsBundle));
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected final void notifyActive() {
        this.mState = State.ACTIVE;
        this.notifyState();
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected final void notifyInactive() {
        this.mState = State.INACTIVE;
        this.notifyState();
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected final void notifyReset() {
        final Iterator<StateChangeCallback> iterator = this.mStateChangeCallbacks.iterator();
        while (iterator.hasNext()) {
            iterator.next().onUseCaseReset(this);
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public final void notifyState() {
        final int n = UseCase$1.$SwitchMap$androidx$camera$core$UseCase$State[this.mState.ordinal()];
        if (n != 1) {
            if (n == 2) {
                final Iterator<StateChangeCallback> iterator = this.mStateChangeCallbacks.iterator();
                while (iterator.hasNext()) {
                    iterator.next().onUseCaseActive(this);
                }
            }
        }
        else {
            final Iterator<StateChangeCallback> iterator2 = this.mStateChangeCallbacks.iterator();
            while (iterator2.hasNext()) {
                iterator2.next().onUseCaseInactive(this);
            }
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected final void notifyUpdated() {
        final Iterator<StateChangeCallback> iterator = this.mStateChangeCallbacks.iterator();
        while (iterator.hasNext()) {
            iterator.next().onUseCaseUpdated(this);
        }
    }
    
    @SuppressLint({ "WrongConstant" })
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void onAttach(@NonNull final CameraInternal mCamera, @Nullable final UseCaseConfig<?> mExtendedConfig, @Nullable final UseCaseConfig<?> mCameraConfig) {
        synchronized (this.mCameraLock) {
            this.addStateChangeCallback((StateChangeCallback)(this.mCamera = mCamera));
            monitorexit(this.mCameraLock);
            this.mExtendedConfig = mExtendedConfig;
            this.mCameraConfig = mCameraConfig;
            final UseCaseConfig<?> mergeConfigs = this.mergeConfigs(mCamera.getCameraInfoInternal(), this.mExtendedConfig, this.mCameraConfig);
            this.mCurrentConfig = mergeConfigs;
            final EventCallback useCaseEventCallback = mergeConfigs.getUseCaseEventCallback(null);
            if (useCaseEventCallback != null) {
                useCaseEventCallback.onAttach(mCamera.getCameraInfoInternal());
            }
            this.onAttached();
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void onAttached() {
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected void onCameraControlReady() {
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public void onDetach(@NonNull final CameraInternal cameraInternal) {
        this.onDetached();
        final EventCallback useCaseEventCallback = this.mCurrentConfig.getUseCaseEventCallback(null);
        if (useCaseEventCallback != null) {
            useCaseEventCallback.onDetach();
        }
        synchronized (this.mCameraLock) {
            Preconditions.checkArgument(cameraInternal == this.mCamera);
            this.removeStateChangeCallback((StateChangeCallback)this.mCamera);
            this.mCamera = null;
            monitorexit(this.mCameraLock);
            this.mAttachedResolution = null;
            this.mViewPortCropRect = null;
            this.mCurrentConfig = this.mUseCaseConfig;
            this.mExtendedConfig = null;
            this.mCameraConfig = null;
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void onDetached() {
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected UseCaseConfig<?> onMergeConfig(@NonNull final CameraInfoInternal cameraInfoInternal, @NonNull final UseCaseConfig.Builder<?, ?, ?> builder) {
        return (UseCaseConfig<?>)builder.getUseCaseConfig();
    }
    
    @CallSuper
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void onStateAttached() {
        this.onCameraControlReady();
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void onStateDetached() {
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected abstract Size onSuggestedResolutionUpdated(@NonNull final Size p0);
    
    @CallSuper
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void setSensorToBufferTransformMatrix(@NonNull final Matrix matrix) {
        this.mSensorToBufferTransformMatrix = new Matrix(matrix);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected boolean setTargetRotationInternal(final int n) {
        final int targetRotation = ((ImageOutputConfig)this.getCurrentConfig()).getTargetRotation(-1);
        if (targetRotation != -1 && targetRotation == n) {
            return false;
        }
        final UseCaseConfig.Builder<?, ?, ?> useCaseConfigBuilder = this.getUseCaseConfigBuilder(this.mUseCaseConfig);
        UseCaseConfigUtil.updateTargetRotationAndRelatedConfigs(useCaseConfigBuilder, n);
        this.mUseCaseConfig = (UseCaseConfig<?>)useCaseConfigBuilder.getUseCaseConfig();
        final CameraInternal camera = this.getCamera();
        if (camera == null) {
            this.mCurrentConfig = this.mUseCaseConfig;
        }
        else {
            this.mCurrentConfig = this.mergeConfigs(camera.getCameraInfoInternal(), this.mExtendedConfig, this.mCameraConfig);
        }
        return true;
    }
    
    @CallSuper
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void setViewPortCropRect(@NonNull final Rect mViewPortCropRect) {
        this.mViewPortCropRect = mViewPortCropRect;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected void updateSessionConfig(@NonNull final SessionConfig mAttachedSessionConfig) {
        this.mAttachedSessionConfig = mAttachedSessionConfig;
        for (final DeferrableSurface deferrableSurface : mAttachedSessionConfig.getSurfaces()) {
            if (deferrableSurface.getContainerClass() == null) {
                deferrableSurface.setContainerClass(this.getClass());
            }
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void updateSuggestedResolution(@NonNull final Size size) {
        this.mAttachedResolution = this.onSuggestedResolutionUpdated(size);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public interface EventCallback
    {
        void onAttach(@NonNull final CameraInfo p0);
        
        void onDetach();
    }
    
    enum State
    {
        private static final State[] $VALUES;
        
        ACTIVE, 
        INACTIVE;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public interface StateChangeCallback
    {
        void onUseCaseActive(@NonNull final UseCase p0);
        
        void onUseCaseInactive(@NonNull final UseCase p0);
        
        void onUseCaseReset(@NonNull final UseCase p0);
        
        void onUseCaseUpdated(@NonNull final UseCase p0);
    }
}

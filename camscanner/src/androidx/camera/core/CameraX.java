// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import android.os.BaseBundle;
import android.os.SystemClock;
import androidx.camera.core.impl.CameraValidator;
import androidx.camera.core.impl.CameraThreadConfig;
import androidx.core.util.Preconditions;
import android.app.Application;
import android.content.pm.PackageManager$NameNotFoundException;
import java.lang.reflect.InvocationTargetException;
import android.content.ComponentName;
import androidx.camera.core.impl.MetadataHolderService;
import androidx.camera.core.impl.utils.ContextUtil;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.camera.core.impl.Config;
import androidx.core.os.HandlerCompat;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.CameraDeviceSurfaceManager;
import androidx.annotation.Nullable;
import android.os.HandlerThread;
import android.os.Handler;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.camera.core.impl.UseCaseConfigFactory;
import androidx.camera.core.impl.CameraRepository;
import androidx.camera.core.impl.CameraFactory;
import java.util.concurrent.Executor;
import android.content.Context;
import androidx.annotation.GuardedBy;
import android.util.SparseArray;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;
import androidx.annotation.MainThread;

@MainThread
@RequiresApi(21)
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public final class CameraX
{
    private static final Object MIN_LOG_LEVEL_LOCK;
    private static final long RETRY_SLEEP_MILLIS = 500L;
    private static final String RETRY_TOKEN = "retry_token";
    private static final String TAG = "CameraX";
    private static final long WAIT_INITIALIZED_TIMEOUT_MILLIS = 3000L;
    @GuardedBy("MIN_LOG_LEVEL_LOCK")
    private static final SparseArray<Integer> sMinLogLevelReferenceCountMap;
    private Context mAppContext;
    private final Executor mCameraExecutor;
    private CameraFactory mCameraFactory;
    final CameraRepository mCameraRepository;
    private final CameraXConfig mCameraXConfig;
    private UseCaseConfigFactory mDefaultConfigFactory;
    private final ListenableFuture<Void> mInitInternalFuture;
    @GuardedBy("mInitializeLock")
    private InternalInitState mInitState;
    private final Object mInitializeLock;
    private final Integer mMinLogLevel;
    private final Handler mSchedulerHandler;
    @Nullable
    private final HandlerThread mSchedulerThread;
    @GuardedBy("mInitializeLock")
    private ListenableFuture<Void> mShutdownInternalFuture;
    private CameraDeviceSurfaceManager mSurfaceManager;
    
    static {
        MIN_LOG_LEVEL_LOCK = new Object();
        sMinLogLevelReferenceCountMap = new SparseArray();
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public CameraX(@NonNull final Context context, @Nullable CameraXConfig.Provider configProvider) {
        this.mCameraRepository = new CameraRepository();
        this.mInitializeLock = new Object();
        this.mInitState = InternalInitState.UNINITIALIZED;
        this.mShutdownInternalFuture = Futures.immediateFuture((Void)null);
        if (configProvider != null) {
            this.mCameraXConfig = configProvider.getCameraXConfig();
        }
        else {
            configProvider = getConfigProvider(context);
            if (configProvider == null) {
                throw new IllegalStateException("CameraX is not configured properly. The most likely cause is you did not include a default implementation in your build such as 'camera-camera2'.");
            }
            this.mCameraXConfig = configProvider.getCameraXConfig();
        }
        final Executor cameraExecutor = this.mCameraXConfig.getCameraExecutor(null);
        final Handler schedulerHandler = this.mCameraXConfig.getSchedulerHandler(null);
        Executor mCameraExecutor = cameraExecutor;
        if (cameraExecutor == null) {
            mCameraExecutor = new CameraExecutor();
        }
        this.mCameraExecutor = mCameraExecutor;
        if (schedulerHandler == null) {
            final HandlerThread mSchedulerThread = new HandlerThread("CameraX-scheduler", 10);
            ((Thread)(this.mSchedulerThread = mSchedulerThread)).start();
            this.mSchedulerHandler = HandlerCompat.createAsync(mSchedulerThread.getLooper());
        }
        else {
            this.mSchedulerThread = null;
            this.mSchedulerHandler = schedulerHandler;
        }
        increaseMinLogLevelReference(this.mMinLogLevel = (Integer)this.mCameraXConfig.retrieveOption(CameraXConfig.OPTION_MIN_LOGGING_LEVEL, null));
        this.mInitInternalFuture = this.initInternal(context);
    }
    
    private static void decreaseMinLogLevelReference(@Nullable final Integer n) {
        final Object min_LOG_LEVEL_LOCK = CameraX.MIN_LOG_LEVEL_LOCK;
        monitorenter(min_LOG_LEVEL_LOCK);
        Label_0013: {
            if (n != null) {
                break Label_0013;
            }
            try {
                monitorexit(min_LOG_LEVEL_LOCK);
                return;
            Label_0061_Outer:
                while (true) {
                    final SparseArray<Integer> sMinLogLevelReferenceCountMap;
                    sMinLogLevelReferenceCountMap.remove((int)n);
                    while (true) {
                        updateOrResetMinLogLevel();
                        return;
                        Label_0049: {
                            final int i;
                            sMinLogLevelReferenceCountMap.put((int)n, (Object)i);
                        }
                        continue;
                    }
                    sMinLogLevelReferenceCountMap = CameraX.sMinLogLevelReferenceCountMap;
                    final int i = (int)sMinLogLevelReferenceCountMap.get((int)n) - 1;
                    iftrue(Label_0049:)(i != 0);
                    continue Label_0061_Outer;
                }
            }
            finally {
                monitorexit(min_LOG_LEVEL_LOCK);
            }
        }
    }
    
    @Nullable
    private static CameraXConfig.Provider getConfigProvider(@NonNull Context className) {
        final Application applicationFromContext = ContextUtil.getApplicationFromContext((Context)className);
        if (applicationFromContext instanceof CameraXConfig.Provider) {
            className = (NullPointerException)applicationFromContext;
        }
        else {
            final NullPointerException ex = null;
            try {
                className = (NullPointerException)ContextUtil.getApplicationContext((Context)className);
                className = (NullPointerException)((Context)className).getPackageManager().getServiceInfo(new ComponentName((Context)className, (Class)MetadataHolderService.class), 640).metaData;
                if (className != null) {
                    className = (NullPointerException)((BaseBundle)className).getString("androidx.camera.core.impl.MetadataHolderService.DEFAULT_CONFIG_PROVIDER");
                }
                else {
                    className = null;
                }
                if (className == null) {
                    Logger.e("CameraX", "No default CameraXConfig.Provider specified in meta-data. The most likely cause is you did not include a default implementation in your build such as 'camera-camera2'.");
                    return null;
                }
                className = (NullPointerException)Class.forName((String)className).getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
                return (CameraXConfig.Provider)className;
            }
            catch (final NullPointerException className) {}
            catch (final IllegalAccessException className) {}
            catch (final NoSuchMethodException className) {}
            catch (final InvocationTargetException className) {}
            catch (final InstantiationException className) {}
            catch (final ClassNotFoundException className) {}
            catch (final PackageManager$NameNotFoundException ex2) {}
            Logger.e("CameraX", "Failed to retrieve default CameraXConfig.Provider from meta-data", className);
            className = ex;
        }
        return (CameraXConfig.Provider)className;
    }
    
    private static void increaseMinLogLevelReference(@Nullable final Integer n) {
        final Object min_LOG_LEVEL_LOCK = CameraX.MIN_LOG_LEVEL_LOCK;
        monitorenter(min_LOG_LEVEL_LOCK);
        Label_0013: {
            if (n != null) {
                break Label_0013;
            }
            try {
                monitorexit(min_LOG_LEVEL_LOCK);
                return;
                while (true) {
                    final SparseArray<Integer> sMinLogLevelReferenceCountMap;
                    int i = 0;
                    sMinLogLevelReferenceCountMap.put((int)n, (Object)i);
                    updateOrResetMinLogLevel();
                    return;
                    Preconditions.checkArgumentInRange(n, 3, 6, "minLogLevel");
                    sMinLogLevelReferenceCountMap = CameraX.sMinLogLevelReferenceCountMap;
                    final Object value = sMinLogLevelReferenceCountMap.get((int)n);
                    i = 1;
                    iftrue(Label_0066:)(value == null);
                    i = 1 + (int)sMinLogLevelReferenceCountMap.get((int)n);
                    continue;
                }
            }
            finally {
                monitorexit(min_LOG_LEVEL_LOCK);
            }
        }
    }
    
    private void initAndRetryRecursively(@NonNull final Executor executor, final long n, @NonNull final Context context, @NonNull final CallbackToFutureAdapter.Completer<Void> completer) {
        executor.execute(new OO0o\u3007\u3007\u3007\u30070(this, context, executor, completer, n));
    }
    
    private ListenableFuture<Void> initInternal(@NonNull final Context context) {
        synchronized (this.mInitializeLock) {
            Preconditions.checkState(this.mInitState == InternalInitState.UNINITIALIZED, "CameraX.initInternal() should only be called once per instance");
            this.mInitState = InternalInitState.INITIALIZING;
            return CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new \u30078o8o\u3007(this, context));
        }
    }
    
    private void setStateToInitialized() {
        synchronized (this.mInitializeLock) {
            this.mInitState = InternalInitState.INITIALIZED;
        }
    }
    
    @NonNull
    private ListenableFuture<Void> shutdownInternal() {
        synchronized (this.mInitializeLock) {
            this.mSchedulerHandler.removeCallbacksAndMessages((Object)"retry_token");
            final int n = CameraX$1.$SwitchMap$androidx$camera$core$CameraX$InternalInitState[this.mInitState.ordinal()];
            if (n == 1) {
                this.mInitState = InternalInitState.SHUTDOWN;
                return Futures.immediateFuture((Void)null);
            }
            if (n != 2) {
                if (n == 3 || n == 4) {
                    this.mInitState = InternalInitState.SHUTDOWN;
                    decreaseMinLogLevelReference(this.mMinLogLevel);
                    this.mShutdownInternalFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new \u3007\u3007888(this));
                }
                return this.mShutdownInternalFuture;
            }
            throw new IllegalStateException("CameraX could not be shutdown when it is initializing.");
        }
    }
    
    @GuardedBy("MIN_LOG_LEVEL_LOCK")
    private static void updateOrResetMinLogLevel() {
        final SparseArray<Integer> sMinLogLevelReferenceCountMap = CameraX.sMinLogLevelReferenceCountMap;
        if (sMinLogLevelReferenceCountMap.size() == 0) {
            Logger.resetMinLogLevel();
            return;
        }
        if (sMinLogLevelReferenceCountMap.get(3) != null) {
            Logger.setMinLogLevel(3);
        }
        else if (sMinLogLevelReferenceCountMap.get(4) != null) {
            Logger.setMinLogLevel(4);
        }
        else if (sMinLogLevelReferenceCountMap.get(5) != null) {
            Logger.setMinLogLevel(5);
        }
        else if (sMinLogLevelReferenceCountMap.get(6) != null) {
            Logger.setMinLogLevel(6);
        }
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public CameraDeviceSurfaceManager getCameraDeviceSurfaceManager() {
        final CameraDeviceSurfaceManager mSurfaceManager = this.mSurfaceManager;
        if (mSurfaceManager != null) {
            return mSurfaceManager;
        }
        throw new IllegalStateException("CameraX not initialized yet.");
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public CameraFactory getCameraFactory() {
        final CameraFactory mCameraFactory = this.mCameraFactory;
        if (mCameraFactory != null) {
            return mCameraFactory;
        }
        throw new IllegalStateException("CameraX not initialized yet.");
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public CameraRepository getCameraRepository() {
        return this.mCameraRepository;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public UseCaseConfigFactory getDefaultConfigFactory() {
        final UseCaseConfigFactory mDefaultConfigFactory = this.mDefaultConfigFactory;
        if (mDefaultConfigFactory != null) {
            return mDefaultConfigFactory;
        }
        throw new IllegalStateException("CameraX not initialized yet.");
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public ListenableFuture<Void> getInitializeFuture() {
        return this.mInitInternalFuture;
    }
    
    boolean isInitialized() {
        synchronized (this.mInitializeLock) {
            return this.mInitState == InternalInitState.INITIALIZED;
        }
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public ListenableFuture<Void> shutdown() {
        return this.shutdownInternal();
    }
    
    private enum InternalInitState
    {
        private static final InternalInitState[] $VALUES;
        
        INITIALIZED, 
        INITIALIZING, 
        INITIALIZING_ERROR, 
        SHUTDOWN, 
        UNINITIALIZED;
    }
}

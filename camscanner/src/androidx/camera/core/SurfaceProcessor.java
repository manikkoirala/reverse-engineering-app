// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public interface SurfaceProcessor
{
    void onInputSurface(@NonNull final SurfaceRequest p0);
    
    void onOutputSurface(@NonNull final SurfaceOutput p0);
}

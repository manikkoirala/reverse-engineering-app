// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.NonNull;
import android.media.ImageReader;
import androidx.camera.core.impl.ImageReaderProxy;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public final class ImageReaderProxys
{
    private ImageReaderProxys() {
    }
    
    @NonNull
    public static ImageReaderProxy createIsolatedReader(final int n, final int n2, final int n3, final int n4) {
        return new AndroidImageReaderProxy(ImageReader.newInstance(n, n2, n3, n4));
    }
}

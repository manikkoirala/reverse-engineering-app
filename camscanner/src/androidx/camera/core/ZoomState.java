// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface ZoomState
{
    float getLinearZoom();
    
    float getMaxZoomRatio();
    
    float getMinZoomRatio();
    
    float getZoomRatio();
}

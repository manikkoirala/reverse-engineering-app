// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.NonNull;
import java.util.List;
import androidx.annotation.RestrictTo;
import androidx.camera.core.impl.Identifier;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface CameraFilter
{
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final Identifier DEFAULT_ID = Identifier.create(new Object());
    
    @NonNull
    List<CameraInfo> filter(@NonNull final List<CameraInfo> p0);
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    Identifier getIdentifier();
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.util.concurrent.atomic.AtomicBoolean;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
final class SingleCloseImageProxy extends ForwardingImageProxy
{
    private final AtomicBoolean mClosed;
    
    SingleCloseImageProxy(final ImageProxy imageProxy) {
        super(imageProxy);
        this.mClosed = new AtomicBoolean(false);
    }
    
    @Override
    public void close() {
        if (!this.mClosed.getAndSet(true)) {
            super.close();
        }
    }
}

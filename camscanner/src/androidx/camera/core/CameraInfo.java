// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.lifecycle.LiveData;
import androidx.annotation.RestrictTo;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface CameraInfo
{
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final String IMPLEMENTATION_TYPE_CAMERA2 = "androidx.camera.camera2";
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final String IMPLEMENTATION_TYPE_CAMERA2_LEGACY = "androidx.camera.camera2.legacy";
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final String IMPLEMENTATION_TYPE_FAKE = "androidx.camera.fake";
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final String IMPLEMENTATION_TYPE_UNKNOWN = "<unknown>";
    
    @NonNull
    CameraSelector getCameraSelector();
    
    @NonNull
    LiveData<CameraState> getCameraState();
    
    @NonNull
    ExposureState getExposureState();
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    String getImplementationType();
    
    int getSensorRotationDegrees();
    
    int getSensorRotationDegrees(final int p0);
    
    @NonNull
    LiveData<Integer> getTorchState();
    
    @NonNull
    LiveData<ZoomState> getZoomState();
    
    boolean hasFlashUnit();
    
    boolean isFocusMeteringSupported(@NonNull final FocusMeteringAction p0);
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    boolean isPrivateReprocessingSupported();
    
    @ExperimentalZeroShutterLag
    boolean isZslSupported();
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public @interface ImplementationType {
    }
}

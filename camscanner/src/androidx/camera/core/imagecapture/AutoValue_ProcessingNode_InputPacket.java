// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.annotation.NonNull;
import androidx.camera.core.ImageProxy;

final class AutoValue_ProcessingNode_InputPacket extends InputPacket
{
    private final ImageProxy imageProxy;
    private final ProcessingRequest processingRequest;
    
    AutoValue_ProcessingNode_InputPacket(final ProcessingRequest processingRequest, final ImageProxy imageProxy) {
        if (processingRequest == null) {
            throw new NullPointerException("Null processingRequest");
        }
        this.processingRequest = processingRequest;
        if (imageProxy != null) {
            this.imageProxy = imageProxy;
            return;
        }
        throw new NullPointerException("Null imageProxy");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof InputPacket) {
            final InputPacket inputPacket = (InputPacket)o;
            if (!this.processingRequest.equals(inputPacket.getProcessingRequest()) || !this.imageProxy.equals(inputPacket.getImageProxy())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @NonNull
    @Override
    ImageProxy getImageProxy() {
        return this.imageProxy;
    }
    
    @NonNull
    @Override
    ProcessingRequest getProcessingRequest() {
        return this.processingRequest;
    }
    
    @Override
    public int hashCode() {
        return (this.processingRequest.hashCode() ^ 0xF4243) * 1000003 ^ this.imageProxy.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("InputPacket{processingRequest=");
        sb.append(this.processingRequest);
        sb.append(", imageProxy=");
        sb.append(this.imageProxy);
        sb.append("}");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import java.util.Objects;
import androidx.camera.core.ExperimentalGetImage;
import android.media.Image;
import androidx.camera.core.impl.utils.ExifData;
import androidx.camera.core.impl.TagBundle;
import androidx.core.util.Preconditions;
import java.nio.ByteBuffer;
import androidx.camera.core.processing.Packet;
import androidx.annotation.VisibleForTesting;
import androidx.camera.core.internal.utils.ImageUtil;
import android.graphics.Matrix;
import android.graphics.Bitmap;
import androidx.annotation.Nullable;
import androidx.annotation.GuardedBy;
import androidx.camera.core.ImageInfo;
import androidx.annotation.NonNull;
import android.graphics.Rect;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;
import androidx.camera.core.ImageProxy;

@RequiresApi(api = 21)
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public final class RgbaImageProxy implements ImageProxy
{
    @NonNull
    private final Rect mCropRect;
    private final int mHeight;
    @NonNull
    private final ImageInfo mImageInfo;
    private final Object mLock;
    @GuardedBy("mLock")
    @Nullable
    PlaneProxy[] mPlaneProxy;
    private final int mWidth;
    
    @VisibleForTesting
    RgbaImageProxy(@NonNull final Bitmap bitmap, @NonNull final Rect rect, final int n, @NonNull final Matrix matrix, final long n2) {
        this(ImageUtil.createDirectByteBuffer(bitmap), 4, bitmap.getWidth(), bitmap.getHeight(), rect, n, matrix, n2);
    }
    
    public RgbaImageProxy(@NonNull final Packet<Bitmap> packet) {
        this((Bitmap)packet.getData(), packet.getCropRect(), packet.getRotationDegrees(), packet.getSensorToBufferTransform(), packet.getCameraCaptureResult().getTimestamp());
    }
    
    public RgbaImageProxy(@NonNull final ByteBuffer byteBuffer, final int n, final int mWidth, final int mHeight, @NonNull final Rect mCropRect, final int n2, @NonNull final Matrix matrix, final long n3) {
        this.mLock = new Object();
        this.mWidth = mWidth;
        this.mHeight = mHeight;
        this.mCropRect = mCropRect;
        this.mImageInfo = createImageInfo(n3, n2, matrix);
        byteBuffer.rewind();
        this.mPlaneProxy = new PlaneProxy[] { createPlaneProxy(byteBuffer, mWidth * n, n) };
    }
    
    private void checkNotClosed() {
        synchronized (this.mLock) {
            Preconditions.checkState(this.mPlaneProxy != null, "The image is closed.");
        }
    }
    
    private static ImageInfo createImageInfo(final long n, final int n2, @NonNull final Matrix matrix) {
        return new ImageInfo(n, n2, matrix) {
            final int val$rotationDegrees;
            final Matrix val$sensorToBuffer;
            final long val$timestamp;
            
            @Override
            public int getRotationDegrees() {
                return this.val$rotationDegrees;
            }
            
            @NonNull
            @Override
            public Matrix getSensorToBufferTransformMatrix() {
                return new Matrix(this.val$sensorToBuffer);
            }
            
            @NonNull
            @Override
            public TagBundle getTagBundle() {
                throw new UnsupportedOperationException("Custom ImageProxy does not contain TagBundle");
            }
            
            @Override
            public long getTimestamp() {
                return this.val$timestamp;
            }
            
            @Override
            public void populateExifData(@NonNull final ExifData.Builder builder) {
                throw new UnsupportedOperationException("Custom ImageProxy does not contain Exif data.");
            }
        };
    }
    
    private static PlaneProxy createPlaneProxy(@NonNull final ByteBuffer byteBuffer, final int n, final int n2) {
        return new PlaneProxy(n, n2, byteBuffer) {
            final ByteBuffer val$byteBuffer;
            final int val$pixelStride;
            final int val$rowStride;
            
            @NonNull
            @Override
            public ByteBuffer getBuffer() {
                return this.val$byteBuffer;
            }
            
            @Override
            public int getPixelStride() {
                return this.val$pixelStride;
            }
            
            @Override
            public int getRowStride() {
                return this.val$rowStride;
            }
        };
    }
    
    @Override
    public void close() {
        synchronized (this.mLock) {
            this.checkNotClosed();
            this.mPlaneProxy = null;
        }
    }
    
    @NonNull
    public Bitmap createBitmap() {
        synchronized (this.mLock) {
            this.checkNotClosed();
            return ImageUtil.createBitmapFromPlane(this.getPlanes(), this.getWidth(), this.getHeight());
        }
    }
    
    @NonNull
    @Override
    public Rect getCropRect() {
        synchronized (this.mLock) {
            this.checkNotClosed();
            return this.mCropRect;
        }
    }
    
    @Override
    public int getFormat() {
        synchronized (this.mLock) {
            this.checkNotClosed();
            return 1;
        }
    }
    
    @Override
    public int getHeight() {
        synchronized (this.mLock) {
            this.checkNotClosed();
            return this.mHeight;
        }
    }
    
    @Nullable
    @ExperimentalGetImage
    @Override
    public Image getImage() {
        synchronized (this.mLock) {
            this.checkNotClosed();
            return null;
        }
    }
    
    @NonNull
    @Override
    public ImageInfo getImageInfo() {
        synchronized (this.mLock) {
            this.checkNotClosed();
            return this.mImageInfo;
        }
    }
    
    @NonNull
    @Override
    public PlaneProxy[] getPlanes() {
        synchronized (this.mLock) {
            this.checkNotClosed();
            final PlaneProxy[] mPlaneProxy = this.mPlaneProxy;
            Objects.requireNonNull(mPlaneProxy);
            return mPlaneProxy;
        }
    }
    
    @Override
    public int getWidth() {
        synchronized (this.mLock) {
            this.checkNotClosed();
            return this.mWidth;
        }
    }
    
    @Override
    public void setCropRect(@Nullable final Rect rect) {
        synchronized (this.mLock) {
            this.checkNotClosed();
            if (rect != null) {
                this.mCropRect.set(rect);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.core.util.Consumer;
import androidx.annotation.MainThread;
import java.util.Objects;
import androidx.core.util.Preconditions;
import androidx.camera.core.impl.utils.Threads;
import androidx.annotation.NonNull;
import androidx.camera.core.ImageProxy;
import androidx.annotation.RequiresApi;

@RequiresApi(api = 21)
class SingleBundlingNode implements BundlingNode
{
    private ProcessingNode.In mOutputEdge;
    private ProcessingRequest mPendingRequest;
    
    @MainThread
    private void matchImageWithRequest(@NonNull final ImageProxy imageProxy) {
        Threads.checkMainThread();
        final ProcessingRequest mPendingRequest = this.mPendingRequest;
        final boolean b = true;
        Preconditions.checkState(mPendingRequest != null);
        final Object tag = imageProxy.getImageInfo().getTagBundle().getTag(this.mPendingRequest.getTagBundleKey());
        Objects.requireNonNull(tag);
        Preconditions.checkState((int)tag == this.mPendingRequest.getStageIds().get(0) && b);
        this.mOutputEdge.getEdge().accept(ProcessingNode.InputPacket.of(this.mPendingRequest, imageProxy));
        this.mPendingRequest = null;
    }
    
    @MainThread
    private void trackIncomingRequest(@NonNull final ProcessingRequest mPendingRequest) {
        Threads.checkMainThread();
        final int size = mPendingRequest.getStageIds().size();
        final boolean b = false;
        Preconditions.checkState(size == 1, "Cannot handle multi-image capture.");
        boolean b2 = b;
        if (this.mPendingRequest == null) {
            b2 = true;
        }
        Preconditions.checkState(b2, "Already has an existing request.");
        this.mPendingRequest = mPendingRequest;
    }
    
    @Override
    public void release() {
    }
    
    @NonNull
    @Override
    public ProcessingNode.In transform(@NonNull final CaptureNode.Out out) {
        out.getImageEdge().setListener(new \u300780\u3007808\u3007O(this));
        out.getRequestEdge().setListener(new OO0o\u3007\u3007\u3007\u30070(this));
        return this.mOutputEdge = ProcessingNode.In.of(out.getFormat());
    }
}

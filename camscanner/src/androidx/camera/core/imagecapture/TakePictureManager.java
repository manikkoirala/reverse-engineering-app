// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.ImageProxy;
import androidx.core.util.Pair;
import java.util.Objects;
import java.util.Iterator;
import androidx.core.util.Preconditions;
import java.util.concurrent.Executor;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import androidx.annotation.MainThread;
import androidx.camera.core.impl.utils.Threads;
import java.util.ArrayDeque;
import androidx.annotation.NonNull;
import java.util.Deque;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.camera.core.ForwardingImageProxy;

@RequiresApi(api = 21)
public class TakePictureManager implements OnImageCloseListener
{
    private static final String TAG = "TakePictureManager";
    final ImageCaptureControl mImageCaptureControl;
    final ImagePipeline mImagePipeline;
    @Nullable
    @VisibleForTesting
    RequestWithCallback mInFlightRequest;
    @VisibleForTesting
    final Deque<TakePictureRequest> mNewRequests;
    boolean mPaused;
    
    @MainThread
    public TakePictureManager(@NonNull final ImageCaptureControl mImageCaptureControl, @NonNull final ImagePipeline mImagePipeline) {
        this.mNewRequests = new ArrayDeque<TakePictureRequest>();
        this.mPaused = false;
        Threads.checkMainThread();
        this.mImageCaptureControl = mImageCaptureControl;
        (this.mImagePipeline = mImagePipeline).setOnImageCloseListener(this);
    }
    
    @MainThread
    private void submitCameraRequest(@NonNull final CameraRequest cameraRequest, @NonNull final Runnable runnable) {
        Threads.checkMainThread();
        this.mImageCaptureControl.lockFlashMode();
        Futures.addCallback(this.mImageCaptureControl.submitStillCaptureRequests(cameraRequest.getCaptureConfigs()), new FutureCallback<Void>(this, runnable, cameraRequest) {
            final TakePictureManager this$0;
            final CameraRequest val$cameraRequest;
            final Runnable val$successRunnable;
            
            @Override
            public void onFailure(@NonNull final Throwable t) {
                if (t instanceof ImageCaptureException) {
                    this.val$cameraRequest.onCaptureFailure((ImageCaptureException)t);
                }
                else {
                    this.val$cameraRequest.onCaptureFailure(new ImageCaptureException(2, "Failed to submit capture request", t));
                }
                this.this$0.mImageCaptureControl.unlockFlashMode();
            }
            
            @Override
            public void onSuccess(@Nullable final Void void1) {
                this.val$successRunnable.run();
                this.this$0.mImageCaptureControl.unlockFlashMode();
            }
        }, CameraXExecutors.mainThreadExecutor());
    }
    
    private void trackCurrentRequest(@NonNull final RequestWithCallback mInFlightRequest) {
        Preconditions.checkState(this.hasInFlightRequest() ^ true);
        this.mInFlightRequest = mInFlightRequest;
        mInFlightRequest.getCaptureFuture().addListener((Runnable)new \u3007O8o08O(this), CameraXExecutors.directExecutor());
    }
    
    @MainThread
    public void abortRequests() {
        Threads.checkMainThread();
        final ImageCaptureException ex = new ImageCaptureException(3, "Camera is closed.", null);
        final Iterator<TakePictureRequest> iterator = this.mNewRequests.iterator();
        while (iterator.hasNext()) {
            iterator.next().onError(ex);
        }
        this.mNewRequests.clear();
        final RequestWithCallback mInFlightRequest = this.mInFlightRequest;
        if (mInFlightRequest != null) {
            mInFlightRequest.abort(ex);
        }
    }
    
    @VisibleForTesting
    boolean hasInFlightRequest() {
        return this.mInFlightRequest != null;
    }
    
    @MainThread
    void issueNextRequest() {
        Threads.checkMainThread();
        if (this.hasInFlightRequest()) {
            return;
        }
        if (this.mPaused) {
            return;
        }
        if (this.mImagePipeline.getCapacity() == 0) {
            return;
        }
        final TakePictureRequest takePictureRequest = this.mNewRequests.poll();
        if (takePictureRequest == null) {
            return;
        }
        final RequestWithCallback requestWithCallback = new RequestWithCallback(takePictureRequest);
        this.trackCurrentRequest(requestWithCallback);
        final Pair<CameraRequest, ProcessingRequest> requests = this.mImagePipeline.createRequests(takePictureRequest, requestWithCallback);
        final CameraRequest obj = requests.first;
        Objects.requireNonNull(obj);
        final ProcessingRequest obj2 = requests.second;
        Objects.requireNonNull(obj2);
        this.submitCameraRequest(obj, new \u30078o8o\u3007(this, obj2));
    }
    
    @MainThread
    public void offerRequest(@NonNull final TakePictureRequest takePictureRequest) {
        Threads.checkMainThread();
        this.mNewRequests.offer(takePictureRequest);
        this.issueNextRequest();
    }
    
    @Override
    public void onImageClose(@NonNull final ImageProxy imageProxy) {
        CameraXExecutors.mainThreadExecutor().execute(new OO0o\u3007\u3007(this));
    }
    
    @MainThread
    public void pause() {
        Threads.checkMainThread();
        this.mPaused = true;
    }
    
    @MainThread
    public void resume() {
        Threads.checkMainThread();
        this.mPaused = false;
        this.issueNextRequest();
    }
}

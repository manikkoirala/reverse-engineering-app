// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.annotation.MainThread;
import androidx.camera.core.impl.utils.Threads;
import androidx.camera.core.ImageCaptureException;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.CaptureConfig;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(api = 21)
public final class CameraRequest
{
    private final TakePictureCallback mCallback;
    private final List<CaptureConfig> mCaptureConfigs;
    
    public CameraRequest(@NonNull final List<CaptureConfig> mCaptureConfigs, @NonNull final TakePictureCallback mCallback) {
        this.mCaptureConfigs = mCaptureConfigs;
        this.mCallback = mCallback;
    }
    
    @NonNull
    List<CaptureConfig> getCaptureConfigs() {
        return this.mCaptureConfigs;
    }
    
    @MainThread
    void onCaptureFailure(@NonNull final ImageCaptureException ex) {
        Threads.checkMainThread();
        this.mCallback.onCaptureFailure(ex);
    }
}

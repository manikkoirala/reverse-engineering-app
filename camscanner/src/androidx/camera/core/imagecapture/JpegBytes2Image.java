// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.impl.utils.Exif;
import java.util.Objects;
import androidx.camera.core.impl.ImageReaderProxy;
import androidx.camera.core.ImageProcessingUtil;
import androidx.camera.core.SafeCloseImageReaderProxy;
import androidx.camera.core.ImageReaderProxys;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.processing.Packet;
import androidx.camera.core.processing.Operation;

@RequiresApi(api = 21)
public class JpegBytes2Image implements Operation<Packet<byte[]>, Packet<ImageProxy>>
{
    private static final int MAX_IMAGES = 2;
    
    @NonNull
    @Override
    public Packet<ImageProxy> apply(@NonNull final Packet<byte[]> packet) throws ImageCaptureException {
        final SafeCloseImageReaderProxy safeCloseImageReaderProxy = new SafeCloseImageReaderProxy(ImageReaderProxys.createIsolatedReader(packet.getSize().getWidth(), packet.getSize().getHeight(), 256, 2));
        final ImageProxy convertJpegBytesToImage = ImageProcessingUtil.convertJpegBytesToImage(safeCloseImageReaderProxy, packet.getData());
        safeCloseImageReaderProxy.safeClose();
        Objects.requireNonNull(convertJpegBytesToImage);
        final ImageProxy imageProxy = convertJpegBytesToImage;
        final Exif exif = packet.getExif();
        Objects.requireNonNull(exif);
        return Packet.of(imageProxy, exif, packet.getCropRect(), packet.getRotationDegrees(), packet.getSensorToBufferTransform(), packet.getCameraCaptureResult());
    }
}

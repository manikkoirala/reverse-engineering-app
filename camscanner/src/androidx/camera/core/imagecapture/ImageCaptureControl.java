// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import com.google.common.util.concurrent.ListenableFuture;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.CaptureConfig;
import java.util.List;
import androidx.annotation.MainThread;
import androidx.annotation.RequiresApi;

@RequiresApi(api = 21)
public interface ImageCaptureControl
{
    @MainThread
    void lockFlashMode();
    
    @MainThread
    @NonNull
    ListenableFuture<Void> submitStillCaptureRequests(@NonNull final List<CaptureConfig> p0);
    
    @MainThread
    void unlockFlashMode();
}

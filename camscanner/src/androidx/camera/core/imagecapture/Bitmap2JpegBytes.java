// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.impl.utils.Exif;
import java.util.Objects;
import java.io.OutputStream;
import android.graphics.Bitmap$CompressFormat;
import android.graphics.Bitmap;
import java.io.ByteArrayOutputStream;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.camera.core.processing.Packet;
import androidx.camera.core.processing.Operation;

@RequiresApi(api = 21)
class Bitmap2JpegBytes implements Operation<In, Packet<byte[]>>
{
    @NonNull
    @Override
    public Packet<byte[]> apply(@NonNull final In in) throws ImageCaptureException {
        final Packet<Bitmap> packet = in.getPacket();
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        packet.getData().compress(Bitmap$CompressFormat.JPEG, in.getJpegQuality(), (OutputStream)byteArrayOutputStream);
        ((Bitmap)packet.getData()).recycle();
        final byte[] byteArray = byteArrayOutputStream.toByteArray();
        final Exif exif = packet.getExif();
        Objects.requireNonNull(exif);
        return Packet.of(byteArray, exif, 256, packet.getSize(), packet.getCropRect(), packet.getRotationDegrees(), packet.getSensorToBufferTransform(), packet.getCameraCaptureResult());
    }
    
    abstract static class In
    {
        @NonNull
        static In of(@NonNull final Packet<Bitmap> packet, final int n) {
            return (In)new AutoValue_Bitmap2JpegBytes_In(packet, n);
        }
        
        abstract int getJpegQuality();
        
        abstract Packet<Bitmap> getPacket();
    }
}

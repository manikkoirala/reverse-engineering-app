// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.impl.ImmediateSurface;
import androidx.camera.core.processing.Edge;
import androidx.camera.core.impl.DeferrableSurface;
import androidx.camera.core.impl.CameraCaptureCallback;
import android.view.Surface;
import android.util.Size;
import androidx.core.util.Consumer;
import java.util.concurrent.Executor;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.MetadataImageReader;
import androidx.camera.core.ForwardingImageProxy;
import java.util.Iterator;
import java.util.Collection;
import androidx.annotation.MainThread;
import androidx.camera.core.impl.utils.Threads;
import androidx.core.util.Preconditions;
import java.util.Objects;
import androidx.camera.core.impl.ImageReaderProxy;
import java.util.HashSet;
import androidx.camera.core.SafeCloseImageReaderProxy;
import androidx.annotation.NonNull;
import androidx.camera.core.ImageProxy;
import java.util.Set;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.RequiresApi;
import androidx.camera.core.processing.Node;

@RequiresApi(api = 21)
class CaptureNode implements Node<In, Out>
{
    @VisibleForTesting
    static final int MAX_IMAGES = 4;
    private ProcessingRequest mCurrentRequest;
    private In mInputEdge;
    private Out mOutputEdge;
    private final Set<ImageProxy> mPendingImages;
    @NonNull
    private final Set<Integer> mPendingStageIds;
    SafeCloseImageReaderProxy mSafeCloseImageReaderProxy;
    
    CaptureNode() {
        this.mPendingStageIds = new HashSet<Integer>();
        this.mPendingImages = new HashSet<ImageProxy>();
        this.mCurrentRequest = null;
    }
    
    private void matchAndPropagateImage(@NonNull final ImageProxy imageProxy) {
        final Object tag = imageProxy.getImageInfo().getTagBundle().getTag(this.mCurrentRequest.getTagBundleKey());
        Objects.requireNonNull(tag);
        final int intValue = (int)tag;
        final boolean contains = this.mPendingStageIds.contains(intValue);
        final StringBuilder sb = new StringBuilder();
        sb.append("Received an unexpected stage id");
        sb.append(intValue);
        Preconditions.checkState(contains, sb.toString());
        this.mPendingStageIds.remove(intValue);
        if (this.mPendingStageIds.isEmpty()) {
            this.mCurrentRequest.onImageCaptured();
            this.mCurrentRequest = null;
        }
        this.mOutputEdge.getImageEdge().accept(imageProxy);
    }
    
    @MainThread
    public int getCapacity() {
        Threads.checkMainThread();
        Preconditions.checkState(this.mSafeCloseImageReaderProxy != null, "The ImageReader is not initialized.");
        return this.mSafeCloseImageReaderProxy.getCapacity();
    }
    
    @NonNull
    @VisibleForTesting
    In getInputEdge() {
        return this.mInputEdge;
    }
    
    @MainThread
    @VisibleForTesting
    void onImageProxyAvailable(@NonNull final ImageProxy imageProxy) {
        Threads.checkMainThread();
        if (this.mCurrentRequest == null) {
            this.mPendingImages.add(imageProxy);
        }
        else {
            this.matchAndPropagateImage(imageProxy);
        }
    }
    
    @MainThread
    @VisibleForTesting
    void onRequestAvailable(@NonNull final ProcessingRequest mCurrentRequest) {
        Threads.checkMainThread();
        final int capacity = this.getCapacity();
        final boolean b = true;
        Preconditions.checkState(capacity > 0, "Too many acquire images. Close image to be able to process next.");
        boolean b2 = b;
        if (this.mCurrentRequest != null) {
            b2 = (this.mPendingStageIds.isEmpty() && b);
        }
        Preconditions.checkState(b2, "The previous request is not complete");
        this.mCurrentRequest = mCurrentRequest;
        this.mPendingStageIds.addAll(mCurrentRequest.getStageIds());
        this.mOutputEdge.getRequestEdge().accept(mCurrentRequest);
        final Iterator<ImageProxy> iterator = this.mPendingImages.iterator();
        while (iterator.hasNext()) {
            this.matchAndPropagateImage(iterator.next());
        }
        this.mPendingImages.clear();
    }
    
    @MainThread
    @Override
    public void release() {
        Threads.checkMainThread();
        final SafeCloseImageReaderProxy mSafeCloseImageReaderProxy = this.mSafeCloseImageReaderProxy;
        if (mSafeCloseImageReaderProxy != null) {
            mSafeCloseImageReaderProxy.safeClose();
        }
        final In mInputEdge = this.mInputEdge;
        if (mInputEdge != null) {
            mInputEdge.closeSurface();
        }
    }
    
    @MainThread
    public void setOnImageCloseListener(final ForwardingImageProxy.OnImageCloseListener onImageCloseListener) {
        Threads.checkMainThread();
        Preconditions.checkState(this.mSafeCloseImageReaderProxy != null, "The ImageReader is not initialized.");
        this.mSafeCloseImageReaderProxy.setOnImageCloseListener(onImageCloseListener);
    }
    
    @NonNull
    @Override
    public Out transform(@NonNull final In mInputEdge) {
        this.mInputEdge = mInputEdge;
        final Size size = mInputEdge.getSize();
        final MetadataImageReader metadataImageReader = new MetadataImageReader(size.getWidth(), size.getHeight(), mInputEdge.getFormat(), 4);
        this.mSafeCloseImageReaderProxy = new SafeCloseImageReaderProxy(metadataImageReader);
        mInputEdge.setCameraCaptureCallback(metadataImageReader.getCameraCaptureCallback());
        final Surface surface = metadataImageReader.getSurface();
        Objects.requireNonNull(surface);
        mInputEdge.setSurface(surface);
        metadataImageReader.setOnImageAvailableListener(new \u3007080(this), CameraXExecutors.mainThreadExecutor());
        mInputEdge.getRequestEdge().setListener(new \u3007o00\u3007\u3007Oo(this));
        return this.mOutputEdge = Out.of(mInputEdge.getFormat());
    }
    
    abstract static class In
    {
        private CameraCaptureCallback mCameraCaptureCallback;
        private DeferrableSurface mSurface;
        
        @NonNull
        static In of(final Size size, final int n) {
            return (In)new AutoValue_CaptureNode_In(size, n, new Edge<ProcessingRequest>());
        }
        
        void closeSurface() {
            this.mSurface.close();
        }
        
        CameraCaptureCallback getCameraCaptureCallback() {
            return this.mCameraCaptureCallback;
        }
        
        abstract int getFormat();
        
        @NonNull
        abstract Edge<ProcessingRequest> getRequestEdge();
        
        abstract Size getSize();
        
        @NonNull
        DeferrableSurface getSurface() {
            return this.mSurface;
        }
        
        void setCameraCaptureCallback(@NonNull final CameraCaptureCallback mCameraCaptureCallback) {
            this.mCameraCaptureCallback = mCameraCaptureCallback;
        }
        
        void setSurface(@NonNull final Surface surface) {
            Preconditions.checkState(this.mSurface == null, "The surface is already set.");
            this.mSurface = new ImmediateSurface(surface);
        }
    }
    
    abstract static class Out
    {
        static Out of(final int n) {
            return (Out)new AutoValue_CaptureNode_Out(new Edge<ImageProxy>(), new Edge<ProcessingRequest>(), n);
        }
        
        abstract int getFormat();
        
        abstract Edge<ImageProxy> getImageEdge();
        
        abstract Edge<ProcessingRequest> getRequestEdge();
    }
}

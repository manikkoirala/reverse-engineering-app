// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.processing.Edge;
import androidx.core.util.Consumer;
import java.util.Objects;
import androidx.annotation.WorkerThread;
import androidx.annotation.VisibleForTesting;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.ImageCapture;
import android.graphics.Bitmap;
import androidx.camera.core.ImageProxy;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;
import androidx.camera.core.processing.Packet;
import androidx.camera.core.processing.Operation;
import androidx.annotation.RequiresApi;
import androidx.camera.core.processing.Node;

@RequiresApi(api = 21)
public class ProcessingNode implements Node<In, Void>
{
    private Operation<Bitmap2JpegBytes.In, Packet<byte[]>> mBitmap2JpegBytes;
    @NonNull
    private final Executor mBlockingExecutor;
    private Operation<Image2JpegBytes.In, Packet<byte[]>> mImage2JpegBytes;
    private Operation<InputPacket, Packet<ImageProxy>> mInput2Packet;
    private Operation<Packet<byte[]>, Packet<Bitmap>> mJpegBytes2CroppedBitmap;
    private Operation<JpegBytes2Disk.In, ImageCapture.OutputFileResults> mJpegBytes2Disk;
    private Operation<Packet<byte[]>, Packet<ImageProxy>> mJpegBytes2Image;
    private Operation<Packet<ImageProxy>, ImageProxy> mJpegImage2Result;
    
    ProcessingNode(@NonNull final Executor mBlockingExecutor) {
        this.mBlockingExecutor = mBlockingExecutor;
    }
    
    private static void sendError(@NonNull final ProcessingRequest processingRequest, @NonNull final ImageCaptureException ex) {
        CameraXExecutors.mainThreadExecutor().execute(new O8(processingRequest, ex));
    }
    
    @VisibleForTesting
    void injectJpegBytes2CroppedBitmapForTesting(@NonNull final Operation<Packet<byte[]>, Packet<Bitmap>> mJpegBytes2CroppedBitmap) {
        this.mJpegBytes2CroppedBitmap = mJpegBytes2CroppedBitmap;
    }
    
    @NonNull
    @WorkerThread
    ImageProxy processInMemoryCapture(@NonNull final InputPacket inputPacket) throws ImageCaptureException {
        final ProcessingRequest processingRequest = inputPacket.getProcessingRequest();
        Packet packet2;
        final Packet packet = packet2 = this.mInput2Packet.apply(inputPacket);
        if (packet.getFormat() == 35) {
            packet2 = this.mJpegBytes2Image.apply(this.mImage2JpegBytes.apply(Image2JpegBytes.In.of(packet, processingRequest.getJpegQuality())));
        }
        return this.mJpegImage2Result.apply(packet2);
    }
    
    @WorkerThread
    void processInputPacket(@NonNull final InputPacket inputPacket) {
        final ProcessingRequest processingRequest = inputPacket.getProcessingRequest();
        try {
            if (inputPacket.getProcessingRequest().isInMemoryCapture()) {
                CameraXExecutors.mainThreadExecutor().execute(new o\u30070(processingRequest, this.processInMemoryCapture(inputPacket)));
            }
            else {
                CameraXExecutors.mainThreadExecutor().execute(new \u3007\u3007888(processingRequest, this.processOnDiskCapture(inputPacket)));
            }
        }
        catch (final RuntimeException ex) {
            sendError(processingRequest, new ImageCaptureException(0, "Processing failed.", ex));
        }
        catch (final ImageCaptureException ex2) {
            sendError(processingRequest, ex2);
        }
    }
    
    @NonNull
    @WorkerThread
    ImageCapture.OutputFileResults processOnDiskCapture(@NonNull final InputPacket inputPacket) throws ImageCaptureException {
        final ProcessingRequest processingRequest = inputPacket.getProcessingRequest();
        Packet packet2;
        final Packet packet = packet2 = this.mImage2JpegBytes.apply(Image2JpegBytes.In.of(this.mInput2Packet.apply(inputPacket), processingRequest.getJpegQuality()));
        if (packet.hasCropping()) {
            packet2 = this.mBitmap2JpegBytes.apply(Bitmap2JpegBytes.In.of(this.mJpegBytes2CroppedBitmap.apply(packet), processingRequest.getJpegQuality()));
        }
        final Operation<JpegBytes2Disk.In, ImageCapture.OutputFileResults> mJpegBytes2Disk = this.mJpegBytes2Disk;
        final ImageCapture.OutputFileOptions outputFileOptions = processingRequest.getOutputFileOptions();
        Objects.requireNonNull(outputFileOptions);
        return mJpegBytes2Disk.apply(JpegBytes2Disk.In.of(packet2, outputFileOptions));
    }
    
    @Override
    public void release() {
    }
    
    @NonNull
    @Override
    public Void transform(@NonNull final In in) {
        in.getEdge().setListener(new \u3007o\u3007(this));
        this.mInput2Packet = new ProcessingInput2Packet();
        this.mImage2JpegBytes = new Image2JpegBytes();
        this.mJpegBytes2CroppedBitmap = new JpegBytes2CroppedBitmap();
        this.mBitmap2JpegBytes = new Bitmap2JpegBytes();
        this.mJpegBytes2Disk = new JpegBytes2Disk();
        this.mJpegImage2Result = new JpegImage2Result();
        if (in.getFormat() == 35) {
            this.mJpegBytes2Image = new JpegBytes2Image();
        }
        return null;
    }
    
    abstract static class In
    {
        static In of(final int n) {
            return (In)new AutoValue_ProcessingNode_In(new Edge<InputPacket>(), n);
        }
        
        abstract Edge<InputPacket> getEdge();
        
        abstract int getFormat();
    }
    
    abstract static class InputPacket
    {
        static InputPacket of(@NonNull final ProcessingRequest processingRequest, @NonNull final ImageProxy imageProxy) {
            return (InputPacket)new AutoValue_ProcessingNode_InputPacket(processingRequest, imageProxy);
        }
        
        @NonNull
        abstract ImageProxy getImageProxy();
        
        @NonNull
        abstract ProcessingRequest getProcessingRequest();
    }
}

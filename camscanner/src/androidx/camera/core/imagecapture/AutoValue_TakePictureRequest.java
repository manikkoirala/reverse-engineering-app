// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.camera.core.impl.CameraCaptureCallback;
import java.util.List;
import android.graphics.Matrix;
import androidx.camera.core.ImageCapture;
import android.graphics.Rect;
import java.util.concurrent.Executor;

final class AutoValue_TakePictureRequest extends TakePictureRequest
{
    private final Executor appExecutor;
    private final int captureMode;
    private final Rect cropRect;
    private final ImageCapture.OnImageCapturedCallback inMemoryCallback;
    private final int jpegQuality;
    private final ImageCapture.OnImageSavedCallback onDiskCallback;
    private final ImageCapture.OutputFileOptions outputFileOptions;
    private final int rotationDegrees;
    private final Matrix sensorToBufferTransform;
    private final List<CameraCaptureCallback> sessionConfigCameraCaptureCallbacks;
    
    AutoValue_TakePictureRequest(final Executor appExecutor, @Nullable final ImageCapture.OnImageCapturedCallback inMemoryCallback, @Nullable final ImageCapture.OnImageSavedCallback onDiskCallback, @Nullable final ImageCapture.OutputFileOptions outputFileOptions, final Rect cropRect, final Matrix sensorToBufferTransform, final int rotationDegrees, final int jpegQuality, final int captureMode, final List<CameraCaptureCallback> sessionConfigCameraCaptureCallbacks) {
        if (appExecutor == null) {
            throw new NullPointerException("Null appExecutor");
        }
        this.appExecutor = appExecutor;
        this.inMemoryCallback = inMemoryCallback;
        this.onDiskCallback = onDiskCallback;
        this.outputFileOptions = outputFileOptions;
        if (cropRect == null) {
            throw new NullPointerException("Null cropRect");
        }
        this.cropRect = cropRect;
        if (sensorToBufferTransform == null) {
            throw new NullPointerException("Null sensorToBufferTransform");
        }
        this.sensorToBufferTransform = sensorToBufferTransform;
        this.rotationDegrees = rotationDegrees;
        this.jpegQuality = jpegQuality;
        this.captureMode = captureMode;
        if (sessionConfigCameraCaptureCallbacks != null) {
            this.sessionConfigCameraCaptureCallbacks = sessionConfigCameraCaptureCallbacks;
            return;
        }
        throw new NullPointerException("Null sessionConfigCameraCaptureCallbacks");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof TakePictureRequest) {
            final TakePictureRequest takePictureRequest = (TakePictureRequest)o;
            if (this.appExecutor.equals(takePictureRequest.getAppExecutor())) {
                final ImageCapture.OnImageCapturedCallback inMemoryCallback = this.inMemoryCallback;
                if (inMemoryCallback == null) {
                    if (takePictureRequest.getInMemoryCallback() != null) {
                        return false;
                    }
                }
                else if (!inMemoryCallback.equals(takePictureRequest.getInMemoryCallback())) {
                    return false;
                }
                final ImageCapture.OnImageSavedCallback onDiskCallback = this.onDiskCallback;
                if (onDiskCallback == null) {
                    if (takePictureRequest.getOnDiskCallback() != null) {
                        return false;
                    }
                }
                else if (!onDiskCallback.equals(takePictureRequest.getOnDiskCallback())) {
                    return false;
                }
                final ImageCapture.OutputFileOptions outputFileOptions = this.outputFileOptions;
                if (outputFileOptions == null) {
                    if (takePictureRequest.getOutputFileOptions() != null) {
                        return false;
                    }
                }
                else if (!outputFileOptions.equals(takePictureRequest.getOutputFileOptions())) {
                    return false;
                }
                if (this.cropRect.equals((Object)takePictureRequest.getCropRect()) && this.sensorToBufferTransform.equals((Object)takePictureRequest.getSensorToBufferTransform()) && this.rotationDegrees == takePictureRequest.getRotationDegrees() && this.jpegQuality == takePictureRequest.getJpegQuality() && this.captureMode == takePictureRequest.getCaptureMode() && this.sessionConfigCameraCaptureCallbacks.equals(takePictureRequest.getSessionConfigCameraCaptureCallbacks())) {
                    return b;
                }
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @NonNull
    @Override
    Executor getAppExecutor() {
        return this.appExecutor;
    }
    
    @Override
    int getCaptureMode() {
        return this.captureMode;
    }
    
    @NonNull
    @Override
    Rect getCropRect() {
        return this.cropRect;
    }
    
    @Nullable
    @Override
    ImageCapture.OnImageCapturedCallback getInMemoryCallback() {
        return this.inMemoryCallback;
    }
    
    @IntRange(from = 1L, to = 100L)
    @Override
    int getJpegQuality() {
        return this.jpegQuality;
    }
    
    @Nullable
    @Override
    ImageCapture.OnImageSavedCallback getOnDiskCallback() {
        return this.onDiskCallback;
    }
    
    @Nullable
    @Override
    ImageCapture.OutputFileOptions getOutputFileOptions() {
        return this.outputFileOptions;
    }
    
    @Override
    int getRotationDegrees() {
        return this.rotationDegrees;
    }
    
    @NonNull
    @Override
    Matrix getSensorToBufferTransform() {
        return this.sensorToBufferTransform;
    }
    
    @NonNull
    @Override
    List<CameraCaptureCallback> getSessionConfigCameraCaptureCallbacks() {
        return this.sessionConfigCameraCaptureCallbacks;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.appExecutor.hashCode();
        final ImageCapture.OnImageCapturedCallback inMemoryCallback = this.inMemoryCallback;
        int hashCode2 = 0;
        int hashCode3;
        if (inMemoryCallback == null) {
            hashCode3 = 0;
        }
        else {
            hashCode3 = inMemoryCallback.hashCode();
        }
        final ImageCapture.OnImageSavedCallback onDiskCallback = this.onDiskCallback;
        int hashCode4;
        if (onDiskCallback == null) {
            hashCode4 = 0;
        }
        else {
            hashCode4 = onDiskCallback.hashCode();
        }
        final ImageCapture.OutputFileOptions outputFileOptions = this.outputFileOptions;
        if (outputFileOptions != null) {
            hashCode2 = outputFileOptions.hashCode();
        }
        return (((((((((hashCode ^ 0xF4243) * 1000003 ^ hashCode3) * 1000003 ^ hashCode4) * 1000003 ^ hashCode2) * 1000003 ^ this.cropRect.hashCode()) * 1000003 ^ this.sensorToBufferTransform.hashCode()) * 1000003 ^ this.rotationDegrees) * 1000003 ^ this.jpegQuality) * 1000003 ^ this.captureMode) * 1000003 ^ this.sessionConfigCameraCaptureCallbacks.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("TakePictureRequest{appExecutor=");
        sb.append(this.appExecutor);
        sb.append(", inMemoryCallback=");
        sb.append(this.inMemoryCallback);
        sb.append(", onDiskCallback=");
        sb.append(this.onDiskCallback);
        sb.append(", outputFileOptions=");
        sb.append(this.outputFileOptions);
        sb.append(", cropRect=");
        sb.append(this.cropRect);
        sb.append(", sensorToBufferTransform=");
        sb.append(this.sensorToBufferTransform);
        sb.append(", rotationDegrees=");
        sb.append(this.rotationDegrees);
        sb.append(", jpegQuality=");
        sb.append(this.jpegQuality);
        sb.append(", captureMode=");
        sb.append(this.captureMode);
        sb.append(", sessionConfigCameraCaptureCallbacks=");
        sb.append(this.sessionConfigCameraCaptureCallbacks);
        sb.append("}");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.ImageProxy;
import androidx.camera.core.ImageCapture;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.camera.core.ImageCaptureException;

interface TakePictureCallback
{
    boolean isAborted();
    
    @MainThread
    void onCaptureFailure(@NonNull final ImageCaptureException p0);
    
    @MainThread
    void onFinalResult(@NonNull final ImageCapture.OutputFileResults p0);
    
    @MainThread
    void onFinalResult(@NonNull final ImageProxy p0);
    
    @MainThread
    void onImageCaptured();
    
    @MainThread
    void onProcessFailure(@NonNull final ImageCaptureException p0);
}

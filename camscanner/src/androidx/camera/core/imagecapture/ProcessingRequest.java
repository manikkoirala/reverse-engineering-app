// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.ImageProxy;
import androidx.annotation.MainThread;
import java.util.Iterator;
import androidx.camera.core.impl.CaptureStage;
import java.util.Objects;
import java.util.ArrayList;
import androidx.camera.core.impl.CaptureBundle;
import java.util.List;
import android.graphics.Matrix;
import androidx.annotation.Nullable;
import androidx.camera.core.ImageCapture;
import android.graphics.Rect;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(api = 21)
class ProcessingRequest
{
    @NonNull
    private final TakePictureCallback mCallback;
    @NonNull
    private final Rect mCropRect;
    private final int mJpegQuality;
    @Nullable
    private final ImageCapture.OutputFileOptions mOutputFileOptions;
    private final int mRotationDegrees;
    @NonNull
    private final Matrix mSensorToBufferTransform;
    @NonNull
    private final List<Integer> mStageIds;
    @NonNull
    private final String mTagBundleKey;
    
    ProcessingRequest(@NonNull final CaptureBundle captureBundle, @Nullable final ImageCapture.OutputFileOptions mOutputFileOptions, @NonNull final Rect mCropRect, final int mRotationDegrees, final int mJpegQuality, @NonNull final Matrix mSensorToBufferTransform, @NonNull final TakePictureCallback mCallback) {
        this.mOutputFileOptions = mOutputFileOptions;
        this.mJpegQuality = mJpegQuality;
        this.mRotationDegrees = mRotationDegrees;
        this.mCropRect = mCropRect;
        this.mSensorToBufferTransform = mSensorToBufferTransform;
        this.mCallback = mCallback;
        this.mTagBundleKey = String.valueOf(captureBundle.hashCode());
        this.mStageIds = new ArrayList<Integer>();
        final List<CaptureStage> captureStages = captureBundle.getCaptureStages();
        Objects.requireNonNull(captureStages);
        final Iterator<CaptureStage> iterator = captureStages.iterator();
        while (iterator.hasNext()) {
            this.mStageIds.add(iterator.next().getId());
        }
    }
    
    @NonNull
    Rect getCropRect() {
        return this.mCropRect;
    }
    
    int getJpegQuality() {
        return this.mJpegQuality;
    }
    
    @Nullable
    ImageCapture.OutputFileOptions getOutputFileOptions() {
        return this.mOutputFileOptions;
    }
    
    int getRotationDegrees() {
        return this.mRotationDegrees;
    }
    
    @NonNull
    Matrix getSensorToBufferTransform() {
        return this.mSensorToBufferTransform;
    }
    
    @NonNull
    List<Integer> getStageIds() {
        return this.mStageIds;
    }
    
    @NonNull
    String getTagBundleKey() {
        return this.mTagBundleKey;
    }
    
    boolean isAborted() {
        return this.mCallback.isAborted();
    }
    
    boolean isInMemoryCapture() {
        return this.getOutputFileOptions() == null;
    }
    
    @MainThread
    void onFinalResult(@NonNull final ImageCapture.OutputFileResults outputFileResults) {
        this.mCallback.onFinalResult(outputFileResults);
    }
    
    @MainThread
    void onFinalResult(@NonNull final ImageProxy imageProxy) {
        this.mCallback.onFinalResult(imageProxy);
    }
    
    @MainThread
    void onImageCaptured() {
        this.mCallback.onImageCaptured();
    }
    
    @MainThread
    void onProcessFailure(@NonNull final ImageCaptureException ex) {
        this.mCallback.onProcessFailure(ex);
    }
}

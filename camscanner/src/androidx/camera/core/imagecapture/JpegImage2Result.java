// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.SettableImageProxy;
import androidx.camera.core.ImmutableImageInfo;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.processing.Packet;
import androidx.camera.core.processing.Operation;

@RequiresApi(api = 21)
public class JpegImage2Result implements Operation<Packet<ImageProxy>, ImageProxy>
{
    @NonNull
    @Override
    public ImageProxy apply(@NonNull final Packet<ImageProxy> packet) throws ImageCaptureException {
        final ImageProxy imageProxy = (ImageProxy)packet.getData();
        final SettableImageProxy settableImageProxy = new SettableImageProxy(imageProxy, packet.getSize(), ImmutableImageInfo.create(imageProxy.getImageInfo().getTagBundle(), imageProxy.getImageInfo().getTimestamp(), packet.getRotationDegrees(), packet.getSensorToBufferTransform()));
        settableImageProxy.setCropRect(packet.getCropRect());
        return settableImageProxy;
    }
}

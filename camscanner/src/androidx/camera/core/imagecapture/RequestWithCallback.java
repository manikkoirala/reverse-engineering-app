// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import java.util.concurrent.Future;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.ImageCapture;
import androidx.annotation.MainThread;
import androidx.camera.core.impl.utils.Threads;
import androidx.camera.core.ImageCaptureException;
import androidx.core.util.Preconditions;
import androidx.annotation.NonNull;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.annotation.RequiresApi;

@RequiresApi(api = 21)
class RequestWithCallback implements TakePictureCallback
{
    private CallbackToFutureAdapter.Completer<Void> mCaptureCompleter;
    private final ListenableFuture<Void> mCaptureFuture;
    private boolean mIsAborted;
    private boolean mIsComplete;
    private final TakePictureRequest mTakePictureRequest;
    
    RequestWithCallback(@NonNull final TakePictureRequest mTakePictureRequest) {
        this.mIsComplete = false;
        this.mIsAborted = false;
        this.mTakePictureRequest = mTakePictureRequest;
        this.mCaptureFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new oO80(this));
    }
    
    private void checkOnImageCaptured() {
        Preconditions.checkState(((Future)this.mCaptureFuture).isDone(), "onImageCaptured() must be called before onFinalResult()");
    }
    
    private void markComplete() {
        Preconditions.checkState(this.mIsComplete ^ true, "The callback can only complete once.");
        this.mIsComplete = true;
    }
    
    @MainThread
    private void onFailure(@NonNull final ImageCaptureException ex) {
        Threads.checkMainThread();
        this.mTakePictureRequest.onError(ex);
    }
    
    @MainThread
    void abort(@NonNull final ImageCaptureException ex) {
        Threads.checkMainThread();
        this.mIsAborted = true;
        this.mCaptureCompleter.set(null);
        this.onFailure(ex);
    }
    
    @MainThread
    @NonNull
    ListenableFuture<Void> getCaptureFuture() {
        Threads.checkMainThread();
        return this.mCaptureFuture;
    }
    
    @Override
    public boolean isAborted() {
        return this.mIsAborted;
    }
    
    @MainThread
    @Override
    public void onCaptureFailure(@NonNull final ImageCaptureException ex) {
        Threads.checkMainThread();
        if (this.mIsAborted) {
            return;
        }
        this.markComplete();
        this.mCaptureCompleter.set(null);
        this.onFailure(ex);
    }
    
    @MainThread
    @Override
    public void onFinalResult(@NonNull final ImageCapture.OutputFileResults outputFileResults) {
        Threads.checkMainThread();
        if (this.mIsAborted) {
            return;
        }
        this.checkOnImageCaptured();
        this.markComplete();
        this.mTakePictureRequest.onResult(outputFileResults);
    }
    
    @MainThread
    @Override
    public void onFinalResult(@NonNull final ImageProxy imageProxy) {
        Threads.checkMainThread();
        if (this.mIsAborted) {
            return;
        }
        this.checkOnImageCaptured();
        this.markComplete();
        this.mTakePictureRequest.onResult(imageProxy);
    }
    
    @MainThread
    @Override
    public void onImageCaptured() {
        Threads.checkMainThread();
        if (this.mIsAborted) {
            return;
        }
        this.mCaptureCompleter.set(null);
    }
    
    @MainThread
    @Override
    public void onProcessFailure(@NonNull final ImageCaptureException ex) {
        Threads.checkMainThread();
        if (this.mIsAborted) {
            return;
        }
        this.checkOnImageCaptured();
        this.markComplete();
        this.onFailure(ex);
    }
}

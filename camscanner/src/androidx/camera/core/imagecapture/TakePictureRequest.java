// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.annotation.IntRange;
import androidx.core.util.Preconditions;
import androidx.camera.core.impl.CameraCaptureCallback;
import java.util.List;
import android.graphics.Matrix;
import android.graphics.Rect;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.ImageCapture;
import java.util.Objects;
import androidx.camera.core.ImageCaptureException;
import androidx.annotation.RequiresApi;

@RequiresApi(api = 21)
public abstract class TakePictureRequest
{
    @NonNull
    public static TakePictureRequest of(@NonNull final Executor executor, @Nullable final ImageCapture.OnImageCapturedCallback onImageCapturedCallback, @Nullable final ImageCapture.OnImageSavedCallback onImageSavedCallback, @Nullable final ImageCapture.OutputFileOptions outputFileOptions, @NonNull final Rect rect, @NonNull final Matrix matrix, final int n, final int n2, final int n3, @NonNull final List<CameraCaptureCallback> list) {
        final int n4 = 1;
        Preconditions.checkArgument(onImageSavedCallback == null == (outputFileOptions == null), (Object)"onDiskCallback and outputFileOptions should be both null or both non-null.");
        final boolean b = onImageSavedCallback == null;
        int n5;
        if (onImageCapturedCallback == null) {
            n5 = n4;
        }
        else {
            n5 = 0;
        }
        Preconditions.checkArgument((boolean)((n5 ^ (b ? 1 : 0)) != 0x0), (Object)"One and only one on-disk or in-memory callback should be present.");
        return new AutoValue_TakePictureRequest(executor, onImageCapturedCallback, onImageSavedCallback, outputFileOptions, rect, matrix, n, n2, n3, list);
    }
    
    @NonNull
    abstract Executor getAppExecutor();
    
    abstract int getCaptureMode();
    
    @NonNull
    abstract Rect getCropRect();
    
    @Nullable
    abstract ImageCapture.OnImageCapturedCallback getInMemoryCallback();
    
    @IntRange(from = 1L, to = 100L)
    abstract int getJpegQuality();
    
    @Nullable
    abstract ImageCapture.OnImageSavedCallback getOnDiskCallback();
    
    @Nullable
    abstract ImageCapture.OutputFileOptions getOutputFileOptions();
    
    abstract int getRotationDegrees();
    
    @NonNull
    abstract Matrix getSensorToBufferTransform();
    
    @NonNull
    abstract List<CameraCaptureCallback> getSessionConfigCameraCaptureCallbacks();
    
    void onError(@NonNull final ImageCaptureException ex) {
        this.getAppExecutor().execute(new \u3007\u3007808\u3007(this, ex));
    }
    
    void onResult(@Nullable final ImageCapture.OutputFileResults outputFileResults) {
        this.getAppExecutor().execute(new Oooo8o0\u3007(this, outputFileResults));
    }
    
    void onResult(@Nullable final ImageProxy imageProxy) {
        this.getAppExecutor().execute(new \u3007O00(this, imageProxy));
    }
}

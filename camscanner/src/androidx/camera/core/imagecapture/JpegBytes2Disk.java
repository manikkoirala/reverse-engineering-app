// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import java.io.InputStream;
import androidx.camera.core.processing.Packet;
import java.io.FileOutputStream;
import androidx.camera.core.impl.utils.Exif;
import android.os.Build$VERSION;
import java.util.UUID;
import java.io.FileNotFoundException;
import androidx.annotation.Nullable;
import java.io.FileInputStream;
import java.io.OutputStream;
import android.content.ContentResolver;
import java.io.IOException;
import android.content.ContentValues;
import java.util.Objects;
import androidx.camera.core.ImageCaptureException;
import android.net.Uri;
import androidx.annotation.NonNull;
import java.io.File;
import androidx.annotation.RequiresApi;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.processing.Operation;

@RequiresApi(api = 21)
class JpegBytes2Disk implements Operation<In, ImageCapture.OutputFileResults>
{
    private static final int COPY_BUFFER_SIZE = 1024;
    private static final int NOT_PENDING = 0;
    private static final int PENDING = 1;
    private static final String TEMP_FILE_PREFIX = "CameraX";
    private static final String TEMP_FILE_SUFFIX = ".tmp";
    
    private static Uri copyFileToFile(@NonNull final File file, @NonNull final File dest) throws ImageCaptureException {
        if (dest.exists()) {
            dest.delete();
        }
        if (file.renameTo(dest)) {
            return Uri.fromFile(dest);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Failed to overwrite the file: ");
        sb.append(dest.getAbsolutePath());
        throw new ImageCaptureException(1, sb.toString(), null);
    }
    
    private static Uri copyFileToMediaStore(@NonNull final File file, @NonNull ImageCapture.OutputFileOptions insert) throws ImageCaptureException {
        final ContentResolver contentResolver = insert.getContentResolver();
        Objects.requireNonNull(contentResolver);
        ContentValues contentValues;
        if (insert.getContentValues() != null) {
            contentValues = new ContentValues(insert.getContentValues());
        }
        else {
            contentValues = new ContentValues();
        }
        setContentValuePendingFlag(contentValues, 1);
        insert = (ImageCapture.OutputFileOptions)contentResolver.insert(insert.getSaveCollection(), contentValues);
        if (insert == null) {
            throw new ImageCaptureException(1, "Failed to insert a MediaStore URI.", null);
        }
        try {
            try {
                copyTempFileToUri(file, (Uri)insert, contentResolver);
                updateUriPendingStatus((Uri)insert, contentResolver, 0);
                return (Uri)insert;
            }
            finally {}
        }
        catch (final IOException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to write to MediaStore URI: ");
            sb.append(insert);
            throw new ImageCaptureException(1, sb.toString(), ex);
        }
        updateUriPendingStatus((Uri)insert, contentResolver, 0);
    }
    
    private static void copyFileToOutputStream(@NonNull File file, @NonNull final OutputStream outputStream) throws IOException {
        file = (File)new FileInputStream(file);
        try {
            final byte[] array = new byte[1024];
            while (true) {
                final int read = ((InputStream)file).read(array);
                if (read <= 0) {
                    break;
                }
                outputStream.write(array, 0, read);
            }
            ((InputStream)file).close();
        }
        finally {
            try {
                ((InputStream)file).close();
            }
            finally {
                final Throwable exception;
                ((Throwable)outputStream).addSuppressed(exception);
            }
        }
    }
    
    @Nullable
    private static Uri copyFileToTarget(@NonNull final File file, @NonNull final ImageCapture.OutputFileOptions outputFileOptions) throws ImageCaptureException {
        if (isSaveToMediaStore(outputFileOptions)) {
            return copyFileToMediaStore(file, outputFileOptions);
        }
        if (isSaveToOutputStream(outputFileOptions)) {
            try {
                final OutputStream outputStream = outputFileOptions.getOutputStream();
                Objects.requireNonNull(outputStream);
                copyFileToOutputStream(file, outputStream);
                return null;
            }
            catch (final IOException ex) {
                throw new ImageCaptureException(1, "Failed to write to OutputStream.", null);
            }
        }
        if (isSaveToFile(outputFileOptions)) {
            final File file2 = outputFileOptions.getFile();
            Objects.requireNonNull(file2);
            return copyFileToFile(file, file2);
        }
        throw new ImageCaptureException(0, "Invalid OutputFileOptions", null);
    }
    
    private static void copyTempFileToUri(@NonNull final File file, @NonNull final Uri obj, @NonNull ContentResolver openOutputStream) throws IOException {
        openOutputStream = (ContentResolver)openOutputStream.openOutputStream(obj);
        Label_0024: {
            if (openOutputStream == null) {
                break Label_0024;
            }
            try {
                copyFileToOutputStream(file, (OutputStream)openOutputStream);
                ((OutputStream)openOutputStream).close();
            }
            finally {
                if (openOutputStream != null) {
                    try {
                        ((OutputStream)openOutputStream).close();
                    }
                    finally {
                        final Throwable exception;
                        ((Throwable)file).addSuppressed(exception);
                    }
                }
                final StringBuilder sb = new StringBuilder();
                sb.append(obj);
                sb.append(" cannot be resolved.");
                throw new FileNotFoundException(sb.toString());
            }
        }
    }
    
    @NonNull
    private static File createTempFile(@NonNull final ImageCapture.OutputFileOptions outputFileOptions) throws ImageCaptureException {
        try {
            final File file = outputFileOptions.getFile();
            if (file != null) {
                final String parent = file.getParent();
                final StringBuilder sb = new StringBuilder();
                sb.append("CameraX");
                sb.append(UUID.randomUUID().toString());
                sb.append(".tmp");
                return new File(parent, sb.toString());
            }
            return File.createTempFile("CameraX", ".tmp");
        }
        catch (final IOException ex) {
            throw new ImageCaptureException(1, "Failed to create temp file.", ex);
        }
    }
    
    private static boolean isSaveToFile(final ImageCapture.OutputFileOptions outputFileOptions) {
        return outputFileOptions.getFile() != null;
    }
    
    private static boolean isSaveToMediaStore(final ImageCapture.OutputFileOptions outputFileOptions) {
        return outputFileOptions.getSaveCollection() != null && outputFileOptions.getContentResolver() != null && outputFileOptions.getContentValues() != null;
    }
    
    private static boolean isSaveToOutputStream(final ImageCapture.OutputFileOptions outputFileOptions) {
        return outputFileOptions.getOutputStream() != null;
    }
    
    private static void setContentValuePendingFlag(@NonNull final ContentValues contentValues, final int i) {
        if (Build$VERSION.SDK_INT >= 29) {
            contentValues.put("is_pending", Integer.valueOf(i));
        }
    }
    
    private static void updateFileExif(@NonNull final File file, @NonNull final Exif exif, @NonNull final ImageCapture.OutputFileOptions outputFileOptions, final int n) throws ImageCaptureException {
        try {
            final Exif fromFile = Exif.createFromFile(file);
            exif.copyToCroppedImage(fromFile);
            if (fromFile.getRotation() == 0 && n != 0) {
                fromFile.rotate(n);
            }
            final ImageCapture.Metadata metadata = outputFileOptions.getMetadata();
            if (metadata.isReversedHorizontal()) {
                fromFile.flipHorizontally();
            }
            if (metadata.isReversedVertical()) {
                fromFile.flipVertically();
            }
            if (metadata.getLocation() != null) {
                fromFile.attachLocation(metadata.getLocation());
            }
            fromFile.save();
        }
        catch (final IOException ex) {
            throw new ImageCaptureException(1, "Failed to update Exif data", ex);
        }
    }
    
    private static void updateUriPendingStatus(@NonNull final Uri uri, @NonNull final ContentResolver contentResolver, final int n) {
        if (Build$VERSION.SDK_INT >= 29) {
            final ContentValues contentValues = new ContentValues();
            setContentValuePendingFlag(contentValues, n);
            contentResolver.update(uri, contentValues, (String)null, (String[])null);
        }
    }
    
    private static void writeBytesToFile(@NonNull final File file, @NonNull final byte[] b) throws ImageCaptureException {
        try {
            final FileOutputStream fileOutputStream = new FileOutputStream(file);
            try {
                fileOutputStream.write(b);
                fileOutputStream.close();
            }
            finally {
                try {
                    fileOutputStream.close();
                }
                finally {
                    final Throwable exception;
                    ((Throwable)file).addSuppressed(exception);
                }
            }
        }
        catch (final IOException ex) {
            throw new ImageCaptureException(1, "Failed to write to temp file", ex);
        }
    }
    
    @NonNull
    @Override
    public ImageCapture.OutputFileResults apply(@NonNull final In in) throws ImageCaptureException {
        final Packet<byte[]> packet = in.getPacket();
        final ImageCapture.OutputFileOptions outputFileOptions = in.getOutputFileOptions();
        final File tempFile = createTempFile(outputFileOptions);
        writeBytesToFile(tempFile, packet.getData());
        final Exif exif = packet.getExif();
        Objects.requireNonNull(exif);
        updateFileExif(tempFile, exif, outputFileOptions, packet.getRotationDegrees());
        return new ImageCapture.OutputFileResults(copyFileToTarget(tempFile, outputFileOptions));
    }
    
    abstract static class In
    {
        @NonNull
        static In of(@NonNull final Packet<byte[]> packet, @NonNull final ImageCapture.OutputFileOptions outputFileOptions) {
            return (In)new AutoValue_JpegBytes2Disk_In(packet, outputFileOptions);
        }
        
        @NonNull
        abstract ImageCapture.OutputFileOptions getOutputFileOptions();
        
        @NonNull
        abstract Packet<byte[]> getPacket();
    }
}

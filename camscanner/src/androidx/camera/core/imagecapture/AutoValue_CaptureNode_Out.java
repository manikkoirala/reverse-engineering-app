// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.imagecapture;

import androidx.camera.core.ImageProxy;
import androidx.camera.core.processing.Edge;

final class AutoValue_CaptureNode_Out extends Out
{
    private final int format;
    private final Edge<ImageProxy> imageEdge;
    private final Edge<ProcessingRequest> requestEdge;
    
    AutoValue_CaptureNode_Out(final Edge<ImageProxy> imageEdge, final Edge<ProcessingRequest> requestEdge, final int format) {
        if (imageEdge == null) {
            throw new NullPointerException("Null imageEdge");
        }
        this.imageEdge = imageEdge;
        if (requestEdge != null) {
            this.requestEdge = requestEdge;
            this.format = format;
            return;
        }
        throw new NullPointerException("Null requestEdge");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof Out) {
            final Out out = (Out)o;
            if (!this.imageEdge.equals(out.getImageEdge()) || !this.requestEdge.equals(out.getRequestEdge()) || this.format != out.getFormat()) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    int getFormat() {
        return this.format;
    }
    
    @Override
    Edge<ImageProxy> getImageEdge() {
        return this.imageEdge;
    }
    
    @Override
    Edge<ProcessingRequest> getRequestEdge() {
        return this.requestEdge;
    }
    
    @Override
    public int hashCode() {
        return ((this.imageEdge.hashCode() ^ 0xF4243) * 1000003 ^ this.requestEdge.hashCode()) * 1000003 ^ this.format;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Out{imageEdge=");
        sb.append(this.imageEdge);
        sb.append(", requestEdge=");
        sb.append(this.requestEdge);
        sb.append(", format=");
        sb.append(this.format);
        sb.append("}");
        return sb.toString();
    }
}

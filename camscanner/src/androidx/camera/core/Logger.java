// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import android.os.Build$VERSION;
import androidx.annotation.IntRange;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public final class Logger
{
    static final int DEFAULT_MIN_LOG_LEVEL = 3;
    private static final int MAX_TAG_LENGTH = 23;
    private static int sMinLogLevel = 3;
    
    private Logger() {
    }
    
    public static void d(@NonNull final String s, @NonNull final String s2) {
        isLogLevelEnabled(truncateTag(s), 3);
    }
    
    public static void d(@NonNull final String s, @NonNull final String s2, @NonNull final Throwable t) {
        isLogLevelEnabled(truncateTag(s), 3);
    }
    
    public static void e(@NonNull final String s, @NonNull final String s2) {
        isLogLevelEnabled(truncateTag(s), 6);
    }
    
    public static void e(@NonNull final String s, @NonNull final String s2, @NonNull final Throwable t) {
        isLogLevelEnabled(truncateTag(s), 6);
    }
    
    static int getMinLogLevel() {
        return Logger.sMinLogLevel;
    }
    
    public static void i(@NonNull final String s, @NonNull final String s2) {
        isLogLevelEnabled(truncateTag(s), 4);
    }
    
    public static void i(@NonNull final String s, @NonNull final String s2, @NonNull final Throwable t) {
        isLogLevelEnabled(truncateTag(s), 4);
    }
    
    public static boolean isDebugEnabled(@NonNull final String s) {
        return isLogLevelEnabled(truncateTag(s), 3);
    }
    
    public static boolean isErrorEnabled(@NonNull final String s) {
        return isLogLevelEnabled(truncateTag(s), 6);
    }
    
    public static boolean isInfoEnabled(@NonNull final String s) {
        return isLogLevelEnabled(truncateTag(s), 4);
    }
    
    private static boolean isLogLevelEnabled(@NonNull final String s, final int n) {
        return Logger.sMinLogLevel <= n || Log.isLoggable(s, n);
    }
    
    public static boolean isWarnEnabled(@NonNull final String s) {
        return isLogLevelEnabled(truncateTag(s), 5);
    }
    
    static void resetMinLogLevel() {
        Logger.sMinLogLevel = 3;
    }
    
    static void setMinLogLevel(@IntRange(from = 3L, to = 6L) final int sMinLogLevel) {
        Logger.sMinLogLevel = sMinLogLevel;
    }
    
    @NonNull
    private static String truncateTag(@NonNull final String s) {
        String substring = s;
        if (Build$VERSION.SDK_INT <= 25) {
            substring = s;
            if (23 < s.length()) {
                substring = s.substring(0, 23);
            }
        }
        return substring;
    }
    
    public static void w(@NonNull final String s, @NonNull final String s2) {
        isLogLevelEnabled(truncateTag(s), 5);
    }
    
    public static void w(@NonNull final String s, @NonNull final String s2, @NonNull final Throwable t) {
        isLogLevelEnabled(truncateTag(s), 5);
    }
}

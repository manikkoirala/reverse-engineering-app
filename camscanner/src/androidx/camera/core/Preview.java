// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.camera.core.impl.ConfigProvider;
import androidx.camera.core.internal.UseCaseEventConfig;
import java.util.UUID;
import android.util.Pair;
import java.util.List;
import androidx.camera.core.impl.CaptureConfig;
import androidx.camera.core.impl.OptionsBundle;
import androidx.camera.core.impl.MutableConfig;
import androidx.camera.core.internal.TargetConfig;
import androidx.camera.core.impl.MutableOptionsBundle;
import androidx.camera.core.internal.ThreadConfig;
import androidx.camera.core.impl.ImageOutputConfig;
import androidx.annotation.UiThread;
import androidx.camera.core.impl.ImageInputConfig;
import androidx.camera.core.impl.CameraInfoInternal;
import androidx.camera.core.impl.Config;
import androidx.camera.core.impl.Oooo8o0\u3007;
import androidx.camera.core.impl.UseCaseConfigFactory;
import androidx.camera.core.internal.CameraCaptureResultImageInfo;
import androidx.camera.core.impl.CameraCaptureResult;
import androidx.camera.core.impl.CameraCaptureCallback;
import androidx.camera.core.impl.ImageInfoProcessor;
import android.os.Handler;
import android.os.HandlerThread;
import androidx.camera.core.impl.CaptureStage;
import androidx.camera.core.impl.CaptureProcessor;
import android.graphics.Rect;
import androidx.camera.core.impl.CameraInternal;
import androidx.camera.core.processing.SurfaceEdge;
import java.util.Collections;
import androidx.camera.core.processing.SettableSurface;
import java.util.Objects;
import android.graphics.Matrix;
import androidx.core.util.Preconditions;
import androidx.camera.core.impl.utils.Threads;
import androidx.camera.core.impl.SessionConfig;
import androidx.annotation.MainThread;
import androidx.camera.core.impl.UseCaseConfig;
import androidx.camera.core.impl.PreviewConfig;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import android.util.Size;
import androidx.annotation.NonNull;
import androidx.camera.core.processing.SurfaceProcessorInternal;
import androidx.camera.core.impl.DeferrableSurface;
import androidx.camera.core.processing.SurfaceProcessorNode;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.Nullable;
import java.util.concurrent.Executor;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class Preview extends UseCase
{
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final Defaults DEFAULT_CONFIG;
    private static final Executor DEFAULT_SURFACE_PROVIDER_EXECUTOR;
    private static final String TAG = "Preview";
    @Nullable
    @VisibleForTesting
    SurfaceRequest mCurrentSurfaceRequest;
    @Nullable
    private SurfaceProcessorNode mNode;
    private DeferrableSurface mSessionDeferrableSurface;
    @Nullable
    private SurfaceProcessorInternal mSurfaceProcessor;
    @Nullable
    private SurfaceProvider mSurfaceProvider;
    @NonNull
    private Executor mSurfaceProviderExecutor;
    @Nullable
    private Size mSurfaceSize;
    
    static {
        DEFAULT_CONFIG = new Defaults();
        DEFAULT_SURFACE_PROVIDER_EXECUTOR = CameraXExecutors.mainThreadExecutor();
    }
    
    @MainThread
    Preview(@NonNull final PreviewConfig previewConfig) {
        super(previewConfig);
        this.mSurfaceProviderExecutor = Preview.DEFAULT_SURFACE_PROVIDER_EXECUTOR;
    }
    
    private void addCameraSurfaceAndErrorListener(@NonNull final SessionConfig.Builder builder, @NonNull final String s, @NonNull final PreviewConfig previewConfig, @NonNull final Size size) {
        if (this.mSurfaceProvider != null) {
            builder.addSurface(this.mSessionDeferrableSurface);
        }
        builder.addErrorListener(new O000(this, s, previewConfig, size));
    }
    
    private void clearPipeline() {
        final DeferrableSurface mSessionDeferrableSurface = this.mSessionDeferrableSurface;
        if (mSessionDeferrableSurface != null) {
            mSessionDeferrableSurface.close();
            this.mSessionDeferrableSurface = null;
        }
        final SurfaceProcessorNode mNode = this.mNode;
        if (mNode != null) {
            mNode.release();
            this.mNode = null;
        }
        this.mCurrentSurfaceRequest = null;
    }
    
    @MainThread
    @NonNull
    private SessionConfig.Builder createPipelineWithNode(@NonNull final String s, @NonNull final PreviewConfig previewConfig, @NonNull final Size size) {
        Threads.checkMainThread();
        Preconditions.checkNotNull(this.mSurfaceProcessor);
        final CameraInternal camera = this.getCamera();
        Preconditions.checkNotNull(camera);
        this.clearPipeline();
        this.mNode = new SurfaceProcessorNode(camera, SurfaceOutput.GlTransformOptions.USE_SURFACE_TEXTURE_TRANSFORM, this.mSurfaceProcessor);
        final Matrix matrix = new Matrix();
        final Rect cropRect = this.getCropRect(size);
        Objects.requireNonNull(cropRect);
        final SettableSurface settableSurface = new SettableSurface(1, size, 34, matrix, true, cropRect, this.getRelativeRotation(camera), false);
        final SettableSurface settableSurface2 = this.mNode.transform(SurfaceEdge.create(Collections.singletonList(settableSurface))).getSurfaces().get(0);
        this.mSessionDeferrableSurface = settableSurface;
        this.mCurrentSurfaceRequest = settableSurface2.createSurfaceRequest(camera);
        if (this.mSurfaceProvider != null) {
            this.sendSurfaceRequest();
        }
        final SessionConfig.Builder from = SessionConfig.Builder.createFrom(previewConfig);
        this.addCameraSurfaceAndErrorListener(from, s, previewConfig, size);
        return from;
    }
    
    @Nullable
    private Rect getCropRect(@Nullable final Size size) {
        if (this.getViewPortCropRect() != null) {
            return this.getViewPortCropRect();
        }
        if (size != null) {
            return new Rect(0, 0, size.getWidth(), size.getHeight());
        }
        return null;
    }
    
    private void sendSurfaceRequest() {
        this.mSurfaceProviderExecutor.execute(new oO00OOO(Preconditions.checkNotNull(this.mSurfaceProvider), Preconditions.checkNotNull(this.mCurrentSurfaceRequest)));
        this.sendTransformationInfoIfReady();
    }
    
    private void sendTransformationInfoIfReady() {
        final CameraInternal camera = this.getCamera();
        final SurfaceProvider mSurfaceProvider = this.mSurfaceProvider;
        final Rect cropRect = this.getCropRect(this.mSurfaceSize);
        final SurfaceRequest mCurrentSurfaceRequest = this.mCurrentSurfaceRequest;
        if (camera != null && mSurfaceProvider != null && cropRect != null && mCurrentSurfaceRequest != null) {
            mCurrentSurfaceRequest.updateTransformationInfo(SurfaceRequest.TransformationInfo.of(cropRect, this.getRelativeRotation(camera), this.getAppTargetRotation()));
        }
    }
    
    private void updateConfigAndOutput(@NonNull final String s, @NonNull final PreviewConfig previewConfig, @NonNull final Size size) {
        this.updateSessionConfig(this.createPipeline(s, previewConfig, size).build());
    }
    
    @MainThread
    SessionConfig.Builder createPipeline(@NonNull final String s, @NonNull final PreviewConfig previewConfig, @NonNull final Size size) {
        if (this.mSurfaceProcessor != null) {
            return this.createPipelineWithNode(s, previewConfig, size);
        }
        Threads.checkMainThread();
        final SessionConfig.Builder from = SessionConfig.Builder.createFrom(previewConfig);
        final CaptureProcessor captureProcessor = previewConfig.getCaptureProcessor(null);
        this.clearPipeline();
        final SurfaceRequest mCurrentSurfaceRequest = new SurfaceRequest(size, this.getCamera(), previewConfig.isRgba8888SurfaceRequired(false));
        this.mCurrentSurfaceRequest = mCurrentSurfaceRequest;
        if (this.mSurfaceProvider != null) {
            this.sendSurfaceRequest();
        }
        if (captureProcessor != null) {
            final CaptureStage.DefaultCaptureStage defaultCaptureStage = new CaptureStage.DefaultCaptureStage();
            final HandlerThread handlerThread = new HandlerThread("CameraX-preview_processing");
            ((Thread)handlerThread).start();
            final String string = Integer.toString(defaultCaptureStage.hashCode());
            final ProcessingSurface mSessionDeferrableSurface = new ProcessingSurface(size.getWidth(), size.getHeight(), previewConfig.getInputFormat(), new Handler(handlerThread.getLooper()), defaultCaptureStage, captureProcessor, mCurrentSurfaceRequest.getDeferrableSurface(), string);
            from.addCameraCaptureCallback(mSessionDeferrableSurface.getCameraCaptureCallback());
            mSessionDeferrableSurface.getTerminationFuture().addListener((Runnable)new \u300780(handlerThread), CameraXExecutors.directExecutor());
            this.mSessionDeferrableSurface = mSessionDeferrableSurface;
            from.addTag(string, defaultCaptureStage.getId());
        }
        else {
            final ImageInfoProcessor imageInfoProcessor = previewConfig.getImageInfoProcessor(null);
            if (imageInfoProcessor != null) {
                from.addCameraCaptureCallback(new CameraCaptureCallback(this, imageInfoProcessor) {
                    final Preview this$0;
                    final ImageInfoProcessor val$processor;
                    
                    @Override
                    public void onCaptureCompleted(@NonNull final CameraCaptureResult cameraCaptureResult) {
                        super.onCaptureCompleted(cameraCaptureResult);
                        if (this.val$processor.process(new CameraCaptureResultImageInfo(cameraCaptureResult))) {
                            this.this$0.notifyUpdated();
                        }
                    }
                });
            }
            this.mSessionDeferrableSurface = mCurrentSurfaceRequest.getDeferrableSurface();
        }
        this.addCameraSurfaceAndErrorListener(from, s, previewConfig, size);
        return from;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    public UseCaseConfig<?> getDefaultConfig(final boolean b, @NonNull final UseCaseConfigFactory useCaseConfigFactory) {
        Config config2;
        final Config config = config2 = useCaseConfigFactory.getConfig(UseCaseConfigFactory.CaptureType.PREVIEW, 1);
        if (b) {
            config2 = Oooo8o0\u3007.\u3007o00\u3007\u3007Oo(config, Preview.DEFAULT_CONFIG.getConfig());
        }
        Object useCaseConfig;
        if (config2 == null) {
            useCaseConfig = null;
        }
        else {
            useCaseConfig = this.getUseCaseConfigBuilder(config2).getUseCaseConfig();
        }
        return (UseCaseConfig<?>)useCaseConfig;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @VisibleForTesting
    public SurfaceProcessorInternal getProcessor() {
        return this.mSurfaceProcessor;
    }
    
    @Nullable
    @Override
    public ResolutionInfo getResolutionInfo() {
        return super.getResolutionInfo();
    }
    
    public int getTargetRotation() {
        return this.getTargetRotationInternal();
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    public UseCaseConfig.Builder<?, ?, ?> getUseCaseConfigBuilder(@NonNull final Config config) {
        return Builder.fromConfig(config);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    public void onDetached() {
        this.clearPipeline();
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    protected UseCaseConfig<?> onMergeConfig(@NonNull final CameraInfoInternal cameraInfoInternal, @NonNull final UseCaseConfig.Builder<?, ?, ?> builder) {
        if (builder.getMutableConfig().retrieveOption(PreviewConfig.OPTION_PREVIEW_CAPTURE_PROCESSOR, null) != null) {
            builder.getMutableConfig().insertOption(ImageInputConfig.OPTION_INPUT_FORMAT, 35);
        }
        else {
            builder.getMutableConfig().insertOption(ImageInputConfig.OPTION_INPUT_FORMAT, 34);
        }
        return (UseCaseConfig<?>)builder.getUseCaseConfig();
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    protected Size onSuggestedResolutionUpdated(@NonNull final Size mSurfaceSize) {
        this.mSurfaceSize = mSurfaceSize;
        this.updateConfigAndOutput(this.getCameraId(), (PreviewConfig)this.getCurrentConfig(), this.mSurfaceSize);
        return mSurfaceSize;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void setProcessor(@Nullable final SurfaceProcessorInternal mSurfaceProcessor) {
        this.mSurfaceProcessor = mSurfaceProcessor;
    }
    
    @UiThread
    public void setSurfaceProvider(@Nullable final SurfaceProvider surfaceProvider) {
        this.setSurfaceProvider(Preview.DEFAULT_SURFACE_PROVIDER_EXECUTOR, surfaceProvider);
    }
    
    @UiThread
    public void setSurfaceProvider(@NonNull final Executor mSurfaceProviderExecutor, @Nullable final SurfaceProvider mSurfaceProvider) {
        Threads.checkMainThread();
        if (mSurfaceProvider == null) {
            this.mSurfaceProvider = null;
            this.notifyInactive();
        }
        else {
            this.mSurfaceProvider = mSurfaceProvider;
            this.mSurfaceProviderExecutor = mSurfaceProviderExecutor;
            this.notifyActive();
            if (this.getAttachedSurfaceResolution() != null) {
                this.updateConfigAndOutput(this.getCameraId(), (PreviewConfig)this.getCurrentConfig(), this.getAttachedSurfaceResolution());
                this.notifyReset();
            }
        }
    }
    
    public void setTargetRotation(final int targetRotationInternal) {
        if (this.setTargetRotationInternal(targetRotationInternal)) {
            this.sendTransformationInfoIfReady();
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    @Override
    public void setViewPortCropRect(@NonNull final Rect viewPortCropRect) {
        super.setViewPortCropRect(viewPortCropRect);
        this.sendTransformationInfoIfReady();
    }
    
    @NonNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Preview:");
        sb.append(this.getName());
        return sb.toString();
    }
    
    public static final class Builder implements UseCaseConfig.Builder<Preview, PreviewConfig, Builder>, ImageOutputConfig.Builder<Builder>, ThreadConfig.Builder<Builder>
    {
        private final MutableOptionsBundle mMutableConfig;
        
        public Builder() {
            this(MutableOptionsBundle.create());
        }
        
        private Builder(final MutableOptionsBundle mMutableConfig) {
            this.mMutableConfig = mMutableConfig;
            final Class<?> obj = mMutableConfig.retrieveOption(TargetConfig.OPTION_TARGET_CLASS, null);
            if (obj != null && !obj.equals(Preview.class)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid target class configuration for ");
                sb.append(this);
                sb.append(": ");
                sb.append(obj);
                throw new IllegalArgumentException(sb.toString());
            }
            this.setTargetClass(Preview.class);
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        static Builder fromConfig(@NonNull final Config config) {
            return new Builder(MutableOptionsBundle.from(config));
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public static Builder fromConfig(@NonNull final PreviewConfig previewConfig) {
            return new Builder(MutableOptionsBundle.from(previewConfig));
        }
        
        @NonNull
        @Override
        public Preview build() {
            if (this.getMutableConfig().retrieveOption(ImageOutputConfig.OPTION_TARGET_ASPECT_RATIO, null) != null && this.getMutableConfig().retrieveOption(ImageOutputConfig.OPTION_TARGET_RESOLUTION, null) != null) {
                throw new IllegalArgumentException("Cannot use both setTargetResolution and setTargetAspectRatio on the same config.");
            }
            return new Preview(this.getUseCaseConfig());
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        @Override
        public MutableConfig getMutableConfig() {
            return this.mMutableConfig;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public PreviewConfig getUseCaseConfig() {
            return new PreviewConfig(OptionsBundle.from(this.mMutableConfig));
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setBackgroundExecutor(@NonNull final Executor executor) {
            this.getMutableConfig().insertOption(ThreadConfig.OPTION_BACKGROUND_EXECUTOR, executor);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setCameraSelector(@NonNull final CameraSelector cameraSelector) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_CAMERA_SELECTOR, cameraSelector);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setCaptureOptionUnpacker(@NonNull final CaptureConfig.OptionUnpacker optionUnpacker) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_CAPTURE_CONFIG_UNPACKER, optionUnpacker);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setCaptureProcessor(@NonNull final CaptureProcessor captureProcessor) {
            this.getMutableConfig().insertOption(PreviewConfig.OPTION_PREVIEW_CAPTURE_PROCESSOR, captureProcessor);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setDefaultCaptureConfig(@NonNull final CaptureConfig captureConfig) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_DEFAULT_CAPTURE_CONFIG, captureConfig);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setDefaultResolution(@NonNull final Size size) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_DEFAULT_RESOLUTION, size);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setDefaultSessionConfig(@NonNull final SessionConfig sessionConfig) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_DEFAULT_SESSION_CONFIG, sessionConfig);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setImageInfoProcessor(@NonNull final ImageInfoProcessor imageInfoProcessor) {
            this.getMutableConfig().insertOption(PreviewConfig.IMAGE_INFO_PROCESSOR, imageInfoProcessor);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setIsRgba8888SurfaceRequired(final boolean b) {
            this.getMutableConfig().insertOption(PreviewConfig.OPTION_RGBA8888_SURFACE_REQUIRED, b);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setMaxResolution(@NonNull final Size size) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_MAX_RESOLUTION, size);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setSessionOptionUnpacker(@NonNull final SessionConfig.OptionUnpacker optionUnpacker) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_SESSION_CONFIG_UNPACKER, optionUnpacker);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setSupportedResolutions(@NonNull final List<Pair<Integer, Size[]>> list) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_SUPPORTED_RESOLUTIONS, list);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setSurfaceOccupancyPriority(final int i) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_SURFACE_OCCUPANCY_PRIORITY, i);
            return this;
        }
        
        @NonNull
        public Builder setTargetAspectRatio(final int i) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_TARGET_ASPECT_RATIO, i);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setTargetClass(@NonNull final Class<Preview> clazz) {
            this.getMutableConfig().insertOption(TargetConfig.OPTION_TARGET_CLASS, clazz);
            if (this.getMutableConfig().retrieveOption(TargetConfig.OPTION_TARGET_NAME, null) == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append(clazz.getCanonicalName());
                sb.append("-");
                sb.append(UUID.randomUUID());
                this.setTargetName(sb.toString());
            }
            return this;
        }
        
        @NonNull
        public Builder setTargetName(@NonNull final String s) {
            this.getMutableConfig().insertOption(TargetConfig.OPTION_TARGET_NAME, s);
            return this;
        }
        
        @NonNull
        public Builder setTargetResolution(@NonNull final Size size) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_TARGET_RESOLUTION, size);
            return this;
        }
        
        @NonNull
        public Builder setTargetRotation(final int n) {
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_TARGET_ROTATION, n);
            this.getMutableConfig().insertOption(ImageOutputConfig.OPTION_APP_TARGET_ROTATION, n);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setUseCaseEventCallback(@NonNull final EventCallback eventCallback) {
            this.getMutableConfig().insertOption(UseCaseEventConfig.OPTION_USE_CASE_EVENT_CALLBACK, eventCallback);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setZslDisabled(final boolean b) {
            this.getMutableConfig().insertOption(UseCaseConfig.OPTION_ZSL_DISABLED, b);
            return this;
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final class Defaults implements ConfigProvider<PreviewConfig>
    {
        private static final int DEFAULT_ASPECT_RATIO = 0;
        private static final PreviewConfig DEFAULT_CONFIG;
        private static final int DEFAULT_SURFACE_OCCUPANCY_PRIORITY = 2;
        
        static {
            DEFAULT_CONFIG = new Builder().setSurfaceOccupancyPriority(2).setTargetAspectRatio(0).getUseCaseConfig();
        }
        
        @NonNull
        @Override
        public PreviewConfig getConfig() {
            return Defaults.DEFAULT_CONFIG;
        }
    }
    
    public interface SurfaceProvider
    {
        void onSurfaceRequested(@NonNull final SurfaceRequest p0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.RestrictTo;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class FocusMeteringResult
{
    private boolean mIsFocusSuccessful;
    
    private FocusMeteringResult(final boolean mIsFocusSuccessful) {
        this.mIsFocusSuccessful = mIsFocusSuccessful;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static FocusMeteringResult create(final boolean b) {
        return new FocusMeteringResult(b);
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static FocusMeteringResult emptyInstance() {
        return new FocusMeteringResult(false);
    }
    
    public boolean isFocusSuccessful() {
        return this.mIsFocusSuccessful;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.util.concurrent.Future;
import java.util.concurrent.ExecutionException;
import androidx.camera.core.impl.ImageProxyBundle;
import java.util.Collections;
import androidx.core.util.Preconditions;
import android.media.ImageReader;
import android.util.Size;
import android.view.Surface;
import androidx.arch.core.util.Function;
import java.util.concurrent.RejectedExecutionException;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import java.util.Collection;
import androidx.camera.core.impl.utils.futures.Futures;
import java.util.ArrayList;
import java.util.List;
import androidx.camera.core.impl.ImageReaderProxy;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.annotation.GuardedBy;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.CaptureProcessor;

@RequiresApi(21)
class CaptureProcessorPipeline implements CaptureProcessor
{
    private static final String TAG = "CaptureProcessorPipeline";
    @GuardedBy("mLock")
    CallbackToFutureAdapter.Completer<Void> mCloseCompleter;
    @GuardedBy("mLock")
    private ListenableFuture<Void> mCloseFuture;
    @GuardedBy("mLock")
    private boolean mClosed;
    @NonNull
    final Executor mExecutor;
    private ImageReaderProxy mIntermediateImageReader;
    private final Object mLock;
    private final int mMaxImages;
    @NonNull
    private final CaptureProcessor mPostCaptureProcessor;
    @NonNull
    private final CaptureProcessor mPreCaptureProcessor;
    @GuardedBy("mLock")
    private boolean mProcessing;
    private ImageInfo mSourceImageInfo;
    @NonNull
    private final ListenableFuture<List<Void>> mUnderlyingCaptureProcessorsCloseFuture;
    
    CaptureProcessorPipeline(@NonNull final CaptureProcessor mPreCaptureProcessor, final int mMaxImages, @NonNull final CaptureProcessor mPostCaptureProcessor, @NonNull final Executor mExecutor) {
        this.mIntermediateImageReader = null;
        this.mSourceImageInfo = null;
        this.mLock = new Object();
        this.mClosed = false;
        this.mProcessing = false;
        this.mPreCaptureProcessor = mPreCaptureProcessor;
        this.mPostCaptureProcessor = mPostCaptureProcessor;
        final ArrayList list = new ArrayList();
        list.add(mPreCaptureProcessor.getCloseFuture());
        list.add(mPostCaptureProcessor.getCloseFuture());
        this.mUnderlyingCaptureProcessorsCloseFuture = (ListenableFuture<List<Void>>)Futures.allAsList((Collection<? extends com.google.common.util.concurrent.ListenableFuture<?>>)list);
        this.mExecutor = mExecutor;
        this.mMaxImages = mMaxImages;
    }
    
    private void closeAndCompleteFutureIfNecessary() {
        synchronized (this.mLock) {
            final boolean mClosed = this.mClosed;
            final boolean mProcessing = this.mProcessing;
            final CallbackToFutureAdapter.Completer<Void> mCloseCompleter = this.mCloseCompleter;
            if (mClosed && !mProcessing) {
                this.mIntermediateImageReader.close();
            }
            monitorexit(this.mLock);
            if (mClosed && !mProcessing && mCloseCompleter != null) {
                this.mUnderlyingCaptureProcessorsCloseFuture.addListener((Runnable)new \u3007O00(mCloseCompleter), CameraXExecutors.directExecutor());
            }
        }
    }
    
    @Override
    public void close() {
        synchronized (this.mLock) {
            if (this.mClosed) {
                return;
            }
            this.mClosed = true;
            monitorexit(this.mLock);
            this.mPreCaptureProcessor.close();
            this.mPostCaptureProcessor.close();
            this.closeAndCompleteFutureIfNecessary();
        }
    }
    
    @NonNull
    @Override
    public ListenableFuture<Void> getCloseFuture() {
        synchronized (this.mLock) {
            com.google.common.util.concurrent.ListenableFuture<Void> listenableFuture;
            if (this.mClosed && !this.mProcessing) {
                listenableFuture = Futures.transform(this.mUnderlyingCaptureProcessorsCloseFuture, (Function<? super List<Void>, ? extends Void>)new OO0o\u3007\u3007(), CameraXExecutors.directExecutor());
            }
            else {
                if (this.mCloseFuture == null) {
                    this.mCloseFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new Oooo8o0\u3007(this));
                }
                listenableFuture = Futures.nonCancellationPropagating(this.mCloseFuture);
            }
            return listenableFuture;
        }
    }
    
    @Override
    public void onOutputSurface(@NonNull final Surface surface, final int n) {
        this.mPostCaptureProcessor.onOutputSurface(surface, n);
    }
    
    @Override
    public void onResolutionUpdate(@NonNull final Size size) {
        final AndroidImageReaderProxy mIntermediateImageReader = new AndroidImageReaderProxy(ImageReader.newInstance(size.getWidth(), size.getHeight(), 35, this.mMaxImages));
        this.mIntermediateImageReader = mIntermediateImageReader;
        this.mPreCaptureProcessor.onOutputSurface(mIntermediateImageReader.getSurface(), 35);
        this.mPreCaptureProcessor.onResolutionUpdate(size);
        this.mPostCaptureProcessor.onResolutionUpdate(size);
        this.mIntermediateImageReader.setOnImageAvailableListener((ImageReaderProxy.OnImageAvailableListener)new \u3007O8o08O(this), CameraXExecutors.directExecutor());
    }
    
    void postProcess(final ImageProxy imageProxy) {
        Object mLock = this.mLock;
        synchronized (mLock) {
            final boolean mClosed = this.mClosed;
            monitorexit(mLock);
            if (!mClosed) {
                final Size size = new Size(imageProxy.getWidth(), imageProxy.getHeight());
                Preconditions.checkNotNull(this.mSourceImageInfo);
                mLock = this.mSourceImageInfo.getTagBundle().listKeys().iterator().next();
                final int intValue = (int)this.mSourceImageInfo.getTagBundle().getTag((String)mLock);
                final SettableImageProxy settableImageProxy = new SettableImageProxy(imageProxy, size, this.mSourceImageInfo);
                this.mSourceImageInfo = null;
                mLock = new SettableImageProxyBundle(Collections.singletonList(intValue), (String)mLock);
                ((SettableImageProxyBundle)mLock).addImageProxy(settableImageProxy);
                try {
                    this.mPostCaptureProcessor.process((ImageProxyBundle)mLock);
                }
                catch (final Exception ex) {
                    mLock = new StringBuilder();
                    ((StringBuilder)mLock).append("Post processing image failed! ");
                    ((StringBuilder)mLock).append(ex.getMessage());
                    Logger.e("CaptureProcessorPipeline", ((StringBuilder)mLock).toString());
                }
            }
            synchronized (this.mLock) {
                this.mProcessing = false;
                monitorexit(this.mLock);
                this.closeAndCompleteFutureIfNecessary();
            }
        }
    }
    
    @Override
    public void process(@NonNull final ImageProxyBundle imageProxyBundle) {
        Object o = this.mLock;
        synchronized (o) {
            if (this.mClosed) {
                return;
            }
            this.mProcessing = true;
            monitorexit(o);
            o = imageProxyBundle.getImageProxy(imageProxyBundle.getCaptureIds().get(0));
            Preconditions.checkArgument(((Future)o).isDone());
            try {
                this.mSourceImageInfo = ((ImageProxy)((Future)o).get()).getImageInfo();
                this.mPreCaptureProcessor.process(imageProxyBundle);
            }
            catch (final ExecutionException | InterruptedException ex) {
                throw new IllegalArgumentException("Can not successfully extract ImageProxy from the ImageProxyBundle.");
            }
        }
    }
}

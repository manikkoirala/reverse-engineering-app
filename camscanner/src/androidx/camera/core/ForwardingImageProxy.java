// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.util.Iterator;
import java.util.Collection;
import androidx.annotation.Nullable;
import android.media.Image;
import android.graphics.Rect;
import java.util.HashSet;
import androidx.annotation.NonNull;
import androidx.annotation.GuardedBy;
import java.util.Set;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public abstract class ForwardingImageProxy implements ImageProxy
{
    protected final ImageProxy mImage;
    private final Object mLock;
    @GuardedBy("mLock")
    private final Set<OnImageCloseListener> mOnImageCloseListeners;
    
    protected ForwardingImageProxy(@NonNull final ImageProxy mImage) {
        this.mLock = new Object();
        this.mOnImageCloseListeners = new HashSet<OnImageCloseListener>();
        this.mImage = mImage;
    }
    
    public void addOnImageCloseListener(@NonNull final OnImageCloseListener onImageCloseListener) {
        synchronized (this.mLock) {
            this.mOnImageCloseListeners.add(onImageCloseListener);
        }
    }
    
    @Override
    public void close() {
        this.mImage.close();
        this.notifyOnImageCloseListeners();
    }
    
    @NonNull
    @Override
    public Rect getCropRect() {
        return this.mImage.getCropRect();
    }
    
    @Override
    public int getFormat() {
        return this.mImage.getFormat();
    }
    
    @Override
    public int getHeight() {
        return this.mImage.getHeight();
    }
    
    @Nullable
    @ExperimentalGetImage
    @Override
    public Image getImage() {
        return this.mImage.getImage();
    }
    
    @NonNull
    @Override
    public ImageInfo getImageInfo() {
        return this.mImage.getImageInfo();
    }
    
    @NonNull
    @Override
    public PlaneProxy[] getPlanes() {
        return this.mImage.getPlanes();
    }
    
    @Override
    public int getWidth() {
        return this.mImage.getWidth();
    }
    
    protected void notifyOnImageCloseListeners() {
        Object o = this.mLock;
        synchronized (o) {
            final HashSet set = new HashSet(this.mOnImageCloseListeners);
            monitorexit(o);
            o = set.iterator();
            while (((Iterator)o).hasNext()) {
                ((OnImageCloseListener)((Iterator)o).next()).onImageClose(this);
            }
        }
    }
    
    @Override
    public void setCropRect(@Nullable final Rect cropRect) {
        this.mImage.setCropRect(cropRect);
    }
    
    public interface OnImageCloseListener
    {
        void onImageClose(@NonNull final ImageProxy p0);
    }
}

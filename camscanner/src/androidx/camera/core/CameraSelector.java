// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.annotation.Nullable;
import androidx.camera.core.impl.LensFacingCameraFilter;
import java.util.Collections;
import java.util.Collection;
import androidx.annotation.RestrictTo;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import androidx.camera.core.impl.CameraInternal;
import java.util.LinkedHashSet;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class CameraSelector
{
    @NonNull
    public static final CameraSelector DEFAULT_BACK_CAMERA;
    @NonNull
    public static final CameraSelector DEFAULT_FRONT_CAMERA;
    public static final int LENS_FACING_BACK = 1;
    public static final int LENS_FACING_FRONT = 0;
    private LinkedHashSet<CameraFilter> mCameraFilterSet;
    
    static {
        DEFAULT_FRONT_CAMERA = new Builder().requireLensFacing(0).build();
        DEFAULT_BACK_CAMERA = new Builder().requireLensFacing(1).build();
    }
    
    CameraSelector(final LinkedHashSet<CameraFilter> mCameraFilterSet) {
        this.mCameraFilterSet = mCameraFilterSet;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public LinkedHashSet<CameraInternal> filter(@NonNull final LinkedHashSet<CameraInternal> set) {
        final ArrayList list = new ArrayList();
        final Iterator<Object> iterator = set.iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getCameraInfo());
        }
        final List<CameraInfo> filter = this.filter(list);
        final LinkedHashSet set2 = new LinkedHashSet();
        for (final CameraInternal e : set) {
            if (filter.contains(e.getCameraInfo())) {
                set2.add(e);
            }
        }
        return set2;
    }
    
    @NonNull
    public List<CameraInfo> filter(@NonNull final List<CameraInfo> c) {
        List<CameraInfo> filter = new ArrayList<CameraInfo>((Collection<? extends T>)c);
        final Iterator<Object> iterator = this.mCameraFilterSet.iterator();
        while (iterator.hasNext()) {
            filter = iterator.next().filter(Collections.unmodifiableList((List<? extends CameraInfo>)filter));
        }
        filter.retainAll(c);
        return filter;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public LinkedHashSet<CameraFilter> getCameraFilterSet() {
        return this.mCameraFilterSet;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public Integer getLensFacing() {
        final Iterator<Object> iterator = this.mCameraFilterSet.iterator();
        Integer n = null;
        while (iterator.hasNext()) {
            final CameraFilter cameraFilter = iterator.next();
            if (cameraFilter instanceof LensFacingCameraFilter) {
                final Integer value = ((LensFacingCameraFilter)cameraFilter).getLensFacing();
                if (n == null) {
                    n = value;
                }
                else {
                    if (n.equals(value)) {
                        continue;
                    }
                    throw new IllegalStateException("Multiple conflicting lens facing requirements exist.");
                }
            }
        }
        return n;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public CameraInternal select(@NonNull final LinkedHashSet<CameraInternal> set) {
        final Iterator<Object> iterator = this.filter(set).iterator();
        if (iterator.hasNext()) {
            return iterator.next();
        }
        throw new IllegalArgumentException("No available camera can be found");
    }
    
    public static final class Builder
    {
        private final LinkedHashSet<CameraFilter> mCameraFilterSet;
        
        public Builder() {
            this.mCameraFilterSet = new LinkedHashSet<CameraFilter>();
        }
        
        private Builder(@NonNull final LinkedHashSet<CameraFilter> c) {
            this.mCameraFilterSet = new LinkedHashSet<CameraFilter>(c);
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public static Builder fromSelector(@NonNull final CameraSelector cameraSelector) {
            return new Builder(cameraSelector.getCameraFilterSet());
        }
        
        @NonNull
        public Builder addCameraFilter(@NonNull final CameraFilter e) {
            this.mCameraFilterSet.add(e);
            return this;
        }
        
        @NonNull
        public CameraSelector build() {
            return new CameraSelector(this.mCameraFilterSet);
        }
        
        @NonNull
        public Builder requireLensFacing(final int n) {
            this.mCameraFilterSet.add(new LensFacingCameraFilter(n));
            return this;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public @interface LensFacing {
    }
}

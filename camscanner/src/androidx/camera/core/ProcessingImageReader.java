// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.util.concurrent.Future;
import java.util.concurrent.Executors;
import java.util.Collection;
import androidx.core.util.Preconditions;
import java.util.Iterator;
import androidx.camera.core.impl.CaptureStage;
import androidx.camera.core.impl.CaptureBundle;
import android.view.Surface;
import androidx.arch.core.util.Function;
import androidx.camera.core.impl.CameraCaptureCallback;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import android.util.Size;
import android.media.ImageReader;
import androidx.camera.core.impl.utils.futures.Futures;
import java.util.ArrayList;
import java.util.Collections;
import androidx.camera.core.impl.ImageProxyBundle;
import androidx.annotation.Nullable;
import java.util.concurrent.Executor;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.annotation.GuardedBy;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.CaptureProcessor;
import java.util.List;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.ImageReaderProxy;

@RequiresApi(21)
class ProcessingImageReader implements ImageReaderProxy
{
    private static final int EXIF_MAX_SIZE_BYTES = 64000;
    private static final String TAG = "ProcessingImageReader";
    private final List<Integer> mCaptureIdList;
    @NonNull
    final CaptureProcessor mCaptureProcessor;
    private FutureCallback<List<ImageProxy>> mCaptureStageReadyCallback;
    @GuardedBy("mLock")
    CallbackToFutureAdapter.Completer<Void> mCloseCompleter;
    @GuardedBy("mLock")
    private ListenableFuture<Void> mCloseFuture;
    @GuardedBy("mLock")
    boolean mClosed;
    @GuardedBy("mLock")
    Executor mErrorCallbackExecutor;
    @GuardedBy("mLock")
    @Nullable
    Executor mExecutor;
    private OnImageAvailableListener mImageProcessedListener;
    @GuardedBy("mLock")
    final ImageReaderProxy mInputImageReader;
    @GuardedBy("mLock")
    @Nullable
    OnImageAvailableListener mListener;
    final Object mLock;
    @GuardedBy("mLock")
    OnProcessingErrorCallback mOnProcessingErrorCallback;
    @GuardedBy("mLock")
    final ImageReaderProxy mOutputImageReader;
    @NonNull
    final Executor mPostProcessExecutor;
    @GuardedBy("mLock")
    boolean mProcessing;
    @GuardedBy("mLock")
    @NonNull
    SettableImageProxyBundle mSettableImageProxyBundle;
    private ListenableFuture<List<ImageProxy>> mSettableImageProxyFutureList;
    private String mTagBundleKey;
    private OnImageAvailableListener mTransformedListener;
    @NonNull
    private final ListenableFuture<Void> mUnderlyingCaptureProcessorCloseFuture;
    
    ProcessingImageReader(@NonNull final Builder builder) {
        this.mLock = new Object();
        this.mTransformedListener = new OnImageAvailableListener() {
            final ProcessingImageReader this$0;
            
            @Override
            public void onImageAvailable(@NonNull final ImageReaderProxy imageReaderProxy) {
                this.this$0.imageIncoming(imageReaderProxy);
            }
        };
        this.mImageProcessedListener = new OnImageAvailableListener() {
            final ProcessingImageReader this$0;
            
            @Override
            public void onImageAvailable(@NonNull final ImageReaderProxy imageReaderProxy) {
                synchronized (this.this$0.mLock) {
                    final ProcessingImageReader this$0 = this.this$0;
                    final OnImageAvailableListener mListener = this$0.mListener;
                    final Executor mExecutor = this$0.mExecutor;
                    this$0.mSettableImageProxyBundle.reset();
                    this.this$0.setupSettableImageProxyBundleCallbacks();
                    monitorexit(this.this$0.mLock);
                    if (mListener != null) {
                        if (mExecutor != null) {
                            mExecutor.execute(new O0o\u3007\u3007Oo(this, mListener));
                        }
                        else {
                            mListener.onImageAvailable(this.this$0);
                        }
                    }
                }
            }
        };
        this.mCaptureStageReadyCallback = new FutureCallback<List<ImageProxy>>() {
            final ProcessingImageReader this$0;
            
            @Override
            public void onFailure(@NonNull final Throwable t) {
            }
            
            @Override
            public void onSuccess(@Nullable List<ImageProxy> mOnProcessingErrorCallback) {
                synchronized (this.this$0.mLock) {
                    final ProcessingImageReader this$0 = this.this$0;
                    if (this$0.mClosed) {
                        return;
                    }
                    this$0.mProcessing = true;
                    final SettableImageProxyBundle mSettableImageProxyBundle = this$0.mSettableImageProxyBundle;
                    mOnProcessingErrorCallback = this$0.mOnProcessingErrorCallback;
                    final Executor mErrorCallbackExecutor = this$0.mErrorCallbackExecutor;
                    monitorexit(this.this$0.mLock);
                    try {
                        this$0.mCaptureProcessor.process(mSettableImageProxyBundle);
                    }
                    catch (final Exception ex) {
                        final Object mLock = this.this$0.mLock;
                        synchronized (this.this$0.mLock) {
                            this.this$0.mSettableImageProxyBundle.reset();
                            if (mOnProcessingErrorCallback != null && mErrorCallbackExecutor != null) {
                                mErrorCallbackExecutor.execute(new OO8oO0o\u3007(mOnProcessingErrorCallback, ex));
                            }
                            monitorexit(this.this$0.mLock);
                            synchronized (this.this$0.mLock) {
                                final ProcessingImageReader this$2 = this.this$0;
                                this$2.mProcessing = false;
                                monitorexit(this.this$0.mLock);
                                this$2.closeAndCompleteFutureIfNecessary();
                            }
                        }
                    }
                }
            }
        };
        this.mClosed = false;
        this.mProcessing = false;
        this.mTagBundleKey = new String();
        this.mSettableImageProxyBundle = new SettableImageProxyBundle(Collections.emptyList(), this.mTagBundleKey);
        this.mCaptureIdList = new ArrayList<Integer>();
        this.mSettableImageProxyFutureList = (ListenableFuture<List<ImageProxy>>)Futures.immediateFuture(new ArrayList());
        if (builder.mInputImageReader.getMaxImages() >= builder.mCaptureBundle.getCaptureStages().size()) {
            final ImageReaderProxy mInputImageReader = builder.mInputImageReader;
            this.mInputImageReader = mInputImageReader;
            final int width = mInputImageReader.getWidth();
            final int height = mInputImageReader.getHeight();
            final int mOutputFormat = builder.mOutputFormat;
            int n = width;
            int n2 = height;
            if (mOutputFormat == 256) {
                n = (int)(width * height * 1.5f) + 64000;
                n2 = 1;
            }
            final AndroidImageReaderProxy mOutputImageReader = new AndroidImageReaderProxy(ImageReader.newInstance(n, n2, mOutputFormat, mInputImageReader.getMaxImages()));
            this.mOutputImageReader = mOutputImageReader;
            this.mPostProcessExecutor = builder.mPostProcessExecutor;
            final CaptureProcessor mCaptureProcessor = builder.mCaptureProcessor;
            (this.mCaptureProcessor = mCaptureProcessor).onOutputSurface(mOutputImageReader.getSurface(), builder.mOutputFormat);
            mCaptureProcessor.onResolutionUpdate(new Size(mInputImageReader.getWidth(), mInputImageReader.getHeight()));
            this.mUnderlyingCaptureProcessorCloseFuture = mCaptureProcessor.getCloseFuture();
            this.setCaptureBundle(builder.mCaptureBundle);
            return;
        }
        throw new IllegalArgumentException("MetadataImageReader is smaller than CaptureBundle.");
    }
    
    private void cancelSettableImageProxyBundleFutureList() {
        synchronized (this.mLock) {
            if (!((Future)this.mSettableImageProxyFutureList).isDone()) {
                ((Future)this.mSettableImageProxyFutureList).cancel(true);
            }
            this.mSettableImageProxyBundle.reset();
        }
    }
    
    @Nullable
    @Override
    public ImageProxy acquireLatestImage() {
        synchronized (this.mLock) {
            return this.mOutputImageReader.acquireLatestImage();
        }
    }
    
    @Nullable
    @Override
    public ImageProxy acquireNextImage() {
        synchronized (this.mLock) {
            return this.mOutputImageReader.acquireNextImage();
        }
    }
    
    @Override
    public void clearOnImageAvailableListener() {
        synchronized (this.mLock) {
            this.mListener = null;
            this.mExecutor = null;
            this.mInputImageReader.clearOnImageAvailableListener();
            this.mOutputImageReader.clearOnImageAvailableListener();
            if (!this.mProcessing) {
                this.mSettableImageProxyBundle.close();
            }
        }
    }
    
    @Override
    public void close() {
        synchronized (this.mLock) {
            if (this.mClosed) {
                return;
            }
            this.mInputImageReader.clearOnImageAvailableListener();
            this.mOutputImageReader.clearOnImageAvailableListener();
            this.mClosed = true;
            monitorexit(this.mLock);
            this.mCaptureProcessor.close();
            this.closeAndCompleteFutureIfNecessary();
        }
    }
    
    void closeAndCompleteFutureIfNecessary() {
        synchronized (this.mLock) {
            final boolean mClosed = this.mClosed;
            final boolean mProcessing = this.mProcessing;
            final CallbackToFutureAdapter.Completer<Void> mCloseCompleter = this.mCloseCompleter;
            if (mClosed && !mProcessing) {
                this.mInputImageReader.close();
                this.mSettableImageProxyBundle.close();
                this.mOutputImageReader.close();
            }
            monitorexit(this.mLock);
            if (mClosed && !mProcessing) {
                this.mUnderlyingCaptureProcessorCloseFuture.addListener((Runnable)new ooo\u30078oO(this, mCloseCompleter), CameraXExecutors.directExecutor());
            }
        }
    }
    
    @Nullable
    CameraCaptureCallback getCameraCaptureCallback() {
        synchronized (this.mLock) {
            final ImageReaderProxy mInputImageReader = this.mInputImageReader;
            if (mInputImageReader instanceof MetadataImageReader) {
                return ((MetadataImageReader)mInputImageReader).getCameraCaptureCallback();
            }
            return new CameraCaptureCallback(this) {
                final ProcessingImageReader this$0;
            };
        }
    }
    
    @NonNull
    ListenableFuture<Void> getCloseFuture() {
        synchronized (this.mLock) {
            com.google.common.util.concurrent.ListenableFuture<Void> listenableFuture;
            if (this.mClosed && !this.mProcessing) {
                listenableFuture = Futures.transform(this.mUnderlyingCaptureProcessorCloseFuture, (Function<? super Void, ? extends Void>)new Ooo(), CameraXExecutors.directExecutor());
            }
            else {
                if (this.mCloseFuture == null) {
                    this.mCloseFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new \u3007O\u300780o08O(this));
                }
                listenableFuture = Futures.nonCancellationPropagating(this.mCloseFuture);
            }
            return listenableFuture;
        }
    }
    
    @Override
    public int getHeight() {
        synchronized (this.mLock) {
            return this.mInputImageReader.getHeight();
        }
    }
    
    @Override
    public int getImageFormat() {
        synchronized (this.mLock) {
            return this.mOutputImageReader.getImageFormat();
        }
    }
    
    @Override
    public int getMaxImages() {
        synchronized (this.mLock) {
            return this.mInputImageReader.getMaxImages();
        }
    }
    
    @Nullable
    @Override
    public Surface getSurface() {
        synchronized (this.mLock) {
            return this.mInputImageReader.getSurface();
        }
    }
    
    @NonNull
    public String getTagBundleKey() {
        return this.mTagBundleKey;
    }
    
    @Override
    public int getWidth() {
        synchronized (this.mLock) {
            return this.mInputImageReader.getWidth();
        }
    }
    
    void imageIncoming(final ImageReaderProxy imageReaderProxy) {
        synchronized (this.mLock) {
            if (this.mClosed) {
                return;
            }
            try {
                try {
                    final ImageProxy acquireNextImage = imageReaderProxy.acquireNextImage();
                    if (acquireNextImage == null) {
                        return;
                    }
                    final Integer obj = (Integer)acquireNextImage.getImageInfo().getTagBundle().getTag(this.mTagBundleKey);
                    if (!this.mCaptureIdList.contains(obj)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("ImageProxyBundle does not contain this id: ");
                        sb.append(obj);
                        Logger.w("ProcessingImageReader", sb.toString());
                        acquireNextImage.close();
                        return;
                    }
                    this.mSettableImageProxyBundle.addImageProxy(acquireNextImage);
                }
                finally {}
            }
            catch (final IllegalStateException ex) {
                Logger.e("ProcessingImageReader", "Failed to acquire latest image.", ex);
            }
        }
    }
    
    public void setCaptureBundle(@NonNull final CaptureBundle captureBundle) {
        synchronized (this.mLock) {
            if (this.mClosed) {
                return;
            }
            this.cancelSettableImageProxyBundleFutureList();
            if (captureBundle.getCaptureStages() != null) {
                if (this.mInputImageReader.getMaxImages() < captureBundle.getCaptureStages().size()) {
                    throw new IllegalArgumentException("CaptureBundle is larger than InputImageReader.");
                }
                this.mCaptureIdList.clear();
                for (final CaptureStage captureStage : captureBundle.getCaptureStages()) {
                    if (captureStage != null) {
                        this.mCaptureIdList.add(captureStage.getId());
                    }
                }
            }
            final String string = Integer.toString(captureBundle.hashCode());
            this.mTagBundleKey = string;
            this.mSettableImageProxyBundle = new SettableImageProxyBundle(this.mCaptureIdList, string);
            this.setupSettableImageProxyBundleCallbacks();
        }
    }
    
    @Override
    public void setOnImageAvailableListener(@NonNull final OnImageAvailableListener onImageAvailableListener, @NonNull final Executor executor) {
        synchronized (this.mLock) {
            this.mListener = Preconditions.checkNotNull(onImageAvailableListener);
            this.mExecutor = Preconditions.checkNotNull(executor);
            this.mInputImageReader.setOnImageAvailableListener(this.mTransformedListener, executor);
            this.mOutputImageReader.setOnImageAvailableListener(this.mImageProcessedListener, executor);
        }
    }
    
    public void setOnProcessingErrorCallback(@NonNull final Executor mErrorCallbackExecutor, @NonNull final OnProcessingErrorCallback mOnProcessingErrorCallback) {
        synchronized (this.mLock) {
            this.mErrorCallbackExecutor = mErrorCallbackExecutor;
            this.mOnProcessingErrorCallback = mOnProcessingErrorCallback;
        }
    }
    
    @GuardedBy("mLock")
    void setupSettableImageProxyBundleCallbacks() {
        final ArrayList list = new ArrayList();
        final Iterator<Integer> iterator = this.mCaptureIdList.iterator();
        while (iterator.hasNext()) {
            list.add(this.mSettableImageProxyBundle.getImageProxy(iterator.next()));
        }
        this.mSettableImageProxyFutureList = (ListenableFuture<List<ImageProxy>>)Futures.allAsList((Collection<? extends com.google.common.util.concurrent.ListenableFuture<?>>)list);
        Futures.addCallback(Futures.allAsList((Collection<? extends com.google.common.util.concurrent.ListenableFuture<?>>)list), (FutureCallback<? super List<Object>>)this.mCaptureStageReadyCallback, this.mPostProcessExecutor);
    }
    
    static final class Builder
    {
        @NonNull
        protected final CaptureBundle mCaptureBundle;
        @NonNull
        protected final CaptureProcessor mCaptureProcessor;
        @NonNull
        protected final ImageReaderProxy mInputImageReader;
        protected int mOutputFormat;
        @NonNull
        protected Executor mPostProcessExecutor;
        
        Builder(final int n, final int n2, final int n3, final int n4, @NonNull final CaptureBundle captureBundle, @NonNull final CaptureProcessor captureProcessor) {
            this(new MetadataImageReader(n, n2, n3, n4), captureBundle, captureProcessor);
        }
        
        Builder(@NonNull final ImageReaderProxy mInputImageReader, @NonNull final CaptureBundle mCaptureBundle, @NonNull final CaptureProcessor mCaptureProcessor) {
            this.mPostProcessExecutor = Executors.newSingleThreadExecutor();
            this.mInputImageReader = mInputImageReader;
            this.mCaptureBundle = mCaptureBundle;
            this.mCaptureProcessor = mCaptureProcessor;
            this.mOutputFormat = mInputImageReader.getImageFormat();
        }
        
        ProcessingImageReader build() {
            return new ProcessingImageReader(this);
        }
        
        @NonNull
        Builder setOutputFormat(final int mOutputFormat) {
            this.mOutputFormat = mOutputFormat;
            return this;
        }
        
        @NonNull
        Builder setPostProcessExecutor(@NonNull final Executor mPostProcessExecutor) {
            this.mPostProcessExecutor = mPostProcessExecutor;
            return this;
        }
    }
    
    interface OnProcessingErrorCallback
    {
        void notifyProcessingError(@Nullable final String p0, @Nullable final Throwable p1);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.camera.core.impl.utils.MainThreadAsyncHandler;
import androidx.annotation.NonNull;
import android.view.Surface;
import android.os.Handler;
import android.media.ImageReader$OnImageAvailableListener;
import androidx.annotation.Nullable;
import android.media.Image;
import java.util.concurrent.Executor;
import androidx.annotation.GuardedBy;
import android.media.ImageReader;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.ImageReaderProxy;

@RequiresApi(21)
class AndroidImageReaderProxy implements ImageReaderProxy
{
    @GuardedBy("mLock")
    private final ImageReader mImageReader;
    @GuardedBy("mLock")
    private boolean mIsImageAvailableListenerCleared;
    private final Object mLock;
    
    AndroidImageReaderProxy(final ImageReader mImageReader) {
        this.mLock = new Object();
        this.mIsImageAvailableListenerCleared = true;
        this.mImageReader = mImageReader;
    }
    
    private boolean isImageReaderContextNotInitializedException(final RuntimeException ex) {
        return "ImageReaderContext is not initialized".equals(ex.getMessage());
    }
    
    @Nullable
    @Override
    public ImageProxy acquireLatestImage() {
        final Object mLock = this.mLock;
        monitorenter(mLock);
        try {
            try {
                final Image acquireLatestImage = this.mImageReader.acquireLatestImage();
            }
            finally {
                monitorexit(mLock);
                final Image acquireLatestImage = null;
                Label_0033: {
                    break Label_0033;
                    final AndroidImageProxy androidImageProxy;
                    Label_0041: {
                        androidImageProxy = new AndroidImageProxy(acquireLatestImage);
                    }
                    monitorexit(mLock);
                    return androidImageProxy;
                }
                iftrue(Label_0041:)(acquireLatestImage != null);
                monitorexit(mLock);
                return null;
            }
        }
        catch (final RuntimeException ex) {}
    }
    
    @Nullable
    @Override
    public ImageProxy acquireNextImage() {
        final Object mLock = this.mLock;
        monitorenter(mLock);
        try {
            try {
                final Image acquireNextImage = this.mImageReader.acquireNextImage();
            }
            finally {
                monitorexit(mLock);
                final Image acquireNextImage = null;
                Label_0033: {
                    break Label_0033;
                    final AndroidImageProxy androidImageProxy;
                    Label_0041: {
                        androidImageProxy = new AndroidImageProxy(acquireNextImage);
                    }
                    monitorexit(mLock);
                    return androidImageProxy;
                }
                iftrue(Label_0041:)(acquireNextImage != null);
                monitorexit(mLock);
                return null;
            }
        }
        catch (final RuntimeException ex) {}
    }
    
    @Override
    public void clearOnImageAvailableListener() {
        synchronized (this.mLock) {
            this.mIsImageAvailableListenerCleared = true;
            this.mImageReader.setOnImageAvailableListener((ImageReader$OnImageAvailableListener)null, (Handler)null);
        }
    }
    
    @Override
    public void close() {
        synchronized (this.mLock) {
            this.mImageReader.close();
        }
    }
    
    @Override
    public int getHeight() {
        synchronized (this.mLock) {
            return this.mImageReader.getHeight();
        }
    }
    
    @Override
    public int getImageFormat() {
        synchronized (this.mLock) {
            return this.mImageReader.getImageFormat();
        }
    }
    
    @Override
    public int getMaxImages() {
        synchronized (this.mLock) {
            return this.mImageReader.getMaxImages();
        }
    }
    
    @Nullable
    @Override
    public Surface getSurface() {
        synchronized (this.mLock) {
            return this.mImageReader.getSurface();
        }
    }
    
    @Override
    public int getWidth() {
        synchronized (this.mLock) {
            return this.mImageReader.getWidth();
        }
    }
    
    @Override
    public void setOnImageAvailableListener(@NonNull final OnImageAvailableListener onImageAvailableListener, @NonNull final Executor executor) {
        synchronized (this.mLock) {
            this.mIsImageAvailableListenerCleared = false;
            this.mImageReader.setOnImageAvailableListener((ImageReader$OnImageAvailableListener)new \u3007o00\u3007\u3007Oo(this, executor, onImageAvailableListener), MainThreadAsyncHandler.getInstance());
        }
    }
}

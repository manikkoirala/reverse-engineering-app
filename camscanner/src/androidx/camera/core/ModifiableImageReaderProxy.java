// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.media.ImageReader;
import androidx.camera.core.impl.TagBundle;
import android.graphics.Matrix;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
class ModifiableImageReaderProxy extends AndroidImageReaderProxy
{
    private volatile Integer mRotationDegrees;
    private volatile Matrix mSensorToBufferTransformMatrix;
    private volatile TagBundle mTagBundle;
    private volatile Long mTimestamp;
    
    ModifiableImageReaderProxy(@NonNull final ImageReader imageReader) {
        super(imageReader);
        this.mTagBundle = null;
        this.mTimestamp = null;
        this.mRotationDegrees = null;
        this.mSensorToBufferTransformMatrix = null;
    }
    
    private ImageProxy modifyImage(final ImageProxy imageProxy) {
        final ImageInfo imageInfo = imageProxy.getImageInfo();
        TagBundle tagBundle;
        if (this.mTagBundle != null) {
            tagBundle = this.mTagBundle;
        }
        else {
            tagBundle = imageInfo.getTagBundle();
        }
        long n;
        if (this.mTimestamp != null) {
            n = this.mTimestamp;
        }
        else {
            n = imageInfo.getTimestamp();
        }
        int n2;
        if (this.mRotationDegrees != null) {
            n2 = this.mRotationDegrees;
        }
        else {
            n2 = imageInfo.getRotationDegrees();
        }
        Matrix matrix;
        if (this.mSensorToBufferTransformMatrix != null) {
            matrix = this.mSensorToBufferTransformMatrix;
        }
        else {
            matrix = imageInfo.getSensorToBufferTransformMatrix();
        }
        return new SettableImageProxy(imageProxy, ImmutableImageInfo.create(tagBundle, n, n2, matrix));
    }
    
    @Nullable
    @Override
    public ImageProxy acquireLatestImage() {
        return this.modifyImage(super.acquireNextImage());
    }
    
    @Nullable
    @Override
    public ImageProxy acquireNextImage() {
        return this.modifyImage(super.acquireNextImage());
    }
    
    void setImageRotationDegrees(final int i) {
        this.mRotationDegrees = i;
    }
    
    void setImageSensorToBufferTransformaMatrix(@NonNull final Matrix mSensorToBufferTransformMatrix) {
        this.mSensorToBufferTransformMatrix = mSensorToBufferTransformMatrix;
    }
    
    void setImageTagBundle(@NonNull final TagBundle mTagBundle) {
        this.mTagBundle = mTagBundle;
    }
    
    void setImageTimeStamp(final long l) {
        this.mTimestamp = l;
    }
}

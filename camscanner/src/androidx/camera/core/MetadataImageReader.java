// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.camera.core.internal.CameraCaptureResultImageInfo;
import android.view.Surface;
import java.util.Collection;
import java.util.Iterator;
import androidx.core.util.Preconditions;
import android.media.ImageReader;
import java.util.ArrayList;
import androidx.camera.core.impl.CameraCaptureResult;
import androidx.annotation.NonNull;
import android.util.LongSparseArray;
import androidx.annotation.Nullable;
import java.util.concurrent.Executor;
import androidx.camera.core.impl.CameraCaptureCallback;
import androidx.annotation.GuardedBy;
import java.util.List;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.ImageReaderProxy;

@RequiresApi(21)
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class MetadataImageReader implements ImageReaderProxy, OnImageCloseListener
{
    private static final String TAG = "MetadataImageReader";
    @GuardedBy("mLock")
    private final List<ImageProxy> mAcquiredImageProxies;
    private CameraCaptureCallback mCameraCaptureCallback;
    @GuardedBy("mLock")
    private boolean mClosed;
    @GuardedBy("mLock")
    @Nullable
    private Executor mExecutor;
    @GuardedBy("mLock")
    private int mImageProxiesIndex;
    @GuardedBy("mLock")
    private final ImageReaderProxy mImageReaderProxy;
    @GuardedBy("mLock")
    @Nullable
    OnImageAvailableListener mListener;
    private final Object mLock;
    @GuardedBy("mLock")
    private final List<ImageProxy> mMatchedImageProxies;
    @GuardedBy("mLock")
    private final LongSparseArray<ImageInfo> mPendingImageInfos;
    @GuardedBy("mLock")
    private final LongSparseArray<ImageProxy> mPendingImages;
    private OnImageAvailableListener mTransformedListener;
    @GuardedBy("mLock")
    private int mUnAcquiredAvailableImageCount;
    
    public MetadataImageReader(final int n, final int n2, final int n3, final int n4) {
        this(createImageReaderProxy(n, n2, n3, n4));
    }
    
    MetadataImageReader(@NonNull final ImageReaderProxy mImageReaderProxy) {
        this.mLock = new Object();
        this.mCameraCaptureCallback = new CameraCaptureCallback() {
            final MetadataImageReader this$0;
            
            @Override
            public void onCaptureCompleted(@NonNull final CameraCaptureResult cameraCaptureResult) {
                super.onCaptureCompleted(cameraCaptureResult);
                this.this$0.resultIncoming(cameraCaptureResult);
            }
        };
        this.mUnAcquiredAvailableImageCount = 0;
        this.mTransformedListener = new o\u3007O(this);
        this.mClosed = false;
        this.mPendingImageInfos = (LongSparseArray<ImageInfo>)new LongSparseArray();
        this.mPendingImages = (LongSparseArray<ImageProxy>)new LongSparseArray();
        this.mAcquiredImageProxies = new ArrayList<ImageProxy>();
        this.mImageReaderProxy = mImageReaderProxy;
        this.mImageProxiesIndex = 0;
        this.mMatchedImageProxies = new ArrayList<ImageProxy>(this.getMaxImages());
    }
    
    private static ImageReaderProxy createImageReaderProxy(final int n, final int n2, final int n3, final int n4) {
        return new AndroidImageReaderProxy(ImageReader.newInstance(n, n2, n3, n4));
    }
    
    private void dequeImageProxy(final ImageProxy imageProxy) {
        synchronized (this.mLock) {
            final int index = this.mMatchedImageProxies.indexOf(imageProxy);
            if (index >= 0) {
                this.mMatchedImageProxies.remove(index);
                final int mImageProxiesIndex = this.mImageProxiesIndex;
                if (index <= mImageProxiesIndex) {
                    this.mImageProxiesIndex = mImageProxiesIndex - 1;
                }
            }
            this.mAcquiredImageProxies.remove(imageProxy);
            if (this.mUnAcquiredAvailableImageCount > 0) {
                this.imageIncoming(this.mImageReaderProxy);
            }
        }
    }
    
    private void enqueueImageProxy(final SettableImageProxy settableImageProxy) {
        synchronized (this.mLock) {
            OnImageAvailableListener mListener;
            Executor mExecutor;
            if (this.mMatchedImageProxies.size() < this.getMaxImages()) {
                settableImageProxy.addOnImageCloseListener((ForwardingImageProxy.OnImageCloseListener)this);
                this.mMatchedImageProxies.add(settableImageProxy);
                mListener = this.mListener;
                mExecutor = this.mExecutor;
            }
            else {
                Logger.d("TAG", "Maximum image number reached.");
                settableImageProxy.close();
                mListener = null;
                mExecutor = null;
            }
            monitorexit(this.mLock);
            if (mListener != null) {
                if (mExecutor != null) {
                    mExecutor.execute(new o\u30078oOO88(this, mListener));
                }
                else {
                    mListener.onImageAvailable(this);
                }
            }
        }
    }
    
    private void matchImages() {
        synchronized (this.mLock) {
            for (int i = this.mPendingImageInfos.size() - 1; i >= 0; --i) {
                final ImageInfo imageInfo = (ImageInfo)this.mPendingImageInfos.valueAt(i);
                final long timestamp = imageInfo.getTimestamp();
                final ImageProxy imageProxy = (ImageProxy)this.mPendingImages.get(timestamp);
                if (imageProxy != null) {
                    this.mPendingImages.remove(timestamp);
                    this.mPendingImageInfos.removeAt(i);
                    this.enqueueImageProxy(new SettableImageProxy(imageProxy, imageInfo));
                }
            }
            this.removeStaleData();
        }
    }
    
    private void removeStaleData() {
        synchronized (this.mLock) {
            if (this.mPendingImages.size() != 0 && this.mPendingImageInfos.size() != 0) {
                final LongSparseArray<ImageProxy> mPendingImages = this.mPendingImages;
                boolean b = false;
                final Long value = mPendingImages.keyAt(0);
                final Long value2 = this.mPendingImageInfos.keyAt(0);
                if (!value2.equals(value)) {
                    b = true;
                }
                Preconditions.checkArgument(b);
                if (value2 > value) {
                    for (int i = this.mPendingImages.size() - 1; i >= 0; --i) {
                        if (this.mPendingImages.keyAt(i) < value2) {
                            ((ImageProxy)this.mPendingImages.valueAt(i)).close();
                            this.mPendingImages.removeAt(i);
                        }
                    }
                }
                else {
                    for (int j = this.mPendingImageInfos.size() - 1; j >= 0; --j) {
                        if (this.mPendingImageInfos.keyAt(j) < value) {
                            this.mPendingImageInfos.removeAt(j);
                        }
                    }
                }
            }
        }
    }
    
    @Nullable
    @Override
    public ImageProxy acquireLatestImage() {
        synchronized (this.mLock) {
            if (this.mMatchedImageProxies.isEmpty()) {
                return null;
            }
            if (this.mImageProxiesIndex < this.mMatchedImageProxies.size()) {
                final ArrayList list = new ArrayList();
                for (int i = 0; i < this.mMatchedImageProxies.size() - 1; ++i) {
                    if (!this.mAcquiredImageProxies.contains(this.mMatchedImageProxies.get(i))) {
                        list.add(this.mMatchedImageProxies.get(i));
                    }
                }
                final Iterator iterator = list.iterator();
                while (iterator.hasNext()) {
                    ((ImageProxy)iterator.next()).close();
                }
                final int n = this.mMatchedImageProxies.size() - 1;
                final List<ImageProxy> mMatchedImageProxies = this.mMatchedImageProxies;
                this.mImageProxiesIndex = n + 1;
                final ImageProxy imageProxy = mMatchedImageProxies.get(n);
                this.mAcquiredImageProxies.add(imageProxy);
                return imageProxy;
            }
            throw new IllegalStateException("Maximum image number reached.");
        }
    }
    
    @Nullable
    @Override
    public ImageProxy acquireNextImage() {
        synchronized (this.mLock) {
            if (this.mMatchedImageProxies.isEmpty()) {
                return null;
            }
            if (this.mImageProxiesIndex < this.mMatchedImageProxies.size()) {
                final ImageProxy imageProxy = this.mMatchedImageProxies.get(this.mImageProxiesIndex++);
                this.mAcquiredImageProxies.add(imageProxy);
                return imageProxy;
            }
            throw new IllegalStateException("Maximum image number reached.");
        }
    }
    
    @Override
    public void clearOnImageAvailableListener() {
        synchronized (this.mLock) {
            this.mImageReaderProxy.clearOnImageAvailableListener();
            this.mListener = null;
            this.mExecutor = null;
            this.mUnAcquiredAvailableImageCount = 0;
        }
    }
    
    @Override
    public void close() {
        synchronized (this.mLock) {
            if (this.mClosed) {
                return;
            }
            final Iterator iterator = new ArrayList(this.mMatchedImageProxies).iterator();
            while (iterator.hasNext()) {
                ((ImageProxy)iterator.next()).close();
            }
            this.mMatchedImageProxies.clear();
            this.mImageReaderProxy.close();
            this.mClosed = true;
        }
    }
    
    @NonNull
    public CameraCaptureCallback getCameraCaptureCallback() {
        return this.mCameraCaptureCallback;
    }
    
    @Override
    public int getHeight() {
        synchronized (this.mLock) {
            return this.mImageReaderProxy.getHeight();
        }
    }
    
    @Override
    public int getImageFormat() {
        synchronized (this.mLock) {
            return this.mImageReaderProxy.getImageFormat();
        }
    }
    
    @Override
    public int getMaxImages() {
        synchronized (this.mLock) {
            return this.mImageReaderProxy.getMaxImages();
        }
    }
    
    @Nullable
    @Override
    public Surface getSurface() {
        synchronized (this.mLock) {
            return this.mImageReaderProxy.getSurface();
        }
    }
    
    @Override
    public int getWidth() {
        synchronized (this.mLock) {
            return this.mImageReaderProxy.getWidth();
        }
    }
    
    void imageIncoming(final ImageReaderProxy imageReaderProxy) {
        synchronized (this.mLock) {
            if (this.mClosed) {
                return;
            }
            int n = this.mPendingImages.size() + this.mMatchedImageProxies.size();
            int n2;
            if ((n2 = n) >= imageReaderProxy.getMaxImages()) {
                Logger.d("MetadataImageReader", "Skip to acquire the next image because the acquired image count has reached the max images count.");
                return;
            }
            while (true) {
                ImageProxy imageProxy;
                try {
                    try {
                        final ImageProxy acquireNextImage = imageReaderProxy.acquireNextImage();
                        n = n2;
                        if ((imageProxy = acquireNextImage) != null) {
                            --this.mUnAcquiredAvailableImageCount;
                            n = n2 + 1;
                            this.mPendingImages.put(acquireNextImage.getImageInfo().getTimestamp(), (Object)acquireNextImage);
                            this.matchImages();
                            imageProxy = acquireNextImage;
                        }
                    }
                    finally {}
                }
                catch (final IllegalStateException ex) {
                    Logger.d("MetadataImageReader", "Failed to acquire next image.", ex);
                    imageProxy = null;
                    n = n2;
                }
                if (imageProxy == null || this.mUnAcquiredAvailableImageCount <= 0 || (n2 = n) >= imageReaderProxy.getMaxImages()) {
                    return;
                }
            }
        }
    }
    
    @Override
    public void onImageClose(@NonNull final ImageProxy imageProxy) {
        synchronized (this.mLock) {
            this.dequeImageProxy(imageProxy);
        }
    }
    
    void resultIncoming(final CameraCaptureResult cameraCaptureResult) {
        synchronized (this.mLock) {
            if (this.mClosed) {
                return;
            }
            this.mPendingImageInfos.put(cameraCaptureResult.getTimestamp(), (Object)new CameraCaptureResultImageInfo(cameraCaptureResult));
            this.matchImages();
        }
    }
    
    @Override
    public void setOnImageAvailableListener(@NonNull final OnImageAvailableListener onImageAvailableListener, @NonNull final Executor executor) {
        synchronized (this.mLock) {
            this.mListener = Preconditions.checkNotNull(onImageAvailableListener);
            this.mExecutor = Preconditions.checkNotNull(executor);
            this.mImageReaderProxy.setOnImageAvailableListener(this.mTransformedListener, executor);
        }
    }
}

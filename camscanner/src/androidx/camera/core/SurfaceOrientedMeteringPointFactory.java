// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.RestrictTo;
import android.graphics.PointF;
import android.util.Size;
import android.util.Rational;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class SurfaceOrientedMeteringPointFactory extends MeteringPointFactory
{
    private final float mHeight;
    private final float mWidth;
    
    public SurfaceOrientedMeteringPointFactory(final float mWidth, final float mHeight) {
        this.mWidth = mWidth;
        this.mHeight = mHeight;
    }
    
    public SurfaceOrientedMeteringPointFactory(final float mWidth, final float mHeight, @NonNull final UseCase useCase) {
        super(getUseCaseAspectRatio(useCase));
        this.mWidth = mWidth;
        this.mHeight = mHeight;
    }
    
    @Nullable
    private static Rational getUseCaseAspectRatio(@Nullable final UseCase obj) {
        if (obj == null) {
            return null;
        }
        final Size attachedSurfaceResolution = obj.getAttachedSurfaceResolution();
        if (attachedSurfaceResolution != null) {
            return new Rational(attachedSurfaceResolution.getWidth(), attachedSurfaceResolution.getHeight());
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("UseCase ");
        sb.append(obj);
        sb.append(" is not bound.");
        throw new IllegalStateException(sb.toString());
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    protected PointF convertPoint(final float n, final float n2) {
        return new PointF(n / this.mWidth, n2 / this.mHeight);
    }
}

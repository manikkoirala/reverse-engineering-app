// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.Nullable;
import androidx.camera.core.impl.CameraConfig;
import androidx.annotation.RestrictTo;
import androidx.camera.core.impl.CameraInternal;
import java.util.LinkedHashSet;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface Camera
{
    @NonNull
    CameraControl getCameraControl();
    
    @NonNull
    CameraInfo getCameraInfo();
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    LinkedHashSet<CameraInternal> getCameraInternals();
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    CameraConfig getExtendedConfig();
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    boolean isUseCasesCombinationSupported(@NonNull final UseCase... p0);
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    void setExtendedConfig(@Nullable final CameraConfig p0);
}

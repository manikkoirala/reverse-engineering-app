// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.util.concurrent.Future;
import android.graphics.Rect;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.concurrent.ExecutionException;
import android.annotation.SuppressLint;
import androidx.core.util.Consumer;
import java.util.concurrent.CancellationException;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import androidx.core.util.Preconditions;
import java.util.concurrent.atomic.AtomicReference;
import androidx.annotation.RestrictTo;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;
import androidx.annotation.GuardedBy;
import android.view.Surface;
import com.google.common.util.concurrent.ListenableFuture;
import android.util.Size;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.camera.core.impl.DeferrableSurface;
import androidx.annotation.Nullable;
import android.util.Range;
import androidx.camera.core.impl.CameraInternal;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class SurfaceRequest
{
    private final CameraInternal mCamera;
    @Nullable
    private final Range<Integer> mExpectedFrameRate;
    private final DeferrableSurface mInternalDeferrableSurface;
    private final Object mLock;
    private final boolean mRGBA8888Required;
    private final CallbackToFutureAdapter.Completer<Void> mRequestCancellationCompleter;
    private final Size mResolution;
    private final ListenableFuture<Void> mSessionStatusFuture;
    private final CallbackToFutureAdapter.Completer<Surface> mSurfaceCompleter;
    final ListenableFuture<Surface> mSurfaceFuture;
    @GuardedBy("mLock")
    @Nullable
    private TransformationInfo mTransformationInfo;
    @GuardedBy("mLock")
    @Nullable
    private Executor mTransformationInfoExecutor;
    @GuardedBy("mLock")
    @Nullable
    private TransformationInfoListener mTransformationInfoListener;
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public SurfaceRequest(@NonNull final Size size, @NonNull final CameraInternal cameraInternal, final boolean b) {
        this(size, cameraInternal, b, null);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public SurfaceRequest(@NonNull final Size size, @NonNull final CameraInternal mCamera, final boolean mrgba8888Required, @Nullable final Range<Integer> mExpectedFrameRate) {
        this.mLock = new Object();
        this.mResolution = size;
        this.mCamera = mCamera;
        this.mRGBA8888Required = mrgba8888Required;
        this.mExpectedFrameRate = mExpectedFrameRate;
        final StringBuilder sb = new StringBuilder();
        sb.append("SurfaceRequest[size: ");
        sb.append(size);
        sb.append(", id: ");
        sb.append(this.hashCode());
        sb.append("]");
        final String string = sb.toString();
        final AtomicReference atomicReference = new AtomicReference(null);
        final com.google.common.util.concurrent.ListenableFuture<Object> future = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Object>)new \u3007o0O0O8(atomicReference, string));
        final CallbackToFutureAdapter.Completer mRequestCancellationCompleter = (CallbackToFutureAdapter.Completer)Preconditions.checkNotNull((CallbackToFutureAdapter.Completer)atomicReference.get());
        this.mRequestCancellationCompleter = mRequestCancellationCompleter;
        final AtomicReference atomicReference2 = new AtomicReference(null);
        Futures.addCallback(this.mSessionStatusFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new \u3007\u3007o8(atomicReference2, string)), new FutureCallback<Void>(this, mRequestCancellationCompleter, future) {
            final SurfaceRequest this$0;
            final CallbackToFutureAdapter.Completer val$requestCancellationCompleter;
            final ListenableFuture val$requestCancellationFuture;
            
            @Override
            public void onFailure(@NonNull final Throwable t) {
                if (t instanceof RequestCancelledException) {
                    Preconditions.checkState(((Future)this.val$requestCancellationFuture).cancel(false));
                }
                else {
                    Preconditions.checkState(this.val$requestCancellationCompleter.set(null));
                }
            }
            
            @Override
            public void onSuccess(@Nullable final Void void1) {
                Preconditions.checkState(this.val$requestCancellationCompleter.set(null));
            }
        }, CameraXExecutors.directExecutor());
        final CallbackToFutureAdapter.Completer completer = (CallbackToFutureAdapter.Completer)Preconditions.checkNotNull((CallbackToFutureAdapter.Completer)atomicReference2.get());
        final AtomicReference atomicReference3 = new AtomicReference(null);
        final com.google.common.util.concurrent.ListenableFuture<Surface> future2 = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Surface>)new Oo\u3007O(atomicReference3, string));
        this.mSurfaceFuture = future2;
        this.mSurfaceCompleter = (CallbackToFutureAdapter.Completer<Surface>)Preconditions.checkNotNull(atomicReference3.get());
        final DeferrableSurface mInternalDeferrableSurface = new DeferrableSurface(this, size, 34) {
            final SurfaceRequest this$0;
            
            @NonNull
            @Override
            protected ListenableFuture<Surface> provideSurface() {
                return this.this$0.mSurfaceFuture;
            }
        };
        this.mInternalDeferrableSurface = mInternalDeferrableSurface;
        final ListenableFuture<Void> terminationFuture = mInternalDeferrableSurface.getTerminationFuture();
        Futures.addCallback((com.google.common.util.concurrent.ListenableFuture<Object>)future2, (FutureCallback<? super Object>)new FutureCallback<Surface>(this, terminationFuture, completer, string) {
            final SurfaceRequest this$0;
            final CallbackToFutureAdapter.Completer val$sessionStatusCompleter;
            final String val$surfaceRequestString;
            final ListenableFuture val$terminationFuture;
            
            @Override
            public void onFailure(@NonNull final Throwable t) {
                if (t instanceof CancellationException) {
                    final CallbackToFutureAdapter.Completer val$sessionStatusCompleter = this.val$sessionStatusCompleter;
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this.val$surfaceRequestString);
                    sb.append(" cancelled.");
                    Preconditions.checkState(val$sessionStatusCompleter.setException(new RequestCancelledException(sb.toString(), t)));
                }
                else {
                    this.val$sessionStatusCompleter.set(null);
                }
            }
            
            @Override
            public void onSuccess(@Nullable final Surface surface) {
                Futures.propagate((com.google.common.util.concurrent.ListenableFuture<Object>)this.val$terminationFuture, this.val$sessionStatusCompleter);
            }
        }, CameraXExecutors.directExecutor());
        terminationFuture.addListener((Runnable)new O0(this), CameraXExecutors.directExecutor());
    }
    
    @SuppressLint({ "PairedRegistration" })
    public void addRequestCancellationListener(@NonNull final Executor executor, @NonNull final Runnable runnable) {
        this.mRequestCancellationCompleter.addCancellationListener(runnable, executor);
    }
    
    public void clearTransformationInfoListener() {
        synchronized (this.mLock) {
            this.mTransformationInfoListener = null;
            this.mTransformationInfoExecutor = null;
        }
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public CameraInternal getCamera() {
        return this.mCamera;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public DeferrableSurface getDeferrableSurface() {
        return this.mInternalDeferrableSurface;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public Range<Integer> getExpectedFrameRate() {
        return this.mExpectedFrameRate;
    }
    
    @NonNull
    public Size getResolution() {
        return this.mResolution;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public boolean isRGBA8888Required() {
        return this.mRGBA8888Required;
    }
    
    public void provideSurface(@NonNull final Surface surface, @NonNull final Executor executor, @NonNull final Consumer<Result> consumer) {
        if (!this.mSurfaceCompleter.set(surface) && !((Future)this.mSurfaceFuture).isCancelled()) {
            Preconditions.checkState(((Future)this.mSurfaceFuture).isDone());
            try {
                this.mSurfaceFuture.get();
                executor.execute(new ooOO(consumer, surface));
            }
            catch (final InterruptedException | ExecutionException ex) {
                executor.execute(new OOO8o\u3007\u3007(consumer, surface));
            }
        }
        else {
            Futures.addCallback(this.mSessionStatusFuture, new FutureCallback<Void>(this, consumer, surface) {
                final SurfaceRequest this$0;
                final Consumer val$resultListener;
                final Surface val$surface;
                
                @Override
                public void onFailure(@NonNull final Throwable obj) {
                    final boolean b = obj instanceof RequestCancelledException;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Camera surface session should only fail with request cancellation. Instead failed due to:\n");
                    sb.append(obj);
                    Preconditions.checkState(b, sb.toString());
                    this.val$resultListener.accept(Result.of(1, this.val$surface));
                }
                
                @Override
                public void onSuccess(@Nullable final Void void1) {
                    this.val$resultListener.accept(Result.of(0, this.val$surface));
                }
            }, executor);
        }
    }
    
    public void setTransformationInfoListener(@NonNull final Executor mTransformationInfoExecutor, @NonNull final TransformationInfoListener mTransformationInfoListener) {
        synchronized (this.mLock) {
            this.mTransformationInfoListener = mTransformationInfoListener;
            this.mTransformationInfoExecutor = mTransformationInfoExecutor;
            final TransformationInfo mTransformationInfo = this.mTransformationInfo;
            monitorexit(this.mLock);
            if (mTransformationInfo != null) {
                mTransformationInfoExecutor.execute(new \u30070O\u3007Oo(mTransformationInfoListener, mTransformationInfo));
            }
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void updateTransformationInfo(@NonNull final TransformationInfo mTransformationInfo) {
        synchronized (this.mLock) {
            this.mTransformationInfo = mTransformationInfo;
            final TransformationInfoListener mTransformationInfoListener = this.mTransformationInfoListener;
            final Executor mTransformationInfoExecutor = this.mTransformationInfoExecutor;
            monitorexit(this.mLock);
            if (mTransformationInfoListener != null && mTransformationInfoExecutor != null) {
                mTransformationInfoExecutor.execute(new \u300700O0O0(mTransformationInfoListener, mTransformationInfo));
            }
        }
    }
    
    public boolean willNotProvideSurface() {
        return this.mSurfaceCompleter.setException(new DeferrableSurface.SurfaceUnavailableException("Surface request will not complete."));
    }
    
    private static final class RequestCancelledException extends RuntimeException
    {
        RequestCancelledException(@NonNull final String message, @NonNull final Throwable cause) {
            super(message, cause);
        }
    }
    
    public abstract static class Result
    {
        public static final int RESULT_INVALID_SURFACE = 2;
        public static final int RESULT_REQUEST_CANCELLED = 1;
        public static final int RESULT_SURFACE_ALREADY_PROVIDED = 3;
        public static final int RESULT_SURFACE_USED_SUCCESSFULLY = 0;
        public static final int RESULT_WILL_NOT_PROVIDE_SURFACE = 4;
        
        Result() {
        }
        
        @NonNull
        static Result of(final int n, @NonNull final Surface surface) {
            return (Result)new AutoValue_SurfaceRequest_Result(n, surface);
        }
        
        public abstract int getResultCode();
        
        @NonNull
        public abstract Surface getSurface();
        
        @Retention(RetentionPolicy.SOURCE)
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public @interface ResultCode {
        }
    }
    
    public abstract static class TransformationInfo
    {
        TransformationInfo() {
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public static TransformationInfo of(@NonNull final Rect rect, final int n, final int n2) {
            return (TransformationInfo)new AutoValue_SurfaceRequest_TransformationInfo(rect, n, n2);
        }
        
        @NonNull
        public abstract Rect getCropRect();
        
        public abstract int getRotationDegrees();
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public abstract int getTargetRotation();
    }
    
    public interface TransformationInfoListener
    {
        void onTransformationInfoUpdate(@NonNull final TransformationInfo p0);
    }
}

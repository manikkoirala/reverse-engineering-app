// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import android.graphics.Rect;
import androidx.annotation.NonNull;
import android.util.Size;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public abstract class ResolutionInfo
{
    ResolutionInfo() {
    }
    
    @NonNull
    static ResolutionInfo create(@NonNull final Size size, @NonNull final Rect rect, final int n) {
        return new AutoValue_ResolutionInfo(size, rect, n);
    }
    
    @NonNull
    public abstract Rect getCropRect();
    
    @NonNull
    public abstract Size getResolution();
    
    public abstract int getRotationDegrees();
}

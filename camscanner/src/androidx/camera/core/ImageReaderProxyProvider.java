// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.NonNull;
import androidx.camera.core.impl.ImageReaderProxy;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public interface ImageReaderProxyProvider
{
    @NonNull
    ImageReaderProxy newInstance(final int p0, final int p1, final int p2, final int p3, final long p4);
}

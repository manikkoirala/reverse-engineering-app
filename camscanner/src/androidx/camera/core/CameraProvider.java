// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.NonNull;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface CameraProvider
{
    @NonNull
    List<CameraInfo> getAvailableCameraInfos();
    
    boolean hasCamera(@NonNull final CameraSelector p0) throws CameraInfoUnavailableException;
}

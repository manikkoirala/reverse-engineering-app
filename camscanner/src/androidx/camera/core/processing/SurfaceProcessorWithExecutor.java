// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

import androidx.annotation.VisibleForTesting;
import androidx.camera.core.SurfaceOutput;
import androidx.camera.core.SurfaceRequest;
import androidx.core.util.Preconditions;
import androidx.camera.core.SurfaceProcessor;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;

public class SurfaceProcessorWithExecutor implements SurfaceProcessorInternal
{
    @NonNull
    private final Executor mExecutor;
    @NonNull
    private final SurfaceProcessor mSurfaceProcessor;
    
    public SurfaceProcessorWithExecutor(@NonNull final SurfaceProcessor mSurfaceProcessor, @NonNull final Executor mExecutor) {
        Preconditions.checkState(mSurfaceProcessor instanceof SurfaceProcessorInternal ^ true, "SurfaceProcessorInternal should always be thread safe. Do not wrap.");
        this.mSurfaceProcessor = mSurfaceProcessor;
        this.mExecutor = mExecutor;
    }
    
    @NonNull
    @VisibleForTesting
    public Executor getExecutor() {
        return this.mExecutor;
    }
    
    @NonNull
    @VisibleForTesting
    public SurfaceProcessor getProcessor() {
        return this.mSurfaceProcessor;
    }
    
    @Override
    public void onInputSurface(@NonNull final SurfaceRequest surfaceRequest) {
        this.mExecutor.execute(new o800o8O(this, surfaceRequest));
    }
    
    @Override
    public void onOutputSurface(@NonNull final SurfaceOutput surfaceOutput) {
        this.mExecutor.execute(new \u3007O888o0o(this, surfaceOutput));
    }
    
    @Override
    public void release() {
    }
}

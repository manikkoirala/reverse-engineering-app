// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

import androidx.annotation.NonNull;
import androidx.camera.core.ImageProxy;
import java.util.List;
import androidx.annotation.RequiresApi;
import androidx.camera.core.ImageProcessor;

@RequiresApi(api = 21)
public class ImageProcessorRequest implements Request
{
    @NonNull
    private final List<ImageProxy> mImageProxies;
    private final int mOutputFormat;
    
    public ImageProcessorRequest(@NonNull final List<ImageProxy> mImageProxies, final int mOutputFormat) {
        this.mImageProxies = mImageProxies;
        this.mOutputFormat = mOutputFormat;
    }
    
    @NonNull
    @Override
    public List<ImageProxy> getInputImages() {
        return this.mImageProxies;
    }
    
    @Override
    public int getOutputFormat() {
        return this.mOutputFormat;
    }
}

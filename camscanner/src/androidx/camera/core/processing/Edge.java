// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

import kotlin.jvm.internal.Intrinsics;
import androidx.annotation.NonNull;
import androidx.core.util.Consumer;

public class Edge<T> implements Consumer<T>
{
    private Consumer<T> mListener;
    
    @Override
    public void accept(@NonNull final T t) {
        Intrinsics.o\u30070((Object)this.mListener, "Listener is not set.");
        this.mListener.accept(t);
    }
    
    public void setListener(@NonNull final Consumer<T> mListener) {
        this.mListener = mListener;
    }
}

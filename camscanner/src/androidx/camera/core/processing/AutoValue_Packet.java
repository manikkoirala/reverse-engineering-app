// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.Size;
import android.graphics.Matrix;
import androidx.camera.core.impl.utils.Exif;
import android.graphics.Rect;
import androidx.camera.core.impl.CameraCaptureResult;

final class AutoValue_Packet<T> extends Packet<T>
{
    private final CameraCaptureResult cameraCaptureResult;
    private final Rect cropRect;
    private final T data;
    private final Exif exif;
    private final int format;
    private final int rotationDegrees;
    private final Matrix sensorToBufferTransform;
    private final Size size;
    
    AutoValue_Packet(final T data, @Nullable final Exif exif, final int format, final Size size, final Rect cropRect, final int rotationDegrees, final Matrix sensorToBufferTransform, final CameraCaptureResult cameraCaptureResult) {
        if (data == null) {
            throw new NullPointerException("Null data");
        }
        this.data = data;
        this.exif = exif;
        this.format = format;
        if (size == null) {
            throw new NullPointerException("Null size");
        }
        this.size = size;
        if (cropRect == null) {
            throw new NullPointerException("Null cropRect");
        }
        this.cropRect = cropRect;
        this.rotationDegrees = rotationDegrees;
        if (sensorToBufferTransform == null) {
            throw new NullPointerException("Null sensorToBufferTransform");
        }
        this.sensorToBufferTransform = sensorToBufferTransform;
        if (cameraCaptureResult != null) {
            this.cameraCaptureResult = cameraCaptureResult;
            return;
        }
        throw new NullPointerException("Null cameraCaptureResult");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof Packet) {
            final Packet packet = (Packet)o;
            if (this.data.equals(packet.getData())) {
                final Exif exif = this.exif;
                if (exif == null) {
                    if (packet.getExif() != null) {
                        return false;
                    }
                }
                else if (!exif.equals(packet.getExif())) {
                    return false;
                }
                if (this.format == packet.getFormat() && this.size.equals((Object)packet.getSize()) && this.cropRect.equals((Object)packet.getCropRect()) && this.rotationDegrees == packet.getRotationDegrees() && this.sensorToBufferTransform.equals((Object)packet.getSensorToBufferTransform()) && this.cameraCaptureResult.equals(packet.getCameraCaptureResult())) {
                    return b;
                }
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @NonNull
    @Override
    public CameraCaptureResult getCameraCaptureResult() {
        return this.cameraCaptureResult;
    }
    
    @NonNull
    @Override
    public Rect getCropRect() {
        return this.cropRect;
    }
    
    @NonNull
    @Override
    public T getData() {
        return this.data;
    }
    
    @Nullable
    @Override
    public Exif getExif() {
        return this.exif;
    }
    
    @Override
    public int getFormat() {
        return this.format;
    }
    
    @Override
    public int getRotationDegrees() {
        return this.rotationDegrees;
    }
    
    @NonNull
    @Override
    public Matrix getSensorToBufferTransform() {
        return this.sensorToBufferTransform;
    }
    
    @NonNull
    @Override
    public Size getSize() {
        return this.size;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.data.hashCode();
        final Exif exif = this.exif;
        int hashCode2;
        if (exif == null) {
            hashCode2 = 0;
        }
        else {
            hashCode2 = exif.hashCode();
        }
        return (((((((hashCode ^ 0xF4243) * 1000003 ^ hashCode2) * 1000003 ^ this.format) * 1000003 ^ this.size.hashCode()) * 1000003 ^ this.cropRect.hashCode()) * 1000003 ^ this.rotationDegrees) * 1000003 ^ this.sensorToBufferTransform.hashCode()) * 1000003 ^ this.cameraCaptureResult.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Packet{data=");
        sb.append(this.data);
        sb.append(", exif=");
        sb.append(this.exif);
        sb.append(", format=");
        sb.append(this.format);
        sb.append(", size=");
        sb.append(this.size);
        sb.append(", cropRect=");
        sb.append(this.cropRect);
        sb.append(", rotationDegrees=");
        sb.append(this.rotationDegrees);
        sb.append(", sensorToBufferTransform=");
        sb.append(this.sensorToBufferTransform);
        sb.append(", cameraCaptureResult=");
        sb.append(this.cameraCaptureResult);
        sb.append("}");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public interface ShaderProvider
{
    public static final ShaderProvider DEFAULT = new ShaderProvider() {};
    
    @Nullable
    String createFragmentShader(@NonNull final String p0, @NonNull final String p1);
}

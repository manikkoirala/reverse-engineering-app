// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

import android.util.Range;
import androidx.camera.core.impl.CameraInternal;
import java.util.concurrent.Executor;
import androidx.camera.core.impl.utils.futures.AsyncFunction;
import androidx.camera.core.impl.utils.Threads;
import androidx.annotation.MainThread;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.core.util.Preconditions;
import androidx.camera.core.SurfaceOutput;
import androidx.annotation.NonNull;
import android.util.Size;
import com.google.common.util.concurrent.ListenableFuture;
import android.graphics.Matrix;
import androidx.camera.core.SurfaceRequest;
import android.graphics.Rect;
import androidx.annotation.Nullable;
import android.view.Surface;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.DeferrableSurface;

@RequiresApi(api = 21)
public class SettableSurface extends DeferrableSurface
{
    CallbackToFutureAdapter.Completer<Surface> mCompleter;
    @Nullable
    private SurfaceOutputImpl mConsumerToNotify;
    private final Rect mCropRect;
    private boolean mHasConsumer;
    private final boolean mHasEmbeddedTransform;
    private boolean mHasProvider;
    private final boolean mMirroring;
    @Nullable
    private SurfaceRequest mProviderSurfaceRequest;
    private int mRotationDegrees;
    private final Matrix mSensorToBufferTransform;
    private final ListenableFuture<Surface> mSurfaceFuture;
    private final int mTargets;
    
    public SettableSurface(final int mTargets, @NonNull final Size size, final int n, @NonNull final Matrix mSensorToBufferTransform, final boolean mHasEmbeddedTransform, @NonNull final Rect mCropRect, final int mRotationDegrees, final boolean mMirroring) {
        super(size, n);
        this.mHasProvider = false;
        this.mHasConsumer = false;
        this.mTargets = mTargets;
        this.mSensorToBufferTransform = mSensorToBufferTransform;
        this.mHasEmbeddedTransform = mHasEmbeddedTransform;
        this.mCropRect = mCropRect;
        this.mRotationDegrees = mRotationDegrees;
        this.mMirroring = mMirroring;
        this.mSurfaceFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Surface>)new OO0o\u3007\u3007\u3007\u30070(this, size));
    }
    
    @MainThread
    private void notifyTransformationInfoUpdate() {
        final SurfaceRequest mProviderSurfaceRequest = this.mProviderSurfaceRequest;
        if (mProviderSurfaceRequest != null) {
            mProviderSurfaceRequest.updateTransformationInfo(SurfaceRequest.TransformationInfo.of(this.mCropRect, this.mRotationDegrees, -1));
        }
    }
    
    @Override
    public final void close() {
        super.close();
        CameraXExecutors.mainThreadExecutor().execute(new \u3007O8o08O(this));
    }
    
    @MainThread
    @NonNull
    public ListenableFuture<SurfaceOutput> createSurfaceOutputFuture(@NonNull final SurfaceOutput.GlTransformOptions glTransformOptions, @NonNull final Size size, @NonNull final Rect rect, final int n, final boolean b) {
        Threads.checkMainThread();
        Preconditions.checkState(this.mHasConsumer ^ true, "Consumer can only be linked once.");
        this.mHasConsumer = true;
        return Futures.transformAsync(this.getSurface(), (AsyncFunction<? super Surface, ? extends SurfaceOutput>)new Oooo8o0\u3007(this, glTransformOptions, size, rect, n, b), (Executor)CameraXExecutors.mainThreadExecutor());
    }
    
    @MainThread
    @NonNull
    public SurfaceRequest createSurfaceRequest(@NonNull final CameraInternal cameraInternal) {
        return this.createSurfaceRequest(cameraInternal, null);
    }
    
    @MainThread
    @NonNull
    public SurfaceRequest createSurfaceRequest(@NonNull final CameraInternal cameraInternal, @Nullable final Range<Integer> range) {
        Threads.checkMainThread();
        final SurfaceRequest mProviderSurfaceRequest = new SurfaceRequest(this.getSize(), cameraInternal, true, range);
        try {
            this.setProvider(mProviderSurfaceRequest.getDeferrableSurface());
            this.mProviderSurfaceRequest = mProviderSurfaceRequest;
            this.notifyTransformationInfoUpdate();
            return mProviderSurfaceRequest;
        }
        catch (final SurfaceClosedException cause) {
            throw new AssertionError("Surface is somehow already closed", cause);
        }
    }
    
    @NonNull
    public Rect getCropRect() {
        return this.mCropRect;
    }
    
    public int getFormat() {
        return this.getPrescribedStreamFormat();
    }
    
    public boolean getMirroring() {
        return this.mMirroring;
    }
    
    public int getRotationDegrees() {
        return this.mRotationDegrees;
    }
    
    @NonNull
    public Matrix getSensorToBufferTransform() {
        return this.mSensorToBufferTransform;
    }
    
    @NonNull
    public Size getSize() {
        return this.getPrescribedSize();
    }
    
    public int getTargets() {
        return this.mTargets;
    }
    
    public boolean hasEmbeddedTransform() {
        return this.mHasEmbeddedTransform;
    }
    
    @NonNull
    @Override
    protected ListenableFuture<Surface> provideSurface() {
        return this.mSurfaceFuture;
    }
    
    @MainThread
    public void setProvider(@NonNull final DeferrableSurface deferrableSurface) throws SurfaceClosedException {
        Threads.checkMainThread();
        this.setProvider(deferrableSurface.getSurface());
        deferrableSurface.incrementUseCount();
        this.getTerminationFuture().addListener((Runnable)new OO0o\u3007\u3007(deferrableSurface), CameraXExecutors.directExecutor());
    }
    
    @MainThread
    public void setProvider(@NonNull final ListenableFuture<Surface> listenableFuture) {
        Threads.checkMainThread();
        Preconditions.checkState(this.mHasProvider ^ true, "Provider can only be linked once.");
        this.mHasProvider = true;
        Futures.propagate(listenableFuture, this.mCompleter);
    }
    
    @MainThread
    public void setRotationDegrees(final int mRotationDegrees) {
        Threads.checkMainThread();
        if (this.mRotationDegrees == mRotationDegrees) {
            return;
        }
        this.mRotationDegrees = mRotationDegrees;
        this.notifyTransformationInfoUpdate();
    }
}

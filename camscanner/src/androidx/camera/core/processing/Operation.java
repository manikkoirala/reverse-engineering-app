// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

import androidx.annotation.WorkerThread;
import androidx.camera.core.ImageCaptureException;
import androidx.annotation.NonNull;

public interface Operation<I, O>
{
    @NonNull
    @WorkerThread
    O apply(@NonNull final I p0) throws ImageCaptureException;
}

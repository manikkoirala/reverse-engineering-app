// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

import java.util.concurrent.Future;
import androidx.camera.core.ImageCaptureException;
import java.util.concurrent.ExecutionException;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import java.util.Objects;
import androidx.core.util.Preconditions;
import androidx.camera.core.CameraEffect;
import androidx.camera.core.ImageProcessor;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;
import androidx.annotation.RequiresApi;

@RequiresApi(api = 21)
public class InternalImageProcessor
{
    @NonNull
    private final Executor mExecutor;
    @NonNull
    private final ImageProcessor mImageProcessor;
    
    public InternalImageProcessor(@NonNull final CameraEffect cameraEffect) {
        Preconditions.checkArgument(cameraEffect.getTargets() == 4);
        this.mExecutor = cameraEffect.getProcessorExecutor();
        final ImageProcessor imageProcessor = cameraEffect.getImageProcessor();
        Objects.requireNonNull(imageProcessor);
        this.mImageProcessor = imageProcessor;
    }
    
    @NonNull
    public ImageProcessor.Response safeProcess(@NonNull ImageProcessor.Request ex) throws ImageCaptureException {
        try {
            ex = (InterruptedException)((Future<ImageProcessor.Response>)CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Object>)new \u300780\u3007808\u3007O(this, (ImageProcessor.Request)ex))).get();
            return (ImageProcessor.Response)ex;
        }
        catch (final InterruptedException ex) {}
        catch (final ExecutionException ex2) {}
        Throwable cause = ex;
        if (ex.getCause() != null) {
            cause = ex.getCause();
        }
        throw new ImageCaptureException(0, "Failed to invoke ImageProcessor.", cause);
    }
}

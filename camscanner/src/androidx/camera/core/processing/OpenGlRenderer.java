// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

import android.opengl.EGLExt;
import java.nio.Buffer;
import java.util.Iterator;
import androidx.camera.core.Logger;
import android.util.Size;
import java.nio.ByteOrder;
import java.nio.ByteBuffer;
import java.util.Objects;
import androidx.core.util.Preconditions;
import android.opengl.GLES20;
import android.opengl.EGL14;
import java.util.HashMap;
import java.util.Locale;
import android.opengl.EGLSurface;
import android.view.Surface;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import android.opengl.EGLDisplay;
import androidx.annotation.NonNull;
import android.opengl.EGLContext;
import android.opengl.EGLConfig;
import androidx.annotation.Nullable;
import java.nio.FloatBuffer;
import androidx.annotation.WorkerThread;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
@WorkerThread
public final class OpenGlRenderer
{
    private static final String DEFAULT_FRAGMENT_SHADER;
    private static final String DEFAULT_VERTEX_SHADER;
    private static final int SIZEOF_FLOAT = 4;
    private static final String TAG = "OpenGlRenderer";
    private static final FloatBuffer TEX_BUF;
    private static final float[] TEX_COORDS;
    private static final int TEX_TARGET = 36197;
    private static final String VAR_TEXTURE = "sTexture";
    private static final String VAR_TEXTURE_COORD = "vTextureCoord";
    private static final FloatBuffer VERTEX_BUF;
    private static final float[] VERTEX_COORDS;
    @Nullable
    private OutputSurface mCurrentOutputSurface;
    @Nullable
    private EGLConfig mEglConfig;
    @NonNull
    private EGLContext mEglContext;
    @NonNull
    private EGLDisplay mEglDisplay;
    @Nullable
    private Thread mGlThread;
    private final AtomicBoolean mInitialized;
    private final Map<Surface, OutputSurface> mOutputSurfaceMap;
    private int mPositionLoc;
    private int mProgramHandle;
    @NonNull
    private EGLSurface mTempSurface;
    private int mTexCoordLoc;
    private int mTexId;
    private int mTexMatrixLoc;
    
    static {
        final Locale us = Locale.US;
        DEFAULT_VERTEX_SHADER = String.format(us, "uniform mat4 uTexMatrix;\nattribute vec4 aPosition;\nattribute vec4 aTextureCoord;\nvarying vec2 %s;\nvoid main() {\n    gl_Position = aPosition;\n    %s = (uTexMatrix * aTextureCoord).xy;\n}\n", "vTextureCoord", "vTextureCoord");
        DEFAULT_FRAGMENT_SHADER = String.format(us, "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 %s;\nuniform samplerExternalOES %s;\nvoid main() {\n    gl_FragColor = texture2D(%s, %s);\n}\n", "vTextureCoord", "sTexture", "sTexture", "vTextureCoord");
        final float[] array;
        final float[] vertex_COORDS = array = new float[8];
        array[1] = (array[0] = -1.0f);
        array[2] = 1.0f;
        array[4] = (array[3] = -1.0f);
        array[5] = 1.0f;
        array[7] = (array[6] = 1.0f);
        VERTEX_COORDS = vertex_COORDS;
        VERTEX_BUF = createFloatBuffer(vertex_COORDS);
        final float[] array2;
        final float[] tex_COORDS = array2 = new float[8];
        array2[1] = (array2[0] = 0.0f);
        array2[2] = 1.0f;
        array2[4] = (array2[3] = 0.0f);
        array2[5] = 1.0f;
        array2[7] = (array2[6] = 1.0f);
        TEX_COORDS = tex_COORDS;
        TEX_BUF = createFloatBuffer(tex_COORDS);
    }
    
    public OpenGlRenderer() {
        this.mInitialized = new AtomicBoolean(false);
        this.mOutputSurfaceMap = new HashMap<Surface, OutputSurface>();
        this.mEglDisplay = EGL14.EGL_NO_DISPLAY;
        this.mEglContext = EGL14.EGL_NO_CONTEXT;
        this.mTempSurface = EGL14.EGL_NO_SURFACE;
        this.mTexId = -1;
        this.mProgramHandle = -1;
        this.mTexMatrixLoc = -1;
        this.mPositionLoc = -1;
        this.mTexCoordLoc = -1;
    }
    
    private static void checkEglErrorOrThrow(@NonNull final String str) {
        final int eglGetError = EGL14.eglGetError();
        if (eglGetError == 12288) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(": EGL error: 0x");
        sb.append(Integer.toHexString(eglGetError));
        throw new IllegalStateException(sb.toString());
    }
    
    private static void checkGlErrorOrThrow(@NonNull final String str) {
        final int glGetError = GLES20.glGetError();
        if (glGetError == 0) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(": GL error 0x");
        sb.append(Integer.toHexString(glGetError));
        throw new IllegalStateException(sb.toString());
    }
    
    private void checkGlThreadOrThrow() {
        Preconditions.checkState(this.mGlThread == Thread.currentThread(), "Method call must be called on the GL thread.");
    }
    
    private void checkInitializedOrThrow(final boolean b) {
        final boolean b2 = b == this.mInitialized.get();
        String s;
        if (b) {
            s = "OpenGlRenderer is not initialized";
        }
        else {
            s = "OpenGlRenderer is already initialized";
        }
        Preconditions.checkState(b2, s);
    }
    
    private static void checkLocationOrThrow(final int n, @NonNull final String str) {
        if (n >= 0) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unable to locate '");
        sb.append(str);
        sb.append("' in program");
        throw new IllegalStateException(sb.toString());
    }
    
    private void createEglContext() {
        final EGLDisplay eglGetDisplay = EGL14.eglGetDisplay(0);
        this.mEglDisplay = eglGetDisplay;
        if (Objects.equals(eglGetDisplay, EGL14.EGL_NO_DISPLAY)) {
            throw new IllegalStateException("Unable to get EGL14 display");
        }
        final int[] array = new int[2];
        if (!EGL14.eglInitialize(this.mEglDisplay, array, 0, array, 1)) {
            this.mEglDisplay = EGL14.EGL_NO_DISPLAY;
            throw new IllegalStateException("Unable to initialize EGL14");
        }
        final EGLConfig[] array2 = { null };
        if (EGL14.eglChooseConfig(this.mEglDisplay, new int[] { 12324, 8, 12323, 8, 12322, 8, 12321, 8, 12352, 4, 12610, 1, 12339, 5, 12344 }, 0, array2, 0, 1, new int[] { 0 }, 0)) {
            final EGLConfig mEglConfig = array2[0];
            final EGLContext eglCreateContext = EGL14.eglCreateContext(this.mEglDisplay, mEglConfig, EGL14.EGL_NO_CONTEXT, new int[] { 12440, 2, 12344 }, 0);
            checkEglErrorOrThrow("eglCreateContext");
            this.mEglConfig = mEglConfig;
            this.mEglContext = eglCreateContext;
            final int[] array3 = { 0 };
            EGL14.eglQueryContext(this.mEglDisplay, eglCreateContext, 12440, array3, 0);
            final StringBuilder sb = new StringBuilder();
            sb.append("EGLContext created, client version ");
            sb.append(array3[0]);
            return;
        }
        throw new IllegalStateException("Unable to find a suitable EGLConfig");
    }
    
    @NonNull
    public static FloatBuffer createFloatBuffer(@NonNull final float[] src) {
        final ByteBuffer allocateDirect = ByteBuffer.allocateDirect(src.length * 4);
        allocateDirect.order(ByteOrder.nativeOrder());
        final FloatBuffer floatBuffer = allocateDirect.asFloatBuffer();
        floatBuffer.put(src);
        floatBuffer.position(0);
        return floatBuffer;
    }
    
    @NonNull
    private static EGLSurface createPBufferSurface(@NonNull final EGLDisplay eglDisplay, @NonNull final EGLConfig eglConfig, final int n, final int n2) {
        final EGLSurface eglCreatePbufferSurface = EGL14.eglCreatePbufferSurface(eglDisplay, eglConfig, new int[] { 12375, n, 12374, n2, 12344 }, 0);
        checkEglErrorOrThrow("eglCreatePbufferSurface");
        if (eglCreatePbufferSurface != null) {
            return eglCreatePbufferSurface;
        }
        throw new IllegalStateException("surface was null");
    }
    
    private void createProgram(@NonNull final ShaderProvider ex) {
        int loadShader = 0;
        int loadFragmentShader = 0;
        Label_0164: {
            try {
                loadShader = loadShader(35633, OpenGlRenderer.DEFAULT_VERTEX_SHADER);
                try {
                    loadFragmentShader = this.loadFragmentShader((ShaderProvider)ex);
                    try {
                        final int glCreateProgram = GLES20.glCreateProgram();
                        try {
                            checkGlErrorOrThrow("glCreateProgram");
                            GLES20.glAttachShader(glCreateProgram, loadShader);
                            checkGlErrorOrThrow("glAttachShader");
                            GLES20.glAttachShader(glCreateProgram, loadFragmentShader);
                            checkGlErrorOrThrow("glAttachShader");
                            GLES20.glLinkProgram(glCreateProgram);
                            final int[] array = { 0 };
                            GLES20.glGetProgramiv(glCreateProgram, 35714, array, 0);
                            if (array[0] == 1) {
                                this.mProgramHandle = glCreateProgram;
                                return;
                            }
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Could not link program: ");
                            sb.append(GLES20.glGetProgramInfoLog(glCreateProgram));
                            throw new IllegalStateException(sb.toString());
                        }
                        catch (final IllegalArgumentException ex) {}
                        catch (final IllegalStateException ex) {}
                    }
                    catch (final IllegalArgumentException ex) {}
                    catch (final IllegalStateException ex) {}
                }
                catch (final IllegalArgumentException ex) {}
                catch (final IllegalStateException ex2) {}
                loadFragmentShader = -1;
                break Label_0164;
            }
            catch (final IllegalArgumentException ex) {}
            catch (final IllegalStateException ex3) {}
            loadFragmentShader = -1;
            loadShader = -1;
        }
        final int glCreateProgram = -1;
        if (loadShader != -1) {
            GLES20.glDeleteShader(loadShader);
        }
        if (loadFragmentShader != -1) {
            GLES20.glDeleteShader(loadFragmentShader);
        }
        if (glCreateProgram != -1) {
            GLES20.glDeleteProgram(glCreateProgram);
        }
        throw ex;
    }
    
    private void createTempSurface() {
        final EGLDisplay mEglDisplay = this.mEglDisplay;
        final EGLConfig mEglConfig = this.mEglConfig;
        Objects.requireNonNull(mEglConfig);
        this.mTempSurface = createPBufferSurface(mEglDisplay, mEglConfig, 1, 1);
    }
    
    private void createTexture() {
        final int[] array = { 0 };
        GLES20.glGenTextures(1, array, 0);
        checkGlErrorOrThrow("glGenTextures");
        final int n = array[0];
        GLES20.glBindTexture(36197, n);
        final StringBuilder sb = new StringBuilder();
        sb.append("glBindTexture ");
        sb.append(n);
        checkGlErrorOrThrow(sb.toString());
        GLES20.glTexParameterf(36197, 10241, 9728.0f);
        GLES20.glTexParameterf(36197, 10240, 9729.0f);
        GLES20.glTexParameteri(36197, 10242, 33071);
        GLES20.glTexParameteri(36197, 10243, 33071);
        checkGlErrorOrThrow("glTexParameter");
        this.mTexId = n;
    }
    
    @NonNull
    private static EGLSurface createWindowSurface(@NonNull final EGLDisplay eglDisplay, @NonNull final EGLConfig eglConfig, @NonNull final Surface surface) {
        final EGLSurface eglCreateWindowSurface = EGL14.eglCreateWindowSurface(eglDisplay, eglConfig, (Object)surface, new int[] { 12344 }, 0);
        checkEglErrorOrThrow("eglCreateWindowSurface");
        if (eglCreateWindowSurface != null) {
            return eglCreateWindowSurface;
        }
        throw new IllegalStateException("surface was null");
    }
    
    @NonNull
    private Size getSurfaceSize(@NonNull final EGLSurface eglSurface) {
        return new Size(querySurface(this.mEglDisplay, eglSurface, 12375), querySurface(this.mEglDisplay, eglSurface, 12374));
    }
    
    private int loadFragmentShader(@NonNull final ShaderProvider shaderProvider) {
        if (shaderProvider == ShaderProvider.DEFAULT) {
            return loadShader(35632, OpenGlRenderer.DEFAULT_FRAGMENT_SHADER);
        }
        try {
            final String fragmentShader = shaderProvider.createFragmentShader("sTexture", "vTextureCoord");
            if (fragmentShader != null && fragmentShader.contains("vTextureCoord") && fragmentShader.contains("sTexture")) {
                return loadShader(35632, fragmentShader);
            }
            throw new IllegalArgumentException("Invalid fragment shader");
        }
        finally {
            final Throwable cause;
            IllegalArgumentException ex = null;
            if (!(cause instanceof IllegalArgumentException)) {
                ex = new IllegalArgumentException("Unable to compile fragment shader", cause);
            }
            throw ex;
        }
    }
    
    private void loadLocations() {
        checkLocationOrThrow(this.mPositionLoc = GLES20.glGetAttribLocation(this.mProgramHandle, "aPosition"), "aPosition");
        checkLocationOrThrow(this.mTexCoordLoc = GLES20.glGetAttribLocation(this.mProgramHandle, "aTextureCoord"), "aTextureCoord");
        checkLocationOrThrow(this.mTexMatrixLoc = GLES20.glGetUniformLocation(this.mProgramHandle, "uTexMatrix"), "uTexMatrix");
    }
    
    private static int loadShader(final int n, @NonNull final String str) {
        final int glCreateShader = GLES20.glCreateShader(n);
        final StringBuilder sb = new StringBuilder();
        sb.append("glCreateShader type=");
        sb.append(n);
        checkGlErrorOrThrow(sb.toString());
        GLES20.glShaderSource(glCreateShader, str);
        GLES20.glCompileShader(glCreateShader);
        final int[] array = { 0 };
        GLES20.glGetShaderiv(glCreateShader, 35713, array, 0);
        if (array[0] != 0) {
            return glCreateShader;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Could not compile shader: ");
        sb2.append(str);
        Logger.w("OpenGlRenderer", sb2.toString());
        GLES20.glDeleteShader(glCreateShader);
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("Could not compile shader type ");
        sb3.append(n);
        sb3.append(":");
        sb3.append(GLES20.glGetShaderInfoLog(glCreateShader));
        throw new IllegalStateException(sb3.toString());
    }
    
    private void makeCurrent(@NonNull final EGLSurface eglSurface) {
        Preconditions.checkNotNull(this.mEglDisplay);
        Preconditions.checkNotNull(this.mEglContext);
        if (EGL14.eglMakeCurrent(this.mEglDisplay, eglSurface, eglSurface, this.mEglContext)) {
            return;
        }
        throw new IllegalStateException("eglMakeCurrent failed");
    }
    
    private static int querySurface(@NonNull final EGLDisplay eglDisplay, @NonNull final EGLSurface eglSurface, final int n) {
        final int[] array = { 0 };
        EGL14.eglQuerySurface(eglDisplay, eglSurface, n, array, 0);
        return array[0];
    }
    
    private void releaseInternal() {
        final int mProgramHandle = this.mProgramHandle;
        if (mProgramHandle != -1) {
            GLES20.glDeleteProgram(mProgramHandle);
            this.mProgramHandle = -1;
        }
        final Iterator<OutputSurface> iterator = this.mOutputSurfaceMap.values().iterator();
        while (iterator.hasNext()) {
            EGL14.eglDestroySurface(this.mEglDisplay, iterator.next().getEglSurface());
        }
        this.mOutputSurfaceMap.clear();
        if (!Objects.equals(this.mTempSurface, EGL14.EGL_NO_SURFACE)) {
            EGL14.eglDestroySurface(this.mEglDisplay, this.mTempSurface);
            this.mTempSurface = EGL14.EGL_NO_SURFACE;
        }
        if (!Objects.equals(this.mEglDisplay, EGL14.EGL_NO_DISPLAY)) {
            if (!Objects.equals(this.mEglContext, EGL14.EGL_NO_CONTEXT)) {
                final EGLDisplay mEglDisplay = this.mEglDisplay;
                final EGLSurface egl_NO_SURFACE = EGL14.EGL_NO_SURFACE;
                EGL14.eglMakeCurrent(mEglDisplay, egl_NO_SURFACE, egl_NO_SURFACE, this.mEglContext);
                EGL14.eglDestroyContext(this.mEglDisplay, this.mEglContext);
                this.mEglContext = EGL14.EGL_NO_CONTEXT;
            }
            EGL14.eglTerminate(this.mEglDisplay);
            this.mEglDisplay = EGL14.EGL_NO_DISPLAY;
        }
        this.mEglConfig = null;
        this.mProgramHandle = -1;
        this.mTexMatrixLoc = -1;
        this.mPositionLoc = -1;
        this.mTexCoordLoc = -1;
        this.mTexId = -1;
        this.mCurrentOutputSurface = null;
        this.mGlThread = null;
    }
    
    public int getTextureName() {
        this.checkInitializedOrThrow(true);
        this.checkGlThreadOrThrow();
        return this.mTexId;
    }
    
    public void init(@NonNull final ShaderProvider ex) {
        this.checkInitializedOrThrow(false);
        try {
            this.createEglContext();
            this.createTempSurface();
            this.makeCurrent(this.mTempSurface);
            this.createProgram((ShaderProvider)ex);
            this.loadLocations();
            this.createTexture();
            this.mGlThread = Thread.currentThread();
            this.mInitialized.set(true);
            return;
        }
        catch (final IllegalArgumentException ex) {}
        catch (final IllegalStateException ex2) {}
        this.releaseInternal();
        throw ex;
    }
    
    public void release() {
        if (!this.mInitialized.getAndSet(false)) {
            return;
        }
        this.checkGlThreadOrThrow();
        this.releaseInternal();
    }
    
    public void render(final long n, @NonNull final float[] array) {
        this.checkInitializedOrThrow(true);
        this.checkGlThreadOrThrow();
        if (this.mCurrentOutputSurface == null) {
            return;
        }
        GLES20.glUseProgram(this.mProgramHandle);
        checkGlErrorOrThrow("glUseProgram");
        GLES20.glActiveTexture(33984);
        GLES20.glBindTexture(36197, this.mTexId);
        GLES20.glUniformMatrix4fv(this.mTexMatrixLoc, 1, false, array, 0);
        checkGlErrorOrThrow("glUniformMatrix4fv");
        GLES20.glEnableVertexAttribArray(this.mPositionLoc);
        checkGlErrorOrThrow("glEnableVertexAttribArray");
        GLES20.glVertexAttribPointer(this.mPositionLoc, 2, 5126, false, 0, (Buffer)OpenGlRenderer.VERTEX_BUF);
        checkGlErrorOrThrow("glVertexAttribPointer");
        GLES20.glEnableVertexAttribArray(this.mTexCoordLoc);
        checkGlErrorOrThrow("glEnableVertexAttribArray");
        GLES20.glVertexAttribPointer(this.mTexCoordLoc, 2, 5126, false, 0, (Buffer)OpenGlRenderer.TEX_BUF);
        checkGlErrorOrThrow("glVertexAttribPointer");
        GLES20.glDrawArrays(5, 0, 4);
        checkGlErrorOrThrow("glDrawArrays");
        GLES20.glDisableVertexAttribArray(this.mPositionLoc);
        GLES20.glDisableVertexAttribArray(this.mTexCoordLoc);
        GLES20.glUseProgram(0);
        GLES20.glBindTexture(36197, 0);
        EGLExt.eglPresentationTimeANDROID(this.mEglDisplay, this.mCurrentOutputSurface.getEglSurface(), n);
        if (!EGL14.eglSwapBuffers(this.mEglDisplay, this.mCurrentOutputSurface.getEglSurface())) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to swap buffers with EGL error: 0x");
            sb.append(Integer.toHexString(EGL14.eglGetError()));
            Logger.w("OpenGlRenderer", sb.toString());
        }
    }
    
    public void setOutputSurface(@NonNull final Surface surface) {
        this.checkInitializedOrThrow(true);
        this.checkGlThreadOrThrow();
        if (!this.mOutputSurfaceMap.containsKey(surface)) {
            final EGLDisplay mEglDisplay = this.mEglDisplay;
            final EGLConfig mEglConfig = this.mEglConfig;
            Objects.requireNonNull(mEglConfig);
            final EGLSurface windowSurface = createWindowSurface(mEglDisplay, mEglConfig, surface);
            final Size surfaceSize = this.getSurfaceSize(windowSurface);
            this.mOutputSurfaceMap.put(surface, OutputSurface.of(windowSurface, surfaceSize.getWidth(), surfaceSize.getHeight()));
        }
        final OutputSurface outputSurface = this.mOutputSurfaceMap.get(surface);
        Objects.requireNonNull(outputSurface);
        this.mCurrentOutputSurface = outputSurface;
        this.makeCurrent(outputSurface.getEglSurface());
        GLES20.glViewport(0, 0, this.mCurrentOutputSurface.getWidth(), this.mCurrentOutputSurface.getHeight());
        GLES20.glScissor(0, 0, this.mCurrentOutputSurface.getWidth(), this.mCurrentOutputSurface.getHeight());
    }
    
    abstract static class OutputSurface
    {
        @NonNull
        static OutputSurface of(@NonNull final EGLSurface eglSurface, final int n, final int n2) {
            return (OutputSurface)new AutoValue_OpenGlRenderer_OutputSurface(eglSurface, n, n2);
        }
        
        @NonNull
        abstract EGLSurface getEglSurface();
        
        abstract int getHeight();
        
        abstract int getWidth();
    }
}

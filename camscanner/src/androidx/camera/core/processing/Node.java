// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;

public interface Node<I, O>
{
    void release();
    
    @MainThread
    @NonNull
    O transform(@NonNull final I p0);
}

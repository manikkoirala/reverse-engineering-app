// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core.processing;

import androidx.camera.core.impl.utils.TransformUtils;
import androidx.core.util.Preconditions;
import androidx.annotation.Nullable;
import androidx.camera.core.ImageProxy;
import android.util.Size;
import androidx.camera.core.impl.CameraCaptureResult;
import android.graphics.Matrix;
import android.graphics.Rect;
import androidx.camera.core.impl.utils.Exif;
import androidx.annotation.NonNull;
import android.graphics.Bitmap;
import androidx.annotation.RequiresApi;

@RequiresApi(api = 21)
public abstract class Packet<T>
{
    @NonNull
    public static Packet<Bitmap> of(@NonNull final Bitmap bitmap, @NonNull final Exif exif, @NonNull final Rect rect, final int n, @NonNull final Matrix matrix, @NonNull final CameraCaptureResult cameraCaptureResult) {
        return new AutoValue_Packet<Bitmap>(bitmap, exif, 42, new Size(bitmap.getWidth(), bitmap.getHeight()), rect, n, matrix, cameraCaptureResult);
    }
    
    @NonNull
    public static Packet<ImageProxy> of(@NonNull final ImageProxy imageProxy, @Nullable final Exif exif, @NonNull final Rect rect, final int n, @NonNull final Matrix matrix, @NonNull final CameraCaptureResult cameraCaptureResult) {
        if (imageProxy.getFormat() == 256) {
            Preconditions.checkNotNull(exif, "JPEG image must have Exif.");
        }
        return new AutoValue_Packet<ImageProxy>(imageProxy, exif, imageProxy.getFormat(), new Size(imageProxy.getWidth(), imageProxy.getHeight()), rect, n, matrix, cameraCaptureResult);
    }
    
    @NonNull
    public static Packet<byte[]> of(@NonNull final byte[] array, @NonNull final Exif exif, final int n, @NonNull final Size size, @NonNull final Rect rect, final int n2, @NonNull final Matrix matrix, @NonNull final CameraCaptureResult cameraCaptureResult) {
        return new AutoValue_Packet<byte[]>(array, exif, n, size, rect, n2, matrix, cameraCaptureResult);
    }
    
    @NonNull
    public abstract CameraCaptureResult getCameraCaptureResult();
    
    @NonNull
    public abstract Rect getCropRect();
    
    @NonNull
    public abstract T getData();
    
    @Nullable
    public abstract Exif getExif();
    
    public abstract int getFormat();
    
    public abstract int getRotationDegrees();
    
    @NonNull
    public abstract Matrix getSensorToBufferTransform();
    
    @NonNull
    public abstract Size getSize();
    
    public boolean hasCropping() {
        return TransformUtils.hasCropping(this.getCropRect(), this.getSize());
    }
}

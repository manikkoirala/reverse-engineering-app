// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

final class AutoValue_ImageReaderFormatRecommender_FormatCombo extends FormatCombo
{
    private final int imageAnalysisFormat;
    private final int imageCaptureFormat;
    
    AutoValue_ImageReaderFormatRecommender_FormatCombo(final int imageCaptureFormat, final int imageAnalysisFormat) {
        this.imageCaptureFormat = imageCaptureFormat;
        this.imageAnalysisFormat = imageAnalysisFormat;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof FormatCombo) {
            final FormatCombo formatCombo = (FormatCombo)o;
            if (this.imageCaptureFormat != formatCombo.imageCaptureFormat() || this.imageAnalysisFormat != formatCombo.imageAnalysisFormat()) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return (this.imageCaptureFormat ^ 0xF4243) * 1000003 ^ this.imageAnalysisFormat;
    }
    
    @Override
    int imageAnalysisFormat() {
        return this.imageAnalysisFormat;
    }
    
    @Override
    int imageCaptureFormat() {
        return this.imageCaptureFormat;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("FormatCombo{imageCaptureFormat=");
        sb.append(this.imageCaptureFormat);
        sb.append(", imageAnalysisFormat=");
        sb.append(this.imageAnalysisFormat);
        sb.append("}");
        return sb.toString();
    }
}

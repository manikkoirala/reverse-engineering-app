// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.RestrictTo;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.concurrent.TimeUnit;
import androidx.annotation.IntRange;
import androidx.core.util.Preconditions;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import java.util.Collections;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class FocusMeteringAction
{
    static final long DEFAULT_AUTOCANCEL_DURATION = 5000L;
    static final int DEFAULT_METERING_MODE = 7;
    public static final int FLAG_AE = 2;
    public static final int FLAG_AF = 1;
    public static final int FLAG_AWB = 4;
    private final long mAutoCancelDurationInMillis;
    private final List<MeteringPoint> mMeteringPointsAe;
    private final List<MeteringPoint> mMeteringPointsAf;
    private final List<MeteringPoint> mMeteringPointsAwb;
    
    FocusMeteringAction(final Builder builder) {
        this.mMeteringPointsAf = Collections.unmodifiableList((List<? extends MeteringPoint>)builder.mMeteringPointsAf);
        this.mMeteringPointsAe = Collections.unmodifiableList((List<? extends MeteringPoint>)builder.mMeteringPointsAe);
        this.mMeteringPointsAwb = Collections.unmodifiableList((List<? extends MeteringPoint>)builder.mMeteringPointsAwb);
        this.mAutoCancelDurationInMillis = builder.mAutoCancelDurationInMillis;
    }
    
    public long getAutoCancelDurationInMillis() {
        return this.mAutoCancelDurationInMillis;
    }
    
    @NonNull
    public List<MeteringPoint> getMeteringPointsAe() {
        return this.mMeteringPointsAe;
    }
    
    @NonNull
    public List<MeteringPoint> getMeteringPointsAf() {
        return this.mMeteringPointsAf;
    }
    
    @NonNull
    public List<MeteringPoint> getMeteringPointsAwb() {
        return this.mMeteringPointsAwb;
    }
    
    public boolean isAutoCancelEnabled() {
        return this.mAutoCancelDurationInMillis > 0L;
    }
    
    public static class Builder
    {
        long mAutoCancelDurationInMillis;
        final List<MeteringPoint> mMeteringPointsAe;
        final List<MeteringPoint> mMeteringPointsAf;
        final List<MeteringPoint> mMeteringPointsAwb;
        
        public Builder(@NonNull final MeteringPoint meteringPoint) {
            this(meteringPoint, 7);
        }
        
        public Builder(@NonNull final MeteringPoint meteringPoint, final int n) {
            this.mMeteringPointsAf = new ArrayList<MeteringPoint>();
            this.mMeteringPointsAe = new ArrayList<MeteringPoint>();
            this.mMeteringPointsAwb = new ArrayList<MeteringPoint>();
            this.mAutoCancelDurationInMillis = 5000L;
            this.addPoint(meteringPoint, n);
        }
        
        @NonNull
        public Builder addPoint(@NonNull final MeteringPoint meteringPoint) {
            return this.addPoint(meteringPoint, 7);
        }
        
        @NonNull
        public Builder addPoint(@NonNull final MeteringPoint meteringPoint, final int i) {
            final boolean b = false;
            Preconditions.checkArgument(meteringPoint != null, (Object)"Point cannot be null.");
            boolean b2 = b;
            if (i >= 1) {
                b2 = b;
                if (i <= 7) {
                    b2 = true;
                }
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid metering mode ");
            sb.append(i);
            Preconditions.checkArgument(b2, (Object)sb.toString());
            if ((i & 0x1) != 0x0) {
                this.mMeteringPointsAf.add(meteringPoint);
            }
            if ((i & 0x2) != 0x0) {
                this.mMeteringPointsAe.add(meteringPoint);
            }
            if ((i & 0x4) != 0x0) {
                this.mMeteringPointsAwb.add(meteringPoint);
            }
            return this;
        }
        
        @NonNull
        public FocusMeteringAction build() {
            return new FocusMeteringAction(this);
        }
        
        @NonNull
        public Builder disableAutoCancel() {
            this.mAutoCancelDurationInMillis = 0L;
            return this;
        }
        
        @NonNull
        public Builder setAutoCancelDuration(@IntRange(from = 1L) final long duration, @NonNull final TimeUnit timeUnit) {
            Preconditions.checkArgument(duration >= 1L, (Object)"autoCancelDuration must be at least 1");
            this.mAutoCancelDurationInMillis = timeUnit.toMillis(duration);
            return this;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public @interface MeteringMode {
    }
}

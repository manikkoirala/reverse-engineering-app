// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
final class CameraClosedException extends RuntimeException
{
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    CameraClosedException(final String message) {
        super(message);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    CameraClosedException(final String message, final Throwable cause) {
        super(message, cause);
    }
}

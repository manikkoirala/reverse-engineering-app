// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import android.util.Size;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.GuardedBy;
import android.graphics.Rect;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public final class SettableImageProxy extends ForwardingImageProxy
{
    @GuardedBy("mLock")
    @Nullable
    private Rect mCropRect;
    private final int mHeight;
    private final ImageInfo mImageInfo;
    private final Object mLock;
    private final int mWidth;
    
    public SettableImageProxy(@NonNull final ImageProxy imageProxy, @Nullable final Size size, @NonNull final ImageInfo mImageInfo) {
        super(imageProxy);
        this.mLock = new Object();
        if (size == null) {
            this.mWidth = super.getWidth();
            this.mHeight = super.getHeight();
        }
        else {
            this.mWidth = size.getWidth();
            this.mHeight = size.getHeight();
        }
        this.mImageInfo = mImageInfo;
    }
    
    SettableImageProxy(final ImageProxy imageProxy, final ImageInfo imageInfo) {
        this(imageProxy, null, imageInfo);
    }
    
    @NonNull
    @Override
    public Rect getCropRect() {
        synchronized (this.mLock) {
            if (this.mCropRect == null) {
                return new Rect(0, 0, this.getWidth(), this.getHeight());
            }
            return new Rect(this.mCropRect);
        }
    }
    
    @Override
    public int getHeight() {
        return this.mHeight;
    }
    
    @NonNull
    @Override
    public ImageInfo getImageInfo() {
        return this.mImageInfo;
    }
    
    @Override
    public int getWidth() {
        return this.mWidth;
    }
    
    @Override
    public void setCropRect(@Nullable final Rect rect) {
        Rect mCropRect = rect;
        if (rect != null) {
            mCropRect = new Rect(rect);
            if (!mCropRect.intersect(0, 0, this.getWidth(), this.getHeight())) {
                mCropRect.setEmpty();
            }
        }
        synchronized (this.mLock) {
            this.mCropRect = mCropRect;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.NonNull;
import android.graphics.PointF;
import androidx.annotation.RestrictTo;
import androidx.annotation.Nullable;
import android.util.Rational;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public abstract class MeteringPointFactory
{
    @Nullable
    private Rational mSurfaceAspectRatio;
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public MeteringPointFactory() {
        this(null);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public MeteringPointFactory(@Nullable final Rational mSurfaceAspectRatio) {
        this.mSurfaceAspectRatio = mSurfaceAspectRatio;
    }
    
    public static float getDefaultPointSize() {
        return 0.15f;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected abstract PointF convertPoint(final float p0, final float p1);
    
    @NonNull
    public final MeteringPoint createPoint(final float n, final float n2) {
        return this.createPoint(n, n2, getDefaultPointSize());
    }
    
    @NonNull
    public final MeteringPoint createPoint(final float n, final float n2, final float n3) {
        final PointF convertPoint = this.convertPoint(n, n2);
        return new MeteringPoint(convertPoint.x, convertPoint.y, n3, this.mSurfaceAspectRatio);
    }
}

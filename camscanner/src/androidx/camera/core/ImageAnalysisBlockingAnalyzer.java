// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.ImageReaderProxy;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
final class ImageAnalysisBlockingAnalyzer extends ImageAnalysisAbstractAnalyzer
{
    @Nullable
    @Override
    ImageProxy acquireImage(@NonNull final ImageReaderProxy imageReaderProxy) {
        return imageReaderProxy.acquireNextImage();
    }
    
    @Override
    void clearCache() {
    }
    
    @Override
    void onValidImageAvailable(@NonNull final ImageProxy imageProxy) {
        Futures.addCallback(this.analyzeImage(imageProxy), new FutureCallback<Void>(this, imageProxy) {
            final ImageAnalysisBlockingAnalyzer this$0;
            final ImageProxy val$imageProxy;
            
            @Override
            public void onFailure(@NonNull final Throwable t) {
                this.val$imageProxy.close();
            }
            
            @Override
            public void onSuccess(final Void void1) {
            }
        }, CameraXExecutors.directExecutor());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public abstract class CameraState
{
    public static final int ERROR_CAMERA_DISABLED = 5;
    public static final int ERROR_CAMERA_FATAL_ERROR = 6;
    public static final int ERROR_CAMERA_IN_USE = 2;
    public static final int ERROR_DO_NOT_DISTURB_MODE_ENABLED = 7;
    public static final int ERROR_MAX_CAMERAS_IN_USE = 1;
    public static final int ERROR_OTHER_RECOVERABLE_ERROR = 3;
    public static final int ERROR_STREAM_CONFIG = 4;
    
    @NonNull
    public static CameraState create(@NonNull final Type type) {
        return create(type, null);
    }
    
    @NonNull
    public static CameraState create(@NonNull final Type type, @Nullable final StateError stateError) {
        return new AutoValue_CameraState(type, stateError);
    }
    
    @Nullable
    public abstract StateError getError();
    
    @NonNull
    public abstract Type getType();
    
    public enum ErrorType
    {
        private static final ErrorType[] $VALUES;
        
        CRITICAL, 
        RECOVERABLE;
    }
    
    public abstract static class StateError
    {
        @NonNull
        public static StateError create(final int n) {
            return create(n, null);
        }
        
        @NonNull
        public static StateError create(final int n, @Nullable final Throwable t) {
            return (StateError)new AutoValue_CameraState_StateError(n, t);
        }
        
        @Nullable
        public abstract Throwable getCause();
        
        public abstract int getCode();
        
        @NonNull
        public ErrorType getType() {
            final int code = this.getCode();
            if (code != 2 && code != 1 && code != 3) {
                return ErrorType.CRITICAL;
            }
            return ErrorType.RECOVERABLE;
        }
    }
    
    public enum Type
    {
        private static final Type[] $VALUES;
        
        CLOSED, 
        CLOSING, 
        OPEN, 
        OPENING, 
        PENDING_OPEN;
    }
}

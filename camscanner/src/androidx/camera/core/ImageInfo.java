// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.camera.core.impl.utils.ExifData;
import androidx.annotation.RestrictTo;
import androidx.camera.core.impl.TagBundle;
import androidx.annotation.NonNull;
import android.graphics.Matrix;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface ImageInfo
{
    int getRotationDegrees();
    
    @NonNull
    Matrix getSensorToBufferTransformMatrix();
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    TagBundle getTagBundle();
    
    long getTimestamp();
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    void populateExifData(@NonNull final ExifData.Builder p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.util.UUID;
import androidx.annotation.IntRange;
import androidx.camera.core.impl.MutableConfig;
import androidx.camera.core.impl.MutableOptionsBundle;
import androidx.camera.core.internal.O8;
import java.util.Set;
import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;
import androidx.annotation.Nullable;
import androidx.camera.core.impl.ReadableConfig;
import androidx.camera.core.impl.o\u3007\u30070\u3007;
import androidx.camera.core.impl.OptionsBundle;
import androidx.camera.core.impl.UseCaseConfigFactory;
import android.os.Handler;
import androidx.camera.core.impl.CameraDeviceSurfaceManager;
import androidx.camera.core.impl.CameraFactory;
import java.util.concurrent.Executor;
import androidx.camera.core.impl.Config;
import androidx.annotation.RequiresApi;
import androidx.camera.core.internal.TargetConfig;

@RequiresApi(21)
public final class CameraXConfig implements TargetConfig<CameraX>
{
    static final Option<CameraSelector> OPTION_AVAILABLE_CAMERAS_LIMITER;
    static final Option<Executor> OPTION_CAMERA_EXECUTOR;
    static final Option<CameraFactory.Provider> OPTION_CAMERA_FACTORY_PROVIDER;
    static final Option<CameraDeviceSurfaceManager.Provider> OPTION_DEVICE_SURFACE_MANAGER_PROVIDER;
    static final Option<Integer> OPTION_MIN_LOGGING_LEVEL;
    static final Option<Handler> OPTION_SCHEDULER_HANDLER;
    static final Option<UseCaseConfigFactory.Provider> OPTION_USECASE_CONFIG_FACTORY_PROVIDER;
    private final OptionsBundle mConfig;
    
    static {
        OPTION_CAMERA_FACTORY_PROVIDER = (Option)Config.Option.create("camerax.core.appConfig.cameraFactoryProvider", CameraFactory.Provider.class);
        OPTION_DEVICE_SURFACE_MANAGER_PROVIDER = (Option)Config.Option.create("camerax.core.appConfig.deviceSurfaceManagerProvider", CameraDeviceSurfaceManager.Provider.class);
        OPTION_USECASE_CONFIG_FACTORY_PROVIDER = (Option)Config.Option.create("camerax.core.appConfig.useCaseConfigFactoryProvider", UseCaseConfigFactory.Provider.class);
        OPTION_CAMERA_EXECUTOR = (Option)Config.Option.create("camerax.core.appConfig.cameraExecutor", Executor.class);
        OPTION_SCHEDULER_HANDLER = (Option)Config.Option.create("camerax.core.appConfig.schedulerHandler", Handler.class);
        OPTION_MIN_LOGGING_LEVEL = (Option)Config.Option.create("camerax.core.appConfig.minimumLoggingLevel", Integer.TYPE);
        OPTION_AVAILABLE_CAMERAS_LIMITER = (Option)Config.Option.create("camerax.core.appConfig.availableCamerasLimiter", CameraSelector.class);
    }
    
    CameraXConfig(final OptionsBundle mConfig) {
        this.mConfig = mConfig;
    }
    
    @Nullable
    public CameraSelector getAvailableCamerasLimiter(@Nullable final CameraSelector cameraSelector) {
        return this.mConfig.retrieveOption(CameraXConfig.OPTION_AVAILABLE_CAMERAS_LIMITER, cameraSelector);
    }
    
    @Nullable
    public Executor getCameraExecutor(@Nullable final Executor executor) {
        return this.mConfig.retrieveOption(CameraXConfig.OPTION_CAMERA_EXECUTOR, executor);
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public CameraFactory.Provider getCameraFactoryProvider(@Nullable final CameraFactory.Provider provider) {
        return this.mConfig.retrieveOption(CameraXConfig.OPTION_CAMERA_FACTORY_PROVIDER, provider);
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    public Config getConfig() {
        return this.mConfig;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public CameraDeviceSurfaceManager.Provider getDeviceSurfaceManagerProvider(@Nullable final CameraDeviceSurfaceManager.Provider provider) {
        return this.mConfig.retrieveOption(CameraXConfig.OPTION_DEVICE_SURFACE_MANAGER_PROVIDER, provider);
    }
    
    public int getMinimumLoggingLevel() {
        return this.mConfig.retrieveOption(CameraXConfig.OPTION_MIN_LOGGING_LEVEL, 3);
    }
    
    @Nullable
    public Handler getSchedulerHandler(@Nullable final Handler handler) {
        return this.mConfig.retrieveOption(CameraXConfig.OPTION_SCHEDULER_HANDLER, handler);
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public UseCaseConfigFactory.Provider getUseCaseConfigFactoryProvider(@Nullable final UseCaseConfigFactory.Provider provider) {
        return this.mConfig.retrieveOption(CameraXConfig.OPTION_USECASE_CONFIG_FACTORY_PROVIDER, provider);
    }
    
    public static final class Builder implements TargetConfig.Builder<CameraX, Builder>
    {
        private final MutableOptionsBundle mMutableConfig;
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder() {
            this(MutableOptionsBundle.create());
        }
        
        private Builder(final MutableOptionsBundle mMutableConfig) {
            this.mMutableConfig = mMutableConfig;
            final Class<?> obj = mMutableConfig.retrieveOption(TargetConfig.OPTION_TARGET_CLASS, null);
            if (obj != null && !obj.equals(CameraX.class)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid target class configuration for ");
                sb.append(this);
                sb.append(": ");
                sb.append(obj);
                throw new IllegalArgumentException(sb.toString());
            }
            this.setTargetClass(CameraX.class);
        }
        
        @NonNull
        public static Builder fromConfig(@NonNull final CameraXConfig cameraXConfig) {
            return new Builder(MutableOptionsBundle.from(cameraXConfig));
        }
        
        @NonNull
        private MutableConfig getMutableConfig() {
            return this.mMutableConfig;
        }
        
        @NonNull
        public CameraXConfig build() {
            return new CameraXConfig(OptionsBundle.from(this.mMutableConfig));
        }
        
        @NonNull
        public Builder setAvailableCamerasLimiter(@NonNull final CameraSelector cameraSelector) {
            this.getMutableConfig().insertOption(CameraXConfig.OPTION_AVAILABLE_CAMERAS_LIMITER, cameraSelector);
            return this;
        }
        
        @NonNull
        public Builder setCameraExecutor(@NonNull final Executor executor) {
            this.getMutableConfig().insertOption(CameraXConfig.OPTION_CAMERA_EXECUTOR, executor);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setCameraFactoryProvider(@NonNull final CameraFactory.Provider provider) {
            this.getMutableConfig().insertOption(CameraXConfig.OPTION_CAMERA_FACTORY_PROVIDER, provider);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setDeviceSurfaceManagerProvider(@NonNull final CameraDeviceSurfaceManager.Provider provider) {
            this.getMutableConfig().insertOption(CameraXConfig.OPTION_DEVICE_SURFACE_MANAGER_PROVIDER, provider);
            return this;
        }
        
        @NonNull
        public Builder setMinimumLoggingLevel(@IntRange(from = 3L, to = 6L) final int i) {
            this.getMutableConfig().insertOption(CameraXConfig.OPTION_MIN_LOGGING_LEVEL, i);
            return this;
        }
        
        @NonNull
        public Builder setSchedulerHandler(@NonNull final Handler handler) {
            this.getMutableConfig().insertOption(CameraXConfig.OPTION_SCHEDULER_HANDLER, handler);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setTargetClass(@NonNull final Class<CameraX> clazz) {
            this.getMutableConfig().insertOption(TargetConfig.OPTION_TARGET_CLASS, clazz);
            if (this.getMutableConfig().retrieveOption(TargetConfig.OPTION_TARGET_NAME, null) == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append(clazz.getCanonicalName());
                sb.append("-");
                sb.append(UUID.randomUUID());
                this.setTargetName(sb.toString());
            }
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setTargetName(@NonNull final String s) {
            this.getMutableConfig().insertOption(TargetConfig.OPTION_TARGET_NAME, s);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setUseCaseConfigFactoryProvider(@NonNull final UseCaseConfigFactory.Provider provider) {
            this.getMutableConfig().insertOption(CameraXConfig.OPTION_USECASE_CONFIG_FACTORY_PROVIDER, provider);
            return this;
        }
    }
    
    public interface Provider
    {
        @NonNull
        CameraXConfig getCameraXConfig();
    }
}

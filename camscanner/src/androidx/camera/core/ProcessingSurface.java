// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.arch.core.util.Function;
import androidx.camera.core.impl.utils.futures.FutureChain;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.camera.core.impl.ImageProxyBundle;
import androidx.camera.core.impl.SingleImageProxyBundle;
import java.util.concurrent.ScheduledExecutorService;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import java.util.concurrent.Executor;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import android.os.Looper;
import androidx.annotation.Nullable;
import androidx.camera.core.impl.ImageReaderProxy;
import android.util.Size;
import android.view.Surface;
import android.os.Handler;
import androidx.camera.core.impl.CaptureStage;
import androidx.annotation.NonNull;
import androidx.annotation.GuardedBy;
import androidx.camera.core.impl.CaptureProcessor;
import androidx.camera.core.impl.CameraCaptureCallback;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.DeferrableSurface;

@RequiresApi(21)
final class ProcessingSurface extends DeferrableSurface
{
    private static final int MAX_IMAGES = 2;
    private static final String TAG = "ProcessingSurfaceTextur";
    private final CameraCaptureCallback mCameraCaptureCallback;
    @GuardedBy("mLock")
    @NonNull
    final CaptureProcessor mCaptureProcessor;
    final CaptureStage mCaptureStage;
    private final Handler mImageReaderHandler;
    private final MetadataImageReader mInputImageReader;
    private final Surface mInputSurface;
    final Object mLock;
    private final DeferrableSurface mOutputDeferrableSurface;
    @GuardedBy("mLock")
    boolean mReleased;
    @NonNull
    private final Size mResolution;
    private String mTagBundleKey;
    private final ImageReaderProxy.OnImageAvailableListener mTransformedListener;
    
    ProcessingSurface(final int n, final int n2, final int n3, @Nullable final Handler mImageReaderHandler, @NonNull final CaptureStage mCaptureStage, @NonNull final CaptureProcessor mCaptureProcessor, @NonNull final DeferrableSurface mOutputDeferrableSurface, @NonNull final String mTagBundleKey) {
        super(new Size(n, n2), n3);
        this.mLock = new Object();
        final \u30070 mTransformedListener = new \u30070(this);
        this.mTransformedListener = mTransformedListener;
        this.mReleased = false;
        final Size mResolution = new Size(n, n2);
        this.mResolution = mResolution;
        if (mImageReaderHandler != null) {
            this.mImageReaderHandler = mImageReaderHandler;
        }
        else {
            final Looper myLooper = Looper.myLooper();
            if (myLooper == null) {
                throw new IllegalStateException("Creating a ProcessingSurface requires a non-null Handler, or be created  on a thread with a Looper.");
            }
            this.mImageReaderHandler = new Handler(myLooper);
        }
        final ScheduledExecutorService handlerExecutor = CameraXExecutors.newHandlerExecutor(this.mImageReaderHandler);
        final MetadataImageReader mInputImageReader = new MetadataImageReader(n, n2, n3, 2);
        (this.mInputImageReader = mInputImageReader).setOnImageAvailableListener(mTransformedListener, handlerExecutor);
        this.mInputSurface = mInputImageReader.getSurface();
        this.mCameraCaptureCallback = mInputImageReader.getCameraCaptureCallback();
        (this.mCaptureProcessor = mCaptureProcessor).onResolutionUpdate(mResolution);
        this.mCaptureStage = mCaptureStage;
        this.mOutputDeferrableSurface = mOutputDeferrableSurface;
        this.mTagBundleKey = mTagBundleKey;
        Futures.addCallback(mOutputDeferrableSurface.getSurface(), new FutureCallback<Surface>(this) {
            final ProcessingSurface this$0;
            
            @Override
            public void onFailure(@NonNull final Throwable t) {
                Logger.e("ProcessingSurfaceTextur", "Failed to extract Listenable<Surface>.", t);
            }
            
            @Override
            public void onSuccess(@Nullable final Surface surface) {
                synchronized (this.this$0.mLock) {
                    this.this$0.mCaptureProcessor.onOutputSurface(surface, 1);
                }
            }
        }, CameraXExecutors.directExecutor());
        this.getTerminationFuture().addListener((Runnable)new o88\u3007OO08\u3007(this), CameraXExecutors.directExecutor());
    }
    
    private void release() {
        synchronized (this.mLock) {
            if (this.mReleased) {
                return;
            }
            this.mInputImageReader.clearOnImageAvailableListener();
            this.mInputImageReader.close();
            this.mInputSurface.release();
            this.mOutputDeferrableSurface.close();
            this.mReleased = true;
        }
    }
    
    @Nullable
    CameraCaptureCallback getCameraCaptureCallback() {
        synchronized (this.mLock) {
            if (!this.mReleased) {
                return this.mCameraCaptureCallback;
            }
            throw new IllegalStateException("ProcessingSurface already released!");
        }
    }
    
    @GuardedBy("mLock")
    void imageIncoming(final ImageReaderProxy imageReaderProxy) {
        if (this.mReleased) {
            return;
        }
        ImageProxy acquireNextImage;
        try {
            acquireNextImage = imageReaderProxy.acquireNextImage();
        }
        catch (final IllegalStateException ex) {
            Logger.e("ProcessingSurfaceTextur", "Failed to acquire next image.", ex);
            acquireNextImage = null;
        }
        if (acquireNextImage == null) {
            return;
        }
        final ImageInfo imageInfo = acquireNextImage.getImageInfo();
        if (imageInfo == null) {
            acquireNextImage.close();
            return;
        }
        final Integer obj = (Integer)imageInfo.getTagBundle().getTag(this.mTagBundleKey);
        if (obj == null) {
            acquireNextImage.close();
            return;
        }
        if (this.mCaptureStage.getId() != obj) {
            final Object o = new StringBuilder();
            ((StringBuilder)o).append("ImageProxyBundle does not contain this id: ");
            ((StringBuilder)o).append(obj);
            Logger.w("ProcessingSurfaceTextur", ((StringBuilder)o).toString());
            acquireNextImage.close();
            return;
        }
        final Object o = new SingleImageProxyBundle(acquireNextImage, this.mTagBundleKey);
        try {
            this.incrementUseCount();
            this.mCaptureProcessor.process((ImageProxyBundle)o);
            ((SingleImageProxyBundle)o).close();
            this.decrementUseCount();
        }
        catch (final SurfaceClosedException ex2) {
            Logger.d("ProcessingSurfaceTextur", "The ProcessingSurface has been closed. Don't process the incoming image.");
            ((SingleImageProxyBundle)o).close();
        }
    }
    
    @NonNull
    public ListenableFuture<Surface> provideSurface() {
        return (ListenableFuture<Surface>)FutureChain.from(this.mOutputDeferrableSurface.getSurface()).transform((Function<? super Surface, Object>)new o0O0(this), CameraXExecutors.directExecutor());
    }
}

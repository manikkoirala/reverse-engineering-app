// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.io.InputStream;
import java.io.Serializable;
import androidx.core.util.Preconditions;
import android.os.Build$VERSION;
import android.content.ContentValues;
import androidx.camera.core.internal.compat.workaround.ExifRotationAvailability;
import androidx.camera.core.impl.utils.Exif;
import java.io.FileOutputStream;
import java.util.UUID;
import java.util.concurrent.RejectedExecutionException;
import androidx.annotation.Nullable;
import android.graphics.Rect;
import androidx.camera.core.internal.utils.ImageUtil;
import android.net.Uri;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.io.File;
import androidx.annotation.IntRange;
import java.util.concurrent.Executor;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
final class ImageSaver implements Runnable
{
    private static final int COPY_BUFFER_SIZE = 1024;
    private static final int NOT_PENDING = 0;
    private static final int PENDING = 1;
    private static final String TAG = "ImageSaver";
    private static final String TEMP_FILE_PREFIX = "CameraX";
    private static final String TEMP_FILE_SUFFIX = ".tmp";
    @NonNull
    private final OnImageSavedCallback mCallback;
    private final ImageProxy mImage;
    private final int mJpegQuality;
    private final int mOrientation;
    @NonNull
    private final ImageCapture.OutputFileOptions mOutputFileOptions;
    @NonNull
    private final Executor mSequentialIoExecutor;
    @NonNull
    private final Executor mUserCallbackExecutor;
    
    ImageSaver(@NonNull final ImageProxy mImage, @NonNull final ImageCapture.OutputFileOptions mOutputFileOptions, final int mOrientation, @IntRange(from = 1L, to = 100L) final int mJpegQuality, @NonNull final Executor mUserCallbackExecutor, @NonNull final Executor mSequentialIoExecutor, @NonNull final OnImageSavedCallback mCallback) {
        this.mImage = mImage;
        this.mOutputFileOptions = mOutputFileOptions;
        this.mOrientation = mOrientation;
        this.mJpegQuality = mJpegQuality;
        this.mCallback = mCallback;
        this.mUserCallbackExecutor = mUserCallbackExecutor;
        this.mSequentialIoExecutor = mSequentialIoExecutor;
    }
    
    private void copyTempFileToOutputStream(@NonNull File file, @NonNull final OutputStream outputStream) throws IOException {
        file = (File)new FileInputStream(file);
        try {
            final byte[] array = new byte[1024];
            while (true) {
                final int read = ((InputStream)file).read(array);
                if (read <= 0) {
                    break;
                }
                outputStream.write(array, 0, read);
            }
            ((InputStream)file).close();
        }
        finally {
            try {
                ((InputStream)file).close();
            }
            finally {
                final Throwable exception;
                ((Throwable)outputStream).addSuppressed(exception);
            }
        }
    }
    
    private boolean copyTempFileToUri(@NonNull final File file, @NonNull Uri openOutputStream) throws IOException {
        openOutputStream = (Uri)this.mOutputFileOptions.getContentResolver().openOutputStream(openOutputStream);
        if (openOutputStream == null) {
            if (openOutputStream != null) {
                ((OutputStream)openOutputStream).close();
            }
            return false;
        }
        try {
            this.copyTempFileToOutputStream(file, (OutputStream)openOutputStream);
            ((OutputStream)openOutputStream).close();
            return true;
        }
        finally {
            try {
                ((OutputStream)openOutputStream).close();
            }
            finally {
                final Throwable exception;
                ((Throwable)file).addSuppressed(exception);
            }
        }
    }
    
    @NonNull
    private byte[] imageToJpegByteArray(@NonNull final ImageProxy imageProxy, @IntRange(from = 1L, to = 100L) final int n) throws ImageUtil.CodecFailedException {
        final boolean shouldCropImage = ImageUtil.shouldCropImage(imageProxy);
        final int format = imageProxy.getFormat();
        if (format == 256) {
            if (!shouldCropImage) {
                return ImageUtil.jpegImageToJpegByteArray(imageProxy);
            }
            return ImageUtil.jpegImageToJpegByteArray(imageProxy, imageProxy.getCropRect(), n);
        }
        else {
            Rect cropRect = null;
            if (format == 35) {
                if (shouldCropImage) {
                    cropRect = imageProxy.getCropRect();
                }
                return ImageUtil.yuvImageToJpegByteArray(imageProxy, cropRect, n);
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Unrecognized image format: ");
            sb.append(format);
            Logger.w("ImageSaver", sb.toString());
            return null;
        }
    }
    
    private boolean isSaveToFile() {
        return this.mOutputFileOptions.getFile() != null;
    }
    
    private boolean isSaveToMediaStore() {
        return this.mOutputFileOptions.getSaveCollection() != null && this.mOutputFileOptions.getContentResolver() != null && this.mOutputFileOptions.getContentValues() != null;
    }
    
    private boolean isSaveToOutputStream() {
        return this.mOutputFileOptions.getOutputStream() != null;
    }
    
    private void postError(final SaveError saveError, final String s, @Nullable final Throwable t) {
        try {
            this.mUserCallbackExecutor.execute(new O\u3007O\u3007oO(this, saveError, s, t));
        }
        catch (final RejectedExecutionException ex) {
            Logger.e("ImageSaver", "Application executor rejected executing OnImageSavedCallback.onError callback. Skipping.");
        }
    }
    
    private void postSuccess(@Nullable final Uri uri) {
        try {
            this.mUserCallbackExecutor.execute(new \u30078\u30070\u3007o\u3007O(this, uri));
        }
        catch (final RejectedExecutionException ex) {
            Logger.e("ImageSaver", "Application executor rejected executing OnImageSavedCallback.onImageSaved callback. Skipping.");
        }
    }
    
    @Nullable
    private File saveImageToTempFile() {
        try {
            File tempFile;
            if (this.isSaveToFile()) {
                final String parent = this.mOutputFileOptions.getFile().getParent();
                final StringBuilder sb = new StringBuilder();
                sb.append("CameraX");
                sb.append(UUID.randomUUID().toString());
                sb.append(".tmp");
                tempFile = new File(parent, sb.toString());
            }
            else {
                tempFile = File.createTempFile("CameraX", ".tmp");
            }
            Object exception = null;
            final ImageUtil.CodecFailedException ex;
            String s = null;
            Label_0378: {
                try {
                    exception = this.mImage;
                    try {
                        FileOutputStream fileOutputStream = new FileOutputStream(tempFile);
                        try {
                            fileOutputStream.write(this.imageToJpegByteArray(this.mImage, this.mJpegQuality));
                            final Exif fromFile = Exif.createFromFile(tempFile);
                            Exif.createFromImageProxy(this.mImage).copyToCroppedImage(fromFile);
                            if (!new ExifRotationAvailability().shouldUseExifOrientation(this.mImage)) {
                                fromFile.rotate(this.mOrientation);
                            }
                            final ImageCapture.Metadata metadata = this.mOutputFileOptions.getMetadata();
                            if (metadata.isReversedHorizontal()) {
                                fromFile.flipHorizontally();
                            }
                            if (metadata.isReversedVertical()) {
                                fromFile.flipVertically();
                            }
                            if (metadata.getLocation() != null) {
                                fromFile.attachLocation(this.mOutputFileOptions.getMetadata().getLocation());
                            }
                            fromFile.save();
                            fileOutputStream.close();
                            if (exception != null) {
                                ((ImageProxy)exception).close();
                            }
                            exception = (fileOutputStream = null);
                        }
                        finally {
                            try {
                                fileOutputStream.close();
                            }
                            finally {
                                final Throwable exception2;
                                ex.addSuppressed(exception2);
                            }
                        }
                    }
                    finally {
                        if (exception != null) {
                            try {
                                ((ImageProxy)exception).close();
                            }
                            finally {
                                ((Throwable)s).addSuppressed((Throwable)exception);
                            }
                        }
                    }
                }
                catch (final ImageUtil.CodecFailedException ex) {
                    final int n = ImageSaver$1.$SwitchMap$androidx$camera$core$internal$utils$ImageUtil$CodecFailedException$FailureType[ex.getFailureType().ordinal()];
                    if (n == 1) {
                        exception = SaveError.ENCODE_FAILED;
                        s = "Failed to encode mImage";
                        break Label_0378;
                    }
                    if (n != 2) {
                        exception = SaveError.UNKNOWN;
                        s = "Failed to transcode mImage";
                        break Label_0378;
                    }
                    exception = SaveError.CROP_FAILED;
                    s = "Failed to crop mImage";
                    break Label_0378;
                }
                catch (final IllegalArgumentException ex) {}
                catch (final IOException ex2) {}
                exception = SaveError.FILE_IO_FAILED;
                s = "Failed to write temp file";
            }
            if (exception != null) {
                this.postError((SaveError)exception, s, ex);
                tempFile.delete();
                return null;
            }
            return tempFile;
        }
        catch (final IOException ex3) {
            this.postError(SaveError.FILE_IO_FAILED, "Failed to create temp file", ex3);
            return null;
        }
    }
    
    private void setContentValuePending(@NonNull final ContentValues contentValues, final int i) {
        if (Build$VERSION.SDK_INT >= 29) {
            contentValues.put("is_pending", Integer.valueOf(i));
        }
    }
    
    private void setUriNotPending(@NonNull final Uri uri) {
        if (Build$VERSION.SDK_INT >= 29) {
            final ContentValues contentValues = new ContentValues();
            this.setContentValuePending(contentValues, 0);
            this.mOutputFileOptions.getContentResolver().update(uri, contentValues, (String)null, (String[])null);
        }
    }
    
    void copyTempFileToDestination(@NonNull final File file) {
        Preconditions.checkNotNull(file);
        Serializable s = null;
        Object o = null;
        String s2 = null;
        final SaveError file_IO_FAILED4;
        Label_0256: {
            Label_0248: {
                try {
                    try {
                        if (this.isSaveToMediaStore()) {
                            ContentValues contentValues;
                            if (this.mOutputFileOptions.getContentValues() != null) {
                                contentValues = new ContentValues(this.mOutputFileOptions.getContentValues());
                            }
                            else {
                                contentValues = new ContentValues();
                            }
                            this.setContentValuePending(contentValues, 1);
                            o = this.mOutputFileOptions.getContentResolver().insert(this.mOutputFileOptions.getSaveCollection(), contentValues);
                            Label_0093: {
                                if (o != null) {
                                    break Label_0093;
                                }
                                try {
                                    final SaveError file_IO_FAILED = SaveError.FILE_IO_FAILED;
                                    s2 = "Failed to insert URI.";
                                    break Label_0256;
                                Label_0118:
                                    while (true) {
                                        final SaveError file_IO_FAILED2 = SaveError.FILE_IO_FAILED;
                                        s2 = "Failed to save to URI.";
                                        break Label_0118;
                                        Label_0114: {
                                            s2 = null;
                                        }
                                        break Label_0118;
                                        iftrue(Label_0114:)(this.copyTempFileToUri(file, (Uri)o));
                                        continue;
                                    }
                                    this.setUriNotPending((Uri)o);
                                    break Label_0256;
                                }
                                catch (final IllegalArgumentException s) {
                                    break Label_0248;
                                }
                                catch (final IOException s) {
                                    break Label_0248;
                                }
                            }
                        }
                        if (this.isSaveToOutputStream()) {
                            this.copyTempFileToOutputStream(file, this.mOutputFileOptions.getOutputStream());
                        }
                        else if (this.isSaveToFile()) {
                            o = this.mOutputFileOptions.getFile();
                            if (((File)o).exists()) {
                                ((File)o).delete();
                            }
                            if (!file.renameTo((File)o)) {
                                final SaveError file_IO_FAILED3 = SaveError.FILE_IO_FAILED;
                                s2 = "Failed to rename file.";
                            }
                            else {
                                s2 = null;
                            }
                            o = Uri.fromFile((File)o);
                            break Label_0256;
                        }
                        o = null;
                        s2 = null;
                    }
                    finally {}
                }
                catch (final IllegalArgumentException file_IO_FAILED4) {}
                catch (final IOException ex) {}
                o = null;
                s = file_IO_FAILED4;
            }
            file_IO_FAILED4 = SaveError.FILE_IO_FAILED;
            s2 = "Failed to write destination file.";
        }
        file.delete();
        if (file_IO_FAILED4 != null) {
            this.postError(file_IO_FAILED4, s2, (Throwable)s);
        }
        else {
            this.postSuccess((Uri)o);
        }
        return;
        file.delete();
    }
    
    @Override
    public void run() {
        final File saveImageToTempFile = this.saveImageToTempFile();
        if (saveImageToTempFile != null) {
            this.mSequentialIoExecutor.execute(new o8oO\u3007(this, saveImageToTempFile));
        }
    }
    
    public interface OnImageSavedCallback
    {
        void onError(@NonNull final SaveError p0, @NonNull final String p1, @Nullable final Throwable p2);
        
        void onImageSaved(@NonNull final ImageCapture.OutputFileResults p0);
    }
    
    public enum SaveError
    {
        private static final SaveError[] $VALUES;
        
        CROP_FAILED, 
        ENCODE_FAILED, 
        FILE_IO_FAILED, 
        UNKNOWN;
    }
}

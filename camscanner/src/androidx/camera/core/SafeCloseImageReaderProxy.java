// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import java.util.concurrent.Executor;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.Surface;
import androidx.annotation.GuardedBy;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.ImageReaderProxy;

@RequiresApi(21)
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class SafeCloseImageReaderProxy implements ImageReaderProxy
{
    private ForwardingImageProxy.OnImageCloseListener mForwardingImageCloseListener;
    private final ForwardingImageProxy.OnImageCloseListener mImageCloseListener;
    @GuardedBy("mLock")
    private final ImageReaderProxy mImageReaderProxy;
    @GuardedBy("mLock")
    private boolean mIsClosed;
    private final Object mLock;
    @GuardedBy("mLock")
    private int mOutstandingImages;
    @Nullable
    private final Surface mSurface;
    
    public SafeCloseImageReaderProxy(@NonNull final ImageReaderProxy mImageReaderProxy) {
        this.mLock = new Object();
        this.mOutstandingImages = 0;
        this.mIsClosed = false;
        this.mImageCloseListener = new O8O\u3007(this);
        this.mImageReaderProxy = mImageReaderProxy;
        this.mSurface = mImageReaderProxy.getSurface();
    }
    
    @GuardedBy("mLock")
    @Nullable
    private ImageProxy wrapImageProxy(@Nullable final ImageProxy imageProxy) {
        if (imageProxy != null) {
            ++this.mOutstandingImages;
            final SingleCloseImageProxy singleCloseImageProxy = new SingleCloseImageProxy(imageProxy);
            singleCloseImageProxy.addOnImageCloseListener(this.mImageCloseListener);
            return singleCloseImageProxy;
        }
        return null;
    }
    
    @Nullable
    @Override
    public ImageProxy acquireLatestImage() {
        synchronized (this.mLock) {
            return this.wrapImageProxy(this.mImageReaderProxy.acquireLatestImage());
        }
    }
    
    @Nullable
    @Override
    public ImageProxy acquireNextImage() {
        synchronized (this.mLock) {
            return this.wrapImageProxy(this.mImageReaderProxy.acquireNextImage());
        }
    }
    
    @Override
    public void clearOnImageAvailableListener() {
        synchronized (this.mLock) {
            this.mImageReaderProxy.clearOnImageAvailableListener();
        }
    }
    
    @Override
    public void close() {
        synchronized (this.mLock) {
            final Surface mSurface = this.mSurface;
            if (mSurface != null) {
                mSurface.release();
            }
            this.mImageReaderProxy.close();
        }
    }
    
    public int getCapacity() {
        synchronized (this.mLock) {
            final int maxImages = this.mImageReaderProxy.getMaxImages();
            final int mOutstandingImages = this.mOutstandingImages;
            monitorexit(this.mLock);
            return maxImages - mOutstandingImages;
        }
    }
    
    @Override
    public int getHeight() {
        synchronized (this.mLock) {
            return this.mImageReaderProxy.getHeight();
        }
    }
    
    @Override
    public int getImageFormat() {
        synchronized (this.mLock) {
            return this.mImageReaderProxy.getImageFormat();
        }
    }
    
    @Override
    public int getMaxImages() {
        synchronized (this.mLock) {
            return this.mImageReaderProxy.getMaxImages();
        }
    }
    
    @Nullable
    @Override
    public Surface getSurface() {
        synchronized (this.mLock) {
            return this.mImageReaderProxy.getSurface();
        }
    }
    
    @Override
    public int getWidth() {
        synchronized (this.mLock) {
            return this.mImageReaderProxy.getWidth();
        }
    }
    
    public void safeClose() {
        synchronized (this.mLock) {
            this.mIsClosed = true;
            this.mImageReaderProxy.clearOnImageAvailableListener();
            if (this.mOutstandingImages == 0) {
                this.close();
            }
        }
    }
    
    @Override
    public void setOnImageAvailableListener(@NonNull final OnImageAvailableListener onImageAvailableListener, @NonNull final Executor executor) {
        synchronized (this.mLock) {
            this.mImageReaderProxy.setOnImageAvailableListener((OnImageAvailableListener)new O0O8OO088(this, onImageAvailableListener), executor);
        }
    }
    
    public void setOnImageCloseListener(@NonNull final ForwardingImageProxy.OnImageCloseListener mForwardingImageCloseListener) {
        synchronized (this.mLock) {
            this.mForwardingImageCloseListener = mForwardingImageCloseListener;
        }
    }
}

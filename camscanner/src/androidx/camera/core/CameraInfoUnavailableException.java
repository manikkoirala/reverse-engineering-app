// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.core;

import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class CameraInfoUnavailableException extends Exception
{
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public CameraInfoUnavailableException(final String message) {
        super(message);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public CameraInfoUnavailableException(final String message, final Throwable cause) {
        super(message, cause);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.impl;

import java.util.Iterator;
import androidx.camera.core.impl.MutableConfig;
import androidx.camera.core.impl.OptionsBundle;
import androidx.camera.core.impl.MutableOptionsBundle;
import androidx.camera.core.ExtendableBuilder;
import androidx.annotation.Nullable;
import android.hardware.camera2.CaptureRequest$Key;
import androidx.annotation.NonNull;
import android.hardware.camera2.CameraCaptureSession$StateCallback;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import android.hardware.camera2.CameraDevice$StateCallback;
import androidx.annotation.RestrictTo;
import androidx.camera.core.impl.Config;
import androidx.annotation.RequiresApi;
import androidx.camera.camera2.interop.ExperimentalCamera2Interop;
import androidx.annotation.OptIn;
import androidx.camera.camera2.interop.CaptureRequestOptions;

@OptIn(markerClass = { ExperimentalCamera2Interop.class })
@RequiresApi(21)
public final class Camera2ImplConfig extends CaptureRequestOptions
{
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final Option<CameraEventCallbacks> CAMERA_EVENT_CALLBACK_OPTION;
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final String CAPTURE_REQUEST_ID_STEM = "camera2.captureRequest.option.";
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final Option<Object> CAPTURE_REQUEST_TAG_OPTION;
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final Option<CameraDevice$StateCallback> DEVICE_STATE_CALLBACK_OPTION;
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final Option<CameraCaptureSession$CaptureCallback> SESSION_CAPTURE_CALLBACK_OPTION;
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final Option<String> SESSION_PHYSICAL_CAMERA_ID_OPTION;
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final Option<CameraCaptureSession$StateCallback> SESSION_STATE_CALLBACK_OPTION;
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final Option<Long> STREAM_USE_CASE_OPTION;
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final Option<Integer> TEMPLATE_TYPE_OPTION;
    
    static {
        TEMPLATE_TYPE_OPTION = (Option)Config.Option.create("camera2.captureRequest.templateType", Integer.TYPE);
        STREAM_USE_CASE_OPTION = (Option)Config.Option.create("camera2.cameraCaptureSession.streamUseCase", Long.TYPE);
        DEVICE_STATE_CALLBACK_OPTION = (Option)Config.Option.create("camera2.cameraDevice.stateCallback", CameraDevice$StateCallback.class);
        SESSION_STATE_CALLBACK_OPTION = (Option)Config.Option.create("camera2.cameraCaptureSession.stateCallback", CameraCaptureSession$StateCallback.class);
        SESSION_CAPTURE_CALLBACK_OPTION = (Option)Config.Option.create("camera2.cameraCaptureSession.captureCallback", CameraCaptureSession$CaptureCallback.class);
        CAMERA_EVENT_CALLBACK_OPTION = (Option)Config.Option.create("camera2.cameraEvent.callback", CameraEventCallbacks.class);
        CAPTURE_REQUEST_TAG_OPTION = (Option)Config.Option.create("camera2.captureRequest.tag", Object.class);
        SESSION_PHYSICAL_CAMERA_ID_OPTION = (Option)Config.Option.create("camera2.cameraCaptureSession.physicalCameraId", String.class);
    }
    
    public Camera2ImplConfig(@NonNull final Config config) {
        super(config);
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static Option<Object> createCaptureRequestOption(@NonNull final CaptureRequest$Key<?> captureRequest$Key) {
        final StringBuilder sb = new StringBuilder();
        sb.append("camera2.captureRequest.option.");
        sb.append(captureRequest$Key.getName());
        return Config.Option.create(sb.toString(), Object.class, captureRequest$Key);
    }
    
    @Nullable
    public CameraEventCallbacks getCameraEventCallback(@Nullable final CameraEventCallbacks cameraEventCallbacks) {
        return this.getConfig().retrieveOption(Camera2ImplConfig.CAMERA_EVENT_CALLBACK_OPTION, cameraEventCallbacks);
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public CaptureRequestOptions getCaptureRequestOptions() {
        return CaptureRequestOptions.Builder.from(this.getConfig()).build();
    }
    
    @Nullable
    public Object getCaptureRequestTag(@Nullable final Object o) {
        return this.getConfig().retrieveOption(Camera2ImplConfig.CAPTURE_REQUEST_TAG_OPTION, o);
    }
    
    public int getCaptureRequestTemplate(final int i) {
        return this.getConfig().retrieveOption(Camera2ImplConfig.TEMPLATE_TYPE_OPTION, i);
    }
    
    @Nullable
    public CameraDevice$StateCallback getDeviceStateCallback(@Nullable final CameraDevice$StateCallback cameraDevice$StateCallback) {
        return this.getConfig().retrieveOption(Camera2ImplConfig.DEVICE_STATE_CALLBACK_OPTION, cameraDevice$StateCallback);
    }
    
    @Nullable
    public String getPhysicalCameraId(@Nullable final String s) {
        return this.getConfig().retrieveOption(Camera2ImplConfig.SESSION_PHYSICAL_CAMERA_ID_OPTION, s);
    }
    
    @Nullable
    public CameraCaptureSession$CaptureCallback getSessionCaptureCallback(@Nullable final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) {
        return this.getConfig().retrieveOption(Camera2ImplConfig.SESSION_CAPTURE_CALLBACK_OPTION, cameraCaptureSession$CaptureCallback);
    }
    
    @Nullable
    public CameraCaptureSession$StateCallback getSessionStateCallback(@Nullable final CameraCaptureSession$StateCallback cameraCaptureSession$StateCallback) {
        return this.getConfig().retrieveOption(Camera2ImplConfig.SESSION_STATE_CALLBACK_OPTION, cameraCaptureSession$StateCallback);
    }
    
    public long getStreamUseCase(final long l) {
        return this.getConfig().retrieveOption(Camera2ImplConfig.STREAM_USE_CASE_OPTION, l);
    }
    
    public static final class Builder implements ExtendableBuilder<Camera2ImplConfig>
    {
        private final MutableOptionsBundle mMutableOptionsBundle;
        
        public Builder() {
            this.mMutableOptionsBundle = MutableOptionsBundle.create();
        }
        
        @NonNull
        @Override
        public Camera2ImplConfig build() {
            return new Camera2ImplConfig(OptionsBundle.from(this.mMutableOptionsBundle));
        }
        
        @NonNull
        @Override
        public MutableConfig getMutableConfig() {
            return this.mMutableOptionsBundle;
        }
        
        @NonNull
        public Builder insertAllOptions(@NonNull final Config config) {
            for (final Config.Option<ValueT> option : config.listOptions()) {
                this.mMutableOptionsBundle.insertOption((Option<Object>)option, config.retrieveOption((Config.Option<ValueT>)option));
            }
            return this;
        }
        
        @NonNull
        public <ValueT> Builder setCaptureRequestOption(@NonNull final CaptureRequest$Key<ValueT> captureRequest$Key, @NonNull final ValueT valueT) {
            this.mMutableOptionsBundle.insertOption((Option<ValueT>)Camera2ImplConfig.createCaptureRequestOption(captureRequest$Key), valueT);
            return this;
        }
        
        @NonNull
        public <ValueT> Builder setCaptureRequestOptionWithPriority(@NonNull final CaptureRequest$Key<ValueT> captureRequest$Key, @NonNull final ValueT valueT, @NonNull final OptionPriority optionPriority) {
            this.mMutableOptionsBundle.insertOption((Option<ValueT>)Camera2ImplConfig.createCaptureRequestOption(captureRequest$Key), optionPriority, valueT);
            return this;
        }
    }
    
    public static final class Extender<T>
    {
        ExtendableBuilder<T> mBaseBuilder;
        
        public Extender(@NonNull final ExtendableBuilder<T> mBaseBuilder) {
            this.mBaseBuilder = mBaseBuilder;
        }
        
        @NonNull
        public Extender<T> setCameraEventCallback(@NonNull final CameraEventCallbacks cameraEventCallbacks) {
            this.mBaseBuilder.getMutableConfig().insertOption(Camera2ImplConfig.CAMERA_EVENT_CALLBACK_OPTION, cameraEventCallbacks);
            return this;
        }
    }
}

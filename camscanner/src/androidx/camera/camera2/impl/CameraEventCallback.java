// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.impl;

import androidx.annotation.Nullable;
import androidx.camera.core.impl.CaptureConfig;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public abstract class CameraEventCallback
{
    public void onDeInitSession() {
    }
    
    @Nullable
    public CaptureConfig onDisableSession() {
        return null;
    }
    
    @Nullable
    public CaptureConfig onEnableSession() {
        return null;
    }
    
    @Nullable
    public CaptureConfig onInitSession() {
        return null;
    }
    
    @Nullable
    public CaptureConfig onRepeating() {
        return null;
    }
}

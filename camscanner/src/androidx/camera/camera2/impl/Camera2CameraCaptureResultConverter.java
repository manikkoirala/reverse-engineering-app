// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.impl;

import androidx.camera.camera2.internal.Camera2CameraCaptureResult;
import android.hardware.camera2.CaptureResult;
import androidx.camera.core.impl.CameraCaptureResult;
import androidx.annotation.Nullable;
import androidx.camera.camera2.internal.Camera2CameraCaptureFailure;
import android.hardware.camera2.CaptureFailure;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.CameraCaptureFailure;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class Camera2CameraCaptureResultConverter
{
    private Camera2CameraCaptureResultConverter() {
    }
    
    @Nullable
    public static CaptureFailure getCaptureFailure(@NonNull final CameraCaptureFailure cameraCaptureFailure) {
        if (cameraCaptureFailure instanceof Camera2CameraCaptureFailure) {
            return ((Camera2CameraCaptureFailure)cameraCaptureFailure).getCaptureFailure();
        }
        return null;
    }
    
    @Nullable
    public static CaptureResult getCaptureResult(@Nullable final CameraCaptureResult cameraCaptureResult) {
        if (cameraCaptureResult instanceof Camera2CameraCaptureResult) {
            return ((Camera2CameraCaptureResult)cameraCaptureResult).getCaptureResult();
        }
        return null;
    }
}

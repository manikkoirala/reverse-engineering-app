// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2;

import androidx.annotation.RestrictTo;
import androidx.camera.camera2.internal.Camera2UseCaseConfigFactory;
import androidx.camera.core.CameraUnavailableException;
import androidx.camera.core.InitializationException;
import androidx.camera.camera2.internal.Camera2DeviceSurfaceManager;
import java.util.Set;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.UseCaseConfigFactory;
import \u3007080.\u3007o\u3007;
import androidx.camera.core.impl.CameraDeviceSurfaceManager;
import \u3007080.\u3007o00\u3007\u3007Oo;
import androidx.camera.core.impl.CameraFactory;
import \u3007080.\u3007080;
import androidx.camera.core.CameraXConfig;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class Camera2Config
{
    private Camera2Config() {
    }
    
    @NonNull
    public static CameraXConfig defaultConfig() {
        return new CameraXConfig.Builder().setCameraFactoryProvider(new \u3007080()).setDeviceSurfaceManagerProvider(new \u3007o00\u3007\u3007Oo()).setUseCaseConfigFactoryProvider(new \u3007o\u3007()).build();
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final class DefaultProvider implements Provider
    {
        @NonNull
        @Override
        public CameraXConfig getCameraXConfig() {
            return Camera2Config.defaultConfig();
        }
    }
}

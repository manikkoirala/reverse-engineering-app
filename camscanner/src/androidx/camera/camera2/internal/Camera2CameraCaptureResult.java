// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import android.os.Build$VERSION;
import java.nio.BufferUnderflowException;
import android.graphics.Rect;
import androidx.camera.core.impl.\u3007080;
import androidx.camera.core.impl.utils.ExifData;
import androidx.camera.core.Logger;
import androidx.camera.core.impl.CameraCaptureMetaData;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.TagBundle;
import android.hardware.camera2.CaptureResult;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.CameraCaptureResult;

@RequiresApi(21)
public class Camera2CameraCaptureResult implements CameraCaptureResult
{
    private static final String TAG = "C2CameraCaptureResult";
    private final CaptureResult mCaptureResult;
    private final TagBundle mTagBundle;
    
    public Camera2CameraCaptureResult(@NonNull final CaptureResult captureResult) {
        this(TagBundle.emptyBundle(), captureResult);
    }
    
    public Camera2CameraCaptureResult(@NonNull final TagBundle mTagBundle, @NonNull final CaptureResult mCaptureResult) {
        this.mTagBundle = mTagBundle;
        this.mCaptureResult = mCaptureResult;
    }
    
    @NonNull
    @Override
    public CameraCaptureMetaData.AeState getAeState() {
        final Integer obj = (Integer)this.mCaptureResult.get(CaptureResult.CONTROL_AE_STATE);
        if (obj == null) {
            return CameraCaptureMetaData.AeState.UNKNOWN;
        }
        final int intValue = obj;
        if (intValue != 0) {
            if (intValue != 1) {
                if (intValue == 2) {
                    return CameraCaptureMetaData.AeState.CONVERGED;
                }
                if (intValue == 3) {
                    return CameraCaptureMetaData.AeState.LOCKED;
                }
                if (intValue == 4) {
                    return CameraCaptureMetaData.AeState.FLASH_REQUIRED;
                }
                if (intValue != 5) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Undefined ae state: ");
                    sb.append(obj);
                    Logger.e("C2CameraCaptureResult", sb.toString());
                    return CameraCaptureMetaData.AeState.UNKNOWN;
                }
            }
            return CameraCaptureMetaData.AeState.SEARCHING;
        }
        return CameraCaptureMetaData.AeState.INACTIVE;
    }
    
    @NonNull
    @Override
    public CameraCaptureMetaData.AfMode getAfMode() {
        final Integer obj = (Integer)this.mCaptureResult.get(CaptureResult.CONTROL_AF_MODE);
        if (obj == null) {
            return CameraCaptureMetaData.AfMode.UNKNOWN;
        }
        final int intValue = obj;
        if (intValue != 0) {
            if (intValue == 1 || intValue == 2) {
                return CameraCaptureMetaData.AfMode.ON_MANUAL_AUTO;
            }
            if (intValue == 3 || intValue == 4) {
                return CameraCaptureMetaData.AfMode.ON_CONTINUOUS_AUTO;
            }
            if (intValue != 5) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Undefined af mode: ");
                sb.append(obj);
                Logger.e("C2CameraCaptureResult", sb.toString());
                return CameraCaptureMetaData.AfMode.UNKNOWN;
            }
        }
        return CameraCaptureMetaData.AfMode.OFF;
    }
    
    @NonNull
    @Override
    public CameraCaptureMetaData.AfState getAfState() {
        final Integer obj = (Integer)this.mCaptureResult.get(CaptureResult.CONTROL_AF_STATE);
        if (obj == null) {
            return CameraCaptureMetaData.AfState.UNKNOWN;
        }
        switch (obj) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Undefined af state: ");
                sb.append(obj);
                Logger.e("C2CameraCaptureResult", sb.toString());
                return CameraCaptureMetaData.AfState.UNKNOWN;
            }
            case 6: {
                return CameraCaptureMetaData.AfState.PASSIVE_NOT_FOCUSED;
            }
            case 5: {
                return CameraCaptureMetaData.AfState.LOCKED_NOT_FOCUSED;
            }
            case 4: {
                return CameraCaptureMetaData.AfState.LOCKED_FOCUSED;
            }
            case 2: {
                return CameraCaptureMetaData.AfState.PASSIVE_FOCUSED;
            }
            case 1:
            case 3: {
                return CameraCaptureMetaData.AfState.SCANNING;
            }
            case 0: {
                return CameraCaptureMetaData.AfState.INACTIVE;
            }
        }
    }
    
    @NonNull
    @Override
    public CameraCaptureMetaData.AwbState getAwbState() {
        final Integer obj = (Integer)this.mCaptureResult.get(CaptureResult.CONTROL_AWB_STATE);
        if (obj == null) {
            return CameraCaptureMetaData.AwbState.UNKNOWN;
        }
        final int intValue = obj;
        if (intValue == 0) {
            return CameraCaptureMetaData.AwbState.INACTIVE;
        }
        if (intValue == 1) {
            return CameraCaptureMetaData.AwbState.METERING;
        }
        if (intValue == 2) {
            return CameraCaptureMetaData.AwbState.CONVERGED;
        }
        if (intValue != 3) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Undefined awb state: ");
            sb.append(obj);
            Logger.e("C2CameraCaptureResult", sb.toString());
            return CameraCaptureMetaData.AwbState.UNKNOWN;
        }
        return CameraCaptureMetaData.AwbState.LOCKED;
    }
    
    @NonNull
    @Override
    public CaptureResult getCaptureResult() {
        return this.mCaptureResult;
    }
    
    @NonNull
    @Override
    public CameraCaptureMetaData.FlashState getFlashState() {
        final Integer obj = (Integer)this.mCaptureResult.get(CaptureResult.FLASH_STATE);
        if (obj == null) {
            return CameraCaptureMetaData.FlashState.UNKNOWN;
        }
        final int intValue = obj;
        if (intValue == 0 || intValue == 1) {
            return CameraCaptureMetaData.FlashState.NONE;
        }
        if (intValue == 2) {
            return CameraCaptureMetaData.FlashState.READY;
        }
        if (intValue != 3 && intValue != 4) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Undefined flash state: ");
            sb.append(obj);
            Logger.e("C2CameraCaptureResult", sb.toString());
            return CameraCaptureMetaData.FlashState.UNKNOWN;
        }
        return CameraCaptureMetaData.FlashState.FIRED;
    }
    
    @NonNull
    @Override
    public TagBundle getTagBundle() {
        return this.mTagBundle;
    }
    
    @Override
    public long getTimestamp() {
        final Long n = (Long)this.mCaptureResult.get(CaptureResult.SENSOR_TIMESTAMP);
        if (n == null) {
            return -1L;
        }
        return n;
    }
    
    @Override
    public void populateExifData(@NonNull final ExifData.Builder builder) {
        \u3007080.\u3007o00\u3007\u3007Oo(this, builder);
        final Rect rect = (Rect)this.mCaptureResult.get(CaptureResult.SCALER_CROP_REGION);
        if (rect != null) {
            builder.setImageWidth(rect.width()).setImageHeight(rect.height());
        }
        try {
            final Integer n = (Integer)this.mCaptureResult.get(CaptureResult.JPEG_ORIENTATION);
            if (n != null) {
                builder.setOrientationDegrees(n);
            }
        }
        catch (final BufferUnderflowException ex) {
            Logger.w("C2CameraCaptureResult", "Failed to get JPEG orientation.");
        }
        final Long n2 = (Long)this.mCaptureResult.get(CaptureResult.SENSOR_EXPOSURE_TIME);
        if (n2 != null) {
            builder.setExposureTimeNanos(n2);
        }
        final Float n3 = (Float)this.mCaptureResult.get(CaptureResult.LENS_APERTURE);
        if (n3 != null) {
            builder.setLensFNumber(n3);
        }
        final Integer n4 = (Integer)this.mCaptureResult.get(CaptureResult.SENSOR_SENSITIVITY);
        if (n4 != null) {
            Integer value = n4;
            if (Build$VERSION.SDK_INT >= 24) {
                final Integer n5 = (Integer)this.mCaptureResult.get(\u3007o\u3007.\u3007080());
                value = n4;
                if (n5 != null) {
                    value = n4 * (int)(n5 / 100.0f);
                }
            }
            builder.setIso(value);
        }
        final Float n6 = (Float)this.mCaptureResult.get(CaptureResult.LENS_FOCAL_LENGTH);
        if (n6 != null) {
            builder.setFocalLength(n6);
        }
        final Integer n7 = (Integer)this.mCaptureResult.get(CaptureResult.CONTROL_AWB_MODE);
        if (n7 != null) {
            ExifData.WhiteBalanceMode whiteBalanceMode = ExifData.WhiteBalanceMode.AUTO;
            if (n7 == 0) {
                whiteBalanceMode = ExifData.WhiteBalanceMode.MANUAL;
            }
            builder.setWhiteBalanceMode(whiteBalanceMode);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.annotation;

import androidx.annotation.RestrictTo;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.SOURCE)
@Target({ ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD })
@RestrictTo({ RestrictTo.Scope.LIBRARY })
public @interface CameraExecutor {
}

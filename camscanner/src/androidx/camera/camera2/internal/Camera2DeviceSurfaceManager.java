// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import android.util.Size;
import androidx.camera.core.impl.UseCaseConfig;
import androidx.camera.core.impl.AttachedSurfaceInfo;
import androidx.camera.core.impl.SurfaceConfig;
import java.util.List;
import java.util.Iterator;
import androidx.annotation.RestrictTo;
import android.media.CamcorderProfile;
import androidx.camera.core.CameraUnavailableException;
import androidx.camera.camera2.internal.compat.CameraManagerCompat;
import androidx.core.util.Preconditions;
import java.util.HashMap;
import java.util.Set;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.content.Context;
import java.util.Map;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.CameraDeviceSurfaceManager;

@RequiresApi(21)
public final class Camera2DeviceSurfaceManager implements CameraDeviceSurfaceManager
{
    private static final String TAG = "Camera2DeviceSurfaceManager";
    private final CamcorderProfileHelper mCamcorderProfileHelper;
    private final Map<String, SupportedSurfaceCombination> mCameraSupportedSurfaceCombinationMap;
    
    Camera2DeviceSurfaceManager(@NonNull final Context context, @NonNull final CamcorderProfileHelper mCamcorderProfileHelper, @Nullable final Object o, @NonNull final Set<String> set) throws CameraUnavailableException {
        this.mCameraSupportedSurfaceCombinationMap = new HashMap<String, SupportedSurfaceCombination>();
        Preconditions.checkNotNull(mCamcorderProfileHelper);
        this.mCamcorderProfileHelper = mCamcorderProfileHelper;
        CameraManagerCompat from;
        if (o instanceof CameraManagerCompat) {
            from = (CameraManagerCompat)o;
        }
        else {
            from = CameraManagerCompat.from(context);
        }
        this.init(context, from, set);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public Camera2DeviceSurfaceManager(@NonNull final Context context, @Nullable final Object o, @NonNull final Set<String> set) throws CameraUnavailableException {
        this(context, new CamcorderProfileHelper() {
            @Override
            public CamcorderProfile get(final int n, final int n2) {
                return CamcorderProfile.get(n, n2);
            }
            
            @Override
            public boolean hasProfile(final int n, final int n2) {
                return CamcorderProfile.hasProfile(n, n2);
            }
        }, o, set);
    }
    
    private void init(@NonNull final Context context, @NonNull final CameraManagerCompat cameraManagerCompat, @NonNull final Set<String> set) throws CameraUnavailableException {
        Preconditions.checkNotNull(context);
        for (final String s : set) {
            this.mCameraSupportedSurfaceCombinationMap.put(s, new SupportedSurfaceCombination(context, s, cameraManagerCompat, this.mCamcorderProfileHelper));
        }
    }
    
    @Override
    public boolean checkSupported(@NonNull final String s, @Nullable final List<SurfaceConfig> list) {
        if (list != null && !list.isEmpty()) {
            final SupportedSurfaceCombination supportedSurfaceCombination = this.mCameraSupportedSurfaceCombinationMap.get(s);
            return supportedSurfaceCombination != null && supportedSurfaceCombination.checkSupported(list);
        }
        return true;
    }
    
    @NonNull
    @Override
    public Map<UseCaseConfig<?>, Size> getSuggestedResolutions(@NonNull final String str, @NonNull final List<AttachedSurfaceInfo> list, @NonNull final List<UseCaseConfig<?>> list2) {
        Preconditions.checkArgument(list2.isEmpty() ^ true, (Object)"No new use cases to be bound.");
        final SupportedSurfaceCombination supportedSurfaceCombination = this.mCameraSupportedSurfaceCombinationMap.get(str);
        if (supportedSurfaceCombination != null) {
            return supportedSurfaceCombination.getSuggestedResolutions(list, list2);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("No such camera id in supported combination list: ");
        sb.append(str);
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Nullable
    @Override
    public SurfaceConfig transformSurfaceConfig(@NonNull final String s, final int n, @NonNull final Size size) {
        final SupportedSurfaceCombination supportedSurfaceCombination = this.mCameraSupportedSurfaceCombinationMap.get(s);
        SurfaceConfig transformSurfaceConfig;
        if (supportedSurfaceCombination != null) {
            transformSurfaceConfig = supportedSurfaceCombination.transformSurfaceConfig(n, size);
        }
        else {
            transformSurfaceConfig = null;
        }
        return transformSurfaceConfig;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.annotation.Nullable;
import androidx.camera.core.ImageProxy;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.SessionConfig;

interface ZslControl
{
    void addZslConfig(@NonNull final SessionConfig.Builder p0);
    
    @Nullable
    ImageProxy dequeueImageFromBuffer();
    
    boolean enqueueImageToImageWriter(@NonNull final ImageProxy p0);
    
    boolean isZslDisabledByFlashMode();
    
    boolean isZslDisabledByUserCaseConfig();
    
    void setZslDisabledByFlashMode(final boolean p0);
    
    void setZslDisabledByUserCaseConfig(final boolean p0);
}

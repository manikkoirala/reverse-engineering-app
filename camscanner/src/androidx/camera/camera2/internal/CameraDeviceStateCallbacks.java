// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import android.hardware.camera2.CameraDevice;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Arrays;
import androidx.annotation.NonNull;
import android.hardware.camera2.CameraDevice$StateCallback;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class CameraDeviceStateCallbacks
{
    private CameraDeviceStateCallbacks() {
    }
    
    @NonNull
    public static CameraDevice$StateCallback createComboCallback(@NonNull final List<CameraDevice$StateCallback> list) {
        if (list.isEmpty()) {
            return createNoOpCallback();
        }
        if (list.size() == 1) {
            return list.get(0);
        }
        return new ComboDeviceStateCallback(list);
    }
    
    @NonNull
    public static CameraDevice$StateCallback createComboCallback(@NonNull final CameraDevice$StateCallback... a) {
        return createComboCallback(Arrays.asList(a));
    }
    
    @NonNull
    public static CameraDevice$StateCallback createNoOpCallback() {
        return new NoOpDeviceStateCallback();
    }
    
    private static final class ComboDeviceStateCallback extends CameraDevice$StateCallback
    {
        private final List<CameraDevice$StateCallback> mCallbacks;
        
        ComboDeviceStateCallback(@NonNull final List<CameraDevice$StateCallback> list) {
            this.mCallbacks = new ArrayList<CameraDevice$StateCallback>();
            for (final CameraDevice$StateCallback cameraDevice$StateCallback : list) {
                if (!(cameraDevice$StateCallback instanceof NoOpDeviceStateCallback)) {
                    this.mCallbacks.add(cameraDevice$StateCallback);
                }
            }
        }
        
        public void onClosed(@NonNull final CameraDevice cameraDevice) {
            final Iterator<CameraDevice$StateCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onClosed(cameraDevice);
            }
        }
        
        public void onDisconnected(@NonNull final CameraDevice cameraDevice) {
            final Iterator<CameraDevice$StateCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onDisconnected(cameraDevice);
            }
        }
        
        public void onError(@NonNull final CameraDevice cameraDevice, final int n) {
            final Iterator<CameraDevice$StateCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onError(cameraDevice, n);
            }
        }
        
        public void onOpened(@NonNull final CameraDevice cameraDevice) {
            final Iterator<CameraDevice$StateCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onOpened(cameraDevice);
            }
        }
    }
    
    static final class NoOpDeviceStateCallback extends CameraDevice$StateCallback
    {
        public void onClosed(@NonNull final CameraDevice cameraDevice) {
        }
        
        public void onDisconnected(@NonNull final CameraDevice cameraDevice) {
        }
        
        public void onError(@NonNull final CameraDevice cameraDevice, final int n) {
        }
        
        public void onOpened(@NonNull final CameraDevice cameraDevice) {
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.core.CameraUnavailableException;
import androidx.annotation.NonNull;
import androidx.camera.camera2.internal.compat.CameraAccessExceptionCompat;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class CameraUnavailableExceptionHelper
{
    private CameraUnavailableExceptionHelper() {
    }
    
    @NonNull
    public static CameraUnavailableException createFrom(@NonNull final CameraAccessExceptionCompat cameraAccessExceptionCompat) {
        final int reason = cameraAccessExceptionCompat.getReason();
        int n = 1;
        if (reason != 1) {
            n = 2;
            if (reason != 2) {
                n = 3;
                if (reason != 3) {
                    n = 4;
                    if (reason != 4) {
                        n = 5;
                        if (reason != 5) {
                            if (reason != 10001) {
                                n = 0;
                            }
                            else {
                                n = 6;
                            }
                        }
                    }
                }
            }
        }
        return new CameraUnavailableException(n, cameraAccessExceptionCompat);
    }
}

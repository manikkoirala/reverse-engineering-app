// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import java.util.concurrent.Future;
import android.view.Surface;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.core.Logger;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import android.hardware.camera2.CaptureRequest;
import androidx.camera.camera2.internal.compat.params.SessionConfigurationCompat;
import android.hardware.camera2.CameraDevice;
import android.os.Handler;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Executor;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.Quirks;
import androidx.camera.camera2.internal.compat.workaround.WaitForRepeatingRequestStart;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.camera.camera2.internal.compat.workaround.ForceCloseCaptureSession;
import androidx.annotation.Nullable;
import androidx.annotation.GuardedBy;
import androidx.camera.core.impl.DeferrableSurface;
import java.util.List;
import androidx.camera.camera2.internal.compat.workaround.ForceCloseDeferrableSurface;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
class SynchronizedCaptureSessionImpl extends SynchronizedCaptureSessionBaseImpl
{
    private static final String TAG = "SyncCaptureSessionImpl";
    private final ForceCloseDeferrableSurface mCloseSurfaceQuirk;
    @GuardedBy("mObjectLock")
    @Nullable
    private List<DeferrableSurface> mDeferrableSurfaces;
    private final ForceCloseCaptureSession mForceCloseSessionQuirk;
    private final Object mObjectLock;
    @GuardedBy("mObjectLock")
    @Nullable
    ListenableFuture<Void> mOpeningCaptureSession;
    private final WaitForRepeatingRequestStart mWaitForOtherSessionCompleteQuirk;
    
    SynchronizedCaptureSessionImpl(@NonNull final Quirks quirks, @NonNull final Quirks quirks2, @NonNull final CaptureSessionRepository captureSessionRepository, @NonNull final Executor executor, @NonNull final ScheduledExecutorService scheduledExecutorService, @NonNull final Handler handler) {
        super(captureSessionRepository, executor, scheduledExecutorService, handler);
        this.mObjectLock = new Object();
        this.mCloseSurfaceQuirk = new ForceCloseDeferrableSurface(quirks, quirks2);
        this.mWaitForOtherSessionCompleteQuirk = new WaitForRepeatingRequestStart(quirks);
        this.mForceCloseSessionQuirk = new ForceCloseCaptureSession(quirks2);
    }
    
    @Override
    public void close() {
        this.debugLog("Session call close()");
        this.mWaitForOtherSessionCompleteQuirk.onSessionEnd();
        this.mWaitForOtherSessionCompleteQuirk.getStartStreamFuture().addListener((Runnable)new O0\u3007OO8(this), this.getExecutor());
    }
    
    void debugLog(final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(this);
        sb.append("] ");
        sb.append(str);
        Logger.d("SyncCaptureSessionImpl", sb.toString());
    }
    
    @NonNull
    @Override
    public ListenableFuture<Void> getOpeningBlocker() {
        return this.mWaitForOtherSessionCompleteQuirk.getStartStreamFuture();
    }
    
    @Override
    public void onClosed(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
        synchronized (this.mObjectLock) {
            this.mCloseSurfaceQuirk.onSessionEnd(this.mDeferrableSurfaces);
            monitorexit(this.mObjectLock);
            this.debugLog("onClosed()");
            super.onClosed(synchronizedCaptureSession);
        }
    }
    
    @Override
    public void onConfigured(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
        this.debugLog("Session onConfigured()");
        this.mForceCloseSessionQuirk.onSessionConfigured(synchronizedCaptureSession, super.mCaptureSessionRepository.getCreatingCaptureSessions(), super.mCaptureSessionRepository.getCaptureSessions(), (ForceCloseCaptureSession.OnConfigured)new \u3007000\u3007\u300708(this));
    }
    
    @NonNull
    @Override
    public ListenableFuture<Void> openCaptureSession(@NonNull final CameraDevice cameraDevice, @NonNull final SessionConfigurationCompat sessionConfigurationCompat, @NonNull final List<DeferrableSurface> list) {
        synchronized (this.mObjectLock) {
            final ListenableFuture<Void> openCaptureSession = this.mWaitForOtherSessionCompleteQuirk.openCaptureSession(cameraDevice, sessionConfigurationCompat, list, super.mCaptureSessionRepository.getClosingCaptureSession(), (WaitForRepeatingRequestStart.OpenCaptureSession)new Oo\u3007O8o\u30078(this));
            this.mOpeningCaptureSession = openCaptureSession;
            return Futures.nonCancellationPropagating(openCaptureSession);
        }
    }
    
    @Override
    public int setSingleRepeatingRequest(@NonNull final CaptureRequest captureRequest, @NonNull final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        return this.mWaitForOtherSessionCompleteQuirk.setSingleRepeatingRequest(captureRequest, cameraCaptureSession$CaptureCallback, (WaitForRepeatingRequestStart.SingleRepeatingRequest)new O8O\u300788oO0(this));
    }
    
    @NonNull
    @Override
    public ListenableFuture<List<Surface>> startWithDeferrableSurface(@NonNull final List<DeferrableSurface> mDeferrableSurfaces, final long n) {
        synchronized (this.mObjectLock) {
            this.mDeferrableSurfaces = mDeferrableSurfaces;
            return super.startWithDeferrableSurface(mDeferrableSurfaces, n);
        }
    }
    
    @Override
    public boolean stop() {
        synchronized (this.mObjectLock) {
            if (this.isCameraCaptureSessionOpen()) {
                this.mCloseSurfaceQuirk.onSessionEnd(this.mDeferrableSurfaces);
            }
            else {
                final ListenableFuture<Void> mOpeningCaptureSession = this.mOpeningCaptureSession;
                if (mOpeningCaptureSession != null) {
                    ((Future)mOpeningCaptureSession).cancel(true);
                }
            }
            return super.stop();
        }
    }
}

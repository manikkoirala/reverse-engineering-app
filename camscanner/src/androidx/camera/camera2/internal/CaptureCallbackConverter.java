// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import java.util.Iterator;
import androidx.camera.core.impl.CameraCaptureCallbacks;
import java.util.List;
import java.util.ArrayList;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import androidx.camera.core.impl.CameraCaptureCallback;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
final class CaptureCallbackConverter
{
    private CaptureCallbackConverter() {
    }
    
    static CameraCaptureSession$CaptureCallback toCaptureCallback(final CameraCaptureCallback cameraCaptureCallback) {
        if (cameraCaptureCallback == null) {
            return null;
        }
        final ArrayList list = new ArrayList();
        toCaptureCallback(cameraCaptureCallback, list);
        CameraCaptureSession$CaptureCallback comboCallback;
        if (list.size() == 1) {
            comboCallback = (CameraCaptureSession$CaptureCallback)list.get(0);
        }
        else {
            comboCallback = Camera2CaptureCallbacks.createComboCallback(list);
        }
        return comboCallback;
    }
    
    static void toCaptureCallback(final CameraCaptureCallback cameraCaptureCallback, final List<CameraCaptureSession$CaptureCallback> list) {
        if (cameraCaptureCallback instanceof CameraCaptureCallbacks.ComboCameraCaptureCallback) {
            final Iterator<CameraCaptureCallback> iterator = ((CameraCaptureCallbacks.ComboCameraCaptureCallback)cameraCaptureCallback).getCallbacks().iterator();
            while (iterator.hasNext()) {
                toCaptureCallback(iterator.next(), list);
            }
        }
        else if (cameraCaptureCallback instanceof CaptureCallbackContainer) {
            list.add(((CaptureCallbackContainer)cameraCaptureCallback).getCaptureCallback());
        }
        else {
            list.add(new CaptureCallbackAdapter(cameraCaptureCallback));
        }
    }
}

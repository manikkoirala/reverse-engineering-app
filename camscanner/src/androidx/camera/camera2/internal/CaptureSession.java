// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.core.impl.utils.futures.FutureCallback;
import androidx.camera.core.impl.utils.futures.AsyncFunction;
import androidx.camera.core.impl.utils.futures.FutureChain;
import androidx.camera.camera2.interop.ExperimentalCamera2Interop;
import androidx.annotation.OptIn;
import androidx.camera.camera2.internal.compat.params.SessionConfigurationCompat;
import android.hardware.camera2.CameraAccessException;
import androidx.camera.camera2.internal.compat.params.InputConfigurationCompat;
import androidx.camera.camera2.impl.Camera2ImplConfig;
import androidx.camera.core.impl.utils.futures.Futures;
import java.util.concurrent.CancellationException;
import androidx.camera.core.Logger;
import java.util.Objects;
import androidx.camera.core.impl.MutableOptionsBundle;
import android.hardware.camera2.CameraDevice;
import androidx.core.util.Preconditions;
import androidx.camera.camera2.internal.compat.params.OutputConfigurationCompat;
import java.util.Iterator;
import java.util.Collection;
import androidx.camera.core.impl.CameraCaptureCallback;
import java.util.Collections;
import java.util.HashMap;
import androidx.camera.core.impl.OptionsBundle;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CameraCaptureSession;
import java.util.ArrayList;
import androidx.camera.camera2.internal.compat.workaround.TorchStateReset;
import androidx.camera.camera2.internal.compat.workaround.StillCaptureFlow;
import androidx.annotation.Nullable;
import androidx.camera.core.impl.SessionConfig;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import android.view.Surface;
import java.util.Map;
import androidx.camera.core.impl.DeferrableSurface;
import androidx.camera.core.impl.CaptureConfig;
import java.util.List;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import androidx.camera.core.impl.Config;
import androidx.annotation.NonNull;
import androidx.annotation.GuardedBy;
import androidx.camera.camera2.impl.CameraEventCallbacks;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
final class CaptureSession implements CaptureSessionInterface
{
    private static final String TAG = "CaptureSession";
    private static final long TIMEOUT_GET_SURFACE_IN_MS = 5000L;
    @GuardedBy("mSessionLock")
    @NonNull
    CameraEventCallbacks mCameraEventCallbacks;
    @GuardedBy("mSessionLock")
    @NonNull
    Config mCameraEventOnRepeatingOptions;
    private final CameraCaptureSession$CaptureCallback mCaptureCallback;
    @GuardedBy("mSessionLock")
    private final List<CaptureConfig> mCaptureConfigs;
    @GuardedBy("mSessionLock")
    private final StateCallback mCaptureSessionStateCallback;
    @GuardedBy("mSessionLock")
    List<DeferrableSurface> mConfiguredDeferrableSurfaces;
    @GuardedBy("mSessionLock")
    private final Map<DeferrableSurface, Surface> mConfiguredSurfaceMap;
    @GuardedBy("mSessionLock")
    CallbackToFutureAdapter.Completer<Void> mReleaseCompleter;
    @GuardedBy("mSessionLock")
    ListenableFuture<Void> mReleaseFuture;
    @GuardedBy("mSessionLock")
    @Nullable
    SessionConfig mSessionConfig;
    final Object mSessionLock;
    @GuardedBy("mSessionLock")
    State mState;
    final StillCaptureFlow mStillCaptureFlow;
    @GuardedBy("mSessionLock")
    @Nullable
    SynchronizedCaptureSession mSynchronizedCaptureSession;
    @GuardedBy("mSessionLock")
    @Nullable
    SynchronizedCaptureSessionOpener mSynchronizedCaptureSessionOpener;
    final TorchStateReset mTorchStateReset;
    
    CaptureSession() {
        this.mSessionLock = new Object();
        this.mCaptureConfigs = new ArrayList<CaptureConfig>();
        this.mCaptureCallback = new CameraCaptureSession$CaptureCallback() {
            final CaptureSession this$0;
            
            public void onCaptureCompleted(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, @NonNull final TotalCaptureResult totalCaptureResult) {
            }
        };
        this.mCameraEventOnRepeatingOptions = OptionsBundle.emptyBundle();
        this.mCameraEventCallbacks = CameraEventCallbacks.createEmptyCallback();
        this.mConfiguredSurfaceMap = new HashMap<DeferrableSurface, Surface>();
        this.mConfiguredDeferrableSurfaces = Collections.emptyList();
        this.mState = State.UNINITIALIZED;
        this.mStillCaptureFlow = new StillCaptureFlow();
        this.mTorchStateReset = new TorchStateReset();
        this.mState = State.INITIALIZED;
        this.mCaptureSessionStateCallback = new StateCallback();
    }
    
    @GuardedBy("mSessionLock")
    private CameraCaptureSession$CaptureCallback createCamera2CaptureCallback(final List<CameraCaptureCallback> list, final CameraCaptureSession$CaptureCallback... elements) {
        final ArrayList c = new ArrayList(list.size() + elements.length);
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            c.add(CaptureCallbackConverter.toCaptureCallback((CameraCaptureCallback)iterator.next()));
        }
        Collections.addAll(c, elements);
        return Camera2CaptureCallbacks.createComboCallback(c);
    }
    
    @NonNull
    private OutputConfigurationCompat getOutputConfigurationCompat(@NonNull final SessionConfig.OutputConfig outputConfig, @NonNull final Map<DeferrableSurface, Surface> map, @Nullable final String physicalCameraId) {
        final Surface surface = map.get(outputConfig.getSurface());
        Preconditions.checkNotNull(surface, "Surface in OutputConfig not found in configuredSurfaceMap.");
        final OutputConfigurationCompat outputConfigurationCompat = new OutputConfigurationCompat(outputConfig.getSurfaceGroupId(), surface);
        if (physicalCameraId != null) {
            outputConfigurationCompat.setPhysicalCameraId(physicalCameraId);
        }
        else {
            outputConfigurationCompat.setPhysicalCameraId(outputConfig.getPhysicalCameraId());
        }
        if (!outputConfig.getSharedSurfaces().isEmpty()) {
            outputConfigurationCompat.enableSurfaceSharing();
            final Iterator<DeferrableSurface> iterator = outputConfig.getSharedSurfaces().iterator();
            while (iterator.hasNext()) {
                final Surface surface2 = map.get(iterator.next());
                Preconditions.checkNotNull(surface2, "Surface in OutputConfig not found in configuredSurfaceMap.");
                outputConfigurationCompat.addSurface(surface2);
            }
        }
        return outputConfigurationCompat;
    }
    
    @NonNull
    private List<OutputConfigurationCompat> getUniqueOutputConfigurations(@NonNull final List<OutputConfigurationCompat> list) {
        final ArrayList list2 = new ArrayList();
        final ArrayList list3 = new ArrayList();
        for (final OutputConfigurationCompat outputConfigurationCompat : list) {
            if (list2.contains(outputConfigurationCompat.getSurface())) {
                continue;
            }
            list2.add(outputConfigurationCompat.getSurface());
            list3.add(outputConfigurationCompat);
        }
        return list3;
    }
    
    @NonNull
    private static Config mergeOptions(final List<CaptureConfig> list) {
        final MutableOptionsBundle create = MutableOptionsBundle.create();
        final Iterator<CaptureConfig> iterator = list.iterator();
        while (iterator.hasNext()) {
            final Config implementationOptions = iterator.next().getImplementationOptions();
            for (final Config.Option<Object> option : implementationOptions.listOptions()) {
                final Object retrieveOption = implementationOptions.retrieveOption(option, null);
                if (create.containsOption((Config.Option<?>)option)) {
                    final ValueT retrieveOption2 = create.retrieveOption((Config.Option<ValueT>)option, (ValueT)null);
                    if (Objects.equals(retrieveOption2, retrieveOption)) {
                        continue;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Detect conflicting option ");
                    sb.append(option.getId());
                    sb.append(" : ");
                    sb.append(retrieveOption);
                    sb.append(" != ");
                    sb.append(retrieveOption2);
                    Logger.d("CaptureSession", sb.toString());
                }
                else {
                    create.insertOption(option, retrieveOption);
                }
            }
        }
        return create;
    }
    
    @NonNull
    @OptIn(markerClass = { ExperimentalCamera2Interop.class })
    private ListenableFuture<Void> openCaptureSession(@NonNull final List<Surface> list, @NonNull final SessionConfig sessionConfig, @NonNull final CameraDevice cameraDevice) {
        synchronized (this.mSessionLock) {
            final int n = CaptureSession$4.$SwitchMap$androidx$camera$camera2$internal$CaptureSession$State[this.mState.ordinal()];
            if (n != 1 && n != 2) {
                if (n != 3) {
                    if (n != 5) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("openCaptureSession() not execute in state: ");
                        sb.append(this.mState);
                        return (ListenableFuture<Void>)Futures.immediateFailedFuture(new CancellationException(sb.toString()));
                    }
                }
                else {
                    this.mConfiguredSurfaceMap.clear();
                    for (int i = 0; i < list.size(); ++i) {
                        this.mConfiguredSurfaceMap.put(this.mConfiguredDeferrableSurfaces.get(i), (Surface)list.get(i));
                    }
                    this.mState = State.OPENING;
                    Logger.d("CaptureSession", "Opening capture session.");
                    final SynchronizedCaptureSession.StateCallback comboCallback = SynchronizedCaptureSessionStateCallbacks.createComboCallback(this.mCaptureSessionStateCallback, new SynchronizedCaptureSessionStateCallbacks.Adapter(sessionConfig.getSessionStateCallbacks()));
                    final Camera2ImplConfig camera2ImplConfig = new Camera2ImplConfig(sessionConfig.getImplementationOptions());
                    final CameraEventCallbacks cameraEventCallback = camera2ImplConfig.getCameraEventCallback(CameraEventCallbacks.createEmptyCallback());
                    this.mCameraEventCallbacks = cameraEventCallback;
                    final List<CaptureConfig> onInitSession = cameraEventCallback.createComboCallback().onInitSession();
                    final CaptureConfig.Builder from = CaptureConfig.Builder.from(sessionConfig.getRepeatingCaptureConfig());
                    final Iterator<CaptureConfig> iterator = onInitSession.iterator();
                    while (iterator.hasNext()) {
                        from.addImplementationOptions(iterator.next().getImplementationOptions());
                    }
                    final ArrayList list2 = new ArrayList();
                    final String physicalCameraId = camera2ImplConfig.getPhysicalCameraId(null);
                    final Iterator<SessionConfig.OutputConfig> iterator2 = sessionConfig.getOutputConfigs().iterator();
                    while (iterator2.hasNext()) {
                        final OutputConfigurationCompat outputConfigurationCompat = this.getOutputConfigurationCompat(iterator2.next(), this.mConfiguredSurfaceMap, physicalCameraId);
                        final Config implementationOptions = sessionConfig.getImplementationOptions();
                        final Config.Option<Long> stream_USE_CASE_OPTION = Camera2ImplConfig.STREAM_USE_CASE_OPTION;
                        if (implementationOptions.containsOption((Config.Option<?>)stream_USE_CASE_OPTION)) {
                            outputConfigurationCompat.setStreamUseCase(sessionConfig.getImplementationOptions().retrieveOption(stream_USE_CASE_OPTION));
                        }
                        list2.add(outputConfigurationCompat);
                    }
                    final SessionConfigurationCompat sessionConfigurationCompat = this.mSynchronizedCaptureSessionOpener.createSessionConfigurationCompat(0, this.getUniqueOutputConfigurations(list2), comboCallback);
                    if (sessionConfig.getTemplateType() == 5 && sessionConfig.getInputConfiguration() != null) {
                        sessionConfigurationCompat.setInputConfiguration(InputConfigurationCompat.wrap(sessionConfig.getInputConfiguration()));
                    }
                    try {
                        final CaptureRequest buildWithoutTarget = Camera2CaptureRequestBuilder.buildWithoutTarget(from.build(), cameraDevice);
                        if (buildWithoutTarget != null) {
                            sessionConfigurationCompat.setSessionParameters(buildWithoutTarget);
                        }
                        return this.mSynchronizedCaptureSessionOpener.openCaptureSession(cameraDevice, sessionConfigurationCompat, this.mConfiguredDeferrableSurfaces);
                    }
                    catch (final CameraAccessException ex) {
                        return Futures.immediateFailedFuture((Throwable)ex);
                    }
                }
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("openCaptureSession() should not be possible in state: ");
            sb2.append(this.mState);
            return (ListenableFuture<Void>)Futures.immediateFailedFuture(new IllegalStateException(sb2.toString()));
        }
    }
    
    void abortCaptures() {
        synchronized (this.mSessionLock) {
            if (this.mState != State.OPENED) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to abort captures. Incorrect state:");
                sb.append(this.mState);
                Logger.e("CaptureSession", sb.toString());
                return;
            }
            try {
                this.mSynchronizedCaptureSession.abortCaptures();
            }
            catch (final CameraAccessException ex) {
                Logger.e("CaptureSession", "Unable to abort captures.", (Throwable)ex);
            }
        }
    }
    
    @Override
    public void cancelIssuedCaptureRequests() {
        Object o = this.mSessionLock;
        synchronized (o) {
            List list;
            if (!this.mCaptureConfigs.isEmpty()) {
                list = new ArrayList(this.mCaptureConfigs);
                this.mCaptureConfigs.clear();
            }
            else {
                list = null;
            }
            monitorexit(o);
            if (list != null) {
                final Iterator iterator = list.iterator();
                while (iterator.hasNext()) {
                    o = ((CaptureConfig)iterator.next()).getCameraCaptureCallbacks().iterator();
                    while (((Iterator)o).hasNext()) {
                        ((CameraCaptureCallback)((Iterator)o).next()).onCaptureCancelled();
                    }
                }
            }
        }
    }
    
    @Override
    public void close() {
        synchronized (this.mSessionLock) {
            final int n = CaptureSession$4.$SwitchMap$androidx$camera$camera2$internal$CaptureSession$State[this.mState.ordinal()];
            if (n != 1) {
                if (n != 2) {
                    if (n != 3) {
                        if (n != 4) {
                            if (n != 5) {
                                return;
                            }
                            if (this.mSessionConfig != null) {
                                final List<CaptureConfig> onDisableSession = this.mCameraEventCallbacks.createComboCallback().onDisableSession();
                                if (!onDisableSession.isEmpty()) {
                                    try {
                                        this.issueCaptureRequests(this.setupConfiguredSurface(onDisableSession));
                                    }
                                    catch (final IllegalStateException ex) {
                                        Logger.e("CaptureSession", "Unable to issue the request before close the capture session", ex);
                                    }
                                }
                            }
                        }
                        final SynchronizedCaptureSessionOpener mSynchronizedCaptureSessionOpener = this.mSynchronizedCaptureSessionOpener;
                        final StringBuilder sb = new StringBuilder();
                        sb.append("The Opener shouldn't null in state:");
                        sb.append(this.mState);
                        Preconditions.checkNotNull(mSynchronizedCaptureSessionOpener, sb.toString());
                        this.mSynchronizedCaptureSessionOpener.stop();
                        this.mState = State.CLOSED;
                        this.mSessionConfig = null;
                        return;
                    }
                    final SynchronizedCaptureSessionOpener mSynchronizedCaptureSessionOpener2 = this.mSynchronizedCaptureSessionOpener;
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("The Opener shouldn't null in state:");
                    sb2.append(this.mState);
                    Preconditions.checkNotNull(mSynchronizedCaptureSessionOpener2, sb2.toString());
                    this.mSynchronizedCaptureSessionOpener.stop();
                }
                this.mState = State.RELEASED;
                return;
            }
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("close() should not be possible in state: ");
            sb3.append(this.mState);
            throw new IllegalStateException(sb3.toString());
        }
    }
    
    @GuardedBy("mSessionLock")
    void finishClose() {
        final State mState = this.mState;
        final State released = State.RELEASED;
        if (mState == released) {
            Logger.d("CaptureSession", "Skipping finishClose due to being state RELEASED.");
            return;
        }
        this.mState = released;
        this.mSynchronizedCaptureSession = null;
        final CallbackToFutureAdapter.Completer<Void> mReleaseCompleter = this.mReleaseCompleter;
        if (mReleaseCompleter != null) {
            mReleaseCompleter.set(null);
            this.mReleaseCompleter = null;
        }
    }
    
    @NonNull
    @Override
    public List<CaptureConfig> getCaptureConfigs() {
        synchronized (this.mSessionLock) {
            return Collections.unmodifiableList((List<? extends CaptureConfig>)this.mCaptureConfigs);
        }
    }
    
    @Nullable
    @Override
    public SessionConfig getSessionConfig() {
        synchronized (this.mSessionLock) {
            return this.mSessionConfig;
        }
    }
    
    State getState() {
        synchronized (this.mSessionLock) {
            return this.mState;
        }
    }
    
    int issueBurstCaptureRequest(final List<CaptureConfig> list) {
        synchronized (this.mSessionLock) {
            if (list.isEmpty()) {
                return -1;
            }
            try {
                final CameraBurstCaptureCallback cameraBurstCaptureCallback = new CameraBurstCaptureCallback();
                final ArrayList list2 = new ArrayList();
                Logger.d("CaptureSession", "Issuing capture request.");
                final Iterator iterator = list.iterator();
                boolean b = false;
            Label_0060:
                while (iterator.hasNext()) {
                    final CaptureConfig captureConfig = (CaptureConfig)iterator.next();
                    if (!captureConfig.getSurfaces().isEmpty()) {
                        while (true) {
                            for (final DeferrableSurface obj : captureConfig.getSurfaces()) {
                                if (!this.mConfiguredSurfaceMap.containsKey(obj)) {
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append("Skipping capture request with invalid surface: ");
                                    sb.append(obj);
                                    Logger.d("CaptureSession", sb.toString());
                                    final boolean b2 = false;
                                    if (!b2) {
                                        continue Label_0060;
                                    }
                                    if (captureConfig.getTemplateType() == 2) {
                                        b = true;
                                    }
                                    final CaptureConfig.Builder from = CaptureConfig.Builder.from(captureConfig);
                                    if (captureConfig.getTemplateType() == 5 && captureConfig.getCameraCaptureResult() != null) {
                                        from.setCameraCaptureResult(captureConfig.getCameraCaptureResult());
                                    }
                                    final SessionConfig mSessionConfig = this.mSessionConfig;
                                    if (mSessionConfig != null) {
                                        from.addImplementationOptions(mSessionConfig.getRepeatingCaptureConfig().getImplementationOptions());
                                    }
                                    from.addImplementationOptions(this.mCameraEventOnRepeatingOptions);
                                    from.addImplementationOptions(captureConfig.getImplementationOptions());
                                    final CaptureRequest build = Camera2CaptureRequestBuilder.build(from.build(), this.mSynchronizedCaptureSession.getDevice(), this.mConfiguredSurfaceMap);
                                    if (build == null) {
                                        Logger.d("CaptureSession", "Skipping issuing request without surface.");
                                        return -1;
                                    }
                                    final ArrayList<CameraCaptureSession$CaptureCallback> list3 = new ArrayList<CameraCaptureSession$CaptureCallback>();
                                    final Iterator<CameraCaptureCallback> iterator3 = captureConfig.getCameraCaptureCallbacks().iterator();
                                    while (iterator3.hasNext()) {
                                        CaptureCallbackConverter.toCaptureCallback(iterator3.next(), list3);
                                    }
                                    cameraBurstCaptureCallback.addCamera2Callbacks(build, list3);
                                    list2.add(build);
                                    continue Label_0060;
                                }
                            }
                            final boolean b2 = true;
                            continue;
                        }
                    }
                    Logger.d("CaptureSession", "Skipping issuing empty capture request.");
                }
                if (!list2.isEmpty()) {
                    if (this.mStillCaptureFlow.shouldStopRepeatingBeforeCapture(list2, b)) {
                        this.mSynchronizedCaptureSession.stopRepeating();
                        cameraBurstCaptureCallback.setCaptureSequenceCallback((CameraBurstCaptureCallback.CaptureSequenceCallback)new \u300780(this));
                    }
                    if (this.mTorchStateReset.isTorchResetRequired(list2, b)) {
                        cameraBurstCaptureCallback.addCamera2Callbacks((CaptureRequest)list2.get(list2.size() - 1), (List<CameraCaptureSession$CaptureCallback>)Collections.singletonList(new CameraCaptureSession$CaptureCallback(this) {
                            final CaptureSession this$0;
                            
                            public void onCaptureCompleted(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, @NonNull final TotalCaptureResult totalCaptureResult) {
                                synchronized (this.this$0.mSessionLock) {
                                    final SessionConfig mSessionConfig = this.this$0.mSessionConfig;
                                    if (mSessionConfig == null) {
                                        return;
                                    }
                                    final CaptureConfig repeatingCaptureConfig = mSessionConfig.getRepeatingCaptureConfig();
                                    Logger.d("CaptureSession", "Submit FLASH_MODE_OFF request");
                                    final CaptureSession this$0 = this.this$0;
                                    this$0.issueCaptureRequests(Collections.singletonList(this$0.mTorchStateReset.createTorchResetRequest(repeatingCaptureConfig)));
                                }
                            }
                        }));
                    }
                    return this.mSynchronizedCaptureSession.captureBurstRequests(list2, cameraBurstCaptureCallback);
                }
                Logger.d("CaptureSession", "Skipping issuing burst request due to no valid request elements");
            }
            catch (final CameraAccessException ex) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Unable to access camera: ");
                sb2.append(((Throwable)ex).getMessage());
                Logger.e("CaptureSession", sb2.toString());
                Thread.dumpStack();
            }
            return -1;
        }
    }
    
    @Override
    public void issueCaptureRequests(@NonNull final List<CaptureConfig> list) {
        synchronized (this.mSessionLock) {
            switch (CaptureSession$4.$SwitchMap$androidx$camera$camera2$internal$CaptureSession$State[this.mState.ordinal()]) {
                case 6:
                case 7:
                case 8: {
                    throw new IllegalStateException("Cannot issue capture request on a closed/released session.");
                }
                case 5: {
                    this.mCaptureConfigs.addAll(list);
                    this.issuePendingCaptureRequest();
                    break;
                }
                case 2:
                case 3:
                case 4: {
                    this.mCaptureConfigs.addAll(list);
                    break;
                }
                case 1: {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("issueCaptureRequests() should not be possible in state: ");
                    sb.append(this.mState);
                    throw new IllegalStateException(sb.toString());
                }
            }
        }
    }
    
    @GuardedBy("mSessionLock")
    void issuePendingCaptureRequest() {
        if (this.mCaptureConfigs.isEmpty()) {
            return;
        }
        try {
            this.issueBurstCaptureRequest(this.mCaptureConfigs);
        }
        finally {
            this.mCaptureConfigs.clear();
        }
    }
    
    int issueRepeatingCaptureRequests(@Nullable final SessionConfig sessionConfig) {
        final Object mSessionLock = this.mSessionLock;
        monitorenter(mSessionLock);
        Label_0023: {
            if (sessionConfig != null) {
                break Label_0023;
            }
            try {
                Logger.d("CaptureSession", "Skipping issueRepeatingCaptureRequests for no configuration case.");
                return -1;
                final CaptureConfig repeatingCaptureConfig = sessionConfig.getRepeatingCaptureConfig();
                iftrue(Label_0104:)(!repeatingCaptureConfig.getSurfaces().isEmpty());
                Logger.d("CaptureSession", "Skipping issueRepeatingCaptureRequests for no surface.");
                try {
                    this.mSynchronizedCaptureSession.stopRepeating();
                }
                catch (final CameraAccessException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unable to access camera: ");
                    sb.append(((Throwable)ex).getMessage());
                    Logger.e("CaptureSession", sb.toString());
                    Thread.dumpStack();
                }
                return -1;
                try {
                    Label_0104: {
                        Logger.d("CaptureSession", "Issuing request for session.");
                    }
                    final CaptureConfig.Builder from = CaptureConfig.Builder.from(repeatingCaptureConfig);
                    from.addImplementationOptions(this.mCameraEventOnRepeatingOptions = mergeOptions(this.mCameraEventCallbacks.createComboCallback().onRepeating()));
                    final CaptureRequest build = Camera2CaptureRequestBuilder.build(from.build(), this.mSynchronizedCaptureSession.getDevice(), this.mConfiguredSurfaceMap);
                    if (build == null) {
                        Logger.d("CaptureSession", "Skipping issuing empty request for session.");
                        return -1;
                    }
                    return this.mSynchronizedCaptureSession.setSingleRepeatingRequest(build, this.createCamera2CaptureCallback(repeatingCaptureConfig.getCameraCaptureCallbacks(), this.mCaptureCallback));
                }
                catch (final CameraAccessException ex2) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Unable to access camera: ");
                    sb2.append(((Throwable)ex2).getMessage());
                    Logger.e("CaptureSession", sb2.toString());
                    Thread.dumpStack();
                    return -1;
                }
                return -1;
            }
            finally {
                monitorexit(mSessionLock);
            }
        }
    }
    
    @NonNull
    @Override
    public ListenableFuture<Void> open(@NonNull final SessionConfig sessionConfig, @NonNull final CameraDevice cameraDevice, @NonNull final SynchronizedCaptureSessionOpener mSynchronizedCaptureSessionOpener) {
        synchronized (this.mSessionLock) {
            if (CaptureSession$4.$SwitchMap$androidx$camera$camera2$internal$CaptureSession$State[this.mState.ordinal()] != 2) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Open not allowed in state: ");
                sb.append(this.mState);
                Logger.e("CaptureSession", sb.toString());
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("open() should not allow the state: ");
                sb2.append(this.mState);
                return (ListenableFuture<Void>)Futures.immediateFailedFuture(new IllegalStateException(sb2.toString()));
            }
            this.mState = State.GET_SURFACE;
            final ArrayList<DeferrableSurface> mConfiguredDeferrableSurfaces = new ArrayList<DeferrableSurface>(sessionConfig.getSurfaces());
            this.mConfiguredDeferrableSurfaces = mConfiguredDeferrableSurfaces;
            this.mSynchronizedCaptureSessionOpener = mSynchronizedCaptureSessionOpener;
            final FutureChain<Object> transformAsync = FutureChain.from(mSynchronizedCaptureSessionOpener.startWithDeferrableSurface(mConfiguredDeferrableSurfaces, 5000L)).transformAsync((AsyncFunction<? super List<Surface>, Object>)new Ooo(this, sessionConfig, cameraDevice), this.mSynchronizedCaptureSessionOpener.getExecutor());
            Futures.addCallback((com.google.common.util.concurrent.ListenableFuture<Object>)transformAsync, (FutureCallback<? super Object>)new FutureCallback<Void>(this) {
                final CaptureSession this$0;
                
                @Override
                public void onFailure(@NonNull final Throwable t) {
                    synchronized (this.this$0.mSessionLock) {
                        this.this$0.mSynchronizedCaptureSessionOpener.stop();
                        final int n = CaptureSession$4.$SwitchMap$androidx$camera$camera2$internal$CaptureSession$State[this.this$0.mState.ordinal()];
                        if (n == 4 || n == 6 || n == 7) {
                            if (!(t instanceof CancellationException)) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Opening session with fail ");
                                sb.append(this.this$0.mState);
                                Logger.w("CaptureSession", sb.toString(), t);
                                this.this$0.finishClose();
                            }
                        }
                    }
                }
                
                @Override
                public void onSuccess(@Nullable final Void void1) {
                }
            }, this.mSynchronizedCaptureSessionOpener.getExecutor());
            return Futures.nonCancellationPropagating((com.google.common.util.concurrent.ListenableFuture<Void>)transformAsync);
        }
    }
    
    @NonNull
    @Override
    public ListenableFuture<Void> release(final boolean b) {
        synchronized (this.mSessionLock) {
            switch (CaptureSession$4.$SwitchMap$androidx$camera$camera2$internal$CaptureSession$State[this.mState.ordinal()]) {
                case 5:
                case 6: {
                    final SynchronizedCaptureSession mSynchronizedCaptureSession = this.mSynchronizedCaptureSession;
                    if (mSynchronizedCaptureSession != null) {
                        if (b) {
                            try {
                                mSynchronizedCaptureSession.abortCaptures();
                            }
                            catch (final CameraAccessException ex) {
                                Logger.e("CaptureSession", "Unable to abort captures.", (Throwable)ex);
                            }
                        }
                        this.mSynchronizedCaptureSession.close();
                    }
                }
                case 4: {
                    this.mCameraEventCallbacks.createComboCallback().onDeInitSession();
                    this.mState = State.RELEASING;
                    final SynchronizedCaptureSessionOpener mSynchronizedCaptureSessionOpener = this.mSynchronizedCaptureSessionOpener;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("The Opener shouldn't null in state:");
                    sb.append(this.mState);
                    Preconditions.checkNotNull(mSynchronizedCaptureSessionOpener, sb.toString());
                    if (this.mSynchronizedCaptureSessionOpener.stop()) {
                        this.finishClose();
                        break;
                    }
                }
                case 7: {
                    if (this.mReleaseFuture == null) {
                        this.mReleaseFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new \u3007O\u300780o08O(this));
                    }
                    return this.mReleaseFuture;
                }
                case 3: {
                    final SynchronizedCaptureSessionOpener mSynchronizedCaptureSessionOpener2 = this.mSynchronizedCaptureSessionOpener;
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("The Opener shouldn't null in state:");
                    sb2.append(this.mState);
                    Preconditions.checkNotNull(mSynchronizedCaptureSessionOpener2, sb2.toString());
                    this.mSynchronizedCaptureSessionOpener.stop();
                }
                case 2: {
                    this.mState = State.RELEASED;
                    break;
                }
                case 1: {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("release() should not be possible in state: ");
                    sb3.append(this.mState);
                    throw new IllegalStateException(sb3.toString());
                }
            }
            monitorexit(this.mSessionLock);
            return Futures.immediateFuture((Void)null);
        }
    }
    
    @Override
    public void setSessionConfig(@Nullable final SessionConfig sessionConfig) {
        synchronized (this.mSessionLock) {
            switch (CaptureSession$4.$SwitchMap$androidx$camera$camera2$internal$CaptureSession$State[this.mState.ordinal()]) {
                case 6:
                case 7:
                case 8: {
                    throw new IllegalStateException("Session configuration cannot be set on a closed/released session.");
                }
                case 5: {
                    this.mSessionConfig = sessionConfig;
                    if (sessionConfig == null) {
                        return;
                    }
                    if (!this.mConfiguredSurfaceMap.keySet().containsAll(sessionConfig.getSurfaces())) {
                        Logger.e("CaptureSession", "Does not have the proper configured lists");
                        return;
                    }
                    Logger.d("CaptureSession", "Attempting to submit CaptureRequest after setting");
                    this.issueRepeatingCaptureRequests(this.mSessionConfig);
                    break;
                }
                case 2:
                case 3:
                case 4: {
                    this.mSessionConfig = sessionConfig;
                    break;
                }
                case 1: {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("setSessionConfig() should not be possible in state: ");
                    sb.append(this.mState);
                    throw new IllegalStateException(sb.toString());
                }
            }
        }
    }
    
    @GuardedBy("mSessionLock")
    List<CaptureConfig> setupConfiguredSurface(final List<CaptureConfig> list) {
        final ArrayList list2 = new ArrayList();
        final Iterator<CaptureConfig> iterator = list.iterator();
        while (iterator.hasNext()) {
            final CaptureConfig.Builder from = CaptureConfig.Builder.from(iterator.next());
            from.setTemplateType(1);
            final Iterator<DeferrableSurface> iterator2 = this.mSessionConfig.getRepeatingCaptureConfig().getSurfaces().iterator();
            while (iterator2.hasNext()) {
                from.addSurface(iterator2.next());
            }
            list2.add(from.build());
        }
        return list2;
    }
    
    void stopRepeating() {
        synchronized (this.mSessionLock) {
            if (this.mState != State.OPENED) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to stop repeating. Incorrect state:");
                sb.append(this.mState);
                Logger.e("CaptureSession", sb.toString());
                return;
            }
            try {
                this.mSynchronizedCaptureSession.stopRepeating();
            }
            catch (final CameraAccessException ex) {
                Logger.e("CaptureSession", "Unable to stop repeating.", (Throwable)ex);
            }
        }
    }
    
    enum State
    {
        private static final State[] $VALUES;
        
        CLOSED, 
        GET_SURFACE, 
        INITIALIZED, 
        OPENED, 
        OPENING, 
        RELEASED, 
        RELEASING, 
        UNINITIALIZED;
    }
    
    final class StateCallback extends SynchronizedCaptureSession.StateCallback
    {
        final CaptureSession this$0;
        
        StateCallback(final CaptureSession this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public void onConfigureFailed(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
            synchronized (this.this$0.mSessionLock) {
                switch (CaptureSession$4.$SwitchMap$androidx$camera$camera2$internal$CaptureSession$State[this.this$0.mState.ordinal()]) {
                    case 8: {
                        Logger.d("CaptureSession", "ConfigureFailed callback after change to RELEASED state");
                        break;
                    }
                    case 4:
                    case 6:
                    case 7: {
                        this.this$0.finishClose();
                        break;
                    }
                    case 1:
                    case 2:
                    case 3:
                    case 5: {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("onConfigureFailed() should not be possible in state: ");
                        sb.append(this.this$0.mState);
                        throw new IllegalStateException(sb.toString());
                    }
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("CameraCaptureSession.onConfigureFailed() ");
                sb2.append(this.this$0.mState);
                Logger.e("CaptureSession", sb2.toString());
            }
        }
        
        public void onConfigured(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
            synchronized (this.this$0.mSessionLock) {
                switch (CaptureSession$4.$SwitchMap$androidx$camera$camera2$internal$CaptureSession$State[this.this$0.mState.ordinal()]) {
                    case 7: {
                        synchronizedCaptureSession.close();
                        break;
                    }
                    case 6: {
                        this.this$0.mSynchronizedCaptureSession = synchronizedCaptureSession;
                        break;
                    }
                    case 4: {
                        final CaptureSession this$0 = this.this$0;
                        this$0.mState = State.OPENED;
                        this$0.mSynchronizedCaptureSession = synchronizedCaptureSession;
                        if (this$0.mSessionConfig != null) {
                            final List<CaptureConfig> onEnableSession = this$0.mCameraEventCallbacks.createComboCallback().onEnableSession();
                            if (!onEnableSession.isEmpty()) {
                                final CaptureSession this$2 = this.this$0;
                                this$2.issueBurstCaptureRequest(this$2.setupConfiguredSurface(onEnableSession));
                            }
                        }
                        Logger.d("CaptureSession", "Attempting to send capture request onConfigured");
                        final CaptureSession this$3 = this.this$0;
                        this$3.issueRepeatingCaptureRequests(this$3.mSessionConfig);
                        this.this$0.issuePendingCaptureRequest();
                        break;
                    }
                    case 1:
                    case 2:
                    case 3:
                    case 5:
                    case 8: {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("onConfigured() should not be possible in state: ");
                        sb.append(this.this$0.mState);
                        throw new IllegalStateException(sb.toString());
                    }
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("CameraCaptureSession.onConfigured() mState=");
                sb2.append(this.this$0.mState);
                Logger.d("CaptureSession", sb2.toString());
            }
        }
        
        public void onReady(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
            synchronized (this.this$0.mSessionLock) {
                if (CaptureSession$4.$SwitchMap$androidx$camera$camera2$internal$CaptureSession$State[this.this$0.mState.ordinal()] != 1) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("CameraCaptureSession.onReady() ");
                    sb.append(this.this$0.mState);
                    Logger.d("CaptureSession", sb.toString());
                    return;
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("onReady() should not be possible in state: ");
                sb2.append(this.this$0.mState);
                throw new IllegalStateException(sb2.toString());
            }
        }
        
        public void onSessionFinished(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
            synchronized (this.this$0.mSessionLock) {
                if (this.this$0.mState != State.UNINITIALIZED) {
                    Logger.d("CaptureSession", "onSessionFinished()");
                    this.this$0.finishClose();
                    return;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("onSessionFinished() should not be possible in state: ");
                sb.append(this.this$0.mState);
                throw new IllegalStateException(sb.toString());
            }
        }
    }
}

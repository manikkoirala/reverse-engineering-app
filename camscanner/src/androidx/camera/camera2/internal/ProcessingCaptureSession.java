// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import java.util.concurrent.Future;
import androidx.arch.core.util.Function;
import androidx.camera.core.impl.utils.futures.AsyncFunction;
import androidx.camera.core.impl.utils.futures.FutureChain;
import java.util.Collection;
import androidx.camera.core.impl.RequestProcessor;
import android.hardware.camera2.CaptureResult$Key;
import java.util.Map;
import androidx.camera.core.impl.CameraCaptureResult;
import androidx.camera.core.impl.CameraCaptureFailure;
import android.hardware.camera2.CaptureRequest$Key;
import android.hardware.camera2.CaptureRequest;
import java.util.Collections;
import java.util.Arrays;
import androidx.camera.core.impl.Config;
import androidx.camera.camera2.impl.Camera2ImplConfig;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.CameraInfo;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.impl.OutputSurface;
import android.util.Size;
import android.view.Surface;
import java.util.Objects;
import androidx.camera.core.Preview;
import androidx.camera.core.impl.utils.futures.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import android.hardware.camera2.CameraDevice;
import androidx.camera.core.impl.DeferrableSurfaces;
import androidx.core.util.Preconditions;
import androidx.camera.core.impl.SessionProcessorSurface;
import java.util.Iterator;
import androidx.camera.core.impl.CameraCaptureCallback;
import androidx.camera.core.Logger;
import androidx.annotation.NonNull;
import java.util.ArrayList;
import androidx.camera.core.impl.SessionProcessor;
import androidx.camera.camera2.interop.CaptureRequestOptions;
import java.util.concurrent.ScheduledExecutorService;
import androidx.camera.core.impl.SessionConfig;
import androidx.annotation.Nullable;
import androidx.camera.core.impl.CaptureConfig;
import java.util.concurrent.Executor;
import androidx.camera.core.impl.DeferrableSurface;
import java.util.List;
import androidx.annotation.RequiresApi;
import androidx.camera.camera2.interop.ExperimentalCamera2Interop;
import androidx.annotation.OptIn;

@OptIn(markerClass = { ExperimentalCamera2Interop.class })
@RequiresApi(21)
final class ProcessingCaptureSession implements CaptureSessionInterface
{
    private static final String TAG = "ProcessingCaptureSession";
    private static final long TIMEOUT_GET_SURFACE_IN_MS = 5000L;
    private static List<DeferrableSurface> sHeldProcessorSurfaces;
    private static int sNextInstanceId;
    private final Camera2CameraInfoImpl mCamera2CameraInfoImpl;
    private final CaptureSession mCaptureSession;
    final Executor mExecutor;
    private int mInstanceId;
    volatile boolean mIsExecutingStillCaptureRequest;
    private List<DeferrableSurface> mOutputSurfaces;
    @Nullable
    private volatile CaptureConfig mPendingCaptureConfig;
    @Nullable
    private SessionConfig mProcessorSessionConfig;
    private ProcessorState mProcessorState;
    @Nullable
    private Camera2RequestProcessor mRequestProcessor;
    private final ScheduledExecutorService mScheduledExecutorService;
    @Nullable
    private SessionConfig mSessionConfig;
    private CaptureRequestOptions mSessionOptions;
    private final SessionProcessor mSessionProcessor;
    private final SessionProcessorCaptureCallback mSessionProcessorCaptureCallback;
    private CaptureRequestOptions mStillCaptureOptions;
    
    static {
        ProcessingCaptureSession.sHeldProcessorSurfaces = new ArrayList<DeferrableSurface>();
        ProcessingCaptureSession.sNextInstanceId = 0;
    }
    
    ProcessingCaptureSession(@NonNull final SessionProcessor mSessionProcessor, @NonNull final Camera2CameraInfoImpl mCamera2CameraInfoImpl, @NonNull final Executor mExecutor, @NonNull final ScheduledExecutorService mScheduledExecutorService) {
        this.mOutputSurfaces = new ArrayList<DeferrableSurface>();
        this.mPendingCaptureConfig = null;
        this.mIsExecutingStillCaptureRequest = false;
        this.mSessionOptions = new CaptureRequestOptions.Builder().build();
        this.mStillCaptureOptions = new CaptureRequestOptions.Builder().build();
        this.mInstanceId = 0;
        this.mCaptureSession = new CaptureSession();
        this.mSessionProcessor = mSessionProcessor;
        this.mCamera2CameraInfoImpl = mCamera2CameraInfoImpl;
        this.mExecutor = mExecutor;
        this.mScheduledExecutorService = mScheduledExecutorService;
        this.mProcessorState = ProcessorState.UNINITIALIZED;
        this.mSessionProcessorCaptureCallback = new SessionProcessorCaptureCallback();
        final int sNextInstanceId = ProcessingCaptureSession.sNextInstanceId;
        ProcessingCaptureSession.sNextInstanceId = sNextInstanceId + 1;
        this.mInstanceId = sNextInstanceId;
        final StringBuilder sb = new StringBuilder();
        sb.append("New ProcessingCaptureSession (id=");
        sb.append(this.mInstanceId);
        sb.append(")");
        Logger.d("ProcessingCaptureSession", sb.toString());
    }
    
    private static void cancelRequests(@NonNull final List<CaptureConfig> list) {
        final Iterator<CaptureConfig> iterator = list.iterator();
        while (iterator.hasNext()) {
            final Iterator<CameraCaptureCallback> iterator2 = iterator.next().getCameraCaptureCallbacks().iterator();
            while (iterator2.hasNext()) {
                iterator2.next().onCaptureCancelled();
            }
        }
    }
    
    private static List<SessionProcessorSurface> getSessionProcessorSurfaceList(final List<DeferrableSurface> list) {
        final ArrayList list2 = new ArrayList();
        for (final DeferrableSurface deferrableSurface : list) {
            Preconditions.checkArgument(deferrableSurface instanceof SessionProcessorSurface, (Object)"Surface must be SessionProcessorSurface");
            list2.add(deferrableSurface);
        }
        return list2;
    }
    
    private boolean isStillCapture(@NonNull final List<CaptureConfig> list) {
        if (list.isEmpty()) {
            return false;
        }
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            if (((CaptureConfig)iterator.next()).getTemplateType() != 2) {
                return false;
            }
        }
        return true;
    }
    
    private void updateParameters(@NonNull final CaptureRequestOptions captureRequestOptions, @NonNull final CaptureRequestOptions captureRequestOptions2) {
        final Camera2ImplConfig.Builder builder = new Camera2ImplConfig.Builder();
        builder.insertAllOptions(captureRequestOptions);
        builder.insertAllOptions(captureRequestOptions2);
        this.mSessionProcessor.setParameters(builder.build());
    }
    
    @Override
    public void cancelIssuedCaptureRequests() {
        final StringBuilder sb = new StringBuilder();
        sb.append("cancelIssuedCaptureRequests (id=");
        sb.append(this.mInstanceId);
        sb.append(")");
        Logger.d("ProcessingCaptureSession", sb.toString());
        if (this.mPendingCaptureConfig != null) {
            final Iterator<CameraCaptureCallback> iterator = this.mPendingCaptureConfig.getCameraCaptureCallbacks().iterator();
            while (iterator.hasNext()) {
                iterator.next().onCaptureCancelled();
            }
            this.mPendingCaptureConfig = null;
        }
    }
    
    @Override
    public void close() {
        final StringBuilder sb = new StringBuilder();
        sb.append("close (id=");
        sb.append(this.mInstanceId);
        sb.append(") state=");
        sb.append(this.mProcessorState);
        Logger.d("ProcessingCaptureSession", sb.toString());
        final int n = ProcessingCaptureSession$3.$SwitchMap$androidx$camera$camera2$internal$ProcessingCaptureSession$ProcessorState[this.mProcessorState.ordinal()];
        Label_0125: {
            if (n != 2) {
                if (n != 3) {
                    if (n != 4) {
                        if (n != 5) {
                            break Label_0125;
                        }
                        return;
                    }
                }
                else {
                    this.mSessionProcessor.onCaptureSessionEnd();
                    final Camera2RequestProcessor mRequestProcessor = this.mRequestProcessor;
                    if (mRequestProcessor != null) {
                        mRequestProcessor.close();
                    }
                    this.mProcessorState = ProcessorState.ON_CAPTURE_SESSION_ENDED;
                }
            }
            this.mSessionProcessor.deInitSession();
        }
        this.mProcessorState = ProcessorState.CLOSED;
        this.mCaptureSession.close();
    }
    
    @NonNull
    @Override
    public List<CaptureConfig> getCaptureConfigs() {
        Object o;
        if (this.mPendingCaptureConfig != null) {
            o = Arrays.asList(this.mPendingCaptureConfig);
        }
        else {
            o = Collections.emptyList();
        }
        return (List<CaptureConfig>)o;
    }
    
    @Nullable
    @Override
    public SessionConfig getSessionConfig() {
        return this.mSessionConfig;
    }
    
    @Override
    public void issueCaptureRequests(@NonNull final List<CaptureConfig> list) {
        if (list.isEmpty()) {
            return;
        }
        if (list.size() > 1 || !this.isStillCapture(list)) {
            cancelRequests(list);
            return;
        }
        if (this.mPendingCaptureConfig == null && !this.mIsExecutingStillCaptureRequest) {
            final CaptureConfig mPendingCaptureConfig = list.get(0);
            final StringBuilder sb = new StringBuilder();
            sb.append("issueCaptureRequests (id=");
            sb.append(this.mInstanceId);
            sb.append(") + state =");
            sb.append(this.mProcessorState);
            Logger.d("ProcessingCaptureSession", sb.toString());
            final int n = ProcessingCaptureSession$3.$SwitchMap$androidx$camera$camera2$internal$ProcessingCaptureSession$ProcessorState[this.mProcessorState.ordinal()];
            if (n != 1 && n != 2) {
                if (n != 3) {
                    if (n == 4 || n == 5) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Run issueCaptureRequests in wrong state, state = ");
                        sb2.append(this.mProcessorState);
                        Logger.d("ProcessingCaptureSession", sb2.toString());
                        cancelRequests(list);
                    }
                }
                else {
                    this.mIsExecutingStillCaptureRequest = true;
                    final CaptureRequestOptions.Builder from = CaptureRequestOptions.Builder.from(mPendingCaptureConfig.getImplementationOptions());
                    final Config implementationOptions = mPendingCaptureConfig.getImplementationOptions();
                    final Config.Option<Integer> option_ROTATION = CaptureConfig.OPTION_ROTATION;
                    if (implementationOptions.containsOption((Config.Option<?>)option_ROTATION)) {
                        from.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Object>)CaptureRequest.JPEG_ORIENTATION, mPendingCaptureConfig.getImplementationOptions().retrieveOption((Config.Option<ValueT>)option_ROTATION));
                    }
                    final Config implementationOptions2 = mPendingCaptureConfig.getImplementationOptions();
                    final Config.Option<Integer> option_JPEG_QUALITY = CaptureConfig.OPTION_JPEG_QUALITY;
                    if (implementationOptions2.containsOption((Config.Option<?>)option_JPEG_QUALITY)) {
                        from.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Byte>)CaptureRequest.JPEG_QUALITY, mPendingCaptureConfig.getImplementationOptions().retrieveOption(option_JPEG_QUALITY).byteValue());
                    }
                    final CaptureRequestOptions build = from.build();
                    this.mStillCaptureOptions = build;
                    this.updateParameters(this.mSessionOptions, build);
                    this.mSessionProcessor.startCapture((SessionProcessor.CaptureCallback)new SessionProcessor.CaptureCallback(this, mPendingCaptureConfig) {
                        final ProcessingCaptureSession this$0;
                        final CaptureConfig val$captureConfig;
                        
                        @Override
                        public void onCaptureCompleted(final long n, final int n2, @NonNull final Map<CaptureResult$Key, Object> map) {
                        }
                        
                        @Override
                        public void onCaptureFailed(final int n) {
                            this.this$0.mExecutor.execute(new O880oOO08(this, this.val$captureConfig));
                        }
                        
                        @Override
                        public void onCaptureProcessStarted(final int n) {
                        }
                        
                        @Override
                        public void onCaptureSequenceAborted(final int n) {
                        }
                        
                        @Override
                        public void onCaptureSequenceCompleted(final int n) {
                            this.this$0.mExecutor.execute(new OOo8o\u3007O(this, this.val$captureConfig));
                        }
                        
                        @Override
                        public void onCaptureStarted(final int n, final long n2) {
                        }
                    });
                }
            }
            else {
                this.mPendingCaptureConfig = mPendingCaptureConfig;
            }
            return;
        }
        cancelRequests(list);
    }
    
    void onConfigured(@NonNull final CaptureSession captureSession) {
        final boolean b = this.mProcessorState == ProcessorState.SESSION_INITIALIZED;
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid state state:");
        sb.append(this.mProcessorState);
        Preconditions.checkArgument(b, (Object)sb.toString());
        final Camera2RequestProcessor mRequestProcessor = new Camera2RequestProcessor(captureSession, getSessionProcessorSurfaceList(this.mProcessorSessionConfig.getSurfaces()));
        this.mRequestProcessor = mRequestProcessor;
        this.mSessionProcessor.onCaptureSessionStart(mRequestProcessor);
        this.mProcessorState = ProcessorState.ON_CAPTURE_SESSION_STARTED;
        final SessionConfig mSessionConfig = this.mSessionConfig;
        if (mSessionConfig != null) {
            this.setSessionConfig(mSessionConfig);
        }
        if (this.mPendingCaptureConfig != null) {
            final List<CaptureConfig> list = Arrays.asList(this.mPendingCaptureConfig);
            this.mPendingCaptureConfig = null;
            this.issueCaptureRequests(list);
        }
    }
    
    @NonNull
    @Override
    public ListenableFuture<Void> open(@NonNull final SessionConfig sessionConfig, @NonNull final CameraDevice cameraDevice, @NonNull final SynchronizedCaptureSessionOpener synchronizedCaptureSessionOpener) {
        final boolean b = this.mProcessorState == ProcessorState.UNINITIALIZED;
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid state state:");
        sb.append(this.mProcessorState);
        Preconditions.checkArgument(b, (Object)sb.toString());
        Preconditions.checkArgument(sessionConfig.getSurfaces().isEmpty() ^ true, (Object)"SessionConfig contains no surfaces");
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("open (id=");
        sb2.append(this.mInstanceId);
        sb2.append(")");
        Logger.d("ProcessingCaptureSession", sb2.toString());
        final List<DeferrableSurface> surfaces = sessionConfig.getSurfaces();
        this.mOutputSurfaces = surfaces;
        return (ListenableFuture<Void>)FutureChain.from(DeferrableSurfaces.surfaceListWithTimeout(surfaces, false, 5000L, this.mExecutor, this.mScheduledExecutorService)).transformAsync((AsyncFunction<? super List<Surface>, Object>)new \u3007000O0(this, sessionConfig, cameraDevice, synchronizedCaptureSessionOpener), this.mExecutor).transform((Function<? super Object, Object>)new ooo0\u3007O88O(this), this.mExecutor);
    }
    
    @NonNull
    @Override
    public ListenableFuture<Void> release(final boolean b) {
        Preconditions.checkState(this.mProcessorState == ProcessorState.CLOSED, "release() can only be called in CLOSED state");
        final StringBuilder sb = new StringBuilder();
        sb.append("release (id=");
        sb.append(this.mInstanceId);
        sb.append(")");
        Logger.d("ProcessingCaptureSession", sb.toString());
        return this.mCaptureSession.release(b);
    }
    
    @Override
    public void setSessionConfig(@Nullable final SessionConfig mSessionConfig) {
        final StringBuilder sb = new StringBuilder();
        sb.append("setSessionConfig (id=");
        sb.append(this.mInstanceId);
        sb.append(")");
        Logger.d("ProcessingCaptureSession", sb.toString());
        this.mSessionConfig = mSessionConfig;
        if (mSessionConfig == null) {
            return;
        }
        final Camera2RequestProcessor mRequestProcessor = this.mRequestProcessor;
        if (mRequestProcessor != null) {
            mRequestProcessor.updateSessionConfig(mSessionConfig);
        }
        if (this.mProcessorState == ProcessorState.ON_CAPTURE_SESSION_STARTED) {
            this.updateParameters(this.mSessionOptions = CaptureRequestOptions.Builder.from(mSessionConfig.getImplementationOptions()).build(), this.mStillCaptureOptions);
            this.mSessionProcessor.startRepeating((SessionProcessor.CaptureCallback)this.mSessionProcessorCaptureCallback);
        }
    }
    
    private enum ProcessorState
    {
        private static final ProcessorState[] $VALUES;
        
        CLOSED, 
        ON_CAPTURE_SESSION_ENDED, 
        ON_CAPTURE_SESSION_STARTED, 
        SESSION_INITIALIZED, 
        UNINITIALIZED;
    }
    
    private static class SessionProcessorCaptureCallback implements CaptureCallback
    {
        SessionProcessorCaptureCallback() {
        }
        
        @Override
        public void onCaptureCompleted(final long n, final int n2, @NonNull final Map<CaptureResult$Key, Object> map) {
        }
        
        @Override
        public void onCaptureFailed(final int n) {
        }
        
        @Override
        public void onCaptureProcessStarted(final int n) {
        }
        
        @Override
        public void onCaptureSequenceAborted(final int n) {
        }
        
        @Override
        public void onCaptureSequenceCompleted(final int n) {
        }
        
        @Override
        public void onCaptureStarted(final int n, final long n2) {
        }
    }
}

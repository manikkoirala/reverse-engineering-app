// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.annotation.DoNotInline;
import java.util.ArrayList;
import android.hardware.camera2.CameraAccessException;
import java.util.Iterator;
import androidx.camera.core.impl.CameraCaptureResult;
import java.util.List;
import android.hardware.camera2.TotalCaptureResult;
import android.os.Build$VERSION;
import android.hardware.camera2.CaptureRequest;
import android.view.Surface;
import androidx.camera.core.impl.DeferrableSurface;
import java.util.Map;
import androidx.annotation.Nullable;
import android.hardware.camera2.CameraDevice;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.CaptureConfig;
import androidx.camera.camera2.interop.ExperimentalCamera2Interop;
import androidx.annotation.OptIn;
import androidx.camera.core.Logger;
import android.hardware.camera2.CaptureRequest$Key;
import androidx.camera.camera2.interop.CaptureRequestOptions;
import androidx.camera.core.impl.Config;
import android.hardware.camera2.CaptureRequest$Builder;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
class Camera2CaptureRequestBuilder
{
    private static final String TAG = "Camera2CaptureRequestBuilder";
    
    private Camera2CaptureRequestBuilder() {
    }
    
    @OptIn(markerClass = { ExperimentalCamera2Interop.class })
    private static void applyImplementationOptionToCaptureBuilder(final CaptureRequest$Builder captureRequest$Builder, Config iterator) {
        final CaptureRequestOptions build = CaptureRequestOptions.Builder.from(iterator).build();
        iterator = (Config)build.listOptions().iterator();
        while (((Iterator)iterator).hasNext()) {
            final Config.Option option = (Config.Option)((Iterator<Config.Option>)iterator).next();
            final CaptureRequest$Key obj = (CaptureRequest$Key)option.getToken();
            try {
                captureRequest$Builder.set(obj, build.retrieveOption(option));
            }
            catch (final IllegalArgumentException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("CaptureRequest.Key is not supported: ");
                sb.append(obj);
                Logger.e("Camera2CaptureRequestBuilder", sb.toString());
            }
        }
    }
    
    @Nullable
    public static CaptureRequest build(@NonNull final CaptureConfig captureConfig, @Nullable final CameraDevice cameraDevice, @NonNull final Map<DeferrableSurface, Surface> map) throws CameraAccessException {
        if (cameraDevice == null) {
            return null;
        }
        final List<Surface> configuredSurfaces = getConfiguredSurfaces(captureConfig.getSurfaces(), map);
        if (configuredSurfaces.isEmpty()) {
            return null;
        }
        final CameraCaptureResult cameraCaptureResult = captureConfig.getCameraCaptureResult();
        CaptureRequest$Builder captureRequest$Builder;
        if (Build$VERSION.SDK_INT >= 23 && captureConfig.getTemplateType() == 5 && cameraCaptureResult != null && cameraCaptureResult.getCaptureResult() instanceof TotalCaptureResult) {
            Logger.d("Camera2CaptureRequestBuilder", "createReprocessCaptureRequest");
            captureRequest$Builder = Api23Impl.createReprocessCaptureRequest(cameraDevice, (TotalCaptureResult)cameraCaptureResult.getCaptureResult());
        }
        else {
            Logger.d("Camera2CaptureRequestBuilder", "createCaptureRequest");
            captureRequest$Builder = cameraDevice.createCaptureRequest(captureConfig.getTemplateType());
        }
        applyImplementationOptionToCaptureBuilder(captureRequest$Builder, captureConfig.getImplementationOptions());
        final Config implementationOptions = captureConfig.getImplementationOptions();
        final Config.Option<Integer> option_ROTATION = CaptureConfig.OPTION_ROTATION;
        if (implementationOptions.containsOption((Config.Option<?>)option_ROTATION)) {
            captureRequest$Builder.set(CaptureRequest.JPEG_ORIENTATION, (Object)captureConfig.getImplementationOptions().retrieveOption(option_ROTATION));
        }
        final Config implementationOptions2 = captureConfig.getImplementationOptions();
        final Config.Option<Integer> option_JPEG_QUALITY = CaptureConfig.OPTION_JPEG_QUALITY;
        if (implementationOptions2.containsOption((Config.Option<?>)option_JPEG_QUALITY)) {
            captureRequest$Builder.set(CaptureRequest.JPEG_QUALITY, (Object)captureConfig.getImplementationOptions().retrieveOption(option_JPEG_QUALITY).byteValue());
        }
        final Iterator<Surface> iterator = configuredSurfaces.iterator();
        while (iterator.hasNext()) {
            captureRequest$Builder.addTarget((Surface)iterator.next());
        }
        captureRequest$Builder.setTag((Object)captureConfig.getTagBundle());
        return captureRequest$Builder.build();
    }
    
    @Nullable
    public static CaptureRequest buildWithoutTarget(@NonNull final CaptureConfig captureConfig, @Nullable final CameraDevice cameraDevice) throws CameraAccessException {
        if (cameraDevice == null) {
            return null;
        }
        final CaptureRequest$Builder captureRequest = cameraDevice.createCaptureRequest(captureConfig.getTemplateType());
        applyImplementationOptionToCaptureBuilder(captureRequest, captureConfig.getImplementationOptions());
        return captureRequest.build();
    }
    
    @NonNull
    private static List<Surface> getConfiguredSurfaces(final List<DeferrableSurface> list, final Map<DeferrableSurface, Surface> map) {
        final ArrayList list2 = new ArrayList();
        final Iterator<DeferrableSurface> iterator = list.iterator();
        while (iterator.hasNext()) {
            final Surface surface = map.get(iterator.next());
            if (surface == null) {
                throw new IllegalArgumentException("DeferrableSurface not in configuredSurfaceMap");
            }
            list2.add(surface);
        }
        return list2;
    }
    
    @RequiresApi(23)
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        @DoNotInline
        static CaptureRequest$Builder createReprocessCaptureRequest(@NonNull final CameraDevice cameraDevice, @NonNull final TotalCaptureResult totalCaptureResult) throws CameraAccessException {
            return O000.\u3007080(cameraDevice, totalCaptureResult);
        }
    }
}

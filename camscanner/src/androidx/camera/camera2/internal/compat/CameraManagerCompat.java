// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraManager$AvailabilityCallback;
import androidx.annotation.RequiresPermission;
import android.hardware.camera2.CameraDevice$StateCallback;
import java.util.concurrent.Executor;
import androidx.annotation.RestrictTo;
import android.os.Handler;
import androidx.camera.core.impl.utils.MainThreadAsyncHandler;
import androidx.annotation.NonNull;
import android.content.Context;
import android.util.ArrayMap;
import androidx.annotation.GuardedBy;
import java.util.Map;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class CameraManagerCompat
{
    @GuardedBy("mCameraCharacteristicsMap")
    private final Map<String, CameraCharacteristicsCompat> mCameraCharacteristicsMap;
    private final CameraManagerCompatImpl mImpl;
    
    private CameraManagerCompat(final CameraManagerCompatImpl mImpl) {
        this.mCameraCharacteristicsMap = (Map<String, CameraCharacteristicsCompat>)new ArrayMap(4);
        this.mImpl = mImpl;
    }
    
    @NonNull
    public static CameraManagerCompat from(@NonNull final Context context) {
        return from(context, MainThreadAsyncHandler.getInstance());
    }
    
    @NonNull
    public static CameraManagerCompat from(@NonNull final Context context, @NonNull final Handler handler) {
        return new CameraManagerCompat(o0ooO.\u3007080(context, handler));
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.TESTS })
    public static CameraManagerCompat from(@NonNull final CameraManagerCompatImpl cameraManagerCompatImpl) {
        return new CameraManagerCompat(cameraManagerCompatImpl);
    }
    
    @NonNull
    public CameraCharacteristicsCompat getCameraCharacteristicsCompat(@NonNull final String s) throws CameraAccessExceptionCompat {
        synchronized (this.mCameraCharacteristicsMap) {
            CameraCharacteristicsCompat cameraCharacteristicsCompat;
            if ((cameraCharacteristicsCompat = this.mCameraCharacteristicsMap.get(s)) == null) {
                try {
                    cameraCharacteristicsCompat = CameraCharacteristicsCompat.toCameraCharacteristicsCompat(this.mImpl.getCameraCharacteristics(s));
                    this.mCameraCharacteristicsMap.put(s, cameraCharacteristicsCompat);
                }
                catch (final AssertionError assertionError) {
                    throw new CameraAccessExceptionCompat(10002, assertionError.getMessage(), assertionError);
                }
            }
            return cameraCharacteristicsCompat;
        }
    }
    
    @NonNull
    public String[] getCameraIdList() throws CameraAccessExceptionCompat {
        return this.mImpl.getCameraIdList();
    }
    
    @RequiresPermission("android.permission.CAMERA")
    public void openCamera(@NonNull final String s, @NonNull final Executor executor, @NonNull final CameraDevice$StateCallback cameraDevice$StateCallback) throws CameraAccessExceptionCompat {
        this.mImpl.openCamera(s, executor, cameraDevice$StateCallback);
    }
    
    public void registerAvailabilityCallback(@NonNull final Executor executor, @NonNull final CameraManager$AvailabilityCallback cameraManager$AvailabilityCallback) {
        this.mImpl.registerAvailabilityCallback(executor, cameraManager$AvailabilityCallback);
    }
    
    public void unregisterAvailabilityCallback(@NonNull final CameraManager$AvailabilityCallback cameraManager$AvailabilityCallback) {
        this.mImpl.unregisterAvailabilityCallback(cameraManager$AvailabilityCallback);
    }
    
    @NonNull
    public CameraManager unwrap() {
        return this.mImpl.getCameraManager();
    }
    
    @RequiresApi(21)
    static final class AvailabilityCallbackExecutorWrapper extends CameraManager$AvailabilityCallback
    {
        @GuardedBy("mLock")
        private boolean mDisabled;
        private final Executor mExecutor;
        private final Object mLock;
        final CameraManager$AvailabilityCallback mWrappedCallback;
        
        AvailabilityCallbackExecutorWrapper(@NonNull final Executor mExecutor, @NonNull final CameraManager$AvailabilityCallback mWrappedCallback) {
            this.mLock = new Object();
            this.mDisabled = false;
            this.mExecutor = mExecutor;
            this.mWrappedCallback = mWrappedCallback;
        }
        
        @RequiresApi(29)
        public void onCameraAccessPrioritiesChanged() {
            synchronized (this.mLock) {
                if (!this.mDisabled) {
                    this.mExecutor.execute(new O8\u3007o(this));
                }
            }
        }
        
        public void onCameraAvailable(@NonNull final String s) {
            synchronized (this.mLock) {
                if (!this.mDisabled) {
                    this.mExecutor.execute(new \u3007o(this, s));
                }
            }
        }
        
        public void onCameraUnavailable(@NonNull final String s) {
            synchronized (this.mLock) {
                if (!this.mDisabled) {
                    this.mExecutor.execute(new \u300700\u30078(this, s));
                }
            }
        }
        
        void setDisabled() {
            synchronized (this.mLock) {
                this.mDisabled = true;
            }
        }
    }
    
    public interface CameraManagerCompatImpl
    {
        @NonNull
        CameraCharacteristics getCameraCharacteristics(@NonNull final String p0) throws CameraAccessExceptionCompat;
        
        @NonNull
        String[] getCameraIdList() throws CameraAccessExceptionCompat;
        
        @NonNull
        CameraManager getCameraManager();
        
        @RequiresPermission("android.permission.CAMERA")
        void openCamera(@NonNull final String p0, @NonNull final Executor p1, @NonNull final CameraDevice$StateCallback p2) throws CameraAccessExceptionCompat;
        
        void registerAvailabilityCallback(@NonNull final Executor p0, @NonNull final CameraManager$AvailabilityCallback p1);
        
        void unregisterAvailabilityCallback(@NonNull final CameraManager$AvailabilityCallback p0);
    }
}

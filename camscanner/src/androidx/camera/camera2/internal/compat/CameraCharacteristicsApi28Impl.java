// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import java.util.Set;
import androidx.annotation.NonNull;
import android.hardware.camera2.CameraCharacteristics;
import androidx.annotation.RequiresApi;

@RequiresApi(28)
class CameraCharacteristicsApi28Impl extends CameraCharacteristicsBaseImpl
{
    CameraCharacteristicsApi28Impl(@NonNull final CameraCharacteristics cameraCharacteristics) {
        super(cameraCharacteristics);
    }
    
    @NonNull
    @Override
    public Set<String> getPhysicalCameraIds() {
        return \u3007oo\u3007.\u3007080(super.mCameraCharacteristics);
    }
}

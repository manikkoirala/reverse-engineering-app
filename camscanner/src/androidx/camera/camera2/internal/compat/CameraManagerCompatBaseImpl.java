// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import java.util.HashMap;
import androidx.annotation.GuardedBy;
import java.util.Map;
import android.hardware.camera2.CameraManager$AvailabilityCallback;
import androidx.annotation.RequiresPermission;
import androidx.core.util.Preconditions;
import android.hardware.camera2.CameraDevice$StateCallback;
import java.util.concurrent.Executor;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.content.Context;
import android.hardware.camera2.CameraManager;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
class CameraManagerCompatBaseImpl implements CameraManagerCompatImpl
{
    final CameraManager mCameraManager;
    final Object mObject;
    
    CameraManagerCompatBaseImpl(@NonNull final Context context, @Nullable final Object mObject) {
        this.mCameraManager = (CameraManager)context.getSystemService("camera");
        this.mObject = mObject;
    }
    
    static CameraManagerCompatBaseImpl create(@NonNull final Context context, @NonNull final Handler handler) {
        return new CameraManagerCompatBaseImpl(context, new CameraManagerCompatParamsApi21(handler));
    }
    
    @NonNull
    @Override
    public CameraCharacteristics getCameraCharacteristics(@NonNull final String s) throws CameraAccessExceptionCompat {
        try {
            return this.mCameraManager.getCameraCharacteristics(s);
        }
        catch (final CameraAccessException ex) {
            throw CameraAccessExceptionCompat.toCameraAccessExceptionCompat(ex);
        }
    }
    
    @NonNull
    @Override
    public String[] getCameraIdList() throws CameraAccessExceptionCompat {
        try {
            return this.mCameraManager.getCameraIdList();
        }
        catch (final CameraAccessException ex) {
            throw CameraAccessExceptionCompat.toCameraAccessExceptionCompat(ex);
        }
    }
    
    @NonNull
    @Override
    public CameraManager getCameraManager() {
        return this.mCameraManager;
    }
    
    @RequiresPermission("android.permission.CAMERA")
    @Override
    public void openCamera(@NonNull final String s, @NonNull final Executor executor, @NonNull final CameraDevice$StateCallback cameraDevice$StateCallback) throws CameraAccessExceptionCompat {
        Preconditions.checkNotNull(executor);
        Preconditions.checkNotNull(cameraDevice$StateCallback);
        final CameraDeviceCompat.StateCallbackExecutorWrapper stateCallbackExecutorWrapper = new CameraDeviceCompat.StateCallbackExecutorWrapper(executor, cameraDevice$StateCallback);
        final CameraManagerCompatParamsApi21 cameraManagerCompatParamsApi21 = (CameraManagerCompatParamsApi21)this.mObject;
        try {
            this.mCameraManager.openCamera(s, (CameraDevice$StateCallback)stateCallbackExecutorWrapper, cameraManagerCompatParamsApi21.mCompatHandler);
        }
        catch (final CameraAccessException ex) {
            throw CameraAccessExceptionCompat.toCameraAccessExceptionCompat(ex);
        }
    }
    
    @Override
    public void registerAvailabilityCallback(@NonNull final Executor executor, @NonNull final CameraManager$AvailabilityCallback cameraManager$AvailabilityCallback) {
        if (executor != null) {
            final CameraManagerCompatParamsApi21 cameraManagerCompatParamsApi21 = (CameraManagerCompatParamsApi21)this.mObject;
            AvailabilityCallbackExecutorWrapper availabilityCallbackExecutorWrapper = null;
            Label_0088: {
                if (cameraManager$AvailabilityCallback != null) {
                    synchronized (cameraManagerCompatParamsApi21.mWrapperMap) {
                        if ((availabilityCallbackExecutorWrapper = cameraManagerCompatParamsApi21.mWrapperMap.get(cameraManager$AvailabilityCallback)) == null) {
                            availabilityCallbackExecutorWrapper = new CameraManagerCompat.AvailabilityCallbackExecutorWrapper(executor, cameraManager$AvailabilityCallback);
                            cameraManagerCompatParamsApi21.mWrapperMap.put(cameraManager$AvailabilityCallback, availabilityCallbackExecutorWrapper);
                        }
                        break Label_0088;
                    }
                }
                availabilityCallbackExecutorWrapper = null;
            }
            this.mCameraManager.registerAvailabilityCallback((CameraManager$AvailabilityCallback)availabilityCallbackExecutorWrapper, cameraManagerCompatParamsApi21.mCompatHandler);
            return;
        }
        throw new IllegalArgumentException("executor was null");
    }
    
    @Override
    public void unregisterAvailabilityCallback(@NonNull final CameraManager$AvailabilityCallback cameraManager$AvailabilityCallback) {
        final CameraManagerCompat.AvailabilityCallbackExecutorWrapper availabilityCallbackExecutorWrapper2;
        Label_0045: {
            if (cameraManager$AvailabilityCallback != null) {
                final CameraManagerCompatParamsApi21 cameraManagerCompatParamsApi21 = (CameraManagerCompatParamsApi21)this.mObject;
                synchronized (cameraManagerCompatParamsApi21.mWrapperMap) {
                    final AvailabilityCallbackExecutorWrapper availabilityCallbackExecutorWrapper = cameraManagerCompatParamsApi21.mWrapperMap.remove(cameraManager$AvailabilityCallback);
                    break Label_0045;
                }
            }
            availabilityCallbackExecutorWrapper2 = null;
        }
        if (availabilityCallbackExecutorWrapper2 != null) {
            availabilityCallbackExecutorWrapper2.setDisabled();
        }
        this.mCameraManager.unregisterAvailabilityCallback((CameraManager$AvailabilityCallback)availabilityCallbackExecutorWrapper2);
    }
    
    static final class CameraManagerCompatParamsApi21
    {
        final Handler mCompatHandler;
        @GuardedBy("mWrapperMap")
        final Map<CameraManager$AvailabilityCallback, AvailabilityCallbackExecutorWrapper> mWrapperMap;
        
        CameraManagerCompatParamsApi21(@NonNull final Handler mCompatHandler) {
            this.mWrapperMap = new HashMap<CameraManager$AvailabilityCallback, AvailabilityCallbackExecutorWrapper>();
            this.mCompatHandler = mCompatHandler;
        }
    }
}

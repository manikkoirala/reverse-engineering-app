// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import java.util.Collections;
import java.util.Set;
import androidx.annotation.Nullable;
import android.hardware.camera2.CameraCharacteristics$Key;
import androidx.annotation.NonNull;
import android.hardware.camera2.CameraCharacteristics;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
class CameraCharacteristicsBaseImpl implements CameraCharacteristicsCompatImpl
{
    @NonNull
    protected final CameraCharacteristics mCameraCharacteristics;
    
    CameraCharacteristicsBaseImpl(@NonNull final CameraCharacteristics mCameraCharacteristics) {
        this.mCameraCharacteristics = mCameraCharacteristics;
    }
    
    @Nullable
    @Override
    public <T> T get(@NonNull final CameraCharacteristics$Key<T> cameraCharacteristics$Key) {
        return (T)this.mCameraCharacteristics.get((CameraCharacteristics$Key)cameraCharacteristics$Key);
    }
    
    @NonNull
    @Override
    public Set<String> getPhysicalCameraIds() {
        return Collections.emptySet();
    }
    
    @NonNull
    @Override
    public CameraCharacteristics unwrap() {
        return this.mCameraCharacteristics;
    }
}

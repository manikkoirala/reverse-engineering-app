// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession$StateCallback;
import java.util.ArrayList;
import android.view.Surface;
import android.os.Handler;
import androidx.camera.camera2.internal.compat.params.SessionConfigurationCompat;
import java.util.Iterator;
import androidx.camera.core.Logger;
import androidx.camera.camera2.internal.compat.params.OutputConfigurationCompat;
import java.util.List;
import androidx.core.util.Preconditions;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.hardware.camera2.CameraDevice;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
class CameraDeviceCompatBaseImpl implements CameraDeviceCompatImpl
{
    final CameraDevice mCameraDevice;
    final Object mImplParams;
    
    CameraDeviceCompatBaseImpl(@NonNull final CameraDevice cameraDevice, @Nullable final Object mImplParams) {
        this.mCameraDevice = Preconditions.checkNotNull(cameraDevice);
        this.mImplParams = mImplParams;
    }
    
    private static void checkPhysicalCameraIdValid(final CameraDevice cameraDevice, @NonNull final List<OutputConfigurationCompat> list) {
        final String id = cameraDevice.getId();
        final Iterator<OutputConfigurationCompat> iterator = list.iterator();
        while (iterator.hasNext()) {
            final String physicalCameraId = iterator.next().getPhysicalCameraId();
            if (physicalCameraId != null && !physicalCameraId.isEmpty()) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Camera ");
                sb.append(id);
                sb.append(": Camera doesn't support physicalCameraId ");
                sb.append(physicalCameraId);
                sb.append(". Ignoring.");
                Logger.w("CameraDeviceCompat", sb.toString());
            }
        }
    }
    
    static void checkPreconditions(final CameraDevice cameraDevice, final SessionConfigurationCompat sessionConfigurationCompat) {
        Preconditions.checkNotNull(cameraDevice);
        Preconditions.checkNotNull(sessionConfigurationCompat);
        Preconditions.checkNotNull(sessionConfigurationCompat.getStateCallback());
        final List<OutputConfigurationCompat> outputConfigurations = sessionConfigurationCompat.getOutputConfigurations();
        if (outputConfigurations == null) {
            throw new IllegalArgumentException("Invalid output configurations");
        }
        if (sessionConfigurationCompat.getExecutor() != null) {
            checkPhysicalCameraIdValid(cameraDevice, outputConfigurations);
            return;
        }
        throw new IllegalArgumentException("Invalid executor");
    }
    
    static CameraDeviceCompatBaseImpl create(@NonNull final CameraDevice cameraDevice, @NonNull final Handler handler) {
        return new CameraDeviceCompatBaseImpl(cameraDevice, new CameraDeviceCompatParamsApi21(handler));
    }
    
    static List<Surface> unpackSurfaces(@NonNull final List<OutputConfigurationCompat> list) {
        final ArrayList list2 = new ArrayList(list.size());
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            list2.add(((OutputConfigurationCompat)iterator.next()).getSurface());
        }
        return list2;
    }
    
    void createBaseCaptureSession(@NonNull final CameraDevice cameraDevice, @NonNull final List<Surface> list, @NonNull final CameraCaptureSession$StateCallback cameraCaptureSession$StateCallback, @NonNull final Handler handler) throws CameraAccessExceptionCompat {
        try {
            cameraDevice.createCaptureSession((List)list, cameraCaptureSession$StateCallback, handler);
        }
        catch (final CameraAccessException ex) {
            throw CameraAccessExceptionCompat.toCameraAccessExceptionCompat(ex);
        }
    }
    
    @Override
    public void createCaptureSession(@NonNull final SessionConfigurationCompat sessionConfigurationCompat) throws CameraAccessExceptionCompat {
        checkPreconditions(this.mCameraDevice, sessionConfigurationCompat);
        if (sessionConfigurationCompat.getInputConfiguration() != null) {
            throw new IllegalArgumentException("Reprocessing sessions not supported until API 23");
        }
        if (sessionConfigurationCompat.getSessionType() != 1) {
            this.createBaseCaptureSession(this.mCameraDevice, unpackSurfaces(sessionConfigurationCompat.getOutputConfigurations()), new CameraCaptureSessionCompat.StateCallbackExecutorWrapper(sessionConfigurationCompat.getExecutor(), sessionConfigurationCompat.getStateCallback()), ((CameraDeviceCompatParamsApi21)this.mImplParams).mCompatHandler);
            return;
        }
        throw new IllegalArgumentException("High speed capture sessions not supported until API 23");
    }
    
    @NonNull
    @Override
    public CameraDevice unwrap() {
        return this.mCameraDevice;
    }
    
    static class CameraDeviceCompatParamsApi21
    {
        final Handler mCompatHandler;
        
        CameraDeviceCompatParamsApi21(@NonNull final Handler mCompatHandler) {
            this.mCompatHandler = mCompatHandler;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import java.util.concurrent.Executor;
import android.hardware.camera2.CameraDevice$StateCallback;
import androidx.camera.camera2.internal.compat.params.SessionConfigurationCompat;
import androidx.camera.core.impl.utils.MainThreadAsyncHandler;
import android.os.Build$VERSION;
import android.os.Handler;
import androidx.annotation.NonNull;
import android.hardware.camera2.CameraDevice;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class CameraDeviceCompat
{
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final int SESSION_OPERATION_MODE_CONSTRAINED_HIGH_SPEED = 1;
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final int SESSION_OPERATION_MODE_NORMAL = 0;
    private final CameraDeviceCompatImpl mImpl;
    
    private CameraDeviceCompat(@NonNull final CameraDevice cameraDevice, @NonNull final Handler handler) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 28) {
            this.mImpl = (CameraDeviceCompatImpl)new CameraDeviceCompatApi28Impl(cameraDevice);
        }
        else if (sdk_INT >= 24) {
            this.mImpl = (CameraDeviceCompatImpl)CameraDeviceCompatApi24Impl.create(cameraDevice, handler);
        }
        else if (sdk_INT >= 23) {
            this.mImpl = (CameraDeviceCompatImpl)CameraDeviceCompatApi23Impl.create(cameraDevice, handler);
        }
        else {
            this.mImpl = (CameraDeviceCompatImpl)CameraDeviceCompatBaseImpl.create(cameraDevice, handler);
        }
    }
    
    @NonNull
    public static CameraDeviceCompat toCameraDeviceCompat(@NonNull final CameraDevice cameraDevice) {
        return toCameraDeviceCompat(cameraDevice, MainThreadAsyncHandler.getInstance());
    }
    
    @NonNull
    public static CameraDeviceCompat toCameraDeviceCompat(@NonNull final CameraDevice cameraDevice, @NonNull final Handler handler) {
        return new CameraDeviceCompat(cameraDevice, handler);
    }
    
    public void createCaptureSession(@NonNull final SessionConfigurationCompat sessionConfigurationCompat) throws CameraAccessExceptionCompat {
        this.mImpl.createCaptureSession(sessionConfigurationCompat);
    }
    
    @NonNull
    public CameraDevice toCameraDevice() {
        return this.mImpl.unwrap();
    }
    
    interface CameraDeviceCompatImpl
    {
        void createCaptureSession(@NonNull final SessionConfigurationCompat p0) throws CameraAccessExceptionCompat;
        
        @NonNull
        CameraDevice unwrap();
    }
    
    @RequiresApi(21)
    static final class StateCallbackExecutorWrapper extends CameraDevice$StateCallback
    {
        private final Executor mExecutor;
        final CameraDevice$StateCallback mWrappedCallback;
        
        StateCallbackExecutorWrapper(@NonNull final Executor mExecutor, @NonNull final CameraDevice$StateCallback mWrappedCallback) {
            this.mExecutor = mExecutor;
            this.mWrappedCallback = mWrappedCallback;
        }
        
        public void onClosed(@NonNull final CameraDevice cameraDevice) {
            this.mExecutor.execute(new o\u3007O8\u3007\u3007o(this, cameraDevice));
        }
        
        public void onDisconnected(@NonNull final CameraDevice cameraDevice) {
            this.mExecutor.execute(new \u300700(this, cameraDevice));
        }
        
        public void onError(@NonNull final CameraDevice cameraDevice, final int n) {
            this.mExecutor.execute(new O8ooOoo\u3007(this, cameraDevice, n));
        }
        
        public void onOpened(@NonNull final CameraDevice cameraDevice) {
            this.mExecutor.execute(new O\u30078O8\u3007008(this, cameraDevice));
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import androidx.camera.camera2.internal.compat.params.InputConfigurationCompat;
import android.view.Surface;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession$StateCallback;
import java.util.List;
import android.hardware.camera2.params.InputConfiguration;
import androidx.core.util.Preconditions;
import androidx.camera.camera2.internal.compat.params.SessionConfigurationCompat;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.hardware.camera2.CameraDevice;
import androidx.annotation.RequiresApi;

@RequiresApi(23)
class CameraDeviceCompatApi23Impl extends CameraDeviceCompatBaseImpl
{
    CameraDeviceCompatApi23Impl(@NonNull final CameraDevice cameraDevice, @Nullable final Object o) {
        super(cameraDevice, o);
    }
    
    static CameraDeviceCompatApi23Impl create(@NonNull final CameraDevice cameraDevice, @NonNull final Handler handler) {
        return new CameraDeviceCompatApi23Impl(cameraDevice, new CameraDeviceCompatParamsApi21(handler));
    }
    
    @Override
    public void createCaptureSession(@NonNull final SessionConfigurationCompat sessionConfigurationCompat) throws CameraAccessExceptionCompat {
        CameraDeviceCompatBaseImpl.checkPreconditions(super.mCameraDevice, sessionConfigurationCompat);
        final CameraCaptureSessionCompat.StateCallbackExecutorWrapper stateCallbackExecutorWrapper = new CameraCaptureSessionCompat.StateCallbackExecutorWrapper(sessionConfigurationCompat.getExecutor(), sessionConfigurationCompat.getStateCallback());
        final List<Surface> unpackSurfaces = CameraDeviceCompatBaseImpl.unpackSurfaces(sessionConfigurationCompat.getOutputConfigurations());
        final Handler mCompatHandler = Preconditions.checkNotNull(super.mImplParams).mCompatHandler;
        final InputConfigurationCompat inputConfiguration = sessionConfigurationCompat.getInputConfiguration();
        Label_0090: {
            if (inputConfiguration == null) {
                break Label_0090;
            }
            try {
                final InputConfiguration inputConfiguration2 = (InputConfiguration)inputConfiguration.unwrap();
                Preconditions.checkNotNull(inputConfiguration2);
                \u3007oOO8O8.\u3007080(super.mCameraDevice, inputConfiguration2, (List)unpackSurfaces, (CameraCaptureSession$StateCallback)stateCallbackExecutorWrapper, mCompatHandler);
                return;
                Label_0112: {
                    this.createBaseCaptureSession(super.mCameraDevice, unpackSurfaces, stateCallbackExecutorWrapper, mCompatHandler);
                }
                return;
                while (true) {
                    \u30070000OOO.\u3007080(super.mCameraDevice, (List)unpackSurfaces, (CameraCaptureSession$StateCallback)stateCallbackExecutorWrapper, mCompatHandler);
                    return;
                    iftrue(Label_0112:)(sessionConfigurationCompat.getSessionType() != 1);
                    continue;
                }
            }
            catch (final CameraAccessException ex) {
                throw CameraAccessExceptionCompat.toCameraAccessExceptionCompat(ex);
            }
        }
    }
}

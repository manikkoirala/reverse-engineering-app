// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import androidx.annotation.NonNull;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import androidx.annotation.Nullable;
import android.util.Range;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.Quirk;

@RequiresApi(21)
public class AeFpsRangeLegacyQuirk implements Quirk
{
    @Nullable
    private final Range<Integer> mAeFpsRange;
    
    public AeFpsRangeLegacyQuirk(@NonNull final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        this.mAeFpsRange = this.pickSuitableFpsRange(cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Range<Integer>[]>)CameraCharacteristics.CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES));
    }
    
    @NonNull
    private Range<Integer> getCorrectedFpsRange(@NonNull final Range<Integer> range) {
        int intValue = (int)range.getUpper();
        int intValue2 = (int)range.getLower();
        if ((int)range.getUpper() >= 1000) {
            intValue = (int)range.getUpper() / 1000;
        }
        if ((int)range.getLower() >= 1000) {
            intValue2 = (int)range.getLower() / 1000;
        }
        return (Range<Integer>)new Range((Comparable)intValue2, (Comparable)intValue);
    }
    
    static boolean load(@NonNull final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        final Integer n = cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);
        return n != null && n == 2;
    }
    
    @Nullable
    private Range<Integer> pickSuitableFpsRange(@Nullable final Range<Integer>[] array) {
        final Range<Integer> range = null;
        Range<Integer> range2 = null;
        Range<Integer> range3 = range;
        if (array != null) {
            if (array.length == 0) {
                range3 = range;
            }
            else {
                final int length = array.length;
                int n = 0;
                while (true) {
                    range3 = range2;
                    if (n >= length) {
                        break;
                    }
                    final Range<Integer> correctedFpsRange = this.getCorrectedFpsRange(array[n]);
                    Range<Integer> range4 = null;
                    Label_0113: {
                        if ((int)correctedFpsRange.getUpper() != 30) {
                            range4 = range2;
                        }
                        else {
                            if (range2 != null) {
                                range4 = range2;
                                if ((int)correctedFpsRange.getLower() >= (int)range2.getLower()) {
                                    break Label_0113;
                                }
                            }
                            range4 = correctedFpsRange;
                        }
                    }
                    ++n;
                    range2 = range4;
                }
            }
        }
        return range3;
    }
    
    @Nullable
    public Range<Integer> getRange() {
        return this.mAeFpsRange;
    }
}

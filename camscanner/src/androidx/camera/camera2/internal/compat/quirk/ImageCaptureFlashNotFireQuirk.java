// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import java.util.Locale;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import java.util.Arrays;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class ImageCaptureFlashNotFireQuirk implements UseTorchAsFlashQuirk
{
    private static final List<String> BUILD_MODELS;
    private static final List<String> BUILD_MODELS_FRONT_CAMERA;
    
    static {
        BUILD_MODELS = Arrays.asList("itel w6004");
        BUILD_MODELS_FRONT_CAMERA = Arrays.asList("sm-j700f", "sm-j710f");
    }
    
    static boolean load(@NonNull final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        final List<String> build_MODELS_FRONT_CAMERA = ImageCaptureFlashNotFireQuirk.BUILD_MODELS_FRONT_CAMERA;
        final String model = Build.MODEL;
        final Locale us = Locale.US;
        final boolean contains = build_MODELS_FRONT_CAMERA.contains(model.toLowerCase(us));
        final boolean b = true;
        final boolean b2 = contains && cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.LENS_FACING) == 0;
        final boolean contains2 = ImageCaptureFlashNotFireQuirk.BUILD_MODELS.contains(model.toLowerCase(us));
        boolean b3 = b;
        if (!b2) {
            b3 = (contains2 && b);
        }
        return b3;
    }
}

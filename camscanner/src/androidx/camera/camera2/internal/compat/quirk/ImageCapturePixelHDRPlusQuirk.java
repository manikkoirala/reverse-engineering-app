// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import android.os.Build$VERSION;
import android.os.Build;
import java.util.Arrays;
import java.util.List;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.Quirk;

@RequiresApi(21)
public class ImageCapturePixelHDRPlusQuirk implements Quirk
{
    public static final List<String> BUILD_MODELS;
    
    static {
        BUILD_MODELS = Arrays.asList("Pixel 2", "Pixel 2 XL", "Pixel 3", "Pixel 3 XL");
    }
    
    static boolean load() {
        return ImageCapturePixelHDRPlusQuirk.BUILD_MODELS.contains(Build.MODEL) && "Google".equals(Build.MANUFACTURER) && Build$VERSION.SDK_INT >= 26;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import java.util.Locale;
import android.os.Build;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import android.util.Pair;
import java.util.Set;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.Quirk;

@RequiresApi(21)
public class FlashAvailabilityBufferUnderflowQuirk implements Quirk
{
    private static final Set<Pair<String, String>> KNOWN_AFFECTED_MODELS;
    
    static {
        KNOWN_AFFECTED_MODELS = new HashSet<Pair<String, String>>((Collection<? extends Pair<String, String>>)Arrays.asList(new Pair((Object)"sprd", (Object)"lemp")));
    }
    
    static boolean load() {
        final Set<Pair<String, String>> known_AFFECTED_MODELS = FlashAvailabilityBufferUnderflowQuirk.KNOWN_AFFECTED_MODELS;
        final String manufacturer = Build.MANUFACTURER;
        final Locale us = Locale.US;
        return known_AFFECTED_MODELS.contains(new Pair((Object)manufacturer.toLowerCase(us), (Object)Build.MODEL.toLowerCase(us)));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import java.util.Locale;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.Quirk;

@RequiresApi(21)
public class StillCaptureFlashStopRepeatingQuirk implements Quirk
{
    static boolean load() {
        final String manufacturer = Build.MANUFACTURER;
        final Locale us = Locale.US;
        return "SAMSUNG".equals(manufacturer.toUpperCase(us)) && Build.MODEL.toUpperCase(us).startsWith("SM-A716");
    }
}

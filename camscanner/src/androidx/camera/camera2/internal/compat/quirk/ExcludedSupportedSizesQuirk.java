// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import java.util.Collections;
import androidx.camera.core.Logger;
import android.os.Build$VERSION;
import java.util.Locale;
import android.os.Build;
import java.util.ArrayList;
import android.util.Size;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.Quirk;

@RequiresApi(21)
public class ExcludedSupportedSizesQuirk implements Quirk
{
    private static final String TAG = "ExcludedSupportedSizesQuirk";
    
    @NonNull
    private List<Size> getHuaweiP20LiteExcludedSizes(@NonNull final String s, final int n) {
        final ArrayList list = new ArrayList();
        if (s.equals("0") && (n == 34 || n == 35)) {
            list.add(new Size(720, 720));
            list.add(new Size(400, 400));
        }
        return list;
    }
    
    @NonNull
    private List<Size> getOnePlus6ExcludedSizes(@NonNull final String s, final int n) {
        final ArrayList list = new ArrayList();
        if (s.equals("0") && n == 256) {
            list.add(new Size(4160, 3120));
            list.add(new Size(4000, 3000));
        }
        return list;
    }
    
    @NonNull
    private List<Size> getOnePlus6TExcludedSizes(@NonNull final String s, final int n) {
        final ArrayList list = new ArrayList();
        if (s.equals("0") && n == 256) {
            list.add(new Size(4160, 3120));
            list.add(new Size(4000, 3000));
        }
        return list;
    }
    
    @NonNull
    private List<Size> getSamsungJ7Api27AboveExcludedSizes(@NonNull final String s, final int n) {
        final ArrayList list = new ArrayList();
        if (s.equals("0")) {
            if (n != 34) {
                if (n == 35) {
                    list.add(new Size(2048, 1536));
                    list.add(new Size(2048, 1152));
                    list.add(new Size(1920, 1080));
                }
            }
            else {
                list.add(new Size(4128, 3096));
                list.add(new Size(4128, 2322));
                list.add(new Size(3088, 3088));
                list.add(new Size(3264, 2448));
                list.add(new Size(3264, 1836));
                list.add(new Size(2048, 1536));
                list.add(new Size(2048, 1152));
                list.add(new Size(1920, 1080));
            }
        }
        else if (s.equals("1") && (n == 34 || n == 35)) {
            list.add(new Size(2576, 1932));
            list.add(new Size(2560, 1440));
            list.add(new Size(1920, 1920));
            list.add(new Size(2048, 1536));
            list.add(new Size(2048, 1152));
            list.add(new Size(1920, 1080));
        }
        return list;
    }
    
    @NonNull
    private List<Size> getSamsungJ7PrimeApi27AboveExcludedSizes(@NonNull final String s, final int n) {
        final ArrayList list = new ArrayList();
        if (s.equals("0")) {
            if (n != 34) {
                if (n == 35) {
                    list.add(new Size(4128, 2322));
                    list.add(new Size(3088, 3088));
                    list.add(new Size(3264, 2448));
                    list.add(new Size(3264, 1836));
                    list.add(new Size(2048, 1536));
                    list.add(new Size(2048, 1152));
                    list.add(new Size(1920, 1080));
                }
            }
            else {
                list.add(new Size(4128, 3096));
                list.add(new Size(4128, 2322));
                list.add(new Size(3088, 3088));
                list.add(new Size(3264, 2448));
                list.add(new Size(3264, 1836));
                list.add(new Size(2048, 1536));
                list.add(new Size(2048, 1152));
                list.add(new Size(1920, 1080));
            }
        }
        else if (s.equals("1") && (n == 34 || n == 35)) {
            list.add(new Size(3264, 2448));
            list.add(new Size(3264, 1836));
            list.add(new Size(2448, 2448));
            list.add(new Size(1920, 1920));
            list.add(new Size(2048, 1536));
            list.add(new Size(2048, 1152));
            list.add(new Size(1920, 1080));
        }
        return list;
    }
    
    private static boolean isHuaweiP20Lite() {
        return "HUAWEI".equalsIgnoreCase(Build.BRAND) && "HWANE".equalsIgnoreCase(Build.DEVICE);
    }
    
    private static boolean isOnePlus6() {
        return "OnePlus".equalsIgnoreCase(Build.BRAND) && "OnePlus6".equalsIgnoreCase(Build.DEVICE);
    }
    
    private static boolean isOnePlus6T() {
        return "OnePlus".equalsIgnoreCase(Build.BRAND) && "OnePlus6T".equalsIgnoreCase(Build.DEVICE);
    }
    
    private static boolean isSamsungJ7Api27Above() {
        final String brand = Build.BRAND;
        final Locale us = Locale.US;
        return "SAMSUNG".equalsIgnoreCase(brand.toUpperCase(us)) && "J7XELTE".equalsIgnoreCase(Build.DEVICE.toUpperCase(us)) && Build$VERSION.SDK_INT >= 27;
    }
    
    private static boolean isSamsungJ7PrimeApi27Above() {
        final String brand = Build.BRAND;
        final Locale us = Locale.US;
        return "SAMSUNG".equalsIgnoreCase(brand.toUpperCase(us)) && "ON7XELTE".equalsIgnoreCase(Build.DEVICE.toUpperCase(us)) && Build$VERSION.SDK_INT >= 27;
    }
    
    static boolean load() {
        return isOnePlus6() || isOnePlus6T() || isHuaweiP20Lite() || isSamsungJ7PrimeApi27Above() || isSamsungJ7Api27Above();
    }
    
    @NonNull
    public List<Size> getExcludedSizes(@NonNull final String s, final int n) {
        if (isOnePlus6()) {
            return this.getOnePlus6ExcludedSizes(s, n);
        }
        if (isOnePlus6T()) {
            return this.getOnePlus6TExcludedSizes(s, n);
        }
        if (isHuaweiP20Lite()) {
            return this.getHuaweiP20LiteExcludedSizes(s, n);
        }
        if (isSamsungJ7PrimeApi27Above()) {
            return this.getSamsungJ7PrimeApi27AboveExcludedSizes(s, n);
        }
        if (isSamsungJ7Api27Above()) {
            return this.getSamsungJ7Api27AboveExcludedSizes(s, n);
        }
        Logger.w("ExcludedSupportedSizesQuirk", "Cannot retrieve list of supported sizes to exclude on this device.");
        return Collections.emptyList();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import java.util.Locale;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import java.util.Arrays;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class FlashTooSlowQuirk implements UseTorchAsFlashQuirk
{
    private static final List<String> AFFECTED_MODELS;
    
    static {
        AFFECTED_MODELS = Arrays.asList("PIXEL 3A", "PIXEL 3A XL");
    }
    
    static boolean load(@NonNull final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        if (FlashTooSlowQuirk.AFFECTED_MODELS.contains(Build.MODEL.toUpperCase(Locale.US))) {
            final int intValue = cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.LENS_FACING);
            final boolean b = true;
            if (intValue == 1) {
                return b;
            }
        }
        return false;
    }
}

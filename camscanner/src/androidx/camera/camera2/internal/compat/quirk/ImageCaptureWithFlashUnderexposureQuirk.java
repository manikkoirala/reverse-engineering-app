// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import java.util.Locale;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import java.util.Arrays;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class ImageCaptureWithFlashUnderexposureQuirk implements UseTorchAsFlashQuirk
{
    public static final List<String> BUILD_MODELS;
    
    static {
        BUILD_MODELS = Arrays.asList("sm-a260f", "sm-j530f", "sm-j600g", "sm-j701f", "sm-g610f", "sm-j710mn");
    }
    
    static boolean load(@NonNull final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        if (ImageCaptureWithFlashUnderexposureQuirk.BUILD_MODELS.contains(Build.MODEL.toLowerCase(Locale.US))) {
            final int intValue = cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.LENS_FACING);
            final boolean b = true;
            if (intValue == 1) {
                return b;
            }
        }
        return false;
    }
}

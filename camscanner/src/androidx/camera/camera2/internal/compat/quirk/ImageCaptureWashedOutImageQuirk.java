// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import java.util.Locale;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import java.util.Arrays;
import androidx.annotation.VisibleForTesting;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class ImageCaptureWashedOutImageQuirk implements UseTorchAsFlashQuirk
{
    @VisibleForTesting
    public static final List<String> BUILD_MODELS;
    
    static {
        BUILD_MODELS = Arrays.asList("SM-G9300", "SM-G930R", "SM-G930A", "SM-G930V", "SM-G930T", "SM-G930U", "SM-G930P", "SM-SC02H", "SM-SCV33", "SM-G9350", "SM-G935R", "SM-G935A", "SM-G935V", "SM-G935T", "SM-G935U", "SM-G935P");
    }
    
    static boolean load(@NonNull final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        if (ImageCaptureWashedOutImageQuirk.BUILD_MODELS.contains(Build.MODEL.toUpperCase(Locale.US))) {
            final int intValue = cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.LENS_FACING);
            final boolean b = true;
            if (intValue == 1) {
                return b;
            }
        }
        return false;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import java.util.Locale;
import android.os.Build$VERSION;
import android.os.Build;
import java.util.Arrays;
import java.util.List;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.Quirk;

@RequiresApi(21)
public class Nexus4AndroidLTargetAspectRatioQuirk implements Quirk
{
    private static final List<String> DEVICE_MODELS;
    
    static {
        DEVICE_MODELS = Arrays.asList("NEXUS 4");
    }
    
    static boolean load() {
        return "GOOGLE".equalsIgnoreCase(Build.BRAND) && Build$VERSION.SDK_INT < 23 && Nexus4AndroidLTargetAspectRatioQuirk.DEVICE_MODELS.contains(Build.MODEL.toUpperCase(Locale.US));
    }
    
    public int getCorrectedAspectRatio() {
        return 2;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import java.util.Locale;
import android.os.Build;
import java.util.Arrays;
import java.util.List;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.Quirk;

@RequiresApi(21)
public class CrashWhenTakingPhotoWithAutoFlashAEModeQuirk implements Quirk
{
    static final List<String> AFFECTED_MODELS;
    
    static {
        AFFECTED_MODELS = Arrays.asList("SM-A3000", "SM-A3009", "SM-A300F", "SM-A300FU", "SM-A300G", "SM-A300H", "SM-A300M", "SM-A300X", "SM-A300XU", "SM-A300XZ", "SM-A300Y", "SM-A300YZ", "SM-J510FN", "5059X");
    }
    
    static boolean load() {
        return CrashWhenTakingPhotoWithAutoFlashAEModeQuirk.AFFECTED_MODELS.contains(Build.MODEL.toUpperCase(Locale.US));
    }
}

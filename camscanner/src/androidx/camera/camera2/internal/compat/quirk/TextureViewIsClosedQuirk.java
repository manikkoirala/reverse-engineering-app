// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import android.os.Build$VERSION;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.Quirk;

@RequiresApi(21)
public class TextureViewIsClosedQuirk implements Quirk
{
    static boolean load() {
        return Build$VERSION.SDK_INT <= 23;
    }
}

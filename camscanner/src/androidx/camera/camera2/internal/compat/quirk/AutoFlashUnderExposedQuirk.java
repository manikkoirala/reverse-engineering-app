// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import androidx.annotation.NonNull;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.Quirk;

@RequiresApi(21)
public class AutoFlashUnderExposedQuirk implements Quirk
{
    static boolean load(@NonNull final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        return false;
    }
}

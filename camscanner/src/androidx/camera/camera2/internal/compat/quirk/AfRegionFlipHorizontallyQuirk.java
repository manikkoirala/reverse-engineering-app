// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.quirk;

import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import android.os.Build$VERSION;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.Quirk;

@RequiresApi(21)
public class AfRegionFlipHorizontallyQuirk implements Quirk
{
    static boolean load(@NonNull final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        return Build.BRAND.equalsIgnoreCase("SAMSUNG") && Build$VERSION.SDK_INT < 33 && cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.LENS_FACING) == 0;
    }
}

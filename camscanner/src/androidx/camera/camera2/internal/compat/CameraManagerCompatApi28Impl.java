// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import android.hardware.camera2.CameraManager$AvailabilityCallback;
import androidx.annotation.RequiresPermission;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraDevice$StateCallback;
import java.util.concurrent.Executor;
import android.hardware.camera2.CameraCharacteristics;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.annotation.RequiresApi;

@RequiresApi(28)
class CameraManagerCompatApi28Impl extends CameraManagerCompatBaseImpl
{
    CameraManagerCompatApi28Impl(@NonNull final Context context) {
        super(context, null);
    }
    
    static CameraManagerCompatApi28Impl create(@NonNull final Context context) {
        return new CameraManagerCompatApi28Impl(context);
    }
    
    private boolean isDndFailCase(@NonNull final Throwable t) {
        return Build$VERSION.SDK_INT == 28 && isDndRuntimeException(t);
    }
    
    private static boolean isDndRuntimeException(@NonNull final Throwable t) {
        if (t.getClass().equals(RuntimeException.class)) {
            final StackTraceElement[] stackTrace = t.getStackTrace();
            if (stackTrace != null) {
                if (stackTrace.length >= 0) {
                    return "_enableShutterSound".equals(stackTrace[0].getMethodName());
                }
            }
        }
        return false;
    }
    
    private void throwDndException(@NonNull final Throwable t) throws CameraAccessExceptionCompat {
        throw new CameraAccessExceptionCompat(10001, t);
    }
    
    @NonNull
    @Override
    public CameraCharacteristics getCameraCharacteristics(@NonNull final String s) throws CameraAccessExceptionCompat {
        try {
            return super.getCameraCharacteristics(s);
        }
        catch (final RuntimeException ex) {
            if (this.isDndFailCase(ex)) {
                this.throwDndException(ex);
            }
            throw ex;
        }
    }
    
    @RequiresPermission("android.permission.CAMERA")
    @Override
    public void openCamera(@NonNull final String s, @NonNull final Executor executor, @NonNull final CameraDevice$StateCallback cameraDevice$StateCallback) throws CameraAccessExceptionCompat {
        try {
            Oo8Oo00oo.\u3007080(super.mCameraManager, s, executor, cameraDevice$StateCallback);
        }
        catch (final RuntimeException ex) {
            if (this.isDndFailCase(ex)) {
                this.throwDndException(ex);
            }
            throw ex;
        }
        catch (final SecurityException ex2) {
            throw ex2;
        }
        catch (final IllegalArgumentException ex2) {
            throw ex2;
        }
        catch (final CameraAccessException ex3) {
            throw CameraAccessExceptionCompat.toCameraAccessExceptionCompat(ex3);
        }
    }
    
    @Override
    public void registerAvailabilityCallback(@NonNull final Executor executor, @NonNull final CameraManager$AvailabilityCallback cameraManager$AvailabilityCallback) {
        o\u30078.\u3007080(super.mCameraManager, executor, cameraManager$AvailabilityCallback);
    }
    
    @Override
    public void unregisterAvailabilityCallback(@NonNull final CameraManager$AvailabilityCallback cameraManager$AvailabilityCallback) {
        super.mCameraManager.unregisterAvailabilityCallback(cameraManager$AvailabilityCallback);
    }
}

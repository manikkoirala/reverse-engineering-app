// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import java.util.Set;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import android.os.Build$VERSION;
import java.util.HashMap;
import android.hardware.camera2.CameraCharacteristics;
import androidx.annotation.GuardedBy;
import android.hardware.camera2.CameraCharacteristics$Key;
import java.util.Map;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class CameraCharacteristicsCompat
{
    @NonNull
    private final CameraCharacteristicsCompatImpl mCameraCharacteristicsImpl;
    @GuardedBy("this")
    @NonNull
    private final Map<CameraCharacteristics$Key<?>, Object> mValuesCache;
    
    private CameraCharacteristicsCompat(@NonNull final CameraCharacteristics cameraCharacteristics) {
        this.mValuesCache = new HashMap<CameraCharacteristics$Key<?>, Object>();
        if (Build$VERSION.SDK_INT >= 28) {
            this.mCameraCharacteristicsImpl = (CameraCharacteristicsCompatImpl)new CameraCharacteristicsApi28Impl(cameraCharacteristics);
        }
        else {
            this.mCameraCharacteristicsImpl = (CameraCharacteristicsCompatImpl)new CameraCharacteristicsBaseImpl(cameraCharacteristics);
        }
    }
    
    private boolean isKeyNonCacheable(@NonNull final CameraCharacteristics$Key<?> cameraCharacteristics$Key) {
        return cameraCharacteristics$Key.equals((Object)CameraCharacteristics.SENSOR_ORIENTATION);
    }
    
    @NonNull
    @VisibleForTesting(otherwise = 3)
    public static CameraCharacteristicsCompat toCameraCharacteristicsCompat(@NonNull final CameraCharacteristics cameraCharacteristics) {
        return new CameraCharacteristicsCompat(cameraCharacteristics);
    }
    
    @Nullable
    public <T> T get(@NonNull final CameraCharacteristics$Key<T> cameraCharacteristics$Key) {
        if (this.isKeyNonCacheable(cameraCharacteristics$Key)) {
            return this.mCameraCharacteristicsImpl.get(cameraCharacteristics$Key);
        }
        synchronized (this) {
            final Object value = this.mValuesCache.get(cameraCharacteristics$Key);
            if (value != null) {
                return (T)value;
            }
            final T value2 = this.mCameraCharacteristicsImpl.get(cameraCharacteristics$Key);
            if (value2 != null) {
                this.mValuesCache.put(cameraCharacteristics$Key, value2);
            }
            return value2;
        }
    }
    
    @NonNull
    public Set<String> getPhysicalCameraIds() {
        return this.mCameraCharacteristicsImpl.getPhysicalCameraIds();
    }
    
    @NonNull
    public CameraCharacteristics toCameraCharacteristics() {
        return this.mCameraCharacteristicsImpl.unwrap();
    }
    
    public interface CameraCharacteristicsCompatImpl
    {
        @Nullable
         <T> T get(@NonNull final CameraCharacteristics$Key<T> p0);
        
        @NonNull
        Set<String> getPhysicalCameraIds();
        
        @NonNull
        CameraCharacteristics unwrap();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.params.SessionConfiguration;
import androidx.camera.camera2.internal.compat.params.SessionConfigurationCompat;
import androidx.core.util.Preconditions;
import androidx.annotation.NonNull;
import android.hardware.camera2.CameraDevice;
import androidx.annotation.RequiresApi;

@RequiresApi(28)
class CameraDeviceCompatApi28Impl extends CameraDeviceCompatApi24Impl
{
    CameraDeviceCompatApi28Impl(@NonNull final CameraDevice cameraDevice) {
        super(Preconditions.checkNotNull(cameraDevice), null);
    }
    
    @Override
    public void createCaptureSession(@NonNull final SessionConfigurationCompat sessionConfigurationCompat) throws CameraAccessExceptionCompat {
        final SessionConfiguration sessionConfiguration = (SessionConfiguration)sessionConfigurationCompat.unwrap();
        Preconditions.checkNotNull(sessionConfiguration);
        try {
            oo\u3007.\u3007080(super.mCameraDevice, sessionConfiguration);
        }
        catch (final CameraAccessException ex) {
            throw CameraAccessExceptionCompat.toCameraAccessExceptionCompat(ex);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import android.hardware.camera2.CameraCaptureSession$StateCallback;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.CaptureFailure;
import android.hardware.camera2.TotalCaptureResult;
import android.view.Surface;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import java.util.concurrent.Executor;
import android.hardware.camera2.CaptureRequest;
import java.util.List;
import androidx.camera.core.impl.utils.MainThreadAsyncHandler;
import android.os.Build$VERSION;
import android.os.Handler;
import androidx.annotation.NonNull;
import android.hardware.camera2.CameraCaptureSession;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class CameraCaptureSessionCompat
{
    private final CameraCaptureSessionCompatImpl mImpl;
    
    private CameraCaptureSessionCompat(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final Handler handler) {
        if (Build$VERSION.SDK_INT >= 28) {
            this.mImpl = (CameraCaptureSessionCompatImpl)new CameraCaptureSessionCompatApi28Impl(cameraCaptureSession);
        }
        else {
            this.mImpl = CameraCaptureSessionCompatBaseImpl.create(cameraCaptureSession, handler);
        }
    }
    
    @NonNull
    public static CameraCaptureSessionCompat toCameraCaptureSessionCompat(@NonNull final CameraCaptureSession cameraCaptureSession) {
        return toCameraCaptureSessionCompat(cameraCaptureSession, MainThreadAsyncHandler.getInstance());
    }
    
    @NonNull
    public static CameraCaptureSessionCompat toCameraCaptureSessionCompat(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final Handler handler) {
        return new CameraCaptureSessionCompat(cameraCaptureSession, handler);
    }
    
    public int captureBurstRequests(@NonNull final List<CaptureRequest> list, @NonNull final Executor executor, @NonNull final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        return this.mImpl.captureBurstRequests(list, executor, cameraCaptureSession$CaptureCallback);
    }
    
    public int captureSingleRequest(@NonNull final CaptureRequest captureRequest, @NonNull final Executor executor, @NonNull final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        return this.mImpl.captureSingleRequest(captureRequest, executor, cameraCaptureSession$CaptureCallback);
    }
    
    public int setRepeatingBurstRequests(@NonNull final List<CaptureRequest> list, @NonNull final Executor executor, @NonNull final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        return this.mImpl.setRepeatingBurstRequests(list, executor, cameraCaptureSession$CaptureCallback);
    }
    
    public int setSingleRepeatingRequest(@NonNull final CaptureRequest captureRequest, @NonNull final Executor executor, @NonNull final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        return this.mImpl.setSingleRepeatingRequest(captureRequest, executor, cameraCaptureSession$CaptureCallback);
    }
    
    @NonNull
    public CameraCaptureSession toCameraCaptureSession() {
        return this.mImpl.unwrap();
    }
    
    interface CameraCaptureSessionCompatImpl
    {
        int captureBurstRequests(@NonNull final List<CaptureRequest> p0, @NonNull final Executor p1, @NonNull final CameraCaptureSession$CaptureCallback p2) throws CameraAccessException;
        
        int captureSingleRequest(@NonNull final CaptureRequest p0, @NonNull final Executor p1, @NonNull final CameraCaptureSession$CaptureCallback p2) throws CameraAccessException;
        
        int setRepeatingBurstRequests(@NonNull final List<CaptureRequest> p0, @NonNull final Executor p1, @NonNull final CameraCaptureSession$CaptureCallback p2) throws CameraAccessException;
        
        int setSingleRepeatingRequest(@NonNull final CaptureRequest p0, @NonNull final Executor p1, @NonNull final CameraCaptureSession$CaptureCallback p2) throws CameraAccessException;
        
        @NonNull
        CameraCaptureSession unwrap();
    }
    
    @RequiresApi(21)
    static final class CaptureCallbackExecutorWrapper extends CameraCaptureSession$CaptureCallback
    {
        private final Executor mExecutor;
        final CameraCaptureSession$CaptureCallback mWrappedCallback;
        
        CaptureCallbackExecutorWrapper(@NonNull final Executor mExecutor, @NonNull final CameraCaptureSession$CaptureCallback mWrappedCallback) {
            this.mExecutor = mExecutor;
            this.mWrappedCallback = mWrappedCallback;
        }
        
        @RequiresApi(24)
        public void onCaptureBufferLost(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, @NonNull final Surface surface, final long n) {
            this.mExecutor.execute(new o\u30070(this, cameraCaptureSession, captureRequest, surface, n));
        }
        
        public void onCaptureCompleted(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, @NonNull final TotalCaptureResult totalCaptureResult) {
            this.mExecutor.execute(new \u300780\u3007808\u3007O(this, cameraCaptureSession, captureRequest, totalCaptureResult));
        }
        
        public void onCaptureFailed(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, @NonNull final CaptureFailure captureFailure) {
            this.mExecutor.execute(new \u30078o8o\u3007(this, cameraCaptureSession, captureRequest, captureFailure));
        }
        
        public void onCaptureProgressed(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, @NonNull final CaptureResult captureResult) {
            this.mExecutor.execute(new oO80(this, cameraCaptureSession, captureRequest, captureResult));
        }
        
        public void onCaptureSequenceAborted(@NonNull final CameraCaptureSession cameraCaptureSession, final int n) {
            this.mExecutor.execute(new Oo08(this, cameraCaptureSession, n));
        }
        
        public void onCaptureSequenceCompleted(@NonNull final CameraCaptureSession cameraCaptureSession, final int n, final long n2) {
            this.mExecutor.execute(new \u3007\u3007888(this, cameraCaptureSession, n, n2));
        }
        
        public void onCaptureStarted(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, final long n, final long n2) {
            this.mExecutor.execute(new OO0o\u3007\u3007\u3007\u30070(this, cameraCaptureSession, captureRequest, n, n2));
        }
    }
    
    @RequiresApi(21)
    static final class StateCallbackExecutorWrapper extends CameraCaptureSession$StateCallback
    {
        private final Executor mExecutor;
        final CameraCaptureSession$StateCallback mWrappedCallback;
        
        StateCallbackExecutorWrapper(@NonNull final Executor mExecutor, @NonNull final CameraCaptureSession$StateCallback mWrappedCallback) {
            this.mExecutor = mExecutor;
            this.mWrappedCallback = mWrappedCallback;
        }
        
        public void onActive(@NonNull final CameraCaptureSession cameraCaptureSession) {
            this.mExecutor.execute(new \u3007\u3007808\u3007(this, cameraCaptureSession));
        }
        
        @RequiresApi(26)
        public void onCaptureQueueEmpty(@NonNull final CameraCaptureSession cameraCaptureSession) {
            this.mExecutor.execute(new Oooo8o0\u3007(this, cameraCaptureSession));
        }
        
        public void onClosed(@NonNull final CameraCaptureSession cameraCaptureSession) {
            this.mExecutor.execute(new OO0o\u3007\u3007(this, cameraCaptureSession));
        }
        
        public void onConfigureFailed(@NonNull final CameraCaptureSession cameraCaptureSession) {
            this.mExecutor.execute(new \u3007O00(this, cameraCaptureSession));
        }
        
        public void onConfigured(@NonNull final CameraCaptureSession cameraCaptureSession) {
            this.mExecutor.execute(new \u3007O8o08O(this, cameraCaptureSession));
        }
        
        public void onReady(@NonNull final CameraCaptureSession cameraCaptureSession) {
            this.mExecutor.execute(new \u3007\u30078O0\u30078(this, cameraCaptureSession));
        }
        
        @RequiresApi(23)
        public void onSurfacePrepared(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final Surface surface) {
            this.mExecutor.execute(new \u30070\u3007O0088o(this, cameraCaptureSession, surface));
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import androidx.annotation.RequiresPermission;
import android.hardware.camera2.CameraDevice$StateCallback;
import java.util.concurrent.Executor;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.annotation.RequiresApi;

@RequiresApi(29)
class CameraManagerCompatApi29Impl extends CameraManagerCompatApi28Impl
{
    CameraManagerCompatApi29Impl(@NonNull final Context context) {
        super(context);
    }
    
    @NonNull
    @Override
    public CameraCharacteristics getCameraCharacteristics(@NonNull final String s) throws CameraAccessExceptionCompat {
        try {
            return super.mCameraManager.getCameraCharacteristics(s);
        }
        catch (final CameraAccessException ex) {
            throw CameraAccessExceptionCompat.toCameraAccessExceptionCompat(ex);
        }
    }
    
    @RequiresPermission("android.permission.CAMERA")
    @Override
    public void openCamera(@NonNull final String s, @NonNull final Executor executor, @NonNull final CameraDevice$StateCallback cameraDevice$StateCallback) throws CameraAccessExceptionCompat {
        try {
            Oo8Oo00oo.\u3007080(super.mCameraManager, s, executor, cameraDevice$StateCallback);
        }
        catch (final CameraAccessException ex) {
            throw CameraAccessExceptionCompat.toCameraAccessExceptionCompat(ex);
        }
    }
}

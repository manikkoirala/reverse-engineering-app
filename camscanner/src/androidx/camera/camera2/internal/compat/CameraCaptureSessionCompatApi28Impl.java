// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import java.util.concurrent.Executor;
import android.hardware.camera2.CaptureRequest;
import java.util.List;
import androidx.annotation.NonNull;
import android.hardware.camera2.CameraCaptureSession;
import androidx.annotation.RequiresApi;

@RequiresApi(28)
class CameraCaptureSessionCompatApi28Impl extends CameraCaptureSessionCompatBaseImpl
{
    CameraCaptureSessionCompatApi28Impl(@NonNull final CameraCaptureSession cameraCaptureSession) {
        super(cameraCaptureSession, null);
    }
    
    @Override
    public int captureBurstRequests(@NonNull final List<CaptureRequest> list, @NonNull final Executor executor, @NonNull final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        return OoO8.\u3007080(super.mCameraCaptureSession, (List)list, executor, cameraCaptureSession$CaptureCallback);
    }
    
    @Override
    public int captureSingleRequest(@NonNull final CaptureRequest captureRequest, @NonNull final Executor executor, @NonNull final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        return o800o8O.\u3007080(super.mCameraCaptureSession, captureRequest, executor, cameraCaptureSession$CaptureCallback);
    }
    
    @Override
    public int setRepeatingBurstRequests(@NonNull final List<CaptureRequest> list, @NonNull final Executor executor, @NonNull final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        return \u3007O888o0o.\u3007080(super.mCameraCaptureSession, (List)list, executor, cameraCaptureSession$CaptureCallback);
    }
    
    @Override
    public int setSingleRepeatingRequest(@NonNull final CaptureRequest captureRequest, @NonNull final Executor executor, @NonNull final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        return oo88o8O.\u3007080(super.mCameraCaptureSession, captureRequest, executor, cameraCaptureSession$CaptureCallback);
    }
}

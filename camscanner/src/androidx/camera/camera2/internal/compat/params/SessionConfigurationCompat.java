// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.params;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.Collection;
import android.hardware.camera2.params.InputConfiguration;
import java.util.Objects;
import java.util.Collections;
import android.hardware.camera2.params.SessionConfiguration;
import android.hardware.camera2.CaptureRequest;
import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
import java.util.Iterator;
import java.util.ArrayList;
import android.hardware.camera2.params.OutputConfiguration;
import android.os.Build$VERSION;
import android.hardware.camera2.CameraCaptureSession$StateCallback;
import java.util.concurrent.Executor;
import androidx.annotation.NonNull;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class SessionConfigurationCompat
{
    public static final int SESSION_HIGH_SPEED = 1;
    public static final int SESSION_REGULAR = 0;
    private final SessionConfigurationCompatImpl mImpl;
    
    public SessionConfigurationCompat(final int n, @NonNull final List<OutputConfigurationCompat> list, @NonNull final Executor executor, @NonNull final CameraCaptureSession$StateCallback cameraCaptureSession$StateCallback) {
        if (Build$VERSION.SDK_INT < 28) {
            this.mImpl = (SessionConfigurationCompatImpl)new SessionConfigurationCompatBaseImpl(n, list, executor, cameraCaptureSession$StateCallback);
        }
        else {
            this.mImpl = (SessionConfigurationCompatImpl)new SessionConfigurationCompatApi28Impl(n, list, executor, cameraCaptureSession$StateCallback);
        }
    }
    
    private SessionConfigurationCompat(@NonNull final SessionConfigurationCompatImpl mImpl) {
        this.mImpl = mImpl;
    }
    
    @NonNull
    @RequiresApi(24)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static List<OutputConfiguration> transformFromCompat(@NonNull final List<OutputConfigurationCompat> list) {
        final ArrayList list2 = new ArrayList(list.size());
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            list2.add(((OutputConfigurationCompat)iterator.next()).unwrap());
        }
        return list2;
    }
    
    @RequiresApi(24)
    static List<OutputConfigurationCompat> transformToCompat(@NonNull final List<OutputConfiguration> list) {
        final ArrayList list2 = new ArrayList(list.size());
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            list2.add(OutputConfigurationCompat.wrap(iterator.next()));
        }
        return list2;
    }
    
    @Nullable
    public static SessionConfigurationCompat wrap(@Nullable final Object o) {
        if (o == null) {
            return null;
        }
        if (Build$VERSION.SDK_INT < 28) {
            return null;
        }
        return new SessionConfigurationCompat((SessionConfigurationCompatImpl)new SessionConfigurationCompatApi28Impl(o));
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        return o instanceof SessionConfigurationCompat && this.mImpl.equals(((SessionConfigurationCompat)o).mImpl);
    }
    
    @NonNull
    public Executor getExecutor() {
        return this.mImpl.getExecutor();
    }
    
    @Nullable
    public InputConfigurationCompat getInputConfiguration() {
        return this.mImpl.getInputConfiguration();
    }
    
    @NonNull
    public List<OutputConfigurationCompat> getOutputConfigurations() {
        return this.mImpl.getOutputConfigurations();
    }
    
    @Nullable
    public CaptureRequest getSessionParameters() {
        return this.mImpl.getSessionParameters();
    }
    
    public int getSessionType() {
        return this.mImpl.getSessionType();
    }
    
    @NonNull
    public CameraCaptureSession$StateCallback getStateCallback() {
        return this.mImpl.getStateCallback();
    }
    
    @Override
    public int hashCode() {
        return this.mImpl.hashCode();
    }
    
    public void setInputConfiguration(@NonNull final InputConfigurationCompat inputConfiguration) {
        this.mImpl.setInputConfiguration(inputConfiguration);
    }
    
    public void setSessionParameters(@NonNull final CaptureRequest sessionParameters) {
        this.mImpl.setSessionParameters(sessionParameters);
    }
    
    @Nullable
    public Object unwrap() {
        return this.mImpl.getSessionConfiguration();
    }
    
    @RequiresApi(28)
    private static final class SessionConfigurationCompatApi28Impl implements SessionConfigurationCompatImpl
    {
        private final SessionConfiguration mObject;
        private final List<OutputConfigurationCompat> mOutputConfigurations;
        
        SessionConfigurationCompatApi28Impl(final int n, @NonNull final List<OutputConfigurationCompat> list, @NonNull final Executor executor, @NonNull final CameraCaptureSession$StateCallback cameraCaptureSession$StateCallback) {
            this(new SessionConfiguration(n, (List)SessionConfigurationCompat.transformFromCompat(list), executor, cameraCaptureSession$StateCallback));
        }
        
        SessionConfigurationCompatApi28Impl(@NonNull final Object o) {
            final SessionConfiguration mObject = (SessionConfiguration)o;
            this.mObject = mObject;
            this.mOutputConfigurations = (List<OutputConfigurationCompat>)Collections.unmodifiableList((List<?>)SessionConfigurationCompat.transformToCompat(\u3007\u30078O0\u30078.\u3007080(mObject)));
        }
        
        @Override
        public boolean equals(@Nullable final Object o) {
            return o instanceof SessionConfigurationCompatApi28Impl && Objects.equals(this.mObject, ((SessionConfigurationCompatApi28Impl)o).mObject);
        }
        
        @NonNull
        @Override
        public Executor getExecutor() {
            return \u3007O00.\u3007080(this.mObject);
        }
        
        @Override
        public InputConfigurationCompat getInputConfiguration() {
            return InputConfigurationCompat.wrap(OoO8.\u3007080(this.mObject));
        }
        
        @NonNull
        @Override
        public List<OutputConfigurationCompat> getOutputConfigurations() {
            return this.mOutputConfigurations;
        }
        
        @Nullable
        @Override
        public Object getSessionConfiguration() {
            return this.mObject;
        }
        
        @Override
        public CaptureRequest getSessionParameters() {
            return \u3007oo\u3007.\u3007080(this.mObject);
        }
        
        @Override
        public int getSessionType() {
            return oo88o8O.\u3007080(this.mObject);
        }
        
        @NonNull
        @Override
        public CameraCaptureSession$StateCallback getStateCallback() {
            return \u3007O888o0o.\u3007080(this.mObject);
        }
        
        @Override
        public int hashCode() {
            return o\u3007O8\u3007\u3007o.\u3007080(this.mObject);
        }
        
        @Override
        public void setInputConfiguration(@NonNull final InputConfigurationCompat inputConfigurationCompat) {
            o800o8O.\u3007080(this.mObject, (InputConfiguration)inputConfigurationCompat.unwrap());
        }
        
        @Override
        public void setSessionParameters(@NonNull final CaptureRequest captureRequest) {
            \u30070\u3007O0088o.\u3007080(this.mObject, captureRequest);
        }
    }
    
    @RequiresApi(21)
    private static final class SessionConfigurationCompatBaseImpl implements SessionConfigurationCompatImpl
    {
        private final Executor mExecutor;
        private InputConfigurationCompat mInputConfig;
        private final List<OutputConfigurationCompat> mOutputConfigurations;
        private CaptureRequest mSessionParameters;
        private final int mSessionType;
        private final CameraCaptureSession$StateCallback mStateCallback;
        
        SessionConfigurationCompatBaseImpl(final int mSessionType, @NonNull final List<OutputConfigurationCompat> c, @NonNull final Executor mExecutor, @NonNull final CameraCaptureSession$StateCallback mStateCallback) {
            this.mInputConfig = null;
            this.mSessionParameters = null;
            this.mSessionType = mSessionType;
            this.mOutputConfigurations = Collections.unmodifiableList((List<? extends OutputConfigurationCompat>)new ArrayList<OutputConfigurationCompat>(c));
            this.mStateCallback = mStateCallback;
            this.mExecutor = mExecutor;
        }
        
        @Override
        public boolean equals(@Nullable final Object o) {
            if (this == o) {
                return true;
            }
            if (o instanceof SessionConfigurationCompatBaseImpl) {
                final SessionConfigurationCompatBaseImpl sessionConfigurationCompatBaseImpl = (SessionConfigurationCompatBaseImpl)o;
                if (Objects.equals(this.mInputConfig, sessionConfigurationCompatBaseImpl.mInputConfig) && this.mSessionType == sessionConfigurationCompatBaseImpl.mSessionType) {
                    if (this.mOutputConfigurations.size() == sessionConfigurationCompatBaseImpl.mOutputConfigurations.size()) {
                        for (int i = 0; i < this.mOutputConfigurations.size(); ++i) {
                            if (!this.mOutputConfigurations.get(i).equals(sessionConfigurationCompatBaseImpl.mOutputConfigurations.get(i))) {
                                return false;
                            }
                        }
                        return true;
                    }
                }
            }
            return false;
        }
        
        @NonNull
        @Override
        public Executor getExecutor() {
            return this.mExecutor;
        }
        
        @Nullable
        @Override
        public InputConfigurationCompat getInputConfiguration() {
            return this.mInputConfig;
        }
        
        @NonNull
        @Override
        public List<OutputConfigurationCompat> getOutputConfigurations() {
            return this.mOutputConfigurations;
        }
        
        @Nullable
        @Override
        public Object getSessionConfiguration() {
            return null;
        }
        
        @Override
        public CaptureRequest getSessionParameters() {
            return this.mSessionParameters;
        }
        
        @Override
        public int getSessionType() {
            return this.mSessionType;
        }
        
        @NonNull
        @Override
        public CameraCaptureSession$StateCallback getStateCallback() {
            return this.mStateCallback;
        }
        
        @Override
        public int hashCode() {
            final int n = this.mOutputConfigurations.hashCode() ^ 0x1F;
            final InputConfigurationCompat mInputConfig = this.mInputConfig;
            int hashCode;
            if (mInputConfig == null) {
                hashCode = 0;
            }
            else {
                hashCode = mInputConfig.hashCode();
            }
            final int n2 = hashCode ^ (n << 5) - n;
            return this.mSessionType ^ (n2 << 5) - n2;
        }
        
        @Override
        public void setInputConfiguration(@NonNull final InputConfigurationCompat mInputConfig) {
            if (this.mSessionType != 1) {
                this.mInputConfig = mInputConfig;
                return;
            }
            throw new UnsupportedOperationException("Method not supported for high speed session types");
        }
        
        @Override
        public void setSessionParameters(@NonNull final CaptureRequest mSessionParameters) {
            this.mSessionParameters = mSessionParameters;
        }
    }
    
    private interface SessionConfigurationCompatImpl
    {
        @NonNull
        Executor getExecutor();
        
        @Nullable
        InputConfigurationCompat getInputConfiguration();
        
        @NonNull
        List<OutputConfigurationCompat> getOutputConfigurations();
        
        @Nullable
        Object getSessionConfiguration();
        
        @Nullable
        CaptureRequest getSessionParameters();
        
        int getSessionType();
        
        @NonNull
        CameraCaptureSession$StateCallback getStateCallback();
        
        void setInputConfiguration(@NonNull final InputConfigurationCompat p0);
        
        void setSessionParameters(@NonNull final CaptureRequest p0);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface SessionMode {
    }
}

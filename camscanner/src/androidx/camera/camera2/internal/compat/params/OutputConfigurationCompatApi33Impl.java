// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.params;

import java.util.List;
import androidx.annotation.Nullable;
import android.hardware.camera2.params.OutputConfiguration;
import androidx.annotation.NonNull;
import android.view.Surface;
import androidx.annotation.RequiresApi;

@RequiresApi(33)
public class OutputConfigurationCompatApi33Impl extends OutputConfigurationCompatApi28Impl
{
    OutputConfigurationCompatApi33Impl(final int n, @NonNull final Surface surface) {
        this(new OutputConfiguration(n, surface));
    }
    
    OutputConfigurationCompatApi33Impl(@NonNull final Surface surface) {
        super(new OutputConfiguration(surface));
    }
    
    OutputConfigurationCompatApi33Impl(@NonNull final Object o) {
        super(o);
    }
    
    @RequiresApi(33)
    static OutputConfigurationCompatApi33Impl wrap(@NonNull final OutputConfiguration outputConfiguration) {
        return new OutputConfigurationCompatApi33Impl(outputConfiguration);
    }
    
    @Override
    public long getStreamUseCase() {
        return ((OutputConfiguration)this.getOutputConfiguration()).getStreamUseCase();
    }
    
    @Override
    public void setStreamUseCase(final long streamUseCase) {
        if (streamUseCase == -1L) {
            return;
        }
        ((OutputConfiguration)this.getOutputConfiguration()).setStreamUseCase(streamUseCase);
    }
}

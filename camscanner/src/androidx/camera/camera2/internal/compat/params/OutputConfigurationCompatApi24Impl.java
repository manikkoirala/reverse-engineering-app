// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.params;

import java.util.Objects;
import java.util.Collections;
import java.util.List;
import androidx.annotation.Nullable;
import androidx.core.util.Preconditions;
import android.hardware.camera2.params.OutputConfiguration;
import androidx.annotation.NonNull;
import android.view.Surface;
import androidx.annotation.RequiresApi;

@RequiresApi(24)
class OutputConfigurationCompatApi24Impl extends OutputConfigurationCompatBaseImpl
{
    OutputConfigurationCompatApi24Impl(final int n, @NonNull final Surface surface) {
        this(new OutputConfigurationParamsApi24(new OutputConfiguration(n, surface)));
    }
    
    OutputConfigurationCompatApi24Impl(@NonNull final Surface surface) {
        this(new OutputConfigurationParamsApi24(new OutputConfiguration(surface)));
    }
    
    OutputConfigurationCompatApi24Impl(@NonNull final Object o) {
        super(o);
    }
    
    @RequiresApi(24)
    static OutputConfigurationCompatApi24Impl wrap(@NonNull final OutputConfiguration outputConfiguration) {
        return new OutputConfigurationCompatApi24Impl(new OutputConfigurationParamsApi24(outputConfiguration));
    }
    
    @Override
    public void enableSurfaceSharing() {
        ((OutputConfigurationParamsApi24)super.mObject).mIsShared = true;
    }
    
    @NonNull
    @Override
    public Object getOutputConfiguration() {
        Preconditions.checkArgument(super.mObject instanceof OutputConfigurationParamsApi24);
        return ((OutputConfigurationParamsApi24)super.mObject).mOutputConfiguration;
    }
    
    @Nullable
    @Override
    public String getPhysicalCameraId() {
        return ((OutputConfigurationParamsApi24)super.mObject).mPhysicalCameraId;
    }
    
    @Nullable
    @Override
    public Surface getSurface() {
        return oO80.\u3007080((OutputConfiguration)this.getOutputConfiguration());
    }
    
    @Override
    public int getSurfaceGroupId() {
        return \u3007\u3007888.\u3007080((OutputConfiguration)this.getOutputConfiguration());
    }
    
    @NonNull
    @Override
    public List<Surface> getSurfaces() {
        return Collections.singletonList(this.getSurface());
    }
    
    @Override
    boolean isSurfaceSharingEnabled() {
        return ((OutputConfigurationParamsApi24)super.mObject).mIsShared;
    }
    
    @Override
    public void setPhysicalCameraId(@Nullable final String mPhysicalCameraId) {
        ((OutputConfigurationParamsApi24)super.mObject).mPhysicalCameraId = mPhysicalCameraId;
    }
    
    private static final class OutputConfigurationParamsApi24
    {
        boolean mIsShared;
        @NonNull
        final OutputConfiguration mOutputConfiguration;
        @Nullable
        String mPhysicalCameraId;
        
        OutputConfigurationParamsApi24(@NonNull final OutputConfiguration mOutputConfiguration) {
            this.mOutputConfiguration = mOutputConfiguration;
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof OutputConfigurationParamsApi24;
            final boolean b2 = false;
            if (!b) {
                return false;
            }
            final OutputConfigurationParamsApi24 outputConfigurationParamsApi24 = (OutputConfigurationParamsApi24)o;
            boolean b3 = b2;
            if (Objects.equals(this.mOutputConfiguration, outputConfigurationParamsApi24.mOutputConfiguration)) {
                b3 = b2;
                if (this.mIsShared == outputConfigurationParamsApi24.mIsShared) {
                    b3 = b2;
                    if (Objects.equals(this.mPhysicalCameraId, outputConfigurationParamsApi24.mPhysicalCameraId)) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public int hashCode() {
            final int n = \u300780\u3007808\u3007O.\u3007080(this.mOutputConfiguration) ^ 0x1F;
            final int n2 = (this.mIsShared ? 1 : 0) ^ (n << 5) - n;
            final String mPhysicalCameraId = this.mPhysicalCameraId;
            int hashCode;
            if (mPhysicalCameraId == null) {
                hashCode = 0;
            }
            else {
                hashCode = mPhysicalCameraId.hashCode();
            }
            return hashCode ^ (n2 << 5) - n2;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.params;

import androidx.annotation.Nullable;
import androidx.core.util.Preconditions;
import android.hardware.camera2.params.OutputConfiguration;
import androidx.annotation.NonNull;
import android.view.Surface;
import androidx.annotation.RequiresApi;

@RequiresApi(28)
class OutputConfigurationCompatApi28Impl extends OutputConfigurationCompatApi26Impl
{
    OutputConfigurationCompatApi28Impl(final int n, @NonNull final Surface surface) {
        this(new OutputConfiguration(n, surface));
    }
    
    OutputConfigurationCompatApi28Impl(@NonNull final Surface surface) {
        super(new OutputConfiguration(surface));
    }
    
    OutputConfigurationCompatApi28Impl(@NonNull final Object o) {
        super(o);
    }
    
    @RequiresApi(28)
    static OutputConfigurationCompatApi28Impl wrap(@NonNull final OutputConfiguration outputConfiguration) {
        return new OutputConfigurationCompatApi28Impl(outputConfiguration);
    }
    
    @Override
    public int getMaxSharedSurfaceCount() {
        return OO0o\u3007\u3007.\u3007080((OutputConfiguration)this.getOutputConfiguration());
    }
    
    @NonNull
    @Override
    public Object getOutputConfiguration() {
        Preconditions.checkArgument(super.mObject instanceof OutputConfiguration);
        return super.mObject;
    }
    
    @Nullable
    @Override
    public String getPhysicalCameraId() {
        return null;
    }
    
    @Override
    public void removeSurface(@NonNull final Surface surface) {
        \u3007\u3007808\u3007.\u3007080((OutputConfiguration)this.getOutputConfiguration(), surface);
    }
    
    @Override
    public void setPhysicalCameraId(@Nullable final String s) {
        Oooo8o0\u3007.\u3007080((OutputConfiguration)this.getOutputConfiguration(), s);
    }
}

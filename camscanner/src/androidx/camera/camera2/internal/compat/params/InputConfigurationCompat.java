// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.params;

import android.annotation.SuppressLint;
import androidx.annotation.VisibleForTesting;
import java.util.Objects;
import android.hardware.camera2.params.InputConfiguration;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.os.Build$VERSION;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class InputConfigurationCompat
{
    private final InputConfigurationCompatImpl mImpl;
    
    public InputConfigurationCompat(final int n, final int n2, final int n3) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 31) {
            this.mImpl = (InputConfigurationCompatImpl)new InputConfigurationCompatApi31Impl(n, n2, n3);
        }
        else if (sdk_INT >= 23) {
            this.mImpl = (InputConfigurationCompatImpl)new InputConfigurationCompatApi23Impl(n, n2, n3);
        }
        else {
            this.mImpl = (InputConfigurationCompatImpl)new InputConfigurationCompatBaseImpl(n, n2, n3);
        }
    }
    
    private InputConfigurationCompat(@NonNull final InputConfigurationCompatImpl mImpl) {
        this.mImpl = mImpl;
    }
    
    @Nullable
    public static InputConfigurationCompat wrap(@Nullable final Object o) {
        if (o == null) {
            return null;
        }
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT < 23) {
            return null;
        }
        if (sdk_INT >= 31) {
            return new InputConfigurationCompat((InputConfigurationCompatImpl)new InputConfigurationCompatApi31Impl(o));
        }
        return new InputConfigurationCompat((InputConfigurationCompatImpl)new InputConfigurationCompatApi23Impl(o));
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof InputConfigurationCompat && this.mImpl.equals(((InputConfigurationCompat)o).mImpl);
    }
    
    public int getFormat() {
        return this.mImpl.getFormat();
    }
    
    public int getHeight() {
        return this.mImpl.getHeight();
    }
    
    public int getWidth() {
        return this.mImpl.getWidth();
    }
    
    @Override
    public int hashCode() {
        return this.mImpl.hashCode();
    }
    
    public boolean isMultiResolution() {
        return this.mImpl.isMultiResolution();
    }
    
    @NonNull
    @Override
    public String toString() {
        return this.mImpl.toString();
    }
    
    @Nullable
    public Object unwrap() {
        return this.mImpl.getInputConfiguration();
    }
    
    @RequiresApi(23)
    private static class InputConfigurationCompatApi23Impl implements InputConfigurationCompatImpl
    {
        private final InputConfiguration mObject;
        
        InputConfigurationCompatApi23Impl(final int n, final int n2, final int n3) {
            this(new InputConfiguration(n, n2, n3));
        }
        
        InputConfigurationCompatApi23Impl(@NonNull final Object o) {
            this.mObject = (InputConfiguration)o;
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof InputConfigurationCompatImpl && Objects.equals(this.mObject, ((InputConfigurationCompatImpl)o).getInputConfiguration());
        }
        
        @Override
        public int getFormat() {
            return Oo08.\u3007080(this.mObject);
        }
        
        @Override
        public int getHeight() {
            return \u3007o00\u3007\u3007Oo.\u3007080(this.mObject);
        }
        
        @Nullable
        @Override
        public Object getInputConfiguration() {
            return this.mObject;
        }
        
        @Override
        public int getWidth() {
            return O8.\u3007080(this.mObject);
        }
        
        @Override
        public int hashCode() {
            return \u3007o\u3007.\u3007080(this.mObject);
        }
        
        @Override
        public boolean isMultiResolution() {
            return false;
        }
        
        @NonNull
        @Override
        public String toString() {
            return \u3007080.\u3007080(this.mObject);
        }
    }
    
    @RequiresApi(31)
    private static final class InputConfigurationCompatApi31Impl extends InputConfigurationCompatApi23Impl
    {
        InputConfigurationCompatApi31Impl(final int n, final int n2, final int n3) {
            super(n, n2, n3);
        }
        
        InputConfigurationCompatApi31Impl(@NonNull final Object o) {
            super(o);
        }
        
        @Override
        public boolean isMultiResolution() {
            return o\u30070.\u3007080((InputConfiguration)((InputConfigurationCompatApi23Impl)this).getInputConfiguration());
        }
    }
    
    @VisibleForTesting
    static final class InputConfigurationCompatBaseImpl implements InputConfigurationCompatImpl
    {
        private final int mFormat;
        private final int mHeight;
        private final int mWidth;
        
        InputConfigurationCompatBaseImpl(final int mWidth, final int mHeight, final int mFormat) {
            this.mWidth = mWidth;
            this.mHeight = mHeight;
            this.mFormat = mFormat;
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof InputConfigurationCompatBaseImpl;
            final boolean b2 = false;
            if (!b) {
                return false;
            }
            final InputConfigurationCompatBaseImpl inputConfigurationCompatBaseImpl = (InputConfigurationCompatBaseImpl)o;
            boolean b3 = b2;
            if (inputConfigurationCompatBaseImpl.getWidth() == this.mWidth) {
                b3 = b2;
                if (inputConfigurationCompatBaseImpl.getHeight() == this.mHeight) {
                    b3 = b2;
                    if (inputConfigurationCompatBaseImpl.getFormat() == this.mFormat) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public int getFormat() {
            return this.mFormat;
        }
        
        @Override
        public int getHeight() {
            return this.mHeight;
        }
        
        @Override
        public Object getInputConfiguration() {
            return null;
        }
        
        @Override
        public int getWidth() {
            return this.mWidth;
        }
        
        @Override
        public int hashCode() {
            final int n = 0x1F ^ this.mWidth;
            final int n2 = this.mHeight ^ (n << 5) - n;
            return this.mFormat ^ (n2 << 5) - n2;
        }
        
        @Override
        public boolean isMultiResolution() {
            return false;
        }
        
        @SuppressLint({ "DefaultLocale" })
        @NonNull
        @Override
        public String toString() {
            return String.format("InputConfiguration(w:%d, h:%d, format:%d)", this.mWidth, this.mHeight, this.mFormat);
        }
    }
    
    private interface InputConfigurationCompatImpl
    {
        int getFormat();
        
        int getHeight();
        
        @Nullable
        Object getInputConfiguration();
        
        int getWidth();
        
        boolean isMultiResolution();
    }
}

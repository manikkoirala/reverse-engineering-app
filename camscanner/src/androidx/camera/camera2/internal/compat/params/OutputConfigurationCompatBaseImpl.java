// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.params;

import android.annotation.SuppressLint;
import java.lang.reflect.Method;
import androidx.camera.core.Logger;
import java.lang.reflect.InvocationTargetException;
import android.os.Build$VERSION;
import java.util.Collections;
import android.util.Size;
import java.util.List;
import androidx.annotation.Nullable;
import java.util.Objects;
import androidx.core.util.Preconditions;
import androidx.annotation.NonNull;
import android.view.Surface;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
class OutputConfigurationCompatBaseImpl implements OutputConfigurationCompatImpl
{
    static final String TAG = "OutputConfigCompat";
    final Object mObject;
    
    OutputConfigurationCompatBaseImpl(@NonNull final Surface surface) {
        this.mObject = new OutputConfigurationParamsApi21(surface);
    }
    
    OutputConfigurationCompatBaseImpl(@NonNull final Object mObject) {
        this.mObject = mObject;
    }
    
    @Override
    public void addSurface(@NonNull final Surface surface) {
        Preconditions.checkNotNull(surface, "Surface must not be null");
        if (this.getSurface() == surface) {
            throw new IllegalStateException("Surface is already added!");
        }
        if (!this.isSurfaceSharingEnabled()) {
            throw new IllegalStateException("Cannot have 2 surfaces for a non-sharing configuration");
        }
        throw new IllegalArgumentException("Exceeds maximum number of surfaces");
    }
    
    @Override
    public void enableSurfaceSharing() {
        ((OutputConfigurationParamsApi21)this.mObject).mIsShared = true;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof OutputConfigurationCompatBaseImpl && Objects.equals(this.mObject, ((OutputConfigurationCompatBaseImpl)o).mObject);
    }
    
    @Override
    public int getMaxSharedSurfaceCount() {
        return 1;
    }
    
    @Nullable
    @Override
    public Object getOutputConfiguration() {
        return null;
    }
    
    @Nullable
    @Override
    public String getPhysicalCameraId() {
        return ((OutputConfigurationParamsApi21)this.mObject).mPhysicalCameraId;
    }
    
    @Override
    public long getStreamUseCase() {
        return -1L;
    }
    
    @Nullable
    @Override
    public Surface getSurface() {
        final List<Surface> mSurfaces = ((OutputConfigurationParamsApi21)this.mObject).mSurfaces;
        if (mSurfaces.size() == 0) {
            return null;
        }
        return mSurfaces.get(0);
    }
    
    @Override
    public int getSurfaceGroupId() {
        return -1;
    }
    
    @NonNull
    @Override
    public List<Surface> getSurfaces() {
        return ((OutputConfigurationParamsApi21)this.mObject).mSurfaces;
    }
    
    @Override
    public int hashCode() {
        return this.mObject.hashCode();
    }
    
    boolean isSurfaceSharingEnabled() {
        return ((OutputConfigurationParamsApi21)this.mObject).mIsShared;
    }
    
    @Override
    public void removeSurface(@NonNull final Surface surface) {
        if (this.getSurface() == surface) {
            throw new IllegalArgumentException("Cannot remove surface associated with this output configuration");
        }
        throw new IllegalArgumentException("Surface is not part of this output configuration");
    }
    
    @Override
    public void setPhysicalCameraId(@Nullable final String mPhysicalCameraId) {
        ((OutputConfigurationParamsApi21)this.mObject).mPhysicalCameraId = mPhysicalCameraId;
    }
    
    @Override
    public void setStreamUseCase(final long n) {
    }
    
    @RequiresApi(21)
    private static final class OutputConfigurationParamsApi21
    {
        private static final String DETECT_SURFACE_TYPE_METHOD = "detectSurfaceType";
        private static final String GET_GENERATION_ID_METHOD = "getGenerationId";
        private static final String GET_SURFACE_SIZE_METHOD = "getSurfaceSize";
        private static final String LEGACY_CAMERA_DEVICE_CLASS = "android.hardware.camera2.legacy.LegacyCameraDevice";
        static final int MAX_SURFACES_COUNT = 1;
        final int mConfiguredFormat;
        final int mConfiguredGenerationId;
        final Size mConfiguredSize;
        boolean mIsShared;
        @Nullable
        String mPhysicalCameraId;
        final List<Surface> mSurfaces;
        
        OutputConfigurationParamsApi21(@NonNull final Surface o) {
            this.mIsShared = false;
            Preconditions.checkNotNull(o, "Surface must not be null");
            this.mSurfaces = Collections.singletonList(o);
            this.mConfiguredSize = getSurfaceSize(o);
            this.mConfiguredFormat = getSurfaceFormat(o);
            this.mConfiguredGenerationId = getSurfaceGenerationId(o);
        }
        
        @SuppressLint({ "BlockedPrivateApi", "BanUncheckedReflection" })
        private static int getSurfaceFormat(@NonNull final Surface ex) {
            try {
                final Method declaredMethod = Class.forName("android.hardware.camera2.legacy.LegacyCameraDevice").getDeclaredMethod("detectSurfaceType", Surface.class);
                if (Build$VERSION.SDK_INT < 22) {
                    declaredMethod.setAccessible(true);
                }
                return (int)declaredMethod.invoke(null, ex);
            }
            catch (final InvocationTargetException ex) {}
            catch (final IllegalAccessException ex) {}
            catch (final NoSuchMethodException ex) {}
            catch (final ClassNotFoundException ex2) {}
            Logger.e("OutputConfigCompat", "Unable to retrieve surface format.", ex);
            return 0;
        }
        
        @SuppressLint({ "SoonBlockedPrivateApi", "BlockedPrivateApi", "BanUncheckedReflection" })
        private static int getSurfaceGenerationId(@NonNull final Surface obj) {
            try {
                return (int)Surface.class.getDeclaredMethod("getGenerationId", (Class<?>[])new Class[0]).invoke(obj, new Object[0]);
            }
            catch (final InvocationTargetException obj) {}
            catch (final IllegalAccessException obj) {}
            catch (final NoSuchMethodException ex) {}
            Logger.e("OutputConfigCompat", "Unable to retrieve surface generation id.", obj);
            return -1;
        }
        
        @SuppressLint({ "BlockedPrivateApi", "BanUncheckedReflection" })
        private static Size getSurfaceSize(@NonNull Surface ex) {
            try {
                final Method declaredMethod = Class.forName("android.hardware.camera2.legacy.LegacyCameraDevice").getDeclaredMethod("getSurfaceSize", Surface.class);
                declaredMethod.setAccessible(true);
                ex = (InvocationTargetException)declaredMethod.invoke(null, ex);
                return (Size)ex;
            }
            catch (final InvocationTargetException ex) {}
            catch (final IllegalAccessException ex) {}
            catch (final NoSuchMethodException ex) {}
            catch (final ClassNotFoundException ex2) {}
            Logger.e("OutputConfigCompat", "Unable to retrieve surface size.", ex);
            return null;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (!(o instanceof OutputConfigurationParamsApi21)) {
                return false;
            }
            final OutputConfigurationParamsApi21 outputConfigurationParamsApi21 = (OutputConfigurationParamsApi21)o;
            if (this.mConfiguredSize.equals((Object)outputConfigurationParamsApi21.mConfiguredSize) && this.mConfiguredFormat == outputConfigurationParamsApi21.mConfiguredFormat && this.mConfiguredGenerationId == outputConfigurationParamsApi21.mConfiguredGenerationId && this.mIsShared == outputConfigurationParamsApi21.mIsShared && Objects.equals(this.mPhysicalCameraId, outputConfigurationParamsApi21.mPhysicalCameraId)) {
                for (int min = Math.min(this.mSurfaces.size(), outputConfigurationParamsApi21.mSurfaces.size()), i = 0; i < min; ++i) {
                    if (this.mSurfaces.get(i) != outputConfigurationParamsApi21.mSurfaces.get(i)) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            final int n = this.mSurfaces.hashCode() ^ 0x1F;
            final int n2 = this.mConfiguredGenerationId ^ (n << 5) - n;
            final int n3 = this.mConfiguredSize.hashCode() ^ (n2 << 5) - n2;
            final int n4 = this.mConfiguredFormat ^ (n3 << 5) - n3;
            final int n5 = (this.mIsShared ? 1 : 0) ^ (n4 << 5) - n4;
            final String mPhysicalCameraId = this.mPhysicalCameraId;
            int hashCode;
            if (mPhysicalCameraId == null) {
                hashCode = 0;
            }
            else {
                hashCode = mPhysicalCameraId.hashCode();
            }
            return hashCode ^ (n5 << 5) - n5;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.params;

import java.util.Objects;
import androidx.annotation.Nullable;
import androidx.core.util.Preconditions;
import androidx.camera.core.Logger;
import java.util.List;
import android.annotation.SuppressLint;
import java.lang.reflect.Field;
import android.hardware.camera2.params.OutputConfiguration;
import androidx.annotation.NonNull;
import android.view.Surface;
import androidx.annotation.RequiresApi;

@RequiresApi(26)
class OutputConfigurationCompatApi26Impl extends OutputConfigurationCompatApi24Impl
{
    private static final String MAX_SHARED_SURFACES_COUNT_FIELD = "MAX_SURFACES_COUNT";
    private static final String SURFACES_FIELD = "mSurfaces";
    
    OutputConfigurationCompatApi26Impl(final int n, @NonNull final Surface surface) {
        this(new OutputConfigurationParamsApi26(new OutputConfiguration(n, surface)));
    }
    
    OutputConfigurationCompatApi26Impl(@NonNull final Surface surface) {
        this(new OutputConfigurationParamsApi26(new OutputConfiguration(surface)));
    }
    
    OutputConfigurationCompatApi26Impl(@NonNull final Object o) {
        super(o);
    }
    
    @SuppressLint({ "SoonBlockedPrivateApi" })
    private static int getMaxSharedSurfaceCountApi26() throws NoSuchFieldException, IllegalAccessException {
        final Field declaredField = OutputConfiguration.class.getDeclaredField("MAX_SURFACES_COUNT");
        declaredField.setAccessible(true);
        return declaredField.getInt(null);
    }
    
    @SuppressLint({ "SoonBlockedPrivateApi" })
    private static List<Surface> getMutableSurfaceListApi26(final OutputConfiguration obj) throws NoSuchFieldException, IllegalAccessException {
        final Field declaredField = OutputConfiguration.class.getDeclaredField("mSurfaces");
        declaredField.setAccessible(true);
        return (List)declaredField.get(obj);
    }
    
    @RequiresApi(26)
    static OutputConfigurationCompatApi26Impl wrap(@NonNull final OutputConfiguration outputConfiguration) {
        return new OutputConfigurationCompatApi26Impl(new OutputConfigurationParamsApi26(outputConfiguration));
    }
    
    @Override
    public void addSurface(@NonNull final Surface surface) {
        \u3007O8o08O.\u3007080((OutputConfiguration)this.getOutputConfiguration(), surface);
    }
    
    @Override
    public void enableSurfaceSharing() {
        \u30078o8o\u3007.\u3007080((OutputConfiguration)this.getOutputConfiguration());
    }
    
    @Override
    public int getMaxSharedSurfaceCount() {
        try {
            return getMaxSharedSurfaceCountApi26();
        }
        catch (final IllegalAccessException ex) {}
        catch (final NoSuchFieldException ex2) {}
        final IllegalAccessException ex;
        Logger.e("OutputConfigCompat", "Unable to retrieve max shared surface count.", ex);
        return super.getMaxSharedSurfaceCount();
    }
    
    @NonNull
    @Override
    public Object getOutputConfiguration() {
        Preconditions.checkArgument(super.mObject instanceof OutputConfigurationParamsApi26);
        return ((OutputConfigurationParamsApi26)super.mObject).mOutputConfiguration;
    }
    
    @Nullable
    @Override
    public String getPhysicalCameraId() {
        return ((OutputConfigurationParamsApi26)super.mObject).mPhysicalCameraId;
    }
    
    @NonNull
    @Override
    public List<Surface> getSurfaces() {
        return OO0o\u3007\u3007\u3007\u30070.\u3007080((OutputConfiguration)this.getOutputConfiguration());
    }
    
    @Override
    final boolean isSurfaceSharingEnabled() {
        throw new AssertionError((Object)"isSurfaceSharingEnabled() should not be called on API >= 26");
    }
    
    @Override
    public void removeSurface(@NonNull Surface ex) {
        if (this.getSurface() != ex) {
            try {
                if (getMutableSurfaceListApi26((OutputConfiguration)this.getOutputConfiguration()).remove(ex)) {
                    return;
                }
                ex = (NoSuchFieldException)new IllegalArgumentException("Surface is not part of this output configuration");
                throw ex;
            }
            catch (final NoSuchFieldException ex) {}
            catch (final IllegalAccessException ex2) {}
            Logger.e("OutputConfigCompat", "Unable to remove surface from this output configuration.", ex);
            return;
        }
        throw new IllegalArgumentException("Cannot remove surface associated with this output configuration");
    }
    
    @Override
    public void setPhysicalCameraId(@Nullable final String mPhysicalCameraId) {
        ((OutputConfigurationParamsApi26)super.mObject).mPhysicalCameraId = mPhysicalCameraId;
    }
    
    private static final class OutputConfigurationParamsApi26
    {
        @NonNull
        final OutputConfiguration mOutputConfiguration;
        @Nullable
        String mPhysicalCameraId;
        
        OutputConfigurationParamsApi26(@NonNull final OutputConfiguration mOutputConfiguration) {
            this.mOutputConfiguration = mOutputConfiguration;
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof OutputConfigurationParamsApi26;
            final boolean b2 = false;
            if (!b) {
                return false;
            }
            final OutputConfigurationParamsApi26 outputConfigurationParamsApi26 = (OutputConfigurationParamsApi26)o;
            boolean b3 = b2;
            if (Objects.equals(this.mOutputConfiguration, outputConfigurationParamsApi26.mOutputConfiguration)) {
                b3 = b2;
                if (Objects.equals(this.mPhysicalCameraId, outputConfigurationParamsApi26.mPhysicalCameraId)) {
                    b3 = true;
                }
            }
            return b3;
        }
        
        @Override
        public int hashCode() {
            final int n = \u300780\u3007808\u3007O.\u3007080(this.mOutputConfiguration) ^ 0x1F;
            final String mPhysicalCameraId = this.mPhysicalCameraId;
            int hashCode;
            if (mPhysicalCameraId == null) {
                hashCode = 0;
            }
            else {
                hashCode = mPhysicalCameraId.hashCode();
            }
            return hashCode ^ (n << 5) - n;
        }
    }
}

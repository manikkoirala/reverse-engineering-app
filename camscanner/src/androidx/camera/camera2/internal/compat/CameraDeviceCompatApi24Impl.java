// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import androidx.camera.camera2.internal.compat.params.InputConfigurationCompat;
import androidx.camera.camera2.internal.compat.params.OutputConfigurationCompat;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession$StateCallback;
import java.util.List;
import android.hardware.camera2.params.InputConfiguration;
import androidx.core.util.Preconditions;
import androidx.camera.camera2.internal.compat.params.SessionConfigurationCompat;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.hardware.camera2.CameraDevice;
import androidx.annotation.RequiresApi;

@RequiresApi(24)
class CameraDeviceCompatApi24Impl extends CameraDeviceCompatApi23Impl
{
    CameraDeviceCompatApi24Impl(@NonNull final CameraDevice cameraDevice, @Nullable final Object o) {
        super(cameraDevice, o);
    }
    
    static CameraDeviceCompatApi24Impl create(@NonNull final CameraDevice cameraDevice, @NonNull final Handler handler) {
        return new CameraDeviceCompatApi24Impl(cameraDevice, new CameraDeviceCompatParamsApi21(handler));
    }
    
    @Override
    public void createCaptureSession(@NonNull final SessionConfigurationCompat sessionConfigurationCompat) throws CameraAccessExceptionCompat {
        CameraDeviceCompatBaseImpl.checkPreconditions(super.mCameraDevice, sessionConfigurationCompat);
        final CameraCaptureSessionCompat.StateCallbackExecutorWrapper stateCallbackExecutorWrapper = new CameraCaptureSessionCompat.StateCallbackExecutorWrapper(sessionConfigurationCompat.getExecutor(), sessionConfigurationCompat.getStateCallback());
        final List<OutputConfigurationCompat> outputConfigurations = sessionConfigurationCompat.getOutputConfigurations();
        final Handler mCompatHandler = Preconditions.checkNotNull(super.mImplParams).mCompatHandler;
        final InputConfigurationCompat inputConfiguration = sessionConfigurationCompat.getInputConfiguration();
        Label_0090: {
            if (inputConfiguration == null) {
                break Label_0090;
            }
            try {
                final InputConfiguration inputConfiguration2 = (InputConfiguration)inputConfiguration.unwrap();
                Preconditions.checkNotNull(inputConfiguration2);
                o\u3007\u30070\u3007.\u3007080(super.mCameraDevice, inputConfiguration2, (List)SessionConfigurationCompat.transformFromCompat(outputConfigurations), (CameraCaptureSession$StateCallback)stateCallbackExecutorWrapper, mCompatHandler);
                return;
                iftrue(Label_0115:)(sessionConfigurationCompat.getSessionType() != 1);
                Block_3: {
                    break Block_3;
                    Label_0115: {
                        OOO\u3007O0.\u3007080(super.mCameraDevice, (List)SessionConfigurationCompat.transformFromCompat(outputConfigurations), (CameraCaptureSession$StateCallback)stateCallbackExecutorWrapper, mCompatHandler);
                    }
                    return;
                }
                \u30070000OOO.\u3007080(super.mCameraDevice, (List)CameraDeviceCompatBaseImpl.unpackSurfaces(outputConfigurations), (CameraCaptureSession$StateCallback)stateCallbackExecutorWrapper, mCompatHandler);
            }
            catch (final CameraAccessException ex) {
                throw CameraAccessExceptionCompat.toCameraAccessExceptionCompat(ex);
            }
        }
    }
}

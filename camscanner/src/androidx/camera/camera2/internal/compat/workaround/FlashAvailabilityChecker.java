// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.workaround;

import androidx.camera.camera2.internal.compat.quirk.DeviceQuirks;
import androidx.camera.camera2.internal.compat.quirk.FlashAvailabilityBufferUnderflowQuirk;
import java.nio.BufferUnderflowException;
import androidx.camera.core.Logger;
import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import androidx.annotation.NonNull;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class FlashAvailabilityChecker
{
    private static final String TAG = "FlashAvailability";
    
    private FlashAvailabilityChecker() {
    }
    
    private static boolean checkFlashAvailabilityNormally(@NonNull final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        final Boolean b = cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Boolean>)CameraCharacteristics.FLASH_INFO_AVAILABLE);
        if (b == null) {
            Logger.w("FlashAvailability", "Characteristics did not contain key FLASH_INFO_AVAILABLE. Flash is not available.");
        }
        return b != null && b;
    }
    
    private static boolean checkFlashAvailabilityWithPossibleBufferUnderflow(@NonNull final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        try {
            return checkFlashAvailabilityNormally(cameraCharacteristicsCompat);
        }
        catch (final BufferUnderflowException ex) {
            return false;
        }
    }
    
    public static boolean isFlashAvailable(@NonNull final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        if (DeviceQuirks.get(FlashAvailabilityBufferUnderflowQuirk.class) != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Device has quirk ");
            sb.append(FlashAvailabilityBufferUnderflowQuirk.class.getSimpleName());
            sb.append(". Checking for flash availability safely...");
            Logger.d("FlashAvailability", sb.toString());
            return checkFlashAvailabilityWithPossibleBufferUnderflow(cameraCharacteristicsCompat);
        }
        return checkFlashAvailabilityNormally(cameraCharacteristicsCompat);
    }
}

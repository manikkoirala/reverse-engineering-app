// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.workaround;

import java.util.ArrayList;
import androidx.camera.core.impl.SurfaceCombination;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.camera.camera2.internal.compat.quirk.DeviceQuirks;
import androidx.camera.camera2.internal.compat.quirk.ExtraSupportedSurfaceCombinationsQuirk;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class ExtraSupportedSurfaceCombinationsContainer
{
    private final ExtraSupportedSurfaceCombinationsQuirk mQuirk;
    
    public ExtraSupportedSurfaceCombinationsContainer() {
        this.mQuirk = DeviceQuirks.get(ExtraSupportedSurfaceCombinationsQuirk.class);
    }
    
    @NonNull
    public List<SurfaceCombination> get(@NonNull final String s, final int n) {
        final ExtraSupportedSurfaceCombinationsQuirk mQuirk = this.mQuirk;
        if (mQuirk == null) {
            return new ArrayList<SurfaceCombination>();
        }
        return mQuirk.getExtraSupportedSurfaceCombinations(s, n);
    }
}

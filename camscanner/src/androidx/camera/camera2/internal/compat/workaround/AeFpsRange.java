// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.workaround;

import android.hardware.camera2.CaptureRequest$Key;
import android.hardware.camera2.CaptureRequest;
import androidx.camera.camera2.impl.Camera2ImplConfig;
import androidx.camera.camera2.internal.compat.quirk.AeFpsRangeLegacyQuirk;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.Quirks;
import androidx.annotation.Nullable;
import android.util.Range;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class AeFpsRange
{
    @Nullable
    private final Range<Integer> mAeTargetFpsRange;
    
    public AeFpsRange(@NonNull final Quirks quirks) {
        final AeFpsRangeLegacyQuirk aeFpsRangeLegacyQuirk = quirks.get(AeFpsRangeLegacyQuirk.class);
        if (aeFpsRangeLegacyQuirk == null) {
            this.mAeTargetFpsRange = null;
        }
        else {
            this.mAeTargetFpsRange = aeFpsRangeLegacyQuirk.getRange();
        }
    }
    
    public void addAeFpsRangeOptions(@NonNull final Camera2ImplConfig.Builder builder) {
        final Range<Integer> mAeTargetFpsRange = this.mAeTargetFpsRange;
        if (mAeTargetFpsRange != null) {
            builder.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Range<Integer>>)CaptureRequest.CONTROL_AE_TARGET_FPS_RANGE, mAeTargetFpsRange);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.workaround;

import androidx.camera.core.impl.Quirk;
import androidx.camera.camera2.internal.compat.quirk.AfRegionFlipHorizontallyQuirk;
import android.graphics.PointF;
import androidx.camera.core.MeteringPoint;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.Quirks;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class MeteringRegionCorrection
{
    private final Quirks mCameraQuirks;
    
    public MeteringRegionCorrection(@NonNull final Quirks mCameraQuirks) {
        this.mCameraQuirks = mCameraQuirks;
    }
    
    @NonNull
    public PointF getCorrectedPoint(@NonNull final MeteringPoint meteringPoint, final int n) {
        if (n == 1 && this.mCameraQuirks.contains(AfRegionFlipHorizontallyQuirk.class)) {
            return new PointF(1.0f - meteringPoint.getX(), meteringPoint.getY());
        }
        return new PointF(meteringPoint.getX(), meteringPoint.getY());
    }
}

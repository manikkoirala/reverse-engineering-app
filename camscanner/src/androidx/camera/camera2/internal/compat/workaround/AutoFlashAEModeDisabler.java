// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.workaround;

import androidx.camera.camera2.internal.compat.quirk.DeviceQuirks;
import androidx.camera.camera2.internal.compat.quirk.CrashWhenTakingPhotoWithAutoFlashAEModeQuirk;
import androidx.camera.core.impl.Quirk;
import androidx.camera.camera2.internal.compat.quirk.ImageCaptureFailWithAutoFlashQuirk;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.Quirks;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class AutoFlashAEModeDisabler
{
    private final boolean mIsCrashWhenTakingPhotoWithAutoFlashAEModeQuirkEnabled;
    private final boolean mIsImageCaptureFailWithAutoFlashQuirkEnabled;
    
    public AutoFlashAEModeDisabler(@NonNull final Quirks quirks) {
        this.mIsImageCaptureFailWithAutoFlashQuirkEnabled = quirks.contains(ImageCaptureFailWithAutoFlashQuirk.class);
        this.mIsCrashWhenTakingPhotoWithAutoFlashAEModeQuirkEnabled = (DeviceQuirks.get(CrashWhenTakingPhotoWithAutoFlashAEModeQuirk.class) != null);
    }
    
    public int getCorrectedAeMode(final int n) {
        if (!this.mIsImageCaptureFailWithAutoFlashQuirkEnabled) {
            final int n2 = n;
            if (!this.mIsCrashWhenTakingPhotoWithAutoFlashAEModeQuirkEnabled) {
                return n2;
            }
        }
        int n2;
        if ((n2 = n) == 2) {
            n2 = 1;
        }
        return n2;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.workaround;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Iterator;
import androidx.camera.camera2.internal.SynchronizedCaptureSession;
import java.util.Set;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.Quirks;
import androidx.annotation.Nullable;
import androidx.camera.camera2.internal.compat.quirk.CaptureSessionOnClosedNotCalledQuirk;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class ForceCloseCaptureSession
{
    @Nullable
    private final CaptureSessionOnClosedNotCalledQuirk mCaptureSessionOnClosedNotCalledQuirk;
    
    public ForceCloseCaptureSession(@NonNull final Quirks quirks) {
        this.mCaptureSessionOnClosedNotCalledQuirk = quirks.get(CaptureSessionOnClosedNotCalledQuirk.class);
    }
    
    private void forceOnClosed(@NonNull final Set<SynchronizedCaptureSession> set) {
        for (final SynchronizedCaptureSession synchronizedCaptureSession : set) {
            synchronizedCaptureSession.getStateCallback().onClosed(synchronizedCaptureSession);
        }
    }
    
    private void forceOnConfigureFailed(@NonNull final Set<SynchronizedCaptureSession> set) {
        for (final SynchronizedCaptureSession synchronizedCaptureSession : set) {
            synchronizedCaptureSession.getStateCallback().onConfigureFailed(synchronizedCaptureSession);
        }
    }
    
    public void onSessionConfigured(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession, @NonNull final List<SynchronizedCaptureSession> list, @NonNull final List<SynchronizedCaptureSession> list2, @NonNull final OnConfigured onConfigured) {
        if (this.shouldForceClose()) {
            final LinkedHashSet set = new LinkedHashSet();
            for (final SynchronizedCaptureSession synchronizedCaptureSession2 : list) {
                if (synchronizedCaptureSession2 == synchronizedCaptureSession) {
                    break;
                }
                set.add(synchronizedCaptureSession2);
            }
            this.forceOnConfigureFailed(set);
        }
        onConfigured.run(synchronizedCaptureSession);
        if (this.shouldForceClose()) {
            final LinkedHashSet set2 = new LinkedHashSet();
            for (final SynchronizedCaptureSession synchronizedCaptureSession3 : list2) {
                if (synchronizedCaptureSession3 == synchronizedCaptureSession) {
                    break;
                }
                set2.add(synchronizedCaptureSession3);
            }
            this.forceOnClosed(set2);
        }
    }
    
    public boolean shouldForceClose() {
        return this.mCaptureSessionOnClosedNotCalledQuirk != null;
    }
    
    @FunctionalInterface
    public interface OnConfigured
    {
        void run(@NonNull final SynchronizedCaptureSession p0);
    }
}

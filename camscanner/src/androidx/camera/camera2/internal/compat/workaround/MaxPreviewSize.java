// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.workaround;

import androidx.camera.core.impl.SurfaceConfig;
import androidx.annotation.NonNull;
import android.util.Size;
import androidx.annotation.VisibleForTesting;
import androidx.camera.camera2.internal.compat.quirk.DeviceQuirks;
import androidx.annotation.Nullable;
import androidx.camera.camera2.internal.compat.quirk.ExtraCroppingQuirk;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class MaxPreviewSize
{
    @Nullable
    private final ExtraCroppingQuirk mExtraCroppingQuirk;
    
    public MaxPreviewSize() {
        this(DeviceQuirks.get(ExtraCroppingQuirk.class));
    }
    
    @VisibleForTesting
    MaxPreviewSize(@Nullable final ExtraCroppingQuirk mExtraCroppingQuirk) {
        this.mExtraCroppingQuirk = mExtraCroppingQuirk;
    }
    
    @NonNull
    public Size getMaxPreviewResolution(@NonNull Size size) {
        final ExtraCroppingQuirk mExtraCroppingQuirk = this.mExtraCroppingQuirk;
        if (mExtraCroppingQuirk == null) {
            return size;
        }
        final Size verifiedResolution = mExtraCroppingQuirk.getVerifiedResolution(SurfaceConfig.ConfigType.PRIV);
        if (verifiedResolution == null) {
            return size;
        }
        if (verifiedResolution.getWidth() * verifiedResolution.getHeight() > size.getWidth() * size.getHeight()) {
            size = verifiedResolution;
        }
        return size;
    }
}

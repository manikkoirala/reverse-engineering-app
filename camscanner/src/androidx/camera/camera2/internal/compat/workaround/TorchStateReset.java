// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.workaround;

import java.util.List;
import androidx.camera.camera2.interop.ExperimentalCamera2Interop;
import androidx.annotation.OptIn;
import java.util.Iterator;
import androidx.camera.core.impl.Config;
import android.hardware.camera2.CaptureRequest$Key;
import android.hardware.camera2.CaptureRequest;
import androidx.camera.camera2.impl.Camera2ImplConfig;
import androidx.camera.core.impl.DeferrableSurface;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.CaptureConfig;
import androidx.camera.camera2.internal.compat.quirk.DeviceQuirks;
import androidx.camera.camera2.internal.compat.quirk.TorchIsClosedAfterImageCapturingQuirk;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class TorchStateReset
{
    private final boolean mIsImageCaptureTorchIsClosedQuirkEnabled;
    
    public TorchStateReset() {
        this.mIsImageCaptureTorchIsClosedQuirkEnabled = (DeviceQuirks.get(TorchIsClosedAfterImageCapturingQuirk.class) != null);
    }
    
    @NonNull
    @OptIn(markerClass = { ExperimentalCamera2Interop.class })
    public CaptureConfig createTorchResetRequest(@NonNull final CaptureConfig captureConfig) {
        final CaptureConfig.Builder builder = new CaptureConfig.Builder();
        builder.setTemplateType(captureConfig.getTemplateType());
        final Iterator<DeferrableSurface> iterator = captureConfig.getSurfaces().iterator();
        while (iterator.hasNext()) {
            builder.addSurface(iterator.next());
        }
        builder.addImplementationOptions(captureConfig.getImplementationOptions());
        final Camera2ImplConfig.Builder builder2 = new Camera2ImplConfig.Builder();
        builder2.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.FLASH_MODE, 0);
        builder.addImplementationOptions(builder2.build());
        return builder.build();
    }
    
    public boolean isTorchResetRequired(@NonNull final List<CaptureRequest> list, final boolean b) {
        if (this.mIsImageCaptureTorchIsClosedQuirkEnabled && b) {
            final Iterator<CaptureRequest> iterator = list.iterator();
            while (iterator.hasNext()) {
                final Integer n = (Integer)iterator.next().get(CaptureRequest.FLASH_MODE);
                if (n != null && n == 2) {
                    return true;
                }
            }
        }
        return false;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.workaround;

import java.util.concurrent.Future;
import android.hardware.camera2.CameraAccessException;
import androidx.camera.camera2.internal.Camera2CaptureCallbacks;
import java.util.Iterator;
import androidx.camera.core.impl.utils.futures.AsyncFunction;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.impl.utils.futures.FutureChain;
import java.util.Collection;
import java.util.ArrayList;
import androidx.camera.camera2.internal.SynchronizedCaptureSession;
import androidx.camera.core.impl.DeferrableSurface;
import java.util.List;
import androidx.camera.camera2.internal.compat.params.SessionConfigurationCompat;
import android.hardware.camera2.CameraDevice;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.core.impl.Quirk;
import androidx.camera.camera2.internal.compat.quirk.CaptureSessionStuckQuirk;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CameraCaptureSession;
import androidx.camera.core.impl.Quirks;
import androidx.annotation.NonNull;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class WaitForRepeatingRequestStart
{
    private final CameraCaptureSession$CaptureCallback mCaptureCallback;
    private final boolean mHasCaptureSessionStuckQuirk;
    private boolean mHasSubmittedRepeating;
    private final Object mLock;
    CallbackToFutureAdapter.Completer<Void> mStartStreamingCompleter;
    @NonNull
    private final ListenableFuture<Void> mStartStreamingFuture;
    
    public WaitForRepeatingRequestStart(@NonNull final Quirks quirks) {
        this.mLock = new Object();
        this.mCaptureCallback = new CameraCaptureSession$CaptureCallback() {
            final WaitForRepeatingRequestStart this$0;
            
            public void onCaptureSequenceAborted(@NonNull final CameraCaptureSession cameraCaptureSession, final int n) {
                final CallbackToFutureAdapter.Completer<Void> mStartStreamingCompleter = this.this$0.mStartStreamingCompleter;
                if (mStartStreamingCompleter != null) {
                    mStartStreamingCompleter.setCancelled();
                    this.this$0.mStartStreamingCompleter = null;
                }
            }
            
            public void onCaptureStarted(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, final long n, final long n2) {
                final CallbackToFutureAdapter.Completer<Void> mStartStreamingCompleter = this.this$0.mStartStreamingCompleter;
                if (mStartStreamingCompleter != null) {
                    mStartStreamingCompleter.set(null);
                    this.this$0.mStartStreamingCompleter = null;
                }
            }
        };
        this.mHasCaptureSessionStuckQuirk = quirks.contains(CaptureSessionStuckQuirk.class);
        if (this.shouldWaitRepeatingSubmit()) {
            this.mStartStreamingFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new \u3007o00\u3007\u3007Oo(this));
        }
        else {
            this.mStartStreamingFuture = Futures.immediateFuture((Void)null);
        }
    }
    
    @NonNull
    public ListenableFuture<Void> getStartStreamFuture() {
        return Futures.nonCancellationPropagating(this.mStartStreamingFuture);
    }
    
    public void onSessionEnd() {
        synchronized (this.mLock) {
            if (this.shouldWaitRepeatingSubmit() && !this.mHasSubmittedRepeating) {
                ((Future)this.mStartStreamingFuture).cancel(true);
            }
        }
    }
    
    @NonNull
    public ListenableFuture<Void> openCaptureSession(@NonNull final CameraDevice cameraDevice, @NonNull final SessionConfigurationCompat sessionConfigurationCompat, @NonNull final List<DeferrableSurface> list, @NonNull final List<SynchronizedCaptureSession> list2, @NonNull final OpenCaptureSession openCaptureSession) {
        final ArrayList list3 = new ArrayList();
        final Iterator<SynchronizedCaptureSession> iterator = list2.iterator();
        while (iterator.hasNext()) {
            list3.add(iterator.next().getOpeningBlocker());
        }
        return (ListenableFuture<Void>)FutureChain.from(Futures.successfulAsList((Collection<? extends com.google.common.util.concurrent.ListenableFuture<?>>)list3)).transformAsync((AsyncFunction<? super List<Object>, Object>)new \u3007o\u3007(openCaptureSession, cameraDevice, sessionConfigurationCompat, list), CameraXExecutors.directExecutor());
    }
    
    public int setSingleRepeatingRequest(@NonNull final CaptureRequest captureRequest, @NonNull final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback, @NonNull final SingleRepeatingRequest singleRepeatingRequest) throws CameraAccessException {
        final Object mLock = this.mLock;
        monitorenter(mLock);
        CameraCaptureSession$CaptureCallback comboCallback = cameraCaptureSession$CaptureCallback;
        try {
            if (this.shouldWaitRepeatingSubmit()) {
                comboCallback = Camera2CaptureCallbacks.createComboCallback(this.mCaptureCallback, cameraCaptureSession$CaptureCallback);
                this.mHasSubmittedRepeating = true;
            }
            return singleRepeatingRequest.run(captureRequest, comboCallback);
        }
        finally {
            monitorexit(mLock);
        }
    }
    
    public boolean shouldWaitRepeatingSubmit() {
        return this.mHasCaptureSessionStuckQuirk;
    }
    
    @FunctionalInterface
    public interface OpenCaptureSession
    {
        @NonNull
        ListenableFuture<Void> run(@NonNull final CameraDevice p0, @NonNull final SessionConfigurationCompat p1, @NonNull final List<DeferrableSurface> p2);
    }
    
    @FunctionalInterface
    public interface SingleRepeatingRequest
    {
        int run(@NonNull final CaptureRequest p0, @NonNull final CameraCaptureSession$CaptureCallback p1) throws CameraAccessException;
    }
}

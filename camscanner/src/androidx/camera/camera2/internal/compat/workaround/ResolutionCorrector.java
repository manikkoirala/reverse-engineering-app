// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.workaround;

import java.util.Iterator;
import java.util.ArrayList;
import android.util.Size;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.SurfaceConfig;
import androidx.annotation.VisibleForTesting;
import androidx.camera.camera2.internal.compat.quirk.DeviceQuirks;
import androidx.annotation.Nullable;
import androidx.camera.camera2.internal.compat.quirk.ExtraCroppingQuirk;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class ResolutionCorrector
{
    @Nullable
    private final ExtraCroppingQuirk mExtraCroppingQuirk;
    
    public ResolutionCorrector() {
        this(DeviceQuirks.get(ExtraCroppingQuirk.class));
    }
    
    @VisibleForTesting
    ResolutionCorrector(@Nullable final ExtraCroppingQuirk mExtraCroppingQuirk) {
        this.mExtraCroppingQuirk = mExtraCroppingQuirk;
    }
    
    @NonNull
    public List<Size> insertOrPrioritize(@NonNull final SurfaceConfig.ConfigType configType, @NonNull final List<Size> list) {
        final ExtraCroppingQuirk mExtraCroppingQuirk = this.mExtraCroppingQuirk;
        if (mExtraCroppingQuirk == null) {
            return list;
        }
        final Size verifiedResolution = mExtraCroppingQuirk.getVerifiedResolution(configType);
        if (verifiedResolution == null) {
            return list;
        }
        final ArrayList list2 = new ArrayList();
        list2.add(verifiedResolution);
        for (final Size size : list) {
            if (!size.equals((Object)verifiedResolution)) {
                list2.add(size);
            }
        }
        return list2;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.workaround;

import java.util.Iterator;
import androidx.camera.core.Logger;
import androidx.annotation.Nullable;
import androidx.camera.core.impl.DeferrableSurface;
import java.util.List;
import androidx.camera.camera2.internal.compat.quirk.ConfigureSurfaceToSecondarySessionFailQuirk;
import androidx.camera.camera2.internal.compat.quirk.PreviewOrientationIncorrectQuirk;
import androidx.camera.core.impl.Quirk;
import androidx.camera.camera2.internal.compat.quirk.TextureViewIsClosedQuirk;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.Quirks;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class ForceCloseDeferrableSurface
{
    private final boolean mHasConfigureSurfaceToSecondarySessionFailQuirk;
    private final boolean mHasPreviewOrientationIncorrectQuirk;
    private final boolean mHasTextureViewIsClosedQuirk;
    
    public ForceCloseDeferrableSurface(@NonNull final Quirks quirks, @NonNull final Quirks quirks2) {
        this.mHasTextureViewIsClosedQuirk = quirks2.contains(TextureViewIsClosedQuirk.class);
        this.mHasPreviewOrientationIncorrectQuirk = quirks.contains(PreviewOrientationIncorrectQuirk.class);
        this.mHasConfigureSurfaceToSecondarySessionFailQuirk = quirks.contains(ConfigureSurfaceToSecondarySessionFailQuirk.class);
    }
    
    public void onSessionEnd(@Nullable final List<DeferrableSurface> list) {
        if (this.shouldForceClose() && list != null) {
            final Iterator<DeferrableSurface> iterator = list.iterator();
            while (iterator.hasNext()) {
                iterator.next().close();
            }
            Logger.d("ForceCloseDeferrableSurface", "deferrableSurface closed");
        }
    }
    
    public boolean shouldForceClose() {
        return this.mHasTextureViewIsClosedQuirk || this.mHasPreviewOrientationIncorrectQuirk || this.mHasConfigureSurfaceToSecondarySessionFailQuirk;
    }
}

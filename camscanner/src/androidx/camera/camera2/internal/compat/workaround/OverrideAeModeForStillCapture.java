// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.workaround;

import androidx.camera.camera2.internal.compat.quirk.AutoFlashUnderExposedQuirk;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.Quirks;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public class OverrideAeModeForStillCapture
{
    private boolean mAePrecaptureStarted;
    private final boolean mHasAutoFlashUnderExposedQuirk;
    
    public OverrideAeModeForStillCapture(@NonNull final Quirks quirks) {
        boolean mHasAutoFlashUnderExposedQuirk = false;
        this.mAePrecaptureStarted = false;
        if (quirks.get(AutoFlashUnderExposedQuirk.class) != null) {
            mHasAutoFlashUnderExposedQuirk = true;
        }
        this.mHasAutoFlashUnderExposedQuirk = mHasAutoFlashUnderExposedQuirk;
    }
    
    public void onAePrecaptureFinished() {
        this.mAePrecaptureStarted = false;
    }
    
    public void onAePrecaptureStarted() {
        this.mAePrecaptureStarted = true;
    }
    
    public boolean shouldSetAeModeAlwaysFlash(final int n) {
        return this.mAePrecaptureStarted && n == 0 && this.mHasAutoFlashUnderExposedQuirk;
    }
}

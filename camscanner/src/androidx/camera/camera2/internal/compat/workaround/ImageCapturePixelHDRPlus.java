// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat.workaround;

import android.annotation.SuppressLint;
import android.hardware.camera2.CaptureRequest$Key;
import androidx.camera.camera2.internal.compat.quirk.DeviceQuirks;
import androidx.camera.camera2.internal.compat.quirk.ImageCapturePixelHDRPlusQuirk;
import androidx.annotation.NonNull;
import androidx.camera.camera2.impl.Camera2ImplConfig;
import androidx.annotation.RequiresApi;
import androidx.camera.camera2.interop.ExperimentalCamera2Interop;
import androidx.annotation.OptIn;

@OptIn(markerClass = { ExperimentalCamera2Interop.class })
@RequiresApi(21)
public class ImageCapturePixelHDRPlus
{
    @SuppressLint({ "NewApi" })
    public void toggleHDRPlus(final int n, @NonNull final Camera2ImplConfig.Builder builder) {
        if (DeviceQuirks.get(ImageCapturePixelHDRPlusQuirk.class) == null) {
            return;
        }
        if (n != 0) {
            if (n == 1) {
                builder.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Boolean>)\u3007080.\u3007080(), Boolean.FALSE);
            }
        }
        else {
            builder.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Boolean>)\u3007080.\u3007080(), Boolean.TRUE);
        }
    }
}

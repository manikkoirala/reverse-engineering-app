// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal.compat;

import android.hardware.camera2.CameraManager$AvailabilityCallback;
import android.hardware.camera2.params.OutputConfiguration;
import android.util.Size;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import android.view.Surface;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCaptureSession$StateCallback;
import androidx.annotation.DoNotInline;
import androidx.annotation.NonNull;
import android.hardware.camera2.CameraDevice;
import androidx.annotation.RequiresApi;

public final class ApiCompat
{
    private ApiCompat() {
    }
    
    @RequiresApi(21)
    public static class Api21Impl
    {
        private Api21Impl() {
        }
        
        @DoNotInline
        public static void close(@NonNull final CameraDevice cameraDevice) {
            cameraDevice.close();
        }
    }
    
    @RequiresApi(23)
    public static class Api23Impl
    {
        private Api23Impl() {
        }
        
        @DoNotInline
        public static void onSurfacePrepared(@NonNull final CameraCaptureSession$StateCallback cameraCaptureSession$StateCallback, @NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final Surface surface) {
            \u3007080.\u3007080(cameraCaptureSession$StateCallback, cameraCaptureSession, surface);
        }
    }
    
    @RequiresApi(24)
    public static class Api24Impl
    {
        private Api24Impl() {
        }
        
        @DoNotInline
        public static void onCaptureBufferLost(@NonNull final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback, @NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, @NonNull final Surface surface, final long n) {
            \u3007o00\u3007\u3007Oo.\u3007080(cameraCaptureSession$CaptureCallback, cameraCaptureSession, captureRequest, surface, n);
        }
    }
    
    @RequiresApi(26)
    public static class Api26Impl
    {
        private Api26Impl() {
        }
        
        @DoNotInline
        @NonNull
        public static <T> OutputConfiguration newOutputConfiguration(@NonNull final Size size, @NonNull final Class<T> clazz) {
            return new OutputConfiguration(size, (Class)clazz);
        }
        
        @DoNotInline
        public static void onCaptureQueueEmpty(@NonNull final CameraCaptureSession$StateCallback cameraCaptureSession$StateCallback, @NonNull final CameraCaptureSession cameraCaptureSession) {
            \u3007o\u3007.\u3007080(cameraCaptureSession$StateCallback, cameraCaptureSession);
        }
    }
    
    @RequiresApi(29)
    public static class Api29Impl
    {
        private Api29Impl() {
        }
        
        @DoNotInline
        public static void onCameraAccessPrioritiesChanged(@NonNull final CameraManager$AvailabilityCallback cameraManager$AvailabilityCallback) {
            O8.\u3007080(cameraManager$AvailabilityCallback);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.core.internal.UseCaseEventConfig;
import androidx.camera.core.internal.o\u30070;
import android.util.Range;
import androidx.camera.core.internal.TargetConfig;
import androidx.camera.core.internal.O8;
import java.util.Set;
import androidx.camera.core.impl.ImageInputConfig;
import androidx.camera.core.impl.oo88o8O;
import androidx.camera.core.impl.CaptureConfig;
import androidx.camera.core.impl.\u300700\u30078;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.impl.ReadableConfig;
import androidx.camera.core.impl.o\u3007\u30070\u3007;
import androidx.camera.core.impl.MutableOptionsBundle;
import androidx.camera.core.impl.Config;
import androidx.camera.core.UseCase;
import java.util.Comparator;
import java.util.List;
import java.util.Collections;
import java.util.Arrays;
import android.os.Build$VERSION;
import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.util.Size;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.annotation.Nullable;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import androidx.camera.core.impl.ImmediateSurface;
import androidx.camera.core.impl.UseCaseConfig;
import android.view.Surface;
import androidx.camera.core.Logger;
import android.graphics.SurfaceTexture;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import androidx.camera.camera2.internal.compat.workaround.SupportedRepeatingSurfaceSize;
import androidx.camera.core.impl.SessionConfig;
import androidx.camera.core.impl.DeferrableSurface;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
class MeteringRepeatingSession
{
    private static final String TAG = "MeteringRepeating";
    @NonNull
    private final MeteringRepeatingConfig mConfigWithDefaults;
    private DeferrableSurface mDeferrableSurface;
    @NonNull
    private final SessionConfig mSessionConfig;
    @NonNull
    private final SupportedRepeatingSurfaceSize mSupportedRepeatingSurfaceSize;
    
    MeteringRepeatingSession(@NonNull final CameraCharacteristicsCompat cameraCharacteristicsCompat, @NonNull final DisplayInfoManager displayInfoManager) {
        this.mSupportedRepeatingSurfaceSize = new SupportedRepeatingSurfaceSize();
        final MeteringRepeatingConfig mConfigWithDefaults = new MeteringRepeatingConfig();
        this.mConfigWithDefaults = mConfigWithDefaults;
        final SurfaceTexture surfaceTexture = new SurfaceTexture(0);
        final Size properPreviewSize = this.getProperPreviewSize(cameraCharacteristicsCompat, displayInfoManager);
        final StringBuilder sb = new StringBuilder();
        sb.append("MeteringSession SurfaceTexture size: ");
        sb.append(properPreviewSize);
        Logger.d("MeteringRepeating", sb.toString());
        surfaceTexture.setDefaultBufferSize(properPreviewSize.getWidth(), properPreviewSize.getHeight());
        final Surface surface = new Surface(surfaceTexture);
        final SessionConfig.Builder from = SessionConfig.Builder.createFrom(mConfigWithDefaults);
        from.setTemplateType(1);
        final ImmediateSurface mDeferrableSurface = new ImmediateSurface(surface);
        this.mDeferrableSurface = mDeferrableSurface;
        Futures.addCallback(mDeferrableSurface.getTerminationFuture(), new FutureCallback<Void>(this, surface, surfaceTexture) {
            final MeteringRepeatingSession this$0;
            final Surface val$surface;
            final SurfaceTexture val$surfaceTexture;
            
            @Override
            public void onFailure(@NonNull final Throwable cause) {
                throw new IllegalStateException("Future should never fail. Did it get completed by GC?", cause);
            }
            
            @Override
            public void onSuccess(@Nullable final Void void1) {
                this.val$surface.release();
                this.val$surfaceTexture.release();
            }
        }, CameraXExecutors.directExecutor());
        from.addSurface(this.mDeferrableSurface);
        this.mSessionConfig = from.build();
    }
    
    @NonNull
    private Size getProperPreviewSize(@NonNull final CameraCharacteristicsCompat cameraCharacteristicsCompat, @NonNull final DisplayInfoManager displayInfoManager) {
        final StreamConfigurationMap streamConfigurationMap = cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<StreamConfigurationMap>)CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
        if (streamConfigurationMap == null) {
            Logger.e("MeteringRepeating", "Can not retrieve SCALER_STREAM_CONFIGURATION_MAP.");
            return new Size(0, 0);
        }
        Size[] array;
        if (Build$VERSION.SDK_INT < 23) {
            array = streamConfigurationMap.getOutputSizes((Class)SurfaceTexture.class);
        }
        else {
            array = streamConfigurationMap.getOutputSizes(34);
        }
        if (array == null) {
            Logger.e("MeteringRepeating", "Can not get output size list.");
            return new Size(0, 0);
        }
        final Size[] supportedSizes = this.mSupportedRepeatingSurfaceSize.getSupportedSizes(array);
        final List<Size> list = Arrays.asList(supportedSizes);
        Collections.sort((List<Object>)list, new \u30070O\u3007Oo());
        final Size previewSize = displayInfoManager.getPreviewSize();
        final long min = Math.min(previewSize.getWidth() * (long)previewSize.getHeight(), 307200L);
        final int length = supportedSizes.length;
        Size size = null;
        int i = 0;
        while (i < length) {
            final Size size2 = supportedSizes[i];
            final long n = lcmp(size2.getWidth() * (long)size2.getHeight(), min);
            if (n == 0) {
                return size2;
            }
            if (n > 0) {
                if (size != null) {
                    return size;
                }
                break;
            }
            else {
                ++i;
                size = size2;
            }
        }
        return list.get(0);
    }
    
    void clear() {
        Logger.d("MeteringRepeating", "MeteringRepeating clear!");
        final DeferrableSurface mDeferrableSurface = this.mDeferrableSurface;
        if (mDeferrableSurface != null) {
            mDeferrableSurface.close();
        }
        this.mDeferrableSurface = null;
    }
    
    @NonNull
    String getName() {
        return "MeteringRepeating";
    }
    
    @NonNull
    SessionConfig getSessionConfig() {
        return this.mSessionConfig;
    }
    
    @NonNull
    UseCaseConfig<?> getUseCaseConfig() {
        return this.mConfigWithDefaults;
    }
    
    private static class MeteringRepeatingConfig implements UseCaseConfig<UseCase>
    {
        @NonNull
        private final Config mConfig;
        
        MeteringRepeatingConfig() {
            final MutableOptionsBundle create = MutableOptionsBundle.create();
            create.insertOption((Option<Camera2SessionOptionUnpacker>)UseCaseConfig.OPTION_SESSION_CONFIG_UNPACKER, new Camera2SessionOptionUnpacker());
            this.mConfig = create;
        }
        
        @NonNull
        @Override
        public Config getConfig() {
            return this.mConfig;
        }
    }
}

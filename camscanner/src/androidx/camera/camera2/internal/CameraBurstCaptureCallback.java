// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.CaptureFailure;
import android.hardware.camera2.TotalCaptureResult;
import java.util.Iterator;
import androidx.camera.camera2.internal.compat.ApiCompat;
import android.view.Surface;
import androidx.annotation.NonNull;
import android.hardware.camera2.CameraCaptureSession;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import android.hardware.camera2.CaptureRequest;
import java.util.Map;
import androidx.annotation.RequiresApi;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;

@RequiresApi(21)
class CameraBurstCaptureCallback extends CameraCaptureSession$CaptureCallback
{
    final Map<CaptureRequest, List<CameraCaptureSession$CaptureCallback>> mCallbackMap;
    CaptureSequenceCallback mCaptureSequenceCallback;
    
    CameraBurstCaptureCallback() {
        this.mCaptureSequenceCallback = null;
        this.mCallbackMap = new HashMap<CaptureRequest, List<CameraCaptureSession$CaptureCallback>>();
    }
    
    private List<CameraCaptureSession$CaptureCallback> getCallbacks(final CaptureRequest captureRequest) {
        List<Object> emptyList = (List<Object>)this.mCallbackMap.get(captureRequest);
        if (emptyList == null) {
            emptyList = Collections.emptyList();
        }
        return (List<CameraCaptureSession$CaptureCallback>)emptyList;
    }
    
    void addCamera2Callbacks(final CaptureRequest captureRequest, final List<CameraCaptureSession$CaptureCallback> list) {
        final List list2 = this.mCallbackMap.get(captureRequest);
        if (list2 != null) {
            final ArrayList list3 = new ArrayList(list.size() + list2.size());
            list3.addAll((Collection)list);
            list3.addAll(list2);
            this.mCallbackMap.put(captureRequest, (ArrayList)list3);
        }
        else {
            this.mCallbackMap.put(captureRequest, list);
        }
    }
    
    @RequiresApi(api = 24)
    public void onCaptureBufferLost(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, @NonNull final Surface surface, final long n) {
        final Iterator<CameraCaptureSession$CaptureCallback> iterator = this.getCallbacks(captureRequest).iterator();
        while (iterator.hasNext()) {
            ApiCompat.Api24Impl.onCaptureBufferLost(iterator.next(), cameraCaptureSession, captureRequest, surface, n);
        }
    }
    
    public void onCaptureCompleted(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, @NonNull final TotalCaptureResult totalCaptureResult) {
        final Iterator<CameraCaptureSession$CaptureCallback> iterator = this.getCallbacks(captureRequest).iterator();
        while (iterator.hasNext()) {
            iterator.next().onCaptureCompleted(cameraCaptureSession, captureRequest, totalCaptureResult);
        }
    }
    
    public void onCaptureFailed(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, @NonNull final CaptureFailure captureFailure) {
        final Iterator<CameraCaptureSession$CaptureCallback> iterator = this.getCallbacks(captureRequest).iterator();
        while (iterator.hasNext()) {
            iterator.next().onCaptureFailed(cameraCaptureSession, captureRequest, captureFailure);
        }
    }
    
    public void onCaptureProgressed(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, @NonNull final CaptureResult captureResult) {
        final Iterator<CameraCaptureSession$CaptureCallback> iterator = this.getCallbacks(captureRequest).iterator();
        while (iterator.hasNext()) {
            iterator.next().onCaptureProgressed(cameraCaptureSession, captureRequest, captureResult);
        }
    }
    
    public void onCaptureSequenceAborted(@NonNull final CameraCaptureSession cameraCaptureSession, final int n) {
        final Iterator<List<CameraCaptureSession$CaptureCallback>> iterator = this.mCallbackMap.values().iterator();
        while (iterator.hasNext()) {
            final Iterator iterator2 = iterator.next().iterator();
            while (iterator2.hasNext()) {
                ((CameraCaptureSession$CaptureCallback)iterator2.next()).onCaptureSequenceAborted(cameraCaptureSession, n);
            }
        }
        final CaptureSequenceCallback mCaptureSequenceCallback = this.mCaptureSequenceCallback;
        if (mCaptureSequenceCallback != null) {
            mCaptureSequenceCallback.onCaptureSequenceCompletedOrAborted(cameraCaptureSession, n, true);
        }
    }
    
    public void onCaptureSequenceCompleted(@NonNull final CameraCaptureSession cameraCaptureSession, final int n, final long n2) {
        final Iterator<List<CameraCaptureSession$CaptureCallback>> iterator = this.mCallbackMap.values().iterator();
        while (iterator.hasNext()) {
            final Iterator iterator2 = iterator.next().iterator();
            while (iterator2.hasNext()) {
                ((CameraCaptureSession$CaptureCallback)iterator2.next()).onCaptureSequenceCompleted(cameraCaptureSession, n, n2);
            }
        }
        final CaptureSequenceCallback mCaptureSequenceCallback = this.mCaptureSequenceCallback;
        if (mCaptureSequenceCallback != null) {
            mCaptureSequenceCallback.onCaptureSequenceCompletedOrAborted(cameraCaptureSession, n, false);
        }
    }
    
    public void onCaptureStarted(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, final long n, final long n2) {
        final Iterator<CameraCaptureSession$CaptureCallback> iterator = this.getCallbacks(captureRequest).iterator();
        while (iterator.hasNext()) {
            iterator.next().onCaptureStarted(cameraCaptureSession, captureRequest, n, n2);
        }
    }
    
    public void setCaptureSequenceCallback(@NonNull final CaptureSequenceCallback mCaptureSequenceCallback) {
        this.mCaptureSequenceCallback = mCaptureSequenceCallback;
    }
    
    interface CaptureSequenceCallback
    {
        void onCaptureSequenceCompletedOrAborted(@NonNull final CameraCaptureSession p0, final int p1, final boolean p2);
    }
}

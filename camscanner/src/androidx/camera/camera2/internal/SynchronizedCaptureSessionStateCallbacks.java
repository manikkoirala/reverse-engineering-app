// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.camera2.internal.compat.ApiCompat;
import android.hardware.camera2.CameraCaptureSession$StateCallback;
import android.view.Surface;
import java.util.Iterator;
import java.util.Arrays;
import java.util.Collection;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
final class SynchronizedCaptureSessionStateCallbacks extends StateCallback
{
    private final List<StateCallback> mCallbacks;
    
    SynchronizedCaptureSessionStateCallbacks(@NonNull final List<StateCallback> list) {
        (this.mCallbacks = new ArrayList<StateCallback>()).addAll(list);
    }
    
    @NonNull
    static StateCallback createComboCallback(@NonNull final StateCallback... a) {
        return new SynchronizedCaptureSessionStateCallbacks(Arrays.asList(a));
    }
    
    public void onActive(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
        final Iterator<StateCallback> iterator = this.mCallbacks.iterator();
        while (iterator.hasNext()) {
            iterator.next().onActive(synchronizedCaptureSession);
        }
    }
    
    @RequiresApi(api = 26)
    public void onCaptureQueueEmpty(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
        final Iterator<StateCallback> iterator = this.mCallbacks.iterator();
        while (iterator.hasNext()) {
            iterator.next().onCaptureQueueEmpty(synchronizedCaptureSession);
        }
    }
    
    @Override
    public void onClosed(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
        final Iterator<StateCallback> iterator = this.mCallbacks.iterator();
        while (iterator.hasNext()) {
            iterator.next().onClosed(synchronizedCaptureSession);
        }
    }
    
    @Override
    public void onConfigureFailed(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
        final Iterator<StateCallback> iterator = this.mCallbacks.iterator();
        while (iterator.hasNext()) {
            iterator.next().onConfigureFailed(synchronizedCaptureSession);
        }
    }
    
    public void onConfigured(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
        final Iterator<StateCallback> iterator = this.mCallbacks.iterator();
        while (iterator.hasNext()) {
            iterator.next().onConfigured(synchronizedCaptureSession);
        }
    }
    
    public void onReady(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
        final Iterator<StateCallback> iterator = this.mCallbacks.iterator();
        while (iterator.hasNext()) {
            iterator.next().onReady(synchronizedCaptureSession);
        }
    }
    
    @Override
    void onSessionFinished(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
        final Iterator<StateCallback> iterator = this.mCallbacks.iterator();
        while (iterator.hasNext()) {
            iterator.next().onSessionFinished(synchronizedCaptureSession);
        }
    }
    
    @RequiresApi(api = 23)
    public void onSurfacePrepared(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession, @NonNull final Surface surface) {
        final Iterator<StateCallback> iterator = this.mCallbacks.iterator();
        while (iterator.hasNext()) {
            iterator.next().onSurfacePrepared(synchronizedCaptureSession, surface);
        }
    }
    
    @RequiresApi(21)
    static class Adapter extends StateCallback
    {
        @NonNull
        private final CameraCaptureSession$StateCallback mCameraCaptureSessionStateCallback;
        
        Adapter(@NonNull final CameraCaptureSession$StateCallback mCameraCaptureSessionStateCallback) {
            this.mCameraCaptureSessionStateCallback = mCameraCaptureSessionStateCallback;
        }
        
        Adapter(@NonNull final List<CameraCaptureSession$StateCallback> list) {
            this(CameraCaptureSessionStateCallbacks.createComboCallback(list));
        }
        
        public void onActive(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
            this.mCameraCaptureSessionStateCallback.onActive(synchronizedCaptureSession.toCameraCaptureSessionCompat().toCameraCaptureSession());
        }
        
        @RequiresApi(api = 26)
        public void onCaptureQueueEmpty(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
            ApiCompat.Api26Impl.onCaptureQueueEmpty(this.mCameraCaptureSessionStateCallback, synchronizedCaptureSession.toCameraCaptureSessionCompat().toCameraCaptureSession());
        }
        
        @Override
        public void onClosed(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
            this.mCameraCaptureSessionStateCallback.onClosed(synchronizedCaptureSession.toCameraCaptureSessionCompat().toCameraCaptureSession());
        }
        
        @Override
        public void onConfigureFailed(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
            this.mCameraCaptureSessionStateCallback.onConfigureFailed(synchronizedCaptureSession.toCameraCaptureSessionCompat().toCameraCaptureSession());
        }
        
        public void onConfigured(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
            this.mCameraCaptureSessionStateCallback.onConfigured(synchronizedCaptureSession.toCameraCaptureSessionCompat().toCameraCaptureSession());
        }
        
        public void onReady(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
            this.mCameraCaptureSessionStateCallback.onReady(synchronizedCaptureSession.toCameraCaptureSessionCompat().toCameraCaptureSession());
        }
        
        @Override
        void onSessionFinished(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
        }
        
        @RequiresApi(api = 23)
        public void onSurfacePrepared(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession, @NonNull final Surface surface) {
            ApiCompat.Api23Impl.onSurfacePrepared(this.mCameraCaptureSessionStateCallback, synchronizedCaptureSession.toCameraCaptureSessionCompat().toCameraCaptureSession(), surface);
        }
    }
}

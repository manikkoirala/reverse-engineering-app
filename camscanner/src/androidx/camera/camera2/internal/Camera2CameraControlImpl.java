// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.CameraCaptureSession;
import java.util.Collection;
import java.util.concurrent.RejectedExecutionException;
import androidx.camera.core.impl.CameraCaptureFailure;
import androidx.camera.core.impl.CameraCaptureResult;
import android.util.ArrayMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Map;
import androidx.camera.core.impl.utils.futures.AsyncFunction;
import androidx.camera.core.impl.utils.futures.FutureChain;
import androidx.camera.core.FocusMeteringResult;
import androidx.camera.core.FocusMeteringAction;
import androidx.annotation.Nullable;
import android.util.Rational;
import androidx.camera.core.Logger;
import java.util.Iterator;
import androidx.core.util.Preconditions;
import android.graphics.Rect;
import java.util.Collections;
import android.hardware.camera2.CaptureRequest$Key;
import android.hardware.camera2.CaptureRequest;
import androidx.camera.camera2.impl.Camera2ImplConfig;
import androidx.camera.core.CameraControl;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.camera2.interop.CaptureRequestOptions;
import androidx.camera.core.impl.Config;
import androidx.camera.core.impl.CaptureConfig;
import androidx.camera.core.impl.TagBundle;
import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.TotalCaptureResult;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import android.os.Build$VERSION;
import androidx.camera.core.impl.CameraCaptureCallback;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import androidx.camera.core.impl.utils.futures.Futures;
import java.util.List;
import androidx.camera.core.impl.Quirks;
import androidx.camera.core.impl.Quirk;
import java.util.ArrayList;
import java.util.concurrent.ScheduledExecutorService;
import androidx.annotation.GuardedBy;
import androidx.camera.core.impl.SessionConfig;
import androidx.annotation.VisibleForTesting;
import java.util.concurrent.atomic.AtomicLong;
import androidx.annotation.NonNull;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Executor;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import androidx.camera.camera2.interop.Camera2CameraControl;
import androidx.camera.camera2.internal.compat.workaround.AutoFlashAEModeDisabler;
import androidx.camera.camera2.internal.compat.workaround.AeFpsRange;
import androidx.annotation.RequiresApi;
import androidx.camera.camera2.interop.ExperimentalCamera2Interop;
import androidx.annotation.OptIn;
import androidx.camera.core.impl.CameraControlInternal;

@OptIn(markerClass = { ExperimentalCamera2Interop.class })
@RequiresApi(21)
public class Camera2CameraControlImpl implements CameraControlInternal
{
    private static final int DEFAULT_TEMPLATE = 1;
    private static final String TAG = "Camera2CameraControlImp";
    static final String TAG_SESSION_UPDATE_ID = "CameraControlSessionUpdateId";
    private final AeFpsRange mAeFpsRange;
    private final AutoFlashAEModeDisabler mAutoFlashAEModeDisabler;
    private final Camera2CameraControl mCamera2CameraControl;
    private final Camera2CapturePipeline mCamera2CapturePipeline;
    private final CameraCaptureCallbackSet mCameraCaptureCallbackSet;
    private final CameraCharacteristicsCompat mCameraCharacteristics;
    private final ControlUpdateCallback mControlUpdateCallback;
    private long mCurrentSessionUpdateId;
    final Executor mExecutor;
    private final ExposureControl mExposureControl;
    private volatile int mFlashMode;
    @NonNull
    private volatile ListenableFuture<Void> mFlashModeChangeSessionUpdateFuture;
    private final FocusMeteringControl mFocusMeteringControl;
    private volatile boolean mIsTorchOn;
    private final Object mLock;
    private final AtomicLong mNextSessionUpdateId;
    @VisibleForTesting
    final CameraControlSessionCallback mSessionCallback;
    private final SessionConfig.Builder mSessionConfigBuilder;
    private int mTemplate;
    private final TorchControl mTorchControl;
    @GuardedBy("mLock")
    private int mUseCount;
    private final ZoomControl mZoomControl;
    @VisibleForTesting
    ZslControl mZslControl;
    
    @VisibleForTesting
    Camera2CameraControlImpl(@NonNull final CameraCharacteristicsCompat cameraCharacteristicsCompat, @NonNull final ScheduledExecutorService scheduledExecutorService, @NonNull final Executor executor, @NonNull final ControlUpdateCallback controlUpdateCallback) {
        this(cameraCharacteristicsCompat, scheduledExecutorService, executor, controlUpdateCallback, new Quirks(new ArrayList<Quirk>()));
    }
    
    Camera2CameraControlImpl(@NonNull final CameraCharacteristicsCompat mCameraCharacteristics, @NonNull final ScheduledExecutorService scheduledExecutorService, @NonNull final Executor mExecutor, @NonNull final ControlUpdateCallback mControlUpdateCallback, @NonNull final Quirks quirks) {
        this.mLock = new Object();
        final SessionConfig.Builder mSessionConfigBuilder = new SessionConfig.Builder();
        this.mSessionConfigBuilder = mSessionConfigBuilder;
        this.mUseCount = 0;
        this.mIsTorchOn = false;
        this.mFlashMode = 2;
        this.mNextSessionUpdateId = new AtomicLong(0L);
        this.mFlashModeChangeSessionUpdateFuture = Futures.immediateFuture((Void)null);
        this.mTemplate = 1;
        this.mCurrentSessionUpdateId = 0L;
        final CameraCaptureCallbackSet mCameraCaptureCallbackSet = new CameraCaptureCallbackSet();
        this.mCameraCaptureCallbackSet = mCameraCaptureCallbackSet;
        this.mCameraCharacteristics = mCameraCharacteristics;
        this.mControlUpdateCallback = mControlUpdateCallback;
        this.mExecutor = mExecutor;
        final CameraControlSessionCallback mSessionCallback = new CameraControlSessionCallback(mExecutor);
        this.mSessionCallback = mSessionCallback;
        mSessionConfigBuilder.setTemplateType(this.mTemplate);
        mSessionConfigBuilder.addRepeatingCameraCaptureCallback(CaptureCallbackContainer.create(mSessionCallback));
        mSessionConfigBuilder.addRepeatingCameraCaptureCallback(mCameraCaptureCallbackSet);
        this.mExposureControl = new ExposureControl(this, mCameraCharacteristics, mExecutor);
        this.mFocusMeteringControl = new FocusMeteringControl(this, scheduledExecutorService, mExecutor, quirks);
        this.mZoomControl = new ZoomControl(this, mCameraCharacteristics, mExecutor);
        this.mTorchControl = new TorchControl(this, mCameraCharacteristics, mExecutor);
        if (Build$VERSION.SDK_INT >= 23) {
            this.mZslControl = new ZslControlImpl(mCameraCharacteristics);
        }
        else {
            this.mZslControl = new ZslControlNoOpImpl();
        }
        this.mAeFpsRange = new AeFpsRange(quirks);
        this.mAutoFlashAEModeDisabler = new AutoFlashAEModeDisabler(quirks);
        this.mCamera2CameraControl = new Camera2CameraControl(this, mExecutor);
        this.mCamera2CapturePipeline = new Camera2CapturePipeline(this, mCameraCharacteristics, quirks, mExecutor);
        mExecutor.execute(new Oooo8o0\u3007(this));
    }
    
    private int getSupportedAwbMode(final int n) {
        final int[] array = this.mCameraCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<int[]>)CameraCharacteristics.CONTROL_AWB_AVAILABLE_MODES);
        if (array == null) {
            return 0;
        }
        if (this.isModeInList(n, array)) {
            return n;
        }
        if (this.isModeInList(1, array)) {
            return 1;
        }
        return 0;
    }
    
    private boolean isControlInUse() {
        return this.getUseCount() > 0;
    }
    
    private boolean isModeInList(final int n, final int[] array) {
        for (int length = array.length, i = 0; i < length; ++i) {
            if (n == array[i]) {
                return true;
            }
        }
        return false;
    }
    
    static boolean isSessionUpdated(@NonNull final TotalCaptureResult totalCaptureResult, final long n) {
        if (((CaptureResult)totalCaptureResult).getRequest() == null) {
            return false;
        }
        final Object tag = ((CaptureResult)totalCaptureResult).getRequest().getTag();
        if (tag instanceof TagBundle) {
            final Long n2 = (Long)((TagBundle)tag).getTag("CameraControlSessionUpdateId");
            if (n2 == null) {
                return false;
            }
            if (n2 >= n) {
                return true;
            }
        }
        return false;
    }
    
    @NonNull
    private ListenableFuture<Void> waitForSessionUpdateId(final long n) {
        return CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new OO0o\u3007\u3007\u3007\u30070(this, n));
    }
    
    void addCaptureResultListener(@NonNull final CaptureResultListener captureResultListener) {
        this.mSessionCallback.addListener(captureResultListener);
    }
    
    @Override
    public void addInteropConfig(@NonNull final Config config) {
        this.mCamera2CameraControl.addCaptureRequestOptions(CaptureRequestOptions.Builder.from(config).build()).addListener((Runnable)new Oo08(), CameraXExecutors.directExecutor());
    }
    
    void addSessionCameraCaptureCallback(@NonNull final Executor executor, @NonNull final CameraCaptureCallback cameraCaptureCallback) {
        this.mExecutor.execute(new \u300780\u3007808\u3007O(this, executor, cameraCaptureCallback));
    }
    
    @Override
    public void addZslConfig(@NonNull final SessionConfig.Builder builder) {
        this.mZslControl.addZslConfig(builder);
    }
    
    @NonNull
    @Override
    public ListenableFuture<Void> cancelFocusAndMetering() {
        if (!this.isControlInUse()) {
            return Futures.immediateFailedFuture(new CameraControl.OperationCanceledException("Camera is not active."));
        }
        return Futures.nonCancellationPropagating(this.mFocusMeteringControl.cancelFocusAndMetering());
    }
    
    @Override
    public void clearInteropConfig() {
        this.mCamera2CameraControl.clearCaptureRequestOptions().addListener((Runnable)new oO80(), CameraXExecutors.directExecutor());
    }
    
    void decrementUseCount() {
        synchronized (this.mLock) {
            final int mUseCount = this.mUseCount;
            if (mUseCount != 0) {
                this.mUseCount = mUseCount - 1;
                return;
            }
            throw new IllegalStateException("Decrementing use count occurs more times than incrementing");
        }
    }
    
    @NonNull
    @Override
    public ListenableFuture<Void> enableTorch(final boolean b) {
        if (!this.isControlInUse()) {
            return Futures.immediateFailedFuture(new CameraControl.OperationCanceledException("Camera is not active."));
        }
        return Futures.nonCancellationPropagating(this.mTorchControl.enableTorch(b));
    }
    
    void enableTorchInternal(final boolean mIsTorchOn) {
        if (!(this.mIsTorchOn = mIsTorchOn)) {
            final CaptureConfig.Builder builder = new CaptureConfig.Builder();
            builder.setTemplateType(this.mTemplate);
            builder.setUseRepeatingSurface(true);
            final Camera2ImplConfig.Builder builder2 = new Camera2ImplConfig.Builder();
            builder2.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.CONTROL_AE_MODE, this.getSupportedAeMode(1));
            builder2.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.FLASH_MODE, 0);
            builder.addImplementationOptions(builder2.build());
            this.submitCaptureRequestsInternal(Collections.singletonList(builder.build()));
        }
        this.updateSessionConfigSynchronous();
    }
    
    @NonNull
    public Camera2CameraControl getCamera2CameraControl() {
        return this.mCamera2CameraControl;
    }
    
    @NonNull
    Rect getCropSensorRegion() {
        return this.mZoomControl.getCropSensorRegion();
    }
    
    @VisibleForTesting
    long getCurrentSessionUpdateId() {
        return this.mCurrentSessionUpdateId;
    }
    
    @NonNull
    public ExposureControl getExposureControl() {
        return this.mExposureControl;
    }
    
    @Override
    public int getFlashMode() {
        return this.mFlashMode;
    }
    
    @NonNull
    public FocusMeteringControl getFocusMeteringControl() {
        return this.mFocusMeteringControl;
    }
    
    @NonNull
    @Override
    public Config getInteropConfig() {
        return this.mCamera2CameraControl.getCamera2ImplConfig();
    }
    
    int getMaxAeRegionCount() {
        final Integer n = this.mCameraCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.CONTROL_MAX_REGIONS_AE);
        int intValue;
        if (n == null) {
            intValue = 0;
        }
        else {
            intValue = n;
        }
        return intValue;
    }
    
    int getMaxAfRegionCount() {
        final Integer n = this.mCameraCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.CONTROL_MAX_REGIONS_AF);
        int intValue;
        if (n == null) {
            intValue = 0;
        }
        else {
            intValue = n;
        }
        return intValue;
    }
    
    int getMaxAwbRegionCount() {
        final Integer n = this.mCameraCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.CONTROL_MAX_REGIONS_AWB);
        int intValue;
        if (n == null) {
            intValue = 0;
        }
        else {
            intValue = n;
        }
        return intValue;
    }
    
    @NonNull
    @Override
    public Rect getSensorRect() {
        return Preconditions.checkNotNull((Rect)this.mCameraCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<T>)CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE));
    }
    
    @NonNull
    @Override
    public SessionConfig getSessionConfig() {
        this.mSessionConfigBuilder.setTemplateType(this.mTemplate);
        this.mSessionConfigBuilder.setImplementationOptions(this.getSessionOptions());
        final Object captureRequestTag = this.mCamera2CameraControl.getCamera2ImplConfig().getCaptureRequestTag(null);
        if (captureRequestTag != null && captureRequestTag instanceof Integer) {
            this.mSessionConfigBuilder.addTag("Camera2CameraControl", captureRequestTag);
        }
        this.mSessionConfigBuilder.addTag("CameraControlSessionUpdateId", this.mCurrentSessionUpdateId);
        return this.mSessionConfigBuilder.build();
    }
    
    @VisibleForTesting
    Config getSessionOptions() {
        final Camera2ImplConfig.Builder captureRequestOption = new Camera2ImplConfig.Builder();
        captureRequestOption.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.CONTROL_MODE, 1);
        this.mFocusMeteringControl.addFocusMeteringOptions(captureRequestOption);
        this.mAeFpsRange.addAeFpsRangeOptions(captureRequestOption);
        this.mZoomControl.addZoomOption(captureRequestOption);
        int correctedAeMode = 0;
        Label_0105: {
            if (this.mIsTorchOn) {
                captureRequestOption.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.FLASH_MODE, 2);
            }
            else {
                final int mFlashMode = this.mFlashMode;
                if (mFlashMode == 0) {
                    correctedAeMode = this.mAutoFlashAEModeDisabler.getCorrectedAeMode(2);
                    break Label_0105;
                }
                if (mFlashMode == 1) {
                    correctedAeMode = 3;
                    break Label_0105;
                }
            }
            correctedAeMode = 1;
        }
        captureRequestOption.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.CONTROL_AE_MODE, this.getSupportedAeMode(correctedAeMode));
        captureRequestOption.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.CONTROL_AWB_MODE, this.getSupportedAwbMode(1));
        this.mExposureControl.setCaptureRequestOption(captureRequestOption);
        final Camera2ImplConfig camera2ImplConfig = this.mCamera2CameraControl.getCamera2ImplConfig();
        for (final Config.Option<ValueT> option : camera2ImplConfig.listOptions()) {
            captureRequestOption.getMutableConfig().insertOption((Config.Option<Object>)option, Config.OptionPriority.ALWAYS_OVERRIDE, camera2ImplConfig.retrieveOption((Config.Option<ValueT>)option));
        }
        return captureRequestOption.build();
    }
    
    int getSupportedAeMode(final int n) {
        final int[] array = this.mCameraCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<int[]>)CameraCharacteristics.CONTROL_AE_AVAILABLE_MODES);
        if (array == null) {
            return 0;
        }
        if (this.isModeInList(n, array)) {
            return n;
        }
        if (this.isModeInList(1, array)) {
            return 1;
        }
        return 0;
    }
    
    int getSupportedAfMode(final int n) {
        final int[] array = this.mCameraCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<int[]>)CameraCharacteristics.CONTROL_AF_AVAILABLE_MODES);
        if (array == null) {
            return 0;
        }
        if (this.isModeInList(n, array)) {
            return n;
        }
        if (this.isModeInList(4, array)) {
            return 4;
        }
        if (this.isModeInList(1, array)) {
            return 1;
        }
        return 0;
    }
    
    @NonNull
    public TorchControl getTorchControl() {
        return this.mTorchControl;
    }
    
    @VisibleForTesting
    int getUseCount() {
        synchronized (this.mLock) {
            return this.mUseCount;
        }
    }
    
    @NonNull
    public ZoomControl getZoomControl() {
        return this.mZoomControl;
    }
    
    @NonNull
    public ZslControl getZslControl() {
        return this.mZslControl;
    }
    
    void incrementUseCount() {
        synchronized (this.mLock) {
            ++this.mUseCount;
        }
    }
    
    boolean isTorchOn() {
        return this.mIsTorchOn;
    }
    
    @Override
    public boolean isZslDisabledByByUserCaseConfig() {
        return this.mZslControl.isZslDisabledByUserCaseConfig();
    }
    
    void removeCaptureResultListener(@NonNull final CaptureResultListener captureResultListener) {
        this.mSessionCallback.removeListener(captureResultListener);
    }
    
    void removeSessionCameraCaptureCallback(@NonNull final CameraCaptureCallback cameraCaptureCallback) {
        this.mExecutor.execute(new O8(this, cameraCaptureCallback));
    }
    
    void resetTemplate() {
        this.setTemplate(1);
    }
    
    void setActive(final boolean active) {
        this.mFocusMeteringControl.setActive(active);
        this.mZoomControl.setActive(active);
        this.mTorchControl.setActive(active);
        this.mExposureControl.setActive(active);
        this.mCamera2CameraControl.setActive(active);
    }
    
    @NonNull
    @Override
    public ListenableFuture<Integer> setExposureCompensationIndex(final int exposureCompensationIndex) {
        if (!this.isControlInUse()) {
            return Futures.immediateFailedFuture(new CameraControl.OperationCanceledException("Camera is not active."));
        }
        return this.mExposureControl.setExposureCompensationIndex(exposureCompensationIndex);
    }
    
    @Override
    public void setFlashMode(int mFlashMode) {
        if (!this.isControlInUse()) {
            Logger.w("Camera2CameraControlImp", "Camera is not active.");
            return;
        }
        this.mFlashMode = mFlashMode;
        final ZslControl mZslControl = this.mZslControl;
        mFlashMode = this.mFlashMode;
        boolean zslDisabledByFlashMode = true;
        if (mFlashMode != 1) {
            zslDisabledByFlashMode = (this.mFlashMode == 0 && zslDisabledByFlashMode);
        }
        mZslControl.setZslDisabledByFlashMode(zslDisabledByFlashMode);
        this.mFlashModeChangeSessionUpdateFuture = this.updateSessionConfigAsync();
    }
    
    @NonNull
    @Override
    public ListenableFuture<Void> setLinearZoom(final float linearZoom) {
        if (!this.isControlInUse()) {
            return Futures.immediateFailedFuture(new CameraControl.OperationCanceledException("Camera is not active."));
        }
        return Futures.nonCancellationPropagating(this.mZoomControl.setLinearZoom(linearZoom));
    }
    
    public void setPreviewAspectRatio(@Nullable final Rational previewAspectRatio) {
        this.mFocusMeteringControl.setPreviewAspectRatio(previewAspectRatio);
    }
    
    void setTemplate(final int n) {
        this.mTemplate = n;
        this.mFocusMeteringControl.setTemplate(n);
        this.mCamera2CapturePipeline.setTemplate(this.mTemplate);
    }
    
    @NonNull
    @Override
    public ListenableFuture<Void> setZoomRatio(final float zoomRatio) {
        if (!this.isControlInUse()) {
            return Futures.immediateFailedFuture(new CameraControl.OperationCanceledException("Camera is not active."));
        }
        return Futures.nonCancellationPropagating(this.mZoomControl.setZoomRatio(zoomRatio));
    }
    
    @Override
    public void setZslDisabledByUserCaseConfig(final boolean zslDisabledByUserCaseConfig) {
        this.mZslControl.setZslDisabledByUserCaseConfig(zslDisabledByUserCaseConfig);
    }
    
    @NonNull
    @Override
    public ListenableFuture<FocusMeteringResult> startFocusAndMetering(@NonNull final FocusMeteringAction focusMeteringAction) {
        if (!this.isControlInUse()) {
            return Futures.immediateFailedFuture(new CameraControl.OperationCanceledException("Camera is not active."));
        }
        return Futures.nonCancellationPropagating(this.mFocusMeteringControl.startFocusAndMetering(focusMeteringAction));
    }
    
    void submitCaptureRequestsInternal(final List<CaptureConfig> list) {
        this.mControlUpdateCallback.onCameraControlCaptureRequests(list);
    }
    
    @NonNull
    @Override
    public ListenableFuture<List<Void>> submitStillCaptureRequests(@NonNull final List<CaptureConfig> list, final int n, final int n2) {
        if (!this.isControlInUse()) {
            Logger.w("Camera2CameraControlImp", "Camera is not active.");
            return Futures.immediateFailedFuture(new CameraControl.OperationCanceledException("Camera is not active."));
        }
        return (ListenableFuture<List<Void>>)FutureChain.from((com.google.common.util.concurrent.ListenableFuture<Object>)Futures.nonCancellationPropagating((com.google.common.util.concurrent.ListenableFuture<V>)this.mFlashModeChangeSessionUpdateFuture)).transformAsync((AsyncFunction<? super Object, Object>)new \u3007O8o08O(this, list, n, this.getFlashMode(), n2), this.mExecutor);
    }
    
    public void updateSessionConfig() {
        this.mExecutor.execute(new o\u30070(this));
    }
    
    @NonNull
    ListenableFuture<Void> updateSessionConfigAsync() {
        return Futures.nonCancellationPropagating((com.google.common.util.concurrent.ListenableFuture<Void>)CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<V>)new \u3007\u3007888(this)));
    }
    
    long updateSessionConfigSynchronous() {
        this.mCurrentSessionUpdateId = this.mNextSessionUpdateId.getAndIncrement();
        this.mControlUpdateCallback.onCameraControlUpdateSessionConfig();
        return this.mCurrentSessionUpdateId;
    }
    
    @RequiresApi(21)
    static final class CameraCaptureCallbackSet extends CameraCaptureCallback
    {
        Map<CameraCaptureCallback, Executor> mCallbackExecutors;
        Set<CameraCaptureCallback> mCallbacks;
        
        CameraCaptureCallbackSet() {
            this.mCallbacks = new HashSet<CameraCaptureCallback>();
            this.mCallbackExecutors = (Map<CameraCaptureCallback, Executor>)new ArrayMap();
        }
        
        void addCaptureCallback(@NonNull final Executor executor, @NonNull final CameraCaptureCallback cameraCaptureCallback) {
            this.mCallbacks.add(cameraCaptureCallback);
            this.mCallbackExecutors.put(cameraCaptureCallback, executor);
        }
        
        @Override
        public void onCaptureCancelled() {
            for (final CameraCaptureCallback cameraCaptureCallback : this.mCallbacks) {
                try {
                    this.mCallbackExecutors.get(cameraCaptureCallback).execute(new \u3007O00(cameraCaptureCallback));
                }
                catch (final RejectedExecutionException ex) {
                    Logger.e("Camera2CameraControlImp", "Executor rejected to invoke onCaptureCancelled.", ex);
                }
            }
        }
        
        @Override
        public void onCaptureCompleted(@NonNull final CameraCaptureResult cameraCaptureResult) {
            for (final CameraCaptureCallback cameraCaptureCallback : this.mCallbacks) {
                try {
                    this.mCallbackExecutors.get(cameraCaptureCallback).execute(new \u3007\u30078O0\u30078(cameraCaptureCallback, cameraCaptureResult));
                }
                catch (final RejectedExecutionException ex) {
                    Logger.e("Camera2CameraControlImp", "Executor rejected to invoke onCaptureCompleted.", ex);
                }
            }
        }
        
        @Override
        public void onCaptureFailed(@NonNull final CameraCaptureFailure cameraCaptureFailure) {
            for (final CameraCaptureCallback cameraCaptureCallback : this.mCallbacks) {
                try {
                    this.mCallbackExecutors.get(cameraCaptureCallback).execute(new \u3007\u3007808\u3007(cameraCaptureCallback, cameraCaptureFailure));
                }
                catch (final RejectedExecutionException ex) {
                    Logger.e("Camera2CameraControlImp", "Executor rejected to invoke onCaptureFailed.", ex);
                }
            }
        }
        
        void removeCaptureCallback(@NonNull final CameraCaptureCallback cameraCaptureCallback) {
            this.mCallbacks.remove(cameraCaptureCallback);
            this.mCallbackExecutors.remove(cameraCaptureCallback);
        }
    }
    
    static final class CameraControlSessionCallback extends CameraCaptureSession$CaptureCallback
    {
        private final Executor mExecutor;
        final Set<CaptureResultListener> mResultListeners;
        
        CameraControlSessionCallback(@NonNull final Executor mExecutor) {
            this.mResultListeners = new HashSet<CaptureResultListener>();
            this.mExecutor = mExecutor;
        }
        
        void addListener(@NonNull final CaptureResultListener captureResultListener) {
            this.mResultListeners.add(captureResultListener);
        }
        
        public void onCaptureCompleted(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, @NonNull final TotalCaptureResult totalCaptureResult) {
            this.mExecutor.execute(new \u30070\u3007O0088o(this, totalCaptureResult));
        }
        
        void removeListener(@NonNull final CaptureResultListener captureResultListener) {
            this.mResultListeners.remove(captureResultListener);
        }
    }
    
    public interface CaptureResultListener
    {
        boolean onCaptureResult(@NonNull final TotalCaptureResult p0);
    }
}

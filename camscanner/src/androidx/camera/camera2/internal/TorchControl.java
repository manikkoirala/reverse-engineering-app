// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import android.hardware.camera2.CaptureResult;
import androidx.lifecycle.LiveData;
import androidx.camera.core.CameraControl;
import androidx.annotation.Nullable;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.core.Logger;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.camera.core.impl.utils.Threads;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import androidx.camera.camera2.internal.compat.workaround.FlashAvailabilityChecker;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import java.util.concurrent.Executor;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
final class TorchControl
{
    static final int DEFAULT_TORCH_STATE = 0;
    private static final String TAG = "TorchControl";
    private final Camera2CameraControlImpl mCamera2CameraControlImpl;
    CallbackToFutureAdapter.Completer<Void> mEnableTorchCompleter;
    private final Executor mExecutor;
    private final boolean mHasFlashUnit;
    private boolean mIsActive;
    boolean mTargetTorchEnabled;
    private final MutableLiveData<Integer> mTorchState;
    
    TorchControl(@NonNull final Camera2CameraControlImpl mCamera2CameraControlImpl, @NonNull final CameraCharacteristicsCompat cameraCharacteristicsCompat, @NonNull final Executor mExecutor) {
        this.mCamera2CameraControlImpl = mCamera2CameraControlImpl;
        this.mExecutor = mExecutor;
        this.mHasFlashUnit = FlashAvailabilityChecker.isFlashAvailable(cameraCharacteristicsCompat);
        this.mTorchState = new MutableLiveData<Integer>(0);
        mCamera2CameraControlImpl.addCaptureResultListener((Camera2CameraControlImpl.CaptureResultListener)new o8O0(this));
    }
    
    private <T> void setLiveDataValue(@NonNull final MutableLiveData<T> mutableLiveData, final T value) {
        if (Threads.isMainThread()) {
            mutableLiveData.setValue(value);
        }
        else {
            mutableLiveData.postValue(value);
        }
    }
    
    ListenableFuture<Void> enableTorch(final boolean i) {
        if (!this.mHasFlashUnit) {
            Logger.d("TorchControl", "Unable to enableTorch due to there is no flash unit.");
            return Futures.immediateFailedFuture(new IllegalStateException("No flash unit"));
        }
        this.setLiveDataValue(this.mTorchState, i ? 1 : 0);
        return CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new O00(this, i));
    }
    
    void enableTorchInternal(@Nullable final CallbackToFutureAdapter.Completer<Void> mEnableTorchCompleter, final boolean b) {
        if (!this.mHasFlashUnit) {
            if (mEnableTorchCompleter != null) {
                mEnableTorchCompleter.setException(new IllegalStateException("No flash unit"));
            }
            return;
        }
        if (!this.mIsActive) {
            this.setLiveDataValue(this.mTorchState, 0);
            if (mEnableTorchCompleter != null) {
                mEnableTorchCompleter.setException(new CameraControl.OperationCanceledException("Camera is not active."));
            }
            return;
        }
        this.mTargetTorchEnabled = b;
        this.mCamera2CameraControlImpl.enableTorchInternal(b);
        this.setLiveDataValue(this.mTorchState, b ? 1 : 0);
        final CallbackToFutureAdapter.Completer<Void> mEnableTorchCompleter2 = this.mEnableTorchCompleter;
        if (mEnableTorchCompleter2 != null) {
            mEnableTorchCompleter2.setException(new CameraControl.OperationCanceledException("There is a new enableTorch being set"));
        }
        this.mEnableTorchCompleter = mEnableTorchCompleter;
    }
    
    @NonNull
    LiveData<Integer> getTorchState() {
        return this.mTorchState;
    }
    
    void setActive(final boolean mIsActive) {
        if (this.mIsActive == mIsActive) {
            return;
        }
        if (!(this.mIsActive = mIsActive)) {
            if (this.mTargetTorchEnabled) {
                this.mTargetTorchEnabled = false;
                this.mCamera2CameraControlImpl.enableTorchInternal(false);
                this.setLiveDataValue(this.mTorchState, 0);
            }
            final CallbackToFutureAdapter.Completer<Void> mEnableTorchCompleter = this.mEnableTorchCompleter;
            if (mEnableTorchCompleter != null) {
                mEnableTorchCompleter.setException(new CameraControl.OperationCanceledException("Camera is not active."));
                this.mEnableTorchCompleter = null;
            }
        }
    }
}

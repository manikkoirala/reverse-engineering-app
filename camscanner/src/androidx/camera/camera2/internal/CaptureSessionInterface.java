// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import com.google.common.util.concurrent.ListenableFuture;
import android.hardware.camera2.CameraDevice;
import androidx.annotation.Nullable;
import androidx.camera.core.impl.SessionConfig;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.CaptureConfig;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
interface CaptureSessionInterface
{
    void cancelIssuedCaptureRequests();
    
    void close();
    
    @NonNull
    List<CaptureConfig> getCaptureConfigs();
    
    @Nullable
    SessionConfig getSessionConfig();
    
    void issueCaptureRequests(@NonNull final List<CaptureConfig> p0);
    
    @NonNull
    ListenableFuture<Void> open(@NonNull final SessionConfig p0, @NonNull final CameraDevice p1, @NonNull final SynchronizedCaptureSessionOpener p2);
    
    @NonNull
    ListenableFuture<Void> release(final boolean p0);
    
    void setSessionConfig(@Nullable final SessionConfig p0);
}

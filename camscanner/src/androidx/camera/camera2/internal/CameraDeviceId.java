// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
abstract class CameraDeviceId
{
    @NonNull
    public static CameraDeviceId create(@NonNull final String s, @NonNull final String s2, @NonNull final String s3, @NonNull final String s4) {
        return new AutoValue_CameraDeviceId(s.toLowerCase(), s2.toLowerCase(), s3.toLowerCase(), s4.toLowerCase());
    }
    
    @NonNull
    public abstract String getBrand();
    
    @NonNull
    public abstract String getCameraId();
    
    @NonNull
    public abstract String getDevice();
    
    @NonNull
    public abstract String getModel();
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.camera2.internal.compat.CameraCaptureSessionCompat;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.annotation.Nullable;
import android.view.Surface;
import android.hardware.camera2.CameraDevice;
import java.util.concurrent.Executor;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import androidx.annotation.NonNull;
import android.hardware.camera2.CaptureRequest;
import java.util.List;
import android.hardware.camera2.CameraAccessException;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public interface SynchronizedCaptureSession
{
    void abortCaptures() throws CameraAccessException;
    
    int captureBurstRequests(@NonNull final List<CaptureRequest> p0, @NonNull final CameraCaptureSession$CaptureCallback p1) throws CameraAccessException;
    
    int captureBurstRequests(@NonNull final List<CaptureRequest> p0, @NonNull final Executor p1, @NonNull final CameraCaptureSession$CaptureCallback p2) throws CameraAccessException;
    
    int captureSingleRequest(@NonNull final CaptureRequest p0, @NonNull final CameraCaptureSession$CaptureCallback p1) throws CameraAccessException;
    
    int captureSingleRequest(@NonNull final CaptureRequest p0, @NonNull final Executor p1, @NonNull final CameraCaptureSession$CaptureCallback p2) throws CameraAccessException;
    
    void close();
    
    void finishClose();
    
    @NonNull
    CameraDevice getDevice();
    
    @Nullable
    Surface getInputSurface();
    
    @NonNull
    ListenableFuture<Void> getOpeningBlocker();
    
    @NonNull
    StateCallback getStateCallback();
    
    int setRepeatingBurstRequests(@NonNull final List<CaptureRequest> p0, @NonNull final CameraCaptureSession$CaptureCallback p1) throws CameraAccessException;
    
    int setRepeatingBurstRequests(@NonNull final List<CaptureRequest> p0, @NonNull final Executor p1, @NonNull final CameraCaptureSession$CaptureCallback p2) throws CameraAccessException;
    
    int setSingleRepeatingRequest(@NonNull final CaptureRequest p0, @NonNull final CameraCaptureSession$CaptureCallback p1) throws CameraAccessException;
    
    int setSingleRepeatingRequest(@NonNull final CaptureRequest p0, @NonNull final Executor p1, @NonNull final CameraCaptureSession$CaptureCallback p2) throws CameraAccessException;
    
    void stopRepeating() throws CameraAccessException;
    
    @NonNull
    CameraCaptureSessionCompat toCameraCaptureSessionCompat();
    
    public abstract static class StateCallback
    {
        void onActive(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
        }
        
        @RequiresApi(api = 26)
        void onCaptureQueueEmpty(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
        }
        
        public void onClosed(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
        }
        
        public void onConfigureFailed(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
        }
        
        void onConfigured(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
        }
        
        void onReady(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
        }
        
        void onSessionFinished(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
        }
        
        @RequiresApi(api = 23)
        void onSurfacePrepared(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession, @NonNull final Surface surface) {
        }
    }
}

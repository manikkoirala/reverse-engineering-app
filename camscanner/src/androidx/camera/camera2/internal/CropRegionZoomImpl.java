// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import android.hardware.camera2.CaptureResult;
import androidx.camera.core.CameraControl;
import android.hardware.camera2.TotalCaptureResult;
import androidx.camera.camera2.interop.ExperimentalCamera2Interop;
import androidx.annotation.OptIn;
import android.hardware.camera2.CaptureRequest$Key;
import android.hardware.camera2.CaptureRequest;
import androidx.camera.camera2.impl.Camera2ImplConfig;
import androidx.core.util.Preconditions;
import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import androidx.annotation.NonNull;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import android.graphics.Rect;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
final class CropRegionZoomImpl implements ZoomImpl
{
    public static final float MIN_DIGITAL_ZOOM = 1.0f;
    private final CameraCharacteristicsCompat mCameraCharacteristics;
    private Rect mCurrentCropRect;
    private Rect mPendingZoomCropRegion;
    private CallbackToFutureAdapter.Completer<Void> mPendingZoomRatioCompleter;
    
    CropRegionZoomImpl(@NonNull final CameraCharacteristicsCompat mCameraCharacteristics) {
        this.mCurrentCropRect = null;
        this.mPendingZoomCropRegion = null;
        this.mCameraCharacteristics = mCameraCharacteristics;
    }
    
    @NonNull
    private static Rect getCropRectByRatio(@NonNull final Rect rect, float n) {
        final float n2 = rect.width() / n;
        final float n3 = rect.height() / n;
        final float n4 = (rect.width() - n2) / 2.0f;
        n = (rect.height() - n3) / 2.0f;
        return new Rect((int)n4, (int)n, (int)(n4 + n2), (int)(n + n3));
    }
    
    private Rect getSensorRect() {
        return Preconditions.checkNotNull((Rect)this.mCameraCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<T>)CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE));
    }
    
    @OptIn(markerClass = { ExperimentalCamera2Interop.class })
    @Override
    public void addRequestOption(@NonNull final Camera2ImplConfig.Builder builder) {
        final Rect mCurrentCropRect = this.mCurrentCropRect;
        if (mCurrentCropRect != null) {
            builder.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Rect>)CaptureRequest.SCALER_CROP_REGION, mCurrentCropRect);
        }
    }
    
    @NonNull
    @Override
    public Rect getCropSensorRegion() {
        Rect rect = this.mCurrentCropRect;
        if (rect == null) {
            rect = this.getSensorRect();
        }
        return rect;
    }
    
    @Override
    public float getMaxZoom() {
        final Float n = this.mCameraCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<Float>)CameraCharacteristics.SCALER_AVAILABLE_MAX_DIGITAL_ZOOM);
        if (n == null) {
            return 1.0f;
        }
        if (n < this.getMinZoom()) {
            return this.getMinZoom();
        }
        return n;
    }
    
    @Override
    public float getMinZoom() {
        return 1.0f;
    }
    
    @Override
    public void onCaptureResult(@NonNull final TotalCaptureResult totalCaptureResult) {
        if (this.mPendingZoomRatioCompleter != null) {
            final CaptureRequest request = ((CaptureResult)totalCaptureResult).getRequest();
            Object o;
            if (request == null) {
                o = null;
            }
            else {
                o = request.get(CaptureRequest.SCALER_CROP_REGION);
            }
            final Rect mPendingZoomCropRegion = this.mPendingZoomCropRegion;
            if (mPendingZoomCropRegion != null && mPendingZoomCropRegion.equals(o)) {
                this.mPendingZoomRatioCompleter.set(null);
                this.mPendingZoomRatioCompleter = null;
                this.mPendingZoomCropRegion = null;
            }
        }
    }
    
    @Override
    public void resetZoom() {
        this.mPendingZoomCropRegion = null;
        this.mCurrentCropRect = null;
        final CallbackToFutureAdapter.Completer<Void> mPendingZoomRatioCompleter = this.mPendingZoomRatioCompleter;
        if (mPendingZoomRatioCompleter != null) {
            mPendingZoomRatioCompleter.setException(new CameraControl.OperationCanceledException("Camera is not active."));
            this.mPendingZoomRatioCompleter = null;
        }
    }
    
    @Override
    public void setZoomRatio(final float n, @NonNull final CallbackToFutureAdapter.Completer<Void> mPendingZoomRatioCompleter) {
        this.mCurrentCropRect = getCropRectByRatio(this.getSensorRect(), n);
        final CallbackToFutureAdapter.Completer<Void> mPendingZoomRatioCompleter2 = this.mPendingZoomRatioCompleter;
        if (mPendingZoomRatioCompleter2 != null) {
            mPendingZoomRatioCompleter2.setException(new CameraControl.OperationCanceledException("There is a new zoomRatio being set"));
        }
        this.mPendingZoomCropRegion = this.mCurrentCropRect;
        this.mPendingZoomRatioCompleter = mPendingZoomRatioCompleter;
    }
}

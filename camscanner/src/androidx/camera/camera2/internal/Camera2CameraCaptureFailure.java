// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.annotation.NonNull;
import android.hardware.camera2.CaptureFailure;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.CameraCaptureFailure;

@RequiresApi(21)
public final class Camera2CameraCaptureFailure extends CameraCaptureFailure
{
    private final CaptureFailure mCaptureFailure;
    
    public Camera2CameraCaptureFailure(@NonNull final Reason reason, @NonNull final CaptureFailure mCaptureFailure) {
        super(reason);
        this.mCaptureFailure = mCaptureFailure;
    }
    
    @NonNull
    public CaptureFailure getCaptureFailure() {
        return this.mCaptureFailure;
    }
}

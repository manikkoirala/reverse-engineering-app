// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import java.util.Iterator;
import androidx.camera.core.CameraUnavailableException;
import androidx.camera.core.InitializationException;
import androidx.camera.core.impl.CameraInfoInternal;
import androidx.camera.core.CameraInfo;
import java.util.Arrays;
import java.util.ArrayList;
import androidx.annotation.Nullable;
import androidx.camera.core.CameraSelector;
import androidx.annotation.NonNull;
import androidx.camera.camera2.internal.compat.CameraAccessExceptionCompat;
import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import java.util.List;
import androidx.camera.camera2.internal.compat.CameraManagerCompat;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
class CameraSelectionOptimizer
{
    private CameraSelectionOptimizer() {
    }
    
    private static String decideSkippedCameraIdByHeuristic(final CameraManagerCompat cameraManagerCompat, final Integer n, final List<String> list) throws CameraAccessExceptionCompat {
        final String s = null;
        if (n == null) {
            return null;
        }
        String s2 = s;
        if (list.contains("0")) {
            if (!list.contains("1")) {
                s2 = s;
            }
            else if (n == 1) {
                s2 = s;
                if (cameraManagerCompat.getCameraCharacteristicsCompat("0").get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.LENS_FACING) == 1) {
                    s2 = "1";
                }
            }
            else {
                s2 = s;
                if (n == 0) {
                    s2 = s;
                    if (cameraManagerCompat.getCameraCharacteristicsCompat("1").get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.LENS_FACING) == 0) {
                        s2 = "0";
                    }
                }
            }
        }
        return s2;
    }
    
    static List<String> getSelectedAvailableCameraIds(@NonNull final Camera2CameraFactory camera2CameraFactory, @Nullable final CameraSelector cameraSelector) throws InitializationException {
        try {
            final ArrayList list = new ArrayList();
            final List<String> list2 = Arrays.asList(camera2CameraFactory.getCameraManager().getCameraIdList());
            if (cameraSelector == null) {
                final Iterator<String> iterator = list2.iterator();
                while (iterator.hasNext()) {
                    list.add(iterator.next());
                }
                return list;
            }
            String decideSkippedCameraIdByHeuristic;
            try {
                decideSkippedCameraIdByHeuristic = decideSkippedCameraIdByHeuristic(camera2CameraFactory.getCameraManager(), cameraSelector.getLensFacing(), list2);
            }
            catch (final IllegalStateException ex) {
                decideSkippedCameraIdByHeuristic = null;
            }
            final ArrayList list3 = new ArrayList();
            for (final String s : list2) {
                if (s.equals(decideSkippedCameraIdByHeuristic)) {
                    continue;
                }
                list3.add(camera2CameraFactory.getCameraInfo(s));
            }
            final Iterator<CameraInfo> iterator3 = cameraSelector.filter(list3).iterator();
            while (iterator3.hasNext()) {
                list.add(((CameraInfoInternal)iterator3.next()).getCameraId());
            }
            return list;
        }
        catch (final CameraUnavailableException ex2) {
            throw new InitializationException(ex2);
        }
        catch (final CameraAccessExceptionCompat cameraAccessExceptionCompat) {
            throw new InitializationException(CameraUnavailableExceptionHelper.createFrom(cameraAccessExceptionCompat));
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.core.impl.SurfaceConfig;
import androidx.annotation.NonNull;
import java.util.Collection;
import java.util.ArrayList;
import androidx.camera.core.impl.SurfaceCombination;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class GuaranteedConfigurationsUtil
{
    private GuaranteedConfigurationsUtil() {
    }
    
    @NonNull
    public static List<SurfaceCombination> generateSupportedCombinationList(final int n, final boolean b, final boolean b2) {
        final ArrayList list = new ArrayList();
        list.addAll(getLegacySupportedCombinationList());
        if (n == 0 || n == 1 || n == 3) {
            list.addAll(getLimitedSupportedCombinationList());
        }
        if (n == 1 || n == 3) {
            list.addAll(getFullSupportedCombinationList());
        }
        if (b) {
            list.addAll(getRAWSupportedCombinationList());
        }
        if (b2 && n == 0) {
            list.addAll(getBurstSupportedCombinationList());
        }
        if (n == 3) {
            list.addAll(getLevel3SupportedCombinationList());
        }
        return list;
    }
    
    @NonNull
    public static List<SurfaceCombination> getBurstSupportedCombinationList() {
        final ArrayList list = new ArrayList();
        final SurfaceCombination surfaceCombination = new SurfaceCombination();
        final SurfaceConfig.ConfigType priv = SurfaceConfig.ConfigType.PRIV;
        final SurfaceConfig.ConfigSize preview = SurfaceConfig.ConfigSize.PREVIEW;
        surfaceCombination.addSurfaceConfig(SurfaceConfig.create(priv, preview));
        final SurfaceConfig.ConfigSize maximum = SurfaceConfig.ConfigSize.MAXIMUM;
        surfaceCombination.addSurfaceConfig(SurfaceConfig.create(priv, maximum));
        list.add(surfaceCombination);
        final SurfaceCombination surfaceCombination2 = new SurfaceCombination();
        surfaceCombination2.addSurfaceConfig(SurfaceConfig.create(priv, preview));
        final SurfaceConfig.ConfigType yuv = SurfaceConfig.ConfigType.YUV;
        surfaceCombination2.addSurfaceConfig(SurfaceConfig.create(yuv, maximum));
        list.add(surfaceCombination2);
        final SurfaceCombination surfaceCombination3 = new SurfaceCombination();
        surfaceCombination3.addSurfaceConfig(SurfaceConfig.create(yuv, preview));
        surfaceCombination3.addSurfaceConfig(SurfaceConfig.create(yuv, maximum));
        list.add(surfaceCombination3);
        return list;
    }
    
    @NonNull
    public static List<SurfaceCombination> getFullSupportedCombinationList() {
        final ArrayList list = new ArrayList();
        final SurfaceCombination surfaceCombination = new SurfaceCombination();
        final SurfaceConfig.ConfigType priv = SurfaceConfig.ConfigType.PRIV;
        final SurfaceConfig.ConfigSize preview = SurfaceConfig.ConfigSize.PREVIEW;
        surfaceCombination.addSurfaceConfig(SurfaceConfig.create(priv, preview));
        final SurfaceConfig.ConfigSize maximum = SurfaceConfig.ConfigSize.MAXIMUM;
        surfaceCombination.addSurfaceConfig(SurfaceConfig.create(priv, maximum));
        list.add(surfaceCombination);
        final SurfaceCombination surfaceCombination2 = new SurfaceCombination();
        surfaceCombination2.addSurfaceConfig(SurfaceConfig.create(priv, preview));
        final SurfaceConfig.ConfigType yuv = SurfaceConfig.ConfigType.YUV;
        surfaceCombination2.addSurfaceConfig(SurfaceConfig.create(yuv, maximum));
        list.add(surfaceCombination2);
        final SurfaceCombination surfaceCombination3 = new SurfaceCombination();
        surfaceCombination3.addSurfaceConfig(SurfaceConfig.create(yuv, preview));
        surfaceCombination3.addSurfaceConfig(SurfaceConfig.create(yuv, maximum));
        list.add(surfaceCombination3);
        final SurfaceCombination surfaceCombination4 = new SurfaceCombination();
        surfaceCombination4.addSurfaceConfig(SurfaceConfig.create(priv, preview));
        surfaceCombination4.addSurfaceConfig(SurfaceConfig.create(priv, preview));
        surfaceCombination4.addSurfaceConfig(SurfaceConfig.create(SurfaceConfig.ConfigType.JPEG, maximum));
        list.add(surfaceCombination4);
        final SurfaceCombination surfaceCombination5 = new SurfaceCombination();
        final SurfaceConfig.ConfigSize vga = SurfaceConfig.ConfigSize.VGA;
        surfaceCombination5.addSurfaceConfig(SurfaceConfig.create(yuv, vga));
        surfaceCombination5.addSurfaceConfig(SurfaceConfig.create(priv, preview));
        surfaceCombination5.addSurfaceConfig(SurfaceConfig.create(yuv, maximum));
        list.add(surfaceCombination5);
        final SurfaceCombination surfaceCombination6 = new SurfaceCombination();
        surfaceCombination6.addSurfaceConfig(SurfaceConfig.create(yuv, vga));
        surfaceCombination6.addSurfaceConfig(SurfaceConfig.create(yuv, preview));
        surfaceCombination6.addSurfaceConfig(SurfaceConfig.create(yuv, maximum));
        list.add(surfaceCombination6);
        return list;
    }
    
    @NonNull
    public static List<SurfaceCombination> getLegacySupportedCombinationList() {
        final ArrayList list = new ArrayList();
        final SurfaceCombination surfaceCombination = new SurfaceCombination();
        final SurfaceConfig.ConfigType priv = SurfaceConfig.ConfigType.PRIV;
        final SurfaceConfig.ConfigSize maximum = SurfaceConfig.ConfigSize.MAXIMUM;
        surfaceCombination.addSurfaceConfig(SurfaceConfig.create(priv, maximum));
        list.add(surfaceCombination);
        final SurfaceCombination surfaceCombination2 = new SurfaceCombination();
        final SurfaceConfig.ConfigType jpeg = SurfaceConfig.ConfigType.JPEG;
        surfaceCombination2.addSurfaceConfig(SurfaceConfig.create(jpeg, maximum));
        list.add(surfaceCombination2);
        final SurfaceCombination surfaceCombination3 = new SurfaceCombination();
        final SurfaceConfig.ConfigType yuv = SurfaceConfig.ConfigType.YUV;
        surfaceCombination3.addSurfaceConfig(SurfaceConfig.create(yuv, maximum));
        list.add(surfaceCombination3);
        final SurfaceCombination surfaceCombination4 = new SurfaceCombination();
        final SurfaceConfig.ConfigSize preview = SurfaceConfig.ConfigSize.PREVIEW;
        surfaceCombination4.addSurfaceConfig(SurfaceConfig.create(priv, preview));
        surfaceCombination4.addSurfaceConfig(SurfaceConfig.create(jpeg, maximum));
        list.add(surfaceCombination4);
        final SurfaceCombination surfaceCombination5 = new SurfaceCombination();
        surfaceCombination5.addSurfaceConfig(SurfaceConfig.create(yuv, preview));
        surfaceCombination5.addSurfaceConfig(SurfaceConfig.create(jpeg, maximum));
        list.add(surfaceCombination5);
        final SurfaceCombination surfaceCombination6 = new SurfaceCombination();
        surfaceCombination6.addSurfaceConfig(SurfaceConfig.create(priv, preview));
        surfaceCombination6.addSurfaceConfig(SurfaceConfig.create(priv, preview));
        list.add(surfaceCombination6);
        final SurfaceCombination surfaceCombination7 = new SurfaceCombination();
        surfaceCombination7.addSurfaceConfig(SurfaceConfig.create(priv, preview));
        surfaceCombination7.addSurfaceConfig(SurfaceConfig.create(yuv, preview));
        list.add(surfaceCombination7);
        final SurfaceCombination surfaceCombination8 = new SurfaceCombination();
        surfaceCombination8.addSurfaceConfig(SurfaceConfig.create(priv, preview));
        surfaceCombination8.addSurfaceConfig(SurfaceConfig.create(yuv, preview));
        surfaceCombination8.addSurfaceConfig(SurfaceConfig.create(jpeg, maximum));
        list.add(surfaceCombination8);
        return list;
    }
    
    @NonNull
    public static List<SurfaceCombination> getLevel3SupportedCombinationList() {
        final ArrayList list = new ArrayList();
        final SurfaceCombination surfaceCombination = new SurfaceCombination();
        final SurfaceConfig.ConfigType priv = SurfaceConfig.ConfigType.PRIV;
        final SurfaceConfig.ConfigSize preview = SurfaceConfig.ConfigSize.PREVIEW;
        surfaceCombination.addSurfaceConfig(SurfaceConfig.create(priv, preview));
        final SurfaceConfig.ConfigSize vga = SurfaceConfig.ConfigSize.VGA;
        surfaceCombination.addSurfaceConfig(SurfaceConfig.create(priv, vga));
        final SurfaceConfig.ConfigType yuv = SurfaceConfig.ConfigType.YUV;
        final SurfaceConfig.ConfigSize maximum = SurfaceConfig.ConfigSize.MAXIMUM;
        surfaceCombination.addSurfaceConfig(SurfaceConfig.create(yuv, maximum));
        final SurfaceConfig.ConfigType raw = SurfaceConfig.ConfigType.RAW;
        surfaceCombination.addSurfaceConfig(SurfaceConfig.create(raw, maximum));
        list.add(surfaceCombination);
        final SurfaceCombination surfaceCombination2 = new SurfaceCombination();
        surfaceCombination2.addSurfaceConfig(SurfaceConfig.create(priv, preview));
        surfaceCombination2.addSurfaceConfig(SurfaceConfig.create(priv, vga));
        surfaceCombination2.addSurfaceConfig(SurfaceConfig.create(SurfaceConfig.ConfigType.JPEG, maximum));
        surfaceCombination2.addSurfaceConfig(SurfaceConfig.create(raw, maximum));
        list.add(surfaceCombination2);
        return list;
    }
    
    @NonNull
    public static List<SurfaceCombination> getLimitedSupportedCombinationList() {
        final ArrayList list = new ArrayList();
        final SurfaceCombination surfaceCombination = new SurfaceCombination();
        final SurfaceConfig.ConfigType priv = SurfaceConfig.ConfigType.PRIV;
        final SurfaceConfig.ConfigSize preview = SurfaceConfig.ConfigSize.PREVIEW;
        surfaceCombination.addSurfaceConfig(SurfaceConfig.create(priv, preview));
        final SurfaceConfig.ConfigSize record = SurfaceConfig.ConfigSize.RECORD;
        surfaceCombination.addSurfaceConfig(SurfaceConfig.create(priv, record));
        list.add(surfaceCombination);
        final SurfaceCombination surfaceCombination2 = new SurfaceCombination();
        surfaceCombination2.addSurfaceConfig(SurfaceConfig.create(priv, preview));
        final SurfaceConfig.ConfigType yuv = SurfaceConfig.ConfigType.YUV;
        surfaceCombination2.addSurfaceConfig(SurfaceConfig.create(yuv, record));
        list.add(surfaceCombination2);
        final SurfaceCombination surfaceCombination3 = new SurfaceCombination();
        surfaceCombination3.addSurfaceConfig(SurfaceConfig.create(yuv, preview));
        surfaceCombination3.addSurfaceConfig(SurfaceConfig.create(yuv, record));
        list.add(surfaceCombination3);
        final SurfaceCombination surfaceCombination4 = new SurfaceCombination();
        surfaceCombination4.addSurfaceConfig(SurfaceConfig.create(priv, preview));
        surfaceCombination4.addSurfaceConfig(SurfaceConfig.create(priv, record));
        final SurfaceConfig.ConfigType jpeg = SurfaceConfig.ConfigType.JPEG;
        surfaceCombination4.addSurfaceConfig(SurfaceConfig.create(jpeg, record));
        list.add(surfaceCombination4);
        final SurfaceCombination surfaceCombination5 = new SurfaceCombination();
        surfaceCombination5.addSurfaceConfig(SurfaceConfig.create(priv, preview));
        surfaceCombination5.addSurfaceConfig(SurfaceConfig.create(yuv, record));
        surfaceCombination5.addSurfaceConfig(SurfaceConfig.create(jpeg, record));
        list.add(surfaceCombination5);
        final SurfaceCombination surfaceCombination6 = new SurfaceCombination();
        surfaceCombination6.addSurfaceConfig(SurfaceConfig.create(yuv, preview));
        surfaceCombination6.addSurfaceConfig(SurfaceConfig.create(yuv, preview));
        surfaceCombination6.addSurfaceConfig(SurfaceConfig.create(jpeg, SurfaceConfig.ConfigSize.MAXIMUM));
        list.add(surfaceCombination6);
        return list;
    }
    
    @NonNull
    public static List<SurfaceCombination> getRAWSupportedCombinationList() {
        final ArrayList list = new ArrayList();
        final SurfaceCombination surfaceCombination = new SurfaceCombination();
        final SurfaceConfig.ConfigType raw = SurfaceConfig.ConfigType.RAW;
        final SurfaceConfig.ConfigSize maximum = SurfaceConfig.ConfigSize.MAXIMUM;
        surfaceCombination.addSurfaceConfig(SurfaceConfig.create(raw, maximum));
        list.add(surfaceCombination);
        final SurfaceCombination surfaceCombination2 = new SurfaceCombination();
        final SurfaceConfig.ConfigType priv = SurfaceConfig.ConfigType.PRIV;
        final SurfaceConfig.ConfigSize preview = SurfaceConfig.ConfigSize.PREVIEW;
        surfaceCombination2.addSurfaceConfig(SurfaceConfig.create(priv, preview));
        surfaceCombination2.addSurfaceConfig(SurfaceConfig.create(raw, maximum));
        list.add(surfaceCombination2);
        final SurfaceCombination surfaceCombination3 = new SurfaceCombination();
        final SurfaceConfig.ConfigType yuv = SurfaceConfig.ConfigType.YUV;
        surfaceCombination3.addSurfaceConfig(SurfaceConfig.create(yuv, preview));
        surfaceCombination3.addSurfaceConfig(SurfaceConfig.create(raw, maximum));
        list.add(surfaceCombination3);
        final SurfaceCombination surfaceCombination4 = new SurfaceCombination();
        surfaceCombination4.addSurfaceConfig(SurfaceConfig.create(priv, preview));
        surfaceCombination4.addSurfaceConfig(SurfaceConfig.create(priv, preview));
        surfaceCombination4.addSurfaceConfig(SurfaceConfig.create(raw, maximum));
        list.add(surfaceCombination4);
        final SurfaceCombination surfaceCombination5 = new SurfaceCombination();
        surfaceCombination5.addSurfaceConfig(SurfaceConfig.create(priv, preview));
        surfaceCombination5.addSurfaceConfig(SurfaceConfig.create(yuv, preview));
        surfaceCombination5.addSurfaceConfig(SurfaceConfig.create(raw, maximum));
        list.add(surfaceCombination5);
        final SurfaceCombination surfaceCombination6 = new SurfaceCombination();
        surfaceCombination6.addSurfaceConfig(SurfaceConfig.create(yuv, preview));
        surfaceCombination6.addSurfaceConfig(SurfaceConfig.create(yuv, preview));
        surfaceCombination6.addSurfaceConfig(SurfaceConfig.create(raw, maximum));
        list.add(surfaceCombination6);
        final SurfaceCombination surfaceCombination7 = new SurfaceCombination();
        surfaceCombination7.addSurfaceConfig(SurfaceConfig.create(priv, preview));
        final SurfaceConfig.ConfigType jpeg = SurfaceConfig.ConfigType.JPEG;
        surfaceCombination7.addSurfaceConfig(SurfaceConfig.create(jpeg, maximum));
        surfaceCombination7.addSurfaceConfig(SurfaceConfig.create(raw, maximum));
        list.add(surfaceCombination7);
        final SurfaceCombination surfaceCombination8 = new SurfaceCombination();
        surfaceCombination8.addSurfaceConfig(SurfaceConfig.create(yuv, preview));
        surfaceCombination8.addSurfaceConfig(SurfaceConfig.create(jpeg, maximum));
        surfaceCombination8.addSurfaceConfig(SurfaceConfig.create(raw, maximum));
        list.add(surfaceCombination8);
        return list;
    }
}

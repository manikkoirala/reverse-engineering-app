// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import android.view.Surface;
import androidx.camera.camera2.internal.compat.ApiCompat;
import android.hardware.camera2.CameraCaptureSession;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Arrays;
import androidx.annotation.NonNull;
import android.hardware.camera2.CameraCaptureSession$StateCallback;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class CameraCaptureSessionStateCallbacks
{
    private CameraCaptureSessionStateCallbacks() {
    }
    
    @NonNull
    public static CameraCaptureSession$StateCallback createComboCallback(@NonNull final List<CameraCaptureSession$StateCallback> list) {
        if (list.isEmpty()) {
            return createNoOpCallback();
        }
        if (list.size() == 1) {
            return list.get(0);
        }
        return new ComboSessionStateCallback(list);
    }
    
    @NonNull
    public static CameraCaptureSession$StateCallback createComboCallback(@NonNull final CameraCaptureSession$StateCallback... a) {
        return createComboCallback(Arrays.asList(a));
    }
    
    @NonNull
    public static CameraCaptureSession$StateCallback createNoOpCallback() {
        return new NoOpSessionStateCallback();
    }
    
    @RequiresApi(21)
    static final class ComboSessionStateCallback extends CameraCaptureSession$StateCallback
    {
        private final List<CameraCaptureSession$StateCallback> mCallbacks;
        
        ComboSessionStateCallback(@NonNull final List<CameraCaptureSession$StateCallback> list) {
            this.mCallbacks = new ArrayList<CameraCaptureSession$StateCallback>();
            for (final CameraCaptureSession$StateCallback cameraCaptureSession$StateCallback : list) {
                if (!(cameraCaptureSession$StateCallback instanceof NoOpSessionStateCallback)) {
                    this.mCallbacks.add(cameraCaptureSession$StateCallback);
                }
            }
        }
        
        public void onActive(@NonNull final CameraCaptureSession cameraCaptureSession) {
            final Iterator<CameraCaptureSession$StateCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onActive(cameraCaptureSession);
            }
        }
        
        @RequiresApi(api = 26)
        public void onCaptureQueueEmpty(@NonNull final CameraCaptureSession cameraCaptureSession) {
            final Iterator<CameraCaptureSession$StateCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                ApiCompat.Api26Impl.onCaptureQueueEmpty(iterator.next(), cameraCaptureSession);
            }
        }
        
        public void onClosed(@NonNull final CameraCaptureSession cameraCaptureSession) {
            final Iterator<CameraCaptureSession$StateCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onClosed(cameraCaptureSession);
            }
        }
        
        public void onConfigureFailed(@NonNull final CameraCaptureSession cameraCaptureSession) {
            final Iterator<CameraCaptureSession$StateCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onConfigureFailed(cameraCaptureSession);
            }
        }
        
        public void onConfigured(@NonNull final CameraCaptureSession cameraCaptureSession) {
            final Iterator<CameraCaptureSession$StateCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onConfigured(cameraCaptureSession);
            }
        }
        
        public void onReady(@NonNull final CameraCaptureSession cameraCaptureSession) {
            final Iterator<CameraCaptureSession$StateCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onReady(cameraCaptureSession);
            }
        }
        
        @RequiresApi(api = 23)
        public void onSurfacePrepared(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final Surface surface) {
            final Iterator<CameraCaptureSession$StateCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                ApiCompat.Api23Impl.onSurfacePrepared(iterator.next(), cameraCaptureSession, surface);
            }
        }
    }
    
    static final class NoOpSessionStateCallback extends CameraCaptureSession$StateCallback
    {
        public void onActive(@NonNull final CameraCaptureSession cameraCaptureSession) {
        }
        
        public void onCaptureQueueEmpty(@NonNull final CameraCaptureSession cameraCaptureSession) {
        }
        
        public void onClosed(@NonNull final CameraCaptureSession cameraCaptureSession) {
        }
        
        public void onConfigureFailed(@NonNull final CameraCaptureSession cameraCaptureSession) {
        }
        
        public void onConfigured(@NonNull final CameraCaptureSession cameraCaptureSession) {
        }
        
        public void onReady(@NonNull final CameraCaptureSession cameraCaptureSession) {
        }
        
        public void onSurfacePrepared(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final Surface surface) {
        }
    }
}

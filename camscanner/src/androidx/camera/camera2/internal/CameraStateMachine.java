// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import java.util.Objects;
import androidx.camera.core.Logger;
import androidx.annotation.Nullable;
import androidx.camera.core.impl.CameraInternal;
import androidx.lifecycle.LiveData;
import androidx.camera.core.CameraState;
import androidx.lifecycle.MutableLiveData;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.CameraStateRegistry;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
class CameraStateMachine
{
    private static final String TAG = "CameraStateMachine";
    @NonNull
    private final CameraStateRegistry mCameraStateRegistry;
    @NonNull
    private final MutableLiveData<CameraState> mCameraStates;
    
    CameraStateMachine(@NonNull final CameraStateRegistry mCameraStateRegistry) {
        this.mCameraStateRegistry = mCameraStateRegistry;
        (this.mCameraStates = new MutableLiveData<CameraState>()).postValue(CameraState.create(CameraState.Type.CLOSED));
    }
    
    private CameraState onCameraPendingOpen() {
        CameraState cameraState;
        if (this.mCameraStateRegistry.isCameraClosing()) {
            cameraState = CameraState.create(CameraState.Type.OPENING);
        }
        else {
            cameraState = CameraState.create(CameraState.Type.PENDING_OPEN);
        }
        return cameraState;
    }
    
    @NonNull
    public LiveData<CameraState> getStateLiveData() {
        return this.mCameraStates;
    }
    
    public void updateState(@NonNull final CameraInternal.State state, @Nullable final CameraState.StateError obj) {
        CameraState obj2 = null;
        switch (CameraStateMachine$1.$SwitchMap$androidx$camera$core$impl$CameraInternal$State[state.ordinal()]) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown internal camera state: ");
                sb.append(state);
                throw new IllegalStateException(sb.toString());
            }
            case 6:
            case 7: {
                obj2 = CameraState.create(CameraState.Type.CLOSED, obj);
                break;
            }
            case 4:
            case 5: {
                obj2 = CameraState.create(CameraState.Type.CLOSING, obj);
                break;
            }
            case 3: {
                obj2 = CameraState.create(CameraState.Type.OPEN, obj);
                break;
            }
            case 2: {
                obj2 = CameraState.create(CameraState.Type.OPENING, obj);
                break;
            }
            case 1: {
                obj2 = this.onCameraPendingOpen();
                break;
            }
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("New public camera state ");
        sb2.append(obj2);
        sb2.append(" from ");
        sb2.append(state);
        sb2.append(" and ");
        sb2.append(obj);
        Logger.d("CameraStateMachine", sb2.toString());
        if (!Objects.equals(this.mCameraStates.getValue(), obj2)) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Publishing new public camera state ");
            sb3.append(obj2);
            Logger.d("CameraStateMachine", sb3.toString());
            this.mCameraStates.postValue(obj2);
        }
    }
}

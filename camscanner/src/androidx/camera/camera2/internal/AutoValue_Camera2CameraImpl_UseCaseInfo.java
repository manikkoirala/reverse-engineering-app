// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.camera.core.impl.UseCaseConfig;
import android.util.Size;
import androidx.camera.core.impl.SessionConfig;

final class AutoValue_Camera2CameraImpl_UseCaseInfo extends UseCaseInfo
{
    private final SessionConfig sessionConfig;
    private final Size surfaceResolution;
    private final UseCaseConfig<?> useCaseConfig;
    private final String useCaseId;
    private final Class<?> useCaseType;
    
    AutoValue_Camera2CameraImpl_UseCaseInfo(final String useCaseId, final Class<?> useCaseType, final SessionConfig sessionConfig, final UseCaseConfig<?> useCaseConfig, @Nullable final Size surfaceResolution) {
        if (useCaseId == null) {
            throw new NullPointerException("Null useCaseId");
        }
        this.useCaseId = useCaseId;
        if (useCaseType == null) {
            throw new NullPointerException("Null useCaseType");
        }
        this.useCaseType = useCaseType;
        if (sessionConfig == null) {
            throw new NullPointerException("Null sessionConfig");
        }
        this.sessionConfig = sessionConfig;
        if (useCaseConfig != null) {
            this.useCaseConfig = useCaseConfig;
            this.surfaceResolution = surfaceResolution;
            return;
        }
        throw new NullPointerException("Null useCaseConfig");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof UseCaseInfo) {
            final UseCaseInfo useCaseInfo = (UseCaseInfo)o;
            if (this.useCaseId.equals(useCaseInfo.getUseCaseId()) && this.useCaseType.equals(useCaseInfo.getUseCaseType()) && this.sessionConfig.equals(useCaseInfo.getSessionConfig()) && this.useCaseConfig.equals(useCaseInfo.getUseCaseConfig())) {
                final Size surfaceResolution = this.surfaceResolution;
                if (surfaceResolution == null) {
                    if (useCaseInfo.getSurfaceResolution() == null) {
                        return b;
                    }
                }
                else if (surfaceResolution.equals((Object)useCaseInfo.getSurfaceResolution())) {
                    return b;
                }
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @NonNull
    @Override
    SessionConfig getSessionConfig() {
        return this.sessionConfig;
    }
    
    @Nullable
    @Override
    Size getSurfaceResolution() {
        return this.surfaceResolution;
    }
    
    @NonNull
    @Override
    UseCaseConfig<?> getUseCaseConfig() {
        return this.useCaseConfig;
    }
    
    @NonNull
    @Override
    String getUseCaseId() {
        return this.useCaseId;
    }
    
    @NonNull
    @Override
    Class<?> getUseCaseType() {
        return this.useCaseType;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.useCaseId.hashCode();
        final int hashCode2 = this.useCaseType.hashCode();
        final int hashCode3 = this.sessionConfig.hashCode();
        final int hashCode4 = this.useCaseConfig.hashCode();
        final Size surfaceResolution = this.surfaceResolution;
        int hashCode5;
        if (surfaceResolution == null) {
            hashCode5 = 0;
        }
        else {
            hashCode5 = surfaceResolution.hashCode();
        }
        return ((((hashCode ^ 0xF4243) * 1000003 ^ hashCode2) * 1000003 ^ hashCode3) * 1000003 ^ hashCode4) * 1000003 ^ hashCode5;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("UseCaseInfo{useCaseId=");
        sb.append(this.useCaseId);
        sb.append(", useCaseType=");
        sb.append(this.useCaseType);
        sb.append(", sessionConfig=");
        sb.append(this.sessionConfig);
        sb.append(", useCaseConfig=");
        sb.append(this.useCaseConfig);
        sb.append(", surfaceResolution=");
        sb.append(this.surfaceResolution);
        sb.append("}");
        return sb.toString();
    }
}

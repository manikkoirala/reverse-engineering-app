// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.core.impl.CameraControlInternal;
import androidx.camera.core.impl.CameraCaptureFailure;
import androidx.camera.core.impl.CameraCaptureCallback;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.Nullable;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.camera.core.impl.Config;
import android.os.Build$VERSION;
import androidx.camera.core.impl.CaptureConfig;
import android.hardware.camera2.CaptureRequest$Key;
import android.hardware.camera2.CaptureRequest;
import androidx.camera.camera2.impl.Camera2ImplConfig;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import java.util.Iterator;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import android.graphics.PointF;
import androidx.camera.core.MeteringPoint;
import android.graphics.Rect;
import androidx.camera.core.CameraControl;
import java.util.concurrent.TimeUnit;
import androidx.camera.core.impl.CameraCaptureResult;
import androidx.camera.core.FocusMeteringAction;
import androidx.camera.core.impl.Quirks;
import java.util.concurrent.ScheduledExecutorService;
import androidx.camera.core.FocusMeteringResult;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import android.util.Rational;
import androidx.camera.camera2.internal.compat.workaround.MeteringRegionCorrection;
import java.util.concurrent.Executor;
import androidx.annotation.NonNull;
import java.util.concurrent.ScheduledFuture;
import android.hardware.camera2.params.MeteringRectangle;
import androidx.annotation.RequiresApi;
import androidx.camera.camera2.interop.ExperimentalCamera2Interop;
import androidx.annotation.OptIn;

@OptIn(markerClass = { ExperimentalCamera2Interop.class })
@RequiresApi(21)
class FocusMeteringControl
{
    static final long AUTO_FOCUS_TIMEOUT_DURATION = 5000L;
    private static final MeteringRectangle[] EMPTY_RECTANGLES;
    private MeteringRectangle[] mAeRects;
    private MeteringRectangle[] mAfRects;
    private ScheduledFuture<?> mAutoCancelHandle;
    private ScheduledFuture<?> mAutoFocusTimeoutHandle;
    private MeteringRectangle[] mAwbRects;
    private final Camera2CameraControlImpl mCameraControl;
    @NonNull
    Integer mCurrentAfState;
    final Executor mExecutor;
    long mFocusTimeoutCounter;
    private volatile boolean mIsActive;
    boolean mIsAutoFocusCompleted;
    boolean mIsFocusSuccessful;
    private boolean mIsInAfAutoMode;
    @NonNull
    private final MeteringRegionCorrection mMeteringRegionCorrection;
    private volatile Rational mPreviewAspectRatio;
    CallbackToFutureAdapter.Completer<FocusMeteringResult> mRunningActionCompleter;
    CallbackToFutureAdapter.Completer<Void> mRunningCancelCompleter;
    private final ScheduledExecutorService mScheduler;
    private Camera2CameraControlImpl.CaptureResultListener mSessionListenerForCancel;
    private Camera2CameraControlImpl.CaptureResultListener mSessionListenerForFocus;
    private int mTemplate;
    
    static {
        EMPTY_RECTANGLES = new MeteringRectangle[0];
    }
    
    FocusMeteringControl(@NonNull final Camera2CameraControlImpl mCameraControl, @NonNull final ScheduledExecutorService mScheduler, @NonNull final Executor mExecutor, @NonNull final Quirks quirks) {
        this.mIsActive = false;
        this.mPreviewAspectRatio = null;
        this.mIsInAfAutoMode = false;
        this.mCurrentAfState = 0;
        this.mFocusTimeoutCounter = 0L;
        this.mIsAutoFocusCompleted = false;
        this.mIsFocusSuccessful = false;
        this.mTemplate = 1;
        this.mSessionListenerForFocus = null;
        this.mSessionListenerForCancel = null;
        final MeteringRectangle[] empty_RECTANGLES = FocusMeteringControl.EMPTY_RECTANGLES;
        this.mAfRects = empty_RECTANGLES;
        this.mAeRects = empty_RECTANGLES;
        this.mAwbRects = empty_RECTANGLES;
        this.mRunningActionCompleter = null;
        this.mRunningCancelCompleter = null;
        this.mCameraControl = mCameraControl;
        this.mExecutor = mExecutor;
        this.mScheduler = mScheduler;
        this.mMeteringRegionCorrection = new MeteringRegionCorrection(quirks);
    }
    
    private void clearAutoFocusTimeoutHandle() {
        final ScheduledFuture<?> mAutoFocusTimeoutHandle = this.mAutoFocusTimeoutHandle;
        if (mAutoFocusTimeoutHandle != null) {
            mAutoFocusTimeoutHandle.cancel(true);
            this.mAutoFocusTimeoutHandle = null;
        }
    }
    
    private void completeCancelFuture() {
        final CallbackToFutureAdapter.Completer<Void> mRunningCancelCompleter = this.mRunningCancelCompleter;
        if (mRunningCancelCompleter != null) {
            mRunningCancelCompleter.set(null);
            this.mRunningCancelCompleter = null;
        }
    }
    
    private void disableAutoCancel() {
        final ScheduledFuture<?> mAutoCancelHandle = this.mAutoCancelHandle;
        if (mAutoCancelHandle != null) {
            mAutoCancelHandle.cancel(true);
            this.mAutoCancelHandle = null;
        }
    }
    
    private void executeMeteringAction(@NonNull final MeteringRectangle[] mAfRects, @NonNull final MeteringRectangle[] mAeRects, @NonNull final MeteringRectangle[] mAwbRects, final FocusMeteringAction focusMeteringAction, final long n) {
        this.mCameraControl.removeCaptureResultListener(this.mSessionListenerForFocus);
        this.disableAutoCancel();
        this.clearAutoFocusTimeoutHandle();
        this.mAfRects = mAfRects;
        this.mAeRects = mAeRects;
        this.mAwbRects = mAwbRects;
        long n2;
        if (this.shouldTriggerAF()) {
            this.mIsInAfAutoMode = true;
            this.mIsAutoFocusCompleted = false;
            this.mIsFocusSuccessful = false;
            n2 = this.mCameraControl.updateSessionConfigSynchronous();
            this.triggerAf(null, true);
        }
        else {
            this.mIsInAfAutoMode = false;
            this.mIsAutoFocusCompleted = true;
            this.mIsFocusSuccessful = false;
            n2 = this.mCameraControl.updateSessionConfigSynchronous();
        }
        this.mCurrentAfState = 0;
        final Oo\u3007O mSessionListenerForFocus = new Oo\u3007O(this, this.isAfModeSupported(), n2);
        this.mSessionListenerForFocus = mSessionListenerForFocus;
        this.mCameraControl.addCaptureResultListener((Camera2CameraControlImpl.CaptureResultListener)mSessionListenerForFocus);
        final long mFocusTimeoutCounter = this.mFocusTimeoutCounter + 1L;
        this.mFocusTimeoutCounter = mFocusTimeoutCounter;
        final O0 o0 = new O0(this, mFocusTimeoutCounter);
        final ScheduledExecutorService mScheduler = this.mScheduler;
        final TimeUnit milliseconds = TimeUnit.MILLISECONDS;
        this.mAutoFocusTimeoutHandle = mScheduler.schedule(o0, n, milliseconds);
        if (focusMeteringAction.isAutoCancelEnabled()) {
            this.mAutoCancelHandle = this.mScheduler.schedule(new ooOO(this, mFocusTimeoutCounter), focusMeteringAction.getAutoCancelDurationInMillis(), milliseconds);
        }
    }
    
    private void failActionFuture(final String s) {
        this.mCameraControl.removeCaptureResultListener(this.mSessionListenerForFocus);
        final CallbackToFutureAdapter.Completer<FocusMeteringResult> mRunningActionCompleter = this.mRunningActionCompleter;
        if (mRunningActionCompleter != null) {
            mRunningActionCompleter.setException(new CameraControl.OperationCanceledException(s));
            this.mRunningActionCompleter = null;
        }
    }
    
    private void failCancelFuture(final String s) {
        this.mCameraControl.removeCaptureResultListener(this.mSessionListenerForCancel);
        final CallbackToFutureAdapter.Completer<Void> mRunningCancelCompleter = this.mRunningCancelCompleter;
        if (mRunningCancelCompleter != null) {
            mRunningCancelCompleter.setException(new CameraControl.OperationCanceledException(s));
            this.mRunningCancelCompleter = null;
        }
    }
    
    private Rational getDefaultAspectRatio() {
        if (this.mPreviewAspectRatio != null) {
            return this.mPreviewAspectRatio;
        }
        final Rect cropSensorRegion = this.mCameraControl.getCropSensorRegion();
        return new Rational(cropSensorRegion.width(), cropSensorRegion.height());
    }
    
    private static PointF getFovAdjustedPoint(@NonNull final MeteringPoint meteringPoint, @NonNull final Rational rational, @NonNull Rational surfaceAspectRatio, final int n, final MeteringRegionCorrection meteringRegionCorrection) {
        if (meteringPoint.getSurfaceAspectRatio() != null) {
            surfaceAspectRatio = meteringPoint.getSurfaceAspectRatio();
        }
        final PointF correctedPoint = meteringRegionCorrection.getCorrectedPoint(meteringPoint, n);
        if (!surfaceAspectRatio.equals((Object)rational)) {
            if (surfaceAspectRatio.compareTo(rational) > 0) {
                final float n2 = (float)(surfaceAspectRatio.doubleValue() / rational.doubleValue());
                correctedPoint.y = ((float)((n2 - 1.0) / 2.0) + correctedPoint.y) * (1.0f / n2);
            }
            else {
                final float n3 = (float)(rational.doubleValue() / surfaceAspectRatio.doubleValue());
                correctedPoint.x = ((float)((n3 - 1.0) / 2.0) + correctedPoint.x) * (1.0f / n3);
            }
        }
        return correctedPoint;
    }
    
    private static MeteringRectangle getMeteringRect(final MeteringPoint meteringPoint, final PointF pointF, final Rect rect) {
        final int n = (int)(rect.left + pointF.x * rect.width());
        final int n2 = (int)(rect.top + pointF.y * rect.height());
        final int n3 = (int)(meteringPoint.getSize() * rect.width());
        final int n4 = (int)(meteringPoint.getSize() * rect.height());
        final int n5 = n3 / 2;
        final int n6 = n4 / 2;
        final Rect rect2 = new Rect(n - n5, n2 - n6, n + n5, n2 + n6);
        rect2.left = rangeLimit(rect2.left, rect.right, rect.left);
        rect2.right = rangeLimit(rect2.right, rect.right, rect.left);
        rect2.top = rangeLimit(rect2.top, rect.bottom, rect.top);
        rect2.bottom = rangeLimit(rect2.bottom, rect.bottom, rect.top);
        return new MeteringRectangle(rect2, 1000);
    }
    
    @NonNull
    private List<MeteringRectangle> getMeteringRectangles(@NonNull final List<MeteringPoint> list, final int n, @NonNull final Rational rational, @NonNull final Rect rect, final int n2) {
        if (!list.isEmpty() && n != 0) {
            final ArrayList list2 = new ArrayList();
            final Rational rational2 = new Rational(rect.width(), rect.height());
            for (final MeteringPoint meteringPoint : list) {
                if (list2.size() == n) {
                    break;
                }
                if (!isValid(meteringPoint)) {
                    continue;
                }
                final MeteringRectangle meteringRect = getMeteringRect(meteringPoint, getFovAdjustedPoint(meteringPoint, rational2, rational, n2, this.mMeteringRegionCorrection), rect);
                if (meteringRect.getWidth() == 0) {
                    continue;
                }
                if (meteringRect.getHeight() == 0) {
                    continue;
                }
                list2.add(meteringRect);
            }
            return (List<MeteringRectangle>)Collections.unmodifiableList((List<?>)list2);
        }
        return Collections.emptyList();
    }
    
    private boolean isAfModeSupported() {
        final Camera2CameraControlImpl mCameraControl = this.mCameraControl;
        boolean b = true;
        if (mCameraControl.getSupportedAfMode(1) != 1) {
            b = false;
        }
        return b;
    }
    
    private static boolean isValid(@NonNull final MeteringPoint meteringPoint) {
        return meteringPoint.getX() >= 0.0f && meteringPoint.getX() <= 1.0f && meteringPoint.getY() >= 0.0f && meteringPoint.getY() <= 1.0f;
    }
    
    private static int rangeLimit(final int a, final int b, final int b2) {
        return Math.min(Math.max(a, b2), b);
    }
    
    private boolean shouldTriggerAF() {
        return this.mAfRects.length > 0;
    }
    
    void addFocusMeteringOptions(@NonNull final Camera2ImplConfig.Builder builder) {
        int defaultAfMode;
        if (this.mIsInAfAutoMode) {
            defaultAfMode = 1;
        }
        else {
            defaultAfMode = this.getDefaultAfMode();
        }
        builder.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.CONTROL_AF_MODE, this.mCameraControl.getSupportedAfMode(defaultAfMode));
        final MeteringRectangle[] mAfRects = this.mAfRects;
        if (mAfRects.length != 0) {
            builder.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<MeteringRectangle[]>)CaptureRequest.CONTROL_AF_REGIONS, mAfRects);
        }
        final MeteringRectangle[] mAeRects = this.mAeRects;
        if (mAeRects.length != 0) {
            builder.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<MeteringRectangle[]>)CaptureRequest.CONTROL_AE_REGIONS, mAeRects);
        }
        final MeteringRectangle[] mAwbRects = this.mAwbRects;
        if (mAwbRects.length != 0) {
            builder.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<MeteringRectangle[]>)CaptureRequest.CONTROL_AWB_REGIONS, mAwbRects);
        }
    }
    
    void cancelAfAeTrigger(final boolean b, final boolean b2) {
        if (!this.mIsActive) {
            return;
        }
        final CaptureConfig.Builder builder = new CaptureConfig.Builder();
        builder.setUseRepeatingSurface(true);
        builder.setTemplateType(this.mTemplate);
        final Camera2ImplConfig.Builder builder2 = new Camera2ImplConfig.Builder();
        if (b) {
            builder2.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.CONTROL_AF_TRIGGER, 2);
        }
        if (Build$VERSION.SDK_INT >= 23 && b2) {
            builder2.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER, 2);
        }
        builder.addImplementationOptions(builder2.build());
        this.mCameraControl.submitCaptureRequestsInternal(Collections.singletonList(builder.build()));
    }
    
    ListenableFuture<Void> cancelFocusAndMetering() {
        return CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new O0O8OO088(this));
    }
    
    void cancelFocusAndMeteringInternal(@Nullable final CallbackToFutureAdapter.Completer<Void> mRunningCancelCompleter) {
        this.failCancelFuture("Cancelled by another cancelFocusAndMetering()");
        this.failActionFuture("Cancelled by cancelFocusAndMetering()");
        this.mRunningCancelCompleter = mRunningCancelCompleter;
        this.disableAutoCancel();
        this.clearAutoFocusTimeoutHandle();
        if (this.shouldTriggerAF()) {
            this.cancelAfAeTrigger(true, false);
        }
        final MeteringRectangle[] empty_RECTANGLES = FocusMeteringControl.EMPTY_RECTANGLES;
        this.mAfRects = empty_RECTANGLES;
        this.mAeRects = empty_RECTANGLES;
        this.mAwbRects = empty_RECTANGLES;
        this.mIsInAfAutoMode = false;
        final long updateSessionConfigSynchronous = this.mCameraControl.updateSessionConfigSynchronous();
        if (this.mRunningCancelCompleter != null) {
            final OOO8o\u3007\u3007 mSessionListenerForCancel = new OOO8o\u3007\u3007(this, this.mCameraControl.getSupportedAfMode(this.getDefaultAfMode()), updateSessionConfigSynchronous);
            this.mSessionListenerForCancel = mSessionListenerForCancel;
            this.mCameraControl.addCaptureResultListener((Camera2CameraControlImpl.CaptureResultListener)mSessionListenerForCancel);
        }
    }
    
    void cancelFocusAndMeteringWithoutAsyncResult() {
        this.cancelFocusAndMeteringInternal(null);
    }
    
    void completeActionFuture(final boolean b) {
        this.clearAutoFocusTimeoutHandle();
        final CallbackToFutureAdapter.Completer<FocusMeteringResult> mRunningActionCompleter = this.mRunningActionCompleter;
        if (mRunningActionCompleter != null) {
            mRunningActionCompleter.set(FocusMeteringResult.create(b));
            this.mRunningActionCompleter = null;
        }
    }
    
    @VisibleForTesting
    int getDefaultAfMode() {
        if (this.mTemplate != 3) {
            return 4;
        }
        return 3;
    }
    
    boolean isFocusMeteringSupported(@NonNull final FocusMeteringAction focusMeteringAction) {
        final Rect cropSensorRegion = this.mCameraControl.getCropSensorRegion();
        final Rational defaultAspectRatio = this.getDefaultAspectRatio();
        final List<MeteringRectangle> meteringRectangles = this.getMeteringRectangles(focusMeteringAction.getMeteringPointsAf(), this.mCameraControl.getMaxAfRegionCount(), defaultAspectRatio, cropSensorRegion, 1);
        final List<MeteringRectangle> meteringRectangles2 = this.getMeteringRectangles(focusMeteringAction.getMeteringPointsAe(), this.mCameraControl.getMaxAeRegionCount(), defaultAspectRatio, cropSensorRegion, 2);
        final List<MeteringRectangle> meteringRectangles3 = this.getMeteringRectangles(focusMeteringAction.getMeteringPointsAwb(), this.mCameraControl.getMaxAwbRegionCount(), defaultAspectRatio, cropSensorRegion, 4);
        return !meteringRectangles.isEmpty() || !meteringRectangles2.isEmpty() || !meteringRectangles3.isEmpty();
    }
    
    void setActive(final boolean mIsActive) {
        if (mIsActive == this.mIsActive) {
            return;
        }
        if (!(this.mIsActive = mIsActive)) {
            this.cancelFocusAndMeteringWithoutAsyncResult();
        }
    }
    
    public void setPreviewAspectRatio(@Nullable final Rational mPreviewAspectRatio) {
        this.mPreviewAspectRatio = mPreviewAspectRatio;
    }
    
    void setTemplate(final int mTemplate) {
        this.mTemplate = mTemplate;
    }
    
    @NonNull
    ListenableFuture<FocusMeteringResult> startFocusAndMetering(@NonNull final FocusMeteringAction focusMeteringAction) {
        return this.startFocusAndMetering(focusMeteringAction, 5000L);
    }
    
    @NonNull
    @VisibleForTesting
    ListenableFuture<FocusMeteringResult> startFocusAndMetering(@NonNull final FocusMeteringAction focusMeteringAction, final long n) {
        return CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<FocusMeteringResult>)new o88\u3007OO08\u3007(this, focusMeteringAction, n));
    }
    
    void startFocusAndMeteringInternal(@NonNull final CallbackToFutureAdapter.Completer<FocusMeteringResult> mRunningActionCompleter, @NonNull final FocusMeteringAction focusMeteringAction, final long n) {
        if (!this.mIsActive) {
            mRunningActionCompleter.setException(new CameraControl.OperationCanceledException("Camera is not active."));
            return;
        }
        final Rect cropSensorRegion = this.mCameraControl.getCropSensorRegion();
        final Rational defaultAspectRatio = this.getDefaultAspectRatio();
        final List<MeteringRectangle> meteringRectangles = this.getMeteringRectangles(focusMeteringAction.getMeteringPointsAf(), this.mCameraControl.getMaxAfRegionCount(), defaultAspectRatio, cropSensorRegion, 1);
        final List<MeteringRectangle> meteringRectangles2 = this.getMeteringRectangles(focusMeteringAction.getMeteringPointsAe(), this.mCameraControl.getMaxAeRegionCount(), defaultAspectRatio, cropSensorRegion, 2);
        final List<MeteringRectangle> meteringRectangles3 = this.getMeteringRectangles(focusMeteringAction.getMeteringPointsAwb(), this.mCameraControl.getMaxAwbRegionCount(), defaultAspectRatio, cropSensorRegion, 4);
        if (meteringRectangles.isEmpty() && meteringRectangles2.isEmpty() && meteringRectangles3.isEmpty()) {
            mRunningActionCompleter.setException(new IllegalArgumentException("None of the specified AF/AE/AWB MeteringPoints is supported on this camera."));
            return;
        }
        this.failActionFuture("Cancelled by another startFocusAndMetering()");
        this.failCancelFuture("Cancelled by another startFocusAndMetering()");
        this.disableAutoCancel();
        this.mRunningActionCompleter = mRunningActionCompleter;
        final MeteringRectangle[] empty_RECTANGLES = FocusMeteringControl.EMPTY_RECTANGLES;
        this.executeMeteringAction(meteringRectangles.toArray(empty_RECTANGLES), meteringRectangles2.toArray(empty_RECTANGLES), meteringRectangles3.toArray(empty_RECTANGLES), focusMeteringAction, n);
    }
    
    void triggerAePrecapture(@Nullable final CallbackToFutureAdapter.Completer<Void> completer) {
        if (!this.mIsActive) {
            if (completer != null) {
                completer.setException(new CameraControl.OperationCanceledException("Camera is not active."));
            }
            return;
        }
        final CaptureConfig.Builder builder = new CaptureConfig.Builder();
        builder.setTemplateType(this.mTemplate);
        builder.setUseRepeatingSurface(true);
        final Camera2ImplConfig.Builder builder2 = new Camera2ImplConfig.Builder();
        builder2.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER, 1);
        builder.addImplementationOptions(builder2.build());
        builder.addCameraCaptureCallback(new CameraCaptureCallback(this, completer) {
            final FocusMeteringControl this$0;
            final CallbackToFutureAdapter.Completer val$completer;
            
            @Override
            public void onCaptureCancelled() {
                final CallbackToFutureAdapter.Completer val$completer = this.val$completer;
                if (val$completer != null) {
                    val$completer.setException(new CameraControl.OperationCanceledException("Camera is closed"));
                }
            }
            
            @Override
            public void onCaptureCompleted(@NonNull final CameraCaptureResult cameraCaptureResult) {
                final CallbackToFutureAdapter.Completer val$completer = this.val$completer;
                if (val$completer != null) {
                    val$completer.set(null);
                }
            }
            
            @Override
            public void onCaptureFailed(@NonNull final CameraCaptureFailure cameraCaptureFailure) {
                final CallbackToFutureAdapter.Completer val$completer = this.val$completer;
                if (val$completer != null) {
                    val$completer.setException(new CameraControlInternal.CameraControlException(cameraCaptureFailure));
                }
            }
        });
        this.mCameraControl.submitCaptureRequestsInternal(Collections.singletonList(builder.build()));
    }
    
    void triggerAf(@Nullable final CallbackToFutureAdapter.Completer<CameraCaptureResult> completer, final boolean b) {
        if (!this.mIsActive) {
            if (completer != null) {
                completer.setException(new CameraControl.OperationCanceledException("Camera is not active."));
            }
            return;
        }
        final CaptureConfig.Builder builder = new CaptureConfig.Builder();
        builder.setTemplateType(this.mTemplate);
        builder.setUseRepeatingSurface(true);
        final Camera2ImplConfig.Builder builder2 = new Camera2ImplConfig.Builder();
        builder2.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.CONTROL_AF_TRIGGER, 1);
        if (b) {
            builder2.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.CONTROL_AE_MODE, this.mCameraControl.getSupportedAeMode(1));
        }
        builder.addImplementationOptions(builder2.build());
        builder.addCameraCaptureCallback(new CameraCaptureCallback(this, completer) {
            final FocusMeteringControl this$0;
            final CallbackToFutureAdapter.Completer val$completer;
            
            @Override
            public void onCaptureCancelled() {
                final CallbackToFutureAdapter.Completer val$completer = this.val$completer;
                if (val$completer != null) {
                    val$completer.setException(new CameraControl.OperationCanceledException("Camera is closed"));
                }
            }
            
            @Override
            public void onCaptureCompleted(@NonNull final CameraCaptureResult cameraCaptureResult) {
                final CallbackToFutureAdapter.Completer val$completer = this.val$completer;
                if (val$completer != null) {
                    val$completer.set(cameraCaptureResult);
                }
            }
            
            @Override
            public void onCaptureFailed(@NonNull final CameraCaptureFailure cameraCaptureFailure) {
                final CallbackToFutureAdapter.Completer val$completer = this.val$completer;
                if (val$completer != null) {
                    val$completer.setException(new CameraControlInternal.CameraControlException(cameraCaptureFailure));
                }
            }
        });
        this.mCameraControl.submitCaptureRequestsInternal(Collections.singletonList(builder.build()));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.core.impl.OptionsBundle;
import androidx.camera.core.impl.ImageOutputConfig;
import androidx.camera.core.impl.CaptureConfig;
import androidx.camera.core.impl.UseCaseConfig;
import androidx.camera.camera2.internal.compat.workaround.PreviewPixelHDRnet;
import androidx.camera.core.impl.SessionConfig;
import androidx.camera.core.impl.MutableOptionsBundle;
import androidx.camera.core.impl.Config;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.UseCaseConfigFactory;

@RequiresApi(21)
public final class Camera2UseCaseConfigFactory implements UseCaseConfigFactory
{
    final DisplayInfoManager mDisplayInfoManager;
    
    public Camera2UseCaseConfigFactory(@NonNull final Context context) {
        this.mDisplayInfoManager = DisplayInfoManager.getInstance(context);
    }
    
    @NonNull
    @Override
    public Config getConfig(@NonNull final CaptureType captureType, int rotation) {
        final MutableOptionsBundle create = MutableOptionsBundle.create();
        final SessionConfig.Builder hdRnet = new SessionConfig.Builder();
        final int[] $SwitchMap$androidx$camera$core$impl$UseCaseConfigFactory$CaptureType = Camera2UseCaseConfigFactory$1.$SwitchMap$androidx$camera$core$impl$UseCaseConfigFactory$CaptureType;
        final int n = $SwitchMap$androidx$camera$core$impl$UseCaseConfigFactory$CaptureType[captureType.ordinal()];
        final int n2 = 5;
        if (n != 1) {
            if (n != 2 && n != 3) {
                if (n == 4) {
                    hdRnet.setTemplateType(3);
                }
            }
            else {
                hdRnet.setTemplateType(1);
            }
        }
        else {
            int templateType;
            if (rotation == 2) {
                templateType = 5;
            }
            else {
                templateType = 1;
            }
            hdRnet.setTemplateType(templateType);
        }
        final CaptureType preview = CaptureType.PREVIEW;
        if (captureType == preview) {
            PreviewPixelHDRnet.setHDRnet(hdRnet);
        }
        create.insertOption(UseCaseConfig.OPTION_DEFAULT_SESSION_CONFIG, hdRnet.build());
        create.insertOption(UseCaseConfig.OPTION_SESSION_CONFIG_UNPACKER, Camera2SessionOptionUnpacker.INSTANCE);
        final CaptureConfig.Builder builder = new CaptureConfig.Builder();
        final int n3 = $SwitchMap$androidx$camera$core$impl$UseCaseConfigFactory$CaptureType[captureType.ordinal()];
        if (n3 != 1) {
            if (n3 != 2 && n3 != 3) {
                if (n3 == 4) {
                    builder.setTemplateType(3);
                }
            }
            else {
                builder.setTemplateType(1);
            }
        }
        else {
            if (rotation == 2) {
                rotation = n2;
            }
            else {
                rotation = 2;
            }
            builder.setTemplateType(rotation);
        }
        create.insertOption(UseCaseConfig.OPTION_DEFAULT_CAPTURE_CONFIG, builder.build());
        final Config.Option<CaptureConfig.OptionUnpacker> option_CAPTURE_CONFIG_UNPACKER = UseCaseConfig.OPTION_CAPTURE_CONFIG_UNPACKER;
        Camera2CaptureOptionUnpacker camera2CaptureOptionUnpacker;
        if (captureType == CaptureType.IMAGE_CAPTURE) {
            camera2CaptureOptionUnpacker = ImageCaptureOptionUnpacker.INSTANCE;
        }
        else {
            camera2CaptureOptionUnpacker = Camera2CaptureOptionUnpacker.INSTANCE;
        }
        create.insertOption((Config.Option<ImageCaptureOptionUnpacker>)option_CAPTURE_CONFIG_UNPACKER, (ImageCaptureOptionUnpacker)camera2CaptureOptionUnpacker);
        if (captureType == preview) {
            create.insertOption(ImageOutputConfig.OPTION_MAX_RESOLUTION, this.mDisplayInfoManager.getPreviewSize());
        }
        rotation = this.mDisplayInfoManager.getMaxSizeDisplay().getRotation();
        create.insertOption(ImageOutputConfig.OPTION_TARGET_ROTATION, rotation);
        if (captureType == CaptureType.VIDEO_CAPTURE) {
            create.insertOption(UseCaseConfig.OPTION_ZSL_DISABLED, Boolean.TRUE);
        }
        return OptionsBundle.from(create);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.annotation.DoNotInline;
import java.util.concurrent.Future;
import androidx.camera.core.impl.utils.futures.AsyncFunction;
import androidx.camera.core.impl.utils.futures.FutureChain;
import java.util.Collection;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import java.util.concurrent.CancellationException;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.impl.DeferrableSurfaces;
import android.os.Build$VERSION;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraCaptureSession$StateCallback;
import androidx.camera.camera2.internal.compat.params.OutputConfigurationCompat;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CameraAccessException;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.core.Logger;
import androidx.core.util.Preconditions;
import androidx.camera.camera2.internal.compat.params.SessionConfigurationCompat;
import androidx.camera.camera2.internal.compat.CameraDeviceCompat;
import java.util.Objects;
import android.view.Surface;
import java.util.concurrent.ScheduledExecutorService;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.camera.core.impl.DeferrableSurface;
import java.util.List;
import java.util.concurrent.Executor;
import android.os.Handler;
import androidx.annotation.GuardedBy;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.camera.camera2.internal.compat.CameraCaptureSessionCompat;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
class SynchronizedCaptureSessionBaseImpl extends StateCallback implements SynchronizedCaptureSession, OpenerImpl
{
    private static final String TAG = "SyncCaptureSessionBase";
    @Nullable
    CameraCaptureSessionCompat mCameraCaptureSessionCompat;
    @NonNull
    final CaptureSessionRepository mCaptureSessionRepository;
    @Nullable
    StateCallback mCaptureSessionStateCallback;
    @GuardedBy("mLock")
    private boolean mClosed;
    @NonNull
    final Handler mCompatHandler;
    @NonNull
    final Executor mExecutor;
    @GuardedBy("mLock")
    @Nullable
    private List<DeferrableSurface> mHeldDeferrableSurfaces;
    final Object mLock;
    @GuardedBy("mLock")
    @Nullable
    CallbackToFutureAdapter.Completer<Void> mOpenCaptureSessionCompleter;
    @GuardedBy("mLock")
    @Nullable
    ListenableFuture<Void> mOpenCaptureSessionFuture;
    @GuardedBy("mLock")
    private boolean mOpenerDisabled;
    @NonNull
    private final ScheduledExecutorService mScheduledExecutorService;
    @GuardedBy("mLock")
    private boolean mSessionFinished;
    @GuardedBy("mLock")
    @Nullable
    private ListenableFuture<List<Surface>> mStartingSurface;
    
    SynchronizedCaptureSessionBaseImpl(@NonNull final CaptureSessionRepository mCaptureSessionRepository, @NonNull final Executor mExecutor, @NonNull final ScheduledExecutorService mScheduledExecutorService, @NonNull final Handler mCompatHandler) {
        this.mLock = new Object();
        this.mHeldDeferrableSurfaces = null;
        this.mClosed = false;
        this.mOpenerDisabled = false;
        this.mSessionFinished = false;
        this.mCaptureSessionRepository = mCaptureSessionRepository;
        this.mCompatHandler = mCompatHandler;
        this.mExecutor = mExecutor;
        this.mScheduledExecutorService = mScheduledExecutorService;
    }
    
    @Override
    public void abortCaptures() throws CameraAccessException {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat, "Need to call openCaptureSession before using this API.");
        this.mCameraCaptureSessionCompat.toCameraCaptureSession().abortCaptures();
    }
    
    @Override
    public int captureBurstRequests(@NonNull final List<CaptureRequest> list, @NonNull final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat, "Need to call openCaptureSession before using this API.");
        return this.mCameraCaptureSessionCompat.captureBurstRequests(list, this.getExecutor(), cameraCaptureSession$CaptureCallback);
    }
    
    @Override
    public int captureBurstRequests(@NonNull final List<CaptureRequest> list, @NonNull final Executor executor, @NonNull final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat, "Need to call openCaptureSession before using this API.");
        return this.mCameraCaptureSessionCompat.captureBurstRequests(list, executor, cameraCaptureSession$CaptureCallback);
    }
    
    @Override
    public int captureSingleRequest(@NonNull final CaptureRequest captureRequest, @NonNull final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat, "Need to call openCaptureSession before using this API.");
        return this.mCameraCaptureSessionCompat.captureSingleRequest(captureRequest, this.getExecutor(), cameraCaptureSession$CaptureCallback);
    }
    
    @Override
    public int captureSingleRequest(@NonNull final CaptureRequest captureRequest, @NonNull final Executor executor, @NonNull final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat, "Need to call openCaptureSession before using this API.");
        return this.mCameraCaptureSessionCompat.captureSingleRequest(captureRequest, executor, cameraCaptureSession$CaptureCallback);
    }
    
    @Override
    public void close() {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat, "Need to call openCaptureSession before using this API.");
        this.mCaptureSessionRepository.onCaptureSessionClosing(this);
        this.mCameraCaptureSessionCompat.toCameraCaptureSession().close();
        this.getExecutor().execute(new O0OO8\u30070(this));
    }
    
    void createCaptureSessionCompat(@NonNull final CameraCaptureSession cameraCaptureSession) {
        if (this.mCameraCaptureSessionCompat == null) {
            this.mCameraCaptureSessionCompat = CameraCaptureSessionCompat.toCameraCaptureSessionCompat(cameraCaptureSession, this.mCompatHandler);
        }
    }
    
    @NonNull
    @Override
    public SessionConfigurationCompat createSessionConfigurationCompat(final int n, @NonNull final List<OutputConfigurationCompat> list, @NonNull final StateCallback mCaptureSessionStateCallback) {
        this.mCaptureSessionStateCallback = mCaptureSessionStateCallback;
        return new SessionConfigurationCompat(n, list, this.getExecutor(), new CameraCaptureSession$StateCallback(this) {
            final SynchronizedCaptureSessionBaseImpl this$0;
            
            public void onActive(@NonNull final CameraCaptureSession cameraCaptureSession) {
                this.this$0.createCaptureSessionCompat(cameraCaptureSession);
                final SynchronizedCaptureSessionBaseImpl this$0 = this.this$0;
                this$0.onActive(this$0);
            }
            
            @RequiresApi(api = 26)
            public void onCaptureQueueEmpty(@NonNull final CameraCaptureSession cameraCaptureSession) {
                this.this$0.createCaptureSessionCompat(cameraCaptureSession);
                final SynchronizedCaptureSessionBaseImpl this$0 = this.this$0;
                this$0.onCaptureQueueEmpty(this$0);
            }
            
            public void onClosed(@NonNull final CameraCaptureSession cameraCaptureSession) {
                this.this$0.createCaptureSessionCompat(cameraCaptureSession);
                final SynchronizedCaptureSessionBaseImpl this$0 = this.this$0;
                this$0.onClosed(this$0);
            }
            
            public void onConfigureFailed(@NonNull final CameraCaptureSession cameraCaptureSession) {
                try {
                    this.this$0.createCaptureSessionCompat(cameraCaptureSession);
                    final SynchronizedCaptureSessionBaseImpl this$0 = this.this$0;
                    this$0.onConfigureFailed(this$0);
                    synchronized (this.this$0.mLock) {
                        Preconditions.checkNotNull(this.this$0.mOpenCaptureSessionCompleter, "OpenCaptureSession completer should not null");
                        final SynchronizedCaptureSessionBaseImpl this$2 = this.this$0;
                        final CallbackToFutureAdapter.Completer<Void> mOpenCaptureSessionCompleter = this$2.mOpenCaptureSessionCompleter;
                        this$2.mOpenCaptureSessionCompleter = null;
                        monitorexit(this.this$0.mLock);
                        mOpenCaptureSessionCompleter.setException(new IllegalStateException("onConfigureFailed"));
                    }
                }
                finally {
                    synchronized (this.this$0.mLock) {
                        Preconditions.checkNotNull(this.this$0.mOpenCaptureSessionCompleter, "OpenCaptureSession completer should not null");
                        final SynchronizedCaptureSessionBaseImpl this$3 = this.this$0;
                        final CallbackToFutureAdapter.Completer<Void> mOpenCaptureSessionCompleter2 = this$3.mOpenCaptureSessionCompleter;
                        this$3.mOpenCaptureSessionCompleter = null;
                        monitorexit(this.this$0.mLock);
                        mOpenCaptureSessionCompleter2.setException(new IllegalStateException("onConfigureFailed"));
                    }
                }
            }
            
            public void onConfigured(@NonNull final CameraCaptureSession cameraCaptureSession) {
                try {
                    this.this$0.createCaptureSessionCompat(cameraCaptureSession);
                    final SynchronizedCaptureSessionBaseImpl this$0 = this.this$0;
                    this$0.onConfigured(this$0);
                    synchronized (this.this$0.mLock) {
                        Preconditions.checkNotNull(this.this$0.mOpenCaptureSessionCompleter, "OpenCaptureSession completer should not null");
                        final SynchronizedCaptureSessionBaseImpl this$2 = this.this$0;
                        final CallbackToFutureAdapter.Completer<Void> mOpenCaptureSessionCompleter = this$2.mOpenCaptureSessionCompleter;
                        this$2.mOpenCaptureSessionCompleter = null;
                        monitorexit(this.this$0.mLock);
                        mOpenCaptureSessionCompleter.set(null);
                    }
                }
                finally {
                    synchronized (this.this$0.mLock) {
                        Preconditions.checkNotNull(this.this$0.mOpenCaptureSessionCompleter, "OpenCaptureSession completer should not null");
                        final SynchronizedCaptureSessionBaseImpl this$3 = this.this$0;
                        final CallbackToFutureAdapter.Completer<Void> mOpenCaptureSessionCompleter2 = this$3.mOpenCaptureSessionCompleter;
                        this$3.mOpenCaptureSessionCompleter = null;
                        monitorexit(this.this$0.mLock);
                        mOpenCaptureSessionCompleter2.set(null);
                    }
                }
            }
            
            public void onReady(@NonNull final CameraCaptureSession cameraCaptureSession) {
                this.this$0.createCaptureSessionCompat(cameraCaptureSession);
                final SynchronizedCaptureSessionBaseImpl this$0 = this.this$0;
                this$0.onReady(this$0);
            }
            
            @RequiresApi(api = 23)
            public void onSurfacePrepared(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final Surface surface) {
                this.this$0.createCaptureSessionCompat(cameraCaptureSession);
                final SynchronizedCaptureSessionBaseImpl this$0 = this.this$0;
                this$0.onSurfacePrepared(this$0, surface);
            }
        });
    }
    
    @Override
    public void finishClose() {
        this.releaseDeferrableSurfaces();
    }
    
    @NonNull
    @Override
    public CameraDevice getDevice() {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat);
        return this.mCameraCaptureSessionCompat.toCameraCaptureSession().getDevice();
    }
    
    @NonNull
    @Override
    public Executor getExecutor() {
        return this.mExecutor;
    }
    
    @Nullable
    @Override
    public Surface getInputSurface() {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat);
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getInputSurface(this.mCameraCaptureSessionCompat.toCameraCaptureSession());
        }
        return null;
    }
    
    @NonNull
    @Override
    public ListenableFuture<Void> getOpeningBlocker() {
        return Futures.immediateFuture((Void)null);
    }
    
    @NonNull
    @Override
    public StateCallback getStateCallback() {
        return this;
    }
    
    void holdDeferrableSurfaces(@NonNull final List<DeferrableSurface> mHeldDeferrableSurfaces) throws DeferrableSurface.SurfaceClosedException {
        synchronized (this.mLock) {
            this.releaseDeferrableSurfaces();
            DeferrableSurfaces.incrementAll(mHeldDeferrableSurfaces);
            this.mHeldDeferrableSurfaces = mHeldDeferrableSurfaces;
        }
    }
    
    boolean isCameraCaptureSessionOpen() {
        synchronized (this.mLock) {
            return this.mOpenCaptureSessionFuture != null;
        }
    }
    
    public void onActive(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
        Objects.requireNonNull(this.mCaptureSessionStateCallback);
        this.mCaptureSessionStateCallback.onActive(synchronizedCaptureSession);
    }
    
    @RequiresApi(api = 26)
    public void onCaptureQueueEmpty(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
        Objects.requireNonNull(this.mCaptureSessionStateCallback);
        this.mCaptureSessionStateCallback.onCaptureQueueEmpty(synchronizedCaptureSession);
    }
    
    @Override
    public void onClosed(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
        synchronized (this.mLock) {
            ListenableFuture mOpenCaptureSessionFuture;
            if (!this.mClosed) {
                this.mClosed = true;
                Preconditions.checkNotNull(this.mOpenCaptureSessionFuture, "Need to call openCaptureSession before using this API.");
                mOpenCaptureSessionFuture = this.mOpenCaptureSessionFuture;
            }
            else {
                mOpenCaptureSessionFuture = null;
            }
            monitorexit(this.mLock);
            this.finishClose();
            if (mOpenCaptureSessionFuture != null) {
                mOpenCaptureSessionFuture.addListener((Runnable)new O\u300708(this, synchronizedCaptureSession), CameraXExecutors.directExecutor());
            }
        }
    }
    
    @Override
    public void onConfigureFailed(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
        Objects.requireNonNull(this.mCaptureSessionStateCallback);
        this.finishClose();
        this.mCaptureSessionRepository.onCaptureSessionConfigureFail(this);
        this.mCaptureSessionStateCallback.onConfigureFailed(synchronizedCaptureSession);
    }
    
    public void onConfigured(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
        Objects.requireNonNull(this.mCaptureSessionStateCallback);
        this.mCaptureSessionRepository.onCaptureSessionCreated(this);
        this.mCaptureSessionStateCallback.onConfigured(synchronizedCaptureSession);
    }
    
    public void onReady(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
        Objects.requireNonNull(this.mCaptureSessionStateCallback);
        this.mCaptureSessionStateCallback.onReady(synchronizedCaptureSession);
    }
    
    @Override
    void onSessionFinished(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession) {
        synchronized (this.mLock) {
            ListenableFuture mOpenCaptureSessionFuture;
            if (!this.mSessionFinished) {
                this.mSessionFinished = true;
                Preconditions.checkNotNull(this.mOpenCaptureSessionFuture, "Need to call openCaptureSession before using this API.");
                mOpenCaptureSessionFuture = this.mOpenCaptureSessionFuture;
            }
            else {
                mOpenCaptureSessionFuture = null;
            }
            monitorexit(this.mLock);
            if (mOpenCaptureSessionFuture != null) {
                mOpenCaptureSessionFuture.addListener((Runnable)new OOo0O(this, synchronizedCaptureSession), CameraXExecutors.directExecutor());
            }
        }
    }
    
    @RequiresApi(api = 23)
    public void onSurfacePrepared(@NonNull final SynchronizedCaptureSession synchronizedCaptureSession, @NonNull final Surface surface) {
        Objects.requireNonNull(this.mCaptureSessionStateCallback);
        this.mCaptureSessionStateCallback.onSurfacePrepared(synchronizedCaptureSession, surface);
    }
    
    @NonNull
    @Override
    public ListenableFuture<Void> openCaptureSession(@NonNull final CameraDevice cameraDevice, @NonNull final SessionConfigurationCompat sessionConfigurationCompat, @NonNull final List<DeferrableSurface> list) {
        synchronized (this.mLock) {
            if (this.mOpenerDisabled) {
                return Futures.immediateFailedFuture(new CancellationException("Opener is disabled"));
            }
            this.mCaptureSessionRepository.onCreateCaptureSession(this);
            Futures.addCallback(this.mOpenCaptureSessionFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new O\u3007OO(this, list, CameraDeviceCompat.toCameraDeviceCompat(cameraDevice, this.mCompatHandler), sessionConfigurationCompat)), new FutureCallback<Void>(this) {
                final SynchronizedCaptureSessionBaseImpl this$0;
                
                @Override
                public void onFailure(@NonNull final Throwable t) {
                    this.this$0.finishClose();
                    final SynchronizedCaptureSessionBaseImpl this$0 = this.this$0;
                    this$0.mCaptureSessionRepository.onCaptureSessionConfigureFail(this$0);
                }
                
                @Override
                public void onSuccess(@Nullable final Void void1) {
                }
            }, CameraXExecutors.directExecutor());
            return Futures.nonCancellationPropagating(this.mOpenCaptureSessionFuture);
        }
    }
    
    void releaseDeferrableSurfaces() {
        synchronized (this.mLock) {
            final List<DeferrableSurface> mHeldDeferrableSurfaces = this.mHeldDeferrableSurfaces;
            if (mHeldDeferrableSurfaces != null) {
                DeferrableSurfaces.decrementAll(mHeldDeferrableSurfaces);
                this.mHeldDeferrableSurfaces = null;
            }
        }
    }
    
    @Override
    public int setRepeatingBurstRequests(@NonNull final List<CaptureRequest> list, @NonNull final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat, "Need to call openCaptureSession before using this API.");
        return this.mCameraCaptureSessionCompat.setRepeatingBurstRequests(list, this.getExecutor(), cameraCaptureSession$CaptureCallback);
    }
    
    @Override
    public int setRepeatingBurstRequests(@NonNull final List<CaptureRequest> list, @NonNull final Executor executor, @NonNull final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat, "Need to call openCaptureSession before using this API.");
        return this.mCameraCaptureSessionCompat.setRepeatingBurstRequests(list, executor, cameraCaptureSession$CaptureCallback);
    }
    
    @Override
    public int setSingleRepeatingRequest(@NonNull final CaptureRequest captureRequest, @NonNull final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat, "Need to call openCaptureSession before using this API.");
        return this.mCameraCaptureSessionCompat.setSingleRepeatingRequest(captureRequest, this.getExecutor(), cameraCaptureSession$CaptureCallback);
    }
    
    @Override
    public int setSingleRepeatingRequest(@NonNull final CaptureRequest captureRequest, @NonNull final Executor executor, @NonNull final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) throws CameraAccessException {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat, "Need to call openCaptureSession before using this API.");
        return this.mCameraCaptureSessionCompat.setSingleRepeatingRequest(captureRequest, executor, cameraCaptureSession$CaptureCallback);
    }
    
    @NonNull
    @Override
    public ListenableFuture<List<Surface>> startWithDeferrableSurface(@NonNull final List<DeferrableSurface> list, final long n) {
        synchronized (this.mLock) {
            if (this.mOpenerDisabled) {
                return Futures.immediateFailedFuture(new CancellationException("Opener is disabled"));
            }
            final FutureChain<Object> transformAsync = FutureChain.from(DeferrableSurfaces.surfaceListWithTimeout(list, false, n, this.getExecutor(), this.mScheduledExecutorService)).transformAsync((AsyncFunction<? super List<Surface>, Object>)new O\u3007Oooo\u3007\u3007(this, list), this.getExecutor());
            this.mStartingSurface = (ListenableFuture<List<Surface>>)transformAsync;
            return Futures.nonCancellationPropagating((com.google.common.util.concurrent.ListenableFuture<List<Surface>>)transformAsync);
        }
    }
    
    @Override
    public boolean stop() {
        final Future future = null;
        final Future future2 = null;
        final Future future3 = null;
        final Future future4 = null;
        Future future5 = future3;
        try {
            final Object mLock = this.mLock;
            future5 = future3;
            monitorenter(mLock);
            Object o = future;
            future5 = future2;
            try {
                if (!this.mOpenerDisabled) {
                    future5 = future2;
                    final ListenableFuture<List<Surface>> mStartingSurface = this.mStartingSurface;
                    o = future4;
                    if (mStartingSurface != null) {
                        o = mStartingSurface;
                    }
                    future5 = (Future)o;
                    this.mOpenerDisabled = true;
                }
                future5 = (Future)o;
                final boolean b = !this.isCameraCaptureSessionOpen();
                future5 = (Future)o;
                return b;
            }
            finally {
                monitorexit(mLock);
            }
        }
        finally {
            if (future5 != null) {
                future5.cancel(true);
            }
        }
    }
    
    @Override
    public void stopRepeating() throws CameraAccessException {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat, "Need to call openCaptureSession before using this API.");
        this.mCameraCaptureSessionCompat.toCameraCaptureSession().stopRepeating();
    }
    
    @NonNull
    @Override
    public CameraCaptureSessionCompat toCameraCaptureSessionCompat() {
        Preconditions.checkNotNull(this.mCameraCaptureSessionCompat);
        return this.mCameraCaptureSessionCompat;
    }
    
    @RequiresApi(23)
    private static class Api23Impl
    {
        @DoNotInline
        static Surface getInputSurface(final CameraCaptureSession cameraCaptureSession) {
            return ooO\u300700O.\u3007080(cameraCaptureSession);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import android.util.Rational;
import androidx.annotation.NonNull;
import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import android.util.Range;
import androidx.annotation.GuardedBy;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import androidx.annotation.RequiresApi;
import androidx.camera.core.ExposureState;

@RequiresApi(21)
class ExposureStateImpl implements ExposureState
{
    private final CameraCharacteristicsCompat mCameraCharacteristics;
    @GuardedBy("mLock")
    private int mExposureCompensation;
    private final Object mLock;
    
    ExposureStateImpl(final CameraCharacteristicsCompat mCameraCharacteristics, final int mExposureCompensation) {
        this.mLock = new Object();
        this.mCameraCharacteristics = mCameraCharacteristics;
        this.mExposureCompensation = mExposureCompensation;
    }
    
    @Override
    public int getExposureCompensationIndex() {
        synchronized (this.mLock) {
            return this.mExposureCompensation;
        }
    }
    
    @NonNull
    @Override
    public Range<Integer> getExposureCompensationRange() {
        return (Range<Integer>)this.mCameraCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<Range>)CameraCharacteristics.CONTROL_AE_COMPENSATION_RANGE);
    }
    
    @NonNull
    @Override
    public Rational getExposureCompensationStep() {
        if (!this.isExposureCompensationSupported()) {
            return Rational.ZERO;
        }
        return this.mCameraCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<Rational>)CameraCharacteristics.CONTROL_AE_COMPENSATION_STEP);
    }
    
    @Override
    public boolean isExposureCompensationSupported() {
        final Range range = this.mCameraCharacteristics.get((android.hardware.camera2.CameraCharacteristics$Key<Range>)CameraCharacteristics.CONTROL_AE_COMPENSATION_RANGE);
        return range != null && (int)range.getLower() != 0 && (int)range.getUpper() != 0;
    }
    
    void setExposureCompensationIndex(final int mExposureCompensation) {
        synchronized (this.mLock) {
            this.mExposureCompensation = mExposureCompensation;
        }
    }
}

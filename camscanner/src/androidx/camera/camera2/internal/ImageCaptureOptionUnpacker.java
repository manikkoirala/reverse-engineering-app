// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.core.impl.Config;
import androidx.camera.camera2.impl.Camera2ImplConfig;
import androidx.camera.core.impl.ImageCaptureConfig;
import androidx.camera.core.impl.CaptureConfig;
import androidx.camera.core.impl.UseCaseConfig;
import androidx.annotation.NonNull;
import androidx.camera.camera2.internal.compat.workaround.ImageCapturePixelHDRPlus;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
final class ImageCaptureOptionUnpacker extends Camera2CaptureOptionUnpacker
{
    static final ImageCaptureOptionUnpacker INSTANCE;
    @NonNull
    private final ImageCapturePixelHDRPlus mImageCapturePixelHDRPlus;
    
    static {
        INSTANCE = new ImageCaptureOptionUnpacker(new ImageCapturePixelHDRPlus());
    }
    
    private ImageCaptureOptionUnpacker(@NonNull final ImageCapturePixelHDRPlus mImageCapturePixelHDRPlus) {
        this.mImageCapturePixelHDRPlus = mImageCapturePixelHDRPlus;
    }
    
    @Override
    public void unpack(@NonNull final UseCaseConfig<?> useCaseConfig, @NonNull final Builder builder) {
        super.unpack(useCaseConfig, builder);
        if (useCaseConfig instanceof ImageCaptureConfig) {
            final ImageCaptureConfig imageCaptureConfig = (ImageCaptureConfig)useCaseConfig;
            final Camera2ImplConfig.Builder builder2 = new Camera2ImplConfig.Builder();
            if (imageCaptureConfig.hasCaptureMode()) {
                this.mImageCapturePixelHDRPlus.toggleHDRPlus(imageCaptureConfig.getCaptureMode(), builder2);
            }
            builder.addImplementationOptions(builder2.build());
            return;
        }
        throw new IllegalArgumentException("config is not ImageCaptureConfig");
    }
}

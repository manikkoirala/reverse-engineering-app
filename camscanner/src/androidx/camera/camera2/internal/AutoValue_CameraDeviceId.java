// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.annotation.NonNull;

final class AutoValue_CameraDeviceId extends CameraDeviceId
{
    private final String brand;
    private final String cameraId;
    private final String device;
    private final String model;
    
    AutoValue_CameraDeviceId(final String brand, final String device, final String model, final String cameraId) {
        if (brand == null) {
            throw new NullPointerException("Null brand");
        }
        this.brand = brand;
        if (device == null) {
            throw new NullPointerException("Null device");
        }
        this.device = device;
        if (model == null) {
            throw new NullPointerException("Null model");
        }
        this.model = model;
        if (cameraId != null) {
            this.cameraId = cameraId;
            return;
        }
        throw new NullPointerException("Null cameraId");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof CameraDeviceId) {
            final CameraDeviceId cameraDeviceId = (CameraDeviceId)o;
            if (!this.brand.equals(cameraDeviceId.getBrand()) || !this.device.equals(cameraDeviceId.getDevice()) || !this.model.equals(cameraDeviceId.getModel()) || !this.cameraId.equals(cameraDeviceId.getCameraId())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @NonNull
    @Override
    public String getBrand() {
        return this.brand;
    }
    
    @NonNull
    @Override
    public String getCameraId() {
        return this.cameraId;
    }
    
    @NonNull
    @Override
    public String getDevice() {
        return this.device;
    }
    
    @NonNull
    @Override
    public String getModel() {
        return this.model;
    }
    
    @Override
    public int hashCode() {
        return (((this.brand.hashCode() ^ 0xF4243) * 1000003 ^ this.device.hashCode()) * 1000003 ^ this.model.hashCode()) * 1000003 ^ this.cameraId.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("CameraDeviceId{brand=");
        sb.append(this.brand);
        sb.append(", device=");
        sb.append(this.device);
        sb.append(", model=");
        sb.append(this.model);
        sb.append(", cameraId=");
        sb.append(this.cameraId);
        sb.append("}");
        return sb.toString();
    }
}

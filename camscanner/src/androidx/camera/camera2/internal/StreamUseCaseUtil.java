// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import java.util.Iterator;
import androidx.camera.core.impl.VideoCaptureConfig;
import androidx.camera.core.impl.ImageCaptureConfig;
import androidx.camera.core.impl.PreviewConfig;
import androidx.camera.core.impl.ImageAnalysisConfig;
import android.os.Build$VERSION;
import androidx.camera.core.impl.SessionConfig;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.UseCaseConfig;
import java.util.Collection;

public final class StreamUseCaseUtil
{
    private StreamUseCaseUtil() {
    }
    
    public static long getStreamUseCaseFromUseCaseConfigs(@NonNull final Collection<UseCaseConfig<?>> collection, @NonNull final Collection<SessionConfig> collection2) {
        if (Build$VERSION.SDK_INT < 33) {
            return -1L;
        }
        if (collection.isEmpty()) {
            return 0L;
        }
        final Iterator<SessionConfig> iterator = collection2.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getTemplateType() == 5) {
                return 0L;
            }
        }
        final Iterator iterator2 = collection.iterator();
        boolean b = false;
        int n = 0;
        boolean b2 = false;
        while (iterator2.hasNext()) {
            final UseCaseConfig useCaseConfig = (UseCaseConfig)iterator2.next();
            if (useCaseConfig instanceof ImageAnalysisConfig) {
                return 0L;
            }
            if (useCaseConfig instanceof PreviewConfig) {
                b2 = true;
            }
            else if (useCaseConfig instanceof ImageCaptureConfig) {
                if (n != 0) {
                    return 4L;
                }
                b = true;
            }
            else {
                if (!(useCaseConfig instanceof VideoCaptureConfig)) {
                    continue;
                }
                if (b) {
                    return 4L;
                }
                n = 1;
            }
        }
        if (b) {
            return 2L;
        }
        if (n != 0) {
            return 3L;
        }
        if (!b2) {
            return 0L;
        }
        return 1L;
    }
}

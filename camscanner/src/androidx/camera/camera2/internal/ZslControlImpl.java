// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import android.media.Image;
import android.os.Build$VERSION;
import java.util.NoSuchElementException;
import com.google.common.util.concurrent.ListenableFuture;
import android.hardware.camera2.params.InputConfiguration;
import android.view.Surface;
import androidx.camera.core.internal.compat.ImageWriterCompat;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCaptureSession$StateCallback;
import java.util.Objects;
import androidx.camera.core.impl.ImmediateSurface;
import androidx.camera.core.MetadataImageReader;
import androidx.camera.core.impl.SessionConfig;
import androidx.camera.core.Logger;
import androidx.camera.core.impl.ImageReaderProxy;
import java.util.Comparator;
import java.util.Arrays;
import androidx.camera.core.impl.utils.CompareSizesByArea;
import java.util.HashMap;
import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.params.StreamConfigurationMap;
import java.util.concurrent.Executor;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.internal.utils.RingBuffer;
import android.util.Size;
import java.util.Map;
import androidx.annotation.Nullable;
import android.media.ImageWriter;
import androidx.camera.core.SafeCloseImageReaderProxy;
import androidx.camera.core.impl.DeferrableSurface;
import androidx.camera.core.impl.CameraCaptureCallback;
import androidx.camera.core.internal.utils.ZslRingBuffer;
import androidx.annotation.NonNull;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.RequiresApi;

@RequiresApi(23)
final class ZslControlImpl implements ZslControl
{
    @VisibleForTesting
    static final int MAX_IMAGES = 9;
    @VisibleForTesting
    static final int RING_BUFFER_CAPACITY = 3;
    private static final String TAG = "ZslControlImpl";
    @NonNull
    private final CameraCharacteristicsCompat mCameraCharacteristicsCompat;
    @NonNull
    @VisibleForTesting
    final ZslRingBuffer mImageRingBuffer;
    private boolean mIsPrivateReprocessingSupported;
    private boolean mIsZslDisabledByFlashMode;
    private boolean mIsZslDisabledByUseCaseConfig;
    private CameraCaptureCallback mMetadataMatchingCaptureCallback;
    private DeferrableSurface mReprocessingImageDeferrableSurface;
    SafeCloseImageReaderProxy mReprocessingImageReader;
    @Nullable
    ImageWriter mReprocessingImageWriter;
    @NonNull
    private final Map<Integer, Size> mReprocessingInputSizeMap;
    
    ZslControlImpl(@NonNull final CameraCharacteristicsCompat mCameraCharacteristicsCompat) {
        this.mIsZslDisabledByUseCaseConfig = false;
        this.mIsZslDisabledByFlashMode = false;
        this.mIsPrivateReprocessingSupported = false;
        this.mCameraCharacteristicsCompat = mCameraCharacteristicsCompat;
        this.mIsPrivateReprocessingSupported = ZslUtil.isCapabilitySupported(mCameraCharacteristicsCompat, 4);
        this.mReprocessingInputSizeMap = this.createReprocessingInputSizeMap(mCameraCharacteristicsCompat);
        this.mImageRingBuffer = new ZslRingBuffer(3, new ooo\u3007\u3007O\u3007());
    }
    
    private void cleanup() {
        final ZslRingBuffer mImageRingBuffer = this.mImageRingBuffer;
        while (!mImageRingBuffer.isEmpty()) {
            mImageRingBuffer.dequeue().close();
        }
        final DeferrableSurface mReprocessingImageDeferrableSurface = this.mReprocessingImageDeferrableSurface;
        if (mReprocessingImageDeferrableSurface != null) {
            final SafeCloseImageReaderProxy mReprocessingImageReader = this.mReprocessingImageReader;
            if (mReprocessingImageReader != null) {
                mReprocessingImageDeferrableSurface.getTerminationFuture().addListener((Runnable)new \u3007o\u30078(mReprocessingImageReader), (Executor)CameraXExecutors.mainThreadExecutor());
                this.mReprocessingImageReader = null;
            }
            mReprocessingImageDeferrableSurface.close();
            this.mReprocessingImageDeferrableSurface = null;
        }
        final ImageWriter mReprocessingImageWriter = this.mReprocessingImageWriter;
        if (mReprocessingImageWriter != null) {
            \u30070OO8.\u3007080(mReprocessingImageWriter);
            this.mReprocessingImageWriter = null;
        }
    }
    
    @NonNull
    private Map<Integer, Size> createReprocessingInputSizeMap(@NonNull final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        final StreamConfigurationMap streamConfigurationMap = cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<StreamConfigurationMap>)CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
        if (streamConfigurationMap != null && o\u30078\u3007.\u3007080(streamConfigurationMap) != null) {
            final HashMap hashMap = new HashMap();
            for (final int j : o\u30078\u3007.\u3007080(streamConfigurationMap)) {
                final Size[] \u300781 = \u3007\u300700OO.\u3007080(streamConfigurationMap, j);
                if (\u300781 != null) {
                    Arrays.sort(\u300781, new CompareSizesByArea(true));
                    hashMap.put(j, \u300781[0]);
                }
            }
            return hashMap;
        }
        return new HashMap<Integer, Size>();
    }
    
    private boolean isJpegValidOutputForInputFormat(@NonNull final CameraCharacteristicsCompat cameraCharacteristicsCompat, int i) {
        final StreamConfigurationMap streamConfigurationMap = cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<StreamConfigurationMap>)CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
        if (streamConfigurationMap == null) {
            return false;
        }
        final int[] \u3007080 = O0oO008.\u3007080(streamConfigurationMap, i);
        if (\u3007080 == null) {
            return false;
        }
        int length;
        for (length = \u3007080.length, i = 0; i < length; ++i) {
            if (\u3007080[i] == 256) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public void addZslConfig(@NonNull final SessionConfig.Builder builder) {
        this.cleanup();
        if (this.mIsZslDisabledByUseCaseConfig) {
            return;
        }
        if (this.mIsPrivateReprocessingSupported && !this.mReprocessingInputSizeMap.isEmpty() && this.mReprocessingInputSizeMap.containsKey(34)) {
            if (this.isJpegValidOutputForInputFormat(this.mCameraCharacteristicsCompat, 34)) {
                final Size size = this.mReprocessingInputSizeMap.get(34);
                final MetadataImageReader metadataImageReader = new MetadataImageReader(size.getWidth(), size.getHeight(), 34, 9);
                this.mMetadataMatchingCaptureCallback = metadataImageReader.getCameraCaptureCallback();
                this.mReprocessingImageReader = new SafeCloseImageReaderProxy(metadataImageReader);
                metadataImageReader.setOnImageAvailableListener(new O0oo0o0\u3007(this), CameraXExecutors.ioExecutor());
                final ImmediateSurface mReprocessingImageDeferrableSurface = new ImmediateSurface(this.mReprocessingImageReader.getSurface(), new Size(this.mReprocessingImageReader.getWidth(), this.mReprocessingImageReader.getHeight()), 34);
                this.mReprocessingImageDeferrableSurface = mReprocessingImageDeferrableSurface;
                final SafeCloseImageReaderProxy mReprocessingImageReader = this.mReprocessingImageReader;
                final ListenableFuture<Void> terminationFuture = mReprocessingImageDeferrableSurface.getTerminationFuture();
                Objects.requireNonNull(mReprocessingImageReader);
                terminationFuture.addListener((Runnable)new \u3007o\u30078(mReprocessingImageReader), (Executor)CameraXExecutors.mainThreadExecutor());
                builder.addSurface(this.mReprocessingImageDeferrableSurface);
                builder.addCameraCaptureCallback(this.mMetadataMatchingCaptureCallback);
                builder.addSessionStateCallback(new CameraCaptureSession$StateCallback(this) {
                    final ZslControlImpl this$0;
                    
                    public void onConfigureFailed(@NonNull final CameraCaptureSession cameraCaptureSession) {
                    }
                    
                    public void onConfigured(@NonNull final CameraCaptureSession cameraCaptureSession) {
                        final Surface \u3007080 = ooO\u300700O.\u3007080(cameraCaptureSession);
                        if (\u3007080 != null) {
                            this.this$0.mReprocessingImageWriter = ImageWriterCompat.newInstance(\u3007080, 1);
                        }
                    }
                });
                builder.setInputConfiguration(new InputConfiguration(this.mReprocessingImageReader.getWidth(), this.mReprocessingImageReader.getHeight(), this.mReprocessingImageReader.getImageFormat()));
            }
        }
    }
    
    @Nullable
    @Override
    public ImageProxy dequeueImageFromBuffer() {
        ImageProxy imageProxy;
        try {
            imageProxy = this.mImageRingBuffer.dequeue();
        }
        catch (final NoSuchElementException ex) {
            Logger.e("ZslControlImpl", "dequeueImageFromBuffer no such element");
            imageProxy = null;
        }
        return imageProxy;
    }
    
    @Override
    public boolean enqueueImageToImageWriter(@NonNull final ImageProxy imageProxy) {
        final Image image = imageProxy.getImage();
        if (Build$VERSION.SDK_INT >= 23) {
            final ImageWriter mReprocessingImageWriter = this.mReprocessingImageWriter;
            if (mReprocessingImageWriter != null && image != null) {
                try {
                    ImageWriterCompat.queueInputImage(mReprocessingImageWriter, image);
                    return true;
                }
                catch (final IllegalStateException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("enqueueImageToImageWriter throws IllegalStateException = ");
                    sb.append(ex.getMessage());
                    Logger.e("ZslControlImpl", sb.toString());
                }
            }
        }
        return false;
    }
    
    @Override
    public boolean isZslDisabledByFlashMode() {
        return this.mIsZslDisabledByFlashMode;
    }
    
    @Override
    public boolean isZslDisabledByUserCaseConfig() {
        return this.mIsZslDisabledByUseCaseConfig;
    }
    
    @Override
    public void setZslDisabledByFlashMode(final boolean mIsZslDisabledByFlashMode) {
        this.mIsZslDisabledByFlashMode = mIsZslDisabledByFlashMode;
    }
    
    @Override
    public void setZslDisabledByUserCaseConfig(final boolean mIsZslDisabledByUseCaseConfig) {
        this.mIsZslDisabledByUseCaseConfig = mIsZslDisabledByUseCaseConfig;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.core.ImageProxy;
import androidx.camera.core.impl.CameraCaptureResults;
import java.util.Objects;
import androidx.camera.core.impl.utils.futures.AsyncFunction;
import androidx.camera.core.impl.CameraCaptureFailure;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.impl.CameraCaptureCallback;
import androidx.camera.camera2.interop.ExperimentalCamera2Interop;
import androidx.annotation.OptIn;
import androidx.camera.core.impl.Config;
import android.hardware.camera2.CaptureRequest$Key;
import android.hardware.camera2.CaptureRequest;
import androidx.camera.camera2.impl.Camera2ImplConfig;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import androidx.annotation.VisibleForTesting;
import androidx.camera.core.impl.CameraCaptureResult;
import androidx.arch.core.util.Function;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.impl.utils.futures.FutureChain;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.camera2.internal.compat.workaround.OverrideAeModeForStillCapture;
import androidx.camera.core.impl.CaptureConfig;
import java.util.List;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.camera.core.Logger;
import android.hardware.camera2.CaptureResult;
import androidx.annotation.Nullable;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.CameraCharacteristics$Key;
import android.hardware.camera2.CameraCharacteristics;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import androidx.camera.camera2.internal.compat.workaround.UseTorchAsFlash;
import java.util.concurrent.Executor;
import androidx.camera.core.impl.Quirks;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.CameraCaptureMetaData;
import java.util.Set;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
class Camera2CapturePipeline
{
    private static final Set<CameraCaptureMetaData.AeState> AE_CONVERGED_STATE_SET;
    private static final Set<CameraCaptureMetaData.AeState> AE_TORCH_AS_FLASH_CONVERGED_STATE_SET;
    private static final Set<CameraCaptureMetaData.AfState> AF_CONVERGED_STATE_SET;
    private static final Set<CameraCaptureMetaData.AwbState> AWB_CONVERGED_STATE_SET;
    private static final String TAG = "Camera2CapturePipeline";
    @NonNull
    private final Camera2CameraControlImpl mCameraControl;
    @NonNull
    private final Quirks mCameraQuirk;
    @NonNull
    private final Executor mExecutor;
    private final boolean mIsLegacyDevice;
    private int mTemplate;
    @NonNull
    private final UseTorchAsFlash mUseTorchAsFlash;
    
    static {
        AF_CONVERGED_STATE_SET = Collections.unmodifiableSet((Set<? extends CameraCaptureMetaData.AfState>)EnumSet.of(CameraCaptureMetaData.AfState.PASSIVE_FOCUSED, CameraCaptureMetaData.AfState.PASSIVE_NOT_FOCUSED, CameraCaptureMetaData.AfState.LOCKED_FOCUSED, CameraCaptureMetaData.AfState.LOCKED_NOT_FOCUSED));
        AWB_CONVERGED_STATE_SET = Collections.unmodifiableSet((Set<? extends CameraCaptureMetaData.AwbState>)EnumSet.of(CameraCaptureMetaData.AwbState.CONVERGED, CameraCaptureMetaData.AwbState.UNKNOWN));
        final CameraCaptureMetaData.AeState converged = CameraCaptureMetaData.AeState.CONVERGED;
        final CameraCaptureMetaData.AeState flash_REQUIRED = CameraCaptureMetaData.AeState.FLASH_REQUIRED;
        final CameraCaptureMetaData.AeState unknown = CameraCaptureMetaData.AeState.UNKNOWN;
        final EnumSet<CameraCaptureMetaData.AeState> copy = EnumSet.copyOf((Collection<CameraCaptureMetaData.AeState>)(AE_CONVERGED_STATE_SET = Collections.unmodifiableSet((Set<? extends CameraCaptureMetaData.AeState>)EnumSet.of(converged, flash_REQUIRED, unknown))));
        copy.remove(flash_REQUIRED);
        copy.remove(unknown);
        AE_TORCH_AS_FLASH_CONVERGED_STATE_SET = Collections.unmodifiableSet((Set<?>)copy);
    }
    
    Camera2CapturePipeline(@NonNull final Camera2CameraControlImpl mCameraControl, @NonNull final CameraCharacteristicsCompat cameraCharacteristicsCompat, @NonNull final Quirks mCameraQuirk, @NonNull final Executor mExecutor) {
        boolean mIsLegacyDevice = true;
        this.mTemplate = 1;
        this.mCameraControl = mCameraControl;
        final Integer n = cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Integer>)CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);
        if (n == null || n != 2) {
            mIsLegacyDevice = false;
        }
        this.mIsLegacyDevice = mIsLegacyDevice;
        this.mExecutor = mExecutor;
        this.mCameraQuirk = mCameraQuirk;
        this.mUseTorchAsFlash = new UseTorchAsFlash(mCameraQuirk);
    }
    
    static boolean is3AConverged(@Nullable final TotalCaptureResult totalCaptureResult, final boolean b) {
        final boolean b2 = false;
        if (totalCaptureResult == null) {
            return false;
        }
        final Camera2CameraCaptureResult camera2CameraCaptureResult = new Camera2CameraCaptureResult((CaptureResult)totalCaptureResult);
        final boolean b3 = camera2CameraCaptureResult.getAfMode() == CameraCaptureMetaData.AfMode.OFF || camera2CameraCaptureResult.getAfMode() == CameraCaptureMetaData.AfMode.UNKNOWN || Camera2CapturePipeline.AF_CONVERGED_STATE_SET.contains(camera2CameraCaptureResult.getAfState());
        final boolean b4 = (int)((CaptureResult)totalCaptureResult).get(CaptureResult.CONTROL_AE_MODE) == 0;
        boolean b5 = false;
        Label_0150: {
            Label_0122: {
                if (b) {
                    if (b4) {
                        break Label_0122;
                    }
                    if (Camera2CapturePipeline.AE_TORCH_AS_FLASH_CONVERGED_STATE_SET.contains(camera2CameraCaptureResult.getAeState())) {
                        break Label_0122;
                    }
                }
                else {
                    if (b4) {
                        break Label_0122;
                    }
                    if (Camera2CapturePipeline.AE_CONVERGED_STATE_SET.contains(camera2CameraCaptureResult.getAeState())) {
                        break Label_0122;
                    }
                }
                b5 = false;
                break Label_0150;
            }
            b5 = true;
        }
        final boolean b6 = (int)((CaptureResult)totalCaptureResult).get(CaptureResult.CONTROL_AWB_MODE) == 0 || Camera2CapturePipeline.AWB_CONVERGED_STATE_SET.contains(camera2CameraCaptureResult.getAwbState());
        final StringBuilder sb = new StringBuilder();
        sb.append("checkCaptureResult, AE=");
        sb.append(camera2CameraCaptureResult.getAeState());
        sb.append(" AF =");
        sb.append(camera2CameraCaptureResult.getAfState());
        sb.append(" AWB=");
        sb.append(camera2CameraCaptureResult.getAwbState());
        Logger.d("Camera2CapturePipeline", sb.toString());
        boolean b7 = b2;
        if (b3) {
            b7 = b2;
            if (b5) {
                b7 = b2;
                if (b6) {
                    b7 = true;
                }
            }
        }
        return b7;
    }
    
    static boolean isFlashRequired(final int detailMessage, @Nullable final TotalCaptureResult totalCaptureResult) {
        final boolean b = false;
        if (detailMessage == 0) {
            Integer n;
            if (totalCaptureResult != null) {
                n = (Integer)((CaptureResult)totalCaptureResult).get(CaptureResult.CONTROL_AE_STATE);
            }
            else {
                n = null;
            }
            boolean b2 = b;
            if (n != null) {
                b2 = b;
                if (n == 4) {
                    b2 = true;
                }
            }
            return b2;
        }
        if (detailMessage == 1) {
            return true;
        }
        if (detailMessage == 2) {
            return false;
        }
        throw new AssertionError(detailMessage);
    }
    
    private boolean isTorchAsFlash(final int n) {
        final boolean shouldUseTorchAsFlash = this.mUseTorchAsFlash.shouldUseTorchAsFlash();
        boolean b2;
        final boolean b = b2 = true;
        if (!shouldUseTorchAsFlash) {
            b2 = b;
            if (this.mTemplate != 3) {
                b2 = (n == 1 && b);
            }
        }
        return b2;
    }
    
    @NonNull
    static ListenableFuture<TotalCaptureResult> waitForResult(final long n, @NonNull final Camera2CameraControlImpl camera2CameraControlImpl, @Nullable final Checker checker) {
        final ResultListener resultListener = new ResultListener(n, checker);
        camera2CameraControlImpl.addCaptureResultListener((Camera2CameraControlImpl.CaptureResultListener)resultListener);
        return resultListener.getFuture();
    }
    
    public void setTemplate(final int mTemplate) {
        this.mTemplate = mTemplate;
    }
    
    @NonNull
    public ListenableFuture<List<Void>> submitStillCaptures(@NonNull final List<CaptureConfig> list, final int n, final int n2, final int n3) {
        final OverrideAeModeForStillCapture overrideAeModeForStillCapture = new OverrideAeModeForStillCapture(this.mCameraQuirk);
        final Pipeline pipeline = new Pipeline(this.mTemplate, this.mExecutor, this.mCameraControl, this.mIsLegacyDevice, overrideAeModeForStillCapture);
        if (n == 0) {
            pipeline.addTask(new AfTask(this.mCameraControl));
        }
        if (this.isTorchAsFlash(n3)) {
            pipeline.addTask(new TorchTask(this.mCameraControl, n2, this.mExecutor));
        }
        else {
            pipeline.addTask(new AePreCaptureTask(this.mCameraControl, n2, overrideAeModeForStillCapture));
        }
        return Futures.nonCancellationPropagating(pipeline.executeCapture(list, n2));
    }
    
    static class AePreCaptureTask implements PipelineTask
    {
        private final Camera2CameraControlImpl mCameraControl;
        private final int mFlashMode;
        private boolean mIsExecuted;
        private final OverrideAeModeForStillCapture mOverrideAeModeForStillCapture;
        
        AePreCaptureTask(@NonNull final Camera2CameraControlImpl mCameraControl, final int mFlashMode, @NonNull final OverrideAeModeForStillCapture mOverrideAeModeForStillCapture) {
            this.mIsExecuted = false;
            this.mCameraControl = mCameraControl;
            this.mFlashMode = mFlashMode;
            this.mOverrideAeModeForStillCapture = mOverrideAeModeForStillCapture;
        }
        
        @Override
        public boolean isCaptureResultNeeded() {
            return this.mFlashMode == 0;
        }
        
        @Override
        public void postCapture() {
            if (this.mIsExecuted) {
                Logger.d("Camera2CapturePipeline", "cancel TriggerAePreCapture");
                this.mCameraControl.getFocusMeteringControl().cancelAfAeTrigger(false, true);
                this.mOverrideAeModeForStillCapture.onAePrecaptureFinished();
            }
        }
        
        @NonNull
        @Override
        public ListenableFuture<Boolean> preCapture(@Nullable final TotalCaptureResult totalCaptureResult) {
            if (Camera2CapturePipeline.isFlashRequired(this.mFlashMode, totalCaptureResult)) {
                Logger.d("Camera2CapturePipeline", "Trigger AE");
                this.mIsExecuted = true;
                return (ListenableFuture<Boolean>)FutureChain.from((com.google.common.util.concurrent.ListenableFuture<Object>)CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<V>)new Oo8Oo00oo(this))).transform((Function<? super Object, Object>)new \u3007\u3007\u30070\u3007\u30070(), CameraXExecutors.directExecutor());
            }
            return Futures.immediateFuture(Boolean.FALSE);
        }
    }
    
    interface PipelineTask
    {
        boolean isCaptureResultNeeded();
        
        void postCapture();
        
        @NonNull
        ListenableFuture<Boolean> preCapture(@Nullable final TotalCaptureResult p0);
    }
    
    static class AfTask implements PipelineTask
    {
        private final Camera2CameraControlImpl mCameraControl;
        private boolean mIsExecuted;
        
        AfTask(@NonNull final Camera2CameraControlImpl mCameraControl) {
            this.mIsExecuted = false;
            this.mCameraControl = mCameraControl;
        }
        
        @Override
        public boolean isCaptureResultNeeded() {
            return true;
        }
        
        @Override
        public void postCapture() {
            if (this.mIsExecuted) {
                Logger.d("Camera2CapturePipeline", "cancel TriggerAF");
                this.mCameraControl.getFocusMeteringControl().cancelAfAeTrigger(true, false);
            }
        }
        
        @NonNull
        @Override
        public ListenableFuture<Boolean> preCapture(@Nullable final TotalCaptureResult totalCaptureResult) {
            final com.google.common.util.concurrent.ListenableFuture<Boolean> immediateFuture = Futures.immediateFuture(Boolean.TRUE);
            if (totalCaptureResult == null) {
                return immediateFuture;
            }
            final Integer n = (Integer)((CaptureResult)totalCaptureResult).get(CaptureResult.CONTROL_AF_MODE);
            if (n == null) {
                return immediateFuture;
            }
            final int intValue = n;
            if (intValue == 1 || intValue == 2) {
                Logger.d("Camera2CapturePipeline", "TriggerAf? AF mode auto");
                final Integer n2 = (Integer)((CaptureResult)totalCaptureResult).get(CaptureResult.CONTROL_AF_STATE);
                if (n2 != null && n2 == 0) {
                    Logger.d("Camera2CapturePipeline", "Trigger AF");
                    this.mIsExecuted = true;
                    this.mCameraControl.getFocusMeteringControl().triggerAf(null, false);
                }
            }
            return immediateFuture;
        }
    }
    
    @VisibleForTesting
    static class Pipeline
    {
        private static final long CHECK_3A_TIMEOUT_IN_NS;
        private static final long CHECK_3A_WITH_FLASH_TIMEOUT_IN_NS;
        private final Camera2CameraControlImpl mCameraControl;
        private final Executor mExecutor;
        private final boolean mIsLegacyDevice;
        private final OverrideAeModeForStillCapture mOverrideAeModeForStillCapture;
        private final PipelineTask mPipelineSubTask;
        final List<PipelineTask> mTasks;
        private final int mTemplate;
        private long mTimeout3A;
        
        static {
            final TimeUnit seconds = TimeUnit.SECONDS;
            CHECK_3A_TIMEOUT_IN_NS = seconds.toNanos(1L);
            CHECK_3A_WITH_FLASH_TIMEOUT_IN_NS = seconds.toNanos(5L);
        }
        
        Pipeline(final int mTemplate, @NonNull final Executor mExecutor, @NonNull final Camera2CameraControlImpl mCameraControl, final boolean mIsLegacyDevice, @NonNull final OverrideAeModeForStillCapture mOverrideAeModeForStillCapture) {
            this.mTimeout3A = Pipeline.CHECK_3A_TIMEOUT_IN_NS;
            this.mTasks = new ArrayList<PipelineTask>();
            this.mPipelineSubTask = new PipelineTask() {
                final Pipeline this$0;
                
                @Override
                public boolean isCaptureResultNeeded() {
                    final Iterator<PipelineTask> iterator = this.this$0.mTasks.iterator();
                    while (iterator.hasNext()) {
                        if (iterator.next().isCaptureResultNeeded()) {
                            return true;
                        }
                    }
                    return false;
                }
                
                @Override
                public void postCapture() {
                    final Iterator<PipelineTask> iterator = this.this$0.mTasks.iterator();
                    while (iterator.hasNext()) {
                        iterator.next().postCapture();
                    }
                }
                
                @NonNull
                @Override
                public ListenableFuture<Boolean> preCapture(@Nullable final TotalCaptureResult totalCaptureResult) {
                    final ArrayList list = new ArrayList();
                    final Iterator<PipelineTask> iterator = this.this$0.mTasks.iterator();
                    while (iterator.hasNext()) {
                        list.add(iterator.next().preCapture(totalCaptureResult));
                    }
                    return (ListenableFuture<Boolean>)Futures.transform(Futures.allAsList((Collection<? extends com.google.common.util.concurrent.ListenableFuture<?>>)list), (Function<? super List<Object>, ?>)new \u30078\u30070\u3007o\u3007O(), CameraXExecutors.directExecutor());
                }
            };
            this.mTemplate = mTemplate;
            this.mExecutor = mExecutor;
            this.mCameraControl = mCameraControl;
            this.mIsLegacyDevice = mIsLegacyDevice;
            this.mOverrideAeModeForStillCapture = mOverrideAeModeForStillCapture;
        }
        
        @OptIn(markerClass = { ExperimentalCamera2Interop.class })
        private void applyAeModeQuirk(@NonNull final CaptureConfig.Builder builder) {
            final Camera2ImplConfig.Builder builder2 = new Camera2ImplConfig.Builder();
            builder2.setCaptureRequestOption((android.hardware.camera2.CaptureRequest$Key<Integer>)CaptureRequest.CONTROL_AE_MODE, 3);
            builder.addImplementationOptions(builder2.build());
        }
        
        private void applyStillCaptureTemplate(@NonNull final CaptureConfig.Builder builder, @NonNull final CaptureConfig captureConfig) {
            int templateType;
            if (this.mTemplate == 3 && !this.mIsLegacyDevice) {
                templateType = 4;
            }
            else if (captureConfig.getTemplateType() != -1 && captureConfig.getTemplateType() != 5) {
                templateType = -1;
            }
            else {
                templateType = 2;
            }
            if (templateType != -1) {
                builder.setTemplateType(templateType);
            }
        }
        
        private void setTimeout3A(final long mTimeout3A) {
            this.mTimeout3A = mTimeout3A;
        }
        
        void addTask(@NonNull final PipelineTask pipelineTask) {
            this.mTasks.add(pipelineTask);
        }
        
        @NonNull
        ListenableFuture<List<Void>> executeCapture(@NonNull final List<CaptureConfig> list, final int n) {
            Object o = Futures.immediateFuture((V)null);
            if (!this.mTasks.isEmpty()) {
                ListenableFuture<TotalCaptureResult> listenableFuture;
                if (this.mPipelineSubTask.isCaptureResultNeeded()) {
                    listenableFuture = Camera2CapturePipeline.waitForResult(0L, this.mCameraControl, null);
                }
                else {
                    listenableFuture = (ListenableFuture<TotalCaptureResult>)Futures.immediateFuture((V)null);
                }
                o = FutureChain.from((com.google.common.util.concurrent.ListenableFuture<Object>)listenableFuture).transformAsync((AsyncFunction<? super Object, Object>)new \u300708O8o\u30070(this, n), this.mExecutor).transformAsync((AsyncFunction<? super Object, Object>)new oO(this), this.mExecutor);
            }
            final FutureChain<Object> transformAsync = FutureChain.from((com.google.common.util.concurrent.ListenableFuture<Object>)o).transformAsync((AsyncFunction<? super Object, Object>)new \u30078(this, list, n), this.mExecutor);
            final PipelineTask mPipelineSubTask = this.mPipelineSubTask;
            Objects.requireNonNull(mPipelineSubTask);
            ((ListenableFuture)transformAsync).addListener(new O08000(mPipelineSubTask), this.mExecutor);
            return (ListenableFuture<List<Void>>)transformAsync;
        }
        
        @NonNull
        ListenableFuture<List<Void>> submitConfigsInternal(@NonNull final List<CaptureConfig> list, final int n) {
            final ArrayList list2 = new ArrayList();
            final ArrayList list3 = new ArrayList();
            for (final CaptureConfig captureConfig : list) {
                final CaptureConfig.Builder from = CaptureConfig.Builder.from(captureConfig);
                CameraCaptureResult retrieveCameraCaptureResult = null;
                Label_0153: {
                    if (captureConfig.getTemplateType() == 5 && !this.mCameraControl.getZslControl().isZslDisabledByFlashMode() && !this.mCameraControl.getZslControl().isZslDisabledByUserCaseConfig()) {
                        final ImageProxy dequeueImageFromBuffer = this.mCameraControl.getZslControl().dequeueImageFromBuffer();
                        if (dequeueImageFromBuffer != null && this.mCameraControl.getZslControl().enqueueImageToImageWriter(dequeueImageFromBuffer)) {
                            retrieveCameraCaptureResult = CameraCaptureResults.retrieveCameraCaptureResult(dequeueImageFromBuffer.getImageInfo());
                            break Label_0153;
                        }
                    }
                    retrieveCameraCaptureResult = null;
                }
                if (retrieveCameraCaptureResult != null) {
                    from.setCameraCaptureResult(retrieveCameraCaptureResult);
                }
                else {
                    this.applyStillCaptureTemplate(from, captureConfig);
                }
                if (this.mOverrideAeModeForStillCapture.shouldSetAeModeAlwaysFlash(n)) {
                    this.applyAeModeQuirk(from);
                }
                list2.add(CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Object>)new o\u30070OOo\u30070(this, from)));
                list3.add(from.build());
            }
            this.mCameraControl.submitCaptureRequestsInternal(list3);
            return (ListenableFuture<List<Void>>)Futures.allAsList((Collection<? extends com.google.common.util.concurrent.ListenableFuture<?>>)list2);
        }
    }
    
    static class ResultListener implements CaptureResultListener
    {
        static final long NO_TIMEOUT = 0L;
        private final Checker mChecker;
        private CallbackToFutureAdapter.Completer<TotalCaptureResult> mCompleter;
        private final ListenableFuture<TotalCaptureResult> mFuture;
        private final long mTimeLimitNs;
        private volatile Long mTimestampOfFirstUpdateNs;
        
        ResultListener(final long mTimeLimitNs, @Nullable final Checker mChecker) {
            this.mFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<TotalCaptureResult>)new O\u3007O\u3007oO(this));
            this.mTimestampOfFirstUpdateNs = null;
            this.mTimeLimitNs = mTimeLimitNs;
            this.mChecker = mChecker;
        }
        
        @NonNull
        public ListenableFuture<TotalCaptureResult> getFuture() {
            return this.mFuture;
        }
        
        @Override
        public boolean onCaptureResult(@NonNull final TotalCaptureResult totalCaptureResult) {
            final Long n = (Long)((CaptureResult)totalCaptureResult).get(CaptureResult.SENSOR_TIMESTAMP);
            if (n != null && this.mTimestampOfFirstUpdateNs == null) {
                this.mTimestampOfFirstUpdateNs = n;
            }
            final Long mTimestampOfFirstUpdateNs = this.mTimestampOfFirstUpdateNs;
            if (0L != this.mTimeLimitNs && mTimestampOfFirstUpdateNs != null && n != null && n - mTimestampOfFirstUpdateNs > this.mTimeLimitNs) {
                this.mCompleter.set(null);
                final StringBuilder sb = new StringBuilder();
                sb.append("Wait for capture result timeout, current:");
                sb.append(n);
                sb.append(" first: ");
                sb.append(mTimestampOfFirstUpdateNs);
                Logger.d("Camera2CapturePipeline", sb.toString());
                return true;
            }
            final Checker mChecker = this.mChecker;
            if (mChecker != null && !mChecker.check(totalCaptureResult)) {
                return false;
            }
            this.mCompleter.set(totalCaptureResult);
            return true;
        }
        
        interface Checker
        {
            boolean check(@NonNull final TotalCaptureResult p0);
        }
    }
    
    static class TorchTask implements PipelineTask
    {
        private static final long CHECK_3A_WITH_TORCH_TIMEOUT_IN_NS;
        private final Camera2CameraControlImpl mCameraControl;
        private final Executor mExecutor;
        private final int mFlashMode;
        private boolean mIsExecuted;
        
        static {
            CHECK_3A_WITH_TORCH_TIMEOUT_IN_NS = TimeUnit.SECONDS.toNanos(2L);
        }
        
        TorchTask(@NonNull final Camera2CameraControlImpl mCameraControl, final int mFlashMode, @NonNull final Executor mExecutor) {
            this.mIsExecuted = false;
            this.mCameraControl = mCameraControl;
            this.mFlashMode = mFlashMode;
            this.mExecutor = mExecutor;
        }
        
        @Override
        public boolean isCaptureResultNeeded() {
            return this.mFlashMode == 0;
        }
        
        @Override
        public void postCapture() {
            if (this.mIsExecuted) {
                this.mCameraControl.getTorchControl().enableTorchInternal(null, false);
                Logger.d("Camera2CapturePipeline", "Turn off torch");
            }
        }
        
        @NonNull
        @Override
        public ListenableFuture<Boolean> preCapture(@Nullable final TotalCaptureResult totalCaptureResult) {
            if (Camera2CapturePipeline.isFlashRequired(this.mFlashMode, totalCaptureResult)) {
                if (!this.mCameraControl.isTorchOn()) {
                    Logger.d("Camera2CapturePipeline", "Turn on torch");
                    this.mIsExecuted = true;
                    return (ListenableFuture<Boolean>)FutureChain.from((com.google.common.util.concurrent.ListenableFuture<Object>)CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<V>)new o\u30078oOO88(this))).transformAsync((AsyncFunction<? super Object, Object>)new o\u3007O(this), this.mExecutor).transform((Function<? super Object, Object>)new oO00OOO(), CameraXExecutors.directExecutor());
                }
                Logger.d("Camera2CapturePipeline", "Torch already on, not turn on");
            }
            return Futures.immediateFuture(Boolean.FALSE);
        }
    }
}

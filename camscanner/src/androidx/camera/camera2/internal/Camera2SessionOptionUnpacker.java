// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import androidx.camera.camera2.interop.ExperimentalCamera2Interop;
import androidx.annotation.OptIn;
import androidx.camera.camera2.impl.CameraEventCallbacks;
import androidx.camera.core.impl.MutableOptionsBundle;
import androidx.camera.core.impl.Config;
import androidx.camera.camera2.impl.Camera2ImplConfig;
import androidx.camera.core.impl.CameraCaptureCallback;
import android.hardware.camera2.CameraDevice$StateCallback;
import java.util.Collection;
import androidx.camera.core.impl.OptionsBundle;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.UseCaseConfig;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.SessionConfig;

@RequiresApi(21)
final class Camera2SessionOptionUnpacker implements OptionUnpacker
{
    static final Camera2SessionOptionUnpacker INSTANCE;
    
    static {
        INSTANCE = new Camera2SessionOptionUnpacker();
    }
    
    @OptIn(markerClass = { ExperimentalCamera2Interop.class })
    @Override
    public void unpack(@NonNull final UseCaseConfig<?> useCaseConfig, @NonNull final SessionConfig.Builder builder) {
        final SessionConfig defaultSessionConfig = useCaseConfig.getDefaultSessionConfig(null);
        Config implementationOptions = OptionsBundle.emptyBundle();
        int n = SessionConfig.defaultEmptySessionConfig().getTemplateType();
        if (defaultSessionConfig != null) {
            n = defaultSessionConfig.getTemplateType();
            builder.addAllDeviceStateCallbacks(defaultSessionConfig.getDeviceStateCallbacks());
            builder.addAllSessionStateCallbacks(defaultSessionConfig.getSessionStateCallbacks());
            builder.addAllRepeatingCameraCaptureCallbacks(defaultSessionConfig.getRepeatingCameraCaptureCallbacks());
            implementationOptions = defaultSessionConfig.getImplementationOptions();
        }
        builder.setImplementationOptions(implementationOptions);
        final Camera2ImplConfig camera2ImplConfig = new Camera2ImplConfig(useCaseConfig);
        builder.setTemplateType(camera2ImplConfig.getCaptureRequestTemplate(n));
        builder.addDeviceStateCallback(camera2ImplConfig.getDeviceStateCallback(CameraDeviceStateCallbacks.createNoOpCallback()));
        builder.addSessionStateCallback(camera2ImplConfig.getSessionStateCallback(CameraCaptureSessionStateCallbacks.createNoOpCallback()));
        builder.addCameraCaptureCallback(CaptureCallbackContainer.create(camera2ImplConfig.getSessionCaptureCallback(Camera2CaptureCallbacks.createNoOpCallback())));
        final MutableOptionsBundle create = MutableOptionsBundle.create();
        create.insertOption(Camera2ImplConfig.CAMERA_EVENT_CALLBACK_OPTION, camera2ImplConfig.getCameraEventCallback(CameraEventCallbacks.createEmptyCallback()));
        create.insertOption(Camera2ImplConfig.SESSION_PHYSICAL_CAMERA_ID_OPTION, camera2ImplConfig.getPhysicalCameraId(null));
        create.insertOption(Camera2ImplConfig.STREAM_USE_CASE_OPTION, camera2ImplConfig.getStreamUseCase(-1L));
        builder.addImplementationOptions(create);
        builder.addImplementationOptions(camera2ImplConfig.getCaptureRequestOptions());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import com.google.common.util.concurrent.ListenableFuture;
import androidx.annotation.FloatRange;
import androidx.lifecycle.LiveData;
import android.graphics.Rect;
import androidx.camera.camera2.impl.Camera2ImplConfig;
import android.os.Looper;
import androidx.camera.core.CameraControl;
import androidx.annotation.VisibleForTesting;
import android.os.Build$VERSION;
import androidx.camera.core.Logger;
import android.hardware.camera2.CameraCharacteristics$Key;
import android.util.Range;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.camera.core.internal.ImmutableZoomState;
import android.hardware.camera2.TotalCaptureResult;
import androidx.camera.camera2.internal.compat.CameraCharacteristicsCompat;
import androidx.camera.core.ZoomState;
import androidx.lifecycle.MutableLiveData;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;
import androidx.annotation.GuardedBy;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
final class ZoomControl
{
    public static final float DEFAULT_ZOOM_RATIO = 1.0f;
    private static final String TAG = "ZoomControl";
    private final Camera2CameraControlImpl mCamera2CameraControlImpl;
    private Camera2CameraControlImpl.CaptureResultListener mCaptureResultListener;
    @GuardedBy("mCurrentZoomState")
    private final ZoomStateImpl mCurrentZoomState;
    private final Executor mExecutor;
    private boolean mIsActive;
    @NonNull
    final ZoomImpl mZoomImpl;
    private final MutableLiveData<ZoomState> mZoomStateLiveData;
    
    ZoomControl(@NonNull final Camera2CameraControlImpl mCamera2CameraControlImpl, @NonNull final CameraCharacteristicsCompat cameraCharacteristicsCompat, @NonNull final Executor mExecutor) {
        this.mIsActive = false;
        this.mCaptureResultListener = new Camera2CameraControlImpl.CaptureResultListener() {
            final ZoomControl this$0;
            
            @Override
            public boolean onCaptureResult(@NonNull final TotalCaptureResult totalCaptureResult) {
                this.this$0.mZoomImpl.onCaptureResult(totalCaptureResult);
                return false;
            }
        };
        this.mCamera2CameraControlImpl = mCamera2CameraControlImpl;
        this.mExecutor = mExecutor;
        final ZoomImpl zoomImpl = createZoomImpl(cameraCharacteristicsCompat);
        this.mZoomImpl = zoomImpl;
        final ZoomStateImpl mCurrentZoomState = new ZoomStateImpl(zoomImpl.getMaxZoom(), zoomImpl.getMinZoom());
        (this.mCurrentZoomState = mCurrentZoomState).setZoomRatio(1.0f);
        this.mZoomStateLiveData = new MutableLiveData<ZoomState>(ImmutableZoomState.create(mCurrentZoomState));
        mCamera2CameraControlImpl.addCaptureResultListener(this.mCaptureResultListener);
    }
    
    private static ZoomImpl createZoomImpl(@NonNull final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        if (isAndroidRZoomSupported(cameraCharacteristicsCompat)) {
            return (ZoomImpl)new AndroidRZoomImpl(cameraCharacteristicsCompat);
        }
        return (ZoomImpl)new CropRegionZoomImpl(cameraCharacteristicsCompat);
    }
    
    static ZoomState getDefaultZoomState(final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        final ZoomImpl zoomImpl = createZoomImpl(cameraCharacteristicsCompat);
        final ZoomStateImpl zoomStateImpl = new ZoomStateImpl(zoomImpl.getMaxZoom(), zoomImpl.getMinZoom());
        zoomStateImpl.setZoomRatio(1.0f);
        return ImmutableZoomState.create(zoomStateImpl);
    }
    
    @RequiresApi(30)
    private static Range<Float> getZoomRatioRange(final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        try {
            return (Range<Float>)cameraCharacteristicsCompat.get((android.hardware.camera2.CameraCharacteristics$Key<Range>)\u3007080.\u3007080());
        }
        catch (final AssertionError assertionError) {
            Logger.w("ZoomControl", "AssertionError, fail to get camera characteristic.", assertionError);
            return null;
        }
    }
    
    @VisibleForTesting
    static boolean isAndroidRZoomSupported(final CameraCharacteristicsCompat cameraCharacteristicsCompat) {
        return Build$VERSION.SDK_INT >= 30 && getZoomRatioRange(cameraCharacteristicsCompat) != null;
    }
    
    private void submitCameraZoomRatio(@NonNull final CallbackToFutureAdapter.Completer<Void> completer, @NonNull final ZoomState zoomState) {
        if (!this.mIsActive) {
            synchronized (this.mCurrentZoomState) {
                this.mCurrentZoomState.setZoomRatio(1.0f);
                final ZoomState create = ImmutableZoomState.create(this.mCurrentZoomState);
                monitorexit(this.mCurrentZoomState);
                this.updateLiveData(create);
                completer.setException(new CameraControl.OperationCanceledException("Camera is not active."));
                return;
            }
        }
        this.updateLiveData(zoomState);
        this.mZoomImpl.setZoomRatio(zoomState.getZoomRatio(), completer);
        this.mCamera2CameraControlImpl.updateSessionConfigSynchronous();
    }
    
    private void updateLiveData(final ZoomState value) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            this.mZoomStateLiveData.setValue(value);
        }
        else {
            this.mZoomStateLiveData.postValue(value);
        }
    }
    
    void addZoomOption(@NonNull final Camera2ImplConfig.Builder builder) {
        this.mZoomImpl.addRequestOption(builder);
    }
    
    @NonNull
    Rect getCropSensorRegion() {
        return this.mZoomImpl.getCropSensorRegion();
    }
    
    LiveData<ZoomState> getZoomState() {
        return this.mZoomStateLiveData;
    }
    
    void setActive(final boolean mIsActive) {
        if (this.mIsActive == mIsActive) {
            return;
        }
        if (!(this.mIsActive = mIsActive)) {
            synchronized (this.mCurrentZoomState) {
                this.mCurrentZoomState.setZoomRatio(1.0f);
                final ZoomState create = ImmutableZoomState.create(this.mCurrentZoomState);
                monitorexit(this.mCurrentZoomState);
                this.updateLiveData(create);
                this.mZoomImpl.resetZoom();
                this.mCamera2CameraControlImpl.updateSessionConfigSynchronous();
            }
        }
    }
    
    @NonNull
    ListenableFuture<Void> setLinearZoom(@FloatRange(from = 0.0, to = 1.0) final float linearZoom) {
        final ZoomStateImpl mCurrentZoomState = this.mCurrentZoomState;
        monitorenter(mCurrentZoomState);
        try {
            try {
                this.mCurrentZoomState.setLinearZoom(linearZoom);
                final ZoomState create = ImmutableZoomState.create(this.mCurrentZoomState);
                monitorexit(mCurrentZoomState);
                this.updateLiveData(create);
                return CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new Oo0oO\u3007O\u3007O(this, create));
            }
            finally {
                monitorexit(mCurrentZoomState);
            }
        }
        catch (final IllegalArgumentException ex) {}
    }
    
    @NonNull
    ListenableFuture<Void> setZoomRatio(final float zoomRatio) {
        final ZoomStateImpl mCurrentZoomState = this.mCurrentZoomState;
        monitorenter(mCurrentZoomState);
        try {
            try {
                this.mCurrentZoomState.setZoomRatio(zoomRatio);
                final ZoomState create = ImmutableZoomState.create(this.mCurrentZoomState);
                monitorexit(mCurrentZoomState);
                this.updateLiveData(create);
                return CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new O0o(this, create));
            }
            finally {
                monitorexit(mCurrentZoomState);
            }
        }
        catch (final IllegalArgumentException ex) {}
    }
    
    interface ZoomImpl
    {
        void addRequestOption(@NonNull final Camera2ImplConfig.Builder p0);
        
        @NonNull
        Rect getCropSensorRegion();
        
        float getMaxZoom();
        
        float getMinZoom();
        
        void onCaptureResult(@NonNull final TotalCaptureResult p0);
        
        void resetZoom();
        
        void setZoomRatio(final float p0, @NonNull final CallbackToFutureAdapter.Completer<Void> p1);
    }
}

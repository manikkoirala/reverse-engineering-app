// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.internal;

import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.CaptureFailure;
import android.hardware.camera2.TotalCaptureResult;
import androidx.camera.camera2.internal.compat.ApiCompat;
import android.view.Surface;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CameraCaptureSession;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Arrays;
import androidx.annotation.NonNull;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class Camera2CaptureCallbacks
{
    private Camera2CaptureCallbacks() {
    }
    
    static CameraCaptureSession$CaptureCallback createComboCallback(final List<CameraCaptureSession$CaptureCallback> list) {
        return new ComboSessionCaptureCallback(list);
    }
    
    @NonNull
    public static CameraCaptureSession$CaptureCallback createComboCallback(@NonNull final CameraCaptureSession$CaptureCallback... a) {
        return createComboCallback(Arrays.asList(a));
    }
    
    @NonNull
    public static CameraCaptureSession$CaptureCallback createNoOpCallback() {
        return new NoOpSessionCaptureCallback();
    }
    
    @RequiresApi(21)
    private static final class ComboSessionCaptureCallback extends CameraCaptureSession$CaptureCallback
    {
        private final List<CameraCaptureSession$CaptureCallback> mCallbacks;
        
        ComboSessionCaptureCallback(final List<CameraCaptureSession$CaptureCallback> list) {
            this.mCallbacks = new ArrayList<CameraCaptureSession$CaptureCallback>();
            for (final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback : list) {
                if (!(cameraCaptureSession$CaptureCallback instanceof NoOpSessionCaptureCallback)) {
                    this.mCallbacks.add(cameraCaptureSession$CaptureCallback);
                }
            }
        }
        
        @RequiresApi(api = 24)
        public void onCaptureBufferLost(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, @NonNull final Surface surface, final long n) {
            final Iterator<CameraCaptureSession$CaptureCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                ApiCompat.Api24Impl.onCaptureBufferLost(iterator.next(), cameraCaptureSession, captureRequest, surface, n);
            }
        }
        
        public void onCaptureCompleted(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, @NonNull final TotalCaptureResult totalCaptureResult) {
            final Iterator<CameraCaptureSession$CaptureCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onCaptureCompleted(cameraCaptureSession, captureRequest, totalCaptureResult);
            }
        }
        
        public void onCaptureFailed(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, @NonNull final CaptureFailure captureFailure) {
            final Iterator<CameraCaptureSession$CaptureCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onCaptureFailed(cameraCaptureSession, captureRequest, captureFailure);
            }
        }
        
        public void onCaptureProgressed(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, @NonNull final CaptureResult captureResult) {
            final Iterator<CameraCaptureSession$CaptureCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onCaptureProgressed(cameraCaptureSession, captureRequest, captureResult);
            }
        }
        
        public void onCaptureSequenceAborted(@NonNull final CameraCaptureSession cameraCaptureSession, final int n) {
            final Iterator<CameraCaptureSession$CaptureCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onCaptureSequenceAborted(cameraCaptureSession, n);
            }
        }
        
        public void onCaptureSequenceCompleted(@NonNull final CameraCaptureSession cameraCaptureSession, final int n, final long n2) {
            final Iterator<CameraCaptureSession$CaptureCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onCaptureSequenceCompleted(cameraCaptureSession, n, n2);
            }
        }
        
        public void onCaptureStarted(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, final long n, final long n2) {
            final Iterator<CameraCaptureSession$CaptureCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onCaptureStarted(cameraCaptureSession, captureRequest, n, n2);
            }
        }
    }
    
    static final class NoOpSessionCaptureCallback extends CameraCaptureSession$CaptureCallback
    {
        public void onCaptureBufferLost(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, @NonNull final Surface surface, final long n) {
        }
        
        public void onCaptureCompleted(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, @NonNull final TotalCaptureResult totalCaptureResult) {
        }
        
        public void onCaptureFailed(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, @NonNull final CaptureFailure captureFailure) {
        }
        
        public void onCaptureProgressed(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, @NonNull final CaptureResult captureResult) {
        }
        
        public void onCaptureSequenceAborted(@NonNull final CameraCaptureSession cameraCaptureSession, final int n) {
        }
        
        public void onCaptureSequenceCompleted(@NonNull final CameraCaptureSession cameraCaptureSession, final int n, final long n2) {
        }
        
        public void onCaptureStarted(@NonNull final CameraCaptureSession cameraCaptureSession, @NonNull final CaptureRequest captureRequest, final long n, final long n2) {
        }
    }
}

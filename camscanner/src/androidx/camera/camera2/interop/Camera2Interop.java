// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.interop;

import android.hardware.camera2.CameraCaptureSession$StateCallback;
import android.hardware.camera2.CameraCaptureSession$CaptureCallback;
import android.annotation.SuppressLint;
import android.hardware.camera2.CameraDevice$StateCallback;
import androidx.annotation.RestrictTo;
import androidx.camera.core.impl.Config;
import androidx.camera.camera2.impl.Camera2ImplConfig;
import android.hardware.camera2.CaptureRequest$Key;
import androidx.annotation.NonNull;
import androidx.camera.core.ExtendableBuilder;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
@ExperimentalCamera2Interop
public final class Camera2Interop
{
    private Camera2Interop() {
    }
    
    @RequiresApi(21)
    public static final class Extender<T>
    {
        ExtendableBuilder<T> mBaseBuilder;
        
        public Extender(@NonNull final ExtendableBuilder<T> mBaseBuilder) {
            this.mBaseBuilder = mBaseBuilder;
        }
        
        @NonNull
        public <ValueT> Extender<T> setCaptureRequestOption(@NonNull final CaptureRequest$Key<ValueT> captureRequest$Key, @NonNull final ValueT valueT) {
            this.mBaseBuilder.getMutableConfig().insertOption((Config.Option<ValueT>)Camera2ImplConfig.createCaptureRequestOption(captureRequest$Key), Config.OptionPriority.ALWAYS_OVERRIDE, valueT);
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY })
        public Extender<T> setCaptureRequestTemplate(final int i) {
            this.mBaseBuilder.getMutableConfig().insertOption(Camera2ImplConfig.TEMPLATE_TYPE_OPTION, i);
            return this;
        }
        
        @SuppressLint({ "ExecutorRegistration" })
        @NonNull
        public Extender<T> setDeviceStateCallback(@NonNull final CameraDevice$StateCallback cameraDevice$StateCallback) {
            this.mBaseBuilder.getMutableConfig().insertOption(Camera2ImplConfig.DEVICE_STATE_CALLBACK_OPTION, cameraDevice$StateCallback);
            return this;
        }
        
        @NonNull
        @RequiresApi(28)
        public Extender<T> setPhysicalCameraId(@NonNull final String s) {
            this.mBaseBuilder.getMutableConfig().insertOption(Camera2ImplConfig.SESSION_PHYSICAL_CAMERA_ID_OPTION, s);
            return this;
        }
        
        @SuppressLint({ "ExecutorRegistration" })
        @NonNull
        public Extender<T> setSessionCaptureCallback(@NonNull final CameraCaptureSession$CaptureCallback cameraCaptureSession$CaptureCallback) {
            this.mBaseBuilder.getMutableConfig().insertOption(Camera2ImplConfig.SESSION_CAPTURE_CALLBACK_OPTION, cameraCaptureSession$CaptureCallback);
            return this;
        }
        
        @SuppressLint({ "ExecutorRegistration" })
        @NonNull
        public Extender<T> setSessionStateCallback(@NonNull final CameraCaptureSession$StateCallback cameraCaptureSession$StateCallback) {
            this.mBaseBuilder.getMutableConfig().insertOption(Camera2ImplConfig.SESSION_STATE_CALLBACK_OPTION, cameraCaptureSession$StateCallback);
            return this;
        }
        
        @NonNull
        @RequiresApi(33)
        public Extender<T> setStreamUseCase(final long l) {
            this.mBaseBuilder.getMutableConfig().insertOption(Camera2ImplConfig.STREAM_USE_CASE_OPTION, l);
            return this;
        }
    }
}

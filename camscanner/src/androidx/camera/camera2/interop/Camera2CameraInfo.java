// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.interop;

import java.util.Map;
import androidx.annotation.Nullable;
import android.hardware.camera2.CameraCharacteristics$Key;
import androidx.core.util.Preconditions;
import android.hardware.camera2.CameraCharacteristics;
import androidx.camera.core.CameraInfo;
import androidx.annotation.RestrictTo;
import androidx.annotation.NonNull;
import androidx.camera.camera2.internal.Camera2CameraInfoImpl;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
@ExperimentalCamera2Interop
public final class Camera2CameraInfo
{
    private static final String TAG = "Camera2CameraInfo";
    private final Camera2CameraInfoImpl mCamera2CameraInfoImpl;
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public Camera2CameraInfo(@NonNull final Camera2CameraInfoImpl mCamera2CameraInfoImpl) {
        this.mCamera2CameraInfoImpl = mCamera2CameraInfoImpl;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static CameraCharacteristics extractCameraCharacteristics(@NonNull final CameraInfo cameraInfo) {
        Preconditions.checkState(cameraInfo instanceof Camera2CameraInfoImpl, "CameraInfo does not contain any Camera2 information.");
        return ((Camera2CameraInfoImpl)cameraInfo).getCameraCharacteristicsCompat().toCameraCharacteristics();
    }
    
    @NonNull
    public static Camera2CameraInfo from(@NonNull final CameraInfo cameraInfo) {
        Preconditions.checkArgument(cameraInfo instanceof Camera2CameraInfoImpl, (Object)"CameraInfo doesn't contain Camera2 implementation.");
        return ((Camera2CameraInfoImpl)cameraInfo).getCamera2CameraInfo();
    }
    
    @Nullable
    public <T> T getCameraCharacteristic(@NonNull final CameraCharacteristics$Key<T> cameraCharacteristics$Key) {
        return this.mCamera2CameraInfoImpl.getCameraCharacteristicsCompat().get(cameraCharacteristics$Key);
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public Map<String, CameraCharacteristics> getCameraCharacteristicsMap() {
        return this.mCamera2CameraInfoImpl.getCameraCharacteristicsMap();
    }
    
    @NonNull
    public String getCameraId() {
        return this.mCamera2CameraInfoImpl.getCameraId();
    }
}

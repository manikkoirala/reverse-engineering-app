// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.interop;

import android.hardware.camera2.CaptureResult;
import androidx.camera.core.impl.utils.futures.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.camera.core.impl.TagBundle;
import androidx.core.util.Preconditions;
import androidx.camera.core.CameraControl;
import java.util.Iterator;
import androidx.camera.core.impl.Config;
import android.hardware.camera2.TotalCaptureResult;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.camera.camera2.internal.Camera2CameraControlImpl;
import androidx.annotation.GuardedBy;
import androidx.camera.camera2.impl.Camera2ImplConfig;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
@ExperimentalCamera2Interop
public final class Camera2CameraControl
{
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final String TAG_KEY = "Camera2CameraControl";
    @GuardedBy("mLock")
    private Camera2ImplConfig.Builder mBuilder;
    private final Camera2CameraControlImpl mCamera2CameraControlImpl;
    private final Camera2CameraControlImpl.CaptureResultListener mCaptureResultListener;
    CallbackToFutureAdapter.Completer<Void> mCompleter;
    final Executor mExecutor;
    private boolean mIsActive;
    final Object mLock;
    private boolean mPendingUpdate;
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public Camera2CameraControl(@NonNull final Camera2CameraControlImpl mCamera2CameraControlImpl, @NonNull final Executor mExecutor) {
        this.mIsActive = false;
        this.mPendingUpdate = false;
        this.mLock = new Object();
        this.mBuilder = new Camera2ImplConfig.Builder();
        this.mCaptureResultListener = new Oo08(this);
        this.mCamera2CameraControlImpl = mCamera2CameraControlImpl;
        this.mExecutor = mExecutor;
    }
    
    private void addCaptureRequestOptionsInternal(@NonNull final CaptureRequestOptions captureRequestOptions) {
        synchronized (this.mLock) {
            for (final Config.Option option : captureRequestOptions.listOptions()) {
                this.mBuilder.getMutableConfig().insertOption(option, captureRequestOptions.retrieveOption(option));
            }
        }
    }
    
    private void clearCaptureRequestOptionsInternal() {
        synchronized (this.mLock) {
            this.mBuilder = new Camera2ImplConfig.Builder();
        }
    }
    
    @NonNull
    public static Camera2CameraControl from(@NonNull final CameraControl cameraControl) {
        Preconditions.checkArgument(cameraControl instanceof Camera2CameraControlImpl, (Object)"CameraControl doesn't contain Camera2 implementation.");
        return ((Camera2CameraControlImpl)cameraControl).getCamera2CameraControl();
    }
    
    private void setActiveInternal(final boolean mIsActive) {
        if (this.mIsActive == mIsActive) {
            return;
        }
        this.mIsActive = mIsActive;
        if (mIsActive) {
            if (this.mPendingUpdate) {
                this.updateSession();
            }
        }
        else {
            final CallbackToFutureAdapter.Completer<Void> mCompleter = this.mCompleter;
            if (mCompleter != null) {
                mCompleter.setException(new CameraControl.OperationCanceledException("The camera control has became inactive."));
                this.mCompleter = null;
            }
        }
    }
    
    private void updateConfig(final CallbackToFutureAdapter.Completer<Void> mCompleter) {
        this.mPendingUpdate = true;
        CallbackToFutureAdapter.Completer<Void> mCompleter2 = this.mCompleter;
        if (mCompleter2 == null) {
            mCompleter2 = null;
        }
        this.mCompleter = mCompleter;
        if (this.mIsActive) {
            this.updateSession();
        }
        if (mCompleter2 != null) {
            mCompleter2.setException(new CameraControl.OperationCanceledException("Camera2CameraControl was updated with new options."));
        }
    }
    
    private void updateSession() {
        this.mCamera2CameraControlImpl.updateSessionConfig();
        this.mPendingUpdate = false;
    }
    
    @NonNull
    public ListenableFuture<Void> addCaptureRequestOptions(@NonNull final CaptureRequestOptions captureRequestOptions) {
        this.addCaptureRequestOptionsInternal(captureRequestOptions);
        return Futures.nonCancellationPropagating((com.google.common.util.concurrent.ListenableFuture<Void>)CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<V>)new O8(this)));
    }
    
    @NonNull
    public ListenableFuture<Void> clearCaptureRequestOptions() {
        this.clearCaptureRequestOptionsInternal();
        return Futures.nonCancellationPropagating((com.google.common.util.concurrent.ListenableFuture<Void>)CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<V>)new \u3007o\u3007(this)));
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public Camera2ImplConfig getCamera2ImplConfig() {
        synchronized (this.mLock) {
            if (this.mCompleter != null) {
                this.mBuilder.getMutableConfig().insertOption(Camera2ImplConfig.CAPTURE_REQUEST_TAG_OPTION, this.mCompleter.hashCode());
            }
            return this.mBuilder.build();
        }
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public Camera2CameraControlImpl.CaptureResultListener getCaptureRequestListener() {
        return this.mCaptureResultListener;
    }
    
    @NonNull
    public CaptureRequestOptions getCaptureRequestOptions() {
        synchronized (this.mLock) {
            return CaptureRequestOptions.Builder.from(this.mBuilder.build()).build();
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public void setActive(final boolean b) {
        this.mExecutor.execute(new \u3007o00\u3007\u3007Oo(this, b));
    }
    
    @NonNull
    public ListenableFuture<Void> setCaptureRequestOptions(@NonNull final CaptureRequestOptions captureRequestOptions) {
        this.clearCaptureRequestOptionsInternal();
        this.addCaptureRequestOptionsInternal(captureRequestOptions);
        return Futures.nonCancellationPropagating((com.google.common.util.concurrent.ListenableFuture<Void>)CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<V>)new o\u30070(this)));
    }
}

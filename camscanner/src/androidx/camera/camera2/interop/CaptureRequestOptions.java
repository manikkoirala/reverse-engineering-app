// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.interop;

import androidx.camera.core.impl.MutableConfig;
import androidx.camera.core.impl.OptionsBundle;
import androidx.camera.core.impl.MutableOptionsBundle;
import androidx.camera.core.ExtendableBuilder;
import java.util.Set;
import androidx.annotation.Nullable;
import androidx.camera.camera2.impl.Camera2ImplConfig;
import android.hardware.camera2.CaptureRequest$Key;
import androidx.camera.core.impl.o\u3007\u30070\u3007;
import androidx.annotation.RestrictTo;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.Config;
import androidx.annotation.RequiresApi;
import androidx.camera.core.impl.ReadableConfig;

@RequiresApi(21)
@ExperimentalCamera2Interop
public class CaptureRequestOptions implements ReadableConfig
{
    private final Config mConfig;
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public CaptureRequestOptions(@NonNull final Config mConfig) {
        this.mConfig = mConfig;
    }
    
    @Nullable
    public <ValueT> ValueT getCaptureRequestOption(@NonNull final CaptureRequest$Key<ValueT> captureRequest$Key) {
        return this.mConfig.retrieveOption((Config.Option<ValueT>)Camera2ImplConfig.createCaptureRequestOption(captureRequest$Key), (ValueT)null);
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public <ValueT> ValueT getCaptureRequestOption(@NonNull final CaptureRequest$Key<ValueT> captureRequest$Key, @Nullable final ValueT valueT) {
        return this.mConfig.retrieveOption((Config.Option<ValueT>)Camera2ImplConfig.createCaptureRequestOption(captureRequest$Key), valueT);
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    @Override
    public Config getConfig() {
        return this.mConfig;
    }
    
    @RequiresApi(21)
    public static final class Builder implements ExtendableBuilder<CaptureRequestOptions>
    {
        private final MutableOptionsBundle mMutableOptionsBundle;
        
        public Builder() {
            this.mMutableOptionsBundle = MutableOptionsBundle.create();
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY })
        public static Builder from(@NonNull final Config config) {
            final Builder builder = new Builder();
            config.findOptions("camera2.captureRequest.option.", (Config.OptionMatcher)new \u300780\u3007808\u3007O(builder, config));
            return builder;
        }
        
        @NonNull
        @Override
        public CaptureRequestOptions build() {
            return new CaptureRequestOptions(OptionsBundle.from(this.mMutableOptionsBundle));
        }
        
        @NonNull
        public <ValueT> Builder clearCaptureRequestOption(@NonNull final CaptureRequest$Key<ValueT> captureRequest$Key) {
            this.mMutableOptionsBundle.removeOption(Camera2ImplConfig.createCaptureRequestOption(captureRequest$Key));
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY })
        @Override
        public MutableConfig getMutableConfig() {
            return this.mMutableOptionsBundle;
        }
        
        @NonNull
        public <ValueT> Builder setCaptureRequestOption(@NonNull final CaptureRequest$Key<ValueT> captureRequest$Key, @NonNull final ValueT valueT) {
            this.mMutableOptionsBundle.insertOption((Option<ValueT>)Camera2ImplConfig.createCaptureRequestOption(captureRequest$Key), valueT);
            return this;
        }
    }
}

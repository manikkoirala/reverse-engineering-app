// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.camera2.interop;

import androidx.annotation.RequiresOptIn;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.CLASS)
@RequiresOptIn
public @interface ExperimentalCamera2Interop {
}

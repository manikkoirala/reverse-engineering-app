// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.lifecycle;

import androidx.lifecycle.OnLifecycleEvent;
import java.util.Collections;
import androidx.camera.core.internal.CameraUseCaseAdapter;
import androidx.lifecycle.Lifecycle;
import androidx.camera.core.UseCase;
import java.util.Collection;
import androidx.camera.core.CameraEffect;
import java.util.List;
import androidx.annotation.Nullable;
import androidx.camera.core.ViewPort;
import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleObserver;
import java.util.HashSet;
import androidx.core.util.Preconditions;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Set;
import java.util.Map;
import androidx.annotation.GuardedBy;
import androidx.lifecycle.LifecycleOwner;
import java.util.ArrayDeque;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
final class LifecycleCameraRepository
{
    @GuardedBy("mLock")
    private final ArrayDeque<LifecycleOwner> mActiveLifecycleOwners;
    @GuardedBy("mLock")
    private final Map<Key, LifecycleCamera> mCameraMap;
    @GuardedBy("mLock")
    private final Map<LifecycleCameraRepositoryObserver, Set<Key>> mLifecycleObserverMap;
    private final Object mLock;
    
    LifecycleCameraRepository() {
        this.mLock = new Object();
        this.mCameraMap = new HashMap<Key, LifecycleCamera>();
        this.mLifecycleObserverMap = new HashMap<LifecycleCameraRepositoryObserver, Set<Key>>();
        this.mActiveLifecycleOwners = new ArrayDeque<LifecycleOwner>();
    }
    
    private LifecycleCameraRepositoryObserver getLifecycleCameraRepositoryObserver(final LifecycleOwner lifecycleOwner) {
        synchronized (this.mLock) {
            for (final LifecycleCameraRepositoryObserver lifecycleCameraRepositoryObserver : this.mLifecycleObserverMap.keySet()) {
                if (lifecycleOwner.equals(lifecycleCameraRepositoryObserver.getLifecycleOwner())) {
                    return lifecycleCameraRepositoryObserver;
                }
            }
            return null;
        }
    }
    
    private boolean hasUseCaseBound(final LifecycleOwner lifecycleOwner) {
        synchronized (this.mLock) {
            final LifecycleCameraRepositoryObserver lifecycleCameraRepositoryObserver = this.getLifecycleCameraRepositoryObserver(lifecycleOwner);
            if (lifecycleCameraRepositoryObserver == null) {
                return false;
            }
            final Iterator iterator = this.mLifecycleObserverMap.get(lifecycleCameraRepositoryObserver).iterator();
            while (iterator.hasNext()) {
                if (!Preconditions.checkNotNull(this.mCameraMap.get(iterator.next())).getUseCases().isEmpty()) {
                    return true;
                }
            }
            return false;
        }
    }
    
    private void registerCamera(final LifecycleCamera lifecycleCamera) {
        synchronized (this.mLock) {
            final LifecycleOwner lifecycleOwner = lifecycleCamera.getLifecycleOwner();
            final Key create = Key.create(lifecycleOwner, lifecycleCamera.getCameraUseCaseAdapter().getCameraId());
            final LifecycleCameraRepositoryObserver lifecycleCameraRepositoryObserver = this.getLifecycleCameraRepositoryObserver(lifecycleOwner);
            Set set;
            if (lifecycleCameraRepositoryObserver != null) {
                set = this.mLifecycleObserverMap.get(lifecycleCameraRepositoryObserver);
            }
            else {
                set = new HashSet();
            }
            set.add(create);
            this.mCameraMap.put(create, lifecycleCamera);
            if (lifecycleCameraRepositoryObserver == null) {
                final LifecycleCameraRepositoryObserver lifecycleCameraRepositoryObserver2 = new LifecycleCameraRepositoryObserver(lifecycleOwner, this);
                this.mLifecycleObserverMap.put(lifecycleCameraRepositoryObserver2, set);
                lifecycleOwner.getLifecycle().addObserver(lifecycleCameraRepositoryObserver2);
            }
        }
    }
    
    private void suspendUseCases(final LifecycleOwner lifecycleOwner) {
        synchronized (this.mLock) {
            final LifecycleCameraRepositoryObserver lifecycleCameraRepositoryObserver = this.getLifecycleCameraRepositoryObserver(lifecycleOwner);
            if (lifecycleCameraRepositoryObserver == null) {
                return;
            }
            final Iterator iterator = this.mLifecycleObserverMap.get(lifecycleCameraRepositoryObserver).iterator();
            while (iterator.hasNext()) {
                Preconditions.checkNotNull(this.mCameraMap.get(iterator.next())).suspend();
            }
        }
    }
    
    private void unsuspendUseCases(final LifecycleOwner lifecycleOwner) {
        synchronized (this.mLock) {
            final Iterator iterator = this.mLifecycleObserverMap.get(this.getLifecycleCameraRepositoryObserver(lifecycleOwner)).iterator();
            while (iterator.hasNext()) {
                final LifecycleCamera lifecycleCamera = this.mCameraMap.get(iterator.next());
                if (!Preconditions.checkNotNull(lifecycleCamera).getUseCases().isEmpty()) {
                    lifecycleCamera.unsuspend();
                }
            }
        }
    }
    
    void bindToLifecycleCamera(@NonNull final LifecycleCamera obj, @Nullable final ViewPort viewPort, @NonNull final List<CameraEffect> effects, @NonNull final Collection<UseCase> collection) {
        synchronized (this.mLock) {
            Preconditions.checkArgument(!collection.isEmpty());
            final LifecycleOwner lifecycleOwner = obj.getLifecycleOwner();
            final Iterator iterator = this.mLifecycleObserverMap.get(this.getLifecycleCameraRepositoryObserver(lifecycleOwner)).iterator();
            while (iterator.hasNext()) {
                final LifecycleCamera lifecycleCamera = Preconditions.checkNotNull(this.mCameraMap.get(iterator.next()));
                if (!lifecycleCamera.equals(obj)) {
                    if (lifecycleCamera.getUseCases().isEmpty()) {
                        continue;
                    }
                    throw new IllegalArgumentException("Multiple LifecycleCameras with use cases are registered to the same LifecycleOwner.");
                }
            }
            try {
                obj.getCameraUseCaseAdapter().setViewPort(viewPort);
                obj.getCameraUseCaseAdapter().setEffects(effects);
                obj.bind(collection);
                if (lifecycleOwner.getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
                    this.setActive(lifecycleOwner);
                }
            }
            catch (final CameraUseCaseAdapter.CameraException ex) {
                throw new IllegalArgumentException(ex.getMessage());
            }
        }
    }
    
    void clear() {
        synchronized (this.mLock) {
            final Iterator iterator = new HashSet(this.mLifecycleObserverMap.keySet()).iterator();
            while (iterator.hasNext()) {
                this.unregisterLifecycle(((LifecycleCameraRepositoryObserver)iterator.next()).getLifecycleOwner());
            }
        }
    }
    
    LifecycleCamera createLifecycleCamera(@NonNull final LifecycleOwner lifecycleOwner, @NonNull final CameraUseCaseAdapter cameraUseCaseAdapter) {
        synchronized (this.mLock) {
            Preconditions.checkArgument(this.mCameraMap.get(Key.create(lifecycleOwner, cameraUseCaseAdapter.getCameraId())) == null, (Object)"LifecycleCamera already exists for the given LifecycleOwner and set of cameras");
            if (lifecycleOwner.getLifecycle().getCurrentState() != Lifecycle.State.DESTROYED) {
                final LifecycleCamera lifecycleCamera = new LifecycleCamera(lifecycleOwner, cameraUseCaseAdapter);
                if (cameraUseCaseAdapter.getUseCases().isEmpty()) {
                    lifecycleCamera.suspend();
                }
                this.registerCamera(lifecycleCamera);
                return lifecycleCamera;
            }
            throw new IllegalArgumentException("Trying to create LifecycleCamera with destroyed lifecycle.");
        }
    }
    
    @Nullable
    LifecycleCamera getLifecycleCamera(final LifecycleOwner lifecycleOwner, final CameraUseCaseAdapter.CameraId cameraId) {
        synchronized (this.mLock) {
            return this.mCameraMap.get(Key.create(lifecycleOwner, cameraId));
        }
    }
    
    Collection<LifecycleCamera> getLifecycleCameras() {
        synchronized (this.mLock) {
            return Collections.unmodifiableCollection((Collection<? extends LifecycleCamera>)this.mCameraMap.values());
        }
    }
    
    void setActive(final LifecycleOwner e) {
        synchronized (this.mLock) {
            if (!this.hasUseCaseBound(e)) {
                return;
            }
            if (this.mActiveLifecycleOwners.isEmpty()) {
                this.mActiveLifecycleOwners.push(e);
            }
            else {
                final LifecycleOwner obj = this.mActiveLifecycleOwners.peek();
                if (!e.equals(obj)) {
                    this.suspendUseCases(obj);
                    this.mActiveLifecycleOwners.remove(e);
                    this.mActiveLifecycleOwners.push(e);
                }
            }
            this.unsuspendUseCases(e);
        }
    }
    
    void setInactive(final LifecycleOwner o) {
        synchronized (this.mLock) {
            this.mActiveLifecycleOwners.remove(o);
            this.suspendUseCases(o);
            if (!this.mActiveLifecycleOwners.isEmpty()) {
                this.unsuspendUseCases(this.mActiveLifecycleOwners.peek());
            }
        }
    }
    
    void unbind(@NonNull final Collection<UseCase> collection) {
        synchronized (this.mLock) {
            final Iterator<Key> iterator = this.mCameraMap.keySet().iterator();
            while (iterator.hasNext()) {
                final LifecycleCamera lifecycleCamera = this.mCameraMap.get(iterator.next());
                final boolean b = !lifecycleCamera.getUseCases().isEmpty();
                lifecycleCamera.unbind(collection);
                if (b && lifecycleCamera.getUseCases().isEmpty()) {
                    this.setInactive(lifecycleCamera.getLifecycleOwner());
                }
            }
        }
    }
    
    void unbindAll() {
        synchronized (this.mLock) {
            final Iterator<Key> iterator = this.mCameraMap.keySet().iterator();
            while (iterator.hasNext()) {
                final LifecycleCamera lifecycleCamera = this.mCameraMap.get(iterator.next());
                lifecycleCamera.unbindAll();
                this.setInactive(lifecycleCamera.getLifecycleOwner());
            }
        }
    }
    
    void unregisterLifecycle(final LifecycleOwner inactive) {
        synchronized (this.mLock) {
            final LifecycleCameraRepositoryObserver lifecycleCameraRepositoryObserver = this.getLifecycleCameraRepositoryObserver(inactive);
            if (lifecycleCameraRepositoryObserver == null) {
                return;
            }
            this.setInactive(inactive);
            final Iterator iterator = this.mLifecycleObserverMap.get(lifecycleCameraRepositoryObserver).iterator();
            while (iterator.hasNext()) {
                this.mCameraMap.remove(iterator.next());
            }
            this.mLifecycleObserverMap.remove(lifecycleCameraRepositoryObserver);
            lifecycleCameraRepositoryObserver.getLifecycleOwner().getLifecycle().removeObserver(lifecycleCameraRepositoryObserver);
        }
    }
    
    abstract static class Key
    {
        static Key create(@NonNull final LifecycleOwner lifecycleOwner, @NonNull final CameraUseCaseAdapter.CameraId cameraId) {
            return (Key)new AutoValue_LifecycleCameraRepository_Key(lifecycleOwner, cameraId);
        }
        
        @NonNull
        public abstract CameraUseCaseAdapter.CameraId getCameraId();
        
        @NonNull
        public abstract LifecycleOwner getLifecycleOwner();
    }
    
    private static class LifecycleCameraRepositoryObserver implements LifecycleObserver
    {
        private final LifecycleCameraRepository mLifecycleCameraRepository;
        private final LifecycleOwner mLifecycleOwner;
        
        LifecycleCameraRepositoryObserver(final LifecycleOwner mLifecycleOwner, final LifecycleCameraRepository mLifecycleCameraRepository) {
            this.mLifecycleOwner = mLifecycleOwner;
            this.mLifecycleCameraRepository = mLifecycleCameraRepository;
        }
        
        LifecycleOwner getLifecycleOwner() {
            return this.mLifecycleOwner;
        }
        
        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        public void onDestroy(final LifecycleOwner lifecycleOwner) {
            this.mLifecycleCameraRepository.unregisterLifecycle(lifecycleOwner);
        }
        
        @OnLifecycleEvent(Lifecycle.Event.ON_START)
        public void onStart(final LifecycleOwner active) {
            this.mLifecycleCameraRepository.setActive(active);
        }
        
        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        public void onStop(final LifecycleOwner inactive) {
            this.mLifecycleCameraRepository.setInactive(inactive);
        }
    }
}

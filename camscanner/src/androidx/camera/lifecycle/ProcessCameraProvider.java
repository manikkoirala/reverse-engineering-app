// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.lifecycle;

import androidx.annotation.RestrictTo;
import androidx.camera.core.CameraInfoUnavailableException;
import java.util.ArrayList;
import androidx.camera.core.CameraInfo;
import java.util.Collections;
import androidx.camera.core.impl.CameraInternal;
import java.util.LinkedHashSet;
import java.util.Iterator;
import androidx.camera.core.impl.CameraConfig;
import java.util.Collection;
import java.util.Arrays;
import androidx.camera.core.impl.ExtendedCameraConfigProviderStore;
import androidx.camera.core.internal.CameraUseCaseAdapter;
import androidx.camera.core.CameraFilter;
import androidx.camera.core.impl.utils.Threads;
import androidx.camera.core.CameraEffect;
import java.util.List;
import androidx.camera.core.ViewPort;
import androidx.annotation.MainThread;
import androidx.camera.core.UseCase;
import androidx.camera.core.Camera;
import androidx.camera.core.UseCaseGroup;
import androidx.camera.core.CameraSelector;
import androidx.lifecycle.LifecycleOwner;
import androidx.annotation.Nullable;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import androidx.camera.core.impl.utils.futures.AsyncFunction;
import androidx.camera.core.impl.utils.futures.FutureChain;
import androidx.camera.core.impl.utils.ContextUtil;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import androidx.arch.core.util.Function;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.core.util.Preconditions;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.utils.futures.Futures;
import android.content.Context;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.annotation.GuardedBy;
import androidx.camera.core.CameraXConfig;
import androidx.camera.core.CameraX;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class ProcessCameraProvider implements LifecycleCameraProvider
{
    private static final ProcessCameraProvider sAppInstance;
    private CameraX mCameraX;
    @GuardedBy("mLock")
    private CameraXConfig.Provider mCameraXConfigProvider;
    @GuardedBy("mLock")
    private ListenableFuture<CameraX> mCameraXInitializeFuture;
    @GuardedBy("mLock")
    private ListenableFuture<Void> mCameraXShutdownFuture;
    private Context mContext;
    private final LifecycleCameraRepository mLifecycleCameraRepository;
    private final Object mLock;
    
    static {
        sAppInstance = new ProcessCameraProvider();
    }
    
    private ProcessCameraProvider() {
        this.mLock = new Object();
        this.mCameraXConfigProvider = null;
        this.mCameraXShutdownFuture = Futures.immediateFuture((Void)null);
        this.mLifecycleCameraRepository = new LifecycleCameraRepository();
    }
    
    @ExperimentalCameraProviderConfiguration
    public static void configureInstance(@NonNull final CameraXConfig cameraXConfig) {
        ProcessCameraProvider.sAppInstance.configureInstanceInternal(cameraXConfig);
    }
    
    private void configureInstanceInternal(@NonNull final CameraXConfig cameraXConfig) {
        synchronized (this.mLock) {
            Preconditions.checkNotNull(cameraXConfig);
            Preconditions.checkState(this.mCameraXConfigProvider == null, "CameraX has already been configured. To use a different configuration, shutdown() must be called.");
            this.mCameraXConfigProvider = new \u3007o00\u3007\u3007Oo(cameraXConfig);
        }
    }
    
    @NonNull
    public static ListenableFuture<ProcessCameraProvider> getInstance(@NonNull final Context context) {
        Preconditions.checkNotNull(context);
        return Futures.transform(ProcessCameraProvider.sAppInstance.getOrCreateCameraXInstance(context), (Function<? super CameraX, ? extends ProcessCameraProvider>)new \u3007o\u3007(context), CameraXExecutors.directExecutor());
    }
    
    private ListenableFuture<CameraX> getOrCreateCameraXInstance(@NonNull final Context context) {
        synchronized (this.mLock) {
            final ListenableFuture<CameraX> mCameraXInitializeFuture = this.mCameraXInitializeFuture;
            if (mCameraXInitializeFuture != null) {
                return mCameraXInitializeFuture;
            }
            return this.mCameraXInitializeFuture = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<CameraX>)new O8(this, new CameraX(context, this.mCameraXConfigProvider)));
        }
    }
    
    private void setCameraX(final CameraX mCameraX) {
        this.mCameraX = mCameraX;
    }
    
    private void setContext(final Context mContext) {
        this.mContext = mContext;
    }
    
    @MainThread
    @NonNull
    public Camera bindToLifecycle(@NonNull final LifecycleOwner lifecycleOwner, @NonNull final CameraSelector cameraSelector, @NonNull final UseCaseGroup useCaseGroup) {
        return this.bindToLifecycle(lifecycleOwner, cameraSelector, useCaseGroup.getViewPort(), useCaseGroup.getEffects(), (UseCase[])useCaseGroup.getUseCases().toArray(new UseCase[0]));
    }
    
    @NonNull
    Camera bindToLifecycle(@NonNull final LifecycleOwner lifecycleOwner, @NonNull final CameraSelector cameraSelector, @Nullable final ViewPort viewPort, @NonNull final List<CameraEffect> list, @NonNull final UseCase... a) {
        Threads.checkMainThread();
        final CameraSelector.Builder fromSelector = CameraSelector.Builder.fromSelector(cameraSelector);
        final int length = a.length;
        int n = 0;
        CameraConfig cameraConfig;
        while (true) {
            cameraConfig = null;
            if (n >= length) {
                break;
            }
            final CameraSelector cameraSelector2 = a[n].getCurrentConfig().getCameraSelector(null);
            if (cameraSelector2 != null) {
                final Iterator<Object> iterator = cameraSelector2.getCameraFilterSet().iterator();
                while (iterator.hasNext()) {
                    fromSelector.addCameraFilter(iterator.next());
                }
            }
            ++n;
        }
        final LinkedHashSet<CameraInternal> filter = fromSelector.build().filter(this.mCameraX.getCameraRepository().getCameras());
        if (filter.isEmpty()) {
            throw new IllegalArgumentException("Provided camera selector unable to resolve a camera for the given use case");
        }
        final LifecycleCamera lifecycleCamera = this.mLifecycleCameraRepository.getLifecycleCamera(lifecycleOwner, CameraUseCaseAdapter.generateCameraId(filter));
        final Collection<LifecycleCamera> lifecycleCameras = this.mLifecycleCameraRepository.getLifecycleCameras();
        for (final UseCase useCase : a) {
            for (final LifecycleCamera lifecycleCamera2 : lifecycleCameras) {
                if (lifecycleCamera2.isBound(useCase)) {
                    if (lifecycleCamera2 == lifecycleCamera) {
                        continue;
                    }
                    throw new IllegalStateException(String.format("Use case %s already bound to a different lifecycle.", useCase));
                }
            }
        }
        LifecycleCamera lifecycleCamera3;
        if ((lifecycleCamera3 = lifecycleCamera) == null) {
            lifecycleCamera3 = this.mLifecycleCameraRepository.createLifecycleCamera(lifecycleOwner, new CameraUseCaseAdapter(filter, this.mCameraX.getCameraDeviceSurfaceManager(), this.mCameraX.getDefaultConfigFactory()));
        }
        final Iterator<Object> iterator3 = cameraSelector.getCameraFilterSet().iterator();
        CameraConfig extendedConfig = cameraConfig;
        while (iterator3.hasNext()) {
            final CameraFilter cameraFilter = iterator3.next();
            if (cameraFilter.getIdentifier() != CameraFilter.DEFAULT_ID) {
                final CameraConfig config = ExtendedCameraConfigProviderStore.getConfigProvider(cameraFilter.getIdentifier()).getConfig(lifecycleCamera3.getCameraInfo(), this.mContext);
                if (config == null) {
                    continue;
                }
                if (extendedConfig != null) {
                    throw new IllegalArgumentException("Cannot apply multiple extended camera configs at the same time.");
                }
                extendedConfig = config;
            }
        }
        lifecycleCamera3.setExtendedConfig(extendedConfig);
        if (a.length == 0) {
            return lifecycleCamera3;
        }
        this.mLifecycleCameraRepository.bindToLifecycleCamera(lifecycleCamera3, viewPort, list, Arrays.asList(a));
        return lifecycleCamera3;
    }
    
    @MainThread
    @NonNull
    public Camera bindToLifecycle(@NonNull final LifecycleOwner lifecycleOwner, @NonNull final CameraSelector cameraSelector, @NonNull final UseCase... array) {
        return this.bindToLifecycle(lifecycleOwner, cameraSelector, null, Collections.emptyList(), array);
    }
    
    @NonNull
    @Override
    public List<CameraInfo> getAvailableCameraInfos() {
        final ArrayList list = new ArrayList();
        final Iterator<Object> iterator = this.mCameraX.getCameraRepository().getCameras().iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getCameraInfo());
        }
        return list;
    }
    
    @Override
    public boolean hasCamera(@NonNull final CameraSelector cameraSelector) throws CameraInfoUnavailableException {
        try {
            cameraSelector.select(this.mCameraX.getCameraRepository().getCameras());
            return true;
        }
        catch (final IllegalArgumentException ex) {
            return false;
        }
    }
    
    @Override
    public boolean isBound(@NonNull final UseCase useCase) {
        final Iterator<LifecycleCamera> iterator = this.mLifecycleCameraRepository.getLifecycleCameras().iterator();
        while (iterator.hasNext()) {
            if (iterator.next().isBound(useCase)) {
                return true;
            }
        }
        return false;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.TESTS })
    public ListenableFuture<Void> shutdown() {
        this.mLifecycleCameraRepository.clear();
        final CameraX mCameraX = this.mCameraX;
        ListenableFuture<Void> mCameraXShutdownFuture;
        if (mCameraX != null) {
            mCameraXShutdownFuture = mCameraX.shutdown();
        }
        else {
            mCameraXShutdownFuture = Futures.immediateFuture((Void)null);
        }
        synchronized (this.mLock) {
            this.mCameraXConfigProvider = null;
            this.mCameraXInitializeFuture = null;
            this.mCameraXShutdownFuture = mCameraXShutdownFuture;
            monitorexit(this.mLock);
            this.mCameraX = null;
            this.mContext = null;
            return mCameraXShutdownFuture;
        }
    }
    
    @MainThread
    @Override
    public void unbind(@NonNull final UseCase... a) {
        Threads.checkMainThread();
        this.mLifecycleCameraRepository.unbind(Arrays.asList(a));
    }
    
    @MainThread
    @Override
    public void unbindAll() {
        Threads.checkMainThread();
        this.mLifecycleCameraRepository.unbindAll();
    }
}

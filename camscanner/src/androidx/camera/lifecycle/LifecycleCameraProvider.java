// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.lifecycle;

import androidx.annotation.NonNull;
import androidx.camera.core.UseCase;
import androidx.annotation.RequiresApi;
import androidx.camera.core.CameraProvider;

@RequiresApi(21)
interface LifecycleCameraProvider extends CameraProvider
{
    boolean isBound(@NonNull final UseCase p0);
    
    void unbind(@NonNull final UseCase... p0);
    
    void unbindAll();
}

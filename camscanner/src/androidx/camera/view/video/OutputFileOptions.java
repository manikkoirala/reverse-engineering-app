// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view.video;

import androidx.annotation.RestrictTo;
import androidx.camera.core.VideoCapture;
import androidx.annotation.Nullable;
import java.io.File;
import androidx.core.util.Preconditions;
import android.os.Build$VERSION;
import android.os.ParcelFileDescriptor;
import android.content.ContentValues;
import android.net.Uri;
import androidx.annotation.NonNull;
import android.content.ContentResolver;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
@ExperimentalVideo
public abstract class OutputFileOptions
{
    private static final Metadata EMPTY_METADATA;
    
    static {
        EMPTY_METADATA = Metadata.builder().build();
    }
    
    OutputFileOptions() {
    }
    
    @NonNull
    public static Builder builder(@NonNull final ContentResolver contentResolver, @NonNull final Uri saveCollection, @NonNull final ContentValues contentValues) {
        return new AutoValue_OutputFileOptions.Builder().setMetadata(OutputFileOptions.EMPTY_METADATA).setContentResolver(contentResolver).setSaveCollection(saveCollection).setContentValues(contentValues);
    }
    
    @NonNull
    public static Builder builder(@NonNull final ParcelFileDescriptor fileDescriptor) {
        Preconditions.checkArgument(Build$VERSION.SDK_INT >= 26, (Object)"Using a ParcelFileDescriptor to record a video is only supported for Android 8.0 or above.");
        return new AutoValue_OutputFileOptions.Builder().setMetadata(OutputFileOptions.EMPTY_METADATA).setFileDescriptor(fileDescriptor);
    }
    
    @NonNull
    public static Builder builder(@NonNull final File file) {
        return new AutoValue_OutputFileOptions.Builder().setMetadata(OutputFileOptions.EMPTY_METADATA).setFile(file);
    }
    
    private boolean isSavingToFile() {
        return this.getFile() != null;
    }
    
    private boolean isSavingToFileDescriptor() {
        return this.getFileDescriptor() != null;
    }
    
    private boolean isSavingToMediaStore() {
        return this.getSaveCollection() != null && this.getContentResolver() != null && this.getContentValues() != null;
    }
    
    @Nullable
    abstract ContentResolver getContentResolver();
    
    @Nullable
    abstract ContentValues getContentValues();
    
    @Nullable
    abstract File getFile();
    
    @Nullable
    abstract ParcelFileDescriptor getFileDescriptor();
    
    @NonNull
    public abstract Metadata getMetadata();
    
    @Nullable
    abstract Uri getSaveCollection();
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public VideoCapture.OutputFileOptions toVideoCaptureOutputFileOptions() {
        VideoCapture.OutputFileOptions.Builder builder;
        if (this.isSavingToFile()) {
            builder = new VideoCapture.OutputFileOptions.Builder(Preconditions.checkNotNull(this.getFile()));
        }
        else if (this.isSavingToFileDescriptor()) {
            builder = new VideoCapture.OutputFileOptions.Builder(Preconditions.checkNotNull(this.getFileDescriptor()).getFileDescriptor());
        }
        else {
            Preconditions.checkState(this.isSavingToMediaStore());
            builder = new VideoCapture.OutputFileOptions.Builder(Preconditions.checkNotNull(this.getContentResolver()), Preconditions.checkNotNull(this.getSaveCollection()), Preconditions.checkNotNull(this.getContentValues()));
        }
        final VideoCapture.Metadata metadata = new VideoCapture.Metadata();
        metadata.location = this.getMetadata().getLocation();
        builder.setMetadata(metadata);
        return builder.build();
    }
    
    public abstract static class Builder
    {
        Builder() {
        }
        
        @NonNull
        public abstract OutputFileOptions build();
        
        abstract Builder setContentResolver(@Nullable final ContentResolver p0);
        
        abstract Builder setContentValues(@Nullable final ContentValues p0);
        
        abstract Builder setFile(@Nullable final File p0);
        
        abstract Builder setFileDescriptor(@Nullable final ParcelFileDescriptor p0);
        
        @NonNull
        public abstract Builder setMetadata(@NonNull final Metadata p0);
        
        abstract Builder setSaveCollection(@Nullable final Uri p0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view.video;

import androidx.annotation.Nullable;
import android.location.Location;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
@ExperimentalVideo
public abstract class Metadata
{
    Metadata() {
    }
    
    @NonNull
    public static Builder builder() {
        return (Builder)new AutoValue_Metadata.Builder();
    }
    
    @Nullable
    public abstract Location getLocation();
    
    public abstract static class Builder
    {
        Builder() {
        }
        
        @NonNull
        public abstract Metadata build();
        
        @NonNull
        public abstract Builder setLocation(@Nullable final Location p0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view.video;

import androidx.annotation.Nullable;
import android.location.Location;

final class AutoValue_Metadata extends Metadata
{
    private final Location location;
    
    private AutoValue_Metadata(@Nullable final Location location) {
        this.location = location;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean equals = true;
        if (o == this) {
            return true;
        }
        if (o instanceof Metadata) {
            final Metadata metadata = (Metadata)o;
            final Location location = this.location;
            final Location location2 = metadata.getLocation();
            if (location == null) {
                if (location2 != null) {
                    equals = false;
                }
            }
            else {
                equals = location.equals((Object)location2);
            }
            return equals;
        }
        return false;
    }
    
    @Nullable
    @Override
    public Location getLocation() {
        return this.location;
    }
    
    @Override
    public int hashCode() {
        final Location location = this.location;
        int hashCode;
        if (location == null) {
            hashCode = 0;
        }
        else {
            hashCode = location.hashCode();
        }
        return hashCode ^ 0xF4243;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Metadata{location=");
        sb.append(this.location);
        sb.append("}");
        return sb.toString();
    }
    
    static final class Builder extends Metadata.Builder
    {
        private Location location;
        
        @Override
        public Metadata build() {
            return new AutoValue_Metadata(this.location, null);
        }
        
        @Override
        public Metadata.Builder setLocation(@Nullable final Location location) {
            this.location = location;
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view.video;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import java.io.File;
import android.content.ContentValues;
import android.content.ContentResolver;

final class AutoValue_OutputFileOptions extends OutputFileOptions
{
    private final ContentResolver contentResolver;
    private final ContentValues contentValues;
    private final File file;
    private final ParcelFileDescriptor fileDescriptor;
    private final Metadata metadata;
    private final Uri saveCollection;
    
    private AutoValue_OutputFileOptions(@Nullable final File file, @Nullable final ParcelFileDescriptor fileDescriptor, @Nullable final ContentResolver contentResolver, @Nullable final Uri saveCollection, @Nullable final ContentValues contentValues, final Metadata metadata) {
        this.file = file;
        this.fileDescriptor = fileDescriptor;
        this.contentResolver = contentResolver;
        this.saveCollection = saveCollection;
        this.contentValues = contentValues;
        this.metadata = metadata;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof OutputFileOptions) {
            final OutputFileOptions outputFileOptions = (OutputFileOptions)o;
            final File file = this.file;
            if (file == null) {
                if (outputFileOptions.getFile() != null) {
                    return false;
                }
            }
            else if (!file.equals(outputFileOptions.getFile())) {
                return false;
            }
            final ParcelFileDescriptor fileDescriptor = this.fileDescriptor;
            if (fileDescriptor == null) {
                if (outputFileOptions.getFileDescriptor() != null) {
                    return false;
                }
            }
            else if (!fileDescriptor.equals(outputFileOptions.getFileDescriptor())) {
                return false;
            }
            final ContentResolver contentResolver = this.contentResolver;
            if (contentResolver == null) {
                if (outputFileOptions.getContentResolver() != null) {
                    return false;
                }
            }
            else if (!contentResolver.equals(outputFileOptions.getContentResolver())) {
                return false;
            }
            final Uri saveCollection = this.saveCollection;
            if (saveCollection == null) {
                if (outputFileOptions.getSaveCollection() != null) {
                    return false;
                }
            }
            else if (!saveCollection.equals((Object)outputFileOptions.getSaveCollection())) {
                return false;
            }
            final ContentValues contentValues = this.contentValues;
            if (contentValues == null) {
                if (outputFileOptions.getContentValues() != null) {
                    return false;
                }
            }
            else if (!contentValues.equals((Object)outputFileOptions.getContentValues())) {
                return false;
            }
            if (this.metadata.equals(outputFileOptions.getMetadata())) {
                return b;
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Nullable
    @Override
    ContentResolver getContentResolver() {
        return this.contentResolver;
    }
    
    @Nullable
    @Override
    ContentValues getContentValues() {
        return this.contentValues;
    }
    
    @Nullable
    @Override
    File getFile() {
        return this.file;
    }
    
    @Nullable
    @Override
    ParcelFileDescriptor getFileDescriptor() {
        return this.fileDescriptor;
    }
    
    @NonNull
    @Override
    public Metadata getMetadata() {
        return this.metadata;
    }
    
    @Nullable
    @Override
    Uri getSaveCollection() {
        return this.saveCollection;
    }
    
    @Override
    public int hashCode() {
        final File file = this.file;
        int hashCode = 0;
        int hashCode2;
        if (file == null) {
            hashCode2 = 0;
        }
        else {
            hashCode2 = file.hashCode();
        }
        final ParcelFileDescriptor fileDescriptor = this.fileDescriptor;
        int hashCode3;
        if (fileDescriptor == null) {
            hashCode3 = 0;
        }
        else {
            hashCode3 = fileDescriptor.hashCode();
        }
        final ContentResolver contentResolver = this.contentResolver;
        int hashCode4;
        if (contentResolver == null) {
            hashCode4 = 0;
        }
        else {
            hashCode4 = contentResolver.hashCode();
        }
        final Uri saveCollection = this.saveCollection;
        int hashCode5;
        if (saveCollection == null) {
            hashCode5 = 0;
        }
        else {
            hashCode5 = saveCollection.hashCode();
        }
        final ContentValues contentValues = this.contentValues;
        if (contentValues != null) {
            hashCode = contentValues.hashCode();
        }
        return (((((hashCode2 ^ 0xF4243) * 1000003 ^ hashCode3) * 1000003 ^ hashCode4) * 1000003 ^ hashCode5) * 1000003 ^ hashCode) * 1000003 ^ this.metadata.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("OutputFileOptions{file=");
        sb.append(this.file);
        sb.append(", fileDescriptor=");
        sb.append(this.fileDescriptor);
        sb.append(", contentResolver=");
        sb.append(this.contentResolver);
        sb.append(", saveCollection=");
        sb.append(this.saveCollection);
        sb.append(", contentValues=");
        sb.append(this.contentValues);
        sb.append(", metadata=");
        sb.append(this.metadata);
        sb.append("}");
        return sb.toString();
    }
    
    static final class Builder extends OutputFileOptions.Builder
    {
        private ContentResolver contentResolver;
        private ContentValues contentValues;
        private File file;
        private ParcelFileDescriptor fileDescriptor;
        private Metadata metadata;
        private Uri saveCollection;
        
        @Override
        public OutputFileOptions build() {
            final Metadata metadata = this.metadata;
            String string = "";
            if (metadata == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(" metadata");
                string = sb.toString();
            }
            if (string.isEmpty()) {
                return new AutoValue_OutputFileOptions(this.file, this.fileDescriptor, this.contentResolver, this.saveCollection, this.contentValues, this.metadata, null);
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Missing required properties:");
            sb2.append(string);
            throw new IllegalStateException(sb2.toString());
        }
        
        @Override
        OutputFileOptions.Builder setContentResolver(@Nullable final ContentResolver contentResolver) {
            this.contentResolver = contentResolver;
            return this;
        }
        
        @Override
        OutputFileOptions.Builder setContentValues(@Nullable final ContentValues contentValues) {
            this.contentValues = contentValues;
            return this;
        }
        
        @Override
        OutputFileOptions.Builder setFile(@Nullable final File file) {
            this.file = file;
            return this;
        }
        
        @Override
        OutputFileOptions.Builder setFileDescriptor(@Nullable final ParcelFileDescriptor fileDescriptor) {
            this.fileDescriptor = fileDescriptor;
            return this;
        }
        
        @Override
        public OutputFileOptions.Builder setMetadata(final Metadata metadata) {
            if (metadata != null) {
                this.metadata = metadata;
                return this;
            }
            throw new NullPointerException("Null metadata");
        }
        
        @Override
        OutputFileOptions.Builder setSaveCollection(@Nullable final Uri saveCollection) {
            this.saveCollection = saveCollection;
            return this;
        }
    }
}

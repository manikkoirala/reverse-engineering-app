// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view.video;

import androidx.annotation.RestrictTo;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
@ExperimentalVideo
public interface OnVideoSavedCallback
{
    public static final int ERROR_ENCODER = 1;
    public static final int ERROR_FILE_IO = 4;
    public static final int ERROR_INVALID_CAMERA = 5;
    public static final int ERROR_MUXER = 2;
    public static final int ERROR_RECORDING_IN_PROGRESS = 3;
    public static final int ERROR_UNKNOWN = 0;
    
    void onError(final int p0, @NonNull final String p1, @Nullable final Throwable p2);
    
    void onVideoSaved(@NonNull final OutputFileResults p0);
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface VideoCaptureError {
    }
}

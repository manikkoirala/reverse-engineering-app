// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view.video;

import androidx.annotation.RestrictTo;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.net.Uri;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
@ExperimentalVideo
public abstract class OutputFileResults
{
    OutputFileResults() {
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static OutputFileResults create(@Nullable final Uri uri) {
        return new AutoValue_OutputFileResults(uri);
    }
    
    @Nullable
    public abstract Uri getSavedUri();
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view;

import android.view.ScaleGestureDetector$SimpleOnScaleGestureListener;
import androidx.annotation.RestrictTo;
import androidx.annotation.OptIn;
import android.view.ViewConfiguration;
import android.annotation.SuppressLint;
import android.util.Rational;
import androidx.lifecycle.LiveData;
import android.graphics.Rect;
import android.graphics.Matrix;
import androidx.camera.core.impl.utils.TransformUtils;
import android.util.Size;
import androidx.camera.view.transform.OutputTransform;
import androidx.camera.core.MeteringPointFactory;
import android.graphics.Bitmap;
import android.hardware.display.DisplayManager$DisplayListener;
import android.os.Handler;
import android.os.Looper;
import android.os.Build$VERSION;
import androidx.camera.view.internal.compat.quirk.SurfaceViewNotCroppedByParentQuirk;
import androidx.camera.view.internal.compat.quirk.DeviceQuirks;
import androidx.camera.view.internal.compat.quirk.SurfaceViewStretchedQuirk;
import android.hardware.display.DisplayManager;
import androidx.annotation.MainThread;
import androidx.camera.core.ViewPort;
import android.view.Display;
import android.content.res.TypedArray;
import android.content.res.Resources$Theme;
import android.view.ScaleGestureDetector$OnScaleGestureListener;
import android.view.View;
import androidx.core.view.ViewCompat;
import androidx.annotation.AnyThread;
import androidx.core.content.ContextCompat;
import androidx.camera.core.impl.utils.Threads;
import androidx.camera.core.impl.Observable;
import androidx.camera.core.Logger;
import androidx.camera.core.impl.CameraInternal;
import androidx.camera.core.SurfaceRequest;
import androidx.annotation.UiThread;
import android.util.AttributeSet;
import android.content.Context;
import android.view.MotionEvent;
import androidx.camera.core.Preview;
import android.view.ScaleGestureDetector;
import androidx.lifecycle.MutableLiveData;
import android.view.View$OnLayoutChangeListener;
import java.util.concurrent.Executor;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.NonNull;
import androidx.camera.core.impl.CameraInfoInternal;
import androidx.annotation.Nullable;
import java.util.concurrent.atomic.AtomicReference;
import androidx.annotation.ColorRes;
import androidx.annotation.RequiresApi;
import android.widget.FrameLayout;

@RequiresApi(21)
public final class PreviewView extends FrameLayout
{
    @ColorRes
    static final int DEFAULT_BACKGROUND_COLOR = 17170444;
    private static final ImplementationMode DEFAULT_IMPL_MODE;
    private static final String TAG = "PreviewView";
    @Nullable
    final AtomicReference<PreviewStreamStateObserver> mActiveStreamStateObserver;
    CameraController mCameraController;
    @Nullable
    CameraInfoInternal mCameraInfoInternal;
    @NonNull
    private final DisplayRotationListener mDisplayRotationListener;
    @Nullable
    @VisibleForTesting
    PreviewViewImplementation mImplementation;
    @NonNull
    ImplementationMode mImplementationMode;
    @Nullable
    OnFrameUpdateListener mOnFrameUpdateListener;
    @Nullable
    Executor mOnFrameUpdateListenerExecutor;
    private final View$OnLayoutChangeListener mOnLayoutChangeListener;
    @NonNull
    final MutableLiveData<StreamState> mPreviewStreamStateLiveData;
    @NonNull
    final PreviewTransformation mPreviewTransform;
    @NonNull
    PreviewViewMeteringPointFactory mPreviewViewMeteringPointFactory;
    @NonNull
    private final ScaleGestureDetector mScaleGestureDetector;
    final Preview.SurfaceProvider mSurfaceProvider;
    @Nullable
    private MotionEvent mTouchUpEvent;
    boolean mUseDisplayRotation;
    
    static {
        DEFAULT_IMPL_MODE = ImplementationMode.PERFORMANCE;
    }
    
    @UiThread
    public PreviewView(@NonNull final Context context) {
        this(context, null);
    }
    
    @UiThread
    public PreviewView(@NonNull final Context context, @Nullable final AttributeSet set) {
        this(context, set, 0);
    }
    
    @UiThread
    public PreviewView(@NonNull final Context context, @Nullable final AttributeSet set, final int n) {
        this(context, set, n, 0);
    }
    
    @UiThread
    public PreviewView(@NonNull final Context context, @Nullable final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        final ImplementationMode default_IMPL_MODE = PreviewView.DEFAULT_IMPL_MODE;
        this.mImplementationMode = default_IMPL_MODE;
        final PreviewTransformation mPreviewTransform = new PreviewTransformation();
        this.mPreviewTransform = mPreviewTransform;
        this.mUseDisplayRotation = true;
        this.mPreviewStreamStateLiveData = new MutableLiveData<StreamState>(StreamState.IDLE);
        this.mActiveStreamStateObserver = new AtomicReference<PreviewStreamStateObserver>();
        this.mPreviewViewMeteringPointFactory = new PreviewViewMeteringPointFactory(mPreviewTransform);
        this.mDisplayRotationListener = new DisplayRotationListener();
        this.mOnLayoutChangeListener = (View$OnLayoutChangeListener)new \u300780\u3007808\u3007O(this);
        this.mSurfaceProvider = new Preview.SurfaceProvider() {
            final PreviewView this$0;
            
            @AnyThread
            @Override
            public void onSurfaceRequested(@NonNull final SurfaceRequest surfaceRequest) {
                if (!Threads.isMainThread()) {
                    ContextCompat.getMainExecutor(((View)this.this$0).getContext()).execute(new \u30078o8o\u3007(this, surfaceRequest));
                    return;
                }
                Logger.d("PreviewView", "Surface requested by Preview.");
                final CameraInternal camera = surfaceRequest.getCamera();
                this.this$0.mCameraInfoInternal = camera.getCameraInfoInternal();
                surfaceRequest.setTransformationInfoListener(ContextCompat.getMainExecutor(((View)this.this$0).getContext()), (SurfaceRequest.TransformationInfoListener)new \u3007O8o08O(this, camera, surfaceRequest));
                final PreviewView this$0 = this.this$0;
                PreviewViewImplementation mImplementation;
                if (PreviewView.shouldUseTextureView(surfaceRequest, this$0.mImplementationMode)) {
                    final PreviewView this$2 = this.this$0;
                    mImplementation = new TextureViewImplementation(this$2, this$2.mPreviewTransform);
                }
                else {
                    final PreviewView this$3 = this.this$0;
                    mImplementation = new SurfaceViewImplementation(this$3, this$3.mPreviewTransform);
                }
                this$0.mImplementation = mImplementation;
                final CameraInfoInternal cameraInfoInternal = camera.getCameraInfoInternal();
                final PreviewView this$4 = this.this$0;
                final PreviewStreamStateObserver newValue = new PreviewStreamStateObserver(cameraInfoInternal, this$4.mPreviewStreamStateLiveData, this$4.mImplementation);
                this.this$0.mActiveStreamStateObserver.set(newValue);
                camera.getCameraState().addObserver(ContextCompat.getMainExecutor(((View)this.this$0).getContext()), (Observable.Observer<? super CameraInternal.State>)newValue);
                this.this$0.mImplementation.onSurfaceRequested(surfaceRequest, (PreviewViewImplementation.OnSurfaceNotInUseListener)new OO0o\u3007\u3007(this, newValue, camera));
                final PreviewView this$5 = this.this$0;
                final OnFrameUpdateListener mOnFrameUpdateListener = this$5.mOnFrameUpdateListener;
                if (mOnFrameUpdateListener != null) {
                    final Executor mOnFrameUpdateListenerExecutor = this$5.mOnFrameUpdateListenerExecutor;
                    if (mOnFrameUpdateListenerExecutor != null) {
                        this$5.mImplementation.setFrameUpdateListener(mOnFrameUpdateListenerExecutor, mOnFrameUpdateListener);
                    }
                }
            }
        };
        Threads.checkMainThread();
        final Resources$Theme theme = context.getTheme();
        final int[] previewView = R.styleable.PreviewView;
        final TypedArray obtainStyledAttributes = theme.obtainStyledAttributes(set, previewView, n, n2);
        ViewCompat.saveAttributeDataForStyleable((View)this, context, previewView, set, obtainStyledAttributes, n, n2);
        try {
            this.setScaleType(ScaleType.fromId(obtainStyledAttributes.getInteger(R.styleable.PreviewView_scaleType, mPreviewTransform.getScaleType().getId())));
            this.setImplementationMode(ImplementationMode.fromId(obtainStyledAttributes.getInteger(R.styleable.PreviewView_implementationMode, default_IMPL_MODE.getId())));
            obtainStyledAttributes.recycle();
            this.mScaleGestureDetector = new ScaleGestureDetector(context, (ScaleGestureDetector$OnScaleGestureListener)new PinchToZoomOnScaleGestureListener());
            if (((View)this).getBackground() == null) {
                ((View)this).setBackgroundColor(ContextCompat.getColor(((View)this).getContext(), 17170444));
            }
        }
        finally {
            obtainStyledAttributes.recycle();
        }
    }
    
    @MainThread
    private void attachToControllerIfReady(final boolean b) {
        Threads.checkMainThread();
        final Display display = ((View)this).getDisplay();
        final ViewPort viewPort = this.getViewPort();
        if (this.mCameraController != null && viewPort != null && ((View)this).isAttachedToWindow() && display != null) {
            try {
                this.mCameraController.attachPreviewSurface(this.getSurfaceProvider(), viewPort, display);
            }
            catch (final IllegalStateException ex) {
                if (!b) {
                    throw ex;
                }
                Logger.e("PreviewView", ex.toString(), ex);
            }
        }
    }
    
    @Nullable
    private DisplayManager getDisplayManager() {
        final Context context = ((View)this).getContext();
        if (context == null) {
            return null;
        }
        return (DisplayManager)context.getApplicationContext().getSystemService("display");
    }
    
    private int getViewPortScaleType() {
        switch (PreviewView$2.$SwitchMap$androidx$camera$view$PreviewView$ScaleType[this.getScaleType().ordinal()]) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unexpected scale type: ");
                sb.append(this.getScaleType());
                throw new IllegalStateException(sb.toString());
            }
            case 4:
            case 5:
            case 6: {
                return 3;
            }
            case 3: {
                return 0;
            }
            case 2: {
                return 1;
            }
            case 1: {
                return 2;
            }
        }
    }
    
    static boolean shouldUseTextureView(@NonNull final SurfaceRequest surfaceRequest, @NonNull final ImplementationMode obj) {
        final boolean equals = surfaceRequest.getCamera().getCameraInfoInternal().getImplementationType().equals("androidx.camera.camera2.legacy");
        final boolean b = DeviceQuirks.get(SurfaceViewStretchedQuirk.class) != null || DeviceQuirks.get(SurfaceViewNotCroppedByParentQuirk.class) != null;
        if (!surfaceRequest.isRGBA8888Required() && Build$VERSION.SDK_INT > 24 && !equals) {
            if (!b) {
                final int n = PreviewView$2.$SwitchMap$androidx$camera$view$PreviewView$ImplementationMode[obj.ordinal()];
                if (n != 1) {
                    if (n == 2) {
                        return false;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Invalid implementation mode: ");
                    sb.append(obj);
                    throw new IllegalArgumentException(sb.toString());
                }
            }
        }
        return true;
    }
    
    private void startListeningToDisplayChange() {
        final DisplayManager displayManager = this.getDisplayManager();
        if (displayManager == null) {
            return;
        }
        displayManager.registerDisplayListener((DisplayManager$DisplayListener)this.mDisplayRotationListener, new Handler(Looper.getMainLooper()));
    }
    
    private void stopListeningToDisplayChange() {
        final DisplayManager displayManager = this.getDisplayManager();
        if (displayManager == null) {
            return;
        }
        displayManager.unregisterDisplayListener((DisplayManager$DisplayListener)this.mDisplayRotationListener);
    }
    
    @Nullable
    @UiThread
    public Bitmap getBitmap() {
        Threads.checkMainThread();
        final PreviewViewImplementation mImplementation = this.mImplementation;
        Bitmap bitmap;
        if (mImplementation == null) {
            bitmap = null;
        }
        else {
            bitmap = mImplementation.getBitmap();
        }
        return bitmap;
    }
    
    @Nullable
    @UiThread
    public CameraController getController() {
        Threads.checkMainThread();
        return this.mCameraController;
    }
    
    @NonNull
    @UiThread
    public ImplementationMode getImplementationMode() {
        Threads.checkMainThread();
        return this.mImplementationMode;
    }
    
    @NonNull
    @UiThread
    public MeteringPointFactory getMeteringPointFactory() {
        Threads.checkMainThread();
        return this.mPreviewViewMeteringPointFactory;
    }
    
    @Nullable
    @TransformExperimental
    public OutputTransform getOutputTransform() {
        Threads.checkMainThread();
        Matrix surfaceToPreviewViewMatrix;
        try {
            surfaceToPreviewViewMatrix = this.mPreviewTransform.getSurfaceToPreviewViewMatrix(new Size(((View)this).getWidth(), ((View)this).getHeight()), ((View)this).getLayoutDirection());
        }
        catch (final IllegalStateException ex) {
            surfaceToPreviewViewMatrix = null;
        }
        final Rect surfaceCropRect = this.mPreviewTransform.getSurfaceCropRect();
        if (surfaceToPreviewViewMatrix != null && surfaceCropRect != null) {
            surfaceToPreviewViewMatrix.preConcat(TransformUtils.getNormalizedToBuffer(surfaceCropRect));
            if (this.mImplementation instanceof TextureViewImplementation) {
                surfaceToPreviewViewMatrix.postConcat(((View)this).getMatrix());
            }
            else {
                Logger.w("PreviewView", "PreviewView needs to be in COMPATIBLE mode for the transform to work correctly.");
            }
            return new OutputTransform(surfaceToPreviewViewMatrix, new Size(surfaceCropRect.width(), surfaceCropRect.height()));
        }
        Logger.d("PreviewView", "Transform info is not ready");
        return null;
    }
    
    @NonNull
    public LiveData<StreamState> getPreviewStreamState() {
        return this.mPreviewStreamStateLiveData;
    }
    
    @NonNull
    @UiThread
    public ScaleType getScaleType() {
        Threads.checkMainThread();
        return this.mPreviewTransform.getScaleType();
    }
    
    @NonNull
    @UiThread
    public Preview.SurfaceProvider getSurfaceProvider() {
        Threads.checkMainThread();
        return this.mSurfaceProvider;
    }
    
    @Nullable
    @UiThread
    public ViewPort getViewPort() {
        Threads.checkMainThread();
        if (((View)this).getDisplay() == null) {
            return null;
        }
        return this.getViewPort(((View)this).getDisplay().getRotation());
    }
    
    @SuppressLint({ "WrongConstant" })
    @Nullable
    @UiThread
    public ViewPort getViewPort(final int n) {
        Threads.checkMainThread();
        if (((View)this).getWidth() != 0 && ((View)this).getHeight() != 0) {
            return new ViewPort.Builder(new Rational(((View)this).getWidth(), ((View)this).getHeight()), n).setScaleType(this.getViewPortScaleType()).setLayoutDirection(((View)this).getLayoutDirection()).build();
        }
        return null;
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.updateDisplayRotationIfNeeded();
        this.startListeningToDisplayChange();
        ((View)this).addOnLayoutChangeListener(this.mOnLayoutChangeListener);
        final PreviewViewImplementation mImplementation = this.mImplementation;
        if (mImplementation != null) {
            mImplementation.onAttachedToWindow();
        }
        this.attachToControllerIfReady(true);
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        ((View)this).removeOnLayoutChangeListener(this.mOnLayoutChangeListener);
        final PreviewViewImplementation mImplementation = this.mImplementation;
        if (mImplementation != null) {
            mImplementation.onDetachedFromWindow();
        }
        final CameraController mCameraController = this.mCameraController;
        if (mCameraController != null) {
            mCameraController.clearPreviewSurface();
        }
        this.stopListeningToDisplayChange();
    }
    
    public boolean onTouchEvent(@NonNull final MotionEvent mTouchUpEvent) {
        if (this.mCameraController == null) {
            return super.onTouchEvent(mTouchUpEvent);
        }
        final int pointerCount = mTouchUpEvent.getPointerCount();
        boolean b = false;
        final boolean b2 = pointerCount == 1;
        final boolean b3 = mTouchUpEvent.getAction() == 1;
        final boolean b4 = mTouchUpEvent.getEventTime() - mTouchUpEvent.getDownTime() < ViewConfiguration.getLongPressTimeout();
        if (b2 && b3 && b4) {
            this.mTouchUpEvent = mTouchUpEvent;
            this.performClick();
            return true;
        }
        if (this.mScaleGestureDetector.onTouchEvent(mTouchUpEvent) || super.onTouchEvent(mTouchUpEvent)) {
            b = true;
        }
        return b;
    }
    
    public boolean performClick() {
        if (this.mCameraController != null) {
            final MotionEvent mTouchUpEvent = this.mTouchUpEvent;
            float x;
            if (mTouchUpEvent != null) {
                x = mTouchUpEvent.getX();
            }
            else {
                x = ((View)this).getWidth() / 2.0f;
            }
            final MotionEvent mTouchUpEvent2 = this.mTouchUpEvent;
            float y;
            if (mTouchUpEvent2 != null) {
                y = mTouchUpEvent2.getY();
            }
            else {
                y = ((View)this).getHeight() / 2.0f;
            }
            this.mCameraController.onTapToFocus(this.mPreviewViewMeteringPointFactory, x, y);
        }
        this.mTouchUpEvent = null;
        return super.performClick();
    }
    
    @MainThread
    @OptIn(markerClass = { TransformExperimental.class })
    void redrawPreview() {
        Threads.checkMainThread();
        final PreviewViewImplementation mImplementation = this.mImplementation;
        if (mImplementation != null) {
            mImplementation.redrawPreview();
        }
        this.mPreviewViewMeteringPointFactory.recalculate(new Size(((View)this).getWidth(), ((View)this).getHeight()), ((View)this).getLayoutDirection());
        final CameraController mCameraController = this.mCameraController;
        if (mCameraController != null) {
            mCameraController.updatePreviewViewTransform(this.getOutputTransform());
        }
    }
    
    @UiThread
    public void setController(@Nullable final CameraController mCameraController) {
        Threads.checkMainThread();
        final CameraController mCameraController2 = this.mCameraController;
        if (mCameraController2 != null && mCameraController2 != mCameraController) {
            mCameraController2.clearPreviewSurface();
        }
        this.mCameraController = mCameraController;
        this.attachToControllerIfReady(false);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void setFrameUpdateListener(@NonNull final Executor mOnFrameUpdateListenerExecutor, @NonNull final OnFrameUpdateListener mOnFrameUpdateListener) {
        if (this.mImplementationMode != ImplementationMode.PERFORMANCE) {
            this.mOnFrameUpdateListener = mOnFrameUpdateListener;
            this.mOnFrameUpdateListenerExecutor = mOnFrameUpdateListenerExecutor;
            final PreviewViewImplementation mImplementation = this.mImplementation;
            if (mImplementation != null) {
                mImplementation.setFrameUpdateListener(mOnFrameUpdateListenerExecutor, mOnFrameUpdateListener);
            }
            return;
        }
        throw new IllegalArgumentException("PERFORMANCE mode doesn't support frame update listener");
    }
    
    @UiThread
    public void setImplementationMode(@NonNull final ImplementationMode mImplementationMode) {
        Threads.checkMainThread();
        this.mImplementationMode = mImplementationMode;
        if (mImplementationMode == ImplementationMode.PERFORMANCE && this.mOnFrameUpdateListener != null) {
            throw new IllegalArgumentException("PERFORMANCE mode doesn't support frame update listener");
        }
    }
    
    @UiThread
    public void setScaleType(@NonNull final ScaleType scaleType) {
        Threads.checkMainThread();
        this.mPreviewTransform.setScaleType(scaleType);
        this.redrawPreview();
        this.attachToControllerIfReady(false);
    }
    
    void updateDisplayRotationIfNeeded() {
        if (this.mUseDisplayRotation) {
            final Display display = ((View)this).getDisplay();
            if (display != null) {
                final CameraInfoInternal mCameraInfoInternal = this.mCameraInfoInternal;
                if (mCameraInfoInternal != null) {
                    this.mPreviewTransform.overrideWithDisplayRotation(mCameraInfoInternal.getSensorRotationDegrees(display.getRotation()), display.getRotation());
                }
            }
        }
    }
    
    class DisplayRotationListener implements DisplayManager$DisplayListener
    {
        final PreviewView this$0;
        
        DisplayRotationListener(final PreviewView this$0) {
            this.this$0 = this$0;
        }
        
        public void onDisplayAdded(final int n) {
        }
        
        public void onDisplayChanged(final int n) {
            final Display display = ((View)this.this$0).getDisplay();
            if (display != null && display.getDisplayId() == n) {
                this.this$0.updateDisplayRotationIfNeeded();
                this.this$0.redrawPreview();
            }
        }
        
        public void onDisplayRemoved(final int n) {
        }
    }
    
    @RequiresApi(21)
    public enum ImplementationMode
    {
        private static final ImplementationMode[] $VALUES;
        
        COMPATIBLE(1), 
        PERFORMANCE(0);
        
        private final int mId;
        
        private ImplementationMode(final int mId) {
            this.mId = mId;
        }
        
        static ImplementationMode fromId(final int i) {
            for (final ImplementationMode implementationMode : values()) {
                if (implementationMode.mId == i) {
                    return implementationMode;
                }
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Unknown implementation mode id ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        }
        
        int getId() {
            return this.mId;
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public interface OnFrameUpdateListener
    {
        void onFrameUpdate(final long p0);
    }
    
    class PinchToZoomOnScaleGestureListener extends ScaleGestureDetector$SimpleOnScaleGestureListener
    {
        final PreviewView this$0;
        
        PinchToZoomOnScaleGestureListener(final PreviewView this$0) {
            this.this$0 = this$0;
        }
        
        public boolean onScale(final ScaleGestureDetector scaleGestureDetector) {
            final CameraController mCameraController = this.this$0.mCameraController;
            if (mCameraController != null) {
                mCameraController.onPinchToZoom(scaleGestureDetector.getScaleFactor());
            }
            return true;
        }
    }
    
    @RequiresApi(21)
    public enum ScaleType
    {
        private static final ScaleType[] $VALUES;
        
        FILL_CENTER(1), 
        FILL_END(2), 
        FILL_START(0), 
        FIT_CENTER(4), 
        FIT_END(5), 
        FIT_START(3);
        
        private final int mId;
        
        private ScaleType(final int mId) {
            this.mId = mId;
        }
        
        static ScaleType fromId(final int i) {
            for (final ScaleType scaleType : values()) {
                if (scaleType.mId == i) {
                    return scaleType;
                }
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Unknown scale type id ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        }
        
        int getId() {
            return this.mId;
        }
    }
    
    public enum StreamState
    {
        private static final StreamState[] $VALUES;
        
        IDLE, 
        STREAMING;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view;

public final class R
{
    private R() {
    }
    
    public static final class attr
    {
        public static final int implementationMode = 2130969363;
        public static final int scaleType = 2130969877;
        
        private attr() {
        }
    }
    
    public static final class id
    {
        public static final int compatible = 2131362977;
        public static final int fillCenter = 2131363305;
        public static final int fillEnd = 2131363306;
        public static final int fillStart = 2131363307;
        public static final int fitCenter = 2131363312;
        public static final int fitEnd = 2131363313;
        public static final int fitStart = 2131363314;
        public static final int performance = 2131365497;
        
        private id() {
        }
    }
    
    public static final class styleable
    {
        public static final int[] PreviewView;
        public static final int PreviewView_implementationMode = 0;
        public static final int PreviewView_scaleType = 1;
        
        static {
            PreviewView = new int[] { 2130969363, 2130969877 };
        }
        
        private styleable() {
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view;

import android.view.ViewGroup;
import android.util.Size;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.core.content.ContextCompat;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import android.view.TextureView$SurfaceTextureListener;
import android.view.ViewGroup$LayoutParams;
import android.widget.FrameLayout$LayoutParams;
import androidx.core.util.Preconditions;
import android.graphics.Bitmap;
import android.view.View;
import androidx.core.util.Consumer;
import java.util.Objects;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import androidx.camera.core.Logger;
import android.view.Surface;
import androidx.annotation.NonNull;
import android.widget.FrameLayout;
import android.view.TextureView;
import androidx.camera.core.SurfaceRequest;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.concurrent.futures.CallbackToFutureAdapter;
import java.util.concurrent.atomic.AtomicReference;
import androidx.annotation.Nullable;
import java.util.concurrent.Executor;
import android.graphics.SurfaceTexture;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
final class TextureViewImplementation extends PreviewViewImplementation
{
    private static final String TAG = "TextureViewImpl";
    SurfaceTexture mDetachedSurfaceTexture;
    @Nullable
    Executor mFrameUpdateExecutor;
    boolean mIsSurfaceTextureDetachedFromView;
    AtomicReference<CallbackToFutureAdapter.Completer<Void>> mNextFrameCompleter;
    @Nullable
    PreviewView.OnFrameUpdateListener mOnFrameUpdateListener;
    @Nullable
    OnSurfaceNotInUseListener mOnSurfaceNotInUseListener;
    ListenableFuture<SurfaceRequest.Result> mSurfaceReleaseFuture;
    SurfaceRequest mSurfaceRequest;
    SurfaceTexture mSurfaceTexture;
    TextureView mTextureView;
    
    TextureViewImplementation(@NonNull final FrameLayout frameLayout, @NonNull final PreviewTransformation previewTransformation) {
        super(frameLayout, previewTransformation);
        this.mIsSurfaceTextureDetachedFromView = false;
        this.mNextFrameCompleter = new AtomicReference<CallbackToFutureAdapter.Completer<Void>>();
    }
    
    private void notifySurfaceNotInUse() {
        final OnSurfaceNotInUseListener mOnSurfaceNotInUseListener = this.mOnSurfaceNotInUseListener;
        if (mOnSurfaceNotInUseListener != null) {
            mOnSurfaceNotInUseListener.onSurfaceNotInUse();
            this.mOnSurfaceNotInUseListener = null;
        }
    }
    
    private void reattachSurfaceTexture() {
        if (this.mIsSurfaceTextureDetachedFromView && this.mDetachedSurfaceTexture != null) {
            final SurfaceTexture surfaceTexture = this.mTextureView.getSurfaceTexture();
            final SurfaceTexture mDetachedSurfaceTexture = this.mDetachedSurfaceTexture;
            if (surfaceTexture != mDetachedSurfaceTexture) {
                this.mTextureView.setSurfaceTexture(mDetachedSurfaceTexture);
                this.mDetachedSurfaceTexture = null;
                this.mIsSurfaceTextureDetachedFromView = false;
            }
        }
    }
    
    @Nullable
    @Override
    View getPreview() {
        return (View)this.mTextureView;
    }
    
    @Nullable
    @Override
    Bitmap getPreviewBitmap() {
        final TextureView mTextureView = this.mTextureView;
        if (mTextureView != null && mTextureView.isAvailable()) {
            return this.mTextureView.getBitmap();
        }
        return null;
    }
    
    public void initializePreview() {
        Preconditions.checkNotNull(super.mParent);
        Preconditions.checkNotNull(super.mResolution);
        ((View)(this.mTextureView = new TextureView(((View)super.mParent).getContext()))).setLayoutParams((ViewGroup$LayoutParams)new FrameLayout$LayoutParams(super.mResolution.getWidth(), super.mResolution.getHeight()));
        this.mTextureView.setSurfaceTextureListener((TextureView$SurfaceTextureListener)new TextureView$SurfaceTextureListener(this) {
            final TextureViewImplementation this$0;
            
            public void onSurfaceTextureAvailable(@NonNull final SurfaceTexture mSurfaceTexture, final int i, final int j) {
                final StringBuilder sb = new StringBuilder();
                sb.append("SurfaceTexture available. Size: ");
                sb.append(i);
                sb.append("x");
                sb.append(j);
                Logger.d("TextureViewImpl", sb.toString());
                final TextureViewImplementation this$0 = this.this$0;
                this$0.mSurfaceTexture = mSurfaceTexture;
                if (this$0.mSurfaceReleaseFuture != null) {
                    Preconditions.checkNotNull(this$0.mSurfaceRequest);
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Surface invalidated ");
                    sb2.append(this.this$0.mSurfaceRequest);
                    Logger.d("TextureViewImpl", sb2.toString());
                    this.this$0.mSurfaceRequest.getDeferrableSurface().close();
                }
                else {
                    this$0.tryToProvidePreviewSurface();
                }
            }
            
            public boolean onSurfaceTextureDestroyed(@NonNull final SurfaceTexture mDetachedSurfaceTexture) {
                final TextureViewImplementation this$0 = this.this$0;
                this$0.mSurfaceTexture = null;
                final ListenableFuture<SurfaceRequest.Result> mSurfaceReleaseFuture = this$0.mSurfaceReleaseFuture;
                if (mSurfaceReleaseFuture != null) {
                    Futures.addCallback((com.google.common.util.concurrent.ListenableFuture<Object>)mSurfaceReleaseFuture, (FutureCallback<? super Object>)new FutureCallback<SurfaceRequest.Result>(this, mDetachedSurfaceTexture) {
                        final TextureViewImplementation$1 this$1;
                        final SurfaceTexture val$surfaceTexture;
                        
                        @Override
                        public void onFailure(@NonNull final Throwable cause) {
                            throw new IllegalStateException("SurfaceReleaseFuture did not complete nicely.", cause);
                        }
                        
                        @Override
                        public void onSuccess(final SurfaceRequest.Result result) {
                            Preconditions.checkState(result.getResultCode() != 3, "Unexpected result from SurfaceRequest. Surface was provided twice.");
                            Logger.d("TextureViewImpl", "SurfaceTexture about to manually be destroyed");
                            this.val$surfaceTexture.release();
                            final TextureViewImplementation this$0 = this.this$1.this$0;
                            if (this$0.mDetachedSurfaceTexture != null) {
                                this$0.mDetachedSurfaceTexture = null;
                            }
                        }
                    }, ContextCompat.getMainExecutor(((View)this.this$0.mTextureView).getContext()));
                    this.this$0.mDetachedSurfaceTexture = mDetachedSurfaceTexture;
                    return false;
                }
                Logger.d("TextureViewImpl", "SurfaceTexture about to be destroyed");
                return true;
            }
            
            public void onSurfaceTextureSizeChanged(@NonNull final SurfaceTexture surfaceTexture, final int i, final int j) {
                final StringBuilder sb = new StringBuilder();
                sb.append("SurfaceTexture size changed: ");
                sb.append(i);
                sb.append("x");
                sb.append(j);
                Logger.d("TextureViewImpl", sb.toString());
            }
            
            public void onSurfaceTextureUpdated(@NonNull final SurfaceTexture surfaceTexture) {
                final CallbackToFutureAdapter.Completer completer = this.this$0.mNextFrameCompleter.getAndSet(null);
                if (completer != null) {
                    completer.set(null);
                }
                final TextureViewImplementation this$0 = this.this$0;
                final PreviewView.OnFrameUpdateListener mOnFrameUpdateListener = this$0.mOnFrameUpdateListener;
                final Executor mFrameUpdateExecutor = this$0.mFrameUpdateExecutor;
                if (mOnFrameUpdateListener != null && mFrameUpdateExecutor != null) {
                    mFrameUpdateExecutor.execute(new \u300700(mOnFrameUpdateListener, surfaceTexture));
                }
            }
        });
        ((ViewGroup)super.mParent).removeAllViews();
        ((ViewGroup)super.mParent).addView((View)this.mTextureView);
    }
    
    @Override
    void onAttachedToWindow() {
        this.reattachSurfaceTexture();
    }
    
    @Override
    void onDetachedFromWindow() {
        this.mIsSurfaceTextureDetachedFromView = true;
    }
    
    @Override
    void onSurfaceRequested(@NonNull final SurfaceRequest mSurfaceRequest, @Nullable final OnSurfaceNotInUseListener mOnSurfaceNotInUseListener) {
        super.mResolution = mSurfaceRequest.getResolution();
        this.mOnSurfaceNotInUseListener = mOnSurfaceNotInUseListener;
        this.initializePreview();
        final SurfaceRequest mSurfaceRequest2 = this.mSurfaceRequest;
        if (mSurfaceRequest2 != null) {
            mSurfaceRequest2.willNotProvideSurface();
        }
        (this.mSurfaceRequest = mSurfaceRequest).addRequestCancellationListener(ContextCompat.getMainExecutor(((View)this.mTextureView).getContext()), new o\u3007O8\u3007\u3007o(this, mSurfaceRequest));
        this.tryToProvidePreviewSurface();
    }
    
    @Override
    void setFrameUpdateListener(@NonNull final Executor mFrameUpdateExecutor, @NonNull final PreviewView.OnFrameUpdateListener mOnFrameUpdateListener) {
        this.mOnFrameUpdateListener = mOnFrameUpdateListener;
        this.mFrameUpdateExecutor = mFrameUpdateExecutor;
    }
    
    void tryToProvidePreviewSurface() {
        final Size mResolution = super.mResolution;
        if (mResolution != null) {
            final SurfaceTexture mSurfaceTexture = this.mSurfaceTexture;
            if (mSurfaceTexture != null) {
                if (this.mSurfaceRequest != null) {
                    mSurfaceTexture.setDefaultBufferSize(mResolution.getWidth(), super.mResolution.getHeight());
                    final Surface surface = new Surface(this.mSurfaceTexture);
                    final SurfaceRequest mSurfaceRequest = this.mSurfaceRequest;
                    final com.google.common.util.concurrent.ListenableFuture<SurfaceRequest.Result> future = CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<SurfaceRequest.Result>)new o800o8O(this, surface));
                    (this.mSurfaceReleaseFuture = future).addListener((Runnable)new \u3007O888o0o(this, surface, future, mSurfaceRequest), ContextCompat.getMainExecutor(((View)this.mTextureView).getContext()));
                    this.onSurfaceProvided();
                }
            }
        }
    }
    
    @NonNull
    @Override
    ListenableFuture<Void> waitForNextFrame() {
        return CallbackToFutureAdapter.getFuture((CallbackToFutureAdapter.Resolver<Void>)new oo88o8O(this));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view;

import android.view.ViewGroup;
import android.view.SurfaceHolder;
import android.view.Surface;
import androidx.core.util.Consumer;
import androidx.annotation.UiThread;
import android.util.Size;
import androidx.annotation.DoNotInline;
import android.os.Handler;
import androidx.camera.core.impl.utils.futures.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Executor;
import androidx.core.content.ContextCompat;
import android.view.SurfaceHolder$Callback;
import android.view.ViewGroup$LayoutParams;
import android.widget.FrameLayout$LayoutParams;
import androidx.core.util.Preconditions;
import android.view.PixelCopy$OnPixelCopyFinishedListener;
import android.graphics.Bitmap$Config;
import android.graphics.Bitmap;
import android.view.View;
import androidx.camera.core.SurfaceRequest;
import androidx.camera.core.Logger;
import androidx.annotation.NonNull;
import android.widget.FrameLayout;
import android.view.SurfaceView;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
final class SurfaceViewImplementation extends PreviewViewImplementation
{
    private static final String TAG = "SurfaceViewImpl";
    @Nullable
    private OnSurfaceNotInUseListener mOnSurfaceNotInUseListener;
    final SurfaceRequestCallback mSurfaceRequestCallback;
    SurfaceView mSurfaceView;
    
    SurfaceViewImplementation(@NonNull final FrameLayout frameLayout, @NonNull final PreviewTransformation previewTransformation) {
        super(frameLayout, previewTransformation);
        this.mSurfaceRequestCallback = new SurfaceRequestCallback();
    }
    
    @Nullable
    @Override
    View getPreview() {
        return (View)this.mSurfaceView;
    }
    
    @Nullable
    @RequiresApi(24)
    @Override
    Bitmap getPreviewBitmap() {
        final SurfaceView mSurfaceView = this.mSurfaceView;
        if (mSurfaceView != null && mSurfaceView.getHolder().getSurface() != null && this.mSurfaceView.getHolder().getSurface().isValid()) {
            final Bitmap bitmap = Bitmap.createBitmap(((View)this.mSurfaceView).getWidth(), ((View)this.mSurfaceView).getHeight(), Bitmap$Config.ARGB_8888);
            Api24Impl.pixelCopyRequest(this.mSurfaceView, bitmap, (PixelCopy$OnPixelCopyFinishedListener)new \u3007\u30078O0\u30078(), ((View)this.mSurfaceView).getHandler());
            return bitmap;
        }
        return null;
    }
    
    @Override
    void initializePreview() {
        Preconditions.checkNotNull(super.mParent);
        Preconditions.checkNotNull(super.mResolution);
        ((View)(this.mSurfaceView = new SurfaceView(((View)super.mParent).getContext()))).setLayoutParams((ViewGroup$LayoutParams)new FrameLayout$LayoutParams(super.mResolution.getWidth(), super.mResolution.getHeight()));
        ((ViewGroup)super.mParent).removeAllViews();
        ((ViewGroup)super.mParent).addView((View)this.mSurfaceView);
        this.mSurfaceView.getHolder().addCallback((SurfaceHolder$Callback)this.mSurfaceRequestCallback);
    }
    
    void notifySurfaceNotInUse() {
        final OnSurfaceNotInUseListener mOnSurfaceNotInUseListener = this.mOnSurfaceNotInUseListener;
        if (mOnSurfaceNotInUseListener != null) {
            mOnSurfaceNotInUseListener.onSurfaceNotInUse();
            this.mOnSurfaceNotInUseListener = null;
        }
    }
    
    @Override
    void onAttachedToWindow() {
    }
    
    @Override
    void onDetachedFromWindow() {
    }
    
    @Override
    void onSurfaceRequested(@NonNull final SurfaceRequest surfaceRequest, @Nullable final OnSurfaceNotInUseListener mOnSurfaceNotInUseListener) {
        super.mResolution = surfaceRequest.getResolution();
        this.mOnSurfaceNotInUseListener = mOnSurfaceNotInUseListener;
        this.initializePreview();
        surfaceRequest.addRequestCancellationListener(ContextCompat.getMainExecutor(((View)this.mSurfaceView).getContext()), new \u3007\u3007808\u3007(this));
        ((View)this.mSurfaceView).post((Runnable)new \u3007O00(this, surfaceRequest));
    }
    
    @Override
    void setFrameUpdateListener(@NonNull final Executor executor, @NonNull final PreviewView.OnFrameUpdateListener onFrameUpdateListener) {
        throw new IllegalArgumentException("SurfaceView doesn't support frame update listener");
    }
    
    @NonNull
    @Override
    ListenableFuture<Void> waitForNextFrame() {
        return Futures.immediateFuture((Void)null);
    }
    
    @RequiresApi(24)
    private static class Api24Impl
    {
        @DoNotInline
        static void pixelCopyRequest(@NonNull final SurfaceView surfaceView, @NonNull final Bitmap bitmap, @NonNull final PixelCopy$OnPixelCopyFinishedListener pixelCopy$OnPixelCopyFinishedListener, @NonNull final Handler handler) {
            \u30070\u3007O0088o.\u3007080(surfaceView, bitmap, pixelCopy$OnPixelCopyFinishedListener, handler);
        }
    }
    
    @RequiresApi(21)
    class SurfaceRequestCallback implements SurfaceHolder$Callback
    {
        @Nullable
        private Size mCurrentSurfaceSize;
        @Nullable
        private SurfaceRequest mSurfaceRequest;
        @Nullable
        private Size mTargetSize;
        private boolean mWasSurfaceProvided;
        final SurfaceViewImplementation this$0;
        
        SurfaceRequestCallback(final SurfaceViewImplementation this$0) {
            this.this$0 = this$0;
            this.mWasSurfaceProvided = false;
        }
        
        private boolean canProvideSurface() {
            if (!this.mWasSurfaceProvided && this.mSurfaceRequest != null) {
                final Size mTargetSize = this.mTargetSize;
                if (mTargetSize != null && mTargetSize.equals((Object)this.mCurrentSurfaceSize)) {
                    return true;
                }
            }
            return false;
        }
        
        @UiThread
        private void cancelPreviousRequest() {
            if (this.mSurfaceRequest != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Request canceled: ");
                sb.append(this.mSurfaceRequest);
                Logger.d("SurfaceViewImpl", sb.toString());
                this.mSurfaceRequest.willNotProvideSurface();
            }
        }
        
        @UiThread
        private void invalidateSurface() {
            if (this.mSurfaceRequest != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Surface invalidated ");
                sb.append(this.mSurfaceRequest);
                Logger.d("SurfaceViewImpl", sb.toString());
                this.mSurfaceRequest.getDeferrableSurface().close();
            }
        }
        
        @UiThread
        private boolean tryToComplete() {
            final Surface surface = this.this$0.mSurfaceView.getHolder().getSurface();
            if (this.canProvideSurface()) {
                Logger.d("SurfaceViewImpl", "Surface set on Preview.");
                this.mSurfaceRequest.provideSurface(surface, ContextCompat.getMainExecutor(((View)this.this$0.mSurfaceView).getContext()), new OoO8(this));
                this.mWasSurfaceProvided = true;
                this.this$0.onSurfaceProvided();
                return true;
            }
            return false;
        }
        
        @UiThread
        void setSurfaceRequest(@NonNull final SurfaceRequest mSurfaceRequest) {
            this.cancelPreviousRequest();
            this.mSurfaceRequest = mSurfaceRequest;
            final Size resolution = mSurfaceRequest.getResolution();
            this.mTargetSize = resolution;
            this.mWasSurfaceProvided = false;
            if (!this.tryToComplete()) {
                Logger.d("SurfaceViewImpl", "Wait for new Surface creation.");
                this.this$0.mSurfaceView.getHolder().setFixedSize(resolution.getWidth(), resolution.getHeight());
            }
        }
        
        public void surfaceChanged(@NonNull final SurfaceHolder surfaceHolder, final int n, final int i, final int j) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Surface changed. Size: ");
            sb.append(i);
            sb.append("x");
            sb.append(j);
            Logger.d("SurfaceViewImpl", sb.toString());
            this.mCurrentSurfaceSize = new Size(i, j);
            this.tryToComplete();
        }
        
        public void surfaceCreated(@NonNull final SurfaceHolder surfaceHolder) {
            Logger.d("SurfaceViewImpl", "Surface created.");
        }
        
        public void surfaceDestroyed(@NonNull final SurfaceHolder surfaceHolder) {
            Logger.d("SurfaceViewImpl", "Surface destroyed.");
            if (this.mWasSurfaceProvided) {
                this.invalidateSurface();
            }
            else {
                this.cancelPreviousRequest();
            }
            this.mWasSurfaceProvided = false;
            this.mSurfaceRequest = null;
            this.mCurrentSurfaceSize = null;
            this.mTargetSize = null;
        }
    }
}

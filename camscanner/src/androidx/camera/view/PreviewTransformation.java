// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view;

import android.view.Display;
import android.view.TextureView;
import android.view.View;
import androidx.camera.core.SurfaceRequest;
import androidx.annotation.VisibleForTesting;
import androidx.camera.core.impl.utils.CameraOrientationUtil;
import androidx.annotation.Nullable;
import android.graphics.Paint;
import android.graphics.Canvas;
import androidx.annotation.NonNull;
import android.graphics.Bitmap;
import android.graphics.Matrix$ScaleToFit;
import androidx.camera.core.Logger;
import android.graphics.Matrix;
import androidx.core.util.Preconditions;
import androidx.camera.core.impl.utils.TransformUtils;
import android.graphics.RectF;
import android.graphics.Rect;
import android.util.Size;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
final class PreviewTransformation
{
    private static final PreviewView.ScaleType DEFAULT_SCALE_TYPE;
    private static final String TAG = "PreviewTransform";
    private boolean mIsFrontCamera;
    private int mPreviewRotationDegrees;
    private Size mResolution;
    private PreviewView.ScaleType mScaleType;
    private Rect mSurfaceCropRect;
    private int mTargetRotation;
    
    static {
        DEFAULT_SCALE_TYPE = PreviewView.ScaleType.FILL_CENTER;
    }
    
    PreviewTransformation() {
        this.mScaleType = PreviewTransformation.DEFAULT_SCALE_TYPE;
    }
    
    private static RectF flipHorizontally(final RectF rectF, float n) {
        n += n;
        return new RectF(n - rectF.right, rectF.top, n - rectF.left, rectF.bottom);
    }
    
    private Size getRotatedViewportSize() {
        if (TransformUtils.is90or270(this.mPreviewRotationDegrees)) {
            return new Size(this.mSurfaceCropRect.height(), this.mSurfaceCropRect.width());
        }
        return new Size(this.mSurfaceCropRect.width(), this.mSurfaceCropRect.height());
    }
    
    private RectF getTransformedSurfaceRect(final Size size, final int n) {
        Preconditions.checkState(this.isTransformationInfoReady());
        final Matrix surfaceToPreviewViewMatrix = this.getSurfaceToPreviewViewMatrix(size, n);
        final RectF rectF = new RectF(0.0f, 0.0f, (float)this.mResolution.getWidth(), (float)this.mResolution.getHeight());
        surfaceToPreviewViewMatrix.mapRect(rectF);
        return rectF;
    }
    
    private boolean isTransformationInfoReady() {
        return this.mSurfaceCropRect != null && this.mResolution != null && this.mTargetRotation != -1;
    }
    
    private static void setMatrixRectToRect(final Matrix matrix, final RectF rectF, final RectF rectF2, final PreviewView.ScaleType obj) {
        Matrix$ScaleToFit matrix$ScaleToFit = null;
        switch (PreviewTransformation$1.$SwitchMap$androidx$camera$view$PreviewView$ScaleType[obj.ordinal()]) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unexpected crop rect: ");
                sb.append(obj);
                Logger.e("PreviewTransform", sb.toString());
                matrix$ScaleToFit = Matrix$ScaleToFit.FILL;
                break;
            }
            case 5:
            case 6: {
                matrix$ScaleToFit = Matrix$ScaleToFit.START;
                break;
            }
            case 3:
            case 4: {
                matrix$ScaleToFit = Matrix$ScaleToFit.END;
                break;
            }
            case 1:
            case 2: {
                matrix$ScaleToFit = Matrix$ScaleToFit.CENTER;
                break;
            }
        }
        if (obj == PreviewView.ScaleType.FIT_CENTER || obj == PreviewView.ScaleType.FIT_START || obj == PreviewView.ScaleType.FIT_END) {
            matrix.setRectToRect(rectF, rectF2, matrix$ScaleToFit);
        }
        else {
            matrix.setRectToRect(rectF2, rectF, matrix$ScaleToFit);
            matrix.invert(matrix);
        }
    }
    
    Bitmap createTransformedBitmap(@NonNull final Bitmap bitmap, final Size size, final int n) {
        if (!this.isTransformationInfoReady()) {
            return bitmap;
        }
        final Matrix textureViewCorrectionMatrix = this.getTextureViewCorrectionMatrix();
        final RectF transformedSurfaceRect = this.getTransformedSurfaceRect(size, n);
        final Bitmap bitmap2 = Bitmap.createBitmap(size.getWidth(), size.getHeight(), bitmap.getConfig());
        final Canvas canvas = new Canvas(bitmap2);
        final Matrix matrix = new Matrix();
        matrix.postConcat(textureViewCorrectionMatrix);
        matrix.postScale(transformedSurfaceRect.width() / this.mResolution.getWidth(), transformedSurfaceRect.height() / this.mResolution.getHeight());
        matrix.postTranslate(transformedSurfaceRect.left, transformedSurfaceRect.top);
        canvas.drawBitmap(bitmap, matrix, new Paint(7));
        return bitmap2;
    }
    
    @Nullable
    Matrix getPreviewViewToNormalizedSurfaceMatrix(final Size size, final int n) {
        if (!this.isTransformationInfoReady()) {
            return null;
        }
        final Matrix matrix = new Matrix();
        this.getSurfaceToPreviewViewMatrix(size, n).invert(matrix);
        final Matrix matrix2 = new Matrix();
        matrix2.setRectToRect(new RectF(0.0f, 0.0f, (float)this.mResolution.getWidth(), (float)this.mResolution.getHeight()), new RectF(0.0f, 0.0f, 1.0f, 1.0f), Matrix$ScaleToFit.FILL);
        matrix.postConcat(matrix2);
        return matrix;
    }
    
    RectF getPreviewViewViewportRectForMismatchedAspectRatios(final Size size, final int n) {
        final RectF rectF = new RectF(0.0f, 0.0f, (float)size.getWidth(), (float)size.getHeight());
        final Size rotatedViewportSize = this.getRotatedViewportSize();
        final RectF rectF2 = new RectF(0.0f, 0.0f, (float)rotatedViewportSize.getWidth(), (float)rotatedViewportSize.getHeight());
        final Matrix matrix = new Matrix();
        setMatrixRectToRect(matrix, rectF2, rectF, this.mScaleType);
        matrix.mapRect(rectF2);
        if (n == 1) {
            return flipHorizontally(rectF2, size.getWidth() / 2.0f);
        }
        return rectF2;
    }
    
    PreviewView.ScaleType getScaleType() {
        return this.mScaleType;
    }
    
    @Nullable
    Rect getSurfaceCropRect() {
        return this.mSurfaceCropRect;
    }
    
    Matrix getSurfaceToPreviewViewMatrix(final Size size, final int n) {
        Preconditions.checkState(this.isTransformationInfoReady());
        RectF previewViewViewportRectForMismatchedAspectRatios;
        if (this.isViewportAspectRatioMatchPreviewView(size)) {
            previewViewViewportRectForMismatchedAspectRatios = new RectF(0.0f, 0.0f, (float)size.getWidth(), (float)size.getHeight());
        }
        else {
            previewViewViewportRectForMismatchedAspectRatios = this.getPreviewViewViewportRectForMismatchedAspectRatios(size, n);
        }
        final Matrix rectToRect = TransformUtils.getRectToRect(new RectF(this.mSurfaceCropRect), previewViewViewportRectForMismatchedAspectRatios, this.mPreviewRotationDegrees);
        if (this.mIsFrontCamera) {
            if (TransformUtils.is90or270(this.mPreviewRotationDegrees)) {
                rectToRect.preScale(1.0f, -1.0f, (float)this.mSurfaceCropRect.centerX(), (float)this.mSurfaceCropRect.centerY());
            }
            else {
                rectToRect.preScale(-1.0f, 1.0f, (float)this.mSurfaceCropRect.centerX(), (float)this.mSurfaceCropRect.centerY());
            }
        }
        return rectToRect;
    }
    
    @VisibleForTesting
    Matrix getTextureViewCorrectionMatrix() {
        Preconditions.checkState(this.isTransformationInfoReady());
        final RectF rectF = new RectF(0.0f, 0.0f, (float)this.mResolution.getWidth(), (float)this.mResolution.getHeight());
        return TransformUtils.getRectToRect(rectF, rectF, -CameraOrientationUtil.surfaceRotationToDegrees(this.mTargetRotation));
    }
    
    @VisibleForTesting
    boolean isViewportAspectRatioMatchPreviewView(final Size size) {
        return TransformUtils.isAspectRatioMatchingWithRoundingError(size, true, this.getRotatedViewportSize(), false);
    }
    
    void overrideWithDisplayRotation(final int mPreviewRotationDegrees, final int mTargetRotation) {
        this.mPreviewRotationDegrees = mPreviewRotationDegrees;
        this.mTargetRotation = mTargetRotation;
    }
    
    void setScaleType(final PreviewView.ScaleType mScaleType) {
        this.mScaleType = mScaleType;
    }
    
    void setTransformationInfo(@NonNull final SurfaceRequest.TransformationInfo obj, final Size size, final boolean b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Transformation info set: ");
        sb.append(obj);
        sb.append(" ");
        sb.append(size);
        sb.append(" ");
        sb.append(b);
        Logger.d("PreviewTransform", sb.toString());
        this.mSurfaceCropRect = obj.getCropRect();
        this.mPreviewRotationDegrees = obj.getRotationDegrees();
        this.mTargetRotation = obj.getTargetRotation();
        this.mResolution = size;
        this.mIsFrontCamera = b;
    }
    
    void transformView(final Size obj, final int n, @NonNull final View view) {
        if (obj.getHeight() == 0 || obj.getWidth() == 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Transform not applied due to PreviewView size: ");
            sb.append(obj);
            Logger.w("PreviewTransform", sb.toString());
            return;
        }
        if (!this.isTransformationInfoReady()) {
            return;
        }
        if (view instanceof TextureView) {
            ((TextureView)view).setTransform(this.getTextureViewCorrectionMatrix());
        }
        else {
            final Display display = view.getDisplay();
            if (display != null && display.getRotation() != this.mTargetRotation) {
                Logger.e("PreviewTransform", "Non-display rotation not supported with SurfaceView / PERFORMANCE mode.");
            }
        }
        final RectF transformedSurfaceRect = this.getTransformedSurfaceRect(obj, n);
        view.setPivotX(0.0f);
        view.setPivotY(0.0f);
        view.setScaleX(transformedSurfaceRect.width() / this.mResolution.getWidth());
        view.setScaleY(transformedSurfaceRect.height() / this.mResolution.getHeight());
        view.setTranslationX(transformedSurfaceRect.left - view.getLeft());
        view.setTranslationY(transformedSurfaceRect.top - view.getTop());
    }
}

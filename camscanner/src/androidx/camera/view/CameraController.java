// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.util.Size;
import androidx.annotation.DoNotInline;
import androidx.camera.core.impl.utils.Oo08;
import android.graphics.Matrix;
import androidx.camera.view.transform.OutputTransform;
import androidx.camera.view.video.OutputFileResults;
import androidx.camera.view.video.OnVideoSavedCallback;
import androidx.camera.view.video.OutputFileOptions;
import androidx.annotation.FloatRange;
import androidx.camera.core.FocusMeteringResult;
import androidx.camera.core.impl.utils.futures.FutureCallback;
import androidx.camera.core.FocusMeteringAction;
import androidx.camera.core.MeteringPointFactory;
import androidx.camera.core.CameraInfoUnavailableException;
import androidx.core.util.Preconditions;
import androidx.lifecycle.LiveData;
import androidx.camera.core.CameraInfo;
import androidx.camera.core.CameraControl;
import androidx.annotation.RestrictTo;
import androidx.annotation.OptIn;
import java.util.Iterator;
import androidx.camera.core.UseCaseGroup;
import android.annotation.SuppressLint;
import androidx.annotation.MainThread;
import androidx.camera.core.UseCase;
import androidx.camera.core.impl.utils.Threads;
import androidx.camera.core.Logger;
import androidx.camera.core.impl.ImageOutputConfig;
import java.util.Objects;
import android.os.Build$VERSION;
import androidx.arch.core.util.Function;
import androidx.camera.core.impl.utils.futures.Futures;
import androidx.camera.core.impl.utils.executor.CameraXExecutors;
import java.util.Collections;
import androidx.camera.core.ZoomState;
import androidx.camera.core.ViewPort;
import java.util.concurrent.atomic.AtomicBoolean;
import androidx.camera.core.VideoCapture;
import androidx.lifecycle.MutableLiveData;
import android.view.Display;
import androidx.camera.core.Preview;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.CameraEffect;
import java.util.List;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.NonNull;
import androidx.camera.core.CameraSelector;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.core.Camera;
import android.content.Context;
import java.util.concurrent.Executor;
import androidx.annotation.Nullable;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.view.video.ExperimentalVideo;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public abstract class CameraController
{
    private static final float AE_SIZE = 0.25f;
    private static final float AF_SIZE = 0.16666667f;
    private static final String CAMERA_NOT_ATTACHED = "Use cases not attached to camera.";
    private static final String CAMERA_NOT_INITIALIZED = "Camera not initialized.";
    public static final int COORDINATE_SYSTEM_VIEW_REFERENCED = 1;
    public static final int IMAGE_ANALYSIS = 2;
    public static final int IMAGE_CAPTURE = 1;
    private static final String IMAGE_CAPTURE_DISABLED = "ImageCapture disabled.";
    private static final String PREVIEW_VIEW_NOT_ATTACHED = "PreviewView not attached to CameraController.";
    private static final String TAG = "CameraController";
    public static final int TAP_TO_FOCUS_FAILED = 4;
    public static final int TAP_TO_FOCUS_FOCUSED = 2;
    public static final int TAP_TO_FOCUS_NOT_FOCUSED = 3;
    public static final int TAP_TO_FOCUS_NOT_STARTED = 0;
    public static final int TAP_TO_FOCUS_STARTED = 1;
    @ExperimentalVideo
    public static final int VIDEO_CAPTURE = 4;
    private static final String VIDEO_CAPTURE_DISABLED = "VideoCapture disabled.";
    @Nullable
    private ImageAnalysis.Analyzer mAnalysisAnalyzer;
    @Nullable
    private Executor mAnalysisBackgroundExecutor;
    @Nullable
    private Executor mAnalysisExecutor;
    private final Context mAppContext;
    @Nullable
    Camera mCamera;
    @Nullable
    ProcessCameraProvider mCameraProvider;
    CameraSelector mCameraSelector;
    @NonNull
    @VisibleForTesting
    final RotationProvider.Listener mDeviceRotationListener;
    @NonNull
    private List<CameraEffect> mEffects;
    private int mEnabledUseCases;
    @NonNull
    ImageAnalysis mImageAnalysis;
    @Nullable
    OutputSize mImageAnalysisTargetSize;
    @NonNull
    ImageCapture mImageCapture;
    @Nullable
    Executor mImageCaptureIoExecutor;
    @Nullable
    OutputSize mImageCaptureTargetSize;
    @NonNull
    private final ListenableFuture<Void> mInitializationFuture;
    private boolean mPinchToZoomEnabled;
    @NonNull
    Preview mPreview;
    @Nullable
    Display mPreviewDisplay;
    @Nullable
    OutputSize mPreviewTargetSize;
    private final RotationProvider mRotationProvider;
    @Nullable
    Preview.SurfaceProvider mSurfaceProvider;
    private boolean mTapToFocusEnabled;
    final MutableLiveData<Integer> mTapToFocusState;
    private final ForwardingLiveData<Integer> mTorchState;
    @NonNull
    VideoCapture mVideoCapture;
    @Nullable
    OutputSize mVideoCaptureOutputSize;
    @NonNull
    final AtomicBoolean mVideoIsRecording;
    @Nullable
    ViewPort mViewPort;
    private final ForwardingLiveData<ZoomState> mZoomState;
    
    CameraController(@NonNull Context applicationContext) {
        this.mCameraSelector = CameraSelector.DEFAULT_BACK_CAMERA;
        this.mEnabledUseCases = 3;
        this.mVideoIsRecording = new AtomicBoolean(false);
        this.mPinchToZoomEnabled = true;
        this.mTapToFocusEnabled = true;
        this.mZoomState = new ForwardingLiveData<ZoomState>();
        this.mTorchState = new ForwardingLiveData<Integer>();
        this.mTapToFocusState = new MutableLiveData<Integer>(0);
        this.mEffects = Collections.emptyList();
        applicationContext = getApplicationContext(applicationContext);
        this.mAppContext = applicationContext;
        this.mPreview = new Preview.Builder().build();
        this.mImageCapture = new ImageCapture.Builder().build();
        this.mImageAnalysis = new ImageAnalysis.Builder().build();
        this.mVideoCapture = new VideoCapture.Builder().build();
        this.mInitializationFuture = Futures.transform(ProcessCameraProvider.getInstance(applicationContext), (Function<? super ProcessCameraProvider, ? extends Void>)new \u3007o00\u3007\u3007Oo(this), (Executor)CameraXExecutors.mainThreadExecutor());
        this.mRotationProvider = new RotationProvider(applicationContext);
        this.mDeviceRotationListener = new \u3007o\u3007(this);
    }
    
    private static Context getApplicationContext(@NonNull final Context context) {
        final Context applicationContext = context.getApplicationContext();
        if (Build$VERSION.SDK_INT >= 30) {
            final String attributionTag = Api30Impl.getAttributionTag(context);
            if (attributionTag != null) {
                return Api30Impl.createAttributionContext(applicationContext, attributionTag);
            }
        }
        return applicationContext;
    }
    
    private boolean isCameraAttached() {
        return this.mCamera != null;
    }
    
    private boolean isCameraInitialized() {
        return this.mCameraProvider != null;
    }
    
    private boolean isOutputSizeEqual(@Nullable final OutputSize outputSize, @Nullable final OutputSize obj) {
        boolean b = true;
        if (outputSize == obj) {
            return true;
        }
        if (outputSize == null || !outputSize.equals(obj)) {
            b = false;
        }
        return b;
    }
    
    private boolean isPreviewViewAttached() {
        return this.mSurfaceProvider != null && this.mViewPort != null && this.mPreviewDisplay != null;
    }
    
    private boolean isUseCaseEnabled(final int n) {
        return (n & this.mEnabledUseCases) != 0x0;
    }
    
    private void restartCameraIfAnalyzerResolutionChanged(@Nullable final ImageAnalysis.Analyzer analyzer, @Nullable final ImageAnalysis.Analyzer analyzer2) {
        final Object o = null;
        Object defaultTargetResolution;
        if (analyzer == null) {
            defaultTargetResolution = null;
        }
        else {
            defaultTargetResolution = analyzer.getDefaultTargetResolution();
        }
        Object defaultTargetResolution2;
        if (analyzer2 == null) {
            defaultTargetResolution2 = o;
        }
        else {
            defaultTargetResolution2 = analyzer2.getDefaultTargetResolution();
        }
        if (!Objects.equals(defaultTargetResolution, defaultTargetResolution2)) {
            this.unbindImageAnalysisAndRecreate(this.mImageAnalysis.getBackpressureStrategy(), this.mImageAnalysis.getImageQueueDepth());
            this.startCameraAndTrackStates();
        }
    }
    
    private void setTargetOutputSize(@NonNull final ImageOutputConfig.Builder<?> builder, @Nullable final OutputSize obj) {
        if (obj == null) {
            return;
        }
        if (obj.getResolution() != null) {
            builder.setTargetResolution(obj.getResolution());
        }
        else if (obj.getAspectRatio() != -1) {
            builder.setTargetAspectRatio(obj.getAspectRatio());
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid target surface size. ");
            sb.append(obj);
            Logger.e("CameraController", sb.toString());
        }
    }
    
    private float speedUpZoomBy2X(final float n) {
        if (n > 1.0f) {
            return (n - 1.0f) * 2.0f + 1.0f;
        }
        return 1.0f - (1.0f - n) * 2.0f;
    }
    
    private void startListeningToRotationEvents() {
        this.mRotationProvider.addListener(CameraXExecutors.mainThreadExecutor(), this.mDeviceRotationListener);
    }
    
    private void stopListeningToRotationEvents() {
        this.mRotationProvider.removeListener(this.mDeviceRotationListener);
    }
    
    @MainThread
    private void unbindImageAnalysisAndRecreate(final int backpressureStrategy, final int imageQueueDepth) {
        Threads.checkMainThread();
        if (this.isCameraInitialized()) {
            this.mCameraProvider.unbind(this.mImageAnalysis);
        }
        final ImageAnalysis.Builder setImageQueueDepth = new ImageAnalysis.Builder().setBackpressureStrategy(backpressureStrategy).setImageQueueDepth(imageQueueDepth);
        this.setTargetOutputSize(setImageQueueDepth, this.mImageAnalysisTargetSize);
        final Executor mAnalysisBackgroundExecutor = this.mAnalysisBackgroundExecutor;
        if (mAnalysisBackgroundExecutor != null) {
            setImageQueueDepth.setBackgroundExecutor(mAnalysisBackgroundExecutor);
        }
        final ImageAnalysis build = setImageQueueDepth.build();
        this.mImageAnalysis = build;
        final Executor mAnalysisExecutor = this.mAnalysisExecutor;
        if (mAnalysisExecutor != null) {
            final ImageAnalysis.Analyzer mAnalysisAnalyzer = this.mAnalysisAnalyzer;
            if (mAnalysisAnalyzer != null) {
                build.setAnalyzer(mAnalysisExecutor, mAnalysisAnalyzer);
            }
        }
    }
    
    private void unbindImageCaptureAndRecreate(final int captureMode) {
        if (this.isCameraInitialized()) {
            this.mCameraProvider.unbind(this.mImageCapture);
        }
        final ImageCapture.Builder setCaptureMode = new ImageCapture.Builder().setCaptureMode(captureMode);
        this.setTargetOutputSize(setCaptureMode, this.mImageCaptureTargetSize);
        final Executor mImageCaptureIoExecutor = this.mImageCaptureIoExecutor;
        if (mImageCaptureIoExecutor != null) {
            setCaptureMode.setIoExecutor(mImageCaptureIoExecutor);
        }
        this.mImageCapture = setCaptureMode.build();
    }
    
    private void unbindPreviewAndRecreate() {
        if (this.isCameraInitialized()) {
            this.mCameraProvider.unbind(this.mPreview);
        }
        final Preview.Builder builder = new Preview.Builder();
        this.setTargetOutputSize(builder, this.mPreviewTargetSize);
        this.mPreview = builder.build();
    }
    
    private void unbindVideoAndRecreate() {
        if (this.isCameraInitialized()) {
            this.mCameraProvider.unbind(this.mVideoCapture);
        }
        final VideoCapture.Builder builder = new VideoCapture.Builder();
        this.setTargetOutputSize(builder, this.mVideoCaptureOutputSize);
        this.mVideoCapture = builder.build();
    }
    
    @SuppressLint({ "MissingPermission", "WrongConstant" })
    @MainThread
    void attachPreviewSurface(@NonNull final Preview.SurfaceProvider surfaceProvider, @NonNull final ViewPort mViewPort, @NonNull final Display mPreviewDisplay) {
        Threads.checkMainThread();
        if (this.mSurfaceProvider != surfaceProvider) {
            this.mSurfaceProvider = surfaceProvider;
            this.mPreview.setSurfaceProvider(surfaceProvider);
        }
        this.mViewPort = mViewPort;
        this.mPreviewDisplay = mPreviewDisplay;
        this.startListeningToRotationEvents();
        this.startCameraAndTrackStates();
    }
    
    @MainThread
    public void clearImageAnalysisAnalyzer() {
        Threads.checkMainThread();
        final ImageAnalysis.Analyzer mAnalysisAnalyzer = this.mAnalysisAnalyzer;
        this.mAnalysisExecutor = null;
        this.mAnalysisAnalyzer = null;
        this.mImageAnalysis.clearAnalyzer();
        this.restartCameraIfAnalyzerResolutionChanged(mAnalysisAnalyzer, null);
    }
    
    @MainThread
    void clearPreviewSurface() {
        Threads.checkMainThread();
        final ProcessCameraProvider mCameraProvider = this.mCameraProvider;
        if (mCameraProvider != null) {
            mCameraProvider.unbind(this.mPreview, this.mImageCapture, this.mImageAnalysis, this.mVideoCapture);
        }
        this.mPreview.setSurfaceProvider(null);
        this.mCamera = null;
        this.mSurfaceProvider = null;
        this.mViewPort = null;
        this.mPreviewDisplay = null;
        this.stopListeningToRotationEvents();
    }
    
    @Nullable
    @OptIn(markerClass = { ExperimentalVideo.class })
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected UseCaseGroup createUseCaseGroup() {
        if (!this.isCameraInitialized()) {
            Logger.d("CameraController", "Camera not initialized.");
            return null;
        }
        if (!this.isPreviewViewAttached()) {
            Logger.d("CameraController", "PreviewView not attached to CameraController.");
            return null;
        }
        final UseCaseGroup.Builder addUseCase = new UseCaseGroup.Builder().addUseCase(this.mPreview);
        if (this.isImageCaptureEnabled()) {
            addUseCase.addUseCase(this.mImageCapture);
        }
        else {
            this.mCameraProvider.unbind(this.mImageCapture);
        }
        if (this.isImageAnalysisEnabled()) {
            addUseCase.addUseCase(this.mImageAnalysis);
        }
        else {
            this.mCameraProvider.unbind(this.mImageAnalysis);
        }
        if (this.isVideoCaptureEnabled()) {
            addUseCase.addUseCase(this.mVideoCapture);
        }
        else {
            this.mCameraProvider.unbind(this.mVideoCapture);
        }
        addUseCase.setViewPort(this.mViewPort);
        final Iterator<CameraEffect> iterator = this.mEffects.iterator();
        while (iterator.hasNext()) {
            addUseCase.addEffect(iterator.next());
        }
        return addUseCase.build();
    }
    
    @MainThread
    @NonNull
    public ListenableFuture<Void> enableTorch(final boolean b) {
        Threads.checkMainThread();
        if (!this.isCameraAttached()) {
            Logger.w("CameraController", "Use cases not attached to camera.");
            return Futures.immediateFuture((Void)null);
        }
        return this.mCamera.getCameraControl().enableTorch(b);
    }
    
    @MainThread
    @Nullable
    public CameraControl getCameraControl() {
        Threads.checkMainThread();
        final Camera mCamera = this.mCamera;
        CameraControl cameraControl;
        if (mCamera == null) {
            cameraControl = null;
        }
        else {
            cameraControl = mCamera.getCameraControl();
        }
        return cameraControl;
    }
    
    @MainThread
    @Nullable
    public CameraInfo getCameraInfo() {
        Threads.checkMainThread();
        final Camera mCamera = this.mCamera;
        CameraInfo cameraInfo;
        if (mCamera == null) {
            cameraInfo = null;
        }
        else {
            cameraInfo = mCamera.getCameraInfo();
        }
        return cameraInfo;
    }
    
    @MainThread
    @NonNull
    public CameraSelector getCameraSelector() {
        Threads.checkMainThread();
        return this.mCameraSelector;
    }
    
    @MainThread
    @Nullable
    public Executor getImageAnalysisBackgroundExecutor() {
        Threads.checkMainThread();
        return this.mAnalysisBackgroundExecutor;
    }
    
    @MainThread
    public int getImageAnalysisBackpressureStrategy() {
        Threads.checkMainThread();
        return this.mImageAnalysis.getBackpressureStrategy();
    }
    
    @MainThread
    public int getImageAnalysisImageQueueDepth() {
        Threads.checkMainThread();
        return this.mImageAnalysis.getImageQueueDepth();
    }
    
    @MainThread
    @Nullable
    public OutputSize getImageAnalysisTargetSize() {
        Threads.checkMainThread();
        return this.mImageAnalysisTargetSize;
    }
    
    @MainThread
    public int getImageCaptureFlashMode() {
        Threads.checkMainThread();
        return this.mImageCapture.getFlashMode();
    }
    
    @MainThread
    @Nullable
    public Executor getImageCaptureIoExecutor() {
        Threads.checkMainThread();
        return this.mImageCaptureIoExecutor;
    }
    
    @MainThread
    public int getImageCaptureMode() {
        Threads.checkMainThread();
        return this.mImageCapture.getCaptureMode();
    }
    
    @MainThread
    @Nullable
    public OutputSize getImageCaptureTargetSize() {
        Threads.checkMainThread();
        return this.mImageCaptureTargetSize;
    }
    
    @NonNull
    public ListenableFuture<Void> getInitializationFuture() {
        return this.mInitializationFuture;
    }
    
    @MainThread
    @Nullable
    public OutputSize getPreviewTargetSize() {
        Threads.checkMainThread();
        return this.mPreviewTargetSize;
    }
    
    @MainThread
    @NonNull
    public LiveData<Integer> getTapToFocusState() {
        Threads.checkMainThread();
        return this.mTapToFocusState;
    }
    
    @MainThread
    @NonNull
    public LiveData<Integer> getTorchState() {
        Threads.checkMainThread();
        return this.mTorchState;
    }
    
    @MainThread
    @Nullable
    @ExperimentalVideo
    public OutputSize getVideoCaptureTargetSize() {
        Threads.checkMainThread();
        return this.mVideoCaptureOutputSize;
    }
    
    @MainThread
    @NonNull
    public LiveData<ZoomState> getZoomState() {
        Threads.checkMainThread();
        return this.mZoomState;
    }
    
    @MainThread
    public boolean hasCamera(@NonNull final CameraSelector cameraSelector) {
        Threads.checkMainThread();
        Preconditions.checkNotNull(cameraSelector);
        final ProcessCameraProvider mCameraProvider = this.mCameraProvider;
        if (mCameraProvider != null) {
            try {
                return mCameraProvider.hasCamera(cameraSelector);
            }
            catch (final CameraInfoUnavailableException ex) {
                Logger.w("CameraController", "Failed to check camera availability", ex);
                return false;
            }
        }
        throw new IllegalStateException("Camera not initialized. Please wait for the initialization future to finish. See #getInitializationFuture().");
    }
    
    @MainThread
    public boolean isImageAnalysisEnabled() {
        Threads.checkMainThread();
        return this.isUseCaseEnabled(2);
    }
    
    @MainThread
    public boolean isImageCaptureEnabled() {
        Threads.checkMainThread();
        return this.isUseCaseEnabled(1);
    }
    
    @MainThread
    public boolean isPinchToZoomEnabled() {
        Threads.checkMainThread();
        return this.mPinchToZoomEnabled;
    }
    
    @MainThread
    @ExperimentalVideo
    public boolean isRecording() {
        Threads.checkMainThread();
        return this.mVideoIsRecording.get();
    }
    
    @MainThread
    public boolean isTapToFocusEnabled() {
        Threads.checkMainThread();
        return this.mTapToFocusEnabled;
    }
    
    @MainThread
    @ExperimentalVideo
    public boolean isVideoCaptureEnabled() {
        Threads.checkMainThread();
        return this.isUseCaseEnabled(4);
    }
    
    void onPinchToZoom(final float f) {
        if (!this.isCameraAttached()) {
            Logger.w("CameraController", "Use cases not attached to camera.");
            return;
        }
        if (!this.mPinchToZoomEnabled) {
            Logger.d("CameraController", "Pinch to zoom disabled.");
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Pinch to zoom with scale: ");
        sb.append(f);
        Logger.d("CameraController", sb.toString());
        final ZoomState zoomState = this.getZoomState().getValue();
        if (zoomState == null) {
            return;
        }
        this.setZoomRatio(Math.min(Math.max(zoomState.getZoomRatio() * this.speedUpZoomBy2X(f), zoomState.getMinZoomRatio()), zoomState.getMaxZoomRatio()));
    }
    
    void onTapToFocus(final MeteringPointFactory meteringPointFactory, final float f, final float f2) {
        if (!this.isCameraAttached()) {
            Logger.w("CameraController", "Use cases not attached to camera.");
            return;
        }
        if (!this.mTapToFocusEnabled) {
            Logger.d("CameraController", "Tap to focus disabled. ");
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Tap to focus started: ");
        sb.append(f);
        sb.append(", ");
        sb.append(f2);
        Logger.d("CameraController", sb.toString());
        this.mTapToFocusState.postValue(1);
        Futures.addCallback(this.mCamera.getCameraControl().startFocusAndMetering(new FocusMeteringAction.Builder(meteringPointFactory.createPoint(f, f2, 0.16666667f), 1).addPoint(meteringPointFactory.createPoint(f, f2, 0.25f), 2).build()), new FutureCallback<FocusMeteringResult>(this) {
            final CameraController this$0;
            
            @Override
            public void onFailure(@NonNull final Throwable t) {
                if (t instanceof CameraControl.OperationCanceledException) {
                    Logger.d("CameraController", "Tap-to-focus is canceled by new action.");
                    return;
                }
                Logger.d("CameraController", "Tap to focus failed.", t);
                this.this$0.mTapToFocusState.postValue(4);
            }
            
            @Override
            public void onSuccess(@Nullable final FocusMeteringResult focusMeteringResult) {
                if (focusMeteringResult == null) {
                    return;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Tap to focus onSuccess: ");
                sb.append(focusMeteringResult.isFocusSuccessful());
                Logger.d("CameraController", sb.toString());
                final MutableLiveData<Integer> mTapToFocusState = this.this$0.mTapToFocusState;
                int i;
                if (focusMeteringResult.isFocusSuccessful()) {
                    i = 2;
                }
                else {
                    i = 3;
                }
                mTapToFocusState.postValue(i);
            }
        }, CameraXExecutors.directExecutor());
    }
    
    @MainThread
    public void setCameraSelector(@NonNull final CameraSelector mCameraSelector) {
        Threads.checkMainThread();
        final CameraSelector mCameraSelector2 = this.mCameraSelector;
        if (mCameraSelector2 == mCameraSelector) {
            return;
        }
        this.mCameraSelector = mCameraSelector;
        final ProcessCameraProvider mCameraProvider = this.mCameraProvider;
        if (mCameraProvider == null) {
            return;
        }
        mCameraProvider.unbind(this.mPreview, this.mImageCapture, this.mImageAnalysis, this.mVideoCapture);
        this.startCameraAndTrackStates(new \u3007080(this, mCameraSelector2));
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void setEffects(@NonNull final List<CameraEffect> list) {
        if (Objects.equals(this.mEffects, list)) {
            return;
        }
        final ProcessCameraProvider mCameraProvider = this.mCameraProvider;
        if (mCameraProvider != null) {
            mCameraProvider.unbindAll();
        }
        this.mEffects = list;
        this.startCameraAndTrackStates();
    }
    
    @MainThread
    @OptIn(markerClass = { ExperimentalVideo.class })
    public void setEnabledUseCases(final int mEnabledUseCases) {
        Threads.checkMainThread();
        final int mEnabledUseCases2 = this.mEnabledUseCases;
        if (mEnabledUseCases == mEnabledUseCases2) {
            return;
        }
        this.mEnabledUseCases = mEnabledUseCases;
        if (!this.isVideoCaptureEnabled()) {
            this.stopRecording();
        }
        this.startCameraAndTrackStates(new O8(this, mEnabledUseCases2));
    }
    
    @MainThread
    public void setImageAnalysisAnalyzer(@NonNull final Executor mAnalysisExecutor, @NonNull final ImageAnalysis.Analyzer mAnalysisAnalyzer) {
        Threads.checkMainThread();
        final ImageAnalysis.Analyzer mAnalysisAnalyzer2 = this.mAnalysisAnalyzer;
        if (mAnalysisAnalyzer2 == mAnalysisAnalyzer && this.mAnalysisExecutor == mAnalysisExecutor) {
            return;
        }
        this.mAnalysisExecutor = mAnalysisExecutor;
        this.mAnalysisAnalyzer = mAnalysisAnalyzer;
        this.mImageAnalysis.setAnalyzer(mAnalysisExecutor, mAnalysisAnalyzer);
        this.restartCameraIfAnalyzerResolutionChanged(mAnalysisAnalyzer2, mAnalysisAnalyzer);
    }
    
    @MainThread
    public void setImageAnalysisBackgroundExecutor(@Nullable final Executor mAnalysisBackgroundExecutor) {
        Threads.checkMainThread();
        if (this.mAnalysisBackgroundExecutor == mAnalysisBackgroundExecutor) {
            return;
        }
        this.mAnalysisBackgroundExecutor = mAnalysisBackgroundExecutor;
        this.unbindImageAnalysisAndRecreate(this.mImageAnalysis.getBackpressureStrategy(), this.mImageAnalysis.getImageQueueDepth());
        this.startCameraAndTrackStates();
    }
    
    @MainThread
    public void setImageAnalysisBackpressureStrategy(final int n) {
        Threads.checkMainThread();
        if (this.mImageAnalysis.getBackpressureStrategy() == n) {
            return;
        }
        this.unbindImageAnalysisAndRecreate(n, this.mImageAnalysis.getImageQueueDepth());
        this.startCameraAndTrackStates();
    }
    
    @MainThread
    public void setImageAnalysisImageQueueDepth(final int n) {
        Threads.checkMainThread();
        if (this.mImageAnalysis.getImageQueueDepth() == n) {
            return;
        }
        this.unbindImageAnalysisAndRecreate(this.mImageAnalysis.getBackpressureStrategy(), n);
        this.startCameraAndTrackStates();
    }
    
    @MainThread
    public void setImageAnalysisTargetSize(@Nullable final OutputSize mImageAnalysisTargetSize) {
        Threads.checkMainThread();
        if (this.isOutputSizeEqual(this.mImageAnalysisTargetSize, mImageAnalysisTargetSize)) {
            return;
        }
        this.mImageAnalysisTargetSize = mImageAnalysisTargetSize;
        this.unbindImageAnalysisAndRecreate(this.mImageAnalysis.getBackpressureStrategy(), this.mImageAnalysis.getImageQueueDepth());
        this.startCameraAndTrackStates();
    }
    
    @MainThread
    public void setImageCaptureFlashMode(final int flashMode) {
        Threads.checkMainThread();
        this.mImageCapture.setFlashMode(flashMode);
    }
    
    @MainThread
    public void setImageCaptureIoExecutor(@Nullable final Executor mImageCaptureIoExecutor) {
        Threads.checkMainThread();
        if (this.mImageCaptureIoExecutor == mImageCaptureIoExecutor) {
            return;
        }
        this.mImageCaptureIoExecutor = mImageCaptureIoExecutor;
        this.unbindImageCaptureAndRecreate(this.mImageCapture.getCaptureMode());
        this.startCameraAndTrackStates();
    }
    
    @MainThread
    public void setImageCaptureMode(final int n) {
        Threads.checkMainThread();
        if (this.mImageCapture.getCaptureMode() == n) {
            return;
        }
        this.unbindImageCaptureAndRecreate(n);
        this.startCameraAndTrackStates();
    }
    
    @MainThread
    public void setImageCaptureTargetSize(@Nullable final OutputSize mImageCaptureTargetSize) {
        Threads.checkMainThread();
        if (this.isOutputSizeEqual(this.mImageCaptureTargetSize, mImageCaptureTargetSize)) {
            return;
        }
        this.mImageCaptureTargetSize = mImageCaptureTargetSize;
        this.unbindImageCaptureAndRecreate(this.getImageCaptureMode());
        this.startCameraAndTrackStates();
    }
    
    @MainThread
    @NonNull
    public ListenableFuture<Void> setLinearZoom(@FloatRange(from = 0.0, to = 1.0) final float linearZoom) {
        Threads.checkMainThread();
        if (!this.isCameraAttached()) {
            Logger.w("CameraController", "Use cases not attached to camera.");
            return Futures.immediateFuture((Void)null);
        }
        return this.mCamera.getCameraControl().setLinearZoom(linearZoom);
    }
    
    @MainThread
    public void setPinchToZoomEnabled(final boolean mPinchToZoomEnabled) {
        Threads.checkMainThread();
        this.mPinchToZoomEnabled = mPinchToZoomEnabled;
    }
    
    @MainThread
    public void setPreviewTargetSize(@Nullable final OutputSize mPreviewTargetSize) {
        Threads.checkMainThread();
        if (this.isOutputSizeEqual(this.mPreviewTargetSize, mPreviewTargetSize)) {
            return;
        }
        this.mPreviewTargetSize = mPreviewTargetSize;
        this.unbindPreviewAndRecreate();
        this.startCameraAndTrackStates();
    }
    
    @MainThread
    public void setTapToFocusEnabled(final boolean mTapToFocusEnabled) {
        Threads.checkMainThread();
        this.mTapToFocusEnabled = mTapToFocusEnabled;
    }
    
    @MainThread
    @ExperimentalVideo
    public void setVideoCaptureTargetSize(@Nullable final OutputSize mVideoCaptureOutputSize) {
        Threads.checkMainThread();
        if (this.isOutputSizeEqual(this.mVideoCaptureOutputSize, mVideoCaptureOutputSize)) {
            return;
        }
        this.mVideoCaptureOutputSize = mVideoCaptureOutputSize;
        this.unbindVideoAndRecreate();
        this.startCameraAndTrackStates();
    }
    
    @MainThread
    @NonNull
    public ListenableFuture<Void> setZoomRatio(final float zoomRatio) {
        Threads.checkMainThread();
        if (!this.isCameraAttached()) {
            Logger.w("CameraController", "Use cases not attached to camera.");
            return Futures.immediateFuture((Void)null);
        }
        return this.mCamera.getCameraControl().setZoomRatio(zoomRatio);
    }
    
    @Nullable
    abstract Camera startCamera();
    
    void startCameraAndTrackStates() {
        this.startCameraAndTrackStates(null);
    }
    
    void startCameraAndTrackStates(@Nullable final Runnable runnable) {
        try {
            this.mCamera = this.startCamera();
            if (!this.isCameraAttached()) {
                Logger.d("CameraController", "Use cases not attached to camera.");
                return;
            }
            this.mZoomState.setSource(this.mCamera.getCameraInfo().getZoomState());
            this.mTorchState.setSource(this.mCamera.getCameraInfo().getTorchState());
        }
        catch (final IllegalArgumentException cause) {
            if (runnable != null) {
                runnable.run();
            }
            throw new IllegalStateException("The selected camera does not support the enabled use cases. Please disable use case and/or select a different camera. e.g. #setVideoCaptureEnabled(false)", cause);
        }
    }
    
    @SuppressLint({ "MissingPermission" })
    @MainThread
    @ExperimentalVideo
    public void startRecording(@NonNull final OutputFileOptions outputFileOptions, @NonNull final Executor executor, @NonNull final OnVideoSavedCallback onVideoSavedCallback) {
        Threads.checkMainThread();
        Preconditions.checkState(this.isCameraInitialized(), "Camera not initialized.");
        Preconditions.checkState(this.isVideoCaptureEnabled(), "VideoCapture disabled.");
        this.mVideoCapture.startRecording(outputFileOptions.toVideoCaptureOutputFileOptions(), executor, (VideoCapture.OnVideoSavedCallback)new VideoCapture.OnVideoSavedCallback(this, onVideoSavedCallback) {
            final CameraController this$0;
            final androidx.camera.view.video.OnVideoSavedCallback val$callback;
            
            @Override
            public void onError(final int n, @NonNull final String s, @Nullable final Throwable t) {
                this.this$0.mVideoIsRecording.set(false);
                this.val$callback.onError(n, s, t);
            }
            
            @Override
            public void onVideoSaved(@NonNull final OutputFileResults outputFileResults) {
                this.this$0.mVideoIsRecording.set(false);
                this.val$callback.onVideoSaved(androidx.camera.view.video.OutputFileResults.create(outputFileResults.getSavedUri()));
            }
        });
        this.mVideoIsRecording.set(true);
    }
    
    @MainThread
    @ExperimentalVideo
    public void stopRecording() {
        Threads.checkMainThread();
        if (this.mVideoIsRecording.get()) {
            this.mVideoCapture.stopRecording();
        }
    }
    
    @MainThread
    public void takePicture(@NonNull final ImageCapture.OutputFileOptions outputFileOptions, @NonNull final Executor executor, @NonNull final ImageCapture.OnImageSavedCallback onImageSavedCallback) {
        Threads.checkMainThread();
        Preconditions.checkState(this.isCameraInitialized(), "Camera not initialized.");
        Preconditions.checkState(this.isImageCaptureEnabled(), "ImageCapture disabled.");
        this.updateMirroringFlagInOutputFileOptions(outputFileOptions);
        this.mImageCapture.takePicture(outputFileOptions, executor, onImageSavedCallback);
    }
    
    @MainThread
    public void takePicture(@NonNull final Executor executor, @NonNull final ImageCapture.OnImageCapturedCallback onImageCapturedCallback) {
        Threads.checkMainThread();
        Preconditions.checkState(this.isCameraInitialized(), "Camera not initialized.");
        Preconditions.checkState(this.isImageCaptureEnabled(), "ImageCapture disabled.");
        this.mImageCapture.takePicture(executor, onImageCapturedCallback);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @VisibleForTesting
    void updateMirroringFlagInOutputFileOptions(@NonNull final ImageCapture.OutputFileOptions outputFileOptions) {
        if (this.mCameraSelector.getLensFacing() != null && !outputFileOptions.getMetadata().isReversedHorizontalSet()) {
            outputFileOptions.getMetadata().setReversedHorizontal(this.mCameraSelector.getLensFacing() == 0);
        }
    }
    
    @MainThread
    @OptIn(markerClass = { TransformExperimental.class })
    void updatePreviewViewTransform(@Nullable final OutputTransform outputTransform) {
        Threads.checkMainThread();
        final ImageAnalysis.Analyzer mAnalysisAnalyzer = this.mAnalysisAnalyzer;
        if (mAnalysisAnalyzer == null) {
            return;
        }
        if (outputTransform == null) {
            mAnalysisAnalyzer.updateTransform(null);
        }
        else if (mAnalysisAnalyzer.getTargetCoordinateSystem() == 1) {
            this.mAnalysisAnalyzer.updateTransform(outputTransform.getMatrix());
        }
    }
    
    @RequiresApi(30)
    private static class Api30Impl
    {
        @DoNotInline
        @NonNull
        static Context createAttributionContext(@NonNull final Context context, @Nullable final String s) {
            return Oo08.\u3007080(context, s);
        }
        
        @DoNotInline
        @Nullable
        static String getAttributionTag(@NonNull final Context context) {
            return androidx.camera.core.impl.utils.O8.\u3007080(context);
        }
    }
    
    @RequiresApi(21)
    public static final class OutputSize
    {
        public static final int UNASSIGNED_ASPECT_RATIO = -1;
        private final int mAspectRatio;
        @Nullable
        private final Size mResolution;
        
        public OutputSize(final int mAspectRatio) {
            Preconditions.checkArgument(mAspectRatio != -1);
            this.mAspectRatio = mAspectRatio;
            this.mResolution = null;
        }
        
        public OutputSize(@NonNull final Size mResolution) {
            Preconditions.checkNotNull(mResolution);
            this.mAspectRatio = -1;
            this.mResolution = mResolution;
        }
        
        public int getAspectRatio() {
            return this.mAspectRatio;
        }
        
        @Nullable
        public Size getResolution() {
            return this.mResolution;
        }
        
        @NonNull
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("aspect ratio: ");
            sb.append(this.mAspectRatio);
            sb.append(" resolution: ");
            sb.append(this.mResolution);
            return sb.toString();
        }
        
        @Retention(RetentionPolicy.SOURCE)
        @RestrictTo({ RestrictTo.Scope.LIBRARY })
        public @interface OutputAspectRatio {
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface TapToFocusStates {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @OptIn(markerClass = { ExperimentalVideo.class })
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface UseCases {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view;

import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.Executor;
import androidx.camera.core.SurfaceRequest;
import android.view.View;
import android.graphics.Bitmap;
import androidx.annotation.Nullable;
import android.util.Size;
import androidx.annotation.NonNull;
import android.widget.FrameLayout;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
abstract class PreviewViewImplementation
{
    @NonNull
    FrameLayout mParent;
    @NonNull
    private final PreviewTransformation mPreviewTransform;
    @Nullable
    Size mResolution;
    private boolean mWasSurfaceProvided;
    
    PreviewViewImplementation(@NonNull final FrameLayout mParent, @NonNull final PreviewTransformation mPreviewTransform) {
        this.mWasSurfaceProvided = false;
        this.mParent = mParent;
        this.mPreviewTransform = mPreviewTransform;
    }
    
    @Nullable
    Bitmap getBitmap() {
        final Bitmap previewBitmap = this.getPreviewBitmap();
        if (previewBitmap == null) {
            return null;
        }
        return this.mPreviewTransform.createTransformedBitmap(previewBitmap, new Size(((View)this.mParent).getWidth(), ((View)this.mParent).getHeight()), ((View)this.mParent).getLayoutDirection());
    }
    
    @Nullable
    abstract View getPreview();
    
    @Nullable
    abstract Bitmap getPreviewBitmap();
    
    abstract void initializePreview();
    
    abstract void onAttachedToWindow();
    
    abstract void onDetachedFromWindow();
    
    void onSurfaceProvided() {
        this.mWasSurfaceProvided = true;
        this.redrawPreview();
    }
    
    abstract void onSurfaceRequested(@NonNull final SurfaceRequest p0, @Nullable final OnSurfaceNotInUseListener p1);
    
    void redrawPreview() {
        final View preview = this.getPreview();
        if (preview != null) {
            if (this.mWasSurfaceProvided) {
                this.mPreviewTransform.transformView(new Size(((View)this.mParent).getWidth(), ((View)this.mParent).getHeight()), ((View)this.mParent).getLayoutDirection(), preview);
            }
        }
    }
    
    void setFrameUpdateListener(@NonNull final Executor executor, @NonNull final PreviewView.OnFrameUpdateListener onFrameUpdateListener) {
    }
    
    @NonNull
    abstract ListenableFuture<Void> waitForNextFrame();
    
    interface OnSurfaceNotInUseListener
    {
        void onSurfaceNotInUse();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view;

import androidx.annotation.RequiresPermission;
import androidx.camera.core.UseCaseGroup;
import androidx.camera.core.Camera;
import androidx.annotation.RestrictTo;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.annotation.MainThread;
import android.annotation.SuppressLint;
import androidx.camera.core.impl.utils.Threads;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleOwner;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
public final class LifecycleCameraController extends CameraController
{
    private static final String TAG = "CamLifecycleController";
    @Nullable
    private LifecycleOwner mLifecycleOwner;
    
    public LifecycleCameraController(@NonNull final Context context) {
        super(context);
    }
    
    @SuppressLint({ "MissingPermission" })
    @MainThread
    public void bindToLifecycle(@NonNull final LifecycleOwner mLifecycleOwner) {
        Threads.checkMainThread();
        this.mLifecycleOwner = mLifecycleOwner;
        this.startCameraAndTrackStates();
    }
    
    @RestrictTo({ RestrictTo.Scope.TESTS })
    void shutDownForTests() {
        final ProcessCameraProvider mCameraProvider = super.mCameraProvider;
        if (mCameraProvider != null) {
            mCameraProvider.unbindAll();
            super.mCameraProvider.shutdown();
        }
    }
    
    @Nullable
    @RequiresPermission("android.permission.CAMERA")
    @Override
    Camera startCamera() {
        if (this.mLifecycleOwner == null) {
            return null;
        }
        if (super.mCameraProvider == null) {
            return null;
        }
        final UseCaseGroup useCaseGroup = this.createUseCaseGroup();
        if (useCaseGroup == null) {
            return null;
        }
        return super.mCameraProvider.bindToLifecycle(this.mLifecycleOwner, super.mCameraSelector, useCaseGroup);
    }
    
    @MainThread
    public void unbind() {
        Threads.checkMainThread();
        this.mLifecycleOwner = null;
        super.mCamera = null;
        final ProcessCameraProvider mCameraProvider = super.mCameraProvider;
        if (mCameraProvider != null) {
            mCameraProvider.unbindAll();
        }
    }
}

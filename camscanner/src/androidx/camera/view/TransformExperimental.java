// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view;

import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresOptIn;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.CLASS)
@RequiresOptIn
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public @interface TransformExperimental {
}

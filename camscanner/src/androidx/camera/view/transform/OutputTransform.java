// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view.transform;

import androidx.annotation.RestrictTo;
import android.util.Size;
import androidx.annotation.NonNull;
import android.graphics.Matrix;
import androidx.camera.view.TransformExperimental;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
@TransformExperimental
public final class OutputTransform
{
    @NonNull
    final Matrix mMatrix;
    @NonNull
    final Size mViewPortSize;
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public OutputTransform(@NonNull final Matrix mMatrix, @NonNull final Size mViewPortSize) {
        this.mMatrix = mMatrix;
        this.mViewPortSize = mViewPortSize;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public Matrix getMatrix() {
        return this.mMatrix;
    }
    
    @NonNull
    Size getViewPortSize() {
        return this.mViewPortSize;
    }
}

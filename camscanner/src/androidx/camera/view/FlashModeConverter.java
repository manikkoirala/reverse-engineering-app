// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
final class FlashModeConverter
{
    private FlashModeConverter() {
    }
    
    @NonNull
    public static String nameOf(final int i) {
        if (i == 0) {
            return "AUTO";
        }
        if (i == 1) {
            return "ON";
        }
        if (i == 2) {
            return "OFF";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unknown flash mode ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static int valueOf(@Nullable final String str) {
        if (str == null) {
            throw new NullPointerException("name cannot be null");
        }
        final int hashCode = str.hashCode();
        int n = -1;
        switch (hashCode) {
            case 2020783: {
                if (!str.equals("AUTO")) {
                    break;
                }
                n = 2;
                break;
            }
            case 78159: {
                if (!str.equals("OFF")) {
                    break;
                }
                n = 1;
                break;
            }
            case 2527: {
                if (!str.equals("ON")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        switch (n) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown flash mode name ");
                sb.append(str);
                throw new IllegalArgumentException(sb.toString());
            }
            case 2: {
                return 0;
            }
            case 1: {
                return 2;
            }
            case 0: {
                return 1;
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.camera.view;

import androidx.annotation.UiThread;
import androidx.camera.core.impl.utils.Threads;
import android.util.Size;
import androidx.annotation.AnyThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.GuardedBy;
import android.graphics.Matrix;
import android.graphics.PointF;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;
import androidx.camera.core.MeteringPointFactory;

@RequiresApi(21)
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
class PreviewViewMeteringPointFactory extends MeteringPointFactory
{
    static final PointF INVALID_POINT;
    @GuardedBy("this")
    @Nullable
    private Matrix mMatrix;
    @NonNull
    private final PreviewTransformation mPreviewTransformation;
    
    static {
        INVALID_POINT = new PointF(2.0f, 2.0f);
    }
    
    PreviewViewMeteringPointFactory(@NonNull final PreviewTransformation mPreviewTransformation) {
        this.mPreviewTransformation = mPreviewTransformation;
    }
    
    @AnyThread
    @NonNull
    @Override
    protected PointF convertPoint(final float n, final float n2) {
        final float[] array = { n, n2 };
        synchronized (this) {
            final Matrix mMatrix = this.mMatrix;
            if (mMatrix == null) {
                return PreviewViewMeteringPointFactory.INVALID_POINT;
            }
            mMatrix.mapPoints(array);
            monitorexit(this);
            return new PointF(array[0], array[1]);
        }
    }
    
    @UiThread
    void recalculate(@NonNull final Size size, final int n) {
        Threads.checkMainThread();
        synchronized (this) {
            if (size.getWidth() != 0 && size.getHeight() != 0) {
                this.mMatrix = this.mPreviewTransformation.getPreviewViewToNormalizedSurfaceMatrix(size, n);
                return;
            }
            this.mMatrix = null;
        }
    }
}

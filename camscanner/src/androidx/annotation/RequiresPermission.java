// 
// Decompiled by Procyon v0.6.0
// 

package androidx.annotation;

import kotlin.Metadata;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.lang.annotation.Documented;

@Documented
@Retention(RetentionPolicy.CLASS)
@Target({ ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.CONSTRUCTOR, ElementType.ANNOTATION_TYPE })
@Metadata
public @interface RequiresPermission {
    String[] allOf() default {};
    
    String[] anyOf() default {};
    
    boolean conditional() default false;
    
    String value() default "";
    
    @Retention(RetentionPolicy.RUNTIME)
    @Target({ ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER })
    @Metadata
    public @interface Read {
        RequiresPermission value() default @RequiresPermission;
    }
    
    @Retention(RetentionPolicy.RUNTIME)
    @Target({ ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER })
    @Metadata
    public @interface Write {
        RequiresPermission value() default @RequiresPermission;
    }
}

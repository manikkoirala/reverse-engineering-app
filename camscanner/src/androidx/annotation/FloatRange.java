// 
// Decompiled by Procyon v0.6.0
// 

package androidx.annotation;

import kotlin.Metadata;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.lang.annotation.Documented;

@Documented
@Retention(RetentionPolicy.CLASS)
@Target({ ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.LOCAL_VARIABLE, ElementType.ANNOTATION_TYPE })
@Metadata
public @interface FloatRange {
    double from() default Double.NEGATIVE_INFINITY;
    
    boolean fromInclusive() default true;
    
    double to() default Double.POSITIVE_INFINITY;
    
    boolean toInclusive() default true;
}

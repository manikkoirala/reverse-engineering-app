// 
// Decompiled by Procyon v0.6.0
// 

package androidx.annotation;

import kotlin.Metadata;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.lang.annotation.Documented;

@Documented
@Retention(RetentionPolicy.CLASS)
@Target({ ElementType.FIELD, ElementType.METHOD })
@Metadata
public @interface ChecksSdkIntAtLeast {
    int api() default -1;
    
    String codename() default "";
    
    int extension() default 0;
    
    int lambda() default -1;
    
    int parameter() default -1;
}

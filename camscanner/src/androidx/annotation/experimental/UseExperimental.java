// 
// Decompiled by Procyon v0.6.0
// 

package androidx.annotation.experimental;

import java.lang.annotation.Annotation;
import kotlin.Metadata;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.CLASS)
@Target({ ElementType.TYPE, ElementType.METHOD, ElementType.PARAMETER, ElementType.CONSTRUCTOR, ElementType.LOCAL_VARIABLE })
@Metadata
public @interface UseExperimental {
    Class<? extends Annotation>[] markerClass();
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.annotation;

import kotlin.Metadata;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.CLASS)
@Target({ ElementType.ANNOTATION_TYPE })
@Metadata
public @interface RequiresOptIn {
    Level level() default Level.ERROR;
    
    @Metadata
    public enum Level
    {
        private static final Level[] $VALUES;
        
        ERROR, 
        WARNING;
        
        private static final /* synthetic */ Level[] $values() {
            return new Level[] { Level.WARNING, Level.ERROR };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

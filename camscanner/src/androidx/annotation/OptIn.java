// 
// Decompiled by Procyon v0.6.0
// 

package androidx.annotation;

import java.lang.annotation.Annotation;
import kotlin.Metadata;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.CLASS)
@Target({ ElementType.CONSTRUCTOR, ElementType.FIELD, ElementType.LOCAL_VARIABLE, ElementType.METHOD, ElementType.PACKAGE, ElementType.TYPE })
@Metadata
public @interface OptIn {
    Class<? extends Annotation>[] markerClass();
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.annotation;

import kotlin.Metadata;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.SOURCE)
@Target({ ElementType.METHOD })
@Metadata
public @interface InspectableProperty {
    int attributeId() default 0;
    
    EnumEntry[] enumMapping() default {};
    
    FlagEntry[] flagMapping() default {};
    
    boolean hasAttributeId() default true;
    
    String name() default "";
    
    ValueType valueType() default ValueType.INFERRED;
    
    @Retention(RetentionPolicy.SOURCE)
    @Target({ ElementType.TYPE, ElementType.ANNOTATION_TYPE })
    @Metadata
    public @interface EnumEntry {
        String name();
        
        int value();
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @Target({ ElementType.TYPE, ElementType.ANNOTATION_TYPE })
    @Metadata
    public @interface FlagEntry {
        int mask() default 0;
        
        String name();
        
        int target();
    }
    
    @Metadata
    public enum ValueType
    {
        private static final ValueType[] $VALUES;
        
        COLOR, 
        GRAVITY, 
        INFERRED, 
        INT_ENUM, 
        INT_FLAG, 
        NONE, 
        RESOURCE_ID;
        
        private static final /* synthetic */ ValueType[] $values() {
            return new ValueType[] { ValueType.NONE, ValueType.INFERRED, ValueType.INT_ENUM, ValueType.INT_FLAG, ValueType.COLOR, ValueType.GRAVITY, ValueType.RESOURCE_ID };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

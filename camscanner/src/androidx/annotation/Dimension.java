// 
// Decompiled by Procyon v0.6.0
// 

package androidx.annotation;

import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.lang.annotation.Documented;

@Documented
@Retention(RetentionPolicy.CLASS)
@Target({ ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.LOCAL_VARIABLE, ElementType.ANNOTATION_TYPE })
@Metadata
public @interface Dimension {
    @NotNull
    public static final Companion Companion = Dimension.Companion.$$INSTANCE;
    public static final int DP = 0;
    public static final int PX = 1;
    public static final int SP = 2;
    
    int unit() default 1;
    
    @Metadata
    public static final class Companion
    {
        static final Companion $$INSTANCE;
        public static final int DP = 0;
        public static final int PX = 1;
        public static final int SP = 2;
        
        static {
            $$INSTANCE = new Companion();
        }
        
        private Companion() {
        }
    }
}

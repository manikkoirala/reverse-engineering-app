// 
// Decompiled by Procyon v0.6.0
// 

package androidx.annotation;

import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.CLASS)
@Metadata
public @interface VisibleForTesting {
    @NotNull
    public static final Companion Companion = VisibleForTesting.Companion.$$INSTANCE;
    public static final int NONE = 5;
    public static final int PACKAGE_PRIVATE = 3;
    public static final int PRIVATE = 2;
    public static final int PROTECTED = 4;
    
    int otherwise() default 2;
    
    @Metadata
    public static final class Companion
    {
        static final Companion $$INSTANCE;
        public static final int NONE = 5;
        public static final int PACKAGE_PRIVATE = 3;
        public static final int PRIVATE = 2;
        public static final int PROTECTED = 4;
        
        static {
            $$INSTANCE = new Companion();
        }
        
        private Companion() {
        }
    }
}

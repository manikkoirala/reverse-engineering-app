// 
// Decompiled by Procyon v0.6.0
// 

package androidx.annotation;

import kotlin.Metadata;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.lang.annotation.Documented;

@Documented
@Retention(RetentionPolicy.CLASS)
@Target({ ElementType.ANNOTATION_TYPE, ElementType.TYPE, ElementType.METHOD, ElementType.CONSTRUCTOR, ElementType.FIELD, ElementType.PACKAGE })
@Metadata
public @interface RestrictTo {
    Scope[] value();
    
    @Metadata
    public enum Scope
    {
        private static final Scope[] $VALUES;
        
        GROUP_ID, 
        LIBRARY, 
        LIBRARY_GROUP, 
        LIBRARY_GROUP_PREFIX, 
        SUBCLASSES, 
        TESTS;
        
        private static final /* synthetic */ Scope[] $values() {
            return new Scope[] { Scope.LIBRARY, Scope.LIBRARY_GROUP, Scope.LIBRARY_GROUP_PREFIX, Scope.GROUP_ID, Scope.TESTS, Scope.SUBCLASSES };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

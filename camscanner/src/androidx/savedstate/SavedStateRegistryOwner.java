// 
// Decompiled by Procyon v0.6.0
// 

package androidx.savedstate;

import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;
import androidx.lifecycle.LifecycleOwner;

@Metadata
public interface SavedStateRegistryOwner extends LifecycleOwner
{
    @NotNull
    SavedStateRegistry getSavedStateRegistry();
}

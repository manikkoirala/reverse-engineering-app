// 
// Decompiled by Procyon v0.6.0
// 

package androidx.savedstate;

import kotlin.jvm.functions.Function1;
import kotlin.sequences.SequencesKt;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.view.View;
import kotlin.Metadata;

@Metadata
public final class ViewTreeSavedStateRegistryOwner
{
    public static final SavedStateRegistryOwner get(@NotNull final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        return (SavedStateRegistryOwner)SequencesKt.OO0o\u3007\u3007(SequencesKt.\u3007\u30078O0\u30078(SequencesKt.o\u30070((Object)view, (Function1)ViewTreeSavedStateRegistryOwner$findViewTreeSavedStateRegistryOwner.ViewTreeSavedStateRegistryOwner$findViewTreeSavedStateRegistryOwner$1.INSTANCE), (Function1)ViewTreeSavedStateRegistryOwner$findViewTreeSavedStateRegistryOwner.ViewTreeSavedStateRegistryOwner$findViewTreeSavedStateRegistryOwner$2.INSTANCE));
    }
    
    public static final void set(@NotNull final View view, final SavedStateRegistryOwner savedStateRegistryOwner) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        view.setTag(R.id.view_tree_saved_state_registry_owner, (Object)savedStateRegistryOwner);
    }
}

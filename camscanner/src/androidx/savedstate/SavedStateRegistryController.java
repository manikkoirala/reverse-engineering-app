// 
// Decompiled by Procyon v0.6.0
// 

package androidx.savedstate;

import android.os.Bundle;
import androidx.annotation.MainThread;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.Lifecycle;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class SavedStateRegistryController
{
    @NotNull
    public static final Companion Companion;
    private boolean attached;
    @NotNull
    private final SavedStateRegistryOwner owner;
    @NotNull
    private final SavedStateRegistry savedStateRegistry;
    
    static {
        Companion = new Companion(null);
    }
    
    private SavedStateRegistryController(final SavedStateRegistryOwner owner) {
        this.owner = owner;
        this.savedStateRegistry = new SavedStateRegistry();
    }
    
    @NotNull
    public static final SavedStateRegistryController create(@NotNull final SavedStateRegistryOwner savedStateRegistryOwner) {
        return SavedStateRegistryController.Companion.create(savedStateRegistryOwner);
    }
    
    @NotNull
    public final SavedStateRegistry getSavedStateRegistry() {
        return this.savedStateRegistry;
    }
    
    @MainThread
    public final void performAttach() {
        final Lifecycle lifecycle = this.owner.getLifecycle();
        Intrinsics.checkNotNullExpressionValue((Object)lifecycle, "owner.lifecycle");
        if (lifecycle.getCurrentState() == Lifecycle.State.INITIALIZED) {
            lifecycle.addObserver(new Recreator(this.owner));
            this.savedStateRegistry.performAttach$savedstate_release(lifecycle);
            this.attached = true;
            return;
        }
        throw new IllegalStateException("Restarter must be created only during owner's initialization stage".toString());
    }
    
    @MainThread
    public final void performRestore(final Bundle bundle) {
        if (!this.attached) {
            this.performAttach();
        }
        final Lifecycle lifecycle = this.owner.getLifecycle();
        Intrinsics.checkNotNullExpressionValue((Object)lifecycle, "owner.lifecycle");
        if (lifecycle.getCurrentState().isAtLeast(Lifecycle.State.STARTED) ^ true) {
            this.savedStateRegistry.performRestore$savedstate_release(bundle);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("performRestore cannot be called when owner is ");
        sb.append(lifecycle.getCurrentState());
        throw new IllegalStateException(sb.toString().toString());
    }
    
    @MainThread
    public final void performSave(@NotNull final Bundle bundle) {
        Intrinsics.checkNotNullParameter((Object)bundle, "outBundle");
        this.savedStateRegistry.performSave(bundle);
    }
    
    @Metadata
    public static final class Companion
    {
        private Companion() {
        }
        
        @NotNull
        public final SavedStateRegistryController create(@NotNull final SavedStateRegistryOwner savedStateRegistryOwner) {
            Intrinsics.checkNotNullParameter((Object)savedStateRegistryOwner, "owner");
            return new SavedStateRegistryController(savedStateRegistryOwner, null);
        }
    }
}

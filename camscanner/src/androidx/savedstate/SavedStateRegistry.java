// 
// Decompiled by Procyon v0.6.0
// 

package androidx.savedstate;

import android.os.BaseBundle;
import androidx.lifecycle.LifecycleObserver;
import java.util.Iterator;
import java.util.Map;
import androidx.annotation.MainThread;
import kotlin.jvm.internal.Intrinsics;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import kotlin.jvm.internal.DefaultConstructorMarker;
import android.os.Bundle;
import androidx.arch.core.internal.SafeIterableMap;
import org.jetbrains.annotations.NotNull;
import android.annotation.SuppressLint;
import kotlin.Metadata;

@Metadata
@SuppressLint({ "RestrictedApi" })
public final class SavedStateRegistry
{
    @NotNull
    private static final Companion Companion;
    @Deprecated
    @NotNull
    private static final String SAVED_COMPONENTS_KEY = "androidx.lifecycle.BundlableSavedStateRegistry.key";
    private boolean attached;
    @NotNull
    private final SafeIterableMap<String, SavedStateProvider> components;
    private boolean isAllowingSavingState;
    private boolean isRestored;
    private Recreator.SavedStateProvider recreatorProvider;
    private Bundle restoredState;
    
    static {
        Companion = new Companion(null);
    }
    
    public SavedStateRegistry() {
        this.components = new SafeIterableMap<String, SavedStateProvider>();
        this.isAllowingSavingState = true;
    }
    
    private static final void performAttach$lambda-4(final SavedStateRegistry savedStateRegistry, final LifecycleOwner lifecycleOwner, final Lifecycle.Event event) {
        Intrinsics.checkNotNullParameter((Object)savedStateRegistry, "this$0");
        Intrinsics.checkNotNullParameter((Object)lifecycleOwner, "<anonymous parameter 0>");
        Intrinsics.checkNotNullParameter((Object)event, "event");
        if (event == Lifecycle.Event.ON_START) {
            savedStateRegistry.isAllowingSavingState = true;
        }
        else if (event == Lifecycle.Event.ON_STOP) {
            savedStateRegistry.isAllowingSavingState = false;
        }
    }
    
    @MainThread
    public final Bundle consumeRestoredStateForKey(@NotNull final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        if (!this.isRestored) {
            throw new IllegalStateException("You can consumeRestoredStateForKey only after super.onCreate of corresponding component".toString());
        }
        final Bundle restoredState = this.restoredState;
        if (restoredState != null) {
            Bundle bundle;
            if (restoredState != null) {
                bundle = restoredState.getBundle(s);
            }
            else {
                bundle = null;
            }
            final Bundle restoredState2 = this.restoredState;
            if (restoredState2 != null) {
                restoredState2.remove(s);
            }
            final Bundle restoredState3 = this.restoredState;
            int n = 0;
            if (restoredState3 != null) {
                n = n;
                if (!((BaseBundle)restoredState3).isEmpty()) {
                    n = 1;
                }
            }
            if (n == 0) {
                this.restoredState = null;
            }
            return bundle;
        }
        return null;
    }
    
    public final SavedStateProvider getSavedStateProvider(@NotNull final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        for (final Map.Entry<String, V> entry : this.components) {
            Intrinsics.checkNotNullExpressionValue((Object)entry, "components");
            final String s2 = entry.getKey();
            final SavedStateProvider savedStateProvider = (SavedStateProvider)entry.getValue();
            if (Intrinsics.\u3007o\u3007((Object)s2, (Object)s)) {
                return savedStateProvider;
            }
        }
        return null;
    }
    
    public final boolean isAllowingSavingState$savedstate_release() {
        return this.isAllowingSavingState;
    }
    
    @MainThread
    public final boolean isRestored() {
        return this.isRestored;
    }
    
    @MainThread
    public final void performAttach$savedstate_release(@NotNull final Lifecycle lifecycle) {
        Intrinsics.checkNotNullParameter((Object)lifecycle, "lifecycle");
        if (this.attached ^ true) {
            lifecycle.addObserver(new \u3007080(this));
            this.attached = true;
            return;
        }
        throw new IllegalStateException("SavedStateRegistry was already attached.".toString());
    }
    
    @MainThread
    public final void performRestore$savedstate_release(Bundle bundle) {
        if (!this.attached) {
            throw new IllegalStateException("You must call performAttach() before calling performRestore(Bundle).".toString());
        }
        if (this.isRestored ^ true) {
            if (bundle != null) {
                bundle = bundle.getBundle("androidx.lifecycle.BundlableSavedStateRegistry.key");
            }
            else {
                bundle = null;
            }
            this.restoredState = bundle;
            this.isRestored = true;
            return;
        }
        throw new IllegalStateException("SavedStateRegistry was already restored.".toString());
    }
    
    @MainThread
    public final void performSave(@NotNull final Bundle bundle) {
        Intrinsics.checkNotNullParameter((Object)bundle, "outBundle");
        final Bundle bundle2 = new Bundle();
        final Bundle restoredState = this.restoredState;
        if (restoredState != null) {
            bundle2.putAll(restoredState);
        }
        final SafeIterableMap.IteratorWithAdditions iteratorWithAdditions = this.components.iteratorWithAdditions();
        Intrinsics.checkNotNullExpressionValue((Object)iteratorWithAdditions, "this.components.iteratorWithAdditions()");
        while (iteratorWithAdditions.hasNext()) {
            final Map.Entry<String, V> entry = ((Iterator<Map.Entry<String, V>>)iteratorWithAdditions).next();
            bundle2.putBundle((String)entry.getKey(), ((SavedStateProvider)entry.getValue()).saveState());
        }
        if (!((BaseBundle)bundle2).isEmpty()) {
            bundle.putBundle("androidx.lifecycle.BundlableSavedStateRegistry.key", bundle2);
        }
    }
    
    @MainThread
    public final void registerSavedStateProvider(@NotNull final String s, @NotNull final SavedStateProvider savedStateProvider) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        Intrinsics.checkNotNullParameter((Object)savedStateProvider, "provider");
        if (this.components.putIfAbsent(s, savedStateProvider) == null) {
            return;
        }
        throw new IllegalArgumentException("SavedStateProvider with the given key is already registered".toString());
    }
    
    @MainThread
    public final void runOnNextRecreation(@NotNull Class<? extends AutoRecreated> name) {
        Intrinsics.checkNotNullParameter((Object)name, "clazz");
        if (this.isAllowingSavingState) {
            Recreator.SavedStateProvider recreatorProvider;
            if ((recreatorProvider = this.recreatorProvider) == null) {
                recreatorProvider = new Recreator.SavedStateProvider(this);
            }
            this.recreatorProvider = recreatorProvider;
            try {
                ((Class)name).getDeclaredConstructor((Class[])new Class[0]);
                final Recreator.SavedStateProvider recreatorProvider2 = this.recreatorProvider;
                if (recreatorProvider2 != null) {
                    name = ((Class)name).getName();
                    Intrinsics.checkNotNullExpressionValue((Object)name, "clazz.name");
                    recreatorProvider2.add(name);
                }
                return;
            }
            catch (final NoSuchMethodException cause) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Class ");
                sb.append(((Class)name).getSimpleName());
                sb.append(" must have default constructor in order to be automatically recreated");
                throw new IllegalArgumentException(sb.toString(), cause);
            }
        }
        throw new IllegalStateException("Can not perform this action after onSaveInstanceState".toString());
    }
    
    public final void setAllowingSavingState$savedstate_release(final boolean isAllowingSavingState) {
        this.isAllowingSavingState = isAllowingSavingState;
    }
    
    @MainThread
    public final void unregisterSavedStateProvider(@NotNull final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        this.components.remove(s);
    }
    
    @Metadata
    public interface AutoRecreated
    {
        void onRecreated(@NotNull final SavedStateRegistryOwner p0);
    }
    
    @Metadata
    private static final class Companion
    {
    }
    
    @Metadata
    public interface SavedStateProvider
    {
        @NotNull
        Bundle saveState();
    }
}

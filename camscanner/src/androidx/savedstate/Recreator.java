// 
// Decompiled by Procyon v0.6.0
// 

package androidx.savedstate;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.Iterator;
import java.util.ArrayList;
import android.os.Bundle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import java.lang.reflect.Constructor;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;
import androidx.lifecycle.LifecycleEventObserver;

@Metadata
public final class Recreator implements LifecycleEventObserver
{
    @NotNull
    public static final String CLASSES_KEY = "classes_to_restore";
    @NotNull
    public static final String COMPONENT_KEY = "androidx.savedstate.Restarter";
    @NotNull
    public static final Companion Companion;
    @NotNull
    private final SavedStateRegistryOwner owner;
    
    static {
        Companion = new Companion(null);
    }
    
    public Recreator(@NotNull final SavedStateRegistryOwner owner) {
        Intrinsics.checkNotNullParameter((Object)owner, "owner");
        this.owner = owner;
    }
    
    private final void reflectiveNew(String str) {
        try {
            final Class<? extends SavedStateRegistry.AutoRecreated> subclass = Class.forName(str, false, Recreator.class.getClassLoader()).asSubclass(SavedStateRegistry.AutoRecreated.class);
            Intrinsics.checkNotNullExpressionValue((Object)subclass, "{\n                Class.\u2026class.java)\n            }");
            try {
                final Constructor declaredConstructor = subclass.getDeclaredConstructor((Class<?>[])new Class[0]);
                declaredConstructor.setAccessible(true);
                try {
                    final Object instance = declaredConstructor.newInstance(new Object[0]);
                    Intrinsics.checkNotNullExpressionValue(instance, "{\n                constr\u2026wInstance()\n            }");
                    ((SavedStateRegistry.AutoRecreated)instance).onRecreated(this.owner);
                }
                catch (final Exception subclass) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to instantiate ");
                    sb.append(str);
                    throw new RuntimeException(sb.toString(), (Throwable)subclass);
                }
            }
            catch (final NoSuchMethodException cause) {
                str = (String)new StringBuilder();
                ((StringBuilder)str).append("Class ");
                ((StringBuilder)str).append(subclass.getSimpleName());
                ((StringBuilder)str).append(" must have default constructor in order to be automatically recreated");
                throw new IllegalStateException(((StringBuilder)str).toString(), cause);
            }
        }
        catch (final ClassNotFoundException cause2) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Class ");
            sb2.append(str);
            sb2.append(" wasn't found");
            throw new RuntimeException(sb2.toString(), cause2);
        }
    }
    
    @Override
    public void onStateChanged(@NotNull final LifecycleOwner lifecycleOwner, @NotNull final Lifecycle.Event event) {
        Intrinsics.checkNotNullParameter((Object)lifecycleOwner, "source");
        Intrinsics.checkNotNullParameter((Object)event, "event");
        if (event != Lifecycle.Event.ON_CREATE) {
            throw new AssertionError((Object)"Next event must be ON_CREATE");
        }
        lifecycleOwner.getLifecycle().removeObserver(this);
        final Bundle consumeRestoredStateForKey = this.owner.getSavedStateRegistry().consumeRestoredStateForKey("androidx.savedstate.Restarter");
        if (consumeRestoredStateForKey == null) {
            return;
        }
        final ArrayList stringArrayList = consumeRestoredStateForKey.getStringArrayList("classes_to_restore");
        if (stringArrayList != null) {
            final Iterator iterator = stringArrayList.iterator();
            while (iterator.hasNext()) {
                this.reflectiveNew((String)iterator.next());
            }
            return;
        }
        throw new IllegalStateException("Bundle with restored state for the component \"androidx.savedstate.Restarter\" must contain list of strings by the key \"classes_to_restore\"");
    }
    
    @Metadata
    public static final class Companion
    {
        private Companion() {
        }
    }
    
    @Metadata
    public static final class SavedStateProvider implements SavedStateRegistry.SavedStateProvider
    {
        @NotNull
        private final Set<String> classes;
        
        public SavedStateProvider(@NotNull final SavedStateRegistry savedStateRegistry) {
            Intrinsics.checkNotNullParameter((Object)savedStateRegistry, "registry");
            this.classes = new LinkedHashSet<String>();
            savedStateRegistry.registerSavedStateProvider("androidx.savedstate.Restarter", (SavedStateRegistry.SavedStateProvider)this);
        }
        
        public final void add(@NotNull final String s) {
            Intrinsics.checkNotNullParameter((Object)s, "className");
            this.classes.add(s);
        }
        
        @NotNull
        @Override
        public Bundle saveState() {
            final Bundle bundle = new Bundle();
            bundle.putStringArrayList("classes_to_restore", new ArrayList((Collection<? extends E>)this.classes));
            return bundle;
        }
    }
}

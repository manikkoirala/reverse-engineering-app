// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.Rect;
import android.graphics.Path;
import kotlin.jvm.internal.InlineMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import org.jetbrains.annotations.NotNull;
import android.graphics.Canvas;
import kotlin.Metadata;

@Metadata
public final class CanvasKt
{
    public static final void withClip(@NotNull final Canvas canvas, final float n, final float n2, final float n3, final float n4, @NotNull final Function1<? super Canvas, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)canvas, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final int save = canvas.save();
        canvas.clipRect(n, n2, n3, n4);
        try {
            function1.invoke((Object)canvas);
        }
        finally {
            InlineMarker.\u3007o00\u3007\u3007Oo(1);
            canvas.restoreToCount(save);
            InlineMarker.\u3007080(1);
        }
    }
    
    public static final void withClip(@NotNull final Canvas canvas, final int n, final int n2, final int n3, final int n4, @NotNull final Function1<? super Canvas, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)canvas, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final int save = canvas.save();
        canvas.clipRect(n, n2, n3, n4);
        try {
            function1.invoke((Object)canvas);
        }
        finally {
            InlineMarker.\u3007o00\u3007\u3007Oo(1);
            canvas.restoreToCount(save);
            InlineMarker.\u3007080(1);
        }
    }
    
    public static final void withClip(@NotNull final Canvas canvas, @NotNull final Path path, @NotNull final Function1<? super Canvas, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)canvas, "<this>");
        Intrinsics.checkNotNullParameter((Object)path, "clipPath");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final int save = canvas.save();
        canvas.clipPath(path);
        try {
            function1.invoke((Object)canvas);
        }
        finally {
            InlineMarker.\u3007o00\u3007\u3007Oo(1);
            canvas.restoreToCount(save);
            InlineMarker.\u3007080(1);
        }
    }
    
    public static final void withClip(@NotNull final Canvas canvas, @NotNull final Rect rect, @NotNull final Function1<? super Canvas, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)canvas, "<this>");
        Intrinsics.checkNotNullParameter((Object)rect, "clipRect");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final int save = canvas.save();
        canvas.clipRect(rect);
        try {
            function1.invoke((Object)canvas);
        }
        finally {
            InlineMarker.\u3007o00\u3007\u3007Oo(1);
            canvas.restoreToCount(save);
            InlineMarker.\u3007080(1);
        }
    }
    
    public static final void withClip(@NotNull final Canvas canvas, @NotNull final RectF rectF, @NotNull final Function1<? super Canvas, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)canvas, "<this>");
        Intrinsics.checkNotNullParameter((Object)rectF, "clipRect");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final int save = canvas.save();
        canvas.clipRect(rectF);
        try {
            function1.invoke((Object)canvas);
        }
        finally {
            InlineMarker.\u3007o00\u3007\u3007Oo(1);
            canvas.restoreToCount(save);
            InlineMarker.\u3007080(1);
        }
    }
    
    public static final void withMatrix(@NotNull final Canvas canvas, @NotNull final Matrix matrix, @NotNull final Function1<? super Canvas, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)canvas, "<this>");
        Intrinsics.checkNotNullParameter((Object)matrix, "matrix");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final int save = canvas.save();
        canvas.concat(matrix);
        try {
            function1.invoke((Object)canvas);
        }
        finally {
            InlineMarker.\u3007o00\u3007\u3007Oo(1);
            canvas.restoreToCount(save);
            InlineMarker.\u3007080(1);
        }
    }
    
    public static final void withRotation(@NotNull final Canvas canvas, final float n, final float n2, final float n3, @NotNull final Function1<? super Canvas, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)canvas, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final int save = canvas.save();
        canvas.rotate(n, n2, n3);
        try {
            function1.invoke((Object)canvas);
        }
        finally {
            InlineMarker.\u3007o00\u3007\u3007Oo(1);
            canvas.restoreToCount(save);
            InlineMarker.\u3007080(1);
        }
    }
    
    public static final void withSave(@NotNull final Canvas canvas, @NotNull final Function1<? super Canvas, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)canvas, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final int save = canvas.save();
        try {
            function1.invoke((Object)canvas);
        }
        finally {
            InlineMarker.\u3007o00\u3007\u3007Oo(1);
            canvas.restoreToCount(save);
            InlineMarker.\u3007080(1);
        }
    }
    
    public static final void withScale(@NotNull final Canvas canvas, final float n, final float n2, final float n3, final float n4, @NotNull final Function1<? super Canvas, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)canvas, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final int save = canvas.save();
        canvas.scale(n, n2, n3, n4);
        try {
            function1.invoke((Object)canvas);
        }
        finally {
            InlineMarker.\u3007o00\u3007\u3007Oo(1);
            canvas.restoreToCount(save);
            InlineMarker.\u3007080(1);
        }
    }
    
    public static final void withSkew(@NotNull final Canvas canvas, final float n, final float n2, @NotNull final Function1<? super Canvas, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)canvas, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final int save = canvas.save();
        canvas.skew(n, n2);
        try {
            function1.invoke((Object)canvas);
        }
        finally {
            InlineMarker.\u3007o00\u3007\u3007Oo(1);
            canvas.restoreToCount(save);
            InlineMarker.\u3007080(1);
        }
    }
    
    public static final void withTranslation(@NotNull final Canvas canvas, final float n, final float n2, @NotNull final Function1<? super Canvas, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)canvas, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final int save = canvas.save();
        canvas.translate(n, n2);
        try {
            function1.invoke((Object)canvas);
        }
        finally {
            InlineMarker.\u3007o00\u3007\u3007Oo(1);
            canvas.restoreToCount(save);
            InlineMarker.\u3007080(1);
        }
    }
}

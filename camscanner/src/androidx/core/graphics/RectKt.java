// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import android.graphics.Matrix;
import android.graphics.Region$Op;
import android.graphics.Region;
import android.graphics.PointF;
import android.graphics.Point;
import android.graphics.RectF;
import android.annotation.SuppressLint;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.graphics.Rect;
import kotlin.Metadata;

@Metadata
public final class RectKt
{
    @SuppressLint({ "CheckResult" })
    @NotNull
    public static final Rect and(@NotNull Rect rect, @NotNull final Rect rect2) {
        Intrinsics.checkNotNullParameter((Object)rect, "<this>");
        Intrinsics.checkNotNullParameter((Object)rect2, "r");
        rect = new Rect(rect);
        rect.intersect(rect2);
        return rect;
    }
    
    @SuppressLint({ "CheckResult" })
    @NotNull
    public static final RectF and(@NotNull RectF rectF, @NotNull final RectF rectF2) {
        Intrinsics.checkNotNullParameter((Object)rectF, "<this>");
        Intrinsics.checkNotNullParameter((Object)rectF2, "r");
        rectF = new RectF(rectF);
        rectF.intersect(rectF2);
        return rectF;
    }
    
    public static final float component1(@NotNull final RectF rectF) {
        Intrinsics.checkNotNullParameter((Object)rectF, "<this>");
        return rectF.left;
    }
    
    public static final int component1(@NotNull final Rect rect) {
        Intrinsics.checkNotNullParameter((Object)rect, "<this>");
        return rect.left;
    }
    
    public static final float component2(@NotNull final RectF rectF) {
        Intrinsics.checkNotNullParameter((Object)rectF, "<this>");
        return rectF.top;
    }
    
    public static final int component2(@NotNull final Rect rect) {
        Intrinsics.checkNotNullParameter((Object)rect, "<this>");
        return rect.top;
    }
    
    public static final float component3(@NotNull final RectF rectF) {
        Intrinsics.checkNotNullParameter((Object)rectF, "<this>");
        return rectF.right;
    }
    
    public static final int component3(@NotNull final Rect rect) {
        Intrinsics.checkNotNullParameter((Object)rect, "<this>");
        return rect.right;
    }
    
    public static final float component4(@NotNull final RectF rectF) {
        Intrinsics.checkNotNullParameter((Object)rectF, "<this>");
        return rectF.bottom;
    }
    
    public static final int component4(@NotNull final Rect rect) {
        Intrinsics.checkNotNullParameter((Object)rect, "<this>");
        return rect.bottom;
    }
    
    public static final boolean contains(@NotNull final Rect rect, @NotNull final Point point) {
        Intrinsics.checkNotNullParameter((Object)rect, "<this>");
        Intrinsics.checkNotNullParameter((Object)point, "p");
        return rect.contains(point.x, point.y);
    }
    
    public static final boolean contains(@NotNull final RectF rectF, @NotNull final PointF pointF) {
        Intrinsics.checkNotNullParameter((Object)rectF, "<this>");
        Intrinsics.checkNotNullParameter((Object)pointF, "p");
        return rectF.contains(pointF.x, pointF.y);
    }
    
    @NotNull
    public static final Rect minus(@NotNull Rect rect, int n) {
        Intrinsics.checkNotNullParameter((Object)rect, "<this>");
        rect = new Rect(rect);
        n = -n;
        rect.offset(n, n);
        return rect;
    }
    
    @NotNull
    public static final Rect minus(@NotNull Rect rect, @NotNull final Point point) {
        Intrinsics.checkNotNullParameter((Object)rect, "<this>");
        Intrinsics.checkNotNullParameter((Object)point, "xy");
        rect = new Rect(rect);
        rect.offset(-point.x, -point.y);
        return rect;
    }
    
    @NotNull
    public static final RectF minus(@NotNull RectF rectF, float n) {
        Intrinsics.checkNotNullParameter((Object)rectF, "<this>");
        rectF = new RectF(rectF);
        n = -n;
        rectF.offset(n, n);
        return rectF;
    }
    
    @NotNull
    public static final RectF minus(@NotNull RectF rectF, @NotNull final PointF pointF) {
        Intrinsics.checkNotNullParameter((Object)rectF, "<this>");
        Intrinsics.checkNotNullParameter((Object)pointF, "xy");
        rectF = new RectF(rectF);
        rectF.offset(-pointF.x, -pointF.y);
        return rectF;
    }
    
    @NotNull
    public static final Region minus(@NotNull final Rect rect, @NotNull final Rect rect2) {
        Intrinsics.checkNotNullParameter((Object)rect, "<this>");
        Intrinsics.checkNotNullParameter((Object)rect2, "r");
        final Region region = new Region(rect);
        region.op(rect2, Region$Op.DIFFERENCE);
        return region;
    }
    
    @NotNull
    public static final Region minus(@NotNull final RectF rectF, @NotNull final RectF rectF2) {
        Intrinsics.checkNotNullParameter((Object)rectF, "<this>");
        Intrinsics.checkNotNullParameter((Object)rectF2, "r");
        final Rect rect = new Rect();
        rectF.roundOut(rect);
        final Region region = new Region(rect);
        final Rect rect2 = new Rect();
        rectF2.roundOut(rect2);
        region.op(rect2, Region$Op.DIFFERENCE);
        return region;
    }
    
    @NotNull
    public static final Rect or(@NotNull Rect rect, @NotNull final Rect rect2) {
        Intrinsics.checkNotNullParameter((Object)rect, "<this>");
        Intrinsics.checkNotNullParameter((Object)rect2, "r");
        rect = new Rect(rect);
        rect.union(rect2);
        return rect;
    }
    
    @NotNull
    public static final RectF or(@NotNull RectF rectF, @NotNull final RectF rectF2) {
        Intrinsics.checkNotNullParameter((Object)rectF, "<this>");
        Intrinsics.checkNotNullParameter((Object)rectF2, "r");
        rectF = new RectF(rectF);
        rectF.union(rectF2);
        return rectF;
    }
    
    @NotNull
    public static final Rect plus(@NotNull Rect rect, final int n) {
        Intrinsics.checkNotNullParameter((Object)rect, "<this>");
        rect = new Rect(rect);
        rect.offset(n, n);
        return rect;
    }
    
    @NotNull
    public static final Rect plus(@NotNull Rect rect, @NotNull final Point point) {
        Intrinsics.checkNotNullParameter((Object)rect, "<this>");
        Intrinsics.checkNotNullParameter((Object)point, "xy");
        rect = new Rect(rect);
        rect.offset(point.x, point.y);
        return rect;
    }
    
    @NotNull
    public static final Rect plus(@NotNull Rect rect, @NotNull final Rect rect2) {
        Intrinsics.checkNotNullParameter((Object)rect, "<this>");
        Intrinsics.checkNotNullParameter((Object)rect2, "r");
        rect = new Rect(rect);
        rect.union(rect2);
        return rect;
    }
    
    @NotNull
    public static final RectF plus(@NotNull RectF rectF, final float n) {
        Intrinsics.checkNotNullParameter((Object)rectF, "<this>");
        rectF = new RectF(rectF);
        rectF.offset(n, n);
        return rectF;
    }
    
    @NotNull
    public static final RectF plus(@NotNull RectF rectF, @NotNull final PointF pointF) {
        Intrinsics.checkNotNullParameter((Object)rectF, "<this>");
        Intrinsics.checkNotNullParameter((Object)pointF, "xy");
        rectF = new RectF(rectF);
        rectF.offset(pointF.x, pointF.y);
        return rectF;
    }
    
    @NotNull
    public static final RectF plus(@NotNull RectF rectF, @NotNull final RectF rectF2) {
        Intrinsics.checkNotNullParameter((Object)rectF, "<this>");
        Intrinsics.checkNotNullParameter((Object)rectF2, "r");
        rectF = new RectF(rectF);
        rectF.union(rectF2);
        return rectF;
    }
    
    @NotNull
    public static final Rect times(@NotNull Rect rect, final int n) {
        Intrinsics.checkNotNullParameter((Object)rect, "<this>");
        rect = new Rect(rect);
        rect.top *= n;
        rect.left *= n;
        rect.right *= n;
        rect.bottom *= n;
        return rect;
    }
    
    @NotNull
    public static final RectF times(@NotNull RectF rectF, final float n) {
        Intrinsics.checkNotNullParameter((Object)rectF, "<this>");
        rectF = new RectF(rectF);
        rectF.top *= n;
        rectF.left *= n;
        rectF.right *= n;
        rectF.bottom *= n;
        return rectF;
    }
    
    @NotNull
    public static final RectF times(@NotNull RectF rectF, final int n) {
        Intrinsics.checkNotNullParameter((Object)rectF, "<this>");
        final float n2 = (float)n;
        rectF = new RectF(rectF);
        rectF.top *= n2;
        rectF.left *= n2;
        rectF.right *= n2;
        rectF.bottom *= n2;
        return rectF;
    }
    
    @NotNull
    public static final Rect toRect(@NotNull final RectF rectF) {
        Intrinsics.checkNotNullParameter((Object)rectF, "<this>");
        final Rect rect = new Rect();
        rectF.roundOut(rect);
        return rect;
    }
    
    @NotNull
    public static final RectF toRectF(@NotNull final Rect rect) {
        Intrinsics.checkNotNullParameter((Object)rect, "<this>");
        return new RectF(rect);
    }
    
    @NotNull
    public static final Region toRegion(@NotNull final Rect rect) {
        Intrinsics.checkNotNullParameter((Object)rect, "<this>");
        return new Region(rect);
    }
    
    @NotNull
    public static final Region toRegion(@NotNull final RectF rectF) {
        Intrinsics.checkNotNullParameter((Object)rectF, "<this>");
        final Rect rect = new Rect();
        rectF.roundOut(rect);
        return new Region(rect);
    }
    
    @NotNull
    public static final RectF transform(@NotNull final RectF rectF, @NotNull final Matrix matrix) {
        Intrinsics.checkNotNullParameter((Object)rectF, "<this>");
        Intrinsics.checkNotNullParameter((Object)matrix, "m");
        matrix.mapRect(rectF);
        return rectF;
    }
    
    @NotNull
    public static final Region xor(@NotNull final Rect rect, @NotNull final Rect rect2) {
        Intrinsics.checkNotNullParameter((Object)rect, "<this>");
        Intrinsics.checkNotNullParameter((Object)rect2, "r");
        final Region region = new Region(rect);
        region.op(rect2, Region$Op.XOR);
        return region;
    }
    
    @NotNull
    public static final Region xor(@NotNull final RectF rectF, @NotNull final RectF rectF2) {
        Intrinsics.checkNotNullParameter((Object)rectF, "<this>");
        Intrinsics.checkNotNullParameter((Object)rectF2, "r");
        final Rect rect = new Rect();
        rectF.roundOut(rect);
        final Region region = new Region(rect);
        final Rect rect2 = new Rect();
        rectF2.roundOut(rect2);
        region.op(rect2, Region$Op.XOR);
        return region;
    }
}

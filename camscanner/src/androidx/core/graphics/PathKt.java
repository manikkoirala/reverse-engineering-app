// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import java.util.Collection;
import androidx.annotation.RequiresApi;
import android.graphics.Path$Op;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.graphics.Path;
import android.annotation.SuppressLint;
import kotlin.Metadata;

@Metadata
@SuppressLint({ "ClassVerificationFailure" })
public final class PathKt
{
    @RequiresApi(19)
    @NotNull
    public static final Path and(@NotNull final Path path, @NotNull final Path path2) {
        Intrinsics.checkNotNullParameter((Object)path, "<this>");
        Intrinsics.checkNotNullParameter((Object)path2, "p");
        final Path path3 = new Path();
        path3.op(path, path2, Path$Op.INTERSECT);
        return path3;
    }
    
    @RequiresApi(26)
    @NotNull
    public static final Iterable<PathSegment> flatten(@NotNull final Path path, final float n) {
        Intrinsics.checkNotNullParameter((Object)path, "<this>");
        final Collection<PathSegment> flatten = PathUtils.flatten(path, n);
        Intrinsics.checkNotNullExpressionValue((Object)flatten, "flatten(this, error)");
        return flatten;
    }
    
    @RequiresApi(19)
    @NotNull
    public static final Path minus(@NotNull Path path, @NotNull final Path path2) {
        Intrinsics.checkNotNullParameter((Object)path, "<this>");
        Intrinsics.checkNotNullParameter((Object)path2, "p");
        path = new Path(path);
        path.op(path2, Path$Op.DIFFERENCE);
        return path;
    }
    
    @RequiresApi(19)
    @NotNull
    public static final Path or(@NotNull Path path, @NotNull final Path path2) {
        Intrinsics.checkNotNullParameter((Object)path, "<this>");
        Intrinsics.checkNotNullParameter((Object)path2, "p");
        path = new Path(path);
        path.op(path2, Path$Op.UNION);
        return path;
    }
    
    @RequiresApi(19)
    @NotNull
    public static final Path plus(@NotNull Path path, @NotNull final Path path2) {
        Intrinsics.checkNotNullParameter((Object)path, "<this>");
        Intrinsics.checkNotNullParameter((Object)path2, "p");
        path = new Path(path);
        path.op(path2, Path$Op.UNION);
        return path;
    }
    
    @RequiresApi(19)
    @NotNull
    public static final Path xor(@NotNull Path path, @NotNull final Path path2) {
        Intrinsics.checkNotNullParameter((Object)path, "<this>");
        Intrinsics.checkNotNullParameter((Object)path2, "p");
        path = new Path(path);
        path.op(path2, Path$Op.XOR);
        return path;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics.drawable;

import kotlin.jvm.internal.Intrinsics;
import android.graphics.drawable.BitmapDrawable;
import android.content.res.Resources;
import org.jetbrains.annotations.NotNull;
import android.graphics.Bitmap;
import kotlin.Metadata;

@Metadata
public final class BitmapDrawableKt
{
    @NotNull
    public static final BitmapDrawable toDrawable(@NotNull final Bitmap bitmap, @NotNull final Resources resources) {
        Intrinsics.checkNotNullParameter((Object)bitmap, "<this>");
        Intrinsics.checkNotNullParameter((Object)resources, "resources");
        return new BitmapDrawable(resources, bitmap);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics.drawable;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import androidx.annotation.ColorInt;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;
import android.util.AttributeSet;
import org.xmlpull.v1.XmlPullParser;
import android.content.res.Resources;
import androidx.annotation.Nullable;
import android.graphics.ColorFilter;
import android.graphics.drawable.DrawableContainer$DrawableContainerState;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.InsetDrawable;
import android.os.Build$VERSION;
import android.content.res.Resources$Theme;
import androidx.annotation.NonNull;
import android.graphics.drawable.Drawable;
import java.lang.reflect.Method;

public final class DrawableCompat
{
    private static final String TAG = "DrawableCompat";
    private static Method sGetLayoutDirectionMethod;
    private static boolean sGetLayoutDirectionMethodFetched;
    private static Method sSetLayoutDirectionMethod;
    private static boolean sSetLayoutDirectionMethodFetched;
    
    private DrawableCompat() {
    }
    
    public static void applyTheme(@NonNull final Drawable drawable, @NonNull final Resources$Theme resources$Theme) {
        Api21Impl.applyTheme(drawable, resources$Theme);
    }
    
    public static boolean canApplyTheme(@NonNull final Drawable drawable) {
        return Api21Impl.canApplyTheme(drawable);
    }
    
    public static void clearColorFilter(@NonNull final Drawable drawable) {
        if (Build$VERSION.SDK_INT >= 23) {
            drawable.clearColorFilter();
        }
        else {
            drawable.clearColorFilter();
            if (drawable instanceof InsetDrawable) {
                clearColorFilter(Api19Impl.getDrawable((InsetDrawable)drawable));
            }
            else if (drawable instanceof WrappedDrawable) {
                clearColorFilter(((WrappedDrawable)drawable).getWrappedDrawable());
            }
            else if (drawable instanceof DrawableContainer) {
                final DrawableContainer$DrawableContainerState drawableContainer$DrawableContainerState = (DrawableContainer$DrawableContainerState)((DrawableContainer)drawable).getConstantState();
                if (drawableContainer$DrawableContainerState != null) {
                    for (int childCount = drawableContainer$DrawableContainerState.getChildCount(), i = 0; i < childCount; ++i) {
                        final Drawable child = Api19Impl.getChild(drawableContainer$DrawableContainerState, i);
                        if (child != null) {
                            clearColorFilter(child);
                        }
                    }
                }
            }
        }
    }
    
    public static int getAlpha(@NonNull final Drawable drawable) {
        return Api19Impl.getAlpha(drawable);
    }
    
    @Nullable
    public static ColorFilter getColorFilter(@NonNull final Drawable drawable) {
        return Api21Impl.getColorFilter(drawable);
    }
    
    public static int getLayoutDirection(@NonNull final Drawable obj) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getLayoutDirection((Drawable)obj);
        }
        Label_0044: {
            if (DrawableCompat.sGetLayoutDirectionMethodFetched) {
                break Label_0044;
            }
            while (true) {
                try {
                    (DrawableCompat.sGetLayoutDirectionMethod = Drawable.class.getDeclaredMethod("getLayoutDirection", (Class<?>[])new Class[0])).setAccessible(true);
                    DrawableCompat.sGetLayoutDirectionMethodFetched = true;
                    final Method sGetLayoutDirectionMethod = DrawableCompat.sGetLayoutDirectionMethod;
                    if (sGetLayoutDirectionMethod != null) {
                        try {
                            return (int)sGetLayoutDirectionMethod.invoke(obj, new Object[0]);
                        }
                        catch (final Exception obj) {
                            DrawableCompat.sGetLayoutDirectionMethod = null;
                        }
                    }
                    return 0;
                }
                catch (final NoSuchMethodException ex) {
                    continue;
                }
                break;
            }
        }
    }
    
    public static void inflate(@NonNull final Drawable drawable, @NonNull final Resources resources, @NonNull final XmlPullParser xmlPullParser, @NonNull final AttributeSet set, @Nullable final Resources$Theme resources$Theme) throws XmlPullParserException, IOException {
        Api21Impl.inflate(drawable, resources, xmlPullParser, set, resources$Theme);
    }
    
    public static boolean isAutoMirrored(@NonNull final Drawable drawable) {
        return Api19Impl.isAutoMirrored(drawable);
    }
    
    @Deprecated
    public static void jumpToCurrentState(@NonNull final Drawable drawable) {
        drawable.jumpToCurrentState();
    }
    
    public static void setAutoMirrored(@NonNull final Drawable drawable, final boolean b) {
        Api19Impl.setAutoMirrored(drawable, b);
    }
    
    public static void setHotspot(@NonNull final Drawable drawable, final float n, final float n2) {
        Api21Impl.setHotspot(drawable, n, n2);
    }
    
    public static void setHotspotBounds(@NonNull final Drawable drawable, final int n, final int n2, final int n3, final int n4) {
        Api21Impl.setHotspotBounds(drawable, n, n2, n3, n4);
    }
    
    public static boolean setLayoutDirection(@NonNull final Drawable obj, final int i) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.setLayoutDirection((Drawable)obj, i);
        }
        Label_0051: {
            if (DrawableCompat.sSetLayoutDirectionMethodFetched) {
                break Label_0051;
            }
            while (true) {
                try {
                    (DrawableCompat.sSetLayoutDirectionMethod = Drawable.class.getDeclaredMethod("setLayoutDirection", Integer.TYPE)).setAccessible(true);
                    DrawableCompat.sSetLayoutDirectionMethodFetched = true;
                    final Method sSetLayoutDirectionMethod = DrawableCompat.sSetLayoutDirectionMethod;
                    if (sSetLayoutDirectionMethod != null) {
                        try {
                            sSetLayoutDirectionMethod.invoke(obj, i);
                            return true;
                        }
                        catch (final Exception obj) {
                            DrawableCompat.sSetLayoutDirectionMethod = null;
                        }
                    }
                    return false;
                }
                catch (final NoSuchMethodException ex) {
                    continue;
                }
                break;
            }
        }
    }
    
    public static void setTint(@NonNull final Drawable drawable, @ColorInt final int n) {
        Api21Impl.setTint(drawable, n);
    }
    
    public static void setTintList(@NonNull final Drawable drawable, @Nullable final ColorStateList list) {
        Api21Impl.setTintList(drawable, list);
    }
    
    public static void setTintMode(@NonNull final Drawable drawable, @Nullable final PorterDuff$Mode porterDuff$Mode) {
        Api21Impl.setTintMode(drawable, porterDuff$Mode);
    }
    
    public static <T extends Drawable> T unwrap(@NonNull final Drawable drawable) {
        Drawable wrappedDrawable = drawable;
        if (drawable instanceof WrappedDrawable) {
            wrappedDrawable = ((WrappedDrawable)drawable).getWrappedDrawable();
        }
        return (T)wrappedDrawable;
    }
    
    @NonNull
    public static Drawable wrap(@NonNull final Drawable drawable) {
        if (Build$VERSION.SDK_INT >= 23) {
            return drawable;
        }
        if (!(drawable instanceof TintAwareDrawable)) {
            return new WrappedDrawableApi21(drawable);
        }
        return drawable;
    }
    
    @RequiresApi(19)
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        @DoNotInline
        static int getAlpha(final Drawable drawable) {
            return drawable.getAlpha();
        }
        
        @DoNotInline
        static Drawable getChild(final DrawableContainer$DrawableContainerState drawableContainer$DrawableContainerState, final int n) {
            return drawableContainer$DrawableContainerState.getChild(n);
        }
        
        @DoNotInline
        static Drawable getDrawable(final InsetDrawable insetDrawable) {
            return insetDrawable.getDrawable();
        }
        
        @DoNotInline
        static boolean isAutoMirrored(final Drawable drawable) {
            return drawable.isAutoMirrored();
        }
        
        @DoNotInline
        static void setAutoMirrored(final Drawable drawable, final boolean autoMirrored) {
            drawable.setAutoMirrored(autoMirrored);
        }
    }
    
    @RequiresApi(21)
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        @DoNotInline
        static void applyTheme(final Drawable drawable, final Resources$Theme resources$Theme) {
            drawable.applyTheme(resources$Theme);
        }
        
        @DoNotInline
        static boolean canApplyTheme(final Drawable drawable) {
            return drawable.canApplyTheme();
        }
        
        @DoNotInline
        static ColorFilter getColorFilter(final Drawable drawable) {
            return drawable.getColorFilter();
        }
        
        @DoNotInline
        static void inflate(final Drawable drawable, final Resources resources, final XmlPullParser xmlPullParser, final AttributeSet set, final Resources$Theme resources$Theme) throws XmlPullParserException, IOException {
            drawable.inflate(resources, xmlPullParser, set, resources$Theme);
        }
        
        @DoNotInline
        static void setHotspot(final Drawable drawable, final float n, final float n2) {
            drawable.setHotspot(n, n2);
        }
        
        @DoNotInline
        static void setHotspotBounds(final Drawable drawable, final int n, final int n2, final int n3, final int n4) {
            drawable.setHotspotBounds(n, n2, n3, n4);
        }
        
        @DoNotInline
        static void setTint(final Drawable drawable, final int tint) {
            drawable.setTint(tint);
        }
        
        @DoNotInline
        static void setTintList(final Drawable drawable, final ColorStateList tintList) {
            drawable.setTintList(tintList);
        }
        
        @DoNotInline
        static void setTintMode(final Drawable drawable, final PorterDuff$Mode tintMode) {
            drawable.setTintMode(tintMode);
        }
    }
    
    @RequiresApi(23)
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        @DoNotInline
        static int getLayoutDirection(final Drawable drawable) {
            return \u3007o\u3007.\u3007080(drawable);
        }
        
        @DoNotInline
        static boolean setLayoutDirection(final Drawable drawable, final int n) {
            return \u3007o00\u3007\u3007Oo.\u3007080(drawable, n);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics.drawable;

import android.graphics.Rect;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import kotlin.jvm.internal.Intrinsics;
import android.graphics.Bitmap;
import android.graphics.Bitmap$Config;
import androidx.annotation.Px;
import org.jetbrains.annotations.NotNull;
import android.graphics.drawable.Drawable;
import kotlin.Metadata;

@Metadata
public final class DrawableKt
{
    @NotNull
    public static final Bitmap toBitmap(@NotNull final Drawable drawable, @Px final int n, @Px final int n2, final Bitmap$Config bitmap$Config) {
        Intrinsics.checkNotNullParameter((Object)drawable, "<this>");
        if (drawable instanceof BitmapDrawable) {
            final BitmapDrawable bitmapDrawable = (BitmapDrawable)drawable;
            if (bitmapDrawable.getBitmap() == null) {
                throw new IllegalArgumentException("bitmap is null");
            }
            if (bitmap$Config == null || bitmapDrawable.getBitmap().getConfig() == bitmap$Config) {
                if (n == bitmapDrawable.getBitmap().getWidth() && n2 == bitmapDrawable.getBitmap().getHeight()) {
                    final Bitmap bitmap = bitmapDrawable.getBitmap();
                    Intrinsics.checkNotNullExpressionValue((Object)bitmap, "bitmap");
                    return bitmap;
                }
                final Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmapDrawable.getBitmap(), n, n2, true);
                Intrinsics.checkNotNullExpressionValue((Object)scaledBitmap, "createScaledBitmap(bitmap, width, height, true)");
                return scaledBitmap;
            }
        }
        final Rect bounds = drawable.getBounds();
        Intrinsics.checkNotNullExpressionValue((Object)bounds, "bounds");
        final int left = bounds.left;
        final int top = bounds.top;
        final int right = bounds.right;
        final int bottom = bounds.bottom;
        Bitmap$Config argb_8888;
        if ((argb_8888 = bitmap$Config) == null) {
            argb_8888 = Bitmap$Config.ARGB_8888;
        }
        final Bitmap bitmap2 = Bitmap.createBitmap(n, n2, argb_8888);
        drawable.setBounds(0, 0, n, n2);
        drawable.draw(new Canvas(bitmap2));
        drawable.setBounds(left, top, right, bottom);
        Intrinsics.checkNotNullExpressionValue((Object)bitmap2, "bitmap");
        return bitmap2;
    }
    
    public static final Bitmap toBitmapOrNull(@NotNull final Drawable drawable, @Px final int n, @Px final int n2, final Bitmap$Config bitmap$Config) {
        Intrinsics.checkNotNullParameter((Object)drawable, "<this>");
        if (drawable instanceof BitmapDrawable && ((BitmapDrawable)drawable).getBitmap() == null) {
            return null;
        }
        return toBitmap(drawable, n, n2, bitmap$Config);
    }
    
    public static final void updateBounds(@NotNull final Drawable drawable, @Px final int n, @Px final int n2, @Px final int n3, @Px final int n4) {
        Intrinsics.checkNotNullParameter((Object)drawable, "<this>");
        drawable.setBounds(n, n2, n3, n4);
    }
}

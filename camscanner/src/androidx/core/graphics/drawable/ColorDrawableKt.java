// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics.drawable;

import androidx.annotation.RequiresApi;
import android.annotation.SuppressLint;
import kotlin.jvm.internal.Intrinsics;
import android.graphics.Color;
import org.jetbrains.annotations.NotNull;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.ColorInt;
import kotlin.Metadata;

@Metadata
public final class ColorDrawableKt
{
    @NotNull
    public static final ColorDrawable toDrawable(@ColorInt final int n) {
        return new ColorDrawable(n);
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    @NotNull
    public static final ColorDrawable toDrawable(@NotNull final Color color) {
        Intrinsics.checkNotNullParameter((Object)color, "<this>");
        return new ColorDrawable(\u3007080.\u3007080(color));
    }
}

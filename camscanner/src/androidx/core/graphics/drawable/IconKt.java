// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics.drawable;

import android.net.Uri;
import androidx.annotation.RequiresApi;
import kotlin.jvm.internal.Intrinsics;
import android.graphics.drawable.Icon;
import org.jetbrains.annotations.NotNull;
import android.graphics.Bitmap;
import android.annotation.SuppressLint;
import kotlin.Metadata;

@Metadata
@SuppressLint({ "ClassVerificationFailure" })
public final class IconKt
{
    @RequiresApi(26)
    @NotNull
    public static final Icon toAdaptiveIcon(@NotNull final Bitmap bitmap) {
        Intrinsics.checkNotNullParameter((Object)bitmap, "<this>");
        final Icon \u3007080 = \u30078o8o\u3007.\u3007080(bitmap);
        Intrinsics.checkNotNullExpressionValue((Object)\u3007080, "createWithAdaptiveBitmap(this)");
        return \u3007080;
    }
    
    @RequiresApi(26)
    @NotNull
    public static final Icon toIcon(@NotNull final Bitmap bitmap) {
        Intrinsics.checkNotNullParameter((Object)bitmap, "<this>");
        final Icon \u3007080 = Oo08.\u3007080(bitmap);
        Intrinsics.checkNotNullExpressionValue((Object)\u3007080, "createWithBitmap(this)");
        return \u3007080;
    }
    
    @RequiresApi(26)
    @NotNull
    public static final Icon toIcon(@NotNull final Uri uri) {
        Intrinsics.checkNotNullParameter((Object)uri, "<this>");
        final Icon \u3007080 = \u3007\u30078O0\u30078.\u3007080(uri);
        Intrinsics.checkNotNullExpressionValue((Object)\u3007080, "createWithContentUri(this)");
        return \u3007080;
    }
    
    @RequiresApi(26)
    @NotNull
    public static final Icon toIcon(@NotNull final byte[] array) {
        Intrinsics.checkNotNullParameter((Object)array, "<this>");
        final Icon \u3007080 = \u3007\u3007888.\u3007080(array, 0, array.length);
        Intrinsics.checkNotNullExpressionValue((Object)\u3007080, "createWithData(this, 0, size)");
        return \u3007080;
    }
}

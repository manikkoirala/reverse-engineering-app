// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics.drawable;

import android.graphics.drawable.Drawable;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public interface WrappedDrawable
{
    Drawable getWrappedDrawable();
    
    void setWrappedDrawable(final Drawable p0);
}

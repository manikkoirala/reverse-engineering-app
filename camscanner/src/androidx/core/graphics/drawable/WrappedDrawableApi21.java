// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics.drawable;

import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.GradientDrawable;
import android.os.Build$VERSION;
import android.graphics.Outline;
import androidx.annotation.NonNull;
import android.graphics.Rect;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import java.lang.reflect.Method;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
class WrappedDrawableApi21 extends WrappedDrawableApi14
{
    private static final String TAG = "WrappedDrawableApi21";
    private static Method sIsProjectedDrawableMethod;
    
    WrappedDrawableApi21(final Drawable drawable) {
        super(drawable);
        this.findAndCacheIsProjectedDrawableMethod();
    }
    
    WrappedDrawableApi21(final WrappedDrawableState wrappedDrawableState, final Resources resources) {
        super(wrappedDrawableState, resources);
        this.findAndCacheIsProjectedDrawableMethod();
    }
    
    private void findAndCacheIsProjectedDrawableMethod() {
        if (WrappedDrawableApi21.sIsProjectedDrawableMethod != null) {
            return;
        }
        try {
            WrappedDrawableApi21.sIsProjectedDrawableMethod = Drawable.class.getDeclaredMethod("isProjected", (Class<?>[])new Class[0]);
        }
        catch (final Exception ex) {}
    }
    
    @NonNull
    public Rect getDirtyBounds() {
        return super.mDrawable.getDirtyBounds();
    }
    
    public void getOutline(@NonNull final Outline outline) {
        super.mDrawable.getOutline(outline);
    }
    
    @Override
    protected boolean isCompatTintEnabled() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        boolean b = false;
        if (sdk_INT == 21) {
            final Drawable mDrawable = super.mDrawable;
            if (!(mDrawable instanceof GradientDrawable) && !(mDrawable instanceof DrawableContainer) && !(mDrawable instanceof InsetDrawable)) {
                b = b;
                if (!(mDrawable instanceof RippleDrawable)) {
                    return b;
                }
            }
            b = true;
        }
        return b;
    }
    
    public boolean isProjected() {
        final Drawable mDrawable = super.mDrawable;
        if (mDrawable == null) {
            return false;
        }
        final Method sIsProjectedDrawableMethod = WrappedDrawableApi21.sIsProjectedDrawableMethod;
        if (sIsProjectedDrawableMethod == null) {
            return false;
        }
        try {
            return (boolean)sIsProjectedDrawableMethod.invoke(mDrawable, new Object[0]);
        }
        catch (final Exception ex) {
            return false;
        }
    }
    
    public void setHotspot(final float n, final float n2) {
        super.mDrawable.setHotspot(n, n2);
    }
    
    public void setHotspotBounds(final int n, final int n2, final int n3, final int n4) {
        super.mDrawable.setHotspotBounds(n, n2, n3, n4);
    }
    
    @Override
    public boolean setState(@NonNull final int[] state) {
        if (super.setState(state)) {
            this.invalidateSelf();
            return true;
        }
        return false;
    }
    
    @Override
    public void setTint(final int n) {
        if (this.isCompatTintEnabled()) {
            super.setTint(n);
        }
        else {
            super.mDrawable.setTint(n);
        }
    }
    
    @Override
    public void setTintList(final ColorStateList list) {
        if (this.isCompatTintEnabled()) {
            super.setTintList(list);
        }
        else {
            super.mDrawable.setTintList(list);
        }
    }
    
    @Override
    public void setTintMode(@NonNull final PorterDuff$Mode porterDuff$Mode) {
        if (this.isCompatTintEnabled()) {
            super.setTintMode(porterDuff$Mode);
        }
        else {
            super.mDrawable.setTintMode(porterDuff$Mode);
        }
    }
}

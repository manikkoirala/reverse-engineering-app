// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics.drawable;

import androidx.annotation.NonNull;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable$ConstantState;

final class WrappedDrawableState extends Drawable$ConstantState
{
    int mChangingConfigurations;
    Drawable$ConstantState mDrawableState;
    ColorStateList mTint;
    PorterDuff$Mode mTintMode;
    
    WrappedDrawableState(@Nullable final WrappedDrawableState wrappedDrawableState) {
        this.mTint = null;
        this.mTintMode = WrappedDrawableApi14.DEFAULT_TINT_MODE;
        if (wrappedDrawableState != null) {
            this.mChangingConfigurations = wrappedDrawableState.mChangingConfigurations;
            this.mDrawableState = wrappedDrawableState.mDrawableState;
            this.mTint = wrappedDrawableState.mTint;
            this.mTintMode = wrappedDrawableState.mTintMode;
        }
    }
    
    boolean canConstantState() {
        return this.mDrawableState != null;
    }
    
    public int getChangingConfigurations() {
        final int mChangingConfigurations = this.mChangingConfigurations;
        final Drawable$ConstantState mDrawableState = this.mDrawableState;
        int changingConfigurations;
        if (mDrawableState != null) {
            changingConfigurations = mDrawableState.getChangingConfigurations();
        }
        else {
            changingConfigurations = 0;
        }
        return mChangingConfigurations | changingConfigurations;
    }
    
    @NonNull
    public Drawable newDrawable() {
        return this.newDrawable(null);
    }
    
    @NonNull
    public Drawable newDrawable(@Nullable final Resources resources) {
        return new WrappedDrawableApi21(this, resources);
    }
}

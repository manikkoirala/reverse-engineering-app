// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics.drawable;

import androidx.core.graphics.BitmapCompat;
import androidx.core.view.GravityCompat;
import android.graphics.Rect;
import android.graphics.BitmapFactory;
import java.io.InputStream;
import androidx.annotation.Nullable;
import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import android.content.res.Resources;

public final class RoundedBitmapDrawableFactory
{
    private static final String TAG = "RoundedBitmapDrawableFa";
    
    private RoundedBitmapDrawableFactory() {
    }
    
    @NonNull
    public static RoundedBitmapDrawable create(@NonNull final Resources resources, @Nullable final Bitmap bitmap) {
        return new RoundedBitmapDrawable21(resources, bitmap);
    }
    
    @NonNull
    public static RoundedBitmapDrawable create(@NonNull final Resources resources, @NonNull final InputStream obj) {
        final RoundedBitmapDrawable create = create(resources, BitmapFactory.decodeStream(obj));
        if (create.getBitmap() == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("RoundedBitmapDrawable cannot decode ");
            sb.append(obj);
        }
        return create;
    }
    
    @NonNull
    public static RoundedBitmapDrawable create(@NonNull final Resources resources, @NonNull final String str) {
        final RoundedBitmapDrawable create = create(resources, BitmapFactory.decodeFile(str));
        if (create.getBitmap() == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("RoundedBitmapDrawable cannot decode ");
            sb.append(str);
        }
        return create;
    }
    
    private static class DefaultRoundedBitmapDrawable extends RoundedBitmapDrawable
    {
        DefaultRoundedBitmapDrawable(final Resources resources, final Bitmap bitmap) {
            super(resources, bitmap);
        }
        
        @Override
        void gravityCompatApply(final int n, final int n2, final int n3, final Rect rect, final Rect rect2) {
            GravityCompat.apply(n, n2, n3, rect, rect2, 0);
        }
        
        @Override
        public boolean hasMipMap() {
            final Bitmap mBitmap = super.mBitmap;
            return mBitmap != null && BitmapCompat.hasMipMap(mBitmap);
        }
        
        @Override
        public void setMipMap(final boolean b) {
            final Bitmap mBitmap = super.mBitmap;
            if (mBitmap != null) {
                BitmapCompat.setHasMipMap(mBitmap, b);
                this.invalidateSelf();
            }
        }
    }
}

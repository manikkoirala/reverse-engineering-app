// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import androidx.annotation.DoNotInline;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.O\u3007O\u3007oO;
import androidx.appcompat.widget.\u30078\u30070\u3007o\u3007O;
import androidx.appcompat.widget.O08000;
import androidx.appcompat.widget.\u30078;
import android.graphics.Rect;
import androidx.annotation.NonNull;

public final class Insets
{
    @NonNull
    public static final Insets NONE;
    public final int bottom;
    public final int left;
    public final int right;
    public final int top;
    
    static {
        NONE = new Insets(0, 0, 0, 0);
    }
    
    private Insets(final int left, final int top, final int right, final int bottom) {
        this.left = left;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
    }
    
    @NonNull
    public static Insets add(@NonNull final Insets insets, @NonNull final Insets insets2) {
        return of(insets.left + insets2.left, insets.top + insets2.top, insets.right + insets2.right, insets.bottom + insets2.bottom);
    }
    
    @NonNull
    public static Insets max(@NonNull final Insets insets, @NonNull final Insets insets2) {
        return of(Math.max(insets.left, insets2.left), Math.max(insets.top, insets2.top), Math.max(insets.right, insets2.right), Math.max(insets.bottom, insets2.bottom));
    }
    
    @NonNull
    public static Insets min(@NonNull final Insets insets, @NonNull final Insets insets2) {
        return of(Math.min(insets.left, insets2.left), Math.min(insets.top, insets2.top), Math.min(insets.right, insets2.right), Math.min(insets.bottom, insets2.bottom));
    }
    
    @NonNull
    public static Insets of(final int n, final int n2, final int n3, final int n4) {
        if (n == 0 && n2 == 0 && n3 == 0 && n4 == 0) {
            return Insets.NONE;
        }
        return new Insets(n, n2, n3, n4);
    }
    
    @NonNull
    public static Insets of(@NonNull final Rect rect) {
        return of(rect.left, rect.top, rect.right, rect.bottom);
    }
    
    @NonNull
    public static Insets subtract(@NonNull final Insets insets, @NonNull final Insets insets2) {
        return of(insets.left - insets2.left, insets.top - insets2.top, insets.right - insets2.right, insets.bottom - insets2.bottom);
    }
    
    @NonNull
    @RequiresApi(api = 29)
    public static Insets toCompatInsets(@NonNull final android.graphics.Insets insets) {
        return of(\u30078.\u3007080(insets), O08000.\u3007080(insets), \u30078\u30070\u3007o\u3007O.\u3007080(insets), O\u3007O\u3007oO.\u3007080(insets));
    }
    
    @Deprecated
    @NonNull
    @RequiresApi(api = 29)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static Insets wrap(@NonNull final android.graphics.Insets insets) {
        return toCompatInsets(insets);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && Insets.class == o.getClass()) {
            final Insets insets = (Insets)o;
            return this.bottom == insets.bottom && this.left == insets.left && this.right == insets.right && this.top == insets.top;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return ((this.left * 31 + this.top) * 31 + this.right) * 31 + this.bottom;
    }
    
    @NonNull
    @RequiresApi(29)
    public android.graphics.Insets toPlatformInsets() {
        return Api29Impl.of(this.left, this.top, this.right, this.bottom);
    }
    
    @NonNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Insets{left=");
        sb.append(this.left);
        sb.append(", top=");
        sb.append(this.top);
        sb.append(", right=");
        sb.append(this.right);
        sb.append(", bottom=");
        sb.append(this.bottom);
        sb.append('}');
        return sb.toString();
    }
    
    @RequiresApi(29)
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        @DoNotInline
        static android.graphics.Insets of(final int n, final int n2, final int n3, final int n4) {
            return Oo\u3007O.\u3007080(n, n2, n3, n4);
        }
    }
}

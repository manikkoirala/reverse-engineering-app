// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.graphics.Matrix;
import kotlin.Metadata;

@Metadata
public final class MatrixKt
{
    @NotNull
    public static final Matrix rotationMatrix(final float n, final float n2, final float n3) {
        final Matrix matrix = new Matrix();
        matrix.setRotate(n, n2, n3);
        return matrix;
    }
    
    @NotNull
    public static final Matrix scaleMatrix(final float n, final float n2) {
        final Matrix matrix = new Matrix();
        matrix.setScale(n, n2);
        return matrix;
    }
    
    @NotNull
    public static final Matrix times(@NotNull Matrix matrix, @NotNull final Matrix matrix2) {
        Intrinsics.checkNotNullParameter((Object)matrix, "<this>");
        Intrinsics.checkNotNullParameter((Object)matrix2, "m");
        matrix = new Matrix(matrix);
        matrix.preConcat(matrix2);
        return matrix;
    }
    
    @NotNull
    public static final Matrix translationMatrix(final float n, final float n2) {
        final Matrix matrix = new Matrix();
        matrix.setTranslate(n, n2);
        return matrix;
    }
    
    @NotNull
    public static final float[] values(@NotNull final Matrix matrix) {
        Intrinsics.checkNotNullParameter((Object)matrix, "<this>");
        final float[] array = new float[9];
        matrix.getValues(array);
        return array;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import android.graphics.PorterDuffXfermode;
import kotlin.jvm.internal.Intrinsics;
import android.graphics.PorterDuffColorFilter;
import org.jetbrains.annotations.NotNull;
import android.graphics.PorterDuff$Mode;
import kotlin.Metadata;

@Metadata
public final class PorterDuffKt
{
    @NotNull
    public static final PorterDuffColorFilter toColorFilter(@NotNull final PorterDuff$Mode porterDuff$Mode, final int n) {
        Intrinsics.checkNotNullParameter((Object)porterDuff$Mode, "<this>");
        return new PorterDuffColorFilter(n, porterDuff$Mode);
    }
    
    @NotNull
    public static final PorterDuffXfermode toXfermode(@NotNull final PorterDuff$Mode porterDuff$Mode) {
        Intrinsics.checkNotNullParameter((Object)porterDuff$Mode, "<this>");
        return new PorterDuffXfermode(porterDuff$Mode);
    }
}

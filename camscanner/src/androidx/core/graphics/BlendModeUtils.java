// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import androidx.annotation.DoNotInline;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import android.graphics.PorterDuff$Mode;
import androidx.annotation.Nullable;

class BlendModeUtils
{
    private BlendModeUtils() {
    }
    
    @Nullable
    static PorterDuff$Mode obtainPorterDuffFromCompat(@Nullable final BlendModeCompat blendModeCompat) {
        if (blendModeCompat == null) {
            return null;
        }
        switch (BlendModeUtils$1.$SwitchMap$androidx$core$graphics$BlendModeCompat[blendModeCompat.ordinal()]) {
            default: {
                return null;
            }
            case 18: {
                return PorterDuff$Mode.LIGHTEN;
            }
            case 17: {
                return PorterDuff$Mode.DARKEN;
            }
            case 16: {
                return PorterDuff$Mode.OVERLAY;
            }
            case 15: {
                return PorterDuff$Mode.SCREEN;
            }
            case 14: {
                return PorterDuff$Mode.MULTIPLY;
            }
            case 13: {
                return PorterDuff$Mode.ADD;
            }
            case 12: {
                return PorterDuff$Mode.XOR;
            }
            case 11: {
                return PorterDuff$Mode.DST_ATOP;
            }
            case 10: {
                return PorterDuff$Mode.SRC_ATOP;
            }
            case 9: {
                return PorterDuff$Mode.DST_OUT;
            }
            case 8: {
                return PorterDuff$Mode.SRC_OUT;
            }
            case 7: {
                return PorterDuff$Mode.DST_IN;
            }
            case 6: {
                return PorterDuff$Mode.SRC_IN;
            }
            case 5: {
                return PorterDuff$Mode.DST_OVER;
            }
            case 4: {
                return PorterDuff$Mode.SRC_OVER;
            }
            case 3: {
                return PorterDuff$Mode.DST;
            }
            case 2: {
                return PorterDuff$Mode.SRC;
            }
            case 1: {
                return PorterDuff$Mode.CLEAR;
            }
        }
    }
    
    @RequiresApi(29)
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        @DoNotInline
        @Nullable
        static Object obtainBlendModeFromCompat(@NonNull final BlendModeCompat blendModeCompat) {
            switch (BlendModeUtils$1.$SwitchMap$androidx$core$graphics$BlendModeCompat[blendModeCompat.ordinal()]) {
                default: {
                    return null;
                }
                case 29: {
                    return \u3007oo\u3007.\u3007080();
                }
                case 28: {
                    return oo88o8O.\u3007080();
                }
                case 27: {
                    return \u3007O888o0o.\u3007080();
                }
                case 26: {
                    return o800o8O.\u3007080();
                }
                case 25: {
                    return OoO8.\u3007080();
                }
                case 24: {
                    return \u30070\u3007O0088o.\u3007080();
                }
                case 23: {
                    return \u3007\u30078O0\u30078.\u3007080();
                }
                case 22: {
                    return \u3007O00.\u3007080();
                }
                case 21: {
                    return \u3007\u3007808\u3007.\u3007080();
                }
                case 20: {
                    return Oooo8o0\u3007.\u3007080();
                }
                case 19: {
                    return o\u30070OOo\u30070.\u3007080();
                }
                case 18: {
                    return \u3007\u3007\u30070\u3007\u30070.\u3007080();
                }
                case 17: {
                    return Oo8Oo00oo.\u3007080();
                }
                case 16: {
                    return o\u30078.\u3007080();
                }
                case 15: {
                    return o0ooO.\u3007080();
                }
                case 14: {
                    return \u3007o.\u3007080();
                }
                case 13: {
                    return \u300700\u30078.\u3007080();
                }
                case 12: {
                    return O8\u3007o.\u3007080();
                }
                case 11: {
                    return o\u3007O8\u3007\u3007o.\u3007080();
                }
                case 10: {
                    return oo\u3007.\u3007080();
                }
                case 9: {
                    return OOO\u3007O0.\u3007080();
                }
                case 8: {
                    return o\u3007\u30070\u3007.\u3007080();
                }
                case 7: {
                    return \u30070000OOO.\u3007080();
                }
                case 6: {
                    return \u3007oOO8O8.\u3007080();
                }
                case 5: {
                    return O8ooOoo\u3007.\u3007080();
                }
                case 4: {
                    return O\u30078O8\u3007008.\u3007080();
                }
                case 3: {
                    return \u300700.\u3007080();
                }
                case 2: {
                    return oO80.\u3007080();
                }
                case 1: {
                    return OO0o\u3007\u3007.\u3007080();
                }
            }
        }
    }
}

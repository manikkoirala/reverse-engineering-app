// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import android.graphics.ColorSpace;
import android.graphics.Bitmap$Config;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import androidx.annotation.VisibleForTesting;
import android.graphics.Canvas;
import android.graphics.Xfermode;
import android.graphics.PorterDuffXfermode;
import android.graphics.PorterDuff$Mode;
import android.graphics.Paint;
import android.os.Build$VERSION;
import androidx.annotation.Nullable;
import android.graphics.Rect;
import androidx.annotation.NonNull;
import android.graphics.Bitmap;

public final class BitmapCompat
{
    private BitmapCompat() {
    }
    
    @NonNull
    public static Bitmap createScaledBitmap(@NonNull Bitmap bitmap, final int n, final int n2, @Nullable final Rect rect, final boolean b) {
        if (n <= 0 || n2 <= 0) {
            throw new IllegalArgumentException("dstW and dstH must be > 0!");
        }
        if (rect != null && (rect.isEmpty() || rect.left < 0 || rect.right > bitmap.getWidth() || rect.top < 0 || rect.bottom > bitmap.getHeight())) {
            throw new IllegalArgumentException("srcRect must be contained by srcBm!");
        }
        final int sdk_INT = Build$VERSION.SDK_INT;
        Bitmap copyBitmapIfHardware;
        if (sdk_INT >= 27) {
            copyBitmapIfHardware = Api27Impl.copyBitmapIfHardware(bitmap);
        }
        else {
            copyBitmapIfHardware = bitmap;
        }
        int n3;
        if (rect != null) {
            n3 = rect.width();
        }
        else {
            n3 = bitmap.getWidth();
        }
        int n4;
        if (rect != null) {
            n4 = rect.height();
        }
        else {
            n4 = bitmap.getHeight();
        }
        final float n5 = n / (float)n3;
        final float n6 = n2 / (float)n4;
        int left;
        if (rect != null) {
            left = rect.left;
        }
        else {
            left = 0;
        }
        int top;
        if (rect != null) {
            top = rect.top;
        }
        else {
            top = 0;
        }
        if (left == 0 && top == 0 && n == bitmap.getWidth() && n2 == bitmap.getHeight()) {
            if (bitmap.isMutable() && bitmap == copyBitmapIfHardware) {
                return bitmap.copy(bitmap.getConfig(), true);
            }
            return copyBitmapIfHardware;
        }
        else {
            final Paint paintBlendMode = new Paint(1);
            paintBlendMode.setFilterBitmap(true);
            if (sdk_INT >= 29) {
                Api29Impl.setPaintBlendMode(paintBlendMode);
            }
            else {
                paintBlendMode.setXfermode((Xfermode)new PorterDuffXfermode(PorterDuff$Mode.SRC));
            }
            if (n3 == n && n4 == n2) {
                bitmap = Bitmap.createBitmap(n, n2, copyBitmapIfHardware.getConfig());
                new Canvas(bitmap).drawBitmap(copyBitmapIfHardware, (float)(-left), (float)(-top), paintBlendMode);
                return bitmap;
            }
            final double log = Math.log(2.0);
            double n7;
            if (n5 > 1.0f) {
                n7 = Math.ceil(Math.log(n5) / log);
            }
            else {
                n7 = Math.floor(Math.log(n5) / log);
            }
            final int n8 = (int)n7;
            double n9;
            if (n6 > 1.0f) {
                n9 = Math.ceil(Math.log(n6) / log);
            }
            else {
                n9 = Math.floor(Math.log(n6) / log);
            }
            final int n10 = (int)n9;
            int n11;
            Bitmap bitmap2;
            if (b && sdk_INT >= 27 && !Api27Impl.isAlreadyF16AndLinear(bitmap)) {
                int sizeAtStep;
                if (n8 > 0) {
                    sizeAtStep = sizeAtStep(n3, n, 1, n8);
                }
                else {
                    sizeAtStep = n3;
                }
                int sizeAtStep2;
                if (n10 > 0) {
                    sizeAtStep2 = sizeAtStep(n4, n2, 1, n10);
                }
                else {
                    sizeAtStep2 = n4;
                }
                final Bitmap bitmapWithSourceColorspace = Api27Impl.createBitmapWithSourceColorspace(sizeAtStep, sizeAtStep2, bitmap, true);
                new Canvas(bitmapWithSourceColorspace).drawBitmap(copyBitmapIfHardware, (float)(-left), (float)(-top), paintBlendMode);
                top = 0;
                left = 0;
                n11 = 1;
                bitmap2 = copyBitmapIfHardware;
                copyBitmapIfHardware = bitmapWithSourceColorspace;
            }
            else {
                bitmap2 = null;
                n11 = 0;
            }
            Rect rect2 = new Rect(left, top, n3, n4);
            final Rect rect3 = new Rect();
            int n14;
            int n15;
            Rect rect4;
            Bitmap bitmap3;
            for (int n12 = n8, n13 = n10; n12 != 0 || n13 != 0; n13 = n15, rect4 = rect2, bitmap3 = copyBitmapIfHardware, copyBitmapIfHardware = bitmap2, bitmap2 = bitmap3, n12 = n14, rect2 = rect4) {
                if (n12 < 0) {
                    n14 = n12 + 1;
                }
                else if ((n14 = n12) > 0) {
                    n14 = n12 - 1;
                }
                if (n13 < 0) {
                    n15 = n13 + 1;
                }
                else if ((n15 = n13) > 0) {
                    n15 = n13 - 1;
                }
                rect3.set(0, 0, sizeAtStep(n3, n, n14, n8), sizeAtStep(n4, n2, n15, n10));
                final boolean b2 = n14 == 0 && n15 == 0;
                final boolean b3 = bitmap2 != null && bitmap2.getWidth() == n && bitmap2.getHeight() == n2;
                Label_0915: {
                    Label_0792: {
                        if (bitmap2 != null && bitmap2 != bitmap && (!b || Build$VERSION.SDK_INT < 27 || Api27Impl.isAlreadyF16AndLinear(bitmap2))) {
                            if (b2) {
                                if (!b3) {
                                    break Label_0792;
                                }
                                if (n11 != 0) {
                                    break Label_0792;
                                }
                            }
                            break Label_0915;
                        }
                    }
                    if (bitmap2 != bitmap && bitmap2 != null) {
                        bitmap2.recycle();
                    }
                    int n16;
                    if (n14 > 0) {
                        n16 = n11;
                    }
                    else {
                        n16 = n14;
                    }
                    final int sizeAtStep3 = sizeAtStep(n3, n, n16, n8);
                    int n17;
                    if (n15 > 0) {
                        n17 = n11;
                    }
                    else {
                        n17 = n15;
                    }
                    final int sizeAtStep4 = sizeAtStep(n4, n2, n17, n10);
                    if (Build$VERSION.SDK_INT >= 27) {
                        bitmap2 = Api27Impl.createBitmapWithSourceColorspace(sizeAtStep3, sizeAtStep4, bitmap, b && !b2);
                    }
                    else {
                        bitmap2 = Bitmap.createBitmap(sizeAtStep3, sizeAtStep4, copyBitmapIfHardware.getConfig());
                    }
                }
                new Canvas(bitmap2).drawBitmap(copyBitmapIfHardware, rect2, rect3, paintBlendMode);
                rect2.set(rect3);
            }
            if (bitmap2 != bitmap && bitmap2 != null) {
                bitmap2.recycle();
            }
            return copyBitmapIfHardware;
        }
    }
    
    public static int getAllocationByteCount(@NonNull final Bitmap bitmap) {
        return Api19Impl.getAllocationByteCount(bitmap);
    }
    
    public static boolean hasMipMap(@NonNull final Bitmap bitmap) {
        return Api17Impl.hasMipMap(bitmap);
    }
    
    public static void setHasMipMap(@NonNull final Bitmap bitmap, final boolean b) {
        Api17Impl.setHasMipMap(bitmap, b);
    }
    
    @VisibleForTesting
    public static int sizeAtStep(final int n, final int n2, final int n3, final int n4) {
        if (n3 == 0) {
            return n2;
        }
        if (n3 > 0) {
            return n * (1 << n4 - n3);
        }
        return n2 << -n3 - 1;
    }
    
    @RequiresApi(17)
    static class Api17Impl
    {
        private Api17Impl() {
        }
        
        @DoNotInline
        static boolean hasMipMap(final Bitmap bitmap) {
            return bitmap.hasMipMap();
        }
        
        @DoNotInline
        static void setHasMipMap(final Bitmap bitmap, final boolean hasMipMap) {
            bitmap.setHasMipMap(hasMipMap);
        }
    }
    
    @RequiresApi(19)
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        @DoNotInline
        static int getAllocationByteCount(final Bitmap bitmap) {
            return bitmap.getAllocationByteCount();
        }
    }
    
    @RequiresApi(27)
    static class Api27Impl
    {
        private Api27Impl() {
        }
        
        @DoNotInline
        static Bitmap copyBitmapIfHardware(final Bitmap bitmap) {
            Bitmap copy = bitmap;
            if (bitmap.getConfig() == \u3007080.\u3007080()) {
                Bitmap$Config bitmap$Config = Bitmap$Config.ARGB_8888;
                if (Build$VERSION.SDK_INT >= 31) {
                    bitmap$Config = Api31Impl.getHardwareBitmapConfig(bitmap);
                }
                copy = bitmap.copy(bitmap$Config, true);
            }
            return copy;
        }
        
        @DoNotInline
        static Bitmap createBitmapWithSourceColorspace(final int n, final int n2, final Bitmap bitmap, final boolean b) {
            Bitmap$Config bitmap$Config = bitmap.getConfig();
            final ColorSpace \u3007080 = Oo08.\u3007080(bitmap);
            ColorSpace \u300781 = \u3007o\u3007.\u3007080(\u3007o00\u3007\u3007Oo.\u3007080());
            if (b && !o\u30070.\u3007080(Oo08.\u3007080(bitmap), (Object)\u300781)) {
                bitmap$Config = O8.\u3007080();
            }
            else {
                \u300781 = \u3007080;
                if (bitmap.getConfig() == androidx.core.graphics.\u3007080.\u3007080()) {
                    bitmap$Config = Bitmap$Config.ARGB_8888;
                    \u300781 = \u3007080;
                    if (Build$VERSION.SDK_INT >= 31) {
                        bitmap$Config = Api31Impl.getHardwareBitmapConfig(bitmap);
                        \u300781 = \u3007080;
                    }
                }
            }
            return \u3007\u3007888.\u3007080(n, n2, bitmap$Config, bitmap.hasAlpha(), \u300781);
        }
        
        @DoNotInline
        static boolean isAlreadyF16AndLinear(final Bitmap bitmap) {
            final ColorSpace \u3007080 = \u3007o\u3007.\u3007080(\u3007o00\u3007\u3007Oo.\u3007080());
            return bitmap.getConfig() == O8.\u3007080() && o\u30070.\u3007080(Oo08.\u3007080(bitmap), (Object)\u3007080);
        }
    }
    
    @RequiresApi(29)
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        @DoNotInline
        static void setPaintBlendMode(final Paint paint) {
            \u300780\u3007808\u3007O.\u3007080(paint, oO80.\u3007080());
        }
    }
    
    @RequiresApi(31)
    static class Api31Impl
    {
        private Api31Impl() {
        }
        
        @DoNotInline
        static Bitmap$Config getHardwareBitmapConfig(final Bitmap bitmap) {
            if (\u30078o8o\u3007.\u3007080(OO0o\u3007\u3007\u3007\u30070.\u3007080(bitmap)) == 22) {
                return O8.\u3007080();
            }
            return Bitmap$Config.ARGB_8888;
        }
    }
}

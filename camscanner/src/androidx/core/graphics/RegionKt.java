// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import java.util.Iterator;
import android.graphics.RegionIterator;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import android.graphics.Point;
import android.graphics.Region$Op;
import kotlin.jvm.internal.Intrinsics;
import android.graphics.Rect;
import org.jetbrains.annotations.NotNull;
import android.graphics.Region;
import kotlin.Metadata;

@Metadata
public final class RegionKt
{
    @NotNull
    public static final Region and(@NotNull Region region, @NotNull final Rect rect) {
        Intrinsics.checkNotNullParameter((Object)region, "<this>");
        Intrinsics.checkNotNullParameter((Object)rect, "r");
        region = new Region(region);
        region.op(rect, Region$Op.INTERSECT);
        return region;
    }
    
    @NotNull
    public static final Region and(@NotNull Region region, @NotNull final Region region2) {
        Intrinsics.checkNotNullParameter((Object)region, "<this>");
        Intrinsics.checkNotNullParameter((Object)region2, "r");
        region = new Region(region);
        region.op(region2, Region$Op.INTERSECT);
        return region;
    }
    
    public static final boolean contains(@NotNull final Region region, @NotNull final Point point) {
        Intrinsics.checkNotNullParameter((Object)region, "<this>");
        Intrinsics.checkNotNullParameter((Object)point, "p");
        return region.contains(point.x, point.y);
    }
    
    public static final void forEach(@NotNull final Region region, @NotNull final Function1<? super Rect, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)region, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "action");
        final RegionIterator regionIterator = new RegionIterator(region);
        while (true) {
            final Rect rect = new Rect();
            if (!regionIterator.next(rect)) {
                break;
            }
            function1.invoke((Object)rect);
        }
    }
    
    @NotNull
    public static final Iterator<Rect> iterator(@NotNull final Region region) {
        Intrinsics.checkNotNullParameter((Object)region, "<this>");
        return (Iterator<Rect>)new RegionKt$iterator.RegionKt$iterator$1(region);
    }
    
    @NotNull
    public static final Region minus(@NotNull Region region, @NotNull final Rect rect) {
        Intrinsics.checkNotNullParameter((Object)region, "<this>");
        Intrinsics.checkNotNullParameter((Object)rect, "r");
        region = new Region(region);
        region.op(rect, Region$Op.DIFFERENCE);
        return region;
    }
    
    @NotNull
    public static final Region minus(@NotNull Region region, @NotNull final Region region2) {
        Intrinsics.checkNotNullParameter((Object)region, "<this>");
        Intrinsics.checkNotNullParameter((Object)region2, "r");
        region = new Region(region);
        region.op(region2, Region$Op.DIFFERENCE);
        return region;
    }
    
    @NotNull
    public static final Region not(@NotNull final Region region) {
        Intrinsics.checkNotNullParameter((Object)region, "<this>");
        final Region region2 = new Region(region.getBounds());
        region2.op(region, Region$Op.DIFFERENCE);
        return region2;
    }
    
    @NotNull
    public static final Region or(@NotNull Region region, @NotNull final Rect rect) {
        Intrinsics.checkNotNullParameter((Object)region, "<this>");
        Intrinsics.checkNotNullParameter((Object)rect, "r");
        region = new Region(region);
        region.union(rect);
        return region;
    }
    
    @NotNull
    public static final Region or(@NotNull Region region, @NotNull final Region region2) {
        Intrinsics.checkNotNullParameter((Object)region, "<this>");
        Intrinsics.checkNotNullParameter((Object)region2, "r");
        region = new Region(region);
        region.op(region2, Region$Op.UNION);
        return region;
    }
    
    @NotNull
    public static final Region plus(@NotNull Region region, @NotNull final Rect rect) {
        Intrinsics.checkNotNullParameter((Object)region, "<this>");
        Intrinsics.checkNotNullParameter((Object)rect, "r");
        region = new Region(region);
        region.union(rect);
        return region;
    }
    
    @NotNull
    public static final Region plus(@NotNull Region region, @NotNull final Region region2) {
        Intrinsics.checkNotNullParameter((Object)region, "<this>");
        Intrinsics.checkNotNullParameter((Object)region2, "r");
        region = new Region(region);
        region.op(region2, Region$Op.UNION);
        return region;
    }
    
    @NotNull
    public static final Region unaryMinus(@NotNull final Region region) {
        Intrinsics.checkNotNullParameter((Object)region, "<this>");
        final Region region2 = new Region(region.getBounds());
        region2.op(region, Region$Op.DIFFERENCE);
        return region2;
    }
    
    @NotNull
    public static final Region xor(@NotNull Region region, @NotNull final Rect rect) {
        Intrinsics.checkNotNullParameter((Object)region, "<this>");
        Intrinsics.checkNotNullParameter((Object)rect, "r");
        region = new Region(region);
        region.op(rect, Region$Op.XOR);
        return region;
    }
    
    @NotNull
    public static final Region xor(@NotNull Region region, @NotNull final Region region2) {
        Intrinsics.checkNotNullParameter((Object)region, "<this>");
        Intrinsics.checkNotNullParameter((Object)region2, "r");
        region = new Region(region);
        region.op(region2, Region$Op.XOR);
        return region;
    }
}

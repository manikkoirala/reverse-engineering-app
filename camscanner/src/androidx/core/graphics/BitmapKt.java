// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import androidx.annotation.ColorInt;
import androidx.annotation.RequiresApi;
import android.annotation.SuppressLint;
import android.graphics.ColorSpace;
import android.graphics.Bitmap$Config;
import android.graphics.PointF;
import android.graphics.Point;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import android.graphics.Canvas;
import kotlin.jvm.functions.Function1;
import org.jetbrains.annotations.NotNull;
import android.graphics.Bitmap;
import kotlin.Metadata;

@Metadata
public final class BitmapKt
{
    @NotNull
    public static final Bitmap applyCanvas(@NotNull final Bitmap bitmap, @NotNull final Function1<? super Canvas, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)bitmap, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        function1.invoke((Object)new Canvas(bitmap));
        return bitmap;
    }
    
    public static final boolean contains(@NotNull final Bitmap bitmap, @NotNull final Point point) {
        Intrinsics.checkNotNullParameter((Object)bitmap, "<this>");
        Intrinsics.checkNotNullParameter((Object)point, "p");
        final int width = bitmap.getWidth();
        final int x = point.x;
        boolean b = true;
        if (x >= 0 && x < width) {
            final int y = point.y;
            if (y >= 0 && y < bitmap.getHeight()) {
                return b;
            }
        }
        b = false;
        return b;
    }
    
    public static final boolean contains(@NotNull final Bitmap bitmap, @NotNull final PointF pointF) {
        Intrinsics.checkNotNullParameter((Object)bitmap, "<this>");
        Intrinsics.checkNotNullParameter((Object)pointF, "p");
        final float x = pointF.x;
        if (x >= 0.0f && x < bitmap.getWidth()) {
            final float y = pointF.y;
            if (y >= 0.0f && y < bitmap.getHeight()) {
                return true;
            }
        }
        return false;
    }
    
    @NotNull
    public static final Bitmap createBitmap(final int n, final int n2, @NotNull final Bitmap$Config bitmap$Config) {
        Intrinsics.checkNotNullParameter((Object)bitmap$Config, "config");
        final Bitmap bitmap = Bitmap.createBitmap(n, n2, bitmap$Config);
        Intrinsics.checkNotNullExpressionValue((Object)bitmap, "createBitmap(width, height, config)");
        return bitmap;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    @NotNull
    public static final Bitmap createBitmap(final int n, final int n2, @NotNull final Bitmap$Config bitmap$Config, final boolean b, @NotNull final ColorSpace colorSpace) {
        Intrinsics.checkNotNullParameter((Object)bitmap$Config, "config");
        Intrinsics.checkNotNullParameter((Object)colorSpace, "colorSpace");
        final Bitmap \u3007080 = \u3007\u3007888.\u3007080(n, n2, bitmap$Config, b, colorSpace);
        Intrinsics.checkNotNullExpressionValue((Object)\u3007080, "createBitmap(width, heig\u2026ig, hasAlpha, colorSpace)");
        return \u3007080;
    }
    
    public static final int get(@NotNull final Bitmap bitmap, final int n, final int n2) {
        Intrinsics.checkNotNullParameter((Object)bitmap, "<this>");
        return bitmap.getPixel(n, n2);
    }
    
    @NotNull
    public static final Bitmap scale(@NotNull Bitmap scaledBitmap, final int n, final int n2, final boolean b) {
        Intrinsics.checkNotNullParameter((Object)scaledBitmap, "<this>");
        scaledBitmap = Bitmap.createScaledBitmap(scaledBitmap, n, n2, b);
        Intrinsics.checkNotNullExpressionValue((Object)scaledBitmap, "createScaledBitmap(this, width, height, filter)");
        return scaledBitmap;
    }
    
    public static final void set(@NotNull final Bitmap bitmap, final int n, final int n2, @ColorInt final int n3) {
        Intrinsics.checkNotNullParameter((Object)bitmap, "<this>");
        bitmap.setPixel(n, n2, n3);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import android.graphics.Matrix;
import kotlin.jvm.functions.Function1;
import org.jetbrains.annotations.NotNull;
import android.graphics.Shader;
import kotlin.Metadata;

@Metadata
public final class ShaderKt
{
    public static final void transform(@NotNull final Shader shader, @NotNull final Function1<? super Matrix, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)shader, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final Matrix localMatrix = new Matrix();
        shader.getLocalMatrix(localMatrix);
        function1.invoke((Object)localMatrix);
        shader.setLocalMatrix(localMatrix);
    }
}

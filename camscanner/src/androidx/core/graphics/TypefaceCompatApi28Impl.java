// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import java.lang.reflect.Method;
import androidx.appcompat.widget.o\u3007\u30070\u3007;
import androidx.annotation.NonNull;
import android.content.Context;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Array;
import android.graphics.Typeface;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;

@RequiresApi(28)
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public class TypefaceCompatApi28Impl extends TypefaceCompatApi26Impl
{
    private static final String CREATE_FROM_FAMILIES_WITH_DEFAULT_METHOD = "createFromFamiliesWithDefault";
    private static final String DEFAULT_FAMILY = "sans-serif";
    private static final int RESOLVE_BY_FONT_TABLE = -1;
    
    @Override
    protected Typeface createFromFamiliesWithDefault(Object cause) {
        try {
            final Object instance = Array.newInstance(super.mFontFamily, 1);
            Array.set(instance, 0, cause);
            cause = (InvocationTargetException)super.mCreateFromFamiliesWithDefault.invoke(null, instance, "sans-serif", -1, -1);
            return (Typeface)cause;
        }
        catch (final InvocationTargetException cause) {}
        catch (final IllegalAccessException ex) {}
        throw new RuntimeException(cause);
    }
    
    @NonNull
    @Override
    Typeface createWeightStyle(@NonNull final Context context, @NonNull final Typeface typeface, final int n, final boolean b) {
        return o\u3007\u30070\u3007.\u3007080(typeface, n, b);
    }
    
    @Override
    protected Method obtainCreateFromFamiliesWithDefaultMethod(final Class<?> componentType) throws NoSuchMethodException {
        final Class<?> class1 = Array.newInstance(componentType, 1).getClass();
        final Class<Integer> type = Integer.TYPE;
        final Method declaredMethod = Typeface.class.getDeclaredMethod("createFromFamiliesWithDefault", class1, String.class, type, type);
        declaredMethod.setAccessible(true);
        return declaredMethod;
    }
}

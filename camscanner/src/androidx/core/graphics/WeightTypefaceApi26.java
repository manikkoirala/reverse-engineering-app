// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.lang.reflect.InvocationTargetException;
import androidx.annotation.GuardedBy;
import android.util.SparseArray;
import androidx.collection.LongSparseArray;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import android.graphics.Typeface;
import java.lang.reflect.Constructor;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;
import android.annotation.SuppressLint;

@SuppressLint({ "SoonBlockedPrivateApi" })
@RequiresApi(26)
@RestrictTo({ RestrictTo.Scope.LIBRARY })
final class WeightTypefaceApi26
{
    private static final String NATIVE_CREATE_FROM_TYPEFACE_WITH_EXACT_STYLE_METHOD = "nativeCreateFromTypefaceWithExactStyle";
    private static final String NATIVE_INSTANCE_FIELD = "native_instance";
    private static final String TAG = "WeightTypeface";
    private static final Constructor<Typeface> sConstructor;
    private static final Method sNativeCreateFromTypefaceWithExactStyle;
    private static final Field sNativeInstance;
    private static final Object sWeightCacheLock;
    @GuardedBy("sWeightCacheLock")
    private static final LongSparseArray<SparseArray<Typeface>> sWeightTypefaceCache;
    
    static {
        Field declaredField;
        Method declaredMethod;
        Constructor<Typeface> declaredConstructor;
        try {
            declaredField = Typeface.class.getDeclaredField("native_instance");
            final Class<Long> type = Long.TYPE;
            declaredMethod = Typeface.class.getDeclaredMethod("nativeCreateFromTypefaceWithExactStyle", type, Integer.TYPE, Boolean.TYPE);
            declaredMethod.setAccessible(true);
            declaredConstructor = Typeface.class.getDeclaredConstructor(type);
            declaredConstructor.setAccessible(true);
        }
        catch (final NoSuchFieldException | NoSuchMethodException ex) {
            declaredField = null;
            declaredConstructor = null;
            declaredMethod = null;
        }
        sNativeInstance = declaredField;
        sNativeCreateFromTypefaceWithExactStyle = declaredMethod;
        sConstructor = declaredConstructor;
        sWeightTypefaceCache = new LongSparseArray<SparseArray<Typeface>>(3);
        sWeightCacheLock = new Object();
    }
    
    private WeightTypefaceApi26() {
    }
    
    @Nullable
    private static Typeface create(final long l) {
        try {
            return WeightTypefaceApi26.sConstructor.newInstance(l);
        }
        catch (final IllegalAccessException | InstantiationException | InvocationTargetException ex) {
            return null;
        }
    }
    
    @Nullable
    static Typeface createWeightStyle(@NonNull final Typeface typeface, final int n, final boolean b) {
        if (!isPrivateApiAvailable()) {
            return null;
        }
        final int n2 = n << 1 | (b ? 1 : 0);
        synchronized (WeightTypefaceApi26.sWeightCacheLock) {
            final long nativeInstance = getNativeInstance(typeface);
            final LongSparseArray<SparseArray<Typeface>> sWeightTypefaceCache = WeightTypefaceApi26.sWeightTypefaceCache;
            SparseArray sparseArray = sWeightTypefaceCache.get(nativeInstance);
            if (sparseArray == null) {
                sparseArray = new SparseArray(4);
                sWeightTypefaceCache.put(nativeInstance, (SparseArray<Typeface>)sparseArray);
            }
            else {
                final Typeface typeface2 = (Typeface)sparseArray.get(n2);
                if (typeface2 != null) {
                    return typeface2;
                }
            }
            final Typeface create = create(nativeCreateFromTypefaceWithExactStyle(nativeInstance, n, b));
            sparseArray.put(n2, (Object)create);
            return create;
        }
    }
    
    private static long getNativeInstance(@NonNull final Typeface obj) {
        try {
            return WeightTypefaceApi26.sNativeInstance.getLong(obj);
        }
        catch (final IllegalAccessException cause) {
            throw new RuntimeException(cause);
        }
    }
    
    private static boolean isPrivateApiAvailable() {
        return WeightTypefaceApi26.sNativeInstance != null;
    }
    
    @SuppressLint({ "BanUncheckedReflection" })
    private static long nativeCreateFromTypefaceWithExactStyle(long longValue, final int i, final boolean b) {
        try {
            longValue = (long)WeightTypefaceApi26.sNativeCreateFromTypefaceWithExactStyle.invoke(null, longValue, i, b);
            return longValue;
        }
        catch (final InvocationTargetException cause) {
            throw new RuntimeException(cause);
        }
        catch (final IllegalAccessException cause2) {
            throw new RuntimeException(cause2);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import androidx.annotation.DoNotInline;
import android.graphics.BlendModeColorFilter;
import android.graphics.BlendMode;
import androidx.annotation.RequiresApi;
import androidx.annotation.Nullable;
import android.graphics.PorterDuff$Mode;
import android.graphics.PorterDuffColorFilter;
import android.os.Build$VERSION;
import android.graphics.ColorFilter;
import androidx.annotation.NonNull;

public class BlendModeColorFilterCompat
{
    private BlendModeColorFilterCompat() {
    }
    
    @Nullable
    public static ColorFilter createBlendModeColorFilterCompat(final int n, @NonNull final BlendModeCompat blendModeCompat) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        final ColorFilter colorFilter = null;
        final ColorFilter colorFilter2 = null;
        if (sdk_INT >= 29) {
            final Object obtainBlendModeFromCompat = BlendModeUtils.Api29Impl.obtainBlendModeFromCompat(blendModeCompat);
            ColorFilter blendModeColorFilter = colorFilter2;
            if (obtainBlendModeFromCompat != null) {
                blendModeColorFilter = Api29Impl.createBlendModeColorFilter(n, obtainBlendModeFromCompat);
            }
            return blendModeColorFilter;
        }
        final PorterDuff$Mode obtainPorterDuffFromCompat = BlendModeUtils.obtainPorterDuffFromCompat(blendModeCompat);
        Object o = colorFilter;
        if (obtainPorterDuffFromCompat != null) {
            o = new PorterDuffColorFilter(n, obtainPorterDuffFromCompat);
        }
        return (ColorFilter)o;
    }
    
    @RequiresApi(29)
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        @DoNotInline
        static ColorFilter createBlendModeColorFilter(final int n, final Object o) {
            return (ColorFilter)new BlendModeColorFilter(n, (BlendMode)o);
        }
    }
}

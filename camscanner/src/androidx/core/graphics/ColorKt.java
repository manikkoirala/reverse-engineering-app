// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import android.graphics.ColorSpace;
import android.graphics.ColorSpace$Named;
import androidx.annotation.ColorInt;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.graphics.Color;
import androidx.annotation.RequiresApi;
import android.annotation.SuppressLint;
import kotlin.Metadata;

@Metadata
public final class ColorKt
{
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    public static final float component1(final long n) {
        return O\u3007O\u3007oO.\u3007080(n);
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    public static final float component1(@NotNull final Color color) {
        Intrinsics.checkNotNullParameter((Object)color, "<this>");
        return \u30078.\u3007080(color, 0);
    }
    
    public static final int component1(@ColorInt final int n) {
        return n >> 24 & 0xFF;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    public static final float component2(final long n) {
        return o\u30078oOO88.\u3007080(n);
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    public static final float component2(@NotNull final Color color) {
        Intrinsics.checkNotNullParameter((Object)color, "<this>");
        return \u30078.\u3007080(color, 1);
    }
    
    public static final int component2(@ColorInt final int n) {
        return n >> 16 & 0xFF;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    public static final float component3(final long n) {
        return oO00OOO.\u3007080(n);
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    public static final float component3(@NotNull final Color color) {
        Intrinsics.checkNotNullParameter((Object)color, "<this>");
        return \u30078.\u3007080(color, 2);
    }
    
    public static final int component3(@ColorInt final int n) {
        return n >> 8 & 0xFF;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    public static final float component4(final long n) {
        return Ooo.\u3007080(n);
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    public static final float component4(@NotNull final Color color) {
        Intrinsics.checkNotNullParameter((Object)color, "<this>");
        return \u30078.\u3007080(color, 3);
    }
    
    public static final int component4(@ColorInt final int n) {
        return n & 0xFF;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    public static final long convertTo(@ColorInt final int n, @NotNull final ColorSpace$Named colorSpace$Named) {
        Intrinsics.checkNotNullParameter((Object)colorSpace$Named, "colorSpace");
        return o\u3007O.\u3007080(n, \u3007o\u3007.\u3007080(colorSpace$Named));
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    public static final long convertTo(@ColorInt final int n, @NotNull final ColorSpace colorSpace) {
        Intrinsics.checkNotNullParameter((Object)colorSpace, "colorSpace");
        return o\u3007O.\u3007080(n, colorSpace);
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    public static final long convertTo(final long n, @NotNull final ColorSpace$Named colorSpace$Named) {
        Intrinsics.checkNotNullParameter((Object)colorSpace$Named, "colorSpace");
        return \u3007O\u300780o08O.\u3007080(n, \u3007o\u3007.\u3007080(colorSpace$Named));
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    public static final long convertTo(final long n, @NotNull final ColorSpace colorSpace) {
        Intrinsics.checkNotNullParameter((Object)colorSpace, "colorSpace");
        return \u3007O\u300780o08O.\u3007080(n, colorSpace);
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    @NotNull
    public static final Color convertTo(@NotNull Color \u3007080, @NotNull final ColorSpace$Named colorSpace$Named) {
        Intrinsics.checkNotNullParameter((Object)\u3007080, "<this>");
        Intrinsics.checkNotNullParameter((Object)colorSpace$Named, "colorSpace");
        \u3007080 = O08000.\u3007080(\u3007080, \u3007o\u3007.\u3007080(colorSpace$Named));
        Intrinsics.checkNotNullExpressionValue((Object)\u3007080, "convert(ColorSpace.get(colorSpace))");
        return \u3007080;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    @NotNull
    public static final Color convertTo(@NotNull Color \u3007080, @NotNull final ColorSpace colorSpace) {
        Intrinsics.checkNotNullParameter((Object)\u3007080, "<this>");
        Intrinsics.checkNotNullParameter((Object)colorSpace, "colorSpace");
        \u3007080 = O08000.\u3007080(\u3007080, colorSpace);
        Intrinsics.checkNotNullExpressionValue((Object)\u3007080, "convert(colorSpace)");
        return \u3007080;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    public static final float getAlpha(final long n) {
        return Ooo.\u3007080(n);
    }
    
    public static final int getAlpha(@ColorInt final int n) {
        return n >> 24 & 0xFF;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    public static final float getBlue(final long n) {
        return oO00OOO.\u3007080(n);
    }
    
    public static final int getBlue(@ColorInt final int n) {
        return n & 0xFF;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    @NotNull
    public static final ColorSpace getColorSpace(final long n) {
        final ColorSpace \u3007080 = \u30078\u30070\u3007o\u3007O.\u3007080(n);
        Intrinsics.checkNotNullExpressionValue((Object)\u3007080, "colorSpace(this)");
        return \u3007080;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    public static final float getGreen(final long n) {
        return o\u30078oOO88.\u3007080(n);
    }
    
    public static final int getGreen(@ColorInt final int n) {
        return n >> 8 & 0xFF;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    public static final float getLuminance(@ColorInt final int n) {
        return O000.\u3007080(n);
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    public static final float getLuminance(final long n) {
        return \u3007\u30070o.\u3007080(n);
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    public static final float getRed(final long n) {
        return O\u3007O\u3007oO.\u3007080(n);
    }
    
    public static final int getRed(@ColorInt final int n) {
        return n >> 16 & 0xFF;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    public static final boolean isSrgb(final long n) {
        return O0o\u3007\u3007Oo.\u3007080(n);
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    public static final boolean isWideGamut(final long n) {
        return oO.\u3007080(n);
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    @NotNull
    public static final Color plus(@NotNull Color compositeColors, @NotNull final Color color) {
        Intrinsics.checkNotNullParameter((Object)compositeColors, "<this>");
        Intrinsics.checkNotNullParameter((Object)color, "c");
        compositeColors = ColorUtils.compositeColors(color, compositeColors);
        Intrinsics.checkNotNullExpressionValue((Object)compositeColors, "compositeColors(c, this)");
        return compositeColors;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    @NotNull
    public static final Color toColor(@ColorInt final int n) {
        final Color \u3007080 = \u300780.\u3007080(n);
        Intrinsics.checkNotNullExpressionValue((Object)\u3007080, "valueOf(this)");
        return \u3007080;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    @NotNull
    public static final Color toColor(final long n) {
        final Color \u3007080 = \u300708O8o\u30070.\u3007080(n);
        Intrinsics.checkNotNullExpressionValue((Object)\u3007080, "valueOf(this)");
        return \u3007080;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @ColorInt
    @RequiresApi(26)
    public static final int toColorInt(final long n) {
        return o8oO\u3007.\u3007080(n);
    }
    
    @ColorInt
    public static final int toColorInt(@NotNull final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "<this>");
        return Color.parseColor(s);
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(26)
    public static final long toColorLong(@ColorInt final int n) {
        return ooo\u30078oO.\u3007080(n);
    }
}

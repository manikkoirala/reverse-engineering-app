// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import androidx.annotation.DoNotInline;
import java.io.FileNotFoundException;
import android.os.ParcelFileDescriptor;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import androidx.core.provider.FontsContractCompat;
import java.nio.MappedByteBuffer;
import android.content.ContentResolver;
import java.nio.channels.FileChannel;
import java.io.FileInputStream;
import android.net.Uri;
import android.os.CancellationSignal;
import android.os.Process;
import android.os.StrictMode$ThreadPolicy;
import java.io.FileOutputStream;
import android.os.StrictMode;
import java.io.InputStream;
import androidx.annotation.RequiresApi;
import java.io.File;
import java.nio.ByteBuffer;
import android.content.res.Resources;
import androidx.annotation.NonNull;
import android.content.Context;
import java.io.IOException;
import androidx.annotation.Nullable;
import java.io.Closeable;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public class TypefaceCompatUtil
{
    private static final String CACHE_FILE_PREFIX = ".font";
    private static final String TAG = "TypefaceCompatUtil";
    
    private TypefaceCompatUtil() {
    }
    
    public static void closeQuietly(@Nullable final Closeable closeable) {
        if (closeable == null) {
            return;
        }
        try {
            closeable.close();
        }
        catch (final IOException ex) {}
    }
    
    @Nullable
    @RequiresApi(19)
    public static ByteBuffer copyToDirectBuffer(@NonNull Context tempFile, @NonNull final Resources resources, final int n) {
        tempFile = (Context)getTempFile(tempFile);
        if (tempFile == null) {
            return null;
        }
        try {
            if (!copyToFile((File)tempFile, resources, n)) {
                return null;
            }
            return mmap((File)tempFile);
        }
        finally {
            ((File)tempFile).delete();
        }
    }
    
    public static boolean copyToFile(@NonNull final File file, @NonNull final Resources resources, final int n) {
        Closeable closeable;
        try {
            final InputStream openRawResource = resources.openRawResource(n);
            try {
                final boolean copyToFile = copyToFile(file, openRawResource);
                closeQuietly(openRawResource);
                return copyToFile;
            }
            finally {}
        }
        finally {
            closeable = null;
        }
        closeQuietly(closeable);
    }
    
    public static boolean copyToFile(@NonNull final File file, @NonNull final InputStream ex) {
        final StrictMode$ThreadPolicy allowThreadDiskWrites = StrictMode.allowThreadDiskWrites();
        final Closeable closeable = null;
        Closeable closeable2 = null;
        Closeable closeable3;
        try {
            try {
                closeable2 = closeable2;
                final FileOutputStream fileOutputStream = new FileOutputStream(file, false);
                try {
                    final byte[] array = new byte[1024];
                    while (true) {
                        final int read = ((InputStream)ex).read(array);
                        if (read == -1) {
                            break;
                        }
                        fileOutputStream.write(array, 0, read);
                    }
                    closeQuietly(fileOutputStream);
                    StrictMode.setThreadPolicy(allowThreadDiskWrites);
                    return true;
                }
                catch (final IOException ex) {}
                finally {
                    closeable2 = fileOutputStream;
                }
            }
            finally {}
        }
        catch (final IOException ex) {
            closeable3 = closeable;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Error copying resource contents to temp file: ");
        sb.append(ex.getMessage());
        closeQuietly(closeable3);
        StrictMode.setThreadPolicy(allowThreadDiskWrites);
        return false;
        closeQuietly(closeable2);
        StrictMode.setThreadPolicy(allowThreadDiskWrites);
    }
    
    @Nullable
    public static File getTempFile(@NonNull Context cacheDir) {
        cacheDir = (Context)cacheDir.getCacheDir();
        if (cacheDir == null) {
            return null;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(".font");
        sb.append(Process.myPid());
        sb.append("-");
        sb.append(Process.myTid());
        sb.append("-");
        final String string = sb.toString();
        int i = 0;
    Label_0120_Outer:
        while (true) {
            Label_0126: {
                if (i >= 100) {
                    break Label_0126;
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(string);
                sb2.append(i);
                final File file = new File((File)cacheDir, sb2.toString());
                while (true) {
                    try {
                        if (file.createNewFile()) {
                            return file;
                        }
                        ++i;
                        continue Label_0120_Outer;
                        return null;
                    }
                    catch (final IOException ex) {
                        continue;
                    }
                    break;
                }
            }
        }
    }
    
    @Nullable
    @RequiresApi(19)
    public static ByteBuffer mmap(@NonNull Context openFileDescriptor, @Nullable CancellationSignal cancellationSignal, @NonNull final Uri uri) {
        final ContentResolver contentResolver = openFileDescriptor.getContentResolver();
        try {
            openFileDescriptor = (Context)Api19Impl.openFileDescriptor(contentResolver, uri, "r", cancellationSignal);
            if (openFileDescriptor == null) {
                if (openFileDescriptor != null) {
                    ((ParcelFileDescriptor)openFileDescriptor).close();
                }
                return null;
            }
            try {
                cancellationSignal = (CancellationSignal)new FileInputStream(((ParcelFileDescriptor)openFileDescriptor).getFileDescriptor());
                try {
                    final FileChannel channel = ((FileInputStream)cancellationSignal).getChannel();
                    final MappedByteBuffer map = channel.map(FileChannel.MapMode.READ_ONLY, 0L, channel.size());
                    ((FileInputStream)cancellationSignal).close();
                    ((ParcelFileDescriptor)openFileDescriptor).close();
                    return map;
                }
                finally {
                    try {
                        ((FileInputStream)cancellationSignal).close();
                    }
                    finally {
                        final Throwable exception;
                        ((Throwable)uri).addSuppressed(exception);
                    }
                }
            }
            finally {
                try {
                    ((ParcelFileDescriptor)openFileDescriptor).close();
                }
                finally {
                    final Throwable exception2;
                    ((Throwable)cancellationSignal).addSuppressed(exception2);
                }
            }
        }
        catch (final IOException ex) {
            return null;
        }
    }
    
    @Nullable
    @RequiresApi(19)
    private static ByteBuffer mmap(final File file) {
        try {
            final FileInputStream fileInputStream = new FileInputStream(file);
            try {
                final FileChannel channel = fileInputStream.getChannel();
                final MappedByteBuffer map = channel.map(FileChannel.MapMode.READ_ONLY, 0L, channel.size());
                fileInputStream.close();
                return map;
            }
            finally {
                try {
                    fileInputStream.close();
                }
                finally {
                    final Throwable exception;
                    ((Throwable)file).addSuppressed(exception);
                }
            }
        }
        catch (final IOException ex) {
            return null;
        }
    }
    
    @NonNull
    @RequiresApi(19)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static Map<Uri, ByteBuffer> readFontInfoIntoByteBuffer(@NonNull final Context context, @NonNull final FontsContractCompat.FontInfo[] array, @Nullable final CancellationSignal cancellationSignal) {
        final HashMap m = new HashMap();
        for (final FontsContractCompat.FontInfo fontInfo : array) {
            if (fontInfo.getResultCode() == 0) {
                final Uri uri = fontInfo.getUri();
                if (!m.containsKey(uri)) {
                    m.put(uri, mmap(context, cancellationSignal, uri));
                }
            }
        }
        return (Map<Uri, ByteBuffer>)Collections.unmodifiableMap((Map<?, ?>)m);
    }
    
    @RequiresApi(19)
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        @DoNotInline
        static ParcelFileDescriptor openFileDescriptor(final ContentResolver contentResolver, final Uri uri, final String s, final CancellationSignal cancellationSignal) throws FileNotFoundException {
            return contentResolver.openFileDescriptor(uri, s, cancellationSignal);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import android.graphics.Point;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.graphics.PointF;
import kotlin.Metadata;

@Metadata
public final class PointKt
{
    public static final float component1(@NotNull final PointF pointF) {
        Intrinsics.checkNotNullParameter((Object)pointF, "<this>");
        return pointF.x;
    }
    
    public static final int component1(@NotNull final Point point) {
        Intrinsics.checkNotNullParameter((Object)point, "<this>");
        return point.x;
    }
    
    public static final float component2(@NotNull final PointF pointF) {
        Intrinsics.checkNotNullParameter((Object)pointF, "<this>");
        return pointF.y;
    }
    
    public static final int component2(@NotNull final Point point) {
        Intrinsics.checkNotNullParameter((Object)point, "<this>");
        return point.y;
    }
    
    @NotNull
    public static final Point minus(@NotNull Point point, int n) {
        Intrinsics.checkNotNullParameter((Object)point, "<this>");
        point = new Point(point.x, point.y);
        n = -n;
        point.offset(n, n);
        return point;
    }
    
    @NotNull
    public static final Point minus(@NotNull Point point, @NotNull final Point point2) {
        Intrinsics.checkNotNullParameter((Object)point, "<this>");
        Intrinsics.checkNotNullParameter((Object)point2, "p");
        point = new Point(point.x, point.y);
        point.offset(-point2.x, -point2.y);
        return point;
    }
    
    @NotNull
    public static final PointF minus(@NotNull PointF pointF, float n) {
        Intrinsics.checkNotNullParameter((Object)pointF, "<this>");
        pointF = new PointF(pointF.x, pointF.y);
        n = -n;
        pointF.offset(n, n);
        return pointF;
    }
    
    @NotNull
    public static final PointF minus(@NotNull PointF pointF, @NotNull final PointF pointF2) {
        Intrinsics.checkNotNullParameter((Object)pointF, "<this>");
        Intrinsics.checkNotNullParameter((Object)pointF2, "p");
        pointF = new PointF(pointF.x, pointF.y);
        pointF.offset(-pointF2.x, -pointF2.y);
        return pointF;
    }
    
    @NotNull
    public static final Point plus(@NotNull Point point, final int n) {
        Intrinsics.checkNotNullParameter((Object)point, "<this>");
        point = new Point(point.x, point.y);
        point.offset(n, n);
        return point;
    }
    
    @NotNull
    public static final Point plus(@NotNull Point point, @NotNull final Point point2) {
        Intrinsics.checkNotNullParameter((Object)point, "<this>");
        Intrinsics.checkNotNullParameter((Object)point2, "p");
        point = new Point(point.x, point.y);
        point.offset(point2.x, point2.y);
        return point;
    }
    
    @NotNull
    public static final PointF plus(@NotNull PointF pointF, final float n) {
        Intrinsics.checkNotNullParameter((Object)pointF, "<this>");
        pointF = new PointF(pointF.x, pointF.y);
        pointF.offset(n, n);
        return pointF;
    }
    
    @NotNull
    public static final PointF plus(@NotNull PointF pointF, @NotNull final PointF pointF2) {
        Intrinsics.checkNotNullParameter((Object)pointF, "<this>");
        Intrinsics.checkNotNullParameter((Object)pointF2, "p");
        pointF = new PointF(pointF.x, pointF.y);
        pointF.offset(pointF2.x, pointF2.y);
        return pointF;
    }
    
    @NotNull
    public static final Point toPoint(@NotNull final PointF pointF) {
        Intrinsics.checkNotNullParameter((Object)pointF, "<this>");
        return new Point((int)pointF.x, (int)pointF.y);
    }
    
    @NotNull
    public static final PointF toPointF(@NotNull final Point point) {
        Intrinsics.checkNotNullParameter((Object)point, "<this>");
        return new PointF(point);
    }
    
    @NotNull
    public static final Point unaryMinus(@NotNull final Point point) {
        Intrinsics.checkNotNullParameter((Object)point, "<this>");
        return new Point(-point.x, -point.y);
    }
    
    @NotNull
    public static final PointF unaryMinus(@NotNull final PointF pointF) {
        Intrinsics.checkNotNullParameter((Object)pointF, "<this>");
        return new PointF(-pointF.x, -pointF.y);
    }
}

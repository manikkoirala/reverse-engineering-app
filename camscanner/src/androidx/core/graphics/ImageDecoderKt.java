// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import android.graphics.drawable.Drawable;
import androidx.annotation.RequiresApi;
import android.graphics.ImageDecoder$OnHeaderDecodedListener;
import kotlin.jvm.internal.Intrinsics;
import android.graphics.Bitmap;
import kotlin.Unit;
import android.graphics.ImageDecoder$ImageInfo;
import android.graphics.ImageDecoder;
import kotlin.jvm.functions.Function3;
import org.jetbrains.annotations.NotNull;
import android.graphics.ImageDecoder$Source;
import android.annotation.SuppressLint;
import kotlin.Metadata;

@Metadata
@SuppressLint({ "ClassVerificationFailure" })
public final class ImageDecoderKt
{
    @RequiresApi(28)
    @NotNull
    public static final Bitmap decodeBitmap(@NotNull final ImageDecoder$Source imageDecoder$Source, @NotNull final Function3<? super ImageDecoder, ? super ImageDecoder$ImageInfo, ? super ImageDecoder$Source, Unit> function3) {
        Intrinsics.checkNotNullParameter((Object)imageDecoder$Source, "<this>");
        Intrinsics.checkNotNullParameter((Object)function3, "action");
        final Bitmap \u3007080 = \u3007\u3007o8.\u3007080(imageDecoder$Source, (ImageDecoder$OnHeaderDecodedListener)new ImageDecoderKt$decodeBitmap.ImageDecoderKt$decodeBitmap$1((Function3)function3));
        Intrinsics.checkNotNullExpressionValue((Object)\u3007080, "crossinline action: Imag\u2026ction(info, source)\n    }");
        return \u3007080;
    }
    
    @RequiresApi(28)
    @NotNull
    public static final Drawable decodeDrawable(@NotNull final ImageDecoder$Source imageDecoder$Source, @NotNull final Function3<? super ImageDecoder, ? super ImageDecoder$ImageInfo, ? super ImageDecoder$Source, Unit> function3) {
        Intrinsics.checkNotNullParameter((Object)imageDecoder$Source, "<this>");
        Intrinsics.checkNotNullParameter((Object)function3, "action");
        final Drawable \u3007080 = \u3007o0O0O8.\u3007080(imageDecoder$Source, (ImageDecoder$OnHeaderDecodedListener)new ImageDecoderKt$decodeDrawable.ImageDecoderKt$decodeDrawable$1((Function3)function3));
        Intrinsics.checkNotNullExpressionValue((Object)\u3007080, "crossinline action: Imag\u2026ction(info, source)\n    }");
        return \u3007080;
    }
}

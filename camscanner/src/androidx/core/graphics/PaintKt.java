// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.graphics.Paint;
import kotlin.Metadata;

@Metadata
public final class PaintKt
{
    public static final boolean setBlendMode(@NotNull final Paint paint, final BlendModeCompat blendModeCompat) {
        Intrinsics.checkNotNullParameter((Object)paint, "<this>");
        return PaintCompat.setBlendMode(paint, blendModeCompat);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.lang.reflect.InvocationTargetException;
import androidx.annotation.GuardedBy;
import android.util.SparseArray;
import androidx.collection.LongSparseArray;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import android.graphics.Typeface;
import java.lang.reflect.Constructor;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;
import android.annotation.SuppressLint;

@SuppressLint({ "SoonBlockedPrivateApi" })
@RequiresApi(21)
@RestrictTo({ RestrictTo.Scope.LIBRARY })
final class WeightTypefaceApi21
{
    private static final String NATIVE_CREATE_FROM_TYPEFACE_METHOD = "nativeCreateFromTypeface";
    private static final String NATIVE_CREATE_WEIGHT_ALIAS_METHOD = "nativeCreateWeightAlias";
    private static final String NATIVE_INSTANCE_FIELD = "native_instance";
    private static final String TAG = "WeightTypeface";
    private static final Constructor<Typeface> sConstructor;
    private static final Method sNativeCreateFromTypeface;
    private static final Method sNativeCreateWeightAlias;
    private static final Field sNativeInstance;
    private static final Object sWeightCacheLock;
    @GuardedBy("sWeightCacheLock")
    private static final LongSparseArray<SparseArray<Typeface>> sWeightTypefaceCache;
    
    static {
        Field declaredField;
        Method declaredMethod;
        Method declaredMethod2;
        Constructor<Typeface> declaredConstructor;
        try {
            declaredField = Typeface.class.getDeclaredField("native_instance");
            final Class<Long> type = Long.TYPE;
            final Class<Integer> type2 = Integer.TYPE;
            declaredMethod = Typeface.class.getDeclaredMethod("nativeCreateFromTypeface", type, type2);
            declaredMethod.setAccessible(true);
            declaredMethod2 = Typeface.class.getDeclaredMethod("nativeCreateWeightAlias", type, type2);
            declaredMethod2.setAccessible(true);
            declaredConstructor = Typeface.class.getDeclaredConstructor(type);
            declaredConstructor.setAccessible(true);
        }
        catch (final NoSuchFieldException | NoSuchMethodException ex) {
            declaredField = null;
            declaredConstructor = null;
            declaredMethod = (declaredMethod2 = null);
        }
        sNativeInstance = declaredField;
        sNativeCreateFromTypeface = declaredMethod;
        sNativeCreateWeightAlias = declaredMethod2;
        sConstructor = declaredConstructor;
        sWeightTypefaceCache = new LongSparseArray<SparseArray<Typeface>>(3);
        sWeightCacheLock = new Object();
    }
    
    private WeightTypefaceApi21() {
    }
    
    @Nullable
    private static Typeface create(final long l) {
        try {
            return WeightTypefaceApi21.sConstructor.newInstance(l);
        }
        catch (final IllegalAccessException | InstantiationException | InvocationTargetException ex) {
            return null;
        }
    }
    
    @Nullable
    static Typeface createWeightStyle(@NonNull Typeface typeface, final int n, final boolean b) {
        if (!isPrivateApiAvailable()) {
            return null;
        }
        final int n2 = n << 1 | (b ? 1 : 0);
        synchronized (WeightTypefaceApi21.sWeightCacheLock) {
            final long nativeInstance = getNativeInstance(typeface);
            final LongSparseArray<SparseArray<Typeface>> sWeightTypefaceCache = WeightTypefaceApi21.sWeightTypefaceCache;
            SparseArray sparseArray = sWeightTypefaceCache.get(nativeInstance);
            if (sparseArray == null) {
                sparseArray = new SparseArray(4);
                sWeightTypefaceCache.put(nativeInstance, (SparseArray<Typeface>)sparseArray);
            }
            else {
                final Typeface typeface2 = (Typeface)sparseArray.get(n2);
                if (typeface2 != null) {
                    return typeface2;
                }
            }
            if (b == typeface.isItalic()) {
                typeface = create(nativeCreateWeightAlias(nativeInstance, n));
            }
            else {
                typeface = create(nativeCreateFromTypefaceWithExactStyle(nativeInstance, n, b));
            }
            sparseArray.put(n2, (Object)typeface);
            return typeface;
        }
    }
    
    private static long getNativeInstance(@NonNull final Typeface obj) {
        try {
            return WeightTypefaceApi21.sNativeInstance.getLong(obj);
        }
        catch (final IllegalAccessException cause) {
            throw new RuntimeException(cause);
        }
    }
    
    private static boolean isPrivateApiAvailable() {
        return WeightTypefaceApi21.sNativeInstance != null;
    }
    
    @SuppressLint({ "BanUncheckedReflection" })
    private static long nativeCreateFromTypefaceWithExactStyle(long n, final int i, final boolean b) {
        int j;
        if (b) {
            j = 2;
        }
        else {
            j = 0;
        }
        try {
            n = (long)WeightTypefaceApi21.sNativeCreateFromTypeface.invoke(null, n, j);
            n = (long)WeightTypefaceApi21.sNativeCreateWeightAlias.invoke(null, n, i);
            return n;
        }
        catch (final InvocationTargetException cause) {
            throw new RuntimeException(cause);
        }
        catch (final IllegalAccessException cause2) {
            throw new RuntimeException(cause2);
        }
    }
    
    @SuppressLint({ "BanUncheckedReflection" })
    private static long nativeCreateWeightAlias(long longValue, final int i) {
        try {
            longValue = (long)WeightTypefaceApi21.sNativeCreateWeightAlias.invoke(null, longValue, i);
            return longValue;
        }
        catch (final InvocationTargetException cause) {
            throw new RuntimeException(cause);
        }
        catch (final IllegalAccessException cause2) {
            throw new RuntimeException(cause2);
        }
    }
}

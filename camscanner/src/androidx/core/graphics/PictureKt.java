// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import kotlin.jvm.internal.InlineMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import android.graphics.Canvas;
import kotlin.jvm.functions.Function1;
import org.jetbrains.annotations.NotNull;
import android.graphics.Picture;
import kotlin.Metadata;

@Metadata
public final class PictureKt
{
    @NotNull
    public static final Picture record(@NotNull final Picture picture, final int n, final int n2, @NotNull final Function1<? super Canvas, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)picture, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final Canvas beginRecording = picture.beginRecording(n, n2);
        Intrinsics.checkNotNullExpressionValue((Object)beginRecording, "beginRecording(width, height)");
        try {
            function1.invoke((Object)beginRecording);
            return picture;
        }
        finally {
            InlineMarker.\u3007o00\u3007\u3007Oo(1);
            picture.endRecording();
            InlineMarker.\u3007080(1);
        }
    }
}

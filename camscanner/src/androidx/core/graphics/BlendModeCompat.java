// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import androidx.annotation.RequiresApi;

public enum BlendModeCompat
{
    private static final BlendModeCompat[] $VALUES;
    
    CLEAR, 
    @RequiresApi(29)
    COLOR, 
    @RequiresApi(29)
    COLOR_BURN, 
    @RequiresApi(29)
    COLOR_DODGE, 
    DARKEN, 
    @RequiresApi(29)
    DIFFERENCE, 
    DST, 
    DST_ATOP, 
    DST_IN, 
    DST_OUT, 
    DST_OVER, 
    @RequiresApi(29)
    EXCLUSION, 
    @RequiresApi(29)
    HARD_LIGHT, 
    @RequiresApi(29)
    HUE, 
    LIGHTEN, 
    @RequiresApi(29)
    LUMINOSITY, 
    MODULATE, 
    @RequiresApi(29)
    MULTIPLY, 
    OVERLAY, 
    PLUS, 
    @RequiresApi(29)
    SATURATION, 
    SCREEN, 
    @RequiresApi(29)
    SOFT_LIGHT, 
    SRC, 
    SRC_ATOP, 
    SRC_IN, 
    SRC_OUT, 
    SRC_OVER, 
    XOR;
}

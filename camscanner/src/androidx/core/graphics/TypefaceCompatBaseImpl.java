// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.graphics;

import java.io.File;
import java.io.InputStream;
import java.io.IOException;
import java.io.Closeable;
import androidx.annotation.NonNull;
import androidx.core.provider.FontsContractCompat;
import android.os.CancellationSignal;
import android.content.res.Resources;
import android.content.Context;
import java.lang.reflect.Field;
import androidx.annotation.Nullable;
import android.graphics.Typeface;
import android.annotation.SuppressLint;
import androidx.core.content.res.FontResourcesParserCompat;
import java.util.concurrent.ConcurrentHashMap;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
class TypefaceCompatBaseImpl
{
    private static final int INVALID_KEY = 0;
    private static final String TAG = "TypefaceCompatBaseImpl";
    @SuppressLint({ "BanConcurrentHashMap" })
    private ConcurrentHashMap<Long, FontResourcesParserCompat.FontFamilyFilesResourceEntry> mFontFamilies;
    
    TypefaceCompatBaseImpl() {
        this.mFontFamilies = new ConcurrentHashMap<Long, FontResourcesParserCompat.FontFamilyFilesResourceEntry>();
    }
    
    private void addFontFamily(final Typeface typeface, final FontResourcesParserCompat.FontFamilyFilesResourceEntry value) {
        final long uniqueKey = getUniqueKey(typeface);
        if (uniqueKey != 0L) {
            this.mFontFamilies.put(uniqueKey, value);
        }
    }
    
    private FontResourcesParserCompat.FontFileResourceEntry findBestEntry(final FontResourcesParserCompat.FontFamilyFilesResourceEntry fontFamilyFilesResourceEntry, final int n) {
        return findBestFont(fontFamilyFilesResourceEntry.getEntries(), n, (StyleExtractor<FontResourcesParserCompat.FontFileResourceEntry>)new StyleExtractor<FontResourcesParserCompat.FontFileResourceEntry>(this) {
            final TypefaceCompatBaseImpl this$0;
            
            public int getWeight(final FontResourcesParserCompat.FontFileResourceEntry fontFileResourceEntry) {
                return fontFileResourceEntry.getWeight();
            }
            
            public boolean isItalic(final FontResourcesParserCompat.FontFileResourceEntry fontFileResourceEntry) {
                return fontFileResourceEntry.isItalic();
            }
        });
    }
    
    private FontResourcesParserCompat.FontFileResourceEntry findBestEntry(final FontResourcesParserCompat.FontFamilyFilesResourceEntry fontFamilyFilesResourceEntry, final int n, final boolean b) {
        return findBestFont(fontFamilyFilesResourceEntry.getEntries(), n, b, (StyleExtractor<FontResourcesParserCompat.FontFileResourceEntry>)new StyleExtractor<FontResourcesParserCompat.FontFileResourceEntry>(this) {
            final TypefaceCompatBaseImpl this$0;
            
            public int getWeight(final FontResourcesParserCompat.FontFileResourceEntry fontFileResourceEntry) {
                return fontFileResourceEntry.getWeight();
            }
            
            public boolean isItalic(final FontResourcesParserCompat.FontFileResourceEntry fontFileResourceEntry) {
                return fontFileResourceEntry.isItalic();
            }
        });
    }
    
    private static <T> T findBestFont(final T[] array, final int n, final StyleExtractor<T> styleExtractor) {
        int n2;
        if ((n & 0x1) == 0x0) {
            n2 = 400;
        }
        else {
            n2 = 700;
        }
        return findBestFont(array, n2, (n & 0x2) != 0x0, styleExtractor);
    }
    
    private static <T> T findBestFont(final T[] array, final int n, final boolean b, final StyleExtractor<T> styleExtractor) {
        final int length = array.length;
        T t = null;
        int n2 = Integer.MAX_VALUE;
        int n5;
        for (int i = 0; i < length; ++i, n2 = n5) {
            final T t2 = array[i];
            final int abs = Math.abs(styleExtractor.getWeight(t2) - n);
            int n3;
            if (styleExtractor.isItalic(t2) == b) {
                n3 = 0;
            }
            else {
                n3 = 1;
            }
            final int n4 = abs * 2 + n3;
            if (t == null || (n5 = n2) > n4) {
                t = t2;
                n5 = n4;
            }
        }
        return t;
    }
    
    private static long getUniqueKey(@Nullable final Typeface obj) {
        long longValue = 0L;
        if (obj == null) {
            return 0L;
        }
        try {
            final Field declaredField = Typeface.class.getDeclaredField("native_instance");
            declaredField.setAccessible(true);
            longValue = ((Number)declaredField.get(obj)).longValue();
            return longValue;
        }
        catch (final NoSuchFieldException | IllegalAccessException ex) {
            return longValue;
        }
    }
    
    @Nullable
    public Typeface createFromFontFamilyFilesResourceEntry(final Context context, final FontResourcesParserCompat.FontFamilyFilesResourceEntry fontFamilyFilesResourceEntry, final Resources resources, final int n) {
        final FontResourcesParserCompat.FontFileResourceEntry bestEntry = this.findBestEntry(fontFamilyFilesResourceEntry, n);
        if (bestEntry == null) {
            return null;
        }
        final Typeface fromResourcesFontFile = TypefaceCompat.createFromResourcesFontFile(context, resources, bestEntry.getResourceId(), bestEntry.getFileName(), 0, n);
        this.addFontFamily(fromResourcesFontFile, fontFamilyFilesResourceEntry);
        return fromResourcesFontFile;
    }
    
    @Nullable
    Typeface createFromFontFamilyFilesResourceEntry(final Context context, final FontResourcesParserCompat.FontFamilyFilesResourceEntry fontFamilyFilesResourceEntry, final Resources resources, final int n, final boolean b) {
        final FontResourcesParserCompat.FontFileResourceEntry bestEntry = this.findBestEntry(fontFamilyFilesResourceEntry, n, b);
        if (bestEntry == null) {
            return null;
        }
        final Typeface fromResourcesFontFile = TypefaceCompat.createFromResourcesFontFile(context, resources, bestEntry.getResourceId(), bestEntry.getFileName(), 0, 0);
        this.addFontFamily(fromResourcesFontFile, fontFamilyFilesResourceEntry);
        return fromResourcesFontFile;
    }
    
    @Nullable
    public Typeface createFromFontInfo(final Context context, @Nullable final CancellationSignal cancellationSignal, @NonNull final FontsContractCompat.FontInfo[] array, final int n) {
        if (array.length < 1) {
            return null;
        }
        final FontsContractCompat.FontInfo bestInfo = this.findBestInfo(array, n);
        try {
            final InputStream openInputStream = context.getContentResolver().openInputStream(bestInfo.getUri());
            try {
                final Typeface fromInputStream = this.createFromInputStream(context, openInputStream);
                TypefaceCompatUtil.closeQuietly(openInputStream);
                return fromInputStream;
            }
            catch (final IOException ex) {}
        }
        catch (final IOException ex2) {}
    }
    
    protected Typeface createFromInputStream(Context tempFile, final InputStream inputStream) {
        tempFile = (Context)TypefaceCompatUtil.getTempFile(tempFile);
        if (tempFile == null) {
            return null;
        }
        try {
            if (!TypefaceCompatUtil.copyToFile((File)tempFile, inputStream)) {
                return null;
            }
            return Typeface.createFromFile(((File)tempFile).getPath());
        }
        catch (final RuntimeException ex) {
            return null;
        }
        finally {
            ((File)tempFile).delete();
        }
    }
    
    @Nullable
    public Typeface createFromResourcesFontFile(Context tempFile, final Resources resources, final int n, final String s, final int n2) {
        tempFile = (Context)TypefaceCompatUtil.getTempFile(tempFile);
        if (tempFile == null) {
            return null;
        }
        try {
            if (!TypefaceCompatUtil.copyToFile((File)tempFile, resources, n)) {
                return null;
            }
            return Typeface.createFromFile(((File)tempFile).getPath());
        }
        catch (final RuntimeException ex) {
            return null;
        }
        finally {
            ((File)tempFile).delete();
        }
    }
    
    @NonNull
    Typeface createWeightStyle(@NonNull final Context context, @NonNull final Typeface typeface, final int n, final boolean b) {
        Typeface weightStyle;
        try {
            weightStyle = WeightTypefaceApi14.createWeightStyle(this, context, typeface, n, b);
        }
        catch (final RuntimeException ex) {
            weightStyle = null;
        }
        if (weightStyle == null) {
            weightStyle = typeface;
        }
        return weightStyle;
    }
    
    protected FontsContractCompat.FontInfo findBestInfo(final FontsContractCompat.FontInfo[] array, final int n) {
        return findBestFont(array, n, (StyleExtractor<FontsContractCompat.FontInfo>)new StyleExtractor<FontsContractCompat.FontInfo>(this) {
            final TypefaceCompatBaseImpl this$0;
            
            public int getWeight(final FontsContractCompat.FontInfo fontInfo) {
                return fontInfo.getWeight();
            }
            
            public boolean isItalic(final FontsContractCompat.FontInfo fontInfo) {
                return fontInfo.isItalic();
            }
        });
    }
    
    @Nullable
    FontResourcesParserCompat.FontFamilyFilesResourceEntry getFontFamily(final Typeface typeface) {
        final long uniqueKey = getUniqueKey(typeface);
        if (uniqueKey == 0L) {
            return null;
        }
        return this.mFontFamilies.get(uniqueKey);
    }
    
    private interface StyleExtractor<T>
    {
        int getWeight(final T p0);
        
        boolean isItalic(final T p0);
    }
}

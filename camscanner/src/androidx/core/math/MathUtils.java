// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.math;

public class MathUtils
{
    private MathUtils() {
    }
    
    public static int addExact(final int n, final int n2) {
        final int n3 = n + n2;
        if (((n ^ n3) & (n2 ^ n3)) >= 0) {
            return n3;
        }
        throw new ArithmeticException("integer overflow");
    }
    
    public static long addExact(final long n, final long n2) {
        final long n3 = n + n2;
        if (((n ^ n3) & (n2 ^ n3)) >= 0L) {
            return n3;
        }
        throw new ArithmeticException("long overflow");
    }
    
    public static double clamp(final double n, final double n2, final double n3) {
        if (n < n2) {
            return n2;
        }
        if (n > n3) {
            return n3;
        }
        return n;
    }
    
    public static float clamp(final float n, final float n2, final float n3) {
        if (n < n2) {
            return n2;
        }
        if (n > n3) {
            return n3;
        }
        return n;
    }
    
    public static int clamp(final int n, final int n2, final int n3) {
        if (n < n2) {
            return n2;
        }
        if (n > n3) {
            return n3;
        }
        return n;
    }
    
    public static long clamp(final long n, final long n2, final long n3) {
        if (n < n2) {
            return n2;
        }
        if (n > n3) {
            return n3;
        }
        return n;
    }
    
    public static int decrementExact(final int n) {
        if (n != Integer.MIN_VALUE) {
            return n - 1;
        }
        throw new ArithmeticException("integer overflow");
    }
    
    public static long decrementExact(final long n) {
        if (n != Long.MIN_VALUE) {
            return n - 1L;
        }
        throw new ArithmeticException("long overflow");
    }
    
    public static int incrementExact(final int n) {
        if (n != Integer.MAX_VALUE) {
            return n + 1;
        }
        throw new ArithmeticException("integer overflow");
    }
    
    public static long incrementExact(final long n) {
        if (n != Long.MAX_VALUE) {
            return n + 1L;
        }
        throw new ArithmeticException("long overflow");
    }
    
    public static int multiplyExact(int n, final int n2) {
        final long n3 = n * (long)n2;
        n = (int)n3;
        if (n == n3) {
            return n;
        }
        throw new ArithmeticException("integer overflow");
    }
    
    public static long multiplyExact(final long a, final long a2) {
        final long n = a * a2;
        if ((Math.abs(a) | Math.abs(a2)) >>> 31 != 0L) {
            if (a2 == 0L || n / a2 == a) {
                if (a != Long.MIN_VALUE) {
                    return n;
                }
                if (a2 != -1L) {
                    return n;
                }
            }
            throw new ArithmeticException("long overflow");
        }
        return n;
    }
    
    public static int negateExact(final int n) {
        if (n != Integer.MIN_VALUE) {
            return -n;
        }
        throw new ArithmeticException("integer overflow");
    }
    
    public static long negateExact(final long n) {
        if (n != Long.MIN_VALUE) {
            return -n;
        }
        throw new ArithmeticException("long overflow");
    }
    
    public static int subtractExact(final int n, final int n2) {
        final int n3 = n - n2;
        if (((n ^ n3) & (n2 ^ n)) >= 0) {
            return n3;
        }
        throw new ArithmeticException("integer overflow");
    }
    
    public static long subtractExact(final long n, final long n2) {
        final long n3 = n - n2;
        if (((n ^ n3) & (n2 ^ n)) >= 0L) {
            return n3;
        }
        throw new ArithmeticException("long overflow");
    }
    
    public static int toIntExact(final long n) {
        final int n2 = (int)n;
        if (n2 == n) {
            return n2;
        }
        throw new ArithmeticException("integer overflow");
    }
}

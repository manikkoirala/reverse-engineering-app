// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.database;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import androidx.annotation.NonNull;
import android.os.Build$VERSION;
import android.database.CursorWindow;
import androidx.annotation.Nullable;

public final class CursorWindowCompat
{
    private CursorWindowCompat() {
    }
    
    @NonNull
    public static CursorWindow create(@Nullable final String s, final long n) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.createCursorWindow(s, n);
        }
        return Api15Impl.createCursorWindow(s);
    }
    
    @RequiresApi(15)
    static class Api15Impl
    {
        private Api15Impl() {
        }
        
        @DoNotInline
        static CursorWindow createCursorWindow(final String s) {
            return new CursorWindow(s);
        }
    }
    
    @RequiresApi(28)
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        @DoNotInline
        static CursorWindow createCursorWindow(final String s, final long n) {
            return new CursorWindow(s, n);
        }
    }
}

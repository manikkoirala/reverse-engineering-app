// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.database;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.database.Cursor;
import kotlin.Metadata;

@Metadata
public final class CursorKt
{
    public static final byte[] getBlobOrNull(@NotNull final Cursor cursor, final int n) {
        Intrinsics.checkNotNullParameter((Object)cursor, "<this>");
        byte[] blob;
        if (cursor.isNull(n)) {
            blob = null;
        }
        else {
            blob = cursor.getBlob(n);
        }
        return blob;
    }
    
    public static final Double getDoubleOrNull(@NotNull final Cursor cursor, final int n) {
        Intrinsics.checkNotNullParameter((Object)cursor, "<this>");
        Double value;
        if (cursor.isNull(n)) {
            value = null;
        }
        else {
            value = cursor.getDouble(n);
        }
        return value;
    }
    
    public static final Float getFloatOrNull(@NotNull final Cursor cursor, final int n) {
        Intrinsics.checkNotNullParameter((Object)cursor, "<this>");
        Float value;
        if (cursor.isNull(n)) {
            value = null;
        }
        else {
            value = cursor.getFloat(n);
        }
        return value;
    }
    
    public static final Integer getIntOrNull(@NotNull final Cursor cursor, final int n) {
        Intrinsics.checkNotNullParameter((Object)cursor, "<this>");
        Integer value;
        if (cursor.isNull(n)) {
            value = null;
        }
        else {
            value = cursor.getInt(n);
        }
        return value;
    }
    
    public static final Long getLongOrNull(@NotNull final Cursor cursor, final int n) {
        Intrinsics.checkNotNullParameter((Object)cursor, "<this>");
        Long value;
        if (cursor.isNull(n)) {
            value = null;
        }
        else {
            value = cursor.getLong(n);
        }
        return value;
    }
    
    public static final Short getShortOrNull(@NotNull final Cursor cursor, final int n) {
        Intrinsics.checkNotNullParameter((Object)cursor, "<this>");
        Short value;
        if (cursor.isNull(n)) {
            value = null;
        }
        else {
            value = cursor.getShort(n);
        }
        return value;
    }
    
    public static final String getStringOrNull(@NotNull final Cursor cursor, final int n) {
        Intrinsics.checkNotNullParameter((Object)cursor, "<this>");
        String string;
        if (cursor.isNull(n)) {
            string = null;
        }
        else {
            string = cursor.getString(n);
        }
        return string;
    }
}

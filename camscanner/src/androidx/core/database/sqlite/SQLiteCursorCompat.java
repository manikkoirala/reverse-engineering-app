// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.database.sqlite;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.database.sqlite.SQLiteCursor;

public final class SQLiteCursorCompat
{
    private SQLiteCursorCompat() {
    }
    
    public static void setFillWindowForwardOnly(@NonNull final SQLiteCursor sqLiteCursor, final boolean b) {
        if (Build$VERSION.SDK_INT >= 28) {
            Api28Impl.setFillWindowForwardOnly(sqLiteCursor, b);
        }
    }
    
    @RequiresApi(28)
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        @DoNotInline
        static void setFillWindowForwardOnly(final SQLiteCursor sqLiteCursor, final boolean b) {
            \u3007080.\u3007080(sqLiteCursor, b);
        }
    }
}

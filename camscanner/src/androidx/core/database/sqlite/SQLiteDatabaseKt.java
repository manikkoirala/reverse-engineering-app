// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.database.sqlite;

import kotlin.jvm.internal.InlineMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.functions.Function1;
import org.jetbrains.annotations.NotNull;
import android.database.sqlite.SQLiteDatabase;
import kotlin.Metadata;

@Metadata
public final class SQLiteDatabaseKt
{
    public static final <T> T transaction(@NotNull final SQLiteDatabase sqLiteDatabase, final boolean b, @NotNull final Function1<? super SQLiteDatabase, ? extends T> function1) {
        Intrinsics.checkNotNullParameter((Object)sqLiteDatabase, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "body");
        if (b) {
            sqLiteDatabase.beginTransaction();
        }
        else {
            sqLiteDatabase.beginTransactionNonExclusive();
        }
        try {
            final Object invoke = function1.invoke((Object)sqLiteDatabase);
            sqLiteDatabase.setTransactionSuccessful();
            return (T)invoke;
        }
        finally {
            InlineMarker.\u3007o00\u3007\u3007Oo(1);
            sqLiteDatabase.endTransaction();
            InlineMarker.\u3007080(1);
        }
    }
}

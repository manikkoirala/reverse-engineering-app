// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import androidx.annotation.NonNull;
import androidx.core.util.Consumer;

public interface OnTrimMemoryProvider
{
    void addOnTrimMemoryListener(@NonNull final Consumer<Integer> p0);
    
    void removeOnTrimMemoryListener(@NonNull final Consumer<Integer> p0);
}

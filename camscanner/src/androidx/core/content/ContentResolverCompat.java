// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.os.OperationCanceledException;
import android.database.Cursor;
import androidx.core.os.CancellationSignal;
import androidx.annotation.Nullable;
import android.net.Uri;
import androidx.annotation.NonNull;
import android.content.ContentResolver;

public final class ContentResolverCompat
{
    private ContentResolverCompat() {
    }
    
    @Nullable
    public static Cursor query(@NonNull final ContentResolver contentResolver, @NonNull final Uri uri, @Nullable final String[] array, @Nullable final String s, @Nullable final String[] array2, @Nullable final String s2, @Nullable final CancellationSignal cancellationSignal) {
        Label_0019: {
            if (cancellationSignal == null) {
                break Label_0019;
            }
            try {
                final Object cancellationSignalObject = cancellationSignal.getCancellationSignalObject();
                return Api16Impl.query(contentResolver, uri, array, s, array2, s2, (android.os.CancellationSignal)cancellationSignalObject);
            }
            catch (final Exception ex) {
                if (ex instanceof OperationCanceledException) {
                    throw new androidx.core.os.OperationCanceledException();
                }
                throw ex;
                final Object cancellationSignalObject = null;
                return Api16Impl.query(contentResolver, uri, array, s, array2, s2, (android.os.CancellationSignal)cancellationSignalObject);
            }
        }
    }
    
    @RequiresApi(16)
    static class Api16Impl
    {
        private Api16Impl() {
        }
        
        @DoNotInline
        static Cursor query(final ContentResolver contentResolver, final Uri uri, final String[] array, final String s, final String[] array2, final String s2, final android.os.CancellationSignal cancellationSignal) {
            return contentResolver.query(uri, array, s, array2, s2, cancellationSignal);
        }
    }
}

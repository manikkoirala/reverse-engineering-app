// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import kotlin.jvm.internal.Intrinsics;
import android.content.ContentValues;
import org.jetbrains.annotations.NotNull;
import kotlin.Pair;
import kotlin.Metadata;

@Metadata
public final class ContentValuesKt
{
    @NotNull
    public static final ContentValues contentValuesOf(@NotNull final Pair<String, ?>... array) {
        Intrinsics.checkNotNullParameter((Object)array, "pairs");
        final ContentValues contentValues = new ContentValues(array.length);
        for (final Pair<String, ?> pair : array) {
            final String str = (String)pair.component1();
            final Object component2 = pair.component2();
            if (component2 == null) {
                contentValues.putNull(str);
            }
            else if (component2 instanceof String) {
                contentValues.put(str, (String)component2);
            }
            else if (component2 instanceof Integer) {
                contentValues.put(str, (Integer)component2);
            }
            else if (component2 instanceof Long) {
                contentValues.put(str, (Long)component2);
            }
            else if (component2 instanceof Boolean) {
                contentValues.put(str, (Boolean)component2);
            }
            else if (component2 instanceof Float) {
                contentValues.put(str, (Float)component2);
            }
            else if (component2 instanceof Double) {
                contentValues.put(str, (Double)component2);
            }
            else if (component2 instanceof byte[]) {
                contentValues.put(str, (byte[])component2);
            }
            else if (component2 instanceof Byte) {
                contentValues.put(str, (Byte)component2);
            }
            else {
                if (!(component2 instanceof Short)) {
                    final String canonicalName = ((Short)component2).getClass().getCanonicalName();
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Illegal value type ");
                    sb.append(canonicalName);
                    sb.append(" for key \"");
                    sb.append(str);
                    sb.append('\"');
                    throw new IllegalArgumentException(sb.toString());
                }
                contentValues.put(str, (Short)component2);
            }
        }
        return contentValues;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content.pm;

import android.content.pm.PackageItemInfo;
import android.os.BaseBundle;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.core.util.Preconditions;
import androidx.core.net.UriCompat;
import java.util.HashMap;
import android.annotation.SuppressLint;
import java.util.Collection;
import java.util.HashSet;
import android.net.Uri;
import java.util.Map;
import android.text.TextUtils;
import android.content.pm.ShortcutInfo$Builder;
import java.util.Arrays;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.content.pm.PackageManager$NameNotFoundException;
import android.os.Parcelable;
import androidx.annotation.VisibleForTesting;
import androidx.core.app.O88o\u3007;
import android.os.Build$VERSION;
import java.util.Iterator;
import java.util.ArrayList;
import android.content.pm.ShortcutInfo;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;
import androidx.core.app.o08O;
import android.os.UserHandle;
import android.os.Bundle;
import androidx.core.app.Person;
import androidx.annotation.Nullable;
import androidx.core.content.LocusIdCompat;
import android.content.Intent;
import androidx.core.graphics.drawable.IconCompat;
import android.os.PersistableBundle;
import android.content.Context;
import java.util.Set;
import android.content.ComponentName;

public class ShortcutInfoCompat
{
    private static final String EXTRA_LOCUS_ID = "extraLocusId";
    private static final String EXTRA_LONG_LIVED = "extraLongLived";
    private static final String EXTRA_PERSON_ = "extraPerson_";
    private static final String EXTRA_PERSON_COUNT = "extraPersonCount";
    private static final String EXTRA_SLICE_URI = "extraSliceUri";
    public static final int SURFACE_LAUNCHER = 1;
    ComponentName mActivity;
    Set<String> mCategories;
    Context mContext;
    CharSequence mDisabledMessage;
    int mDisabledReason;
    int mExcludedSurfaces;
    PersistableBundle mExtras;
    boolean mHasKeyFieldsOnly;
    IconCompat mIcon;
    String mId;
    Intent[] mIntents;
    boolean mIsAlwaysBadged;
    boolean mIsCached;
    boolean mIsDeclaredInManifest;
    boolean mIsDynamic;
    boolean mIsEnabled;
    boolean mIsImmutable;
    boolean mIsLongLived;
    boolean mIsPinned;
    CharSequence mLabel;
    long mLastChangedTimestamp;
    @Nullable
    LocusIdCompat mLocusId;
    CharSequence mLongLabel;
    String mPackageName;
    Person[] mPersons;
    int mRank;
    Bundle mTransientExtras;
    UserHandle mUser;
    
    ShortcutInfoCompat() {
        this.mIsEnabled = true;
    }
    
    @RequiresApi(22)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    private PersistableBundle buildLegacyExtrasBundle() {
        if (this.mExtras == null) {
            this.mExtras = new PersistableBundle();
        }
        final Person[] mPersons = this.mPersons;
        if (mPersons != null && mPersons.length > 0) {
            ((BaseBundle)this.mExtras).putInt("extraPersonCount", mPersons.length);
            int j;
            for (int i = 0; i < this.mPersons.length; i = j) {
                final PersistableBundle mExtras = this.mExtras;
                final StringBuilder sb = new StringBuilder();
                sb.append("extraPerson_");
                j = i + 1;
                sb.append(j);
                mExtras.putPersistableBundle(sb.toString(), this.mPersons[i].toPersistableBundle());
            }
        }
        final LocusIdCompat mLocusId = this.mLocusId;
        if (mLocusId != null) {
            ((BaseBundle)this.mExtras).putString("extraLocusId", mLocusId.getId());
        }
        o08O.\u3007080(this.mExtras, "extraLongLived", this.mIsLongLived);
        return this.mExtras;
    }
    
    @RequiresApi(25)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    static List<ShortcutInfoCompat> fromShortcuts(@NonNull final Context context, @NonNull final List<ShortcutInfo> list) {
        final ArrayList list2 = new ArrayList(list.size());
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            list2.add(new Builder(context, (ShortcutInfo)iterator.next()).build());
        }
        return list2;
    }
    
    @Nullable
    @RequiresApi(25)
    static LocusIdCompat getLocusId(@NonNull final ShortcutInfo shortcutInfo) {
        if (Build$VERSION.SDK_INT < 29) {
            return getLocusIdFromExtra(OO0o\u3007\u3007\u3007\u30070.\u3007080(shortcutInfo));
        }
        if (O8.\u3007080(shortcutInfo) == null) {
            return null;
        }
        return LocusIdCompat.toLocusIdCompat(O8.\u3007080(shortcutInfo));
    }
    
    @Nullable
    @RequiresApi(25)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    private static LocusIdCompat getLocusIdFromExtra(@Nullable final PersistableBundle persistableBundle) {
        final LocusIdCompat locusIdCompat = null;
        if (persistableBundle == null) {
            return null;
        }
        final String string = ((BaseBundle)persistableBundle).getString("extraLocusId");
        LocusIdCompat locusIdCompat2;
        if (string == null) {
            locusIdCompat2 = locusIdCompat;
        }
        else {
            locusIdCompat2 = new LocusIdCompat(string);
        }
        return locusIdCompat2;
    }
    
    @RequiresApi(25)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    @VisibleForTesting
    static boolean getLongLivedFromExtra(@Nullable final PersistableBundle persistableBundle) {
        return persistableBundle != null && ((BaseBundle)persistableBundle).containsKey("extraLongLived") && O88o\u3007.\u3007080(persistableBundle, "extraLongLived");
    }
    
    @Nullable
    @RequiresApi(25)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    @VisibleForTesting
    static Person[] getPersonsFromExtra(@NonNull final PersistableBundle persistableBundle) {
        if (persistableBundle != null && ((BaseBundle)persistableBundle).containsKey("extraPersonCount")) {
            final int int1 = ((BaseBundle)persistableBundle).getInt("extraPersonCount");
            final Person[] array = new Person[int1];
            int j;
            for (int i = 0; i < int1; i = j) {
                final StringBuilder sb = new StringBuilder();
                sb.append("extraPerson_");
                j = i + 1;
                sb.append(j);
                array[i] = Person.fromPersistableBundle(persistableBundle.getPersistableBundle(sb.toString()));
            }
            return array;
        }
        return null;
    }
    
    Intent addToIntent(final Intent intent) {
        final Intent[] mIntents = this.mIntents;
        intent.putExtra("android.intent.extra.shortcut.INTENT", (Parcelable)mIntents[mIntents.length - 1]).putExtra("android.intent.extra.shortcut.NAME", this.mLabel.toString());
        if (this.mIcon != null) {
            final boolean mIsAlwaysBadged = this.mIsAlwaysBadged;
            Drawable loadIcon = null;
            final Drawable drawable = null;
            if (mIsAlwaysBadged) {
                final PackageManager packageManager = this.mContext.getPackageManager();
                final ComponentName mActivity = this.mActivity;
                Drawable activityIcon = drawable;
                if (mActivity != null) {
                    try {
                        activityIcon = packageManager.getActivityIcon(mActivity);
                    }
                    catch (final PackageManager$NameNotFoundException ex) {
                        activityIcon = drawable;
                    }
                }
                if ((loadIcon = activityIcon) == null) {
                    loadIcon = ((PackageItemInfo)this.mContext.getApplicationInfo()).loadIcon(packageManager);
                }
            }
            this.mIcon.addToShortcutIntent(intent, loadIcon, this.mContext);
        }
        return intent;
    }
    
    @Nullable
    public ComponentName getActivity() {
        return this.mActivity;
    }
    
    @Nullable
    public Set<String> getCategories() {
        return this.mCategories;
    }
    
    @Nullable
    public CharSequence getDisabledMessage() {
        return this.mDisabledMessage;
    }
    
    public int getDisabledReason() {
        return this.mDisabledReason;
    }
    
    public int getExcludedFromSurfaces() {
        return this.mExcludedSurfaces;
    }
    
    @Nullable
    public PersistableBundle getExtras() {
        return this.mExtras;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public IconCompat getIcon() {
        return this.mIcon;
    }
    
    @NonNull
    public String getId() {
        return this.mId;
    }
    
    @NonNull
    public Intent getIntent() {
        final Intent[] mIntents = this.mIntents;
        return mIntents[mIntents.length - 1];
    }
    
    @NonNull
    public Intent[] getIntents() {
        final Intent[] mIntents = this.mIntents;
        return Arrays.copyOf(mIntents, mIntents.length);
    }
    
    public long getLastChangedTimestamp() {
        return this.mLastChangedTimestamp;
    }
    
    @Nullable
    public LocusIdCompat getLocusId() {
        return this.mLocusId;
    }
    
    @Nullable
    public CharSequence getLongLabel() {
        return this.mLongLabel;
    }
    
    @NonNull
    public String getPackage() {
        return this.mPackageName;
    }
    
    public int getRank() {
        return this.mRank;
    }
    
    @NonNull
    public CharSequence getShortLabel() {
        return this.mLabel;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public Bundle getTransientExtras() {
        return this.mTransientExtras;
    }
    
    @Nullable
    public UserHandle getUserHandle() {
        return this.mUser;
    }
    
    public boolean hasKeyFieldsOnly() {
        return this.mHasKeyFieldsOnly;
    }
    
    public boolean isCached() {
        return this.mIsCached;
    }
    
    public boolean isDeclaredInManifest() {
        return this.mIsDeclaredInManifest;
    }
    
    public boolean isDynamic() {
        return this.mIsDynamic;
    }
    
    public boolean isEnabled() {
        return this.mIsEnabled;
    }
    
    public boolean isExcludedFromSurfaces(final int n) {
        return (n & this.mExcludedSurfaces) != 0x0;
    }
    
    public boolean isImmutable() {
        return this.mIsImmutable;
    }
    
    public boolean isPinned() {
        return this.mIsPinned;
    }
    
    @RequiresApi(25)
    public ShortcutInfo toShortcutInfo() {
        final ShortcutInfo$Builder \u3007080 = \u3007O00.\u3007080(\u30078o8o\u3007.\u3007080(new ShortcutInfo$Builder(this.mContext, this.mId), this.mLabel), this.mIntents);
        final IconCompat mIcon = this.mIcon;
        if (mIcon != null) {
            \u3007\u30078O0\u30078.\u3007080(\u3007080, mIcon.toIcon(this.mContext));
        }
        if (!TextUtils.isEmpty(this.mLongLabel)) {
            \u30070\u3007O0088o.\u3007080(\u3007080, this.mLongLabel);
        }
        if (!TextUtils.isEmpty(this.mDisabledMessage)) {
            Oo08.\u3007080(\u3007080, this.mDisabledMessage);
        }
        final ComponentName mActivity = this.mActivity;
        if (mActivity != null) {
            o\u30070.\u3007080(\u3007080, mActivity);
        }
        final Set<String> mCategories = this.mCategories;
        if (mCategories != null) {
            \u3007\u3007888.\u3007080(\u3007080, (Set)mCategories);
        }
        oO80.\u3007080(\u3007080, this.mRank);
        final PersistableBundle mExtras = this.mExtras;
        if (mExtras != null) {
            Oooo8o0\u3007.\u3007080(\u3007080, mExtras);
        }
        if (Build$VERSION.SDK_INT >= 29) {
            final Person[] mPersons = this.mPersons;
            if (mPersons != null && mPersons.length > 0) {
                final int length = mPersons.length;
                final android.app.Person[] array = new android.app.Person[length];
                for (int i = 0; i < length; ++i) {
                    array[i] = this.mPersons[i].toAndroidPerson();
                }
                \u300780\u3007808\u3007O.\u3007080(\u3007080, array);
            }
            final LocusIdCompat mLocusId = this.mLocusId;
            if (mLocusId != null) {
                \u3007O8o08O.\u3007080(\u3007080, mLocusId.toLocusId());
            }
            OO0o\u3007\u3007.\u3007080(\u3007080, this.mIsLongLived);
        }
        else {
            Oooo8o0\u3007.\u3007080(\u3007080, this.buildLegacyExtrasBundle());
        }
        return \u3007\u3007808\u3007.\u3007080(\u3007080);
    }
    
    public static class Builder
    {
        private Map<String, Map<String, List<String>>> mCapabilityBindingParams;
        private Set<String> mCapabilityBindings;
        private final ShortcutInfoCompat mInfo;
        private boolean mIsConversation;
        private Uri mSliceUri;
        
        @RequiresApi(25)
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public Builder(@NonNull final Context mContext, @NonNull final ShortcutInfo shortcutInfo) {
            final ShortcutInfoCompat mInfo = new ShortcutInfoCompat();
            this.mInfo = mInfo;
            mInfo.mContext = mContext;
            mInfo.mId = OoO8.\u3007080(shortcutInfo);
            mInfo.mPackageName = o800o8O.\u3007080(shortcutInfo);
            final Intent[] \u3007080 = oo88o8O.\u3007080(shortcutInfo);
            mInfo.mIntents = Arrays.copyOf(\u3007080, \u3007080.length);
            mInfo.mActivity = \u3007oo\u3007.\u3007080(shortcutInfo);
            mInfo.mLabel = o\u3007O8\u3007\u3007o.\u3007080(shortcutInfo);
            mInfo.mLongLabel = \u300700.\u3007080(shortcutInfo);
            mInfo.mDisabledMessage = O\u30078O8\u3007008.\u3007080(shortcutInfo);
            final int sdk_INT = Build$VERSION.SDK_INT;
            if (sdk_INT >= 28) {
                mInfo.mDisabledReason = O8ooOoo\u3007.\u3007080(shortcutInfo);
            }
            else {
                int mDisabledReason;
                if (o0ooO.\u3007080(shortcutInfo)) {
                    mDisabledReason = 0;
                }
                else {
                    mDisabledReason = 3;
                }
                mInfo.mDisabledReason = mDisabledReason;
            }
            mInfo.mCategories = \u3007oOO8O8.\u3007080(shortcutInfo);
            mInfo.mPersons = ShortcutInfoCompat.getPersonsFromExtra(OO0o\u3007\u3007\u3007\u30070.\u3007080(shortcutInfo));
            mInfo.mUser = \u30070000OOO.\u3007080(shortcutInfo);
            mInfo.mLastChangedTimestamp = o\u3007\u30070\u3007.\u3007080(shortcutInfo);
            if (sdk_INT >= 30) {
                mInfo.mIsCached = OOO\u3007O0.\u3007080(shortcutInfo);
            }
            mInfo.mIsDynamic = oo\u3007.\u3007080(shortcutInfo);
            mInfo.mIsPinned = O8\u3007o.\u3007080(shortcutInfo);
            mInfo.mIsDeclaredInManifest = \u300700\u30078.\u3007080(shortcutInfo);
            mInfo.mIsImmutable = \u3007o.\u3007080(shortcutInfo);
            mInfo.mIsEnabled = o0ooO.\u3007080(shortcutInfo);
            mInfo.mHasKeyFieldsOnly = o\u30078.\u3007080(shortcutInfo);
            mInfo.mLocusId = ShortcutInfoCompat.getLocusId(shortcutInfo);
            mInfo.mRank = \u3007O888o0o.\u3007080(shortcutInfo);
            mInfo.mExtras = OO0o\u3007\u3007\u3007\u30070.\u3007080(shortcutInfo);
        }
        
        public Builder(@NonNull final Context mContext, @NonNull final String mId) {
            final ShortcutInfoCompat mInfo = new ShortcutInfoCompat();
            this.mInfo = mInfo;
            mInfo.mContext = mContext;
            mInfo.mId = mId;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public Builder(@NonNull final ShortcutInfoCompat shortcutInfoCompat) {
            final ShortcutInfoCompat mInfo = new ShortcutInfoCompat();
            this.mInfo = mInfo;
            mInfo.mContext = shortcutInfoCompat.mContext;
            mInfo.mId = shortcutInfoCompat.mId;
            mInfo.mPackageName = shortcutInfoCompat.mPackageName;
            final Intent[] mIntents = shortcutInfoCompat.mIntents;
            mInfo.mIntents = Arrays.copyOf(mIntents, mIntents.length);
            mInfo.mActivity = shortcutInfoCompat.mActivity;
            mInfo.mLabel = shortcutInfoCompat.mLabel;
            mInfo.mLongLabel = shortcutInfoCompat.mLongLabel;
            mInfo.mDisabledMessage = shortcutInfoCompat.mDisabledMessage;
            mInfo.mDisabledReason = shortcutInfoCompat.mDisabledReason;
            mInfo.mIcon = shortcutInfoCompat.mIcon;
            mInfo.mIsAlwaysBadged = shortcutInfoCompat.mIsAlwaysBadged;
            mInfo.mUser = shortcutInfoCompat.mUser;
            mInfo.mLastChangedTimestamp = shortcutInfoCompat.mLastChangedTimestamp;
            mInfo.mIsCached = shortcutInfoCompat.mIsCached;
            mInfo.mIsDynamic = shortcutInfoCompat.mIsDynamic;
            mInfo.mIsPinned = shortcutInfoCompat.mIsPinned;
            mInfo.mIsDeclaredInManifest = shortcutInfoCompat.mIsDeclaredInManifest;
            mInfo.mIsImmutable = shortcutInfoCompat.mIsImmutable;
            mInfo.mIsEnabled = shortcutInfoCompat.mIsEnabled;
            mInfo.mLocusId = shortcutInfoCompat.mLocusId;
            mInfo.mIsLongLived = shortcutInfoCompat.mIsLongLived;
            mInfo.mHasKeyFieldsOnly = shortcutInfoCompat.mHasKeyFieldsOnly;
            mInfo.mRank = shortcutInfoCompat.mRank;
            final Person[] mPersons = shortcutInfoCompat.mPersons;
            if (mPersons != null) {
                mInfo.mPersons = Arrays.copyOf(mPersons, mPersons.length);
            }
            if (shortcutInfoCompat.mCategories != null) {
                mInfo.mCategories = new HashSet<String>(shortcutInfoCompat.mCategories);
            }
            final PersistableBundle mExtras = shortcutInfoCompat.mExtras;
            if (mExtras != null) {
                mInfo.mExtras = mExtras;
            }
            mInfo.mExcludedSurfaces = shortcutInfoCompat.mExcludedSurfaces;
        }
        
        @SuppressLint({ "MissingGetterMatchingBuilder" })
        @NonNull
        public Builder addCapabilityBinding(@NonNull final String s) {
            if (this.mCapabilityBindings == null) {
                this.mCapabilityBindings = new HashSet<String>();
            }
            this.mCapabilityBindings.add(s);
            return this;
        }
        
        @SuppressLint({ "MissingGetterMatchingBuilder" })
        @NonNull
        public Builder addCapabilityBinding(@NonNull final String s, @NonNull final String s2, @NonNull final List<String> list) {
            this.addCapabilityBinding(s);
            if (!list.isEmpty()) {
                if (this.mCapabilityBindingParams == null) {
                    this.mCapabilityBindingParams = new HashMap<String, Map<String, List<String>>>();
                }
                if (this.mCapabilityBindingParams.get(s) == null) {
                    this.mCapabilityBindingParams.put(s, new HashMap<String, List<String>>());
                }
                this.mCapabilityBindingParams.get(s).put(s2, list);
            }
            return this;
        }
        
        @NonNull
        public ShortcutInfoCompat build() {
            if (TextUtils.isEmpty(this.mInfo.mLabel)) {
                throw new IllegalArgumentException("Shortcut must have a non-empty label");
            }
            final ShortcutInfoCompat mInfo = this.mInfo;
            final Intent[] mIntents = mInfo.mIntents;
            if (mIntents != null && mIntents.length != 0) {
                if (this.mIsConversation) {
                    if (mInfo.mLocusId == null) {
                        mInfo.mLocusId = new LocusIdCompat(mInfo.mId);
                    }
                    this.mInfo.mIsLongLived = true;
                }
                if (this.mCapabilityBindings != null) {
                    final ShortcutInfoCompat mInfo2 = this.mInfo;
                    if (mInfo2.mCategories == null) {
                        mInfo2.mCategories = new HashSet<String>();
                    }
                    this.mInfo.mCategories.addAll(this.mCapabilityBindings);
                }
                if (this.mCapabilityBindingParams != null) {
                    final ShortcutInfoCompat mInfo3 = this.mInfo;
                    if (mInfo3.mExtras == null) {
                        mInfo3.mExtras = new PersistableBundle();
                    }
                    for (final String str : this.mCapabilityBindingParams.keySet()) {
                        final Map map = this.mCapabilityBindingParams.get(str);
                        ((BaseBundle)this.mInfo.mExtras).putStringArray(str, (String[])map.keySet().toArray(new String[0]));
                        for (final String str2 : map.keySet()) {
                            final List list = (List)map.get(str2);
                            final PersistableBundle mExtras = this.mInfo.mExtras;
                            final StringBuilder sb = new StringBuilder();
                            sb.append(str);
                            sb.append("/");
                            sb.append(str2);
                            final String string = sb.toString();
                            String[] array;
                            if (list == null) {
                                array = new String[0];
                            }
                            else {
                                array = (String[])list.toArray(new String[0]);
                            }
                            ((BaseBundle)mExtras).putStringArray(string, array);
                        }
                    }
                }
                if (this.mSliceUri != null) {
                    final ShortcutInfoCompat mInfo4 = this.mInfo;
                    if (mInfo4.mExtras == null) {
                        mInfo4.mExtras = new PersistableBundle();
                    }
                    ((BaseBundle)this.mInfo.mExtras).putString("extraSliceUri", UriCompat.toSafeString(this.mSliceUri));
                }
                return this.mInfo;
            }
            throw new IllegalArgumentException("Shortcut must have an intent");
        }
        
        @NonNull
        public Builder setActivity(@NonNull final ComponentName mActivity) {
            this.mInfo.mActivity = mActivity;
            return this;
        }
        
        @NonNull
        public Builder setAlwaysBadged() {
            this.mInfo.mIsAlwaysBadged = true;
            return this;
        }
        
        @NonNull
        public Builder setCategories(@NonNull final Set<String> mCategories) {
            this.mInfo.mCategories = mCategories;
            return this;
        }
        
        @NonNull
        public Builder setDisabledMessage(@NonNull final CharSequence mDisabledMessage) {
            this.mInfo.mDisabledMessage = mDisabledMessage;
            return this;
        }
        
        @NonNull
        public Builder setExcludedFromSurfaces(final int mExcludedSurfaces) {
            this.mInfo.mExcludedSurfaces = mExcludedSurfaces;
            return this;
        }
        
        @NonNull
        public Builder setExtras(@NonNull final PersistableBundle mExtras) {
            this.mInfo.mExtras = mExtras;
            return this;
        }
        
        @NonNull
        public Builder setIcon(final IconCompat mIcon) {
            this.mInfo.mIcon = mIcon;
            return this;
        }
        
        @NonNull
        public Builder setIntent(@NonNull final Intent intent) {
            return this.setIntents(new Intent[] { intent });
        }
        
        @NonNull
        public Builder setIntents(@NonNull final Intent[] mIntents) {
            this.mInfo.mIntents = mIntents;
            return this;
        }
        
        @NonNull
        public Builder setIsConversation() {
            this.mIsConversation = true;
            return this;
        }
        
        @NonNull
        public Builder setLocusId(@Nullable final LocusIdCompat mLocusId) {
            this.mInfo.mLocusId = mLocusId;
            return this;
        }
        
        @NonNull
        public Builder setLongLabel(@NonNull final CharSequence mLongLabel) {
            this.mInfo.mLongLabel = mLongLabel;
            return this;
        }
        
        @Deprecated
        @NonNull
        public Builder setLongLived() {
            this.mInfo.mIsLongLived = true;
            return this;
        }
        
        @NonNull
        public Builder setLongLived(final boolean mIsLongLived) {
            this.mInfo.mIsLongLived = mIsLongLived;
            return this;
        }
        
        @NonNull
        public Builder setPerson(@NonNull final Person person) {
            return this.setPersons(new Person[] { person });
        }
        
        @NonNull
        public Builder setPersons(@NonNull final Person[] mPersons) {
            this.mInfo.mPersons = mPersons;
            return this;
        }
        
        @NonNull
        public Builder setRank(final int mRank) {
            this.mInfo.mRank = mRank;
            return this;
        }
        
        @NonNull
        public Builder setShortLabel(@NonNull final CharSequence mLabel) {
            this.mInfo.mLabel = mLabel;
            return this;
        }
        
        @SuppressLint({ "MissingGetterMatchingBuilder" })
        @NonNull
        public Builder setSliceUri(@NonNull final Uri mSliceUri) {
            this.mSliceUri = mSliceUri;
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public Builder setTransientExtras(@NonNull final Bundle bundle) {
            this.mInfo.mTransientExtras = Preconditions.checkNotNull(bundle);
            return this;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public @interface Surface {
    }
}

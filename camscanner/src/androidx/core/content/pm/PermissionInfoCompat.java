// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content.pm;

import androidx.annotation.RestrictTo;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.annotation.SuppressLint;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.content.pm.PermissionInfo;

public final class PermissionInfoCompat
{
    private PermissionInfoCompat() {
    }
    
    @SuppressLint({ "WrongConstant" })
    public static int getProtection(@NonNull final PermissionInfo permissionInfo) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getProtection(permissionInfo);
        }
        return permissionInfo.protectionLevel & 0xF;
    }
    
    @SuppressLint({ "WrongConstant" })
    public static int getProtectionFlags(@NonNull final PermissionInfo permissionInfo) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getProtectionFlags(permissionInfo);
        }
        return permissionInfo.protectionLevel & 0xFFFFFFF0;
    }
    
    @RequiresApi(28)
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        @DoNotInline
        static int getProtection(final PermissionInfo permissionInfo) {
            return \u3007o00\u3007\u3007Oo.\u3007080(permissionInfo);
        }
        
        @DoNotInline
        static int getProtectionFlags(final PermissionInfo permissionInfo) {
            return \u3007o\u3007.\u3007080(permissionInfo);
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface Protection {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @SuppressLint({ "UniqueConstants" })
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface ProtectionFlags {
    }
}

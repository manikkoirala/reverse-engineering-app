// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content.pm;

import androidx.browser.trusted.\u3007O8o08O;
import androidx.browser.trusted.\u300780\u3007808\u3007O;
import androidx.browser.trusted.\u30078o8o\u3007;
import androidx.annotation.Nullable;
import androidx.annotation.DoNotInline;
import androidx.browser.trusted.OO0o\u3007\u3007\u3007\u30070;
import androidx.annotation.RequiresApi;
import java.util.Iterator;
import java.util.Set;
import androidx.annotation.Size;
import java.util.Map;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.pm.SigningInfo;
import java.util.Collections;
import androidx.browser.trusted.oO80;
import android.content.pm.Signature;
import java.util.List;
import android.content.pm.PackageManager;
import android.os.Build$VERSION;
import android.content.pm.PackageInfo;
import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;
import java.util.Arrays;
import androidx.annotation.NonNull;

public final class PackageInfoCompat
{
    private PackageInfoCompat() {
    }
    
    private static boolean byteArrayContains(@NonNull final byte[][] array, @NonNull final byte[] a) {
        for (int length = array.length, i = 0; i < length; ++i) {
            if (Arrays.equals(a, array[i])) {
                return true;
            }
        }
        return false;
    }
    
    private static byte[] computeSHA256Digest(byte[] digest) {
        try {
            digest = MessageDigest.getInstance("SHA256").digest(digest);
            return digest;
        }
        catch (final NoSuchAlgorithmException cause) {
            throw new RuntimeException("Device doesn't support SHA256 cert checking", cause);
        }
    }
    
    public static long getLongVersionCode(@NonNull final PackageInfo packageInfo) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getLongVersionCode(packageInfo);
        }
        return packageInfo.versionCode;
    }
    
    @NonNull
    public static List<Signature> getSignatures(@NonNull final PackageManager packageManager, @NonNull final String s) throws PackageManager$NameNotFoundException {
        Signature[] a;
        if (Build$VERSION.SDK_INT >= 28) {
            final SigningInfo \u3007080 = oO80.\u3007080(packageManager.getPackageInfo(s, 134217728));
            if (Api28Impl.hasMultipleSigners(\u3007080)) {
                a = Api28Impl.getApkContentsSigners(\u3007080);
            }
            else {
                a = Api28Impl.getSigningCertificateHistory(\u3007080);
            }
        }
        else {
            a = packageManager.getPackageInfo(s, 64).signatures;
        }
        if (a == null) {
            return Collections.emptyList();
        }
        return Arrays.asList(a);
    }
    
    public static boolean hasSignatures(@NonNull final PackageManager packageManager, @NonNull final String str, @NonNull @Size(min = 1L) final Map<byte[], Integer> map, final boolean b) throws PackageManager$NameNotFoundException {
        if (map.isEmpty()) {
            return false;
        }
        final Set<byte[]> keySet = map.keySet();
        for (final byte[] array : keySet) {
            if (array == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Cert byte array cannot be null when verifying ");
                sb.append(str);
                throw new IllegalArgumentException(sb.toString());
            }
            final Integer obj = map.get(array);
            if (obj == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Type must be specified for cert when verifying ");
                sb2.append(str);
                throw new IllegalArgumentException(sb2.toString());
            }
            final int intValue = obj;
            if (intValue == 0) {
                continue;
            }
            if (intValue == 1) {
                continue;
            }
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Unsupported certificate type ");
            sb3.append(obj);
            sb3.append(" when verifying ");
            sb3.append(str);
            throw new IllegalArgumentException(sb3.toString());
        }
        final List<Signature> signatures = getSignatures(packageManager, str);
        if (!b && Build$VERSION.SDK_INT >= 28) {
            for (final byte[] array2 : keySet) {
                if (!Api28Impl.hasSigningCertificate(packageManager, str, array2, map.get(array2))) {
                    return false;
                }
            }
            return true;
        }
        if (signatures.size() && map.size() <= signatures.size()) {
            if (!b || map.size() == signatures.size()) {
                byte[][] array4;
                if (map.containsValue(1)) {
                    final byte[][] array3 = new byte[signatures.size()][];
                    int n = 0;
                    while (true) {
                        array4 = array3;
                        if (n >= signatures.size()) {
                            break;
                        }
                        array3[n] = computeSHA256Digest(((Signature)signatures.get(n)).toByteArray());
                        ++n;
                    }
                }
                else {
                    array4 = null;
                }
                final Iterator<byte[]> iterator3 = keySet.iterator();
                if (iterator3.hasNext()) {
                    final byte[] array5 = iterator3.next();
                    final Integer obj2 = map.get(array5);
                    final int intValue2 = obj2;
                    if (intValue2 != 0) {
                        if (intValue2 != 1) {
                            final StringBuilder sb4 = new StringBuilder();
                            sb4.append("Unsupported certificate type ");
                            sb4.append(obj2);
                            throw new IllegalArgumentException(sb4.toString());
                        }
                        if (!byteArrayContains(array4, array5)) {
                            return false;
                        }
                    }
                    else if (!signatures.contains(new Signature(array5))) {
                        return false;
                    }
                    return true;
                }
            }
        }
        return false;
    }
    
    @RequiresApi(28)
    private static class Api28Impl
    {
        @DoNotInline
        @Nullable
        static Signature[] getApkContentsSigners(@NonNull final SigningInfo signingInfo) {
            return OO0o\u3007\u3007\u3007\u30070.\u3007080(signingInfo);
        }
        
        @DoNotInline
        static long getLongVersionCode(final PackageInfo packageInfo) {
            return \u3007080.\u3007080(packageInfo);
        }
        
        @DoNotInline
        @Nullable
        static Signature[] getSigningCertificateHistory(@NonNull final SigningInfo signingInfo) {
            return \u30078o8o\u3007.\u3007080(signingInfo);
        }
        
        @DoNotInline
        static boolean hasMultipleSigners(@NonNull final SigningInfo signingInfo) {
            return \u300780\u3007808\u3007O.\u3007080(signingInfo);
        }
        
        @DoNotInline
        static boolean hasSigningCertificate(@NonNull final PackageManager packageManager, @NonNull final String s, @NonNull final byte[] array, final int n) {
            return \u3007O8o08O.\u3007080(packageManager, s, array, n);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content.pm;

import android.os.BaseBundle;
import androidx.annotation.RestrictTo;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.annotation.RequiresApi;
import android.content.IntentSender$SendIntentException;
import android.os.Handler;
import android.content.IntentSender$OnFinished;
import android.content.BroadcastReceiver;
import android.content.IntentSender;
import java.util.Objects;
import java.util.Arrays;
import android.text.TextUtils;
import androidx.core.content.ContextCompat;
import java.util.Collections;
import android.os.Bundle;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import androidx.core.util.Preconditions;
import android.util.DisplayMetrics;
import android.app.ActivityManager;
import android.content.pm.ShortcutInfo;
import androidx.annotation.Nullable;
import android.content.Intent;
import java.util.Collection;
import android.graphics.Bitmap;
import java.io.InputStream;
import androidx.core.graphics.drawable.IconCompat;
import android.graphics.BitmapFactory;
import java.util.Iterator;
import androidx.appcompat.widget.oo88o8O;
import android.content.pm.ShortcutManager;
import java.util.ArrayList;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.content.Context;
import java.util.List;
import androidx.annotation.VisibleForTesting;

public class ShortcutManagerCompat
{
    @VisibleForTesting
    static final String ACTION_INSTALL_SHORTCUT = "com.android.launcher.action.INSTALL_SHORTCUT";
    private static final int DEFAULT_MAX_ICON_DIMENSION_DP = 96;
    private static final int DEFAULT_MAX_ICON_DIMENSION_LOWRAM_DP = 48;
    public static final String EXTRA_SHORTCUT_ID = "android.intent.extra.shortcut.ID";
    public static final int FLAG_MATCH_CACHED = 8;
    public static final int FLAG_MATCH_DYNAMIC = 2;
    public static final int FLAG_MATCH_MANIFEST = 1;
    public static final int FLAG_MATCH_PINNED = 4;
    @VisibleForTesting
    static final String INSTALL_SHORTCUT_PERMISSION = "com.android.launcher.permission.INSTALL_SHORTCUT";
    private static final String SHORTCUT_LISTENER_INTENT_FILTER_ACTION = "androidx.core.content.pm.SHORTCUT_LISTENER";
    private static final String SHORTCUT_LISTENER_META_DATA_KEY = "androidx.core.content.pm.shortcut_listener_impl";
    private static volatile List<ShortcutInfoChangeListener> sShortcutInfoChangeListeners;
    private static volatile ShortcutInfoCompatSaver<?> sShortcutInfoCompatSaver;
    
    private ShortcutManagerCompat() {
    }
    
    public static boolean addDynamicShortcuts(@NonNull final Context context, @NonNull final List<ShortcutInfoCompat> list) {
        final List<ShortcutInfoCompat> removeShortcutsExcludedFromSurface = removeShortcutsExcludedFromSurface(list, 1);
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT <= 29) {
            convertUriIconsToBitmapIcons(context, removeShortcutsExcludedFromSurface);
        }
        if (sdk_INT >= 25) {
            final ArrayList list2 = new ArrayList();
            final Iterator<ShortcutInfoCompat> iterator = removeShortcutsExcludedFromSurface.iterator();
            while (iterator.hasNext()) {
                list2.add(iterator.next().toShortcutInfo());
            }
            if (!oO00OOO.\u3007080((ShortcutManager)oo88o8O.\u3007080(context, (Class)ShortcutManager.class), (List)list2)) {
                return false;
            }
        }
        getShortcutInfoSaverInstance(context).addShortcuts(removeShortcutsExcludedFromSurface);
        final Iterator<ShortcutInfoChangeListener> iterator2 = getShortcutInfoListeners(context).iterator();
        while (iterator2.hasNext()) {
            iterator2.next().onShortcutAdded(list);
        }
        return true;
    }
    
    @VisibleForTesting
    static boolean convertUriIconToBitmapIcon(@NonNull final Context context, @NonNull final ShortcutInfoCompat shortcutInfoCompat) {
        final IconCompat mIcon = shortcutInfoCompat.mIcon;
        if (mIcon == null) {
            return false;
        }
        final int mType = mIcon.mType;
        if (mType != 6 && mType != 4) {
            return true;
        }
        final InputStream uriInputStream = mIcon.getUriInputStream(context);
        if (uriInputStream == null) {
            return false;
        }
        final Bitmap decodeStream = BitmapFactory.decodeStream(uriInputStream);
        if (decodeStream == null) {
            return false;
        }
        IconCompat mIcon2;
        if (mType == 6) {
            mIcon2 = IconCompat.createWithAdaptiveBitmap(decodeStream);
        }
        else {
            mIcon2 = IconCompat.createWithBitmap(decodeStream);
        }
        shortcutInfoCompat.mIcon = mIcon2;
        return true;
    }
    
    @VisibleForTesting
    static void convertUriIconsToBitmapIcons(@NonNull final Context context, @NonNull final List<ShortcutInfoCompat> c) {
        for (final ShortcutInfoCompat shortcutInfoCompat : new ArrayList(c)) {
            if (!convertUriIconToBitmapIcon(context, shortcutInfoCompat)) {
                c.remove(shortcutInfoCompat);
            }
        }
    }
    
    @NonNull
    public static Intent createShortcutResultIntent(@NonNull final Context context, @NonNull final ShortcutInfoCompat shortcutInfoCompat) {
        Intent \u3007080;
        if (Build$VERSION.SDK_INT >= 26) {
            \u3007080 = O\u3007O\u3007oO.\u3007080((ShortcutManager)oo88o8O.\u3007080(context, (Class)ShortcutManager.class), shortcutInfoCompat.toShortcutInfo());
        }
        else {
            \u3007080 = null;
        }
        Intent intent = \u3007080;
        if (\u3007080 == null) {
            intent = new Intent();
        }
        return shortcutInfoCompat.addToIntent(intent);
    }
    
    public static void disableShortcuts(@NonNull final Context context, @NonNull final List<String> list, @Nullable final CharSequence charSequence) {
        if (Build$VERSION.SDK_INT >= 25) {
            O0o\u3007\u3007Oo.\u3007080((ShortcutManager)oo88o8O.\u3007080(context, (Class)ShortcutManager.class), (List)list, charSequence);
        }
        getShortcutInfoSaverInstance(context).removeShortcuts(list);
        final Iterator<ShortcutInfoChangeListener> iterator = getShortcutInfoListeners(context).iterator();
        while (iterator.hasNext()) {
            iterator.next().onShortcutRemoved(list);
        }
    }
    
    public static void enableShortcuts(@NonNull final Context context, @NonNull final List<ShortcutInfoCompat> list) {
        final List<ShortcutInfoCompat> removeShortcutsExcludedFromSurface = removeShortcutsExcludedFromSurface(list, 1);
        if (Build$VERSION.SDK_INT >= 25) {
            final ArrayList list2 = new ArrayList(list.size());
            final Iterator<ShortcutInfoCompat> iterator = removeShortcutsExcludedFromSurface.iterator();
            while (iterator.hasNext()) {
                list2.add(iterator.next().mId);
            }
            \u30078\u30070\u3007o\u3007O.\u3007080((ShortcutManager)oo88o8O.\u3007080(context, (Class)ShortcutManager.class), (List)list2);
        }
        getShortcutInfoSaverInstance(context).addShortcuts(removeShortcutsExcludedFromSurface);
        final Iterator<ShortcutInfoChangeListener> iterator2 = getShortcutInfoListeners(context).iterator();
        while (iterator2.hasNext()) {
            iterator2.next().onShortcutAdded(list);
        }
    }
    
    @NonNull
    public static List<ShortcutInfoCompat> getDynamicShortcuts(@NonNull final Context context) {
        if (Build$VERSION.SDK_INT >= 25) {
            final List \u3007080 = Oo8Oo00oo.\u3007080((ShortcutManager)oo88o8O.\u3007080(context, (Class)ShortcutManager.class));
            final ArrayList list = new ArrayList(\u3007080.size());
            final Iterator iterator = \u3007080.iterator();
            while (iterator.hasNext()) {
                list.add((Object)new ShortcutInfoCompat.Builder(context, (ShortcutInfo)iterator.next()).build());
            }
            return (List<ShortcutInfoCompat>)list;
        }
        try {
            return getShortcutInfoSaverInstance(context).getShortcuts();
        }
        catch (final Exception ex) {
            return new ArrayList<ShortcutInfoCompat>();
        }
    }
    
    private static int getIconDimensionInternal(@NonNull final Context context, final boolean b) {
        final ActivityManager activityManager = (ActivityManager)context.getSystemService("activity");
        int b2;
        if (activityManager == null || activityManager.isLowRamDevice()) {
            b2 = 48;
        }
        else {
            b2 = 96;
        }
        final int max = Math.max(1, b2);
        final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float n;
        if (b) {
            n = displayMetrics.xdpi;
        }
        else {
            n = displayMetrics.ydpi;
        }
        return (int)(max * (n / 160.0f));
    }
    
    public static int getIconMaxHeight(@NonNull final Context context) {
        Preconditions.checkNotNull(context);
        if (Build$VERSION.SDK_INT >= 25) {
            return \u30078.\u3007080((ShortcutManager)oo88o8O.\u3007080(context, (Class)ShortcutManager.class));
        }
        return getIconDimensionInternal(context, false);
    }
    
    public static int getIconMaxWidth(@NonNull final Context context) {
        Preconditions.checkNotNull(context);
        if (Build$VERSION.SDK_INT >= 25) {
            return o\u30078oOO88.\u3007080((ShortcutManager)oo88o8O.\u3007080(context, (Class)ShortcutManager.class));
        }
        return getIconDimensionInternal(context, true);
    }
    
    public static int getMaxShortcutCountPerActivity(@NonNull final Context context) {
        Preconditions.checkNotNull(context);
        if (Build$VERSION.SDK_INT >= 25) {
            return o8oO\u3007.\u3007080((ShortcutManager)oo88o8O.\u3007080(context, (Class)ShortcutManager.class));
        }
        return 5;
    }
    
    @VisibleForTesting
    static List<ShortcutInfoChangeListener> getShortcutInfoChangeListeners() {
        return ShortcutManagerCompat.sShortcutInfoChangeListeners;
    }
    
    private static String getShortcutInfoCompatWithLowestRank(@NonNull final List<ShortcutInfoCompat> list) {
        final Iterator<ShortcutInfoCompat> iterator = list.iterator();
        int rank = -1;
        String id = null;
        while (iterator.hasNext()) {
            final ShortcutInfoCompat shortcutInfoCompat = iterator.next();
            if (shortcutInfoCompat.getRank() > rank) {
                id = shortcutInfoCompat.getId();
                rank = shortcutInfoCompat.getRank();
            }
        }
        return id;
    }
    
    private static List<ShortcutInfoChangeListener> getShortcutInfoListeners(final Context context) {
        if (ShortcutManagerCompat.sShortcutInfoChangeListeners == null) {
            final ArrayList sShortcutInfoChangeListeners = new ArrayList();
            final PackageManager packageManager = context.getPackageManager();
            final Intent intent = new Intent("androidx.core.content.pm.SHORTCUT_LISTENER");
            intent.setPackage(context.getPackageName());
            final Iterator iterator = packageManager.queryIntentActivities(intent, 128).iterator();
            while (iterator.hasNext()) {
                final ActivityInfo activityInfo = ((ResolveInfo)iterator.next()).activityInfo;
                if (activityInfo == null) {
                    continue;
                }
                final Bundle metaData = activityInfo.metaData;
                if (metaData == null) {
                    continue;
                }
                final String string = ((BaseBundle)metaData).getString("androidx.core.content.pm.shortcut_listener_impl");
                if (string == null) {
                    continue;
                }
                try {
                    sShortcutInfoChangeListeners.add(Class.forName(string, false, ShortcutManagerCompat.class.getClassLoader()).getMethod("getInstance", Context.class).invoke(null, context));
                }
                catch (final Exception ex) {}
            }
            if (ShortcutManagerCompat.sShortcutInfoChangeListeners == null) {
                ShortcutManagerCompat.sShortcutInfoChangeListeners = sShortcutInfoChangeListeners;
            }
        }
        return ShortcutManagerCompat.sShortcutInfoChangeListeners;
    }
    
    private static ShortcutInfoCompatSaver<?> getShortcutInfoSaverInstance(final Context context) {
        if (ShortcutManagerCompat.sShortcutInfoCompatSaver == null) {
            if (Build$VERSION.SDK_INT >= 23) {
                try {
                    ShortcutManagerCompat.sShortcutInfoCompatSaver = (ShortcutInfoCompatSaver)Class.forName("androidx.sharetarget.ShortcutInfoCompatSaverImpl", false, ShortcutManagerCompat.class.getClassLoader()).getMethod("getInstance", Context.class).invoke(null, context);
                }
                catch (final Exception ex) {}
            }
            if (ShortcutManagerCompat.sShortcutInfoCompatSaver == null) {
                ShortcutManagerCompat.sShortcutInfoCompatSaver = new ShortcutInfoCompatSaver.NoopImpl();
            }
        }
        return ShortcutManagerCompat.sShortcutInfoCompatSaver;
    }
    
    @NonNull
    public static List<ShortcutInfoCompat> getShortcuts(@NonNull final Context context, final int n) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 30) {
            return ShortcutInfoCompat.fromShortcuts(context, OO8oO0o\u3007.\u3007080((ShortcutManager)oo88o8O.\u3007080(context, (Class)ShortcutManager.class), n));
        }
        if (sdk_INT >= 25) {
            final ShortcutManager shortcutManager = (ShortcutManager)oo88o8O.\u3007080(context, (Class)ShortcutManager.class);
            final ArrayList list = new ArrayList();
            if ((n & 0x1) != 0x0) {
                list.addAll(\u3007\u3007\u30070\u3007\u30070.\u3007080(shortcutManager));
            }
            if ((n & 0x2) != 0x0) {
                list.addAll(Oo8Oo00oo.\u3007080(shortcutManager));
            }
            if ((n & 0x4) != 0x0) {
                list.addAll(o\u30070OOo\u30070.\u3007080(shortcutManager));
            }
            return ShortcutInfoCompat.fromShortcuts(context, list);
        }
        Label_0129: {
            if ((n & 0x2) == 0x0) {
                break Label_0129;
            }
            try {
                return getShortcutInfoSaverInstance(context).getShortcuts();
                return Collections.emptyList();
            }
            catch (final Exception ex) {
                return Collections.emptyList();
            }
        }
    }
    
    public static boolean isRateLimitingActive(@NonNull final Context context) {
        Preconditions.checkNotNull(context);
        if (Build$VERSION.SDK_INT >= 25) {
            return \u300780.\u3007080((ShortcutManager)oo88o8O.\u3007080(context, (Class)ShortcutManager.class));
        }
        return getShortcuts(context, 3).size() == getMaxShortcutCountPerActivity(context);
    }
    
    public static boolean isRequestPinShortcutSupported(@NonNull final Context context) {
        if (Build$VERSION.SDK_INT >= 26) {
            return \u300708O8o\u30070.\u3007080((ShortcutManager)oo88o8O.\u3007080(context, (Class)ShortcutManager.class));
        }
        if (ContextCompat.checkSelfPermission(context, "com.android.launcher.permission.INSTALL_SHORTCUT") != 0) {
            return false;
        }
        final Iterator iterator = context.getPackageManager().queryBroadcastReceivers(new Intent("com.android.launcher.action.INSTALL_SHORTCUT"), 0).iterator();
        while (iterator.hasNext()) {
            final String permission = ((ResolveInfo)iterator.next()).activityInfo.permission;
            if (TextUtils.isEmpty((CharSequence)permission) || "com.android.launcher.permission.INSTALL_SHORTCUT".equals(permission)) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean pushDynamicShortcut(@NonNull final Context context, @NonNull final ShortcutInfoCompat shortcutInfoCompat) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(shortcutInfoCompat);
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT <= 31 && shortcutInfoCompat.isExcludedFromSurfaces(1)) {
            final Iterator<ShortcutInfoChangeListener> iterator = getShortcutInfoListeners(context).iterator();
            while (iterator.hasNext()) {
                iterator.next().onShortcutAdded(Collections.singletonList(shortcutInfoCompat));
            }
            return true;
        }
        final int maxShortcutCountPerActivity = getMaxShortcutCountPerActivity(context);
        if (maxShortcutCountPerActivity == 0) {
            return false;
        }
        if (sdk_INT <= 29) {
            convertUriIconToBitmapIcon(context, shortcutInfoCompat);
        }
        if (sdk_INT >= 30) {
            O000.\u3007080((ShortcutManager)oo88o8O.\u3007080(context, (Class)ShortcutManager.class), shortcutInfoCompat.toShortcutInfo());
        }
        else if (sdk_INT >= 25) {
            final ShortcutManager shortcutManager = (ShortcutManager)oo88o8O.\u3007080(context, (Class)ShortcutManager.class);
            if (\u300780.\u3007080(shortcutManager)) {
                return false;
            }
            final List \u3007080 = Oo8Oo00oo.\u3007080(shortcutManager);
            if (\u3007080.size() >= maxShortcutCountPerActivity) {
                Ooo.\u3007080(shortcutManager, (List)Arrays.asList(Api25Impl.getShortcutInfoWithLowestRank(\u3007080)));
            }
            oO00OOO.\u3007080(shortcutManager, (List)Arrays.asList(shortcutInfoCompat.toShortcutInfo()));
        }
        final ShortcutInfoCompatSaver<?> shortcutInfoSaverInstance = getShortcutInfoSaverInstance(context);
        try {
            final List shortcuts = shortcutInfoSaverInstance.getShortcuts();
            if (shortcuts.size() >= maxShortcutCountPerActivity) {
                shortcutInfoSaverInstance.removeShortcuts(Arrays.asList(getShortcutInfoCompatWithLowestRank(shortcuts)));
            }
            shortcutInfoSaverInstance.addShortcuts(Arrays.asList(shortcutInfoCompat));
            return true;
        }
        catch (final Exception ex) {
            return false;
        }
        finally {
            final Iterator<ShortcutInfoChangeListener> iterator2 = getShortcutInfoListeners(context).iterator();
            while (iterator2.hasNext()) {
                iterator2.next().onShortcutAdded(Collections.singletonList(shortcutInfoCompat));
            }
            reportShortcutUsed(context, shortcutInfoCompat.getId());
        }
    }
    
    public static void removeAllDynamicShortcuts(@NonNull final Context context) {
        if (Build$VERSION.SDK_INT >= 25) {
            O08000.\u3007080((ShortcutManager)oo88o8O.\u3007080(context, (Class)ShortcutManager.class));
        }
        getShortcutInfoSaverInstance(context).removeAllShortcuts();
        final Iterator<ShortcutInfoChangeListener> iterator = getShortcutInfoListeners(context).iterator();
        while (iterator.hasNext()) {
            iterator.next().onAllShortcutsRemoved();
        }
    }
    
    public static void removeDynamicShortcuts(@NonNull final Context context, @NonNull final List<String> list) {
        if (Build$VERSION.SDK_INT >= 25) {
            Ooo.\u3007080((ShortcutManager)oo88o8O.\u3007080(context, (Class)ShortcutManager.class), (List)list);
        }
        getShortcutInfoSaverInstance(context).removeShortcuts(list);
        final Iterator<ShortcutInfoChangeListener> iterator = getShortcutInfoListeners(context).iterator();
        while (iterator.hasNext()) {
            iterator.next().onShortcutRemoved(list);
        }
    }
    
    public static void removeLongLivedShortcuts(@NonNull final Context context, @NonNull final List<String> list) {
        if (Build$VERSION.SDK_INT < 30) {
            removeDynamicShortcuts(context, list);
            return;
        }
        oO.\u3007080((ShortcutManager)oo88o8O.\u3007080(context, (Class)ShortcutManager.class), (List)list);
        getShortcutInfoSaverInstance(context).removeShortcuts(list);
        final Iterator<ShortcutInfoChangeListener> iterator = getShortcutInfoListeners(context).iterator();
        while (iterator.hasNext()) {
            iterator.next().onShortcutRemoved(list);
        }
    }
    
    @NonNull
    private static List<ShortcutInfoCompat> removeShortcutsExcludedFromSurface(@NonNull final List<ShortcutInfoCompat> list, final int n) {
        Objects.requireNonNull(list);
        if (Build$VERSION.SDK_INT > 31) {
            return list;
        }
        final ArrayList list2 = new ArrayList((Collection<? extends E>)list);
        for (final ShortcutInfoCompat shortcutInfoCompat : list) {
            if (shortcutInfoCompat.isExcludedFromSurfaces(n)) {
                list2.remove(shortcutInfoCompat);
            }
        }
        return list2;
    }
    
    public static void reportShortcutUsed(@NonNull final Context context, @NonNull final String o) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(o);
        if (Build$VERSION.SDK_INT >= 25) {
            o\u3007O.\u3007080((ShortcutManager)oo88o8O.\u3007080(context, (Class)ShortcutManager.class), o);
        }
        final Iterator<ShortcutInfoChangeListener> iterator = getShortcutInfoListeners(context).iterator();
        while (iterator.hasNext()) {
            iterator.next().onShortcutUsageReported(Collections.singletonList(o));
        }
    }
    
    public static boolean requestPinShortcut(@NonNull final Context context, @NonNull final ShortcutInfoCompat shortcutInfoCompat, @Nullable final IntentSender intentSender) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT <= 31 && shortcutInfoCompat.isExcludedFromSurfaces(1)) {
            return false;
        }
        if (sdk_INT >= 26) {
            return \u3007\u30070o.\u3007080((ShortcutManager)oo88o8O.\u3007080(context, (Class)ShortcutManager.class), shortcutInfoCompat.toShortcutInfo(), intentSender);
        }
        if (!isRequestPinShortcutSupported(context)) {
            return false;
        }
        final Intent addToIntent = shortcutInfoCompat.addToIntent(new Intent("com.android.launcher.action.INSTALL_SHORTCUT"));
        if (intentSender == null) {
            context.sendBroadcast(addToIntent);
            return true;
        }
        context.sendOrderedBroadcast(addToIntent, (String)null, (BroadcastReceiver)new BroadcastReceiver(intentSender) {
            final IntentSender val$callback;
            
            public void onReceive(final Context context, final Intent intent) {
                try {
                    this.val$callback.sendIntent(context, 0, (Intent)null, (IntentSender$OnFinished)null, (Handler)null);
                }
                catch (final IntentSender$SendIntentException ex) {}
            }
        }, (Handler)null, -1, (String)null, (Bundle)null);
        return true;
    }
    
    public static boolean setDynamicShortcuts(@NonNull final Context context, @NonNull final List<ShortcutInfoCompat> list) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(list);
        final List<ShortcutInfoCompat> removeShortcutsExcludedFromSurface = removeShortcutsExcludedFromSurface(list, 1);
        if (Build$VERSION.SDK_INT >= 25) {
            final ArrayList list2 = new ArrayList(removeShortcutsExcludedFromSurface.size());
            final Iterator iterator = removeShortcutsExcludedFromSurface.iterator();
            while (iterator.hasNext()) {
                list2.add((Object)((ShortcutInfoCompat)iterator.next()).toShortcutInfo());
            }
            if (!ooo\u30078oO.\u3007080((ShortcutManager)oo88o8O.\u3007080(context, (Class)ShortcutManager.class), (List)list2)) {
                return false;
            }
        }
        getShortcutInfoSaverInstance(context).removeAllShortcuts();
        getShortcutInfoSaverInstance(context).addShortcuts(removeShortcutsExcludedFromSurface);
        for (final ShortcutInfoChangeListener shortcutInfoChangeListener : getShortcutInfoListeners(context)) {
            shortcutInfoChangeListener.onAllShortcutsRemoved();
            shortcutInfoChangeListener.onShortcutAdded(list);
        }
        return true;
    }
    
    @VisibleForTesting
    static void setShortcutInfoChangeListeners(final List<ShortcutInfoChangeListener> sShortcutInfoChangeListeners) {
        ShortcutManagerCompat.sShortcutInfoChangeListeners = sShortcutInfoChangeListeners;
    }
    
    @VisibleForTesting
    static void setShortcutInfoCompatSaver(final ShortcutInfoCompatSaver<Void> sShortcutInfoCompatSaver) {
        ShortcutManagerCompat.sShortcutInfoCompatSaver = sShortcutInfoCompatSaver;
    }
    
    public static boolean updateShortcuts(@NonNull final Context context, @NonNull final List<ShortcutInfoCompat> list) {
        final List<ShortcutInfoCompat> removeShortcutsExcludedFromSurface = removeShortcutsExcludedFromSurface(list, 1);
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT <= 29) {
            convertUriIconsToBitmapIcons(context, removeShortcutsExcludedFromSurface);
        }
        if (sdk_INT >= 25) {
            final ArrayList list2 = new ArrayList();
            final Iterator<ShortcutInfoCompat> iterator = removeShortcutsExcludedFromSurface.iterator();
            while (iterator.hasNext()) {
                list2.add(iterator.next().toShortcutInfo());
            }
            if (!\u3007O\u300780o08O.\u3007080((ShortcutManager)oo88o8O.\u3007080(context, (Class)ShortcutManager.class), (List)list2)) {
                return false;
            }
        }
        getShortcutInfoSaverInstance(context).addShortcuts(removeShortcutsExcludedFromSurface);
        final Iterator<ShortcutInfoChangeListener> iterator2 = getShortcutInfoListeners(context).iterator();
        while (iterator2.hasNext()) {
            iterator2.next().onShortcutUpdated(list);
        }
        return true;
    }
    
    @RequiresApi(25)
    private static class Api25Impl
    {
        static String getShortcutInfoWithLowestRank(@NonNull final List<ShortcutInfo> list) {
            final Iterator<ShortcutInfo> iterator = list.iterator();
            int \u3007080 = -1;
            String \u300781 = null;
            while (iterator.hasNext()) {
                final ShortcutInfo shortcutInfo = iterator.next();
                if (\u3007O888o0o.\u3007080(shortcutInfo) > \u3007080) {
                    \u300781 = OoO8.\u3007080(shortcutInfo);
                    \u3007080 = \u3007O888o0o.\u3007080(shortcutInfo);
                }
            }
            return \u300781;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public @interface ShortcutMatchFlags {
    }
}

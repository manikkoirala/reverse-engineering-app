// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content.pm;

import androidx.annotation.NonNull;
import java.util.List;
import androidx.annotation.AnyThread;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public abstract class ShortcutInfoChangeListener
{
    @AnyThread
    public void onAllShortcutsRemoved() {
    }
    
    @AnyThread
    public void onShortcutAdded(@NonNull final List<ShortcutInfoCompat> list) {
    }
    
    @AnyThread
    public void onShortcutRemoved(@NonNull final List<String> list) {
    }
    
    @AnyThread
    public void onShortcutUpdated(@NonNull final List<ShortcutInfoCompat> list) {
    }
    
    @AnyThread
    public void onShortcutUsageReported(@NonNull final List<String> list) {
    }
}

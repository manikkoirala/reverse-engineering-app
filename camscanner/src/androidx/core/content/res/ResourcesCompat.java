// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content.res;

import android.annotation.SuppressLint;
import java.lang.reflect.Method;
import android.os.Looper;
import androidx.core.util.ObjectsCompat;
import android.content.res.Configuration;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import androidx.core.graphics.TypefaceCompat;
import android.content.res.XmlResourceParser;
import org.xmlpull.v1.XmlPullParser;
import androidx.core.util.Preconditions;
import androidx.annotation.RestrictTo;
import androidx.annotation.DimenRes;
import android.graphics.drawable.Drawable;
import androidx.annotation.DrawableRes;
import androidx.annotation.ColorInt;
import android.os.Build$VERSION;
import android.content.res.Resources;
import android.content.res.Resources$NotFoundException;
import android.os.Handler;
import android.graphics.Typeface;
import androidx.annotation.FontRes;
import android.content.Context;
import java.util.Iterator;
import androidx.annotation.Nullable;
import android.content.res.Resources$Theme;
import android.content.res.ColorStateList;
import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import android.util.TypedValue;
import androidx.annotation.GuardedBy;
import android.util.SparseArray;
import java.util.WeakHashMap;
import androidx.annotation.AnyRes;

public final class ResourcesCompat
{
    @AnyRes
    public static final int ID_NULL = 0;
    private static final String TAG = "ResourcesCompat";
    private static final Object sColorStateCacheLock;
    @GuardedBy("sColorStateCacheLock")
    private static final WeakHashMap<ColorStateListCacheKey, SparseArray<ColorStateListCacheEntry>> sColorStateCaches;
    private static final ThreadLocal<TypedValue> sTempTypedValue;
    
    static {
        sTempTypedValue = new ThreadLocal<TypedValue>();
        sColorStateCaches = new WeakHashMap<ColorStateListCacheKey, SparseArray<ColorStateListCacheEntry>>(0);
        sColorStateCacheLock = new Object();
    }
    
    private ResourcesCompat() {
    }
    
    private static void addColorStateListToCache(@NonNull final ColorStateListCacheKey colorStateListCacheKey, @ColorRes final int n, @NonNull final ColorStateList list, @Nullable final Resources$Theme resources$Theme) {
        synchronized (ResourcesCompat.sColorStateCacheLock) {
            final WeakHashMap<ColorStateListCacheKey, SparseArray<ColorStateListCacheEntry>> sColorStateCaches = ResourcesCompat.sColorStateCaches;
            SparseArray value;
            if ((value = sColorStateCaches.get(colorStateListCacheKey)) == null) {
                value = new SparseArray();
                sColorStateCaches.put(colorStateListCacheKey, (SparseArray<ColorStateListCacheEntry>)value);
            }
            value.append(n, (Object)new ColorStateListCacheEntry(list, colorStateListCacheKey.mResources.getConfiguration(), resources$Theme));
        }
    }
    
    public static void clearCachesForTheme(@NonNull final Resources$Theme resources$Theme) {
        synchronized (ResourcesCompat.sColorStateCacheLock) {
            final Iterator<ColorStateListCacheKey> iterator = ResourcesCompat.sColorStateCaches.keySet().iterator();
            while (iterator.hasNext()) {
                final ColorStateListCacheKey colorStateListCacheKey = iterator.next();
                if (colorStateListCacheKey != null && resources$Theme.equals((Object)colorStateListCacheKey.mTheme)) {
                    iterator.remove();
                }
            }
        }
    }
    
    @Nullable
    private static ColorStateList getCachedColorStateList(@NonNull final ColorStateListCacheKey key, @ColorRes final int n) {
        synchronized (ResourcesCompat.sColorStateCacheLock) {
            final SparseArray sparseArray = ResourcesCompat.sColorStateCaches.get(key);
            if (sparseArray != null && sparseArray.size() > 0) {
                final ColorStateListCacheEntry colorStateListCacheEntry = (ColorStateListCacheEntry)sparseArray.get(n);
                if (colorStateListCacheEntry != null) {
                    if (colorStateListCacheEntry.mConfiguration.equals(key.mResources.getConfiguration())) {
                        final Resources$Theme mTheme = key.mTheme;
                        if ((mTheme == null && colorStateListCacheEntry.mThemeHash == 0) || (mTheme != null && colorStateListCacheEntry.mThemeHash == mTheme.hashCode())) {
                            return colorStateListCacheEntry.mValue;
                        }
                    }
                    sparseArray.remove(n);
                }
            }
            return null;
        }
    }
    
    @Nullable
    public static Typeface getCachedFont(@NonNull final Context context, @FontRes final int n) throws Resources$NotFoundException {
        if (context.isRestricted()) {
            return null;
        }
        return loadFont(context, n, new TypedValue(), 0, null, null, false, true);
    }
    
    @ColorInt
    public static int getColor(@NonNull final Resources resources, @ColorRes final int n, @Nullable final Resources$Theme resources$Theme) throws Resources$NotFoundException {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getColor(resources, n, resources$Theme);
        }
        return resources.getColor(n);
    }
    
    @Nullable
    public static ColorStateList getColorStateList(@NonNull final Resources resources, @ColorRes final int n, @Nullable final Resources$Theme resources$Theme) throws Resources$NotFoundException {
        final ColorStateListCacheKey colorStateListCacheKey = new ColorStateListCacheKey(resources, resources$Theme);
        final ColorStateList cachedColorStateList = getCachedColorStateList(colorStateListCacheKey, n);
        if (cachedColorStateList != null) {
            return cachedColorStateList;
        }
        final ColorStateList inflateColorStateList = inflateColorStateList(resources, n, resources$Theme);
        if (inflateColorStateList != null) {
            addColorStateListToCache(colorStateListCacheKey, n, inflateColorStateList, resources$Theme);
            return inflateColorStateList;
        }
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getColorStateList(resources, n, resources$Theme);
        }
        return resources.getColorStateList(n);
    }
    
    @Nullable
    public static Drawable getDrawable(@NonNull final Resources resources, @DrawableRes final int n, @Nullable final Resources$Theme resources$Theme) throws Resources$NotFoundException {
        return Api21Impl.getDrawable(resources, n, resources$Theme);
    }
    
    @Nullable
    public static Drawable getDrawableForDensity(@NonNull final Resources resources, @DrawableRes final int n, final int n2, @Nullable final Resources$Theme resources$Theme) throws Resources$NotFoundException {
        return Api21Impl.getDrawableForDensity(resources, n, n2, resources$Theme);
    }
    
    public static float getFloat(@NonNull final Resources resources, @DimenRes final int i) {
        if (Build$VERSION.SDK_INT >= 29) {
            return Api29Impl.getFloat(resources, i);
        }
        final TypedValue typedValue = getTypedValue();
        resources.getValue(i, typedValue, true);
        if (typedValue.type == 4) {
            return typedValue.getFloat();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Resource ID #0x");
        sb.append(Integer.toHexString(i));
        sb.append(" type #0x");
        sb.append(Integer.toHexString(typedValue.type));
        sb.append(" is not valid");
        throw new Resources$NotFoundException(sb.toString());
    }
    
    @Nullable
    public static Typeface getFont(@NonNull final Context context, @FontRes final int n) throws Resources$NotFoundException {
        if (context.isRestricted()) {
            return null;
        }
        return loadFont(context, n, new TypedValue(), 0, null, null, false, false);
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static Typeface getFont(@NonNull final Context context, @FontRes final int n, @NonNull final TypedValue typedValue, final int n2, @Nullable final FontCallback fontCallback) throws Resources$NotFoundException {
        if (context.isRestricted()) {
            return null;
        }
        return loadFont(context, n, typedValue, n2, fontCallback, null, true, false);
    }
    
    public static void getFont(@NonNull final Context context, @FontRes final int n, @NonNull final FontCallback fontCallback, @Nullable final Handler handler) throws Resources$NotFoundException {
        Preconditions.checkNotNull(fontCallback);
        if (context.isRestricted()) {
            fontCallback.callbackFailAsync(-4, handler);
            return;
        }
        loadFont(context, n, new TypedValue(), 0, fontCallback, handler, false, false);
    }
    
    @NonNull
    private static TypedValue getTypedValue() {
        final ThreadLocal<TypedValue> sTempTypedValue = ResourcesCompat.sTempTypedValue;
        TypedValue value;
        if ((value = sTempTypedValue.get()) == null) {
            value = new TypedValue();
            sTempTypedValue.set(value);
        }
        return value;
    }
    
    @Nullable
    private static ColorStateList inflateColorStateList(final Resources resources, final int n, @Nullable final Resources$Theme resources$Theme) {
        if (isColorInt(resources, n)) {
            return null;
        }
        final XmlResourceParser xml = resources.getXml(n);
        try {
            return ColorStateListInflaterCompat.createFromXml(resources, (XmlPullParser)xml, resources$Theme);
        }
        catch (final Exception ex) {
            return null;
        }
    }
    
    private static boolean isColorInt(@NonNull final Resources resources, @ColorRes int type) {
        final TypedValue typedValue = getTypedValue();
        boolean b = true;
        resources.getValue(type, typedValue, true);
        type = typedValue.type;
        if (type < 28 || type > 31) {
            b = false;
        }
        return b;
    }
    
    private static Typeface loadFont(@NonNull final Context context, final int i, @NonNull final TypedValue typedValue, final int n, @Nullable final FontCallback fontCallback, @Nullable final Handler handler, final boolean b, final boolean b2) {
        final Resources resources = context.getResources();
        resources.getValue(i, typedValue, true);
        final Typeface loadFont = loadFont(context, resources, typedValue, i, n, fontCallback, handler, b, b2);
        if (loadFont == null && fontCallback == null && !b2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Font resource ID #0x");
            sb.append(Integer.toHexString(i));
            sb.append(" could not be retrieved.");
            throw new Resources$NotFoundException(sb.toString());
        }
        return loadFont;
    }
    
    private static Typeface loadFont(@NonNull final Context context, final Resources resources, @NonNull final TypedValue obj, final int i, final int n, @Nullable final FontCallback fontCallback, @Nullable final Handler handler, final boolean b, final boolean b2) {
        final CharSequence string = obj.string;
        if (string == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Resource \"");
            sb.append(resources.getResourceName(i));
            sb.append("\" (");
            sb.append(Integer.toHexString(i));
            sb.append(") is not a Font: ");
            sb.append(obj);
            throw new Resources$NotFoundException(sb.toString());
        }
        final String string2 = string.toString();
        if (!string2.startsWith("res/")) {
            if (fontCallback != null) {
                fontCallback.callbackFailAsync(-3, handler);
            }
            return null;
        }
        final Typeface fromCache = TypefaceCompat.findFromCache(resources, i, string2, obj.assetCookie, n);
        if (fromCache != null) {
            if (fontCallback != null) {
                fontCallback.callbackSuccessAsync(fromCache, handler);
            }
            return fromCache;
        }
        if (b2) {
            return null;
        }
        try {
            if (!string2.toLowerCase().endsWith(".xml")) {
                final Typeface fromResourcesFontFile = TypefaceCompat.createFromResourcesFontFile(context, resources, i, string2, obj.assetCookie, n);
                if (fontCallback != null) {
                    if (fromResourcesFontFile != null) {
                        fontCallback.callbackSuccessAsync(fromResourcesFontFile, handler);
                    }
                    else {
                        fontCallback.callbackFailAsync(-3, handler);
                    }
                }
                return fromResourcesFontFile;
            }
            final FontResourcesParserCompat.FamilyResourceEntry parse = FontResourcesParserCompat.parse((XmlPullParser)resources.getXml(i), resources);
            if (parse == null) {
                if (fontCallback != null) {
                    fontCallback.callbackFailAsync(-3, handler);
                }
                return null;
            }
            return TypefaceCompat.createFromResourcesFamilyXml(context, parse, resources, i, string2, obj.assetCookie, n, fontCallback, handler, b);
        }
        catch (final IOException ex) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Failed to read xml resource ");
            sb2.append(string2);
        }
        catch (final XmlPullParserException ex2) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Failed to parse xml resource ");
            sb3.append(string2);
        }
        if (fontCallback != null) {
            fontCallback.callbackFailAsync(-3, handler);
        }
        return null;
    }
    
    @RequiresApi(15)
    static class Api15Impl
    {
        private Api15Impl() {
        }
        
        @DoNotInline
        static Drawable getDrawableForDensity(final Resources resources, final int n, final int n2) {
            return resources.getDrawableForDensity(n, n2);
        }
    }
    
    @RequiresApi(21)
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        @DoNotInline
        static Drawable getDrawable(final Resources resources, final int n, final Resources$Theme resources$Theme) {
            return resources.getDrawable(n, resources$Theme);
        }
        
        @DoNotInline
        static Drawable getDrawableForDensity(final Resources resources, final int n, final int n2, final Resources$Theme resources$Theme) {
            return resources.getDrawableForDensity(n, n2, resources$Theme);
        }
    }
    
    @RequiresApi(23)
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        @DoNotInline
        static int getColor(final Resources resources, final int n, final Resources$Theme resources$Theme) {
            return \u3007080.\u3007080(resources, n, resources$Theme);
        }
        
        @DoNotInline
        @NonNull
        static ColorStateList getColorStateList(@NonNull final Resources resources, @ColorRes final int n, @Nullable final Resources$Theme resources$Theme) {
            return \u3007o00\u3007\u3007Oo.\u3007080(resources, n, resources$Theme);
        }
    }
    
    @RequiresApi(29)
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        @DoNotInline
        static float getFloat(@NonNull final Resources resources, @DimenRes final int n) {
            return \u3007o\u3007.\u3007080(resources, n);
        }
    }
    
    private static class ColorStateListCacheEntry
    {
        final Configuration mConfiguration;
        final int mThemeHash;
        final ColorStateList mValue;
        
        ColorStateListCacheEntry(@NonNull final ColorStateList mValue, @NonNull final Configuration mConfiguration, @Nullable final Resources$Theme resources$Theme) {
            this.mValue = mValue;
            this.mConfiguration = mConfiguration;
            int hashCode;
            if (resources$Theme == null) {
                hashCode = 0;
            }
            else {
                hashCode = resources$Theme.hashCode();
            }
            this.mThemeHash = hashCode;
        }
    }
    
    private static final class ColorStateListCacheKey
    {
        final Resources mResources;
        final Resources$Theme mTheme;
        
        ColorStateListCacheKey(@NonNull final Resources mResources, @Nullable final Resources$Theme mTheme) {
            this.mResources = mResources;
            this.mTheme = mTheme;
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (o != null && ColorStateListCacheKey.class == o.getClass()) {
                final ColorStateListCacheKey colorStateListCacheKey = (ColorStateListCacheKey)o;
                if (!this.mResources.equals(colorStateListCacheKey.mResources) || !ObjectsCompat.equals(this.mTheme, colorStateListCacheKey.mTheme)) {
                    b = false;
                }
                return b;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            return ObjectsCompat.hash(this.mResources, this.mTheme);
        }
    }
    
    public abstract static class FontCallback
    {
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY })
        public static Handler getHandler(@Nullable final Handler handler) {
            Handler handler2 = handler;
            if (handler == null) {
                handler2 = new Handler(Looper.getMainLooper());
            }
            return handler2;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public final void callbackFailAsync(final int n, @Nullable final Handler handler) {
            getHandler(handler).post((Runnable)new Oo08(this, n));
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public final void callbackSuccessAsync(@NonNull final Typeface typeface, @Nullable final Handler handler) {
            getHandler(handler).post((Runnable)new O8(this, typeface));
        }
        
        public abstract void onFontRetrievalFailed(final int p0);
        
        public abstract void onFontRetrieved(@NonNull final Typeface p0);
    }
    
    public static final class ThemeCompat
    {
        private ThemeCompat() {
        }
        
        public static void rebase(@NonNull final Resources$Theme resources$Theme) {
            final int sdk_INT = Build$VERSION.SDK_INT;
            if (sdk_INT >= 29) {
                Api29Impl.rebase(resources$Theme);
            }
            else if (sdk_INT >= 23) {
                Api23Impl.rebase(resources$Theme);
            }
        }
        
        @RequiresApi(23)
        static class Api23Impl
        {
            private static Method sRebaseMethod;
            private static boolean sRebaseMethodFetched;
            private static final Object sRebaseMethodLock;
            
            static {
                sRebaseMethodLock = new Object();
            }
            
            private Api23Impl() {
            }
            
            @SuppressLint({ "BanUncheckedReflection" })
            static void rebase(@NonNull final Resources$Theme p0) {
                // 
                // This method could not be decompiled.
                // 
                // Original Bytecode:
                // 
                //     3: astore_2       
                //     4: aload_2        
                //     5: monitorenter   
                //     6: getstatic       androidx/core/content/res/ResourcesCompat$ThemeCompat$Api23Impl.sRebaseMethodFetched:Z
                //     9: istore_1       
                //    10: iload_1        
                //    11: ifne            39
                //    14: ldc             Landroid/content/res/Resources$Theme;.class
                //    16: ldc             "rebase"
                //    18: iconst_0       
                //    19: anewarray       Ljava/lang/Class;
                //    22: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
                //    25: astore_3       
                //    26: aload_3        
                //    27: putstatic       androidx/core/content/res/ResourcesCompat$ThemeCompat$Api23Impl.sRebaseMethod:Ljava/lang/reflect/Method;
                //    30: aload_3        
                //    31: iconst_1       
                //    32: invokevirtual   java/lang/reflect/AccessibleObject.setAccessible:(Z)V
                //    35: iconst_1       
                //    36: putstatic       androidx/core/content/res/ResourcesCompat$ThemeCompat$Api23Impl.sRebaseMethodFetched:Z
                //    39: getstatic       androidx/core/content/res/ResourcesCompat$ThemeCompat$Api23Impl.sRebaseMethod:Ljava/lang/reflect/Method;
                //    42: astore_3       
                //    43: aload_3        
                //    44: ifnull          65
                //    47: aload_3        
                //    48: aload_0        
                //    49: iconst_0       
                //    50: anewarray       Ljava/lang/Object;
                //    53: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
                //    56: pop            
                //    57: goto            65
                //    60: astore_0       
                //    61: aconst_null    
                //    62: putstatic       androidx/core/content/res/ResourcesCompat$ThemeCompat$Api23Impl.sRebaseMethod:Ljava/lang/reflect/Method;
                //    65: aload_2        
                //    66: monitorexit    
                //    67: return         
                //    68: astore_0       
                //    69: aload_2        
                //    70: monitorexit    
                //    71: aload_0        
                //    72: athrow         
                //    73: astore_3       
                //    74: goto            35
                //    Exceptions:
                //  Try           Handler
                //  Start  End    Start  End    Type                                         
                //  -----  -----  -----  -----  ---------------------------------------------
                //  6      10     68     73     Any
                //  14     35     73     77     Ljava/lang/NoSuchMethodException;
                //  14     35     68     73     Any
                //  35     39     68     73     Any
                //  39     43     68     73     Any
                //  47     57     60     65     Ljava/lang/IllegalAccessException;
                //  47     57     60     65     Ljava/lang/reflect/InvocationTargetException;
                //  47     57     68     73     Any
                //  61     65     68     73     Any
                //  65     67     68     73     Any
                //  69     71     68     73     Any
                // 
                // The error that occurred was:
                // 
                // java.lang.IllegalStateException: Expression is linked from several locations: Label_0035:
                //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
                //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
                //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
                //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
                //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
                //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
                //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
                // 
                throw new IllegalStateException("An error occurred while decompiling this method.");
            }
        }
        
        @RequiresApi(29)
        static class Api29Impl
        {
            private Api29Impl() {
            }
            
            @DoNotInline
            static void rebase(@NonNull final Resources$Theme resources$Theme) {
                o\u30070.\u3007080(resources$Theme);
            }
        }
    }
}

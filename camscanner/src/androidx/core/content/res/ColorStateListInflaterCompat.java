// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content.res;

import androidx.core.math.MathUtils;
import android.graphics.Color;
import androidx.annotation.FloatRange;
import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import android.content.res.TypedArray;
import android.util.StateSet;
import android.os.Build$VERSION;
import androidx.core.R;
import androidx.annotation.XmlRes;
import java.io.IOException;
import android.util.AttributeSet;
import org.xmlpull.v1.XmlPullParserException;
import android.util.Xml;
import android.content.res.ColorStateList;
import androidx.annotation.Nullable;
import android.content.res.Resources$Theme;
import org.xmlpull.v1.XmlPullParser;
import androidx.annotation.NonNull;
import android.content.res.Resources;
import android.util.TypedValue;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public final class ColorStateListInflaterCompat
{
    private static final ThreadLocal<TypedValue> sTempTypedValue;
    
    static {
        sTempTypedValue = new ThreadLocal<TypedValue>();
    }
    
    private ColorStateListInflaterCompat() {
    }
    
    @NonNull
    public static ColorStateList createFromXml(@NonNull final Resources resources, @NonNull final XmlPullParser xmlPullParser, @Nullable final Resources$Theme resources$Theme) throws XmlPullParserException, IOException {
        final AttributeSet attributeSet = Xml.asAttributeSet(xmlPullParser);
        int next;
        do {
            next = xmlPullParser.next();
        } while (next != 2 && next != 1);
        if (next == 2) {
            return createFromXmlInner(resources, xmlPullParser, attributeSet, resources$Theme);
        }
        throw new XmlPullParserException("No start tag found");
    }
    
    @NonNull
    public static ColorStateList createFromXmlInner(@NonNull final Resources resources, @NonNull final XmlPullParser xmlPullParser, @NonNull final AttributeSet set, @Nullable final Resources$Theme resources$Theme) throws XmlPullParserException, IOException {
        final String name = xmlPullParser.getName();
        if (name.equals("selector")) {
            return inflate(resources, xmlPullParser, set, resources$Theme);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(xmlPullParser.getPositionDescription());
        sb.append(": invalid color state list tag ");
        sb.append(name);
        throw new XmlPullParserException(sb.toString());
    }
    
    @NonNull
    private static TypedValue getTypedValue() {
        final ThreadLocal<TypedValue> sTempTypedValue = ColorStateListInflaterCompat.sTempTypedValue;
        TypedValue value;
        if ((value = sTempTypedValue.get()) == null) {
            value = new TypedValue();
            sTempTypedValue.set(value);
        }
        return value;
    }
    
    @Nullable
    public static ColorStateList inflate(@NonNull final Resources resources, @XmlRes final int n, @Nullable final Resources$Theme resources$Theme) {
        try {
            return createFromXml(resources, (XmlPullParser)resources.getXml(n), resources$Theme);
        }
        catch (final Exception ex) {
            return null;
        }
    }
    
    private static ColorStateList inflate(@NonNull final Resources resources, @NonNull final XmlPullParser xmlPullParser, @NonNull final AttributeSet set, @Nullable final Resources$Theme resources$Theme) throws XmlPullParserException, IOException {
        final int n = xmlPullParser.getDepth() + 1;
        int[][] array = new int[20][];
        int[] array2 = new int[20];
        int n2 = 0;
        while (true) {
            final int next = xmlPullParser.next();
            if (next == 1) {
                break;
            }
            final int depth = xmlPullParser.getDepth();
            if (depth < n && next == 3) {
                break;
            }
            int[] append = array2;
            int[][] array3 = array;
            int n3 = n2;
            if (next == 2) {
                append = array2;
                array3 = array;
                n3 = n2;
                if (depth <= n) {
                    if (!xmlPullParser.getName().equals("item")) {
                        append = array2;
                        array3 = array;
                        n3 = n2;
                    }
                    else {
                        final TypedArray obtainAttributes = obtainAttributes(resources, resources$Theme, set, R.styleable.ColorStateListItem);
                        final int colorStateListItem_android_color = R.styleable.ColorStateListItem_android_color;
                        final int resourceId = obtainAttributes.getResourceId(colorStateListItem_android_color, -1);
                        int n4;
                        if (resourceId != -1 && !isColorInt(resources, resourceId)) {
                            try {
                                n4 = createFromXml(resources, (XmlPullParser)resources.getXml(resourceId), resources$Theme).getDefaultColor();
                            }
                            catch (final Exception ex) {
                                n4 = obtainAttributes.getColor(R.styleable.ColorStateListItem_android_color, -65281);
                            }
                        }
                        else {
                            n4 = obtainAttributes.getColor(colorStateListItem_android_color, -65281);
                        }
                        final int colorStateListItem_android_alpha = R.styleable.ColorStateListItem_android_alpha;
                        final boolean hasValue = obtainAttributes.hasValue(colorStateListItem_android_alpha);
                        float n5 = 1.0f;
                        if (hasValue) {
                            n5 = obtainAttributes.getFloat(colorStateListItem_android_alpha, 1.0f);
                        }
                        else {
                            final int colorStateListItem_alpha = R.styleable.ColorStateListItem_alpha;
                            if (obtainAttributes.hasValue(colorStateListItem_alpha)) {
                                n5 = obtainAttributes.getFloat(colorStateListItem_alpha, 1.0f);
                            }
                        }
                        float n6 = 0.0f;
                        Label_0324: {
                            if (Build$VERSION.SDK_INT >= 31) {
                                final int colorStateListItem_android_lStar = R.styleable.ColorStateListItem_android_lStar;
                                if (obtainAttributes.hasValue(colorStateListItem_android_lStar)) {
                                    n6 = obtainAttributes.getFloat(colorStateListItem_android_lStar, -1.0f);
                                    break Label_0324;
                                }
                            }
                            n6 = obtainAttributes.getFloat(R.styleable.ColorStateListItem_lStar, -1.0f);
                        }
                        obtainAttributes.recycle();
                        final int attributeCount = set.getAttributeCount();
                        final int[] array4 = new int[attributeCount];
                        int i = 0;
                        int n7 = 0;
                        while (i < attributeCount) {
                            final int attributeNameResource = set.getAttributeNameResource(i);
                            int n8 = n7;
                            if (attributeNameResource != 16843173) {
                                n8 = n7;
                                if (attributeNameResource != 16843551) {
                                    n8 = n7;
                                    if (attributeNameResource != R.attr.alpha) {
                                        n8 = n7;
                                        if (attributeNameResource != R.attr.lStar) {
                                            int n9;
                                            if (set.getAttributeBooleanValue(i, false)) {
                                                n9 = attributeNameResource;
                                            }
                                            else {
                                                n9 = -attributeNameResource;
                                            }
                                            array4[n7] = n9;
                                            n8 = n7 + 1;
                                        }
                                    }
                                }
                            }
                            ++i;
                            n7 = n8;
                        }
                        final int[] trimStateSet = StateSet.trimStateSet(array4, n7);
                        append = GrowingArrayUtils.append(array2, n2, modulateColorAlpha(n4, n5, n6));
                        array3 = GrowingArrayUtils.append(array, n2, trimStateSet);
                        n3 = n2 + 1;
                    }
                }
            }
            array2 = append;
            array = array3;
            n2 = n3;
        }
        final int[] array5 = new int[n2];
        final int[][] array6 = new int[n2][];
        System.arraycopy(array2, 0, array5, 0, n2);
        System.arraycopy(array, 0, array6, 0, n2);
        return new ColorStateList(array6, array5);
    }
    
    private static boolean isColorInt(@NonNull final Resources resources, @ColorRes int type) {
        final TypedValue typedValue = getTypedValue();
        boolean b = true;
        resources.getValue(type, typedValue, true);
        type = typedValue.type;
        if (type < 28 || type > 31) {
            b = false;
        }
        return b;
    }
    
    @ColorInt
    private static int modulateColorAlpha(@ColorInt final int n, @FloatRange(from = 0.0, to = 1.0) final float n2, @FloatRange(from = 0.0, to = 100.0) final float n3) {
        final boolean b = n3 >= 0.0f && n3 <= 100.0f;
        if (n2 == 1.0f && !b) {
            return n;
        }
        final int clamp = MathUtils.clamp((int)(Color.alpha(n) * n2 + 0.5f), 0, 255);
        int color = n;
        if (b) {
            final CamColor fromColor = CamColor.fromColor(n);
            color = CamColor.toColor(fromColor.getHue(), fromColor.getChroma(), n3);
        }
        return (color & 0xFFFFFF) | clamp << 24;
    }
    
    private static TypedArray obtainAttributes(final Resources resources, final Resources$Theme resources$Theme, final AttributeSet set, final int[] array) {
        TypedArray typedArray;
        if (resources$Theme == null) {
            typedArray = resources.obtainAttributes(set, array);
        }
        else {
            typedArray = resources$Theme.obtainStyledAttributes(set, array, 0, 0);
        }
        return typedArray;
    }
}

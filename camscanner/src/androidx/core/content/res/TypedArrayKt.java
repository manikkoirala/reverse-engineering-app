// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content.res;

import kotlin.jvm.functions.Function1;
import androidx.annotation.AnyRes;
import androidx.annotation.RequiresApi;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import androidx.annotation.Dimension;
import android.content.res.ColorStateList;
import androidx.annotation.ColorInt;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import androidx.annotation.StyleableRes;
import android.content.res.TypedArray;
import kotlin.Metadata;

@Metadata
public final class TypedArrayKt
{
    private static final void checkAttribute(final TypedArray typedArray, @StyleableRes final int n) {
        if (typedArray.hasValue(n)) {
            return;
        }
        throw new IllegalArgumentException("Attribute not defined in set.");
    }
    
    public static final boolean getBooleanOrThrow(@NotNull final TypedArray typedArray, @StyleableRes final int n) {
        Intrinsics.checkNotNullParameter((Object)typedArray, "<this>");
        checkAttribute(typedArray, n);
        return typedArray.getBoolean(n, false);
    }
    
    @ColorInt
    public static final int getColorOrThrow(@NotNull final TypedArray typedArray, @StyleableRes final int n) {
        Intrinsics.checkNotNullParameter((Object)typedArray, "<this>");
        checkAttribute(typedArray, n);
        return typedArray.getColor(n, 0);
    }
    
    @NotNull
    public static final ColorStateList getColorStateListOrThrow(@NotNull final TypedArray typedArray, @StyleableRes final int n) {
        Intrinsics.checkNotNullParameter((Object)typedArray, "<this>");
        checkAttribute(typedArray, n);
        final ColorStateList colorStateList = typedArray.getColorStateList(n);
        if (colorStateList != null) {
            return colorStateList;
        }
        throw new IllegalStateException("Attribute value was not a color or color state list.".toString());
    }
    
    public static final float getDimensionOrThrow(@NotNull final TypedArray typedArray, @StyleableRes final int n) {
        Intrinsics.checkNotNullParameter((Object)typedArray, "<this>");
        checkAttribute(typedArray, n);
        return typedArray.getDimension(n, 0.0f);
    }
    
    @Dimension
    public static final int getDimensionPixelOffsetOrThrow(@NotNull final TypedArray typedArray, @StyleableRes final int n) {
        Intrinsics.checkNotNullParameter((Object)typedArray, "<this>");
        checkAttribute(typedArray, n);
        return typedArray.getDimensionPixelOffset(n, 0);
    }
    
    @Dimension
    public static final int getDimensionPixelSizeOrThrow(@NotNull final TypedArray typedArray, @StyleableRes final int n) {
        Intrinsics.checkNotNullParameter((Object)typedArray, "<this>");
        checkAttribute(typedArray, n);
        return typedArray.getDimensionPixelSize(n, 0);
    }
    
    @NotNull
    public static final Drawable getDrawableOrThrow(@NotNull final TypedArray typedArray, @StyleableRes final int n) {
        Intrinsics.checkNotNullParameter((Object)typedArray, "<this>");
        checkAttribute(typedArray, n);
        final Drawable drawable = typedArray.getDrawable(n);
        Intrinsics.Oo08((Object)drawable);
        return drawable;
    }
    
    public static final float getFloatOrThrow(@NotNull final TypedArray typedArray, @StyleableRes final int n) {
        Intrinsics.checkNotNullParameter((Object)typedArray, "<this>");
        checkAttribute(typedArray, n);
        return typedArray.getFloat(n, 0.0f);
    }
    
    @RequiresApi(26)
    @NotNull
    public static final Typeface getFontOrThrow(@NotNull final TypedArray typedArray, @StyleableRes final int n) {
        Intrinsics.checkNotNullParameter((Object)typedArray, "<this>");
        checkAttribute(typedArray, n);
        return TypedArrayApi26ImplKt.getFont(typedArray, n);
    }
    
    public static final int getIntOrThrow(@NotNull final TypedArray typedArray, @StyleableRes final int n) {
        Intrinsics.checkNotNullParameter((Object)typedArray, "<this>");
        checkAttribute(typedArray, n);
        return typedArray.getInt(n, 0);
    }
    
    public static final int getIntegerOrThrow(@NotNull final TypedArray typedArray, @StyleableRes final int n) {
        Intrinsics.checkNotNullParameter((Object)typedArray, "<this>");
        checkAttribute(typedArray, n);
        return typedArray.getInteger(n, 0);
    }
    
    @AnyRes
    public static final int getResourceIdOrThrow(@NotNull final TypedArray typedArray, @StyleableRes final int n) {
        Intrinsics.checkNotNullParameter((Object)typedArray, "<this>");
        checkAttribute(typedArray, n);
        return typedArray.getResourceId(n, 0);
    }
    
    @NotNull
    public static final String getStringOrThrow(@NotNull final TypedArray typedArray, @StyleableRes final int n) {
        Intrinsics.checkNotNullParameter((Object)typedArray, "<this>");
        checkAttribute(typedArray, n);
        final String string = typedArray.getString(n);
        if (string != null) {
            return string;
        }
        throw new IllegalStateException("Attribute value could not be coerced to String.".toString());
    }
    
    @NotNull
    public static final CharSequence[] getTextArrayOrThrow(@NotNull final TypedArray typedArray, @StyleableRes final int n) {
        Intrinsics.checkNotNullParameter((Object)typedArray, "<this>");
        checkAttribute(typedArray, n);
        final CharSequence[] textArray = typedArray.getTextArray(n);
        Intrinsics.checkNotNullExpressionValue((Object)textArray, "getTextArray(index)");
        return textArray;
    }
    
    @NotNull
    public static final CharSequence getTextOrThrow(@NotNull final TypedArray typedArray, @StyleableRes final int n) {
        Intrinsics.checkNotNullParameter((Object)typedArray, "<this>");
        checkAttribute(typedArray, n);
        final CharSequence text = typedArray.getText(n);
        if (text != null) {
            return text;
        }
        throw new IllegalStateException("Attribute value could not be coerced to CharSequence.".toString());
    }
    
    public static final <R> R use(@NotNull final TypedArray typedArray, @NotNull final Function1<? super TypedArray, ? extends R> function1) {
        Intrinsics.checkNotNullParameter((Object)typedArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final Object invoke = function1.invoke((Object)typedArray);
        typedArray.recycle();
        return (R)invoke;
    }
}

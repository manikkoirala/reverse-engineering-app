// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content.res;

import androidx.annotation.DoNotInline;
import kotlin.jvm.internal.Intrinsics;
import android.graphics.Typeface;
import androidx.annotation.StyleableRes;
import android.content.res.TypedArray;
import org.jetbrains.annotations.NotNull;
import androidx.annotation.RequiresApi;
import kotlin.Metadata;

@Metadata
@RequiresApi(26)
final class TypedArrayApi26ImplKt
{
    @NotNull
    public static final TypedArrayApi26ImplKt INSTANCE;
    
    static {
        INSTANCE = new TypedArrayApi26ImplKt();
    }
    
    private TypedArrayApi26ImplKt() {
    }
    
    @DoNotInline
    @NotNull
    public static final Typeface getFont(@NotNull final TypedArray typedArray, @StyleableRes final int n) {
        Intrinsics.checkNotNullParameter((Object)typedArray, "typedArray");
        final Typeface \u3007080 = \u3007\u3007888.\u3007080(typedArray, n);
        Intrinsics.Oo08((Object)\u3007080);
        return \u3007080;
    }
}

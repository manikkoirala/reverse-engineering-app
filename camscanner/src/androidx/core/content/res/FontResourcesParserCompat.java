// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content.res;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.util.Base64;
import androidx.core.provider.FontRequest;
import androidx.core.R;
import android.util.Xml;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import androidx.annotation.ArrayRes;
import androidx.annotation.Nullable;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;
import android.content.res.Resources;
import androidx.annotation.NonNull;
import org.xmlpull.v1.XmlPullParser;
import android.content.res.TypedArray;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public class FontResourcesParserCompat
{
    private static final int DEFAULT_TIMEOUT_MILLIS = 500;
    public static final int FETCH_STRATEGY_ASYNC = 1;
    public static final int FETCH_STRATEGY_BLOCKING = 0;
    public static final int INFINITE_TIMEOUT_VALUE = -1;
    private static final int ITALIC = 1;
    private static final int NORMAL_WEIGHT = 400;
    
    private FontResourcesParserCompat() {
    }
    
    private static int getType(final TypedArray typedArray, final int n) {
        return Api21Impl.getType(typedArray, n);
    }
    
    @Nullable
    public static FamilyResourceEntry parse(@NonNull final XmlPullParser xmlPullParser, @NonNull final Resources resources) throws XmlPullParserException, IOException {
        int next;
        do {
            next = xmlPullParser.next();
        } while (next != 2 && next != 1);
        if (next == 2) {
            return readFamilies(xmlPullParser, resources);
        }
        throw new XmlPullParserException("No start tag found");
    }
    
    @NonNull
    public static List<List<byte[]>> readCerts(@NonNull final Resources resources, @ArrayRes int i) {
        if (i == 0) {
            return Collections.emptyList();
        }
        final TypedArray obtainTypedArray = resources.obtainTypedArray(i);
        try {
            if (obtainTypedArray.length() == 0) {
                return Collections.emptyList();
            }
            final ArrayList list = new ArrayList();
            if (getType(obtainTypedArray, 0) == 1) {
                int resourceId;
                for (i = 0; i < obtainTypedArray.length(); ++i) {
                    resourceId = obtainTypedArray.getResourceId(i, 0);
                    if (resourceId != 0) {
                        list.add(toByteArrayList(resources.getStringArray(resourceId)));
                    }
                }
            }
            else {
                list.add(toByteArrayList(resources.getStringArray(i)));
            }
            return list;
        }
        finally {
            obtainTypedArray.recycle();
        }
    }
    
    @Nullable
    private static FamilyResourceEntry readFamilies(final XmlPullParser xmlPullParser, final Resources resources) throws XmlPullParserException, IOException {
        xmlPullParser.require(2, (String)null, "font-family");
        if (xmlPullParser.getName().equals("font-family")) {
            return readFamily(xmlPullParser, resources);
        }
        skip(xmlPullParser);
        return null;
    }
    
    @Nullable
    private static FamilyResourceEntry readFamily(final XmlPullParser xmlPullParser, final Resources resources) throws XmlPullParserException, IOException {
        final TypedArray obtainAttributes = resources.obtainAttributes(Xml.asAttributeSet(xmlPullParser), R.styleable.FontFamily);
        final String string = obtainAttributes.getString(R.styleable.FontFamily_fontProviderAuthority);
        final String string2 = obtainAttributes.getString(R.styleable.FontFamily_fontProviderPackage);
        final String string3 = obtainAttributes.getString(R.styleable.FontFamily_fontProviderQuery);
        final int resourceId = obtainAttributes.getResourceId(R.styleable.FontFamily_fontProviderCerts, 0);
        final int integer = obtainAttributes.getInteger(R.styleable.FontFamily_fontProviderFetchStrategy, 1);
        final int integer2 = obtainAttributes.getInteger(R.styleable.FontFamily_fontProviderFetchTimeout, 500);
        final String string4 = obtainAttributes.getString(R.styleable.FontFamily_fontProviderSystemFontFamily);
        obtainAttributes.recycle();
        if (string != null && string2 != null && string3 != null) {
            while (xmlPullParser.next() != 3) {
                skip(xmlPullParser);
            }
            return (FamilyResourceEntry)new ProviderResourceEntry(new FontRequest(string, string2, string3, readCerts(resources, resourceId)), integer, integer2, string4);
        }
        final ArrayList list = new ArrayList();
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() != 2) {
                continue;
            }
            if (xmlPullParser.getName().equals("font")) {
                list.add(readFont(xmlPullParser, resources));
            }
            else {
                skip(xmlPullParser);
            }
        }
        if (list.isEmpty()) {
            return null;
        }
        return (FamilyResourceEntry)new FontFamilyFilesResourceEntry((FontFileResourceEntry[])list.toArray(new FontFileResourceEntry[0]));
    }
    
    private static FontFileResourceEntry readFont(final XmlPullParser xmlPullParser, final Resources resources) throws XmlPullParserException, IOException {
        final TypedArray obtainAttributes = resources.obtainAttributes(Xml.asAttributeSet(xmlPullParser), R.styleable.FontFamilyFont);
        int n = R.styleable.FontFamilyFont_fontWeight;
        if (!obtainAttributes.hasValue(n)) {
            n = R.styleable.FontFamilyFont_android_fontWeight;
        }
        final int int1 = obtainAttributes.getInt(n, 400);
        int n2 = R.styleable.FontFamilyFont_fontStyle;
        if (!obtainAttributes.hasValue(n2)) {
            n2 = R.styleable.FontFamilyFont_android_fontStyle;
        }
        final boolean b = 1 == obtainAttributes.getInt(n2, 0);
        int n3 = R.styleable.FontFamilyFont_ttcIndex;
        if (!obtainAttributes.hasValue(n3)) {
            n3 = R.styleable.FontFamilyFont_android_ttcIndex;
        }
        int n4 = R.styleable.FontFamilyFont_fontVariationSettings;
        if (!obtainAttributes.hasValue(n4)) {
            n4 = R.styleable.FontFamilyFont_android_fontVariationSettings;
        }
        final String string = obtainAttributes.getString(n4);
        final int int2 = obtainAttributes.getInt(n3, 0);
        int n5 = R.styleable.FontFamilyFont_font;
        if (!obtainAttributes.hasValue(n5)) {
            n5 = R.styleable.FontFamilyFont_android_font;
        }
        final int resourceId = obtainAttributes.getResourceId(n5, 0);
        final String string2 = obtainAttributes.getString(n5);
        obtainAttributes.recycle();
        while (xmlPullParser.next() != 3) {
            skip(xmlPullParser);
        }
        return new FontFileResourceEntry(string2, int1, b, string, int2, resourceId);
    }
    
    private static void skip(final XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        int i = 1;
        while (i > 0) {
            final int next = xmlPullParser.next();
            if (next != 2) {
                if (next != 3) {
                    continue;
                }
                --i;
            }
            else {
                ++i;
            }
        }
    }
    
    private static List<byte[]> toByteArrayList(final String[] array) {
        final ArrayList list = new ArrayList();
        for (int length = array.length, i = 0; i < length; ++i) {
            list.add(Base64.decode(array[i], 0));
        }
        return list;
    }
    
    @RequiresApi(21)
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        @DoNotInline
        static int getType(final TypedArray typedArray, final int n) {
            return typedArray.getType(n);
        }
    }
    
    public interface FamilyResourceEntry
    {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface FetchStrategy {
    }
    
    public static final class FontFamilyFilesResourceEntry implements FamilyResourceEntry
    {
        @NonNull
        private final FontFileResourceEntry[] mEntries;
        
        public FontFamilyFilesResourceEntry(@NonNull final FontFileResourceEntry[] mEntries) {
            this.mEntries = mEntries;
        }
        
        @NonNull
        public FontFileResourceEntry[] getEntries() {
            return this.mEntries;
        }
    }
    
    public static final class FontFileResourceEntry
    {
        @NonNull
        private final String mFileName;
        private final boolean mItalic;
        private final int mResourceId;
        private final int mTtcIndex;
        private final String mVariationSettings;
        private final int mWeight;
        
        public FontFileResourceEntry(@NonNull final String mFileName, final int mWeight, final boolean mItalic, @Nullable final String mVariationSettings, final int mTtcIndex, final int mResourceId) {
            this.mFileName = mFileName;
            this.mWeight = mWeight;
            this.mItalic = mItalic;
            this.mVariationSettings = mVariationSettings;
            this.mTtcIndex = mTtcIndex;
            this.mResourceId = mResourceId;
        }
        
        @NonNull
        public String getFileName() {
            return this.mFileName;
        }
        
        public int getResourceId() {
            return this.mResourceId;
        }
        
        public int getTtcIndex() {
            return this.mTtcIndex;
        }
        
        @Nullable
        public String getVariationSettings() {
            return this.mVariationSettings;
        }
        
        public int getWeight() {
            return this.mWeight;
        }
        
        public boolean isItalic() {
            return this.mItalic;
        }
    }
    
    public static final class ProviderResourceEntry implements FamilyResourceEntry
    {
        @NonNull
        private final FontRequest mRequest;
        private final int mStrategy;
        @Nullable
        private final String mSystemFontFamilyName;
        private final int mTimeoutMs;
        
        public ProviderResourceEntry(@NonNull final FontRequest fontRequest, final int n, final int n2) {
            this(fontRequest, n, n2, null);
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY })
        public ProviderResourceEntry(@NonNull final FontRequest mRequest, final int mStrategy, final int mTimeoutMs, @Nullable final String mSystemFontFamilyName) {
            this.mRequest = mRequest;
            this.mStrategy = mStrategy;
            this.mTimeoutMs = mTimeoutMs;
            this.mSystemFontFamilyName = mSystemFontFamilyName;
        }
        
        public int getFetchStrategy() {
            return this.mStrategy;
        }
        
        @NonNull
        public FontRequest getRequest() {
            return this.mRequest;
        }
        
        @Nullable
        @RestrictTo({ RestrictTo.Scope.LIBRARY })
        public String getSystemFontFamilyName() {
            return this.mSystemFontFamilyName;
        }
        
        public int getTimeout() {
            return this.mTimeoutMs;
        }
    }
}

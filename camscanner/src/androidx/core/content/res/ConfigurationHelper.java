// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content.res;

import androidx.annotation.NonNull;
import android.content.res.Resources;

public final class ConfigurationHelper
{
    private ConfigurationHelper() {
    }
    
    public static int getDensityDpi(@NonNull final Resources resources) {
        return resources.getConfiguration().densityDpi;
    }
}

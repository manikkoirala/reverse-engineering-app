// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content.res;

import java.io.IOException;
import android.util.AttributeSet;
import android.content.res.XmlResourceParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParser;
import android.util.Xml;
import androidx.annotation.Nullable;
import android.content.res.Resources$Theme;
import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import android.content.res.Resources;
import androidx.annotation.ColorInt;
import android.graphics.Shader;
import android.content.res.ColorStateList;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public final class ComplexColorCompat
{
    private static final String LOG_TAG = "ComplexColorCompat";
    private int mColor;
    private final ColorStateList mColorStateList;
    private final Shader mShader;
    
    private ComplexColorCompat(final Shader mShader, final ColorStateList mColorStateList, @ColorInt final int mColor) {
        this.mShader = mShader;
        this.mColorStateList = mColorStateList;
        this.mColor = mColor;
    }
    
    @NonNull
    private static ComplexColorCompat createFromXml(@NonNull final Resources resources, @ColorRes int next, @Nullable final Resources$Theme resources$Theme) throws IOException, XmlPullParserException {
        final XmlResourceParser xml = resources.getXml(next);
        final AttributeSet attributeSet = Xml.asAttributeSet((XmlPullParser)xml);
        do {
            next = ((XmlPullParser)xml).next();
        } while (next != 2 && next != 1);
        if (next != 2) {
            throw new XmlPullParserException("No start tag found");
        }
        final String name = ((XmlPullParser)xml).getName();
        name.hashCode();
        if (name.equals("gradient")) {
            return from(GradientColorInflaterCompat.createFromXmlInner(resources, (XmlPullParser)xml, attributeSet, resources$Theme));
        }
        if (name.equals("selector")) {
            return from(ColorStateListInflaterCompat.createFromXmlInner(resources, (XmlPullParser)xml, attributeSet, resources$Theme));
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(((XmlPullParser)xml).getPositionDescription());
        sb.append(": unsupported complex color tag ");
        sb.append(name);
        throw new XmlPullParserException(sb.toString());
    }
    
    static ComplexColorCompat from(@ColorInt final int n) {
        return new ComplexColorCompat(null, null, n);
    }
    
    static ComplexColorCompat from(@NonNull final ColorStateList list) {
        return new ComplexColorCompat(null, list, list.getDefaultColor());
    }
    
    static ComplexColorCompat from(@NonNull final Shader shader) {
        return new ComplexColorCompat(shader, null, 0);
    }
    
    @Nullable
    public static ComplexColorCompat inflate(@NonNull final Resources resources, @ColorRes final int n, @Nullable final Resources$Theme resources$Theme) {
        try {
            return createFromXml(resources, n, resources$Theme);
        }
        catch (final Exception ex) {
            return null;
        }
    }
    
    @ColorInt
    public int getColor() {
        return this.mColor;
    }
    
    @Nullable
    public Shader getShader() {
        return this.mShader;
    }
    
    public boolean isGradient() {
        return this.mShader != null;
    }
    
    public boolean isStateful() {
        if (this.mShader == null) {
            final ColorStateList mColorStateList = this.mColorStateList;
            if (mColorStateList != null && mColorStateList.isStateful()) {
                return true;
            }
        }
        return false;
    }
    
    public boolean onStateChanged(final int[] array) {
        if (this.isStateful()) {
            final ColorStateList mColorStateList = this.mColorStateList;
            final int colorForState = mColorStateList.getColorForState(array, mColorStateList.getDefaultColor());
            if (colorForState != this.mColor) {
                this.mColor = colorForState;
                return true;
            }
        }
        return false;
    }
    
    public void setColor(@ColorInt final int mColor) {
        this.mColor = mColor;
    }
    
    public boolean willDraw() {
        return this.isGradient() || this.mColor != 0;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import android.annotation.SuppressLint;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import android.content.SharedPreferences$Editor;
import kotlin.jvm.functions.Function1;
import org.jetbrains.annotations.NotNull;
import android.content.SharedPreferences;
import kotlin.Metadata;

@Metadata
public final class SharedPreferencesKt
{
    @SuppressLint({ "ApplySharedPref" })
    public static final void edit(@NotNull final SharedPreferences sharedPreferences, final boolean b, @NotNull final Function1<? super SharedPreferences$Editor, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)sharedPreferences, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "action");
        final SharedPreferences$Editor edit = sharedPreferences.edit();
        Intrinsics.checkNotNullExpressionValue((Object)edit, "editor");
        function1.invoke((Object)edit);
        if (b) {
            edit.commit();
        }
        else {
            edit.apply();
        }
    }
}

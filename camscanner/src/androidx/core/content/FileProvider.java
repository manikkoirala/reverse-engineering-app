// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import org.xmlpull.v1.XmlPullParser;
import android.content.pm.PackageItemInfo;
import android.os.BaseBundle;
import java.util.Iterator;
import android.net.Uri$Builder;
import java.util.Map;
import android.text.TextUtils;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.database.MatrixCursor;
import android.database.Cursor;
import java.io.FileNotFoundException;
import android.os.ParcelFileDescriptor;
import android.content.ContentValues;
import android.webkit.MimeTypeMap;
import android.os.Environment;
import android.annotation.SuppressLint;
import android.net.Uri;
import androidx.annotation.NonNull;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;
import androidx.annotation.VisibleForTesting;
import android.os.Bundle;
import android.content.res.XmlResourceParser;
import androidx.annotation.Nullable;
import android.content.pm.ProviderInfo;
import android.content.Context;
import androidx.annotation.XmlRes;
import androidx.annotation.GuardedBy;
import java.util.HashMap;
import java.io.File;
import android.content.ContentProvider;

public class FileProvider extends ContentProvider
{
    private static final String ATTR_NAME = "name";
    private static final String ATTR_PATH = "path";
    private static final String[] COLUMNS;
    private static final File DEVICE_ROOT;
    private static final String DISPLAYNAME_FIELD = "displayName";
    private static final String META_DATA_FILE_PROVIDER_PATHS = "android.support.FILE_PROVIDER_PATHS";
    private static final String TAG_CACHE_PATH = "cache-path";
    private static final String TAG_EXTERNAL = "external-path";
    private static final String TAG_EXTERNAL_CACHE = "external-cache-path";
    private static final String TAG_EXTERNAL_FILES = "external-files-path";
    private static final String TAG_EXTERNAL_MEDIA = "external-media-path";
    private static final String TAG_FILES_PATH = "files-path";
    private static final String TAG_ROOT_PATH = "root-path";
    @GuardedBy("sCache")
    private static final HashMap<String, PathStrategy> sCache;
    private int mResourceId;
    private PathStrategy mStrategy;
    
    static {
        COLUMNS = new String[] { "_display_name", "_size" };
        DEVICE_ROOT = new File("/");
        sCache = new HashMap<String, PathStrategy>();
    }
    
    public FileProvider() {
        this.mResourceId = 0;
    }
    
    protected FileProvider(@XmlRes final int mResourceId) {
        this.mResourceId = mResourceId;
    }
    
    private static File buildPath(File parent, final String... array) {
        File file;
        for (int length = array.length, i = 0; i < length; ++i, parent = file) {
            final String child = array[i];
            file = parent;
            if (child != null) {
                file = new File(parent, child);
            }
        }
        return parent;
    }
    
    private static Object[] copyOf(final Object[] array, final int n) {
        final Object[] array2 = new Object[n];
        System.arraycopy(array, 0, array2, 0, n);
        return array2;
    }
    
    private static String[] copyOf(final String[] array, final int n) {
        final String[] array2 = new String[n];
        System.arraycopy(array, 0, array2, 0, n);
        return array2;
    }
    
    @VisibleForTesting
    static XmlResourceParser getFileProviderPathsMetaData(final Context context, final String str, @Nullable final ProviderInfo providerInfo, final int n) {
        if (providerInfo == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Couldn't find meta-data for provider with authority ");
            sb.append(str);
            throw new IllegalArgumentException(sb.toString());
        }
        if (providerInfo.metaData == null && n != 0) {
            ((BaseBundle)(providerInfo.metaData = new Bundle(1))).putInt("android.support.FILE_PROVIDER_PATHS", n);
        }
        final XmlResourceParser loadXmlMetaData = ((PackageItemInfo)providerInfo).loadXmlMetaData(context.getPackageManager(), "android.support.FILE_PROVIDER_PATHS");
        if (loadXmlMetaData != null) {
            return loadXmlMetaData;
        }
        throw new IllegalArgumentException("Missing android.support.FILE_PROVIDER_PATHS meta-data");
    }
    
    private static PathStrategy getPathStrategy(final Context context, final String s, final int n) {
        final HashMap<String, PathStrategy> sCache = FileProvider.sCache;
        synchronized (sCache) {
            PathStrategy pathStrategy;
            if ((pathStrategy = sCache.get(s)) == null) {
                try {
                    pathStrategy = parsePathStrategy(context, s, n);
                    sCache.put(s, pathStrategy);
                }
                catch (final XmlPullParserException cause) {
                    throw new IllegalArgumentException("Failed to parse android.support.FILE_PROVIDER_PATHS meta-data", (Throwable)cause);
                }
                catch (final IOException cause2) {
                    throw new IllegalArgumentException("Failed to parse android.support.FILE_PROVIDER_PATHS meta-data", cause2);
                }
            }
            return pathStrategy;
        }
    }
    
    public static Uri getUriForFile(@NonNull final Context context, @NonNull final String s, @NonNull final File file) {
        return getPathStrategy(context, s, 0).getUriForFile(file);
    }
    
    @SuppressLint({ "StreamFiles" })
    @NonNull
    public static Uri getUriForFile(@NonNull final Context context, @NonNull final String s, @NonNull final File file, @NonNull final String s2) {
        return getUriForFile(context, s, file).buildUpon().appendQueryParameter("displayName", s2).build();
    }
    
    private static int modeToMode(final String str) {
        int n;
        if ("r".equals(str)) {
            n = 268435456;
        }
        else if (!"w".equals(str) && !"wt".equals(str)) {
            if ("wa".equals(str)) {
                n = 704643072;
            }
            else if ("rw".equals(str)) {
                n = 939524096;
            }
            else {
                if (!"rwt".equals(str)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Invalid mode: ");
                    sb.append(str);
                    throw new IllegalArgumentException(sb.toString());
                }
                n = 1006632960;
            }
        }
        else {
            n = 738197504;
        }
        return n;
    }
    
    private static PathStrategy parsePathStrategy(final Context context, final String s, int next) throws IOException, XmlPullParserException {
        final SimplePathStrategy simplePathStrategy = new SimplePathStrategy(s);
        final XmlResourceParser fileProviderPathsMetaData = getFileProviderPathsMetaData(context, s, context.getPackageManager().resolveContentProvider(s, 128), next);
        while (true) {
            next = ((XmlPullParser)fileProviderPathsMetaData).next();
            if (next == 1) {
                break;
            }
            if (next != 2) {
                continue;
            }
            final String name = ((XmlPullParser)fileProviderPathsMetaData).getName();
            final File file = null;
            final String attributeValue = ((XmlPullParser)fileProviderPathsMetaData).getAttributeValue((String)null, "name");
            final String attributeValue2 = ((XmlPullParser)fileProviderPathsMetaData).getAttributeValue((String)null, "path");
            File file2;
            if ("root-path".equals(name)) {
                file2 = FileProvider.DEVICE_ROOT;
            }
            else if ("files-path".equals(name)) {
                file2 = context.getFilesDir();
            }
            else if ("cache-path".equals(name)) {
                file2 = context.getCacheDir();
            }
            else if ("external-path".equals(name)) {
                file2 = Environment.getExternalStorageDirectory();
            }
            else if ("external-files-path".equals(name)) {
                final File[] externalFilesDirs = ContextCompat.getExternalFilesDirs(context, null);
                file2 = file;
                if (externalFilesDirs.length > 0) {
                    file2 = externalFilesDirs[0];
                }
            }
            else if ("external-cache-path".equals(name)) {
                final File[] externalCacheDirs = ContextCompat.getExternalCacheDirs(context);
                file2 = file;
                if (externalCacheDirs.length > 0) {
                    file2 = externalCacheDirs[0];
                }
            }
            else {
                file2 = file;
                if ("external-media-path".equals(name)) {
                    final File[] externalMediaDirs = Api21Impl.getExternalMediaDirs(context);
                    file2 = file;
                    if (externalMediaDirs.length > 0) {
                        file2 = externalMediaDirs[0];
                    }
                }
            }
            if (file2 == null) {
                continue;
            }
            simplePathStrategy.addRoot(attributeValue, buildPath(file2, attributeValue2));
        }
        return (PathStrategy)simplePathStrategy;
    }
    
    public void attachInfo(@NonNull final Context context, @NonNull ProviderInfo sCache) {
        super.attachInfo(context, sCache);
        if (!sCache.exported) {
            if (sCache.grantUriPermissions) {
                final String key = sCache.authority.split(";")[0];
                sCache = (ProviderInfo)FileProvider.sCache;
                synchronized (sCache) {
                    ((HashMap<Object, Object>)sCache).remove(key);
                    monitorexit(sCache);
                    this.mStrategy = getPathStrategy(context, key, this.mResourceId);
                    return;
                }
            }
            throw new SecurityException("Provider must grant uri permissions");
        }
        throw new SecurityException("Provider must not be exported");
    }
    
    public int delete(@NonNull final Uri uri, @Nullable final String s, @Nullable final String[] array) {
        return this.mStrategy.getFileForUri(uri).delete() ? 1 : 0;
    }
    
    @Nullable
    public String getType(@NonNull final Uri uri) {
        final File fileForUri = this.mStrategy.getFileForUri(uri);
        final int lastIndex = fileForUri.getName().lastIndexOf(46);
        if (lastIndex >= 0) {
            final String mimeTypeFromExtension = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileForUri.getName().substring(lastIndex + 1));
            if (mimeTypeFromExtension != null) {
                return mimeTypeFromExtension;
            }
        }
        return "application/octet-stream";
    }
    
    public Uri insert(@NonNull final Uri uri, @NonNull final ContentValues contentValues) {
        throw new UnsupportedOperationException("No external inserts");
    }
    
    public boolean onCreate() {
        return true;
    }
    
    @SuppressLint({ "UnknownNullness" })
    public ParcelFileDescriptor openFile(@NonNull final Uri uri, @NonNull final String s) throws FileNotFoundException {
        return ParcelFileDescriptor.open(this.mStrategy.getFileForUri(uri), modeToMode(s));
    }
    
    @NonNull
    public Cursor query(@NonNull final Uri uri, @Nullable String[] copy, @Nullable String queryParameter, @Nullable final String[] array, @Nullable final String s) {
        final File fileForUri = this.mStrategy.getFileForUri(uri);
        queryParameter = uri.getQueryParameter("displayName");
        String[] columns = copy;
        if (copy == null) {
            columns = FileProvider.COLUMNS;
        }
        final String[] array2 = new String[columns.length];
        final Object[] array3 = new Object[columns.length];
        final int length = columns.length;
        int i = 0;
        int n = 0;
        while (i < length) {
            final String s2 = columns[i];
            int n4 = 0;
            Label_0162: {
                int n3;
                if ("_display_name".equals(s2)) {
                    array2[n] = "_display_name";
                    final int n2 = n + 1;
                    String name;
                    if (queryParameter == null) {
                        name = fileForUri.getName();
                    }
                    else {
                        name = queryParameter;
                    }
                    array3[n] = name;
                    n3 = n2;
                }
                else {
                    n4 = n;
                    if (!"_size".equals(s2)) {
                        break Label_0162;
                    }
                    array2[n] = "_size";
                    final int n5 = n + 1;
                    array3[n] = fileForUri.length();
                    n3 = n5;
                }
                n4 = n3;
            }
            ++i;
            n = n4;
        }
        copy = copyOf(array2, n);
        final Object[] copy2 = copyOf(array3, n);
        final MatrixCursor matrixCursor = new MatrixCursor(copy, 1);
        matrixCursor.addRow(copy2);
        return (Cursor)matrixCursor;
    }
    
    public int update(@NonNull final Uri uri, @NonNull final ContentValues contentValues, @Nullable final String s, @Nullable final String[] array) {
        throw new UnsupportedOperationException("No external updates");
    }
    
    @RequiresApi(21)
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        @DoNotInline
        static File[] getExternalMediaDirs(final Context context) {
            return context.getExternalMediaDirs();
        }
    }
    
    interface PathStrategy
    {
        File getFileForUri(final Uri p0);
        
        Uri getUriForFile(final File p0);
    }
    
    static class SimplePathStrategy implements PathStrategy
    {
        private final String mAuthority;
        private final HashMap<String, File> mRoots;
        
        SimplePathStrategy(final String mAuthority) {
            this.mRoots = new HashMap<String, File>();
            this.mAuthority = mAuthority;
        }
        
        void addRoot(final String key, final File obj) {
            if (!TextUtils.isEmpty((CharSequence)key)) {
                try {
                    this.mRoots.put(key, obj.getCanonicalFile());
                    return;
                }
                catch (final IOException cause) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to resolve canonical path for ");
                    sb.append(obj);
                    throw new IllegalArgumentException(sb.toString(), cause);
                }
            }
            throw new IllegalArgumentException("Name must not be empty");
        }
        
        @Override
        public File getFileForUri(Uri uri) {
            final String encodedPath = uri.getEncodedPath();
            final int index = encodedPath.indexOf(47, 1);
            final String decode = Uri.decode(encodedPath.substring(1, index));
            final String decode2 = Uri.decode(encodedPath.substring(index + 1));
            final File parent = this.mRoots.get(decode);
            if (parent != null) {
                uri = (Uri)new File(parent, decode2);
                try {
                    final File canonicalFile = ((File)uri).getCanonicalFile();
                    if (canonicalFile.getPath().startsWith(parent.getPath())) {
                        return canonicalFile;
                    }
                    throw new SecurityException("Resolved path jumped beyond configured root");
                }
                catch (final IOException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to resolve canonical path for ");
                    sb.append(uri);
                    throw new IllegalArgumentException(sb.toString());
                }
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Unable to find configured root for ");
            sb2.append(uri);
            throw new IllegalArgumentException(sb2.toString());
        }
        
        @Override
        public Uri getUriForFile(File string) {
            try {
                final String canonicalPath = string.getCanonicalPath();
                final Iterator<Map.Entry<String, File>> iterator = this.mRoots.entrySet().iterator();
                string = null;
                while (iterator.hasNext()) {
                    final Map.Entry<?, File> entry = iterator.next();
                    final String path = entry.getValue().getPath();
                    if (canonicalPath.startsWith(path) && (string == null || path.length() > ((Map.Entry<K, File>)string).getValue().getPath().length())) {
                        string = (File)entry;
                    }
                }
                if (string != null) {
                    final String path2 = ((Map.Entry<K, File>)string).getValue().getPath();
                    String s;
                    if (path2.endsWith("/")) {
                        s = canonicalPath.substring(path2.length());
                    }
                    else {
                        s = canonicalPath.substring(path2.length() + 1);
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append(Uri.encode((String)((Map.Entry<String, V>)string).getKey()));
                    sb.append('/');
                    sb.append(Uri.encode(s, "/"));
                    string = (File)sb.toString();
                    return new Uri$Builder().scheme("content").authority(this.mAuthority).encodedPath((String)string).build();
                }
                string = (File)new StringBuilder();
                ((StringBuilder)string).append("Failed to find configured root that contains ");
                ((StringBuilder)string).append(canonicalPath);
                throw new IllegalArgumentException(((StringBuilder)string).toString());
            }
            catch (final IOException ex) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Failed to resolve canonical path for ");
                sb2.append(string);
                throw new IllegalArgumentException(sb2.toString());
            }
        }
    }
}

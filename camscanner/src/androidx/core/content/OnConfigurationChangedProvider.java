// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import androidx.annotation.NonNull;
import android.content.res.Configuration;
import androidx.core.util.Consumer;

public interface OnConfigurationChangedProvider
{
    void addOnConfigurationChangedListener(@NonNull final Consumer<Configuration> p0);
    
    void removeOnConfigurationChangedListener(@NonNull final Consumer<Configuration> p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import android.os.BaseBundle;
import java.util.Objects;
import android.annotation.SuppressLint;
import androidx.core.util.Preconditions;
import java.util.HashMap;
import android.content.ClipData$Item;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.os.Bundle;
import java.util.Iterator;
import java.util.Set;
import android.os.Build$VERSION;
import androidx.core.util.Consumer;
import androidx.annotation.NonNull;
import java.io.Serializable;
import android.os.Parcelable;
import android.content.Intent;
import java.util.Map;
import android.content.ComponentName;
import android.net.Uri;
import android.content.ClipData;
import androidx.core.util.Predicate;

public class IntentSanitizer
{
    private static final String TAG = "IntentSanitizer";
    private boolean mAllowAnyComponent;
    private boolean mAllowClipDataText;
    private boolean mAllowIdentifier;
    private boolean mAllowSelector;
    private boolean mAllowSourceBounds;
    private Predicate<String> mAllowedActions;
    private Predicate<String> mAllowedCategories;
    private Predicate<ClipData> mAllowedClipData;
    private Predicate<Uri> mAllowedClipDataUri;
    private Predicate<ComponentName> mAllowedComponents;
    private Predicate<Uri> mAllowedData;
    private Map<String, Predicate<Object>> mAllowedExtras;
    private int mAllowedFlags;
    private Predicate<String> mAllowedPackages;
    private Predicate<String> mAllowedTypes;
    
    private IntentSanitizer() {
    }
    
    private void putExtra(final Intent intent, final String s, final Object o) {
        if (o == null) {
            ((BaseBundle)intent.getExtras()).putString(s, (String)null);
        }
        else if (o instanceof Parcelable) {
            intent.putExtra(s, (Parcelable)o);
        }
        else if (o instanceof Parcelable[]) {
            intent.putExtra(s, (Parcelable[])o);
        }
        else {
            if (!(o instanceof Serializable)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unsupported type ");
                sb.append(o.getClass());
                throw new IllegalArgumentException(sb.toString());
            }
            intent.putExtra(s, (Serializable)o);
        }
    }
    
    @NonNull
    public Intent sanitize(@NonNull final Intent intent, @NonNull final Consumer<String> consumer) {
        final Intent intent2 = new Intent();
        final ComponentName component = intent.getComponent();
        if ((this.mAllowAnyComponent && component == null) || this.mAllowedComponents.test(component)) {
            intent2.setComponent(component);
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append("Component is not allowed: ");
            sb.append(component);
            consumer.accept(sb.toString());
            intent2.setComponent(new ComponentName("android", "java.lang.Void"));
        }
        final String package1 = intent.getPackage();
        if (package1 != null && !this.mAllowedPackages.test(package1)) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Package is not allowed: ");
            sb2.append(package1);
            consumer.accept(sb2.toString());
        }
        else {
            intent2.setPackage(package1);
        }
        final int mAllowedFlags = this.mAllowedFlags;
        final int flags = intent.getFlags();
        final int mAllowedFlags2 = this.mAllowedFlags;
        if ((mAllowedFlags | flags) == mAllowedFlags2) {
            intent2.setFlags(intent.getFlags());
        }
        else {
            intent2.setFlags(intent.getFlags() & mAllowedFlags2);
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("The intent contains flags that are not allowed: 0x");
            sb3.append(Integer.toHexString(intent.getFlags() & ~this.mAllowedFlags));
            consumer.accept(sb3.toString());
        }
        final String action = intent.getAction();
        if (action != null && !this.mAllowedActions.test(action)) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Action is not allowed: ");
            sb4.append(action);
            consumer.accept(sb4.toString());
        }
        else {
            intent2.setAction(action);
        }
        final Uri data = intent.getData();
        if (data != null && !this.mAllowedData.test(data)) {
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("Data is not allowed: ");
            sb5.append(data);
            consumer.accept(sb5.toString());
        }
        else {
            intent2.setData(data);
        }
        final String type = intent.getType();
        if (type != null && !this.mAllowedTypes.test(type)) {
            final StringBuilder sb6 = new StringBuilder();
            sb6.append("Type is not allowed: ");
            sb6.append(type);
            consumer.accept(sb6.toString());
        }
        else {
            intent2.setDataAndType(intent2.getData(), type);
        }
        final Set categories = intent.getCategories();
        if (categories != null) {
            for (final String str : categories) {
                if (this.mAllowedCategories.test(str)) {
                    intent2.addCategory(str);
                }
                else {
                    final StringBuilder sb7 = new StringBuilder();
                    sb7.append("Category is not allowed: ");
                    sb7.append(str);
                    consumer.accept(sb7.toString());
                }
            }
        }
        final Bundle extras = intent.getExtras();
        if (extras != null) {
            for (final String str2 : ((BaseBundle)extras).keySet()) {
                if (str2.equals("android.intent.extra.STREAM") && (this.mAllowedFlags & 0x1) == 0x0) {
                    consumer.accept("Allowing Extra Stream requires also allowing at least  FLAG_GRANT_READ_URI_PERMISSION Flag.");
                }
                else if (str2.equals("output") && (~this.mAllowedFlags & 0x3) != 0x0) {
                    consumer.accept("Allowing Extra Output requires also allowing FLAG_GRANT_READ_URI_PERMISSION and FLAG_GRANT_WRITE_URI_PERMISSION Flags.");
                }
                else {
                    final Object value = ((BaseBundle)extras).get(str2);
                    final Predicate predicate = this.mAllowedExtras.get(str2);
                    if (predicate != null && predicate.test(value)) {
                        this.putExtra(intent2, str2, value);
                    }
                    else {
                        final StringBuilder sb8 = new StringBuilder();
                        sb8.append("Extra is not allowed. Key: ");
                        sb8.append(str2);
                        sb8.append(". Value: ");
                        sb8.append(value);
                        consumer.accept(sb8.toString());
                    }
                }
            }
        }
        final int sdk_INT = Build$VERSION.SDK_INT;
        Api16Impl.sanitizeClipData(intent, intent2, this.mAllowedClipData, this.mAllowClipDataText, this.mAllowedClipDataUri, consumer);
        if (sdk_INT >= 29) {
            if (this.mAllowIdentifier) {
                Api29Impl.setIdentifier(intent2, Api29Impl.getIdentifier(intent));
            }
            else if (Api29Impl.getIdentifier(intent) != null) {
                final StringBuilder sb9 = new StringBuilder();
                sb9.append("Identifier is not allowed: ");
                sb9.append(Api29Impl.getIdentifier(intent));
                consumer.accept(sb9.toString());
            }
        }
        if (this.mAllowSelector) {
            Api15Impl.setSelector(intent2, Api15Impl.getSelector(intent));
        }
        else if (Api15Impl.getSelector(intent) != null) {
            final StringBuilder sb10 = new StringBuilder();
            sb10.append("Selector is not allowed: ");
            sb10.append(Api15Impl.getSelector(intent));
            consumer.accept(sb10.toString());
        }
        if (this.mAllowSourceBounds) {
            intent2.setSourceBounds(intent.getSourceBounds());
        }
        else if (intent.getSourceBounds() != null) {
            final StringBuilder sb11 = new StringBuilder();
            sb11.append("SourceBounds is not allowed: ");
            sb11.append(intent.getSourceBounds());
            consumer.accept(sb11.toString());
        }
        return intent2;
    }
    
    @NonNull
    public Intent sanitizeByFiltering(@NonNull final Intent intent) {
        return this.sanitize(intent, new \u300780\u3007808\u3007O());
    }
    
    @NonNull
    public Intent sanitizeByThrowing(@NonNull final Intent intent) {
        return this.sanitize(intent, new OO0o\u3007\u3007\u3007\u30070());
    }
    
    @RequiresApi(15)
    private static class Api15Impl
    {
        @DoNotInline
        static Intent getSelector(final Intent intent) {
            return intent.getSelector();
        }
        
        @DoNotInline
        static void setSelector(final Intent intent, final Intent selector) {
            intent.setSelector(selector);
        }
    }
    
    @RequiresApi(16)
    private static class Api16Impl
    {
        private static void checkOtherMembers(final int i, final ClipData$Item obj, final Consumer<String> consumer) {
            if (obj.getHtmlText() != null || obj.getIntent() != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("ClipData item at position ");
                sb.append(i);
                sb.append(" contains htmlText, textLinks or intent: ");
                sb.append(obj);
                consumer.accept(sb.toString());
            }
        }
        
        @DoNotInline
        static void sanitizeClipData(@NonNull final Intent intent, final Intent intent2, final Predicate<ClipData> predicate, final boolean b, final Predicate<Uri> predicate2, final Consumer<String> consumer) {
            final ClipData clipData = intent.getClipData();
            if (clipData == null) {
                return;
            }
            if (predicate != null && predicate.test(clipData)) {
                intent2.setClipData(clipData);
            }
            else {
                int i = 0;
                ClipData clipData2 = null;
                while (i < clipData.getItemCount()) {
                    final ClipData$Item item = clipData.getItemAt(i);
                    if (Build$VERSION.SDK_INT >= 31) {
                        Api31Impl.checkOtherMembers(i, item, consumer);
                    }
                    else {
                        checkOtherMembers(i, item, consumer);
                    }
                    CharSequence text;
                    if (b) {
                        text = item.getText();
                    }
                    else {
                        if (item.getText() != null) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Item text cannot contain value. Item position: ");
                            sb.append(i);
                            sb.append(". Text: ");
                            sb.append((Object)item.getText());
                            consumer.accept(sb.toString());
                        }
                        text = null;
                    }
                    Uri uri = null;
                    Label_0329: {
                        if (predicate2 == null) {
                            if (item.getUri() != null) {
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("Item URI is not allowed. Item position: ");
                                sb2.append(i);
                                sb2.append(". URI: ");
                                sb2.append(item.getUri());
                                consumer.accept(sb2.toString());
                            }
                        }
                        else {
                            if (item.getUri() == null || predicate2.test(item.getUri())) {
                                uri = item.getUri();
                                break Label_0329;
                            }
                            final StringBuilder sb3 = new StringBuilder();
                            sb3.append("Item URI is not allowed. Item position: ");
                            sb3.append(i);
                            sb3.append(". URI: ");
                            sb3.append(item.getUri());
                            consumer.accept(sb3.toString());
                        }
                        uri = null;
                    }
                    ClipData clipData3 = null;
                    Label_0391: {
                        if (text == null) {
                            clipData3 = clipData2;
                            if (uri == null) {
                                break Label_0391;
                            }
                        }
                        if (clipData2 == null) {
                            clipData3 = new ClipData(clipData.getDescription(), new ClipData$Item(text, (Intent)null, uri));
                        }
                        else {
                            clipData2.addItem(new ClipData$Item(text, (Intent)null, uri));
                            clipData3 = clipData2;
                        }
                    }
                    ++i;
                    clipData2 = clipData3;
                }
                if (clipData2 != null) {
                    intent2.setClipData(clipData2);
                }
            }
        }
        
        @RequiresApi(31)
        private static class Api31Impl
        {
            @DoNotInline
            static void checkOtherMembers(final int i, final ClipData$Item obj, final Consumer<String> consumer) {
                if (obj.getHtmlText() != null || obj.getIntent() != null || \u30078o8o\u3007.\u3007080(obj) != null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("ClipData item at position ");
                    sb.append(i);
                    sb.append(" contains htmlText, textLinks or intent: ");
                    sb.append(obj);
                    consumer.accept(sb.toString());
                }
            }
        }
    }
    
    @RequiresApi(29)
    private static class Api29Impl
    {
        @DoNotInline
        static String getIdentifier(final Intent intent) {
            return \u3007O8o08O.\u3007080(intent);
        }
        
        @DoNotInline
        static Intent setIdentifier(final Intent intent, final String s) {
            return OO0o\u3007\u3007.\u3007080(intent, s);
        }
    }
    
    public static final class Builder
    {
        private static final int HISTORY_STACK_FLAGS = 2112614400;
        private static final int RECEIVER_FLAGS = 2015363072;
        private boolean mAllowAnyComponent;
        private boolean mAllowClipDataText;
        private boolean mAllowIdentifier;
        private boolean mAllowSelector;
        private boolean mAllowSomeComponents;
        private boolean mAllowSourceBounds;
        private Predicate<String> mAllowedActions;
        private Predicate<String> mAllowedCategories;
        private Predicate<ClipData> mAllowedClipData;
        private Predicate<Uri> mAllowedClipDataUri;
        private Predicate<ComponentName> mAllowedComponents;
        private Predicate<Uri> mAllowedData;
        private Map<String, Predicate<Object>> mAllowedExtras;
        private int mAllowedFlags;
        private Predicate<String> mAllowedPackages;
        private Predicate<String> mAllowedTypes;
        
        public Builder() {
            this.mAllowedActions = new \u300700();
            this.mAllowedData = new O\u30078O8\u3007008();
            this.mAllowedTypes = new O8ooOoo\u3007();
            this.mAllowedCategories = new \u3007oOO8O8();
            this.mAllowedPackages = new \u30070000OOO();
            this.mAllowedComponents = new o\u3007\u30070\u3007();
            this.mAllowedExtras = new HashMap<String, Predicate<Object>>();
            this.mAllowClipDataText = false;
            this.mAllowedClipDataUri = new OOO\u3007O0();
            this.mAllowedClipData = new oo\u3007();
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowAction(@NonNull final Predicate<String> predicate) {
            Preconditions.checkNotNull(predicate);
            this.mAllowedActions = this.mAllowedActions.or(predicate);
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowAction(@NonNull final String obj) {
            Preconditions.checkNotNull(obj);
            Objects.requireNonNull(obj);
            this.allowAction(new \u3007O00(obj));
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowAnyComponent() {
            this.mAllowAnyComponent = true;
            this.mAllowedComponents = new \u30070\u3007O0088o();
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowCategory(@NonNull final Predicate<String> predicate) {
            Preconditions.checkNotNull(predicate);
            this.mAllowedCategories = this.mAllowedCategories.or(predicate);
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowCategory(@NonNull final String obj) {
            Preconditions.checkNotNull(obj);
            Objects.requireNonNull(obj);
            return this.allowCategory(new \u3007O00(obj));
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowClipData(@NonNull final Predicate<ClipData> predicate) {
            Preconditions.checkNotNull(predicate);
            this.mAllowedClipData = this.mAllowedClipData.or(predicate);
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowClipDataText() {
            this.mAllowClipDataText = true;
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowClipDataUri(@NonNull final Predicate<Uri> predicate) {
            Preconditions.checkNotNull(predicate);
            this.mAllowedClipDataUri = this.mAllowedClipDataUri.or(predicate);
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowClipDataUriWithAuthority(@NonNull final String s) {
            Preconditions.checkNotNull(s);
            return this.allowClipDataUri(new \u3007\u3007808\u3007(s));
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowComponent(@NonNull final ComponentName obj) {
            Preconditions.checkNotNull(obj);
            Objects.requireNonNull(obj);
            return this.allowComponent(new \u3007oo\u3007(obj));
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowComponent(@NonNull final Predicate<ComponentName> predicate) {
            Preconditions.checkNotNull(predicate);
            this.mAllowSomeComponents = true;
            this.mAllowedComponents = this.mAllowedComponents.or(predicate);
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowComponentWithPackage(@NonNull final String s) {
            Preconditions.checkNotNull(s);
            return this.allowComponent(new o800o8O(s));
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowData(@NonNull final Predicate<Uri> predicate) {
            Preconditions.checkNotNull(predicate);
            this.mAllowedData = this.mAllowedData.or(predicate);
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowDataWithAuthority(@NonNull final String s) {
            Preconditions.checkNotNull(s);
            this.allowData(new \u3007O888o0o(s));
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowExtra(@NonNull final String s, @NonNull final Predicate<Object> predicate) {
            Preconditions.checkNotNull(s);
            Preconditions.checkNotNull(predicate);
            Predicate predicate2;
            if ((predicate2 = this.mAllowedExtras.get(s)) == null) {
                predicate2 = new Oooo8o0\u3007();
            }
            this.mAllowedExtras.put(s, predicate2.or(predicate));
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowExtra(@NonNull final String s, @NonNull final Class<?> clazz) {
            return this.allowExtra(s, clazz, new o\u3007O8\u3007\u3007o());
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public <T> Builder allowExtra(@NonNull final String s, @NonNull final Class<T> clazz, @NonNull final Predicate<T> predicate) {
            Preconditions.checkNotNull(s);
            Preconditions.checkNotNull(clazz);
            Preconditions.checkNotNull(predicate);
            return this.allowExtra(s, new OoO8(clazz, predicate));
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowExtraOutput(@NonNull final Predicate<Uri> predicate) {
            this.allowExtra("output", Uri.class, predicate);
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowExtraOutput(@NonNull final String s) {
            this.allowExtra("output", Uri.class, new oo88o8O(s));
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowExtraStream(@NonNull final Predicate<Uri> predicate) {
            this.allowExtra("android.intent.extra.STREAM", Uri.class, predicate);
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowExtraStreamUriWithAuthority(@NonNull final String s) {
            Preconditions.checkNotNull(s);
            this.allowExtra("android.intent.extra.STREAM", Uri.class, new \u3007\u30078O0\u30078(s));
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowFlags(final int n) {
            this.mAllowedFlags |= n;
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowHistoryStackFlags() {
            this.mAllowedFlags |= 0x7DEBF000;
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowIdentifier() {
            this.mAllowIdentifier = true;
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowPackage(@NonNull final Predicate<String> predicate) {
            Preconditions.checkNotNull(predicate);
            this.mAllowedPackages = this.mAllowedPackages.or(predicate);
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowPackage(@NonNull final String obj) {
            Preconditions.checkNotNull(obj);
            Objects.requireNonNull(obj);
            return this.allowPackage(new \u3007O00(obj));
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowReceiverFlags() {
            this.mAllowedFlags |= 0x78200000;
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowSelector() {
            this.mAllowSelector = true;
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowSourceBounds() {
            this.mAllowSourceBounds = true;
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowType(@NonNull final Predicate<String> predicate) {
            Preconditions.checkNotNull(predicate);
            this.mAllowedTypes = this.mAllowedTypes.or(predicate);
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder allowType(@NonNull final String obj) {
            Preconditions.checkNotNull(obj);
            Objects.requireNonNull(obj);
            return this.allowType(new \u3007O00(obj));
        }
        
        @SuppressLint({ "SyntheticAccessor" })
        @NonNull
        public IntentSanitizer build() {
            final boolean mAllowAnyComponent = this.mAllowAnyComponent;
            if ((!mAllowAnyComponent || !this.mAllowSomeComponents) && (mAllowAnyComponent || this.mAllowSomeComponents)) {
                final IntentSanitizer intentSanitizer = new IntentSanitizer(null);
                intentSanitizer.mAllowedFlags = this.mAllowedFlags;
                intentSanitizer.mAllowedActions = this.mAllowedActions;
                intentSanitizer.mAllowedData = this.mAllowedData;
                intentSanitizer.mAllowedTypes = this.mAllowedTypes;
                intentSanitizer.mAllowedCategories = this.mAllowedCategories;
                intentSanitizer.mAllowedPackages = this.mAllowedPackages;
                intentSanitizer.mAllowAnyComponent = this.mAllowAnyComponent;
                intentSanitizer.mAllowedComponents = this.mAllowedComponents;
                intentSanitizer.mAllowedExtras = this.mAllowedExtras;
                intentSanitizer.mAllowClipDataText = this.mAllowClipDataText;
                intentSanitizer.mAllowedClipDataUri = this.mAllowedClipDataUri;
                intentSanitizer.mAllowedClipData = this.mAllowedClipData;
                intentSanitizer.mAllowIdentifier = this.mAllowIdentifier;
                intentSanitizer.mAllowSelector = this.mAllowSelector;
                intentSanitizer.mAllowSourceBounds = this.mAllowSourceBounds;
                return intentSanitizer;
            }
            throw new SecurityException("You must call either allowAnyComponent or one or more of the allowComponent methods; but not both.");
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import android.os.RemoteException;
import androidx.annotation.RestrictTo;
import androidx.annotation.NonNull;
import androidx.core.app.unusedapprestrictions.IUnusedAppRestrictionsBackportCallback;

public class UnusedAppRestrictionsBackportCallback
{
    private IUnusedAppRestrictionsBackportCallback mCallback;
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public UnusedAppRestrictionsBackportCallback(@NonNull final IUnusedAppRestrictionsBackportCallback mCallback) {
        this.mCallback = mCallback;
    }
    
    public void onResult(final boolean b, final boolean b2) throws RemoteException {
        this.mCallback.onIsPermissionRevocationEnabledForAppResult(b, b2);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import androidx.annotation.RestrictTo;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.view.WindowManager;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pManager;
import android.app.WallpaperManager;
import android.os.Vibrator;
import android.hardware.usb.UsbManager;
import android.app.UiModeManager;
import android.view.textservice.TextServicesManager;
import android.telephony.TelephonyManager;
import android.os.storage.StorageManager;
import android.hardware.SensorManager;
import android.app.SearchManager;
import android.os.PowerManager;
import android.app.NotificationManager;
import android.nfc.NfcManager;
import android.location.LocationManager;
import android.view.LayoutInflater;
import android.app.KeyguardManager;
import android.view.inputmethod.InputMethodManager;
import android.os.DropBoxManager;
import android.app.DownloadManager;
import android.app.admin.DevicePolicyManager;
import android.net.ConnectivityManager;
import android.content.ClipboardManager;
import android.media.AudioManager;
import android.app.AlarmManager;
import android.app.ActivityManager;
import android.accounts.AccountManager;
import android.view.accessibility.AccessibilityManager;
import android.net.nsd.NsdManager;
import android.media.MediaRouter;
import android.hardware.input.InputManager;
import android.os.UserManager;
import android.hardware.display.DisplayManager;
import android.bluetooth.BluetoothManager;
import android.print.PrintManager;
import android.hardware.ConsumerIrManager;
import android.view.accessibility.CaptioningManager;
import android.app.AppOpsManager;
import android.media.tv.TvInputManager;
import android.telecom.TelecomManager;
import android.content.RestrictionsManager;
import android.media.session.MediaSessionManager;
import android.media.projection.MediaProjectionManager;
import android.content.pm.LauncherApps;
import android.app.job.JobScheduler;
import android.hardware.camera2.CameraManager;
import android.os.BatteryManager;
import android.appwidget.AppWidgetManager;
import android.app.usage.UsageStatsManager;
import android.telephony.SubscriptionManager;
import java.util.HashMap;
import android.content.ComponentName;
import androidx.appcompat.widget.oo88o8O;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.os.Bundle;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import androidx.core.os.ExecutorCompat;
import android.os.Handler;
import java.util.concurrent.Executor;
import android.graphics.drawable.Drawable;
import androidx.annotation.DrawableRes;
import androidx.core.content.res.ResourcesCompat;
import android.content.res.ColorStateList;
import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import java.io.File;
import androidx.annotation.Nullable;
import android.os.Build$VERSION;
import androidx.annotation.OptIn;
import android.os.Process;
import androidx.core.app.NotificationManagerCompat;
import android.text.TextUtils;
import androidx.core.os.BuildCompat;
import androidx.core.util.ObjectsCompat;
import androidx.annotation.NonNull;
import android.content.Context;
import android.util.TypedValue;
import android.annotation.SuppressLint;

@SuppressLint({ "PrivateConstructorForUtilityClass" })
public class ContextCompat
{
    private static final String DYNAMIC_RECEIVER_NOT_EXPORTED_PERMISSION_SUFFIX = ".DYNAMIC_RECEIVER_NOT_EXPORTED_PERMISSION";
    public static final int RECEIVER_EXPORTED = 2;
    public static final int RECEIVER_NOT_EXPORTED = 4;
    public static final int RECEIVER_VISIBLE_TO_INSTANT_APPS = 1;
    private static final String TAG = "ContextCompat";
    private static final Object sLock;
    private static final Object sSync;
    private static TypedValue sTempValue;
    
    static {
        sLock = new Object();
        sSync = new Object();
    }
    
    protected ContextCompat() {
    }
    
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    public static int checkSelfPermission(@NonNull final Context context, @NonNull final String s) {
        ObjectsCompat.requireNonNull(s, "permission must be non-null");
        if (!BuildCompat.isAtLeastT() && TextUtils.equals((CharSequence)"android.permission.POST_NOTIFICATIONS", (CharSequence)s)) {
            int n;
            if (NotificationManagerCompat.from(context).areNotificationsEnabled()) {
                n = 0;
            }
            else {
                n = -1;
            }
            return n;
        }
        return context.checkPermission(s, Process.myPid(), Process.myUid());
    }
    
    @Nullable
    public static Context createDeviceProtectedStorageContext(@NonNull final Context context) {
        if (Build$VERSION.SDK_INT >= 24) {
            return Api24Impl.createDeviceProtectedStorageContext(context);
        }
        return null;
    }
    
    private static File createFilesDir(final File file) {
        synchronized (ContextCompat.sSync) {
            if (!file.exists()) {
                if (file.mkdirs()) {
                    return file;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to create files subdir ");
                sb.append(file.getPath());
            }
            return file;
        }
    }
    
    @Nullable
    public static String getAttributionTag(@NonNull final Context context) {
        if (Build$VERSION.SDK_INT >= 30) {
            return Api30Impl.getAttributionTag(context);
        }
        return null;
    }
    
    @NonNull
    public static File getCodeCacheDir(@NonNull final Context context) {
        return Api21Impl.getCodeCacheDir(context);
    }
    
    @ColorInt
    public static int getColor(@NonNull final Context context, @ColorRes final int n) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getColor(context, n);
        }
        return context.getResources().getColor(n);
    }
    
    @Nullable
    public static ColorStateList getColorStateList(@NonNull final Context context, @ColorRes final int n) {
        return ResourcesCompat.getColorStateList(context.getResources(), n, context.getTheme());
    }
    
    @Nullable
    public static File getDataDir(@NonNull final Context context) {
        if (Build$VERSION.SDK_INT >= 24) {
            return Api24Impl.getDataDir(context);
        }
        final String dataDir = context.getApplicationInfo().dataDir;
        File file;
        if (dataDir != null) {
            file = new File(dataDir);
        }
        else {
            file = null;
        }
        return file;
    }
    
    @Nullable
    public static Drawable getDrawable(@NonNull final Context context, @DrawableRes final int n) {
        return Api21Impl.getDrawable(context, n);
    }
    
    @NonNull
    public static File[] getExternalCacheDirs(@NonNull final Context context) {
        return Api19Impl.getExternalCacheDirs(context);
    }
    
    @NonNull
    public static File[] getExternalFilesDirs(@NonNull final Context context, @Nullable final String s) {
        return Api19Impl.getExternalFilesDirs(context, s);
    }
    
    @NonNull
    public static Executor getMainExecutor(@NonNull final Context context) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getMainExecutor(context);
        }
        return ExecutorCompat.create(new Handler(context.getMainLooper()));
    }
    
    @Nullable
    public static File getNoBackupFilesDir(@NonNull final Context context) {
        return Api21Impl.getNoBackupFilesDir(context);
    }
    
    @NonNull
    public static File[] getObbDirs(@NonNull final Context context) {
        return Api19Impl.getObbDirs(context);
    }
    
    @Nullable
    public static <T> T getSystemService(@NonNull final Context context, @NonNull final Class<T> clazz) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getSystemService(context, clazz);
        }
        final String systemServiceName = getSystemServiceName(context, clazz);
        Object systemService;
        if (systemServiceName != null) {
            systemService = context.getSystemService(systemServiceName);
        }
        else {
            systemService = null;
        }
        return (T)systemService;
    }
    
    @Nullable
    public static String getSystemServiceName(@NonNull final Context context, @NonNull final Class<?> key) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getSystemServiceName(context, key);
        }
        return LegacyServiceMapHolder.SERVICES.get(key);
    }
    
    public static boolean isDeviceProtectedStorage(@NonNull final Context context) {
        return Build$VERSION.SDK_INT >= 24 && Api24Impl.isDeviceProtectedStorage(context);
    }
    
    static String obtainAndCheckReceiverPermission(final Context context) {
        final StringBuilder sb = new StringBuilder();
        sb.append(context.getPackageName());
        sb.append(".DYNAMIC_RECEIVER_NOT_EXPORTED_PERMISSION");
        final String string = sb.toString();
        if (PermissionChecker.checkSelfPermission(context, string) == 0) {
            return string;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Permission ");
        sb2.append(string);
        sb2.append(" is required by your application to receive broadcasts, please add it to your manifest");
        throw new RuntimeException(sb2.toString());
    }
    
    @Nullable
    public static Intent registerReceiver(@NonNull final Context context, @Nullable final BroadcastReceiver broadcastReceiver, @NonNull final IntentFilter intentFilter, final int n) {
        return registerReceiver(context, broadcastReceiver, intentFilter, null, null, n);
    }
    
    @Nullable
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    public static Intent registerReceiver(@NonNull final Context context, @Nullable final BroadcastReceiver broadcastReceiver, @NonNull final IntentFilter intentFilter, @Nullable final String s, @Nullable final Handler handler, int n) {
        final int n2 = n & 0x1;
        if (n2 != 0 && (n & 0x4) != 0x0) {
            throw new IllegalArgumentException("Cannot specify both RECEIVER_VISIBLE_TO_INSTANT_APPS and RECEIVER_NOT_EXPORTED");
        }
        int n3 = n;
        if (n2 != 0) {
            n3 = (n | 0x2);
        }
        n = (n3 & 0x2);
        if (n == 0 && (n3 & 0x4) == 0x0) {
            throw new IllegalArgumentException("One of either RECEIVER_EXPORTED or RECEIVER_NOT_EXPORTED is required");
        }
        if (n != 0 && (n3 & 0x4) != 0x0) {
            throw new IllegalArgumentException("Cannot specify both RECEIVER_EXPORTED and RECEIVER_NOT_EXPORTED");
        }
        if (BuildCompat.isAtLeastT()) {
            return Api33Impl.registerReceiver(context, broadcastReceiver, intentFilter, s, handler, n3);
        }
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.registerReceiver(context, broadcastReceiver, intentFilter, s, handler, n3);
        }
        if ((n3 & 0x4) != 0x0 && s == null) {
            return context.registerReceiver(broadcastReceiver, intentFilter, obtainAndCheckReceiverPermission(context), handler);
        }
        return context.registerReceiver(broadcastReceiver, intentFilter, s, handler);
    }
    
    public static boolean startActivities(@NonNull final Context context, @NonNull final Intent[] array) {
        return startActivities(context, array, null);
    }
    
    public static boolean startActivities(@NonNull final Context context, @NonNull final Intent[] array, @Nullable final Bundle bundle) {
        Api16Impl.startActivities(context, array, bundle);
        return true;
    }
    
    public static void startActivity(@NonNull final Context context, @NonNull final Intent intent, @Nullable final Bundle bundle) {
        Api16Impl.startActivity(context, intent, bundle);
    }
    
    public static void startForegroundService(@NonNull final Context context, @NonNull final Intent intent) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.startForegroundService(context, intent);
        }
        else {
            context.startService(intent);
        }
    }
    
    @RequiresApi(16)
    static class Api16Impl
    {
        private Api16Impl() {
        }
        
        @DoNotInline
        static void startActivities(final Context context, final Intent[] array, final Bundle bundle) {
            context.startActivities(array, bundle);
        }
        
        @DoNotInline
        static void startActivity(final Context context, final Intent intent, final Bundle bundle) {
            context.startActivity(intent, bundle);
        }
    }
    
    @RequiresApi(19)
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        @DoNotInline
        static File[] getExternalCacheDirs(final Context context) {
            return context.getExternalCacheDirs();
        }
        
        @DoNotInline
        static File[] getExternalFilesDirs(final Context context, final String s) {
            return context.getExternalFilesDirs(s);
        }
        
        @DoNotInline
        static File[] getObbDirs(final Context context) {
            return context.getObbDirs();
        }
    }
    
    @RequiresApi(21)
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        @DoNotInline
        static File getCodeCacheDir(final Context context) {
            return context.getCodeCacheDir();
        }
        
        @DoNotInline
        static Drawable getDrawable(final Context context, final int n) {
            return context.getDrawable(n);
        }
        
        @DoNotInline
        static File getNoBackupFilesDir(final Context context) {
            return context.getNoBackupFilesDir();
        }
    }
    
    @RequiresApi(23)
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        @DoNotInline
        static int getColor(final Context context, final int n) {
            return \u3007o00\u3007\u3007Oo.\u3007080(context, n);
        }
        
        @DoNotInline
        static <T> T getSystemService(final Context context, final Class<T> clazz) {
            return (T)oo88o8O.\u3007080(context, (Class)clazz);
        }
        
        @DoNotInline
        static String getSystemServiceName(final Context context, final Class<?> clazz) {
            return \u3007080.\u3007080(context, (Class)clazz);
        }
    }
    
    @RequiresApi(24)
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        @DoNotInline
        static Context createDeviceProtectedStorageContext(final Context context) {
            return O8.\u3007080(context);
        }
        
        @DoNotInline
        static File getDataDir(final Context context) {
            return Oo08.\u3007080(context);
        }
        
        @DoNotInline
        static boolean isDeviceProtectedStorage(final Context context) {
            return \u3007o\u3007.\u3007080(context);
        }
    }
    
    @RequiresApi(26)
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        @DoNotInline
        static Intent registerReceiver(final Context context, @Nullable final BroadcastReceiver broadcastReceiver, final IntentFilter intentFilter, final String s, final Handler handler, final int n) {
            if ((n & 0x4) != 0x0 && s == null) {
                return context.registerReceiver(broadcastReceiver, intentFilter, ContextCompat.obtainAndCheckReceiverPermission(context), handler);
            }
            return o\u30070.\u3007080(context, broadcastReceiver, intentFilter, s, handler, n & 0x1);
        }
        
        @DoNotInline
        static ComponentName startForegroundService(final Context context, final Intent intent) {
            return \u3007\u3007888.\u3007080(context, intent);
        }
    }
    
    @RequiresApi(28)
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        @DoNotInline
        static Executor getMainExecutor(final Context context) {
            return oO80.\u3007080(context);
        }
    }
    
    @RequiresApi(30)
    static class Api30Impl
    {
        private Api30Impl() {
        }
        
        @DoNotInline
        static String getAttributionTag(final Context context) {
            return androidx.camera.core.impl.utils.O8.\u3007080(context);
        }
    }
    
    @RequiresApi(33)
    static class Api33Impl
    {
        private Api33Impl() {
        }
        
        @DoNotInline
        static Intent registerReceiver(final Context context, @Nullable final BroadcastReceiver broadcastReceiver, final IntentFilter intentFilter, final String s, final Handler handler, final int n) {
            return o\u30070.\u3007080(context, broadcastReceiver, intentFilter, s, handler, n);
        }
    }
    
    private static final class LegacyServiceMapHolder
    {
        static final HashMap<Class<?>, String> SERVICES;
        
        static {
            final HashMap<Class<?>, String> hashMap = SERVICES = new HashMap<Class<?>, String>();
            if (Build$VERSION.SDK_INT >= 22) {
                hashMap.put(SubscriptionManager.class, "telephony_subscription_service");
                hashMap.put(UsageStatsManager.class, "usagestats");
            }
            hashMap.put(AppWidgetManager.class, "appwidget");
            hashMap.put(BatteryManager.class, "batterymanager");
            hashMap.put(CameraManager.class, "camera");
            hashMap.put(JobScheduler.class, "jobscheduler");
            hashMap.put(LauncherApps.class, "launcherapps");
            hashMap.put(MediaProjectionManager.class, "media_projection");
            hashMap.put(MediaSessionManager.class, "media_session");
            hashMap.put(RestrictionsManager.class, "restrictions");
            hashMap.put(TelecomManager.class, "telecom");
            hashMap.put(TvInputManager.class, "tv_input");
            hashMap.put(AppOpsManager.class, "appops");
            hashMap.put(CaptioningManager.class, "captioning");
            hashMap.put(ConsumerIrManager.class, "consumer_ir");
            hashMap.put(PrintManager.class, "print");
            hashMap.put(BluetoothManager.class, "bluetooth");
            hashMap.put(DisplayManager.class, "display");
            hashMap.put(UserManager.class, "user");
            hashMap.put(InputManager.class, "input");
            hashMap.put(MediaRouter.class, "media_router");
            hashMap.put(NsdManager.class, "servicediscovery");
            hashMap.put(AccessibilityManager.class, "accessibility");
            hashMap.put(AccountManager.class, "account");
            hashMap.put(ActivityManager.class, "activity");
            hashMap.put(AlarmManager.class, "alarm");
            hashMap.put(AudioManager.class, "audio");
            hashMap.put(ClipboardManager.class, "clipboard");
            hashMap.put(ConnectivityManager.class, "connectivity");
            hashMap.put(DevicePolicyManager.class, "device_policy");
            hashMap.put(DownloadManager.class, "download");
            hashMap.put(DropBoxManager.class, "dropbox");
            hashMap.put(InputMethodManager.class, "input_method");
            hashMap.put(KeyguardManager.class, "keyguard");
            hashMap.put(LayoutInflater.class, "layout_inflater");
            hashMap.put(LocationManager.class, "location");
            hashMap.put(NfcManager.class, "nfc");
            hashMap.put(NotificationManager.class, "notification");
            hashMap.put(PowerManager.class, "power");
            hashMap.put(SearchManager.class, "search");
            hashMap.put(SensorManager.class, "sensor");
            hashMap.put(StorageManager.class, "storage");
            hashMap.put(TelephonyManager.class, "phone");
            hashMap.put(TextServicesManager.class, "textservices");
            hashMap.put(UiModeManager.class, "uimode");
            hashMap.put(UsbManager.class, "usb");
            hashMap.put(Vibrator.class, "vibrator");
            hashMap.put(WallpaperManager.class, "wallpaper");
            hashMap.put(WifiP2pManager.class, "wifip2p");
            hashMap.put(WifiManager.class, "wifi");
            hashMap.put(WindowManager.class, "window");
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface RegisterReceiverFlags {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import android.os.Build$VERSION;
import androidx.core.util.Preconditions;
import androidx.annotation.NonNull;
import android.content.LocusId;

public final class LocusIdCompat
{
    private final String mId;
    private final LocusId mWrapped;
    
    public LocusIdCompat(@NonNull final String s) {
        this.mId = Preconditions.checkStringNotEmpty(s, (Object)"id cannot be empty");
        if (Build$VERSION.SDK_INT >= 29) {
            this.mWrapped = Api29Impl.create(s);
        }
        else {
            this.mWrapped = null;
        }
    }
    
    @NonNull
    private String getSanitizedId() {
        final int length = this.mId.length();
        final StringBuilder sb = new StringBuilder();
        sb.append(length);
        sb.append("_chars");
        return sb.toString();
    }
    
    @NonNull
    @RequiresApi(29)
    public static LocusIdCompat toLocusIdCompat(@NonNull final LocusId locusId) {
        Preconditions.checkNotNull(locusId, "locusId cannot be null");
        return new LocusIdCompat(Preconditions.checkStringNotEmpty(Api29Impl.getId(locusId), (Object)"id cannot be empty"));
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (LocusIdCompat.class != o.getClass()) {
            return false;
        }
        final LocusIdCompat locusIdCompat = (LocusIdCompat)o;
        final String mId = this.mId;
        if (mId == null) {
            if (locusIdCompat.mId != null) {
                b = false;
            }
            return b;
        }
        return mId.equals(locusIdCompat.mId);
    }
    
    @NonNull
    public String getId() {
        return this.mId;
    }
    
    @Override
    public int hashCode() {
        final String mId = this.mId;
        int hashCode;
        if (mId == null) {
            hashCode = 0;
        }
        else {
            hashCode = mId.hashCode();
        }
        return 31 + hashCode;
    }
    
    @NonNull
    @RequiresApi(29)
    public LocusId toLocusId() {
        return this.mWrapped;
    }
    
    @NonNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("LocusIdCompat[");
        sb.append(this.getSanitizedId());
        sb.append("]");
        return sb.toString();
    }
    
    @RequiresApi(29)
    private static class Api29Impl
    {
        @NonNull
        static LocusId create(@NonNull final String s) {
            return new LocusId(s);
        }
        
        @NonNull
        static String getId(@NonNull final LocusId locusId) {
            return O8\u3007o.\u3007080(locusId);
        }
    }
}

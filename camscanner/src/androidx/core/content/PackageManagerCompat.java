// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.annotation.RequiresApi;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import androidx.core.os.UserManagerCompat;
import androidx.concurrent.futures.ResolvableFuture;
import com.google.common.util.concurrent.ListenableFuture;
import android.content.Context;
import androidx.annotation.Nullable;
import java.util.Iterator;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.content.Intent;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.content.pm.PackageManager;
import androidx.annotation.RestrictTo;
import android.annotation.SuppressLint;

public final class PackageManagerCompat
{
    @SuppressLint({ "ActionValue" })
    public static final String ACTION_PERMISSION_REVOCATION_SETTINGS = "android.intent.action.AUTO_REVOKE_PERMISSIONS";
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static final String LOG_TAG = "PackageManagerCompat";
    
    private PackageManagerCompat() {
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static boolean areUnusedAppRestrictionsAvailable(@NonNull final PackageManager packageManager) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        final boolean b = true;
        final boolean b2 = sdk_INT >= 30;
        final boolean b3 = sdk_INT >= 23 && sdk_INT < 30;
        final boolean b4 = getPermissionRevocationVerifierApp(packageManager) != null;
        boolean b5 = b;
        if (!b2) {
            b5 = (b3 && b4 && b);
        }
        return b5;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static String getPermissionRevocationVerifierApp(@NonNull final PackageManager packageManager) {
        final Intent intent = new Intent("android.intent.action.AUTO_REVOKE_PERMISSIONS");
        String s = null;
        final Iterator iterator = packageManager.queryIntentActivities(intent.setData(Uri.fromParts("package", "com.example", (String)null)), 0).iterator();
        while (iterator.hasNext()) {
            final String packageName = ((ResolveInfo)iterator.next()).activityInfo.packageName;
            if (packageManager.checkPermission("android.permission.PACKAGE_VERIFICATION_AGENT", packageName) != 0) {
                continue;
            }
            if (s != null) {
                return s;
            }
            s = packageName;
        }
        return s;
    }
    
    @NonNull
    public static ListenableFuture<Integer> getUnusedAppRestrictionsStatus(@NonNull final Context context) {
        final ResolvableFuture<Object> create = ResolvableFuture.create();
        final boolean userUnlocked = UserManagerCompat.isUserUnlocked(context);
        final Integer value = 0;
        if (!userUnlocked) {
            create.set(value);
            return (ListenableFuture<Integer>)create;
        }
        if (!areUnusedAppRestrictionsAvailable(context.getPackageManager())) {
            create.set(1);
            return (ListenableFuture<Integer>)create;
        }
        final int targetSdkVersion = context.getApplicationInfo().targetSdkVersion;
        if (targetSdkVersion < 30) {
            create.set(value);
            return (ListenableFuture<Integer>)create;
        }
        final int sdk_INT = Build$VERSION.SDK_INT;
        int n = 4;
        if (sdk_INT >= 31) {
            if (Api30Impl.areUnusedAppRestrictionsEnabled(context)) {
                if (targetSdkVersion >= 31) {
                    n = 5;
                }
                create.set(n);
            }
            else {
                create.set(2);
            }
            return (ListenableFuture<Integer>)create;
        }
        if (sdk_INT == 30) {
            if (!Api30Impl.areUnusedAppRestrictionsEnabled(context)) {
                n = 2;
            }
            create.set(n);
            return (ListenableFuture<Integer>)create;
        }
        final UnusedAppRestrictionsBackportServiceConnection unusedAppRestrictionsBackportServiceConnection = new UnusedAppRestrictionsBackportServiceConnection(context);
        create.addListener(new \u300700\u30078(unusedAppRestrictionsBackportServiceConnection), Executors.newSingleThreadExecutor());
        unusedAppRestrictionsBackportServiceConnection.connectAndFetchResult((ResolvableFuture<Integer>)create);
        return (ListenableFuture<Integer>)create;
    }
    
    @RequiresApi(30)
    private static class Api30Impl
    {
        static boolean areUnusedAppRestrictionsEnabled(@NonNull final Context context) {
            return \u3007o.\u3007080(context.getPackageManager()) ^ true;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface UnusedAppRestrictionsStatus {
    }
}

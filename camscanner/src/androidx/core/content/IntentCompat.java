// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import androidx.core.util.Preconditions;
import android.net.Uri;
import android.os.Build$VERSION;
import android.content.Intent;
import androidx.annotation.NonNull;
import android.content.Context;
import android.annotation.SuppressLint;

public final class IntentCompat
{
    @SuppressLint({ "ActionValue" })
    public static final String ACTION_CREATE_REMINDER = "android.intent.action.CREATE_REMINDER";
    public static final String CATEGORY_LEANBACK_LAUNCHER = "android.intent.category.LEANBACK_LAUNCHER";
    public static final String EXTRA_HTML_TEXT = "android.intent.extra.HTML_TEXT";
    public static final String EXTRA_START_PLAYBACK = "android.intent.extra.START_PLAYBACK";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_TIME = "android.intent.extra.TIME";
    
    private IntentCompat() {
    }
    
    @NonNull
    public static Intent createManageUnusedAppRestrictionsIntent(@NonNull final Context context, @NonNull final String s) {
        if (!PackageManagerCompat.areUnusedAppRestrictionsAvailable(context.getPackageManager())) {
            throw new UnsupportedOperationException("Unused App Restriction features are not available on this device");
        }
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 31) {
            return new Intent("android.settings.APPLICATION_DETAILS_SETTINGS").setData(Uri.fromParts("package", s, (String)null));
        }
        final Intent setData = new Intent("android.intent.action.AUTO_REVOKE_PERMISSIONS").setData(Uri.fromParts("package", s, (String)null));
        if (sdk_INT >= 30) {
            return setData;
        }
        return setData.setPackage((String)Preconditions.checkNotNull(PackageManagerCompat.getPermissionRevocationVerifierApp(context.getPackageManager())));
    }
    
    @NonNull
    public static Intent makeMainSelectorActivity(@NonNull final String s, @NonNull final String s2) {
        return Api15Impl.makeMainSelectorActivity(s, s2);
    }
    
    @RequiresApi(15)
    static class Api15Impl
    {
        private Api15Impl() {
        }
        
        @DoNotInline
        static Intent makeMainSelectorActivity(final String s, final String s2) {
            return Intent.makeMainSelectorActivity(s, s2);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import androidx.annotation.AttrRes;
import android.util.AttributeSet;
import kotlin.Unit;
import android.content.res.TypedArray;
import kotlin.jvm.functions.Function1;
import androidx.annotation.StyleRes;
import org.jetbrains.annotations.NotNull;
import kotlin.jvm.internal.Intrinsics;
import android.content.Context;
import kotlin.Metadata;

@Metadata
public final class ContextKt
{
    public static final void withStyledAttributes(@NotNull final Context context, @StyleRes final int n, @NotNull final int[] array, @NotNull final Function1<? super TypedArray, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)context, "<this>");
        Intrinsics.checkNotNullParameter((Object)array, "attrs");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(n, array);
        Intrinsics.checkNotNullExpressionValue((Object)obtainStyledAttributes, "obtainStyledAttributes(resourceId, attrs)");
        function1.invoke((Object)obtainStyledAttributes);
        obtainStyledAttributes.recycle();
    }
    
    public static final void withStyledAttributes(@NotNull final Context context, final AttributeSet set, @NotNull final int[] array, @AttrRes final int n, @StyleRes final int n2, @NotNull final Function1<? super TypedArray, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)context, "<this>");
        Intrinsics.checkNotNullParameter((Object)array, "attrs");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, array, n, n2);
        Intrinsics.checkNotNullExpressionValue((Object)obtainStyledAttributes, "obtainStyledAttributes(s\u2026efStyleAttr, defStyleRes)");
        function1.invoke((Object)obtainStyledAttributes);
        obtainStyledAttributes.recycle();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.content;

import android.net.Uri;
import androidx.core.util.Predicate;
import androidx.annotation.NonNull;
import android.content.UriMatcher;

public class UriMatcherCompat
{
    private UriMatcherCompat() {
    }
    
    @NonNull
    public static Predicate<Uri> asPredicate(@NonNull final UriMatcher uriMatcher) {
        return new o0ooO(uriMatcher);
    }
}

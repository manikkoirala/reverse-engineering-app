// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import org.jetbrains.annotations.NotNull;
import androidx.annotation.RequiresApi;
import kotlin.jvm.internal.Intrinsics;
import android.util.Half;
import android.annotation.SuppressLint;
import kotlin.Metadata;

@Metadata
@SuppressLint({ "ClassVerificationFailure" })
public final class HalfKt
{
    @RequiresApi(26)
    @NotNull
    public static final Half toHalf(final double n) {
        final Half \u3007080 = \u3007o\u3007.\u3007080((float)n);
        Intrinsics.checkNotNullExpressionValue((Object)\u3007080, "valueOf(this)");
        return \u3007080;
    }
    
    @RequiresApi(26)
    @NotNull
    public static final Half toHalf(final float n) {
        final Half \u3007080 = \u3007o\u3007.\u3007080(n);
        Intrinsics.checkNotNullExpressionValue((Object)\u3007080, "valueOf(this)");
        return \u3007080;
    }
    
    @RequiresApi(26)
    @NotNull
    public static final Half toHalf(@NotNull final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "<this>");
        final Half \u3007080 = androidx.core.util.\u3007080.\u3007080(s);
        Intrinsics.checkNotNullExpressionValue((Object)\u3007080, "valueOf(this)");
        return \u3007080;
    }
    
    @RequiresApi(26)
    @NotNull
    public static final Half toHalf(final short n) {
        final Half \u3007080 = \u3007o00\u3007\u3007Oo.\u3007080(n);
        Intrinsics.checkNotNullExpressionValue((Object)\u3007080, "valueOf(this)");
        return \u3007080;
    }
}

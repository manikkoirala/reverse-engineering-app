// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.Metadata;

@Metadata
public final class RunnableKt
{
    @NotNull
    public static final Runnable asRunnable(@NotNull final Continuation<? super Unit> continuation) {
        Intrinsics.checkNotNullParameter((Object)continuation, "<this>");
        return new ContinuationRunnable(continuation);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import androidx.annotation.NonNull;
import android.util.SizeF;

public final class SizeFCompat
{
    private final float mHeight;
    private final float mWidth;
    
    public SizeFCompat(final float n, final float n2) {
        this.mWidth = Preconditions.checkArgumentFinite(n, "width");
        this.mHeight = Preconditions.checkArgumentFinite(n2, "height");
    }
    
    @NonNull
    @RequiresApi(21)
    public static SizeFCompat toSizeFCompat(@NonNull final SizeF sizeF) {
        return Api21Impl.toSizeFCompat(sizeF);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof SizeFCompat)) {
            return false;
        }
        final SizeFCompat sizeFCompat = (SizeFCompat)o;
        if (sizeFCompat.mWidth != this.mWidth || sizeFCompat.mHeight != this.mHeight) {
            b = false;
        }
        return b;
    }
    
    public float getHeight() {
        return this.mHeight;
    }
    
    public float getWidth() {
        return this.mWidth;
    }
    
    @Override
    public int hashCode() {
        return Float.floatToIntBits(this.mWidth) ^ Float.floatToIntBits(this.mHeight);
    }
    
    @NonNull
    @RequiresApi(21)
    public SizeF toSizeF() {
        return Api21Impl.toSizeF(this);
    }
    
    @NonNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.mWidth);
        sb.append("x");
        sb.append(this.mHeight);
        return sb.toString();
    }
    
    @RequiresApi(21)
    private static final class Api21Impl
    {
        @DoNotInline
        @NonNull
        static SizeF toSizeF(@NonNull final SizeFCompat sizeFCompat) {
            Preconditions.checkNotNull(sizeFCompat);
            return new SizeF(sizeFCompat.getWidth(), sizeFCompat.getHeight());
        }
        
        @DoNotInline
        @NonNull
        static SizeFCompat toSizeFCompat(@NonNull final SizeF sizeF) {
            Preconditions.checkNotNull(sizeF);
            return new SizeFCompat(sizeF.getWidth(), sizeF.getHeight());
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

public interface Supplier<T>
{
    T get();
}

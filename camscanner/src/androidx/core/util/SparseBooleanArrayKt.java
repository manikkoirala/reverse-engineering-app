// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import kotlin.collections.BooleanIterator;
import kotlin.collections.IntIterator;
import kotlin.jvm.functions.Function0;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.util.SparseBooleanArray;
import kotlin.Metadata;

@Metadata
public final class SparseBooleanArrayKt
{
    public static final boolean contains(@NotNull final SparseBooleanArray sparseBooleanArray, final int n) {
        Intrinsics.checkNotNullParameter((Object)sparseBooleanArray, "<this>");
        return sparseBooleanArray.indexOfKey(n) >= 0;
    }
    
    public static final boolean containsKey(@NotNull final SparseBooleanArray sparseBooleanArray, final int n) {
        Intrinsics.checkNotNullParameter((Object)sparseBooleanArray, "<this>");
        return sparseBooleanArray.indexOfKey(n) >= 0;
    }
    
    public static final boolean containsValue(@NotNull final SparseBooleanArray sparseBooleanArray, final boolean b) {
        Intrinsics.checkNotNullParameter((Object)sparseBooleanArray, "<this>");
        return sparseBooleanArray.indexOfValue(b) >= 0;
    }
    
    public static final void forEach(@NotNull final SparseBooleanArray sparseBooleanArray, @NotNull final Function2<? super Integer, ? super Boolean, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)sparseBooleanArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)function2, "action");
        for (int size = sparseBooleanArray.size(), i = 0; i < size; ++i) {
            function2.invoke((Object)sparseBooleanArray.keyAt(i), (Object)sparseBooleanArray.valueAt(i));
        }
    }
    
    public static final boolean getOrDefault(@NotNull final SparseBooleanArray sparseBooleanArray, final int n, final boolean b) {
        Intrinsics.checkNotNullParameter((Object)sparseBooleanArray, "<this>");
        return sparseBooleanArray.get(n, b);
    }
    
    public static final boolean getOrElse(@NotNull final SparseBooleanArray sparseBooleanArray, int indexOfKey, @NotNull final Function0<Boolean> function0) {
        Intrinsics.checkNotNullParameter((Object)sparseBooleanArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)function0, "defaultValue");
        indexOfKey = sparseBooleanArray.indexOfKey(indexOfKey);
        boolean b;
        if (indexOfKey >= 0) {
            b = sparseBooleanArray.valueAt(indexOfKey);
        }
        else {
            b = (boolean)function0.invoke();
        }
        return b;
    }
    
    public static final int getSize(@NotNull final SparseBooleanArray sparseBooleanArray) {
        Intrinsics.checkNotNullParameter((Object)sparseBooleanArray, "<this>");
        return sparseBooleanArray.size();
    }
    
    public static final boolean isEmpty(@NotNull final SparseBooleanArray sparseBooleanArray) {
        Intrinsics.checkNotNullParameter((Object)sparseBooleanArray, "<this>");
        return sparseBooleanArray.size() == 0;
    }
    
    public static final boolean isNotEmpty(@NotNull final SparseBooleanArray sparseBooleanArray) {
        Intrinsics.checkNotNullParameter((Object)sparseBooleanArray, "<this>");
        return sparseBooleanArray.size() != 0;
    }
    
    @NotNull
    public static final IntIterator keyIterator(@NotNull final SparseBooleanArray sparseBooleanArray) {
        Intrinsics.checkNotNullParameter((Object)sparseBooleanArray, "<this>");
        return (IntIterator)new SparseBooleanArrayKt$keyIterator.SparseBooleanArrayKt$keyIterator$1(sparseBooleanArray);
    }
    
    @NotNull
    public static final SparseBooleanArray plus(@NotNull final SparseBooleanArray sparseBooleanArray, @NotNull final SparseBooleanArray sparseBooleanArray2) {
        Intrinsics.checkNotNullParameter((Object)sparseBooleanArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)sparseBooleanArray2, "other");
        final SparseBooleanArray sparseBooleanArray3 = new SparseBooleanArray(sparseBooleanArray.size() + sparseBooleanArray2.size());
        putAll(sparseBooleanArray3, sparseBooleanArray);
        putAll(sparseBooleanArray3, sparseBooleanArray2);
        return sparseBooleanArray3;
    }
    
    public static final void putAll(@NotNull final SparseBooleanArray sparseBooleanArray, @NotNull final SparseBooleanArray sparseBooleanArray2) {
        Intrinsics.checkNotNullParameter((Object)sparseBooleanArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)sparseBooleanArray2, "other");
        for (int size = sparseBooleanArray2.size(), i = 0; i < size; ++i) {
            sparseBooleanArray.put(sparseBooleanArray2.keyAt(i), sparseBooleanArray2.valueAt(i));
        }
    }
    
    public static final boolean remove(@NotNull final SparseBooleanArray sparseBooleanArray, final int n, final boolean b) {
        Intrinsics.checkNotNullParameter((Object)sparseBooleanArray, "<this>");
        final int indexOfKey = sparseBooleanArray.indexOfKey(n);
        if (indexOfKey >= 0 && b == sparseBooleanArray.valueAt(indexOfKey)) {
            sparseBooleanArray.delete(n);
            return true;
        }
        return false;
    }
    
    public static final void set(@NotNull final SparseBooleanArray sparseBooleanArray, final int n, final boolean b) {
        Intrinsics.checkNotNullParameter((Object)sparseBooleanArray, "<this>");
        sparseBooleanArray.put(n, b);
    }
    
    @NotNull
    public static final BooleanIterator valueIterator(@NotNull final SparseBooleanArray sparseBooleanArray) {
        Intrinsics.checkNotNullParameter((Object)sparseBooleanArray, "<this>");
        return (BooleanIterator)new SparseBooleanArrayKt$valueIterator.SparseBooleanArrayKt$valueIterator$1(sparseBooleanArray);
    }
}

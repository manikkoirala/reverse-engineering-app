// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public class DebugUtils
{
    private DebugUtils() {
    }
    
    public static void buildShortClassTag(final Object o, final StringBuilder sb) {
        if (o == null) {
            sb.append("null");
        }
        else {
            String str;
            if ((str = o.getClass().getSimpleName()).length() <= 0) {
                final String name = o.getClass().getName();
                final int lastIndex = name.lastIndexOf(46);
                str = name;
                if (lastIndex > 0) {
                    str = name.substring(lastIndex + 1);
                }
            }
            sb.append(str);
            sb.append('{');
            sb.append(Integer.toHexString(System.identityHashCode(o)));
        }
    }
}

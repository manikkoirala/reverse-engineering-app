// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.Continuation;
import kotlin.Metadata;

@Metadata
public final class AndroidXConsumerKt
{
    @NotNull
    public static final <T> Consumer<T> asAndroidXConsumer(@NotNull final Continuation<? super T> continuation) {
        Intrinsics.checkNotNullParameter((Object)continuation, "<this>");
        return new AndroidXContinuationConsumer<T>(continuation);
    }
}

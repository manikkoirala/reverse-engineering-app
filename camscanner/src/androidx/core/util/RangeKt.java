// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import kotlin.ranges.ClosedRange;
import androidx.annotation.RequiresApi;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.util.Range;
import android.annotation.SuppressLint;
import kotlin.Metadata;

@Metadata
@SuppressLint({ "ClassVerificationFailure" })
public final class RangeKt
{
    @RequiresApi(21)
    @NotNull
    public static final <T extends Comparable<? super T>> Range<T> and(@NotNull final Range<T> range, @NotNull final Range<T> range2) {
        Intrinsics.checkNotNullParameter((Object)range, "<this>");
        Intrinsics.checkNotNullParameter((Object)range2, "other");
        final Range intersect = range.intersect((Range)range2);
        Intrinsics.checkNotNullExpressionValue((Object)intersect, "intersect(other)");
        return (Range<T>)intersect;
    }
    
    @RequiresApi(21)
    @NotNull
    public static final <T extends Comparable<? super T>> Range<T> plus(@NotNull final Range<T> range, @NotNull final Range<T> range2) {
        Intrinsics.checkNotNullParameter((Object)range, "<this>");
        Intrinsics.checkNotNullParameter((Object)range2, "other");
        final Range extend = range.extend((Range)range2);
        Intrinsics.checkNotNullExpressionValue((Object)extend, "extend(other)");
        return (Range<T>)extend;
    }
    
    @RequiresApi(21)
    @NotNull
    public static final <T extends Comparable<? super T>> Range<T> plus(@NotNull final Range<T> range, @NotNull final T t) {
        Intrinsics.checkNotNullParameter((Object)range, "<this>");
        Intrinsics.checkNotNullParameter((Object)t, "value");
        final Range extend = range.extend((Comparable)t);
        Intrinsics.checkNotNullExpressionValue((Object)extend, "extend(value)");
        return (Range<T>)extend;
    }
    
    @RequiresApi(21)
    @NotNull
    public static final <T extends Comparable<? super T>> Range<T> rangeTo(@NotNull final T t, @NotNull final T t2) {
        Intrinsics.checkNotNullParameter((Object)t, "<this>");
        Intrinsics.checkNotNullParameter((Object)t2, "that");
        return (Range<T>)new Range((Comparable)t, (Comparable)t2);
    }
    
    @RequiresApi(21)
    @NotNull
    public static final <T extends Comparable<? super T>> ClosedRange<T> toClosedRange(@NotNull final Range<T> range) {
        Intrinsics.checkNotNullParameter((Object)range, "<this>");
        return (ClosedRange<T>)new RangeKt$toClosedRange.RangeKt$toClosedRange$1((Range)range);
    }
    
    @RequiresApi(21)
    @NotNull
    public static final <T extends Comparable<? super T>> Range<T> toRange(@NotNull final ClosedRange<T> closedRange) {
        Intrinsics.checkNotNullParameter((Object)closedRange, "<this>");
        return (Range<T>)new Range(closedRange.getStart(), closedRange.getEndInclusive());
    }
}

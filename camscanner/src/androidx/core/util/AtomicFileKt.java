// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import kotlin.jvm.internal.InlineMarker;
import kotlin.Unit;
import java.io.FileOutputStream;
import kotlin.jvm.functions.Function1;
import kotlin.text.Charsets;
import java.nio.charset.Charset;
import androidx.annotation.RequiresApi;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.util.AtomicFile;
import android.annotation.SuppressLint;
import kotlin.Metadata;

@Metadata
@SuppressLint({ "ClassVerificationFailure" })
public final class AtomicFileKt
{
    @RequiresApi(17)
    @NotNull
    public static final byte[] readBytes(@NotNull final AtomicFile atomicFile) {
        Intrinsics.checkNotNullParameter((Object)atomicFile, "<this>");
        final byte[] fully = atomicFile.readFully();
        Intrinsics.checkNotNullExpressionValue((Object)fully, "readFully()");
        return fully;
    }
    
    @RequiresApi(17)
    @NotNull
    public static final String readText(@NotNull final AtomicFile atomicFile, @NotNull final Charset charset) {
        Intrinsics.checkNotNullParameter((Object)atomicFile, "<this>");
        Intrinsics.checkNotNullParameter((Object)charset, "charset");
        final byte[] fully = atomicFile.readFully();
        Intrinsics.checkNotNullExpressionValue((Object)fully, "readFully()");
        return new String(fully, charset);
    }
    
    @RequiresApi(17)
    public static final void tryWrite(@NotNull final AtomicFile atomicFile, @NotNull final Function1<? super FileOutputStream, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)atomicFile, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final FileOutputStream startWrite = atomicFile.startWrite();
        try {
            Intrinsics.checkNotNullExpressionValue((Object)startWrite, "stream");
            function1.invoke((Object)startWrite);
            InlineMarker.\u3007o00\u3007\u3007Oo(1);
            atomicFile.finishWrite(startWrite);
            InlineMarker.\u3007080(1);
        }
        finally {
            InlineMarker.\u3007o00\u3007\u3007Oo(1);
            atomicFile.failWrite(startWrite);
            InlineMarker.\u3007080(1);
        }
    }
    
    @RequiresApi(17)
    public static final void writeBytes(@NotNull final AtomicFile atomicFile, @NotNull final byte[] b) {
        Intrinsics.checkNotNullParameter((Object)atomicFile, "<this>");
        Intrinsics.checkNotNullParameter((Object)b, "array");
        final FileOutputStream startWrite = atomicFile.startWrite();
        try {
            Intrinsics.checkNotNullExpressionValue((Object)startWrite, "stream");
            startWrite.write(b);
            atomicFile.finishWrite(startWrite);
        }
        finally {
            atomicFile.failWrite(startWrite);
        }
    }
    
    @RequiresApi(17)
    public static final void writeText(@NotNull final AtomicFile atomicFile, @NotNull final String s, @NotNull final Charset charset) {
        Intrinsics.checkNotNullParameter((Object)atomicFile, "<this>");
        Intrinsics.checkNotNullParameter((Object)s, "text");
        Intrinsics.checkNotNullParameter((Object)charset, "charset");
        final byte[] bytes = s.getBytes(charset);
        Intrinsics.checkNotNullExpressionValue((Object)bytes, "this as java.lang.String).getBytes(charset)");
        writeBytes(atomicFile, bytes);
    }
}

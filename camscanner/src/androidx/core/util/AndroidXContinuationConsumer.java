// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import kotlin.Result;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.Continuation;
import kotlin.Metadata;
import java.util.concurrent.atomic.AtomicBoolean;

@Metadata
final class AndroidXContinuationConsumer<T> extends AtomicBoolean implements Consumer<T>
{
    @NotNull
    private final Continuation<T> continuation;
    
    public AndroidXContinuationConsumer(@NotNull final Continuation<? super T> continuation) {
        Intrinsics.checkNotNullParameter((Object)continuation, "continuation");
        super(false);
        this.continuation = (Continuation<T>)continuation;
    }
    
    @Override
    public void accept(final T t) {
        if (this.compareAndSet(false, true)) {
            this.continuation.resumeWith(Result.constructor-impl((Object)t));
        }
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ContinuationConsumer(resultAccepted = ");
        sb.append(this.get());
        sb.append(')');
        return sb.toString();
    }
}

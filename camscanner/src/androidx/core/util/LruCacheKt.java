// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import kotlin.jvm.internal.Intrinsics;
import android.util.LruCache;
import kotlin.Unit;
import kotlin.jvm.functions.Function4;
import kotlin.jvm.functions.Function1;
import org.jetbrains.annotations.NotNull;
import kotlin.jvm.functions.Function2;
import kotlin.Metadata;

@Metadata
public final class LruCacheKt
{
    @NotNull
    public static final <K, V> LruCache<K, V> lruCache(final int n, @NotNull final Function2<? super K, ? super V, Integer> function2, @NotNull final Function1<? super K, ? extends V> function3, @NotNull final Function4<? super Boolean, ? super K, ? super V, ? super V, Unit> function4) {
        Intrinsics.checkNotNullParameter((Object)function2, "sizeOf");
        Intrinsics.checkNotNullParameter((Object)function3, "create");
        Intrinsics.checkNotNullParameter((Object)function4, "onEntryRemoved");
        return (LruCache<K, V>)new LruCacheKt$lruCache.LruCacheKt$lruCache$4(n, (Function2)function2, (Function1)function3, (Function4)function4);
    }
}

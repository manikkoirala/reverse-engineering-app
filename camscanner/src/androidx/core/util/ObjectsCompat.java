// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import androidx.annotation.DoNotInline;
import java.util.Objects;
import androidx.annotation.RequiresApi;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ObjectsCompat
{
    private ObjectsCompat() {
    }
    
    public static boolean equals(@Nullable final Object o, @Nullable final Object o2) {
        return Api19Impl.equals(o, o2);
    }
    
    public static int hash(@Nullable final Object... array) {
        return Api19Impl.hash(array);
    }
    
    public static int hashCode(@Nullable final Object o) {
        int hashCode;
        if (o != null) {
            hashCode = o.hashCode();
        }
        else {
            hashCode = 0;
        }
        return hashCode;
    }
    
    @NonNull
    public static <T> T requireNonNull(@Nullable final T t) {
        t.getClass();
        return t;
    }
    
    @NonNull
    public static <T> T requireNonNull(@Nullable final T t, @NonNull final String s) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(s);
    }
    
    @Nullable
    public static String toString(@Nullable final Object o, @Nullable String string) {
        if (o != null) {
            string = o.toString();
        }
        return string;
    }
    
    @RequiresApi(19)
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        @DoNotInline
        static boolean equals(final Object a, final Object b) {
            return Objects.equals(a, b);
        }
        
        @DoNotInline
        static int hash(final Object... values) {
            return Objects.hash(values);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import java.util.Iterator;
import kotlin.collections.IntIterator;
import kotlin.jvm.functions.Function0;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.util.SparseArray;
import kotlin.Metadata;

@Metadata
public final class SparseArrayKt
{
    public static final <T> boolean contains(@NotNull final SparseArray<T> sparseArray, final int n) {
        Intrinsics.checkNotNullParameter((Object)sparseArray, "<this>");
        return sparseArray.indexOfKey(n) >= 0;
    }
    
    public static final <T> boolean containsKey(@NotNull final SparseArray<T> sparseArray, final int n) {
        Intrinsics.checkNotNullParameter((Object)sparseArray, "<this>");
        return sparseArray.indexOfKey(n) >= 0;
    }
    
    public static final <T> boolean containsValue(@NotNull final SparseArray<T> sparseArray, final T t) {
        Intrinsics.checkNotNullParameter((Object)sparseArray, "<this>");
        return sparseArray.indexOfValue((Object)t) >= 0;
    }
    
    public static final <T> void forEach(@NotNull final SparseArray<T> sparseArray, @NotNull final Function2<? super Integer, ? super T, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)sparseArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)function2, "action");
        for (int size = sparseArray.size(), i = 0; i < size; ++i) {
            function2.invoke((Object)sparseArray.keyAt(i), sparseArray.valueAt(i));
        }
    }
    
    public static final <T> T getOrDefault(@NotNull final SparseArray<T> sparseArray, final int n, final T t) {
        Intrinsics.checkNotNullParameter((Object)sparseArray, "<this>");
        Object value = sparseArray.get(n);
        if (value == null) {
            value = t;
        }
        return (T)value;
    }
    
    public static final <T> T getOrElse(@NotNull final SparseArray<T> sparseArray, final int n, @NotNull final Function0<? extends T> function0) {
        Intrinsics.checkNotNullParameter((Object)sparseArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)function0, "defaultValue");
        Object o;
        if ((o = sparseArray.get(n)) == null) {
            o = function0.invoke();
        }
        return (T)o;
    }
    
    public static final <T> int getSize(@NotNull final SparseArray<T> sparseArray) {
        Intrinsics.checkNotNullParameter((Object)sparseArray, "<this>");
        return sparseArray.size();
    }
    
    public static final <T> boolean isEmpty(@NotNull final SparseArray<T> sparseArray) {
        Intrinsics.checkNotNullParameter((Object)sparseArray, "<this>");
        return sparseArray.size() == 0;
    }
    
    public static final <T> boolean isNotEmpty(@NotNull final SparseArray<T> sparseArray) {
        Intrinsics.checkNotNullParameter((Object)sparseArray, "<this>");
        return sparseArray.size() != 0;
    }
    
    @NotNull
    public static final <T> IntIterator keyIterator(@NotNull final SparseArray<T> sparseArray) {
        Intrinsics.checkNotNullParameter((Object)sparseArray, "<this>");
        return (IntIterator)new SparseArrayKt$keyIterator.SparseArrayKt$keyIterator$1((SparseArray)sparseArray);
    }
    
    @NotNull
    public static final <T> SparseArray<T> plus(@NotNull final SparseArray<T> sparseArray, @NotNull final SparseArray<T> sparseArray2) {
        Intrinsics.checkNotNullParameter((Object)sparseArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)sparseArray2, "other");
        final SparseArray sparseArray3 = new SparseArray(sparseArray.size() + sparseArray2.size());
        putAll((android.util.SparseArray<Object>)sparseArray3, (android.util.SparseArray<Object>)sparseArray);
        putAll((android.util.SparseArray<Object>)sparseArray3, (android.util.SparseArray<Object>)sparseArray2);
        return (SparseArray<T>)sparseArray3;
    }
    
    public static final <T> void putAll(@NotNull final SparseArray<T> sparseArray, @NotNull final SparseArray<T> sparseArray2) {
        Intrinsics.checkNotNullParameter((Object)sparseArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)sparseArray2, "other");
        for (int size = sparseArray2.size(), i = 0; i < size; ++i) {
            sparseArray.put(sparseArray2.keyAt(i), sparseArray2.valueAt(i));
        }
    }
    
    public static final <T> boolean remove(@NotNull final SparseArray<T> sparseArray, int indexOfKey, final T t) {
        Intrinsics.checkNotNullParameter((Object)sparseArray, "<this>");
        indexOfKey = sparseArray.indexOfKey(indexOfKey);
        if (indexOfKey >= 0 && Intrinsics.\u3007o\u3007((Object)t, sparseArray.valueAt(indexOfKey))) {
            sparseArray.removeAt(indexOfKey);
            return true;
        }
        return false;
    }
    
    public static final <T> void set(@NotNull final SparseArray<T> sparseArray, final int n, final T t) {
        Intrinsics.checkNotNullParameter((Object)sparseArray, "<this>");
        sparseArray.put(n, (Object)t);
    }
    
    @NotNull
    public static final <T> Iterator<T> valueIterator(@NotNull final SparseArray<T> sparseArray) {
        Intrinsics.checkNotNullParameter((Object)sparseArray, "<this>");
        return (Iterator<T>)new SparseArrayKt$valueIterator.SparseArrayKt$valueIterator$1((SparseArray)sparseArray);
    }
}

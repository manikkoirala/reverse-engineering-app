// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import kotlin.collections.LongIterator;
import kotlin.collections.IntIterator;
import kotlin.jvm.functions.Function0;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import androidx.annotation.RequiresApi;
import android.annotation.SuppressLint;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.util.SparseLongArray;
import kotlin.Metadata;

@Metadata
public final class SparseLongArrayKt
{
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(18)
    public static final boolean contains(@NotNull final SparseLongArray sparseLongArray, final int n) {
        Intrinsics.checkNotNullParameter((Object)sparseLongArray, "<this>");
        return sparseLongArray.indexOfKey(n) >= 0;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(18)
    public static final boolean containsKey(@NotNull final SparseLongArray sparseLongArray, final int n) {
        Intrinsics.checkNotNullParameter((Object)sparseLongArray, "<this>");
        return sparseLongArray.indexOfKey(n) >= 0;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(18)
    public static final boolean containsValue(@NotNull final SparseLongArray sparseLongArray, final long n) {
        Intrinsics.checkNotNullParameter((Object)sparseLongArray, "<this>");
        return sparseLongArray.indexOfValue(n) >= 0;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(18)
    public static final void forEach(@NotNull final SparseLongArray sparseLongArray, @NotNull final Function2<? super Integer, ? super Long, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)sparseLongArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)function2, "action");
        for (int size = sparseLongArray.size(), i = 0; i < size; ++i) {
            function2.invoke((Object)sparseLongArray.keyAt(i), (Object)sparseLongArray.valueAt(i));
        }
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(18)
    public static final long getOrDefault(@NotNull final SparseLongArray sparseLongArray, final int n, final long n2) {
        Intrinsics.checkNotNullParameter((Object)sparseLongArray, "<this>");
        return sparseLongArray.get(n, n2);
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(18)
    public static final long getOrElse(@NotNull final SparseLongArray sparseLongArray, int indexOfKey, @NotNull final Function0<Long> function0) {
        Intrinsics.checkNotNullParameter((Object)sparseLongArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)function0, "defaultValue");
        indexOfKey = sparseLongArray.indexOfKey(indexOfKey);
        long n;
        if (indexOfKey >= 0) {
            n = sparseLongArray.valueAt(indexOfKey);
        }
        else {
            n = ((Number)function0.invoke()).longValue();
        }
        return n;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(18)
    public static final int getSize(@NotNull final SparseLongArray sparseLongArray) {
        Intrinsics.checkNotNullParameter((Object)sparseLongArray, "<this>");
        return sparseLongArray.size();
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(18)
    public static final boolean isEmpty(@NotNull final SparseLongArray sparseLongArray) {
        Intrinsics.checkNotNullParameter((Object)sparseLongArray, "<this>");
        return sparseLongArray.size() == 0;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(18)
    public static final boolean isNotEmpty(@NotNull final SparseLongArray sparseLongArray) {
        Intrinsics.checkNotNullParameter((Object)sparseLongArray, "<this>");
        return sparseLongArray.size() != 0;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(18)
    @NotNull
    public static final IntIterator keyIterator(@NotNull final SparseLongArray sparseLongArray) {
        Intrinsics.checkNotNullParameter((Object)sparseLongArray, "<this>");
        return (IntIterator)new SparseLongArrayKt$keyIterator.SparseLongArrayKt$keyIterator$1(sparseLongArray);
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(18)
    @NotNull
    public static final SparseLongArray plus(@NotNull final SparseLongArray sparseLongArray, @NotNull final SparseLongArray sparseLongArray2) {
        Intrinsics.checkNotNullParameter((Object)sparseLongArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)sparseLongArray2, "other");
        final SparseLongArray sparseLongArray3 = new SparseLongArray(sparseLongArray.size() + sparseLongArray2.size());
        putAll(sparseLongArray3, sparseLongArray);
        putAll(sparseLongArray3, sparseLongArray2);
        return sparseLongArray3;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(18)
    public static final void putAll(@NotNull final SparseLongArray sparseLongArray, @NotNull final SparseLongArray sparseLongArray2) {
        Intrinsics.checkNotNullParameter((Object)sparseLongArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)sparseLongArray2, "other");
        for (int size = sparseLongArray2.size(), i = 0; i < size; ++i) {
            sparseLongArray.put(sparseLongArray2.keyAt(i), sparseLongArray2.valueAt(i));
        }
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(18)
    public static final boolean remove(@NotNull final SparseLongArray sparseLongArray, int indexOfKey, final long n) {
        Intrinsics.checkNotNullParameter((Object)sparseLongArray, "<this>");
        indexOfKey = sparseLongArray.indexOfKey(indexOfKey);
        if (indexOfKey >= 0 && n == sparseLongArray.valueAt(indexOfKey)) {
            sparseLongArray.removeAt(indexOfKey);
            return true;
        }
        return false;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(18)
    public static final void set(@NotNull final SparseLongArray sparseLongArray, final int n, final long n2) {
        Intrinsics.checkNotNullParameter((Object)sparseLongArray, "<this>");
        sparseLongArray.put(n, n2);
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(18)
    @NotNull
    public static final LongIterator valueIterator(@NotNull final SparseLongArray sparseLongArray) {
        Intrinsics.checkNotNullParameter((Object)sparseLongArray, "<this>");
        return (LongIterator)new SparseLongArrayKt$valueIterator.SparseLongArrayKt$valueIterator$1(sparseLongArray);
    }
}

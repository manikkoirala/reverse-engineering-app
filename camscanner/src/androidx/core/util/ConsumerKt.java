// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import kotlin.jvm.internal.Intrinsics;
import java.util.function.Consumer;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.Continuation;
import androidx.annotation.RequiresApi;
import kotlin.Metadata;

@Metadata
@RequiresApi(24)
public final class ConsumerKt
{
    @RequiresApi(24)
    @NotNull
    public static final <T> Consumer<T> asConsumer(@NotNull final Continuation<? super T> continuation) {
        Intrinsics.checkNotNullParameter((Object)continuation, "<this>");
        return new ContinuationConsumer<T>(continuation);
    }
}

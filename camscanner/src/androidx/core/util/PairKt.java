// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import android.annotation.SuppressLint;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.util.Pair;
import kotlin.Metadata;

@Metadata
public final class PairKt
{
    @SuppressLint({ "UnknownNullness" })
    public static final <F, S> F component1(@NotNull final Pair<F, S> pair) {
        Intrinsics.checkNotNullParameter((Object)pair, "<this>");
        return (F)pair.first;
    }
    
    @SuppressLint({ "UnknownNullness" })
    public static final <F, S> F component1(@NotNull final androidx.core.util.Pair<F, S> pair) {
        Intrinsics.checkNotNullParameter((Object)pair, "<this>");
        return pair.first;
    }
    
    @SuppressLint({ "UnknownNullness" })
    public static final <F, S> S component2(@NotNull final Pair<F, S> pair) {
        Intrinsics.checkNotNullParameter((Object)pair, "<this>");
        return (S)pair.second;
    }
    
    @SuppressLint({ "UnknownNullness" })
    public static final <F, S> S component2(@NotNull final androidx.core.util.Pair<F, S> pair) {
        Intrinsics.checkNotNullParameter((Object)pair, "<this>");
        return pair.second;
    }
    
    @NotNull
    public static final <F, S> Pair<F, S> toAndroidPair(@NotNull final kotlin.Pair<? extends F, ? extends S> pair) {
        Intrinsics.checkNotNullParameter((Object)pair, "<this>");
        return (Pair<F, S>)new Pair(pair.getFirst(), pair.getSecond());
    }
    
    @NotNull
    public static final <F, S> androidx.core.util.Pair<F, S> toAndroidXPair(@NotNull final kotlin.Pair<? extends F, ? extends S> pair) {
        Intrinsics.checkNotNullParameter((Object)pair, "<this>");
        return new androidx.core.util.Pair<F, S>((F)pair.getFirst(), (S)pair.getSecond());
    }
    
    @NotNull
    public static final <F, S> kotlin.Pair<F, S> toKotlinPair(@NotNull final Pair<F, S> pair) {
        Intrinsics.checkNotNullParameter((Object)pair, "<this>");
        return (kotlin.Pair<F, S>)new kotlin.Pair(pair.first, pair.second);
    }
    
    @NotNull
    public static final <F, S> kotlin.Pair<F, S> toKotlinPair(@NotNull final androidx.core.util.Pair<F, S> pair) {
        Intrinsics.checkNotNullParameter((Object)pair, "<this>");
        return (kotlin.Pair<F, S>)new kotlin.Pair((Object)pair.first, (Object)pair.second);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import kotlin.Result$Companion;
import kotlin.Result;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.Metadata;
import java.util.concurrent.atomic.AtomicBoolean;

@Metadata
final class ContinuationRunnable extends AtomicBoolean implements Runnable
{
    @NotNull
    private final Continuation<Unit> continuation;
    
    public ContinuationRunnable(@NotNull final Continuation<? super Unit> continuation) {
        Intrinsics.checkNotNullParameter((Object)continuation, "continuation");
        super(false);
        this.continuation = (Continuation<Unit>)continuation;
    }
    
    @Override
    public void run() {
        if (this.compareAndSet(false, true)) {
            final Continuation<Unit> continuation = this.continuation;
            final Result$Companion companion = Result.Companion;
            continuation.resumeWith(Result.constructor-impl((Object)Unit.\u3007080));
        }
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ContinuationRunnable(ran = ");
        sb.append(this.get());
        sb.append(')');
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import android.annotation.SuppressLint;

@SuppressLint({ "UnknownNullness" })
public interface Predicate<T>
{
    @SuppressLint({ "MissingNullability" })
    Predicate<T> and(@SuppressLint({ "MissingNullability" }) final Predicate<? super T> p0);
    
    @SuppressLint({ "MissingNullability" })
    Predicate<T> negate();
    
    @SuppressLint({ "MissingNullability" })
    Predicate<T> or(@SuppressLint({ "MissingNullability" }) final Predicate<? super T> p0);
    
    boolean test(final T p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import kotlin.collections.IntIterator;
import kotlin.jvm.functions.Function0;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.util.SparseIntArray;
import kotlin.Metadata;

@Metadata
public final class SparseIntArrayKt
{
    public static final boolean contains(@NotNull final SparseIntArray sparseIntArray, final int n) {
        Intrinsics.checkNotNullParameter((Object)sparseIntArray, "<this>");
        return sparseIntArray.indexOfKey(n) >= 0;
    }
    
    public static final boolean containsKey(@NotNull final SparseIntArray sparseIntArray, final int n) {
        Intrinsics.checkNotNullParameter((Object)sparseIntArray, "<this>");
        return sparseIntArray.indexOfKey(n) >= 0;
    }
    
    public static final boolean containsValue(@NotNull final SparseIntArray sparseIntArray, final int n) {
        Intrinsics.checkNotNullParameter((Object)sparseIntArray, "<this>");
        return sparseIntArray.indexOfValue(n) >= 0;
    }
    
    public static final void forEach(@NotNull final SparseIntArray sparseIntArray, @NotNull final Function2<? super Integer, ? super Integer, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)sparseIntArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)function2, "action");
        for (int size = sparseIntArray.size(), i = 0; i < size; ++i) {
            function2.invoke((Object)sparseIntArray.keyAt(i), (Object)sparseIntArray.valueAt(i));
        }
    }
    
    public static final int getOrDefault(@NotNull final SparseIntArray sparseIntArray, final int n, final int n2) {
        Intrinsics.checkNotNullParameter((Object)sparseIntArray, "<this>");
        return sparseIntArray.get(n, n2);
    }
    
    public static final int getOrElse(@NotNull final SparseIntArray sparseIntArray, int n, @NotNull final Function0<Integer> function0) {
        Intrinsics.checkNotNullParameter((Object)sparseIntArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)function0, "defaultValue");
        n = sparseIntArray.indexOfKey(n);
        if (n >= 0) {
            n = sparseIntArray.valueAt(n);
        }
        else {
            n = ((Number)function0.invoke()).intValue();
        }
        return n;
    }
    
    public static final int getSize(@NotNull final SparseIntArray sparseIntArray) {
        Intrinsics.checkNotNullParameter((Object)sparseIntArray, "<this>");
        return sparseIntArray.size();
    }
    
    public static final boolean isEmpty(@NotNull final SparseIntArray sparseIntArray) {
        Intrinsics.checkNotNullParameter((Object)sparseIntArray, "<this>");
        return sparseIntArray.size() == 0;
    }
    
    public static final boolean isNotEmpty(@NotNull final SparseIntArray sparseIntArray) {
        Intrinsics.checkNotNullParameter((Object)sparseIntArray, "<this>");
        return sparseIntArray.size() != 0;
    }
    
    @NotNull
    public static final IntIterator keyIterator(@NotNull final SparseIntArray sparseIntArray) {
        Intrinsics.checkNotNullParameter((Object)sparseIntArray, "<this>");
        return (IntIterator)new SparseIntArrayKt$keyIterator.SparseIntArrayKt$keyIterator$1(sparseIntArray);
    }
    
    @NotNull
    public static final SparseIntArray plus(@NotNull final SparseIntArray sparseIntArray, @NotNull final SparseIntArray sparseIntArray2) {
        Intrinsics.checkNotNullParameter((Object)sparseIntArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)sparseIntArray2, "other");
        final SparseIntArray sparseIntArray3 = new SparseIntArray(sparseIntArray.size() + sparseIntArray2.size());
        putAll(sparseIntArray3, sparseIntArray);
        putAll(sparseIntArray3, sparseIntArray2);
        return sparseIntArray3;
    }
    
    public static final void putAll(@NotNull final SparseIntArray sparseIntArray, @NotNull final SparseIntArray sparseIntArray2) {
        Intrinsics.checkNotNullParameter((Object)sparseIntArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)sparseIntArray2, "other");
        for (int size = sparseIntArray2.size(), i = 0; i < size; ++i) {
            sparseIntArray.put(sparseIntArray2.keyAt(i), sparseIntArray2.valueAt(i));
        }
    }
    
    public static final boolean remove(@NotNull final SparseIntArray sparseIntArray, int indexOfKey, final int n) {
        Intrinsics.checkNotNullParameter((Object)sparseIntArray, "<this>");
        indexOfKey = sparseIntArray.indexOfKey(indexOfKey);
        if (indexOfKey >= 0 && n == sparseIntArray.valueAt(indexOfKey)) {
            sparseIntArray.removeAt(indexOfKey);
            return true;
        }
        return false;
    }
    
    public static final void set(@NotNull final SparseIntArray sparseIntArray, final int n, final int n2) {
        Intrinsics.checkNotNullParameter((Object)sparseIntArray, "<this>");
        sparseIntArray.put(n, n2);
    }
    
    @NotNull
    public static final IntIterator valueIterator(@NotNull final SparseIntArray sparseIntArray) {
        Intrinsics.checkNotNullParameter((Object)sparseIntArray, "<this>");
        return (IntIterator)new SparseIntArrayKt$valueIterator.SparseIntArrayKt$valueIterator$1(sparseIntArray);
    }
}

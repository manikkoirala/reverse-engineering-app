// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import android.util.Size;
import androidx.annotation.RequiresApi;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.util.SizeF;
import android.annotation.SuppressLint;
import kotlin.Metadata;

@Metadata
@SuppressLint({ "ClassVerificationFailure" })
public final class SizeKt
{
    @RequiresApi(21)
    public static final float component1(@NotNull final SizeF sizeF) {
        Intrinsics.checkNotNullParameter((Object)sizeF, "<this>");
        return sizeF.getWidth();
    }
    
    public static final float component1(@NotNull final SizeFCompat sizeFCompat) {
        Intrinsics.checkNotNullParameter((Object)sizeFCompat, "<this>");
        return sizeFCompat.getWidth();
    }
    
    @RequiresApi(21)
    public static final int component1(@NotNull final Size size) {
        Intrinsics.checkNotNullParameter((Object)size, "<this>");
        return size.getWidth();
    }
    
    @RequiresApi(21)
    public static final float component2(@NotNull final SizeF sizeF) {
        Intrinsics.checkNotNullParameter((Object)sizeF, "<this>");
        return sizeF.getHeight();
    }
    
    public static final float component2(@NotNull final SizeFCompat sizeFCompat) {
        Intrinsics.checkNotNullParameter((Object)sizeFCompat, "<this>");
        return sizeFCompat.getHeight();
    }
    
    @RequiresApi(21)
    public static final int component2(@NotNull final Size size) {
        Intrinsics.checkNotNullParameter((Object)size, "<this>");
        return size.getHeight();
    }
}

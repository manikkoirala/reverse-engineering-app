// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

import java.util.Iterator;
import kotlin.collections.LongIterator;
import kotlin.jvm.functions.Function0;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import androidx.annotation.RequiresApi;
import android.annotation.SuppressLint;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.util.LongSparseArray;
import kotlin.Metadata;

@Metadata
public final class LongSparseArrayKt
{
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(16)
    public static final <T> boolean contains(@NotNull final LongSparseArray<T> longSparseArray, final long n) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        return longSparseArray.indexOfKey(n) >= 0;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(16)
    public static final <T> boolean containsKey(@NotNull final LongSparseArray<T> longSparseArray, final long n) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        return longSparseArray.indexOfKey(n) >= 0;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(16)
    public static final <T> boolean containsValue(@NotNull final LongSparseArray<T> longSparseArray, final T t) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        return longSparseArray.indexOfValue((Object)t) >= 0;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(16)
    public static final <T> void forEach(@NotNull final LongSparseArray<T> longSparseArray, @NotNull final Function2<? super Long, ? super T, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)function2, "action");
        for (int size = longSparseArray.size(), i = 0; i < size; ++i) {
            function2.invoke((Object)longSparseArray.keyAt(i), longSparseArray.valueAt(i));
        }
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(16)
    public static final <T> T getOrDefault(@NotNull final LongSparseArray<T> longSparseArray, final long n, final T t) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        Object value = longSparseArray.get(n);
        if (value == null) {
            value = t;
        }
        return (T)value;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(16)
    public static final <T> T getOrElse(@NotNull final LongSparseArray<T> longSparseArray, final long n, @NotNull final Function0<? extends T> function0) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)function0, "defaultValue");
        Object o;
        if ((o = longSparseArray.get(n)) == null) {
            o = function0.invoke();
        }
        return (T)o;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(16)
    public static final <T> int getSize(@NotNull final LongSparseArray<T> longSparseArray) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        return longSparseArray.size();
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(16)
    public static final <T> boolean isEmpty(@NotNull final LongSparseArray<T> longSparseArray) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        return longSparseArray.size() == 0;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(16)
    public static final <T> boolean isNotEmpty(@NotNull final LongSparseArray<T> longSparseArray) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        return longSparseArray.size() != 0;
    }
    
    @RequiresApi(16)
    @NotNull
    public static final <T> LongIterator keyIterator(@NotNull final LongSparseArray<T> longSparseArray) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        return (LongIterator)new LongSparseArrayKt$keyIterator.LongSparseArrayKt$keyIterator$1((LongSparseArray)longSparseArray);
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(16)
    @NotNull
    public static final <T> LongSparseArray<T> plus(@NotNull final LongSparseArray<T> longSparseArray, @NotNull final LongSparseArray<T> longSparseArray2) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)longSparseArray2, "other");
        final LongSparseArray longSparseArray3 = new LongSparseArray(longSparseArray.size() + longSparseArray2.size());
        putAll((android.util.LongSparseArray<Object>)longSparseArray3, (android.util.LongSparseArray<Object>)longSparseArray);
        putAll((android.util.LongSparseArray<Object>)longSparseArray3, (android.util.LongSparseArray<Object>)longSparseArray2);
        return (LongSparseArray<T>)longSparseArray3;
    }
    
    @RequiresApi(16)
    public static final <T> void putAll(@NotNull final LongSparseArray<T> longSparseArray, @NotNull final LongSparseArray<T> longSparseArray2) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        Intrinsics.checkNotNullParameter((Object)longSparseArray2, "other");
        for (int size = longSparseArray2.size(), i = 0; i < size; ++i) {
            longSparseArray.put(longSparseArray2.keyAt(i), longSparseArray2.valueAt(i));
        }
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(16)
    public static final <T> boolean remove(@NotNull final LongSparseArray<T> longSparseArray, final long n, final T t) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        final int indexOfKey = longSparseArray.indexOfKey(n);
        if (indexOfKey >= 0 && Intrinsics.\u3007o\u3007((Object)t, longSparseArray.valueAt(indexOfKey))) {
            longSparseArray.removeAt(indexOfKey);
            return true;
        }
        return false;
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(16)
    public static final <T> void set(@NotNull final LongSparseArray<T> longSparseArray, final long n, final T t) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        longSparseArray.put(n, (Object)t);
    }
    
    @RequiresApi(16)
    @NotNull
    public static final <T> Iterator<T> valueIterator(@NotNull final LongSparseArray<T> longSparseArray) {
        Intrinsics.checkNotNullParameter((Object)longSparseArray, "<this>");
        return (Iterator<T>)new LongSparseArrayKt$valueIterator.LongSparseArrayKt$valueIterator$1((LongSparseArray)longSparseArray);
    }
}

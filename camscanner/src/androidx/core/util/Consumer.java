// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.util;

public interface Consumer<T>
{
    void accept(final T p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.text;

import android.text.style.UnderlineSpan;
import android.text.style.SuperscriptSpan;
import android.text.style.SubscriptSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.SpannedString;
import android.text.style.StyleSpan;
import android.text.style.BackgroundColorSpan;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import androidx.annotation.ColorInt;
import org.jetbrains.annotations.NotNull;
import android.text.SpannableStringBuilder;
import kotlin.Metadata;

@Metadata
public final class SpannableStringBuilderKt
{
    @NotNull
    public static final SpannableStringBuilder backgroundColor(@NotNull final SpannableStringBuilder spannableStringBuilder, @ColorInt int length, @NotNull final Function1<? super SpannableStringBuilder, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)spannableStringBuilder, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "builderAction");
        final BackgroundColorSpan backgroundColorSpan = new BackgroundColorSpan(length);
        length = spannableStringBuilder.length();
        function1.invoke((Object)spannableStringBuilder);
        spannableStringBuilder.setSpan((Object)backgroundColorSpan, length, spannableStringBuilder.length(), 17);
        return spannableStringBuilder;
    }
    
    @NotNull
    public static final SpannableStringBuilder bold(@NotNull final SpannableStringBuilder spannableStringBuilder, @NotNull final Function1<? super SpannableStringBuilder, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)spannableStringBuilder, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "builderAction");
        final StyleSpan styleSpan = new StyleSpan(1);
        final int length = spannableStringBuilder.length();
        function1.invoke((Object)spannableStringBuilder);
        spannableStringBuilder.setSpan((Object)styleSpan, length, spannableStringBuilder.length(), 17);
        return spannableStringBuilder;
    }
    
    @NotNull
    public static final SpannedString buildSpannedString(@NotNull final Function1<? super SpannableStringBuilder, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "builderAction");
        final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        function1.invoke((Object)spannableStringBuilder);
        return new SpannedString((CharSequence)spannableStringBuilder);
    }
    
    @NotNull
    public static final SpannableStringBuilder color(@NotNull final SpannableStringBuilder spannableStringBuilder, @ColorInt int length, @NotNull final Function1<? super SpannableStringBuilder, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)spannableStringBuilder, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "builderAction");
        final ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(length);
        length = spannableStringBuilder.length();
        function1.invoke((Object)spannableStringBuilder);
        spannableStringBuilder.setSpan((Object)foregroundColorSpan, length, spannableStringBuilder.length(), 17);
        return spannableStringBuilder;
    }
    
    @NotNull
    public static final SpannableStringBuilder inSpans(@NotNull final SpannableStringBuilder spannableStringBuilder, @NotNull final Object o, @NotNull final Function1<? super SpannableStringBuilder, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)spannableStringBuilder, "<this>");
        Intrinsics.checkNotNullParameter(o, "span");
        Intrinsics.checkNotNullParameter((Object)function1, "builderAction");
        final int length = spannableStringBuilder.length();
        function1.invoke((Object)spannableStringBuilder);
        spannableStringBuilder.setSpan(o, length, spannableStringBuilder.length(), 17);
        return spannableStringBuilder;
    }
    
    @NotNull
    public static final SpannableStringBuilder inSpans(@NotNull final SpannableStringBuilder spannableStringBuilder, @NotNull final Object[] array, @NotNull final Function1<? super SpannableStringBuilder, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)spannableStringBuilder, "<this>");
        Intrinsics.checkNotNullParameter((Object)array, "spans");
        Intrinsics.checkNotNullParameter((Object)function1, "builderAction");
        final int length = spannableStringBuilder.length();
        function1.invoke((Object)spannableStringBuilder);
        for (int length2 = array.length, i = 0; i < length2; ++i) {
            spannableStringBuilder.setSpan(array[i], length, spannableStringBuilder.length(), 17);
        }
        return spannableStringBuilder;
    }
    
    @NotNull
    public static final SpannableStringBuilder italic(@NotNull final SpannableStringBuilder spannableStringBuilder, @NotNull final Function1<? super SpannableStringBuilder, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)spannableStringBuilder, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "builderAction");
        final StyleSpan styleSpan = new StyleSpan(2);
        final int length = spannableStringBuilder.length();
        function1.invoke((Object)spannableStringBuilder);
        spannableStringBuilder.setSpan((Object)styleSpan, length, spannableStringBuilder.length(), 17);
        return spannableStringBuilder;
    }
    
    @NotNull
    public static final SpannableStringBuilder scale(@NotNull final SpannableStringBuilder spannableStringBuilder, final float n, @NotNull final Function1<? super SpannableStringBuilder, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)spannableStringBuilder, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "builderAction");
        final RelativeSizeSpan relativeSizeSpan = new RelativeSizeSpan(n);
        final int length = spannableStringBuilder.length();
        function1.invoke((Object)spannableStringBuilder);
        spannableStringBuilder.setSpan((Object)relativeSizeSpan, length, spannableStringBuilder.length(), 17);
        return spannableStringBuilder;
    }
    
    @NotNull
    public static final SpannableStringBuilder strikeThrough(@NotNull final SpannableStringBuilder spannableStringBuilder, @NotNull final Function1<? super SpannableStringBuilder, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)spannableStringBuilder, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "builderAction");
        final StrikethroughSpan strikethroughSpan = new StrikethroughSpan();
        final int length = spannableStringBuilder.length();
        function1.invoke((Object)spannableStringBuilder);
        spannableStringBuilder.setSpan((Object)strikethroughSpan, length, spannableStringBuilder.length(), 17);
        return spannableStringBuilder;
    }
    
    @NotNull
    public static final SpannableStringBuilder subscript(@NotNull final SpannableStringBuilder spannableStringBuilder, @NotNull final Function1<? super SpannableStringBuilder, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)spannableStringBuilder, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "builderAction");
        final SubscriptSpan subscriptSpan = new SubscriptSpan();
        final int length = spannableStringBuilder.length();
        function1.invoke((Object)spannableStringBuilder);
        spannableStringBuilder.setSpan((Object)subscriptSpan, length, spannableStringBuilder.length(), 17);
        return spannableStringBuilder;
    }
    
    @NotNull
    public static final SpannableStringBuilder superscript(@NotNull final SpannableStringBuilder spannableStringBuilder, @NotNull final Function1<? super SpannableStringBuilder, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)spannableStringBuilder, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "builderAction");
        final SuperscriptSpan superscriptSpan = new SuperscriptSpan();
        final int length = spannableStringBuilder.length();
        function1.invoke((Object)spannableStringBuilder);
        spannableStringBuilder.setSpan((Object)superscriptSpan, length, spannableStringBuilder.length(), 17);
        return spannableStringBuilder;
    }
    
    @NotNull
    public static final SpannableStringBuilder underline(@NotNull final SpannableStringBuilder spannableStringBuilder, @NotNull final Function1<? super SpannableStringBuilder, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)spannableStringBuilder, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "builderAction");
        final UnderlineSpan underlineSpan = new UnderlineSpan();
        final int length = spannableStringBuilder.length();
        function1.invoke((Object)spannableStringBuilder);
        spannableStringBuilder.setSpan((Object)underlineSpan, length, spannableStringBuilder.length(), 17);
        return spannableStringBuilder;
    }
}

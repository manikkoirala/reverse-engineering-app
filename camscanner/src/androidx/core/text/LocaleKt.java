// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.text;

import androidx.annotation.RequiresApi;
import android.text.TextUtils;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import java.util.Locale;
import android.annotation.SuppressLint;
import kotlin.Metadata;

@Metadata
@SuppressLint({ "ClassVerificationFailure" })
public final class LocaleKt
{
    @RequiresApi(17)
    public static final int getLayoutDirection(@NotNull final Locale locale) {
        Intrinsics.checkNotNullParameter((Object)locale, "<this>");
        return TextUtils.getLayoutDirectionFromLocale(locale);
    }
}

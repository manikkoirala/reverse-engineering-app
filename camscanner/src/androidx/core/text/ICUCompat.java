// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.text;

import android.icu.util.ULocale;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.annotation.SuppressLint;
import java.lang.reflect.InvocationTargetException;
import java.util.Locale;
import android.os.Build$VERSION;
import java.lang.reflect.Method;

public final class ICUCompat
{
    private static final String TAG = "ICUCompat";
    private static Method sAddLikelySubtagsMethod;
    private static Method sGetScriptMethod;
    
    static {
        if (Build$VERSION.SDK_INT < 24) {
            try {
                ICUCompat.sAddLikelySubtagsMethod = Class.forName("libcore.icu.ICU").getMethod("addLikelySubtags", Locale.class);
            }
            catch (final Exception cause) {
                throw new IllegalStateException(cause);
            }
        }
    }
    
    private ICUCompat() {
    }
    
    @SuppressLint({ "BanUncheckedReflection" })
    private static String addLikelySubtagsBelowApi21(Locale string) {
        string = (Locale)string.toString();
        try {
            final Method sAddLikelySubtagsMethod = ICUCompat.sAddLikelySubtagsMethod;
            if (sAddLikelySubtagsMethod != null) {
                return (String)sAddLikelySubtagsMethod.invoke(null, string);
            }
            return (String)string;
        }
        catch (final IllegalAccessException | InvocationTargetException ex) {
            return (String)string;
        }
    }
    
    @SuppressLint({ "BanUncheckedReflection" })
    private static String getScriptBelowApi21(String s) {
        try {
            final Method sGetScriptMethod = ICUCompat.sGetScriptMethod;
            if (sGetScriptMethod != null) {
                s = (String)sGetScriptMethod.invoke(null, s);
                return s;
            }
            return null;
        }
        catch (final IllegalAccessException | InvocationTargetException ex) {
            return null;
        }
    }
    
    @Nullable
    public static String maximizeAndGetScript(@NonNull final Locale locale) {
        if (Build$VERSION.SDK_INT >= 24) {
            return Api24Impl.getScript(Api24Impl.addLikelySubtags(Api24Impl.forLocale(locale)));
        }
        try {
            return Api21Impl.getScript((Locale)ICUCompat.sAddLikelySubtagsMethod.invoke(null, locale));
        }
        catch (final InvocationTargetException | IllegalAccessException ex) {
            return Api21Impl.getScript(locale);
        }
    }
    
    @RequiresApi(21)
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        @DoNotInline
        static String getScript(final Locale locale) {
            return locale.getScript();
        }
    }
    
    @RequiresApi(24)
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        @DoNotInline
        static ULocale addLikelySubtags(final Object o) {
            return O8.\u3007080((ULocale)o);
        }
        
        @DoNotInline
        static ULocale forLocale(final Locale locale) {
            return Oo08.\u3007080(locale);
        }
        
        @DoNotInline
        static String getScript(final Object o) {
            return o\u30070.\u3007080((ULocale)o);
        }
    }
}

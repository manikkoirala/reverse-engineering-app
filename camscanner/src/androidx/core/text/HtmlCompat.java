// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.text;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.text.Html$TagHandler;
import androidx.annotation.Nullable;
import android.text.Html$ImageGetter;
import android.text.Html;
import android.os.Build$VERSION;
import android.text.Spanned;
import androidx.annotation.NonNull;
import android.annotation.SuppressLint;

@SuppressLint({ "InlinedApi" })
public final class HtmlCompat
{
    public static final int FROM_HTML_MODE_COMPACT = 63;
    public static final int FROM_HTML_MODE_LEGACY = 0;
    public static final int FROM_HTML_OPTION_USE_CSS_COLORS = 256;
    public static final int FROM_HTML_SEPARATOR_LINE_BREAK_BLOCKQUOTE = 32;
    public static final int FROM_HTML_SEPARATOR_LINE_BREAK_DIV = 16;
    public static final int FROM_HTML_SEPARATOR_LINE_BREAK_HEADING = 2;
    public static final int FROM_HTML_SEPARATOR_LINE_BREAK_LIST = 8;
    public static final int FROM_HTML_SEPARATOR_LINE_BREAK_LIST_ITEM = 4;
    public static final int FROM_HTML_SEPARATOR_LINE_BREAK_PARAGRAPH = 1;
    public static final int TO_HTML_PARAGRAPH_LINES_CONSECUTIVE = 0;
    public static final int TO_HTML_PARAGRAPH_LINES_INDIVIDUAL = 1;
    
    private HtmlCompat() {
    }
    
    @NonNull
    public static Spanned fromHtml(@NonNull final String s, final int n) {
        if (Build$VERSION.SDK_INT >= 24) {
            return Api24Impl.fromHtml(s, n);
        }
        return Html.fromHtml(s);
    }
    
    @NonNull
    public static Spanned fromHtml(@NonNull final String s, final int n, @Nullable final Html$ImageGetter html$ImageGetter, @Nullable final Html$TagHandler html$TagHandler) {
        if (Build$VERSION.SDK_INT >= 24) {
            return Api24Impl.fromHtml(s, n, html$ImageGetter, html$TagHandler);
        }
        return Html.fromHtml(s, html$ImageGetter, html$TagHandler);
    }
    
    @NonNull
    public static String toHtml(@NonNull final Spanned spanned, final int n) {
        if (Build$VERSION.SDK_INT >= 24) {
            return Api24Impl.toHtml(spanned, n);
        }
        return Html.toHtml(spanned);
    }
    
    @RequiresApi(24)
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        @DoNotInline
        static Spanned fromHtml(final String s, final int n) {
            return \u3007o00\u3007\u3007Oo.\u3007080(s, n);
        }
        
        @DoNotInline
        static Spanned fromHtml(final String s, final int n, final Html$ImageGetter html$ImageGetter, final Html$TagHandler html$TagHandler) {
            return \u3007o\u3007.\u3007080(s, n, html$ImageGetter, html$TagHandler);
        }
        
        @DoNotInline
        static String toHtml(final Spanned spanned, final int n) {
            return \u3007080.\u3007080(spanned, n);
        }
    }
}

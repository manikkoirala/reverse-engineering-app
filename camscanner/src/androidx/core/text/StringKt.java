// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.text;

import android.text.TextUtils;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class StringKt
{
    @NotNull
    public static final String htmlEncode(@NotNull String htmlEncode) {
        Intrinsics.checkNotNullParameter((Object)htmlEncode, "<this>");
        htmlEncode = TextUtils.htmlEncode(htmlEncode);
        Intrinsics.checkNotNullExpressionValue((Object)htmlEncode, "htmlEncode(this)");
        return htmlEncode;
    }
}

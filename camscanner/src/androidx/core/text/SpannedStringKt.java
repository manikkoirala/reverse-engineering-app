// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.text;

import android.text.SpannedString;
import org.jetbrains.annotations.NotNull;
import kotlin.jvm.internal.Intrinsics;
import android.text.Spanned;
import kotlin.Metadata;

@Metadata
public final class SpannedStringKt
{
    @NotNull
    public static final Spanned toSpanned(@NotNull final CharSequence charSequence) {
        Intrinsics.checkNotNullParameter((Object)charSequence, "<this>");
        final SpannedString value = SpannedString.valueOf(charSequence);
        Intrinsics.checkNotNullExpressionValue((Object)value, "valueOf(this)");
        return (Spanned)value;
    }
}

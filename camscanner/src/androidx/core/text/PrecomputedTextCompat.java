// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.text;

import android.graphics.Paint;
import android.text.Spanned;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import android.text.TextDirectionHeuristics;
import androidx.core.util.ObjectsCompat;
import android.text.PrecomputedText$Params$Builder;
import android.text.TextDirectionHeuristic;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;
import androidx.annotation.RestrictTo;
import androidx.annotation.IntRange;
import androidx.annotation.UiThread;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import android.annotation.SuppressLint;
import android.text.PrecomputedText$Params;
import android.text.Layout$Alignment;
import android.text.StaticLayout;
import androidx.appcompat.widget.o\u30070OOo\u30070;
import androidx.appcompat.widget.\u3007\u30070o;
import androidx.appcompat.widget.Oo8Oo00oo;
import androidx.appcompat.widget.o0ooO;
import androidx.appcompat.widget.OOO\u3007O0;
import android.text.TextUtils;
import java.util.ArrayList;
import androidx.core.os.TraceCompat;
import androidx.core.util.Preconditions;
import android.text.SpannableString;
import androidx.annotation.RequiresApi;
import android.os.Build$VERSION;
import androidx.annotation.Nullable;
import android.text.PrecomputedText;
import androidx.annotation.NonNull;
import androidx.annotation.GuardedBy;
import java.util.concurrent.Executor;
import android.text.Spannable;

public class PrecomputedTextCompat implements Spannable
{
    private static final char LINE_FEED = '\n';
    @GuardedBy("sLock")
    @NonNull
    private static Executor sExecutor;
    private static final Object sLock;
    @NonNull
    private final int[] mParagraphEnds;
    @NonNull
    private final Params mParams;
    @NonNull
    private final Spannable mText;
    @Nullable
    private final PrecomputedText mWrapped;
    
    static {
        sLock = new Object();
    }
    
    @RequiresApi(28)
    private PrecomputedTextCompat(@NonNull PrecomputedText precomputedText, @NonNull final Params mParams) {
        this.mText = (Spannable)precomputedText;
        this.mParams = mParams;
        this.mParagraphEnds = null;
        if (Build$VERSION.SDK_INT < 29) {
            precomputedText = null;
        }
        this.mWrapped = precomputedText;
    }
    
    private PrecomputedTextCompat(@NonNull final CharSequence charSequence, @NonNull final Params mParams, @NonNull final int[] mParagraphEnds) {
        this.mText = (Spannable)new SpannableString(charSequence);
        this.mParams = mParams;
        this.mParagraphEnds = mParagraphEnds;
        this.mWrapped = null;
    }
    
    @SuppressLint({ "WrongConstant" })
    public static PrecomputedTextCompat create(@NonNull final CharSequence charSequence, @NonNull final Params params) {
        Preconditions.checkNotNull(charSequence);
        Preconditions.checkNotNull(params);
        try {
            TraceCompat.beginSection("PrecomputedText");
            if (Build$VERSION.SDK_INT >= 29) {
                final PrecomputedText$Params mWrapped = params.mWrapped;
                if (mWrapped != null) {
                    return new PrecomputedTextCompat(OO0o\u3007\u3007.\u3007080(charSequence, mWrapped), params);
                }
            }
            final ArrayList<Integer> list = new ArrayList<Integer>();
            final int length = charSequence.length();
            int i = 0;
            while (i < length) {
                i = TextUtils.indexOf(charSequence, '\n', i, length);
                if (i < 0) {
                    i = length;
                }
                else {
                    ++i;
                }
                list.add(i);
            }
            final int[] array = new int[list.size()];
            for (int j = 0; j < list.size(); ++j) {
                array[j] = list.get(j);
            }
            if (Build$VERSION.SDK_INT >= 23) {
                o\u30070OOo\u30070.\u3007080(\u3007\u30070o.\u3007080(Oo8Oo00oo.\u3007080(o0ooO.\u3007080(OOO\u3007O0.\u3007080(charSequence, 0, charSequence.length(), params.getTextPaint(), Integer.MAX_VALUE), params.getBreakStrategy()), params.getHyphenationFrequency()), params.getTextDirection()));
            }
            else {
                new StaticLayout(charSequence, params.getTextPaint(), Integer.MAX_VALUE, Layout$Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
            }
            return new PrecomputedTextCompat(charSequence, params, array);
        }
        finally {
            TraceCompat.endSection();
        }
    }
    
    @UiThread
    public static Future<PrecomputedTextCompat> getTextFuture(@NonNull final CharSequence charSequence, @NonNull Params params, @Nullable final Executor executor) {
        params = (Params)new PrecomputedTextFutureTask(params, charSequence);
        final Executor executor2 = executor;
        if (executor == null) {
            synchronized (PrecomputedTextCompat.sLock) {
                if (PrecomputedTextCompat.sExecutor == null) {
                    PrecomputedTextCompat.sExecutor = Executors.newFixedThreadPool(1);
                }
                final Executor sExecutor = PrecomputedTextCompat.sExecutor;
            }
        }
        executor2.execute((Runnable)params);
        return (Future<PrecomputedTextCompat>)params;
    }
    
    public char charAt(final int n) {
        return ((CharSequence)this.mText).charAt(n);
    }
    
    @IntRange(from = 0L)
    public int getParagraphCount() {
        if (Build$VERSION.SDK_INT >= 29) {
            return \u300780\u3007808\u3007O.\u3007080(this.mWrapped);
        }
        return this.mParagraphEnds.length;
    }
    
    @IntRange(from = 0L)
    public int getParagraphEnd(@IntRange(from = 0L) final int n) {
        Preconditions.checkArgumentInRange(n, 0, this.getParagraphCount(), "paraIndex");
        if (Build$VERSION.SDK_INT >= 29) {
            return \u3007\u3007888.\u3007080(this.mWrapped, n);
        }
        return this.mParagraphEnds[n];
    }
    
    @IntRange(from = 0L)
    public int getParagraphStart(@IntRange(from = 0L) int n) {
        final int paragraphCount = this.getParagraphCount();
        final int n2 = 0;
        Preconditions.checkArgumentInRange(n, 0, paragraphCount, "paraIndex");
        if (Build$VERSION.SDK_INT >= 29) {
            return \u30078o8o\u3007.\u3007080(this.mWrapped, n);
        }
        if (n == 0) {
            n = n2;
        }
        else {
            n = this.mParagraphEnds[n - 1];
        }
        return n;
    }
    
    @NonNull
    public Params getParams() {
        return this.mParams;
    }
    
    @Nullable
    @RequiresApi(28)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public PrecomputedText getPrecomputedText() {
        final Spannable mText = this.mText;
        if (mText instanceof PrecomputedText) {
            return (PrecomputedText)mText;
        }
        return null;
    }
    
    public int getSpanEnd(final Object o) {
        return ((Spanned)this.mText).getSpanEnd(o);
    }
    
    public int getSpanFlags(final Object o) {
        return ((Spanned)this.mText).getSpanFlags(o);
    }
    
    public int getSpanStart(final Object o) {
        return ((Spanned)this.mText).getSpanStart(o);
    }
    
    public <T> T[] getSpans(final int n, final int n2, final Class<T> clazz) {
        if (Build$VERSION.SDK_INT >= 29) {
            return (T[])\u3007O8o08O.\u3007080(this.mWrapped, n, n2, (Class)clazz);
        }
        return (T[])((Spanned)this.mText).getSpans(n, n2, (Class)clazz);
    }
    
    public int length() {
        return ((CharSequence)this.mText).length();
    }
    
    public int nextSpanTransition(final int n, final int n2, final Class clazz) {
        return ((Spanned)this.mText).nextSpanTransition(n, n2, clazz);
    }
    
    public void removeSpan(final Object o) {
        if (!(o instanceof MetricAffectingSpan)) {
            if (Build$VERSION.SDK_INT >= 29) {
                oO80.\u3007080(this.mWrapped, o);
            }
            else {
                this.mText.removeSpan(o);
            }
            return;
        }
        throw new IllegalArgumentException("MetricAffectingSpan can not be removed from PrecomputedText.");
    }
    
    public void setSpan(final Object o, final int n, final int n2, final int n3) {
        if (!(o instanceof MetricAffectingSpan)) {
            if (Build$VERSION.SDK_INT >= 29) {
                OO0o\u3007\u3007\u3007\u30070.\u3007080(this.mWrapped, o, n, n2, n3);
            }
            else {
                this.mText.setSpan(o, n, n2, n3);
            }
            return;
        }
        throw new IllegalArgumentException("MetricAffectingSpan can not be set to PrecomputedText.");
    }
    
    public CharSequence subSequence(final int n, final int n2) {
        return ((CharSequence)this.mText).subSequence(n, n2);
    }
    
    @NonNull
    @Override
    public String toString() {
        return this.mText.toString();
    }
    
    public static final class Params
    {
        private final int mBreakStrategy;
        private final int mHyphenationFrequency;
        @NonNull
        private final TextPaint mPaint;
        @Nullable
        private final TextDirectionHeuristic mTextDir;
        final PrecomputedText$Params mWrapped;
        
        @RequiresApi(28)
        public Params(@NonNull PrecomputedText$Params mWrapped) {
            this.mPaint = \u3007O00.\u3007080(mWrapped);
            this.mTextDir = \u3007\u30078O0\u30078.\u3007080(mWrapped);
            this.mBreakStrategy = \u30070\u3007O0088o.\u3007080(mWrapped);
            this.mHyphenationFrequency = OoO8.\u3007080(mWrapped);
            if (Build$VERSION.SDK_INT < 29) {
                mWrapped = null;
            }
            this.mWrapped = mWrapped;
        }
        
        Params(@NonNull final TextPaint mPaint, @NonNull final TextDirectionHeuristic mTextDir, final int mBreakStrategy, final int mHyphenationFrequency) {
            if (Build$VERSION.SDK_INT >= 29) {
                this.mWrapped = \u3007oo\u3007.\u3007080(oo88o8O.\u3007080(\u3007O888o0o.\u3007080(o800o8O.\u3007080(new PrecomputedText$Params$Builder(mPaint), mBreakStrategy), mHyphenationFrequency), mTextDir));
            }
            else {
                this.mWrapped = null;
            }
            this.mPaint = mPaint;
            this.mTextDir = mTextDir;
            this.mBreakStrategy = mBreakStrategy;
            this.mHyphenationFrequency = mHyphenationFrequency;
        }
        
        @Override
        public boolean equals(@Nullable final Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof Params)) {
                return false;
            }
            final Params params = (Params)o;
            return this.equalsWithoutTextDirection(params) && this.mTextDir == params.getTextDirection();
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public boolean equalsWithoutTextDirection(@NonNull final Params params) {
            final int sdk_INT = Build$VERSION.SDK_INT;
            if (sdk_INT >= 23) {
                if (this.mBreakStrategy != params.getBreakStrategy()) {
                    return false;
                }
                if (this.mHyphenationFrequency != params.getHyphenationFrequency()) {
                    return false;
                }
            }
            if (((Paint)this.mPaint).getTextSize() != ((Paint)params.getTextPaint()).getTextSize()) {
                return false;
            }
            if (((Paint)this.mPaint).getTextScaleX() != ((Paint)params.getTextPaint()).getTextScaleX()) {
                return false;
            }
            if (((Paint)this.mPaint).getTextSkewX() != ((Paint)params.getTextPaint()).getTextSkewX()) {
                return false;
            }
            if (((Paint)this.mPaint).getLetterSpacing() != ((Paint)params.getTextPaint()).getLetterSpacing()) {
                return false;
            }
            if (!TextUtils.equals((CharSequence)((Paint)this.mPaint).getFontFeatureSettings(), (CharSequence)((Paint)params.getTextPaint()).getFontFeatureSettings())) {
                return false;
            }
            if (((Paint)this.mPaint).getFlags() != ((Paint)params.getTextPaint()).getFlags()) {
                return false;
            }
            if (sdk_INT >= 24) {
                if (!androidx.appcompat.app.oO80.\u3007080(Oooo8o0\u3007.\u3007080(this.mPaint), (Object)Oooo8o0\u3007.\u3007080(params.getTextPaint()))) {
                    return false;
                }
            }
            else if (!((Paint)this.mPaint).getTextLocale().equals(((Paint)params.getTextPaint()).getTextLocale())) {
                return false;
            }
            if (((Paint)this.mPaint).getTypeface() == null) {
                if (((Paint)params.getTextPaint()).getTypeface() != null) {
                    return false;
                }
            }
            else if (!((Paint)this.mPaint).getTypeface().equals((Object)((Paint)params.getTextPaint()).getTypeface())) {
                return false;
            }
            return true;
        }
        
        @RequiresApi(23)
        public int getBreakStrategy() {
            return this.mBreakStrategy;
        }
        
        @RequiresApi(23)
        public int getHyphenationFrequency() {
            return this.mHyphenationFrequency;
        }
        
        @Nullable
        @RequiresApi(18)
        public TextDirectionHeuristic getTextDirection() {
            return this.mTextDir;
        }
        
        @NonNull
        public TextPaint getTextPaint() {
            return this.mPaint;
        }
        
        @Override
        public int hashCode() {
            if (Build$VERSION.SDK_INT >= 24) {
                return ObjectsCompat.hash(((Paint)this.mPaint).getTextSize(), ((Paint)this.mPaint).getTextScaleX(), ((Paint)this.mPaint).getTextSkewX(), ((Paint)this.mPaint).getLetterSpacing(), ((Paint)this.mPaint).getFlags(), Oooo8o0\u3007.\u3007080(this.mPaint), ((Paint)this.mPaint).getTypeface(), ((Paint)this.mPaint).isElegantTextHeight(), this.mTextDir, this.mBreakStrategy, this.mHyphenationFrequency);
            }
            return ObjectsCompat.hash(((Paint)this.mPaint).getTextSize(), ((Paint)this.mPaint).getTextScaleX(), ((Paint)this.mPaint).getTextSkewX(), ((Paint)this.mPaint).getLetterSpacing(), ((Paint)this.mPaint).getFlags(), ((Paint)this.mPaint).getTextLocale(), ((Paint)this.mPaint).getTypeface(), ((Paint)this.mPaint).isElegantTextHeight(), this.mTextDir, this.mBreakStrategy, this.mHyphenationFrequency);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("{");
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("textSize=");
            sb2.append(((Paint)this.mPaint).getTextSize());
            sb.append(sb2.toString());
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(", textScaleX=");
            sb3.append(((Paint)this.mPaint).getTextScaleX());
            sb.append(sb3.toString());
            final StringBuilder sb4 = new StringBuilder();
            sb4.append(", textSkewX=");
            sb4.append(((Paint)this.mPaint).getTextSkewX());
            sb.append(sb4.toString());
            final int sdk_INT = Build$VERSION.SDK_INT;
            final StringBuilder sb5 = new StringBuilder();
            sb5.append(", letterSpacing=");
            sb5.append(((Paint)this.mPaint).getLetterSpacing());
            sb.append(sb5.toString());
            final StringBuilder sb6 = new StringBuilder();
            sb6.append(", elegantTextHeight=");
            sb6.append(((Paint)this.mPaint).isElegantTextHeight());
            sb.append(sb6.toString());
            if (sdk_INT >= 24) {
                final StringBuilder sb7 = new StringBuilder();
                sb7.append(", textLocale=");
                sb7.append(Oooo8o0\u3007.\u3007080(this.mPaint));
                sb.append(sb7.toString());
            }
            else {
                final StringBuilder sb8 = new StringBuilder();
                sb8.append(", textLocale=");
                sb8.append(((Paint)this.mPaint).getTextLocale());
                sb.append(sb8.toString());
            }
            final StringBuilder sb9 = new StringBuilder();
            sb9.append(", typeface=");
            sb9.append(((Paint)this.mPaint).getTypeface());
            sb.append(sb9.toString());
            if (sdk_INT >= 26) {
                final StringBuilder sb10 = new StringBuilder();
                sb10.append(", variationSettings=");
                sb10.append(\u3007\u3007808\u3007.\u3007080(this.mPaint));
                sb.append(sb10.toString());
            }
            final StringBuilder sb11 = new StringBuilder();
            sb11.append(", textDir=");
            sb11.append(this.mTextDir);
            sb.append(sb11.toString());
            final StringBuilder sb12 = new StringBuilder();
            sb12.append(", breakStrategy=");
            sb12.append(this.mBreakStrategy);
            sb.append(sb12.toString());
            final StringBuilder sb13 = new StringBuilder();
            sb13.append(", hyphenationFrequency=");
            sb13.append(this.mHyphenationFrequency);
            sb.append(sb13.toString());
            sb.append("}");
            return sb.toString();
        }
        
        public static class Builder
        {
            private int mBreakStrategy;
            private int mHyphenationFrequency;
            @NonNull
            private final TextPaint mPaint;
            private TextDirectionHeuristic mTextDir;
            
            public Builder(@NonNull final TextPaint mPaint) {
                this.mPaint = mPaint;
                if (Build$VERSION.SDK_INT >= 23) {
                    this.mBreakStrategy = 1;
                    this.mHyphenationFrequency = 1;
                }
                else {
                    this.mHyphenationFrequency = 0;
                    this.mBreakStrategy = 0;
                }
                this.mTextDir = TextDirectionHeuristics.FIRSTSTRONG_LTR;
            }
            
            @NonNull
            public Params build() {
                return new Params(this.mPaint, this.mTextDir, this.mBreakStrategy, this.mHyphenationFrequency);
            }
            
            @RequiresApi(23)
            public Builder setBreakStrategy(final int mBreakStrategy) {
                this.mBreakStrategy = mBreakStrategy;
                return this;
            }
            
            @RequiresApi(23)
            public Builder setHyphenationFrequency(final int mHyphenationFrequency) {
                this.mHyphenationFrequency = mHyphenationFrequency;
                return this;
            }
            
            @RequiresApi(18)
            public Builder setTextDirection(@NonNull final TextDirectionHeuristic mTextDir) {
                this.mTextDir = mTextDir;
                return this;
            }
        }
    }
    
    private static class PrecomputedTextFutureTask extends FutureTask<PrecomputedTextCompat>
    {
        PrecomputedTextFutureTask(@NonNull final Params params, @NonNull final CharSequence charSequence) {
            super(new PrecomputedTextCallback(params, charSequence));
        }
        
        private static class PrecomputedTextCallback implements Callable<PrecomputedTextCompat>
        {
            private Params mParams;
            private CharSequence mText;
            
            PrecomputedTextCallback(@NonNull final Params mParams, @NonNull final CharSequence mText) {
                this.mParams = mParams;
                this.mText = mText;
            }
            
            @Override
            public PrecomputedTextCompat call() throws Exception {
                return PrecomputedTextCompat.create(this.mText, this.mParams);
            }
        }
    }
}

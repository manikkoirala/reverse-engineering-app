// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.text;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.text.TextUtils;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import java.util.Locale;

public final class TextUtilsCompat
{
    private static final String ARAB_SCRIPT_SUBTAG = "Arab";
    private static final String HEBR_SCRIPT_SUBTAG = "Hebr";
    private static final Locale ROOT;
    
    static {
        ROOT = new Locale("", "");
    }
    
    private TextUtilsCompat() {
    }
    
    private static int getLayoutDirectionFromFirstChar(@NonNull final Locale inLocale) {
        final byte directionality = Character.getDirectionality(inLocale.getDisplayName(inLocale).charAt(0));
        if (directionality != 1 && directionality != 2) {
            return 0;
        }
        return 1;
    }
    
    public static int getLayoutDirectionFromLocale(@Nullable final Locale locale) {
        return Api17Impl.getLayoutDirectionFromLocale(locale);
    }
    
    @NonNull
    public static String htmlEncode(@NonNull final String s) {
        return TextUtils.htmlEncode(s);
    }
    
    @RequiresApi(17)
    static class Api17Impl
    {
        private Api17Impl() {
        }
        
        @DoNotInline
        static int getLayoutDirectionFromLocale(final Locale locale) {
            return TextUtils.getLayoutDirectionFromLocale(locale);
        }
    }
}

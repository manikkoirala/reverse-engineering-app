// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.text;

import kotlin.jvm.internal.Intrinsics;
import android.text.Spanned;
import android.text.Html$TagHandler;
import android.text.Html$ImageGetter;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class HtmlKt
{
    @NotNull
    public static final Spanned parseAsHtml(@NotNull final String s, final int n, final Html$ImageGetter html$ImageGetter, final Html$TagHandler html$TagHandler) {
        Intrinsics.checkNotNullParameter((Object)s, "<this>");
        final Spanned fromHtml = HtmlCompat.fromHtml(s, n, html$ImageGetter, html$TagHandler);
        Intrinsics.checkNotNullExpressionValue((Object)fromHtml, "fromHtml(this, flags, imageGetter, tagHandler)");
        return fromHtml;
    }
    
    @NotNull
    public static final String toHtml(@NotNull final Spanned spanned, final int n) {
        Intrinsics.checkNotNullParameter((Object)spanned, "<this>");
        final String html = HtmlCompat.toHtml(spanned, n);
        Intrinsics.checkNotNullExpressionValue((Object)html, "toHtml(this, option)");
        return html;
    }
}

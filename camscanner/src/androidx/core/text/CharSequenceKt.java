// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.text;

import android.text.TextUtils;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class CharSequenceKt
{
    public static final boolean isDigitsOnly(@NotNull final CharSequence charSequence) {
        Intrinsics.checkNotNullParameter((Object)charSequence, "<this>");
        return TextUtils.isDigitsOnly(charSequence);
    }
    
    public static final int trimmedLength(@NotNull final CharSequence charSequence) {
        Intrinsics.checkNotNullParameter((Object)charSequence, "<this>");
        return TextUtils.getTrimmedLength(charSequence);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.text;

import android.text.Spanned;
import android.text.SpannableString;
import kotlin.ranges.IntRange;
import android.annotation.SuppressLint;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.text.Spannable;
import kotlin.Metadata;

@Metadata
public final class SpannableStringKt
{
    @SuppressLint({ "SyntheticAccessor" })
    public static final void clearSpans(@NotNull final Spannable spannable) {
        Intrinsics.checkNotNullParameter((Object)spannable, "<this>");
        final int length = ((CharSequence)spannable).length();
        int i = 0;
        final Object[] spans = ((Spanned)spannable).getSpans(0, length, (Class)Object.class);
        Intrinsics.checkNotNullExpressionValue((Object)spans, "getSpans(start, end, T::class.java)");
        while (i < spans.length) {
            spannable.removeSpan(spans[i]);
            ++i;
        }
    }
    
    public static final void set(@NotNull final Spannable spannable, final int n, final int n2, @NotNull final Object o) {
        Intrinsics.checkNotNullParameter((Object)spannable, "<this>");
        Intrinsics.checkNotNullParameter(o, "span");
        spannable.setSpan(o, n, n2, 17);
    }
    
    public static final void set(@NotNull final Spannable spannable, @NotNull final IntRange intRange, @NotNull final Object o) {
        Intrinsics.checkNotNullParameter((Object)spannable, "<this>");
        Intrinsics.checkNotNullParameter((Object)intRange, "range");
        Intrinsics.checkNotNullParameter(o, "span");
        spannable.setSpan(o, (int)intRange.\u300780\u3007808\u3007O(), (int)intRange.oO80(), 17);
    }
    
    @NotNull
    public static final Spannable toSpannable(@NotNull final CharSequence charSequence) {
        Intrinsics.checkNotNullParameter((Object)charSequence, "<this>");
        final SpannableString value = SpannableString.valueOf(charSequence);
        Intrinsics.checkNotNullExpressionValue((Object)value, "valueOf(this)");
        return (Spannable)value;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.internal.view;

import androidx.annotation.NonNull;
import androidx.core.view.ActionProvider;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import androidx.annotation.Nullable;
import android.view.View;
import androidx.annotation.RestrictTo;
import android.view.MenuItem;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public interface SupportMenuItem extends MenuItem
{
    public static final int SHOW_AS_ACTION_ALWAYS = 2;
    public static final int SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW = 8;
    public static final int SHOW_AS_ACTION_IF_ROOM = 1;
    public static final int SHOW_AS_ACTION_NEVER = 0;
    public static final int SHOW_AS_ACTION_WITH_TEXT = 4;
    
    boolean collapseActionView();
    
    boolean expandActionView();
    
    @Nullable
    View getActionView();
    
    int getAlphabeticModifiers();
    
    @Nullable
    CharSequence getContentDescription();
    
    @Nullable
    ColorStateList getIconTintList();
    
    @Nullable
    PorterDuff$Mode getIconTintMode();
    
    int getNumericModifiers();
    
    @Nullable
    ActionProvider getSupportActionProvider();
    
    @Nullable
    CharSequence getTooltipText();
    
    boolean isActionViewExpanded();
    
    boolean requiresActionButton();
    
    boolean requiresOverflow();
    
    @NonNull
    MenuItem setActionView(final int p0);
    
    @NonNull
    MenuItem setActionView(@Nullable final View p0);
    
    @NonNull
    MenuItem setAlphabeticShortcut(final char p0, final int p1);
    
    @NonNull
    SupportMenuItem setContentDescription(@Nullable final CharSequence p0);
    
    @NonNull
    MenuItem setIconTintList(@Nullable final ColorStateList p0);
    
    @NonNull
    MenuItem setIconTintMode(@Nullable final PorterDuff$Mode p0);
    
    @NonNull
    MenuItem setNumericShortcut(final char p0, final int p1);
    
    @NonNull
    MenuItem setShortcut(final char p0, final char p1, final int p2, final int p3);
    
    void setShowAsAction(final int p0);
    
    @NonNull
    MenuItem setShowAsActionFlags(final int p0);
    
    @NonNull
    SupportMenuItem setSupportActionProvider(@Nullable final ActionProvider p0);
    
    @NonNull
    SupportMenuItem setTooltipText(@Nullable final CharSequence p0);
}

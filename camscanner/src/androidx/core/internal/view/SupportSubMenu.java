// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.internal.view;

import androidx.annotation.RestrictTo;
import android.view.SubMenu;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public interface SupportSubMenu extends SupportMenu, SubMenu
{
}

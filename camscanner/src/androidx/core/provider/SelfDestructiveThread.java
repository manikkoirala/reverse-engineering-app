// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.provider;

import java.util.concurrent.Callable;
import androidx.annotation.VisibleForTesting;
import android.os.Message;
import android.os.HandlerThread;
import android.os.Handler;
import androidx.annotation.GuardedBy;
import android.os.Handler$Callback;
import androidx.annotation.RestrictTo;

@Deprecated
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public class SelfDestructiveThread
{
    private static final int MSG_DESTRUCTION = 0;
    private static final int MSG_INVOKE_RUNNABLE = 1;
    private Handler$Callback mCallback;
    private final int mDestructAfterMillisec;
    @GuardedBy("mLock")
    private int mGeneration;
    @GuardedBy("mLock")
    private Handler mHandler;
    private final Object mLock;
    private final int mPriority;
    @GuardedBy("mLock")
    private HandlerThread mThread;
    private final String mThreadName;
    
    public SelfDestructiveThread(final String mThreadName, final int mPriority, final int mDestructAfterMillisec) {
        this.mLock = new Object();
        this.mCallback = (Handler$Callback)new Handler$Callback() {
            final SelfDestructiveThread this$0;
            
            public boolean handleMessage(final Message message) {
                final int what = message.what;
                if (what == 0) {
                    this.this$0.onDestruction();
                    return true;
                }
                if (what != 1) {
                    return true;
                }
                this.this$0.onInvokeRunnable((Runnable)message.obj);
                return true;
            }
        };
        this.mThreadName = mThreadName;
        this.mPriority = mPriority;
        this.mDestructAfterMillisec = mDestructAfterMillisec;
        this.mGeneration = 0;
    }
    
    private void post(final Runnable runnable) {
        synchronized (this.mLock) {
            if (this.mThread == null) {
                ((Thread)(this.mThread = new HandlerThread(this.mThreadName, this.mPriority))).start();
                this.mHandler = new Handler(this.mThread.getLooper(), this.mCallback);
                ++this.mGeneration;
            }
            this.mHandler.removeMessages(0);
            final Handler mHandler = this.mHandler;
            mHandler.sendMessage(mHandler.obtainMessage(1, (Object)runnable));
        }
    }
    
    @VisibleForTesting
    public int getGeneration() {
        synchronized (this.mLock) {
            return this.mGeneration;
        }
    }
    
    @VisibleForTesting
    public boolean isRunning() {
        synchronized (this.mLock) {
            return this.mThread != null;
        }
    }
    
    void onDestruction() {
        synchronized (this.mLock) {
            if (this.mHandler.hasMessages(1)) {
                return;
            }
            this.mThread.quit();
            this.mThread = null;
            this.mHandler = null;
        }
    }
    
    void onInvokeRunnable(final Runnable runnable) {
        runnable.run();
        synchronized (this.mLock) {
            this.mHandler.removeMessages(0);
            final Handler mHandler = this.mHandler;
            mHandler.sendMessageDelayed(mHandler.obtainMessage(0), (long)this.mDestructAfterMillisec);
        }
    }
    
    public <T> void postAndReply(final Callable<T> callable, final ReplyCallback<T> replyCallback) {
        this.post(new Runnable(this, callable, CalleeHandler.create(), replyCallback) {
            final SelfDestructiveThread this$0;
            final Callable val$callable;
            final Handler val$calleeHandler;
            final ReplyCallback val$reply;
            
            @Override
            public void run() {
                Object call;
                try {
                    call = this.val$callable.call();
                }
                catch (final Exception ex) {
                    call = null;
                }
                this.val$calleeHandler.post((Runnable)new Runnable(this, call) {
                    final SelfDestructiveThread$2 this$1;
                    final Object val$result;
                    
                    @Override
                    public void run() {
                        this.this$1.val$reply.onReply(this.val$result);
                    }
                });
            }
        });
    }
    
    public <T> T postAndWait(final Callable<T> p0, final int p1) throws InterruptedException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   java/util/concurrent/locks/ReentrantLock.<init>:()V
        //     7: astore          7
        //     9: aload           7
        //    11: invokevirtual   java/util/concurrent/locks/ReentrantLock.newCondition:()Ljava/util/concurrent/locks/Condition;
        //    14: astore          8
        //    16: new             Ljava/util/concurrent/atomic/AtomicReference;
        //    19: dup            
        //    20: invokespecial   java/util/concurrent/atomic/AtomicReference.<init>:()V
        //    23: astore          10
        //    25: new             Ljava/util/concurrent/atomic/AtomicBoolean;
        //    28: dup            
        //    29: iconst_1       
        //    30: invokespecial   java/util/concurrent/atomic/AtomicBoolean.<init>:(Z)V
        //    33: astore          9
        //    35: aload_0        
        //    36: new             Landroidx/core/provider/SelfDestructiveThread$3;
        //    39: dup            
        //    40: aload_0        
        //    41: aload           10
        //    43: aload_1        
        //    44: aload           7
        //    46: aload           9
        //    48: aload           8
        //    50: invokespecial   androidx/core/provider/SelfDestructiveThread$3.<init>:(Landroidx/core/provider/SelfDestructiveThread;Ljava/util/concurrent/atomic/AtomicReference;Ljava/util/concurrent/Callable;Ljava/util/concurrent/locks/ReentrantLock;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/util/concurrent/locks/Condition;)V
        //    53: invokespecial   androidx/core/provider/SelfDestructiveThread.post:(Ljava/lang/Runnable;)V
        //    56: aload           7
        //    58: invokevirtual   java/util/concurrent/locks/ReentrantLock.lock:()V
        //    61: aload           9
        //    63: invokevirtual   java/util/concurrent/atomic/AtomicBoolean.get:()Z
        //    66: ifne            82
        //    69: aload           10
        //    71: invokevirtual   java/util/concurrent/atomic/AtomicReference.get:()Ljava/lang/Object;
        //    74: astore_1       
        //    75: aload           7
        //    77: invokevirtual   java/util/concurrent/locks/ReentrantLock.unlock:()V
        //    80: aload_1        
        //    81: areturn        
        //    82: getstatic       java/util/concurrent/TimeUnit.MILLISECONDS:Ljava/util/concurrent/TimeUnit;
        //    85: iload_2        
        //    86: i2l            
        //    87: invokevirtual   java/util/concurrent/TimeUnit.toNanos:(J)J
        //    90: lstore_3       
        //    91: aload           8
        //    93: lload_3        
        //    94: invokeinterface java/util/concurrent/locks/Condition.awaitNanos:(J)J
        //    99: lstore          5
        //   101: lload           5
        //   103: lstore_3       
        //   104: aload           9
        //   106: invokevirtual   java/util/concurrent/atomic/AtomicBoolean.get:()Z
        //   109: ifne            125
        //   112: aload           10
        //   114: invokevirtual   java/util/concurrent/atomic/AtomicReference.get:()Ljava/lang/Object;
        //   117: astore_1       
        //   118: aload           7
        //   120: invokevirtual   java/util/concurrent/locks/ReentrantLock.unlock:()V
        //   123: aload_1        
        //   124: areturn        
        //   125: lload_3        
        //   126: lconst_0       
        //   127: lcmp           
        //   128: ifle            134
        //   131: goto            91
        //   134: new             Ljava/lang/InterruptedException;
        //   137: astore_1       
        //   138: aload_1        
        //   139: ldc             "timeout"
        //   141: invokespecial   java/lang/InterruptedException.<init>:(Ljava/lang/String;)V
        //   144: aload_1        
        //   145: athrow         
        //   146: astore_1       
        //   147: aload           7
        //   149: invokevirtual   java/util/concurrent/locks/ReentrantLock.unlock:()V
        //   152: aload_1        
        //   153: athrow         
        //   154: astore_1       
        //   155: goto            104
        //    Exceptions:
        //  throws java.lang.InterruptedException
        //    Signature:
        //  <T:Ljava/lang/Object;>(Ljava/util/concurrent/Callable<TT;>;I)TT;
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                            
        //  -----  -----  -----  -----  --------------------------------
        //  61     75     146    154    Any
        //  82     91     146    154    Any
        //  91     101    154    158    Ljava/lang/InterruptedException;
        //  91     101    146    154    Any
        //  104    118    146    154    Any
        //  134    146    146    154    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0091:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public interface ReplyCallback<T>
    {
        void onReply(final T p0);
    }
}

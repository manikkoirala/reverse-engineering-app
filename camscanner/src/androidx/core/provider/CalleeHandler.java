// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.provider;

import androidx.annotation.NonNull;
import android.os.Looper;
import android.os.Handler;

class CalleeHandler
{
    private CalleeHandler() {
    }
    
    @NonNull
    static Handler create() {
        Handler handler;
        if (Looper.myLooper() == null) {
            handler = new Handler(Looper.getMainLooper());
        }
        else {
            handler = new Handler();
        }
        return handler;
    }
}

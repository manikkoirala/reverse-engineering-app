// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.provider;

import androidx.annotation.DoNotInline;
import android.provider.DocumentsContract;
import androidx.annotation.RequiresApi;
import java.util.List;
import android.os.Build$VERSION;
import android.content.Context;
import java.io.FileNotFoundException;
import android.content.ContentResolver;
import android.net.Uri;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;

public final class DocumentsContractCompat
{
    private static final String PATH_TREE = "tree";
    
    private DocumentsContractCompat() {
    }
    
    @Nullable
    public static Uri buildChildDocumentsUri(@NonNull final String s, @Nullable final String s2) {
        return DocumentsContractApi21Impl.buildChildDocumentsUri(s, s2);
    }
    
    @Nullable
    public static Uri buildChildDocumentsUriUsingTree(@NonNull final Uri uri, @NonNull final String s) {
        return DocumentsContractApi21Impl.buildChildDocumentsUriUsingTree(uri, s);
    }
    
    @Nullable
    public static Uri buildDocumentUri(@NonNull final String s, @NonNull final String s2) {
        return DocumentsContractApi19Impl.buildDocumentUri(s, s2);
    }
    
    @Nullable
    public static Uri buildDocumentUriUsingTree(@NonNull final Uri uri, @NonNull final String s) {
        return DocumentsContractApi21Impl.buildDocumentUriUsingTree(uri, s);
    }
    
    @Nullable
    public static Uri buildTreeDocumentUri(@NonNull final String s, @NonNull final String s2) {
        return DocumentsContractApi21Impl.buildTreeDocumentUri(s, s2);
    }
    
    @Nullable
    public static Uri createDocument(@NonNull final ContentResolver contentResolver, @NonNull final Uri uri, @NonNull final String s, @NonNull final String s2) throws FileNotFoundException {
        return DocumentsContractApi21Impl.createDocument(contentResolver, uri, s, s2);
    }
    
    @Nullable
    public static String getDocumentId(@NonNull final Uri uri) {
        return DocumentsContractApi19Impl.getDocumentId(uri);
    }
    
    @Nullable
    public static String getTreeDocumentId(@NonNull final Uri uri) {
        return DocumentsContractApi21Impl.getTreeDocumentId(uri);
    }
    
    public static boolean isDocumentUri(@NonNull final Context context, @Nullable final Uri uri) {
        return DocumentsContractApi19Impl.isDocumentUri(context, uri);
    }
    
    public static boolean isTreeUri(@NonNull final Uri uri) {
        if (Build$VERSION.SDK_INT < 24) {
            final List pathSegments = uri.getPathSegments();
            final int size = pathSegments.size();
            boolean b = false;
            if (size >= 2) {
                b = b;
                if ("tree".equals(pathSegments.get(0))) {
                    b = true;
                }
            }
            return b;
        }
        return DocumentsContractApi24Impl.isTreeUri(uri);
    }
    
    public static boolean removeDocument(@NonNull final ContentResolver contentResolver, @NonNull final Uri uri, @NonNull final Uri uri2) throws FileNotFoundException {
        if (Build$VERSION.SDK_INT >= 24) {
            return DocumentsContractApi24Impl.removeDocument(contentResolver, uri, uri2);
        }
        return DocumentsContractApi19Impl.deleteDocument(contentResolver, uri);
    }
    
    @Nullable
    public static Uri renameDocument(@NonNull final ContentResolver contentResolver, @NonNull final Uri uri, @NonNull final String s) throws FileNotFoundException {
        return DocumentsContractApi21Impl.renameDocument(contentResolver, uri, s);
    }
    
    public static final class DocumentCompat
    {
        public static final int FLAG_VIRTUAL_DOCUMENT = 512;
        
        private DocumentCompat() {
        }
    }
    
    @RequiresApi(19)
    private static class DocumentsContractApi19Impl
    {
        @DoNotInline
        public static Uri buildDocumentUri(final String s, final String s2) {
            return DocumentsContract.buildDocumentUri(s, s2);
        }
        
        @DoNotInline
        static boolean deleteDocument(final ContentResolver contentResolver, final Uri uri) throws FileNotFoundException {
            return DocumentsContract.deleteDocument(contentResolver, uri);
        }
        
        @DoNotInline
        static String getDocumentId(final Uri uri) {
            return DocumentsContract.getDocumentId(uri);
        }
        
        @DoNotInline
        static boolean isDocumentUri(final Context context, @Nullable final Uri uri) {
            return DocumentsContract.isDocumentUri(context, uri);
        }
    }
    
    @RequiresApi(21)
    private static class DocumentsContractApi21Impl
    {
        @DoNotInline
        static Uri buildChildDocumentsUri(final String s, final String s2) {
            return DocumentsContract.buildChildDocumentsUri(s, s2);
        }
        
        @DoNotInline
        static Uri buildChildDocumentsUriUsingTree(final Uri uri, final String s) {
            return DocumentsContract.buildChildDocumentsUriUsingTree(uri, s);
        }
        
        @DoNotInline
        static Uri buildDocumentUriUsingTree(final Uri uri, final String s) {
            return DocumentsContract.buildDocumentUriUsingTree(uri, s);
        }
        
        @DoNotInline
        public static Uri buildTreeDocumentUri(final String s, final String s2) {
            return DocumentsContract.buildTreeDocumentUri(s, s2);
        }
        
        @DoNotInline
        static Uri createDocument(final ContentResolver contentResolver, final Uri uri, final String s, final String s2) throws FileNotFoundException {
            return DocumentsContract.createDocument(contentResolver, uri, s, s2);
        }
        
        @DoNotInline
        static String getTreeDocumentId(final Uri uri) {
            return DocumentsContract.getTreeDocumentId(uri);
        }
        
        @DoNotInline
        static Uri renameDocument(@NonNull final ContentResolver contentResolver, @NonNull final Uri uri, @NonNull final String s) throws FileNotFoundException {
            return DocumentsContract.renameDocument(contentResolver, uri, s);
        }
    }
    
    @RequiresApi(24)
    private static class DocumentsContractApi24Impl
    {
        @DoNotInline
        static boolean isTreeUri(@NonNull final Uri uri) {
            return \u3007080.\u3007080(uri);
        }
        
        @DoNotInline
        static boolean removeDocument(final ContentResolver contentResolver, final Uri uri, final Uri uri2) throws FileNotFoundException {
            return \u3007o00\u3007\u3007Oo.\u3007080(contentResolver, uri, uri2);
        }
    }
}

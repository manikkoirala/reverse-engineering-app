// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.provider;

import androidx.annotation.DoNotInline;
import android.database.Cursor;
import androidx.annotation.RequiresApi;
import android.content.ContentResolver;
import android.net.Uri;
import android.content.ContentUris;
import android.net.Uri$Builder;
import androidx.annotation.VisibleForTesting;
import java.util.Collection;
import java.util.Collections;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.pm.ProviderInfo;
import androidx.annotation.Nullable;
import android.os.CancellationSignal;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.core.content.res.FontResourcesParserCompat;
import android.content.res.Resources;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import android.content.pm.Signature;
import java.util.Comparator;

class FontProvider
{
    private static final Comparator<byte[]> sByteArrayComparator;
    
    static {
        sByteArrayComparator = new \u3007o\u3007();
    }
    
    private FontProvider() {
    }
    
    private static List<byte[]> convertToByteArrayList(final Signature[] array) {
        final ArrayList list = new ArrayList();
        for (int length = array.length, i = 0; i < length; ++i) {
            list.add(array[i].toByteArray());
        }
        return list;
    }
    
    private static boolean equalsByteArrayList(final List<byte[]> list, final List<byte[]> list2) {
        if (list.size() != list2.size()) {
            return false;
        }
        for (int i = 0; i < list.size(); ++i) {
            if (!Arrays.equals((byte[])list.get(i), (byte[])list2.get(i))) {
                return false;
            }
        }
        return true;
    }
    
    private static List<List<byte[]>> getCertificates(final FontRequest fontRequest, final Resources resources) {
        if (fontRequest.getCertificates() != null) {
            return fontRequest.getCertificates();
        }
        return FontResourcesParserCompat.readCerts(resources, fontRequest.getCertificatesArrayResId());
    }
    
    @NonNull
    static FontsContractCompat.FontFamilyResult getFontFamilyResult(@NonNull final Context context, @NonNull final FontRequest fontRequest, @Nullable final CancellationSignal cancellationSignal) throws PackageManager$NameNotFoundException {
        final ProviderInfo provider = getProvider(context.getPackageManager(), fontRequest, context.getResources());
        if (provider == null) {
            return FontsContractCompat.FontFamilyResult.create(1, null);
        }
        return FontsContractCompat.FontFamilyResult.create(0, query(context, fontRequest, provider.authority, cancellationSignal));
    }
    
    @Nullable
    @VisibleForTesting
    static ProviderInfo getProvider(@NonNull final PackageManager packageManager, @NonNull final FontRequest fontRequest, @Nullable final Resources resources) throws PackageManager$NameNotFoundException {
        final String providerAuthority = fontRequest.getProviderAuthority();
        int i = 0;
        final ProviderInfo resolveContentProvider = packageManager.resolveContentProvider(providerAuthority, 0);
        if (resolveContentProvider == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("No package found for authority: ");
            sb.append(providerAuthority);
            throw new PackageManager$NameNotFoundException(sb.toString());
        }
        if (resolveContentProvider.packageName.equals(fontRequest.getProviderPackage())) {
            final List<byte[]> convertToByteArrayList = convertToByteArrayList(packageManager.getPackageInfo(resolveContentProvider.packageName, 64).signatures);
            Collections.sort((List<Object>)convertToByteArrayList, (Comparator<? super Object>)FontProvider.sByteArrayComparator);
            for (List<List<byte[]>> certificates = getCertificates(fontRequest, resources); i < certificates.size(); ++i) {
                final ArrayList list = new ArrayList<byte[]>((Collection<? extends T>)certificates.get(i));
                Collections.sort((List<E>)list, (Comparator<? super E>)FontProvider.sByteArrayComparator);
                if (equalsByteArrayList(convertToByteArrayList, (List<byte[]>)list)) {
                    return resolveContentProvider;
                }
            }
            return null;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Found content provider ");
        sb2.append(providerAuthority);
        sb2.append(", but package was not ");
        sb2.append(fontRequest.getProviderPackage());
        throw new PackageManager$NameNotFoundException(sb2.toString());
    }
    
    @NonNull
    @VisibleForTesting
    static FontsContractCompat.FontInfo[] query(final Context context, final FontRequest fontRequest, String s, final CancellationSignal cancellationSignal) {
        final ArrayList list = new ArrayList();
        final Uri build = new Uri$Builder().scheme("content").authority(s).build();
        final Uri build2 = new Uri$Builder().scheme("content").authority(s).appendPath("file").build();
        final String s2 = s = null;
        try {
            final ContentResolver contentResolver = context.getContentResolver();
            s = s2;
            final String query = fontRequest.getQuery();
            s = s2;
            final Object query2 = Api16Impl.query(contentResolver, build, new String[] { "_id", "file_id", "font_ttc_index", "font_variation_settings", "font_weight", "font_italic", "result_code" }, "query = ?", new String[] { query }, null, cancellationSignal);
            ArrayList<FontsContractCompat.FontInfo> list2 = list;
            if (query2 != null) {
                list2 = list;
                s = (String)query2;
                if (((Cursor)query2).getCount() > 0) {
                    s = (String)query2;
                    final int columnIndex = ((Cursor)query2).getColumnIndex("result_code");
                    s = (String)query2;
                    s = (String)query2;
                    final ArrayList<FontsContractCompat.FontInfo> list3 = new ArrayList<FontsContractCompat.FontInfo>();
                    s = (String)query2;
                    final int columnIndex2 = ((Cursor)query2).getColumnIndex("_id");
                    s = (String)query2;
                    final int columnIndex3 = ((Cursor)query2).getColumnIndex("file_id");
                    s = (String)query2;
                    final int columnIndex4 = ((Cursor)query2).getColumnIndex("font_ttc_index");
                    s = (String)query2;
                    final int columnIndex5 = ((Cursor)query2).getColumnIndex("font_weight");
                    s = (String)query2;
                    final int columnIndex6 = ((Cursor)query2).getColumnIndex("font_italic");
                    while (true) {
                        s = (String)query2;
                        if (!((Cursor)query2).moveToNext()) {
                            break;
                        }
                        int int1;
                        if (columnIndex != -1) {
                            s = (String)query2;
                            int1 = ((Cursor)query2).getInt(columnIndex);
                        }
                        else {
                            int1 = 0;
                        }
                        int int2;
                        if (columnIndex4 != -1) {
                            s = (String)query2;
                            int2 = ((Cursor)query2).getInt(columnIndex4);
                        }
                        else {
                            int2 = 0;
                        }
                        Uri uri;
                        if (columnIndex3 == -1) {
                            s = (String)query2;
                            uri = ContentUris.withAppendedId(build, ((Cursor)query2).getLong(columnIndex2));
                        }
                        else {
                            s = (String)query2;
                            uri = ContentUris.withAppendedId(build2, ((Cursor)query2).getLong(columnIndex3));
                        }
                        int int3;
                        if (columnIndex5 != -1) {
                            s = (String)query2;
                            int3 = ((Cursor)query2).getInt(columnIndex5);
                        }
                        else {
                            int3 = 400;
                        }
                        boolean b = false;
                        Label_0398: {
                            if (columnIndex6 != -1) {
                                s = (String)query2;
                                if (((Cursor)query2).getInt(columnIndex6) == 1) {
                                    b = true;
                                    break Label_0398;
                                }
                            }
                            b = false;
                        }
                        s = (String)query2;
                        list3.add(FontsContractCompat.FontInfo.create(uri, int2, int3, b, int1));
                    }
                    list2 = list3;
                }
            }
            if (query2 != null) {
                ((Cursor)query2).close();
            }
            return list2.toArray(new FontsContractCompat.FontInfo[0]);
        }
        finally {
            if (s != null) {
                ((Cursor)s).close();
            }
        }
    }
    
    @RequiresApi(16)
    static class Api16Impl
    {
        private Api16Impl() {
        }
        
        @DoNotInline
        static Cursor query(final ContentResolver contentResolver, final Uri uri, final String[] array, final String s, final String[] array2, final String s2, final Object o) {
            return contentResolver.query(uri, array, s, array2, s2, (CancellationSignal)o);
        }
    }
}

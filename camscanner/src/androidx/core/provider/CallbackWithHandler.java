// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.provider;

import android.graphics.Typeface;
import android.os.Handler;
import androidx.annotation.NonNull;

class CallbackWithHandler
{
    @NonNull
    private final FontsContractCompat.FontRequestCallback mCallback;
    @NonNull
    private final Handler mCallbackHandler;
    
    CallbackWithHandler(@NonNull final FontsContractCompat.FontRequestCallback mCallback) {
        this.mCallback = mCallback;
        this.mCallbackHandler = CalleeHandler.create();
    }
    
    CallbackWithHandler(@NonNull final FontsContractCompat.FontRequestCallback mCallback, @NonNull final Handler mCallbackHandler) {
        this.mCallback = mCallback;
        this.mCallbackHandler = mCallbackHandler;
    }
    
    private void onTypefaceRequestFailed(final int n) {
        this.mCallbackHandler.post((Runnable)new Runnable(this, this.mCallback, n) {
            final CallbackWithHandler this$0;
            final FontsContractCompat.FontRequestCallback val$callback;
            final int val$reason;
            
            @Override
            public void run() {
                this.val$callback.onTypefaceRequestFailed(this.val$reason);
            }
        });
    }
    
    private void onTypefaceRetrieved(@NonNull final Typeface typeface) {
        this.mCallbackHandler.post((Runnable)new Runnable(this, this.mCallback, typeface) {
            final CallbackWithHandler this$0;
            final FontsContractCompat.FontRequestCallback val$callback;
            final Typeface val$typeface;
            
            @Override
            public void run() {
                this.val$callback.onTypefaceRetrieved(this.val$typeface);
            }
        });
    }
    
    void onTypefaceResult(@NonNull final FontRequestWorker.TypefaceResult typefaceResult) {
        if (typefaceResult.isSuccess()) {
            this.onTypefaceRetrieved(typefaceResult.mTypeface);
        }
        else {
            this.onTypefaceRequestFailed(typefaceResult.mResult);
        }
    }
}

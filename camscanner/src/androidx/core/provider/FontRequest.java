// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.provider;

import android.util.Base64;
import androidx.annotation.RestrictTo;
import androidx.annotation.Nullable;
import androidx.core.util.Preconditions;
import androidx.annotation.ArrayRes;
import androidx.annotation.NonNull;
import java.util.List;

public final class FontRequest
{
    private final List<List<byte[]>> mCertificates;
    private final int mCertificatesArray;
    private final String mIdentifier;
    private final String mProviderAuthority;
    private final String mProviderPackage;
    private final String mQuery;
    
    public FontRequest(@NonNull final String s, @NonNull final String s2, @NonNull final String s3, @ArrayRes final int mCertificatesArray) {
        this.mProviderAuthority = Preconditions.checkNotNull(s);
        this.mProviderPackage = Preconditions.checkNotNull(s2);
        this.mQuery = Preconditions.checkNotNull(s3);
        this.mCertificates = null;
        Preconditions.checkArgument(mCertificatesArray != 0);
        this.mCertificatesArray = mCertificatesArray;
        this.mIdentifier = this.createIdentifier(s, s2, s3);
    }
    
    public FontRequest(@NonNull final String s, @NonNull final String s2, @NonNull final String s3, @NonNull final List<List<byte[]>> list) {
        this.mProviderAuthority = Preconditions.checkNotNull(s);
        this.mProviderPackage = Preconditions.checkNotNull(s2);
        this.mQuery = Preconditions.checkNotNull(s3);
        this.mCertificates = Preconditions.checkNotNull(list);
        this.mCertificatesArray = 0;
        this.mIdentifier = this.createIdentifier(s, s2, s3);
    }
    
    private String createIdentifier(@NonNull final String str, @NonNull final String str2, @NonNull final String str3) {
        final StringBuilder sb = new StringBuilder(str);
        sb.append("-");
        sb.append(str2);
        sb.append("-");
        sb.append(str3);
        return sb.toString();
    }
    
    @Nullable
    public List<List<byte[]>> getCertificates() {
        return this.mCertificates;
    }
    
    @ArrayRes
    public int getCertificatesArrayResId() {
        return this.mCertificatesArray;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    String getId() {
        return this.mIdentifier;
    }
    
    @Deprecated
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public String getIdentifier() {
        return this.mIdentifier;
    }
    
    @NonNull
    public String getProviderAuthority() {
        return this.mProviderAuthority;
    }
    
    @NonNull
    public String getProviderPackage() {
        return this.mProviderPackage;
    }
    
    @NonNull
    public String getQuery() {
        return this.mQuery;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("FontRequest {mProviderAuthority: ");
        sb2.append(this.mProviderAuthority);
        sb2.append(", mProviderPackage: ");
        sb2.append(this.mProviderPackage);
        sb2.append(", mQuery: ");
        sb2.append(this.mQuery);
        sb2.append(", mCertificates:");
        sb.append(sb2.toString());
        for (int i = 0; i < this.mCertificates.size(); ++i) {
            sb.append(" [");
            final List list = this.mCertificates.get(i);
            for (int j = 0; j < list.size(); ++j) {
                sb.append(" \"");
                sb.append(Base64.encodeToString((byte[])list.get(j), 0));
                sb.append("\"");
            }
            sb.append(" ]");
        }
        sb.append("}");
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("mCertificatesArray: ");
        sb3.append(this.mCertificatesArray);
        sb.append(sb3.toString());
        return sb.toString();
    }
}

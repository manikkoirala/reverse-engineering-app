// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.provider;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.core.util.Preconditions;
import android.provider.BaseColumns;
import java.util.concurrent.Executor;
import androidx.annotation.IntRange;
import androidx.annotation.RequiresApi;
import androidx.core.graphics.TypefaceCompatUtil;
import java.nio.ByteBuffer;
import android.net.Uri;
import java.util.Map;
import androidx.annotation.VisibleForTesting;
import android.content.pm.ProviderInfo;
import android.content.res.Resources;
import android.content.pm.PackageManager;
import android.os.Handler;
import androidx.core.content.res.ResourcesCompat;
import android.content.pm.PackageManager$NameNotFoundException;
import androidx.core.graphics.TypefaceCompat;
import android.graphics.Typeface;
import androidx.annotation.Nullable;
import android.os.CancellationSignal;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.annotation.RestrictTo;

public class FontsContractCompat
{
    @Deprecated
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static final String PARCEL_FONT_RESULTS = "font_results";
    @Deprecated
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    static final int RESULT_CODE_PROVIDER_NOT_FOUND = -1;
    @Deprecated
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    static final int RESULT_CODE_WRONG_CERTIFICATES = -2;
    
    private FontsContractCompat() {
    }
    
    @Nullable
    public static Typeface buildTypeface(@NonNull final Context context, @Nullable final CancellationSignal cancellationSignal, @NonNull final FontInfo[] array) {
        return TypefaceCompat.createFromFontInfo(context, cancellationSignal, array, 0);
    }
    
    @NonNull
    public static FontFamilyResult fetchFonts(@NonNull final Context context, @Nullable final CancellationSignal cancellationSignal, @NonNull final FontRequest fontRequest) throws PackageManager$NameNotFoundException {
        return FontProvider.getFontFamilyResult(context, fontRequest, cancellationSignal);
    }
    
    @Deprecated
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static Typeface getFontSync(final Context context, final FontRequest fontRequest, @Nullable final ResourcesCompat.FontCallback fontCallback, @Nullable final Handler handler, final boolean b, final int n, final int n2) {
        return requestFont(context, fontRequest, n2, b, n, ResourcesCompat.FontCallback.getHandler(handler), (FontRequestCallback)new TypefaceCompat.ResourcesCallbackAdapter(fontCallback));
    }
    
    @Deprecated
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    @VisibleForTesting
    public static ProviderInfo getProvider(@NonNull final PackageManager packageManager, @NonNull final FontRequest fontRequest, @Nullable final Resources resources) throws PackageManager$NameNotFoundException {
        return FontProvider.getProvider(packageManager, fontRequest, resources);
    }
    
    @Deprecated
    @RequiresApi(19)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static Map<Uri, ByteBuffer> prepareFontData(final Context context, final FontInfo[] array, final CancellationSignal cancellationSignal) {
        return TypefaceCompatUtil.readFontInfoIntoByteBuffer(context, array, cancellationSignal);
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static Typeface requestFont(@NonNull final Context context, @NonNull final FontRequest fontRequest, final int n, final boolean b, @IntRange(from = 0L) final int n2, @NonNull final Handler handler, @NonNull final FontRequestCallback fontRequestCallback) {
        final CallbackWithHandler callbackWithHandler = new CallbackWithHandler(fontRequestCallback, handler);
        if (b) {
            return FontRequestWorker.requestFontSync(context, fontRequest, callbackWithHandler, n, n2);
        }
        return FontRequestWorker.requestFontAsync(context, fontRequest, n, null, callbackWithHandler);
    }
    
    public static void requestFont(@NonNull final Context context, @NonNull final FontRequest fontRequest, @NonNull final FontRequestCallback fontRequestCallback, @NonNull final Handler handler) {
        FontRequestWorker.requestFontAsync(context.getApplicationContext(), fontRequest, 0, RequestExecutor.createHandlerExecutor(handler), new CallbackWithHandler(fontRequestCallback));
    }
    
    @Deprecated
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static void resetCache() {
        FontRequestWorker.resetTypefaceCache();
    }
    
    @RestrictTo({ RestrictTo.Scope.TESTS })
    @VisibleForTesting
    public static void resetTypefaceCache() {
        FontRequestWorker.resetTypefaceCache();
    }
    
    public static final class Columns implements BaseColumns
    {
        public static final String FILE_ID = "file_id";
        public static final String ITALIC = "font_italic";
        public static final String RESULT_CODE = "result_code";
        public static final int RESULT_CODE_FONT_NOT_FOUND = 1;
        public static final int RESULT_CODE_FONT_UNAVAILABLE = 2;
        public static final int RESULT_CODE_MALFORMED_QUERY = 3;
        public static final int RESULT_CODE_OK = 0;
        public static final String TTC_INDEX = "font_ttc_index";
        public static final String VARIATION_SETTINGS = "font_variation_settings";
        public static final String WEIGHT = "font_weight";
    }
    
    public static class FontFamilyResult
    {
        public static final int STATUS_OK = 0;
        public static final int STATUS_UNEXPECTED_DATA_PROVIDED = 2;
        public static final int STATUS_WRONG_CERTIFICATES = 1;
        private final FontInfo[] mFonts;
        private final int mStatusCode;
        
        @Deprecated
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public FontFamilyResult(final int mStatusCode, @Nullable final FontInfo[] mFonts) {
            this.mStatusCode = mStatusCode;
            this.mFonts = mFonts;
        }
        
        static FontFamilyResult create(final int n, @Nullable final FontInfo[] array) {
            return new FontFamilyResult(n, array);
        }
        
        public FontInfo[] getFonts() {
            return this.mFonts;
        }
        
        public int getStatusCode() {
            return this.mStatusCode;
        }
    }
    
    public static class FontInfo
    {
        private final boolean mItalic;
        private final int mResultCode;
        private final int mTtcIndex;
        private final Uri mUri;
        private final int mWeight;
        
        @Deprecated
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public FontInfo(@NonNull final Uri uri, @IntRange(from = 0L) final int mTtcIndex, @IntRange(from = 1L, to = 1000L) final int mWeight, final boolean mItalic, final int mResultCode) {
            this.mUri = Preconditions.checkNotNull(uri);
            this.mTtcIndex = mTtcIndex;
            this.mWeight = mWeight;
            this.mItalic = mItalic;
            this.mResultCode = mResultCode;
        }
        
        static FontInfo create(@NonNull final Uri uri, @IntRange(from = 0L) final int n, @IntRange(from = 1L, to = 1000L) final int n2, final boolean b, final int n3) {
            return new FontInfo(uri, n, n2, b, n3);
        }
        
        public int getResultCode() {
            return this.mResultCode;
        }
        
        @IntRange(from = 0L)
        public int getTtcIndex() {
            return this.mTtcIndex;
        }
        
        @NonNull
        public Uri getUri() {
            return this.mUri;
        }
        
        @IntRange(from = 1L, to = 1000L)
        public int getWeight() {
            return this.mWeight;
        }
        
        public boolean isItalic() {
            return this.mItalic;
        }
    }
    
    public static class FontRequestCallback
    {
        public static final int FAIL_REASON_FONT_LOAD_ERROR = -3;
        public static final int FAIL_REASON_FONT_NOT_FOUND = 1;
        public static final int FAIL_REASON_FONT_UNAVAILABLE = 2;
        public static final int FAIL_REASON_MALFORMED_QUERY = 3;
        public static final int FAIL_REASON_PROVIDER_NOT_FOUND = -1;
        public static final int FAIL_REASON_SECURITY_VIOLATION = -4;
        public static final int FAIL_REASON_WRONG_CERTIFICATES = -2;
        @Deprecated
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public static final int RESULT_OK = 0;
        static final int RESULT_SUCCESS = 0;
        
        public void onTypefaceRequestFailed(final int n) {
        }
        
        public void onTypefaceRetrieved(final Typeface typeface) {
        }
        
        @Retention(RetentionPolicy.SOURCE)
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public @interface FontRequestFailReason {
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.location;

import android.os.BaseBundle;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import java.lang.reflect.InvocationTargetException;
import androidx.annotation.FloatRange;
import androidx.core.util.Preconditions;
import java.util.concurrent.TimeUnit;
import android.os.Build$VERSION;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.location.Location;
import androidx.annotation.Nullable;
import java.lang.reflect.Method;

public final class LocationCompat
{
    public static final String EXTRA_BEARING_ACCURACY = "bearingAccuracy";
    public static final String EXTRA_IS_MOCK = "mockLocation";
    public static final String EXTRA_MSL_ALTITUDE = "androidx.core.location.extra.MSL_ALTITUDE";
    public static final String EXTRA_MSL_ALTITUDE_ACCURACY = "androidx.core.location.extra.MSL_ALTITUDE_ACCURACY";
    public static final String EXTRA_SPEED_ACCURACY = "speedAccuracy";
    public static final String EXTRA_VERTICAL_ACCURACY = "verticalAccuracy";
    @Nullable
    private static Method sSetIsFromMockProviderMethod;
    
    private LocationCompat() {
    }
    
    private static boolean containsExtra(@NonNull final Location location, final String s) {
        final Bundle extras = location.getExtras();
        return extras != null && ((BaseBundle)extras).containsKey(s);
    }
    
    public static float getBearingAccuracyDegrees(@NonNull final Location location) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getBearingAccuracyDegrees(location);
        }
        final Bundle extras = location.getExtras();
        if (extras == null) {
            return 0.0f;
        }
        return extras.getFloat("bearingAccuracy", 0.0f);
    }
    
    public static long getElapsedRealtimeMillis(@NonNull final Location location) {
        return TimeUnit.NANOSECONDS.toMillis(Api17Impl.getElapsedRealtimeNanos(location));
    }
    
    public static long getElapsedRealtimeNanos(@NonNull final Location location) {
        return Api17Impl.getElapsedRealtimeNanos(location);
    }
    
    @FloatRange(from = 0.0)
    public static float getMslAltitudeAccuracyMeters(@NonNull final Location location) {
        Preconditions.checkState(hasMslAltitudeAccuracy(location), "The Mean Sea Level altitude accuracy of the location is not set.");
        return getOrCreateExtras(location).getFloat("androidx.core.location.extra.MSL_ALTITUDE_ACCURACY");
    }
    
    public static double getMslAltitudeMeters(@NonNull final Location location) {
        Preconditions.checkState(hasMslAltitude(location), "The Mean Sea Level altitude of the location is not set.");
        return ((BaseBundle)getOrCreateExtras(location)).getDouble("androidx.core.location.extra.MSL_ALTITUDE");
    }
    
    private static Bundle getOrCreateExtras(@NonNull final Location location) {
        Bundle bundle;
        if ((bundle = location.getExtras()) == null) {
            location.setExtras(new Bundle());
            bundle = location.getExtras();
        }
        return bundle;
    }
    
    private static Method getSetIsFromMockProviderMethod() throws NoSuchMethodException {
        if (LocationCompat.sSetIsFromMockProviderMethod == null) {
            (LocationCompat.sSetIsFromMockProviderMethod = Location.class.getDeclaredMethod("setIsFromMockProvider", Boolean.TYPE)).setAccessible(true);
        }
        return LocationCompat.sSetIsFromMockProviderMethod;
    }
    
    public static float getSpeedAccuracyMetersPerSecond(@NonNull final Location location) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getSpeedAccuracyMetersPerSecond(location);
        }
        final Bundle extras = location.getExtras();
        if (extras == null) {
            return 0.0f;
        }
        return extras.getFloat("speedAccuracy", 0.0f);
    }
    
    public static float getVerticalAccuracyMeters(@NonNull final Location location) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getVerticalAccuracyMeters(location);
        }
        final Bundle extras = location.getExtras();
        if (extras == null) {
            return 0.0f;
        }
        return extras.getFloat("verticalAccuracy", 0.0f);
    }
    
    public static boolean hasBearingAccuracy(@NonNull final Location location) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.hasBearingAccuracy(location);
        }
        return containsExtra(location, "bearingAccuracy");
    }
    
    public static boolean hasMslAltitude(@NonNull final Location location) {
        return containsExtra(location, "androidx.core.location.extra.MSL_ALTITUDE");
    }
    
    public static boolean hasMslAltitudeAccuracy(@NonNull final Location location) {
        return containsExtra(location, "androidx.core.location.extra.MSL_ALTITUDE_ACCURACY");
    }
    
    public static boolean hasSpeedAccuracy(@NonNull final Location location) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.hasSpeedAccuracy(location);
        }
        return containsExtra(location, "speedAccuracy");
    }
    
    public static boolean hasVerticalAccuracy(@NonNull final Location location) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.hasVerticalAccuracy(location);
        }
        return containsExtra(location, "verticalAccuracy");
    }
    
    public static boolean isMock(@NonNull final Location location) {
        return Api18Impl.isMock(location);
    }
    
    private static void removeExtra(@NonNull final Location location, final String s) {
        final Bundle extras = location.getExtras();
        if (extras != null) {
            extras.remove(s);
            if (((BaseBundle)extras).isEmpty()) {
                location.setExtras((Bundle)null);
            }
        }
    }
    
    public static void removeMslAltitude(@NonNull final Location location) {
        removeExtra(location, "androidx.core.location.extra.MSL_ALTITUDE");
    }
    
    public static void removeMslAltitudeAccuracy(@NonNull final Location location) {
        removeExtra(location, "androidx.core.location.extra.MSL_ALTITUDE_ACCURACY");
    }
    
    public static void setBearingAccuracyDegrees(@NonNull final Location location, final float n) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setBearingAccuracyDegrees(location, n);
        }
        else {
            getOrCreateExtras(location).putFloat("bearingAccuracy", n);
        }
    }
    
    public static void setMock(@NonNull final Location obj, final boolean b) {
        try {
            getSetIsFromMockProviderMethod().invoke(obj, b);
        }
        catch (final InvocationTargetException cause) {
            throw new RuntimeException(cause);
        }
        catch (final IllegalAccessException cause2) {
            final IllegalAccessError illegalAccessError = new IllegalAccessError();
            illegalAccessError.initCause(cause2);
            throw illegalAccessError;
        }
        catch (final NoSuchMethodException cause3) {
            final NoSuchMethodError noSuchMethodError = new NoSuchMethodError();
            noSuchMethodError.initCause(cause3);
            throw noSuchMethodError;
        }
    }
    
    public static void setMslAltitudeAccuracyMeters(@NonNull final Location location, @FloatRange(from = 0.0) final float n) {
        getOrCreateExtras(location).putFloat("androidx.core.location.extra.MSL_ALTITUDE_ACCURACY", n);
    }
    
    public static void setMslAltitudeMeters(@NonNull final Location location, final double n) {
        ((BaseBundle)getOrCreateExtras(location)).putDouble("androidx.core.location.extra.MSL_ALTITUDE", n);
    }
    
    public static void setSpeedAccuracyMetersPerSecond(@NonNull final Location location, final float n) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setSpeedAccuracyMetersPerSecond(location, n);
        }
        else {
            getOrCreateExtras(location).putFloat("speedAccuracy", n);
        }
    }
    
    public static void setVerticalAccuracyMeters(@NonNull final Location location, final float n) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setVerticalAccuracyMeters(location, n);
        }
        else {
            getOrCreateExtras(location).putFloat("verticalAccuracy", n);
        }
    }
    
    @RequiresApi(17)
    private static class Api17Impl
    {
        @DoNotInline
        static long getElapsedRealtimeNanos(final Location location) {
            return location.getElapsedRealtimeNanos();
        }
    }
    
    @RequiresApi(18)
    private static class Api18Impl
    {
        @DoNotInline
        static boolean isMock(final Location location) {
            return location.isFromMockProvider();
        }
    }
    
    @RequiresApi(26)
    private static class Api26Impl
    {
        @DoNotInline
        static float getBearingAccuracyDegrees(final Location location) {
            return \u3007O888o0o.\u3007080(location);
        }
        
        @DoNotInline
        static float getSpeedAccuracyMetersPerSecond(final Location location) {
            return \u3007O00.\u3007080(location);
        }
        
        @DoNotInline
        static float getVerticalAccuracyMeters(final Location location) {
            return o\u3007O8\u3007\u3007o.\u3007080(location);
        }
        
        @DoNotInline
        static boolean hasBearingAccuracy(final Location location) {
            return o800o8O.\u3007080(location);
        }
        
        @DoNotInline
        static boolean hasSpeedAccuracy(final Location location) {
            return oo88o8O.\u3007080(location);
        }
        
        @DoNotInline
        static boolean hasVerticalAccuracy(final Location location) {
            return OoO8.\u3007080(location);
        }
        
        @DoNotInline
        static void setBearingAccuracyDegrees(final Location location, final float n) {
            \u3007oo\u3007.\u3007080(location, n);
        }
        
        @DoNotInline
        static void setSpeedAccuracyMetersPerSecond(final Location location, final float n) {
            \u3007\u30078O0\u30078.\u3007080(location, n);
        }
        
        @DoNotInline
        static void setVerticalAccuracyMeters(final Location location, final float n) {
            \u30070\u3007O0088o.\u3007080(location, n);
        }
    }
}

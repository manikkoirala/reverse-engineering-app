// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.location;

import java.util.List;
import androidx.core.util.ObjectsCompat;
import java.util.concurrent.RejectedExecutionException;
import android.location.GpsStatus;
import android.location.GnssStatus;
import android.annotation.SuppressLint;
import android.os.Bundle;
import java.util.Objects;
import android.location.GnssStatus$Callback;
import androidx.core.util.Preconditions;
import androidx.annotation.DoNotInline;
import android.location.LocationRequest;
import androidx.collection.SimpleArrayMap;
import java.util.Iterator;
import java.util.ArrayList;
import java.lang.reflect.InvocationTargetException;
import androidx.annotation.RequiresApi;
import androidx.core.os.ExecutorCompat;
import android.os.Handler;
import android.location.GnssMeasurementsEvent$Callback;
import android.location.GpsStatus$Listener;
import androidx.annotation.RequiresPermission;
import android.location.LocationListener;
import android.os.Looper;
import android.os.SystemClock;
import android.os.Build$VERSION;
import android.location.Location;
import androidx.core.util.Consumer;
import java.util.concurrent.Executor;
import androidx.annotation.Nullable;
import androidx.core.os.CancellationSignal;
import androidx.annotation.NonNull;
import android.location.LocationManager;
import androidx.annotation.GuardedBy;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;
import java.lang.reflect.Method;
import java.lang.reflect.Field;

public final class LocationManagerCompat
{
    private static final long GET_CURRENT_LOCATION_TIMEOUT_MS = 30000L;
    private static final long MAX_CURRENT_LOCATION_AGE_MS = 10000L;
    private static final long PRE_N_LOOPER_TIMEOUT_S = 5L;
    private static Field sContextField;
    private static Method sGnssRequestBuilderBuildMethod;
    private static Class<?> sGnssRequestBuilderClass;
    @GuardedBy("sLocationListeners")
    static final WeakHashMap<LocationListenerKey, WeakReference<LocationListenerTransport>> sLocationListeners;
    private static Method sRegisterGnssMeasurementsCallbackMethod;
    
    static {
        sLocationListeners = new WeakHashMap<LocationListenerKey, WeakReference<LocationListenerTransport>>();
    }
    
    private LocationManagerCompat() {
    }
    
    @RequiresPermission(anyOf = { "android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION" })
    public static void getCurrentLocation(@NonNull final LocationManager locationManager, @NonNull final String s, @Nullable final CancellationSignal cancellationSignal, @NonNull final Executor executor, @NonNull final Consumer<Location> consumer) {
        if (Build$VERSION.SDK_INT >= 30) {
            Api30Impl.getCurrentLocation(locationManager, s, cancellationSignal, executor, consumer);
            return;
        }
        if (cancellationSignal != null) {
            cancellationSignal.throwIfCanceled();
        }
        final Location lastKnownLocation = locationManager.getLastKnownLocation(s);
        if (lastKnownLocation != null && SystemClock.elapsedRealtime() - LocationCompat.getElapsedRealtimeMillis(lastKnownLocation) < 10000L) {
            executor.execute(new O8ooOoo\u3007(consumer, lastKnownLocation));
            return;
        }
        final CancellableLocationListener cancellableLocationListener = new CancellableLocationListener(locationManager, executor, consumer);
        locationManager.requestLocationUpdates(s, 0L, 0.0f, (LocationListener)cancellableLocationListener, Looper.getMainLooper());
        if (cancellationSignal != null) {
            cancellationSignal.setOnCancelListener((CancellationSignal.OnCancelListener)new \u3007oOO8O8(cancellableLocationListener));
        }
        cancellableLocationListener.startTimeout(30000L);
    }
    
    @Nullable
    public static String getGnssHardwareModelName(@NonNull final LocationManager locationManager) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getGnssHardwareModelName(locationManager);
        }
        return null;
    }
    
    public static int getGnssYearOfHardware(@NonNull final LocationManager locationManager) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getGnssYearOfHardware(locationManager);
        }
        return 0;
    }
    
    public static boolean hasProvider(@NonNull final LocationManager locationManager, @NonNull final String s) {
        if (Build$VERSION.SDK_INT >= 31) {
            return Api31Impl.hasProvider(locationManager, s);
        }
        final boolean contains = locationManager.getAllProviders().contains(s);
        boolean b = true;
        if (contains) {
            return true;
        }
        try {
            if (locationManager.getProvider(s) == null) {
                b = false;
            }
            return b;
        }
        catch (final SecurityException ex) {
            return false;
        }
    }
    
    public static boolean isLocationEnabled(@NonNull final LocationManager locationManager) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.isLocationEnabled(locationManager);
        }
        return locationManager.isProviderEnabled("network") || locationManager.isProviderEnabled("gps");
    }
    
    @RequiresApi(24)
    @RequiresPermission("android.permission.ACCESS_FINE_LOCATION")
    public static boolean registerGnssMeasurementsCallback(@NonNull final LocationManager locationManager, @NonNull final GnssMeasurementsEvent$Callback gnssMeasurementsEvent$Callback, @NonNull final Handler handler) {
        if (Build$VERSION.SDK_INT != 30) {
            return Api24Impl.registerGnssMeasurementsCallback(locationManager, gnssMeasurementsEvent$Callback, handler);
        }
        return registerGnssMeasurementsCallbackOnR(locationManager, ExecutorCompat.create(handler), gnssMeasurementsEvent$Callback);
    }
    
    @RequiresApi(30)
    @RequiresPermission("android.permission.ACCESS_FINE_LOCATION")
    public static boolean registerGnssMeasurementsCallback(@NonNull final LocationManager locationManager, @NonNull final Executor executor, @NonNull final GnssMeasurementsEvent$Callback gnssMeasurementsEvent$Callback) {
        if (Build$VERSION.SDK_INT > 30) {
            return Api31Impl.registerGnssMeasurementsCallback(locationManager, executor, gnssMeasurementsEvent$Callback);
        }
        return registerGnssMeasurementsCallbackOnR(locationManager, executor, gnssMeasurementsEvent$Callback);
    }
    
    @RequiresApi(30)
    private static boolean registerGnssMeasurementsCallbackOnR(@NonNull final LocationManager obj, @NonNull final Executor executor, @NonNull final GnssMeasurementsEvent$Callback gnssMeasurementsEvent$Callback) {
        Label_0186: {
            if (Build$VERSION.SDK_INT != 30) {
                break Label_0186;
            }
            final boolean b = false;
            try {
                if (LocationManagerCompat.sGnssRequestBuilderClass == null) {
                    LocationManagerCompat.sGnssRequestBuilderClass = Class.forName("android.location.GnssRequest$Builder");
                }
                if (LocationManagerCompat.sGnssRequestBuilderBuildMethod == null) {
                    (LocationManagerCompat.sGnssRequestBuilderBuildMethod = LocationManagerCompat.sGnssRequestBuilderClass.getDeclaredMethod("build", (Class<?>[])new Class[0])).setAccessible(true);
                }
                if (LocationManagerCompat.sRegisterGnssMeasurementsCallbackMethod == null) {
                    (LocationManagerCompat.sRegisterGnssMeasurementsCallbackMethod = LocationManager.class.getDeclaredMethod("registerGnssMeasurementsCallback", Class.forName("android.location.GnssRequest"), Executor.class, GnssMeasurementsEvent$Callback.class)).setAccessible(true);
                }
                final Object invoke = LocationManagerCompat.sRegisterGnssMeasurementsCallbackMethod.invoke(obj, LocationManagerCompat.sGnssRequestBuilderBuildMethod.invoke(LocationManagerCompat.sGnssRequestBuilderClass.getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]), new Object[0]), executor, gnssMeasurementsEvent$Callback);
                boolean b2 = b;
                if (invoke != null) {
                    final boolean booleanValue = (boolean)invoke;
                    b2 = b;
                    if (booleanValue) {
                        b2 = true;
                    }
                }
                return b2;
                throw new IllegalStateException();
            }
            catch (final ClassNotFoundException | InvocationTargetException | NoSuchMethodException | IllegalAccessException | InstantiationException ex) {
                return b;
            }
        }
    }
    
    @RequiresPermission("android.permission.ACCESS_FINE_LOCATION")
    private static boolean registerGnssStatusCallback(final LocationManager p0, final Handler p1, final Executor p2, final GnssStatusCompat.Callback p3) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: istore          4
        //     5: iload           4
        //     7: bipush          30
        //     9: if_icmplt       20
        //    12: aload_0        
        //    13: aload_1        
        //    14: aload_2        
        //    15: aload_3        
        //    16: invokestatic    androidx/core/location/LocationManagerCompat$Api30Impl.registerGnssStatusCallback:(Landroid/location/LocationManager;Landroid/os/Handler;Ljava/util/concurrent/Executor;Landroidx/core/location/GnssStatusCompat$Callback;)Z
        //    19: ireturn        
        //    20: iload           4
        //    22: bipush          24
        //    24: if_icmplt       35
        //    27: aload_0        
        //    28: aload_1        
        //    29: aload_2        
        //    30: aload_3        
        //    31: invokestatic    androidx/core/location/LocationManagerCompat$Api24Impl.registerGnssStatusCallback:(Landroid/location/LocationManager;Landroid/os/Handler;Ljava/util/concurrent/Executor;Landroidx/core/location/GnssStatusCompat$Callback;)Z
        //    34: ireturn        
        //    35: iconst_1       
        //    36: istore          7
        //    38: iconst_1       
        //    39: istore          5
        //    41: iconst_1       
        //    42: istore          6
        //    44: aload_1        
        //    45: ifnull          54
        //    48: iconst_1       
        //    49: istore          8
        //    51: goto            57
        //    54: iconst_0       
        //    55: istore          8
        //    57: iload           8
        //    59: invokestatic    androidx/core/util/Preconditions.checkArgument:(Z)V
        //    62: getstatic       androidx/core/location/LocationManagerCompat$GnssListenersHolder.sGnssStatusListeners:Landroidx/collection/SimpleArrayMap;
        //    65: astore          16
        //    67: aload           16
        //    69: monitorenter   
        //    70: aload           16
        //    72: aload_3        
        //    73: invokevirtual   androidx/collection/SimpleArrayMap.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //    76: checkcast       Landroidx/core/location/LocationManagerCompat$GpsStatusTransport;
        //    79: astore          15
        //    81: aload           15
        //    83: ifnonnull       101
        //    86: new             Landroidx/core/location/LocationManagerCompat$GpsStatusTransport;
        //    89: astore          15
        //    91: aload           15
        //    93: aload_0        
        //    94: aload_3        
        //    95: invokespecial   androidx/core/location/LocationManagerCompat$GpsStatusTransport.<init>:(Landroid/location/LocationManager;Landroidx/core/location/GnssStatusCompat$Callback;)V
        //    98: goto            106
        //   101: aload           15
        //   103: invokevirtual   androidx/core/location/LocationManagerCompat$GpsStatusTransport.unregister:()V
        //   106: aload           15
        //   108: aload_2        
        //   109: invokevirtual   androidx/core/location/LocationManagerCompat$GpsStatusTransport.register:(Ljava/util/concurrent/Executor;)V
        //   112: new             Ljava/util/concurrent/FutureTask;
        //   115: astore_2       
        //   116: new             Landroidx/core/location/O\u30078O8\u3007008;
        //   119: astore          17
        //   121: aload           17
        //   123: aload_0        
        //   124: aload           15
        //   126: invokespecial   androidx/core/location/O\u30078O8\u3007008.<init>:(Landroid/location/LocationManager;Landroidx/core/location/LocationManagerCompat$GpsStatusTransport;)V
        //   129: aload_2        
        //   130: aload           17
        //   132: invokespecial   java/util/concurrent/FutureTask.<init>:(Ljava/util/concurrent/Callable;)V
        //   135: invokestatic    android/os/Looper.myLooper:()Landroid/os/Looper;
        //   138: aload_1        
        //   139: invokevirtual   android/os/Handler.getLooper:()Landroid/os/Looper;
        //   142: if_acmpne       152
        //   145: aload_2        
        //   146: invokevirtual   java/util/concurrent/FutureTask.run:()V
        //   149: goto            164
        //   152: aload_1        
        //   153: aload_2        
        //   154: invokevirtual   android/os/Handler.post:(Ljava/lang/Runnable;)Z
        //   157: istore          8
        //   159: iload           8
        //   161: ifeq            478
        //   164: getstatic       java/util/concurrent/TimeUnit.SECONDS:Ljava/util/concurrent/TimeUnit;
        //   167: ldc2_w          5
        //   170: invokevirtual   java/util/concurrent/TimeUnit.toNanos:(J)J
        //   173: lstore          11
        //   175: invokestatic    java/lang/System.nanoTime:()J
        //   178: lstore          13
        //   180: iconst_0       
        //   181: istore          4
        //   183: lload           11
        //   185: lstore          9
        //   187: aload_2        
        //   188: lload           9
        //   190: getstatic       java/util/concurrent/TimeUnit.NANOSECONDS:Ljava/util/concurrent/TimeUnit;
        //   193: invokevirtual   java/util/concurrent/FutureTask.get:(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
        //   196: checkcast       Ljava/lang/Boolean;
        //   199: invokevirtual   java/lang/Boolean.booleanValue:()Z
        //   202: ifeq            231
        //   205: getstatic       androidx/core/location/LocationManagerCompat$GnssListenersHolder.sGnssStatusListeners:Landroidx/collection/SimpleArrayMap;
        //   208: aload_3        
        //   209: aload           15
        //   211: invokevirtual   androidx/collection/SimpleArrayMap.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   214: pop            
        //   215: iload           4
        //   217: ifeq            226
        //   220: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   223: invokevirtual   java/lang/Thread.interrupt:()V
        //   226: aload           16
        //   228: monitorexit    
        //   229: iconst_1       
        //   230: ireturn        
        //   231: iload           4
        //   233: ifeq            242
        //   236: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   239: invokevirtual   java/lang/Thread.interrupt:()V
        //   242: aload           16
        //   244: monitorexit    
        //   245: iconst_0       
        //   246: ireturn        
        //   247: astore_0       
        //   248: goto            465
        //   251: astore_0       
        //   252: iload           4
        //   254: istore          5
        //   256: goto            320
        //   259: astore_0       
        //   260: iload           4
        //   262: istore          5
        //   264: goto            389
        //   267: astore_0       
        //   268: iload           5
        //   270: istore          4
        //   272: invokestatic    java/lang/System.nanoTime:()J
        //   275: lstore          9
        //   277: lload           13
        //   279: lload           11
        //   281: ladd           
        //   282: lload           9
        //   284: lsub           
        //   285: lstore          9
        //   287: iconst_1       
        //   288: istore          4
        //   290: goto            187
        //   293: astore_0       
        //   294: iload           6
        //   296: istore          5
        //   298: goto            320
        //   301: astore_0       
        //   302: iload           7
        //   304: istore          5
        //   306: goto            389
        //   309: astore_0       
        //   310: iconst_0       
        //   311: istore          4
        //   313: goto            465
        //   316: astore_0       
        //   317: iconst_0       
        //   318: istore          5
        //   320: iload           5
        //   322: istore          4
        //   324: new             Ljava/lang/IllegalStateException;
        //   327: astore_3       
        //   328: iload           5
        //   330: istore          4
        //   332: new             Ljava/lang/StringBuilder;
        //   335: astore_2       
        //   336: iload           5
        //   338: istore          4
        //   340: aload_2        
        //   341: invokespecial   java/lang/StringBuilder.<init>:()V
        //   344: iload           5
        //   346: istore          4
        //   348: aload_2        
        //   349: aload_1        
        //   350: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   353: pop            
        //   354: iload           5
        //   356: istore          4
        //   358: aload_2        
        //   359: ldc_w           " appears to be blocked, please run registerGnssStatusCallback() directly on a Looper thread or ensure the main Looper is not blocked by this thread"
        //   362: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   365: pop            
        //   366: iload           5
        //   368: istore          4
        //   370: aload_3        
        //   371: aload_2        
        //   372: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   375: aload_0        
        //   376: invokespecial   java/lang/IllegalStateException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   379: iload           5
        //   381: istore          4
        //   383: aload_3        
        //   384: athrow         
        //   385: astore_0       
        //   386: iconst_0       
        //   387: istore          5
        //   389: iload           5
        //   391: istore          4
        //   393: aload_0        
        //   394: invokevirtual   java/lang/Throwable.getCause:()Ljava/lang/Throwable;
        //   397: instanceof      Ljava/lang/RuntimeException;
        //   400: ifne            452
        //   403: iload           5
        //   405: istore          4
        //   407: aload_0        
        //   408: invokevirtual   java/lang/Throwable.getCause:()Ljava/lang/Throwable;
        //   411: instanceof      Ljava/lang/Error;
        //   414: ifeq            429
        //   417: iload           5
        //   419: istore          4
        //   421: aload_0        
        //   422: invokevirtual   java/lang/Throwable.getCause:()Ljava/lang/Throwable;
        //   425: checkcast       Ljava/lang/Error;
        //   428: athrow         
        //   429: iload           5
        //   431: istore          4
        //   433: new             Ljava/lang/IllegalStateException;
        //   436: astore_1       
        //   437: iload           5
        //   439: istore          4
        //   441: aload_1        
        //   442: aload_0        
        //   443: invokespecial   java/lang/IllegalStateException.<init>:(Ljava/lang/Throwable;)V
        //   446: iload           5
        //   448: istore          4
        //   450: aload_1        
        //   451: athrow         
        //   452: iload           5
        //   454: istore          4
        //   456: aload_0        
        //   457: invokevirtual   java/lang/Throwable.getCause:()Ljava/lang/Throwable;
        //   460: checkcast       Ljava/lang/RuntimeException;
        //   463: athrow         
        //   464: astore_0       
        //   465: iload           4
        //   467: ifeq            476
        //   470: invokestatic    java/lang/Thread.currentThread:()Ljava/lang/Thread;
        //   473: invokevirtual   java/lang/Thread.interrupt:()V
        //   476: aload_0        
        //   477: athrow         
        //   478: new             Ljava/lang/IllegalStateException;
        //   481: astore_0       
        //   482: new             Ljava/lang/StringBuilder;
        //   485: astore_2       
        //   486: aload_2        
        //   487: invokespecial   java/lang/StringBuilder.<init>:()V
        //   490: aload_2        
        //   491: aload_1        
        //   492: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   495: pop            
        //   496: aload_2        
        //   497: ldc_w           " is shutting down"
        //   500: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   503: pop            
        //   504: aload_0        
        //   505: aload_2        
        //   506: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   509: invokespecial   java/lang/IllegalStateException.<init>:(Ljava/lang/String;)V
        //   512: aload_0        
        //   513: athrow         
        //   514: astore_0       
        //   515: aload           16
        //   517: monitorexit    
        //   518: aload_0        
        //   519: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                     
        //  -----  -----  -----  -----  -----------------------------------------
        //  70     81     514    520    Any
        //  86     98     514    520    Any
        //  101    106    514    520    Any
        //  106    149    514    520    Any
        //  152    159    514    520    Any
        //  164    180    385    389    Ljava/util/concurrent/ExecutionException;
        //  164    180    316    320    Ljava/util/concurrent/TimeoutException;
        //  164    180    309    316    Any
        //  187    215    267    309    Ljava/lang/InterruptedException;
        //  187    215    259    267    Ljava/util/concurrent/ExecutionException;
        //  187    215    251    259    Ljava/util/concurrent/TimeoutException;
        //  187    215    247    251    Any
        //  220    226    514    520    Any
        //  226    229    514    520    Any
        //  236    242    514    520    Any
        //  242    245    514    520    Any
        //  272    277    301    309    Ljava/util/concurrent/ExecutionException;
        //  272    277    293    301    Ljava/util/concurrent/TimeoutException;
        //  272    277    464    465    Any
        //  324    328    464    465    Any
        //  332    336    464    465    Any
        //  340    344    464    465    Any
        //  348    354    464    465    Any
        //  358    366    464    465    Any
        //  370    379    464    465    Any
        //  383    385    464    465    Any
        //  393    403    464    465    Any
        //  407    417    464    465    Any
        //  421    429    464    465    Any
        //  433    437    464    465    Any
        //  441    446    464    465    Any
        //  450    452    464    465    Any
        //  456    464    464    465    Any
        //  470    476    514    520    Any
        //  476    478    514    520    Any
        //  478    514    514    520    Any
        //  515    518    514    520    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0320:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @RequiresPermission("android.permission.ACCESS_FINE_LOCATION")
    public static boolean registerGnssStatusCallback(@NonNull final LocationManager locationManager, @NonNull final GnssStatusCompat.Callback callback, @NonNull final Handler handler) {
        if (Build$VERSION.SDK_INT >= 30) {
            return registerGnssStatusCallback(locationManager, ExecutorCompat.create(handler), callback);
        }
        return registerGnssStatusCallback(locationManager, new InlineHandlerExecutor(handler), callback);
    }
    
    @RequiresPermission("android.permission.ACCESS_FINE_LOCATION")
    public static boolean registerGnssStatusCallback(@NonNull final LocationManager locationManager, @NonNull final Executor executor, @NonNull final GnssStatusCompat.Callback callback) {
        if (Build$VERSION.SDK_INT >= 30) {
            return registerGnssStatusCallback(locationManager, null, executor, callback);
        }
        Looper looper;
        if ((looper = Looper.myLooper()) == null) {
            looper = Looper.getMainLooper();
        }
        return registerGnssStatusCallback(locationManager, new Handler(looper), executor, callback);
    }
    
    @GuardedBy("sLocationListeners")
    @RequiresPermission(anyOf = { "android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION" })
    static void registerLocationListenerTransport(final LocationManager locationManager, LocationListenerTransport referent) {
        final WeakReference<LocationListenerTransport> weakReference = LocationManagerCompat.sLocationListeners.put(referent.getKey(), new WeakReference<LocationListenerTransport>(referent));
        if (weakReference != null) {
            referent = weakReference.get();
        }
        else {
            referent = null;
        }
        if (referent != null) {
            referent.unregister();
            locationManager.removeUpdates((LocationListener)referent);
        }
    }
    
    @RequiresPermission(anyOf = { "android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION" })
    public static void removeUpdates(@NonNull final LocationManager locationManager, @NonNull final LocationListenerCompat locationListenerCompat) {
        final WeakHashMap<LocationListenerKey, WeakReference<LocationListenerTransport>> sLocationListeners = LocationManagerCompat.sLocationListeners;
        synchronized (sLocationListeners) {
            final Iterator<WeakReference<LocationListenerTransport>> iterator = sLocationListeners.values().iterator();
            ArrayList list = null;
            while (iterator.hasNext()) {
                final LocationListenerTransport locationListenerTransport = iterator.next().get();
                if (locationListenerTransport == null) {
                    continue;
                }
                final LocationListenerKey key = locationListenerTransport.getKey();
                if (key.mListener != locationListenerCompat) {
                    continue;
                }
                ArrayList list2;
                if ((list2 = list) == null) {
                    list2 = new ArrayList();
                }
                list2.add(key);
                locationListenerTransport.unregister();
                locationManager.removeUpdates((LocationListener)locationListenerTransport);
                list = list2;
            }
            if (list != null) {
                final Iterator iterator2 = list.iterator();
                while (iterator2.hasNext()) {
                    LocationManagerCompat.sLocationListeners.remove(iterator2.next());
                }
            }
            monitorexit(sLocationListeners);
            locationManager.removeUpdates((LocationListener)locationListenerCompat);
        }
    }
    
    @RequiresPermission(anyOf = { "android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION" })
    public static void requestLocationUpdates(@NonNull final LocationManager locationManager, @NonNull final String s, @NonNull final LocationRequestCompat locationRequestCompat, @NonNull final LocationListenerCompat locationListenerCompat, @NonNull final Looper looper) {
        if (Build$VERSION.SDK_INT >= 31) {
            Api31Impl.requestLocationUpdates(locationManager, s, locationRequestCompat.toLocationRequest(), ExecutorCompat.create(new Handler(looper)), (LocationListener)locationListenerCompat);
            return;
        }
        if (Api19Impl.tryRequestLocationUpdates(locationManager, s, locationRequestCompat, locationListenerCompat, looper)) {
            return;
        }
        locationManager.requestLocationUpdates(s, locationRequestCompat.getIntervalMillis(), locationRequestCompat.getMinUpdateDistanceMeters(), (LocationListener)locationListenerCompat, looper);
    }
    
    @RequiresPermission(anyOf = { "android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION" })
    public static void requestLocationUpdates(@NonNull final LocationManager locationManager, @NonNull final String s, @NonNull final LocationRequestCompat locationRequestCompat, @NonNull final Executor executor, @NonNull final LocationListenerCompat locationListenerCompat) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 31) {
            Api31Impl.requestLocationUpdates(locationManager, s, locationRequestCompat.toLocationRequest(), executor, (LocationListener)locationListenerCompat);
            return;
        }
        if (sdk_INT >= 30 && Api30Impl.tryRequestLocationUpdates(locationManager, s, locationRequestCompat, executor, locationListenerCompat)) {
            return;
        }
        final LocationListenerTransport locationListenerTransport = new LocationListenerTransport(new LocationListenerKey(s, locationListenerCompat), executor);
        if (Api19Impl.tryRequestLocationUpdates(locationManager, s, locationRequestCompat, locationListenerTransport)) {
            return;
        }
        synchronized (LocationManagerCompat.sLocationListeners) {
            locationManager.requestLocationUpdates(s, locationRequestCompat.getIntervalMillis(), locationRequestCompat.getMinUpdateDistanceMeters(), (LocationListener)locationListenerTransport, Looper.getMainLooper());
            registerLocationListenerTransport(locationManager, locationListenerTransport);
        }
    }
    
    @RequiresApi(24)
    public static void unregisterGnssMeasurementsCallback(@NonNull final LocationManager locationManager, @NonNull final GnssMeasurementsEvent$Callback gnssMeasurementsEvent$Callback) {
        Api24Impl.unregisterGnssMeasurementsCallback(locationManager, gnssMeasurementsEvent$Callback);
    }
    
    public static void unregisterGnssStatusCallback(@NonNull final LocationManager locationManager, @NonNull final GnssStatusCompat.Callback callback) {
        if (Build$VERSION.SDK_INT >= 24) {
            final SimpleArrayMap<Object, Object> simpleArrayMap = GnssListenersHolder.sGnssStatusListeners;
            synchronized (simpleArrayMap) {
                final Object remove = simpleArrayMap.remove(callback);
                if (remove != null) {
                    Api24Impl.unregisterGnssStatusCallback(locationManager, remove);
                }
                return;
            }
        }
        final SimpleArrayMap<Object, Object> simpleArrayMap = GnssListenersHolder.sGnssStatusListeners;
        synchronized (simpleArrayMap) {
            final GpsStatusTransport gpsStatusTransport = simpleArrayMap.remove(callback);
            if (gpsStatusTransport != null) {
                gpsStatusTransport.unregister();
                locationManager.removeGpsStatusListener((GpsStatus$Listener)gpsStatusTransport);
            }
        }
    }
    
    @RequiresApi(19)
    static class Api19Impl
    {
        private static Class<?> sLocationRequestClass;
        private static Method sRequestLocationUpdatesLooperMethod;
        
        private Api19Impl() {
        }
        
        @DoNotInline
        static boolean tryRequestLocationUpdates(final LocationManager obj, final String s, final LocationRequestCompat locationRequestCompat, final LocationListenerCompat locationListenerCompat, final Looper looper) {
            try {
                if (Api19Impl.sLocationRequestClass == null) {
                    Api19Impl.sLocationRequestClass = Class.forName("android.location.LocationRequest");
                }
                if (Api19Impl.sRequestLocationUpdatesLooperMethod == null) {
                    (Api19Impl.sRequestLocationUpdatesLooperMethod = LocationManager.class.getDeclaredMethod("requestLocationUpdates", Api19Impl.sLocationRequestClass, LocationListener.class, Looper.class)).setAccessible(true);
                }
                final LocationRequest locationRequest = locationRequestCompat.toLocationRequest(s);
                if (locationRequest != null) {
                    Api19Impl.sRequestLocationUpdatesLooperMethod.invoke(obj, locationRequest, locationListenerCompat, looper);
                    return true;
                }
                return false;
            }
            catch (final NoSuchMethodException | InvocationTargetException | IllegalAccessException | ClassNotFoundException | UnsupportedOperationException ex) {
                return false;
            }
        }
        
        @DoNotInline
        @RequiresPermission(anyOf = { "android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION" })
        static boolean tryRequestLocationUpdates(final LocationManager obj, final String s, final LocationRequestCompat locationRequestCompat, final LocationListenerTransport locationListenerTransport) {
            try {
                if (Api19Impl.sLocationRequestClass == null) {
                    Api19Impl.sLocationRequestClass = Class.forName("android.location.LocationRequest");
                }
                if (Api19Impl.sRequestLocationUpdatesLooperMethod == null) {
                    (Api19Impl.sRequestLocationUpdatesLooperMethod = LocationManager.class.getDeclaredMethod("requestLocationUpdates", Api19Impl.sLocationRequestClass, LocationListener.class, Looper.class)).setAccessible(true);
                }
                final LocationRequest locationRequest = locationRequestCompat.toLocationRequest(s);
                if (locationRequest != null) {
                    synchronized (LocationManagerCompat.sLocationListeners) {
                        Api19Impl.sRequestLocationUpdatesLooperMethod.invoke(obj, locationRequest, locationListenerTransport, Looper.getMainLooper());
                        LocationManagerCompat.registerLocationListenerTransport(obj, locationListenerTransport);
                        return true;
                    }
                }
                return false;
            }
            catch (final NoSuchMethodException | InvocationTargetException | IllegalAccessException | ClassNotFoundException | UnsupportedOperationException ex) {
                return false;
            }
        }
    }
    
    @RequiresApi(24)
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_FINE_LOCATION")
        static boolean registerGnssMeasurementsCallback(@NonNull final LocationManager locationManager, @NonNull final GnssMeasurementsEvent$Callback gnssMeasurementsEvent$Callback, @NonNull final Handler handler) {
            return oo\u3007.\u3007080(locationManager, gnssMeasurementsEvent$Callback, handler);
        }
        
        @DoNotInline
        @RequiresPermission(anyOf = { "android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION" })
        static boolean registerGnssStatusCallback(final LocationManager locationManager, final Handler handler, final Executor executor, final GnssStatusCompat.Callback callback) {
            Preconditions.checkArgument(handler != null);
            final SimpleArrayMap<Object, Object> sGnssStatusListeners = GnssListenersHolder.sGnssStatusListeners;
            synchronized (sGnssStatusListeners) {
                PreRGnssStatusTransport preRGnssStatusTransport = sGnssStatusListeners.get(callback);
                if (preRGnssStatusTransport == null) {
                    preRGnssStatusTransport = new PreRGnssStatusTransport(callback);
                }
                else {
                    preRGnssStatusTransport.unregister();
                }
                preRGnssStatusTransport.register(executor);
                if (OOO\u3007O0.\u3007080(locationManager, (GnssStatus$Callback)preRGnssStatusTransport, handler)) {
                    sGnssStatusListeners.put(callback, preRGnssStatusTransport);
                    return true;
                }
                return false;
            }
        }
        
        @DoNotInline
        static void unregisterGnssMeasurementsCallback(@NonNull final LocationManager locationManager, @NonNull final GnssMeasurementsEvent$Callback gnssMeasurementsEvent$Callback) {
            \u30070000OOO.\u3007080(locationManager, gnssMeasurementsEvent$Callback);
        }
        
        @DoNotInline
        static void unregisterGnssStatusCallback(final LocationManager locationManager, final Object o) {
            if (o instanceof PreRGnssStatusTransport) {
                ((PreRGnssStatusTransport)o).unregister();
            }
            o\u3007\u30070\u3007.\u3007080(locationManager, (GnssStatus$Callback)o);
        }
    }
    
    @RequiresApi(28)
    private static class Api28Impl
    {
        @DoNotInline
        static String getGnssHardwareModelName(final LocationManager locationManager) {
            return \u3007o.\u3007080(locationManager);
        }
        
        @DoNotInline
        static int getGnssYearOfHardware(final LocationManager locationManager) {
            return O8\u3007o.\u3007080(locationManager);
        }
        
        @DoNotInline
        static boolean isLocationEnabled(final LocationManager locationManager) {
            return \u300700\u30078.\u3007080(locationManager);
        }
    }
    
    @RequiresApi(30)
    private static class Api30Impl
    {
        private static Class<?> sLocationRequestClass;
        private static Method sRequestLocationUpdatesExecutorMethod;
        
        @DoNotInline
        @RequiresPermission(anyOf = { "android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION" })
        static void getCurrentLocation(final LocationManager locationManager, @NonNull final String s, @Nullable final CancellationSignal cancellationSignal, @NonNull final Executor executor, @NonNull final Consumer<Location> obj) {
            android.os.CancellationSignal cancellationSignal2;
            if (cancellationSignal != null) {
                cancellationSignal2 = (android.os.CancellationSignal)cancellationSignal.getCancellationSignalObject();
            }
            else {
                cancellationSignal2 = null;
            }
            Objects.requireNonNull(obj);
            o\u30078.\u3007080(locationManager, s, cancellationSignal2, executor, (java.util.function.Consumer)new Oo8Oo00oo(obj));
        }
        
        @DoNotInline
        @RequiresPermission(anyOf = { "android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION" })
        public static boolean registerGnssStatusCallback(final LocationManager locationManager, final Handler handler, final Executor executor, final GnssStatusCompat.Callback callback) {
            final SimpleArrayMap<Object, Object> sGnssStatusListeners = GnssListenersHolder.sGnssStatusListeners;
            synchronized (sGnssStatusListeners) {
                GnssStatusTransport gnssStatusTransport;
                if ((gnssStatusTransport = sGnssStatusListeners.get(callback)) == null) {
                    gnssStatusTransport = new GnssStatusTransport(callback);
                }
                if (o0ooO.\u3007080(locationManager, executor, (GnssStatus$Callback)gnssStatusTransport)) {
                    sGnssStatusListeners.put(callback, gnssStatusTransport);
                    return true;
                }
                return false;
            }
        }
        
        @DoNotInline
        public static boolean tryRequestLocationUpdates(final LocationManager obj, final String s, final LocationRequestCompat locationRequestCompat, final Executor executor, final LocationListenerCompat locationListenerCompat) {
            if (Build$VERSION.SDK_INT < 30) {
                return false;
            }
            try {
                if (Api30Impl.sLocationRequestClass == null) {
                    Api30Impl.sLocationRequestClass = Class.forName("android.location.LocationRequest");
                }
                if (Api30Impl.sRequestLocationUpdatesExecutorMethod == null) {
                    (Api30Impl.sRequestLocationUpdatesExecutorMethod = LocationManager.class.getDeclaredMethod("requestLocationUpdates", Api30Impl.sLocationRequestClass, Executor.class, LocationListener.class)).setAccessible(true);
                }
                final LocationRequest locationRequest = locationRequestCompat.toLocationRequest(s);
                if (locationRequest != null) {
                    Api30Impl.sRequestLocationUpdatesExecutorMethod.invoke(obj, locationRequest, executor, locationListenerCompat);
                    return true;
                }
                return false;
            }
            catch (final NoSuchMethodException | InvocationTargetException | IllegalAccessException | ClassNotFoundException | UnsupportedOperationException ex) {
                return false;
            }
        }
    }
    
    @RequiresApi(31)
    private static class Api31Impl
    {
        @DoNotInline
        static boolean hasProvider(final LocationManager locationManager, @NonNull final String s) {
            return \u3007\u30070o.\u3007080(locationManager, s);
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_FINE_LOCATION")
        static boolean registerGnssMeasurementsCallback(@NonNull final LocationManager locationManager, @NonNull final Executor executor, @NonNull final GnssMeasurementsEvent$Callback gnssMeasurementsEvent$Callback) {
            return \u3007\u3007\u30070\u3007\u30070.\u3007080(locationManager, executor, gnssMeasurementsEvent$Callback);
        }
        
        @DoNotInline
        @RequiresPermission(anyOf = { "android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION" })
        static void requestLocationUpdates(final LocationManager locationManager, @NonNull final String s, @NonNull final LocationRequest locationRequest, @NonNull final Executor executor, @NonNull final LocationListener locationListener) {
            o\u30070OOo\u30070.\u3007080(locationManager, s, locationRequest, executor, locationListener);
        }
    }
    
    private static final class CancellableLocationListener implements LocationListener
    {
        private Consumer<Location> mConsumer;
        private final Executor mExecutor;
        private final LocationManager mLocationManager;
        private final Handler mTimeoutHandler;
        @Nullable
        Runnable mTimeoutRunnable;
        @GuardedBy("this")
        private boolean mTriggered;
        
        CancellableLocationListener(final LocationManager mLocationManager, final Executor mExecutor, final Consumer<Location> mConsumer) {
            this.mLocationManager = mLocationManager;
            this.mExecutor = mExecutor;
            this.mTimeoutHandler = new Handler(Looper.getMainLooper());
            this.mConsumer = mConsumer;
        }
        
        @RequiresPermission(anyOf = { "android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION" })
        private void cleanup() {
            this.mConsumer = null;
            this.mLocationManager.removeUpdates((LocationListener)this);
            final Runnable mTimeoutRunnable = this.mTimeoutRunnable;
            if (mTimeoutRunnable != null) {
                this.mTimeoutHandler.removeCallbacks(mTimeoutRunnable);
                this.mTimeoutRunnable = null;
            }
        }
        
        @RequiresPermission(anyOf = { "android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION" })
        public void cancel() {
            synchronized (this) {
                if (this.mTriggered) {
                    return;
                }
                this.mTriggered = true;
                monitorexit(this);
                this.cleanup();
            }
        }
        
        @RequiresPermission(anyOf = { "android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION" })
        public void onLocationChanged(@Nullable final Location location) {
            synchronized (this) {
                if (this.mTriggered) {
                    return;
                }
                this.mTriggered = true;
                monitorexit(this);
                this.mExecutor.execute(new oO(this.mConsumer, location));
                this.cleanup();
            }
        }
        
        @RequiresPermission(anyOf = { "android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION" })
        public void onProviderDisabled(@NonNull final String s) {
            this.onLocationChanged(null);
        }
        
        public void onProviderEnabled(@NonNull final String s) {
        }
        
        public void onStatusChanged(final String s, final int n, final Bundle bundle) {
        }
        
        @SuppressLint({ "MissingPermission" })
        public void startTimeout(final long n) {
            synchronized (this) {
                if (this.mTriggered) {
                    return;
                }
                final \u300708O8o\u30070 mTimeoutRunnable = new \u300708O8o\u30070(this);
                this.mTimeoutRunnable = mTimeoutRunnable;
                this.mTimeoutHandler.postDelayed((Runnable)mTimeoutRunnable, n);
            }
        }
    }
    
    private static class GnssListenersHolder
    {
        @GuardedBy("sGnssStatusListeners")
        static final SimpleArrayMap<Object, Object> sGnssStatusListeners;
        
        static {
            sGnssStatusListeners = new SimpleArrayMap<Object, Object>();
        }
    }
    
    @RequiresApi(30)
    private static class GnssStatusTransport extends GnssStatus$Callback
    {
        final GnssStatusCompat.Callback mCallback;
        
        GnssStatusTransport(final GnssStatusCompat.Callback mCallback) {
            Preconditions.checkArgument(mCallback != null, (Object)"invalid null callback");
            this.mCallback = mCallback;
        }
        
        public void onFirstFix(final int n) {
            this.mCallback.onFirstFix(n);
        }
        
        public void onSatelliteStatusChanged(final GnssStatus gnssStatus) {
            this.mCallback.onSatelliteStatusChanged(GnssStatusCompat.wrap(gnssStatus));
        }
        
        public void onStarted() {
            this.mCallback.onStarted();
        }
        
        public void onStopped() {
            this.mCallback.onStopped();
        }
    }
    
    private static class GpsStatusTransport implements GpsStatus$Listener
    {
        final GnssStatusCompat.Callback mCallback;
        @Nullable
        volatile Executor mExecutor;
        private final LocationManager mLocationManager;
        
        GpsStatusTransport(final LocationManager mLocationManager, final GnssStatusCompat.Callback mCallback) {
            Preconditions.checkArgument(mCallback != null, (Object)"invalid null callback");
            this.mLocationManager = mLocationManager;
            this.mCallback = mCallback;
        }
        
        @RequiresPermission("android.permission.ACCESS_FINE_LOCATION")
        public void onGpsStatusChanged(final int n) {
            final Executor mExecutor = this.mExecutor;
            if (mExecutor == null) {
                return;
            }
            if (n != 1) {
                if (n != 2) {
                    if (n != 3) {
                        if (n == 4) {
                            final GpsStatus gpsStatus = this.mLocationManager.getGpsStatus((GpsStatus)null);
                            if (gpsStatus != null) {
                                mExecutor.execute(new O\u3007O\u3007oO(this, mExecutor, GnssStatusCompat.wrap(gpsStatus)));
                            }
                        }
                    }
                    else {
                        final GpsStatus gpsStatus2 = this.mLocationManager.getGpsStatus((GpsStatus)null);
                        if (gpsStatus2 != null) {
                            mExecutor.execute(new \u30078\u30070\u3007o\u3007O(this, mExecutor, gpsStatus2.getTimeToFirstFix()));
                        }
                    }
                }
                else {
                    mExecutor.execute(new O08000(this, mExecutor));
                }
            }
            else {
                mExecutor.execute(new \u30078(this, mExecutor));
            }
        }
        
        public void register(final Executor mExecutor) {
            Preconditions.checkState(this.mExecutor == null);
            this.mExecutor = mExecutor;
        }
        
        public void unregister() {
            this.mExecutor = null;
        }
    }
    
    private static final class InlineHandlerExecutor implements Executor
    {
        private final Handler mHandler;
        
        InlineHandlerExecutor(@NonNull final Handler handler) {
            this.mHandler = Preconditions.checkNotNull(handler);
        }
        
        @Override
        public void execute(@NonNull final Runnable runnable) {
            if (Looper.myLooper() == this.mHandler.getLooper()) {
                runnable.run();
            }
            else if (!this.mHandler.post((Runnable)Preconditions.checkNotNull(runnable))) {
                final StringBuilder sb = new StringBuilder();
                sb.append(this.mHandler);
                sb.append(" is shutting down");
                throw new RejectedExecutionException(sb.toString());
            }
        }
    }
    
    private static class LocationListenerKey
    {
        final LocationListenerCompat mListener;
        final String mProvider;
        
        LocationListenerKey(final String s, final LocationListenerCompat locationListenerCompat) {
            this.mProvider = ObjectsCompat.requireNonNull(s, "invalid null provider");
            this.mListener = ObjectsCompat.requireNonNull(locationListenerCompat, "invalid null listener");
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof LocationListenerKey;
            final boolean b2 = false;
            if (!b) {
                return false;
            }
            final LocationListenerKey locationListenerKey = (LocationListenerKey)o;
            boolean b3 = b2;
            if (this.mProvider.equals(locationListenerKey.mProvider)) {
                b3 = b2;
                if (this.mListener.equals(locationListenerKey.mListener)) {
                    b3 = true;
                }
            }
            return b3;
        }
        
        @Override
        public int hashCode() {
            return ObjectsCompat.hash(this.mProvider, this.mListener);
        }
    }
    
    private static class LocationListenerTransport implements LocationListener
    {
        final Executor mExecutor;
        @Nullable
        volatile LocationListenerKey mKey;
        
        LocationListenerTransport(@Nullable final LocationListenerKey mKey, final Executor mExecutor) {
            this.mKey = mKey;
            this.mExecutor = mExecutor;
        }
        
        public LocationListenerKey getKey() {
            return ObjectsCompat.requireNonNull(this.mKey);
        }
        
        public void onFlushComplete(final int n) {
            if (this.mKey == null) {
                return;
            }
            this.mExecutor.execute(new o\u30078oOO88(this, n));
        }
        
        public void onLocationChanged(@NonNull final Location location) {
            if (this.mKey == null) {
                return;
            }
            this.mExecutor.execute(new O000(this, location));
        }
        
        public void onLocationChanged(@NonNull final List<Location> list) {
            if (this.mKey == null) {
                return;
            }
            this.mExecutor.execute(new o\u3007O(this, list));
        }
        
        public void onProviderDisabled(@NonNull final String s) {
            if (this.mKey == null) {
                return;
            }
            this.mExecutor.execute(new oO00OOO(this, s));
        }
        
        public void onProviderEnabled(@NonNull final String s) {
            if (this.mKey == null) {
                return;
            }
            this.mExecutor.execute(new o8oO\u3007(this, s));
        }
        
        public void onStatusChanged(final String s, final int n, final Bundle bundle) {
            if (this.mKey == null) {
                return;
            }
            this.mExecutor.execute(new \u300780(this, s, n, bundle));
        }
        
        public void unregister() {
            this.mKey = null;
        }
    }
    
    @RequiresApi(24)
    private static class PreRGnssStatusTransport extends GnssStatus$Callback
    {
        final GnssStatusCompat.Callback mCallback;
        @Nullable
        volatile Executor mExecutor;
        
        PreRGnssStatusTransport(final GnssStatusCompat.Callback mCallback) {
            Preconditions.checkArgument(mCallback != null, (Object)"invalid null callback");
            this.mCallback = mCallback;
        }
        
        public void onFirstFix(final int n) {
            final Executor mExecutor = this.mExecutor;
            if (mExecutor == null) {
                return;
            }
            mExecutor.execute(new ooo\u30078oO(this, mExecutor, n));
        }
        
        public void onSatelliteStatusChanged(final GnssStatus gnssStatus) {
            final Executor mExecutor = this.mExecutor;
            if (mExecutor == null) {
                return;
            }
            mExecutor.execute(new \u3007O\u300780o08O(this, mExecutor, gnssStatus));
        }
        
        public void onStarted() {
            final Executor mExecutor = this.mExecutor;
            if (mExecutor == null) {
                return;
            }
            mExecutor.execute(new Ooo(this, mExecutor));
        }
        
        public void onStopped() {
            final Executor mExecutor = this.mExecutor;
            if (mExecutor == null) {
                return;
            }
            mExecutor.execute(new O0o\u3007\u3007Oo(this, mExecutor));
        }
        
        public void register(final Executor mExecutor) {
            final boolean b = true;
            Preconditions.checkArgument(mExecutor != null, (Object)"invalid null executor");
            Preconditions.checkState(this.mExecutor == null && b);
            this.mExecutor = mExecutor;
        }
        
        public void unregister() {
            this.mExecutor = null;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.location;

import android.os.Build$VERSION;
import androidx.core.util.Preconditions;
import android.location.GpsStatus;
import androidx.annotation.GuardedBy;
import android.location.GpsSatellite;
import java.util.Iterator;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
class GpsStatusWrapper extends GnssStatusCompat
{
    private static final int BEIDOU_PRN_COUNT = 35;
    private static final int BEIDOU_PRN_OFFSET = 200;
    private static final int GLONASS_PRN_COUNT = 24;
    private static final int GLONASS_PRN_OFFSET = 64;
    private static final int GPS_PRN_COUNT = 32;
    private static final int GPS_PRN_OFFSET = 0;
    private static final int QZSS_SVID_MAX = 200;
    private static final int QZSS_SVID_MIN = 193;
    private static final int SBAS_PRN_MAX = 64;
    private static final int SBAS_PRN_MIN = 33;
    private static final int SBAS_PRN_OFFSET = -87;
    @GuardedBy("mWrapped")
    private Iterator<GpsSatellite> mCachedIterator;
    @GuardedBy("mWrapped")
    private int mCachedIteratorPosition;
    @GuardedBy("mWrapped")
    private GpsSatellite mCachedSatellite;
    @GuardedBy("mWrapped")
    private int mCachedSatelliteCount;
    private final GpsStatus mWrapped;
    
    GpsStatusWrapper(GpsStatus mWrapped) {
        mWrapped = Preconditions.checkNotNull(mWrapped);
        this.mWrapped = mWrapped;
        this.mCachedSatelliteCount = -1;
        this.mCachedIterator = mWrapped.getSatellites().iterator();
        this.mCachedIteratorPosition = -1;
        this.mCachedSatellite = null;
    }
    
    private static int getConstellationFromPrn(final int n) {
        if (n > 0 && n <= 32) {
            return 1;
        }
        if (n >= 33 && n <= 64) {
            return 2;
        }
        if (n > 64 && n <= 88) {
            return 3;
        }
        if (n > 200 && n <= 235) {
            return 5;
        }
        if (n >= 193 && n <= 200) {
            return 4;
        }
        return 0;
    }
    
    private GpsSatellite getSatellite(final int n) {
        synchronized (this.mWrapped) {
            if (n < this.mCachedIteratorPosition) {
                this.mCachedIterator = this.mWrapped.getSatellites().iterator();
                this.mCachedIteratorPosition = -1;
            }
            while (true) {
                final int mCachedIteratorPosition = this.mCachedIteratorPosition;
                if (mCachedIteratorPosition >= n) {
                    break;
                }
                this.mCachedIteratorPosition = mCachedIteratorPosition + 1;
                if (!this.mCachedIterator.hasNext()) {
                    this.mCachedSatellite = null;
                    break;
                }
                this.mCachedSatellite = this.mCachedIterator.next();
            }
            final GpsSatellite mCachedSatellite = this.mCachedSatellite;
            monitorexit(this.mWrapped);
            return Preconditions.checkNotNull(mCachedSatellite);
        }
    }
    
    private static int getSvidFromPrn(int n) {
        final int constellationFromPrn = getConstellationFromPrn(n);
        if (constellationFromPrn != 2) {
            if (constellationFromPrn != 3) {
                if (constellationFromPrn == 5) {
                    n -= 200;
                }
            }
            else {
                n -= 64;
            }
        }
        else {
            n += 87;
        }
        return n;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof GpsStatusWrapper && this.mWrapped.equals(((GpsStatusWrapper)o).mWrapped));
    }
    
    @Override
    public float getAzimuthDegrees(final int n) {
        return this.getSatellite(n).getAzimuth();
    }
    
    @Override
    public float getBasebandCn0DbHz(final int n) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public float getCarrierFrequencyHz(final int n) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public float getCn0DbHz(final int n) {
        return this.getSatellite(n).getSnr();
    }
    
    @Override
    public int getConstellationType(final int n) {
        if (Build$VERSION.SDK_INT < 24) {
            return 1;
        }
        return getConstellationFromPrn(this.getSatellite(n).getPrn());
    }
    
    @Override
    public float getElevationDegrees(final int n) {
        return this.getSatellite(n).getElevation();
    }
    
    @Override
    public int getSatelliteCount() {
        synchronized (this.mWrapped) {
            if (this.mCachedSatelliteCount == -1) {
                for (final GpsSatellite gpsSatellite : this.mWrapped.getSatellites()) {
                    ++this.mCachedSatelliteCount;
                }
                ++this.mCachedSatelliteCount;
            }
            return this.mCachedSatelliteCount;
        }
    }
    
    @Override
    public int getSvid(final int n) {
        if (Build$VERSION.SDK_INT < 24) {
            return this.getSatellite(n).getPrn();
        }
        return getSvidFromPrn(this.getSatellite(n).getPrn());
    }
    
    @Override
    public boolean hasAlmanacData(final int n) {
        return this.getSatellite(n).hasAlmanac();
    }
    
    @Override
    public boolean hasBasebandCn0DbHz(final int n) {
        return false;
    }
    
    @Override
    public boolean hasCarrierFrequencyHz(final int n) {
        return false;
    }
    
    @Override
    public boolean hasEphemerisData(final int n) {
        return this.getSatellite(n).hasEphemeris();
    }
    
    @Override
    public int hashCode() {
        return this.mWrapped.hashCode();
    }
    
    @Override
    public boolean usedInFix(final int n) {
        return this.getSatellite(n).usedInFix();
    }
}

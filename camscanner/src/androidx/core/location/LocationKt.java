// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.location;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.location.Location;
import kotlin.Metadata;

@Metadata
public final class LocationKt
{
    public static final double component1(@NotNull final Location location) {
        Intrinsics.checkNotNullParameter((Object)location, "<this>");
        return location.getLatitude();
    }
    
    public static final double component2(@NotNull final Location location) {
        Intrinsics.checkNotNullParameter((Object)location, "<this>");
        return location.getLongitude();
    }
}

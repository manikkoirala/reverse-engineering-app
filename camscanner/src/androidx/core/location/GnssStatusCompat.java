// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.location;

import androidx.annotation.RestrictTo;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.annotation.FloatRange;
import androidx.annotation.IntRange;
import android.location.GpsStatus;
import androidx.annotation.RequiresApi;
import androidx.annotation.NonNull;
import android.location.GnssStatus;
import android.annotation.SuppressLint;

public abstract class GnssStatusCompat
{
    @SuppressLint({ "InlinedApi" })
    public static final int CONSTELLATION_BEIDOU = 5;
    @SuppressLint({ "InlinedApi" })
    public static final int CONSTELLATION_GALILEO = 6;
    @SuppressLint({ "InlinedApi" })
    public static final int CONSTELLATION_GLONASS = 3;
    @SuppressLint({ "InlinedApi" })
    public static final int CONSTELLATION_GPS = 1;
    @SuppressLint({ "InlinedApi" })
    public static final int CONSTELLATION_IRNSS = 7;
    @SuppressLint({ "InlinedApi" })
    public static final int CONSTELLATION_QZSS = 4;
    @SuppressLint({ "InlinedApi" })
    public static final int CONSTELLATION_SBAS = 2;
    @SuppressLint({ "InlinedApi" })
    public static final int CONSTELLATION_UNKNOWN = 0;
    
    GnssStatusCompat() {
    }
    
    @NonNull
    @RequiresApi(24)
    public static GnssStatusCompat wrap(@NonNull final GnssStatus gnssStatus) {
        return new GnssStatusWrapper(gnssStatus);
    }
    
    @SuppressLint({ "ReferencesDeprecated" })
    @NonNull
    public static GnssStatusCompat wrap(@NonNull final GpsStatus gpsStatus) {
        return new GpsStatusWrapper(gpsStatus);
    }
    
    @FloatRange(from = 0.0, to = 360.0)
    public abstract float getAzimuthDegrees(@IntRange(from = 0L) final int p0);
    
    @FloatRange(from = 0.0, to = 63.0)
    public abstract float getBasebandCn0DbHz(@IntRange(from = 0L) final int p0);
    
    @FloatRange(from = 0.0)
    public abstract float getCarrierFrequencyHz(@IntRange(from = 0L) final int p0);
    
    @FloatRange(from = 0.0, to = 63.0)
    public abstract float getCn0DbHz(@IntRange(from = 0L) final int p0);
    
    public abstract int getConstellationType(@IntRange(from = 0L) final int p0);
    
    @FloatRange(from = -90.0, to = 90.0)
    public abstract float getElevationDegrees(@IntRange(from = 0L) final int p0);
    
    @IntRange(from = 0L)
    public abstract int getSatelliteCount();
    
    @IntRange(from = 1L, to = 200L)
    public abstract int getSvid(@IntRange(from = 0L) final int p0);
    
    public abstract boolean hasAlmanacData(@IntRange(from = 0L) final int p0);
    
    public abstract boolean hasBasebandCn0DbHz(@IntRange(from = 0L) final int p0);
    
    public abstract boolean hasCarrierFrequencyHz(@IntRange(from = 0L) final int p0);
    
    public abstract boolean hasEphemerisData(@IntRange(from = 0L) final int p0);
    
    public abstract boolean usedInFix(@IntRange(from = 0L) final int p0);
    
    public abstract static class Callback
    {
        public void onFirstFix(@IntRange(from = 0L) final int n) {
        }
        
        public void onSatelliteStatusChanged(@NonNull final GnssStatusCompat gnssStatusCompat) {
        }
        
        public void onStarted() {
        }
        
        public void onStopped() {
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface ConstellationType {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.location;

import androidx.annotation.Nullable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.location.Location;
import java.util.List;
import android.location.LocationListener;

public interface LocationListenerCompat extends LocationListener
{
    void onFlushComplete(final int p0);
    
    void onLocationChanged(@NonNull final List<Location> p0);
    
    void onProviderDisabled(@NonNull final String p0);
    
    void onProviderEnabled(@NonNull final String p0);
    
    void onStatusChanged(@NonNull final String p0, final int p1, @Nullable final Bundle p2);
}

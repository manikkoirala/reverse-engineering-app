// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.location;

import androidx.annotation.DoNotInline;
import android.os.Build$VERSION;
import androidx.core.util.Preconditions;
import android.location.GnssStatus;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;

@RequiresApi(24)
@RestrictTo({ RestrictTo.Scope.LIBRARY })
class GnssStatusWrapper extends GnssStatusCompat
{
    private final GnssStatus mWrapped;
    
    GnssStatusWrapper(final Object o) {
        this.mWrapped = Preconditions.checkNotNull(o);
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof GnssStatusWrapper && \u3007o00\u3007\u3007Oo.\u3007080(this.mWrapped, (Object)((GnssStatusWrapper)o).mWrapped));
    }
    
    @Override
    public float getAzimuthDegrees(final int n) {
        return OO0o\u3007\u3007\u3007\u30070.\u3007080(this.mWrapped, n);
    }
    
    @Override
    public float getBasebandCn0DbHz(final int n) {
        if (Build$VERSION.SDK_INT >= 30) {
            return Api30Impl.getBasebandCn0DbHz(this.mWrapped, n);
        }
        throw new UnsupportedOperationException();
    }
    
    @Override
    public float getCarrierFrequencyHz(final int n) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getCarrierFrequencyHz(this.mWrapped, n);
        }
        throw new UnsupportedOperationException();
    }
    
    @Override
    public float getCn0DbHz(final int n) {
        return o\u30070.\u3007080(this.mWrapped, n);
    }
    
    @Override
    public int getConstellationType(final int n) {
        return \u3007\u3007888.\u3007080(this.mWrapped, n);
    }
    
    @Override
    public float getElevationDegrees(final int n) {
        return O8.\u3007080(this.mWrapped, n);
    }
    
    @Override
    public int getSatelliteCount() {
        return oO80.\u3007080(this.mWrapped);
    }
    
    @Override
    public int getSvid(final int n) {
        return Oo08.\u3007080(this.mWrapped, n);
    }
    
    @Override
    public boolean hasAlmanacData(final int n) {
        return \u30078o8o\u3007.\u3007080(this.mWrapped, n);
    }
    
    @Override
    public boolean hasBasebandCn0DbHz(final int n) {
        return Build$VERSION.SDK_INT >= 30 && Api30Impl.hasBasebandCn0DbHz(this.mWrapped, n);
    }
    
    @Override
    public boolean hasCarrierFrequencyHz(final int n) {
        return Build$VERSION.SDK_INT >= 26 && Api26Impl.hasCarrierFrequencyHz(this.mWrapped, n);
    }
    
    @Override
    public boolean hasEphemerisData(final int n) {
        return \u3007080.\u3007080(this.mWrapped, n);
    }
    
    @Override
    public int hashCode() {
        return \u3007o\u3007.\u3007080(this.mWrapped);
    }
    
    @Override
    public boolean usedInFix(final int n) {
        return \u300780\u3007808\u3007O.\u3007080(this.mWrapped, n);
    }
    
    @RequiresApi(26)
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        @DoNotInline
        static float getCarrierFrequencyHz(final GnssStatus gnssStatus, final int n) {
            return \u3007O8o08O.\u3007080(gnssStatus, n);
        }
        
        @DoNotInline
        static boolean hasCarrierFrequencyHz(final GnssStatus gnssStatus, final int n) {
            return OO0o\u3007\u3007.\u3007080(gnssStatus, n);
        }
    }
    
    @RequiresApi(30)
    static class Api30Impl
    {
        private Api30Impl() {
        }
        
        @DoNotInline
        static float getBasebandCn0DbHz(final GnssStatus gnssStatus, final int n) {
            return \u3007\u3007808\u3007.\u3007080(gnssStatus, n);
        }
        
        @DoNotInline
        static boolean hasBasebandCn0DbHz(final GnssStatus gnssStatus, final int n) {
            return Oooo8o0\u3007.\u3007080(gnssStatus, n);
        }
    }
}

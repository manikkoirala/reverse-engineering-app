// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.location;

import androidx.annotation.RestrictTo;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.core.util.Preconditions;
import androidx.annotation.DoNotInline;
import android.location.LocationRequest$Builder;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import androidx.core.util.TimeUtils;
import androidx.annotation.Nullable;
import android.annotation.SuppressLint;
import android.os.Build$VERSION;
import androidx.annotation.RequiresApi;
import androidx.annotation.NonNull;
import android.location.LocationRequest;
import androidx.annotation.FloatRange;
import androidx.annotation.IntRange;

public final class LocationRequestCompat
{
    private static final long IMPLICIT_MIN_UPDATE_INTERVAL = -1L;
    public static final long PASSIVE_INTERVAL = Long.MAX_VALUE;
    public static final int QUALITY_BALANCED_POWER_ACCURACY = 102;
    public static final int QUALITY_HIGH_ACCURACY = 100;
    public static final int QUALITY_LOW_POWER = 104;
    final long mDurationMillis;
    final long mIntervalMillis;
    final long mMaxUpdateDelayMillis;
    final int mMaxUpdates;
    final float mMinUpdateDistanceMeters;
    final long mMinUpdateIntervalMillis;
    final int mQuality;
    
    LocationRequestCompat(final long mIntervalMillis, final int mQuality, final long mDurationMillis, final int mMaxUpdates, final long mMinUpdateIntervalMillis, final float mMinUpdateDistanceMeters, final long mMaxUpdateDelayMillis) {
        this.mIntervalMillis = mIntervalMillis;
        this.mQuality = mQuality;
        this.mMinUpdateIntervalMillis = mMinUpdateIntervalMillis;
        this.mDurationMillis = mDurationMillis;
        this.mMaxUpdates = mMaxUpdates;
        this.mMinUpdateDistanceMeters = mMinUpdateDistanceMeters;
        this.mMaxUpdateDelayMillis = mMaxUpdateDelayMillis;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof LocationRequestCompat)) {
            return false;
        }
        final LocationRequestCompat locationRequestCompat = (LocationRequestCompat)o;
        if (this.mQuality != locationRequestCompat.mQuality || this.mIntervalMillis != locationRequestCompat.mIntervalMillis || this.mMinUpdateIntervalMillis != locationRequestCompat.mMinUpdateIntervalMillis || this.mDurationMillis != locationRequestCompat.mDurationMillis || this.mMaxUpdates != locationRequestCompat.mMaxUpdates || Float.compare(locationRequestCompat.mMinUpdateDistanceMeters, this.mMinUpdateDistanceMeters) != 0 || this.mMaxUpdateDelayMillis != locationRequestCompat.mMaxUpdateDelayMillis) {
            b = false;
        }
        return b;
    }
    
    @IntRange(from = 1L)
    public long getDurationMillis() {
        return this.mDurationMillis;
    }
    
    @IntRange(from = 0L)
    public long getIntervalMillis() {
        return this.mIntervalMillis;
    }
    
    @IntRange(from = 0L)
    public long getMaxUpdateDelayMillis() {
        return this.mMaxUpdateDelayMillis;
    }
    
    @IntRange(from = 1L, to = 2147483647L)
    public int getMaxUpdates() {
        return this.mMaxUpdates;
    }
    
    @FloatRange(from = 0.0, to = 3.4028234663852886E38)
    public float getMinUpdateDistanceMeters() {
        return this.mMinUpdateDistanceMeters;
    }
    
    @IntRange(from = 0L)
    public long getMinUpdateIntervalMillis() {
        long n;
        if ((n = this.mMinUpdateIntervalMillis) == -1L) {
            n = this.mIntervalMillis;
        }
        return n;
    }
    
    public int getQuality() {
        return this.mQuality;
    }
    
    @Override
    public int hashCode() {
        final int mQuality = this.mQuality;
        final long mIntervalMillis = this.mIntervalMillis;
        final int n = (int)(mIntervalMillis ^ mIntervalMillis >>> 32);
        final long mMinUpdateIntervalMillis = this.mMinUpdateIntervalMillis;
        return (mQuality * 31 + n) * 31 + (int)(mMinUpdateIntervalMillis ^ mMinUpdateIntervalMillis >>> 32);
    }
    
    @NonNull
    @RequiresApi(31)
    public LocationRequest toLocationRequest() {
        return Api31Impl.toLocationRequest(this);
    }
    
    @SuppressLint({ "NewApi" })
    @Nullable
    @RequiresApi(19)
    public LocationRequest toLocationRequest(@NonNull final String s) {
        if (Build$VERSION.SDK_INT >= 31) {
            return this.toLocationRequest();
        }
        return (LocationRequest)Api19Impl.toLocationRequest(this, s);
    }
    
    @NonNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Request[");
        if (this.mIntervalMillis != Long.MAX_VALUE) {
            sb.append("@");
            TimeUtils.formatDuration(this.mIntervalMillis, sb);
            final int mQuality = this.mQuality;
            if (mQuality != 100) {
                if (mQuality != 102) {
                    if (mQuality == 104) {
                        sb.append(" LOW_POWER");
                    }
                }
                else {
                    sb.append(" BALANCED");
                }
            }
            else {
                sb.append(" HIGH_ACCURACY");
            }
        }
        else {
            sb.append("PASSIVE");
        }
        if (this.mDurationMillis != Long.MAX_VALUE) {
            sb.append(", duration=");
            TimeUtils.formatDuration(this.mDurationMillis, sb);
        }
        if (this.mMaxUpdates != Integer.MAX_VALUE) {
            sb.append(", maxUpdates=");
            sb.append(this.mMaxUpdates);
        }
        final long mMinUpdateIntervalMillis = this.mMinUpdateIntervalMillis;
        if (mMinUpdateIntervalMillis != -1L && mMinUpdateIntervalMillis < this.mIntervalMillis) {
            sb.append(", minUpdateInterval=");
            TimeUtils.formatDuration(this.mMinUpdateIntervalMillis, sb);
        }
        if (this.mMinUpdateDistanceMeters > 0.0) {
            sb.append(", minUpdateDistance=");
            sb.append(this.mMinUpdateDistanceMeters);
        }
        if (this.mMaxUpdateDelayMillis / 2L > this.mIntervalMillis) {
            sb.append(", maxUpdateDelay=");
            TimeUtils.formatDuration(this.mMaxUpdateDelayMillis, sb);
        }
        sb.append(']');
        return sb.toString();
    }
    
    @RequiresApi(19)
    private static class Api19Impl
    {
        private static Method sCreateFromDeprecatedProviderMethod;
        private static Class<?> sLocationRequestClass;
        private static Method sSetExpireInMethod;
        private static Method sSetFastestIntervalMethod;
        private static Method sSetNumUpdatesMethod;
        private static Method sSetQualityMethod;
        
        public static Object toLocationRequest(final LocationRequestCompat locationRequestCompat, final String s) {
            try {
                if (Api19Impl.sLocationRequestClass == null) {
                    Api19Impl.sLocationRequestClass = Class.forName("android.location.LocationRequest");
                }
                if (Api19Impl.sCreateFromDeprecatedProviderMethod == null) {
                    (Api19Impl.sCreateFromDeprecatedProviderMethod = Api19Impl.sLocationRequestClass.getDeclaredMethod("createFromDeprecatedProvider", String.class, Long.TYPE, Float.TYPE, Boolean.TYPE)).setAccessible(true);
                }
                final Object invoke = Api19Impl.sCreateFromDeprecatedProviderMethod.invoke(null, s, locationRequestCompat.getIntervalMillis(), locationRequestCompat.getMinUpdateDistanceMeters(), Boolean.FALSE);
                if (invoke == null) {
                    return null;
                }
                if (Api19Impl.sSetQualityMethod == null) {
                    (Api19Impl.sSetQualityMethod = Api19Impl.sLocationRequestClass.getDeclaredMethod("setQuality", Integer.TYPE)).setAccessible(true);
                }
                Api19Impl.sSetQualityMethod.invoke(invoke, locationRequestCompat.getQuality());
                if (Api19Impl.sSetFastestIntervalMethod == null) {
                    (Api19Impl.sSetFastestIntervalMethod = Api19Impl.sLocationRequestClass.getDeclaredMethod("setFastestInterval", Long.TYPE)).setAccessible(true);
                }
                Api19Impl.sSetFastestIntervalMethod.invoke(invoke, locationRequestCompat.getMinUpdateIntervalMillis());
                if (locationRequestCompat.getMaxUpdates() < Integer.MAX_VALUE) {
                    if (Api19Impl.sSetNumUpdatesMethod == null) {
                        (Api19Impl.sSetNumUpdatesMethod = Api19Impl.sLocationRequestClass.getDeclaredMethod("setNumUpdates", Integer.TYPE)).setAccessible(true);
                    }
                    Api19Impl.sSetNumUpdatesMethod.invoke(invoke, locationRequestCompat.getMaxUpdates());
                }
                if (locationRequestCompat.getDurationMillis() < Long.MAX_VALUE) {
                    if (Api19Impl.sSetExpireInMethod == null) {
                        (Api19Impl.sSetExpireInMethod = Api19Impl.sLocationRequestClass.getDeclaredMethod("setExpireIn", Long.TYPE)).setAccessible(true);
                    }
                    Api19Impl.sSetExpireInMethod.invoke(invoke, locationRequestCompat.getDurationMillis());
                }
                return invoke;
            }
            catch (final NoSuchMethodException | InvocationTargetException | IllegalAccessException | ClassNotFoundException ex) {
                return null;
            }
        }
    }
    
    @RequiresApi(31)
    private static class Api31Impl
    {
        @DoNotInline
        public static LocationRequest toLocationRequest(final LocationRequestCompat locationRequestCompat) {
            return \u3007o0O0O8.\u3007080(O0O8OO088.\u3007080(O8O\u3007.\u3007080(o88\u3007OO08\u3007.\u3007080(\u30070.\u3007080(o0O0.\u3007080(OO8oO0o\u3007.\u3007080(new LocationRequest$Builder(locationRequestCompat.getIntervalMillis()), locationRequestCompat.getQuality()), locationRequestCompat.getMinUpdateIntervalMillis()), locationRequestCompat.getDurationMillis()), locationRequestCompat.getMaxUpdates()), locationRequestCompat.getMinUpdateDistanceMeters()), locationRequestCompat.getMaxUpdateDelayMillis()));
        }
    }
    
    public static final class Builder
    {
        private long mDurationMillis;
        private long mIntervalMillis;
        private long mMaxUpdateDelayMillis;
        private int mMaxUpdates;
        private float mMinUpdateDistanceMeters;
        private long mMinUpdateIntervalMillis;
        private int mQuality;
        
        public Builder(final long intervalMillis) {
            this.setIntervalMillis(intervalMillis);
            this.mQuality = 102;
            this.mDurationMillis = Long.MAX_VALUE;
            this.mMaxUpdates = Integer.MAX_VALUE;
            this.mMinUpdateIntervalMillis = -1L;
            this.mMinUpdateDistanceMeters = 0.0f;
            this.mMaxUpdateDelayMillis = 0L;
        }
        
        public Builder(@NonNull final LocationRequestCompat locationRequestCompat) {
            this.mIntervalMillis = locationRequestCompat.mIntervalMillis;
            this.mQuality = locationRequestCompat.mQuality;
            this.mDurationMillis = locationRequestCompat.mDurationMillis;
            this.mMaxUpdates = locationRequestCompat.mMaxUpdates;
            this.mMinUpdateIntervalMillis = locationRequestCompat.mMinUpdateIntervalMillis;
            this.mMinUpdateDistanceMeters = locationRequestCompat.mMinUpdateDistanceMeters;
            this.mMaxUpdateDelayMillis = locationRequestCompat.mMaxUpdateDelayMillis;
        }
        
        @NonNull
        public LocationRequestCompat build() {
            Preconditions.checkState(this.mIntervalMillis != Long.MAX_VALUE || this.mMinUpdateIntervalMillis != -1L, "passive location requests must have an explicit minimum update interval");
            final long mIntervalMillis = this.mIntervalMillis;
            return new LocationRequestCompat(mIntervalMillis, this.mQuality, this.mDurationMillis, this.mMaxUpdates, Math.min(this.mMinUpdateIntervalMillis, mIntervalMillis), this.mMinUpdateDistanceMeters, this.mMaxUpdateDelayMillis);
        }
        
        @NonNull
        public Builder clearMinUpdateIntervalMillis() {
            this.mMinUpdateIntervalMillis = -1L;
            return this;
        }
        
        @NonNull
        public Builder setDurationMillis(@IntRange(from = 1L) final long n) {
            this.mDurationMillis = Preconditions.checkArgumentInRange(n, 1L, Long.MAX_VALUE, "durationMillis");
            return this;
        }
        
        @NonNull
        public Builder setIntervalMillis(@IntRange(from = 0L) final long n) {
            this.mIntervalMillis = Preconditions.checkArgumentInRange(n, 0L, Long.MAX_VALUE, "intervalMillis");
            return this;
        }
        
        @NonNull
        public Builder setMaxUpdateDelayMillis(@IntRange(from = 0L) final long mMaxUpdateDelayMillis) {
            this.mMaxUpdateDelayMillis = mMaxUpdateDelayMillis;
            this.mMaxUpdateDelayMillis = Preconditions.checkArgumentInRange(mMaxUpdateDelayMillis, 0L, Long.MAX_VALUE, "maxUpdateDelayMillis");
            return this;
        }
        
        @NonNull
        public Builder setMaxUpdates(@IntRange(from = 1L, to = 2147483647L) final int n) {
            this.mMaxUpdates = Preconditions.checkArgumentInRange(n, 1, Integer.MAX_VALUE, "maxUpdates");
            return this;
        }
        
        @NonNull
        public Builder setMinUpdateDistanceMeters(@FloatRange(from = 0.0, to = 3.4028234663852886E38) final float mMinUpdateDistanceMeters) {
            this.mMinUpdateDistanceMeters = mMinUpdateDistanceMeters;
            this.mMinUpdateDistanceMeters = Preconditions.checkArgumentInRange(mMinUpdateDistanceMeters, 0.0f, Float.MAX_VALUE, "minUpdateDistanceMeters");
            return this;
        }
        
        @NonNull
        public Builder setMinUpdateIntervalMillis(@IntRange(from = 0L) final long n) {
            this.mMinUpdateIntervalMillis = Preconditions.checkArgumentInRange(n, 0L, Long.MAX_VALUE, "minUpdateIntervalMillis");
            return this;
        }
        
        @NonNull
        public Builder setQuality(final int n) {
            Preconditions.checkArgument(n == 104 || n == 102 || n == 100, "quality must be a defined QUALITY constant, not %d", n);
            this.mQuality = n;
            return this;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface Quality {
    }
}

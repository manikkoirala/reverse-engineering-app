// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import androidx.annotation.DoNotInline;
import android.app.ActivityOptions;
import androidx.annotation.RequiresApi;
import android.os.Bundle;
import android.app.PendingIntent;
import android.graphics.Rect;
import android.graphics.Bitmap;
import androidx.annotation.Nullable;
import androidx.core.util.Pair;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import androidx.annotation.NonNull;
import android.os.Build$VERSION;

public class ActivityOptionsCompat
{
    public static final String EXTRA_USAGE_TIME_REPORT = "android.activity.usage_time";
    public static final String EXTRA_USAGE_TIME_REPORT_PACKAGES = "android.usage_time_packages";
    
    protected ActivityOptionsCompat() {
    }
    
    @NonNull
    public static ActivityOptionsCompat makeBasic() {
        if (Build$VERSION.SDK_INT >= 23) {
            return new ActivityOptionsCompatImpl(Api23Impl.makeBasic());
        }
        return new ActivityOptionsCompat();
    }
    
    @NonNull
    public static ActivityOptionsCompat makeClipRevealAnimation(@NonNull final View view, final int n, final int n2, final int n3, final int n4) {
        if (Build$VERSION.SDK_INT >= 23) {
            return new ActivityOptionsCompatImpl(Api23Impl.makeClipRevealAnimation(view, n, n2, n3, n4));
        }
        return new ActivityOptionsCompat();
    }
    
    @NonNull
    public static ActivityOptionsCompat makeCustomAnimation(@NonNull final Context context, final int n, final int n2) {
        return new ActivityOptionsCompatImpl(Api16Impl.makeCustomAnimation(context, n, n2));
    }
    
    @NonNull
    public static ActivityOptionsCompat makeScaleUpAnimation(@NonNull final View view, final int n, final int n2, final int n3, final int n4) {
        return new ActivityOptionsCompatImpl(Api16Impl.makeScaleUpAnimation(view, n, n2, n3, n4));
    }
    
    @NonNull
    public static ActivityOptionsCompat makeSceneTransitionAnimation(@NonNull final Activity activity, @NonNull final View view, @NonNull final String s) {
        return new ActivityOptionsCompatImpl(Api21Impl.makeSceneTransitionAnimation(activity, view, s));
    }
    
    @NonNull
    public static ActivityOptionsCompat makeSceneTransitionAnimation(@NonNull final Activity activity, @Nullable final Pair<View, String>... array) {
        android.util.Pair[] array3;
        if (array != null) {
            final android.util.Pair[] array2 = new android.util.Pair[array.length];
            int n = 0;
            while (true) {
                array3 = array2;
                if (n >= array.length) {
                    break;
                }
                final Pair<View, String> pair = array[n];
                array2[n] = android.util.Pair.create((Object)pair.first, (Object)pair.second);
                ++n;
            }
        }
        else {
            array3 = null;
        }
        return new ActivityOptionsCompatImpl(Api21Impl.makeSceneTransitionAnimation(activity, (android.util.Pair<View, String>[])array3));
    }
    
    @NonNull
    public static ActivityOptionsCompat makeTaskLaunchBehind() {
        return new ActivityOptionsCompatImpl(Api21Impl.makeTaskLaunchBehind());
    }
    
    @NonNull
    public static ActivityOptionsCompat makeThumbnailScaleUpAnimation(@NonNull final View view, @NonNull final Bitmap bitmap, final int n, final int n2) {
        return new ActivityOptionsCompatImpl(Api16Impl.makeThumbnailScaleUpAnimation(view, bitmap, n, n2));
    }
    
    @Nullable
    public Rect getLaunchBounds() {
        return null;
    }
    
    public void requestUsageTimeReport(@NonNull final PendingIntent pendingIntent) {
    }
    
    @NonNull
    public ActivityOptionsCompat setLaunchBounds(@Nullable final Rect rect) {
        return this;
    }
    
    @Nullable
    public Bundle toBundle() {
        return null;
    }
    
    public void update(@NonNull final ActivityOptionsCompat activityOptionsCompat) {
    }
    
    @RequiresApi(16)
    private static class ActivityOptionsCompatImpl extends ActivityOptionsCompat
    {
        private final ActivityOptions mActivityOptions;
        
        ActivityOptionsCompatImpl(final ActivityOptions mActivityOptions) {
            this.mActivityOptions = mActivityOptions;
        }
        
        @Override
        public Rect getLaunchBounds() {
            if (Build$VERSION.SDK_INT < 24) {
                return null;
            }
            return Api24Impl.getLaunchBounds(this.mActivityOptions);
        }
        
        @Override
        public void requestUsageTimeReport(@NonNull final PendingIntent pendingIntent) {
            if (Build$VERSION.SDK_INT >= 23) {
                Api23Impl.requestUsageTimeReport(this.mActivityOptions, pendingIntent);
            }
        }
        
        @NonNull
        @Override
        public ActivityOptionsCompat setLaunchBounds(@Nullable final Rect rect) {
            if (Build$VERSION.SDK_INT < 24) {
                return this;
            }
            return new ActivityOptionsCompatImpl(Api24Impl.setLaunchBounds(this.mActivityOptions, rect));
        }
        
        @Override
        public Bundle toBundle() {
            return this.mActivityOptions.toBundle();
        }
        
        @Override
        public void update(@NonNull final ActivityOptionsCompat activityOptionsCompat) {
            if (activityOptionsCompat instanceof ActivityOptionsCompatImpl) {
                this.mActivityOptions.update(((ActivityOptionsCompatImpl)activityOptionsCompat).mActivityOptions);
            }
        }
    }
    
    @RequiresApi(16)
    static class Api16Impl
    {
        private Api16Impl() {
        }
        
        @DoNotInline
        static ActivityOptions makeCustomAnimation(final Context context, final int n, final int n2) {
            return ActivityOptions.makeCustomAnimation(context, n, n2);
        }
        
        @DoNotInline
        static ActivityOptions makeScaleUpAnimation(final View view, final int n, final int n2, final int n3, final int n4) {
            return ActivityOptions.makeScaleUpAnimation(view, n, n2, n3, n4);
        }
        
        @DoNotInline
        static ActivityOptions makeThumbnailScaleUpAnimation(final View view, final Bitmap bitmap, final int n, final int n2) {
            return ActivityOptions.makeThumbnailScaleUpAnimation(view, bitmap, n, n2);
        }
    }
    
    @RequiresApi(21)
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        @DoNotInline
        static ActivityOptions makeSceneTransitionAnimation(final Activity activity, final View view, final String s) {
            return ActivityOptions.makeSceneTransitionAnimation(activity, view, s);
        }
        
        @SafeVarargs
        @DoNotInline
        static ActivityOptions makeSceneTransitionAnimation(final Activity activity, final android.util.Pair<View, String>... array) {
            return ActivityOptions.makeSceneTransitionAnimation(activity, (android.util.Pair[])array);
        }
        
        @DoNotInline
        static ActivityOptions makeTaskLaunchBehind() {
            return ActivityOptions.makeTaskLaunchBehind();
        }
    }
    
    @RequiresApi(23)
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        @DoNotInline
        static ActivityOptions makeBasic() {
            return OO0o\u3007\u3007.\u3007080();
        }
        
        @DoNotInline
        static ActivityOptions makeClipRevealAnimation(final View view, final int n, final int n2, final int n3, final int n4) {
            return \u3007O8o08O.\u3007080(view, n, n2, n3, n4);
        }
        
        @DoNotInline
        static void requestUsageTimeReport(final ActivityOptions activityOptions, final PendingIntent pendingIntent) {
            \u30078o8o\u3007.\u3007080(activityOptions, pendingIntent);
        }
    }
    
    @RequiresApi(24)
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        @DoNotInline
        static Rect getLaunchBounds(final ActivityOptions activityOptions) {
            return Oooo8o0\u3007.\u3007080(activityOptions);
        }
        
        @DoNotInline
        static ActivityOptions setLaunchBounds(final ActivityOptions activityOptions, final Rect rect) {
            return \u3007\u3007808\u3007.\u3007080(activityOptions, rect);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import java.util.ArrayDeque;
import android.os.Message;
import android.content.pm.ServiceInfo;
import android.content.pm.ResolveInfo;
import android.os.DeadObjectException;
import android.util.Log;
import android.content.Intent;
import java.util.HashMap;
import java.util.Map;
import android.os.HandlerThread;
import android.os.Handler;
import android.content.ServiceConnection;
import android.os.Handler$Callback;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.INotificationSideChannel;
import androidx.annotation.RequiresPermission;
import java.util.Collections;
import androidx.browser.trusted.O8;
import java.util.Collection;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import android.app.NotificationChannelGroup;
import androidx.browser.trusted.\u3007o\u3007;
import android.app.NotificationChannel;
import androidx.annotation.Nullable;
import android.content.pm.ApplicationInfo;
import java.lang.reflect.InvocationTargetException;
import android.app.AppOpsManager;
import android.os.Build$VERSION;
import android.os.Bundle;
import android.app.Notification;
import android.content.ComponentName;
import android.provider.Settings$Secure;
import androidx.annotation.NonNull;
import java.util.HashSet;
import android.app.NotificationManager;
import android.content.Context;
import androidx.annotation.GuardedBy;
import java.util.Set;

public final class NotificationManagerCompat
{
    public static final String ACTION_BIND_SIDE_CHANNEL = "android.support.BIND_NOTIFICATION_SIDE_CHANNEL";
    private static final String CHECK_OP_NO_THROW = "checkOpNoThrow";
    public static final String EXTRA_USE_SIDE_CHANNEL = "android.support.useSideChannel";
    public static final int IMPORTANCE_DEFAULT = 3;
    public static final int IMPORTANCE_HIGH = 4;
    public static final int IMPORTANCE_LOW = 2;
    public static final int IMPORTANCE_MAX = 5;
    public static final int IMPORTANCE_MIN = 1;
    public static final int IMPORTANCE_NONE = 0;
    public static final int IMPORTANCE_UNSPECIFIED = -1000;
    static final int MAX_SIDE_CHANNEL_SDK_VERSION = 19;
    private static final String OP_POST_NOTIFICATION = "OP_POST_NOTIFICATION";
    private static final String SETTING_ENABLED_NOTIFICATION_LISTENERS = "enabled_notification_listeners";
    private static final int SIDE_CHANNEL_RETRY_BASE_INTERVAL_MS = 1000;
    private static final int SIDE_CHANNEL_RETRY_MAX_COUNT = 6;
    private static final String TAG = "NotifManCompat";
    @GuardedBy("sEnabledNotificationListenersLock")
    private static Set<String> sEnabledNotificationListenerPackages;
    @GuardedBy("sEnabledNotificationListenersLock")
    private static String sEnabledNotificationListeners;
    private static final Object sEnabledNotificationListenersLock;
    private static final Object sLock;
    @GuardedBy("sLock")
    private static SideChannelManager sSideChannelManager;
    private final Context mContext;
    private final NotificationManager mNotificationManager;
    
    static {
        sEnabledNotificationListenersLock = new Object();
        NotificationManagerCompat.sEnabledNotificationListenerPackages = new HashSet<String>();
        sLock = new Object();
    }
    
    private NotificationManagerCompat(final Context mContext) {
        this.mContext = mContext;
        this.mNotificationManager = (NotificationManager)mContext.getSystemService("notification");
    }
    
    @NonNull
    public static NotificationManagerCompat from(@NonNull final Context context) {
        return new NotificationManagerCompat(context);
    }
    
    @NonNull
    public static Set<String> getEnabledListenerPackages(@NonNull Context sEnabledNotificationListenersLock) {
        final String string = Settings$Secure.getString(sEnabledNotificationListenersLock.getContentResolver(), "enabled_notification_listeners");
        sEnabledNotificationListenersLock = (Context)NotificationManagerCompat.sEnabledNotificationListenersLock;
        monitorenter(sEnabledNotificationListenersLock);
        Label_0107: {
            if (string == null) {
                break Label_0107;
            }
            try {
                if (!string.equals(NotificationManagerCompat.sEnabledNotificationListeners)) {
                    final String[] split = string.split(":", -1);
                    final HashSet sEnabledNotificationListenerPackages = new HashSet(split.length);
                    for (int length = split.length, i = 0; i < length; ++i) {
                        final ComponentName unflattenFromString = ComponentName.unflattenFromString(split[i]);
                        if (unflattenFromString != null) {
                            sEnabledNotificationListenerPackages.add((Object)unflattenFromString.getPackageName());
                        }
                    }
                    NotificationManagerCompat.sEnabledNotificationListenerPackages = (Set<String>)sEnabledNotificationListenerPackages;
                    NotificationManagerCompat.sEnabledNotificationListeners = string;
                }
                return NotificationManagerCompat.sEnabledNotificationListenerPackages;
            }
            finally {
                monitorexit(sEnabledNotificationListenersLock);
            }
        }
    }
    
    private void pushSideChannelQueue(final Task task) {
        synchronized (NotificationManagerCompat.sLock) {
            if (NotificationManagerCompat.sSideChannelManager == null) {
                NotificationManagerCompat.sSideChannelManager = new SideChannelManager(this.mContext.getApplicationContext());
            }
            NotificationManagerCompat.sSideChannelManager.queueTask(task);
        }
    }
    
    private static boolean useSideChannelForNotification(final Notification notification) {
        final Bundle extras = NotificationCompat.getExtras(notification);
        return extras != null && extras.getBoolean("android.support.useSideChannel");
    }
    
    public boolean areNotificationsEnabled() {
        if (Build$VERSION.SDK_INT >= 24) {
            return OOo88OOo.\u3007080(this.mNotificationManager);
        }
        final AppOpsManager obj = (AppOpsManager)this.mContext.getSystemService("appops");
        final ApplicationInfo applicationInfo = this.mContext.getApplicationInfo();
        final String packageName = this.mContext.getApplicationContext().getPackageName();
        final int uid = applicationInfo.uid;
        boolean b = true;
        try {
            final Class<?> forName = Class.forName(AppOpsManager.class.getName());
            final Class<Integer> type = Integer.TYPE;
            if ((int)forName.getMethod("checkOpNoThrow", type, type, String.class).invoke(obj, (int)forName.getDeclaredField("OP_POST_NOTIFICATION").get(Integer.class), uid, packageName) != 0) {
                b = false;
            }
            return b;
        }
        catch (final ClassNotFoundException | NoSuchMethodException | NoSuchFieldException | InvocationTargetException | IllegalAccessException | RuntimeException ex) {
            return b;
        }
    }
    
    public void cancel(final int n) {
        this.cancel(null, n);
    }
    
    public void cancel(@Nullable final String s, final int n) {
        this.mNotificationManager.cancel(s, n);
    }
    
    public void cancelAll() {
        this.mNotificationManager.cancelAll();
    }
    
    public void createNotificationChannel(@NonNull final NotificationChannel notificationChannel) {
        if (Build$VERSION.SDK_INT >= 26) {
            \u3007o\u3007.\u3007080(this.mNotificationManager, notificationChannel);
        }
    }
    
    public void createNotificationChannel(@NonNull final NotificationChannelCompat notificationChannelCompat) {
        this.createNotificationChannel(notificationChannelCompat.getNotificationChannel());
    }
    
    public void createNotificationChannelGroup(@NonNull final NotificationChannelGroup notificationChannelGroup) {
        if (Build$VERSION.SDK_INT >= 26) {
            \u30078O0O808\u3007.\u3007080(this.mNotificationManager, notificationChannelGroup);
        }
    }
    
    public void createNotificationChannelGroup(@NonNull final NotificationChannelGroupCompat notificationChannelGroupCompat) {
        this.createNotificationChannelGroup(notificationChannelGroupCompat.getNotificationChannelGroup());
    }
    
    public void createNotificationChannelGroups(@NonNull final List<NotificationChannelGroup> list) {
        if (Build$VERSION.SDK_INT >= 26) {
            O8888.\u3007080(this.mNotificationManager, (List)list);
        }
    }
    
    public void createNotificationChannelGroupsCompat(@NonNull final List<NotificationChannelGroupCompat> list) {
        if (Build$VERSION.SDK_INT >= 26 && !list.isEmpty()) {
            final ArrayList list2 = new ArrayList(list.size());
            final Iterator iterator = list.iterator();
            while (iterator.hasNext()) {
                list2.add(((NotificationChannelGroupCompat)iterator.next()).getNotificationChannelGroup());
            }
            O8888.\u3007080(this.mNotificationManager, (List)list2);
        }
    }
    
    public void createNotificationChannels(@NonNull final List<NotificationChannel> list) {
        if (Build$VERSION.SDK_INT >= 26) {
            \u3007OO8Oo0\u3007.\u3007080(this.mNotificationManager, (List)list);
        }
    }
    
    public void createNotificationChannelsCompat(@NonNull final List<NotificationChannelCompat> list) {
        if (Build$VERSION.SDK_INT >= 26 && !list.isEmpty()) {
            final ArrayList list2 = new ArrayList(list.size());
            final Iterator iterator = list.iterator();
            while (iterator.hasNext()) {
                list2.add(((NotificationChannelCompat)iterator.next()).getNotificationChannel());
            }
            \u3007OO8Oo0\u3007.\u3007080(this.mNotificationManager, (List)list2);
        }
    }
    
    public void deleteNotificationChannel(@NonNull final String s) {
        if (Build$VERSION.SDK_INT >= 26) {
            O88\u3007\u3007o0O.\u3007080(this.mNotificationManager, s);
        }
    }
    
    public void deleteNotificationChannelGroup(@NonNull final String s) {
        if (Build$VERSION.SDK_INT >= 26) {
            o88O8.\u3007080(this.mNotificationManager, s);
        }
    }
    
    public void deleteUnlistedNotificationChannels(@NonNull final Collection<String> collection) {
        if (Build$VERSION.SDK_INT >= 26) {
            for (final NotificationChannel notificationChannel : o88O\u30078.\u3007080(this.mNotificationManager)) {
                if (collection.contains(o\u3007\u30070\u3007.\u3007080(notificationChannel))) {
                    continue;
                }
                if (Build$VERSION.SDK_INT >= 30 && collection.contains(o\u30078oOO88.\u3007080(notificationChannel))) {
                    continue;
                }
                O88\u3007\u3007o0O.\u3007080(this.mNotificationManager, o\u3007\u30070\u3007.\u3007080(notificationChannel));
            }
        }
    }
    
    public int getImportance() {
        if (Build$VERSION.SDK_INT >= 24) {
            return o08\u3007\u30070O.\u3007080(this.mNotificationManager);
        }
        return -1000;
    }
    
    @Nullable
    public NotificationChannel getNotificationChannel(@NonNull final String s) {
        if (Build$VERSION.SDK_INT >= 26) {
            return O8.\u3007080(this.mNotificationManager, s);
        }
        return null;
    }
    
    @Nullable
    public NotificationChannel getNotificationChannel(@NonNull final String s, @NonNull final String s2) {
        if (Build$VERSION.SDK_INT >= 30) {
            return \u3007\u300700O\u30070o.\u3007080(this.mNotificationManager, s, s2);
        }
        return this.getNotificationChannel(s);
    }
    
    @Nullable
    public NotificationChannelCompat getNotificationChannelCompat(@NonNull final String s) {
        if (Build$VERSION.SDK_INT >= 26) {
            final NotificationChannel notificationChannel = this.getNotificationChannel(s);
            if (notificationChannel != null) {
                return new NotificationChannelCompat(notificationChannel);
            }
        }
        return null;
    }
    
    @Nullable
    public NotificationChannelCompat getNotificationChannelCompat(@NonNull final String s, @NonNull final String s2) {
        if (Build$VERSION.SDK_INT >= 26) {
            final NotificationChannel notificationChannel = this.getNotificationChannel(s, s2);
            if (notificationChannel != null) {
                return new NotificationChannelCompat(notificationChannel);
            }
        }
        return null;
    }
    
    @Nullable
    public NotificationChannelGroup getNotificationChannelGroup(@NonNull final String anObject) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 28) {
            return o08oOO.\u3007080(this.mNotificationManager, anObject);
        }
        if (sdk_INT >= 26) {
            for (final NotificationChannelGroup notificationChannelGroup : this.getNotificationChannelGroups()) {
                if (ooo\u30078oO.\u3007080(notificationChannelGroup).equals(anObject)) {
                    return notificationChannelGroup;
                }
            }
        }
        return null;
    }
    
    @Nullable
    public NotificationChannelGroupCompat getNotificationChannelGroupCompat(@NonNull final String s) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 28) {
            final NotificationChannelGroup notificationChannelGroup = this.getNotificationChannelGroup(s);
            if (notificationChannelGroup != null) {
                return new NotificationChannelGroupCompat(notificationChannelGroup);
            }
        }
        else if (sdk_INT >= 26) {
            final NotificationChannelGroup notificationChannelGroup2 = this.getNotificationChannelGroup(s);
            if (notificationChannelGroup2 != null) {
                return new NotificationChannelGroupCompat(notificationChannelGroup2, this.getNotificationChannels());
            }
        }
        return null;
    }
    
    @NonNull
    public List<NotificationChannelGroup> getNotificationChannelGroups() {
        if (Build$VERSION.SDK_INT >= 26) {
            return O\u3007oO\u3007oo8o.\u3007080(this.mNotificationManager);
        }
        return Collections.emptyList();
    }
    
    @NonNull
    public List<NotificationChannelGroupCompat> getNotificationChannelGroupsCompat() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 26) {
            final List<NotificationChannelGroup> notificationChannelGroups = this.getNotificationChannelGroups();
            if (!notificationChannelGroups.isEmpty()) {
                Object o;
                if (sdk_INT >= 28) {
                    o = Collections.emptyList();
                }
                else {
                    o = this.getNotificationChannels();
                }
                final ArrayList list = new ArrayList(notificationChannelGroups.size());
                for (final NotificationChannelGroup notificationChannelGroup : notificationChannelGroups) {
                    if (Build$VERSION.SDK_INT >= 28) {
                        list.add((Object)new NotificationChannelGroupCompat(notificationChannelGroup));
                    }
                    else {
                        list.add((Object)new NotificationChannelGroupCompat(notificationChannelGroup, (List<NotificationChannel>)o));
                    }
                }
                return (List<NotificationChannelGroupCompat>)list;
            }
        }
        return Collections.emptyList();
    }
    
    @NonNull
    public List<NotificationChannel> getNotificationChannels() {
        if (Build$VERSION.SDK_INT >= 26) {
            return o88O\u30078.\u3007080(this.mNotificationManager);
        }
        return Collections.emptyList();
    }
    
    @NonNull
    public List<NotificationChannelCompat> getNotificationChannelsCompat() {
        if (Build$VERSION.SDK_INT >= 26) {
            final List<NotificationChannel> notificationChannels = this.getNotificationChannels();
            if (!notificationChannels.isEmpty()) {
                final ArrayList list = new ArrayList(notificationChannels.size());
                final Iterator iterator = notificationChannels.iterator();
                while (iterator.hasNext()) {
                    list.add((Object)new NotificationChannelCompat((NotificationChannel)iterator.next()));
                }
                return (List<NotificationChannelCompat>)list;
            }
        }
        return Collections.emptyList();
    }
    
    @RequiresPermission("android.permission.POST_NOTIFICATIONS")
    public void notify(final int n, @NonNull final Notification notification) {
        this.notify(null, n, notification);
    }
    
    @RequiresPermission("android.permission.POST_NOTIFICATIONS")
    public void notify(@Nullable final String s, final int n, @NonNull final Notification notification) {
        if (useSideChannelForNotification(notification)) {
            this.pushSideChannelQueue((Task)new NotifyTask(this.mContext.getPackageName(), n, s, notification));
            this.mNotificationManager.cancel(s, n);
        }
        else {
            this.mNotificationManager.notify(s, n, notification);
        }
    }
    
    private static class CancelTask implements Task
    {
        final boolean all;
        final int id;
        final String packageName;
        final String tag;
        
        CancelTask(final String packageName) {
            this.packageName = packageName;
            this.id = 0;
            this.tag = null;
            this.all = true;
        }
        
        CancelTask(final String packageName, final int id, final String tag) {
            this.packageName = packageName;
            this.id = id;
            this.tag = tag;
            this.all = false;
        }
        
        @Override
        public void send(final INotificationSideChannel notificationSideChannel) throws RemoteException {
            if (this.all) {
                notificationSideChannel.cancelAll(this.packageName);
            }
            else {
                notificationSideChannel.cancel(this.packageName, this.id, this.tag);
            }
        }
        
        @NonNull
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("CancelTask[");
            sb.append("packageName:");
            sb.append(this.packageName);
            sb.append(", id:");
            sb.append(this.id);
            sb.append(", tag:");
            sb.append(this.tag);
            sb.append(", all:");
            sb.append(this.all);
            sb.append("]");
            return sb.toString();
        }
    }
    
    private interface Task
    {
        void send(final INotificationSideChannel p0) throws RemoteException;
    }
    
    private static class NotifyTask implements Task
    {
        final int id;
        final Notification notif;
        final String packageName;
        final String tag;
        
        NotifyTask(final String packageName, final int id, final String tag, final Notification notif) {
            this.packageName = packageName;
            this.id = id;
            this.tag = tag;
            this.notif = notif;
        }
        
        @Override
        public void send(final INotificationSideChannel notificationSideChannel) throws RemoteException {
            notificationSideChannel.notify(this.packageName, this.id, this.tag, this.notif);
        }
        
        @NonNull
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("NotifyTask[");
            sb.append("packageName:");
            sb.append(this.packageName);
            sb.append(", id:");
            sb.append(this.id);
            sb.append(", tag:");
            sb.append(this.tag);
            sb.append("]");
            return sb.toString();
        }
    }
    
    private static class ServiceConnectedEvent
    {
        final ComponentName componentName;
        final IBinder iBinder;
        
        ServiceConnectedEvent(final ComponentName componentName, final IBinder iBinder) {
            this.componentName = componentName;
            this.iBinder = iBinder;
        }
    }
    
    private static class SideChannelManager implements Handler$Callback, ServiceConnection
    {
        private static final int MSG_QUEUE_TASK = 0;
        private static final int MSG_RETRY_LISTENER_QUEUE = 3;
        private static final int MSG_SERVICE_CONNECTED = 1;
        private static final int MSG_SERVICE_DISCONNECTED = 2;
        private Set<String> mCachedEnabledPackages;
        private final Context mContext;
        private final Handler mHandler;
        private final HandlerThread mHandlerThread;
        private final Map<ComponentName, ListenerRecord> mRecordMap;
        
        SideChannelManager(final Context mContext) {
            this.mRecordMap = new HashMap<ComponentName, ListenerRecord>();
            this.mCachedEnabledPackages = new HashSet<String>();
            this.mContext = mContext;
            final HandlerThread mHandlerThread = new HandlerThread("NotificationManagerCompat");
            ((Thread)(this.mHandlerThread = mHandlerThread)).start();
            this.mHandler = new Handler(mHandlerThread.getLooper(), (Handler$Callback)this);
        }
        
        private boolean ensureServiceBound(final ListenerRecord listenerRecord) {
            if (listenerRecord.bound) {
                return true;
            }
            final boolean bindService = this.mContext.bindService(new Intent("android.support.BIND_NOTIFICATION_SIDE_CHANNEL").setComponent(listenerRecord.componentName), (ServiceConnection)this, 33);
            listenerRecord.bound = bindService;
            if (bindService) {
                listenerRecord.retryCount = 0;
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to bind to listener ");
                sb.append(listenerRecord.componentName);
                this.mContext.unbindService((ServiceConnection)this);
            }
            return listenerRecord.bound;
        }
        
        private void ensureServiceUnbound(final ListenerRecord listenerRecord) {
            if (listenerRecord.bound) {
                this.mContext.unbindService((ServiceConnection)this);
                listenerRecord.bound = false;
            }
            listenerRecord.service = null;
        }
        
        private void handleQueueTask(final Task e) {
            this.updateListenerMap();
            for (final ListenerRecord listenerRecord : this.mRecordMap.values()) {
                listenerRecord.taskQueue.add(e);
                this.processListenerQueue(listenerRecord);
            }
        }
        
        private void handleRetryListenerQueue(final ComponentName componentName) {
            final ListenerRecord listenerRecord = this.mRecordMap.get(componentName);
            if (listenerRecord != null) {
                this.processListenerQueue(listenerRecord);
            }
        }
        
        private void handleServiceConnected(final ComponentName componentName, final IBinder binder) {
            final ListenerRecord listenerRecord = this.mRecordMap.get(componentName);
            if (listenerRecord != null) {
                listenerRecord.service = INotificationSideChannel.Stub.asInterface(binder);
                listenerRecord.retryCount = 0;
                this.processListenerQueue(listenerRecord);
            }
        }
        
        private void handleServiceDisconnected(final ComponentName componentName) {
            final ListenerRecord listenerRecord = this.mRecordMap.get(componentName);
            if (listenerRecord != null) {
                this.ensureServiceUnbound(listenerRecord);
            }
        }
        
        private void processListenerQueue(final ListenerRecord listenerRecord) {
            if (Log.isLoggable("NotifManCompat", 3)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Processing component ");
                sb.append(listenerRecord.componentName);
                sb.append(", ");
                sb.append(listenerRecord.taskQueue.size());
                sb.append(" queued tasks");
            }
            if (listenerRecord.taskQueue.isEmpty()) {
                return;
            }
            if (this.ensureServiceBound(listenerRecord) && listenerRecord.service != null) {
                while (true) {
                    final Task obj = listenerRecord.taskQueue.peek();
                    if (obj == null) {
                        break;
                    }
                    try {
                        if (Log.isLoggable("NotifManCompat", 3)) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Sending task ");
                            sb2.append(obj);
                        }
                        obj.send(listenerRecord.service);
                        listenerRecord.taskQueue.remove();
                        continue;
                    }
                    catch (final RemoteException ex) {
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("RemoteException communicating with ");
                        sb3.append(listenerRecord.componentName);
                    }
                    catch (final DeadObjectException ex2) {
                        if (Log.isLoggable("NotifManCompat", 3)) {
                            final StringBuilder sb4 = new StringBuilder();
                            sb4.append("Remote service has died: ");
                            sb4.append(listenerRecord.componentName);
                        }
                    }
                    break;
                }
                if (!listenerRecord.taskQueue.isEmpty()) {
                    this.scheduleListenerRetry(listenerRecord);
                }
                return;
            }
            this.scheduleListenerRetry(listenerRecord);
        }
        
        private void scheduleListenerRetry(final ListenerRecord listenerRecord) {
            if (this.mHandler.hasMessages(3, (Object)listenerRecord.componentName)) {
                return;
            }
            final int retryCount = listenerRecord.retryCount + 1;
            if ((listenerRecord.retryCount = retryCount) > 6) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Giving up on delivering ");
                sb.append(listenerRecord.taskQueue.size());
                sb.append(" tasks to ");
                sb.append(listenerRecord.componentName);
                sb.append(" after ");
                sb.append(listenerRecord.retryCount);
                sb.append(" retries");
                listenerRecord.taskQueue.clear();
                return;
            }
            final int i = (1 << retryCount - 1) * 1000;
            if (Log.isLoggable("NotifManCompat", 3)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Scheduling retry for ");
                sb2.append(i);
                sb2.append(" ms");
            }
            this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(3, (Object)listenerRecord.componentName), (long)i);
        }
        
        private void updateListenerMap() {
            final Set<String> enabledListenerPackages = NotificationManagerCompat.getEnabledListenerPackages(this.mContext);
            if (enabledListenerPackages.equals(this.mCachedEnabledPackages)) {
                return;
            }
            this.mCachedEnabledPackages = enabledListenerPackages;
            final List queryIntentServices = this.mContext.getPackageManager().queryIntentServices(new Intent().setAction("android.support.BIND_NOTIFICATION_SIDE_CHANNEL"), 0);
            final HashSet set = new HashSet();
            for (final ResolveInfo resolveInfo : queryIntentServices) {
                if (!enabledListenerPackages.contains(resolveInfo.serviceInfo.packageName)) {
                    continue;
                }
                final ServiceInfo serviceInfo = resolveInfo.serviceInfo;
                final ComponentName obj = new ComponentName(serviceInfo.packageName, serviceInfo.name);
                if (resolveInfo.serviceInfo.permission != null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Permission present on component ");
                    sb.append(obj);
                    sb.append(", not adding listener record.");
                }
                else {
                    set.add(obj);
                }
            }
            for (final ComponentName obj2 : set) {
                if (!this.mRecordMap.containsKey(obj2)) {
                    if (Log.isLoggable("NotifManCompat", 3)) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Adding listener record for ");
                        sb2.append(obj2);
                    }
                    this.mRecordMap.put(obj2, new ListenerRecord(obj2));
                }
            }
            final Iterator<Map.Entry<ComponentName, ListenerRecord>> iterator3 = this.mRecordMap.entrySet().iterator();
            while (iterator3.hasNext()) {
                final Map.Entry<Object, V> entry = (Map.Entry<Object, V>)iterator3.next();
                if (!set.contains(entry.getKey())) {
                    if (Log.isLoggable("NotifManCompat", 3)) {
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("Removing listener record for ");
                        sb3.append(entry.getKey());
                    }
                    this.ensureServiceUnbound((ListenerRecord)entry.getValue());
                    iterator3.remove();
                }
            }
        }
        
        public boolean handleMessage(final Message message) {
            final int what = message.what;
            if (what == 0) {
                this.handleQueueTask((Task)message.obj);
                return true;
            }
            if (what == 1) {
                final ServiceConnectedEvent serviceConnectedEvent = (ServiceConnectedEvent)message.obj;
                this.handleServiceConnected(serviceConnectedEvent.componentName, serviceConnectedEvent.iBinder);
                return true;
            }
            if (what == 2) {
                this.handleServiceDisconnected((ComponentName)message.obj);
                return true;
            }
            if (what != 3) {
                return false;
            }
            this.handleRetryListenerQueue((ComponentName)message.obj);
            return true;
        }
        
        public void onServiceConnected(final ComponentName obj, final IBinder binder) {
            if (Log.isLoggable("NotifManCompat", 3)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Connected to service ");
                sb.append(obj);
            }
            this.mHandler.obtainMessage(1, (Object)new ServiceConnectedEvent(obj, binder)).sendToTarget();
        }
        
        public void onServiceDisconnected(final ComponentName obj) {
            if (Log.isLoggable("NotifManCompat", 3)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Disconnected from service ");
                sb.append(obj);
            }
            this.mHandler.obtainMessage(2, (Object)obj).sendToTarget();
        }
        
        public void queueTask(final Task task) {
            this.mHandler.obtainMessage(0, (Object)task).sendToTarget();
        }
        
        private static class ListenerRecord
        {
            boolean bound;
            final ComponentName componentName;
            int retryCount;
            INotificationSideChannel service;
            ArrayDeque<Task> taskQueue;
            
            ListenerRecord(final ComponentName componentName) {
                this.bound = false;
                this.taskQueue = new ArrayDeque<Task>();
                this.retryCount = 0;
                this.componentName = componentName;
            }
        }
    }
}

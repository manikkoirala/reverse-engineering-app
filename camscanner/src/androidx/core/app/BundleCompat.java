// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import android.annotation.SuppressLint;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.os.Bundle;

public final class BundleCompat
{
    private BundleCompat() {
    }
    
    @Nullable
    public static IBinder getBinder(@NonNull final Bundle bundle, @Nullable final String s) {
        return Api18Impl.getBinder(bundle, s);
    }
    
    public static void putBinder(@NonNull final Bundle bundle, @Nullable final String s, @Nullable final IBinder binder) {
        Api18Impl.putBinder(bundle, s, binder);
    }
    
    @RequiresApi(18)
    static class Api18Impl
    {
        private Api18Impl() {
        }
        
        @DoNotInline
        static IBinder getBinder(final Bundle bundle, final String s) {
            return bundle.getBinder(s);
        }
        
        @DoNotInline
        static void putBinder(final Bundle bundle, final String s, final IBinder binder) {
            bundle.putBinder(s, binder);
        }
    }
    
    @SuppressLint({ "BanUncheckedReflection" })
    static class BeforeApi18Impl
    {
        private static final String TAG = "BundleCompatBaseImpl";
        private static Method sGetIBinderMethod;
        private static boolean sGetIBinderMethodFetched;
        private static Method sPutIBinderMethod;
        private static boolean sPutIBinderMethodFetched;
        
        private BeforeApi18Impl() {
        }
        
        public static IBinder getBinder(final Bundle obj, final String s) {
            Label_0036: {
                if (BeforeApi18Impl.sGetIBinderMethodFetched) {
                    break Label_0036;
                }
                while (true) {
                    try {
                        (BeforeApi18Impl.sGetIBinderMethod = Bundle.class.getMethod("getIBinder", String.class)).setAccessible(true);
                        BeforeApi18Impl.sGetIBinderMethodFetched = true;
                        final Method sGetIBinderMethod = BeforeApi18Impl.sGetIBinderMethod;
                        if (sGetIBinderMethod != null) {
                            try {
                                return (IBinder)sGetIBinderMethod.invoke(obj, s);
                            }
                            catch (final InvocationTargetException | IllegalAccessException | IllegalArgumentException obj) {
                                BeforeApi18Impl.sGetIBinderMethod = null;
                            }
                        }
                        return null;
                    }
                    catch (final NoSuchMethodException ex) {
                        continue;
                    }
                    break;
                }
            }
        }
        
        public static void putBinder(final Bundle obj, final String s, final IBinder binder) {
            Label_0041: {
                if (BeforeApi18Impl.sPutIBinderMethodFetched) {
                    break Label_0041;
                }
                while (true) {
                    try {
                        (BeforeApi18Impl.sPutIBinderMethod = Bundle.class.getMethod("putIBinder", String.class, IBinder.class)).setAccessible(true);
                        BeforeApi18Impl.sPutIBinderMethodFetched = true;
                        final Method sPutIBinderMethod = BeforeApi18Impl.sPutIBinderMethod;
                        if (sPutIBinderMethod != null) {
                            try {
                                sPutIBinderMethod.invoke(obj, s, binder);
                            }
                            catch (final InvocationTargetException | IllegalAccessException | IllegalArgumentException obj) {
                                BeforeApi18Impl.sPutIBinderMethod = null;
                            }
                        }
                    }
                    catch (final NoSuchMethodException ex) {
                        continue;
                    }
                    break;
                }
            }
        }
    }
}

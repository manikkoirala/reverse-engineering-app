// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import androidx.annotation.NonNull;
import android.content.Intent;
import androidx.core.util.Consumer;

public interface OnNewIntentProvider
{
    void addOnNewIntentListener(@NonNull final Consumer<Intent> p0);
    
    void removeOnNewIntentListener(@NonNull final Consumer<Intent> p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import androidx.annotation.RestrictTo;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.app.Service;

public final class ServiceCompat
{
    public static final int START_STICKY = 1;
    public static final int STOP_FOREGROUND_DETACH = 2;
    public static final int STOP_FOREGROUND_REMOVE = 1;
    
    private ServiceCompat() {
    }
    
    public static void stopForeground(@NonNull final Service service, final int n) {
        if (Build$VERSION.SDK_INT >= 24) {
            Api24Impl.stopForeground(service, n);
        }
        else {
            boolean b = true;
            if ((n & 0x1) == 0x0) {
                b = false;
            }
            service.stopForeground(b);
        }
    }
    
    @RequiresApi(24)
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        @DoNotInline
        static void stopForeground(final Service service, final int n) {
            O0o\u3007.\u3007080(service, n);
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public @interface StopForegroundFlags {
    }
}

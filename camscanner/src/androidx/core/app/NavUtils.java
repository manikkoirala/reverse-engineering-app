// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.os.BaseBundle;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.os.Bundle;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build$VERSION;
import androidx.annotation.Nullable;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.Context;
import android.content.ComponentName;
import android.content.Intent;
import androidx.annotation.NonNull;
import android.app.Activity;

public final class NavUtils
{
    public static final String PARENT_ACTIVITY = "android.support.PARENT_ACTIVITY";
    private static final String TAG = "NavUtils";
    
    private NavUtils() {
    }
    
    @Nullable
    public static Intent getParentActivityIntent(@NonNull final Activity activity) {
        final Intent parentActivityIntent = Api16Impl.getParentActivityIntent(activity);
        if (parentActivityIntent != null) {
            return parentActivityIntent;
        }
        final String parentActivityName = getParentActivityName(activity);
        if (parentActivityName == null) {
            return null;
        }
        final ComponentName component = new ComponentName((Context)activity, parentActivityName);
        try {
            Intent intent;
            if (getParentActivityName((Context)activity, component) == null) {
                intent = Intent.makeMainActivity(component);
            }
            else {
                intent = new Intent().setComponent(component);
            }
            return intent;
        }
        catch (final PackageManager$NameNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("getParentActivityIntent: bad parentActivityName '");
            sb.append(parentActivityName);
            sb.append("' in manifest");
            return null;
        }
    }
    
    @Nullable
    public static Intent getParentActivityIntent(@NonNull final Context context, @NonNull ComponentName component) throws PackageManager$NameNotFoundException {
        final String parentActivityName = getParentActivityName(context, component);
        if (parentActivityName == null) {
            return null;
        }
        component = new ComponentName(component.getPackageName(), parentActivityName);
        Intent intent;
        if (getParentActivityName(context, component) == null) {
            intent = Intent.makeMainActivity(component);
        }
        else {
            intent = new Intent().setComponent(component);
        }
        return intent;
    }
    
    @Nullable
    public static Intent getParentActivityIntent(@NonNull final Context context, @NonNull final Class<?> clazz) throws PackageManager$NameNotFoundException {
        final String parentActivityName = getParentActivityName(context, new ComponentName(context, (Class)clazz));
        if (parentActivityName == null) {
            return null;
        }
        final ComponentName component = new ComponentName(context, parentActivityName);
        Intent intent;
        if (getParentActivityName(context, component) == null) {
            intent = Intent.makeMainActivity(component);
        }
        else {
            intent = new Intent().setComponent(component);
        }
        return intent;
    }
    
    @Nullable
    public static String getParentActivityName(@NonNull final Activity activity) {
        try {
            return getParentActivityName((Context)activity, activity.getComponentName());
        }
        catch (final PackageManager$NameNotFoundException cause) {
            throw new IllegalArgumentException((Throwable)cause);
        }
    }
    
    @Nullable
    public static String getParentActivityName(@NonNull final Context context, @NonNull final ComponentName componentName) throws PackageManager$NameNotFoundException {
        final PackageManager packageManager = context.getPackageManager();
        final int sdk_INT = Build$VERSION.SDK_INT;
        int n;
        if (sdk_INT >= 29) {
            n = 269222528;
        }
        else if (sdk_INT >= 24) {
            n = 787072;
        }
        else {
            n = 640;
        }
        final ActivityInfo activityInfo = packageManager.getActivityInfo(componentName, n);
        final String parentActivityName = activityInfo.parentActivityName;
        if (parentActivityName != null) {
            return parentActivityName;
        }
        final Bundle metaData = activityInfo.metaData;
        if (metaData == null) {
            return null;
        }
        final String string = ((BaseBundle)metaData).getString("android.support.PARENT_ACTIVITY");
        if (string == null) {
            return null;
        }
        String string2 = string;
        if (string.charAt(0) == '.') {
            final StringBuilder sb = new StringBuilder();
            sb.append(context.getPackageName());
            sb.append(string);
            string2 = sb.toString();
        }
        return string2;
    }
    
    public static void navigateUpFromSameTask(@NonNull final Activity activity) {
        final Intent parentActivityIntent = getParentActivityIntent(activity);
        if (parentActivityIntent != null) {
            navigateUpTo(activity, parentActivityIntent);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Activity ");
        sb.append(activity.getClass().getSimpleName());
        sb.append(" does not have a parent activity name specified. (Did you forget to add the android.support.PARENT_ACTIVITY <meta-data>  element in your manifest?)");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static void navigateUpTo(@NonNull final Activity activity, @NonNull final Intent intent) {
        Api16Impl.navigateUpTo(activity, intent);
    }
    
    public static boolean shouldUpRecreateTask(@NonNull final Activity activity, @NonNull final Intent intent) {
        return Api16Impl.shouldUpRecreateTask(activity, intent);
    }
    
    @RequiresApi(16)
    static class Api16Impl
    {
        private Api16Impl() {
        }
        
        @DoNotInline
        static Intent getParentActivityIntent(final Activity activity) {
            return activity.getParentActivityIntent();
        }
        
        @DoNotInline
        static boolean navigateUpTo(final Activity activity, final Intent intent) {
            return activity.navigateUpTo(intent);
        }
        
        @DoNotInline
        static boolean shouldUpRecreateTask(final Activity activity, final Intent intent) {
            return activity.shouldUpRecreateTask(intent);
        }
    }
}

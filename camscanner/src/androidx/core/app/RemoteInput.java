// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.os.BaseBundle;
import androidx.annotation.RestrictTo;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.HashSet;
import android.app.RemoteInput$Builder;
import androidx.annotation.DoNotInline;
import androidx.annotation.Nullable;
import java.util.HashMap;
import android.content.ClipDescription;
import androidx.annotation.RequiresApi;
import java.util.Iterator;
import android.content.ClipData;
import android.os.Build$VERSION;
import android.net.Uri;
import java.util.Map;
import android.content.Intent;
import androidx.annotation.NonNull;
import android.os.Bundle;
import java.util.Set;

public final class RemoteInput
{
    public static final int EDIT_CHOICES_BEFORE_SENDING_AUTO = 0;
    public static final int EDIT_CHOICES_BEFORE_SENDING_DISABLED = 1;
    public static final int EDIT_CHOICES_BEFORE_SENDING_ENABLED = 2;
    private static final String EXTRA_DATA_TYPE_RESULTS_DATA = "android.remoteinput.dataTypeResultsData";
    public static final String EXTRA_RESULTS_DATA = "android.remoteinput.resultsData";
    private static final String EXTRA_RESULTS_SOURCE = "android.remoteinput.resultsSource";
    public static final String RESULTS_CLIP_LABEL = "android.remoteinput.results";
    public static final int SOURCE_CHOICE = 1;
    public static final int SOURCE_FREE_FORM_INPUT = 0;
    private final boolean mAllowFreeFormTextInput;
    private final Set<String> mAllowedDataTypes;
    private final CharSequence[] mChoices;
    private final int mEditChoicesBeforeSending;
    private final Bundle mExtras;
    private final CharSequence mLabel;
    private final String mResultKey;
    
    RemoteInput(final String mResultKey, final CharSequence mLabel, final CharSequence[] mChoices, final boolean mAllowFreeFormTextInput, final int mEditChoicesBeforeSending, final Bundle mExtras, final Set<String> mAllowedDataTypes) {
        this.mResultKey = mResultKey;
        this.mLabel = mLabel;
        this.mChoices = mChoices;
        this.mAllowFreeFormTextInput = mAllowFreeFormTextInput;
        this.mEditChoicesBeforeSending = mEditChoicesBeforeSending;
        this.mExtras = mExtras;
        this.mAllowedDataTypes = mAllowedDataTypes;
        if (this.getEditChoicesBeforeSending() == 2 && !this.getAllowFreeFormInput()) {
            throw new IllegalArgumentException("setEditChoicesBeforeSending requires setAllowFreeFormInput");
        }
    }
    
    public static void addDataResultToIntent(@NonNull final RemoteInput remoteInput, @NonNull final Intent intent, @NonNull final Map<String, Uri> map) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.addDataResultToIntent(remoteInput, intent, map);
        }
        else {
            Intent clipDataIntentFromIntent;
            if ((clipDataIntentFromIntent = getClipDataIntentFromIntent(intent)) == null) {
                clipDataIntentFromIntent = new Intent();
            }
            for (final Map.Entry<String, V> entry : map.entrySet()) {
                final String s = entry.getKey();
                final Uri uri = (Uri)entry.getValue();
                if (s == null) {
                    continue;
                }
                Bundle bundleExtra;
                if ((bundleExtra = clipDataIntentFromIntent.getBundleExtra(getExtraResultsKeyForData(s))) == null) {
                    bundleExtra = new Bundle();
                }
                ((BaseBundle)bundleExtra).putString(remoteInput.getResultKey(), uri.toString());
                clipDataIntentFromIntent.putExtra(getExtraResultsKeyForData(s), bundleExtra);
            }
            Api16Impl.setClipData(intent, ClipData.newIntent((CharSequence)"android.remoteinput.results", clipDataIntentFromIntent));
        }
    }
    
    public static void addResultsToIntent(@NonNull final RemoteInput[] array, @NonNull final Intent intent, @NonNull Bundle bundle) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api20Impl.addResultsToIntent(fromCompat(array), intent, bundle);
        }
        else {
            final Bundle resultsFromIntent = getResultsFromIntent(intent);
            final int resultsSource = getResultsSource(intent);
            if (resultsFromIntent != null) {
                resultsFromIntent.putAll(bundle);
                bundle = resultsFromIntent;
            }
            for (final RemoteInput remoteInput : array) {
                final Map<String, Uri> dataResultsFromIntent = getDataResultsFromIntent(intent, remoteInput.getResultKey());
                Api20Impl.addResultsToIntent(fromCompat(new RemoteInput[] { remoteInput }), intent, bundle);
                if (dataResultsFromIntent != null) {
                    addDataResultToIntent(remoteInput, intent, dataResultsFromIntent);
                }
            }
            setResultsSource(intent, resultsSource);
        }
    }
    
    @RequiresApi(20)
    static android.app.RemoteInput fromCompat(final RemoteInput remoteInput) {
        return Api20Impl.fromCompat(remoteInput);
    }
    
    @RequiresApi(20)
    static android.app.RemoteInput[] fromCompat(final RemoteInput[] array) {
        if (array == null) {
            return null;
        }
        final android.app.RemoteInput[] array2 = new android.app.RemoteInput[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = fromCompat(array[i]);
        }
        return array2;
    }
    
    @RequiresApi(20)
    static RemoteInput fromPlatform(final android.app.RemoteInput remoteInput) {
        return Api20Impl.fromPlatform(remoteInput);
    }
    
    @RequiresApi(16)
    private static Intent getClipDataIntentFromIntent(final Intent intent) {
        final ClipData clipData = Api16Impl.getClipData(intent);
        if (clipData == null) {
            return null;
        }
        final ClipDescription description = clipData.getDescription();
        if (!description.hasMimeType("text/vnd.android.intent")) {
            return null;
        }
        if (!description.getLabel().toString().contentEquals("android.remoteinput.results")) {
            return null;
        }
        return clipData.getItemAt(0).getIntent();
    }
    
    @Nullable
    public static Map<String, Uri> getDataResultsFromIntent(@NonNull final Intent intent, @NonNull final String s) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getDataResultsFromIntent(intent, s);
        }
        final Intent clipDataIntentFromIntent = getClipDataIntentFromIntent(intent);
        Map<String, Uri> map = null;
        if (clipDataIntentFromIntent == null) {
            return null;
        }
        final HashMap hashMap = new HashMap();
        for (final String s2 : ((BaseBundle)clipDataIntentFromIntent.getExtras()).keySet()) {
            if (s2.startsWith("android.remoteinput.dataTypeResultsData")) {
                final String substring = s2.substring(39);
                if (substring.isEmpty()) {
                    continue;
                }
                final String string = ((BaseBundle)clipDataIntentFromIntent.getBundleExtra(s2)).getString(s);
                if (string == null) {
                    continue;
                }
                if (string.isEmpty()) {
                    continue;
                }
                hashMap.put(substring, Uri.parse(string));
            }
        }
        if (!hashMap.isEmpty()) {
            map = hashMap;
        }
        return map;
    }
    
    private static String getExtraResultsKeyForData(final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("android.remoteinput.dataTypeResultsData");
        sb.append(str);
        return sb.toString();
    }
    
    @Nullable
    public static Bundle getResultsFromIntent(@NonNull final Intent intent) {
        return Api20Impl.getResultsFromIntent(intent);
    }
    
    public static int getResultsSource(@NonNull Intent clipDataIntentFromIntent) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getResultsSource(clipDataIntentFromIntent);
        }
        clipDataIntentFromIntent = getClipDataIntentFromIntent(clipDataIntentFromIntent);
        if (clipDataIntentFromIntent == null) {
            return 0;
        }
        return ((BaseBundle)clipDataIntentFromIntent.getExtras()).getInt("android.remoteinput.resultsSource", 0);
    }
    
    public static void setResultsSource(@NonNull final Intent intent, final int n) {
        if (Build$VERSION.SDK_INT >= 28) {
            Api28Impl.setResultsSource(intent, n);
        }
        else {
            Intent clipDataIntentFromIntent;
            if ((clipDataIntentFromIntent = getClipDataIntentFromIntent(intent)) == null) {
                clipDataIntentFromIntent = new Intent();
            }
            clipDataIntentFromIntent.putExtra("android.remoteinput.resultsSource", n);
            Api16Impl.setClipData(intent, ClipData.newIntent((CharSequence)"android.remoteinput.results", clipDataIntentFromIntent));
        }
    }
    
    public boolean getAllowFreeFormInput() {
        return this.mAllowFreeFormTextInput;
    }
    
    @Nullable
    public Set<String> getAllowedDataTypes() {
        return this.mAllowedDataTypes;
    }
    
    @Nullable
    public CharSequence[] getChoices() {
        return this.mChoices;
    }
    
    public int getEditChoicesBeforeSending() {
        return this.mEditChoicesBeforeSending;
    }
    
    @NonNull
    public Bundle getExtras() {
        return this.mExtras;
    }
    
    @Nullable
    public CharSequence getLabel() {
        return this.mLabel;
    }
    
    @NonNull
    public String getResultKey() {
        return this.mResultKey;
    }
    
    public boolean isDataOnly() {
        return !this.getAllowFreeFormInput() && (this.getChoices() == null || this.getChoices().length == 0) && this.getAllowedDataTypes() != null && !this.getAllowedDataTypes().isEmpty();
    }
    
    @RequiresApi(16)
    static class Api16Impl
    {
        private Api16Impl() {
        }
        
        @DoNotInline
        static ClipData getClipData(final Intent intent) {
            return intent.getClipData();
        }
        
        @DoNotInline
        static void setClipData(final Intent intent, final ClipData clipData) {
            intent.setClipData(clipData);
        }
    }
    
    @RequiresApi(20)
    static class Api20Impl
    {
        private Api20Impl() {
        }
        
        @DoNotInline
        static void addResultsToIntent(final Object o, final Intent intent, final Bundle bundle) {
            android.app.RemoteInput.addResultsToIntent((android.app.RemoteInput[])o, intent, bundle);
        }
        
        public static android.app.RemoteInput fromCompat(final RemoteInput remoteInput) {
            final RemoteInput$Builder addExtras = new RemoteInput$Builder(remoteInput.getResultKey()).setLabel(remoteInput.getLabel()).setChoices(remoteInput.getChoices()).setAllowFreeFormInput(remoteInput.getAllowFreeFormInput()).addExtras(remoteInput.getExtras());
            if (Build$VERSION.SDK_INT >= 26) {
                final Set<String> allowedDataTypes = remoteInput.getAllowedDataTypes();
                if (allowedDataTypes != null) {
                    final Iterator<String> iterator = allowedDataTypes.iterator();
                    while (iterator.hasNext()) {
                        Api26Impl.setAllowDataType(addExtras, iterator.next(), true);
                    }
                }
            }
            if (Build$VERSION.SDK_INT >= 29) {
                Api29Impl.setEditChoicesBeforeSending(addExtras, remoteInput.getEditChoicesBeforeSending());
            }
            return addExtras.build();
        }
        
        static RemoteInput fromPlatform(final Object o) {
            final android.app.RemoteInput remoteInput = (android.app.RemoteInput)o;
            final Builder addExtras = new Builder(remoteInput.getResultKey()).setLabel(remoteInput.getLabel()).setChoices(remoteInput.getChoices()).setAllowFreeFormInput(remoteInput.getAllowFreeFormInput()).addExtras(remoteInput.getExtras());
            if (Build$VERSION.SDK_INT >= 26) {
                final Set<String> allowedDataTypes = Api26Impl.getAllowedDataTypes(remoteInput);
                if (allowedDataTypes != null) {
                    final Iterator<String> iterator = allowedDataTypes.iterator();
                    while (iterator.hasNext()) {
                        addExtras.setAllowDataType(iterator.next(), true);
                    }
                }
            }
            if (Build$VERSION.SDK_INT >= 29) {
                addExtras.setEditChoicesBeforeSending(Api29Impl.getEditChoicesBeforeSending(remoteInput));
            }
            return addExtras.build();
        }
        
        @DoNotInline
        static Bundle getResultsFromIntent(final Intent intent) {
            return android.app.RemoteInput.getResultsFromIntent(intent);
        }
    }
    
    @RequiresApi(26)
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        @DoNotInline
        static void addDataResultToIntent(final RemoteInput remoteInput, final Intent intent, final Map<String, Uri> map) {
            O0o8\u3007O.\u3007080(RemoteInput.fromCompat(remoteInput), intent, (Map)map);
        }
        
        @DoNotInline
        static Set<String> getAllowedDataTypes(final Object o) {
            return \u3007oOo\u3007.\u3007080((android.app.RemoteInput)o);
        }
        
        @DoNotInline
        static Map<String, Uri> getDataResultsFromIntent(final Intent intent, final String s) {
            return o88o0O.\u3007080(intent, s);
        }
        
        @DoNotInline
        static RemoteInput$Builder setAllowDataType(final RemoteInput$Builder remoteInput$Builder, final String s, final boolean b) {
            return \u30070O00oO.\u3007080(remoteInput$Builder, s, b);
        }
    }
    
    @RequiresApi(28)
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        @DoNotInline
        static int getResultsSource(final Intent intent) {
            return \u300708O8o8.\u3007080(intent);
        }
        
        @DoNotInline
        static void setResultsSource(final Intent intent, final int n) {
            O08O0\u3007O.\u3007080(intent, n);
        }
    }
    
    @RequiresApi(29)
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        @DoNotInline
        static int getEditChoicesBeforeSending(final Object o) {
            return ooo0\u3007O88O.\u3007080((android.app.RemoteInput)o);
        }
        
        @DoNotInline
        static RemoteInput$Builder setEditChoicesBeforeSending(final RemoteInput$Builder remoteInput$Builder, final int n) {
            return o\u3007OOo000.\u3007080(remoteInput$Builder, n);
        }
    }
    
    public static final class Builder
    {
        private boolean mAllowFreeFormTextInput;
        private final Set<String> mAllowedDataTypes;
        private CharSequence[] mChoices;
        private int mEditChoicesBeforeSending;
        private final Bundle mExtras;
        private CharSequence mLabel;
        private final String mResultKey;
        
        public Builder(@NonNull final String mResultKey) {
            this.mAllowedDataTypes = new HashSet<String>();
            this.mExtras = new Bundle();
            this.mAllowFreeFormTextInput = true;
            this.mEditChoicesBeforeSending = 0;
            if (mResultKey != null) {
                this.mResultKey = mResultKey;
                return;
            }
            throw new IllegalArgumentException("Result key can't be null");
        }
        
        @NonNull
        public Builder addExtras(@NonNull final Bundle bundle) {
            if (bundle != null) {
                this.mExtras.putAll(bundle);
            }
            return this;
        }
        
        @NonNull
        public RemoteInput build() {
            return new RemoteInput(this.mResultKey, this.mLabel, this.mChoices, this.mAllowFreeFormTextInput, this.mEditChoicesBeforeSending, this.mExtras, this.mAllowedDataTypes);
        }
        
        @NonNull
        public Bundle getExtras() {
            return this.mExtras;
        }
        
        @NonNull
        public Builder setAllowDataType(@NonNull final String s, final boolean b) {
            if (b) {
                this.mAllowedDataTypes.add(s);
            }
            else {
                this.mAllowedDataTypes.remove(s);
            }
            return this;
        }
        
        @NonNull
        public Builder setAllowFreeFormInput(final boolean mAllowFreeFormTextInput) {
            this.mAllowFreeFormTextInput = mAllowFreeFormTextInput;
            return this;
        }
        
        @NonNull
        public Builder setChoices(@Nullable final CharSequence[] mChoices) {
            this.mChoices = mChoices;
            return this;
        }
        
        @NonNull
        public Builder setEditChoicesBeforeSending(final int mEditChoicesBeforeSending) {
            this.mEditChoicesBeforeSending = mEditChoicesBeforeSending;
            return this;
        }
        
        @NonNull
        public Builder setLabel(@Nullable final CharSequence mLabel) {
            this.mLabel = mLabel;
            return this;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public @interface EditChoicesBeforeSending {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public @interface Source {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import androidx.appcompat.widget.oo88o8O;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import androidx.annotation.Nullable;
import android.app.AppOpsManager;
import android.os.Binder;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.content.Context;

public final class AppOpsManagerCompat
{
    public static final int MODE_ALLOWED = 0;
    public static final int MODE_DEFAULT = 3;
    public static final int MODE_ERRORED = 2;
    public static final int MODE_IGNORED = 1;
    
    private AppOpsManagerCompat() {
    }
    
    public static int checkOrNoteProxyOp(@NonNull final Context context, final int n, @NonNull final String s, @NonNull final String s2) {
        if (Build$VERSION.SDK_INT < 29) {
            return noteProxyOpNoThrow(context, s, s2);
        }
        final AppOpsManager systemService = Api29Impl.getSystemService(context);
        final int checkOpNoThrow = Api29Impl.checkOpNoThrow(systemService, s, Binder.getCallingUid(), s2);
        if (checkOpNoThrow != 0) {
            return checkOpNoThrow;
        }
        return Api29Impl.checkOpNoThrow(systemService, s, n, Api29Impl.getOpPackageName(context));
    }
    
    public static int noteOp(@NonNull final Context context, @NonNull final String s, final int n, @NonNull final String s2) {
        return Api19Impl.noteOp((AppOpsManager)context.getSystemService("appops"), s, n, s2);
    }
    
    public static int noteOpNoThrow(@NonNull final Context context, @NonNull final String s, final int n, @NonNull final String s2) {
        return Api19Impl.noteOpNoThrow((AppOpsManager)context.getSystemService("appops"), s, n, s2);
    }
    
    public static int noteProxyOp(@NonNull final Context context, @NonNull final String s, @NonNull final String s2) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.noteProxyOp(Api23Impl.getSystemService(context, AppOpsManager.class), s, s2);
        }
        return 1;
    }
    
    public static int noteProxyOpNoThrow(@NonNull final Context context, @NonNull final String s, @NonNull final String s2) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.noteProxyOpNoThrow(Api23Impl.getSystemService(context, AppOpsManager.class), s, s2);
        }
        return 1;
    }
    
    @Nullable
    public static String permissionToOp(@NonNull final String s) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.permissionToOp(s);
        }
        return null;
    }
    
    @RequiresApi(19)
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        @DoNotInline
        static int noteOp(final AppOpsManager appOpsManager, final String s, final int n, final String s2) {
            return appOpsManager.noteOp(s, n, s2);
        }
        
        @DoNotInline
        static int noteOpNoThrow(final AppOpsManager appOpsManager, final String s, final int n, final String s2) {
            return appOpsManager.noteOpNoThrow(s, n, s2);
        }
    }
    
    @RequiresApi(23)
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        @DoNotInline
        static <T> T getSystemService(final Context context, final Class<T> clazz) {
            return (T)oo88o8O.\u3007080(context, (Class)clazz);
        }
        
        @DoNotInline
        static int noteProxyOp(final AppOpsManager appOpsManager, final String s, final String s2) {
            return o800o8O.\u3007080(appOpsManager, s, s2);
        }
        
        @DoNotInline
        static int noteProxyOpNoThrow(final AppOpsManager appOpsManager, final String s, final String s2) {
            return \u30070\u3007O0088o.\u3007080(appOpsManager, s, s2);
        }
        
        @DoNotInline
        static String permissionToOp(final String s) {
            return OoO8.\u3007080(s);
        }
    }
    
    @RequiresApi(29)
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        @DoNotInline
        static int checkOpNoThrow(@Nullable final AppOpsManager appOpsManager, @NonNull final String s, final int n, @NonNull final String s2) {
            if (appOpsManager == null) {
                return 1;
            }
            return appOpsManager.checkOpNoThrow(s, n, s2);
        }
        
        @DoNotInline
        @NonNull
        static String getOpPackageName(@NonNull final Context context) {
            return \u3007O888o0o.\u3007080(context);
        }
        
        @DoNotInline
        @Nullable
        static AppOpsManager getSystemService(@NonNull final Context context) {
            return (AppOpsManager)oo88o8O.\u3007080(context, (Class)AppOpsManager.class);
        }
    }
}

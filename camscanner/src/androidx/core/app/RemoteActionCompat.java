// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import androidx.annotation.DoNotInline;
import android.graphics.drawable.Icon;
import android.annotation.SuppressLint;
import androidx.annotation.RequiresApi;
import android.os.Build$VERSION;
import android.app.RemoteAction;
import androidx.core.util.Preconditions;
import androidx.core.graphics.drawable.IconCompat;
import androidx.annotation.RestrictTo;
import androidx.annotation.NonNull;
import android.app.PendingIntent;
import androidx.versionedparcelable.VersionedParcelable;

public final class RemoteActionCompat implements VersionedParcelable
{
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public PendingIntent mActionIntent;
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public CharSequence mContentDescription;
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public boolean mEnabled;
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public IconCompat mIcon;
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public boolean mShouldShowIcon;
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public CharSequence mTitle;
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public RemoteActionCompat() {
    }
    
    public RemoteActionCompat(@NonNull final RemoteActionCompat remoteActionCompat) {
        Preconditions.checkNotNull(remoteActionCompat);
        this.mIcon = remoteActionCompat.mIcon;
        this.mTitle = remoteActionCompat.mTitle;
        this.mContentDescription = remoteActionCompat.mContentDescription;
        this.mActionIntent = remoteActionCompat.mActionIntent;
        this.mEnabled = remoteActionCompat.mEnabled;
        this.mShouldShowIcon = remoteActionCompat.mShouldShowIcon;
    }
    
    public RemoteActionCompat(@NonNull final IconCompat iconCompat, @NonNull final CharSequence charSequence, @NonNull final CharSequence charSequence2, @NonNull final PendingIntent pendingIntent) {
        this.mIcon = Preconditions.checkNotNull(iconCompat);
        this.mTitle = Preconditions.checkNotNull(charSequence);
        this.mContentDescription = Preconditions.checkNotNull(charSequence2);
        this.mActionIntent = Preconditions.checkNotNull(pendingIntent);
        this.mEnabled = true;
        this.mShouldShowIcon = true;
    }
    
    @NonNull
    @RequiresApi(26)
    public static RemoteActionCompat createFromRemoteAction(@NonNull final RemoteAction remoteAction) {
        Preconditions.checkNotNull(remoteAction);
        final RemoteActionCompat remoteActionCompat = new RemoteActionCompat(IconCompat.createFromIcon(Api26Impl.getIcon(remoteAction)), Api26Impl.getTitle(remoteAction), Api26Impl.getContentDescription(remoteAction), Api26Impl.getActionIntent(remoteAction));
        remoteActionCompat.setEnabled(Api26Impl.isEnabled(remoteAction));
        if (Build$VERSION.SDK_INT >= 28) {
            remoteActionCompat.setShouldShowIcon(Api28Impl.shouldShowIcon(remoteAction));
        }
        return remoteActionCompat;
    }
    
    @NonNull
    public PendingIntent getActionIntent() {
        return this.mActionIntent;
    }
    
    @NonNull
    public CharSequence getContentDescription() {
        return this.mContentDescription;
    }
    
    @NonNull
    public IconCompat getIcon() {
        return this.mIcon;
    }
    
    @NonNull
    public CharSequence getTitle() {
        return this.mTitle;
    }
    
    public boolean isEnabled() {
        return this.mEnabled;
    }
    
    public void setEnabled(final boolean mEnabled) {
        this.mEnabled = mEnabled;
    }
    
    public void setShouldShowIcon(final boolean mShouldShowIcon) {
        this.mShouldShowIcon = mShouldShowIcon;
    }
    
    @SuppressLint({ "KotlinPropertyAccess" })
    public boolean shouldShowIcon() {
        return this.mShouldShowIcon;
    }
    
    @NonNull
    @RequiresApi(26)
    public RemoteAction toRemoteAction() {
        final RemoteAction remoteAction = Api26Impl.createRemoteAction(this.mIcon.toIcon(), this.mTitle, this.mContentDescription, this.mActionIntent);
        Api26Impl.setEnabled(remoteAction, this.isEnabled());
        if (Build$VERSION.SDK_INT >= 28) {
            Api28Impl.setShouldShowIcon(remoteAction, this.shouldShowIcon());
        }
        return remoteAction;
    }
    
    @RequiresApi(26)
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        @DoNotInline
        static RemoteAction createRemoteAction(final Icon icon, final CharSequence charSequence, final CharSequence charSequence2, final PendingIntent pendingIntent) {
            return new RemoteAction(icon, charSequence, charSequence2, pendingIntent);
        }
        
        @DoNotInline
        static PendingIntent getActionIntent(final RemoteAction remoteAction) {
            return \u30078\u3007o\u30078.\u3007080(remoteAction);
        }
        
        @DoNotInline
        static CharSequence getContentDescription(final RemoteAction remoteAction) {
            return \u300708\u30070\u3007o\u30078.\u3007080(remoteAction);
        }
        
        @DoNotInline
        static Icon getIcon(final RemoteAction remoteAction) {
            return o8\u3007.\u3007080(remoteAction);
        }
        
        @DoNotInline
        static CharSequence getTitle(final RemoteAction remoteAction) {
            return o0oO.\u3007080(remoteAction);
        }
        
        @DoNotInline
        static boolean isEnabled(final RemoteAction remoteAction) {
            return O8O\u30078oo08.\u3007080(remoteAction);
        }
        
        @DoNotInline
        static void setEnabled(final RemoteAction remoteAction, final boolean b) {
            \u3007\u3007O00\u30078.\u3007080(remoteAction, b);
        }
    }
    
    @RequiresApi(28)
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        @DoNotInline
        static void setShouldShowIcon(final RemoteAction remoteAction, final boolean b) {
            \u3007o\u3007o.\u3007080(remoteAction, b);
        }
        
        @DoNotInline
        static boolean shouldShowIcon(final RemoteAction remoteAction) {
            return \u3007\u3007o0o.\u3007080(remoteAction);
        }
    }
}

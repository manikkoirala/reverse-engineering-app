// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.os.BaseBundle;
import android.app.Notification$Action$Builder;
import android.app.Notification$MessagingStyle$Message;
import android.app.Notification$Builder;
import android.text.SpannableStringBuilder;
import androidx.core.text.BidiFormatter;
import android.content.res.ColorStateList;
import android.text.style.TextAppearanceSpan;
import java.util.Collections;
import android.app.Notification$Style;
import android.app.RemoteInput$Builder;
import android.media.AudioAttributes$Builder;
import android.net.Uri;
import androidx.core.content.pm.ShortcutInfoCompat;
import androidx.browser.trusted.o\u30070;
import android.text.TextUtils;
import android.app.Notification$BubbleMetadata$Builder;
import androidx.annotation.Dimension;
import android.app.Notification$BubbleMetadata;
import androidx.annotation.DimenRes;
import android.os.SystemClock;
import java.text.NumberFormat;
import android.widget.RemoteViews;
import android.graphics.drawable.Drawable;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuff$Mode;
import android.graphics.Bitmap$Config;
import android.app.Notification$DecoratedCustomViewStyle;
import android.app.Notification$MessagingStyle;
import android.app.Notification$InboxStyle;
import android.app.Notification$BigTextStyle;
import androidx.core.R;
import android.content.Context;
import android.app.Notification$BigPictureStyle;
import android.graphics.Bitmap;
import android.graphics.drawable.Icon;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.Collection;
import java.util.Arrays;
import android.content.res.Resources;
import android.app.PendingIntent;
import java.util.Iterator;
import android.os.Parcelable;
import android.content.LocusId;
import androidx.core.content.LocusIdCompat;
import java.util.ArrayList;
import java.util.List;
import androidx.annotation.RestrictTo;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.core.graphics.drawable.IconCompat;
import java.util.Set;
import android.os.Build$VERSION;
import android.app.Notification$Action;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.app.Notification;
import android.annotation.SuppressLint;
import androidx.annotation.ColorInt;

public class NotificationCompat
{
    public static final int BADGE_ICON_LARGE = 2;
    public static final int BADGE_ICON_NONE = 0;
    public static final int BADGE_ICON_SMALL = 1;
    public static final String CATEGORY_ALARM = "alarm";
    public static final String CATEGORY_CALL = "call";
    public static final String CATEGORY_EMAIL = "email";
    public static final String CATEGORY_ERROR = "err";
    public static final String CATEGORY_EVENT = "event";
    public static final String CATEGORY_LOCATION_SHARING = "location_sharing";
    public static final String CATEGORY_MESSAGE = "msg";
    public static final String CATEGORY_MISSED_CALL = "missed_call";
    public static final String CATEGORY_NAVIGATION = "navigation";
    public static final String CATEGORY_PROGRESS = "progress";
    public static final String CATEGORY_PROMO = "promo";
    public static final String CATEGORY_RECOMMENDATION = "recommendation";
    public static final String CATEGORY_REMINDER = "reminder";
    public static final String CATEGORY_SERVICE = "service";
    public static final String CATEGORY_SOCIAL = "social";
    public static final String CATEGORY_STATUS = "status";
    public static final String CATEGORY_STOPWATCH = "stopwatch";
    public static final String CATEGORY_SYSTEM = "sys";
    public static final String CATEGORY_TRANSPORT = "transport";
    public static final String CATEGORY_WORKOUT = "workout";
    @ColorInt
    public static final int COLOR_DEFAULT = 0;
    public static final int DEFAULT_ALL = -1;
    public static final int DEFAULT_LIGHTS = 4;
    public static final int DEFAULT_SOUND = 1;
    public static final int DEFAULT_VIBRATE = 2;
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_AUDIO_CONTENTS_URI = "android.audioContents";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_BACKGROUND_IMAGE_URI = "android.backgroundImageUri";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_BIG_TEXT = "android.bigText";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_CHANNEL_GROUP_ID = "android.intent.extra.CHANNEL_GROUP_ID";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_CHANNEL_ID = "android.intent.extra.CHANNEL_ID";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_CHRONOMETER_COUNT_DOWN = "android.chronometerCountDown";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_COLORIZED = "android.colorized";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_COMPACT_ACTIONS = "android.compactActions";
    public static final String EXTRA_COMPAT_TEMPLATE = "androidx.core.app.extra.COMPAT_TEMPLATE";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_CONVERSATION_TITLE = "android.conversationTitle";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_HIDDEN_CONVERSATION_TITLE = "android.hiddenConversationTitle";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_HISTORIC_MESSAGES = "android.messages.historic";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_INFO_TEXT = "android.infoText";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_IS_GROUP_CONVERSATION = "android.isGroupConversation";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_LARGE_ICON = "android.largeIcon";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_LARGE_ICON_BIG = "android.largeIcon.big";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_MEDIA_SESSION = "android.mediaSession";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_MESSAGES = "android.messages";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_MESSAGING_STYLE_USER = "android.messagingStyleUser";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_NOTIFICATION_ID = "android.intent.extra.NOTIFICATION_ID";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_NOTIFICATION_TAG = "android.intent.extra.NOTIFICATION_TAG";
    @Deprecated
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_PEOPLE = "android.people";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_PEOPLE_LIST = "android.people.list";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_PICTURE = "android.picture";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_PICTURE_CONTENT_DESCRIPTION = "android.pictureContentDescription";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_PICTURE_ICON = "android.pictureIcon";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_PROGRESS = "android.progress";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_PROGRESS_INDETERMINATE = "android.progressIndeterminate";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_PROGRESS_MAX = "android.progressMax";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_REMOTE_INPUT_HISTORY = "android.remoteInputHistory";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_SELF_DISPLAY_NAME = "android.selfDisplayName";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_SHOW_BIG_PICTURE_WHEN_COLLAPSED = "android.showBigPictureWhenCollapsed";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_SHOW_CHRONOMETER = "android.showChronometer";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_SHOW_WHEN = "android.showWhen";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_SMALL_ICON = "android.icon";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_SUB_TEXT = "android.subText";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_SUMMARY_TEXT = "android.summaryText";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_TEMPLATE = "android.template";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_TEXT = "android.text";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_TEXT_LINES = "android.textLines";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_TITLE = "android.title";
    @SuppressLint({ "ActionValue" })
    public static final String EXTRA_TITLE_BIG = "android.title.big";
    public static final int FLAG_AUTO_CANCEL = 16;
    public static final int FLAG_BUBBLE = 4096;
    public static final int FLAG_FOREGROUND_SERVICE = 64;
    public static final int FLAG_GROUP_SUMMARY = 512;
    @Deprecated
    public static final int FLAG_HIGH_PRIORITY = 128;
    public static final int FLAG_INSISTENT = 4;
    public static final int FLAG_LOCAL_ONLY = 256;
    public static final int FLAG_NO_CLEAR = 32;
    public static final int FLAG_ONGOING_EVENT = 2;
    public static final int FLAG_ONLY_ALERT_ONCE = 8;
    public static final int FLAG_SHOW_LIGHTS = 1;
    public static final int FOREGROUND_SERVICE_DEFAULT = 0;
    public static final int FOREGROUND_SERVICE_DEFERRED = 2;
    public static final int FOREGROUND_SERVICE_IMMEDIATE = 1;
    public static final int GROUP_ALERT_ALL = 0;
    public static final int GROUP_ALERT_CHILDREN = 2;
    public static final int GROUP_ALERT_SUMMARY = 1;
    public static final String GROUP_KEY_SILENT = "silent";
    @SuppressLint({ "ActionValue" })
    public static final String INTENT_CATEGORY_NOTIFICATION_PREFERENCES = "android.intent.category.NOTIFICATION_PREFERENCES";
    public static final int PRIORITY_DEFAULT = 0;
    public static final int PRIORITY_HIGH = 1;
    public static final int PRIORITY_LOW = -1;
    public static final int PRIORITY_MAX = 2;
    public static final int PRIORITY_MIN = -2;
    public static final int STREAM_DEFAULT = -1;
    public static final int VISIBILITY_PRIVATE = 0;
    public static final int VISIBILITY_PUBLIC = 1;
    public static final int VISIBILITY_SECRET = -1;
    
    @Deprecated
    public NotificationCompat() {
    }
    
    @Nullable
    public static Action getAction(@NonNull final Notification notification, final int n) {
        return getActionCompatFromAction(notification.actions[n]);
    }
    
    @NonNull
    @RequiresApi(20)
    static Action getActionCompatFromAction(@NonNull final Notification$Action notification$Action) {
        final android.app.RemoteInput[] remoteInputs = notification$Action.getRemoteInputs();
        IconCompat fromIconOrNullIfZeroResId = null;
        RemoteInput[] array;
        if (remoteInputs == null) {
            array = null;
        }
        else {
            array = new RemoteInput[remoteInputs.length];
            for (int i = 0; i < remoteInputs.length; ++i) {
                final android.app.RemoteInput remoteInput = remoteInputs[i];
                final String resultKey = remoteInput.getResultKey();
                final CharSequence label = remoteInput.getLabel();
                final CharSequence[] choices = remoteInput.getChoices();
                final boolean allowFreeFormInput = remoteInput.getAllowFreeFormInput();
                int \u3007080;
                if (Build$VERSION.SDK_INT >= 29) {
                    \u3007080 = ooo0\u3007O88O.\u3007080(remoteInput);
                }
                else {
                    \u3007080 = 0;
                }
                array[i] = new RemoteInput(resultKey, label, choices, allowFreeFormInput, \u3007080, remoteInput.getExtras(), null);
            }
        }
        final int sdk_INT = Build$VERSION.SDK_INT;
        boolean boolean1;
        if (sdk_INT >= 24) {
            boolean1 = (notification$Action.getExtras().getBoolean("android.support.allowGeneratedReplies") || OOo8o\u3007O.\u3007080(notification$Action));
        }
        else {
            boolean1 = notification$Action.getExtras().getBoolean("android.support.allowGeneratedReplies");
        }
        final boolean boolean2 = notification$Action.getExtras().getBoolean("android.support.action.showsUserInterface", true);
        int n;
        if (sdk_INT >= 28) {
            n = O880oOO08.\u3007080(notification$Action);
        }
        else {
            n = ((BaseBundle)notification$Action.getExtras()).getInt("android.support.action.semanticAction", 0);
        }
        final boolean b = sdk_INT >= 29 && O0O8OO088.\u3007080(notification$Action);
        final boolean b2 = sdk_INT >= 31 && \u3007o0O0O8.\u3007080(notification$Action);
        if (sdk_INT >= 23) {
            if (\u3007\u3007o8.\u3007080(notification$Action) == null) {
                final int icon = notification$Action.icon;
                if (icon != 0) {
                    return new Action(icon, notification$Action.title, notification$Action.actionIntent, notification$Action.getExtras(), array, null, boolean1, n, boolean2, b, b2);
                }
            }
            if (\u3007\u3007o8.\u3007080(notification$Action) != null) {
                fromIconOrNullIfZeroResId = IconCompat.createFromIconOrNullIfZeroResId(\u3007\u3007o8.\u3007080(notification$Action));
            }
            return new Action(fromIconOrNullIfZeroResId, notification$Action.title, notification$Action.actionIntent, notification$Action.getExtras(), array, null, boolean1, n, boolean2, b, b2);
        }
        return new Action(notification$Action.icon, notification$Action.title, notification$Action.actionIntent, notification$Action.getExtras(), array, null, boolean1, n, boolean2, b, b2);
    }
    
    public static int getActionCount(@NonNull final Notification notification) {
        final Notification$Action[] actions = notification.actions;
        int length;
        if (actions != null) {
            length = actions.length;
        }
        else {
            length = 0;
        }
        return length;
    }
    
    public static boolean getAllowSystemGeneratedContextualActions(@NonNull final Notification notification) {
        return Build$VERSION.SDK_INT >= 29 && O8O\u3007.\u3007080(notification);
    }
    
    public static boolean getAutoCancel(@NonNull final Notification notification) {
        return (notification.flags & 0x10) != 0x0;
    }
    
    public static int getBadgeIconType(@NonNull final Notification notification) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Oo\u3007O.\u3007080(notification);
        }
        return 0;
    }
    
    @Nullable
    public static BubbleMetadata getBubbleMetadata(@NonNull final Notification notification) {
        if (Build$VERSION.SDK_INT >= 29) {
            return BubbleMetadata.fromPlatform(\u30070O\u3007Oo.\u3007080(notification));
        }
        return null;
    }
    
    @Nullable
    public static String getCategory(@NonNull final Notification notification) {
        return notification.category;
    }
    
    @Nullable
    public static String getChannelId(@NonNull final Notification notification) {
        if (Build$VERSION.SDK_INT >= 26) {
            return \u3007000O0.\u3007080(notification);
        }
        return null;
    }
    
    public static int getColor(@NonNull final Notification notification) {
        return notification.color;
    }
    
    @Nullable
    @RequiresApi(19)
    public static CharSequence getContentInfo(@NonNull final Notification notification) {
        return notification.extras.getCharSequence("android.infoText");
    }
    
    @Nullable
    @RequiresApi(19)
    public static CharSequence getContentText(@NonNull final Notification notification) {
        return notification.extras.getCharSequence("android.text");
    }
    
    @Nullable
    @RequiresApi(19)
    public static CharSequence getContentTitle(@NonNull final Notification notification) {
        return notification.extras.getCharSequence("android.title");
    }
    
    @Nullable
    public static Bundle getExtras(@NonNull final Notification notification) {
        return notification.extras;
    }
    
    @Nullable
    public static String getGroup(@NonNull final Notification notification) {
        return notification.getGroup();
    }
    
    public static int getGroupAlertBehavior(@NonNull final Notification notification) {
        if (Build$VERSION.SDK_INT >= 26) {
            return \u300700O0O0.\u3007080(notification);
        }
        return 0;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    static boolean getHighPriority(@NonNull final Notification notification) {
        return (notification.flags & 0x80) != 0x0;
    }
    
    @NonNull
    @RequiresApi(21)
    public static List<Action> getInvisibleActions(@NonNull final Notification notification) {
        final ArrayList list = new ArrayList();
        final Bundle bundle = notification.extras.getBundle("android.car.EXTENSIONS");
        if (bundle == null) {
            return list;
        }
        final Bundle bundle2 = bundle.getBundle("invisible_actions");
        if (bundle2 != null) {
            for (int i = 0; i < ((BaseBundle)bundle2).size(); ++i) {
                list.add(NotificationCompatJellybean.getActionFromBundle(bundle2.getBundle(Integer.toString(i))));
            }
        }
        return list;
    }
    
    public static boolean getLocalOnly(@NonNull final Notification notification) {
        return (notification.flags & 0x100) != 0x0;
    }
    
    @Nullable
    public static LocusIdCompat getLocusId(@NonNull final Notification notification) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        LocusIdCompat locusIdCompat = null;
        if (sdk_INT >= 29) {
            final LocusId \u3007080 = ooOO.\u3007080(notification);
            if (\u3007080 == null) {
                locusIdCompat = locusIdCompat;
            }
            else {
                locusIdCompat = LocusIdCompat.toLocusIdCompat(\u3007080);
            }
        }
        return locusIdCompat;
    }
    
    @NonNull
    static Notification[] getNotificationArrayFromBundle(@NonNull final Bundle bundle, @NonNull final String s) {
        final Parcelable[] parcelableArray = bundle.getParcelableArray(s);
        if (!(parcelableArray instanceof Notification[]) && parcelableArray != null) {
            final Notification[] array = new Notification[parcelableArray.length];
            for (int i = 0; i < parcelableArray.length; ++i) {
                array[i] = (Notification)parcelableArray[i];
            }
            bundle.putParcelableArray(s, (Parcelable[])array);
            return array;
        }
        return (Notification[])parcelableArray;
    }
    
    public static boolean getOngoing(@NonNull final Notification notification) {
        return (notification.flags & 0x2) != 0x0;
    }
    
    public static boolean getOnlyAlertOnce(@NonNull final Notification notification) {
        return (notification.flags & 0x8) != 0x0;
    }
    
    @NonNull
    public static List<Person> getPeople(@NonNull final Notification notification) {
        final ArrayList list = new ArrayList();
        if (Build$VERSION.SDK_INT >= 28) {
            final ArrayList parcelableArrayList = notification.extras.getParcelableArrayList("android.people.list");
            if (parcelableArrayList != null && !parcelableArrayList.isEmpty()) {
                final Iterator iterator = parcelableArrayList.iterator();
                while (iterator.hasNext()) {
                    list.add(Person.fromAndroidPerson((android.app.Person)iterator.next()));
                }
            }
        }
        else {
            final String[] stringArray = ((BaseBundle)notification.extras).getStringArray("android.people");
            if (stringArray != null && stringArray.length != 0) {
                for (int length = stringArray.length, i = 0; i < length; ++i) {
                    list.add(new Person.Builder().setUri(stringArray[i]).build());
                }
            }
        }
        return list;
    }
    
    @Nullable
    public static Notification getPublicVersion(@NonNull final Notification notification) {
        return notification.publicVersion;
    }
    
    @Nullable
    public static CharSequence getSettingsText(@NonNull final Notification notification) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Ooo8\u3007\u3007.\u3007080(notification);
        }
        return null;
    }
    
    @Nullable
    public static String getShortcutId(@NonNull final Notification notification) {
        if (Build$VERSION.SDK_INT >= 26) {
            return OOO8o\u3007\u3007.\u3007080(notification);
        }
        return null;
    }
    
    @RequiresApi(19)
    public static boolean getShowWhen(@NonNull final Notification notification) {
        return notification.extras.getBoolean("android.showWhen");
    }
    
    @Nullable
    public static String getSortKey(@NonNull final Notification notification) {
        return notification.getSortKey();
    }
    
    @Nullable
    @RequiresApi(19)
    public static CharSequence getSubText(@NonNull final Notification notification) {
        return notification.extras.getCharSequence("android.subText");
    }
    
    public static long getTimeoutAfter(@NonNull final Notification notification) {
        if (Build$VERSION.SDK_INT >= 26) {
            return O0.\u3007080(notification);
        }
        return 0L;
    }
    
    @RequiresApi(19)
    public static boolean getUsesChronometer(@NonNull final Notification notification) {
        return notification.extras.getBoolean("android.showChronometer");
    }
    
    public static int getVisibility(@NonNull final Notification notification) {
        return notification.visibility;
    }
    
    public static boolean isGroupSummary(@NonNull final Notification notification) {
        return (notification.flags & 0x200) != 0x0;
    }
    
    public static class Action
    {
        static final String EXTRA_SEMANTIC_ACTION = "android.support.action.semanticAction";
        static final String EXTRA_SHOWS_USER_INTERFACE = "android.support.action.showsUserInterface";
        public static final int SEMANTIC_ACTION_ARCHIVE = 5;
        public static final int SEMANTIC_ACTION_CALL = 10;
        public static final int SEMANTIC_ACTION_DELETE = 4;
        public static final int SEMANTIC_ACTION_MARK_AS_READ = 2;
        public static final int SEMANTIC_ACTION_MARK_AS_UNREAD = 3;
        public static final int SEMANTIC_ACTION_MUTE = 6;
        public static final int SEMANTIC_ACTION_NONE = 0;
        public static final int SEMANTIC_ACTION_REPLY = 1;
        public static final int SEMANTIC_ACTION_THUMBS_DOWN = 9;
        public static final int SEMANTIC_ACTION_THUMBS_UP = 8;
        public static final int SEMANTIC_ACTION_UNMUTE = 7;
        public PendingIntent actionIntent;
        @Deprecated
        public int icon;
        private boolean mAllowGeneratedReplies;
        private boolean mAuthenticationRequired;
        private final RemoteInput[] mDataOnlyRemoteInputs;
        final Bundle mExtras;
        @Nullable
        private IconCompat mIcon;
        private final boolean mIsContextual;
        private final RemoteInput[] mRemoteInputs;
        private final int mSemanticAction;
        boolean mShowsUserInterface;
        public CharSequence title;
        
        public Action(final int n, @Nullable final CharSequence charSequence, @Nullable final PendingIntent pendingIntent) {
            IconCompat withResource = null;
            if (n != 0) {
                withResource = IconCompat.createWithResource(null, "", n);
            }
            this(withResource, charSequence, pendingIntent);
        }
        
        Action(final int n, @Nullable final CharSequence charSequence, @Nullable final PendingIntent pendingIntent, @Nullable final Bundle bundle, @Nullable final RemoteInput[] array, @Nullable final RemoteInput[] array2, final boolean b, final int n2, final boolean b2, final boolean b3, final boolean b4) {
            IconCompat withResource = null;
            if (n != 0) {
                withResource = IconCompat.createWithResource(null, "", n);
            }
            this(withResource, charSequence, pendingIntent, bundle, array, array2, b, n2, b2, b3, b4);
        }
        
        public Action(@Nullable final IconCompat iconCompat, @Nullable final CharSequence charSequence, @Nullable final PendingIntent pendingIntent) {
            this(iconCompat, charSequence, pendingIntent, new Bundle(), null, null, true, 0, true, false, false);
        }
        
        Action(@Nullable final IconCompat mIcon, @Nullable final CharSequence charSequence, @Nullable final PendingIntent actionIntent, @Nullable Bundle mExtras, @Nullable final RemoteInput[] mRemoteInputs, @Nullable final RemoteInput[] mDataOnlyRemoteInputs, final boolean mAllowGeneratedReplies, final int mSemanticAction, final boolean mShowsUserInterface, final boolean mIsContextual, final boolean mAuthenticationRequired) {
            this.mShowsUserInterface = true;
            this.mIcon = mIcon;
            if (mIcon != null && mIcon.getType() == 2) {
                this.icon = mIcon.getResId();
            }
            this.title = NotificationCompat.Builder.limitCharSequenceLength(charSequence);
            this.actionIntent = actionIntent;
            if (mExtras == null) {
                mExtras = new Bundle();
            }
            this.mExtras = mExtras;
            this.mRemoteInputs = mRemoteInputs;
            this.mDataOnlyRemoteInputs = mDataOnlyRemoteInputs;
            this.mAllowGeneratedReplies = mAllowGeneratedReplies;
            this.mSemanticAction = mSemanticAction;
            this.mShowsUserInterface = mShowsUserInterface;
            this.mIsContextual = mIsContextual;
            this.mAuthenticationRequired = mAuthenticationRequired;
        }
        
        @Nullable
        public PendingIntent getActionIntent() {
            return this.actionIntent;
        }
        
        public boolean getAllowGeneratedReplies() {
            return this.mAllowGeneratedReplies;
        }
        
        @Nullable
        public RemoteInput[] getDataOnlyRemoteInputs() {
            return this.mDataOnlyRemoteInputs;
        }
        
        @NonNull
        public Bundle getExtras() {
            return this.mExtras;
        }
        
        @Deprecated
        public int getIcon() {
            return this.icon;
        }
        
        @Nullable
        public IconCompat getIconCompat() {
            if (this.mIcon == null) {
                final int icon = this.icon;
                if (icon != 0) {
                    this.mIcon = IconCompat.createWithResource(null, "", icon);
                }
            }
            return this.mIcon;
        }
        
        @Nullable
        public RemoteInput[] getRemoteInputs() {
            return this.mRemoteInputs;
        }
        
        public int getSemanticAction() {
            return this.mSemanticAction;
        }
        
        public boolean getShowsUserInterface() {
            return this.mShowsUserInterface;
        }
        
        @Nullable
        public CharSequence getTitle() {
            return this.title;
        }
        
        public boolean isAuthenticationRequired() {
            return this.mAuthenticationRequired;
        }
        
        public boolean isContextual() {
            return this.mIsContextual;
        }
        
        public static final class Builder
        {
            private boolean mAllowGeneratedReplies;
            private boolean mAuthenticationRequired;
            private final Bundle mExtras;
            private final IconCompat mIcon;
            private final PendingIntent mIntent;
            private boolean mIsContextual;
            private ArrayList<RemoteInput> mRemoteInputs;
            private int mSemanticAction;
            private boolean mShowsUserInterface;
            private final CharSequence mTitle;
            
            public Builder(final int n, @Nullable final CharSequence charSequence, @Nullable final PendingIntent pendingIntent) {
                IconCompat withResource = null;
                if (n != 0) {
                    withResource = IconCompat.createWithResource(null, "", n);
                }
                this(withResource, charSequence, pendingIntent, new Bundle(), null, true, 0, true, false, false);
            }
            
            public Builder(@NonNull final Action action) {
                this(action.getIconCompat(), action.title, action.actionIntent, new Bundle(action.mExtras), action.getRemoteInputs(), action.getAllowGeneratedReplies(), action.getSemanticAction(), action.mShowsUserInterface, action.isContextual(), action.isAuthenticationRequired());
            }
            
            public Builder(@Nullable final IconCompat iconCompat, @Nullable final CharSequence charSequence, @Nullable final PendingIntent pendingIntent) {
                this(iconCompat, charSequence, pendingIntent, new Bundle(), null, true, 0, true, false, false);
            }
            
            private Builder(@Nullable final IconCompat mIcon, @Nullable final CharSequence charSequence, @Nullable final PendingIntent mIntent, @NonNull final Bundle mExtras, @Nullable final RemoteInput[] a, final boolean mAllowGeneratedReplies, final int mSemanticAction, final boolean mShowsUserInterface, final boolean mIsContextual, final boolean mAuthenticationRequired) {
                this.mAllowGeneratedReplies = true;
                this.mShowsUserInterface = true;
                this.mIcon = mIcon;
                this.mTitle = NotificationCompat.Builder.limitCharSequenceLength(charSequence);
                this.mIntent = mIntent;
                this.mExtras = mExtras;
                ArrayList<RemoteInput> mRemoteInputs;
                if (a == null) {
                    mRemoteInputs = null;
                }
                else {
                    mRemoteInputs = new ArrayList<RemoteInput>(Arrays.asList(a));
                }
                this.mRemoteInputs = mRemoteInputs;
                this.mAllowGeneratedReplies = mAllowGeneratedReplies;
                this.mSemanticAction = mSemanticAction;
                this.mShowsUserInterface = mShowsUserInterface;
                this.mIsContextual = mIsContextual;
                this.mAuthenticationRequired = mAuthenticationRequired;
            }
            
            private void checkContextualActionNullFields() {
                if (!this.mIsContextual) {
                    return;
                }
                if (this.mIntent != null) {
                    return;
                }
                throw new NullPointerException("Contextual Actions must contain a valid PendingIntent");
            }
            
            @NonNull
            @RequiresApi(19)
            @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
            public static Builder fromAndroidAction(@NonNull final Notification$Action notification$Action) {
                Builder builder;
                if (Build$VERSION.SDK_INT >= 23 && \u3007\u3007o8.\u3007080(notification$Action) != null) {
                    builder = new Builder(IconCompat.createFromIcon(\u3007\u3007o8.\u3007080(notification$Action)), notification$Action.title, notification$Action.actionIntent);
                }
                else {
                    builder = new Builder(notification$Action.icon, notification$Action.title, notification$Action.actionIntent);
                }
                final android.app.RemoteInput[] remoteInputs = notification$Action.getRemoteInputs();
                if (remoteInputs != null && remoteInputs.length != 0) {
                    for (int length = remoteInputs.length, i = 0; i < length; ++i) {
                        builder.addRemoteInput(RemoteInput.fromPlatform(remoteInputs[i]));
                    }
                }
                final int sdk_INT = Build$VERSION.SDK_INT;
                if (sdk_INT >= 24) {
                    builder.mAllowGeneratedReplies = OOo8o\u3007O.\u3007080(notification$Action);
                }
                if (sdk_INT >= 28) {
                    builder.setSemanticAction(O880oOO08.\u3007080(notification$Action));
                }
                if (sdk_INT >= 29) {
                    builder.setContextual(O0O8OO088.\u3007080(notification$Action));
                }
                if (sdk_INT >= 31) {
                    builder.setAuthenticationRequired(\u3007o0O0O8.\u3007080(notification$Action));
                }
                return builder;
            }
            
            @NonNull
            public Builder addExtras(@Nullable final Bundle bundle) {
                if (bundle != null) {
                    this.mExtras.putAll(bundle);
                }
                return this;
            }
            
            @NonNull
            public Builder addRemoteInput(@Nullable final RemoteInput e) {
                if (this.mRemoteInputs == null) {
                    this.mRemoteInputs = new ArrayList<RemoteInput>();
                }
                if (e != null) {
                    this.mRemoteInputs.add(e);
                }
                return this;
            }
            
            @NonNull
            public Action build() {
                this.checkContextualActionNullFields();
                final ArrayList list = new ArrayList();
                final ArrayList list2 = new ArrayList();
                final ArrayList<RemoteInput> mRemoteInputs = this.mRemoteInputs;
                if (mRemoteInputs != null) {
                    for (final RemoteInput remoteInput : mRemoteInputs) {
                        if (remoteInput.isDataOnly()) {
                            list.add(remoteInput);
                        }
                        else {
                            list2.add(remoteInput);
                        }
                    }
                }
                final boolean empty = list.isEmpty();
                RemoteInput[] array = null;
                RemoteInput[] array2;
                if (empty) {
                    array2 = null;
                }
                else {
                    array2 = (RemoteInput[])list.toArray(new RemoteInput[list.size()]);
                }
                if (!list2.isEmpty()) {
                    array = (RemoteInput[])list2.toArray(new RemoteInput[list2.size()]);
                }
                return new Action(this.mIcon, this.mTitle, this.mIntent, this.mExtras, array, array2, this.mAllowGeneratedReplies, this.mSemanticAction, this.mShowsUserInterface, this.mIsContextual, this.mAuthenticationRequired);
            }
            
            @NonNull
            public Builder extend(@NonNull final Extender extender) {
                extender.extend(this);
                return this;
            }
            
            @NonNull
            public Bundle getExtras() {
                return this.mExtras;
            }
            
            @NonNull
            public Builder setAllowGeneratedReplies(final boolean mAllowGeneratedReplies) {
                this.mAllowGeneratedReplies = mAllowGeneratedReplies;
                return this;
            }
            
            @NonNull
            public Builder setAuthenticationRequired(final boolean mAuthenticationRequired) {
                this.mAuthenticationRequired = mAuthenticationRequired;
                return this;
            }
            
            @NonNull
            public Builder setContextual(final boolean mIsContextual) {
                this.mIsContextual = mIsContextual;
                return this;
            }
            
            @NonNull
            public Builder setSemanticAction(final int mSemanticAction) {
                this.mSemanticAction = mSemanticAction;
                return this;
            }
            
            @NonNull
            public Builder setShowsUserInterface(final boolean mShowsUserInterface) {
                this.mShowsUserInterface = mShowsUserInterface;
                return this;
            }
        }
        
        public interface Extender
        {
            @NonNull
            Builder extend(@NonNull final Builder p0);
        }
        
        @Retention(RetentionPolicy.SOURCE)
        public @interface SemanticAction {
        }
        
        public static final class WearableExtender implements Extender
        {
            private static final int DEFAULT_FLAGS = 1;
            private static final String EXTRA_WEARABLE_EXTENSIONS = "android.wearable.EXTENSIONS";
            private static final int FLAG_AVAILABLE_OFFLINE = 1;
            private static final int FLAG_HINT_DISPLAY_INLINE = 4;
            private static final int FLAG_HINT_LAUNCHES_ACTIVITY = 2;
            private static final String KEY_CANCEL_LABEL = "cancelLabel";
            private static final String KEY_CONFIRM_LABEL = "confirmLabel";
            private static final String KEY_FLAGS = "flags";
            private static final String KEY_IN_PROGRESS_LABEL = "inProgressLabel";
            private CharSequence mCancelLabel;
            private CharSequence mConfirmLabel;
            private int mFlags;
            private CharSequence mInProgressLabel;
            
            public WearableExtender() {
                this.mFlags = 1;
            }
            
            public WearableExtender(@NonNull final Action action) {
                this.mFlags = 1;
                final Bundle bundle = action.getExtras().getBundle("android.wearable.EXTENSIONS");
                if (bundle != null) {
                    this.mFlags = ((BaseBundle)bundle).getInt("flags", 1);
                    this.mInProgressLabel = bundle.getCharSequence("inProgressLabel");
                    this.mConfirmLabel = bundle.getCharSequence("confirmLabel");
                    this.mCancelLabel = bundle.getCharSequence("cancelLabel");
                }
            }
            
            private void setFlag(final int n, final boolean b) {
                if (b) {
                    this.mFlags |= n;
                }
                else {
                    this.mFlags &= ~n;
                }
            }
            
            @NonNull
            public WearableExtender clone() {
                final WearableExtender wearableExtender = new WearableExtender();
                wearableExtender.mFlags = this.mFlags;
                wearableExtender.mInProgressLabel = this.mInProgressLabel;
                wearableExtender.mConfirmLabel = this.mConfirmLabel;
                wearableExtender.mCancelLabel = this.mCancelLabel;
                return wearableExtender;
            }
            
            @NonNull
            @Override
            public Builder extend(@NonNull final Builder builder) {
                final Bundle bundle = new Bundle();
                final int mFlags = this.mFlags;
                if (mFlags != 1) {
                    ((BaseBundle)bundle).putInt("flags", mFlags);
                }
                final CharSequence mInProgressLabel = this.mInProgressLabel;
                if (mInProgressLabel != null) {
                    bundle.putCharSequence("inProgressLabel", mInProgressLabel);
                }
                final CharSequence mConfirmLabel = this.mConfirmLabel;
                if (mConfirmLabel != null) {
                    bundle.putCharSequence("confirmLabel", mConfirmLabel);
                }
                final CharSequence mCancelLabel = this.mCancelLabel;
                if (mCancelLabel != null) {
                    bundle.putCharSequence("cancelLabel", mCancelLabel);
                }
                builder.getExtras().putBundle("android.wearable.EXTENSIONS", bundle);
                return builder;
            }
            
            @Deprecated
            @Nullable
            public CharSequence getCancelLabel() {
                return this.mCancelLabel;
            }
            
            @Deprecated
            @Nullable
            public CharSequence getConfirmLabel() {
                return this.mConfirmLabel;
            }
            
            public boolean getHintDisplayActionInline() {
                return (this.mFlags & 0x4) != 0x0;
            }
            
            public boolean getHintLaunchesActivity() {
                return (this.mFlags & 0x2) != 0x0;
            }
            
            @Deprecated
            @Nullable
            public CharSequence getInProgressLabel() {
                return this.mInProgressLabel;
            }
            
            public boolean isAvailableOffline() {
                final int mFlags = this.mFlags;
                boolean b = true;
                if ((mFlags & 0x1) == 0x0) {
                    b = false;
                }
                return b;
            }
            
            @NonNull
            public WearableExtender setAvailableOffline(final boolean b) {
                this.setFlag(1, b);
                return this;
            }
            
            @Deprecated
            @NonNull
            public WearableExtender setCancelLabel(@Nullable final CharSequence mCancelLabel) {
                this.mCancelLabel = mCancelLabel;
                return this;
            }
            
            @Deprecated
            @NonNull
            public WearableExtender setConfirmLabel(@Nullable final CharSequence mConfirmLabel) {
                this.mConfirmLabel = mConfirmLabel;
                return this;
            }
            
            @NonNull
            public WearableExtender setHintDisplayActionInline(final boolean b) {
                this.setFlag(4, b);
                return this;
            }
            
            @NonNull
            public WearableExtender setHintLaunchesActivity(final boolean b) {
                this.setFlag(2, b);
                return this;
            }
            
            @Deprecated
            @NonNull
            public WearableExtender setInProgressLabel(@Nullable final CharSequence mInProgressLabel) {
                this.mInProgressLabel = mInProgressLabel;
                return this;
            }
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public @interface BadgeIconType {
    }
    
    public static class BigPictureStyle extends Style
    {
        private static final String TEMPLATE_CLASS_NAME = "androidx.core.app.NotificationCompat$BigPictureStyle";
        private IconCompat mBigLargeIcon;
        private boolean mBigLargeIconSet;
        private CharSequence mPictureContentDescription;
        private IconCompat mPictureIcon;
        private boolean mShowBigPictureWhenCollapsed;
        
        public BigPictureStyle() {
        }
        
        public BigPictureStyle(@Nullable final NotificationCompat.Builder builder) {
            ((Style)this).setBuilder(builder);
        }
        
        @Nullable
        private static IconCompat asIconCompat(@Nullable final Parcelable parcelable) {
            if (parcelable != null) {
                if (Build$VERSION.SDK_INT >= 23 && parcelable instanceof Icon) {
                    return IconCompat.createFromIcon((Icon)parcelable);
                }
                if (parcelable instanceof Bitmap) {
                    return IconCompat.createWithBitmap((Bitmap)parcelable);
                }
            }
            return null;
        }
        
        @Nullable
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public static IconCompat getPictureIcon(@Nullable final Bundle bundle) {
            if (bundle == null) {
                return null;
            }
            final Parcelable parcelable = bundle.getParcelable("android.picture");
            if (parcelable != null) {
                return asIconCompat(parcelable);
            }
            return asIconCompat(bundle.getParcelable("android.pictureIcon"));
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        @Override
        public void apply(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            final int sdk_INT = Build$VERSION.SDK_INT;
            final Notification$BigPictureStyle setBigContentTitle = new Notification$BigPictureStyle(notificationBuilderWithBuilderAccessor.getBuilder()).setBigContentTitle(super.mBigContentTitle);
            final IconCompat mPictureIcon = this.mPictureIcon;
            Context context = null;
            Notification$BigPictureStyle bigPicture = setBigContentTitle;
            if (mPictureIcon != null) {
                if (sdk_INT >= 31) {
                    Context context2;
                    if (notificationBuilderWithBuilderAccessor instanceof NotificationCompatBuilder) {
                        context2 = ((NotificationCompatBuilder)notificationBuilderWithBuilderAccessor).getContext();
                    }
                    else {
                        context2 = null;
                    }
                    Api31Impl.setBigPicture(setBigContentTitle, this.mPictureIcon.toIcon(context2));
                    bigPicture = setBigContentTitle;
                }
                else {
                    bigPicture = setBigContentTitle;
                    if (mPictureIcon.getType() == 1) {
                        bigPicture = setBigContentTitle.bigPicture(this.mPictureIcon.getBitmap());
                    }
                }
            }
            if (this.mBigLargeIconSet) {
                final IconCompat mBigLargeIcon = this.mBigLargeIcon;
                if (mBigLargeIcon == null) {
                    Api16Impl.setBigLargeIcon(bigPicture, null);
                }
                else if (sdk_INT >= 23) {
                    if (notificationBuilderWithBuilderAccessor instanceof NotificationCompatBuilder) {
                        context = ((NotificationCompatBuilder)notificationBuilderWithBuilderAccessor).getContext();
                    }
                    Api23Impl.setBigLargeIcon(bigPicture, this.mBigLargeIcon.toIcon(context));
                }
                else if (mBigLargeIcon.getType() == 1) {
                    Api16Impl.setBigLargeIcon(bigPicture, this.mBigLargeIcon.getBitmap());
                }
                else {
                    Api16Impl.setBigLargeIcon(bigPicture, null);
                }
            }
            if (super.mSummaryTextSet) {
                Api16Impl.setSummaryText(bigPicture, super.mSummaryText);
            }
            if (sdk_INT >= 31) {
                Api31Impl.showBigPictureWhenCollapsed(bigPicture, this.mShowBigPictureWhenCollapsed);
                Api31Impl.setContentDescription(bigPicture, this.mPictureContentDescription);
            }
        }
        
        @NonNull
        public BigPictureStyle bigLargeIcon(@Nullable final Bitmap bitmap) {
            IconCompat withBitmap;
            if (bitmap == null) {
                withBitmap = null;
            }
            else {
                withBitmap = IconCompat.createWithBitmap(bitmap);
            }
            this.mBigLargeIcon = withBitmap;
            this.mBigLargeIconSet = true;
            return this;
        }
        
        @NonNull
        public BigPictureStyle bigPicture(@Nullable final Bitmap bitmap) {
            IconCompat withBitmap;
            if (bitmap == null) {
                withBitmap = null;
            }
            else {
                withBitmap = IconCompat.createWithBitmap(bitmap);
            }
            this.mPictureIcon = withBitmap;
            return this;
        }
        
        @NonNull
        @RequiresApi(31)
        public BigPictureStyle bigPicture(@Nullable final Icon icon) {
            this.mPictureIcon = IconCompat.createFromIcon(icon);
            return this;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        @Override
        protected void clearCompatExtraKeys(@NonNull final Bundle bundle) {
            super.clearCompatExtraKeys(bundle);
            bundle.remove("android.largeIcon.big");
            bundle.remove("android.picture");
            bundle.remove("android.pictureIcon");
            bundle.remove("android.showBigPictureWhenCollapsed");
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        @Override
        protected String getClassName() {
            return "androidx.core.app.NotificationCompat$BigPictureStyle";
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        @Override
        protected void restoreFromCompatExtras(@NonNull final Bundle bundle) {
            super.restoreFromCompatExtras(bundle);
            if (((BaseBundle)bundle).containsKey("android.largeIcon.big")) {
                this.mBigLargeIcon = asIconCompat(bundle.getParcelable("android.largeIcon.big"));
                this.mBigLargeIconSet = true;
            }
            this.mPictureIcon = getPictureIcon(bundle);
            this.mShowBigPictureWhenCollapsed = bundle.getBoolean("android.showBigPictureWhenCollapsed");
        }
        
        @NonNull
        public BigPictureStyle setBigContentTitle(@Nullable final CharSequence charSequence) {
            super.mBigContentTitle = NotificationCompat.Builder.limitCharSequenceLength(charSequence);
            return this;
        }
        
        @NonNull
        @RequiresApi(31)
        public BigPictureStyle setContentDescription(@Nullable final CharSequence mPictureContentDescription) {
            this.mPictureContentDescription = mPictureContentDescription;
            return this;
        }
        
        @NonNull
        public BigPictureStyle setSummaryText(@Nullable final CharSequence charSequence) {
            super.mSummaryText = NotificationCompat.Builder.limitCharSequenceLength(charSequence);
            super.mSummaryTextSet = true;
            return this;
        }
        
        @NonNull
        @RequiresApi(31)
        public BigPictureStyle showBigPictureWhenCollapsed(final boolean mShowBigPictureWhenCollapsed) {
            this.mShowBigPictureWhenCollapsed = mShowBigPictureWhenCollapsed;
            return this;
        }
        
        @RequiresApi(16)
        private static class Api16Impl
        {
            @RequiresApi(16)
            static void setBigLargeIcon(final Notification$BigPictureStyle notification$BigPictureStyle, final Bitmap bitmap) {
                notification$BigPictureStyle.bigLargeIcon(bitmap);
            }
            
            @RequiresApi(16)
            static void setSummaryText(final Notification$BigPictureStyle notification$BigPictureStyle, final CharSequence summaryText) {
                notification$BigPictureStyle.setSummaryText(summaryText);
            }
        }
        
        @RequiresApi(23)
        private static class Api23Impl
        {
            @RequiresApi(23)
            static void setBigLargeIcon(final Notification$BigPictureStyle notification$BigPictureStyle, final Icon icon) {
                OOo0O.\u3007080(notification$BigPictureStyle, icon);
            }
        }
        
        @RequiresApi(31)
        private static class Api31Impl
        {
            @RequiresApi(31)
            static void setBigPicture(final Notification$BigPictureStyle notification$BigPictureStyle, final Icon icon) {
                O\u3007OO.\u3007080(notification$BigPictureStyle, icon);
            }
            
            @RequiresApi(31)
            static void setContentDescription(final Notification$BigPictureStyle notification$BigPictureStyle, final CharSequence charSequence) {
                O\u300708.\u3007080(notification$BigPictureStyle, charSequence);
            }
            
            @RequiresApi(31)
            static void showBigPictureWhenCollapsed(final Notification$BigPictureStyle notification$BigPictureStyle, final boolean b) {
                O\u3007Oooo\u3007\u3007.\u3007080(notification$BigPictureStyle, b);
            }
        }
    }
    
    public abstract static class Style
    {
        CharSequence mBigContentTitle;
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        protected NotificationCompat.Builder mBuilder;
        CharSequence mSummaryText;
        boolean mSummaryTextSet;
        
        public Style() {
            this.mSummaryTextSet = false;
        }
        
        private int calculateTopPadding() {
            final Resources resources = this.mBuilder.mContext.getResources();
            final int dimensionPixelSize = resources.getDimensionPixelSize(R.dimen.notification_top_pad);
            final int dimensionPixelSize2 = resources.getDimensionPixelSize(R.dimen.notification_top_pad_large_text);
            final float n = (constrain(resources.getConfiguration().fontScale, 1.0f, 1.3f) - 1.0f) / 0.29999995f;
            return Math.round((1.0f - n) * dimensionPixelSize + n * dimensionPixelSize2);
        }
        
        private static float constrain(final float n, float n2, final float n3) {
            if (n >= n2) {
                n2 = n;
                if (n > n3) {
                    n2 = n3;
                }
            }
            return n2;
        }
        
        @Nullable
        static Style constructCompatStyleByName(@Nullable final String s) {
            if (s != null) {
                final int hashCode = s.hashCode();
                int n = -1;
                switch (hashCode) {
                    case 2090799565: {
                        if (!s.equals("androidx.core.app.NotificationCompat$MessagingStyle")) {
                            break;
                        }
                        n = 4;
                        break;
                    }
                    case 919595044: {
                        if (!s.equals("androidx.core.app.NotificationCompat$BigTextStyle")) {
                            break;
                        }
                        n = 3;
                        break;
                    }
                    case 912942987: {
                        if (!s.equals("androidx.core.app.NotificationCompat$InboxStyle")) {
                            break;
                        }
                        n = 2;
                        break;
                    }
                    case -171946061: {
                        if (!s.equals("androidx.core.app.NotificationCompat$BigPictureStyle")) {
                            break;
                        }
                        n = 1;
                        break;
                    }
                    case -716705180: {
                        if (!s.equals("androidx.core.app.NotificationCompat$DecoratedCustomViewStyle")) {
                            break;
                        }
                        n = 0;
                        break;
                    }
                }
                switch (n) {
                    case 4: {
                        return (Style)new MessagingStyle();
                    }
                    case 3: {
                        return (Style)new BigTextStyle();
                    }
                    case 2: {
                        return (Style)new InboxStyle();
                    }
                    case 1: {
                        return (Style)new BigPictureStyle();
                    }
                    case 0: {
                        return (Style)new DecoratedCustomViewStyle();
                    }
                }
            }
            return null;
        }
        
        @Nullable
        private static Style constructCompatStyleByPlatformName(@Nullable final String s) {
            if (s == null) {
                return null;
            }
            final int sdk_INT = Build$VERSION.SDK_INT;
            if (s.equals(Notification$BigPictureStyle.class.getName())) {
                return (Style)new BigPictureStyle();
            }
            if (s.equals(Notification$BigTextStyle.class.getName())) {
                return (Style)new BigTextStyle();
            }
            if (s.equals(Notification$InboxStyle.class.getName())) {
                return (Style)new InboxStyle();
            }
            if (sdk_INT >= 24) {
                if (s.equals(Notification$MessagingStyle.class.getName())) {
                    return (Style)new MessagingStyle();
                }
                if (s.equals(Notification$DecoratedCustomViewStyle.class.getName())) {
                    return (Style)new DecoratedCustomViewStyle();
                }
            }
            return null;
        }
        
        @Nullable
        static Style constructCompatStyleForBundle(@NonNull final Bundle bundle) {
            final Style constructCompatStyleByName = constructCompatStyleByName(((BaseBundle)bundle).getString("androidx.core.app.extra.COMPAT_TEMPLATE"));
            if (constructCompatStyleByName != null) {
                return constructCompatStyleByName;
            }
            if (((BaseBundle)bundle).containsKey("android.selfDisplayName") || ((BaseBundle)bundle).containsKey("android.messagingStyleUser")) {
                return (Style)new MessagingStyle();
            }
            if (((BaseBundle)bundle).containsKey("android.picture") || ((BaseBundle)bundle).containsKey("android.pictureIcon")) {
                return (Style)new BigPictureStyle();
            }
            if (((BaseBundle)bundle).containsKey("android.bigText")) {
                return (Style)new BigTextStyle();
            }
            if (((BaseBundle)bundle).containsKey("android.textLines")) {
                return (Style)new InboxStyle();
            }
            return constructCompatStyleByPlatformName(((BaseBundle)bundle).getString("android.template"));
        }
        
        @Nullable
        static Style constructStyleForExtras(@NonNull final Bundle bundle) {
            final Style constructCompatStyleForBundle = constructCompatStyleForBundle(bundle);
            if (constructCompatStyleForBundle == null) {
                return null;
            }
            try {
                constructCompatStyleForBundle.restoreFromCompatExtras(bundle);
                return constructCompatStyleForBundle;
            }
            catch (final ClassCastException ex) {
                return null;
            }
        }
        
        private Bitmap createColoredBitmap(final int n, final int n2, final int n3) {
            return this.createColoredBitmap(IconCompat.createWithResource(this.mBuilder.mContext, n), n2, n3);
        }
        
        private Bitmap createColoredBitmap(@NonNull final IconCompat iconCompat, final int n, final int n2) {
            final Drawable loadDrawable = iconCompat.loadDrawable(this.mBuilder.mContext);
            int intrinsicWidth;
            if (n2 == 0) {
                intrinsicWidth = loadDrawable.getIntrinsicWidth();
            }
            else {
                intrinsicWidth = n2;
            }
            int intrinsicHeight = n2;
            if (n2 == 0) {
                intrinsicHeight = loadDrawable.getIntrinsicHeight();
            }
            final Bitmap bitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, Bitmap$Config.ARGB_8888);
            loadDrawable.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
            if (n != 0) {
                loadDrawable.mutate().setColorFilter((ColorFilter)new PorterDuffColorFilter(n, PorterDuff$Mode.SRC_IN));
            }
            loadDrawable.draw(new Canvas(bitmap));
            return bitmap;
        }
        
        private Bitmap createIconWithBackground(int n, int n2, final int n3, final int n4) {
            final int notification_icon_background = R.drawable.notification_icon_background;
            int n5 = n4;
            if (n4 == 0) {
                n5 = 0;
            }
            final Bitmap coloredBitmap = this.createColoredBitmap(notification_icon_background, n5, n2);
            final Canvas canvas = new Canvas(coloredBitmap);
            final Drawable mutate = this.mBuilder.mContext.getResources().getDrawable(n).mutate();
            mutate.setFilterBitmap(true);
            n = (n2 - n3) / 2;
            n2 = n3 + n;
            mutate.setBounds(n, n, n2, n2);
            mutate.setColorFilter((ColorFilter)new PorterDuffColorFilter(-1, PorterDuff$Mode.SRC_ATOP));
            mutate.draw(canvas);
            return coloredBitmap;
        }
        
        @Nullable
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public static Style extractStyleFromNotification(@NonNull final Notification notification) {
            final Bundle extras = NotificationCompat.getExtras(notification);
            if (extras == null) {
                return null;
            }
            return constructStyleForExtras(extras);
        }
        
        private void hideNormalContent(final RemoteViews remoteViews) {
            remoteViews.setViewVisibility(R.id.title, 8);
            remoteViews.setViewVisibility(R.id.text2, 8);
            remoteViews.setViewVisibility(R.id.text, 8);
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public void addCompatExtras(@NonNull final Bundle bundle) {
            if (this.mSummaryTextSet) {
                bundle.putCharSequence("android.summaryText", this.mSummaryText);
            }
            final CharSequence mBigContentTitle = this.mBigContentTitle;
            if (mBigContentTitle != null) {
                bundle.putCharSequence("android.title.big", mBigContentTitle);
            }
            final String className = this.getClassName();
            if (className != null) {
                ((BaseBundle)bundle).putString("androidx.core.app.extra.COMPAT_TEMPLATE", className);
            }
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public void apply(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public RemoteViews applyStandardTemplate(final boolean b, int n, final boolean b2) {
            final Resources resources = this.mBuilder.mContext.getResources();
            final RemoteViews remoteViews = new RemoteViews(this.mBuilder.mContext.getPackageName(), n);
            this.mBuilder.getPriority();
            final int sdk_INT = Build$VERSION.SDK_INT;
            final NotificationCompat.Builder mBuilder = this.mBuilder;
            final Bitmap mLargeIcon = mBuilder.mLargeIcon;
            final int n2 = 0;
            if (mLargeIcon != null) {
                n = R.id.icon;
                remoteViews.setViewVisibility(n, 0);
                remoteViews.setImageViewBitmap(n, this.mBuilder.mLargeIcon);
                if (b && this.mBuilder.mNotification.icon != 0) {
                    final int dimensionPixelSize = resources.getDimensionPixelSize(R.dimen.notification_right_icon_size);
                    n = resources.getDimensionPixelSize(R.dimen.notification_small_icon_background_padding);
                    final NotificationCompat.Builder mBuilder2 = this.mBuilder;
                    final Bitmap iconWithBackground = this.createIconWithBackground(mBuilder2.mNotification.icon, dimensionPixelSize, dimensionPixelSize - n * 2, mBuilder2.getColor());
                    n = R.id.right_icon;
                    remoteViews.setImageViewBitmap(n, iconWithBackground);
                    remoteViews.setViewVisibility(n, 0);
                }
            }
            else if (b && mBuilder.mNotification.icon != 0) {
                final int icon = R.id.icon;
                remoteViews.setViewVisibility(icon, 0);
                final int dimensionPixelSize2 = resources.getDimensionPixelSize(R.dimen.notification_large_icon_width);
                n = resources.getDimensionPixelSize(R.dimen.notification_big_circle_margin);
                final int dimensionPixelSize3 = resources.getDimensionPixelSize(R.dimen.notification_small_icon_size_as_large);
                final NotificationCompat.Builder mBuilder3 = this.mBuilder;
                remoteViews.setImageViewBitmap(icon, this.createIconWithBackground(mBuilder3.mNotification.icon, dimensionPixelSize2 - n, dimensionPixelSize3, mBuilder3.getColor()));
            }
            final CharSequence mContentTitle = this.mBuilder.mContentTitle;
            if (mContentTitle != null) {
                remoteViews.setTextViewText(R.id.title, mContentTitle);
            }
            final CharSequence mContentText = this.mBuilder.mContentText;
            final int n3 = 1;
            if (mContentText != null) {
                remoteViews.setTextViewText(R.id.text, mContentText);
                n = 1;
            }
            else {
                n = 0;
            }
            final NotificationCompat.Builder mBuilder4 = this.mBuilder;
            final CharSequence mContentInfo = mBuilder4.mContentInfo;
            int n5 = 0;
            Label_0481: {
                if (mContentInfo != null) {
                    n = R.id.info;
                    remoteViews.setTextViewText(n, mContentInfo);
                    remoteViews.setViewVisibility(n, 0);
                }
                else {
                    if (mBuilder4.mNumber <= 0) {
                        remoteViews.setViewVisibility(R.id.info, 8);
                        final int n4 = 0;
                        n5 = n;
                        n = n4;
                        break Label_0481;
                    }
                    n = resources.getInteger(R.integer.status_bar_notification_info_maxnum);
                    if (this.mBuilder.mNumber > n) {
                        remoteViews.setTextViewText(R.id.info, (CharSequence)resources.getString(R.string.status_bar_notification_info_overflow));
                    }
                    else {
                        remoteViews.setTextViewText(R.id.info, (CharSequence)NumberFormat.getIntegerInstance().format(this.mBuilder.mNumber));
                    }
                    remoteViews.setViewVisibility(R.id.info, 0);
                }
                n5 = 1;
                n = 1;
            }
            final CharSequence mSubText = this.mBuilder.mSubText;
            boolean b3 = false;
            Label_0560: {
                if (mSubText != null) {
                    remoteViews.setTextViewText(R.id.text, mSubText);
                    final CharSequence mContentText2 = this.mBuilder.mContentText;
                    if (mContentText2 != null) {
                        final int text2 = R.id.text2;
                        remoteViews.setTextViewText(text2, mContentText2);
                        remoteViews.setViewVisibility(text2, 0);
                        b3 = true;
                        break Label_0560;
                    }
                    remoteViews.setViewVisibility(R.id.text2, 8);
                }
                b3 = false;
            }
            if (b3) {
                if (b2) {
                    remoteViews.setTextViewTextSize(R.id.text, 0, (float)resources.getDimensionPixelSize(R.dimen.notification_subtext_size));
                }
                remoteViews.setViewPadding(R.id.line1, 0, 0, 0, 0);
            }
            if (this.mBuilder.getWhenIfShowing() != 0L) {
                if (this.mBuilder.mUseChronometer) {
                    final int chronometer = R.id.chronometer;
                    remoteViews.setViewVisibility(chronometer, 0);
                    remoteViews.setLong(chronometer, "setBase", this.mBuilder.getWhenIfShowing() + (SystemClock.elapsedRealtime() - System.currentTimeMillis()));
                    remoteViews.setBoolean(chronometer, "setStarted", true);
                    final boolean mChronometerCountDown = this.mBuilder.mChronometerCountDown;
                    n = n3;
                    if (mChronometerCountDown) {
                        n = n3;
                        if (sdk_INT >= 24) {
                            Oo08OO8oO.\u3007080(remoteViews, chronometer, mChronometerCountDown);
                            n = n3;
                        }
                    }
                }
                else {
                    n = R.id.time;
                    remoteViews.setViewVisibility(n, 0);
                    remoteViews.setLong(n, "setTime", this.mBuilder.getWhenIfShowing());
                    n = n3;
                }
            }
            final int right_side = R.id.right_side;
            if (n != 0) {
                n = 0;
            }
            else {
                n = 8;
            }
            remoteViews.setViewVisibility(right_side, n);
            final int line3 = R.id.line3;
            if (n5 != 0) {
                n = n2;
            }
            else {
                n = 8;
            }
            remoteViews.setViewVisibility(line3, n);
            return remoteViews;
        }
        
        @Nullable
        public Notification build() {
            final NotificationCompat.Builder mBuilder = this.mBuilder;
            Notification build;
            if (mBuilder != null) {
                build = mBuilder.build();
            }
            else {
                build = null;
            }
            return build;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public void buildIntoRemoteViews(final RemoteViews remoteViews, final RemoteViews remoteViews2) {
            this.hideNormalContent(remoteViews);
            final int notification_main_column = R.id.notification_main_column;
            remoteViews.removeAllViews(notification_main_column);
            remoteViews.addView(notification_main_column, remoteViews2.clone());
            remoteViews.setViewVisibility(notification_main_column, 0);
            remoteViews.setViewPadding(R.id.notification_main_column_container, 0, this.calculateTopPadding(), 0, 0);
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        protected void clearCompatExtraKeys(@NonNull final Bundle bundle) {
            bundle.remove("android.summaryText");
            bundle.remove("android.title.big");
            bundle.remove("androidx.core.app.extra.COMPAT_TEMPLATE");
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public Bitmap createColoredBitmap(final int n, final int n2) {
            return this.createColoredBitmap(n, n2, 0);
        }
        
        Bitmap createColoredBitmap(@NonNull final IconCompat iconCompat, final int n) {
            return this.createColoredBitmap(iconCompat, n, 0);
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public boolean displayCustomViewInline() {
            return false;
        }
        
        @Nullable
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        protected String getClassName() {
            return null;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public RemoteViews makeBigContentView(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            return null;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public RemoteViews makeContentView(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            return null;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public RemoteViews makeHeadsUpContentView(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            return null;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        protected void restoreFromCompatExtras(@NonNull final Bundle bundle) {
            if (((BaseBundle)bundle).containsKey("android.summaryText")) {
                this.mSummaryText = bundle.getCharSequence("android.summaryText");
                this.mSummaryTextSet = true;
            }
            this.mBigContentTitle = bundle.getCharSequence("android.title.big");
        }
        
        public void setBuilder(@Nullable final NotificationCompat.Builder mBuilder) {
            if (this.mBuilder != mBuilder && (this.mBuilder = mBuilder) != null) {
                mBuilder.setStyle(this);
            }
        }
    }
    
    public static class BigTextStyle extends Style
    {
        private static final String TEMPLATE_CLASS_NAME = "androidx.core.app.NotificationCompat$BigTextStyle";
        private CharSequence mBigText;
        
        public BigTextStyle() {
        }
        
        public BigTextStyle(@Nullable final NotificationCompat.Builder builder) {
            ((Style)this).setBuilder(builder);
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        @Override
        public void addCompatExtras(@NonNull final Bundle bundle) {
            super.addCompatExtras(bundle);
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        @Override
        public void apply(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            final Notification$BigTextStyle bigText = new Notification$BigTextStyle(notificationBuilderWithBuilderAccessor.getBuilder()).setBigContentTitle(super.mBigContentTitle).bigText(this.mBigText);
            if (super.mSummaryTextSet) {
                bigText.setSummaryText(super.mSummaryText);
            }
        }
        
        @NonNull
        public BigTextStyle bigText(@Nullable final CharSequence charSequence) {
            this.mBigText = NotificationCompat.Builder.limitCharSequenceLength(charSequence);
            return this;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        @Override
        protected void clearCompatExtraKeys(@NonNull final Bundle bundle) {
            super.clearCompatExtraKeys(bundle);
            bundle.remove("android.bigText");
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        @Override
        protected String getClassName() {
            return "androidx.core.app.NotificationCompat$BigTextStyle";
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        @Override
        protected void restoreFromCompatExtras(@NonNull final Bundle bundle) {
            super.restoreFromCompatExtras(bundle);
            this.mBigText = bundle.getCharSequence("android.bigText");
        }
        
        @NonNull
        public BigTextStyle setBigContentTitle(@Nullable final CharSequence charSequence) {
            super.mBigContentTitle = NotificationCompat.Builder.limitCharSequenceLength(charSequence);
            return this;
        }
        
        @NonNull
        public BigTextStyle setSummaryText(@Nullable final CharSequence charSequence) {
            super.mSummaryText = NotificationCompat.Builder.limitCharSequenceLength(charSequence);
            super.mSummaryTextSet = true;
            return this;
        }
    }
    
    public static final class BubbleMetadata
    {
        private static final int FLAG_AUTO_EXPAND_BUBBLE = 1;
        private static final int FLAG_SUPPRESS_NOTIFICATION = 2;
        private PendingIntent mDeleteIntent;
        private int mDesiredHeight;
        @DimenRes
        private int mDesiredHeightResId;
        private int mFlags;
        private IconCompat mIcon;
        private PendingIntent mPendingIntent;
        private String mShortcutId;
        
        private BubbleMetadata(@Nullable final PendingIntent mPendingIntent, @Nullable final PendingIntent mDeleteIntent, @Nullable final IconCompat mIcon, final int mDesiredHeight, @DimenRes final int mDesiredHeightResId, final int mFlags, @Nullable final String mShortcutId) {
            this.mPendingIntent = mPendingIntent;
            this.mIcon = mIcon;
            this.mDesiredHeight = mDesiredHeight;
            this.mDesiredHeightResId = mDesiredHeightResId;
            this.mDeleteIntent = mDeleteIntent;
            this.mFlags = mFlags;
            this.mShortcutId = mShortcutId;
        }
        
        @Nullable
        public static BubbleMetadata fromPlatform(@Nullable final Notification$BubbleMetadata notification$BubbleMetadata) {
            if (notification$BubbleMetadata == null) {
                return null;
            }
            final int sdk_INT = Build$VERSION.SDK_INT;
            if (sdk_INT >= 30) {
                return Api30Impl.fromPlatform(notification$BubbleMetadata);
            }
            if (sdk_INT == 29) {
                return Api29Impl.fromPlatform(notification$BubbleMetadata);
            }
            return null;
        }
        
        @Nullable
        public static Notification$BubbleMetadata toPlatform(@Nullable final BubbleMetadata bubbleMetadata) {
            if (bubbleMetadata == null) {
                return null;
            }
            final int sdk_INT = Build$VERSION.SDK_INT;
            if (sdk_INT >= 30) {
                return Api30Impl.toPlatform(bubbleMetadata);
            }
            if (sdk_INT == 29) {
                return Api29Impl.toPlatform(bubbleMetadata);
            }
            return null;
        }
        
        public boolean getAutoExpandBubble() {
            final int mFlags = this.mFlags;
            boolean b = true;
            if ((mFlags & 0x1) == 0x0) {
                b = false;
            }
            return b;
        }
        
        @Nullable
        public PendingIntent getDeleteIntent() {
            return this.mDeleteIntent;
        }
        
        @Dimension(unit = 0)
        public int getDesiredHeight() {
            return this.mDesiredHeight;
        }
        
        @DimenRes
        public int getDesiredHeightResId() {
            return this.mDesiredHeightResId;
        }
        
        @SuppressLint({ "InvalidNullConversion" })
        @Nullable
        public IconCompat getIcon() {
            return this.mIcon;
        }
        
        @SuppressLint({ "InvalidNullConversion" })
        @Nullable
        public PendingIntent getIntent() {
            return this.mPendingIntent;
        }
        
        @Nullable
        public String getShortcutId() {
            return this.mShortcutId;
        }
        
        public boolean isNotificationSuppressed() {
            return (this.mFlags & 0x2) != 0x0;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public void setFlags(final int mFlags) {
            this.mFlags = mFlags;
        }
        
        @RequiresApi(29)
        private static class Api29Impl
        {
            @Nullable
            @RequiresApi(29)
            static BubbleMetadata fromPlatform(@Nullable final Notification$BubbleMetadata notification$BubbleMetadata) {
                if (notification$BubbleMetadata == null) {
                    return null;
                }
                if (O0oO008.\u3007080(notification$BubbleMetadata) == null) {
                    return null;
                }
                final Builder setSuppressNotification = new Builder(O0oO008.\u3007080(notification$BubbleMetadata), IconCompat.createFromIcon(o\u30078\u3007.\u3007080(notification$BubbleMetadata))).setAutoExpandBubble(ooO\u300700O.\u3007080(notification$BubbleMetadata)).setDeleteIntent(O0\u3007OO8.\u3007080(notification$BubbleMetadata)).setSuppressNotification(Oo\u3007O8o\u30078.\u3007080(notification$BubbleMetadata));
                if (\u3007000\u3007\u300708.\u3007080(notification$BubbleMetadata) != 0) {
                    setSuppressNotification.setDesiredHeight(\u3007000\u3007\u300708.\u3007080(notification$BubbleMetadata));
                }
                if (O8O\u300788oO0.\u3007080(notification$BubbleMetadata) != 0) {
                    setSuppressNotification.setDesiredHeightResId(O8O\u300788oO0.\u3007080(notification$BubbleMetadata));
                }
                return setSuppressNotification.build();
            }
            
            @Nullable
            @RequiresApi(29)
            static Notification$BubbleMetadata toPlatform(@Nullable final BubbleMetadata bubbleMetadata) {
                if (bubbleMetadata == null) {
                    return null;
                }
                if (bubbleMetadata.getIntent() == null) {
                    return null;
                }
                final Notification$BubbleMetadata$Builder \u3007080 = O0o.\u3007080(o8O0.\u3007080(O00.\u3007080(o80ooO.\u3007080(O0OO8\u30070.\u3007080(new Notification$BubbleMetadata$Builder(), bubbleMetadata.getIcon().toIcon()), bubbleMetadata.getIntent()), bubbleMetadata.getDeleteIntent()), bubbleMetadata.getAutoExpandBubble()), bubbleMetadata.isNotificationSuppressed());
                if (bubbleMetadata.getDesiredHeight() != 0) {
                    O0o\u3007O0\u3007.\u3007080(\u3007080, bubbleMetadata.getDesiredHeight());
                }
                if (bubbleMetadata.getDesiredHeightResId() != 0) {
                    o0O\u30078o0O.\u3007080(\u3007080, bubbleMetadata.getDesiredHeightResId());
                }
                return Oo0oO\u3007O\u3007O.\u3007080(\u3007080);
            }
        }
        
        @RequiresApi(30)
        private static class Api30Impl
        {
            @Nullable
            @RequiresApi(30)
            static BubbleMetadata fromPlatform(@Nullable final Notification$BubbleMetadata notification$BubbleMetadata) {
                if (notification$BubbleMetadata == null) {
                    return null;
                }
                Builder builder;
                if (\u3007\u300700OO.\u3007080(notification$BubbleMetadata) != null) {
                    builder = new Builder(\u3007\u300700OO.\u3007080(notification$BubbleMetadata));
                }
                else {
                    builder = new Builder(O0oO008.\u3007080(notification$BubbleMetadata), IconCompat.createFromIcon(o\u30078\u3007.\u3007080(notification$BubbleMetadata)));
                }
                builder.setAutoExpandBubble(ooO\u300700O.\u3007080(notification$BubbleMetadata)).setDeleteIntent(O0\u3007OO8.\u3007080(notification$BubbleMetadata)).setSuppressNotification(Oo\u3007O8o\u30078.\u3007080(notification$BubbleMetadata));
                if (\u3007000\u3007\u300708.\u3007080(notification$BubbleMetadata) != 0) {
                    builder.setDesiredHeight(\u3007000\u3007\u300708.\u3007080(notification$BubbleMetadata));
                }
                if (O8O\u300788oO0.\u3007080(notification$BubbleMetadata) != 0) {
                    builder.setDesiredHeightResId(O8O\u300788oO0.\u3007080(notification$BubbleMetadata));
                }
                return builder.build();
            }
            
            @Nullable
            @RequiresApi(30)
            static Notification$BubbleMetadata toPlatform(@Nullable final BubbleMetadata bubbleMetadata) {
                if (bubbleMetadata == null) {
                    return null;
                }
                Notification$BubbleMetadata$Builder notification$BubbleMetadata$Builder;
                if (bubbleMetadata.getShortcutId() != null) {
                    notification$BubbleMetadata$Builder = new Notification$BubbleMetadata$Builder(bubbleMetadata.getShortcutId());
                }
                else {
                    notification$BubbleMetadata$Builder = new Notification$BubbleMetadata$Builder(bubbleMetadata.getIntent(), bubbleMetadata.getIcon().toIcon());
                }
                O0o.\u3007080(o8O0.\u3007080(O00.\u3007080(notification$BubbleMetadata$Builder, bubbleMetadata.getDeleteIntent()), bubbleMetadata.getAutoExpandBubble()), bubbleMetadata.isNotificationSuppressed());
                if (bubbleMetadata.getDesiredHeight() != 0) {
                    O0o\u3007O0\u3007.\u3007080(notification$BubbleMetadata$Builder, bubbleMetadata.getDesiredHeight());
                }
                if (bubbleMetadata.getDesiredHeightResId() != 0) {
                    o0O\u30078o0O.\u3007080(notification$BubbleMetadata$Builder, bubbleMetadata.getDesiredHeightResId());
                }
                return Oo0oO\u3007O\u3007O.\u3007080(notification$BubbleMetadata$Builder);
            }
        }
        
        public static final class Builder
        {
            private PendingIntent mDeleteIntent;
            private int mDesiredHeight;
            @DimenRes
            private int mDesiredHeightResId;
            private int mFlags;
            private IconCompat mIcon;
            private PendingIntent mPendingIntent;
            private String mShortcutId;
            
            @Deprecated
            public Builder() {
            }
            
            public Builder(@NonNull final PendingIntent mPendingIntent, @NonNull final IconCompat mIcon) {
                if (mPendingIntent == null) {
                    throw new NullPointerException("Bubble requires non-null pending intent");
                }
                if (mIcon != null) {
                    this.mPendingIntent = mPendingIntent;
                    this.mIcon = mIcon;
                    return;
                }
                throw new NullPointerException("Bubbles require non-null icon");
            }
            
            @RequiresApi(30)
            public Builder(@NonNull final String mShortcutId) {
                if (!TextUtils.isEmpty((CharSequence)mShortcutId)) {
                    this.mShortcutId = mShortcutId;
                    return;
                }
                throw new NullPointerException("Bubble requires a non-null shortcut id");
            }
            
            @NonNull
            private Builder setFlag(final int n, final boolean b) {
                if (b) {
                    this.mFlags |= n;
                }
                else {
                    this.mFlags &= ~n;
                }
                return this;
            }
            
            @SuppressLint({ "SyntheticAccessor" })
            @NonNull
            public BubbleMetadata build() {
                final String mShortcutId = this.mShortcutId;
                if (mShortcutId == null && this.mPendingIntent == null) {
                    throw new NullPointerException("Must supply pending intent or shortcut to bubble");
                }
                if (mShortcutId == null && this.mIcon == null) {
                    throw new NullPointerException("Must supply an icon or shortcut for the bubble");
                }
                final BubbleMetadata bubbleMetadata = new BubbleMetadata(this.mPendingIntent, this.mDeleteIntent, this.mIcon, this.mDesiredHeight, this.mDesiredHeightResId, this.mFlags, mShortcutId);
                bubbleMetadata.setFlags(this.mFlags);
                return bubbleMetadata;
            }
            
            @NonNull
            public Builder setAutoExpandBubble(final boolean b) {
                this.setFlag(1, b);
                return this;
            }
            
            @NonNull
            public Builder setDeleteIntent(@Nullable final PendingIntent mDeleteIntent) {
                this.mDeleteIntent = mDeleteIntent;
                return this;
            }
            
            @NonNull
            public Builder setDesiredHeight(@Dimension(unit = 0) final int a) {
                this.mDesiredHeight = Math.max(a, 0);
                this.mDesiredHeightResId = 0;
                return this;
            }
            
            @NonNull
            public Builder setDesiredHeightResId(@DimenRes final int mDesiredHeightResId) {
                this.mDesiredHeightResId = mDesiredHeightResId;
                this.mDesiredHeight = 0;
                return this;
            }
            
            @NonNull
            public Builder setIcon(@NonNull final IconCompat mIcon) {
                if (this.mShortcutId != null) {
                    throw new IllegalStateException("Created as a shortcut bubble, cannot set an Icon. Consider using BubbleMetadata.Builder(PendingIntent,Icon) instead.");
                }
                if (mIcon != null) {
                    this.mIcon = mIcon;
                    return this;
                }
                throw new NullPointerException("Bubbles require non-null icon");
            }
            
            @NonNull
            public Builder setIntent(@NonNull final PendingIntent mPendingIntent) {
                if (this.mShortcutId != null) {
                    throw new IllegalStateException("Created as a shortcut bubble, cannot set a PendingIntent. Consider using BubbleMetadata.Builder(PendingIntent,Icon) instead.");
                }
                if (mPendingIntent != null) {
                    this.mPendingIntent = mPendingIntent;
                    return this;
                }
                throw new NullPointerException("Bubble requires non-null pending intent");
            }
            
            @NonNull
            public Builder setSuppressNotification(final boolean b) {
                this.setFlag(2, b);
                return this;
            }
        }
    }
    
    public static class Builder
    {
        private static final int MAX_CHARSEQUENCE_LENGTH = 5120;
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public ArrayList<Action> mActions;
        boolean mAllowSystemGeneratedContextualActions;
        int mBadgeIcon;
        RemoteViews mBigContentView;
        BubbleMetadata mBubbleMetadata;
        String mCategory;
        String mChannelId;
        boolean mChronometerCountDown;
        int mColor;
        boolean mColorized;
        boolean mColorizedSet;
        CharSequence mContentInfo;
        PendingIntent mContentIntent;
        CharSequence mContentText;
        CharSequence mContentTitle;
        RemoteViews mContentView;
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public Context mContext;
        Bundle mExtras;
        int mFgsDeferBehavior;
        PendingIntent mFullScreenIntent;
        int mGroupAlertBehavior;
        String mGroupKey;
        boolean mGroupSummary;
        RemoteViews mHeadsUpContentView;
        ArrayList<Action> mInvisibleActions;
        Bitmap mLargeIcon;
        boolean mLocalOnly;
        LocusIdCompat mLocusId;
        Notification mNotification;
        int mNumber;
        @Deprecated
        public ArrayList<String> mPeople;
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public ArrayList<Person> mPersonList;
        int mPriority;
        int mProgress;
        boolean mProgressIndeterminate;
        int mProgressMax;
        Notification mPublicVersion;
        CharSequence[] mRemoteInputHistory;
        CharSequence mSettingsText;
        String mShortcutId;
        boolean mShowWhen;
        boolean mSilent;
        Icon mSmallIcon;
        String mSortKey;
        Style mStyle;
        CharSequence mSubText;
        RemoteViews mTickerView;
        long mTimeout;
        boolean mUseChronometer;
        int mVisibility;
        
        @Deprecated
        public Builder(@NonNull final Context context) {
            this(context, (String)null);
        }
        
        @RequiresApi(19)
        public Builder(@NonNull final Context context, @NonNull final Notification notification) {
            this(context, NotificationCompat.getChannelId(notification));
            final Bundle extras = notification.extras;
            final Style styleFromNotification = Style.extractStyleFromNotification(notification);
            this.setContentTitle(NotificationCompat.getContentTitle(notification)).setContentText(NotificationCompat.getContentText(notification)).setContentInfo(NotificationCompat.getContentInfo(notification)).setSubText(NotificationCompat.getSubText(notification)).setSettingsText(NotificationCompat.getSettingsText(notification)).setStyle(styleFromNotification).setContentIntent(notification.contentIntent).setGroup(NotificationCompat.getGroup(notification)).setGroupSummary(NotificationCompat.isGroupSummary(notification)).setLocusId(NotificationCompat.getLocusId(notification)).setWhen(notification.when).setShowWhen(NotificationCompat.getShowWhen(notification)).setUsesChronometer(NotificationCompat.getUsesChronometer(notification)).setAutoCancel(NotificationCompat.getAutoCancel(notification)).setOnlyAlertOnce(NotificationCompat.getOnlyAlertOnce(notification)).setOngoing(NotificationCompat.getOngoing(notification)).setLocalOnly(NotificationCompat.getLocalOnly(notification)).setLargeIcon(notification.largeIcon).setBadgeIconType(NotificationCompat.getBadgeIconType(notification)).setCategory(NotificationCompat.getCategory(notification)).setBubbleMetadata(NotificationCompat.getBubbleMetadata(notification)).setNumber(notification.number).setTicker(notification.tickerText).setContentIntent(notification.contentIntent).setDeleteIntent(notification.deleteIntent).setFullScreenIntent(notification.fullScreenIntent, NotificationCompat.getHighPriority(notification)).setSound(notification.sound, notification.audioStreamType).setVibrate(notification.vibrate).setLights(notification.ledARGB, notification.ledOnMS, notification.ledOffMS).setDefaults(notification.defaults).setPriority(notification.priority).setColor(NotificationCompat.getColor(notification)).setVisibility(NotificationCompat.getVisibility(notification)).setPublicVersion(NotificationCompat.getPublicVersion(notification)).setSortKey(NotificationCompat.getSortKey(notification)).setTimeoutAfter(NotificationCompat.getTimeoutAfter(notification)).setShortcutId(NotificationCompat.getShortcutId(notification)).setProgress(((BaseBundle)extras).getInt("android.progressMax"), ((BaseBundle)extras).getInt("android.progress"), extras.getBoolean("android.progressIndeterminate")).setAllowSystemGeneratedContextualActions(NotificationCompat.getAllowSystemGeneratedContextualActions(notification)).setSmallIcon(notification.icon, notification.iconLevel).addExtras(getExtrasWithoutDuplicateData(notification, styleFromNotification));
            if (Build$VERSION.SDK_INT >= 23) {
                this.mSmallIcon = ooo\u3007\u3007O\u3007.\u3007080(notification);
            }
            final Notification$Action[] actions = notification.actions;
            final int n = 0;
            if (actions != null && actions.length != 0) {
                for (int length = actions.length, i = 0; i < length; ++i) {
                    this.addAction(Action.Builder.fromAndroidAction(actions[i]).build());
                }
            }
            final List<Action> invisibleActions = NotificationCompat.getInvisibleActions(notification);
            if (!invisibleActions.isEmpty()) {
                final Iterator iterator = invisibleActions.iterator();
                while (iterator.hasNext()) {
                    this.addInvisibleAction((Action)iterator.next());
                }
            }
            final String[] stringArray = ((BaseBundle)notification.extras).getStringArray("android.people");
            if (stringArray != null && stringArray.length != 0) {
                for (int length2 = stringArray.length, j = n; j < length2; ++j) {
                    this.addPerson(stringArray[j]);
                }
            }
            if (Build$VERSION.SDK_INT >= 28) {
                final ArrayList parcelableArrayList = notification.extras.getParcelableArrayList("android.people.list");
                if (parcelableArrayList != null && !parcelableArrayList.isEmpty()) {
                    final Iterator iterator2 = parcelableArrayList.iterator();
                    while (iterator2.hasNext()) {
                        this.addPerson(Person.fromAndroidPerson((android.app.Person)iterator2.next()));
                    }
                }
            }
            final int sdk_INT = Build$VERSION.SDK_INT;
            if (sdk_INT >= 24 && ((BaseBundle)extras).containsKey("android.chronometerCountDown")) {
                this.setChronometerCountDown(extras.getBoolean("android.chronometerCountDown"));
            }
            if (sdk_INT >= 26 && ((BaseBundle)extras).containsKey("android.colorized")) {
                this.setColorized(extras.getBoolean("android.colorized"));
            }
        }
        
        public Builder(@NonNull final Context mContext, @NonNull final String mChannelId) {
            this.mActions = new ArrayList<Action>();
            this.mPersonList = new ArrayList<Person>();
            this.mInvisibleActions = new ArrayList<Action>();
            this.mShowWhen = true;
            this.mLocalOnly = false;
            this.mColor = 0;
            this.mVisibility = 0;
            this.mBadgeIcon = 0;
            this.mGroupAlertBehavior = 0;
            this.mFgsDeferBehavior = 0;
            final Notification mNotification = new Notification();
            this.mNotification = mNotification;
            this.mContext = mContext;
            this.mChannelId = mChannelId;
            mNotification.when = System.currentTimeMillis();
            this.mNotification.audioStreamType = -1;
            this.mPriority = 0;
            this.mPeople = new ArrayList<String>();
            this.mAllowSystemGeneratedContextualActions = true;
        }
        
        @Nullable
        @RequiresApi(19)
        private static Bundle getExtrasWithoutDuplicateData(@NonNull final Notification notification, @Nullable final Style style) {
            if (notification.extras == null) {
                return null;
            }
            final Bundle bundle = new Bundle(notification.extras);
            bundle.remove("android.title");
            bundle.remove("android.text");
            bundle.remove("android.infoText");
            bundle.remove("android.subText");
            bundle.remove("android.intent.extra.CHANNEL_ID");
            bundle.remove("android.intent.extra.CHANNEL_GROUP_ID");
            bundle.remove("android.showWhen");
            bundle.remove("android.progress");
            bundle.remove("android.progressMax");
            bundle.remove("android.progressIndeterminate");
            bundle.remove("android.chronometerCountDown");
            bundle.remove("android.colorized");
            bundle.remove("android.people.list");
            bundle.remove("android.people");
            bundle.remove("android.support.sortKey");
            bundle.remove("android.support.groupKey");
            bundle.remove("android.support.isGroupSummary");
            bundle.remove("android.support.localOnly");
            bundle.remove("android.support.actionExtras");
            final Bundle bundle2 = bundle.getBundle("android.car.EXTENSIONS");
            if (bundle2 != null) {
                final Bundle bundle3 = new Bundle(bundle2);
                bundle3.remove("invisible_actions");
                bundle.putBundle("android.car.EXTENSIONS", bundle3);
            }
            if (style != null) {
                style.clearCompatExtraKeys(bundle);
            }
            return bundle;
        }
        
        @Nullable
        protected static CharSequence limitCharSequenceLength(@Nullable final CharSequence charSequence) {
            if (charSequence == null) {
                return charSequence;
            }
            CharSequence subSequence = charSequence;
            if (charSequence.length() > 5120) {
                subSequence = charSequence.subSequence(0, 5120);
            }
            return subSequence;
        }
        
        @Nullable
        private Bitmap reduceLargeIconSize(@Nullable final Bitmap bitmap) {
            Bitmap scaledBitmap = bitmap;
            if (bitmap != null) {
                if (Build$VERSION.SDK_INT >= 27) {
                    scaledBitmap = bitmap;
                }
                else {
                    final Resources resources = this.mContext.getResources();
                    final int dimensionPixelSize = resources.getDimensionPixelSize(R.dimen.compat_notification_large_icon_max_width);
                    final int dimensionPixelSize2 = resources.getDimensionPixelSize(R.dimen.compat_notification_large_icon_max_height);
                    if (bitmap.getWidth() <= dimensionPixelSize && bitmap.getHeight() <= dimensionPixelSize2) {
                        return bitmap;
                    }
                    final double min = Math.min(dimensionPixelSize / (double)Math.max(1, bitmap.getWidth()), dimensionPixelSize2 / (double)Math.max(1, bitmap.getHeight()));
                    scaledBitmap = Bitmap.createScaledBitmap(bitmap, (int)Math.ceil(bitmap.getWidth() * min), (int)Math.ceil(bitmap.getHeight() * min), true);
                }
            }
            return scaledBitmap;
        }
        
        private void setFlag(final int n, final boolean b) {
            if (b) {
                final Notification mNotification = this.mNotification;
                mNotification.flags |= n;
            }
            else {
                final Notification mNotification2 = this.mNotification;
                mNotification2.flags &= ~n;
            }
        }
        
        private boolean useExistingRemoteView() {
            final Style mStyle = this.mStyle;
            return mStyle == null || !mStyle.displayCustomViewInline();
        }
        
        @NonNull
        public Builder addAction(final int n, @Nullable final CharSequence charSequence, @Nullable final PendingIntent pendingIntent) {
            this.mActions.add(new Action(n, charSequence, pendingIntent));
            return this;
        }
        
        @NonNull
        public Builder addAction(@Nullable final Action e) {
            if (e != null) {
                this.mActions.add(e);
            }
            return this;
        }
        
        @NonNull
        public Builder addExtras(@Nullable final Bundle bundle) {
            if (bundle != null) {
                final Bundle mExtras = this.mExtras;
                if (mExtras == null) {
                    this.mExtras = new Bundle(bundle);
                }
                else {
                    mExtras.putAll(bundle);
                }
            }
            return this;
        }
        
        @NonNull
        @RequiresApi(21)
        public Builder addInvisibleAction(final int n, @Nullable final CharSequence charSequence, @Nullable final PendingIntent pendingIntent) {
            this.mInvisibleActions.add(new Action(n, charSequence, pendingIntent));
            return this;
        }
        
        @NonNull
        @RequiresApi(21)
        public Builder addInvisibleAction(@Nullable final Action e) {
            if (e != null) {
                this.mInvisibleActions.add(e);
            }
            return this;
        }
        
        @NonNull
        public Builder addPerson(@Nullable final Person e) {
            if (e != null) {
                this.mPersonList.add(e);
            }
            return this;
        }
        
        @Deprecated
        @NonNull
        public Builder addPerson(@Nullable final String e) {
            if (e != null && !e.isEmpty()) {
                this.mPeople.add(e);
            }
            return this;
        }
        
        @NonNull
        public Notification build() {
            return new NotificationCompatBuilder(this).build();
        }
        
        @NonNull
        public Builder clearActions() {
            this.mActions.clear();
            return this;
        }
        
        @NonNull
        public Builder clearInvisibleActions() {
            this.mInvisibleActions.clear();
            final Bundle bundle = this.mExtras.getBundle("android.car.EXTENSIONS");
            if (bundle != null) {
                final Bundle bundle2 = new Bundle(bundle);
                bundle2.remove("invisible_actions");
                this.mExtras.putBundle("android.car.EXTENSIONS", bundle2);
            }
            return this;
        }
        
        @NonNull
        public Builder clearPeople() {
            this.mPersonList.clear();
            this.mPeople.clear();
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @Nullable
        public RemoteViews createBigContentView() {
            final int sdk_INT = Build$VERSION.SDK_INT;
            if (this.mBigContentView != null && this.useExistingRemoteView()) {
                return this.mBigContentView;
            }
            final NotificationCompatBuilder notificationCompatBuilder = new NotificationCompatBuilder(this);
            final Style mStyle = this.mStyle;
            if (mStyle != null) {
                final RemoteViews bigContentView = mStyle.makeBigContentView(notificationCompatBuilder);
                if (bigContentView != null) {
                    return bigContentView;
                }
            }
            final Notification build = notificationCompatBuilder.build();
            if (sdk_INT >= 24) {
                return \u30070OO8.\u3007080(o\u30070.\u3007080(this.mContext, build));
            }
            return build.bigContentView;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @Nullable
        public RemoteViews createContentView() {
            if (this.mContentView != null && this.useExistingRemoteView()) {
                return this.mContentView;
            }
            final NotificationCompatBuilder notificationCompatBuilder = new NotificationCompatBuilder(this);
            final Style mStyle = this.mStyle;
            if (mStyle != null) {
                final RemoteViews contentView = mStyle.makeContentView(notificationCompatBuilder);
                if (contentView != null) {
                    return contentView;
                }
            }
            final Notification build = notificationCompatBuilder.build();
            if (Build$VERSION.SDK_INT >= 24) {
                return \u3007o\u30078.\u3007080(o\u30070.\u3007080(this.mContext, build));
            }
            return build.contentView;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @Nullable
        public RemoteViews createHeadsUpContentView() {
            final int sdk_INT = Build$VERSION.SDK_INT;
            if (this.mHeadsUpContentView != null && this.useExistingRemoteView()) {
                return this.mHeadsUpContentView;
            }
            final NotificationCompatBuilder notificationCompatBuilder = new NotificationCompatBuilder(this);
            final Style mStyle = this.mStyle;
            if (mStyle != null) {
                final RemoteViews headsUpContentView = mStyle.makeHeadsUpContentView(notificationCompatBuilder);
                if (headsUpContentView != null) {
                    return headsUpContentView;
                }
            }
            final Notification build = notificationCompatBuilder.build();
            if (sdk_INT >= 24) {
                return O0oo0o0\u3007.\u3007080(o\u30070.\u3007080(this.mContext, build));
            }
            return build.headsUpContentView;
        }
        
        @NonNull
        public Builder extend(@NonNull final NotificationCompat.Extender extender) {
            extender.extend(this);
            return this;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public RemoteViews getBigContentView() {
            return this.mBigContentView;
        }
        
        @Nullable
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public BubbleMetadata getBubbleMetadata() {
            return this.mBubbleMetadata;
        }
        
        @ColorInt
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public int getColor() {
            return this.mColor;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public RemoteViews getContentView() {
            return this.mContentView;
        }
        
        @NonNull
        public Bundle getExtras() {
            if (this.mExtras == null) {
                this.mExtras = new Bundle();
            }
            return this.mExtras;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public int getForegroundServiceBehavior() {
            return this.mFgsDeferBehavior;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public RemoteViews getHeadsUpContentView() {
            return this.mHeadsUpContentView;
        }
        
        @Deprecated
        @NonNull
        public Notification getNotification() {
            return this.build();
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public int getPriority() {
            return this.mPriority;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public long getWhenIfShowing() {
            long when;
            if (this.mShowWhen) {
                when = this.mNotification.when;
            }
            else {
                when = 0L;
            }
            return when;
        }
        
        @NonNull
        public Builder setAllowSystemGeneratedContextualActions(final boolean mAllowSystemGeneratedContextualActions) {
            this.mAllowSystemGeneratedContextualActions = mAllowSystemGeneratedContextualActions;
            return this;
        }
        
        @NonNull
        public Builder setAutoCancel(final boolean b) {
            this.setFlag(16, b);
            return this;
        }
        
        @NonNull
        public Builder setBadgeIconType(final int mBadgeIcon) {
            this.mBadgeIcon = mBadgeIcon;
            return this;
        }
        
        @NonNull
        public Builder setBubbleMetadata(@Nullable final BubbleMetadata mBubbleMetadata) {
            this.mBubbleMetadata = mBubbleMetadata;
            return this;
        }
        
        @NonNull
        public Builder setCategory(@Nullable final String mCategory) {
            this.mCategory = mCategory;
            return this;
        }
        
        @NonNull
        public Builder setChannelId(@NonNull final String mChannelId) {
            this.mChannelId = mChannelId;
            return this;
        }
        
        @NonNull
        @RequiresApi(24)
        public Builder setChronometerCountDown(final boolean mChronometerCountDown) {
            this.mChronometerCountDown = mChronometerCountDown;
            this.getExtras().putBoolean("android.chronometerCountDown", mChronometerCountDown);
            return this;
        }
        
        @NonNull
        public Builder setColor(@ColorInt final int mColor) {
            this.mColor = mColor;
            return this;
        }
        
        @NonNull
        public Builder setColorized(final boolean mColorized) {
            this.mColorized = mColorized;
            this.mColorizedSet = true;
            return this;
        }
        
        @NonNull
        public Builder setContent(@Nullable final RemoteViews contentView) {
            this.mNotification.contentView = contentView;
            return this;
        }
        
        @NonNull
        public Builder setContentInfo(@Nullable final CharSequence charSequence) {
            this.mContentInfo = limitCharSequenceLength(charSequence);
            return this;
        }
        
        @NonNull
        public Builder setContentIntent(@Nullable final PendingIntent mContentIntent) {
            this.mContentIntent = mContentIntent;
            return this;
        }
        
        @NonNull
        public Builder setContentText(@Nullable final CharSequence charSequence) {
            this.mContentText = limitCharSequenceLength(charSequence);
            return this;
        }
        
        @NonNull
        public Builder setContentTitle(@Nullable final CharSequence charSequence) {
            this.mContentTitle = limitCharSequenceLength(charSequence);
            return this;
        }
        
        @NonNull
        public Builder setCustomBigContentView(@Nullable final RemoteViews mBigContentView) {
            this.mBigContentView = mBigContentView;
            return this;
        }
        
        @NonNull
        public Builder setCustomContentView(@Nullable final RemoteViews mContentView) {
            this.mContentView = mContentView;
            return this;
        }
        
        @NonNull
        public Builder setCustomHeadsUpContentView(@Nullable final RemoteViews mHeadsUpContentView) {
            this.mHeadsUpContentView = mHeadsUpContentView;
            return this;
        }
        
        @NonNull
        public Builder setDefaults(final int defaults) {
            final Notification mNotification = this.mNotification;
            mNotification.defaults = defaults;
            if ((defaults & 0x4) != 0x0) {
                mNotification.flags |= 0x1;
            }
            return this;
        }
        
        @NonNull
        public Builder setDeleteIntent(@Nullable final PendingIntent deleteIntent) {
            this.mNotification.deleteIntent = deleteIntent;
            return this;
        }
        
        @NonNull
        public Builder setExtras(@Nullable final Bundle mExtras) {
            this.mExtras = mExtras;
            return this;
        }
        
        @NonNull
        public Builder setForegroundServiceBehavior(final int mFgsDeferBehavior) {
            this.mFgsDeferBehavior = mFgsDeferBehavior;
            return this;
        }
        
        @NonNull
        public Builder setFullScreenIntent(@Nullable final PendingIntent mFullScreenIntent, final boolean b) {
            this.mFullScreenIntent = mFullScreenIntent;
            this.setFlag(128, b);
            return this;
        }
        
        @NonNull
        public Builder setGroup(@Nullable final String mGroupKey) {
            this.mGroupKey = mGroupKey;
            return this;
        }
        
        @NonNull
        public Builder setGroupAlertBehavior(final int mGroupAlertBehavior) {
            this.mGroupAlertBehavior = mGroupAlertBehavior;
            return this;
        }
        
        @NonNull
        public Builder setGroupSummary(final boolean mGroupSummary) {
            this.mGroupSummary = mGroupSummary;
            return this;
        }
        
        @NonNull
        public Builder setLargeIcon(@Nullable final Bitmap bitmap) {
            this.mLargeIcon = this.reduceLargeIconSize(bitmap);
            return this;
        }
        
        @NonNull
        public Builder setLights(@ColorInt int ledARGB, final int ledOnMS, final int ledOffMS) {
            final Notification mNotification = this.mNotification;
            mNotification.ledARGB = ledARGB;
            mNotification.ledOnMS = ledOnMS;
            mNotification.ledOffMS = ledOffMS;
            if (ledOnMS != 0 && ledOffMS != 0) {
                ledARGB = 1;
            }
            else {
                ledARGB = 0;
            }
            mNotification.flags = (ledARGB | (mNotification.flags & 0xFFFFFFFE));
            return this;
        }
        
        @NonNull
        public Builder setLocalOnly(final boolean mLocalOnly) {
            this.mLocalOnly = mLocalOnly;
            return this;
        }
        
        @NonNull
        public Builder setLocusId(@Nullable final LocusIdCompat mLocusId) {
            this.mLocusId = mLocusId;
            return this;
        }
        
        @Deprecated
        @NonNull
        public Builder setNotificationSilent() {
            this.mSilent = true;
            return this;
        }
        
        @NonNull
        public Builder setNumber(final int mNumber) {
            this.mNumber = mNumber;
            return this;
        }
        
        @NonNull
        public Builder setOngoing(final boolean b) {
            this.setFlag(2, b);
            return this;
        }
        
        @NonNull
        public Builder setOnlyAlertOnce(final boolean b) {
            this.setFlag(8, b);
            return this;
        }
        
        @NonNull
        public Builder setPriority(final int mPriority) {
            this.mPriority = mPriority;
            return this;
        }
        
        @NonNull
        public Builder setProgress(final int mProgressMax, final int mProgress, final boolean mProgressIndeterminate) {
            this.mProgressMax = mProgressMax;
            this.mProgress = mProgress;
            this.mProgressIndeterminate = mProgressIndeterminate;
            return this;
        }
        
        @NonNull
        public Builder setPublicVersion(@Nullable final Notification mPublicVersion) {
            this.mPublicVersion = mPublicVersion;
            return this;
        }
        
        @NonNull
        public Builder setRemoteInputHistory(@Nullable final CharSequence[] mRemoteInputHistory) {
            this.mRemoteInputHistory = mRemoteInputHistory;
            return this;
        }
        
        @NonNull
        public Builder setSettingsText(@Nullable final CharSequence charSequence) {
            this.mSettingsText = limitCharSequenceLength(charSequence);
            return this;
        }
        
        @NonNull
        public Builder setShortcutId(@Nullable final String mShortcutId) {
            this.mShortcutId = mShortcutId;
            return this;
        }
        
        @NonNull
        public Builder setShortcutInfo(@Nullable final ShortcutInfoCompat shortcutInfoCompat) {
            if (shortcutInfoCompat == null) {
                return this;
            }
            this.mShortcutId = shortcutInfoCompat.getId();
            if (this.mLocusId == null) {
                if (shortcutInfoCompat.getLocusId() != null) {
                    this.mLocusId = shortcutInfoCompat.getLocusId();
                }
                else if (shortcutInfoCompat.getId() != null) {
                    this.mLocusId = new LocusIdCompat(shortcutInfoCompat.getId());
                }
            }
            if (this.mContentTitle == null) {
                this.setContentTitle(shortcutInfoCompat.getShortLabel());
            }
            return this;
        }
        
        @NonNull
        public Builder setShowWhen(final boolean mShowWhen) {
            this.mShowWhen = mShowWhen;
            return this;
        }
        
        @NonNull
        public Builder setSilent(final boolean mSilent) {
            this.mSilent = mSilent;
            return this;
        }
        
        @NonNull
        public Builder setSmallIcon(final int icon) {
            this.mNotification.icon = icon;
            return this;
        }
        
        @NonNull
        public Builder setSmallIcon(final int icon, final int iconLevel) {
            final Notification mNotification = this.mNotification;
            mNotification.icon = icon;
            mNotification.iconLevel = iconLevel;
            return this;
        }
        
        @NonNull
        @RequiresApi(23)
        public Builder setSmallIcon(@NonNull final IconCompat iconCompat) {
            this.mSmallIcon = iconCompat.toIcon(this.mContext);
            return this;
        }
        
        @NonNull
        public Builder setSortKey(@Nullable final String mSortKey) {
            this.mSortKey = mSortKey;
            return this;
        }
        
        @NonNull
        public Builder setSound(@Nullable final Uri sound) {
            final Notification mNotification = this.mNotification;
            mNotification.sound = sound;
            mNotification.audioStreamType = -1;
            mNotification.audioAttributes = new AudioAttributes$Builder().setContentType(4).setUsage(5).build();
            return this;
        }
        
        @NonNull
        public Builder setSound(@Nullable final Uri sound, final int n) {
            final Notification mNotification = this.mNotification;
            mNotification.sound = sound;
            mNotification.audioStreamType = n;
            mNotification.audioAttributes = new AudioAttributes$Builder().setContentType(4).setLegacyStreamType(n).build();
            return this;
        }
        
        @NonNull
        public Builder setStyle(@Nullable final Style mStyle) {
            if (this.mStyle != mStyle && (this.mStyle = mStyle) != null) {
                mStyle.setBuilder(this);
            }
            return this;
        }
        
        @NonNull
        public Builder setSubText(@Nullable final CharSequence charSequence) {
            this.mSubText = limitCharSequenceLength(charSequence);
            return this;
        }
        
        @NonNull
        public Builder setTicker(@Nullable final CharSequence charSequence) {
            this.mNotification.tickerText = limitCharSequenceLength(charSequence);
            return this;
        }
        
        @Deprecated
        @NonNull
        public Builder setTicker(@Nullable final CharSequence charSequence, @Nullable final RemoteViews mTickerView) {
            this.mNotification.tickerText = limitCharSequenceLength(charSequence);
            this.mTickerView = mTickerView;
            return this;
        }
        
        @NonNull
        public Builder setTimeoutAfter(final long mTimeout) {
            this.mTimeout = mTimeout;
            return this;
        }
        
        @NonNull
        public Builder setUsesChronometer(final boolean mUseChronometer) {
            this.mUseChronometer = mUseChronometer;
            return this;
        }
        
        @NonNull
        public Builder setVibrate(@Nullable final long[] vibrate) {
            this.mNotification.vibrate = vibrate;
            return this;
        }
        
        @NonNull
        public Builder setVisibility(final int mVisibility) {
            this.mVisibility = mVisibility;
            return this;
        }
        
        @NonNull
        public Builder setWhen(final long when) {
            this.mNotification.when = when;
            return this;
        }
    }
    
    public static final class CarExtender implements NotificationCompat.Extender
    {
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        static final String EXTRA_CAR_EXTENDER = "android.car.EXTENSIONS";
        private static final String EXTRA_COLOR = "app_color";
        private static final String EXTRA_CONVERSATION = "car_conversation";
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        static final String EXTRA_INVISIBLE_ACTIONS = "invisible_actions";
        private static final String EXTRA_LARGE_ICON = "large_icon";
        private static final String KEY_AUTHOR = "author";
        private static final String KEY_MESSAGES = "messages";
        private static final String KEY_ON_READ = "on_read";
        private static final String KEY_ON_REPLY = "on_reply";
        private static final String KEY_PARTICIPANTS = "participants";
        private static final String KEY_REMOTE_INPUT = "remote_input";
        private static final String KEY_TEXT = "text";
        private static final String KEY_TIMESTAMP = "timestamp";
        private int mColor;
        private Bitmap mLargeIcon;
        private UnreadConversation mUnreadConversation;
        
        public CarExtender() {
            this.mColor = 0;
        }
        
        public CarExtender(@NonNull final Notification notification) {
            this.mColor = 0;
            Object bundle;
            if (NotificationCompat.getExtras(notification) == null) {
                bundle = null;
            }
            else {
                bundle = NotificationCompat.getExtras(notification).getBundle("android.car.EXTENSIONS");
            }
            if (bundle != null) {
                this.mLargeIcon = (Bitmap)((Bundle)bundle).getParcelable("large_icon");
                this.mColor = ((BaseBundle)bundle).getInt("app_color", 0);
                this.mUnreadConversation = getUnreadConversationFromBundle(((Bundle)bundle).getBundle("car_conversation"));
            }
        }
        
        @RequiresApi(21)
        private static Bundle getBundleForUnreadConversation(@NonNull final UnreadConversation unreadConversation) {
            final Bundle bundle = new Bundle();
            final String[] participants = unreadConversation.getParticipants();
            int i = 0;
            String s;
            if (participants != null && unreadConversation.getParticipants().length > 1) {
                s = unreadConversation.getParticipants()[0];
            }
            else {
                s = null;
            }
            final int length = unreadConversation.getMessages().length;
            final Parcelable[] array = new Parcelable[length];
            while (i < length) {
                final Bundle bundle2 = new Bundle();
                ((BaseBundle)bundle2).putString("text", unreadConversation.getMessages()[i]);
                ((BaseBundle)bundle2).putString("author", s);
                array[i] = (Parcelable)bundle2;
                ++i;
            }
            bundle.putParcelableArray("messages", array);
            final RemoteInput remoteInput = unreadConversation.getRemoteInput();
            if (remoteInput != null) {
                bundle.putParcelable("remote_input", (Parcelable)new RemoteInput$Builder(remoteInput.getResultKey()).setLabel(remoteInput.getLabel()).setChoices(remoteInput.getChoices()).setAllowFreeFormInput(remoteInput.getAllowFreeFormInput()).addExtras(remoteInput.getExtras()).build());
            }
            bundle.putParcelable("on_reply", (Parcelable)unreadConversation.getReplyPendingIntent());
            bundle.putParcelable("on_read", (Parcelable)unreadConversation.getReadPendingIntent());
            ((BaseBundle)bundle).putStringArray("participants", unreadConversation.getParticipants());
            ((BaseBundle)bundle).putLong("timestamp", unreadConversation.getLatestTimestamp());
            return bundle;
        }
        
        @RequiresApi(21)
        private static UnreadConversation getUnreadConversationFromBundle(@Nullable final Bundle bundle) {
            final UnreadConversation unreadConversation = null;
            final RemoteInput remoteInput = null;
            if (bundle == null) {
                return null;
            }
            final Parcelable[] parcelableArray = bundle.getParcelableArray("messages");
            String[] array = null;
            Label_0107: {
                if (parcelableArray != null) {
                    final int length = parcelableArray.length;
                    array = new String[length];
                    int i = 0;
                    while (true) {
                        while (i < length) {
                            final Parcelable parcelable = parcelableArray[i];
                            if (parcelable instanceof Bundle && (array[i] = ((BaseBundle)parcelable).getString("text")) != null) {
                                ++i;
                            }
                            else {
                                final boolean b = false;
                                if (b) {
                                    break Label_0107;
                                }
                                return null;
                            }
                        }
                        final boolean b = true;
                        continue;
                    }
                }
                array = null;
            }
            final PendingIntent pendingIntent = (PendingIntent)bundle.getParcelable("on_read");
            final PendingIntent pendingIntent2 = (PendingIntent)bundle.getParcelable("on_reply");
            final android.app.RemoteInput remoteInput2 = (android.app.RemoteInput)bundle.getParcelable("remote_input");
            final String[] stringArray = ((BaseBundle)bundle).getStringArray("participants");
            UnreadConversation unreadConversation2 = unreadConversation;
            if (stringArray != null) {
                if (stringArray.length != 1) {
                    unreadConversation2 = unreadConversation;
                }
                else {
                    RemoteInput remoteInput3 = remoteInput;
                    if (remoteInput2 != null) {
                        final String resultKey = remoteInput2.getResultKey();
                        final CharSequence label = remoteInput2.getLabel();
                        final CharSequence[] choices = remoteInput2.getChoices();
                        final boolean allowFreeFormInput = remoteInput2.getAllowFreeFormInput();
                        int \u3007080;
                        if (Build$VERSION.SDK_INT >= 29) {
                            \u3007080 = ooo0\u3007O88O.\u3007080(remoteInput2);
                        }
                        else {
                            \u3007080 = 0;
                        }
                        remoteInput3 = new RemoteInput(resultKey, label, choices, allowFreeFormInput, \u3007080, remoteInput2.getExtras(), null);
                    }
                    unreadConversation2 = new UnreadConversation(array, remoteInput3, pendingIntent2, pendingIntent, stringArray, ((BaseBundle)bundle).getLong("timestamp"));
                }
            }
            return unreadConversation2;
        }
        
        @NonNull
        @Override
        public NotificationCompat.Builder extend(@NonNull final NotificationCompat.Builder builder) {
            final Bundle bundle = new Bundle();
            final Bitmap mLargeIcon = this.mLargeIcon;
            if (mLargeIcon != null) {
                bundle.putParcelable("large_icon", (Parcelable)mLargeIcon);
            }
            final int mColor = this.mColor;
            if (mColor != 0) {
                ((BaseBundle)bundle).putInt("app_color", mColor);
            }
            final UnreadConversation mUnreadConversation = this.mUnreadConversation;
            if (mUnreadConversation != null) {
                bundle.putBundle("car_conversation", getBundleForUnreadConversation(mUnreadConversation));
            }
            builder.getExtras().putBundle("android.car.EXTENSIONS", bundle);
            return builder;
        }
        
        @ColorInt
        public int getColor() {
            return this.mColor;
        }
        
        @Nullable
        public Bitmap getLargeIcon() {
            return this.mLargeIcon;
        }
        
        @Deprecated
        @Nullable
        public UnreadConversation getUnreadConversation() {
            return this.mUnreadConversation;
        }
        
        @NonNull
        public CarExtender setColor(@ColorInt final int mColor) {
            this.mColor = mColor;
            return this;
        }
        
        @NonNull
        public CarExtender setLargeIcon(@Nullable final Bitmap mLargeIcon) {
            this.mLargeIcon = mLargeIcon;
            return this;
        }
        
        @Deprecated
        @NonNull
        public CarExtender setUnreadConversation(@Nullable final UnreadConversation mUnreadConversation) {
            this.mUnreadConversation = mUnreadConversation;
            return this;
        }
        
        @Deprecated
        public static class UnreadConversation
        {
            private final long mLatestTimestamp;
            private final String[] mMessages;
            private final String[] mParticipants;
            private final PendingIntent mReadPendingIntent;
            private final RemoteInput mRemoteInput;
            private final PendingIntent mReplyPendingIntent;
            
            UnreadConversation(@Nullable final String[] mMessages, @Nullable final RemoteInput mRemoteInput, @Nullable final PendingIntent mReplyPendingIntent, @Nullable final PendingIntent mReadPendingIntent, @Nullable final String[] mParticipants, final long mLatestTimestamp) {
                this.mMessages = mMessages;
                this.mRemoteInput = mRemoteInput;
                this.mReadPendingIntent = mReadPendingIntent;
                this.mReplyPendingIntent = mReplyPendingIntent;
                this.mParticipants = mParticipants;
                this.mLatestTimestamp = mLatestTimestamp;
            }
            
            public long getLatestTimestamp() {
                return this.mLatestTimestamp;
            }
            
            @Nullable
            public String[] getMessages() {
                return this.mMessages;
            }
            
            @Nullable
            public String getParticipant() {
                final String[] mParticipants = this.mParticipants;
                String s;
                if (mParticipants.length > 0) {
                    s = mParticipants[0];
                }
                else {
                    s = null;
                }
                return s;
            }
            
            @Nullable
            public String[] getParticipants() {
                return this.mParticipants;
            }
            
            @Nullable
            public PendingIntent getReadPendingIntent() {
                return this.mReadPendingIntent;
            }
            
            @Nullable
            public RemoteInput getRemoteInput() {
                return this.mRemoteInput;
            }
            
            @Nullable
            public PendingIntent getReplyPendingIntent() {
                return this.mReplyPendingIntent;
            }
            
            public static class Builder
            {
                private long mLatestTimestamp;
                private final List<String> mMessages;
                private final String mParticipant;
                private PendingIntent mReadPendingIntent;
                private RemoteInput mRemoteInput;
                private PendingIntent mReplyPendingIntent;
                
                public Builder(@NonNull final String mParticipant) {
                    this.mMessages = new ArrayList<String>();
                    this.mParticipant = mParticipant;
                }
                
                @NonNull
                public Builder addMessage(@Nullable final String s) {
                    if (s != null) {
                        this.mMessages.add(s);
                    }
                    return this;
                }
                
                @NonNull
                public UnreadConversation build() {
                    final List<String> mMessages = this.mMessages;
                    return new UnreadConversation(mMessages.toArray(new String[mMessages.size()]), this.mRemoteInput, this.mReplyPendingIntent, this.mReadPendingIntent, new String[] { this.mParticipant }, this.mLatestTimestamp);
                }
                
                @NonNull
                public Builder setLatestTimestamp(final long mLatestTimestamp) {
                    this.mLatestTimestamp = mLatestTimestamp;
                    return this;
                }
                
                @NonNull
                public Builder setReadPendingIntent(@Nullable final PendingIntent mReadPendingIntent) {
                    this.mReadPendingIntent = mReadPendingIntent;
                    return this;
                }
                
                @NonNull
                public Builder setReplyAction(@Nullable final PendingIntent mReplyPendingIntent, @Nullable final RemoteInput mRemoteInput) {
                    this.mRemoteInput = mRemoteInput;
                    this.mReplyPendingIntent = mReplyPendingIntent;
                    return this;
                }
            }
        }
    }
    
    public static class DecoratedCustomViewStyle extends Style
    {
        private static final int MAX_ACTION_BUTTONS = 3;
        private static final String TEMPLATE_CLASS_NAME = "androidx.core.app.NotificationCompat$DecoratedCustomViewStyle";
        
        private RemoteViews createRemoteViews(final RemoteViews remoteViews, final boolean b) {
            final int notification_template_custom_big = R.layout.notification_template_custom_big;
            final int n = 1;
            final int n2 = 0;
            final RemoteViews applyStandardTemplate = ((Style)this).applyStandardTemplate(true, notification_template_custom_big, false);
            applyStandardTemplate.removeAllViews(R.id.actions);
            final List<Action> nonContextualActions = getNonContextualActions(super.mBuilder.mActions);
            int n4 = 0;
            Label_0115: {
                if (b && nonContextualActions != null) {
                    final int min = Math.min(nonContextualActions.size(), 3);
                    if (min > 0) {
                        int n3 = 0;
                        while (true) {
                            n4 = n;
                            if (n3 >= min) {
                                break Label_0115;
                            }
                            applyStandardTemplate.addView(R.id.actions, this.generateActionButton((Action)nonContextualActions.get(n3)));
                            ++n3;
                        }
                    }
                }
                n4 = 0;
            }
            int n5;
            if (n4 != 0) {
                n5 = n2;
            }
            else {
                n5 = 8;
            }
            applyStandardTemplate.setViewVisibility(R.id.actions, n5);
            applyStandardTemplate.setViewVisibility(R.id.action_divider, n5);
            ((Style)this).buildIntoRemoteViews(applyStandardTemplate, remoteViews);
            return applyStandardTemplate;
        }
        
        private RemoteViews generateActionButton(final Action action) {
            final boolean b = action.actionIntent == null;
            final String packageName = super.mBuilder.mContext.getPackageName();
            int n;
            if (b) {
                n = R.layout.notification_action_tombstone;
            }
            else {
                n = R.layout.notification_action;
            }
            final RemoteViews remoteViews = new RemoteViews(packageName, n);
            final IconCompat iconCompat = action.getIconCompat();
            if (iconCompat != null) {
                remoteViews.setImageViewBitmap(R.id.action_image, ((Style)this).createColoredBitmap(iconCompat, super.mBuilder.mContext.getResources().getColor(R.color.notification_action_color_filter)));
            }
            remoteViews.setTextViewText(R.id.action_text, action.title);
            if (!b) {
                remoteViews.setOnClickPendingIntent(R.id.action_container, action.actionIntent);
            }
            remoteViews.setContentDescription(R.id.action_container, action.title);
            return remoteViews;
        }
        
        private static List<Action> getNonContextualActions(final List<Action> list) {
            if (list == null) {
                return null;
            }
            final ArrayList list2 = new ArrayList();
            for (final Action action : list) {
                if (!action.isContextual()) {
                    list2.add(action);
                }
            }
            return list2;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        @Override
        public void apply(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            if (Build$VERSION.SDK_INT >= 24) {
                notificationBuilderWithBuilderAccessor.getBuilder().setStyle((Notification$Style)new Notification$DecoratedCustomViewStyle());
            }
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        @Override
        public boolean displayCustomViewInline() {
            return true;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        @Override
        protected String getClassName() {
            return "androidx.core.app.NotificationCompat$DecoratedCustomViewStyle";
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        @Override
        public RemoteViews makeBigContentView(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            if (Build$VERSION.SDK_INT >= 24) {
                return null;
            }
            RemoteViews remoteViews = super.mBuilder.getBigContentView();
            if (remoteViews == null) {
                remoteViews = super.mBuilder.getContentView();
            }
            if (remoteViews == null) {
                return null;
            }
            return this.createRemoteViews(remoteViews, true);
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        @Override
        public RemoteViews makeContentView(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            if (Build$VERSION.SDK_INT >= 24) {
                return null;
            }
            if (super.mBuilder.getContentView() == null) {
                return null;
            }
            return this.createRemoteViews(super.mBuilder.getContentView(), false);
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        @Override
        public RemoteViews makeHeadsUpContentView(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            if (Build$VERSION.SDK_INT >= 24) {
                return null;
            }
            final RemoteViews headsUpContentView = super.mBuilder.getHeadsUpContentView();
            RemoteViews contentView;
            if (headsUpContentView != null) {
                contentView = headsUpContentView;
            }
            else {
                contentView = super.mBuilder.getContentView();
            }
            if (headsUpContentView == null) {
                return null;
            }
            return this.createRemoteViews(contentView, true);
        }
    }
    
    public interface Extender
    {
        @NonNull
        NotificationCompat.Builder extend(@NonNull final NotificationCompat.Builder p0);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public @interface GroupAlertBehavior {
    }
    
    public static class InboxStyle extends Style
    {
        private static final String TEMPLATE_CLASS_NAME = "androidx.core.app.NotificationCompat$InboxStyle";
        private ArrayList<CharSequence> mTexts;
        
        public InboxStyle() {
            this.mTexts = new ArrayList<CharSequence>();
        }
        
        public InboxStyle(@Nullable final NotificationCompat.Builder builder) {
            this.mTexts = new ArrayList<CharSequence>();
            ((Style)this).setBuilder(builder);
        }
        
        @NonNull
        public InboxStyle addLine(@Nullable final CharSequence charSequence) {
            if (charSequence != null) {
                this.mTexts.add(NotificationCompat.Builder.limitCharSequenceLength(charSequence));
            }
            return this;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        @Override
        public void apply(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            final Notification$InboxStyle setBigContentTitle = new Notification$InboxStyle(notificationBuilderWithBuilderAccessor.getBuilder()).setBigContentTitle(super.mBigContentTitle);
            if (super.mSummaryTextSet) {
                setBigContentTitle.setSummaryText(super.mSummaryText);
            }
            final Iterator<CharSequence> iterator = this.mTexts.iterator();
            while (iterator.hasNext()) {
                setBigContentTitle.addLine((CharSequence)iterator.next());
            }
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        @Override
        protected void clearCompatExtraKeys(@NonNull final Bundle bundle) {
            super.clearCompatExtraKeys(bundle);
            bundle.remove("android.textLines");
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        @Override
        protected String getClassName() {
            return "androidx.core.app.NotificationCompat$InboxStyle";
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        @Override
        protected void restoreFromCompatExtras(@NonNull final Bundle bundle) {
            super.restoreFromCompatExtras(bundle);
            this.mTexts.clear();
            if (((BaseBundle)bundle).containsKey("android.textLines")) {
                Collections.addAll(this.mTexts, bundle.getCharSequenceArray("android.textLines"));
            }
        }
        
        @NonNull
        public InboxStyle setBigContentTitle(@Nullable final CharSequence charSequence) {
            super.mBigContentTitle = NotificationCompat.Builder.limitCharSequenceLength(charSequence);
            return this;
        }
        
        @NonNull
        public InboxStyle setSummaryText(@Nullable final CharSequence charSequence) {
            super.mSummaryText = NotificationCompat.Builder.limitCharSequenceLength(charSequence);
            super.mSummaryTextSet = true;
            return this;
        }
    }
    
    public static class MessagingStyle extends Style
    {
        public static final int MAXIMUM_RETAINED_MESSAGES = 25;
        private static final String TEMPLATE_CLASS_NAME = "androidx.core.app.NotificationCompat$MessagingStyle";
        @Nullable
        private CharSequence mConversationTitle;
        private final List<Message> mHistoricMessages;
        @Nullable
        private Boolean mIsGroupConversation;
        private final List<Message> mMessages;
        private Person mUser;
        
        MessagingStyle() {
            this.mMessages = new ArrayList<Message>();
            this.mHistoricMessages = new ArrayList<Message>();
        }
        
        public MessagingStyle(@NonNull final Person mUser) {
            this.mMessages = new ArrayList<Message>();
            this.mHistoricMessages = new ArrayList<Message>();
            if (!TextUtils.isEmpty(mUser.getName())) {
                this.mUser = mUser;
                return;
            }
            throw new IllegalArgumentException("User's name must not be empty.");
        }
        
        @Deprecated
        public MessagingStyle(@NonNull final CharSequence name) {
            this.mMessages = new ArrayList<Message>();
            this.mHistoricMessages = new ArrayList<Message>();
            this.mUser = new Person.Builder().setName(name).build();
        }
        
        @Nullable
        public static MessagingStyle extractMessagingStyleFromNotification(@NonNull final Notification notification) {
            final Style styleFromNotification = Style.extractStyleFromNotification(notification);
            if (styleFromNotification instanceof MessagingStyle) {
                return (MessagingStyle)styleFromNotification;
            }
            return null;
        }
        
        @Nullable
        private Message findLatestIncomingMessage() {
            for (int i = this.mMessages.size() - 1; i >= 0; --i) {
                final Message message = this.mMessages.get(i);
                if (message.getPerson() != null && !TextUtils.isEmpty(message.getPerson().getName())) {
                    return message;
                }
            }
            if (!this.mMessages.isEmpty()) {
                final List<Message> mMessages = this.mMessages;
                return mMessages.get(mMessages.size() - 1);
            }
            return null;
        }
        
        private boolean hasMessagesWithoutSender() {
            for (int i = this.mMessages.size() - 1; i >= 0; --i) {
                final Message message = this.mMessages.get(i);
                if (message.getPerson() != null && message.getPerson().getName() == null) {
                    return true;
                }
            }
            return false;
        }
        
        @NonNull
        private TextAppearanceSpan makeFontColorSpan(final int n) {
            return new TextAppearanceSpan((String)null, 0, 0, ColorStateList.valueOf(n), (ColorStateList)null);
        }
        
        private CharSequence makeMessageLine(@NonNull final Message message) {
            final BidiFormatter instance = BidiFormatter.getInstance();
            final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
            final Person person = message.getPerson();
            final String s = "";
            CharSequence charSequence;
            if (person == null) {
                charSequence = "";
            }
            else {
                charSequence = message.getPerson().getName();
            }
            final boolean empty = TextUtils.isEmpty(charSequence);
            int color = -16777216;
            if (empty) {
                final CharSequence charSequence2 = charSequence = this.mUser.getName();
                color = color;
                if (super.mBuilder.getColor() != 0) {
                    color = super.mBuilder.getColor();
                    charSequence = charSequence2;
                }
            }
            final CharSequence unicodeWrap = instance.unicodeWrap(charSequence);
            spannableStringBuilder.append(unicodeWrap);
            spannableStringBuilder.setSpan((Object)this.makeFontColorSpan(color), spannableStringBuilder.length() - unicodeWrap.length(), spannableStringBuilder.length(), 33);
            CharSequence text;
            if (message.getText() == null) {
                text = s;
            }
            else {
                text = message.getText();
            }
            spannableStringBuilder.append((CharSequence)"  ").append(instance.unicodeWrap(text));
            return (CharSequence)spannableStringBuilder;
        }
        
        @Override
        public void addCompatExtras(@NonNull final Bundle bundle) {
            super.addCompatExtras(bundle);
            bundle.putCharSequence("android.selfDisplayName", this.mUser.getName());
            bundle.putBundle("android.messagingStyleUser", this.mUser.toBundle());
            bundle.putCharSequence("android.hiddenConversationTitle", this.mConversationTitle);
            if (this.mConversationTitle != null && this.mIsGroupConversation) {
                bundle.putCharSequence("android.conversationTitle", this.mConversationTitle);
            }
            if (!this.mMessages.isEmpty()) {
                bundle.putParcelableArray("android.messages", (Parcelable[])Message.getBundleArrayForMessages(this.mMessages));
            }
            if (!this.mHistoricMessages.isEmpty()) {
                bundle.putParcelableArray("android.messages.historic", (Parcelable[])Message.getBundleArrayForMessages(this.mHistoricMessages));
            }
            final Boolean mIsGroupConversation = this.mIsGroupConversation;
            if (mIsGroupConversation != null) {
                bundle.putBoolean("android.isGroupConversation", (boolean)mIsGroupConversation);
            }
        }
        
        @NonNull
        public MessagingStyle addHistoricMessage(@Nullable final Message message) {
            if (message != null) {
                this.mHistoricMessages.add(message);
                if (this.mHistoricMessages.size() > 25) {
                    this.mHistoricMessages.remove(0);
                }
            }
            return this;
        }
        
        @NonNull
        public MessagingStyle addMessage(@Nullable final Message message) {
            if (message != null) {
                this.mMessages.add(message);
                if (this.mMessages.size() > 25) {
                    this.mMessages.remove(0);
                }
            }
            return this;
        }
        
        @NonNull
        public MessagingStyle addMessage(@Nullable final CharSequence charSequence, final long n, @Nullable final Person person) {
            this.addMessage(new Message(charSequence, n, person));
            return this;
        }
        
        @Deprecated
        @NonNull
        public MessagingStyle addMessage(@Nullable final CharSequence charSequence, final long n, @Nullable final CharSequence name) {
            this.mMessages.add(new Message(charSequence, n, new Person.Builder().setName(name).build()));
            if (this.mMessages.size() > 25) {
                this.mMessages.remove(0);
            }
            return this;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        @Override
        public void apply(final NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
            this.setGroupConversation(this.isGroupConversation());
            final int sdk_INT = Build$VERSION.SDK_INT;
            if (sdk_INT >= 24) {
                Notification$MessagingStyle notification$MessagingStyle;
                if (sdk_INT >= 28) {
                    notification$MessagingStyle = new Notification$MessagingStyle(this.mUser.toAndroidPerson());
                }
                else {
                    notification$MessagingStyle = new Notification$MessagingStyle(this.mUser.getName());
                }
                final Iterator<Message> iterator = this.mMessages.iterator();
                while (iterator.hasNext()) {
                    OO\u30070008O8.\u3007080(notification$MessagingStyle, iterator.next().toAndroidMessage());
                }
                if (Build$VERSION.SDK_INT >= 26) {
                    final Iterator<Message> iterator2 = this.mHistoricMessages.iterator();
                    while (iterator2.hasNext()) {
                        \u30078o.\u3007080(notification$MessagingStyle, iterator2.next().toAndroidMessage());
                    }
                }
                if (this.mIsGroupConversation || Build$VERSION.SDK_INT >= 28) {
                    O\u30070\u3007o808\u3007.\u3007080(notification$MessagingStyle, this.mConversationTitle);
                }
                if (Build$VERSION.SDK_INT >= 28) {
                    \u3007o\u3007Oo0.\u3007080(notification$MessagingStyle, (boolean)this.mIsGroupConversation);
                }
                O00O.\u3007080(notification$MessagingStyle, notificationBuilderWithBuilderAccessor.getBuilder());
            }
            else {
                final Message latestIncomingMessage = this.findLatestIncomingMessage();
                if (this.mConversationTitle != null && this.mIsGroupConversation) {
                    notificationBuilderWithBuilderAccessor.getBuilder().setContentTitle(this.mConversationTitle);
                }
                else if (latestIncomingMessage != null) {
                    notificationBuilderWithBuilderAccessor.getBuilder().setContentTitle((CharSequence)"");
                    if (latestIncomingMessage.getPerson() != null) {
                        notificationBuilderWithBuilderAccessor.getBuilder().setContentTitle(latestIncomingMessage.getPerson().getName());
                    }
                }
                if (latestIncomingMessage != null) {
                    final Notification$Builder builder = notificationBuilderWithBuilderAccessor.getBuilder();
                    CharSequence contentText;
                    if (this.mConversationTitle != null) {
                        contentText = this.makeMessageLine(latestIncomingMessage);
                    }
                    else {
                        contentText = latestIncomingMessage.getText();
                    }
                    builder.setContentText(contentText);
                }
                final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
                final boolean b = this.mConversationTitle != null || this.hasMessagesWithoutSender();
                for (int i = this.mMessages.size() - 1; i >= 0; --i) {
                    final Message message = this.mMessages.get(i);
                    CharSequence charSequence;
                    if (b) {
                        charSequence = this.makeMessageLine(message);
                    }
                    else {
                        charSequence = message.getText();
                    }
                    if (i != this.mMessages.size() - 1) {
                        spannableStringBuilder.insert(0, (CharSequence)"\n");
                    }
                    spannableStringBuilder.insert(0, charSequence);
                }
                new Notification$BigTextStyle(notificationBuilderWithBuilderAccessor.getBuilder()).setBigContentTitle((CharSequence)null).bigText((CharSequence)spannableStringBuilder);
            }
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        @Override
        protected void clearCompatExtraKeys(@NonNull final Bundle bundle) {
            super.clearCompatExtraKeys(bundle);
            bundle.remove("android.messagingStyleUser");
            bundle.remove("android.selfDisplayName");
            bundle.remove("android.conversationTitle");
            bundle.remove("android.hiddenConversationTitle");
            bundle.remove("android.messages");
            bundle.remove("android.messages.historic");
            bundle.remove("android.isGroupConversation");
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        @Override
        protected String getClassName() {
            return "androidx.core.app.NotificationCompat$MessagingStyle";
        }
        
        @Nullable
        public CharSequence getConversationTitle() {
            return this.mConversationTitle;
        }
        
        @NonNull
        public List<Message> getHistoricMessages() {
            return this.mHistoricMessages;
        }
        
        @NonNull
        public List<Message> getMessages() {
            return this.mMessages;
        }
        
        @NonNull
        public Person getUser() {
            return this.mUser;
        }
        
        @Deprecated
        @Nullable
        public CharSequence getUserDisplayName() {
            return this.mUser.getName();
        }
        
        public boolean isGroupConversation() {
            final NotificationCompat.Builder mBuilder = super.mBuilder;
            boolean booleanValue = false;
            final boolean b = false;
            if (mBuilder != null && mBuilder.mContext.getApplicationInfo().targetSdkVersion < 28 && this.mIsGroupConversation == null) {
                boolean b2 = b;
                if (this.mConversationTitle != null) {
                    b2 = true;
                }
                return b2;
            }
            final Boolean mIsGroupConversation = this.mIsGroupConversation;
            if (mIsGroupConversation != null) {
                booleanValue = mIsGroupConversation;
            }
            return booleanValue;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        @Override
        protected void restoreFromCompatExtras(@NonNull final Bundle bundle) {
            super.restoreFromCompatExtras(bundle);
            this.mMessages.clear();
            if (((BaseBundle)bundle).containsKey("android.messagingStyleUser")) {
                this.mUser = Person.fromBundle(bundle.getBundle("android.messagingStyleUser"));
            }
            else {
                this.mUser = new Person.Builder().setName(((BaseBundle)bundle).getString("android.selfDisplayName")).build();
            }
            final CharSequence charSequence = bundle.getCharSequence("android.conversationTitle");
            this.mConversationTitle = charSequence;
            if (charSequence == null) {
                this.mConversationTitle = bundle.getCharSequence("android.hiddenConversationTitle");
            }
            final Parcelable[] parcelableArray = bundle.getParcelableArray("android.messages");
            if (parcelableArray != null) {
                this.mMessages.addAll(Message.getMessagesFromBundleArray(parcelableArray));
            }
            final Parcelable[] parcelableArray2 = bundle.getParcelableArray("android.messages.historic");
            if (parcelableArray2 != null) {
                this.mHistoricMessages.addAll(Message.getMessagesFromBundleArray(parcelableArray2));
            }
            if (((BaseBundle)bundle).containsKey("android.isGroupConversation")) {
                this.mIsGroupConversation = bundle.getBoolean("android.isGroupConversation");
            }
        }
        
        @NonNull
        public MessagingStyle setConversationTitle(@Nullable final CharSequence mConversationTitle) {
            this.mConversationTitle = mConversationTitle;
            return this;
        }
        
        @NonNull
        public MessagingStyle setGroupConversation(final boolean b) {
            this.mIsGroupConversation = b;
            return this;
        }
        
        public static final class Message
        {
            static final String KEY_DATA_MIME_TYPE = "type";
            static final String KEY_DATA_URI = "uri";
            static final String KEY_EXTRAS_BUNDLE = "extras";
            static final String KEY_NOTIFICATION_PERSON = "sender_person";
            static final String KEY_PERSON = "person";
            static final String KEY_SENDER = "sender";
            static final String KEY_TEXT = "text";
            static final String KEY_TIMESTAMP = "time";
            @Nullable
            private String mDataMimeType;
            @Nullable
            private Uri mDataUri;
            private Bundle mExtras;
            @Nullable
            private final Person mPerson;
            private final CharSequence mText;
            private final long mTimestamp;
            
            public Message(@Nullable final CharSequence mText, final long mTimestamp, @Nullable final Person mPerson) {
                this.mExtras = new Bundle();
                this.mText = mText;
                this.mTimestamp = mTimestamp;
                this.mPerson = mPerson;
            }
            
            @Deprecated
            public Message(@Nullable final CharSequence charSequence, final long n, @Nullable final CharSequence name) {
                this(charSequence, n, new Person.Builder().setName(name).build());
            }
            
            @NonNull
            static Bundle[] getBundleArrayForMessages(@NonNull final List<Message> list) {
                final Bundle[] array = new Bundle[list.size()];
                for (int size = list.size(), i = 0; i < size; ++i) {
                    array[i] = ((Message)list.get(i)).toBundle();
                }
                return array;
            }
            
            @Nullable
            static Message getMessageFromBundle(@NonNull final Bundle bundle) {
                try {
                    if (((BaseBundle)bundle).containsKey("text")) {
                        if (((BaseBundle)bundle).containsKey("time")) {
                            Person person;
                            if (((BaseBundle)bundle).containsKey("person")) {
                                person = Person.fromBundle(bundle.getBundle("person"));
                            }
                            else if (((BaseBundle)bundle).containsKey("sender_person") && Build$VERSION.SDK_INT >= 28) {
                                person = Person.fromAndroidPerson((android.app.Person)bundle.getParcelable("sender_person"));
                            }
                            else if (((BaseBundle)bundle).containsKey("sender")) {
                                person = new Person.Builder().setName(bundle.getCharSequence("sender")).build();
                            }
                            else {
                                person = null;
                            }
                            final Message message = new Message(bundle.getCharSequence("text"), ((BaseBundle)bundle).getLong("time"), person);
                            if (((BaseBundle)bundle).containsKey("type") && ((BaseBundle)bundle).containsKey("uri")) {
                                message.setData(((BaseBundle)bundle).getString("type"), (Uri)bundle.getParcelable("uri"));
                            }
                            if (((BaseBundle)bundle).containsKey("extras")) {
                                message.getExtras().putAll(bundle.getBundle("extras"));
                            }
                            return message;
                        }
                    }
                    return null;
                }
                catch (final ClassCastException ex) {
                    return null;
                }
            }
            
            @NonNull
            static List<Message> getMessagesFromBundleArray(@NonNull final Parcelable[] array) {
                final ArrayList list = new ArrayList(array.length);
                for (int i = 0; i < array.length; ++i) {
                    final Parcelable parcelable = array[i];
                    if (parcelable instanceof Bundle) {
                        final Message messageFromBundle = getMessageFromBundle((Bundle)parcelable);
                        if (messageFromBundle != null) {
                            list.add(messageFromBundle);
                        }
                    }
                }
                return list;
            }
            
            @NonNull
            private Bundle toBundle() {
                final Bundle bundle = new Bundle();
                final CharSequence mText = this.mText;
                if (mText != null) {
                    bundle.putCharSequence("text", mText);
                }
                ((BaseBundle)bundle).putLong("time", this.mTimestamp);
                final Person mPerson = this.mPerson;
                if (mPerson != null) {
                    bundle.putCharSequence("sender", mPerson.getName());
                    if (Build$VERSION.SDK_INT >= 28) {
                        bundle.putParcelable("sender_person", (Parcelable)this.mPerson.toAndroidPerson());
                    }
                    else {
                        bundle.putBundle("person", this.mPerson.toBundle());
                    }
                }
                final String mDataMimeType = this.mDataMimeType;
                if (mDataMimeType != null) {
                    ((BaseBundle)bundle).putString("type", mDataMimeType);
                }
                final Uri mDataUri = this.mDataUri;
                if (mDataUri != null) {
                    bundle.putParcelable("uri", (Parcelable)mDataUri);
                }
                final Bundle mExtras = this.mExtras;
                if (mExtras != null) {
                    bundle.putBundle("extras", mExtras);
                }
                return bundle;
            }
            
            @Nullable
            public String getDataMimeType() {
                return this.mDataMimeType;
            }
            
            @Nullable
            public Uri getDataUri() {
                return this.mDataUri;
            }
            
            @NonNull
            public Bundle getExtras() {
                return this.mExtras;
            }
            
            @Nullable
            public Person getPerson() {
                return this.mPerson;
            }
            
            @Deprecated
            @Nullable
            public CharSequence getSender() {
                final Person mPerson = this.mPerson;
                CharSequence name;
                if (mPerson == null) {
                    name = null;
                }
                else {
                    name = mPerson.getName();
                }
                return name;
            }
            
            @Nullable
            public CharSequence getText() {
                return this.mText;
            }
            
            public long getTimestamp() {
                return this.mTimestamp;
            }
            
            @NonNull
            public Message setData(@Nullable final String mDataMimeType, @Nullable final Uri mDataUri) {
                this.mDataMimeType = mDataMimeType;
                this.mDataUri = mDataUri;
                return this;
            }
            
            @NonNull
            @RequiresApi(24)
            @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
            Notification$MessagingStyle$Message toAndroidMessage() {
                final Person person = this.getPerson();
                final int sdk_INT = Build$VERSION.SDK_INT;
                final CharSequence charSequence = null;
                android.app.Person androidPerson = null;
                Notification$MessagingStyle$Message notification$MessagingStyle$Message;
                if (sdk_INT >= 28) {
                    final CharSequence text = this.getText();
                    final long timestamp = this.getTimestamp();
                    if (person != null) {
                        androidPerson = person.toAndroidPerson();
                    }
                    notification$MessagingStyle$Message = new Notification$MessagingStyle$Message(text, timestamp, androidPerson);
                }
                else {
                    final CharSequence text2 = this.getText();
                    final long timestamp2 = this.getTimestamp();
                    CharSequence name;
                    if (person == null) {
                        name = charSequence;
                    }
                    else {
                        name = person.getName();
                    }
                    notification$MessagingStyle$Message = new Notification$MessagingStyle$Message(text2, timestamp2, name);
                }
                if (this.getDataMimeType() != null) {
                    OO0\u3007\u30078.\u3007080(notification$MessagingStyle$Message, this.getDataMimeType(), this.getDataUri());
                }
                return notification$MessagingStyle$Message;
            }
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public @interface NotificationVisibility {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public @interface ServiceNotificationBehavior {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public @interface StreamType {
    }
    
    public static final class WearableExtender implements NotificationCompat.Extender
    {
        private static final int DEFAULT_CONTENT_ICON_GRAVITY = 8388613;
        private static final int DEFAULT_FLAGS = 1;
        private static final int DEFAULT_GRAVITY = 80;
        private static final String EXTRA_WEARABLE_EXTENSIONS = "android.wearable.EXTENSIONS";
        private static final int FLAG_BIG_PICTURE_AMBIENT = 32;
        private static final int FLAG_CONTENT_INTENT_AVAILABLE_OFFLINE = 1;
        private static final int FLAG_HINT_AVOID_BACKGROUND_CLIPPING = 16;
        private static final int FLAG_HINT_CONTENT_INTENT_LAUNCHES_ACTIVITY = 64;
        private static final int FLAG_HINT_HIDE_ICON = 2;
        private static final int FLAG_HINT_SHOW_BACKGROUND_ONLY = 4;
        private static final int FLAG_START_SCROLL_BOTTOM = 8;
        private static final String KEY_ACTIONS = "actions";
        private static final String KEY_BACKGROUND = "background";
        private static final String KEY_BRIDGE_TAG = "bridgeTag";
        private static final String KEY_CONTENT_ACTION_INDEX = "contentActionIndex";
        private static final String KEY_CONTENT_ICON = "contentIcon";
        private static final String KEY_CONTENT_ICON_GRAVITY = "contentIconGravity";
        private static final String KEY_CUSTOM_CONTENT_HEIGHT = "customContentHeight";
        private static final String KEY_CUSTOM_SIZE_PRESET = "customSizePreset";
        private static final String KEY_DISMISSAL_ID = "dismissalId";
        private static final String KEY_DISPLAY_INTENT = "displayIntent";
        private static final String KEY_FLAGS = "flags";
        private static final String KEY_GRAVITY = "gravity";
        private static final String KEY_HINT_SCREEN_TIMEOUT = "hintScreenTimeout";
        private static final String KEY_PAGES = "pages";
        @Deprecated
        public static final int SCREEN_TIMEOUT_LONG = -1;
        @Deprecated
        public static final int SCREEN_TIMEOUT_SHORT = 0;
        @Deprecated
        public static final int SIZE_DEFAULT = 0;
        @Deprecated
        public static final int SIZE_FULL_SCREEN = 5;
        @Deprecated
        public static final int SIZE_LARGE = 4;
        @Deprecated
        public static final int SIZE_MEDIUM = 3;
        @Deprecated
        public static final int SIZE_SMALL = 2;
        @Deprecated
        public static final int SIZE_XSMALL = 1;
        public static final int UNSET_ACTION_INDEX = -1;
        private ArrayList<Action> mActions;
        private Bitmap mBackground;
        private String mBridgeTag;
        private int mContentActionIndex;
        private int mContentIcon;
        private int mContentIconGravity;
        private int mCustomContentHeight;
        private int mCustomSizePreset;
        private String mDismissalId;
        private PendingIntent mDisplayIntent;
        private int mFlags;
        private int mGravity;
        private int mHintScreenTimeout;
        private ArrayList<Notification> mPages;
        
        public WearableExtender() {
            this.mActions = new ArrayList<Action>();
            this.mFlags = 1;
            this.mPages = new ArrayList<Notification>();
            this.mContentIconGravity = 8388613;
            this.mContentActionIndex = -1;
            this.mCustomSizePreset = 0;
            this.mGravity = 80;
        }
        
        public WearableExtender(@NonNull final Notification notification) {
            this.mActions = new ArrayList<Action>();
            this.mFlags = 1;
            this.mPages = new ArrayList<Notification>();
            this.mContentIconGravity = 8388613;
            this.mContentActionIndex = -1;
            this.mCustomSizePreset = 0;
            this.mGravity = 80;
            final Bundle extras = NotificationCompat.getExtras(notification);
            Bundle bundle;
            if (extras != null) {
                bundle = extras.getBundle("android.wearable.EXTENSIONS");
            }
            else {
                bundle = null;
            }
            if (bundle != null) {
                final ArrayList parcelableArrayList = bundle.getParcelableArrayList("actions");
                if (parcelableArrayList != null) {
                    final int size = parcelableArrayList.size();
                    final Action[] elements = new Action[size];
                    for (int i = 0; i < size; ++i) {
                        elements[i] = NotificationCompat.getActionCompatFromAction((Notification$Action)parcelableArrayList.get(i));
                    }
                    Collections.addAll(this.mActions, elements);
                }
                this.mFlags = ((BaseBundle)bundle).getInt("flags", 1);
                this.mDisplayIntent = (PendingIntent)bundle.getParcelable("displayIntent");
                final Notification[] notificationArrayFromBundle = NotificationCompat.getNotificationArrayFromBundle(bundle, "pages");
                if (notificationArrayFromBundle != null) {
                    Collections.addAll(this.mPages, notificationArrayFromBundle);
                }
                this.mBackground = (Bitmap)bundle.getParcelable("background");
                this.mContentIcon = ((BaseBundle)bundle).getInt("contentIcon");
                this.mContentIconGravity = ((BaseBundle)bundle).getInt("contentIconGravity", 8388613);
                this.mContentActionIndex = ((BaseBundle)bundle).getInt("contentActionIndex", -1);
                this.mCustomSizePreset = ((BaseBundle)bundle).getInt("customSizePreset", 0);
                this.mCustomContentHeight = ((BaseBundle)bundle).getInt("customContentHeight");
                this.mGravity = ((BaseBundle)bundle).getInt("gravity", 80);
                this.mHintScreenTimeout = ((BaseBundle)bundle).getInt("hintScreenTimeout");
                this.mDismissalId = ((BaseBundle)bundle).getString("dismissalId");
                this.mBridgeTag = ((BaseBundle)bundle).getString("bridgeTag");
            }
        }
        
        @RequiresApi(20)
        private static Notification$Action getActionFromActionCompat(final Action action) {
            final int sdk_INT = Build$VERSION.SDK_INT;
            final int n = 0;
            Notification$Action$Builder notification$Action$Builder;
            if (sdk_INT >= 23) {
                final IconCompat iconCompat = action.getIconCompat();
                Icon icon;
                if (iconCompat == null) {
                    icon = null;
                }
                else {
                    icon = iconCompat.toIcon();
                }
                notification$Action$Builder = new Notification$Action$Builder(icon, action.getTitle(), action.getActionIntent());
            }
            else {
                final IconCompat iconCompat2 = action.getIconCompat();
                int resId;
                if (iconCompat2 != null && iconCompat2.getType() == 2) {
                    resId = iconCompat2.getResId();
                }
                else {
                    resId = 0;
                }
                notification$Action$Builder = new Notification$Action$Builder(resId, action.getTitle(), action.getActionIntent());
            }
            Bundle bundle;
            if (action.getExtras() != null) {
                bundle = new Bundle(action.getExtras());
            }
            else {
                bundle = new Bundle();
            }
            bundle.putBoolean("android.support.allowGeneratedReplies", action.getAllowGeneratedReplies());
            if (sdk_INT >= 24) {
                \u3007Oo\u3007o8.\u3007080(notification$Action$Builder, action.getAllowGeneratedReplies());
            }
            if (sdk_INT >= 31) {
                OoO\u3007.\u3007080(notification$Action$Builder, action.isAuthenticationRequired());
            }
            notification$Action$Builder.addExtras(bundle);
            final RemoteInput[] remoteInputs = action.getRemoteInputs();
            if (remoteInputs != null) {
                final android.app.RemoteInput[] fromCompat = RemoteInput.fromCompat(remoteInputs);
                for (int length = fromCompat.length, i = n; i < length; ++i) {
                    notification$Action$Builder.addRemoteInput(fromCompat[i]);
                }
            }
            return notification$Action$Builder.build();
        }
        
        private void setFlag(final int n, final boolean b) {
            if (b) {
                this.mFlags |= n;
            }
            else {
                this.mFlags &= ~n;
            }
        }
        
        @NonNull
        public WearableExtender addAction(@NonNull final Action e) {
            this.mActions.add(e);
            return this;
        }
        
        @NonNull
        public WearableExtender addActions(@NonNull final List<Action> c) {
            this.mActions.addAll(c);
            return this;
        }
        
        @Deprecated
        @NonNull
        public WearableExtender addPage(@NonNull final Notification e) {
            this.mPages.add(e);
            return this;
        }
        
        @Deprecated
        @NonNull
        public WearableExtender addPages(@NonNull final List<Notification> c) {
            this.mPages.addAll(c);
            return this;
        }
        
        @NonNull
        public WearableExtender clearActions() {
            this.mActions.clear();
            return this;
        }
        
        @Deprecated
        @NonNull
        public WearableExtender clearPages() {
            this.mPages.clear();
            return this;
        }
        
        @NonNull
        public WearableExtender clone() {
            final WearableExtender wearableExtender = new WearableExtender();
            wearableExtender.mActions = new ArrayList<Action>(this.mActions);
            wearableExtender.mFlags = this.mFlags;
            wearableExtender.mDisplayIntent = this.mDisplayIntent;
            wearableExtender.mPages = new ArrayList<Notification>(this.mPages);
            wearableExtender.mBackground = this.mBackground;
            wearableExtender.mContentIcon = this.mContentIcon;
            wearableExtender.mContentIconGravity = this.mContentIconGravity;
            wearableExtender.mContentActionIndex = this.mContentActionIndex;
            wearableExtender.mCustomSizePreset = this.mCustomSizePreset;
            wearableExtender.mCustomContentHeight = this.mCustomContentHeight;
            wearableExtender.mGravity = this.mGravity;
            wearableExtender.mHintScreenTimeout = this.mHintScreenTimeout;
            wearableExtender.mDismissalId = this.mDismissalId;
            wearableExtender.mBridgeTag = this.mBridgeTag;
            return wearableExtender;
        }
        
        @NonNull
        @Override
        public NotificationCompat.Builder extend(@NonNull final NotificationCompat.Builder builder) {
            final Bundle bundle = new Bundle();
            if (!this.mActions.isEmpty()) {
                final ArrayList list = new ArrayList(this.mActions.size());
                final Iterator<Action> iterator = this.mActions.iterator();
                while (iterator.hasNext()) {
                    list.add(getActionFromActionCompat(iterator.next()));
                }
                bundle.putParcelableArrayList("actions", list);
            }
            final int mFlags = this.mFlags;
            if (mFlags != 1) {
                ((BaseBundle)bundle).putInt("flags", mFlags);
            }
            final PendingIntent mDisplayIntent = this.mDisplayIntent;
            if (mDisplayIntent != null) {
                bundle.putParcelable("displayIntent", (Parcelable)mDisplayIntent);
            }
            if (!this.mPages.isEmpty()) {
                final ArrayList<Notification> mPages = this.mPages;
                bundle.putParcelableArray("pages", (Parcelable[])mPages.toArray((Parcelable[])new Notification[mPages.size()]));
            }
            final Bitmap mBackground = this.mBackground;
            if (mBackground != null) {
                bundle.putParcelable("background", (Parcelable)mBackground);
            }
            final int mContentIcon = this.mContentIcon;
            if (mContentIcon != 0) {
                ((BaseBundle)bundle).putInt("contentIcon", mContentIcon);
            }
            final int mContentIconGravity = this.mContentIconGravity;
            if (mContentIconGravity != 8388613) {
                ((BaseBundle)bundle).putInt("contentIconGravity", mContentIconGravity);
            }
            final int mContentActionIndex = this.mContentActionIndex;
            if (mContentActionIndex != -1) {
                ((BaseBundle)bundle).putInt("contentActionIndex", mContentActionIndex);
            }
            final int mCustomSizePreset = this.mCustomSizePreset;
            if (mCustomSizePreset != 0) {
                ((BaseBundle)bundle).putInt("customSizePreset", mCustomSizePreset);
            }
            final int mCustomContentHeight = this.mCustomContentHeight;
            if (mCustomContentHeight != 0) {
                ((BaseBundle)bundle).putInt("customContentHeight", mCustomContentHeight);
            }
            final int mGravity = this.mGravity;
            if (mGravity != 80) {
                ((BaseBundle)bundle).putInt("gravity", mGravity);
            }
            final int mHintScreenTimeout = this.mHintScreenTimeout;
            if (mHintScreenTimeout != 0) {
                ((BaseBundle)bundle).putInt("hintScreenTimeout", mHintScreenTimeout);
            }
            final String mDismissalId = this.mDismissalId;
            if (mDismissalId != null) {
                ((BaseBundle)bundle).putString("dismissalId", mDismissalId);
            }
            final String mBridgeTag = this.mBridgeTag;
            if (mBridgeTag != null) {
                ((BaseBundle)bundle).putString("bridgeTag", mBridgeTag);
            }
            builder.getExtras().putBundle("android.wearable.EXTENSIONS", bundle);
            return builder;
        }
        
        @NonNull
        public List<Action> getActions() {
            return this.mActions;
        }
        
        @Deprecated
        @Nullable
        public Bitmap getBackground() {
            return this.mBackground;
        }
        
        @Nullable
        public String getBridgeTag() {
            return this.mBridgeTag;
        }
        
        public int getContentAction() {
            return this.mContentActionIndex;
        }
        
        @Deprecated
        public int getContentIcon() {
            return this.mContentIcon;
        }
        
        @Deprecated
        public int getContentIconGravity() {
            return this.mContentIconGravity;
        }
        
        public boolean getContentIntentAvailableOffline() {
            final int mFlags = this.mFlags;
            boolean b = true;
            if ((mFlags & 0x1) == 0x0) {
                b = false;
            }
            return b;
        }
        
        @Deprecated
        public int getCustomContentHeight() {
            return this.mCustomContentHeight;
        }
        
        @Deprecated
        public int getCustomSizePreset() {
            return this.mCustomSizePreset;
        }
        
        @Nullable
        public String getDismissalId() {
            return this.mDismissalId;
        }
        
        @Deprecated
        @Nullable
        public PendingIntent getDisplayIntent() {
            return this.mDisplayIntent;
        }
        
        @Deprecated
        public int getGravity() {
            return this.mGravity;
        }
        
        @Deprecated
        public boolean getHintAmbientBigPicture() {
            return (this.mFlags & 0x20) != 0x0;
        }
        
        @Deprecated
        public boolean getHintAvoidBackgroundClipping() {
            return (this.mFlags & 0x10) != 0x0;
        }
        
        public boolean getHintContentIntentLaunchesActivity() {
            return (this.mFlags & 0x40) != 0x0;
        }
        
        @Deprecated
        public boolean getHintHideIcon() {
            return (this.mFlags & 0x2) != 0x0;
        }
        
        @Deprecated
        public int getHintScreenTimeout() {
            return this.mHintScreenTimeout;
        }
        
        @Deprecated
        public boolean getHintShowBackgroundOnly() {
            return (this.mFlags & 0x4) != 0x0;
        }
        
        @Deprecated
        @NonNull
        public List<Notification> getPages() {
            return this.mPages;
        }
        
        public boolean getStartScrollBottom() {
            return (this.mFlags & 0x8) != 0x0;
        }
        
        @Deprecated
        @NonNull
        public WearableExtender setBackground(@Nullable final Bitmap mBackground) {
            this.mBackground = mBackground;
            return this;
        }
        
        @NonNull
        public WearableExtender setBridgeTag(@Nullable final String mBridgeTag) {
            this.mBridgeTag = mBridgeTag;
            return this;
        }
        
        @NonNull
        public WearableExtender setContentAction(final int mContentActionIndex) {
            this.mContentActionIndex = mContentActionIndex;
            return this;
        }
        
        @Deprecated
        @NonNull
        public WearableExtender setContentIcon(final int mContentIcon) {
            this.mContentIcon = mContentIcon;
            return this;
        }
        
        @Deprecated
        @NonNull
        public WearableExtender setContentIconGravity(final int mContentIconGravity) {
            this.mContentIconGravity = mContentIconGravity;
            return this;
        }
        
        @NonNull
        public WearableExtender setContentIntentAvailableOffline(final boolean b) {
            this.setFlag(1, b);
            return this;
        }
        
        @Deprecated
        @NonNull
        public WearableExtender setCustomContentHeight(final int mCustomContentHeight) {
            this.mCustomContentHeight = mCustomContentHeight;
            return this;
        }
        
        @Deprecated
        @NonNull
        public WearableExtender setCustomSizePreset(final int mCustomSizePreset) {
            this.mCustomSizePreset = mCustomSizePreset;
            return this;
        }
        
        @NonNull
        public WearableExtender setDismissalId(@Nullable final String mDismissalId) {
            this.mDismissalId = mDismissalId;
            return this;
        }
        
        @Deprecated
        @NonNull
        public WearableExtender setDisplayIntent(@Nullable final PendingIntent mDisplayIntent) {
            this.mDisplayIntent = mDisplayIntent;
            return this;
        }
        
        @Deprecated
        @NonNull
        public WearableExtender setGravity(final int mGravity) {
            this.mGravity = mGravity;
            return this;
        }
        
        @Deprecated
        @NonNull
        public WearableExtender setHintAmbientBigPicture(final boolean b) {
            this.setFlag(32, b);
            return this;
        }
        
        @Deprecated
        @NonNull
        public WearableExtender setHintAvoidBackgroundClipping(final boolean b) {
            this.setFlag(16, b);
            return this;
        }
        
        @NonNull
        public WearableExtender setHintContentIntentLaunchesActivity(final boolean b) {
            this.setFlag(64, b);
            return this;
        }
        
        @Deprecated
        @NonNull
        public WearableExtender setHintHideIcon(final boolean b) {
            this.setFlag(2, b);
            return this;
        }
        
        @Deprecated
        @NonNull
        public WearableExtender setHintScreenTimeout(final int mHintScreenTimeout) {
            this.mHintScreenTimeout = mHintScreenTimeout;
            return this;
        }
        
        @Deprecated
        @NonNull
        public WearableExtender setHintShowBackgroundOnly(final boolean b) {
            this.setFlag(4, b);
            return this;
        }
        
        @NonNull
        public WearableExtender setStartScrollBottom(final boolean b) {
            this.setFlag(8, b);
            return this;
        }
    }
}

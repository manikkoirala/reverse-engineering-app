// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import androidx.annotation.NonNull;
import android.app.ActivityManager;

public final class ActivityManagerCompat
{
    private ActivityManagerCompat() {
    }
    
    public static boolean isLowRamDevice(@NonNull final ActivityManager activityManager) {
        return activityManager.isLowRamDevice();
    }
}

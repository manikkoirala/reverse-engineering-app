// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import androidx.annotation.CallSuper;
import android.annotation.SuppressLint;
import androidx.lifecycle.ReportFragment;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import android.view.View;
import android.view.Window$Callback;
import android.view.KeyEvent;
import androidx.annotation.OptIn;
import androidx.core.os.BuildCompat;
import android.os.Build$VERSION;
import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleRegistry;
import androidx.collection.SimpleArrayMap;
import androidx.annotation.RestrictTo;
import androidx.core.view.KeyEventDispatcher;
import androidx.lifecycle.LifecycleOwner;
import android.app.Activity;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public class ComponentActivity extends Activity implements LifecycleOwner, Component
{
    private SimpleArrayMap<Class<? extends ExtraData>, ExtraData> mExtraDataMap;
    private LifecycleRegistry mLifecycleRegistry;
    
    public ComponentActivity() {
        this.mExtraDataMap = new SimpleArrayMap<Class<? extends ExtraData>, ExtraData>();
        this.mLifecycleRegistry = new LifecycleRegistry(this);
    }
    
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    private static boolean shouldSkipDump(@Nullable final String[] array) {
        final boolean b = false;
        final boolean b2 = false;
        final boolean b3 = false;
        boolean b4 = b2;
        if (array != null) {
            b4 = b2;
            if (array.length > 0) {
                final String s = array[0];
                s.hashCode();
                final int hashCode = s.hashCode();
                int n = -1;
                switch (hashCode) {
                    case 1455016274: {
                        if (!s.equals("--autofill")) {
                            break;
                        }
                        n = 4;
                        break;
                    }
                    case 1159329357: {
                        if (!s.equals("--contentcapture")) {
                            break;
                        }
                        n = 3;
                        break;
                    }
                    case 472614934: {
                        if (!s.equals("--list-dumpables")) {
                            break;
                        }
                        n = 2;
                        break;
                    }
                    case 100470631: {
                        if (!s.equals("--dump-dumpable")) {
                            break;
                        }
                        n = 1;
                        break;
                    }
                    case -645125871: {
                        if (!s.equals("--translation")) {
                            break;
                        }
                        n = 0;
                        break;
                    }
                }
                switch (n) {
                    default: {
                        b4 = b2;
                        break;
                    }
                    case 4: {
                        boolean b5 = b3;
                        if (Build$VERSION.SDK_INT >= 26) {
                            b5 = true;
                        }
                        return b5;
                    }
                    case 3: {
                        boolean b6 = b;
                        if (Build$VERSION.SDK_INT >= 29) {
                            b6 = true;
                        }
                        return b6;
                    }
                    case 1:
                    case 2: {
                        return BuildCompat.isAtLeastT();
                    }
                    case 0: {
                        b4 = b2;
                        if (Build$VERSION.SDK_INT >= 31) {
                            b4 = true;
                            break;
                        }
                        break;
                    }
                }
            }
        }
        return b4;
    }
    
    public boolean dispatchKeyEvent(final KeyEvent keyEvent) {
        final View decorView = this.getWindow().getDecorView();
        return (decorView != null && KeyEventDispatcher.dispatchBeforeHierarchy(decorView, keyEvent)) || KeyEventDispatcher.dispatchKeyEvent((KeyEventDispatcher.Component)this, decorView, (Window$Callback)this, keyEvent);
    }
    
    public boolean dispatchKeyShortcutEvent(final KeyEvent keyEvent) {
        final View decorView = this.getWindow().getDecorView();
        return (decorView != null && KeyEventDispatcher.dispatchBeforeHierarchy(decorView, keyEvent)) || super.dispatchKeyShortcutEvent(keyEvent);
    }
    
    @Deprecated
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public <T extends ExtraData> T getExtraData(final Class<T> clazz) {
        return (T)this.mExtraDataMap.get(clazz);
    }
    
    @NonNull
    public Lifecycle getLifecycle() {
        return this.mLifecycleRegistry;
    }
    
    @SuppressLint({ "RestrictedApi" })
    protected void onCreate(@Nullable final Bundle bundle) {
        super.onCreate(bundle);
        ReportFragment.injectIfNeededIn(this);
    }
    
    @CallSuper
    protected void onSaveInstanceState(@NonNull final Bundle bundle) {
        this.mLifecycleRegistry.markState(Lifecycle.State.CREATED);
        super.onSaveInstanceState(bundle);
    }
    
    @Deprecated
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void putExtraData(final ExtraData extraData) {
        this.mExtraDataMap.put(extraData.getClass(), extraData);
    }
    
    protected final boolean shouldDumpInternalState(@Nullable final String[] array) {
        return shouldSkipDump(array) ^ true;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public boolean superDispatchKeyEvent(@NonNull final KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent);
    }
    
    @Deprecated
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static class ExtraData
    {
    }
}

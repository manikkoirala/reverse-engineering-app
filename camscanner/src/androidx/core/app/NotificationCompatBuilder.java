// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.os.BaseBundle;
import java.util.Collection;
import androidx.collection.ArraySet;
import androidx.annotation.Nullable;
import androidx.core.graphics.drawable.IconCompat;
import android.app.Notification$Action$Builder;
import androidx.core.content.LocusIdCompat;
import android.graphics.drawable.Icon;
import java.util.Iterator;
import android.app.Notification;
import android.net.Uri;
import android.text.TextUtils;
import android.os.Build$VERSION;
import java.util.ArrayList;
import android.content.Context;
import android.app.Notification$Builder;
import android.widget.RemoteViews;
import android.os.Bundle;
import java.util.List;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
class NotificationCompatBuilder implements NotificationBuilderWithBuilderAccessor
{
    private final List<Bundle> mActionExtrasList;
    private RemoteViews mBigContentView;
    private final Notification$Builder mBuilder;
    private final NotificationCompat.Builder mBuilderCompat;
    private RemoteViews mContentView;
    private final Context mContext;
    private final Bundle mExtras;
    private int mGroupAlertBehavior;
    private RemoteViews mHeadsUpContentView;
    
    NotificationCompatBuilder(final NotificationCompat.Builder mBuilderCompat) {
        this.mActionExtrasList = new ArrayList<Bundle>();
        this.mExtras = new Bundle();
        this.mBuilderCompat = mBuilderCompat;
        this.mContext = mBuilderCompat.mContext;
        if (Build$VERSION.SDK_INT >= 26) {
            this.mBuilder = new Notification$Builder(mBuilderCompat.mContext, mBuilderCompat.mChannelId);
        }
        else {
            this.mBuilder = new Notification$Builder(mBuilderCompat.mContext);
        }
        final Notification mNotification = mBuilderCompat.mNotification;
        this.mBuilder.setWhen(mNotification.when).setSmallIcon(mNotification.icon, mNotification.iconLevel).setContent(mNotification.contentView).setTicker(mNotification.tickerText, mBuilderCompat.mTickerView).setVibrate(mNotification.vibrate).setLights(mNotification.ledARGB, mNotification.ledOnMS, mNotification.ledOffMS).setOngoing((mNotification.flags & 0x2) != 0x0).setOnlyAlertOnce((mNotification.flags & 0x8) != 0x0).setAutoCancel((mNotification.flags & 0x10) != 0x0).setDefaults(mNotification.defaults).setContentTitle(mBuilderCompat.mContentTitle).setContentText(mBuilderCompat.mContentText).setContentInfo(mBuilderCompat.mContentInfo).setContentIntent(mBuilderCompat.mContentIntent).setDeleteIntent(mNotification.deleteIntent).setFullScreenIntent(mBuilderCompat.mFullScreenIntent, (mNotification.flags & 0x80) != 0x0).setLargeIcon(mBuilderCompat.mLargeIcon).setNumber(mBuilderCompat.mNumber).setProgress(mBuilderCompat.mProgressMax, mBuilderCompat.mProgress, mBuilderCompat.mProgressIndeterminate);
        this.mBuilder.setSubText(mBuilderCompat.mSubText).setUsesChronometer(mBuilderCompat.mUseChronometer).setPriority(mBuilderCompat.mPriority);
        final Iterator<NotificationCompat.Action> iterator = mBuilderCompat.mActions.iterator();
        while (iterator.hasNext()) {
            this.addAction(iterator.next());
        }
        final Bundle mExtras = mBuilderCompat.mExtras;
        if (mExtras != null) {
            this.mExtras.putAll(mExtras);
        }
        final int sdk_INT = Build$VERSION.SDK_INT;
        this.mContentView = mBuilderCompat.mContentView;
        this.mBigContentView = mBuilderCompat.mBigContentView;
        this.mBuilder.setShowWhen(mBuilderCompat.mShowWhen);
        this.mBuilder.setLocalOnly(mBuilderCompat.mLocalOnly).setGroup(mBuilderCompat.mGroupKey).setGroupSummary(mBuilderCompat.mGroupSummary).setSortKey(mBuilderCompat.mSortKey);
        this.mGroupAlertBehavior = mBuilderCompat.mGroupAlertBehavior;
        this.mBuilder.setCategory(mBuilderCompat.mCategory).setColor(mBuilderCompat.mColor).setVisibility(mBuilderCompat.mVisibility).setPublicVersion(mBuilderCompat.mPublicVersion).setSound(mNotification.sound, mNotification.audioAttributes);
        List<String> list;
        if (sdk_INT < 28) {
            list = combineLists(getPeople(mBuilderCompat.mPersonList), mBuilderCompat.mPeople);
        }
        else {
            list = mBuilderCompat.mPeople;
        }
        if (list != null && !list.isEmpty()) {
            final Iterator iterator2 = list.iterator();
            while (iterator2.hasNext()) {
                this.mBuilder.addPerson((String)iterator2.next());
            }
        }
        this.mHeadsUpContentView = mBuilderCompat.mHeadsUpContentView;
        if (mBuilderCompat.mInvisibleActions.size() > 0) {
            Bundle bundle;
            if ((bundle = mBuilderCompat.getExtras().getBundle("android.car.EXTENSIONS")) == null) {
                bundle = new Bundle();
            }
            final Bundle bundle2 = new Bundle(bundle);
            final Bundle bundle3 = new Bundle();
            for (int i = 0; i < mBuilderCompat.mInvisibleActions.size(); ++i) {
                bundle3.putBundle(Integer.toString(i), NotificationCompatJellybean.getBundleForAction(mBuilderCompat.mInvisibleActions.get(i)));
            }
            bundle.putBundle("invisible_actions", bundle3);
            bundle2.putBundle("invisible_actions", bundle3);
            mBuilderCompat.getExtras().putBundle("android.car.EXTENSIONS", bundle);
            this.mExtras.putBundle("android.car.EXTENSIONS", bundle2);
        }
        final int sdk_INT2 = Build$VERSION.SDK_INT;
        if (sdk_INT2 >= 23) {
            final Icon mSmallIcon = mBuilderCompat.mSmallIcon;
            if (mSmallIcon != null) {
                ooo8o\u3007o\u3007.\u3007080(this.mBuilder, mSmallIcon);
            }
        }
        if (sdk_INT2 >= 24) {
            \u3007\u30070o8O\u3007\u3007.\u3007080(this.mBuilder.setExtras(mBuilderCompat.mExtras), mBuilderCompat.mRemoteInputHistory);
            final RemoteViews mContentView = mBuilderCompat.mContentView;
            if (mContentView != null) {
                O\u30078oOo8O.\u3007080(this.mBuilder, mContentView);
            }
            final RemoteViews mBigContentView = mBuilderCompat.mBigContentView;
            if (mBigContentView != null) {
                \u30078o\u3007\u30078080.\u3007080(this.mBuilder, mBigContentView);
            }
            final RemoteViews mHeadsUpContentView = mBuilderCompat.mHeadsUpContentView;
            if (mHeadsUpContentView != null) {
                \u3007oo.\u3007080(this.mBuilder, mHeadsUpContentView);
            }
        }
        if (sdk_INT2 >= 26) {
            O0\u3007oo.\u3007080(O8oOo80.\u3007080(Oo0oOo\u30070.\u3007080(o8o\u3007\u30070O.\u3007080(\u30078o8O\u3007O.\u3007080(this.mBuilder, mBuilderCompat.mBadgeIcon), mBuilderCompat.mSettingsText), mBuilderCompat.mShortcutId), mBuilderCompat.mTimeout), mBuilderCompat.mGroupAlertBehavior);
            if (mBuilderCompat.mColorizedSet) {
                oO8o.\u3007080(this.mBuilder, mBuilderCompat.mColorized);
            }
            if (!TextUtils.isEmpty((CharSequence)mBuilderCompat.mChannelId)) {
                this.mBuilder.setSound((Uri)null).setDefaults(0).setLights(0, 0, 0).setVibrate((long[])null);
            }
        }
        if (sdk_INT2 >= 28) {
            final Iterator<Person> iterator3 = mBuilderCompat.mPersonList.iterator();
            while (iterator3.hasNext()) {
                \u3007\u30070\u30070o8.\u3007080(this.mBuilder, iterator3.next().toAndroidPerson());
            }
        }
        final int sdk_INT3 = Build$VERSION.SDK_INT;
        if (sdk_INT3 >= 29) {
            \u3007008\u3007oo.\u3007080(this.mBuilder, mBuilderCompat.mAllowSystemGeneratedContextualActions);
            O\u3007\u3007.\u3007080(this.mBuilder, NotificationCompat.BubbleMetadata.toPlatform(mBuilderCompat.mBubbleMetadata));
            final LocusIdCompat mLocusId = mBuilderCompat.mLocusId;
            if (mLocusId != null) {
                oo0O\u30070\u3007\u3007\u3007.\u3007080(this.mBuilder, mLocusId.toLocusId());
            }
        }
        if (sdk_INT3 >= 31) {
            final int mFgsDeferBehavior = mBuilderCompat.mFgsDeferBehavior;
            if (mFgsDeferBehavior != 0) {
                oO8008O.\u3007080(this.mBuilder, mFgsDeferBehavior);
            }
        }
        if (mBuilderCompat.mSilent) {
            if (this.mBuilderCompat.mGroupSummary) {
                this.mGroupAlertBehavior = 2;
            }
            else {
                this.mGroupAlertBehavior = 1;
            }
            this.mBuilder.setVibrate((long[])null);
            this.mBuilder.setSound((Uri)null);
            final int n = mNotification.defaults & 0xFFFFFFFE & 0xFFFFFFFD;
            mNotification.defaults = n;
            this.mBuilder.setDefaults(n);
            if (sdk_INT3 >= 26) {
                if (TextUtils.isEmpty((CharSequence)this.mBuilderCompat.mGroupKey)) {
                    this.mBuilder.setGroup("silent");
                }
                O0\u3007oo.\u3007080(this.mBuilder, this.mGroupAlertBehavior);
            }
        }
    }
    
    private void addAction(final NotificationCompat.Action action) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        final IconCompat iconCompat = action.getIconCompat();
        final int n = 0;
        Notification$Action$Builder notification$Action$Builder;
        if (sdk_INT >= 23) {
            Icon icon;
            if (iconCompat != null) {
                icon = iconCompat.toIcon();
            }
            else {
                icon = null;
            }
            notification$Action$Builder = new Notification$Action$Builder(icon, action.getTitle(), action.getActionIntent());
        }
        else {
            int resId;
            if (iconCompat != null) {
                resId = iconCompat.getResId();
            }
            else {
                resId = 0;
            }
            notification$Action$Builder = new Notification$Action$Builder(resId, action.getTitle(), action.getActionIntent());
        }
        if (action.getRemoteInputs() != null) {
            final android.app.RemoteInput[] fromCompat = RemoteInput.fromCompat(action.getRemoteInputs());
            for (int length = fromCompat.length, i = n; i < length; ++i) {
                notification$Action$Builder.addRemoteInput(fromCompat[i]);
            }
        }
        Bundle bundle;
        if (action.getExtras() != null) {
            bundle = new Bundle(action.getExtras());
        }
        else {
            bundle = new Bundle();
        }
        bundle.putBoolean("android.support.allowGeneratedReplies", action.getAllowGeneratedReplies());
        final int sdk_INT2 = Build$VERSION.SDK_INT;
        if (sdk_INT2 >= 24) {
            \u3007Oo\u3007o8.\u3007080(notification$Action$Builder, action.getAllowGeneratedReplies());
        }
        ((BaseBundle)bundle).putInt("android.support.action.semanticAction", action.getSemanticAction());
        if (sdk_INT2 >= 28) {
            \u3007o8OO0.\u3007080(notification$Action$Builder, action.getSemanticAction());
        }
        if (sdk_INT2 >= 29) {
            o\u3007\u30070\u300788.\u3007080(notification$Action$Builder, action.isContextual());
        }
        if (sdk_INT2 >= 31) {
            OoO\u3007.\u3007080(notification$Action$Builder, action.isAuthenticationRequired());
        }
        bundle.putBoolean("android.support.action.showsUserInterface", action.getShowsUserInterface());
        notification$Action$Builder.addExtras(bundle);
        this.mBuilder.addAction(notification$Action$Builder.build());
    }
    
    @Nullable
    private static List<String> combineLists(@Nullable final List<String> list, @Nullable final List<String> list2) {
        if (list == null) {
            return list2;
        }
        if (list2 == null) {
            return list;
        }
        final ArraySet c = new ArraySet(list.size() + list2.size());
        c.addAll(list);
        c.addAll(list2);
        return new ArrayList<String>(c);
    }
    
    @Nullable
    private static List<String> getPeople(@Nullable final List<Person> list) {
        if (list == null) {
            return null;
        }
        final ArrayList list2 = new ArrayList(list.size());
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            list2.add(((Person)iterator.next()).resolveToLegacyUri());
        }
        return list2;
    }
    
    private void removeSoundAndVibration(final Notification notification) {
        notification.sound = null;
        notification.vibrate = null;
        notification.defaults = (notification.defaults & 0xFFFFFFFE & 0xFFFFFFFD);
    }
    
    public Notification build() {
        final NotificationCompat.Style mStyle = this.mBuilderCompat.mStyle;
        if (mStyle != null) {
            mStyle.apply(this);
        }
        RemoteViews contentView;
        if (mStyle != null) {
            contentView = mStyle.makeContentView(this);
        }
        else {
            contentView = null;
        }
        final Notification buildInternal = this.buildInternal();
        if (contentView != null) {
            buildInternal.contentView = contentView;
        }
        else {
            final RemoteViews mContentView = this.mBuilderCompat.mContentView;
            if (mContentView != null) {
                buildInternal.contentView = mContentView;
            }
        }
        if (mStyle != null) {
            final RemoteViews bigContentView = mStyle.makeBigContentView(this);
            if (bigContentView != null) {
                buildInternal.bigContentView = bigContentView;
            }
        }
        if (mStyle != null) {
            final RemoteViews headsUpContentView = this.mBuilderCompat.mStyle.makeHeadsUpContentView(this);
            if (headsUpContentView != null) {
                buildInternal.headsUpContentView = headsUpContentView;
            }
        }
        if (mStyle != null) {
            final Bundle extras = NotificationCompat.getExtras(buildInternal);
            if (extras != null) {
                mStyle.addCompatExtras(extras);
            }
        }
        return buildInternal;
    }
    
    protected Notification buildInternal() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 26) {
            return this.mBuilder.build();
        }
        if (sdk_INT >= 24) {
            final Notification build = this.mBuilder.build();
            if (this.mGroupAlertBehavior != 0) {
                if (build.getGroup() != null && (build.flags & 0x200) != 0x0 && this.mGroupAlertBehavior == 2) {
                    this.removeSoundAndVibration(build);
                }
                if (build.getGroup() != null && (build.flags & 0x200) == 0x0 && this.mGroupAlertBehavior == 1) {
                    this.removeSoundAndVibration(build);
                }
            }
            return build;
        }
        this.mBuilder.setExtras(this.mExtras);
        final Notification build2 = this.mBuilder.build();
        final RemoteViews mContentView = this.mContentView;
        if (mContentView != null) {
            build2.contentView = mContentView;
        }
        final RemoteViews mBigContentView = this.mBigContentView;
        if (mBigContentView != null) {
            build2.bigContentView = mBigContentView;
        }
        final RemoteViews mHeadsUpContentView = this.mHeadsUpContentView;
        if (mHeadsUpContentView != null) {
            build2.headsUpContentView = mHeadsUpContentView;
        }
        if (this.mGroupAlertBehavior != 0) {
            if (build2.getGroup() != null && (build2.flags & 0x200) != 0x0 && this.mGroupAlertBehavior == 2) {
                this.removeSoundAndVibration(build2);
            }
            if (build2.getGroup() != null && (build2.flags & 0x200) == 0x0 && this.mGroupAlertBehavior == 1) {
                this.removeSoundAndVibration(build2);
            }
        }
        return build2;
    }
    
    @Override
    public Notification$Builder getBuilder() {
        return this.mBuilder;
    }
    
    Context getContext() {
        return this.mContext;
    }
}

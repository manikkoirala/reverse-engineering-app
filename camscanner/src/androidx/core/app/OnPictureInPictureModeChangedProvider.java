// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import androidx.annotation.NonNull;
import androidx.core.util.Consumer;

public interface OnPictureInPictureModeChangedProvider
{
    void addOnPictureInPictureModeChangedListener(@NonNull final Consumer<PictureInPictureModeChangedInfo> p0);
    
    void removeOnPictureInPictureModeChangedListener(@NonNull final Consumer<PictureInPictureModeChangedInfo> p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.os.Build$VERSION;
import android.view.View;
import androidx.annotation.NonNull;
import android.app.Dialog;

public class DialogCompat
{
    private DialogCompat() {
    }
    
    @NonNull
    public static View requireViewById(@NonNull final Dialog dialog, final int n) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.requireViewById(dialog, n);
        }
        final View viewById = dialog.findViewById(n);
        if (viewById != null) {
            return viewById;
        }
        throw new IllegalArgumentException("ID does not reference a View inside this Dialog");
    }
    
    @RequiresApi(28)
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        @DoNotInline
        static <T> T requireViewById(final Dialog dialog, final int n) {
            return (T)oo88o8O.\u3007080(dialog, n);
        }
    }
}

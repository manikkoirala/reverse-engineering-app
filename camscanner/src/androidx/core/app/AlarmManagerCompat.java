// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.app.AlarmManager$AlarmClockInfo;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.os.Build$VERSION;
import android.annotation.SuppressLint;
import android.app.PendingIntent;
import androidx.annotation.NonNull;
import android.app.AlarmManager;

public final class AlarmManagerCompat
{
    private AlarmManagerCompat() {
    }
    
    @SuppressLint({ "MissingPermission" })
    public static void setAlarmClock(@NonNull final AlarmManager alarmManager, final long n, @NonNull final PendingIntent pendingIntent, @NonNull final PendingIntent pendingIntent2) {
        Api21Impl.setAlarmClock(alarmManager, Api21Impl.createAlarmClockInfo(n, pendingIntent), pendingIntent2);
    }
    
    public static void setAndAllowWhileIdle(@NonNull final AlarmManager alarmManager, final int n, final long n2, @NonNull final PendingIntent pendingIntent) {
        if (Build$VERSION.SDK_INT >= 23) {
            Api23Impl.setAndAllowWhileIdle(alarmManager, n, n2, pendingIntent);
        }
        else {
            alarmManager.set(n, n2, pendingIntent);
        }
    }
    
    public static void setExact(@NonNull final AlarmManager alarmManager, final int n, final long n2, @NonNull final PendingIntent pendingIntent) {
        Api19Impl.setExact(alarmManager, n, n2, pendingIntent);
    }
    
    public static void setExactAndAllowWhileIdle(@NonNull final AlarmManager alarmManager, final int n, final long n2, @NonNull final PendingIntent pendingIntent) {
        if (Build$VERSION.SDK_INT >= 23) {
            Api23Impl.setExactAndAllowWhileIdle(alarmManager, n, n2, pendingIntent);
        }
        else {
            setExact(alarmManager, n, n2, pendingIntent);
        }
    }
    
    @RequiresApi(19)
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        @DoNotInline
        static void setExact(final AlarmManager alarmManager, final int n, final long n2, final PendingIntent pendingIntent) {
            alarmManager.setExact(n, n2, pendingIntent);
        }
    }
    
    @RequiresApi(21)
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        @DoNotInline
        static AlarmManager$AlarmClockInfo createAlarmClockInfo(final long n, final PendingIntent pendingIntent) {
            return new AlarmManager$AlarmClockInfo(n, pendingIntent);
        }
        
        @DoNotInline
        static void setAlarmClock(final AlarmManager alarmManager, final Object o, final PendingIntent pendingIntent) {
            alarmManager.setAlarmClock((AlarmManager$AlarmClockInfo)o, pendingIntent);
        }
    }
    
    @RequiresApi(23)
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        @DoNotInline
        static void setAndAllowWhileIdle(final AlarmManager alarmManager, final int n, final long n2, final PendingIntent pendingIntent) {
            \u3007O00.\u3007080(alarmManager, n, n2, pendingIntent);
        }
        
        @DoNotInline
        static void setExactAndAllowWhileIdle(final AlarmManager alarmManager, final int n, final long n2, final PendingIntent pendingIntent) {
            \u3007\u30078O0\u30078.\u3007080(alarmManager, n, n2, pendingIntent);
        }
    }
}

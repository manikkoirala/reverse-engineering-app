// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import androidx.annotation.Nullable;
import java.util.Iterator;
import java.util.ArrayList;
import androidx.core.util.Preconditions;
import android.os.Build$VERSION;
import android.app.NotificationChannel;
import androidx.annotation.RequiresApi;
import java.util.Collections;
import androidx.annotation.NonNull;
import android.app.NotificationChannelGroup;
import java.util.List;

public class NotificationChannelGroupCompat
{
    private boolean mBlocked;
    private List<NotificationChannelCompat> mChannels;
    String mDescription;
    final String mId;
    CharSequence mName;
    
    @RequiresApi(28)
    NotificationChannelGroupCompat(@NonNull final NotificationChannelGroup notificationChannelGroup) {
        this(notificationChannelGroup, Collections.emptyList());
    }
    
    @RequiresApi(26)
    NotificationChannelGroupCompat(@NonNull final NotificationChannelGroup notificationChannelGroup, @NonNull final List<NotificationChannel> list) {
        this(ooo\u30078oO.\u3007080(notificationChannelGroup));
        this.mName = O0o\u3007\u3007Oo.\u3007080(notificationChannelGroup);
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 28) {
            this.mDescription = OO8oO0o\u3007.\u3007080(notificationChannelGroup);
        }
        if (sdk_INT >= 28) {
            this.mBlocked = o0O0.\u3007080(notificationChannelGroup);
            this.mChannels = this.getChannelsCompat(\u30070.\u3007080(notificationChannelGroup));
        }
        else {
            this.mChannels = this.getChannelsCompat(list);
        }
    }
    
    NotificationChannelGroupCompat(@NonNull final String s) {
        this.mChannels = Collections.emptyList();
        this.mId = Preconditions.checkNotNull(s);
    }
    
    @RequiresApi(26)
    private List<NotificationChannelCompat> getChannelsCompat(final List<NotificationChannel> list) {
        final ArrayList list2 = new ArrayList();
        for (final NotificationChannel notificationChannel : list) {
            if (this.mId.equals(oo\u3007.\u3007080(notificationChannel))) {
                list2.add(new NotificationChannelCompat(notificationChannel));
            }
        }
        return list2;
    }
    
    @NonNull
    public List<NotificationChannelCompat> getChannels() {
        return this.mChannels;
    }
    
    @Nullable
    public String getDescription() {
        return this.mDescription;
    }
    
    @NonNull
    public String getId() {
        return this.mId;
    }
    
    @Nullable
    public CharSequence getName() {
        return this.mName;
    }
    
    NotificationChannelGroup getNotificationChannelGroup() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT < 26) {
            return null;
        }
        final NotificationChannelGroup notificationChannelGroup = new NotificationChannelGroup(this.mId, this.mName);
        if (sdk_INT >= 28) {
            o88\u3007OO08\u3007.\u3007080(notificationChannelGroup, this.mDescription);
        }
        return notificationChannelGroup;
    }
    
    public boolean isBlocked() {
        return this.mBlocked;
    }
    
    @NonNull
    public Builder toBuilder() {
        return new Builder(this.mId).setName(this.mName).setDescription(this.mDescription);
    }
    
    public static class Builder
    {
        final NotificationChannelGroupCompat mGroup;
        
        public Builder(@NonNull final String s) {
            this.mGroup = new NotificationChannelGroupCompat(s);
        }
        
        @NonNull
        public NotificationChannelGroupCompat build() {
            return this.mGroup;
        }
        
        @NonNull
        public Builder setDescription(@Nullable final String mDescription) {
            this.mGroup.mDescription = mDescription;
            return this;
        }
        
        @NonNull
        public Builder setName(@Nullable final CharSequence mName) {
            this.mGroup.mName = mName;
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import androidx.annotation.NonNull;
import androidx.core.util.Consumer;

public interface OnMultiWindowModeChangedProvider
{
    void addOnMultiWindowModeChangedListener(@NonNull final Consumer<MultiWindowModeChangedInfo> p0);
    
    void removeOnMultiWindowModeChangedListener(@NonNull final Consumer<MultiWindowModeChangedInfo> p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentProvider;
import android.app.Application;
import java.lang.reflect.InvocationTargetException;
import android.app.Activity;
import androidx.annotation.Nullable;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(28)
public class AppComponentFactory extends android.app.AppComponentFactory
{
    @NonNull
    public final Activity instantiateActivity(@NonNull final ClassLoader classLoader, @NonNull final String s, @Nullable final Intent intent) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        return CoreComponentFactory.checkCompatWrapper(this.instantiateActivityCompat(classLoader, s, intent));
    }
    
    @NonNull
    public Activity instantiateActivityCompat(@NonNull ClassLoader ex, @NonNull final String name, @Nullable final Intent intent) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        try {
            ex = (NoSuchMethodException)Class.forName(name, false, (ClassLoader)ex).asSubclass(Activity.class).getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
            return (Activity)ex;
        }
        catch (final NoSuchMethodException ex) {}
        catch (final InvocationTargetException ex2) {}
        throw new RuntimeException("Couldn't call constructor", ex);
    }
    
    @NonNull
    public final Application instantiateApplication(@NonNull final ClassLoader classLoader, @NonNull final String s) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        return CoreComponentFactory.checkCompatWrapper(this.instantiateApplicationCompat(classLoader, s));
    }
    
    @NonNull
    public Application instantiateApplicationCompat(@NonNull ClassLoader ex, @NonNull final String name) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        try {
            ex = (NoSuchMethodException)Class.forName(name, false, (ClassLoader)ex).asSubclass(Application.class).getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
            return (Application)ex;
        }
        catch (final NoSuchMethodException ex) {}
        catch (final InvocationTargetException ex2) {}
        throw new RuntimeException("Couldn't call constructor", ex);
    }
    
    @NonNull
    public final ContentProvider instantiateProvider(@NonNull final ClassLoader classLoader, @NonNull final String s) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        return CoreComponentFactory.checkCompatWrapper(this.instantiateProviderCompat(classLoader, s));
    }
    
    @NonNull
    public ContentProvider instantiateProviderCompat(@NonNull ClassLoader ex, @NonNull final String name) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        try {
            ex = (NoSuchMethodException)Class.forName(name, false, (ClassLoader)ex).asSubclass(ContentProvider.class).getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
            return (ContentProvider)ex;
        }
        catch (final NoSuchMethodException ex) {}
        catch (final InvocationTargetException ex2) {}
        throw new RuntimeException("Couldn't call constructor", ex);
    }
    
    @NonNull
    public final BroadcastReceiver instantiateReceiver(@NonNull final ClassLoader classLoader, @NonNull final String s, @Nullable final Intent intent) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        return CoreComponentFactory.checkCompatWrapper(this.instantiateReceiverCompat(classLoader, s, intent));
    }
    
    @NonNull
    public BroadcastReceiver instantiateReceiverCompat(@NonNull ClassLoader ex, @NonNull final String name, @Nullable final Intent intent) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        try {
            ex = (NoSuchMethodException)Class.forName(name, false, (ClassLoader)ex).asSubclass(BroadcastReceiver.class).getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
            return (BroadcastReceiver)ex;
        }
        catch (final NoSuchMethodException ex) {}
        catch (final InvocationTargetException ex2) {}
        throw new RuntimeException("Couldn't call constructor", ex);
    }
    
    @NonNull
    public final Service instantiateService(@NonNull final ClassLoader classLoader, @NonNull final String s, @Nullable final Intent intent) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        return CoreComponentFactory.checkCompatWrapper(this.instantiateServiceCompat(classLoader, s, intent));
    }
    
    @NonNull
    public Service instantiateServiceCompat(@NonNull ClassLoader ex, @NonNull final String name, @Nullable final Intent intent) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        try {
            ex = (NoSuchMethodException)Class.forName(name, false, (ClassLoader)ex).asSubclass(Service.class).getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
            return (Service)ex;
        }
        catch (final NoSuchMethodException ex) {}
        catch (final InvocationTargetException ex2) {}
        throw new RuntimeException("Couldn't call constructor", ex);
    }
}

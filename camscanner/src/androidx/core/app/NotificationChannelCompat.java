// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import androidx.annotation.Nullable;
import android.app.Notification;
import androidx.core.util.Preconditions;
import android.provider.Settings$System;
import androidx.annotation.RequiresApi;
import android.os.Build$VERSION;
import androidx.browser.trusted.Oo08;
import android.app.NotificationChannel;
import android.net.Uri;
import androidx.annotation.NonNull;
import android.media.AudioAttributes;

public class NotificationChannelCompat
{
    public static final String DEFAULT_CHANNEL_ID = "miscellaneous";
    private static final int DEFAULT_LIGHT_COLOR = 0;
    private static final boolean DEFAULT_SHOW_BADGE = true;
    AudioAttributes mAudioAttributes;
    private boolean mBypassDnd;
    private boolean mCanBubble;
    String mConversationId;
    String mDescription;
    String mGroupId;
    @NonNull
    final String mId;
    int mImportance;
    private boolean mImportantConversation;
    int mLightColor;
    boolean mLights;
    private int mLockscreenVisibility;
    CharSequence mName;
    String mParentId;
    boolean mShowBadge;
    Uri mSound;
    boolean mVibrationEnabled;
    long[] mVibrationPattern;
    
    @RequiresApi(26)
    NotificationChannelCompat(@NonNull final NotificationChannel notificationChannel) {
        this(o\u3007\u30070\u3007.\u3007080(notificationChannel), Oo08.\u3007080(notificationChannel));
        this.mName = \u3007O\u300780o08O.\u3007080(notificationChannel);
        this.mDescription = OOO\u3007O0.\u3007080(notificationChannel);
        this.mGroupId = oo\u3007.\u3007080(notificationChannel);
        this.mShowBadge = O8\u3007o.\u3007080(notificationChannel);
        this.mSound = \u300700\u30078.\u3007080(notificationChannel);
        this.mAudioAttributes = \u3007o.\u3007080(notificationChannel);
        this.mLights = o0ooO.\u3007080(notificationChannel);
        this.mLightColor = o\u30078.\u3007080(notificationChannel);
        this.mVibrationEnabled = \u3007\u30070o.\u3007080(notificationChannel);
        this.mVibrationPattern = o8oO\u3007.\u3007080(notificationChannel);
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 30) {
            this.mParentId = o\u30078oOO88.\u3007080(notificationChannel);
            this.mConversationId = o\u3007O.\u3007080(notificationChannel);
        }
        this.mBypassDnd = oO00OOO.\u3007080(notificationChannel);
        this.mLockscreenVisibility = O000.\u3007080(notificationChannel);
        if (sdk_INT >= 29) {
            this.mCanBubble = \u300780.\u3007080(notificationChannel);
        }
        if (sdk_INT >= 30) {
            this.mImportantConversation = Ooo.\u3007080(notificationChannel);
        }
    }
    
    NotificationChannelCompat(@NonNull final String s, final int mImportance) {
        this.mShowBadge = true;
        this.mSound = Settings$System.DEFAULT_NOTIFICATION_URI;
        this.mLightColor = 0;
        this.mId = Preconditions.checkNotNull(s);
        this.mImportance = mImportance;
        this.mAudioAttributes = Notification.AUDIO_ATTRIBUTES_DEFAULT;
    }
    
    public boolean canBubble() {
        return this.mCanBubble;
    }
    
    public boolean canBypassDnd() {
        return this.mBypassDnd;
    }
    
    public boolean canShowBadge() {
        return this.mShowBadge;
    }
    
    @Nullable
    public AudioAttributes getAudioAttributes() {
        return this.mAudioAttributes;
    }
    
    @Nullable
    public String getConversationId() {
        return this.mConversationId;
    }
    
    @Nullable
    public String getDescription() {
        return this.mDescription;
    }
    
    @Nullable
    public String getGroup() {
        return this.mGroupId;
    }
    
    @NonNull
    public String getId() {
        return this.mId;
    }
    
    public int getImportance() {
        return this.mImportance;
    }
    
    public int getLightColor() {
        return this.mLightColor;
    }
    
    public int getLockscreenVisibility() {
        return this.mLockscreenVisibility;
    }
    
    @Nullable
    public CharSequence getName() {
        return this.mName;
    }
    
    NotificationChannel getNotificationChannel() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT < 26) {
            return null;
        }
        final NotificationChannel notificationChannel = new NotificationChannel(this.mId, this.mName, this.mImportance);
        Oo8Oo00oo.\u3007080(notificationChannel, this.mDescription);
        \u3007\u3007\u30070\u3007\u30070.\u3007080(notificationChannel, this.mGroupId);
        o\u30070OOo\u30070.\u3007080(notificationChannel, this.mShowBadge);
        \u300708O8o\u30070.\u3007080(notificationChannel, this.mSound, this.mAudioAttributes);
        oO.\u3007080(notificationChannel, this.mLights);
        \u30078.\u3007080(notificationChannel, this.mLightColor);
        O08000.\u3007080(notificationChannel, this.mVibrationPattern);
        \u30078\u30070\u3007o\u3007O.\u3007080(notificationChannel, this.mVibrationEnabled);
        if (sdk_INT >= 30) {
            final String mParentId = this.mParentId;
            if (mParentId != null) {
                final String mConversationId = this.mConversationId;
                if (mConversationId != null) {
                    O\u3007O\u3007oO.\u3007080(notificationChannel, mParentId, mConversationId);
                }
            }
        }
        return notificationChannel;
    }
    
    @Nullable
    public String getParentChannelId() {
        return this.mParentId;
    }
    
    @Nullable
    public Uri getSound() {
        return this.mSound;
    }
    
    @Nullable
    public long[] getVibrationPattern() {
        return this.mVibrationPattern;
    }
    
    public boolean isImportantConversation() {
        return this.mImportantConversation;
    }
    
    public boolean shouldShowLights() {
        return this.mLights;
    }
    
    public boolean shouldVibrate() {
        return this.mVibrationEnabled;
    }
    
    @NonNull
    public Builder toBuilder() {
        return new Builder(this.mId, this.mImportance).setName(this.mName).setDescription(this.mDescription).setGroup(this.mGroupId).setShowBadge(this.mShowBadge).setSound(this.mSound, this.mAudioAttributes).setLightsEnabled(this.mLights).setLightColor(this.mLightColor).setVibrationEnabled(this.mVibrationEnabled).setVibrationPattern(this.mVibrationPattern).setConversationId(this.mParentId, this.mConversationId);
    }
    
    public static class Builder
    {
        private final NotificationChannelCompat mChannel;
        
        public Builder(@NonNull final String s, final int n) {
            this.mChannel = new NotificationChannelCompat(s, n);
        }
        
        @NonNull
        public NotificationChannelCompat build() {
            return this.mChannel;
        }
        
        @NonNull
        public Builder setConversationId(@NonNull final String mParentId, @NonNull final String mConversationId) {
            if (Build$VERSION.SDK_INT >= 30) {
                final NotificationChannelCompat mChannel = this.mChannel;
                mChannel.mParentId = mParentId;
                mChannel.mConversationId = mConversationId;
            }
            return this;
        }
        
        @NonNull
        public Builder setDescription(@Nullable final String mDescription) {
            this.mChannel.mDescription = mDescription;
            return this;
        }
        
        @NonNull
        public Builder setGroup(@Nullable final String mGroupId) {
            this.mChannel.mGroupId = mGroupId;
            return this;
        }
        
        @NonNull
        public Builder setImportance(final int mImportance) {
            this.mChannel.mImportance = mImportance;
            return this;
        }
        
        @NonNull
        public Builder setLightColor(final int mLightColor) {
            this.mChannel.mLightColor = mLightColor;
            return this;
        }
        
        @NonNull
        public Builder setLightsEnabled(final boolean mLights) {
            this.mChannel.mLights = mLights;
            return this;
        }
        
        @NonNull
        public Builder setName(@Nullable final CharSequence mName) {
            this.mChannel.mName = mName;
            return this;
        }
        
        @NonNull
        public Builder setShowBadge(final boolean mShowBadge) {
            this.mChannel.mShowBadge = mShowBadge;
            return this;
        }
        
        @NonNull
        public Builder setSound(@Nullable final Uri mSound, @Nullable final AudioAttributes mAudioAttributes) {
            final NotificationChannelCompat mChannel = this.mChannel;
            mChannel.mSound = mSound;
            mChannel.mAudioAttributes = mAudioAttributes;
            return this;
        }
        
        @NonNull
        public Builder setVibrationEnabled(final boolean mVibrationEnabled) {
            this.mChannel.mVibrationEnabled = mVibrationEnabled;
            return this;
        }
        
        @NonNull
        public Builder setVibrationPattern(@Nullable final long[] mVibrationPattern) {
            final NotificationChannelCompat mChannel = this.mChannel;
            mChannel.mVibrationEnabled = (mVibrationPattern != null && mVibrationPattern.length > 0);
            mChannel.mVibrationPattern = mVibrationPattern;
            return this;
        }
    }
}

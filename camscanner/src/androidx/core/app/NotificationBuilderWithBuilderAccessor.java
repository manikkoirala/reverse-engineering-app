// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.app.Notification$Builder;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public interface NotificationBuilderWithBuilderAccessor
{
    Notification$Builder getBuilder();
}

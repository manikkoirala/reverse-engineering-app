// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.app.LocaleManager;
import android.os.LocaleList;
import androidx.appcompat.app.o\u30070;
import androidx.appcompat.app.Oo08;
import androidx.annotation.DoNotInline;
import java.util.Locale;
import androidx.annotation.OptIn;
import androidx.annotation.AnyThread;
import androidx.core.os.BuildCompat;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import android.content.Context;
import androidx.annotation.VisibleForTesting;
import android.os.Build$VERSION;
import androidx.core.os.LocaleListCompat;
import android.content.res.Configuration;

public final class LocaleManagerCompat
{
    private LocaleManagerCompat() {
    }
    
    @VisibleForTesting
    static LocaleListCompat getConfigurationLocales(final Configuration configuration) {
        if (Build$VERSION.SDK_INT >= 24) {
            return Api24Impl.getLocales(configuration);
        }
        return LocaleListCompat.forLanguageTags(Api21Impl.toLanguageTag(configuration.locale));
    }
    
    @RequiresApi(33)
    private static Object getLocaleManagerForApplication(final Context context) {
        return context.getSystemService("locale");
    }
    
    @AnyThread
    @NonNull
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    public static LocaleListCompat getSystemLocales(@NonNull final Context context) {
        final LocaleListCompat emptyLocaleList = LocaleListCompat.getEmptyLocaleList();
        LocaleListCompat localeListCompat;
        if (BuildCompat.isAtLeastT()) {
            final Object localeManagerForApplication = getLocaleManagerForApplication(context);
            localeListCompat = emptyLocaleList;
            if (localeManagerForApplication != null) {
                localeListCompat = LocaleListCompat.wrap(Api33Impl.localeManagerGetSystemLocales(localeManagerForApplication));
            }
        }
        else {
            localeListCompat = getConfigurationLocales(context.getApplicationContext().getResources().getConfiguration());
        }
        return localeListCompat;
    }
    
    @RequiresApi(21)
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        @DoNotInline
        static String toLanguageTag(final Locale locale) {
            return locale.toLanguageTag();
        }
    }
    
    @RequiresApi(24)
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        @DoNotInline
        static LocaleListCompat getLocales(final Configuration configuration) {
            return LocaleListCompat.forLanguageTags(o\u30070.\u3007080(Oo08.\u3007080(configuration)));
        }
    }
    
    @RequiresApi(33)
    static class Api33Impl
    {
        private Api33Impl() {
        }
        
        @DoNotInline
        static LocaleList localeManagerGetSystemLocales(final Object o) {
            return ((LocaleManager)o).getSystemLocales();
        }
    }
}

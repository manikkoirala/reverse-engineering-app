// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.os.BaseBundle;
import android.graphics.drawable.Icon;
import android.app.Person$Builder;
import androidx.annotation.DoNotInline;
import android.os.PersistableBundle;
import android.os.Bundle;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.graphics.drawable.IconCompat;

public class Person
{
    private static final String ICON_KEY = "icon";
    private static final String IS_BOT_KEY = "isBot";
    private static final String IS_IMPORTANT_KEY = "isImportant";
    private static final String KEY_KEY = "key";
    private static final String NAME_KEY = "name";
    private static final String URI_KEY = "uri";
    @Nullable
    IconCompat mIcon;
    boolean mIsBot;
    boolean mIsImportant;
    @Nullable
    String mKey;
    @Nullable
    CharSequence mName;
    @Nullable
    String mUri;
    
    Person(final Builder builder) {
        this.mName = builder.mName;
        this.mIcon = builder.mIcon;
        this.mUri = builder.mUri;
        this.mKey = builder.mKey;
        this.mIsBot = builder.mIsBot;
        this.mIsImportant = builder.mIsImportant;
    }
    
    @NonNull
    @RequiresApi(28)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static Person fromAndroidPerson(@NonNull final android.app.Person person) {
        return Api28Impl.fromAndroidPerson(person);
    }
    
    @NonNull
    public static Person fromBundle(@NonNull final Bundle bundle) {
        final Bundle bundle2 = bundle.getBundle("icon");
        final Builder setName = new Builder().setName(bundle.getCharSequence("name"));
        IconCompat fromBundle;
        if (bundle2 != null) {
            fromBundle = IconCompat.createFromBundle(bundle2);
        }
        else {
            fromBundle = null;
        }
        return setName.setIcon(fromBundle).setUri(((BaseBundle)bundle).getString("uri")).setKey(((BaseBundle)bundle).getString("key")).setBot(bundle.getBoolean("isBot")).setImportant(bundle.getBoolean("isImportant")).build();
    }
    
    @NonNull
    @RequiresApi(22)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static Person fromPersistableBundle(@NonNull final PersistableBundle persistableBundle) {
        return Api22Impl.fromPersistableBundle(persistableBundle);
    }
    
    @Nullable
    public IconCompat getIcon() {
        return this.mIcon;
    }
    
    @Nullable
    public String getKey() {
        return this.mKey;
    }
    
    @Nullable
    public CharSequence getName() {
        return this.mName;
    }
    
    @Nullable
    public String getUri() {
        return this.mUri;
    }
    
    public boolean isBot() {
        return this.mIsBot;
    }
    
    public boolean isImportant() {
        return this.mIsImportant;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public String resolveToLegacyUri() {
        final String mUri = this.mUri;
        if (mUri != null) {
            return mUri;
        }
        if (this.mName != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("name:");
            sb.append((Object)this.mName);
            return sb.toString();
        }
        return "";
    }
    
    @NonNull
    @RequiresApi(28)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public android.app.Person toAndroidPerson() {
        return Api28Impl.toAndroidPerson(this);
    }
    
    @NonNull
    public Builder toBuilder() {
        return new Builder(this);
    }
    
    @NonNull
    public Bundle toBundle() {
        final Bundle bundle = new Bundle();
        bundle.putCharSequence("name", this.mName);
        final IconCompat mIcon = this.mIcon;
        Bundle bundle2;
        if (mIcon != null) {
            bundle2 = mIcon.toBundle();
        }
        else {
            bundle2 = null;
        }
        bundle.putBundle("icon", bundle2);
        ((BaseBundle)bundle).putString("uri", this.mUri);
        ((BaseBundle)bundle).putString("key", this.mKey);
        bundle.putBoolean("isBot", this.mIsBot);
        bundle.putBoolean("isImportant", this.mIsImportant);
        return bundle;
    }
    
    @NonNull
    @RequiresApi(22)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public PersistableBundle toPersistableBundle() {
        return Api22Impl.toPersistableBundle(this);
    }
    
    @RequiresApi(22)
    static class Api22Impl
    {
        private Api22Impl() {
        }
        
        @DoNotInline
        static Person fromPersistableBundle(final PersistableBundle persistableBundle) {
            return new Builder().setName(((BaseBundle)persistableBundle).getString("name")).setUri(((BaseBundle)persistableBundle).getString("uri")).setKey(((BaseBundle)persistableBundle).getString("key")).setBot(O88o\u3007.\u3007080(persistableBundle, "isBot")).setImportant(O88o\u3007.\u3007080(persistableBundle, "isImportant")).build();
        }
        
        @DoNotInline
        static PersistableBundle toPersistableBundle(final Person person) {
            final PersistableBundle persistableBundle = new PersistableBundle();
            final CharSequence mName = person.mName;
            String string;
            if (mName != null) {
                string = mName.toString();
            }
            else {
                string = null;
            }
            ((BaseBundle)persistableBundle).putString("name", string);
            ((BaseBundle)persistableBundle).putString("uri", person.mUri);
            ((BaseBundle)persistableBundle).putString("key", person.mKey);
            o08O.\u3007080(persistableBundle, "isBot", person.mIsBot);
            o08O.\u3007080(persistableBundle, "isImportant", person.mIsImportant);
            return persistableBundle;
        }
    }
    
    @RequiresApi(28)
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        @DoNotInline
        static Person fromAndroidPerson(final android.app.Person person) {
            final Builder setName = new Builder().setName(\u3007oO8O0\u3007\u3007O.\u3007080(person));
            IconCompat fromIcon;
            if (OO8\u3007.\u3007080(person) != null) {
                fromIcon = IconCompat.createFromIcon(OO8\u3007.\u3007080(person));
            }
            else {
                fromIcon = null;
            }
            return setName.setIcon(fromIcon).setUri(O8OO08o.\u3007080(person)).setKey(\u3007080O0.\u3007080(person)).setBot(\u3007008\u3007o0\u3007\u3007.\u3007080(person)).setImportant(O80\u3007O\u3007080.\u3007080(person)).build();
        }
        
        @DoNotInline
        static android.app.Person toAndroidPerson(final Person person) {
            final Person$Builder \u3007080 = OoOOo8.\u3007080(new Person$Builder(), person.getName());
            Icon icon;
            if (person.getIcon() != null) {
                icon = person.getIcon().toIcon();
            }
            else {
                icon = null;
            }
            return \u3007o8oO.\u3007080(\u3007O8\u3007OO\u3007.\u3007080(\u3007\u3007\u30070880.\u3007080(o\u30070o\u3007\u3007.\u3007080(\u3007\u3007\u3007.\u3007080(O\u3007.\u3007080(\u3007080, icon), person.getUri()), person.getKey()), person.isBot()), person.isImportant()));
        }
    }
    
    public static class Builder
    {
        @Nullable
        IconCompat mIcon;
        boolean mIsBot;
        boolean mIsImportant;
        @Nullable
        String mKey;
        @Nullable
        CharSequence mName;
        @Nullable
        String mUri;
        
        public Builder() {
        }
        
        Builder(final Person person) {
            this.mName = person.mName;
            this.mIcon = person.mIcon;
            this.mUri = person.mUri;
            this.mKey = person.mKey;
            this.mIsBot = person.mIsBot;
            this.mIsImportant = person.mIsImportant;
        }
        
        @NonNull
        public Person build() {
            return new Person(this);
        }
        
        @NonNull
        public Builder setBot(final boolean mIsBot) {
            this.mIsBot = mIsBot;
            return this;
        }
        
        @NonNull
        public Builder setIcon(@Nullable final IconCompat mIcon) {
            this.mIcon = mIcon;
            return this;
        }
        
        @NonNull
        public Builder setImportant(final boolean mIsImportant) {
            this.mIsImportant = mIsImportant;
            return this;
        }
        
        @NonNull
        public Builder setKey(@Nullable final String mKey) {
            this.mKey = mKey;
            return this;
        }
        
        @NonNull
        public Builder setName(@Nullable final CharSequence mName) {
            this.mName = mName;
            return this;
        }
        
        @NonNull
        public Builder setUri(@Nullable final String mUri) {
            this.mUri = mUri;
            return this;
        }
    }
}

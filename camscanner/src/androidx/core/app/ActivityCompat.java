// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import java.util.Map;
import java.util.List;
import android.content.Context;
import android.os.Parcelable;
import android.graphics.RectF;
import android.graphics.Matrix;
import android.content.LocusId;
import android.view.Display;
import android.app.SharedElementCallback$OnSharedElementsReadyListener;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.content.IntentSender$SendIntentException;
import android.content.IntentSender;
import android.os.Bundle;
import androidx.core.content.LocusIdCompat;
import android.view.View;
import androidx.annotation.IdRes;
import androidx.annotation.OptIn;
import android.content.pm.PackageManager;
import android.os.Looper;
import androidx.core.os.BuildCompat;
import java.util.Arrays;
import android.text.TextUtils;
import java.util.HashSet;
import androidx.annotation.IntRange;
import androidx.core.view.DragAndDropPermissionsCompat;
import android.view.DragEvent;
import android.os.Handler;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Build$VERSION;
import android.net.Uri;
import androidx.annotation.RestrictTo;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.app.Activity;
import androidx.core.content.ContextCompat;

public class ActivityCompat extends ContextCompat
{
    private static PermissionCompatDelegate sDelegate;
    
    protected ActivityCompat() {
    }
    
    public static void finishAffinity(@NonNull final Activity activity) {
        Api16Impl.finishAffinity(activity);
    }
    
    public static void finishAfterTransition(@NonNull final Activity activity) {
        Api21Impl.finishAfterTransition(activity);
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static PermissionCompatDelegate getPermissionCompatDelegate() {
        return ActivityCompat.sDelegate;
    }
    
    @Nullable
    public static Uri getReferrer(@NonNull final Activity activity) {
        if (Build$VERSION.SDK_INT >= 22) {
            return Api22Impl.getReferrer(activity);
        }
        final Intent intent = activity.getIntent();
        final Uri uri = (Uri)intent.getParcelableExtra("android.intent.extra.REFERRER");
        if (uri != null) {
            return uri;
        }
        final String stringExtra = intent.getStringExtra("android.intent.extra.REFERRER_NAME");
        if (stringExtra != null) {
            return Uri.parse(stringExtra);
        }
        return null;
    }
    
    @Deprecated
    public static boolean invalidateOptionsMenu(final Activity activity) {
        activity.invalidateOptionsMenu();
        return true;
    }
    
    public static boolean isLaunchedFromBubble(@NonNull final Activity activity) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 31) {
            return Api31Impl.isLaunchedFromBubble(activity);
        }
        final boolean b = true;
        boolean b2 = true;
        if (sdk_INT == 30) {
            if (Api30Impl.getDisplay((ContextWrapper)activity) == null || Api30Impl.getDisplay((ContextWrapper)activity).getDisplayId() == 0) {
                b2 = false;
            }
            return b2;
        }
        return sdk_INT == 29 && activity.getWindowManager().getDefaultDisplay() != null && activity.getWindowManager().getDefaultDisplay().getDisplayId() != 0 && b;
    }
    
    public static void postponeEnterTransition(@NonNull final Activity activity) {
        Api21Impl.postponeEnterTransition(activity);
    }
    
    public static void recreate(@NonNull final Activity activity) {
        if (Build$VERSION.SDK_INT >= 28) {
            activity.recreate();
        }
        else {
            new Handler(((Context)activity).getMainLooper()).post((Runnable)new \u3007080(activity));
        }
    }
    
    @Nullable
    public static DragAndDropPermissionsCompat requestDragAndDropPermissions(@NonNull final Activity activity, @NonNull final DragEvent dragEvent) {
        return DragAndDropPermissionsCompat.request(activity, dragEvent);
    }
    
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    public static void requestPermissions(@NonNull final Activity activity, @NonNull final String[] a, @IntRange(from = 0L) final int n) {
        final PermissionCompatDelegate sDelegate = ActivityCompat.sDelegate;
        if (sDelegate != null && sDelegate.requestPermissions(activity, a, n)) {
            return;
        }
        final HashSet set = new HashSet();
        final int n2 = 0;
        for (int i = 0; i < a.length; ++i) {
            if (TextUtils.isEmpty((CharSequence)a[i])) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Permission request for permissions ");
                sb.append(Arrays.toString(a));
                sb.append(" must not contain null or empty values");
                throw new IllegalArgumentException(sb.toString());
            }
            if (!BuildCompat.isAtLeastT() && TextUtils.equals((CharSequence)a[i], (CharSequence)"android.permission.POST_NOTIFICATIONS")) {
                set.add(i);
            }
        }
        final int size = set.size();
        String[] array;
        if (size > 0) {
            array = new String[a.length - size];
        }
        else {
            array = a;
        }
        if (size > 0) {
            if (size == a.length) {
                return;
            }
            int n3 = 0;
            int n4;
            for (int j = n2; j < a.length; ++j, n3 = n4) {
                n4 = n3;
                if (!set.contains(j)) {
                    array[n3] = a[j];
                    n4 = n3 + 1;
                }
            }
        }
        if (Build$VERSION.SDK_INT >= 23) {
            if (activity instanceof RequestPermissionsRequestCodeValidator) {
                ((RequestPermissionsRequestCodeValidator)activity).validateRequestPermissionsRequestCode(n);
            }
            Api23Impl.requestPermissions(activity, a, n);
        }
        else if (activity instanceof OnRequestPermissionsResultCallback) {
            new Handler(Looper.getMainLooper()).post((Runnable)new Runnable(array, activity, n) {
                final Activity val$activity;
                final String[] val$permissionsArray;
                final int val$requestCode;
                
                @Override
                public void run() {
                    final int[] array = new int[this.val$permissionsArray.length];
                    final PackageManager packageManager = ((Context)this.val$activity).getPackageManager();
                    final String packageName = ((Context)this.val$activity).getPackageName();
                    for (int length = this.val$permissionsArray.length, i = 0; i < length; ++i) {
                        array[i] = packageManager.checkPermission(this.val$permissionsArray[i], packageName);
                    }
                    ((OnRequestPermissionsResultCallback)this.val$activity).onRequestPermissionsResult(this.val$requestCode, this.val$permissionsArray, array);
                }
            });
        }
    }
    
    @NonNull
    public static <T extends View> T requireViewById(@NonNull final Activity activity, @IdRes final int n) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.requireViewById(activity, n);
        }
        final View viewById = activity.findViewById(n);
        if (viewById != null) {
            return (T)viewById;
        }
        throw new IllegalArgumentException("ID does not reference a View inside this Activity");
    }
    
    public static void setEnterSharedElementCallback(@NonNull final Activity activity, @Nullable final SharedElementCallback sharedElementCallback) {
        SharedElementCallback21Impl sharedElementCallback21Impl;
        if (sharedElementCallback != null) {
            sharedElementCallback21Impl = new SharedElementCallback21Impl(sharedElementCallback);
        }
        else {
            sharedElementCallback21Impl = null;
        }
        Api21Impl.setEnterSharedElementCallback(activity, sharedElementCallback21Impl);
    }
    
    public static void setExitSharedElementCallback(@NonNull final Activity activity, @Nullable final SharedElementCallback sharedElementCallback) {
        SharedElementCallback21Impl sharedElementCallback21Impl;
        if (sharedElementCallback != null) {
            sharedElementCallback21Impl = new SharedElementCallback21Impl(sharedElementCallback);
        }
        else {
            sharedElementCallback21Impl = null;
        }
        Api21Impl.setExitSharedElementCallback(activity, sharedElementCallback21Impl);
    }
    
    public static void setLocusContext(@NonNull final Activity activity, @Nullable final LocusIdCompat locusIdCompat, @Nullable final Bundle bundle) {
        if (Build$VERSION.SDK_INT >= 30) {
            Api30Impl.setLocusContext(activity, locusIdCompat, bundle);
        }
    }
    
    public static void setPermissionCompatDelegate(@Nullable final PermissionCompatDelegate sDelegate) {
        ActivityCompat.sDelegate = sDelegate;
    }
    
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    public static boolean shouldShowRequestPermissionRationale(@NonNull final Activity activity, @NonNull final String s) {
        return (BuildCompat.isAtLeastT() || !TextUtils.equals((CharSequence)"android.permission.POST_NOTIFICATIONS", (CharSequence)s)) && Build$VERSION.SDK_INT >= 23 && Api23Impl.shouldShowRequestPermissionRationale(activity, s);
    }
    
    public static void startActivityForResult(@NonNull final Activity activity, @NonNull final Intent intent, final int n, @Nullable final Bundle bundle) {
        Api16Impl.startActivityForResult(activity, intent, n, bundle);
    }
    
    public static void startIntentSenderForResult(@NonNull final Activity activity, @NonNull final IntentSender intentSender, final int n, @Nullable final Intent intent, final int n2, final int n3, final int n4, @Nullable final Bundle bundle) throws IntentSender$SendIntentException {
        Api16Impl.startIntentSenderForResult(activity, intentSender, n, intent, n2, n3, n4, bundle);
    }
    
    public static void startPostponedEnterTransition(@NonNull final Activity activity) {
        Api21Impl.startPostponedEnterTransition(activity);
    }
    
    @RequiresApi(16)
    static class Api16Impl
    {
        private Api16Impl() {
        }
        
        @DoNotInline
        static void finishAffinity(final Activity activity) {
            activity.finishAffinity();
        }
        
        @DoNotInline
        static void startActivityForResult(final Activity activity, final Intent intent, final int n, final Bundle bundle) {
            activity.startActivityForResult(intent, n, bundle);
        }
        
        @DoNotInline
        static void startIntentSenderForResult(final Activity activity, final IntentSender intentSender, final int n, final Intent intent, final int n2, final int n3, final int n4, final Bundle bundle) throws IntentSender$SendIntentException {
            activity.startIntentSenderForResult(intentSender, n, intent, n2, n3, n4, bundle);
        }
    }
    
    @RequiresApi(21)
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        @DoNotInline
        static void finishAfterTransition(final Activity activity) {
            activity.finishAfterTransition();
        }
        
        @DoNotInline
        static void postponeEnterTransition(final Activity activity) {
            activity.postponeEnterTransition();
        }
        
        @DoNotInline
        static void setEnterSharedElementCallback(final Activity activity, final android.app.SharedElementCallback enterSharedElementCallback) {
            activity.setEnterSharedElementCallback(enterSharedElementCallback);
        }
        
        @DoNotInline
        static void setExitSharedElementCallback(final Activity activity, final android.app.SharedElementCallback exitSharedElementCallback) {
            activity.setExitSharedElementCallback(exitSharedElementCallback);
        }
        
        @DoNotInline
        static void startPostponedEnterTransition(final Activity activity) {
            activity.startPostponedEnterTransition();
        }
    }
    
    @RequiresApi(22)
    static class Api22Impl
    {
        private Api22Impl() {
        }
        
        @DoNotInline
        static Uri getReferrer(final Activity activity) {
            return \u3007o00\u3007\u3007Oo.\u3007080(activity);
        }
    }
    
    @RequiresApi(23)
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        @DoNotInline
        static void onSharedElementsReady(final Object o) {
            O8.\u3007080((SharedElementCallback$OnSharedElementsReadyListener)o);
        }
        
        @DoNotInline
        static void requestPermissions(final Activity activity, final String[] array, final int n) {
            \u3007o\u3007.\u3007080(activity, array, n);
        }
        
        @DoNotInline
        static boolean shouldShowRequestPermissionRationale(final Activity activity, final String s) {
            return Oo08.\u3007080(activity, s);
        }
    }
    
    @RequiresApi(28)
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        @DoNotInline
        static <T> T requireViewById(final Activity activity, final int n) {
            return (T)o\u30070.\u3007080(activity, n);
        }
    }
    
    @RequiresApi(30)
    static class Api30Impl
    {
        private Api30Impl() {
        }
        
        @DoNotInline
        static Display getDisplay(final ContextWrapper contextWrapper) {
            return oO80.\u3007080(contextWrapper);
        }
        
        @DoNotInline
        static void setLocusContext(@NonNull final Activity activity, @Nullable final LocusIdCompat locusIdCompat, @Nullable final Bundle bundle) {
            LocusId locusId;
            if (locusIdCompat == null) {
                locusId = null;
            }
            else {
                locusId = locusIdCompat.toLocusId();
            }
            \u3007\u3007888.\u3007080(activity, locusId, bundle);
        }
    }
    
    @RequiresApi(31)
    static class Api31Impl
    {
        private Api31Impl() {
        }
        
        @DoNotInline
        static boolean isLaunchedFromBubble(@NonNull final Activity activity) {
            return \u300780\u3007808\u3007O.\u3007080(activity);
        }
    }
    
    public interface OnRequestPermissionsResultCallback
    {
        void onRequestPermissionsResult(final int p0, @NonNull final String[] p1, @NonNull final int[] p2);
    }
    
    public interface PermissionCompatDelegate
    {
        boolean onActivityResult(@NonNull final Activity p0, @IntRange(from = 0L) final int p1, final int p2, @Nullable final Intent p3);
        
        boolean requestPermissions(@NonNull final Activity p0, @NonNull final String[] p1, @IntRange(from = 0L) final int p2);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public interface RequestPermissionsRequestCodeValidator
    {
        void validateRequestPermissionsRequestCode(final int p0);
    }
    
    @RequiresApi(21)
    static class SharedElementCallback21Impl extends android.app.SharedElementCallback
    {
        private final SharedElementCallback mCallback;
        
        SharedElementCallback21Impl(final SharedElementCallback mCallback) {
            this.mCallback = mCallback;
        }
        
        public Parcelable onCaptureSharedElementSnapshot(final View view, final Matrix matrix, final RectF rectF) {
            return this.mCallback.onCaptureSharedElementSnapshot(view, matrix, rectF);
        }
        
        public View onCreateSnapshotView(final Context context, final Parcelable parcelable) {
            return this.mCallback.onCreateSnapshotView(context, parcelable);
        }
        
        public void onMapSharedElements(final List<String> list, final Map<String, View> map) {
            this.mCallback.onMapSharedElements(list, map);
        }
        
        public void onRejectSharedElements(final List<View> list) {
            this.mCallback.onRejectSharedElements(list);
        }
        
        public void onSharedElementEnd(final List<String> list, final List<View> list2, final List<View> list3) {
            this.mCallback.onSharedElementEnd(list, list2, list3);
        }
        
        public void onSharedElementStart(final List<String> list, final List<View> list2, final List<View> list3) {
            this.mCallback.onSharedElementStart(list, list2, list3);
        }
        
        @RequiresApi(23)
        public void onSharedElementsArrived(final List<String> list, final List<View> list2, final SharedElementCallback$OnSharedElementsReadyListener sharedElementCallback$OnSharedElementsReadyListener) {
            this.mCallback.onSharedElementsArrived(list, list2, (SharedElementCallback.OnSharedElementsReadyListener)new OO0o\u3007\u3007\u3007\u30070(sharedElementCallback$OnSharedElementsReadyListener));
        }
    }
}

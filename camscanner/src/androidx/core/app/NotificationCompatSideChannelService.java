// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.content.Context;
import android.os.RemoteException;
import android.os.Binder;
import android.support.v4.app.INotificationSideChannel;
import android.os.IBinder;
import android.content.Intent;
import android.app.Notification;
import android.app.Service;

public abstract class NotificationCompatSideChannelService extends Service
{
    public abstract void cancel(final String p0, final int p1, final String p2);
    
    public abstract void cancelAll(final String p0);
    
    void checkPermission(final int i, final String s) {
        final String[] packagesForUid = ((Context)this).getPackageManager().getPackagesForUid(i);
        for (int length = packagesForUid.length, j = 0; j < length; ++j) {
            if (packagesForUid[j].equals(s)) {
                return;
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("NotificationSideChannelService: Uid ");
        sb.append(i);
        sb.append(" is not authorized for package ");
        sb.append(s);
        throw new SecurityException(sb.toString());
    }
    
    public abstract void notify(final String p0, final int p1, final String p2, final Notification p3);
    
    public IBinder onBind(final Intent intent) {
        intent.getAction().equals("android.support.BIND_NOTIFICATION_SIDE_CHANNEL");
        return null;
    }
    
    private class NotificationSideChannelStub extends Stub
    {
        final NotificationCompatSideChannelService this$0;
        
        NotificationSideChannelStub(final NotificationCompatSideChannelService this$0) {
            this.this$0 = this$0;
        }
        
        public void cancel(final String s, final int n, final String s2) throws RemoteException {
            this.this$0.checkPermission(Binder.getCallingUid(), s);
            final long clearCallingIdentity = Binder.clearCallingIdentity();
            try {
                this.this$0.cancel(s, n, s2);
            }
            finally {
                Binder.restoreCallingIdentity(clearCallingIdentity);
            }
        }
        
        public void cancelAll(final String s) {
            this.this$0.checkPermission(Binder.getCallingUid(), s);
            final long clearCallingIdentity = Binder.clearCallingIdentity();
            try {
                this.this$0.cancelAll(s);
            }
            finally {
                Binder.restoreCallingIdentity(clearCallingIdentity);
            }
        }
        
        public void notify(final String s, final int n, final String s2, final Notification notification) throws RemoteException {
            this.this$0.checkPermission(Binder.getCallingUid(), s);
            final long clearCallingIdentity = Binder.clearCallingIdentity();
            try {
                this.this$0.notify(s, n, s2, notification);
            }
            finally {
                Binder.restoreCallingIdentity(clearCallingIdentity);
            }
        }
    }
}

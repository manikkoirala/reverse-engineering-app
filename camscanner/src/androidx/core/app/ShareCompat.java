// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.text.Spanned;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager$NameNotFoundException;
import android.graphics.drawable.Drawable;
import androidx.annotation.StringRes;
import android.os.Parcelable;
import android.content.ContextWrapper;
import androidx.core.util.Preconditions;
import android.content.Context;
import android.content.ClipData;
import android.content.ClipData$Item;
import android.net.Uri;
import java.util.ArrayList;
import androidx.annotation.DoNotInline;
import android.text.Html;
import androidx.annotation.RequiresApi;
import androidx.annotation.Nullable;
import android.content.Intent;
import android.content.ComponentName;
import android.app.Activity;
import android.view.ActionProvider;
import android.widget.ShareActionProvider;
import android.view.MenuItem;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import android.view.Menu;

public final class ShareCompat
{
    public static final String EXTRA_CALLING_ACTIVITY = "androidx.core.app.EXTRA_CALLING_ACTIVITY";
    public static final String EXTRA_CALLING_ACTIVITY_INTEROP = "android.support.v4.app.EXTRA_CALLING_ACTIVITY";
    public static final String EXTRA_CALLING_PACKAGE = "androidx.core.app.EXTRA_CALLING_PACKAGE";
    public static final String EXTRA_CALLING_PACKAGE_INTEROP = "android.support.v4.app.EXTRA_CALLING_PACKAGE";
    private static final String HISTORY_FILENAME_PREFIX = ".sharecompat_";
    
    private ShareCompat() {
    }
    
    @Deprecated
    public static void configureMenuItem(@NonNull final Menu menu, @IdRes final int i, @NonNull final IntentBuilder intentBuilder) {
        final MenuItem item = menu.findItem(i);
        if (item != null) {
            configureMenuItem(item, intentBuilder);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Could not find menu item with id ");
        sb.append(i);
        sb.append(" in the supplied menu");
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Deprecated
    public static void configureMenuItem(@NonNull final MenuItem menuItem, @NonNull final IntentBuilder intentBuilder) {
        final ActionProvider actionProvider = menuItem.getActionProvider();
        ShareActionProvider actionProvider2;
        if (!(actionProvider instanceof ShareActionProvider)) {
            actionProvider2 = new ShareActionProvider(intentBuilder.getContext());
        }
        else {
            actionProvider2 = (ShareActionProvider)actionProvider;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(".sharecompat_");
        sb.append(intentBuilder.getContext().getClass().getName());
        actionProvider2.setShareHistoryFileName(sb.toString());
        actionProvider2.setShareIntent(intentBuilder.getIntent());
        menuItem.setActionProvider((ActionProvider)actionProvider2);
    }
    
    @Nullable
    public static ComponentName getCallingActivity(@NonNull final Activity activity) {
        final Intent intent = activity.getIntent();
        ComponentName componentName;
        if ((componentName = activity.getCallingActivity()) == null) {
            componentName = getCallingActivity(intent);
        }
        return componentName;
    }
    
    @Nullable
    static ComponentName getCallingActivity(@NonNull final Intent intent) {
        ComponentName componentName;
        if ((componentName = (ComponentName)intent.getParcelableExtra("androidx.core.app.EXTRA_CALLING_ACTIVITY")) == null) {
            componentName = (ComponentName)intent.getParcelableExtra("android.support.v4.app.EXTRA_CALLING_ACTIVITY");
        }
        return componentName;
    }
    
    @Nullable
    public static String getCallingPackage(@NonNull final Activity activity) {
        final Intent intent = activity.getIntent();
        String s2;
        final String s = s2 = activity.getCallingPackage();
        if (s == null) {
            s2 = s;
            if (intent != null) {
                s2 = getCallingPackage(intent);
            }
        }
        return s2;
    }
    
    @Nullable
    static String getCallingPackage(@NonNull final Intent intent) {
        String s;
        if ((s = intent.getStringExtra("androidx.core.app.EXTRA_CALLING_PACKAGE")) == null) {
            s = intent.getStringExtra("android.support.v4.app.EXTRA_CALLING_PACKAGE");
        }
        return s;
    }
    
    @RequiresApi(16)
    private static class Api16Impl
    {
        @DoNotInline
        static String escapeHtml(final CharSequence charSequence) {
            return Html.escapeHtml(charSequence);
        }
        
        @DoNotInline
        static void migrateExtraStreamToClipData(@NonNull final Intent intent, @NonNull final ArrayList<Uri> list) {
            final ClipData clipData = new ClipData((CharSequence)null, new String[] { intent.getType() }, new ClipData$Item(intent.getCharSequenceExtra("android.intent.extra.TEXT"), intent.getStringExtra("android.intent.extra.HTML_TEXT"), (Intent)null, (Uri)list.get(0)));
            for (int size = list.size(), i = 1; i < size; ++i) {
                clipData.addItem(new ClipData$Item((Uri)list.get(i)));
            }
            intent.setClipData(clipData);
            intent.addFlags(1);
        }
        
        @DoNotInline
        static void removeClipData(@NonNull final Intent intent) {
            intent.setClipData((ClipData)null);
            intent.setFlags(intent.getFlags() & 0xFFFFFFFE);
        }
    }
    
    public static class IntentBuilder
    {
        @Nullable
        private ArrayList<String> mBccAddresses;
        @Nullable
        private ArrayList<String> mCcAddresses;
        @Nullable
        private CharSequence mChooserTitle;
        @NonNull
        private final Context mContext;
        @NonNull
        private final Intent mIntent;
        @Nullable
        private ArrayList<Uri> mStreams;
        @Nullable
        private ArrayList<String> mToAddresses;
        
        public IntentBuilder(@NonNull Context baseContext) {
            this.mContext = Preconditions.checkNotNull(baseContext);
            final Intent setAction = new Intent().setAction("android.intent.action.SEND");
            (this.mIntent = setAction).putExtra("androidx.core.app.EXTRA_CALLING_PACKAGE", baseContext.getPackageName());
            setAction.putExtra("android.support.v4.app.EXTRA_CALLING_PACKAGE", baseContext.getPackageName());
            setAction.addFlags(524288);
            while (true) {
                while (baseContext instanceof ContextWrapper) {
                    if (baseContext instanceof Activity) {
                        final Activity activity = (Activity)baseContext;
                        if (activity != null) {
                            final ComponentName componentName = activity.getComponentName();
                            this.mIntent.putExtra("androidx.core.app.EXTRA_CALLING_ACTIVITY", (Parcelable)componentName);
                            this.mIntent.putExtra("android.support.v4.app.EXTRA_CALLING_ACTIVITY", (Parcelable)componentName);
                        }
                        return;
                    }
                    baseContext = ((ContextWrapper)baseContext).getBaseContext();
                }
                final Activity activity = null;
                continue;
            }
        }
        
        private void combineArrayExtra(final String s, final ArrayList<String> list) {
            final String[] stringArrayExtra = this.mIntent.getStringArrayExtra(s);
            int length;
            if (stringArrayExtra != null) {
                length = stringArrayExtra.length;
            }
            else {
                length = 0;
            }
            final String[] a = new String[list.size() + length];
            list.toArray(a);
            if (stringArrayExtra != null) {
                System.arraycopy(stringArrayExtra, 0, a, list.size(), length);
            }
            this.mIntent.putExtra(s, a);
        }
        
        private void combineArrayExtra(@Nullable final String s, @NonNull final String[] array) {
            final Intent intent = this.getIntent();
            final String[] stringArrayExtra = intent.getStringArrayExtra(s);
            int length;
            if (stringArrayExtra != null) {
                length = stringArrayExtra.length;
            }
            else {
                length = 0;
            }
            final String[] array2 = new String[array.length + length];
            if (stringArrayExtra != null) {
                System.arraycopy(stringArrayExtra, 0, array2, 0, length);
            }
            System.arraycopy(array, 0, array2, length, array.length);
            intent.putExtra(s, array2);
        }
        
        @Deprecated
        @NonNull
        public static IntentBuilder from(@NonNull final Activity activity) {
            return new IntentBuilder((Context)activity);
        }
        
        @NonNull
        public IntentBuilder addEmailBcc(@NonNull final String e) {
            if (this.mBccAddresses == null) {
                this.mBccAddresses = new ArrayList<String>();
            }
            this.mBccAddresses.add(e);
            return this;
        }
        
        @NonNull
        public IntentBuilder addEmailBcc(@NonNull final String[] array) {
            this.combineArrayExtra("android.intent.extra.BCC", array);
            return this;
        }
        
        @NonNull
        public IntentBuilder addEmailCc(@NonNull final String e) {
            if (this.mCcAddresses == null) {
                this.mCcAddresses = new ArrayList<String>();
            }
            this.mCcAddresses.add(e);
            return this;
        }
        
        @NonNull
        public IntentBuilder addEmailCc(@NonNull final String[] array) {
            this.combineArrayExtra("android.intent.extra.CC", array);
            return this;
        }
        
        @NonNull
        public IntentBuilder addEmailTo(@NonNull final String e) {
            if (this.mToAddresses == null) {
                this.mToAddresses = new ArrayList<String>();
            }
            this.mToAddresses.add(e);
            return this;
        }
        
        @NonNull
        public IntentBuilder addEmailTo(@NonNull final String[] array) {
            this.combineArrayExtra("android.intent.extra.EMAIL", array);
            return this;
        }
        
        @NonNull
        public IntentBuilder addStream(@NonNull final Uri e) {
            if (this.mStreams == null) {
                this.mStreams = new ArrayList<Uri>();
            }
            this.mStreams.add(e);
            return this;
        }
        
        @NonNull
        public Intent createChooserIntent() {
            return Intent.createChooser(this.getIntent(), this.mChooserTitle);
        }
        
        @NonNull
        Context getContext() {
            return this.mContext;
        }
        
        @NonNull
        public Intent getIntent() {
            final ArrayList<String> mToAddresses = this.mToAddresses;
            if (mToAddresses != null) {
                this.combineArrayExtra("android.intent.extra.EMAIL", mToAddresses);
                this.mToAddresses = null;
            }
            final ArrayList<String> mCcAddresses = this.mCcAddresses;
            if (mCcAddresses != null) {
                this.combineArrayExtra("android.intent.extra.CC", mCcAddresses);
                this.mCcAddresses = null;
            }
            final ArrayList<String> mBccAddresses = this.mBccAddresses;
            if (mBccAddresses != null) {
                this.combineArrayExtra("android.intent.extra.BCC", mBccAddresses);
                this.mBccAddresses = null;
            }
            final ArrayList<Uri> mStreams = this.mStreams;
            boolean b = false;
            Label_0089: {
                if (mStreams != null) {
                    final int size = mStreams.size();
                    b = true;
                    if (size > 1) {
                        break Label_0089;
                    }
                }
                b = false;
            }
            if (!b) {
                this.mIntent.setAction("android.intent.action.SEND");
                final ArrayList<Uri> mStreams2 = this.mStreams;
                if (mStreams2 != null && !mStreams2.isEmpty()) {
                    this.mIntent.putExtra("android.intent.extra.STREAM", (Parcelable)this.mStreams.get(0));
                    Api16Impl.migrateExtraStreamToClipData(this.mIntent, this.mStreams);
                }
                else {
                    this.mIntent.removeExtra("android.intent.extra.STREAM");
                    Api16Impl.removeClipData(this.mIntent);
                }
            }
            else {
                this.mIntent.setAction("android.intent.action.SEND_MULTIPLE");
                this.mIntent.putParcelableArrayListExtra("android.intent.extra.STREAM", (ArrayList)this.mStreams);
                Api16Impl.migrateExtraStreamToClipData(this.mIntent, this.mStreams);
            }
            return this.mIntent;
        }
        
        @NonNull
        public IntentBuilder setChooserTitle(@StringRes final int n) {
            return this.setChooserTitle(this.mContext.getText(n));
        }
        
        @NonNull
        public IntentBuilder setChooserTitle(@Nullable final CharSequence mChooserTitle) {
            this.mChooserTitle = mChooserTitle;
            return this;
        }
        
        @NonNull
        public IntentBuilder setEmailBcc(@Nullable final String[] array) {
            this.mIntent.putExtra("android.intent.extra.BCC", array);
            return this;
        }
        
        @NonNull
        public IntentBuilder setEmailCc(@Nullable final String[] array) {
            this.mIntent.putExtra("android.intent.extra.CC", array);
            return this;
        }
        
        @NonNull
        public IntentBuilder setEmailTo(@Nullable final String[] array) {
            if (this.mToAddresses != null) {
                this.mToAddresses = null;
            }
            this.mIntent.putExtra("android.intent.extra.EMAIL", array);
            return this;
        }
        
        @NonNull
        public IntentBuilder setHtmlText(@Nullable final String s) {
            this.mIntent.putExtra("android.intent.extra.HTML_TEXT", s);
            if (!this.mIntent.hasExtra("android.intent.extra.TEXT")) {
                this.setText((CharSequence)Html.fromHtml(s));
            }
            return this;
        }
        
        @NonNull
        public IntentBuilder setStream(@Nullable final Uri uri) {
            this.mStreams = null;
            if (uri != null) {
                this.addStream(uri);
            }
            return this;
        }
        
        @NonNull
        public IntentBuilder setSubject(@Nullable final String s) {
            this.mIntent.putExtra("android.intent.extra.SUBJECT", s);
            return this;
        }
        
        @NonNull
        public IntentBuilder setText(@Nullable final CharSequence charSequence) {
            this.mIntent.putExtra("android.intent.extra.TEXT", charSequence);
            return this;
        }
        
        @NonNull
        public IntentBuilder setType(@Nullable final String type) {
            this.mIntent.setType(type);
            return this;
        }
        
        public void startChooser() {
            this.mContext.startActivity(this.createChooserIntent());
        }
    }
    
    public static class IntentReader
    {
        private static final String TAG = "IntentReader";
        @Nullable
        private final ComponentName mCallingActivity;
        @Nullable
        private final String mCallingPackage;
        @NonNull
        private final Context mContext;
        @NonNull
        private final Intent mIntent;
        @Nullable
        private ArrayList<Uri> mStreams;
        
        public IntentReader(@NonNull final Activity activity) {
            this(Preconditions.checkNotNull((Context)activity), activity.getIntent());
        }
        
        public IntentReader(@NonNull final Context context, @NonNull final Intent intent) {
            this.mContext = Preconditions.checkNotNull(context);
            this.mIntent = Preconditions.checkNotNull(intent);
            this.mCallingPackage = ShareCompat.getCallingPackage(intent);
            this.mCallingActivity = ShareCompat.getCallingActivity(intent);
        }
        
        @Deprecated
        @NonNull
        public static IntentReader from(@NonNull final Activity activity) {
            return new IntentReader(activity);
        }
        
        private static void withinStyle(final StringBuilder sb, final CharSequence charSequence, int i, final int n) {
            while (i < n) {
                final char char1 = charSequence.charAt(i);
                if (char1 == '<') {
                    sb.append("&lt;");
                }
                else if (char1 == '>') {
                    sb.append("&gt;");
                }
                else if (char1 == '&') {
                    sb.append("&amp;");
                }
                else if (char1 <= '~' && char1 >= ' ') {
                    if (char1 == ' ') {
                        while (true) {
                            final int n2 = i + 1;
                            if (n2 >= n || charSequence.charAt(n2) != ' ') {
                                break;
                            }
                            sb.append("&nbsp;");
                            i = n2;
                        }
                        sb.append(' ');
                    }
                    else {
                        sb.append(char1);
                    }
                }
                else {
                    sb.append("&#");
                    sb.append((int)char1);
                    sb.append(";");
                }
                ++i;
            }
        }
        
        @Nullable
        public ComponentName getCallingActivity() {
            return this.mCallingActivity;
        }
        
        @Nullable
        public Drawable getCallingActivityIcon() {
            if (this.mCallingActivity == null) {
                return null;
            }
            final PackageManager packageManager = this.mContext.getPackageManager();
            try {
                return packageManager.getActivityIcon(this.mCallingActivity);
            }
            catch (final PackageManager$NameNotFoundException ex) {
                return null;
            }
        }
        
        @Nullable
        public Drawable getCallingApplicationIcon() {
            if (this.mCallingPackage == null) {
                return null;
            }
            final PackageManager packageManager = this.mContext.getPackageManager();
            try {
                return packageManager.getApplicationIcon(this.mCallingPackage);
            }
            catch (final PackageManager$NameNotFoundException ex) {
                return null;
            }
        }
        
        @Nullable
        public CharSequence getCallingApplicationLabel() {
            if (this.mCallingPackage == null) {
                return null;
            }
            final PackageManager packageManager = this.mContext.getPackageManager();
            try {
                return packageManager.getApplicationLabel(packageManager.getApplicationInfo(this.mCallingPackage, 0));
            }
            catch (final PackageManager$NameNotFoundException ex) {
                return null;
            }
        }
        
        @Nullable
        public String getCallingPackage() {
            return this.mCallingPackage;
        }
        
        @Nullable
        public String[] getEmailBcc() {
            return this.mIntent.getStringArrayExtra("android.intent.extra.BCC");
        }
        
        @Nullable
        public String[] getEmailCc() {
            return this.mIntent.getStringArrayExtra("android.intent.extra.CC");
        }
        
        @Nullable
        public String[] getEmailTo() {
            return this.mIntent.getStringArrayExtra("android.intent.extra.EMAIL");
        }
        
        @Nullable
        public String getHtmlText() {
            String s2;
            final String s = s2 = this.mIntent.getStringExtra("android.intent.extra.HTML_TEXT");
            if (s == null) {
                final CharSequence text = this.getText();
                if (text instanceof Spanned) {
                    s2 = Html.toHtml((Spanned)text);
                }
                else {
                    s2 = s;
                    if (text != null) {
                        s2 = Api16Impl.escapeHtml(text);
                    }
                }
            }
            return s2;
        }
        
        @Nullable
        public Uri getStream() {
            return (Uri)this.mIntent.getParcelableExtra("android.intent.extra.STREAM");
        }
        
        @Nullable
        public Uri getStream(final int n) {
            if (this.mStreams == null && this.isMultipleShare()) {
                this.mStreams = this.mIntent.getParcelableArrayListExtra("android.intent.extra.STREAM");
            }
            final ArrayList<Uri> mStreams = this.mStreams;
            if (mStreams != null) {
                return mStreams.get(n);
            }
            if (n == 0) {
                return (Uri)this.mIntent.getParcelableExtra("android.intent.extra.STREAM");
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Stream items available: ");
            sb.append(this.getStreamCount());
            sb.append(" index requested: ");
            sb.append(n);
            throw new IndexOutOfBoundsException(sb.toString());
        }
        
        public int getStreamCount() {
            if (this.mStreams == null && this.isMultipleShare()) {
                this.mStreams = this.mIntent.getParcelableArrayListExtra("android.intent.extra.STREAM");
            }
            final ArrayList<Uri> mStreams = this.mStreams;
            if (mStreams != null) {
                return mStreams.size();
            }
            return this.mIntent.hasExtra("android.intent.extra.STREAM") ? 1 : 0;
        }
        
        @Nullable
        public String getSubject() {
            return this.mIntent.getStringExtra("android.intent.extra.SUBJECT");
        }
        
        @Nullable
        public CharSequence getText() {
            return this.mIntent.getCharSequenceExtra("android.intent.extra.TEXT");
        }
        
        @Nullable
        public String getType() {
            return this.mIntent.getType();
        }
        
        public boolean isMultipleShare() {
            return "android.intent.action.SEND_MULTIPLE".equals(this.mIntent.getAction());
        }
        
        public boolean isShareIntent() {
            final String action = this.mIntent.getAction();
            return "android.intent.action.SEND".equals(action) || "android.intent.action.SEND_MULTIPLE".equals(action);
        }
        
        public boolean isSingleShare() {
            return "android.intent.action.SEND".equals(this.mIntent.getAction());
        }
    }
}

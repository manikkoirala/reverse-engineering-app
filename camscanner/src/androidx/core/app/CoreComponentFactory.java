// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentProvider;
import android.app.Application;
import android.app.Activity;
import androidx.annotation.Nullable;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;
import android.app.AppComponentFactory;

@RequiresApi(api = 28)
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public class CoreComponentFactory extends AppComponentFactory
{
    static <T> T checkCompatWrapper(final T t) {
        if (t instanceof CompatWrapped) {
            final Object wrapper = ((CompatWrapped)t).getWrapper();
            if (wrapper != null) {
                return (T)wrapper;
            }
        }
        return t;
    }
    
    @NonNull
    public Activity instantiateActivity(@NonNull final ClassLoader classLoader, @NonNull final String s, @Nullable final Intent intent) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        return checkCompatWrapper(super.instantiateActivity(classLoader, s, intent));
    }
    
    @NonNull
    public Application instantiateApplication(@NonNull final ClassLoader classLoader, @NonNull final String s) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        return checkCompatWrapper(super.instantiateApplication(classLoader, s));
    }
    
    @NonNull
    public ContentProvider instantiateProvider(@NonNull final ClassLoader classLoader, @NonNull final String s) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        return checkCompatWrapper(super.instantiateProvider(classLoader, s));
    }
    
    @NonNull
    public BroadcastReceiver instantiateReceiver(@NonNull final ClassLoader classLoader, @NonNull final String s, @Nullable final Intent intent) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        return checkCompatWrapper(super.instantiateReceiver(classLoader, s, intent));
    }
    
    @NonNull
    public Service instantiateService(@NonNull final ClassLoader classLoader, @NonNull final String s, @Nullable final Intent intent) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        return checkCompatWrapper(super.instantiateService(classLoader, s, intent));
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public interface CompatWrapped
    {
        Object getWrapper();
    }
}

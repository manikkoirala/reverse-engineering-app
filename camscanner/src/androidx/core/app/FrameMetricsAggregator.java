// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import androidx.annotation.RestrictTo;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.Iterator;
import android.view.FrameMetrics;
import android.view.Window;
import android.view.Window$OnFrameMetricsAvailableListener;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import android.os.HandlerThread;
import android.os.Handler;
import androidx.annotation.RequiresApi;
import androidx.annotation.Nullable;
import android.util.SparseIntArray;
import androidx.annotation.NonNull;
import android.app.Activity;
import android.os.Build$VERSION;

public class FrameMetricsAggregator
{
    public static final int ANIMATION_DURATION = 256;
    public static final int ANIMATION_INDEX = 8;
    public static final int COMMAND_DURATION = 32;
    public static final int COMMAND_INDEX = 5;
    public static final int DELAY_DURATION = 128;
    public static final int DELAY_INDEX = 7;
    public static final int DRAW_DURATION = 8;
    public static final int DRAW_INDEX = 3;
    public static final int EVERY_DURATION = 511;
    public static final int INPUT_DURATION = 2;
    public static final int INPUT_INDEX = 1;
    private static final int LAST_INDEX = 8;
    public static final int LAYOUT_MEASURE_DURATION = 4;
    public static final int LAYOUT_MEASURE_INDEX = 2;
    public static final int SWAP_DURATION = 64;
    public static final int SWAP_INDEX = 6;
    public static final int SYNC_DURATION = 16;
    public static final int SYNC_INDEX = 4;
    public static final int TOTAL_DURATION = 1;
    public static final int TOTAL_INDEX = 0;
    private final FrameMetricsBaseImpl mInstance;
    
    public FrameMetricsAggregator() {
        this(1);
    }
    
    public FrameMetricsAggregator(final int n) {
        if (Build$VERSION.SDK_INT >= 24) {
            this.mInstance = (FrameMetricsBaseImpl)new FrameMetricsApi24Impl(n);
        }
        else {
            this.mInstance = new FrameMetricsBaseImpl();
        }
    }
    
    public void add(@NonNull final Activity activity) {
        this.mInstance.add(activity);
    }
    
    @Nullable
    public SparseIntArray[] getMetrics() {
        return this.mInstance.getMetrics();
    }
    
    @Nullable
    public SparseIntArray[] remove(@NonNull final Activity activity) {
        return this.mInstance.remove(activity);
    }
    
    @Nullable
    public SparseIntArray[] reset() {
        return this.mInstance.reset();
    }
    
    @Nullable
    public SparseIntArray[] stop() {
        return this.mInstance.stop();
    }
    
    @RequiresApi(24)
    private static class FrameMetricsApi24Impl extends FrameMetricsBaseImpl
    {
        private static final int NANOS_PER_MS = 1000000;
        private static final int NANOS_ROUNDING_VALUE = 500000;
        private static Handler sHandler;
        private static HandlerThread sHandlerThread;
        private final ArrayList<WeakReference<Activity>> mActivities;
        Window$OnFrameMetricsAvailableListener mListener;
        SparseIntArray[] mMetrics;
        int mTrackingFlags;
        
        FrameMetricsApi24Impl(final int mTrackingFlags) {
            this.mMetrics = new SparseIntArray[9];
            this.mActivities = new ArrayList<WeakReference<Activity>>();
            this.mListener = (Window$OnFrameMetricsAvailableListener)new Window$OnFrameMetricsAvailableListener() {
                final FrameMetricsApi24Impl this$0;
                
                public void onFrameMetricsAvailable(final Window window, final FrameMetrics frameMetrics, final int n) {
                    final FrameMetricsApi24Impl this$0 = this.this$0;
                    if ((this$0.mTrackingFlags & 0x1) != 0x0) {
                        this$0.addDurationItem(this$0.mMetrics[0], \u300700.\u3007080(frameMetrics, 8));
                    }
                    final FrameMetricsApi24Impl this$2 = this.this$0;
                    if ((this$2.mTrackingFlags & 0x2) != 0x0) {
                        this$2.addDurationItem(this$2.mMetrics[1], \u300700.\u3007080(frameMetrics, 1));
                    }
                    final FrameMetricsApi24Impl this$3 = this.this$0;
                    if ((this$3.mTrackingFlags & 0x4) != 0x0) {
                        this$3.addDurationItem(this$3.mMetrics[2], \u300700.\u3007080(frameMetrics, 3));
                    }
                    final FrameMetricsApi24Impl this$4 = this.this$0;
                    if ((this$4.mTrackingFlags & 0x8) != 0x0) {
                        this$4.addDurationItem(this$4.mMetrics[3], \u300700.\u3007080(frameMetrics, 4));
                    }
                    final FrameMetricsApi24Impl this$5 = this.this$0;
                    if ((this$5.mTrackingFlags & 0x10) != 0x0) {
                        this$5.addDurationItem(this$5.mMetrics[4], \u300700.\u3007080(frameMetrics, 5));
                    }
                    final FrameMetricsApi24Impl this$6 = this.this$0;
                    if ((this$6.mTrackingFlags & 0x40) != 0x0) {
                        this$6.addDurationItem(this$6.mMetrics[6], \u300700.\u3007080(frameMetrics, 7));
                    }
                    final FrameMetricsApi24Impl this$7 = this.this$0;
                    if ((this$7.mTrackingFlags & 0x20) != 0x0) {
                        this$7.addDurationItem(this$7.mMetrics[5], \u300700.\u3007080(frameMetrics, 6));
                    }
                    final FrameMetricsApi24Impl this$8 = this.this$0;
                    if ((this$8.mTrackingFlags & 0x80) != 0x0) {
                        this$8.addDurationItem(this$8.mMetrics[7], \u300700.\u3007080(frameMetrics, 0));
                    }
                    final FrameMetricsApi24Impl this$9 = this.this$0;
                    if ((this$9.mTrackingFlags & 0x100) != 0x0) {
                        this$9.addDurationItem(this$9.mMetrics[8], \u300700.\u3007080(frameMetrics, 2));
                    }
                }
            };
            this.mTrackingFlags = mTrackingFlags;
        }
        
        @Override
        public void add(final Activity referent) {
            if (FrameMetricsApi24Impl.sHandlerThread == null) {
                ((Thread)(FrameMetricsApi24Impl.sHandlerThread = new HandlerThread("FrameMetricsAggregator"))).start();
                FrameMetricsApi24Impl.sHandler = new Handler(FrameMetricsApi24Impl.sHandlerThread.getLooper());
            }
            for (int i = 0; i <= 8; ++i) {
                final SparseIntArray[] mMetrics = this.mMetrics;
                if (mMetrics[i] == null && (this.mTrackingFlags & 1 << i) != 0x0) {
                    mMetrics[i] = new SparseIntArray();
                }
            }
            \u3007oo\u3007.\u3007080(referent.getWindow(), this.mListener, FrameMetricsApi24Impl.sHandler);
            this.mActivities.add(new WeakReference<Activity>(referent));
        }
        
        void addDurationItem(final SparseIntArray sparseIntArray, final long n) {
            if (sparseIntArray != null) {
                final int n2 = (int)((500000L + n) / 1000000L);
                if (n >= 0L) {
                    sparseIntArray.put(n2, sparseIntArray.get(n2) + 1);
                }
            }
        }
        
        @Override
        public SparseIntArray[] getMetrics() {
            return this.mMetrics;
        }
        
        @Override
        public SparseIntArray[] remove(final Activity activity) {
            for (final WeakReference o : this.mActivities) {
                if (o.get() == activity) {
                    this.mActivities.remove(o);
                    break;
                }
            }
            o\u3007O8\u3007\u3007o.\u3007080(activity.getWindow(), this.mListener);
            return this.mMetrics;
        }
        
        @Override
        public SparseIntArray[] reset() {
            final SparseIntArray[] mMetrics = this.mMetrics;
            this.mMetrics = new SparseIntArray[9];
            return mMetrics;
        }
        
        @Override
        public SparseIntArray[] stop() {
            for (int i = this.mActivities.size() - 1; i >= 0; --i) {
                final WeakReference weakReference = this.mActivities.get(i);
                final Activity activity = (Activity)weakReference.get();
                if (weakReference.get() != null) {
                    o\u3007O8\u3007\u3007o.\u3007080(activity.getWindow(), this.mListener);
                    this.mActivities.remove(i);
                }
            }
            return this.mMetrics;
        }
    }
    
    private static class FrameMetricsBaseImpl
    {
        FrameMetricsBaseImpl() {
        }
        
        public void add(final Activity activity) {
        }
        
        public SparseIntArray[] getMetrics() {
            return null;
        }
        
        public SparseIntArray[] remove(final Activity activity) {
            return null;
        }
        
        public SparseIntArray[] reset() {
            return null;
        }
        
        public SparseIntArray[] stop() {
            return null;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public @interface MetricType {
    }
}

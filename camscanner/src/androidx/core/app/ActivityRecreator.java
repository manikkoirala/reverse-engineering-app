// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.app;

import android.os.Bundle;
import android.app.Application;
import android.app.Application$ActivityLifecycleCallbacks;
import androidx.annotation.NonNull;
import android.os.Build$VERSION;
import android.content.res.Configuration;
import java.util.List;
import android.os.IBinder;
import android.app.Activity;
import android.os.Looper;
import java.lang.reflect.Method;
import java.lang.reflect.Field;
import android.os.Handler;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
final class ActivityRecreator
{
    private static final String LOG_TAG = "ActivityRecreator";
    protected static final Class<?> activityThreadClass;
    private static final Handler mainHandler;
    protected static final Field mainThreadField;
    protected static final Method performStopActivity2ParamsMethod;
    protected static final Method performStopActivity3ParamsMethod;
    protected static final Method requestRelaunchActivityMethod;
    protected static final Field tokenField;
    
    static {
        mainHandler = new Handler(Looper.getMainLooper());
        final Class<?> clazz = activityThreadClass = getActivityThreadClass();
        mainThreadField = getMainThreadField();
        tokenField = getTokenField();
        performStopActivity3ParamsMethod = getPerformStopActivity3Params(clazz);
        performStopActivity2ParamsMethod = getPerformStopActivity2Params(clazz);
        requestRelaunchActivityMethod = getRequestRelaunchActivityMethod(clazz);
    }
    
    private ActivityRecreator() {
    }
    
    private static Class<?> getActivityThreadClass() {
        try {
            return Class.forName("android.app.ActivityThread");
        }
        finally {
            return null;
        }
    }
    
    private static Field getMainThreadField() {
        try {
            final Field declaredField = Activity.class.getDeclaredField("mMainThread");
            declaredField.setAccessible(true);
            return declaredField;
        }
        finally {
            return null;
        }
    }
    
    private static Method getPerformStopActivity2Params(final Class<?> clazz) {
        if (clazz == null) {
            return null;
        }
        try {
            final Method declaredMethod = clazz.getDeclaredMethod("performStopActivity", IBinder.class, Boolean.TYPE);
            declaredMethod.setAccessible(true);
            return declaredMethod;
        }
        finally {
            return null;
        }
    }
    
    private static Method getPerformStopActivity3Params(final Class<?> clazz) {
        if (clazz == null) {
            return null;
        }
        try {
            final Method declaredMethod = clazz.getDeclaredMethod("performStopActivity", IBinder.class, Boolean.TYPE, String.class);
            declaredMethod.setAccessible(true);
            return declaredMethod;
        }
        finally {
            return null;
        }
    }
    
    private static Method getRequestRelaunchActivityMethod(final Class<?> clazz) {
        Label_0084: {
            if (!needsRelaunchCall()) {
                break Label_0084;
            }
            if (clazz == null) {
                break Label_0084;
            }
            try {
                final Class<Integer> type = Integer.TYPE;
                final Class<Boolean> type2 = Boolean.TYPE;
                final Method declaredMethod = clazz.getDeclaredMethod("requestRelaunchActivity", IBinder.class, List.class, List.class, type, type2, Configuration.class, Configuration.class, type2, type2);
                declaredMethod.setAccessible(true);
                return declaredMethod;
                return null;
            }
            finally {
                return null;
            }
        }
    }
    
    private static Field getTokenField() {
        try {
            final Field declaredField = Activity.class.getDeclaredField("mToken");
            declaredField.setAccessible(true);
            return declaredField;
        }
        finally {
            return null;
        }
    }
    
    private static boolean needsRelaunchCall() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        return sdk_INT == 26 || sdk_INT == 27;
    }
    
    protected static boolean queueOnStopIfNecessary(Object o, final int n, final Activity activity) {
        try {
            final Object value = ActivityRecreator.tokenField.get(activity);
            if (value == o) {
                if (activity.hashCode() == n) {
                    final Object value2 = ActivityRecreator.mainThreadField.get(activity);
                    final Handler mainHandler = ActivityRecreator.mainHandler;
                    o = new Runnable(value2, value) {
                        final Object val$activityThread;
                        final Object val$token;
                        
                        @Override
                        public void run() {
                            try {
                                final Method performStopActivity3ParamsMethod = ActivityRecreator.performStopActivity3ParamsMethod;
                                if (performStopActivity3ParamsMethod != null) {
                                    performStopActivity3ParamsMethod.invoke(this.val$activityThread, this.val$token, Boolean.FALSE, "AppCompat recreation");
                                }
                                else {
                                    ActivityRecreator.performStopActivity2ParamsMethod.invoke(this.val$activityThread, this.val$token, Boolean.FALSE);
                                }
                            }
                            catch (final RuntimeException ex) {
                                if (ex.getClass() == RuntimeException.class && ex.getMessage() != null) {
                                    if (ex.getMessage().startsWith("Unable to stop")) {
                                        throw ex;
                                    }
                                }
                            }
                        }
                    };
                    mainHandler.postAtFrontOfQueue((Runnable)o);
                    return true;
                }
            }
            return false;
        }
        finally {
            return false;
        }
    }
    
    static boolean recreate(@NonNull final Activity activity) {
        if (Build$VERSION.SDK_INT >= 28) {
            activity.recreate();
            return true;
        }
        if (needsRelaunchCall() && ActivityRecreator.requestRelaunchActivityMethod == null) {
            return false;
        }
        if (ActivityRecreator.performStopActivity2ParamsMethod == null && ActivityRecreator.performStopActivity3ParamsMethod == null) {
            return false;
        }
        try {
            final Object value = ActivityRecreator.tokenField.get(activity);
            if (value == null) {}
            final Object value2 = ActivityRecreator.mainThreadField.get(activity);
            if (value2 == null) {}
            final Application application = activity.getApplication();
            final LifecycleCheckCallbacks lifecycleCheckCallbacks = new LifecycleCheckCallbacks(activity);
            application.registerActivityLifecycleCallbacks((Application$ActivityLifecycleCallbacks)lifecycleCheckCallbacks);
            final Handler mainHandler = ActivityRecreator.mainHandler;
            mainHandler.post((Runnable)new Runnable(lifecycleCheckCallbacks, value) {
                final LifecycleCheckCallbacks val$callbacks;
                final Object val$token;
                
                @Override
                public void run() {
                    this.val$callbacks.currentlyRecreatingToken = this.val$token;
                }
            });
            try {
                if (needsRelaunchCall()) {
                    final Method requestRelaunchActivityMethod = ActivityRecreator.requestRelaunchActivityMethod;
                    final Boolean false = Boolean.FALSE;
                    requestRelaunchActivityMethod.invoke(value2, value, null, null, 0, false, null, null, false, false);
                }
                else {
                    activity.recreate();
                }
                mainHandler.post((Runnable)new Runnable(application, lifecycleCheckCallbacks) {
                    final Application val$application;
                    final LifecycleCheckCallbacks val$callbacks;
                    
                    @Override
                    public void run() {
                        this.val$application.unregisterActivityLifecycleCallbacks((Application$ActivityLifecycleCallbacks)this.val$callbacks);
                    }
                });
                return true;
            }
            finally {
                ActivityRecreator.mainHandler.post((Runnable)new Runnable(application, lifecycleCheckCallbacks) {
                    final Application val$application;
                    final LifecycleCheckCallbacks val$callbacks;
                    
                    @Override
                    public void run() {
                        this.val$application.unregisterActivityLifecycleCallbacks((Application$ActivityLifecycleCallbacks)this.val$callbacks);
                    }
                });
            }
        }
        finally {
            return false;
        }
    }
    
    private static final class LifecycleCheckCallbacks implements Application$ActivityLifecycleCallbacks
    {
        Object currentlyRecreatingToken;
        private Activity mActivity;
        private boolean mDestroyed;
        private final int mRecreatingHashCode;
        private boolean mStarted;
        private boolean mStopQueued;
        
        LifecycleCheckCallbacks(@NonNull final Activity mActivity) {
            this.mStarted = false;
            this.mDestroyed = false;
            this.mStopQueued = false;
            this.mActivity = mActivity;
            this.mRecreatingHashCode = mActivity.hashCode();
        }
        
        public void onActivityCreated(final Activity activity, final Bundle bundle) {
        }
        
        public void onActivityDestroyed(final Activity activity) {
            if (this.mActivity == activity) {
                this.mActivity = null;
                this.mDestroyed = true;
            }
        }
        
        public void onActivityPaused(final Activity activity) {
            if (this.mDestroyed && !this.mStopQueued && !this.mStarted && ActivityRecreator.queueOnStopIfNecessary(this.currentlyRecreatingToken, this.mRecreatingHashCode, activity)) {
                this.mStopQueued = true;
                this.currentlyRecreatingToken = null;
            }
        }
        
        public void onActivityResumed(final Activity activity) {
        }
        
        public void onActivitySaveInstanceState(final Activity activity, final Bundle bundle) {
        }
        
        public void onActivityStarted(final Activity activity) {
            if (this.mActivity == activity) {
                this.mStarted = true;
            }
        }
        
        public void onActivityStopped(final Activity activity) {
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import java.util.concurrent.RejectedExecutionException;
import androidx.core.util.Preconditions;
import java.util.concurrent.Executor;
import androidx.annotation.NonNull;
import android.os.Handler;

public final class ExecutorCompat
{
    private ExecutorCompat() {
    }
    
    @NonNull
    public static Executor create(@NonNull final Handler handler) {
        return new HandlerExecutor(handler);
    }
    
    private static class HandlerExecutor implements Executor
    {
        private final Handler mHandler;
        
        HandlerExecutor(@NonNull final Handler handler) {
            this.mHandler = Preconditions.checkNotNull(handler);
        }
        
        @Override
        public void execute(@NonNull final Runnable runnable) {
            if (this.mHandler.post((Runnable)Preconditions.checkNotNull(runnable))) {
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append(this.mHandler);
            sb.append(" is shutting down");
            throw new RejectedExecutionException(sb.toString());
        }
    }
}

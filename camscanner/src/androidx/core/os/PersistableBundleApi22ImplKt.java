// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import androidx.annotation.DoNotInline;
import androidx.core.app.o08O;
import kotlin.jvm.internal.Intrinsics;
import android.os.PersistableBundle;
import org.jetbrains.annotations.NotNull;
import androidx.annotation.RequiresApi;
import kotlin.Metadata;

@Metadata
@RequiresApi(22)
final class PersistableBundleApi22ImplKt
{
    @NotNull
    public static final PersistableBundleApi22ImplKt INSTANCE;
    
    static {
        INSTANCE = new PersistableBundleApi22ImplKt();
    }
    
    private PersistableBundleApi22ImplKt() {
    }
    
    @DoNotInline
    public static final void putBoolean(@NotNull final PersistableBundle persistableBundle, final String s, final boolean b) {
        Intrinsics.checkNotNullParameter((Object)persistableBundle, "persistableBundle");
        o08O.\u3007080(persistableBundle, s, b);
    }
    
    @DoNotInline
    public static final void putBooleanArray(@NotNull final PersistableBundle persistableBundle, final String s, @NotNull final boolean[] array) {
        Intrinsics.checkNotNullParameter((Object)persistableBundle, "persistableBundle");
        Intrinsics.checkNotNullParameter((Object)array, "value");
        \u3007O00.\u3007080(persistableBundle, s, array);
    }
}

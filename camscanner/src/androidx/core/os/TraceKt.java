// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import kotlin.jvm.internal.InlineMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.functions.Function0;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class TraceKt
{
    public static final <T> T trace(@NotNull final String s, @NotNull final Function0<? extends T> function0) {
        Intrinsics.checkNotNullParameter((Object)s, "sectionName");
        Intrinsics.checkNotNullParameter((Object)function0, "block");
        TraceCompat.beginSection(s);
        try {
            return (T)function0.invoke();
        }
        finally {
            InlineMarker.\u3007o00\u3007\u3007Oo(1);
            TraceCompat.endSection();
            InlineMarker.\u3007080(1);
        }
    }
}

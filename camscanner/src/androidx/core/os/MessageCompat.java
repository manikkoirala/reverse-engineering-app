// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.annotation.SuppressLint;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.os.Message;

public final class MessageCompat
{
    private static boolean sTryIsAsynchronous = true;
    private static boolean sTrySetAsynchronous = true;
    
    private MessageCompat() {
    }
    
    @SuppressLint({ "NewApi" })
    public static boolean isAsynchronous(@NonNull final Message message) {
        if (Build$VERSION.SDK_INT >= 22) {
            return Api22Impl.isAsynchronous(message);
        }
        if (MessageCompat.sTryIsAsynchronous) {
            try {
                return Api22Impl.isAsynchronous(message);
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                MessageCompat.sTryIsAsynchronous = false;
            }
        }
        return false;
    }
    
    @SuppressLint({ "NewApi" })
    public static void setAsynchronous(@NonNull final Message message, final boolean b) {
        if (Build$VERSION.SDK_INT >= 22) {
            Api22Impl.setAsynchronous(message, b);
            return;
        }
        if (MessageCompat.sTrySetAsynchronous) {
            try {
                Api22Impl.setAsynchronous(message, b);
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                MessageCompat.sTrySetAsynchronous = false;
            }
        }
    }
    
    @RequiresApi(22)
    static class Api22Impl
    {
        private Api22Impl() {
        }
        
        @DoNotInline
        static boolean isAsynchronous(final Message message) {
            return \u3007O8o08O.\u3007080(message);
        }
        
        @DoNotInline
        static void setAsynchronous(final Message message, final boolean b) {
            OO0o\u3007\u3007.\u3007080(message, b);
        }
    }
}

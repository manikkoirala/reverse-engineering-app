// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import androidx.appcompat.app.Oooo8o0\u3007;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import java.util.Locale;
import androidx.appcompat.app.oO80;
import android.os.LocaleList;
import androidx.annotation.RequiresApi;

@RequiresApi(24)
final class LocaleListPlatformWrapper implements LocaleListInterface
{
    private final LocaleList mLocaleList;
    
    LocaleListPlatformWrapper(final Object o) {
        this.mLocaleList = (LocaleList)o;
    }
    
    @Override
    public boolean equals(final Object o) {
        return oO80.\u3007080(this.mLocaleList, ((LocaleListInterface)o).getLocaleList());
    }
    
    @Override
    public Locale get(final int n) {
        return o\u30070.\u3007080(this.mLocaleList, n);
    }
    
    @Nullable
    @Override
    public Locale getFirstMatch(@NonNull final String[] array) {
        return \u3007\u3007888.\u3007080(this.mLocaleList, array);
    }
    
    @Override
    public Object getLocaleList() {
        return this.mLocaleList;
    }
    
    @Override
    public int hashCode() {
        return \u300780\u3007808\u3007O.\u3007080(this.mLocaleList);
    }
    
    @Override
    public int indexOf(final Locale locale) {
        return \u30078o8o\u3007.\u3007080(this.mLocaleList, locale);
    }
    
    @Override
    public boolean isEmpty() {
        return Oooo8o0\u3007.\u3007080(this.mLocaleList);
    }
    
    @Override
    public int size() {
        return androidx.core.os.oO80.\u3007080(this.mLocaleList);
    }
    
    @Override
    public String toLanguageTags() {
        return androidx.appcompat.app.o\u30070.\u3007080(this.mLocaleList);
    }
    
    @Override
    public String toString() {
        return OO0o\u3007\u3007\u3007\u30070.\u3007080(this.mLocaleList);
    }
}

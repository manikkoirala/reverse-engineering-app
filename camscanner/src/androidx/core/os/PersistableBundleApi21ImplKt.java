// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import android.os.BaseBundle;
import android.os.Build$VERSION;
import kotlin.jvm.internal.Intrinsics;
import androidx.annotation.DoNotInline;
import android.os.PersistableBundle;
import org.jetbrains.annotations.NotNull;
import androidx.annotation.RequiresApi;
import kotlin.Metadata;

@Metadata
@RequiresApi(21)
final class PersistableBundleApi21ImplKt
{
    @NotNull
    public static final PersistableBundleApi21ImplKt INSTANCE;
    
    static {
        INSTANCE = new PersistableBundleApi21ImplKt();
    }
    
    private PersistableBundleApi21ImplKt() {
    }
    
    @DoNotInline
    @NotNull
    public static final PersistableBundle createPersistableBundle(final int n) {
        return new PersistableBundle(n);
    }
    
    @DoNotInline
    public static final void putValue(@NotNull final PersistableBundle persistableBundle, final String s, final Object o) {
        Intrinsics.checkNotNullParameter((Object)persistableBundle, "persistableBundle");
        if (o == null) {
            ((BaseBundle)persistableBundle).putString(s, (String)null);
        }
        else if (o instanceof Boolean) {
            if (Build$VERSION.SDK_INT < 22) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Illegal value type boolean for key \"");
                sb.append(s);
                sb.append('\"');
                throw new IllegalArgumentException(sb.toString());
            }
            PersistableBundleApi22ImplKt.putBoolean(persistableBundle, s, (boolean)o);
        }
        else if (o instanceof Double) {
            ((BaseBundle)persistableBundle).putDouble(s, ((Number)o).doubleValue());
        }
        else if (o instanceof Integer) {
            ((BaseBundle)persistableBundle).putInt(s, ((Number)o).intValue());
        }
        else if (o instanceof Long) {
            ((BaseBundle)persistableBundle).putLong(s, ((Number)o).longValue());
        }
        else if (o instanceof String) {
            ((BaseBundle)persistableBundle).putString(s, (String)o);
        }
        else if (o instanceof boolean[]) {
            if (Build$VERSION.SDK_INT < 22) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Illegal value type boolean[] for key \"");
                sb2.append(s);
                sb2.append('\"');
                throw new IllegalArgumentException(sb2.toString());
            }
            PersistableBundleApi22ImplKt.putBooleanArray(persistableBundle, s, (boolean[])o);
        }
        else if (o instanceof double[]) {
            ((BaseBundle)persistableBundle).putDoubleArray(s, (double[])o);
        }
        else if (o instanceof int[]) {
            ((BaseBundle)persistableBundle).putIntArray(s, (int[])o);
        }
        else if (o instanceof long[]) {
            ((BaseBundle)persistableBundle).putLongArray(s, (long[])o);
        }
        else {
            if (!(o instanceof Object[])) {
                final String canonicalName = o.getClass().getCanonicalName();
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Illegal value type ");
                sb3.append(canonicalName);
                sb3.append(" for key \"");
                sb3.append(s);
                sb3.append('\"');
                throw new IllegalArgumentException(sb3.toString());
            }
            final Class<?> componentType = o.getClass().getComponentType();
            Intrinsics.Oo08((Object)componentType);
            if (!String.class.isAssignableFrom(componentType)) {
                final String canonicalName2 = componentType.getCanonicalName();
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("Illegal value array type ");
                sb4.append(canonicalName2);
                sb4.append(" for key \"");
                sb4.append(s);
                sb4.append('\"');
                throw new IllegalArgumentException(sb4.toString());
            }
            Intrinsics.o\u30070(o, "null cannot be cast to non-null type kotlin.Array<kotlin.String>");
            ((BaseBundle)persistableBundle).putStringArray(s, (String[])o);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import androidx.annotation.DoNotInline;
import androidx.appcompat.app.Oo08;
import android.os.LocaleList;
import androidx.annotation.RequiresApi;
import java.util.Locale;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.content.res.Configuration;

public final class ConfigurationCompat
{
    private ConfigurationCompat() {
    }
    
    @NonNull
    public static LocaleListCompat getLocales(@NonNull final Configuration configuration) {
        if (Build$VERSION.SDK_INT >= 24) {
            return LocaleListCompat.wrap(Api24Impl.getLocales(configuration));
        }
        return LocaleListCompat.create(configuration.locale);
    }
    
    @RequiresApi(24)
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        @DoNotInline
        static LocaleList getLocales(final Configuration configuration) {
            return Oo08.\u3007080(configuration);
        }
    }
}

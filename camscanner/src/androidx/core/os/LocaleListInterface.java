// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import androidx.annotation.IntRange;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import java.util.Locale;

interface LocaleListInterface
{
    Locale get(final int p0);
    
    @Nullable
    Locale getFirstMatch(@NonNull final String[] p0);
    
    Object getLocaleList();
    
    @IntRange(from = -1L)
    int indexOf(final Locale p0);
    
    boolean isEmpty();
    
    @IntRange(from = 0L)
    int size();
    
    String toLanguageTags();
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import androidx.annotation.DoNotInline;
import kotlin.jvm.internal.Intrinsics;
import android.os.IBinder;
import android.os.Bundle;
import org.jetbrains.annotations.NotNull;
import androidx.annotation.RequiresApi;
import kotlin.Metadata;

@Metadata
@RequiresApi(18)
final class BundleApi18ImplKt
{
    @NotNull
    public static final BundleApi18ImplKt INSTANCE;
    
    static {
        INSTANCE = new BundleApi18ImplKt();
    }
    
    private BundleApi18ImplKt() {
    }
    
    @DoNotInline
    public static final void putBinder(@NotNull final Bundle bundle, @NotNull final String s, final IBinder binder) {
        Intrinsics.checkNotNullParameter((Object)bundle, "bundle");
        Intrinsics.checkNotNullParameter((Object)s, "key");
        bundle.putBinder(s, binder);
    }
}

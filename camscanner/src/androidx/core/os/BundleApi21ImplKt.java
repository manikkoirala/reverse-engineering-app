// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import android.util.SizeF;
import androidx.annotation.DoNotInline;
import kotlin.jvm.internal.Intrinsics;
import android.util.Size;
import android.os.Bundle;
import org.jetbrains.annotations.NotNull;
import androidx.annotation.RequiresApi;
import kotlin.Metadata;

@Metadata
@RequiresApi(21)
final class BundleApi21ImplKt
{
    @NotNull
    public static final BundleApi21ImplKt INSTANCE;
    
    static {
        INSTANCE = new BundleApi21ImplKt();
    }
    
    private BundleApi21ImplKt() {
    }
    
    @DoNotInline
    public static final void putSize(@NotNull final Bundle bundle, @NotNull final String s, final Size size) {
        Intrinsics.checkNotNullParameter((Object)bundle, "bundle");
        Intrinsics.checkNotNullParameter((Object)s, "key");
        bundle.putSize(s, size);
    }
    
    @DoNotInline
    public static final void putSizeF(@NotNull final Bundle bundle, @NotNull final String s, final SizeF sizeF) {
        Intrinsics.checkNotNullParameter((Object)bundle, "bundle");
        Intrinsics.checkNotNullParameter((Object)s, "key");
        bundle.putSizeF(s, sizeF);
    }
}

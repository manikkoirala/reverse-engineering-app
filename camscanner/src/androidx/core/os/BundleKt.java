// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import android.os.BaseBundle;
import android.util.SizeF;
import android.util.Size;
import android.os.IBinder;
import java.io.Serializable;
import android.os.Parcelable;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Pair;
import org.jetbrains.annotations.NotNull;
import android.os.Bundle;
import kotlin.Metadata;

@Metadata
public final class BundleKt
{
    @NotNull
    public static final Bundle bundleOf() {
        return new Bundle(0);
    }
    
    @NotNull
    public static final Bundle bundleOf(@NotNull final Pair<String, ?>... array) {
        Intrinsics.checkNotNullParameter((Object)array, "pairs");
        final Bundle bundle = new Bundle(array.length);
        for (final Pair<String, ?> pair : array) {
            final String s = (String)pair.component1();
            final Object component2 = pair.component2();
            if (component2 == null) {
                ((BaseBundle)bundle).putString(s, (String)null);
            }
            else if (component2 instanceof Boolean) {
                bundle.putBoolean(s, (boolean)component2);
            }
            else if (component2 instanceof Byte) {
                bundle.putByte(s, ((Number)component2).byteValue());
            }
            else if (component2 instanceof Character) {
                bundle.putChar(s, (char)component2);
            }
            else if (component2 instanceof Double) {
                ((BaseBundle)bundle).putDouble(s, ((Number)component2).doubleValue());
            }
            else if (component2 instanceof Float) {
                bundle.putFloat(s, ((Number)component2).floatValue());
            }
            else if (component2 instanceof Integer) {
                ((BaseBundle)bundle).putInt(s, ((Number)component2).intValue());
            }
            else if (component2 instanceof Long) {
                ((BaseBundle)bundle).putLong(s, ((Number)component2).longValue());
            }
            else if (component2 instanceof Short) {
                bundle.putShort(s, ((Number)component2).shortValue());
            }
            else if (component2 instanceof Bundle) {
                bundle.putBundle(s, (Bundle)component2);
            }
            else if (component2 instanceof CharSequence) {
                bundle.putCharSequence(s, (CharSequence)component2);
            }
            else if (component2 instanceof Parcelable) {
                bundle.putParcelable(s, (Parcelable)component2);
            }
            else if (component2 instanceof boolean[]) {
                bundle.putBooleanArray(s, (boolean[])component2);
            }
            else if (component2 instanceof byte[]) {
                bundle.putByteArray(s, (byte[])component2);
            }
            else if (component2 instanceof char[]) {
                bundle.putCharArray(s, (char[])component2);
            }
            else if (component2 instanceof double[]) {
                ((BaseBundle)bundle).putDoubleArray(s, (double[])component2);
            }
            else if (component2 instanceof float[]) {
                bundle.putFloatArray(s, (float[])component2);
            }
            else if (component2 instanceof int[]) {
                ((BaseBundle)bundle).putIntArray(s, (int[])component2);
            }
            else if (component2 instanceof long[]) {
                ((BaseBundle)bundle).putLongArray(s, (long[])component2);
            }
            else if (component2 instanceof short[]) {
                bundle.putShortArray(s, (short[])component2);
            }
            else if (component2 instanceof Object[]) {
                final Class<?> componentType = ((short[])component2).getClass().getComponentType();
                Intrinsics.Oo08((Object)componentType);
                if (Parcelable.class.isAssignableFrom(componentType)) {
                    Intrinsics.o\u30070(component2, "null cannot be cast to non-null type kotlin.Array<android.os.Parcelable>");
                    bundle.putParcelableArray(s, (Parcelable[])component2);
                }
                else if (String.class.isAssignableFrom(componentType)) {
                    Intrinsics.o\u30070(component2, "null cannot be cast to non-null type kotlin.Array<kotlin.String>");
                    ((BaseBundle)bundle).putStringArray(s, (String[])component2);
                }
                else if (CharSequence.class.isAssignableFrom(componentType)) {
                    Intrinsics.o\u30070(component2, "null cannot be cast to non-null type kotlin.Array<kotlin.CharSequence>");
                    bundle.putCharSequenceArray(s, (CharSequence[])component2);
                }
                else {
                    if (!Serializable.class.isAssignableFrom(componentType)) {
                        final String canonicalName = componentType.getCanonicalName();
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Illegal value array type ");
                        sb.append(canonicalName);
                        sb.append(" for key \"");
                        sb.append(s);
                        sb.append('\"');
                        throw new IllegalArgumentException(sb.toString());
                    }
                    bundle.putSerializable(s, (Serializable)component2);
                }
            }
            else if (component2 instanceof Serializable) {
                bundle.putSerializable(s, (Serializable)component2);
            }
            else if (component2 instanceof IBinder) {
                BundleApi18ImplKt.putBinder(bundle, s, (IBinder)component2);
            }
            else if (component2 instanceof Size) {
                BundleApi21ImplKt.putSize(bundle, s, (Size)component2);
            }
            else {
                if (!(component2 instanceof SizeF)) {
                    final String canonicalName2 = ((SizeF)component2).getClass().getCanonicalName();
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Illegal value type ");
                    sb2.append(canonicalName2);
                    sb2.append(" for key \"");
                    sb2.append(s);
                    sb2.append('\"');
                    throw new IllegalArgumentException(sb2.toString());
                }
                BundleApi21ImplKt.putSizeF(bundle, s, (SizeF)component2);
            }
        }
        return bundle;
    }
}

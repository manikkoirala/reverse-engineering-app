// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import androidx.arch.core.executor.\u3007080;
import android.os.Message;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import java.lang.reflect.InvocationTargetException;
import android.os.Handler$Callback;
import android.os.Build$VERSION;
import android.os.Handler;
import androidx.annotation.NonNull;
import android.os.Looper;

public final class HandlerCompat
{
    private static final String TAG = "HandlerCompat";
    
    private HandlerCompat() {
    }
    
    @NonNull
    public static Handler createAsync(@NonNull final Looper looper) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.createAsync(looper);
        }
        try {
            return Handler.class.getDeclaredConstructor(Looper.class, Handler$Callback.class, Boolean.TYPE).newInstance(looper, null, Boolean.TRUE);
        }
        catch (final InvocationTargetException ex) {
            final Throwable cause = ex.getCause();
            if (cause instanceof RuntimeException) {
                throw (RuntimeException)cause;
            }
            if (cause instanceof Error) {
                throw (Error)cause;
            }
            throw new RuntimeException(cause);
        }
        catch (final IllegalAccessException | InstantiationException | NoSuchMethodException ex2) {
            return new Handler(looper);
        }
    }
    
    @NonNull
    public static Handler createAsync(@NonNull final Looper looper, @NonNull final Handler$Callback handler$Callback) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.createAsync(looper, handler$Callback);
        }
        try {
            return Handler.class.getDeclaredConstructor(Looper.class, Handler$Callback.class, Boolean.TYPE).newInstance(looper, handler$Callback, Boolean.TRUE);
        }
        catch (final InvocationTargetException ex) {
            final Throwable cause = ex.getCause();
            if (cause instanceof RuntimeException) {
                throw (RuntimeException)cause;
            }
            if (cause instanceof Error) {
                throw (Error)cause;
            }
            throw new RuntimeException(cause);
        }
        catch (final IllegalAccessException | InstantiationException | NoSuchMethodException ex2) {
            return new Handler(looper, handler$Callback);
        }
    }
    
    @RequiresApi(16)
    public static boolean hasCallbacks(@NonNull final Handler obj, @NonNull final Runnable runnable) {
        if (Build$VERSION.SDK_INT >= 29) {
            return Api29Impl.hasCallbacks((Handler)obj, runnable);
        }
        try {
            return (boolean)Handler.class.getMethod("hasCallbacks", Runnable.class).invoke(obj, runnable);
        }
        catch (final NullPointerException obj) {
            goto Label_0060;
        }
        catch (final NoSuchMethodException obj) {
            goto Label_0060;
        }
        catch (final IllegalAccessException ex) {}
        catch (final InvocationTargetException ex2) {
            final Throwable cause = ex2.getCause();
            if (cause instanceof RuntimeException) {
                throw (RuntimeException)cause;
            }
            if (cause instanceof Error) {
                throw (Error)cause;
            }
            throw new RuntimeException(cause);
        }
    }
    
    public static boolean postDelayed(@NonNull final Handler handler, @NonNull final Runnable runnable, @Nullable final Object obj, final long n) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.postDelayed(handler, runnable, obj, n);
        }
        final Message obtain = Message.obtain(handler, runnable);
        obtain.obj = obj;
        return handler.sendMessageDelayed(obtain, n);
    }
    
    @RequiresApi(28)
    private static class Api28Impl
    {
        public static Handler createAsync(final Looper looper) {
            return \u3007080.\u3007080(looper);
        }
        
        public static Handler createAsync(final Looper looper, final Handler$Callback handler$Callback) {
            return androidx.core.os.\u3007080.\u3007080(looper, handler$Callback);
        }
        
        public static boolean postDelayed(final Handler handler, final Runnable runnable, final Object o, final long n) {
            return \u3007o00\u3007\u3007Oo.\u3007080(handler, runnable, o, n);
        }
    }
    
    @RequiresApi(29)
    private static class Api29Impl
    {
        public static boolean hasCallbacks(final Handler handler, final Runnable runnable) {
            return \u3007o\u3007.\u3007080(handler, runnable);
        }
    }
}

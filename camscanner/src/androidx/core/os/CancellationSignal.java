// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import androidx.annotation.Nullable;

public final class CancellationSignal
{
    private boolean mCancelInProgress;
    private Object mCancellationSignalObj;
    private boolean mIsCanceled;
    private OnCancelListener mOnCancelListener;
    
    private void waitForCancelFinishedLocked() {
        while (this.mCancelInProgress) {
            try {
                this.wait();
            }
            catch (final InterruptedException ex) {}
        }
    }
    
    public void cancel() {
        synchronized (this) {
            if (this.mIsCanceled) {
                return;
            }
            this.mIsCanceled = true;
            this.mCancelInProgress = true;
            final OnCancelListener mOnCancelListener = this.mOnCancelListener;
            final Object mCancellationSignalObj = this.mCancellationSignalObj;
            monitorexit(this);
            Label_0051: {
                if (mOnCancelListener == null) {
                    break Label_0051;
                }
                try {
                    mOnCancelListener.onCancel();
                    break Label_0051;
                }
                finally {
                    synchronized (this) {
                        this.mCancelInProgress = false;
                        this.notifyAll();
                    }
                    iftrue(Label_0082:)(mCancellationSignalObj == null);
                    Api16Impl.cancel(mCancellationSignalObj);
                }
            }
            Label_0082: {
                synchronized (this) {
                    this.mCancelInProgress = false;
                    this.notifyAll();
                }
            }
        }
    }
    
    @Nullable
    public Object getCancellationSignalObject() {
        synchronized (this) {
            if (this.mCancellationSignalObj == null) {
                final android.os.CancellationSignal cancellationSignal = Api16Impl.createCancellationSignal();
                this.mCancellationSignalObj = cancellationSignal;
                if (this.mIsCanceled) {
                    Api16Impl.cancel(cancellationSignal);
                }
            }
            return this.mCancellationSignalObj;
        }
    }
    
    public boolean isCanceled() {
        synchronized (this) {
            return this.mIsCanceled;
        }
    }
    
    public void setOnCancelListener(@Nullable final OnCancelListener mOnCancelListener) {
        synchronized (this) {
            this.waitForCancelFinishedLocked();
            if (this.mOnCancelListener == mOnCancelListener) {
                return;
            }
            this.mOnCancelListener = mOnCancelListener;
            if (this.mIsCanceled && mOnCancelListener != null) {
                monitorexit(this);
                mOnCancelListener.onCancel();
            }
        }
    }
    
    public void throwIfCanceled() {
        if (!this.isCanceled()) {
            return;
        }
        throw new OperationCanceledException();
    }
    
    @RequiresApi(16)
    static class Api16Impl
    {
        private Api16Impl() {
        }
        
        @DoNotInline
        static void cancel(final Object o) {
            ((android.os.CancellationSignal)o).cancel();
        }
        
        @DoNotInline
        static android.os.CancellationSignal createCancellationSignal() {
            return new android.os.CancellationSignal();
        }
    }
    
    public interface OnCancelListener
    {
        void onCancel();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import android.os.UserHandle;
import android.annotation.SuppressLint;
import java.lang.reflect.Method;
import androidx.annotation.RequiresApi;
import android.os.Build$VERSION;

public final class ProcessCompat
{
    private ProcessCompat() {
    }
    
    public static boolean isApplicationUid(final int n) {
        if (Build$VERSION.SDK_INT >= 24) {
            return Api24Impl.isApplicationUid(n);
        }
        return Api17Impl.isApplicationUid(n);
    }
    
    @RequiresApi(16)
    static class Api16Impl
    {
        private static Method sMethodUserIdIsAppMethod;
        private static boolean sResolved;
        private static final Object sResolvedLock;
        
        static {
            sResolvedLock = new Object();
        }
        
        private Api16Impl() {
        }
        
        @SuppressLint({ "PrivateApi" })
        static boolean isApplicationUid(final int i) {
            try {
                synchronized (Api16Impl.sResolvedLock) {
                    if (!Api16Impl.sResolved) {
                        Api16Impl.sResolved = true;
                        Api16Impl.sMethodUserIdIsAppMethod = Class.forName("android.os.UserId").getDeclaredMethod("isApp", Integer.TYPE);
                    }
                    monitorexit(Api16Impl.sResolvedLock);
                    final Method sMethodUserIdIsAppMethod = Api16Impl.sMethodUserIdIsAppMethod;
                    if (sMethodUserIdIsAppMethod == null) {
                        return true;
                    }
                    final Boolean b = (Boolean)sMethodUserIdIsAppMethod.invoke(null, i);
                    if (b != null) {
                        return b;
                    }
                    throw new NullPointerException();
                }
            }
            catch (final Exception ex) {
                ex.printStackTrace();
            }
            return true;
        }
    }
    
    @RequiresApi(17)
    static class Api17Impl
    {
        private static Method sMethodUserHandleIsAppMethod;
        private static boolean sResolved;
        private static final Object sResolvedLock;
        
        static {
            sResolvedLock = new Object();
        }
        
        private Api17Impl() {
        }
        
        @SuppressLint({ "DiscouragedPrivateApi" })
        static boolean isApplicationUid(final int i) {
            try {
                synchronized (Api17Impl.sResolvedLock) {
                    if (!Api17Impl.sResolved) {
                        Api17Impl.sResolved = true;
                        Api17Impl.sMethodUserHandleIsAppMethod = UserHandle.class.getDeclaredMethod("isApp", Integer.TYPE);
                    }
                    monitorexit(Api17Impl.sResolvedLock);
                    final Method sMethodUserHandleIsAppMethod = Api17Impl.sMethodUserHandleIsAppMethod;
                    if (sMethodUserHandleIsAppMethod == null) {
                        return true;
                    }
                    final Boolean b = (Boolean)sMethodUserHandleIsAppMethod.invoke(null, i);
                    if (b != null) {
                        return b;
                    }
                    throw new NullPointerException();
                }
            }
            catch (final Exception ex) {
                ex.printStackTrace();
            }
            return true;
        }
    }
    
    @RequiresApi(24)
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        static boolean isApplicationUid(final int n) {
            return \u3007\u30078O0\u30078.\u3007080(n);
        }
    }
}

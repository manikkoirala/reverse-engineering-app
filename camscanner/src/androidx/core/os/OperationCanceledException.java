// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import androidx.core.util.ObjectsCompat;
import androidx.annotation.Nullable;

public class OperationCanceledException extends RuntimeException
{
    public OperationCanceledException() {
        this((String)null);
    }
    
    public OperationCanceledException(@Nullable final String s) {
        super(ObjectsCompat.toString(s, "The operation has been canceled."));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import kotlin.Result$Companion;
import kotlin.ResultKt;
import kotlin.Result;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.Continuation;
import androidx.annotation.RequiresApi;
import kotlin.Metadata;
import android.os.OutcomeReceiver;
import java.util.concurrent.atomic.AtomicBoolean;

@Metadata
@RequiresApi(31)
final class ContinuationOutcomeReceiver<R, E extends Throwable> extends AtomicBoolean implements OutcomeReceiver<R, E>
{
    @NotNull
    private final Continuation<R> continuation;
    
    public ContinuationOutcomeReceiver(@NotNull final Continuation<? super R> continuation) {
        Intrinsics.checkNotNullParameter((Object)continuation, "continuation");
        super(false);
        this.continuation = (Continuation<R>)continuation;
    }
    
    public void onError(@NotNull final E e) {
        Intrinsics.checkNotNullParameter((Object)e, "error");
        if (this.compareAndSet(false, true)) {
            final Continuation<R> continuation = this.continuation;
            final Result$Companion companion = Result.Companion;
            continuation.resumeWith(Result.constructor-impl(ResultKt.\u3007080((Throwable)e)));
        }
    }
    
    public void onResult(final R r) {
        if (this.compareAndSet(false, true)) {
            this.continuation.resumeWith(Result.constructor-impl((Object)r));
        }
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ContinuationOutcomeReceiver(outcomeReceived = ");
        sb.append(this.get());
        sb.append(')');
        return sb.toString();
    }
}

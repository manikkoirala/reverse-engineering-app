// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import androidx.annotation.RequiresOptIn;
import android.annotation.SuppressLint;
import androidx.annotation.RestrictTo;
import java.util.Locale;
import androidx.annotation.NonNull;
import androidx.annotation.ChecksSdkIntAtLeast;
import android.os.Build$VERSION;

public class BuildCompat
{
    private BuildCompat() {
    }
    
    @Deprecated
    @ChecksSdkIntAtLeast(api = 24)
    public static boolean isAtLeastN() {
        return Build$VERSION.SDK_INT >= 24;
    }
    
    @Deprecated
    @ChecksSdkIntAtLeast(api = 25)
    public static boolean isAtLeastNMR1() {
        return Build$VERSION.SDK_INT >= 25;
    }
    
    @Deprecated
    @ChecksSdkIntAtLeast(api = 26)
    public static boolean isAtLeastO() {
        return Build$VERSION.SDK_INT >= 26;
    }
    
    @Deprecated
    @ChecksSdkIntAtLeast(api = 27)
    public static boolean isAtLeastOMR1() {
        return Build$VERSION.SDK_INT >= 27;
    }
    
    @Deprecated
    @ChecksSdkIntAtLeast(api = 28)
    public static boolean isAtLeastP() {
        return Build$VERSION.SDK_INT >= 28;
    }
    
    @RestrictTo({ RestrictTo.Scope.TESTS })
    protected static boolean isAtLeastPreReleaseCodename(@NonNull final String s, @NonNull final String anObject) {
        final boolean equals = "REL".equals(anObject);
        boolean b = false;
        if (equals) {
            return false;
        }
        final Locale root = Locale.ROOT;
        if (anObject.toUpperCase(root).compareTo(s.toUpperCase(root)) >= 0) {
            b = true;
        }
        return b;
    }
    
    @Deprecated
    @ChecksSdkIntAtLeast(api = 29)
    public static boolean isAtLeastQ() {
        return Build$VERSION.SDK_INT >= 29;
    }
    
    @Deprecated
    @ChecksSdkIntAtLeast(api = 30)
    public static boolean isAtLeastR() {
        return Build$VERSION.SDK_INT >= 30;
    }
    
    @Deprecated
    @SuppressLint({ "RestrictedApi" })
    @ChecksSdkIntAtLeast(api = 31, codename = "S")
    public static boolean isAtLeastS() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        return sdk_INT >= 31 || (sdk_INT >= 30 && isAtLeastPreReleaseCodename("S", Build$VERSION.CODENAME));
    }
    
    @Deprecated
    @ChecksSdkIntAtLeast(api = 32, codename = "Sv2")
    @PrereleaseSdkCheck
    public static boolean isAtLeastSv2() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        return sdk_INT >= 32 || (sdk_INT >= 31 && isAtLeastPreReleaseCodename("Sv2", Build$VERSION.CODENAME));
    }
    
    @ChecksSdkIntAtLeast(api = 33, codename = "Tiramisu")
    @PrereleaseSdkCheck
    public static boolean isAtLeastT() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        return sdk_INT >= 33 || (sdk_INT >= 32 && isAtLeastPreReleaseCodename("Tiramisu", Build$VERSION.CODENAME));
    }
    
    @ChecksSdkIntAtLeast(codename = "UpsideDownCake")
    @PrereleaseSdkCheck
    public static boolean isAtLeastU() {
        return Build$VERSION.SDK_INT >= 33 && isAtLeastPreReleaseCodename("UpsideDownCake", Build$VERSION.CODENAME);
    }
    
    @RequiresOptIn
    public @interface PrereleaseSdkCheck {
    }
}

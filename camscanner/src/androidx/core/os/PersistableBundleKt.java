// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import java.util.Iterator;
import java.util.Map;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Pair;
import org.jetbrains.annotations.NotNull;
import androidx.annotation.RequiresApi;
import android.os.PersistableBundle;
import kotlin.Metadata;

@Metadata
public final class PersistableBundleKt
{
    @RequiresApi(21)
    @NotNull
    public static final PersistableBundle persistableBundleOf() {
        return PersistableBundleApi21ImplKt.createPersistableBundle(0);
    }
    
    @RequiresApi(21)
    @NotNull
    public static final PersistableBundle persistableBundleOf(@NotNull final Pair<String, ?>... array) {
        Intrinsics.checkNotNullParameter((Object)array, "pairs");
        final PersistableBundle persistableBundle = PersistableBundleApi21ImplKt.createPersistableBundle(array.length);
        for (final Pair<String, ?> pair : array) {
            PersistableBundleApi21ImplKt.putValue(persistableBundle, (String)pair.component1(), pair.component2());
        }
        return persistableBundle;
    }
    
    @RequiresApi(21)
    @NotNull
    public static final PersistableBundle toPersistableBundle(@NotNull final Map<String, ?> map) {
        Intrinsics.checkNotNullParameter((Object)map, "<this>");
        final PersistableBundle persistableBundle = PersistableBundleApi21ImplKt.createPersistableBundle(map.size());
        for (final Map.Entry<String, V> entry : map.entrySet()) {
            PersistableBundleApi21ImplKt.putValue(persistableBundle, entry.getKey(), entry.getValue());
        }
        return persistableBundle;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import androidx.core.text.ICUCompat;
import androidx.annotation.DoNotInline;
import androidx.annotation.IntRange;
import androidx.annotation.RequiresApi;
import androidx.annotation.OptIn;
import android.os.LocaleList;
import androidx.annotation.Size;
import androidx.annotation.Nullable;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import java.util.Locale;

public final class LocaleListCompat
{
    private static final LocaleListCompat sEmptyLocaleList;
    private final LocaleListInterface mImpl;
    
    static {
        sEmptyLocaleList = create(new Locale[0]);
    }
    
    private LocaleListCompat(final LocaleListInterface mImpl) {
        this.mImpl = mImpl;
    }
    
    @NonNull
    public static LocaleListCompat create(@NonNull final Locale... array) {
        if (Build$VERSION.SDK_INT >= 24) {
            return wrap(Api24Impl.createLocaleList(array));
        }
        return new LocaleListCompat(new LocaleListCompatWrapper(array));
    }
    
    static Locale forLanguageTagCompat(final String s) {
        if (s.contains("-")) {
            final String[] split = s.split("-", -1);
            if (split.length > 2) {
                return new Locale(split[0], split[1], split[2]);
            }
            if (split.length > 1) {
                return new Locale(split[0], split[1]);
            }
            if (split.length == 1) {
                return new Locale(split[0]);
            }
        }
        else {
            if (!s.contains("_")) {
                return new Locale(s);
            }
            final String[] split2 = s.split("_", -1);
            if (split2.length > 2) {
                return new Locale(split2[0], split2[1], split2[2]);
            }
            if (split2.length > 1) {
                return new Locale(split2[0], split2[1]);
            }
            if (split2.length == 1) {
                return new Locale(split2[0]);
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Can not parse language tag: [");
        sb.append(s);
        sb.append("]");
        throw new IllegalArgumentException(sb.toString());
    }
    
    @NonNull
    public static LocaleListCompat forLanguageTags(@Nullable final String s) {
        if (s != null && !s.isEmpty()) {
            final String[] split = s.split(",", -1);
            final int length = split.length;
            final Locale[] array = new Locale[length];
            for (int i = 0; i < length; ++i) {
                array[i] = Api21Impl.forLanguageTag(split[i]);
            }
            return create(array);
        }
        return getEmptyLocaleList();
    }
    
    @NonNull
    @Size(min = 1L)
    public static LocaleListCompat getAdjustedDefault() {
        if (Build$VERSION.SDK_INT >= 24) {
            return wrap(Api24Impl.getAdjustedDefault());
        }
        return create(Locale.getDefault());
    }
    
    @NonNull
    @Size(min = 1L)
    public static LocaleListCompat getDefault() {
        if (Build$VERSION.SDK_INT >= 24) {
            return wrap(Api24Impl.getDefault());
        }
        return create(Locale.getDefault());
    }
    
    @NonNull
    public static LocaleListCompat getEmptyLocaleList() {
        return LocaleListCompat.sEmptyLocaleList;
    }
    
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    @RequiresApi(21)
    public static boolean matchesLanguageAndScript(@NonNull final Locale locale, @NonNull final Locale locale2) {
        if (BuildCompat.isAtLeastT()) {
            return LocaleList.matchesLanguageAndScript(locale, locale2);
        }
        return Api21Impl.matchesLanguageAndScript(locale, locale2);
    }
    
    @NonNull
    @RequiresApi(24)
    public static LocaleListCompat wrap(@NonNull final LocaleList list) {
        return new LocaleListCompat(new LocaleListPlatformWrapper(list));
    }
    
    @Deprecated
    @RequiresApi(24)
    public static LocaleListCompat wrap(final Object o) {
        return wrap((LocaleList)o);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof LocaleListCompat && this.mImpl.equals(((LocaleListCompat)o).mImpl);
    }
    
    @Nullable
    public Locale get(final int n) {
        return this.mImpl.get(n);
    }
    
    @Nullable
    public Locale getFirstMatch(@NonNull final String[] array) {
        return this.mImpl.getFirstMatch(array);
    }
    
    @Override
    public int hashCode() {
        return this.mImpl.hashCode();
    }
    
    @IntRange(from = -1L)
    public int indexOf(@Nullable final Locale locale) {
        return this.mImpl.indexOf(locale);
    }
    
    public boolean isEmpty() {
        return this.mImpl.isEmpty();
    }
    
    @IntRange(from = 0L)
    public int size() {
        return this.mImpl.size();
    }
    
    @NonNull
    public String toLanguageTags() {
        return this.mImpl.toLanguageTags();
    }
    
    @NonNull
    @Override
    public String toString() {
        return this.mImpl.toString();
    }
    
    @Nullable
    public Object unwrap() {
        return this.mImpl.getLocaleList();
    }
    
    @RequiresApi(21)
    static class Api21Impl
    {
        private static final Locale[] PSEUDO_LOCALE;
        
        static {
            PSEUDO_LOCALE = new Locale[] { new Locale("en", "XA"), new Locale("ar", "XB") };
        }
        
        private Api21Impl() {
        }
        
        @DoNotInline
        static Locale forLanguageTag(final String languageTag) {
            return Locale.forLanguageTag(languageTag);
        }
        
        private static boolean isPseudoLocale(final Locale obj) {
            final Locale[] pseudo_LOCALE = Api21Impl.PSEUDO_LOCALE;
            for (int length = pseudo_LOCALE.length, i = 0; i < length; ++i) {
                if (pseudo_LOCALE[i].equals(obj)) {
                    return true;
                }
            }
            return false;
        }
        
        @DoNotInline
        static boolean matchesLanguageAndScript(@NonNull final Locale locale, @NonNull final Locale obj) {
            final boolean equals = locale.equals(obj);
            final boolean b = true;
            if (equals) {
                return true;
            }
            if (!locale.getLanguage().equals(obj.getLanguage())) {
                return false;
            }
            if (isPseudoLocale(locale) || isPseudoLocale(obj)) {
                return false;
            }
            final String maximizeAndGetScript = ICUCompat.maximizeAndGetScript(locale);
            if (maximizeAndGetScript.isEmpty()) {
                final String country = locale.getCountry();
                boolean b2 = b;
                if (!country.isEmpty()) {
                    b2 = (country.equals(obj.getCountry()) && b);
                }
                return b2;
            }
            return maximizeAndGetScript.equals(ICUCompat.maximizeAndGetScript(obj));
        }
    }
    
    @RequiresApi(24)
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        @DoNotInline
        static LocaleList createLocaleList(final Locale... array) {
            return new LocaleList(array);
        }
        
        @DoNotInline
        static LocaleList getAdjustedDefault() {
            return Oo08.\u3007080();
        }
        
        @DoNotInline
        static LocaleList getDefault() {
            return O8.\u3007080();
        }
    }
}

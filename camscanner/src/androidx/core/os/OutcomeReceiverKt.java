// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import kotlin.jvm.internal.Intrinsics;
import android.os.OutcomeReceiver;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.Continuation;
import androidx.annotation.RequiresApi;
import kotlin.Metadata;

@Metadata
@RequiresApi(31)
public final class OutcomeReceiverKt
{
    @RequiresApi(31)
    @NotNull
    public static final <R, E extends Throwable> OutcomeReceiver<R, E> asOutcomeReceiver(@NotNull final Continuation<? super R> continuation) {
        Intrinsics.checkNotNullParameter((Object)continuation, "<this>");
        return (OutcomeReceiver<R, E>)new ContinuationOutcomeReceiver((kotlin.coroutines.Continuation<? super Object>)continuation);
    }
}

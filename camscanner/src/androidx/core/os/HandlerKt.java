// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import org.jetbrains.annotations.NotNull;
import android.os.Handler;
import kotlin.Metadata;

@Metadata
public final class HandlerKt
{
    @NotNull
    public static final Runnable postAtTime(@NotNull final Handler handler, final long n, final Object o, @NotNull final Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter((Object)handler, "<this>");
        Intrinsics.checkNotNullParameter((Object)function0, "action");
        final HandlerKt$postAtTime$runnable.HandlerKt$postAtTime$runnable$1 handlerKt$postAtTime$runnable$1 = new HandlerKt$postAtTime$runnable.HandlerKt$postAtTime$runnable$1((Function0)function0);
        handler.postAtTime((Runnable)handlerKt$postAtTime$runnable$1, o, n);
        return (Runnable)handlerKt$postAtTime$runnable$1;
    }
    
    @NotNull
    public static final Runnable postDelayed(@NotNull final Handler handler, final long n, final Object o, @NotNull final Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter((Object)handler, "<this>");
        Intrinsics.checkNotNullParameter((Object)function0, "action");
        final HandlerKt$postDelayed$runnable.HandlerKt$postDelayed$runnable$1 handlerKt$postDelayed$runnable$1 = new HandlerKt$postDelayed$runnable.HandlerKt$postDelayed$runnable$1((Function0)function0);
        if (o == null) {
            handler.postDelayed((Runnable)handlerKt$postDelayed$runnable$1, n);
        }
        else {
            HandlerCompat.postDelayed(handler, (Runnable)handlerKt$postDelayed$runnable$1, o, n);
        }
        return (Runnable)handlerKt$postDelayed$runnable$1;
    }
}

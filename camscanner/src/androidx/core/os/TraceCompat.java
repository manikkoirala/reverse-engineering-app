// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import androidx.annotation.NonNull;
import android.os.Trace;
import android.os.Build$VERSION;
import java.lang.reflect.Method;

@Deprecated
public final class TraceCompat
{
    private static final String TAG = "TraceCompat";
    private static Method sAsyncTraceBeginMethod;
    private static Method sAsyncTraceEndMethod;
    private static Method sIsTagEnabledMethod;
    private static Method sTraceCounterMethod;
    private static long sTraceTagApp;
    
    static {
        if (Build$VERSION.SDK_INT >= 29) {
            return;
        }
        try {
            TraceCompat.sTraceTagApp = Trace.class.getField("TRACE_TAG_APP").getLong(null);
            final Class<Long> type = Long.TYPE;
            TraceCompat.sIsTagEnabledMethod = Trace.class.getMethod("isTagEnabled", type);
            final Class<Integer> type2 = Integer.TYPE;
            TraceCompat.sAsyncTraceBeginMethod = Trace.class.getMethod("asyncTraceBegin", type, String.class, type2);
            TraceCompat.sAsyncTraceEndMethod = Trace.class.getMethod("asyncTraceEnd", type, String.class, type2);
            TraceCompat.sTraceCounterMethod = Trace.class.getMethod("traceCounter", type, String.class, type2);
        }
        catch (final Exception ex) {}
    }
    
    private TraceCompat() {
    }
    
    public static void beginAsyncSection(@NonNull final String s, final int i) {
        if (Build$VERSION.SDK_INT >= 29) {
            Api29Impl.beginAsyncSection(s, i);
            return;
        }
        try {
            TraceCompat.sAsyncTraceBeginMethod.invoke(null, TraceCompat.sTraceTagApp, s, i);
        }
        catch (final Exception ex) {}
    }
    
    public static void beginSection(@NonNull final String s) {
        Api18Impl.beginSection(s);
    }
    
    public static void endAsyncSection(@NonNull final String s, final int i) {
        if (Build$VERSION.SDK_INT >= 29) {
            Api29Impl.endAsyncSection(s, i);
            return;
        }
        try {
            TraceCompat.sAsyncTraceEndMethod.invoke(null, TraceCompat.sTraceTagApp, s, i);
        }
        catch (final Exception ex) {}
    }
    
    public static void endSection() {
        Api18Impl.endSection();
    }
    
    public static boolean isEnabled() {
        if (Build$VERSION.SDK_INT >= 29) {
            return Api29Impl.isEnabled();
        }
        boolean booleanValue = false;
        try {
            booleanValue = (boolean)TraceCompat.sIsTagEnabledMethod.invoke(null, TraceCompat.sTraceTagApp);
            return booleanValue;
        }
        catch (final Exception ex) {
            return booleanValue;
        }
    }
    
    public static void setCounter(@NonNull final String s, final int i) {
        if (Build$VERSION.SDK_INT >= 29) {
            Api29Impl.setCounter(s, i);
            return;
        }
        try {
            TraceCompat.sTraceCounterMethod.invoke(null, TraceCompat.sTraceTagApp, s, i);
        }
        catch (final Exception ex) {}
    }
    
    @RequiresApi(18)
    static class Api18Impl
    {
        private Api18Impl() {
        }
        
        @DoNotInline
        static void beginSection(final String s) {
            Trace.beginSection(s);
        }
        
        @DoNotInline
        static void endSection() {
            Trace.endSection();
        }
    }
    
    @RequiresApi(29)
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        @DoNotInline
        static void beginAsyncSection(final String s, final int n) {
            o800o8O.\u3007080(s, n);
        }
        
        @DoNotInline
        static void endAsyncSection(final String s, final int n) {
            \u3007O888o0o.\u3007080(s, n);
        }
        
        @DoNotInline
        static boolean isEnabled() {
            return OoO8.\u3007080();
        }
        
        @DoNotInline
        static void setCounter(final String s, final long n) {
            \u30070\u3007O0088o.\u3007080(s, n);
        }
    }
}

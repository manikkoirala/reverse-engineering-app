// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import androidx.annotation.DoNotInline;
import android.util.SparseArray;
import java.io.Serializable;
import androidx.annotation.RequiresApi;
import android.os.Parcelable$Creator;
import android.os.Parcelable;
import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;
import androidx.annotation.OptIn;
import android.annotation.SuppressLint;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.os.Parcel;

public final class ParcelCompat
{
    private ParcelCompat() {
    }
    
    @SuppressLint({ "ArrayReturn", "NullableCollection" })
    @Nullable
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    public static <T> T[] readArray(@NonNull final Parcel parcel, @Nullable final ClassLoader classLoader, @NonNull final Class<T> clazz) {
        if (BuildCompat.isAtLeastT()) {
            return TiramisuImpl.readArray(parcel, classLoader, clazz);
        }
        return (T[])parcel.readArray(classLoader);
    }
    
    @SuppressLint({ "ConcreteCollection", "NullableCollection" })
    @Nullable
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    public static <T> ArrayList<T> readArrayList(@NonNull final Parcel parcel, @Nullable final ClassLoader classLoader, @NonNull final Class<? extends T> clazz) {
        if (BuildCompat.isAtLeastT()) {
            return TiramisuImpl.readArrayList(parcel, classLoader, clazz);
        }
        return parcel.readArrayList(classLoader);
    }
    
    public static boolean readBoolean(@NonNull final Parcel parcel) {
        return parcel.readInt() != 0;
    }
    
    @SuppressLint({ "ConcreteCollection", "NullableCollection" })
    @Nullable
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    public static <K, V> HashMap<K, V> readHashMap(@NonNull final Parcel parcel, @Nullable final ClassLoader classLoader, @NonNull final Class<? extends K> clazz, @NonNull final Class<? extends V> clazz2) {
        if (BuildCompat.isAtLeastT()) {
            return TiramisuImpl.readHashMap(parcel, classLoader, clazz, clazz2);
        }
        return parcel.readHashMap(classLoader);
    }
    
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    public static <T> void readList(@NonNull final Parcel parcel, @NonNull final List<? super T> list, @Nullable final ClassLoader classLoader, @NonNull final Class<T> clazz) {
        if (BuildCompat.isAtLeastT()) {
            TiramisuImpl.readList(parcel, list, classLoader, clazz);
        }
        else {
            parcel.readList((List)list, classLoader);
        }
    }
    
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    public static <K, V> void readMap(@NonNull final Parcel parcel, @NonNull final Map<? super K, ? super V> map, @Nullable final ClassLoader classLoader, @NonNull final Class<K> clazz, @NonNull final Class<V> clazz2) {
        if (BuildCompat.isAtLeastT()) {
            TiramisuImpl.readMap(parcel, map, classLoader, clazz, clazz2);
        }
        else {
            parcel.readMap((Map)map, classLoader);
        }
    }
    
    @Nullable
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    public static <T extends Parcelable> T readParcelable(@NonNull final Parcel parcel, @Nullable final ClassLoader classLoader, @NonNull final Class<T> clazz) {
        if (BuildCompat.isAtLeastT()) {
            return TiramisuImpl.readParcelable(parcel, classLoader, clazz);
        }
        return (T)parcel.readParcelable(classLoader);
    }
    
    @SuppressLint({ "ArrayReturn", "NullableCollection" })
    @Nullable
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    public static <T> T[] readParcelableArray(@NonNull final Parcel parcel, @Nullable final ClassLoader classLoader, @NonNull final Class<T> clazz) {
        if (BuildCompat.isAtLeastT()) {
            return TiramisuImpl.readParcelableArray(parcel, classLoader, clazz);
        }
        return (T[])parcel.readParcelableArray(classLoader);
    }
    
    @Nullable
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    @RequiresApi(30)
    public static <T> Parcelable$Creator<T> readParcelableCreator(@NonNull final Parcel parcel, @Nullable final ClassLoader classLoader, @NonNull final Class<T> clazz) {
        if (BuildCompat.isAtLeastT()) {
            return TiramisuImpl.readParcelableCreator(parcel, classLoader, clazz);
        }
        return (Parcelable$Creator<T>)Api30Impl.readParcelableCreator(parcel, classLoader);
    }
    
    @NonNull
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    @RequiresApi(api = 29)
    public static <T> List<T> readParcelableList(@NonNull final Parcel parcel, @NonNull final List<T> list, @Nullable final ClassLoader classLoader, @NonNull final Class<T> clazz) {
        if (BuildCompat.isAtLeastT()) {
            return TiramisuImpl.readParcelableList(parcel, list, classLoader, clazz);
        }
        return Api29Impl.readParcelableList(parcel, list, classLoader);
    }
    
    @Nullable
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    public static <T extends Serializable> T readSerializable(@NonNull final Parcel parcel, @Nullable final ClassLoader classLoader, @NonNull final Class<T> clazz) {
        if (BuildCompat.isAtLeastT()) {
            return TiramisuImpl.readSerializable(parcel, classLoader, clazz);
        }
        return (T)parcel.readSerializable();
    }
    
    @Nullable
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    public static <T> SparseArray<T> readSparseArray(@NonNull final Parcel parcel, @Nullable final ClassLoader classLoader, @NonNull final Class<? extends T> clazz) {
        if (BuildCompat.isAtLeastT()) {
            return TiramisuImpl.readSparseArray(parcel, classLoader, clazz);
        }
        return (SparseArray<T>)parcel.readSparseArray(classLoader);
    }
    
    public static void writeBoolean(@NonNull final Parcel parcel, final boolean b) {
        parcel.writeInt((int)(b ? 1 : 0));
    }
    
    @RequiresApi(29)
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        @DoNotInline
        static final <T extends Parcelable> List<T> readParcelableList(@NonNull final Parcel parcel, @NonNull final List<T> list, @Nullable final ClassLoader classLoader) {
            return Oooo8o0\u3007.\u3007080(parcel, (List)list, classLoader);
        }
    }
    
    @RequiresApi(30)
    static class Api30Impl
    {
        private Api30Impl() {
        }
        
        @DoNotInline
        static final Parcelable$Creator<?> readParcelableCreator(@NonNull final Parcel parcel, @Nullable final ClassLoader classLoader) {
            return (Parcelable$Creator<?>)\u3007\u3007808\u3007.\u3007080(parcel, classLoader);
        }
    }
    
    @RequiresApi(33)
    static class TiramisuImpl
    {
        private TiramisuImpl() {
        }
        
        @DoNotInline
        public static <T> T[] readArray(final Parcel parcel, final ClassLoader classLoader, final Class<T> clazz) {
            return (T[])parcel.readArray(classLoader, (Class)clazz);
        }
        
        @DoNotInline
        public static <T> ArrayList<T> readArrayList(final Parcel parcel, final ClassLoader classLoader, final Class<? extends T> clazz) {
            return parcel.readArrayList(classLoader, (Class)clazz);
        }
        
        @DoNotInline
        public static <V, K> HashMap<K, V> readHashMap(final Parcel parcel, final ClassLoader classLoader, final Class<? extends K> clazz, final Class<? extends V> clazz2) {
            return parcel.readHashMap(classLoader, (Class)clazz, (Class)clazz2);
        }
        
        @DoNotInline
        public static <T> void readList(@NonNull final Parcel parcel, @NonNull final List<? super T> list, @Nullable final ClassLoader classLoader, @NonNull final Class<T> clazz) {
            parcel.readList((List)list, classLoader, (Class)clazz);
        }
        
        @DoNotInline
        public static <K, V> void readMap(final Parcel parcel, final Map<? super K, ? super V> map, final ClassLoader classLoader, final Class<K> clazz, final Class<V> clazz2) {
            parcel.readMap((Map)map, classLoader, (Class)clazz, (Class)clazz2);
        }
        
        @DoNotInline
        static <T extends Parcelable> T readParcelable(@NonNull final Parcel parcel, @Nullable final ClassLoader classLoader, @NonNull final Class<T> clazz) {
            return (T)parcel.readParcelable(classLoader, (Class)clazz);
        }
        
        @DoNotInline
        static <T> T[] readParcelableArray(@NonNull final Parcel parcel, @Nullable final ClassLoader classLoader, @NonNull final Class<T> clazz) {
            return (T[])parcel.readParcelableArray(classLoader, (Class)clazz);
        }
        
        @DoNotInline
        public static <T> Parcelable$Creator<T> readParcelableCreator(final Parcel parcel, final ClassLoader classLoader, final Class<T> clazz) {
            return (Parcelable$Creator<T>)parcel.readParcelableCreator(classLoader, (Class)clazz);
        }
        
        @DoNotInline
        static <T> List<T> readParcelableList(@NonNull final Parcel parcel, @NonNull final List<T> list, @Nullable final ClassLoader classLoader, @NonNull final Class<T> clazz) {
            return parcel.readParcelableList((List)list, classLoader, (Class)clazz);
        }
        
        @DoNotInline
        static <T extends Serializable> T readSerializable(@NonNull final Parcel parcel, @Nullable final ClassLoader classLoader, @NonNull final Class<T> clazz) {
            return (T)parcel.readSerializable(classLoader, (Class)clazz);
        }
        
        @DoNotInline
        public static <T> SparseArray<T> readSparseArray(final Parcel parcel, final ClassLoader classLoader, final Class<? extends T> clazz) {
            return (SparseArray<T>)parcel.readSparseArray(classLoader, (Class)clazz);
        }
    }
}

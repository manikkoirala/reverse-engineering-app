// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import androidx.annotation.DoNotInline;
import androidx.appcompat.widget.oo88o8O;
import android.os.UserManager;
import androidx.annotation.RequiresApi;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.content.Context;

public class UserManagerCompat
{
    private UserManagerCompat() {
    }
    
    public static boolean isUserUnlocked(@NonNull final Context context) {
        return Build$VERSION.SDK_INT < 24 || Api24Impl.isUserUnlocked(context);
    }
    
    @RequiresApi(24)
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        @DoNotInline
        static boolean isUserUnlocked(final Context context) {
            return \u3007oo\u3007.\u3007080((UserManager)oo88o8O.\u3007080(context, (Class)UserManager.class));
        }
    }
}

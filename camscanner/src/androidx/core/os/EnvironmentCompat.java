// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.os;

import androidx.annotation.DoNotInline;
import android.os.Environment;
import androidx.annotation.RequiresApi;
import androidx.annotation.NonNull;
import java.io.File;

public final class EnvironmentCompat
{
    public static final String MEDIA_UNKNOWN = "unknown";
    private static final String TAG = "EnvironmentCompat";
    
    private EnvironmentCompat() {
    }
    
    @NonNull
    public static String getStorageState(@NonNull final File file) {
        return Api21Impl.getExternalStorageState(file);
    }
    
    @RequiresApi(19)
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        @DoNotInline
        static String getStorageState(final File file) {
            return Environment.getStorageState(file);
        }
    }
    
    @RequiresApi(21)
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        @DoNotInline
        static String getExternalStorageState(final File file) {
            return Environment.getExternalStorageState(file);
        }
    }
}

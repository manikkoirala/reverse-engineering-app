// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.hardware.fingerprint;

import java.security.Signature;
import javax.crypto.Mac;
import javax.crypto.Cipher;
import androidx.appcompat.widget.oo88o8O;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresPermission;
import android.os.Build$VERSION;
import android.os.Handler;
import androidx.core.os.CancellationSignal;
import android.hardware.fingerprint.FingerprintManager$AuthenticationResult;
import android.hardware.fingerprint.FingerprintManager$AuthenticationCallback;
import android.hardware.fingerprint.FingerprintManager$CryptoObject;
import androidx.annotation.RequiresApi;
import androidx.annotation.Nullable;
import android.hardware.fingerprint.FingerprintManager;
import androidx.annotation.NonNull;
import android.content.Context;

@Deprecated
public class FingerprintManagerCompat
{
    private final Context mContext;
    
    private FingerprintManagerCompat(final Context mContext) {
        this.mContext = mContext;
    }
    
    @NonNull
    public static FingerprintManagerCompat from(@NonNull final Context context) {
        return new FingerprintManagerCompat(context);
    }
    
    @Nullable
    @RequiresApi(23)
    private static FingerprintManager getFingerprintManagerOrNull(@NonNull final Context context) {
        return Api23Impl.getFingerprintManagerOrNull(context);
    }
    
    @RequiresApi(23)
    static CryptoObject unwrapCryptoObject(final FingerprintManager$CryptoObject fingerprintManager$CryptoObject) {
        return Api23Impl.unwrapCryptoObject(fingerprintManager$CryptoObject);
    }
    
    @RequiresApi(23)
    private static FingerprintManager$AuthenticationCallback wrapCallback(final AuthenticationCallback authenticationCallback) {
        return new FingerprintManager$AuthenticationCallback(authenticationCallback) {
            final AuthenticationCallback val$callback;
            
            public void onAuthenticationError(final int n, final CharSequence charSequence) {
                this.val$callback.onAuthenticationError(n, charSequence);
            }
            
            public void onAuthenticationFailed() {
                this.val$callback.onAuthenticationFailed();
            }
            
            public void onAuthenticationHelp(final int n, final CharSequence charSequence) {
                this.val$callback.onAuthenticationHelp(n, charSequence);
            }
            
            public void onAuthenticationSucceeded(final FingerprintManager$AuthenticationResult fingerprintManager$AuthenticationResult) {
                this.val$callback.onAuthenticationSucceeded(new AuthenticationResult(FingerprintManagerCompat.unwrapCryptoObject(Api23Impl.getCryptoObject(fingerprintManager$AuthenticationResult))));
            }
        };
    }
    
    @RequiresApi(23)
    private static FingerprintManager$CryptoObject wrapCryptoObject(final CryptoObject cryptoObject) {
        return Api23Impl.wrapCryptoObject(cryptoObject);
    }
    
    @RequiresPermission("android.permission.USE_FINGERPRINT")
    public void authenticate(@Nullable final CryptoObject cryptoObject, final int n, @Nullable final CancellationSignal cancellationSignal, @NonNull final AuthenticationCallback authenticationCallback, @Nullable final Handler handler) {
        if (Build$VERSION.SDK_INT >= 23) {
            final FingerprintManager fingerprintManagerOrNull = getFingerprintManagerOrNull(this.mContext);
            if (fingerprintManagerOrNull != null) {
                android.os.CancellationSignal cancellationSignal2;
                if (cancellationSignal != null) {
                    cancellationSignal2 = (android.os.CancellationSignal)cancellationSignal.getCancellationSignalObject();
                }
                else {
                    cancellationSignal2 = null;
                }
                Api23Impl.authenticate(fingerprintManagerOrNull, wrapCryptoObject(cryptoObject), cancellationSignal2, n, wrapCallback(authenticationCallback), handler);
            }
        }
    }
    
    @RequiresPermission("android.permission.USE_FINGERPRINT")
    public boolean hasEnrolledFingerprints() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        boolean b2;
        final boolean b = b2 = false;
        if (sdk_INT >= 23) {
            final FingerprintManager fingerprintManagerOrNull = getFingerprintManagerOrNull(this.mContext);
            b2 = b;
            if (fingerprintManagerOrNull != null) {
                b2 = b;
                if (Api23Impl.hasEnrolledFingerprints(fingerprintManagerOrNull)) {
                    b2 = true;
                }
            }
        }
        return b2;
    }
    
    @RequiresPermission("android.permission.USE_FINGERPRINT")
    public boolean isHardwareDetected() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        boolean b2;
        final boolean b = b2 = false;
        if (sdk_INT >= 23) {
            final FingerprintManager fingerprintManagerOrNull = getFingerprintManagerOrNull(this.mContext);
            b2 = b;
            if (fingerprintManagerOrNull != null) {
                b2 = b;
                if (Api23Impl.isHardwareDetected(fingerprintManagerOrNull)) {
                    b2 = true;
                }
            }
        }
        return b2;
    }
    
    @RequiresApi(23)
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.USE_FINGERPRINT")
        static void authenticate(final Object o, final Object o2, final android.os.CancellationSignal cancellationSignal, final int n, final Object o3, final Handler handler) {
            O8.\u3007080((FingerprintManager)o, (FingerprintManager$CryptoObject)o2, cancellationSignal, n, (FingerprintManager$AuthenticationCallback)o3, handler);
        }
        
        @DoNotInline
        static FingerprintManager$CryptoObject getCryptoObject(final Object o) {
            return \u3007\u3007888.\u3007080((FingerprintManager$AuthenticationResult)o);
        }
        
        @DoNotInline
        public static FingerprintManager getFingerprintManagerOrNull(final Context context) {
            final int sdk_INT = Build$VERSION.SDK_INT;
            if (sdk_INT == 23) {
                return (FingerprintManager)oo88o8O.\u3007080(context, (Class)FingerprintManager.class);
            }
            if (sdk_INT > 23 && context.getPackageManager().hasSystemFeature("android.hardware.fingerprint")) {
                return (FingerprintManager)oo88o8O.\u3007080(context, (Class)FingerprintManager.class);
            }
            return null;
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.USE_FINGERPRINT")
        static boolean hasEnrolledFingerprints(final Object o) {
            return Oo08.\u3007080((FingerprintManager)o);
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.USE_FINGERPRINT")
        static boolean isHardwareDetected(final Object o) {
            return o\u30070.\u3007080((FingerprintManager)o);
        }
        
        @DoNotInline
        public static CryptoObject unwrapCryptoObject(final Object o) {
            final FingerprintManager$CryptoObject fingerprintManager$CryptoObject = (FingerprintManager$CryptoObject)o;
            Object o2 = null;
            if (fingerprintManager$CryptoObject == null) {
                return null;
            }
            if (\u3007080.\u3007080(fingerprintManager$CryptoObject) != null) {
                return new CryptoObject(\u3007080.\u3007080(fingerprintManager$CryptoObject));
            }
            if (\u3007o00\u3007\u3007Oo.\u3007080(fingerprintManager$CryptoObject) != null) {
                return new CryptoObject(\u3007o00\u3007\u3007Oo.\u3007080(fingerprintManager$CryptoObject));
            }
            if (\u3007o\u3007.\u3007080(fingerprintManager$CryptoObject) != null) {
                o2 = new CryptoObject(\u3007o\u3007.\u3007080(fingerprintManager$CryptoObject));
            }
            return (CryptoObject)o2;
        }
        
        @DoNotInline
        public static FingerprintManager$CryptoObject wrapCryptoObject(final CryptoObject cryptoObject) {
            FingerprintManager$CryptoObject fingerprintManager$CryptoObject = null;
            if (cryptoObject == null) {
                return null;
            }
            if (cryptoObject.getCipher() != null) {
                return new FingerprintManager$CryptoObject(cryptoObject.getCipher());
            }
            if (cryptoObject.getSignature() != null) {
                return new FingerprintManager$CryptoObject(cryptoObject.getSignature());
            }
            if (cryptoObject.getMac() != null) {
                fingerprintManager$CryptoObject = new FingerprintManager$CryptoObject(cryptoObject.getMac());
            }
            return fingerprintManager$CryptoObject;
        }
    }
    
    public abstract static class AuthenticationCallback
    {
        public void onAuthenticationError(final int n, final CharSequence charSequence) {
        }
        
        public void onAuthenticationFailed() {
        }
        
        public void onAuthenticationHelp(final int n, final CharSequence charSequence) {
        }
        
        public void onAuthenticationSucceeded(final AuthenticationResult authenticationResult) {
        }
    }
    
    public static final class AuthenticationResult
    {
        private final CryptoObject mCryptoObject;
        
        public AuthenticationResult(final CryptoObject mCryptoObject) {
            this.mCryptoObject = mCryptoObject;
        }
        
        public CryptoObject getCryptoObject() {
            return this.mCryptoObject;
        }
    }
    
    public static class CryptoObject
    {
        private final Cipher mCipher;
        private final Mac mMac;
        private final Signature mSignature;
        
        public CryptoObject(@NonNull final Signature mSignature) {
            this.mSignature = mSignature;
            this.mCipher = null;
            this.mMac = null;
        }
        
        public CryptoObject(@NonNull final Cipher mCipher) {
            this.mCipher = mCipher;
            this.mSignature = null;
            this.mMac = null;
        }
        
        public CryptoObject(@NonNull final Mac mMac) {
            this.mMac = mMac;
            this.mCipher = null;
            this.mSignature = null;
        }
        
        @Nullable
        public Cipher getCipher() {
            return this.mCipher;
        }
        
        @Nullable
        public Mac getMac() {
            return this.mMac;
        }
        
        @Nullable
        public Signature getSignature() {
            return this.mSignature;
        }
    }
}

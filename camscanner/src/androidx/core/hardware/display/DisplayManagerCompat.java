// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.hardware.display;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import androidx.annotation.Nullable;
import android.hardware.display.DisplayManager;
import android.view.Display;
import androidx.annotation.NonNull;
import android.content.Context;
import java.util.WeakHashMap;

public final class DisplayManagerCompat
{
    public static final String DISPLAY_CATEGORY_PRESENTATION = "android.hardware.display.category.PRESENTATION";
    private static final WeakHashMap<Context, DisplayManagerCompat> sInstances;
    private final Context mContext;
    
    static {
        sInstances = new WeakHashMap<Context, DisplayManagerCompat>();
    }
    
    private DisplayManagerCompat(final Context mContext) {
        this.mContext = mContext;
    }
    
    @NonNull
    public static DisplayManagerCompat getInstance(@NonNull final Context context) {
        final WeakHashMap<Context, DisplayManagerCompat> sInstances = DisplayManagerCompat.sInstances;
        synchronized (sInstances) {
            DisplayManagerCompat value;
            if ((value = sInstances.get(context)) == null) {
                value = new DisplayManagerCompat(context);
                sInstances.put(context, value);
            }
            return value;
        }
    }
    
    @Nullable
    public Display getDisplay(final int n) {
        return Api17Impl.getDisplay((DisplayManager)this.mContext.getSystemService("display"), n);
    }
    
    @NonNull
    public Display[] getDisplays() {
        return Api17Impl.getDisplays((DisplayManager)this.mContext.getSystemService("display"));
    }
    
    @NonNull
    public Display[] getDisplays(@Nullable final String s) {
        return Api17Impl.getDisplays((DisplayManager)this.mContext.getSystemService("display"));
    }
    
    @RequiresApi(17)
    static class Api17Impl
    {
        private Api17Impl() {
        }
        
        @DoNotInline
        static Display getDisplay(final DisplayManager displayManager, final int n) {
            return displayManager.getDisplay(n);
        }
        
        @DoNotInline
        static Display[] getDisplays(final DisplayManager displayManager) {
            return displayManager.getDisplays();
        }
    }
}

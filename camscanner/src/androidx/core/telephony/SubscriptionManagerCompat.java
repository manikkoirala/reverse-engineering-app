// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.telephony;

import androidx.annotation.DoNotInline;
import java.lang.reflect.InvocationTargetException;
import android.telephony.SubscriptionManager;
import android.os.Build$VERSION;
import java.lang.reflect.Method;
import androidx.annotation.RequiresApi;

@RequiresApi(22)
public class SubscriptionManagerCompat
{
    private static Method sGetSlotIndexMethod;
    
    private SubscriptionManagerCompat() {
    }
    
    public static int getSlotIndex(int intValue) {
        if (intValue == -1) {
            return -1;
        }
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 29) {
            return Api29Impl.getSlotIndex(intValue);
        }
        try {
            if (SubscriptionManagerCompat.sGetSlotIndexMethod == null) {
                if (sdk_INT >= 26) {
                    SubscriptionManagerCompat.sGetSlotIndexMethod = SubscriptionManager.class.getDeclaredMethod("getSlotIndex", Integer.TYPE);
                }
                else {
                    SubscriptionManagerCompat.sGetSlotIndexMethod = SubscriptionManager.class.getDeclaredMethod("getSlotId", Integer.TYPE);
                }
                SubscriptionManagerCompat.sGetSlotIndexMethod.setAccessible(true);
            }
            final Integer n = (Integer)SubscriptionManagerCompat.sGetSlotIndexMethod.invoke(null, intValue);
            if (n != null) {
                intValue = n;
                return intValue;
            }
            return -1;
        }
        catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
            return -1;
        }
    }
    
    @RequiresApi(29)
    private static class Api29Impl
    {
        @DoNotInline
        static int getSlotIndex(final int n) {
            return \u3007080.\u3007080(n);
        }
    }
}

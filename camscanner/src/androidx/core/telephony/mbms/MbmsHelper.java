// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.telephony.mbms;

import java.util.Iterator;
import java.util.Set;
import androidx.core.os.\u3007\u3007888;
import androidx.appcompat.app.Oo08;
import java.util.Locale;
import androidx.annotation.RequiresApi;
import androidx.annotation.Nullable;
import android.os.Build$VERSION;
import android.telephony.mbms.ServiceInfo;
import androidx.annotation.NonNull;
import android.content.Context;

public final class MbmsHelper
{
    private MbmsHelper() {
    }
    
    @Nullable
    public static CharSequence getBestNameForService(@NonNull final Context context, @NonNull final ServiceInfo serviceInfo) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getBestNameForService(context, serviceInfo);
        }
        return null;
    }
    
    @RequiresApi(28)
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        static CharSequence getBestNameForService(final Context context, final ServiceInfo serviceInfo) {
            final Set \u3007080 = androidx.core.telephony.mbms.\u3007080.\u3007080(serviceInfo);
            final boolean empty = \u3007080.isEmpty();
            final CharSequence charSequence = null;
            if (empty) {
                return null;
            }
            final String[] array = new String[\u3007080.size()];
            final Iterator iterator = androidx.core.telephony.mbms.\u3007080.\u3007080(serviceInfo).iterator();
            int n = 0;
            while (iterator.hasNext()) {
                array[n] = ((Locale)iterator.next()).toLanguageTag();
                ++n;
            }
            final Locale \u300781 = \u3007\u3007888.\u3007080(Oo08.\u3007080(context.getResources().getConfiguration()), array);
            CharSequence \u300782;
            if (\u300781 == null) {
                \u300782 = charSequence;
            }
            else {
                \u300782 = \u3007o00\u3007\u3007Oo.\u3007080(serviceInfo, \u300781);
            }
            return \u300782;
        }
    }
}

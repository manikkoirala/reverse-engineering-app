// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.telephony;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import androidx.annotation.RequiresPermission;
import androidx.annotation.Nullable;
import android.annotation.SuppressLint;
import java.lang.reflect.InvocationTargetException;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.telephony.TelephonyManager;
import java.lang.reflect.Method;

public class TelephonyManagerCompat
{
    private static Method sGetDeviceIdMethod;
    private static Method sGetSubIdMethod;
    
    private TelephonyManagerCompat() {
    }
    
    @SuppressLint({ "MissingPermission" })
    @Nullable
    @RequiresPermission("android.permission.READ_PHONE_STATE")
    public static String getImei(@NonNull final TelephonyManager obj) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 26) {
            return Api26Impl.getImei(obj);
        }
        if (sdk_INT >= 22) {
            final int subscriptionId = getSubscriptionId(obj);
            if (subscriptionId != Integer.MAX_VALUE && subscriptionId != -1) {
                final int slotIndex = SubscriptionManagerCompat.getSlotIndex(subscriptionId);
                if (sdk_INT >= 23) {
                    return Api23Impl.getDeviceId(obj, slotIndex);
                }
                try {
                    if (TelephonyManagerCompat.sGetDeviceIdMethod == null) {
                        (TelephonyManagerCompat.sGetDeviceIdMethod = TelephonyManager.class.getDeclaredMethod("getDeviceId", Integer.TYPE)).setAccessible(true);
                    }
                    return (String)TelephonyManagerCompat.sGetDeviceIdMethod.invoke(obj, slotIndex);
                }
                catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
                    return null;
                }
            }
        }
        return obj.getDeviceId();
    }
    
    @SuppressLint({ "SoonBlockedPrivateApi" })
    public static int getSubscriptionId(@NonNull final TelephonyManager obj) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 30) {
            return Api30Impl.getSubscriptionId(obj);
        }
        if (sdk_INT < 22) {
            return Integer.MAX_VALUE;
        }
        try {
            if (TelephonyManagerCompat.sGetSubIdMethod == null) {
                (TelephonyManagerCompat.sGetSubIdMethod = TelephonyManager.class.getDeclaredMethod("getSubId", (Class<?>[])new Class[0])).setAccessible(true);
            }
            final Integer n = (Integer)TelephonyManagerCompat.sGetSubIdMethod.invoke(obj, new Object[0]);
            if (n != null && n != -1) {
                return n;
            }
            return Integer.MAX_VALUE;
        }
        catch (final InvocationTargetException | IllegalAccessException | NoSuchMethodException ex) {
            return Integer.MAX_VALUE;
        }
    }
    
    @RequiresApi(23)
    private static class Api23Impl
    {
        @SuppressLint({ "MissingPermission" })
        @DoNotInline
        @Nullable
        @RequiresPermission("android.permission.READ_PHONE_STATE")
        static String getDeviceId(final TelephonyManager telephonyManager, final int n) {
            return \u3007o00\u3007\u3007Oo.\u3007080(telephonyManager, n);
        }
    }
    
    @RequiresApi(26)
    private static class Api26Impl
    {
        @SuppressLint({ "MissingPermission" })
        @DoNotInline
        @Nullable
        @RequiresPermission("android.permission.READ_PHONE_STATE")
        static String getImei(final TelephonyManager telephonyManager) {
            return \u3007o\u3007.\u3007080(telephonyManager);
        }
    }
    
    @RequiresApi(30)
    private static class Api30Impl
    {
        @DoNotInline
        static int getSubscriptionId(final TelephonyManager telephonyManager) {
            return O8.\u3007080(telephonyManager);
        }
    }
}

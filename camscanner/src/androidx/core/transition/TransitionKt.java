// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.transition;

import androidx.annotation.RequiresApi;
import kotlin.jvm.internal.Intrinsics;
import android.transition.Transition$TransitionListener;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import org.jetbrains.annotations.NotNull;
import android.transition.Transition;
import android.annotation.SuppressLint;
import kotlin.Metadata;

@Metadata
@SuppressLint({ "ClassVerificationFailure" })
public final class TransitionKt
{
    @RequiresApi(19)
    @NotNull
    public static final Transition$TransitionListener addListener(@NotNull final Transition transition, @NotNull final Function1<? super Transition, Unit> function1, @NotNull final Function1<? super Transition, Unit> function2, @NotNull final Function1<? super Transition, Unit> function3, @NotNull final Function1<? super Transition, Unit> function4, @NotNull final Function1<? super Transition, Unit> function5) {
        Intrinsics.checkNotNullParameter((Object)transition, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "onEnd");
        Intrinsics.checkNotNullParameter((Object)function2, "onStart");
        Intrinsics.checkNotNullParameter((Object)function3, "onCancel");
        Intrinsics.checkNotNullParameter((Object)function4, "onResume");
        Intrinsics.checkNotNullParameter((Object)function5, "onPause");
        final TransitionKt$addListener$listener.TransitionKt$addListener$listener$1 transitionKt$addListener$listener$1 = new TransitionKt$addListener$listener.TransitionKt$addListener$listener$1((Function1)function1, (Function1)function4, (Function1)function5, (Function1)function3, (Function1)function2);
        transition.addListener((Transition$TransitionListener)transitionKt$addListener$listener$1);
        return (Transition$TransitionListener)transitionKt$addListener$listener$1;
    }
    
    @RequiresApi(19)
    @NotNull
    public static final Transition$TransitionListener doOnCancel(@NotNull final Transition transition, @NotNull final Function1<? super Transition, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)transition, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "action");
        final TransitionKt$doOnCancel$$inlined$addListener$default.TransitionKt$doOnCancel$$inlined$addListener$default$1 transitionKt$doOnCancel$$inlined$addListener$default$1 = new TransitionKt$doOnCancel$$inlined$addListener$default.TransitionKt$doOnCancel$$inlined$addListener$default$1((Function1)function1);
        transition.addListener((Transition$TransitionListener)transitionKt$doOnCancel$$inlined$addListener$default$1);
        return (Transition$TransitionListener)transitionKt$doOnCancel$$inlined$addListener$default$1;
    }
    
    @RequiresApi(19)
    @NotNull
    public static final Transition$TransitionListener doOnEnd(@NotNull final Transition transition, @NotNull final Function1<? super Transition, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)transition, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "action");
        final TransitionKt$doOnEnd$$inlined$addListener$default.TransitionKt$doOnEnd$$inlined$addListener$default$1 transitionKt$doOnEnd$$inlined$addListener$default$1 = new TransitionKt$doOnEnd$$inlined$addListener$default.TransitionKt$doOnEnd$$inlined$addListener$default$1((Function1)function1);
        transition.addListener((Transition$TransitionListener)transitionKt$doOnEnd$$inlined$addListener$default$1);
        return (Transition$TransitionListener)transitionKt$doOnEnd$$inlined$addListener$default$1;
    }
    
    @RequiresApi(19)
    @NotNull
    public static final Transition$TransitionListener doOnPause(@NotNull final Transition transition, @NotNull final Function1<? super Transition, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)transition, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "action");
        final TransitionKt$doOnPause$$inlined$addListener$default.TransitionKt$doOnPause$$inlined$addListener$default$1 transitionKt$doOnPause$$inlined$addListener$default$1 = new TransitionKt$doOnPause$$inlined$addListener$default.TransitionKt$doOnPause$$inlined$addListener$default$1((Function1)function1);
        transition.addListener((Transition$TransitionListener)transitionKt$doOnPause$$inlined$addListener$default$1);
        return (Transition$TransitionListener)transitionKt$doOnPause$$inlined$addListener$default$1;
    }
    
    @RequiresApi(19)
    @NotNull
    public static final Transition$TransitionListener doOnResume(@NotNull final Transition transition, @NotNull final Function1<? super Transition, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)transition, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "action");
        final TransitionKt$doOnResume$$inlined$addListener$default.TransitionKt$doOnResume$$inlined$addListener$default$1 transitionKt$doOnResume$$inlined$addListener$default$1 = new TransitionKt$doOnResume$$inlined$addListener$default.TransitionKt$doOnResume$$inlined$addListener$default$1((Function1)function1);
        transition.addListener((Transition$TransitionListener)transitionKt$doOnResume$$inlined$addListener$default$1);
        return (Transition$TransitionListener)transitionKt$doOnResume$$inlined$addListener$default$1;
    }
    
    @RequiresApi(19)
    @NotNull
    public static final Transition$TransitionListener doOnStart(@NotNull final Transition transition, @NotNull final Function1<? super Transition, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)transition, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "action");
        final TransitionKt$doOnStart$$inlined$addListener$default.TransitionKt$doOnStart$$inlined$addListener$default$1 transitionKt$doOnStart$$inlined$addListener$default$1 = new TransitionKt$doOnStart$$inlined$addListener$default.TransitionKt$doOnStart$$inlined$addListener$default$1((Function1)function1);
        transition.addListener((Transition$TransitionListener)transitionKt$doOnStart$$inlined$addListener$default$1);
        return (Transition$TransitionListener)transitionKt$doOnStart$$inlined$addListener$default$1;
    }
}

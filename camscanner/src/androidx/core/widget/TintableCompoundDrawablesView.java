// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import android.graphics.PorterDuff$Mode;
import androidx.annotation.Nullable;
import android.content.res.ColorStateList;

public interface TintableCompoundDrawablesView
{
    @Nullable
    ColorStateList getSupportCompoundDrawablesTintList();
    
    @Nullable
    PorterDuff$Mode getSupportCompoundDrawablesTintMode();
    
    void setSupportCompoundDrawablesTintList(@Nullable final ColorStateList p0);
    
    void setSupportCompoundDrawablesTintMode(@Nullable final PorterDuff$Mode p0);
}

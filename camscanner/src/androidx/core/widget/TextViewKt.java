// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import kotlin.jvm.internal.Intrinsics;
import android.text.TextWatcher;
import android.text.Editable;
import kotlin.jvm.functions.Function1;
import kotlin.Unit;
import kotlin.jvm.functions.Function4;
import org.jetbrains.annotations.NotNull;
import android.widget.TextView;
import kotlin.Metadata;

@Metadata
public final class TextViewKt
{
    @NotNull
    public static final TextWatcher addTextChangedListener(@NotNull final TextView textView, @NotNull final Function4<? super CharSequence, ? super Integer, ? super Integer, ? super Integer, Unit> function4, @NotNull final Function4<? super CharSequence, ? super Integer, ? super Integer, ? super Integer, Unit> function5, @NotNull final Function1<? super Editable, Unit> function6) {
        Intrinsics.checkNotNullParameter((Object)textView, "<this>");
        Intrinsics.checkNotNullParameter((Object)function4, "beforeTextChanged");
        Intrinsics.checkNotNullParameter((Object)function5, "onTextChanged");
        Intrinsics.checkNotNullParameter((Object)function6, "afterTextChanged");
        final TextViewKt$addTextChangedListener$textWatcher.TextViewKt$addTextChangedListener$textWatcher$1 textViewKt$addTextChangedListener$textWatcher$1 = new TextViewKt$addTextChangedListener$textWatcher.TextViewKt$addTextChangedListener$textWatcher$1((Function1)function6, (Function4)function4, (Function4)function5);
        textView.addTextChangedListener((TextWatcher)textViewKt$addTextChangedListener$textWatcher$1);
        return (TextWatcher)textViewKt$addTextChangedListener$textWatcher$1;
    }
    
    @NotNull
    public static final TextWatcher doAfterTextChanged(@NotNull final TextView textView, @NotNull final Function1<? super Editable, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)textView, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "action");
        final TextViewKt$doAfterTextChanged$$inlined$addTextChangedListener$default.TextViewKt$doAfterTextChanged$$inlined$addTextChangedListener$default$1 textViewKt$doAfterTextChanged$$inlined$addTextChangedListener$default$1 = new TextViewKt$doAfterTextChanged$$inlined$addTextChangedListener$default.TextViewKt$doAfterTextChanged$$inlined$addTextChangedListener$default$1((Function1)function1);
        textView.addTextChangedListener((TextWatcher)textViewKt$doAfterTextChanged$$inlined$addTextChangedListener$default$1);
        return (TextWatcher)textViewKt$doAfterTextChanged$$inlined$addTextChangedListener$default$1;
    }
    
    @NotNull
    public static final TextWatcher doBeforeTextChanged(@NotNull final TextView textView, @NotNull final Function4<? super CharSequence, ? super Integer, ? super Integer, ? super Integer, Unit> function4) {
        Intrinsics.checkNotNullParameter((Object)textView, "<this>");
        Intrinsics.checkNotNullParameter((Object)function4, "action");
        final TextViewKt$doBeforeTextChanged$$inlined$addTextChangedListener$default.TextViewKt$doBeforeTextChanged$$inlined$addTextChangedListener$default$1 textViewKt$doBeforeTextChanged$$inlined$addTextChangedListener$default$1 = new TextViewKt$doBeforeTextChanged$$inlined$addTextChangedListener$default.TextViewKt$doBeforeTextChanged$$inlined$addTextChangedListener$default$1((Function4)function4);
        textView.addTextChangedListener((TextWatcher)textViewKt$doBeforeTextChanged$$inlined$addTextChangedListener$default$1);
        return (TextWatcher)textViewKt$doBeforeTextChanged$$inlined$addTextChangedListener$default$1;
    }
    
    @NotNull
    public static final TextWatcher doOnTextChanged(@NotNull final TextView textView, @NotNull final Function4<? super CharSequence, ? super Integer, ? super Integer, ? super Integer, Unit> function4) {
        Intrinsics.checkNotNullParameter((Object)textView, "<this>");
        Intrinsics.checkNotNullParameter((Object)function4, "action");
        final TextViewKt$doOnTextChanged$$inlined$addTextChangedListener$default.TextViewKt$doOnTextChanged$$inlined$addTextChangedListener$default$1 textViewKt$doOnTextChanged$$inlined$addTextChangedListener$default$1 = new TextViewKt$doOnTextChanged$$inlined$addTextChangedListener$default.TextViewKt$doOnTextChanged$$inlined$addTextChangedListener$default$1((Function4)function4);
        textView.addTextChangedListener((TextWatcher)textViewKt$doOnTextChanged$$inlined$addTextChangedListener$default$1);
        return (TextWatcher)textViewKt$doOnTextChanged$$inlined$addTextChangedListener$default$1;
    }
}

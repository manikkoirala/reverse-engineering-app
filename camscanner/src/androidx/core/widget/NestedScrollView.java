// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import android.view.AbsSavedState;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import android.view.View$BaseSavedState;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.os.Bundle;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import android.view.accessibility.AccessibilityRecord;
import androidx.core.view.accessibility.AccessibilityRecordCompat;
import android.widget.ScrollView;
import android.view.accessibility.AccessibilityEvent;
import android.os.Parcelable;
import androidx.core.view.MotionEventCompat;
import android.view.ViewGroup$MarginLayoutParams;
import android.view.View$MeasureSpec;
import android.graphics.Canvas;
import android.view.KeyEvent;
import android.view.FocusFinder;
import android.view.ViewGroup$LayoutParams;
import android.view.animation.AnimationUtils;
import android.view.MotionEvent;
import android.view.ViewParent;
import android.view.ViewConfiguration;
import android.util.TypedValue;
import java.util.ArrayList;
import android.widget.FrameLayout$LayoutParams;
import android.content.res.TypedArray;
import androidx.core.view.AccessibilityDelegateCompat;
import androidx.core.view.ViewCompat;
import android.view.ViewGroup;
import androidx.core.R;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.content.Context;
import android.view.VelocityTracker;
import android.graphics.Rect;
import android.widget.OverScroller;
import androidx.core.view.NestedScrollingParentHelper;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.RestrictTo;
import androidx.annotation.NonNull;
import android.widget.EdgeEffect;
import android.view.View;
import androidx.core.view.NestedScrollingChildHelper;
import androidx.core.view.ScrollingView;
import androidx.core.view.NestedScrollingChild3;
import androidx.core.view.NestedScrollingParent3;
import android.widget.FrameLayout;

public class NestedScrollView extends FrameLayout implements NestedScrollingParent3, NestedScrollingChild3, ScrollingView
{
    private static final AccessibilityDelegate ACCESSIBILITY_DELEGATE;
    static final int ANIMATED_SCROLL_GAP = 250;
    private static final float DECELERATION_RATE;
    private static final int DEFAULT_SMOOTH_SCROLL_DURATION = 250;
    private static final float FLING_DESTRETCH_FACTOR = 4.0f;
    private static final float INFLEXION = 0.35f;
    private static final int INVALID_POINTER = -1;
    static final float MAX_SCROLL_FACTOR = 0.5f;
    private static final int[] SCROLLVIEW_STYLEABLE;
    private static final float SCROLL_FRICTION = 0.015f;
    private static final String TAG = "NestedScrollView";
    private int mActivePointerId;
    private final NestedScrollingChildHelper mChildHelper;
    private View mChildToScrollTo;
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    @VisibleForTesting
    public EdgeEffect mEdgeGlowBottom;
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    @VisibleForTesting
    public EdgeEffect mEdgeGlowTop;
    private boolean mFillViewport;
    private boolean mIsBeingDragged;
    private boolean mIsLaidOut;
    private boolean mIsLayoutDirty;
    private int mLastMotionY;
    private long mLastScroll;
    private int mLastScrollerY;
    private int mMaximumVelocity;
    private int mMinimumVelocity;
    private int mNestedYOffset;
    private OnScrollChangeListener mOnScrollChangeListener;
    private final NestedScrollingParentHelper mParentHelper;
    private final float mPhysicalCoeff;
    private SavedState mSavedState;
    private final int[] mScrollConsumed;
    private final int[] mScrollOffset;
    private OverScroller mScroller;
    private boolean mSmoothScrollingEnabled;
    private final Rect mTempRect;
    private int mTouchSlop;
    private VelocityTracker mVelocityTracker;
    private float mVerticalScrollFactor;
    
    static {
        DECELERATION_RATE = (float)(Math.log(0.78) / Math.log(0.9));
        ACCESSIBILITY_DELEGATE = new AccessibilityDelegate();
        SCROLLVIEW_STYLEABLE = new int[] { 16843130 };
    }
    
    public NestedScrollView(@NonNull final Context context) {
        this(context, null);
    }
    
    public NestedScrollView(@NonNull final Context context, @Nullable final AttributeSet set) {
        this(context, set, R.attr.nestedScrollViewStyle);
    }
    
    public NestedScrollView(@NonNull final Context context, @Nullable final AttributeSet set, final int n) {
        super(context, set, n);
        this.mTempRect = new Rect();
        this.mIsLayoutDirty = true;
        this.mIsLaidOut = false;
        this.mChildToScrollTo = null;
        this.mIsBeingDragged = false;
        this.mSmoothScrollingEnabled = true;
        this.mActivePointerId = -1;
        this.mScrollOffset = new int[2];
        this.mScrollConsumed = new int[2];
        this.mEdgeGlowTop = EdgeEffectCompat.create(context, set);
        this.mEdgeGlowBottom = EdgeEffectCompat.create(context, set);
        this.mPhysicalCoeff = context.getResources().getDisplayMetrics().density * 160.0f * 386.0878f * 0.84f;
        this.initScrollView();
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, NestedScrollView.SCROLLVIEW_STYLEABLE, n, 0);
        this.setFillViewport(obtainStyledAttributes.getBoolean(0, false));
        obtainStyledAttributes.recycle();
        this.mParentHelper = new NestedScrollingParentHelper((ViewGroup)this);
        this.mChildHelper = new NestedScrollingChildHelper((View)this);
        this.setNestedScrollingEnabled(true);
        ViewCompat.setAccessibilityDelegate((View)this, NestedScrollView.ACCESSIBILITY_DELEGATE);
    }
    
    private void abortAnimatedScroll() {
        this.mScroller.abortAnimation();
        this.stopNestedScroll(1);
    }
    
    private boolean canOverScroll() {
        final int overScrollMode = ((View)this).getOverScrollMode();
        boolean b = true;
        if (overScrollMode != 0) {
            b = (overScrollMode == 1 && this.getScrollRange() > 0 && b);
        }
        return b;
    }
    
    private boolean canScroll() {
        final int childCount = ((ViewGroup)this).getChildCount();
        boolean b = false;
        if (childCount > 0) {
            final View child = ((ViewGroup)this).getChildAt(0);
            final FrameLayout$LayoutParams frameLayout$LayoutParams = (FrameLayout$LayoutParams)child.getLayoutParams();
            b = b;
            if (child.getHeight() + frameLayout$LayoutParams.topMargin + frameLayout$LayoutParams.bottomMargin > ((View)this).getHeight() - ((View)this).getPaddingTop() - ((View)this).getPaddingBottom()) {
                b = true;
            }
        }
        return b;
    }
    
    private static int clamp(final int n, final int n2, final int n3) {
        if (n2 >= n3 || n < 0) {
            return 0;
        }
        if (n2 + n > n3) {
            return n3 - n2;
        }
        return n;
    }
    
    private void doScrollY(final int n) {
        if (n != 0) {
            if (this.mSmoothScrollingEnabled) {
                this.smoothScrollBy(0, n);
            }
            else {
                ((View)this).scrollBy(0, n);
            }
        }
    }
    
    private boolean edgeEffectFling(int n) {
        if (EdgeEffectCompat.getDistance(this.mEdgeGlowTop) != 0.0f) {
            if (this.shouldAbsorb(this.mEdgeGlowTop, n)) {
                this.mEdgeGlowTop.onAbsorb(n);
            }
            else {
                this.fling(-n);
            }
        }
        else {
            if (EdgeEffectCompat.getDistance(this.mEdgeGlowBottom) == 0.0f) {
                return false;
            }
            final EdgeEffect mEdgeGlowBottom = this.mEdgeGlowBottom;
            n = -n;
            if (this.shouldAbsorb(mEdgeGlowBottom, n)) {
                this.mEdgeGlowBottom.onAbsorb(n);
            }
            else {
                this.fling(n);
            }
        }
        return true;
    }
    
    private void endDrag() {
        this.mIsBeingDragged = false;
        this.recycleVelocityTracker();
        this.stopNestedScroll(0);
        this.mEdgeGlowTop.onRelease();
        this.mEdgeGlowBottom.onRelease();
    }
    
    private View findFocusableViewInBounds(final boolean b, final int n, final int n2) {
        final ArrayList focusables = ((View)this).getFocusables(2);
        final int size = focusables.size();
        View view = null;
        int i = 0;
        int n3 = 0;
        while (i < size) {
            final View view2 = (View)focusables.get(i);
            final int top = view2.getTop();
            final int bottom = view2.getBottom();
            View view3 = view;
            int n4 = n3;
            Label_0232: {
                if (n < bottom) {
                    view3 = view;
                    n4 = n3;
                    if (top < n2) {
                        final boolean b2 = n < top && bottom < n2;
                        if (view == null) {
                            view3 = view2;
                            n4 = (b2 ? 1 : 0);
                        }
                        else {
                            final boolean b3 = (b && top < view.getTop()) || (!b && bottom > view.getBottom());
                            if (n3 != 0) {
                                view3 = view;
                                n4 = n3;
                                if (!b2) {
                                    break Label_0232;
                                }
                                view3 = view;
                                n4 = n3;
                                if (!b3) {
                                    break Label_0232;
                                }
                            }
                            else {
                                if (b2) {
                                    view3 = view2;
                                    n4 = 1;
                                    break Label_0232;
                                }
                                view3 = view;
                                n4 = n3;
                                if (!b3) {
                                    break Label_0232;
                                }
                            }
                            view3 = view2;
                            n4 = n3;
                        }
                    }
                }
            }
            ++i;
            view = view3;
            n3 = n4;
        }
        return view;
    }
    
    private float getSplineFlingDistance(final int a) {
        final double log = Math.log(Math.abs(a) * 0.35f / (this.mPhysicalCoeff * 0.015f));
        final float deceleration_RATE = NestedScrollView.DECELERATION_RATE;
        return (float)(this.mPhysicalCoeff * 0.015f * Math.exp(deceleration_RATE / (deceleration_RATE - 1.0) * log));
    }
    
    private float getVerticalScrollFactorCompat() {
        if (this.mVerticalScrollFactor == 0.0f) {
            final TypedValue typedValue = new TypedValue();
            final Context context = ((View)this).getContext();
            if (!context.getTheme().resolveAttribute(16842829, typedValue, true)) {
                throw new IllegalStateException("Expected theme to define listPreferredItemHeight.");
            }
            this.mVerticalScrollFactor = typedValue.getDimension(context.getResources().getDisplayMetrics());
        }
        return this.mVerticalScrollFactor;
    }
    
    private boolean inChild(final int n, final int n2) {
        final int childCount = ((ViewGroup)this).getChildCount();
        boolean b2;
        final boolean b = b2 = false;
        if (childCount > 0) {
            final int scrollY = ((View)this).getScrollY();
            final View child = ((ViewGroup)this).getChildAt(0);
            b2 = b;
            if (n2 >= child.getTop() - scrollY) {
                b2 = b;
                if (n2 < child.getBottom() - scrollY) {
                    b2 = b;
                    if (n >= child.getLeft()) {
                        b2 = b;
                        if (n < child.getRight()) {
                            b2 = true;
                        }
                    }
                }
            }
        }
        return b2;
    }
    
    private void initOrResetVelocityTracker() {
        final VelocityTracker mVelocityTracker = this.mVelocityTracker;
        if (mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
        else {
            mVelocityTracker.clear();
        }
    }
    
    private void initScrollView() {
        this.mScroller = new OverScroller(((View)this).getContext());
        ((View)this).setFocusable(true);
        ((ViewGroup)this).setDescendantFocusability(262144);
        ((View)this).setWillNotDraw(false);
        final ViewConfiguration value = ViewConfiguration.get(((View)this).getContext());
        this.mTouchSlop = value.getScaledTouchSlop();
        this.mMinimumVelocity = value.getScaledMinimumFlingVelocity();
        this.mMaximumVelocity = value.getScaledMaximumFlingVelocity();
    }
    
    private void initVelocityTrackerIfNotExists() {
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
    }
    
    private boolean isOffScreen(final View view) {
        return this.isWithinDeltaOfScreen(view, 0, ((View)this).getHeight()) ^ true;
    }
    
    private static boolean isViewDescendantOf(final View view, final View view2) {
        boolean b = true;
        if (view == view2) {
            return true;
        }
        final ViewParent parent = view.getParent();
        if (!(parent instanceof ViewGroup) || !isViewDescendantOf((View)parent, view2)) {
            b = false;
        }
        return b;
    }
    
    private boolean isWithinDeltaOfScreen(final View view, final int n, final int n2) {
        view.getDrawingRect(this.mTempRect);
        ((ViewGroup)this).offsetDescendantRectToMyCoords(view, this.mTempRect);
        return this.mTempRect.bottom + n >= ((View)this).getScrollY() && this.mTempRect.top - n <= ((View)this).getScrollY() + n2;
    }
    
    private void onNestedScrollInternal(final int n, final int n2, @Nullable final int[] array) {
        final int scrollY = ((View)this).getScrollY();
        ((View)this).scrollBy(0, n);
        final int n3 = ((View)this).getScrollY() - scrollY;
        if (array != null) {
            array[1] += n3;
        }
        this.mChildHelper.dispatchNestedScroll(0, n3, 0, n - n3, null, n2, array);
    }
    
    private void onSecondaryPointerUp(final MotionEvent motionEvent) {
        final int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.mActivePointerId) {
            int n;
            if (actionIndex == 0) {
                n = 1;
            }
            else {
                n = 0;
            }
            this.mLastMotionY = (int)motionEvent.getY(n);
            this.mActivePointerId = motionEvent.getPointerId(n);
            final VelocityTracker mVelocityTracker = this.mVelocityTracker;
            if (mVelocityTracker != null) {
                mVelocityTracker.clear();
            }
        }
    }
    
    private void recycleVelocityTracker() {
        final VelocityTracker mVelocityTracker = this.mVelocityTracker;
        if (mVelocityTracker != null) {
            mVelocityTracker.recycle();
            this.mVelocityTracker = null;
        }
    }
    
    private int releaseVerticalGlow(int round, float onPullDistance) {
        final float n = onPullDistance / ((View)this).getWidth();
        final float n2 = round / (float)((View)this).getHeight();
        final float distance = EdgeEffectCompat.getDistance(this.mEdgeGlowTop);
        onPullDistance = 0.0f;
        if (distance != 0.0f) {
            final float n3 = onPullDistance = -EdgeEffectCompat.onPullDistance(this.mEdgeGlowTop, -n2, n);
            if (EdgeEffectCompat.getDistance(this.mEdgeGlowTop) == 0.0f) {
                this.mEdgeGlowTop.onRelease();
                onPullDistance = n3;
            }
        }
        else if (EdgeEffectCompat.getDistance(this.mEdgeGlowBottom) != 0.0f) {
            final float n4 = onPullDistance = EdgeEffectCompat.onPullDistance(this.mEdgeGlowBottom, n2, 1.0f - n);
            if (EdgeEffectCompat.getDistance(this.mEdgeGlowBottom) == 0.0f) {
                this.mEdgeGlowBottom.onRelease();
                onPullDistance = n4;
            }
        }
        round = Math.round(onPullDistance * ((View)this).getHeight());
        if (round != 0) {
            ((View)this).invalidate();
        }
        return round;
    }
    
    private void runAnimatedScroll(final boolean b) {
        if (b) {
            this.startNestedScroll(2, 1);
        }
        else {
            this.stopNestedScroll(1);
        }
        this.mLastScrollerY = ((View)this).getScrollY();
        ViewCompat.postInvalidateOnAnimation((View)this);
    }
    
    private boolean scrollAndFocus(final int n, int n2, final int n3) {
        final int height = ((View)this).getHeight();
        final int scrollY = ((View)this).getScrollY();
        final int n4 = height + scrollY;
        final boolean b = false;
        final boolean b2 = n == 33;
        Object focusableViewInBounds;
        if ((focusableViewInBounds = this.findFocusableViewInBounds(b2, n2, n3)) == null) {
            focusableViewInBounds = this;
        }
        boolean b3;
        if (n2 >= scrollY && n3 <= n4) {
            b3 = b;
        }
        else {
            if (b2) {
                n2 -= scrollY;
            }
            else {
                n2 = n3 - n4;
            }
            this.doScrollY(n2);
            b3 = true;
        }
        if (focusableViewInBounds != ((View)this).findFocus()) {
            ((View)focusableViewInBounds).requestFocus(n);
        }
        return b3;
    }
    
    private void scrollToChild(final View view) {
        view.getDrawingRect(this.mTempRect);
        ((ViewGroup)this).offsetDescendantRectToMyCoords(view, this.mTempRect);
        final int computeScrollDeltaToGetChildRectOnScreen = this.computeScrollDeltaToGetChildRectOnScreen(this.mTempRect);
        if (computeScrollDeltaToGetChildRectOnScreen != 0) {
            ((View)this).scrollBy(0, computeScrollDeltaToGetChildRectOnScreen);
        }
    }
    
    private boolean scrollToChildRect(final Rect rect, final boolean b) {
        final int computeScrollDeltaToGetChildRectOnScreen = this.computeScrollDeltaToGetChildRectOnScreen(rect);
        final boolean b2 = computeScrollDeltaToGetChildRectOnScreen != 0;
        if (b2) {
            if (b) {
                ((View)this).scrollBy(0, computeScrollDeltaToGetChildRectOnScreen);
            }
            else {
                this.smoothScrollBy(0, computeScrollDeltaToGetChildRectOnScreen);
            }
        }
        return b2;
    }
    
    private boolean shouldAbsorb(@NonNull final EdgeEffect edgeEffect, final int n) {
        boolean b = true;
        if (n > 0) {
            return true;
        }
        if (this.getSplineFlingDistance(-n) >= EdgeEffectCompat.getDistance(edgeEffect) * ((View)this).getHeight()) {
            b = false;
        }
        return b;
    }
    
    private void smoothScrollBy(int scrollY, int max, final int n, final boolean b) {
        if (((ViewGroup)this).getChildCount() == 0) {
            return;
        }
        if (AnimationUtils.currentAnimationTimeMillis() - this.mLastScroll > 250L) {
            final View child = ((ViewGroup)this).getChildAt(0);
            final FrameLayout$LayoutParams frameLayout$LayoutParams = (FrameLayout$LayoutParams)child.getLayoutParams();
            final int height = child.getHeight();
            final int topMargin = frameLayout$LayoutParams.topMargin;
            final int bottomMargin = frameLayout$LayoutParams.bottomMargin;
            final int height2 = ((View)this).getHeight();
            final int paddingTop = ((View)this).getPaddingTop();
            final int paddingBottom = ((View)this).getPaddingBottom();
            scrollY = ((View)this).getScrollY();
            max = Math.max(0, Math.min(max + scrollY, Math.max(0, height + topMargin + bottomMargin - (height2 - paddingTop - paddingBottom))));
            this.mScroller.startScroll(((View)this).getScrollX(), scrollY, 0, max - scrollY, n);
            this.runAnimatedScroll(b);
        }
        else {
            if (!this.mScroller.isFinished()) {
                this.abortAnimatedScroll();
            }
            ((View)this).scrollBy(scrollY, max);
        }
        this.mLastScroll = AnimationUtils.currentAnimationTimeMillis();
    }
    
    private boolean stopGlowAnimations(final MotionEvent motionEvent) {
        final float distance = EdgeEffectCompat.getDistance(this.mEdgeGlowTop);
        final boolean b = true;
        boolean b2;
        if (distance != 0.0f) {
            EdgeEffectCompat.onPullDistance(this.mEdgeGlowTop, 0.0f, motionEvent.getX() / ((View)this).getWidth());
            b2 = true;
        }
        else {
            b2 = false;
        }
        if (EdgeEffectCompat.getDistance(this.mEdgeGlowBottom) != 0.0f) {
            EdgeEffectCompat.onPullDistance(this.mEdgeGlowBottom, 0.0f, 1.0f - motionEvent.getX() / ((View)this).getWidth());
            b2 = b;
        }
        return b2;
    }
    
    public void addView(@NonNull final View view) {
        if (((ViewGroup)this).getChildCount() <= 0) {
            super.addView(view);
            return;
        }
        throw new IllegalStateException("ScrollView can host only one direct child");
    }
    
    public void addView(final View view, final int n) {
        if (((ViewGroup)this).getChildCount() <= 0) {
            super.addView(view, n);
            return;
        }
        throw new IllegalStateException("ScrollView can host only one direct child");
    }
    
    public void addView(final View view, final int n, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        if (((ViewGroup)this).getChildCount() <= 0) {
            super.addView(view, n, viewGroup$LayoutParams);
            return;
        }
        throw new IllegalStateException("ScrollView can host only one direct child");
    }
    
    public void addView(final View view, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        if (((ViewGroup)this).getChildCount() <= 0) {
            super.addView(view, viewGroup$LayoutParams);
            return;
        }
        throw new IllegalStateException("ScrollView can host only one direct child");
    }
    
    public boolean arrowScroll(int descendantFocusability) {
        View focus;
        if ((focus = ((View)this).findFocus()) == this) {
            focus = null;
        }
        final View nextFocus = FocusFinder.getInstance().findNextFocus((ViewGroup)this, focus, descendantFocusability);
        final int maxScrollAmount = this.getMaxScrollAmount();
        if (nextFocus != null && this.isWithinDeltaOfScreen(nextFocus, maxScrollAmount, ((View)this).getHeight())) {
            nextFocus.getDrawingRect(this.mTempRect);
            ((ViewGroup)this).offsetDescendantRectToMyCoords(nextFocus, this.mTempRect);
            this.doScrollY(this.computeScrollDeltaToGetChildRectOnScreen(this.mTempRect));
            nextFocus.requestFocus(descendantFocusability);
        }
        else {
            int n;
            if (descendantFocusability == 33 && ((View)this).getScrollY() < maxScrollAmount) {
                n = ((View)this).getScrollY();
            }
            else {
                n = maxScrollAmount;
                if (descendantFocusability == 130) {
                    n = maxScrollAmount;
                    if (((ViewGroup)this).getChildCount() > 0) {
                        final View child = ((ViewGroup)this).getChildAt(0);
                        n = Math.min(child.getBottom() + ((FrameLayout$LayoutParams)child.getLayoutParams()).bottomMargin - (((View)this).getScrollY() + ((View)this).getHeight() - ((View)this).getPaddingBottom()), maxScrollAmount);
                    }
                }
            }
            if (n == 0) {
                return false;
            }
            if (descendantFocusability != 130) {
                n = -n;
            }
            this.doScrollY(n);
        }
        if (focus != null && focus.isFocused() && this.isOffScreen(focus)) {
            descendantFocusability = ((ViewGroup)this).getDescendantFocusability();
            ((ViewGroup)this).setDescendantFocusability(131072);
            ((View)this).requestFocus();
            ((ViewGroup)this).setDescendantFocusability(descendantFocusability);
        }
        return true;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public int computeHorizontalScrollExtent() {
        return super.computeHorizontalScrollExtent();
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public int computeHorizontalScrollOffset() {
        return super.computeHorizontalScrollOffset();
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public int computeHorizontalScrollRange() {
        return super.computeHorizontalScrollRange();
    }
    
    public void computeScroll() {
        if (this.mScroller.isFinished()) {
            return;
        }
        this.mScroller.computeScrollOffset();
        final int currY = this.mScroller.getCurrY();
        final int consumeFlingInVerticalStretch = this.consumeFlingInVerticalStretch(currY - this.mLastScrollerY);
        this.mLastScrollerY = currY;
        final int[] mScrollConsumed = this.mScrollConsumed;
        final boolean b = false;
        this.dispatchNestedPreScroll(mScrollConsumed[1] = 0, consumeFlingInVerticalStretch, mScrollConsumed, null, 1);
        final int n = consumeFlingInVerticalStretch - this.mScrollConsumed[1];
        final int scrollRange = this.getScrollRange();
        int n2;
        if ((n2 = n) != 0) {
            final int scrollY = ((View)this).getScrollY();
            this.overScrollByCompat(0, n, ((View)this).getScrollX(), scrollY, 0, scrollRange, 0, 0, false);
            final int n3 = ((View)this).getScrollY() - scrollY;
            final int n4 = n - n3;
            final int[] mScrollConsumed2 = this.mScrollConsumed;
            this.dispatchNestedScroll(mScrollConsumed2[1] = 0, n3, 0, n4, this.mScrollOffset, 1, mScrollConsumed2);
            n2 = n4 - this.mScrollConsumed[1];
        }
        if (n2 != 0) {
            final int overScrollMode = ((View)this).getOverScrollMode();
            int n5 = 0;
            Label_0189: {
                if (overScrollMode != 0) {
                    n5 = (b ? 1 : 0);
                    if (overScrollMode != 1) {
                        break Label_0189;
                    }
                    n5 = (b ? 1 : 0);
                    if (scrollRange <= 0) {
                        break Label_0189;
                    }
                }
                n5 = 1;
            }
            if (n5 != 0) {
                if (n2 < 0) {
                    if (this.mEdgeGlowTop.isFinished()) {
                        this.mEdgeGlowTop.onAbsorb((int)this.mScroller.getCurrVelocity());
                    }
                }
                else if (this.mEdgeGlowBottom.isFinished()) {
                    this.mEdgeGlowBottom.onAbsorb((int)this.mScroller.getCurrVelocity());
                }
            }
            this.abortAnimatedScroll();
        }
        if (!this.mScroller.isFinished()) {
            ViewCompat.postInvalidateOnAnimation((View)this);
        }
        else {
            this.stopNestedScroll(1);
        }
    }
    
    protected int computeScrollDeltaToGetChildRectOnScreen(final Rect rect) {
        final int childCount = ((ViewGroup)this).getChildCount();
        final boolean b = false;
        if (childCount == 0) {
            return 0;
        }
        final int height = ((View)this).getHeight();
        final int scrollY = ((View)this).getScrollY();
        final int n = scrollY + height;
        final int verticalFadingEdgeLength = ((View)this).getVerticalFadingEdgeLength();
        int n2 = scrollY;
        if (rect.top > 0) {
            n2 = scrollY + verticalFadingEdgeLength;
        }
        final View child = ((ViewGroup)this).getChildAt(0);
        final FrameLayout$LayoutParams frameLayout$LayoutParams = (FrameLayout$LayoutParams)child.getLayoutParams();
        int n3;
        if (rect.bottom < child.getHeight() + frameLayout$LayoutParams.topMargin + frameLayout$LayoutParams.bottomMargin) {
            n3 = n - verticalFadingEdgeLength;
        }
        else {
            n3 = n;
        }
        final int bottom = rect.bottom;
        int n5;
        if (bottom > n3 && rect.top > n2) {
            int n4;
            if (rect.height() > height) {
                n4 = rect.top - n2;
            }
            else {
                n4 = rect.bottom - n3;
            }
            n5 = Math.min(n4 + 0, child.getBottom() + frameLayout$LayoutParams.bottomMargin - n);
        }
        else {
            n5 = (b ? 1 : 0);
            if (rect.top < n2) {
                n5 = (b ? 1 : 0);
                if (bottom < n3) {
                    int a;
                    if (rect.height() > height) {
                        a = 0 - (n3 - rect.bottom);
                    }
                    else {
                        a = 0 - (n2 - rect.top);
                    }
                    n5 = Math.max(a, -((View)this).getScrollY());
                }
            }
        }
        return n5;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public int computeVerticalScrollExtent() {
        return super.computeVerticalScrollExtent();
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public int computeVerticalScrollOffset() {
        return Math.max(0, super.computeVerticalScrollOffset());
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public int computeVerticalScrollRange() {
        final int childCount = ((ViewGroup)this).getChildCount();
        final int n = ((View)this).getHeight() - ((View)this).getPaddingBottom() - ((View)this).getPaddingTop();
        if (childCount == 0) {
            return n;
        }
        final View child = ((ViewGroup)this).getChildAt(0);
        final int n2 = child.getBottom() + ((FrameLayout$LayoutParams)child.getLayoutParams()).bottomMargin;
        final int scrollY = ((View)this).getScrollY();
        final int max = Math.max(0, n2 - n);
        int n3;
        if (scrollY < 0) {
            n3 = n2 - scrollY;
        }
        else {
            n3 = n2;
            if (scrollY > max) {
                n3 = n2 + (scrollY - max);
            }
        }
        return n3;
    }
    
    int consumeFlingInVerticalStretch(final int n) {
        final int height = ((View)this).getHeight();
        if (n > 0 && EdgeEffectCompat.getDistance(this.mEdgeGlowTop) != 0.0f) {
            final int round = Math.round(-height / 4.0f * EdgeEffectCompat.onPullDistance(this.mEdgeGlowTop, -n * 4.0f / height, 0.5f));
            if (round != n) {
                this.mEdgeGlowTop.finish();
            }
            return n - round;
        }
        int n2;
        if ((n2 = n) < 0) {
            n2 = n;
            if (EdgeEffectCompat.getDistance(this.mEdgeGlowBottom) != 0.0f) {
                final float n3 = (float)n;
                final float n4 = (float)height;
                final int round2 = Math.round(n4 / 4.0f * EdgeEffectCompat.onPullDistance(this.mEdgeGlowBottom, n3 * 4.0f / n4, 0.5f));
                if (round2 != n) {
                    this.mEdgeGlowBottom.finish();
                }
                n2 = n - round2;
            }
        }
        return n2;
    }
    
    public boolean dispatchKeyEvent(final KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent) || this.executeKeyEvent(keyEvent);
    }
    
    public boolean dispatchNestedFling(final float n, final float n2, final boolean b) {
        return this.mChildHelper.dispatchNestedFling(n, n2, b);
    }
    
    public boolean dispatchNestedPreFling(final float n, final float n2) {
        return this.mChildHelper.dispatchNestedPreFling(n, n2);
    }
    
    public boolean dispatchNestedPreScroll(final int n, final int n2, @Nullable final int[] array, @Nullable final int[] array2) {
        return this.dispatchNestedPreScroll(n, n2, array, array2, 0);
    }
    
    public boolean dispatchNestedPreScroll(final int n, final int n2, @Nullable final int[] array, @Nullable final int[] array2, final int n3) {
        return this.mChildHelper.dispatchNestedPreScroll(n, n2, array, array2, n3);
    }
    
    public void dispatchNestedScroll(final int n, final int n2, final int n3, final int n4, @Nullable final int[] array, final int n5, @NonNull final int[] array2) {
        this.mChildHelper.dispatchNestedScroll(n, n2, n3, n4, array, n5, array2);
    }
    
    public boolean dispatchNestedScroll(final int n, final int n2, final int n3, final int n4, @Nullable final int[] array) {
        return this.mChildHelper.dispatchNestedScroll(n, n2, n3, n4, array);
    }
    
    public boolean dispatchNestedScroll(final int n, final int n2, final int n3, final int n4, @Nullable final int[] array, final int n5) {
        return this.mChildHelper.dispatchNestedScroll(n, n2, n3, n4, array, n5);
    }
    
    public void draw(@NonNull final Canvas canvas) {
        super.draw(canvas);
        final int scrollY = ((View)this).getScrollY();
        final boolean finished = this.mEdgeGlowTop.isFinished();
        final int n = 0;
        if (!finished) {
            final int save = canvas.save();
            int width = ((View)this).getWidth();
            final int height = ((View)this).getHeight();
            final int min = Math.min(0, scrollY);
            int n2;
            if (Api21Impl.getClipToPadding((ViewGroup)this)) {
                width -= ((View)this).getPaddingLeft() + ((View)this).getPaddingRight();
                n2 = ((View)this).getPaddingLeft() + 0;
            }
            else {
                n2 = 0;
            }
            int n3 = height;
            int n4 = min;
            if (Api21Impl.getClipToPadding((ViewGroup)this)) {
                n3 = height - (((View)this).getPaddingTop() + ((View)this).getPaddingBottom());
                n4 = min + ((View)this).getPaddingTop();
            }
            canvas.translate((float)n2, (float)n4);
            this.mEdgeGlowTop.setSize(width, n3);
            if (this.mEdgeGlowTop.draw(canvas)) {
                ViewCompat.postInvalidateOnAnimation((View)this);
            }
            canvas.restoreToCount(save);
        }
        if (!this.mEdgeGlowBottom.isFinished()) {
            final int save2 = canvas.save();
            final int width2 = ((View)this).getWidth();
            final int height2 = ((View)this).getHeight();
            final int n5 = Math.max(this.getScrollRange(), scrollY) + height2;
            int n6 = n;
            int n7 = width2;
            if (Api21Impl.getClipToPadding((ViewGroup)this)) {
                n7 = width2 - (((View)this).getPaddingLeft() + ((View)this).getPaddingRight());
                n6 = 0 + ((View)this).getPaddingLeft();
            }
            int n8 = n5;
            int n9 = height2;
            if (Api21Impl.getClipToPadding((ViewGroup)this)) {
                n9 = height2 - (((View)this).getPaddingTop() + ((View)this).getPaddingBottom());
                n8 = n5 - ((View)this).getPaddingBottom();
            }
            canvas.translate((float)(n6 - n7), (float)n8);
            canvas.rotate(180.0f, (float)n7, 0.0f);
            this.mEdgeGlowBottom.setSize(n7, n9);
            if (this.mEdgeGlowBottom.draw(canvas)) {
                ViewCompat.postInvalidateOnAnimation((View)this);
            }
            canvas.restoreToCount(save2);
        }
    }
    
    public boolean executeKeyEvent(@NonNull final KeyEvent keyEvent) {
        this.mTempRect.setEmpty();
        final boolean canScroll = this.canScroll();
        final boolean b = false;
        final boolean b2 = false;
        int n = 130;
        if (!canScroll) {
            boolean b3 = b2;
            if (((View)this).isFocused()) {
                b3 = b2;
                if (keyEvent.getKeyCode() != 4) {
                    View focus;
                    if ((focus = ((View)this).findFocus()) == this) {
                        focus = null;
                    }
                    final View nextFocus = FocusFinder.getInstance().findNextFocus((ViewGroup)this, focus, 130);
                    b3 = b2;
                    if (nextFocus != null) {
                        b3 = b2;
                        if (nextFocus != this) {
                            b3 = b2;
                            if (nextFocus.requestFocus(130)) {
                                b3 = true;
                            }
                        }
                    }
                }
            }
            return b3;
        }
        boolean b4 = b;
        if (keyEvent.getAction() == 0) {
            final int keyCode = keyEvent.getKeyCode();
            if (keyCode != 19) {
                if (keyCode != 20) {
                    if (keyCode != 62) {
                        b4 = b;
                    }
                    else {
                        if (keyEvent.isShiftPressed()) {
                            n = 33;
                        }
                        this.pageScroll(n);
                        b4 = b;
                    }
                }
                else if (!keyEvent.isAltPressed()) {
                    b4 = this.arrowScroll(130);
                }
                else {
                    b4 = this.fullScroll(130);
                }
            }
            else if (!keyEvent.isAltPressed()) {
                b4 = this.arrowScroll(33);
            }
            else {
                b4 = this.fullScroll(33);
            }
        }
        return b4;
    }
    
    public void fling(final int n) {
        if (((ViewGroup)this).getChildCount() > 0) {
            this.mScroller.fling(((View)this).getScrollX(), ((View)this).getScrollY(), 0, n, 0, 0, Integer.MIN_VALUE, Integer.MAX_VALUE, 0, 0);
            this.runAnimatedScroll(true);
        }
    }
    
    public boolean fullScroll(final int n) {
        final boolean b = n == 130;
        final int height = ((View)this).getHeight();
        final Rect mTempRect = this.mTempRect;
        mTempRect.top = 0;
        mTempRect.bottom = height;
        if (b) {
            final int childCount = ((ViewGroup)this).getChildCount();
            if (childCount > 0) {
                final View child = ((ViewGroup)this).getChildAt(childCount - 1);
                this.mTempRect.bottom = child.getBottom() + ((FrameLayout$LayoutParams)child.getLayoutParams()).bottomMargin + ((View)this).getPaddingBottom();
                final Rect mTempRect2 = this.mTempRect;
                mTempRect2.top = mTempRect2.bottom - height;
            }
        }
        final Rect mTempRect3 = this.mTempRect;
        return this.scrollAndFocus(n, mTempRect3.top, mTempRect3.bottom);
    }
    
    protected float getBottomFadingEdgeStrength() {
        if (((ViewGroup)this).getChildCount() == 0) {
            return 0.0f;
        }
        final View child = ((ViewGroup)this).getChildAt(0);
        final FrameLayout$LayoutParams frameLayout$LayoutParams = (FrameLayout$LayoutParams)child.getLayoutParams();
        final int verticalFadingEdgeLength = ((View)this).getVerticalFadingEdgeLength();
        final int n = child.getBottom() + frameLayout$LayoutParams.bottomMargin - ((View)this).getScrollY() - (((View)this).getHeight() - ((View)this).getPaddingBottom());
        if (n < verticalFadingEdgeLength) {
            return n / (float)verticalFadingEdgeLength;
        }
        return 1.0f;
    }
    
    public int getMaxScrollAmount() {
        return (int)(((View)this).getHeight() * 0.5f);
    }
    
    public int getNestedScrollAxes() {
        return this.mParentHelper.getNestedScrollAxes();
    }
    
    int getScrollRange() {
        final int childCount = ((ViewGroup)this).getChildCount();
        int max = 0;
        if (childCount > 0) {
            final View child = ((ViewGroup)this).getChildAt(0);
            final FrameLayout$LayoutParams frameLayout$LayoutParams = (FrameLayout$LayoutParams)child.getLayoutParams();
            max = Math.max(0, child.getHeight() + frameLayout$LayoutParams.topMargin + frameLayout$LayoutParams.bottomMargin - (((View)this).getHeight() - ((View)this).getPaddingTop() - ((View)this).getPaddingBottom()));
        }
        return max;
    }
    
    protected float getTopFadingEdgeStrength() {
        if (((ViewGroup)this).getChildCount() == 0) {
            return 0.0f;
        }
        final int verticalFadingEdgeLength = ((View)this).getVerticalFadingEdgeLength();
        final int scrollY = ((View)this).getScrollY();
        if (scrollY < verticalFadingEdgeLength) {
            return scrollY / (float)verticalFadingEdgeLength;
        }
        return 1.0f;
    }
    
    public boolean hasNestedScrollingParent() {
        return this.hasNestedScrollingParent(0);
    }
    
    public boolean hasNestedScrollingParent(final int n) {
        return this.mChildHelper.hasNestedScrollingParent(n);
    }
    
    public boolean isFillViewport() {
        return this.mFillViewport;
    }
    
    public boolean isNestedScrollingEnabled() {
        return this.mChildHelper.isNestedScrollingEnabled();
    }
    
    public boolean isSmoothScrollingEnabled() {
        return this.mSmoothScrollingEnabled;
    }
    
    protected void measureChild(@NonNull final View view, final int n, final int n2) {
        view.measure(ViewGroup.getChildMeasureSpec(n, ((View)this).getPaddingLeft() + ((View)this).getPaddingRight(), view.getLayoutParams().width), View$MeasureSpec.makeMeasureSpec(0, 0));
    }
    
    protected void measureChildWithMargins(final View view, final int n, final int n2, final int n3, final int n4) {
        final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)view.getLayoutParams();
        view.measure(ViewGroup.getChildMeasureSpec(n, ((View)this).getPaddingLeft() + ((View)this).getPaddingRight() + viewGroup$MarginLayoutParams.leftMargin + viewGroup$MarginLayoutParams.rightMargin + n2, viewGroup$MarginLayoutParams.width), View$MeasureSpec.makeMeasureSpec(viewGroup$MarginLayoutParams.topMargin + viewGroup$MarginLayoutParams.bottomMargin, 0));
    }
    
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.mIsLaidOut = false;
    }
    
    public boolean onGenericMotionEvent(@NonNull final MotionEvent motionEvent) {
        final int action = motionEvent.getAction();
        int n = 0;
        final int n2 = 0;
        boolean b = false;
        if (action == 8 && !this.mIsBeingDragged) {
            float n3;
            if (MotionEventCompat.isFromSource(motionEvent, 2)) {
                n3 = motionEvent.getAxisValue(9);
            }
            else if (MotionEventCompat.isFromSource(motionEvent, 4194304)) {
                n3 = motionEvent.getAxisValue(26);
            }
            else {
                n3 = 0.0f;
            }
            if (n3 != 0.0f) {
                final int n4 = (int)(n3 * this.getVerticalScrollFactorCompat());
                final int scrollRange = this.getScrollRange();
                final int scrollY = ((View)this).getScrollY();
                final int n5 = scrollY - n4;
                Label_0261: {
                    if (n5 < 0) {
                        if (this.canOverScroll() && !MotionEventCompat.isFromSource(motionEvent, 8194)) {
                            EdgeEffectCompat.onPullDistance(this.mEdgeGlowTop, -(float)n5 / ((View)this).getHeight(), 0.5f);
                            this.mEdgeGlowTop.onRelease();
                            ((View)this).invalidate();
                            b = true;
                            n = n2;
                            break Label_0261;
                        }
                    }
                    else {
                        if (n5 > scrollRange) {
                            if (this.canOverScroll() && !MotionEventCompat.isFromSource(motionEvent, 8194)) {
                                EdgeEffectCompat.onPullDistance(this.mEdgeGlowBottom, (n5 - scrollRange) / (float)((View)this).getHeight(), 0.5f);
                                this.mEdgeGlowBottom.onRelease();
                                ((View)this).invalidate();
                                b = true;
                            }
                            n = scrollRange;
                            break Label_0261;
                        }
                        n = n5;
                    }
                    b = false;
                }
                if (n != scrollY) {
                    super.scrollTo(((View)this).getScrollX(), n);
                    return true;
                }
                return b;
            }
        }
        return false;
    }
    
    public boolean onInterceptTouchEvent(@NonNull final MotionEvent motionEvent) {
        final int action = motionEvent.getAction();
        final boolean b = true;
        final boolean b2 = true;
        if (action == 2 && this.mIsBeingDragged) {
            return true;
        }
        final int n = action & 0xFF;
        if (n != 0) {
            if (n != 1) {
                if (n != 2) {
                    if (n != 3) {
                        if (n != 6) {
                            return this.mIsBeingDragged;
                        }
                        this.onSecondaryPointerUp(motionEvent);
                        return this.mIsBeingDragged;
                    }
                }
                else {
                    final int mActivePointerId = this.mActivePointerId;
                    if (mActivePointerId == -1) {
                        return this.mIsBeingDragged;
                    }
                    final int pointerIndex = motionEvent.findPointerIndex(mActivePointerId);
                    if (pointerIndex == -1) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Invalid pointerId=");
                        sb.append(mActivePointerId);
                        sb.append(" in onInterceptTouchEvent");
                        return this.mIsBeingDragged;
                    }
                    final int mLastMotionY = (int)motionEvent.getY(pointerIndex);
                    if (Math.abs(mLastMotionY - this.mLastMotionY) <= this.mTouchSlop || (0x2 & this.getNestedScrollAxes()) != 0x0) {
                        return this.mIsBeingDragged;
                    }
                    this.mIsBeingDragged = true;
                    this.mLastMotionY = mLastMotionY;
                    this.initVelocityTrackerIfNotExists();
                    this.mVelocityTracker.addMovement(motionEvent);
                    this.mNestedYOffset = 0;
                    final ViewParent parent = ((View)this).getParent();
                    if (parent != null) {
                        parent.requestDisallowInterceptTouchEvent(true);
                        return this.mIsBeingDragged;
                    }
                    return this.mIsBeingDragged;
                }
            }
            this.mIsBeingDragged = false;
            this.mActivePointerId = -1;
            this.recycleVelocityTracker();
            if (this.mScroller.springBack(((View)this).getScrollX(), ((View)this).getScrollY(), 0, 0, 0, this.getScrollRange())) {
                ViewCompat.postInvalidateOnAnimation((View)this);
            }
            this.stopNestedScroll(0);
        }
        else {
            final int mLastMotionY2 = (int)motionEvent.getY();
            if (!this.inChild((int)motionEvent.getX(), mLastMotionY2)) {
                boolean mIsBeingDragged = b2;
                if (!this.stopGlowAnimations(motionEvent)) {
                    mIsBeingDragged = (!this.mScroller.isFinished() && b2);
                }
                this.mIsBeingDragged = mIsBeingDragged;
                this.recycleVelocityTracker();
            }
            else {
                this.mLastMotionY = mLastMotionY2;
                this.mActivePointerId = motionEvent.getPointerId(0);
                this.initOrResetVelocityTracker();
                this.mVelocityTracker.addMovement(motionEvent);
                this.mScroller.computeScrollOffset();
                boolean mIsBeingDragged2 = b;
                if (!this.stopGlowAnimations(motionEvent)) {
                    mIsBeingDragged2 = (!this.mScroller.isFinished() && b);
                }
                this.mIsBeingDragged = mIsBeingDragged2;
                this.startNestedScroll(2, 0);
            }
        }
        return this.mIsBeingDragged;
    }
    
    protected void onLayout(final boolean b, int clamp, final int n, int scrollY, final int n2) {
        super.onLayout(b, clamp, n, scrollY, n2);
        clamp = 0;
        this.mIsLayoutDirty = false;
        final View mChildToScrollTo = this.mChildToScrollTo;
        if (mChildToScrollTo != null && isViewDescendantOf(mChildToScrollTo, (View)this)) {
            this.scrollToChild(this.mChildToScrollTo);
        }
        this.mChildToScrollTo = null;
        if (!this.mIsLaidOut) {
            if (this.mSavedState != null) {
                this.scrollTo(((View)this).getScrollX(), this.mSavedState.scrollPosition);
                this.mSavedState = null;
            }
            if (((ViewGroup)this).getChildCount() > 0) {
                final View child = ((ViewGroup)this).getChildAt(0);
                final FrameLayout$LayoutParams frameLayout$LayoutParams = (FrameLayout$LayoutParams)child.getLayoutParams();
                clamp = child.getMeasuredHeight() + frameLayout$LayoutParams.topMargin + frameLayout$LayoutParams.bottomMargin;
            }
            final int paddingTop = ((View)this).getPaddingTop();
            final int paddingBottom = ((View)this).getPaddingBottom();
            scrollY = ((View)this).getScrollY();
            clamp = clamp(scrollY, n2 - n - paddingTop - paddingBottom, clamp);
            if (clamp != scrollY) {
                this.scrollTo(((View)this).getScrollX(), clamp);
            }
        }
        this.scrollTo(((View)this).getScrollX(), ((View)this).getScrollY());
        this.mIsLaidOut = true;
    }
    
    protected void onMeasure(final int n, int n2) {
        super.onMeasure(n, n2);
        if (!this.mFillViewport) {
            return;
        }
        if (View$MeasureSpec.getMode(n2) == 0) {
            return;
        }
        if (((ViewGroup)this).getChildCount() > 0) {
            final View child = ((ViewGroup)this).getChildAt(0);
            final FrameLayout$LayoutParams frameLayout$LayoutParams = (FrameLayout$LayoutParams)child.getLayoutParams();
            final int measuredHeight = child.getMeasuredHeight();
            n2 = ((View)this).getMeasuredHeight() - ((View)this).getPaddingTop() - ((View)this).getPaddingBottom() - frameLayout$LayoutParams.topMargin - frameLayout$LayoutParams.bottomMargin;
            if (measuredHeight < n2) {
                child.measure(ViewGroup.getChildMeasureSpec(n, ((View)this).getPaddingLeft() + ((View)this).getPaddingRight() + frameLayout$LayoutParams.leftMargin + frameLayout$LayoutParams.rightMargin, frameLayout$LayoutParams.width), View$MeasureSpec.makeMeasureSpec(n2, 1073741824));
            }
        }
    }
    
    public boolean onNestedFling(@NonNull final View view, final float n, final float n2, final boolean b) {
        if (!b) {
            this.dispatchNestedFling(0.0f, n2, true);
            this.fling((int)n2);
            return true;
        }
        return false;
    }
    
    public boolean onNestedPreFling(@NonNull final View view, final float n, final float n2) {
        return this.dispatchNestedPreFling(n, n2);
    }
    
    public void onNestedPreScroll(@NonNull final View view, final int n, final int n2, @NonNull final int[] array) {
        this.onNestedPreScroll(view, n, n2, array, 0);
    }
    
    public void onNestedPreScroll(@NonNull final View view, final int n, final int n2, @NonNull final int[] array, final int n3) {
        this.dispatchNestedPreScroll(n, n2, array, null, n3);
    }
    
    public void onNestedScroll(@NonNull final View view, final int n, final int n2, final int n3, final int n4) {
        this.onNestedScrollInternal(n4, 0, null);
    }
    
    public void onNestedScroll(@NonNull final View view, final int n, final int n2, final int n3, final int n4, final int n5) {
        this.onNestedScrollInternal(n4, n5, null);
    }
    
    public void onNestedScroll(@NonNull final View view, final int n, final int n2, final int n3, final int n4, final int n5, @NonNull final int[] array) {
        this.onNestedScrollInternal(n4, n5, array);
    }
    
    public void onNestedScrollAccepted(@NonNull final View view, @NonNull final View view2, final int n) {
        this.onNestedScrollAccepted(view, view2, n, 0);
    }
    
    public void onNestedScrollAccepted(@NonNull final View view, @NonNull final View view2, final int n, final int n2) {
        this.mParentHelper.onNestedScrollAccepted(view, view2, n, n2);
        this.startNestedScroll(2, n2);
    }
    
    protected void onOverScrolled(final int n, final int n2, final boolean b, final boolean b2) {
        super.scrollTo(n, n2);
    }
    
    protected boolean onRequestFocusInDescendants(final int n, final Rect rect) {
        int n2;
        if (n == 2) {
            n2 = 130;
        }
        else if ((n2 = n) == 1) {
            n2 = 33;
        }
        View view;
        if (rect == null) {
            view = FocusFinder.getInstance().findNextFocus((ViewGroup)this, (View)null, n2);
        }
        else {
            view = FocusFinder.getInstance().findNextFocusFromRect((ViewGroup)this, rect, n2);
        }
        return view != null && !this.isOffScreen(view) && view.requestFocus(n2, rect);
    }
    
    protected void onRestoreInstanceState(final Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        final SavedState mSavedState = (SavedState)parcelable;
        super.onRestoreInstanceState(((AbsSavedState)mSavedState).getSuperState());
        this.mSavedState = mSavedState;
        this.requestLayout();
    }
    
    @NonNull
    protected Parcelable onSaveInstanceState() {
        final SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.scrollPosition = ((View)this).getScrollY();
        return (Parcelable)savedState;
    }
    
    protected void onScrollChanged(final int n, final int n2, final int n3, final int n4) {
        super.onScrollChanged(n, n2, n3, n4);
        final OnScrollChangeListener mOnScrollChangeListener = this.mOnScrollChangeListener;
        if (mOnScrollChangeListener != null) {
            mOnScrollChangeListener.onScrollChange(this, n, n2, n3, n4);
        }
    }
    
    protected void onSizeChanged(final int n, final int n2, final int n3, final int n4) {
        super.onSizeChanged(n, n2, n3, n4);
        final View focus = ((View)this).findFocus();
        if (focus != null) {
            if (this != focus) {
                if (this.isWithinDeltaOfScreen(focus, 0, n4)) {
                    focus.getDrawingRect(this.mTempRect);
                    ((ViewGroup)this).offsetDescendantRectToMyCoords(focus, this.mTempRect);
                    this.doScrollY(this.computeScrollDeltaToGetChildRectOnScreen(this.mTempRect));
                }
            }
        }
    }
    
    public boolean onStartNestedScroll(@NonNull final View view, @NonNull final View view2, final int n) {
        return this.onStartNestedScroll(view, view2, n, 0);
    }
    
    public boolean onStartNestedScroll(@NonNull final View view, @NonNull final View view2, final int n, final int n2) {
        return (n & 0x2) != 0x0;
    }
    
    public void onStopNestedScroll(@NonNull final View view) {
        this.onStopNestedScroll(view, 0);
    }
    
    public void onStopNestedScroll(@NonNull final View view, final int n) {
        this.mParentHelper.onStopNestedScroll(view, n);
        this.stopNestedScroll(n);
    }
    
    public boolean onTouchEvent(@NonNull final MotionEvent motionEvent) {
        this.initVelocityTrackerIfNotExists();
        final int actionMasked = motionEvent.getActionMasked();
        final int n = 0;
        if (actionMasked == 0) {
            this.mNestedYOffset = 0;
        }
        final MotionEvent obtain = MotionEvent.obtain(motionEvent);
        obtain.offsetLocation(0.0f, (float)this.mNestedYOffset);
        if (actionMasked != 0) {
            if (actionMasked != 1) {
                if (actionMasked != 2) {
                    if (actionMasked != 3) {
                        if (actionMasked != 5) {
                            if (actionMasked == 6) {
                                this.onSecondaryPointerUp(motionEvent);
                                this.mLastMotionY = (int)motionEvent.getY(motionEvent.findPointerIndex(this.mActivePointerId));
                            }
                        }
                        else {
                            final int actionIndex = motionEvent.getActionIndex();
                            this.mLastMotionY = (int)motionEvent.getY(actionIndex);
                            this.mActivePointerId = motionEvent.getPointerId(actionIndex);
                        }
                    }
                    else {
                        if (this.mIsBeingDragged && ((ViewGroup)this).getChildCount() > 0 && this.mScroller.springBack(((View)this).getScrollX(), ((View)this).getScrollY(), 0, 0, 0, this.getScrollRange())) {
                            ViewCompat.postInvalidateOnAnimation((View)this);
                        }
                        this.mActivePointerId = -1;
                        this.endDrag();
                    }
                }
                else {
                    final int pointerIndex = motionEvent.findPointerIndex(this.mActivePointerId);
                    if (pointerIndex == -1) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Invalid pointerId=");
                        sb.append(this.mActivePointerId);
                        sb.append(" in onTouchEvent");
                    }
                    else {
                        final int n2 = (int)motionEvent.getY(pointerIndex);
                        final int n3 = this.mLastMotionY - n2;
                        int n4;
                        final int a = n4 = n3 - this.releaseVerticalGlow(n3, motionEvent.getX(pointerIndex));
                        if (!this.mIsBeingDragged) {
                            n4 = a;
                            if (Math.abs(a) > this.mTouchSlop) {
                                final ViewParent parent = ((View)this).getParent();
                                if (parent != null) {
                                    parent.requestDisallowInterceptTouchEvent(true);
                                }
                                this.mIsBeingDragged = true;
                                if (a > 0) {
                                    n4 = a - this.mTouchSlop;
                                }
                                else {
                                    n4 = a + this.mTouchSlop;
                                }
                            }
                        }
                        if (this.mIsBeingDragged) {
                            int n5 = n4;
                            if (this.dispatchNestedPreScroll(0, n4, this.mScrollConsumed, this.mScrollOffset, 0)) {
                                n5 = n4 - this.mScrollConsumed[1];
                                this.mNestedYOffset += this.mScrollOffset[1];
                            }
                            this.mLastMotionY = n2 - this.mScrollOffset[1];
                            final int scrollY = ((View)this).getScrollY();
                            final int scrollRange = this.getScrollRange();
                            final int overScrollMode = ((View)this).getOverScrollMode();
                            final boolean b = overScrollMode == 0 || (overScrollMode == 1 && scrollRange > 0);
                            int n6;
                            if (this.overScrollByCompat(0, n5, 0, ((View)this).getScrollY(), 0, scrollRange, 0, 0, true) && !this.hasNestedScrollingParent(0)) {
                                n6 = 1;
                            }
                            else {
                                n6 = 0;
                            }
                            final int n7 = ((View)this).getScrollY() - scrollY;
                            final int[] mScrollConsumed = this.mScrollConsumed;
                            this.dispatchNestedScroll(mScrollConsumed[1] = 0, n7, 0, n5 - n7, this.mScrollOffset, 0, mScrollConsumed);
                            final int mLastMotionY = this.mLastMotionY;
                            final int n8 = this.mScrollOffset[1];
                            this.mLastMotionY = mLastMotionY - n8;
                            this.mNestedYOffset += n8;
                            if (b) {
                                final int n9 = n5 - this.mScrollConsumed[1];
                                final int n10 = scrollY + n9;
                                if (n10 < 0) {
                                    EdgeEffectCompat.onPullDistance(this.mEdgeGlowTop, -n9 / (float)((View)this).getHeight(), motionEvent.getX(pointerIndex) / ((View)this).getWidth());
                                    if (!this.mEdgeGlowBottom.isFinished()) {
                                        this.mEdgeGlowBottom.onRelease();
                                    }
                                }
                                else if (n10 > scrollRange) {
                                    EdgeEffectCompat.onPullDistance(this.mEdgeGlowBottom, n9 / (float)((View)this).getHeight(), 1.0f - motionEvent.getX(pointerIndex) / ((View)this).getWidth());
                                    if (!this.mEdgeGlowTop.isFinished()) {
                                        this.mEdgeGlowTop.onRelease();
                                    }
                                }
                                if (!this.mEdgeGlowTop.isFinished() || !this.mEdgeGlowBottom.isFinished()) {
                                    ViewCompat.postInvalidateOnAnimation((View)this);
                                    n6 = n;
                                }
                            }
                            if (n6 != 0) {
                                this.mVelocityTracker.clear();
                            }
                        }
                    }
                }
            }
            else {
                final VelocityTracker mVelocityTracker = this.mVelocityTracker;
                mVelocityTracker.computeCurrentVelocity(1000, (float)this.mMaximumVelocity);
                final int a2 = (int)mVelocityTracker.getYVelocity(this.mActivePointerId);
                if (Math.abs(a2) >= this.mMinimumVelocity) {
                    if (!this.edgeEffectFling(a2)) {
                        final int n11 = -a2;
                        final float n12 = (float)n11;
                        if (!this.dispatchNestedPreFling(0.0f, n12)) {
                            this.dispatchNestedFling(0.0f, n12, true);
                            this.fling(n11);
                        }
                    }
                }
                else if (this.mScroller.springBack(((View)this).getScrollX(), ((View)this).getScrollY(), 0, 0, 0, this.getScrollRange())) {
                    ViewCompat.postInvalidateOnAnimation((View)this);
                }
                this.mActivePointerId = -1;
                this.endDrag();
            }
        }
        else {
            if (((ViewGroup)this).getChildCount() == 0) {
                return false;
            }
            if (this.mIsBeingDragged) {
                final ViewParent parent2 = ((View)this).getParent();
                if (parent2 != null) {
                    parent2.requestDisallowInterceptTouchEvent(true);
                }
            }
            if (!this.mScroller.isFinished()) {
                this.abortAnimatedScroll();
            }
            this.mLastMotionY = (int)motionEvent.getY();
            this.mActivePointerId = motionEvent.getPointerId(0);
            this.startNestedScroll(2, 0);
        }
        final VelocityTracker mVelocityTracker2 = this.mVelocityTracker;
        if (mVelocityTracker2 != null) {
            mVelocityTracker2.addMovement(obtain);
        }
        obtain.recycle();
        return true;
    }
    
    boolean overScrollByCompat(int n, int n2, int n3, int n4, int n5, int n6, int n7, final int n8, final boolean b) {
        final int overScrollMode = ((View)this).getOverScrollMode();
        final int computeHorizontalScrollRange = this.computeHorizontalScrollRange();
        final int computeHorizontalScrollExtent = this.computeHorizontalScrollExtent();
        final boolean b2 = false;
        final boolean b3 = computeHorizontalScrollRange > computeHorizontalScrollExtent;
        final boolean b4 = this.computeVerticalScrollRange() > this.computeVerticalScrollExtent();
        final boolean b5 = overScrollMode == 0 || (overScrollMode == 1 && b3);
        final boolean b6 = overScrollMode == 0 || (overScrollMode == 1 && b4);
        n3 += n;
        if (!b5) {
            n = 0;
        }
        else {
            n = n7;
        }
        n4 += n2;
        if (!b6) {
            n2 = 0;
        }
        else {
            n2 = n8;
        }
        n7 = -n;
        n += n5;
        n5 = -n2;
        n6 += n2;
        boolean b7 = false;
        Label_0198: {
            if (n3 <= n) {
                if (n3 >= n7) {
                    b7 = false;
                    n2 = n3;
                    break Label_0198;
                }
                n = n7;
            }
            b7 = true;
            n2 = n;
        }
        boolean b8 = false;
        Label_0233: {
            if (n4 > n6) {
                n = n6;
            }
            else {
                if (n4 >= n5) {
                    b8 = false;
                    n = n4;
                    break Label_0233;
                }
                n = n5;
            }
            b8 = true;
        }
        if (b8 && !this.hasNestedScrollingParent(1)) {
            this.mScroller.springBack(n2, n, 0, 0, 0, this.getScrollRange());
        }
        this.onOverScrolled(n2, n, b7, b8);
        if (!b7) {
            final boolean b9 = b2;
            if (!b8) {
                return b9;
            }
        }
        return true;
    }
    
    public boolean pageScroll(final int n) {
        final boolean b = n == 130;
        final int height = ((View)this).getHeight();
        if (b) {
            this.mTempRect.top = ((View)this).getScrollY() + height;
            final int childCount = ((ViewGroup)this).getChildCount();
            if (childCount > 0) {
                final View child = ((ViewGroup)this).getChildAt(childCount - 1);
                final int n2 = child.getBottom() + ((FrameLayout$LayoutParams)child.getLayoutParams()).bottomMargin + ((View)this).getPaddingBottom();
                final Rect mTempRect = this.mTempRect;
                if (mTempRect.top + height > n2) {
                    mTempRect.top = n2 - height;
                }
            }
        }
        else {
            this.mTempRect.top = ((View)this).getScrollY() - height;
            final Rect mTempRect2 = this.mTempRect;
            if (mTempRect2.top < 0) {
                mTempRect2.top = 0;
            }
        }
        final Rect mTempRect3 = this.mTempRect;
        final int top = mTempRect3.top;
        final int bottom = height + top;
        mTempRect3.bottom = bottom;
        return this.scrollAndFocus(n, top, bottom);
    }
    
    public void requestChildFocus(final View view, final View mChildToScrollTo) {
        if (!this.mIsLayoutDirty) {
            this.scrollToChild(mChildToScrollTo);
        }
        else {
            this.mChildToScrollTo = mChildToScrollTo;
        }
        super.requestChildFocus(view, mChildToScrollTo);
    }
    
    public boolean requestChildRectangleOnScreen(@NonNull final View view, final Rect rect, final boolean b) {
        rect.offset(view.getLeft() - view.getScrollX(), view.getTop() - view.getScrollY());
        return this.scrollToChildRect(rect, b);
    }
    
    public void requestDisallowInterceptTouchEvent(final boolean b) {
        if (b) {
            this.recycleVelocityTracker();
        }
        super.requestDisallowInterceptTouchEvent(b);
    }
    
    public void requestLayout() {
        this.mIsLayoutDirty = true;
        super.requestLayout();
    }
    
    public void scrollTo(int clamp, int clamp2) {
        if (((ViewGroup)this).getChildCount() > 0) {
            final View child = ((ViewGroup)this).getChildAt(0);
            final FrameLayout$LayoutParams frameLayout$LayoutParams = (FrameLayout$LayoutParams)child.getLayoutParams();
            final int width = ((View)this).getWidth();
            final int paddingLeft = ((View)this).getPaddingLeft();
            final int paddingRight = ((View)this).getPaddingRight();
            final int width2 = child.getWidth();
            final int leftMargin = frameLayout$LayoutParams.leftMargin;
            final int rightMargin = frameLayout$LayoutParams.rightMargin;
            final int height = ((View)this).getHeight();
            final int paddingTop = ((View)this).getPaddingTop();
            final int paddingBottom = ((View)this).getPaddingBottom();
            final int height2 = child.getHeight();
            final int topMargin = frameLayout$LayoutParams.topMargin;
            final int bottomMargin = frameLayout$LayoutParams.bottomMargin;
            clamp = clamp(clamp, width - paddingLeft - paddingRight, width2 + leftMargin + rightMargin);
            clamp2 = clamp(clamp2, height - paddingTop - paddingBottom, height2 + topMargin + bottomMargin);
            if (clamp != ((View)this).getScrollX() || clamp2 != ((View)this).getScrollY()) {
                super.scrollTo(clamp, clamp2);
            }
        }
    }
    
    public void setFillViewport(final boolean mFillViewport) {
        if (mFillViewport != this.mFillViewport) {
            this.mFillViewport = mFillViewport;
            this.requestLayout();
        }
    }
    
    public void setNestedScrollingEnabled(final boolean nestedScrollingEnabled) {
        this.mChildHelper.setNestedScrollingEnabled(nestedScrollingEnabled);
    }
    
    public void setOnScrollChangeListener(@Nullable final OnScrollChangeListener mOnScrollChangeListener) {
        this.mOnScrollChangeListener = mOnScrollChangeListener;
    }
    
    public void setSmoothScrollingEnabled(final boolean mSmoothScrollingEnabled) {
        this.mSmoothScrollingEnabled = mSmoothScrollingEnabled;
    }
    
    public boolean shouldDelayChildPressedState() {
        return true;
    }
    
    public final void smoothScrollBy(final int n, final int n2) {
        this.smoothScrollBy(n, n2, 250, false);
    }
    
    public final void smoothScrollBy(final int n, final int n2, final int n3) {
        this.smoothScrollBy(n, n2, n3, false);
    }
    
    public final void smoothScrollTo(final int n, final int n2) {
        this.smoothScrollTo(n, n2, 250, false);
    }
    
    public final void smoothScrollTo(final int n, final int n2, final int n3) {
        this.smoothScrollTo(n, n2, n3, false);
    }
    
    void smoothScrollTo(final int n, final int n2, final int n3, final boolean b) {
        this.smoothScrollBy(n - ((View)this).getScrollX(), n2 - ((View)this).getScrollY(), n3, b);
    }
    
    void smoothScrollTo(final int n, final int n2, final boolean b) {
        this.smoothScrollTo(n, n2, 250, b);
    }
    
    public boolean startNestedScroll(final int n) {
        return this.startNestedScroll(n, 0);
    }
    
    public boolean startNestedScroll(final int n, final int n2) {
        return this.mChildHelper.startNestedScroll(n, n2);
    }
    
    public void stopNestedScroll() {
        this.stopNestedScroll(0);
    }
    
    public void stopNestedScroll(final int n) {
        this.mChildHelper.stopNestedScroll(n);
    }
    
    static class AccessibilityDelegate extends AccessibilityDelegateCompat
    {
        @Override
        public void onInitializeAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(view, accessibilityEvent);
            final NestedScrollView nestedScrollView = (NestedScrollView)view;
            ((AccessibilityRecord)accessibilityEvent).setClassName((CharSequence)ScrollView.class.getName());
            ((AccessibilityRecord)accessibilityEvent).setScrollable(nestedScrollView.getScrollRange() > 0);
            ((AccessibilityRecord)accessibilityEvent).setScrollX(((View)nestedScrollView).getScrollX());
            ((AccessibilityRecord)accessibilityEvent).setScrollY(((View)nestedScrollView).getScrollY());
            AccessibilityRecordCompat.setMaxScrollX((AccessibilityRecord)accessibilityEvent, ((View)nestedScrollView).getScrollX());
            AccessibilityRecordCompat.setMaxScrollY((AccessibilityRecord)accessibilityEvent, nestedScrollView.getScrollRange());
        }
        
        @Override
        public void onInitializeAccessibilityNodeInfo(final View view, final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            super.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat);
            final NestedScrollView nestedScrollView = (NestedScrollView)view;
            accessibilityNodeInfoCompat.setClassName(ScrollView.class.getName());
            if (((View)nestedScrollView).isEnabled()) {
                final int scrollRange = nestedScrollView.getScrollRange();
                if (scrollRange > 0) {
                    accessibilityNodeInfoCompat.setScrollable(true);
                    if (((View)nestedScrollView).getScrollY() > 0) {
                        accessibilityNodeInfoCompat.addAction(AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_SCROLL_BACKWARD);
                        accessibilityNodeInfoCompat.addAction(AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_SCROLL_UP);
                    }
                    if (((View)nestedScrollView).getScrollY() < scrollRange) {
                        accessibilityNodeInfoCompat.addAction(AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_SCROLL_FORWARD);
                        accessibilityNodeInfoCompat.addAction(AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_SCROLL_DOWN);
                    }
                }
            }
        }
        
        @Override
        public boolean performAccessibilityAction(final View view, int n, final Bundle bundle) {
            if (super.performAccessibilityAction(view, n, bundle)) {
                return true;
            }
            final NestedScrollView nestedScrollView = (NestedScrollView)view;
            if (!((View)nestedScrollView).isEnabled()) {
                return false;
            }
            final int height = ((View)nestedScrollView).getHeight();
            final Rect rect = new Rect();
            int height2 = height;
            if (((View)nestedScrollView).getMatrix().isIdentity()) {
                height2 = height;
                if (((View)nestedScrollView).getGlobalVisibleRect(rect)) {
                    height2 = rect.height();
                }
            }
            if (n != 4096) {
                if (n != 8192 && n != 16908344) {
                    if (n != 16908346) {
                        return false;
                    }
                }
                else {
                    n = ((View)nestedScrollView).getPaddingBottom();
                    n = Math.max(((View)nestedScrollView).getScrollY() - (height2 - n - ((View)nestedScrollView).getPaddingTop()), 0);
                    if (n != ((View)nestedScrollView).getScrollY()) {
                        nestedScrollView.smoothScrollTo(0, n, true);
                        return true;
                    }
                    return false;
                }
            }
            n = ((View)nestedScrollView).getPaddingBottom();
            n = Math.min(((View)nestedScrollView).getScrollY() + (height2 - n - ((View)nestedScrollView).getPaddingTop()), nestedScrollView.getScrollRange());
            if (n != ((View)nestedScrollView).getScrollY()) {
                nestedScrollView.smoothScrollTo(0, n, true);
                return true;
            }
            return false;
        }
    }
    
    @RequiresApi(21)
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        @DoNotInline
        static boolean getClipToPadding(final ViewGroup viewGroup) {
            return viewGroup.getClipToPadding();
        }
    }
    
    public interface OnScrollChangeListener
    {
        void onScrollChange(@NonNull final NestedScrollView p0, final int p1, final int p2, final int p3, final int p4);
    }
    
    static class SavedState extends View$BaseSavedState
    {
        public static final Parcelable$Creator<SavedState> CREATOR;
        public int scrollPosition;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$Creator<SavedState>() {
                public SavedState createFromParcel(final Parcel parcel) {
                    return new SavedState(parcel);
                }
                
                public SavedState[] newArray(final int n) {
                    return new SavedState[n];
                }
            };
        }
        
        SavedState(final Parcel parcel) {
            super(parcel);
            this.scrollPosition = parcel.readInt();
        }
        
        SavedState(final Parcelable parcelable) {
            super(parcelable);
        }
        
        @NonNull
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("HorizontalScrollView.SavedState{");
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            sb.append(" scrollPosition=");
            sb.append(this.scrollPosition);
            sb.append("}");
            return sb.toString();
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeInt(this.scrollPosition);
        }
    }
}

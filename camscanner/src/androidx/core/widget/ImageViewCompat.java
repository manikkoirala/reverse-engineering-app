// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import android.view.View;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.graphics.drawable.Drawable;
import android.os.Build$VERSION;
import android.graphics.PorterDuff$Mode;
import androidx.annotation.Nullable;
import android.content.res.ColorStateList;
import androidx.annotation.NonNull;
import android.widget.ImageView;

public class ImageViewCompat
{
    private ImageViewCompat() {
    }
    
    @Nullable
    public static ColorStateList getImageTintList(@NonNull final ImageView imageView) {
        return Api21Impl.getImageTintList(imageView);
    }
    
    @Nullable
    public static PorterDuff$Mode getImageTintMode(@NonNull final ImageView imageView) {
        return Api21Impl.getImageTintMode(imageView);
    }
    
    public static void setImageTintList(@NonNull final ImageView imageView, @Nullable final ColorStateList list) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        Api21Impl.setImageTintList(imageView, list);
        if (sdk_INT == 21) {
            final Drawable drawable = imageView.getDrawable();
            if (drawable != null && Api21Impl.getImageTintList(imageView) != null) {
                if (drawable.isStateful()) {
                    drawable.setState(((View)imageView).getDrawableState());
                }
                imageView.setImageDrawable(drawable);
            }
        }
    }
    
    public static void setImageTintMode(@NonNull final ImageView imageView, @Nullable final PorterDuff$Mode porterDuff$Mode) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        Api21Impl.setImageTintMode(imageView, porterDuff$Mode);
        if (sdk_INT == 21) {
            final Drawable drawable = imageView.getDrawable();
            if (drawable != null && Api21Impl.getImageTintList(imageView) != null) {
                if (drawable.isStateful()) {
                    drawable.setState(((View)imageView).getDrawableState());
                }
                imageView.setImageDrawable(drawable);
            }
        }
    }
    
    @RequiresApi(21)
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        @DoNotInline
        static ColorStateList getImageTintList(final ImageView imageView) {
            return imageView.getImageTintList();
        }
        
        @DoNotInline
        static PorterDuff$Mode getImageTintMode(final ImageView imageView) {
            return imageView.getImageTintMode();
        }
        
        @DoNotInline
        static void setImageTintList(final ImageView imageView, final ColorStateList imageTintList) {
            imageView.setImageTintList(imageTintList);
        }
        
        @DoNotInline
        static void setImageTintMode(final ImageView imageView, final PorterDuff$Mode imageTintMode) {
            imageView.setImageTintMode(imageTintMode);
        }
    }
}

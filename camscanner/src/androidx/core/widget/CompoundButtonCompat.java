// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import androidx.annotation.Nullable;
import android.os.Build$VERSION;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import android.widget.CompoundButton;
import java.lang.reflect.Field;

public final class CompoundButtonCompat
{
    private static final String TAG = "CompoundButtonCompat";
    private static Field sButtonDrawableField;
    private static boolean sButtonDrawableFieldFetched;
    
    private CompoundButtonCompat() {
    }
    
    @Nullable
    public static Drawable getButtonDrawable(@NonNull final CompoundButton obj) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getButtonDrawable((CompoundButton)obj);
        }
        Label_0040: {
            if (CompoundButtonCompat.sButtonDrawableFieldFetched) {
                break Label_0040;
            }
            while (true) {
                try {
                    (CompoundButtonCompat.sButtonDrawableField = CompoundButton.class.getDeclaredField("mButtonDrawable")).setAccessible(true);
                    CompoundButtonCompat.sButtonDrawableFieldFetched = true;
                    final Field sButtonDrawableField = CompoundButtonCompat.sButtonDrawableField;
                    if (sButtonDrawableField != null) {
                        try {
                            return (Drawable)sButtonDrawableField.get(obj);
                        }
                        catch (final IllegalAccessException obj) {
                            CompoundButtonCompat.sButtonDrawableField = null;
                        }
                    }
                    return null;
                }
                catch (final NoSuchFieldException ex) {
                    continue;
                }
                break;
            }
        }
    }
    
    @Nullable
    public static ColorStateList getButtonTintList(@NonNull final CompoundButton compoundButton) {
        return Api21Impl.getButtonTintList(compoundButton);
    }
    
    @Nullable
    public static PorterDuff$Mode getButtonTintMode(@NonNull final CompoundButton compoundButton) {
        return Api21Impl.getButtonTintMode(compoundButton);
    }
    
    public static void setButtonTintList(@NonNull final CompoundButton compoundButton, @Nullable final ColorStateList list) {
        Api21Impl.setButtonTintList(compoundButton, list);
    }
    
    public static void setButtonTintMode(@NonNull final CompoundButton compoundButton, @Nullable final PorterDuff$Mode porterDuff$Mode) {
        Api21Impl.setButtonTintMode(compoundButton, porterDuff$Mode);
    }
    
    @RequiresApi(21)
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        @DoNotInline
        static ColorStateList getButtonTintList(final CompoundButton compoundButton) {
            return compoundButton.getButtonTintList();
        }
        
        @DoNotInline
        static PorterDuff$Mode getButtonTintMode(final CompoundButton compoundButton) {
            return compoundButton.getButtonTintMode();
        }
        
        @DoNotInline
        static void setButtonTintList(final CompoundButton compoundButton, final ColorStateList buttonTintList) {
            compoundButton.setButtonTintList(buttonTintList);
        }
        
        @DoNotInline
        static void setButtonTintMode(final CompoundButton compoundButton, final PorterDuff$Mode buttonTintMode) {
            compoundButton.setButtonTintMode(buttonTintMode);
        }
    }
    
    @RequiresApi(23)
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        @DoNotInline
        static Drawable getButtonDrawable(final CompoundButton compoundButton) {
            return \u3007080.\u3007080(compoundButton);
        }
    }
}

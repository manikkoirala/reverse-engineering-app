// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import android.view.ViewGroup;
import android.widget.AdapterView;
import android.view.View;
import androidx.annotation.NonNull;
import android.widget.ListView;

public class ListViewAutoScrollHelper extends AutoScrollHelper
{
    private final ListView mTarget;
    
    public ListViewAutoScrollHelper(@NonNull final ListView mTarget) {
        super((View)mTarget);
        this.mTarget = mTarget;
    }
    
    @Override
    public boolean canTargetScrollHorizontally(final int n) {
        return false;
    }
    
    @Override
    public boolean canTargetScrollVertically(final int n) {
        final ListView mTarget = this.mTarget;
        final int count = ((AdapterView)mTarget).getCount();
        if (count == 0) {
            return false;
        }
        final int childCount = ((ViewGroup)mTarget).getChildCount();
        final int firstVisiblePosition = ((AdapterView)mTarget).getFirstVisiblePosition();
        if (n > 0) {
            if (firstVisiblePosition + childCount >= count && ((ViewGroup)mTarget).getChildAt(childCount - 1).getBottom() <= ((View)mTarget).getHeight()) {
                return false;
            }
        }
        else {
            if (n >= 0) {
                return false;
            }
            if (firstVisiblePosition <= 0 && ((ViewGroup)mTarget).getChildAt(0).getTop() >= 0) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public void scrollTargetBy(final int n, final int n2) {
        ListViewCompat.scrollListBy(this.mTarget, n2);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import android.view.View;
import androidx.annotation.UiThread;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import androidx.annotation.NonNull;
import android.content.Context;
import android.widget.ProgressBar;

public class ContentLoadingProgressBar extends ProgressBar
{
    private static final int MIN_DELAY_MS = 500;
    private static final int MIN_SHOW_TIME_MS = 500;
    private final Runnable mDelayedHide;
    private final Runnable mDelayedShow;
    boolean mDismissed;
    boolean mPostedHide;
    boolean mPostedShow;
    long mStartTime;
    
    public ContentLoadingProgressBar(@NonNull final Context context) {
        this(context, null);
    }
    
    public ContentLoadingProgressBar(@NonNull final Context context, @Nullable final AttributeSet set) {
        super(context, set, 0);
        this.mStartTime = -1L;
        this.mPostedHide = false;
        this.mPostedShow = false;
        this.mDismissed = false;
        this.mDelayedHide = new O8(this);
        this.mDelayedShow = new Oo08(this);
    }
    
    @UiThread
    private void hideOnUiThread() {
        this.mDismissed = true;
        ((View)this).removeCallbacks(this.mDelayedShow);
        this.mPostedShow = false;
        final long currentTimeMillis = System.currentTimeMillis();
        final long mStartTime = this.mStartTime;
        final long n = currentTimeMillis - mStartTime;
        if (n < 500L && mStartTime != -1L) {
            if (!this.mPostedHide) {
                ((View)this).postDelayed(this.mDelayedHide, 500L - n);
                this.mPostedHide = true;
            }
        }
        else {
            ((View)this).setVisibility(8);
        }
    }
    
    private void removeCallbacks() {
        ((View)this).removeCallbacks(this.mDelayedHide);
        ((View)this).removeCallbacks(this.mDelayedShow);
    }
    
    @UiThread
    private void showOnUiThread() {
        this.mStartTime = -1L;
        this.mDismissed = false;
        ((View)this).removeCallbacks(this.mDelayedHide);
        this.mPostedHide = false;
        if (!this.mPostedShow) {
            ((View)this).postDelayed(this.mDelayedShow, 500L);
            this.mPostedShow = true;
        }
    }
    
    public void hide() {
        ((View)this).post((Runnable)new \u3007o\u3007(this));
    }
    
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.removeCallbacks();
    }
    
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.removeCallbacks();
    }
    
    public void show() {
        ((View)this).post((Runnable)new \u3007o00\u3007\u3007Oo(this));
    }
}

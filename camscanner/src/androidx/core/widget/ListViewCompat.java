// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import android.widget.AbsListView;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import androidx.annotation.NonNull;
import android.widget.ListView;

public final class ListViewCompat
{
    private ListViewCompat() {
    }
    
    public static boolean canScrollList(@NonNull final ListView listView, final int n) {
        return Api19Impl.canScrollList(listView, n);
    }
    
    public static void scrollListBy(@NonNull final ListView listView, final int n) {
        Api19Impl.scrollListBy(listView, n);
    }
    
    @RequiresApi(19)
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        @DoNotInline
        static boolean canScrollList(final ListView listView, final int n) {
            return ((AbsListView)listView).canScrollList(n);
        }
        
        @DoNotInline
        static void scrollListBy(final ListView listView, final int n) {
            ((AbsListView)listView).scrollListBy(n);
        }
    }
}

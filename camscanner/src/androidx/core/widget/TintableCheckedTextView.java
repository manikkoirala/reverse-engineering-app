// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import android.graphics.PorterDuff$Mode;
import androidx.annotation.Nullable;
import android.content.res.ColorStateList;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public interface TintableCheckedTextView
{
    @Nullable
    ColorStateList getSupportCheckMarkTintList();
    
    @Nullable
    PorterDuff$Mode getSupportCheckMarkTintMode();
    
    void setSupportCheckMarkTintList(@Nullable final ColorStateList p0);
    
    void setSupportCheckMarkTintMode(@Nullable final PorterDuff$Mode p0);
}

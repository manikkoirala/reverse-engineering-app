// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import android.view.ActionMode;
import android.view.MenuItem;
import java.lang.reflect.InvocationTargetException;
import android.view.Menu;
import android.text.Editable;
import java.util.Iterator;
import android.app.Activity;
import java.util.ArrayList;
import java.util.List;
import android.content.pm.PackageManager;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import java.lang.reflect.Method;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.text.PrecomputedText$Params;
import androidx.appcompat.widget.\u30070000OOO;
import androidx.appcompat.widget.O8ooOoo\u3007;
import androidx.appcompat.widget.\u3007oOO8O8;
import android.icu.text.DecimalFormatSymbols;
import androidx.appcompat.widget.o\u30078;
import androidx.appcompat.widget.\u3007o;
import java.util.Locale;
import androidx.annotation.DoNotInline;
import androidx.annotation.RestrictTo;
import androidx.annotation.StyleRes;
import android.graphics.Paint$FontMetricsInt;
import androidx.annotation.Px;
import androidx.annotation.IntRange;
import android.view.ActionMode$Callback;
import androidx.annotation.DrawableRes;
import android.graphics.Paint;
import android.text.TextPaint;
import androidx.core.text.PrecomputedTextCompat;
import android.view.View;
import android.text.method.PasswordTransformationMethod;
import androidx.annotation.RequiresApi;
import android.text.TextDirectionHeuristics;
import android.text.TextDirectionHeuristic;
import android.graphics.drawable.Drawable;
import android.graphics.PorterDuff$Mode;
import androidx.annotation.Nullable;
import androidx.core.util.Preconditions;
import android.content.res.ColorStateList;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.widget.TextView;
import java.lang.reflect.Field;

public final class TextViewCompat
{
    public static final int AUTO_SIZE_TEXT_TYPE_NONE = 0;
    public static final int AUTO_SIZE_TEXT_TYPE_UNIFORM = 1;
    private static final int LINES = 1;
    private static final String LOG_TAG = "TextViewCompat";
    private static Field sMaxModeField;
    private static boolean sMaxModeFieldFetched;
    private static Field sMaximumField;
    private static boolean sMaximumFieldFetched;
    private static Field sMinModeField;
    private static boolean sMinModeFieldFetched;
    private static Field sMinimumField;
    private static boolean sMinimumFieldFetched;
    
    private TextViewCompat() {
    }
    
    public static int getAutoSizeMaxTextSize(@NonNull final TextView textView) {
        if (Build$VERSION.SDK_INT >= 27) {
            return Api26Impl.getAutoSizeMaxTextSize(textView);
        }
        if (textView instanceof AutoSizeableTextView) {
            return ((AutoSizeableTextView)textView).getAutoSizeMaxTextSize();
        }
        return -1;
    }
    
    public static int getAutoSizeMinTextSize(@NonNull final TextView textView) {
        if (Build$VERSION.SDK_INT >= 27) {
            return Api26Impl.getAutoSizeMinTextSize(textView);
        }
        if (textView instanceof AutoSizeableTextView) {
            return ((AutoSizeableTextView)textView).getAutoSizeMinTextSize();
        }
        return -1;
    }
    
    public static int getAutoSizeStepGranularity(@NonNull final TextView textView) {
        if (Build$VERSION.SDK_INT >= 27) {
            return Api26Impl.getAutoSizeStepGranularity(textView);
        }
        if (textView instanceof AutoSizeableTextView) {
            return ((AutoSizeableTextView)textView).getAutoSizeStepGranularity();
        }
        return -1;
    }
    
    @NonNull
    public static int[] getAutoSizeTextAvailableSizes(@NonNull final TextView textView) {
        if (Build$VERSION.SDK_INT >= 27) {
            return Api26Impl.getAutoSizeTextAvailableSizes(textView);
        }
        if (textView instanceof AutoSizeableTextView) {
            return ((AutoSizeableTextView)textView).getAutoSizeTextAvailableSizes();
        }
        return new int[0];
    }
    
    public static int getAutoSizeTextType(@NonNull final TextView textView) {
        if (Build$VERSION.SDK_INT >= 27) {
            return Api26Impl.getAutoSizeTextType(textView);
        }
        if (textView instanceof AutoSizeableTextView) {
            return ((AutoSizeableTextView)textView).getAutoSizeTextType();
        }
        return 0;
    }
    
    @Nullable
    public static ColorStateList getCompoundDrawableTintList(@NonNull final TextView textView) {
        Preconditions.checkNotNull(textView);
        if (Build$VERSION.SDK_INT >= 24) {
            return Api23Impl.getCompoundDrawableTintList(textView);
        }
        if (textView instanceof TintableCompoundDrawablesView) {
            return ((TintableCompoundDrawablesView)textView).getSupportCompoundDrawablesTintList();
        }
        return null;
    }
    
    @Nullable
    public static PorterDuff$Mode getCompoundDrawableTintMode(@NonNull final TextView textView) {
        Preconditions.checkNotNull(textView);
        if (Build$VERSION.SDK_INT >= 24) {
            return Api23Impl.getCompoundDrawableTintMode(textView);
        }
        if (textView instanceof TintableCompoundDrawablesView) {
            return ((TintableCompoundDrawablesView)textView).getSupportCompoundDrawablesTintMode();
        }
        return null;
    }
    
    @NonNull
    public static Drawable[] getCompoundDrawablesRelative(@NonNull final TextView textView) {
        return Api17Impl.getCompoundDrawablesRelative(textView);
    }
    
    public static int getFirstBaselineToTopHeight(@NonNull final TextView textView) {
        return ((View)textView).getPaddingTop() - ((Paint)textView.getPaint()).getFontMetricsInt().top;
    }
    
    public static int getLastBaselineToBottomHeight(@NonNull final TextView textView) {
        return ((View)textView).getPaddingBottom() + ((Paint)textView.getPaint()).getFontMetricsInt().bottom;
    }
    
    public static int getMaxLines(@NonNull final TextView textView) {
        return Api16Impl.getMaxLines(textView);
    }
    
    public static int getMinLines(@NonNull final TextView textView) {
        return Api16Impl.getMinLines(textView);
    }
    
    @RequiresApi(18)
    private static int getTextDirection(@NonNull final TextDirectionHeuristic textDirectionHeuristic) {
        if (textDirectionHeuristic == TextDirectionHeuristics.FIRSTSTRONG_RTL) {
            return 1;
        }
        if (textDirectionHeuristic == TextDirectionHeuristics.FIRSTSTRONG_LTR) {
            return 1;
        }
        if (textDirectionHeuristic == TextDirectionHeuristics.ANYRTL_LTR) {
            return 2;
        }
        if (textDirectionHeuristic == TextDirectionHeuristics.LTR) {
            return 3;
        }
        if (textDirectionHeuristic == TextDirectionHeuristics.RTL) {
            return 4;
        }
        if (textDirectionHeuristic == TextDirectionHeuristics.LOCALE) {
            return 5;
        }
        if (textDirectionHeuristic == TextDirectionHeuristics.FIRSTSTRONG_LTR) {
            return 6;
        }
        if (textDirectionHeuristic == TextDirectionHeuristics.FIRSTSTRONG_RTL) {
            return 7;
        }
        return 1;
    }
    
    @RequiresApi(18)
    private static TextDirectionHeuristic getTextDirectionHeuristic(@NonNull final TextView textView) {
        if (textView.getTransformationMethod() instanceof PasswordTransformationMethod) {
            return TextDirectionHeuristics.LTR;
        }
        final int sdk_INT = Build$VERSION.SDK_INT;
        boolean b = false;
        if (sdk_INT >= 28 && (textView.getInputType() & 0xF) == 0x3) {
            final byte directionality = Character.getDirectionality(Api28Impl.getDigitStrings(Api24Impl.getInstance(Api17Impl.getTextLocale(textView)))[0].codePointAt(0));
            if (directionality != 1 && directionality != 2) {
                return TextDirectionHeuristics.LTR;
            }
            return TextDirectionHeuristics.RTL;
        }
        else {
            if (Api17Impl.getLayoutDirection((View)textView) == 1) {
                b = true;
            }
            switch (Api17Impl.getTextDirection((View)textView)) {
                default: {
                    TextDirectionHeuristic textDirectionHeuristic;
                    if (b) {
                        textDirectionHeuristic = TextDirectionHeuristics.FIRSTSTRONG_RTL;
                    }
                    else {
                        textDirectionHeuristic = TextDirectionHeuristics.FIRSTSTRONG_LTR;
                    }
                    return textDirectionHeuristic;
                }
                case 7: {
                    return TextDirectionHeuristics.FIRSTSTRONG_RTL;
                }
                case 6: {
                    return TextDirectionHeuristics.FIRSTSTRONG_LTR;
                }
                case 5: {
                    return TextDirectionHeuristics.LOCALE;
                }
                case 4: {
                    return TextDirectionHeuristics.RTL;
                }
                case 3: {
                    return TextDirectionHeuristics.LTR;
                }
                case 2: {
                    return TextDirectionHeuristics.ANYRTL_LTR;
                }
            }
        }
    }
    
    @NonNull
    public static PrecomputedTextCompat.Params getTextMetricsParams(@NonNull final TextView textView) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 28) {
            return new PrecomputedTextCompat.Params(Api28Impl.getTextMetricsParams(textView));
        }
        final PrecomputedTextCompat.Params.Builder builder = new PrecomputedTextCompat.Params.Builder(new TextPaint((Paint)textView.getPaint()));
        if (sdk_INT >= 23) {
            builder.setBreakStrategy(Api23Impl.getBreakStrategy(textView));
            builder.setHyphenationFrequency(Api23Impl.getHyphenationFrequency(textView));
        }
        builder.setTextDirection(getTextDirectionHeuristic(textView));
        return builder.build();
    }
    
    private static Field retrieveField(final String s) {
        Field declaredField = null;
        try {
            final Field field = declaredField = TextView.class.getDeclaredField(s);
            field.setAccessible(true);
            declaredField = field;
        }
        catch (final NoSuchFieldException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Could not retrieve ");
            sb.append(s);
            sb.append(" field.");
        }
        return declaredField;
    }
    
    private static int retrieveIntFromField(final Field field, final TextView obj) {
        try {
            return field.getInt(obj);
        }
        catch (final IllegalAccessException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Could not retrieve value of ");
            sb.append(field.getName());
            sb.append(" field.");
            return -1;
        }
    }
    
    public static void setAutoSizeTextTypeUniformWithConfiguration(@NonNull final TextView textView, final int n, final int n2, final int n3, final int n4) throws IllegalArgumentException {
        if (Build$VERSION.SDK_INT >= 27) {
            Api26Impl.setAutoSizeTextTypeUniformWithConfiguration(textView, n, n2, n3, n4);
        }
        else if (textView instanceof AutoSizeableTextView) {
            ((AutoSizeableTextView)textView).setAutoSizeTextTypeUniformWithConfiguration(n, n2, n3, n4);
        }
    }
    
    public static void setAutoSizeTextTypeUniformWithPresetSizes(@NonNull final TextView textView, @NonNull final int[] array, final int n) throws IllegalArgumentException {
        if (Build$VERSION.SDK_INT >= 27) {
            Api26Impl.setAutoSizeTextTypeUniformWithPresetSizes(textView, array, n);
        }
        else if (textView instanceof AutoSizeableTextView) {
            ((AutoSizeableTextView)textView).setAutoSizeTextTypeUniformWithPresetSizes(array, n);
        }
    }
    
    public static void setAutoSizeTextTypeWithDefaults(@NonNull final TextView textView, final int autoSizeTextTypeWithDefaults) {
        if (Build$VERSION.SDK_INT >= 27) {
            Api26Impl.setAutoSizeTextTypeWithDefaults(textView, autoSizeTextTypeWithDefaults);
        }
        else if (textView instanceof AutoSizeableTextView) {
            ((AutoSizeableTextView)textView).setAutoSizeTextTypeWithDefaults(autoSizeTextTypeWithDefaults);
        }
    }
    
    public static void setCompoundDrawableTintList(@NonNull final TextView textView, @Nullable final ColorStateList supportCompoundDrawablesTintList) {
        Preconditions.checkNotNull(textView);
        if (Build$VERSION.SDK_INT >= 24) {
            Api23Impl.setCompoundDrawableTintList(textView, supportCompoundDrawablesTintList);
        }
        else if (textView instanceof TintableCompoundDrawablesView) {
            ((TintableCompoundDrawablesView)textView).setSupportCompoundDrawablesTintList(supportCompoundDrawablesTintList);
        }
    }
    
    public static void setCompoundDrawableTintMode(@NonNull final TextView textView, @Nullable final PorterDuff$Mode supportCompoundDrawablesTintMode) {
        Preconditions.checkNotNull(textView);
        if (Build$VERSION.SDK_INT >= 24) {
            Api23Impl.setCompoundDrawableTintMode(textView, supportCompoundDrawablesTintMode);
        }
        else if (textView instanceof TintableCompoundDrawablesView) {
            ((TintableCompoundDrawablesView)textView).setSupportCompoundDrawablesTintMode(supportCompoundDrawablesTintMode);
        }
    }
    
    public static void setCompoundDrawablesRelative(@NonNull final TextView textView, @Nullable final Drawable drawable, @Nullable final Drawable drawable2, @Nullable final Drawable drawable3, @Nullable final Drawable drawable4) {
        Api17Impl.setCompoundDrawablesRelative(textView, drawable, drawable2, drawable3, drawable4);
    }
    
    public static void setCompoundDrawablesRelativeWithIntrinsicBounds(@NonNull final TextView textView, @DrawableRes final int n, @DrawableRes final int n2, @DrawableRes final int n3, @DrawableRes final int n4) {
        Api17Impl.setCompoundDrawablesRelativeWithIntrinsicBounds(textView, n, n2, n3, n4);
    }
    
    public static void setCompoundDrawablesRelativeWithIntrinsicBounds(@NonNull final TextView textView, @Nullable final Drawable drawable, @Nullable final Drawable drawable2, @Nullable final Drawable drawable3, @Nullable final Drawable drawable4) {
        Api17Impl.setCompoundDrawablesRelativeWithIntrinsicBounds(textView, drawable, drawable2, drawable3, drawable4);
    }
    
    public static void setCustomSelectionActionModeCallback(@NonNull final TextView textView, @NonNull final ActionMode$Callback actionMode$Callback) {
        textView.setCustomSelectionActionModeCallback(wrapCustomSelectionActionModeCallback(textView, actionMode$Callback));
    }
    
    public static void setFirstBaselineToTopHeight(@NonNull final TextView textView, @IntRange(from = 0L) @Px final int n) {
        Preconditions.checkArgumentNonnegative(n);
        if (Build$VERSION.SDK_INT >= 28) {
            Api28Impl.setFirstBaselineToTopHeight(textView, n);
            return;
        }
        final Paint$FontMetricsInt fontMetricsInt = ((Paint)textView.getPaint()).getFontMetricsInt();
        int a;
        if (Api16Impl.getIncludeFontPadding(textView)) {
            a = fontMetricsInt.top;
        }
        else {
            a = fontMetricsInt.ascent;
        }
        if (n > Math.abs(a)) {
            textView.setPadding(((View)textView).getPaddingLeft(), n + a, ((View)textView).getPaddingRight(), ((View)textView).getPaddingBottom());
        }
    }
    
    public static void setLastBaselineToBottomHeight(@NonNull final TextView textView, @IntRange(from = 0L) @Px final int n) {
        Preconditions.checkArgumentNonnegative(n);
        final Paint$FontMetricsInt fontMetricsInt = ((Paint)textView.getPaint()).getFontMetricsInt();
        int a;
        if (Api16Impl.getIncludeFontPadding(textView)) {
            a = fontMetricsInt.bottom;
        }
        else {
            a = fontMetricsInt.descent;
        }
        if (n > Math.abs(a)) {
            textView.setPadding(((View)textView).getPaddingLeft(), ((View)textView).getPaddingTop(), ((View)textView).getPaddingRight(), n - a);
        }
    }
    
    public static void setLineHeight(@NonNull final TextView textView, @IntRange(from = 0L) @Px final int n) {
        Preconditions.checkArgumentNonnegative(n);
        final int fontMetricsInt = ((Paint)textView.getPaint()).getFontMetricsInt((Paint$FontMetricsInt)null);
        if (n != fontMetricsInt) {
            textView.setLineSpacing((float)(n - fontMetricsInt), 1.0f);
        }
    }
    
    public static void setPrecomputedText(@NonNull final TextView textView, @NonNull final PrecomputedTextCompat text) {
        if (Build$VERSION.SDK_INT >= 29) {
            textView.setText((CharSequence)text.getPrecomputedText());
        }
        else {
            if (!getTextMetricsParams(textView).equalsWithoutTextDirection(text.getParams())) {
                throw new IllegalArgumentException("Given text can not be applied to TextView.");
            }
            textView.setText((CharSequence)text);
        }
    }
    
    public static void setTextAppearance(@NonNull final TextView textView, @StyleRes final int n) {
        if (Build$VERSION.SDK_INT >= 23) {
            \u3007O8o08O.\u3007080(textView, n);
        }
        else {
            textView.setTextAppearance(((View)textView).getContext(), n);
        }
    }
    
    public static void setTextMetricsParams(@NonNull final TextView textView, @NonNull final PrecomputedTextCompat.Params params) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        Api17Impl.setTextDirection((View)textView, getTextDirection(params.getTextDirection()));
        if (sdk_INT < 23) {
            final float textScaleX = ((Paint)params.getTextPaint()).getTextScaleX();
            textView.getPaint().set(params.getTextPaint());
            if (textScaleX == textView.getTextScaleX()) {
                textView.setTextScaleX(textScaleX / 2.0f + 1.0f);
            }
            textView.setTextScaleX(textScaleX);
        }
        else {
            textView.getPaint().set(params.getTextPaint());
            Api23Impl.setBreakStrategy(textView, params.getBreakStrategy());
            Api23Impl.setHyphenationFrequency(textView, params.getHyphenationFrequency());
        }
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static ActionMode$Callback unwrapCustomSelectionActionModeCallback(@Nullable final ActionMode$Callback actionMode$Callback) {
        ActionMode$Callback wrappedCallback = actionMode$Callback;
        if (actionMode$Callback instanceof OreoCallback) {
            wrappedCallback = actionMode$Callback;
            if (Build$VERSION.SDK_INT >= 26) {
                wrappedCallback = ((OreoCallback)actionMode$Callback).getWrappedCallback();
            }
        }
        return wrappedCallback;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static ActionMode$Callback wrapCustomSelectionActionModeCallback(@NonNull final TextView textView, @Nullable final ActionMode$Callback actionMode$Callback) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 26 && sdk_INT <= 27 && !(actionMode$Callback instanceof OreoCallback) && actionMode$Callback != null) {
            return (ActionMode$Callback)new OreoCallback(actionMode$Callback, textView);
        }
        return actionMode$Callback;
    }
    
    @RequiresApi(16)
    static class Api16Impl
    {
        private Api16Impl() {
        }
        
        @DoNotInline
        static boolean getIncludeFontPadding(final TextView textView) {
            return textView.getIncludeFontPadding();
        }
        
        @DoNotInline
        static int getMaxLines(final TextView textView) {
            return textView.getMaxLines();
        }
        
        @DoNotInline
        static int getMinLines(final TextView textView) {
            return textView.getMinLines();
        }
    }
    
    @RequiresApi(17)
    static class Api17Impl
    {
        private Api17Impl() {
        }
        
        @DoNotInline
        static Drawable[] getCompoundDrawablesRelative(final TextView textView) {
            return textView.getCompoundDrawablesRelative();
        }
        
        @DoNotInline
        static int getLayoutDirection(final View view) {
            return view.getLayoutDirection();
        }
        
        @DoNotInline
        static int getTextDirection(final View view) {
            return view.getTextDirection();
        }
        
        @DoNotInline
        static Locale getTextLocale(final TextView textView) {
            return textView.getTextLocale();
        }
        
        @DoNotInline
        static void setCompoundDrawablesRelative(final TextView textView, final Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
            textView.setCompoundDrawablesRelative(drawable, drawable2, drawable3, drawable4);
        }
        
        @DoNotInline
        static void setCompoundDrawablesRelativeWithIntrinsicBounds(final TextView textView, final int n, final int n2, final int n3, final int n4) {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(n, n2, n3, n4);
        }
        
        @DoNotInline
        static void setCompoundDrawablesRelativeWithIntrinsicBounds(final TextView textView, final Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
        }
        
        @DoNotInline
        static void setTextDirection(final View view, final int textDirection) {
            view.setTextDirection(textDirection);
        }
    }
    
    @RequiresApi(23)
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        @DoNotInline
        static int getBreakStrategy(final TextView textView) {
            return \u3007o.\u3007080(textView);
        }
        
        @DoNotInline
        static ColorStateList getCompoundDrawableTintList(final TextView textView) {
            return \u3007\u3007808\u3007.\u3007080(textView);
        }
        
        @DoNotInline
        static PorterDuff$Mode getCompoundDrawableTintMode(final TextView textView) {
            return OO0o\u3007\u3007.\u3007080(textView);
        }
        
        @DoNotInline
        static int getHyphenationFrequency(final TextView textView) {
            return o\u30078.\u3007080(textView);
        }
        
        @DoNotInline
        static void setBreakStrategy(final TextView textView, final int n) {
            \u3007\u30078O0\u30078.\u3007080(textView, n);
        }
        
        @DoNotInline
        static void setCompoundDrawableTintList(final TextView textView, final ColorStateList list) {
            \u30070\u3007O0088o.\u3007080(textView, list);
        }
        
        @DoNotInline
        static void setCompoundDrawableTintMode(final TextView textView, final PorterDuff$Mode porterDuff$Mode) {
            Oooo8o0\u3007.\u3007080(textView, porterDuff$Mode);
        }
        
        @DoNotInline
        static void setHyphenationFrequency(final TextView textView, final int n) {
            \u3007O00.\u3007080(textView, n);
        }
    }
    
    @RequiresApi(24)
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        @DoNotInline
        static DecimalFormatSymbols getInstance(final Locale locale) {
            return OoO8.\u3007080(locale);
        }
    }
    
    @RequiresApi(26)
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        @DoNotInline
        static int getAutoSizeMaxTextSize(final TextView textView) {
            return \u3007oo\u3007.\u3007080(textView);
        }
        
        @DoNotInline
        static int getAutoSizeMinTextSize(final TextView textView) {
            return o\u3007O8\u3007\u3007o.\u3007080(textView);
        }
        
        @DoNotInline
        static int getAutoSizeStepGranularity(final TextView textView) {
            return \u3007oOO8O8.\u3007080(textView);
        }
        
        @DoNotInline
        static int[] getAutoSizeTextAvailableSizes(final TextView textView) {
            return oo88o8O.\u3007080(textView);
        }
        
        @DoNotInline
        static int getAutoSizeTextType(final TextView textView) {
            return o800o8O.\u3007080(textView);
        }
        
        @DoNotInline
        static void setAutoSizeTextTypeUniformWithConfiguration(final TextView textView, final int n, final int n2, final int n3, final int n4) {
            O8ooOoo\u3007.\u3007080(textView, n, n2, n3, n4);
        }
        
        @DoNotInline
        static void setAutoSizeTextTypeUniformWithPresetSizes(final TextView textView, final int[] array, final int n) {
            \u30070000OOO.\u3007080(textView, array, n);
        }
        
        @DoNotInline
        static void setAutoSizeTextTypeWithDefaults(final TextView textView, final int n) {
            \u3007O888o0o.\u3007080(textView, n);
        }
    }
    
    @RequiresApi(28)
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        @DoNotInline
        static String[] getDigitStrings(final DecimalFormatSymbols decimalFormatSymbols) {
            return O\u30078O8\u3007008.\u3007080(decimalFormatSymbols);
        }
        
        @DoNotInline
        static PrecomputedText$Params getTextMetricsParams(final TextView textView) {
            return \u300700.\u3007080(textView);
        }
        
        @DoNotInline
        static void setFirstBaselineToTopHeight(final TextView textView, final int n) {
            androidx.core.widget.O8ooOoo\u3007.\u3007080(textView, n);
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public @interface AutoSizeTextType {
    }
    
    @RequiresApi(26)
    private static class OreoCallback implements ActionMode$Callback
    {
        private static final int MENU_ITEM_ORDER_PROCESS_TEXT_INTENT_ACTIONS_START = 100;
        private final ActionMode$Callback mCallback;
        private boolean mCanUseMenuBuilderReferences;
        private boolean mInitializedMenuBuilderReferences;
        private Class<?> mMenuBuilderClass;
        private Method mMenuBuilderRemoveItemAtMethod;
        private final TextView mTextView;
        
        OreoCallback(final ActionMode$Callback mCallback, final TextView mTextView) {
            this.mCallback = mCallback;
            this.mTextView = mTextView;
            this.mInitializedMenuBuilderReferences = false;
        }
        
        private Intent createProcessTextIntent() {
            return new Intent().setAction("android.intent.action.PROCESS_TEXT").setType("text/plain");
        }
        
        private Intent createProcessTextIntentForResolveInfo(final ResolveInfo resolveInfo, final TextView textView) {
            final Intent putExtra = this.createProcessTextIntent().putExtra("android.intent.extra.PROCESS_TEXT_READONLY", this.isEditable(textView) ^ true);
            final ActivityInfo activityInfo = resolveInfo.activityInfo;
            return putExtra.setClassName(activityInfo.packageName, activityInfo.name);
        }
        
        private List<ResolveInfo> getSupportedActivities(final Context context, final PackageManager packageManager) {
            final ArrayList list = new ArrayList();
            if (!(context instanceof Activity)) {
                return list;
            }
            for (final ResolveInfo resolveInfo : packageManager.queryIntentActivities(this.createProcessTextIntent(), 0)) {
                if (this.isSupportedActivity(resolveInfo, context)) {
                    list.add(resolveInfo);
                }
            }
            return list;
        }
        
        private boolean isEditable(final TextView textView) {
            return textView instanceof Editable && textView.onCheckIsTextEditor() && ((View)textView).isEnabled();
        }
        
        private boolean isSupportedActivity(final ResolveInfo resolveInfo, final Context context) {
            final boolean equals = context.getPackageName().equals(resolveInfo.activityInfo.packageName);
            final boolean b = true;
            if (equals) {
                return true;
            }
            final ActivityInfo activityInfo = resolveInfo.activityInfo;
            if (!activityInfo.exported) {
                return false;
            }
            final String permission = activityInfo.permission;
            boolean b2 = b;
            if (permission != null) {
                b2 = (androidx.core.widget.\u3007oOO8O8.\u3007080(context, permission) == 0 && b);
            }
            return b2;
        }
        
        private void recomputeProcessTextMenuItems(final Menu obj) {
            final Context context = ((View)this.mTextView).getContext();
            final PackageManager packageManager = context.getPackageManager();
            if (!this.mInitializedMenuBuilderReferences) {
                this.mInitializedMenuBuilderReferences = true;
                try {
                    final Class<?> forName = Class.forName("com.android.internal.view.menu.MenuBuilder");
                    this.mMenuBuilderClass = forName;
                    this.mMenuBuilderRemoveItemAtMethod = forName.getDeclaredMethod("removeItemAt", Integer.TYPE);
                    this.mCanUseMenuBuilderReferences = true;
                }
                catch (final ClassNotFoundException | NoSuchMethodException ex) {
                    this.mMenuBuilderClass = null;
                    this.mMenuBuilderRemoveItemAtMethod = null;
                    this.mCanUseMenuBuilderReferences = false;
                }
            }
            try {
                Method method;
                if (this.mCanUseMenuBuilderReferences && this.mMenuBuilderClass.isInstance(obj)) {
                    method = this.mMenuBuilderRemoveItemAtMethod;
                }
                else {
                    method = obj.getClass().getDeclaredMethod("removeItemAt", Integer.TYPE);
                }
                for (int i = obj.size() - 1; i >= 0; --i) {
                    final MenuItem item = obj.getItem(i);
                    if (item.getIntent() != null && "android.intent.action.PROCESS_TEXT".equals(item.getIntent().getAction())) {
                        method.invoke(obj, i);
                    }
                }
                final List<ResolveInfo> supportedActivities = this.getSupportedActivities(context, packageManager);
                for (int j = 0; j < supportedActivities.size(); ++j) {
                    final ResolveInfo resolveInfo = supportedActivities.get(j);
                    obj.add(0, 0, j + 100, resolveInfo.loadLabel(packageManager)).setIntent(this.createProcessTextIntentForResolveInfo(resolveInfo, this.mTextView)).setShowAsAction(1);
                }
            }
            catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException ex2) {}
        }
        
        @NonNull
        ActionMode$Callback getWrappedCallback() {
            return this.mCallback;
        }
        
        public boolean onActionItemClicked(final ActionMode actionMode, final MenuItem menuItem) {
            return this.mCallback.onActionItemClicked(actionMode, menuItem);
        }
        
        public boolean onCreateActionMode(final ActionMode actionMode, final Menu menu) {
            return this.mCallback.onCreateActionMode(actionMode, menu);
        }
        
        public void onDestroyActionMode(final ActionMode actionMode) {
            this.mCallback.onDestroyActionMode(actionMode);
        }
        
        public boolean onPrepareActionMode(final ActionMode actionMode, final Menu menu) {
            this.recomputeProcessTextMenuItems(menu);
            return this.mCallback.onPrepareActionMode(actionMode, menu);
        }
    }
}

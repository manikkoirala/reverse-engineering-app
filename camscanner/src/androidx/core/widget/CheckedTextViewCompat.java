// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import androidx.annotation.RequiresApi;
import java.lang.reflect.Field;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import androidx.annotation.Nullable;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import android.widget.CheckedTextView;

public final class CheckedTextViewCompat
{
    private static final String TAG = "CheckedTextViewCompat";
    
    private CheckedTextViewCompat() {
    }
    
    @Nullable
    public static Drawable getCheckMarkDrawable(@NonNull final CheckedTextView checkedTextView) {
        return Api16Impl.getCheckMarkDrawable(checkedTextView);
    }
    
    @Nullable
    public static ColorStateList getCheckMarkTintList(@NonNull final CheckedTextView checkedTextView) {
        return Api21Impl.getCheckMarkTintList(checkedTextView);
    }
    
    @Nullable
    public static PorterDuff$Mode getCheckMarkTintMode(@NonNull final CheckedTextView checkedTextView) {
        return Api21Impl.getCheckMarkTintMode(checkedTextView);
    }
    
    public static void setCheckMarkTintList(@NonNull final CheckedTextView checkedTextView, @Nullable final ColorStateList list) {
        Api21Impl.setCheckMarkTintList(checkedTextView, list);
    }
    
    public static void setCheckMarkTintMode(@NonNull final CheckedTextView checkedTextView, @Nullable final PorterDuff$Mode porterDuff$Mode) {
        Api21Impl.setCheckMarkTintMode(checkedTextView, porterDuff$Mode);
    }
    
    private static class Api14Impl
    {
        private static Field sCheckMarkDrawableField;
        private static boolean sResolved;
        
        @Nullable
        static Drawable getCheckMarkDrawable(@NonNull final CheckedTextView obj) {
            Label_0027: {
                if (Api14Impl.sResolved) {
                    break Label_0027;
                }
                while (true) {
                    try {
                        (Api14Impl.sCheckMarkDrawableField = CheckedTextView.class.getDeclaredField("mCheckMarkDrawable")).setAccessible(true);
                        Api14Impl.sResolved = true;
                        final Field sCheckMarkDrawableField = Api14Impl.sCheckMarkDrawableField;
                        if (sCheckMarkDrawableField != null) {
                            try {
                                return (Drawable)sCheckMarkDrawableField.get(obj);
                            }
                            catch (final IllegalAccessException obj) {
                                Api14Impl.sCheckMarkDrawableField = null;
                            }
                        }
                        return null;
                    }
                    catch (final NoSuchFieldException ex) {
                        continue;
                    }
                    break;
                }
            }
        }
    }
    
    @RequiresApi(16)
    private static class Api16Impl
    {
        @Nullable
        static Drawable getCheckMarkDrawable(@NonNull final CheckedTextView checkedTextView) {
            return checkedTextView.getCheckMarkDrawable();
        }
    }
    
    @RequiresApi(21)
    private static class Api21Impl
    {
        @Nullable
        static ColorStateList getCheckMarkTintList(@NonNull final CheckedTextView checkedTextView) {
            return checkedTextView.getCheckMarkTintList();
        }
        
        @Nullable
        static PorterDuff$Mode getCheckMarkTintMode(@NonNull final CheckedTextView checkedTextView) {
            return checkedTextView.getCheckMarkTintMode();
        }
        
        static void setCheckMarkTintList(@NonNull final CheckedTextView checkedTextView, @Nullable final ColorStateList checkMarkTintList) {
            checkedTextView.setCheckMarkTintList(checkMarkTintList);
        }
        
        static void setCheckMarkTintMode(@NonNull final CheckedTextView checkedTextView, @Nullable final PorterDuff$Mode checkMarkTintMode) {
            checkedTextView.setCheckMarkTintMode(checkMarkTintMode);
        }
    }
}

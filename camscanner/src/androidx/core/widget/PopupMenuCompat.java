// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import androidx.annotation.Nullable;
import android.widget.PopupMenu;
import android.view.View$OnTouchListener;
import androidx.annotation.NonNull;

public final class PopupMenuCompat
{
    private PopupMenuCompat() {
    }
    
    @Nullable
    public static View$OnTouchListener getDragToOpenListener(@NonNull final Object o) {
        return Api19Impl.getDragToOpenListener((PopupMenu)o);
    }
    
    @RequiresApi(19)
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        @DoNotInline
        static View$OnTouchListener getDragToOpenListener(final PopupMenu popupMenu) {
            return popupMenu.getDragToOpenListener();
        }
    }
}

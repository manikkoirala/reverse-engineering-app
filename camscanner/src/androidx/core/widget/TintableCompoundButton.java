// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import android.graphics.PorterDuff$Mode;
import androidx.annotation.Nullable;
import android.content.res.ColorStateList;

public interface TintableCompoundButton
{
    @Nullable
    ColorStateList getSupportButtonTintList();
    
    @Nullable
    PorterDuff$Mode getSupportButtonTintMode();
    
    void setSupportButtonTintList(@Nullable final ColorStateList p0);
    
    void setSupportButtonTintMode(@Nullable final PorterDuff$Mode p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import android.graphics.PorterDuff$Mode;
import androidx.annotation.Nullable;
import android.content.res.ColorStateList;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public interface TintableImageSourceView
{
    @Nullable
    ColorStateList getSupportImageTintList();
    
    @Nullable
    PorterDuff$Mode getSupportImageTintMode();
    
    void setSupportImageTintList(@Nullable final ColorStateList p0);
    
    void setSupportImageTintMode(@Nullable final PorterDuff$Mode p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.widget;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import androidx.annotation.Nullable;
import android.view.View$OnTouchListener;
import android.view.View;
import androidx.annotation.NonNull;
import android.widget.ListPopupWindow;

public final class ListPopupWindowCompat
{
    private ListPopupWindowCompat() {
    }
    
    @Nullable
    public static View$OnTouchListener createDragToOpenListener(@NonNull final ListPopupWindow listPopupWindow, @NonNull final View view) {
        return Api19Impl.createDragToOpenListener(listPopupWindow, view);
    }
    
    @Deprecated
    public static View$OnTouchListener createDragToOpenListener(final Object o, final View view) {
        return createDragToOpenListener((ListPopupWindow)o, view);
    }
    
    @RequiresApi(19)
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        @DoNotInline
        static View$OnTouchListener createDragToOpenListener(final ListPopupWindow listPopupWindow, final View view) {
            return listPopupWindow.createDragToOpenListener(view);
        }
    }
}

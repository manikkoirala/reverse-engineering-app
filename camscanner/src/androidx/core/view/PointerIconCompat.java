// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import androidx.annotation.RestrictTo;
import androidx.annotation.Nullable;
import android.content.res.Resources;
import android.content.Context;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.graphics.Bitmap;
import android.view.PointerIcon;

public final class PointerIconCompat
{
    public static final int TYPE_ALIAS = 1010;
    public static final int TYPE_ALL_SCROLL = 1013;
    public static final int TYPE_ARROW = 1000;
    public static final int TYPE_CELL = 1006;
    public static final int TYPE_CONTEXT_MENU = 1001;
    public static final int TYPE_COPY = 1011;
    public static final int TYPE_CROSSHAIR = 1007;
    public static final int TYPE_DEFAULT = 1000;
    public static final int TYPE_GRAB = 1020;
    public static final int TYPE_GRABBING = 1021;
    public static final int TYPE_HAND = 1002;
    public static final int TYPE_HELP = 1003;
    public static final int TYPE_HORIZONTAL_DOUBLE_ARROW = 1014;
    public static final int TYPE_NO_DROP = 1012;
    public static final int TYPE_NULL = 0;
    public static final int TYPE_TEXT = 1008;
    public static final int TYPE_TOP_LEFT_DIAGONAL_DOUBLE_ARROW = 1017;
    public static final int TYPE_TOP_RIGHT_DIAGONAL_DOUBLE_ARROW = 1016;
    public static final int TYPE_VERTICAL_DOUBLE_ARROW = 1015;
    public static final int TYPE_VERTICAL_TEXT = 1009;
    public static final int TYPE_WAIT = 1004;
    public static final int TYPE_ZOOM_IN = 1018;
    public static final int TYPE_ZOOM_OUT = 1019;
    private final PointerIcon mPointerIcon;
    
    private PointerIconCompat(final PointerIcon mPointerIcon) {
        this.mPointerIcon = mPointerIcon;
    }
    
    @NonNull
    public static PointerIconCompat create(@NonNull final Bitmap bitmap, final float n, final float n2) {
        if (Build$VERSION.SDK_INT >= 24) {
            return new PointerIconCompat(Api24Impl.create(bitmap, n, n2));
        }
        return new PointerIconCompat(null);
    }
    
    @NonNull
    public static PointerIconCompat getSystemIcon(@NonNull final Context context, final int n) {
        if (Build$VERSION.SDK_INT >= 24) {
            return new PointerIconCompat(Api24Impl.getSystemIcon(context, n));
        }
        return new PointerIconCompat(null);
    }
    
    @NonNull
    public static PointerIconCompat load(@NonNull final Resources resources, final int n) {
        if (Build$VERSION.SDK_INT >= 24) {
            return new PointerIconCompat(Api24Impl.load(resources, n));
        }
        return new PointerIconCompat(null);
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public Object getPointerIcon() {
        return this.mPointerIcon;
    }
    
    @RequiresApi(24)
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        @DoNotInline
        static PointerIcon create(final Bitmap bitmap, final float n, final float n2) {
            return O\u3007O\u3007oO.\u3007080(bitmap, n, n2);
        }
        
        @DoNotInline
        static PointerIcon getSystemIcon(final Context context, final int n) {
            return \u30078\u30070\u3007o\u3007O.\u3007080(context, n);
        }
        
        @DoNotInline
        static PointerIcon load(final Resources resources, final int n) {
            return O08000.\u3007080(resources, n);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.annotation.DoNotInline;
import androidx.core.util.Preconditions;
import android.view.Display$Mode;
import androidx.annotation.RequiresApi;
import android.text.TextUtils;
import android.app.UiModeManager;
import android.os.Build;
import androidx.annotation.Nullable;
import android.annotation.SuppressLint;
import android.os.Build$VERSION;
import android.graphics.Point;
import android.view.Display;
import androidx.annotation.NonNull;
import android.content.Context;

public final class DisplayCompat
{
    private static final int DISPLAY_SIZE_4K_HEIGHT = 2160;
    private static final int DISPLAY_SIZE_4K_WIDTH = 3840;
    
    private DisplayCompat() {
    }
    
    static Point getCurrentDisplaySizeFromWorkarounds(@NonNull final Context context, @NonNull final Display display) {
        Point point;
        if (Build$VERSION.SDK_INT < 28) {
            point = parsePhysicalDisplaySizeFromSystemProperties("sys.display-size", display);
        }
        else {
            point = parsePhysicalDisplaySizeFromSystemProperties("vendor.display-size", display);
        }
        if (point != null) {
            return point;
        }
        final boolean sonyBravia4kTv = isSonyBravia4kTv(context);
        Point point2 = null;
        if (sonyBravia4kTv) {
            point2 = point2;
            if (isCurrentModeTheLargestMode(display)) {
                point2 = new Point(3840, 2160);
            }
        }
        return point2;
    }
    
    @NonNull
    private static Point getDisplaySize(@NonNull final Context context, @NonNull final Display display) {
        final Point currentDisplaySizeFromWorkarounds = getCurrentDisplaySizeFromWorkarounds(context, display);
        if (currentDisplaySizeFromWorkarounds != null) {
            return currentDisplaySizeFromWorkarounds;
        }
        final Point point = new Point();
        Api17Impl.getRealSize(display, point);
        return point;
    }
    
    @NonNull
    public static ModeCompat getMode(@NonNull final Context context, @NonNull final Display display) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getMode(context, display);
        }
        return new ModeCompat(getDisplaySize(context, display));
    }
    
    @SuppressLint({ "ArrayReturn" })
    @NonNull
    public static ModeCompat[] getSupportedModes(@NonNull final Context context, @NonNull final Display display) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getSupportedModes(context, display);
        }
        return new ModeCompat[] { getMode(context, display) };
    }
    
    @Nullable
    private static String getSystemProperty(String s) {
        try {
            final Class<?> forName = Class.forName("android.os.SystemProperties");
            s = (String)forName.getMethod("get", String.class).invoke(forName, s);
            return s;
        }
        catch (final Exception ex) {
            return null;
        }
    }
    
    static boolean isCurrentModeTheLargestMode(@NonNull final Display display) {
        return Build$VERSION.SDK_INT < 23 || Api23Impl.isCurrentModeTheLargestMode(display);
    }
    
    private static boolean isSonyBravia4kTv(@NonNull final Context context) {
        return isTv(context) && "Sony".equals(Build.MANUFACTURER) && Build.MODEL.startsWith("BRAVIA") && context.getPackageManager().hasSystemFeature("com.sony.dtv.hardware.panel.qfhd");
    }
    
    private static boolean isTv(@NonNull final Context context) {
        final UiModeManager uiModeManager = (UiModeManager)context.getSystemService("uimode");
        return uiModeManager != null && uiModeManager.getCurrentModeType() == 4;
    }
    
    private static Point parseDisplaySize(@NonNull final String s) throws NumberFormatException {
        final String[] split = s.trim().split("x", -1);
        if (split.length == 2) {
            final int int1 = Integer.parseInt(split[0]);
            final int int2 = Integer.parseInt(split[1]);
            if (int1 > 0 && int2 > 0) {
                return new Point(int1, int2);
            }
        }
        throw new NumberFormatException();
    }
    
    @Nullable
    private static Point parsePhysicalDisplaySizeFromSystemProperties(@NonNull String systemProperty, @NonNull final Display display) {
        if (display.getDisplayId() != 0) {
            return null;
        }
        systemProperty = getSystemProperty(systemProperty);
        Label_0035: {
            if (TextUtils.isEmpty((CharSequence)systemProperty)) {
                break Label_0035;
            }
            if (systemProperty == null) {
                break Label_0035;
            }
            try {
                return parseDisplaySize(systemProperty);
                return null;
            }
            catch (final NumberFormatException ex) {
                return null;
            }
        }
    }
    
    @RequiresApi(17)
    static class Api17Impl
    {
        private Api17Impl() {
        }
        
        static void getRealSize(final Display display, final Point point) {
            display.getRealSize(point);
        }
    }
    
    @RequiresApi(23)
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        @NonNull
        static ModeCompat getMode(@NonNull final Context context, @NonNull final Display display) {
            final Display$Mode \u3007080 = \u3007O00.\u3007080(display);
            final Point currentDisplaySizeFromWorkarounds = DisplayCompat.getCurrentDisplaySizeFromWorkarounds(context, display);
            ModeCompat modeCompat;
            if (currentDisplaySizeFromWorkarounds != null && !physicalSizeEquals(\u3007080, currentDisplaySizeFromWorkarounds)) {
                modeCompat = new ModeCompat(\u3007080, currentDisplaySizeFromWorkarounds);
            }
            else {
                modeCompat = new ModeCompat(\u3007080, true);
            }
            return modeCompat;
        }
        
        @SuppressLint({ "ArrayReturn" })
        @NonNull
        public static ModeCompat[] getSupportedModes(@NonNull final Context context, @NonNull final Display display) {
            final Display$Mode[] \u3007080 = \u3007\u30078O0\u30078.\u3007080(display);
            final ModeCompat[] array = new ModeCompat[\u3007080.length];
            final Display$Mode \u300781 = \u3007O00.\u3007080(display);
            final Point currentDisplaySizeFromWorkarounds = DisplayCompat.getCurrentDisplaySizeFromWorkarounds(context, display);
            int i;
            final int n = i = 0;
            if (currentDisplaySizeFromWorkarounds != null) {
                if (!physicalSizeEquals(\u300781, currentDisplaySizeFromWorkarounds)) {
                    for (int j = 0; j < \u3007080.length; ++j) {
                        ModeCompat modeCompat;
                        if (physicalSizeEquals(\u3007080[j], \u300781)) {
                            modeCompat = new ModeCompat(\u3007080[j], currentDisplaySizeFromWorkarounds);
                        }
                        else {
                            modeCompat = new ModeCompat(\u3007080[j], false);
                        }
                        array[j] = modeCompat;
                    }
                    return array;
                }
                i = n;
            }
            while (i < \u3007080.length) {
                array[i] = new ModeCompat(\u3007080[i], physicalSizeEquals(\u3007080[i], \u300781));
                ++i;
            }
            return array;
        }
        
        static boolean isCurrentModeTheLargestMode(@NonNull final Display display) {
            final Display$Mode \u3007080 = \u3007O00.\u3007080(display);
            for (final Display$Mode display$Mode : \u3007\u30078O0\u30078.\u3007080(display)) {
                if (\u3007\u3007808\u3007.\u3007080(\u3007080) < \u3007\u3007808\u3007.\u3007080(display$Mode) || Oooo8o0\u3007.\u3007080(\u3007080) < Oooo8o0\u3007.\u3007080(display$Mode)) {
                    return false;
                }
            }
            return true;
        }
        
        static boolean physicalSizeEquals(final Display$Mode display$Mode, final Point point) {
            return (Oooo8o0\u3007.\u3007080(display$Mode) == point.x && \u3007\u3007808\u3007.\u3007080(display$Mode) == point.y) || (Oooo8o0\u3007.\u3007080(display$Mode) == point.y && \u3007\u3007808\u3007.\u3007080(display$Mode) == point.x);
        }
        
        static boolean physicalSizeEquals(final Display$Mode display$Mode, final Display$Mode display$Mode2) {
            return Oooo8o0\u3007.\u3007080(display$Mode) == Oooo8o0\u3007.\u3007080(display$Mode2) && \u3007\u3007808\u3007.\u3007080(display$Mode) == \u3007\u3007808\u3007.\u3007080(display$Mode2);
        }
    }
    
    public static final class ModeCompat
    {
        private final boolean mIsNative;
        private final Display$Mode mMode;
        private final Point mPhysicalSize;
        
        ModeCompat(@NonNull final Point mPhysicalSize) {
            Preconditions.checkNotNull(mPhysicalSize, "physicalSize == null");
            this.mPhysicalSize = mPhysicalSize;
            this.mMode = null;
            this.mIsNative = true;
        }
        
        @RequiresApi(23)
        ModeCompat(@NonNull final Display$Mode mMode, @NonNull final Point mPhysicalSize) {
            Preconditions.checkNotNull(mMode, "mode == null, can't wrap a null reference");
            Preconditions.checkNotNull(mPhysicalSize, "physicalSize == null");
            this.mPhysicalSize = mPhysicalSize;
            this.mMode = mMode;
            this.mIsNative = true;
        }
        
        @RequiresApi(23)
        ModeCompat(@NonNull final Display$Mode mMode, final boolean mIsNative) {
            Preconditions.checkNotNull(mMode, "mode == null, can't wrap a null reference");
            this.mPhysicalSize = new Point(Api23Impl.getPhysicalWidth(mMode), Api23Impl.getPhysicalHeight(mMode));
            this.mMode = mMode;
            this.mIsNative = mIsNative;
        }
        
        public int getPhysicalHeight() {
            return this.mPhysicalSize.y;
        }
        
        public int getPhysicalWidth() {
            return this.mPhysicalSize.x;
        }
        
        @Deprecated
        public boolean isNative() {
            return this.mIsNative;
        }
        
        @Nullable
        @RequiresApi(23)
        public Display$Mode toMode() {
            return this.mMode;
        }
        
        @RequiresApi(23)
        static class Api23Impl
        {
            private Api23Impl() {
            }
            
            @DoNotInline
            static int getPhysicalHeight(final Display$Mode display$Mode) {
                return \u3007\u3007808\u3007.\u3007080(display$Mode);
            }
            
            @DoNotInline
            static int getPhysicalWidth(final Display$Mode display$Mode) {
                return Oooo8o0\u3007.\u3007080(display$Mode);
            }
        }
    }
}

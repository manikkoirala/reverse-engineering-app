// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.annotation.NonNull;
import android.view.View;

public interface ViewPropertyAnimatorListener
{
    void onAnimationCancel(@NonNull final View p0);
    
    void onAnimationEnd(@NonNull final View p0);
    
    void onAnimationStart(@NonNull final View p0);
}

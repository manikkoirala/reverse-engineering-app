// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.graphics.Paint;
import androidx.annotation.DoNotInline;
import android.view.ViewPropertyAnimator;
import androidx.annotation.RequiresApi;
import android.annotation.SuppressLint;
import android.animation.ValueAnimator$AnimatorUpdateListener;
import android.animation.TimeInterpolator;
import androidx.annotation.Nullable;
import android.view.animation.Interpolator;
import androidx.annotation.NonNull;
import android.animation.Animator$AnimatorListener;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.view.View;
import java.lang.ref.WeakReference;

public final class ViewPropertyAnimatorCompat
{
    static final int LISTENER_TAG_ID = 2113929216;
    Runnable mEndAction;
    int mOldLayerType;
    Runnable mStartAction;
    private final WeakReference<View> mView;
    
    ViewPropertyAnimatorCompat(final View referent) {
        this.mStartAction = null;
        this.mEndAction = null;
        this.mOldLayerType = -1;
        this.mView = new WeakReference<View>(referent);
    }
    
    private void setListenerInternal(final View view, final ViewPropertyAnimatorListener viewPropertyAnimatorListener) {
        if (viewPropertyAnimatorListener != null) {
            view.animate().setListener((Animator$AnimatorListener)new AnimatorListenerAdapter(this, viewPropertyAnimatorListener, view) {
                final ViewPropertyAnimatorCompat this$0;
                final ViewPropertyAnimatorListener val$listener;
                final View val$view;
                
                public void onAnimationCancel(final Animator animator) {
                    this.val$listener.onAnimationCancel(this.val$view);
                }
                
                public void onAnimationEnd(final Animator animator) {
                    this.val$listener.onAnimationEnd(this.val$view);
                }
                
                public void onAnimationStart(final Animator animator) {
                    this.val$listener.onAnimationStart(this.val$view);
                }
            });
        }
        else {
            view.animate().setListener((Animator$AnimatorListener)null);
        }
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat alpha(final float n) {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().alpha(n);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat alphaBy(final float n) {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().alphaBy(n);
        }
        return this;
    }
    
    public void cancel() {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().cancel();
        }
    }
    
    public long getDuration() {
        final View view = this.mView.get();
        if (view != null) {
            return view.animate().getDuration();
        }
        return 0L;
    }
    
    @Nullable
    public Interpolator getInterpolator() {
        final View view = this.mView.get();
        if (view != null) {
            return Api18Impl.getInterpolator(view.animate());
        }
        return null;
    }
    
    public long getStartDelay() {
        final View view = this.mView.get();
        if (view != null) {
            return view.animate().getStartDelay();
        }
        return 0L;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat rotation(final float n) {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().rotation(n);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat rotationBy(final float n) {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().rotationBy(n);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat rotationX(final float n) {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().rotationX(n);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat rotationXBy(final float n) {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().rotationXBy(n);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat rotationY(final float n) {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().rotationY(n);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat rotationYBy(final float n) {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().rotationYBy(n);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat scaleX(final float n) {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().scaleX(n);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat scaleXBy(final float n) {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().scaleXBy(n);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat scaleY(final float n) {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().scaleY(n);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat scaleYBy(final float n) {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().scaleYBy(n);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat setDuration(final long duration) {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().setDuration(duration);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat setInterpolator(@Nullable final Interpolator interpolator) {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().setInterpolator((TimeInterpolator)interpolator);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat setListener(@Nullable final ViewPropertyAnimatorListener viewPropertyAnimatorListener) {
        final View view = this.mView.get();
        if (view != null) {
            this.setListenerInternal(view, viewPropertyAnimatorListener);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat setStartDelay(final long startDelay) {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().setStartDelay(startDelay);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat setUpdateListener(@Nullable final ViewPropertyAnimatorUpdateListener viewPropertyAnimatorUpdateListener) {
        final View view = this.mView.get();
        if (view != null) {
            Object o;
            if (viewPropertyAnimatorUpdateListener != null) {
                o = new O0oo0o0\u3007(viewPropertyAnimatorUpdateListener, view);
            }
            else {
                o = null;
            }
            Api19Impl.setUpdateListener(view.animate(), (ValueAnimator$AnimatorUpdateListener)o);
        }
        return this;
    }
    
    public void start() {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().start();
        }
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat translationX(final float n) {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().translationX(n);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat translationXBy(final float n) {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().translationXBy(n);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat translationY(final float n) {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().translationY(n);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat translationYBy(final float n) {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().translationYBy(n);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat translationZ(final float n) {
        final View view = this.mView.get();
        if (view != null) {
            Api21Impl.translationZ(view.animate(), n);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat translationZBy(final float n) {
        final View view = this.mView.get();
        if (view != null) {
            Api21Impl.translationZBy(view.animate(), n);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat withEndAction(@NonNull final Runnable runnable) {
        final View view = this.mView.get();
        if (view != null) {
            Api16Impl.withEndAction(view.animate(), runnable);
        }
        return this;
    }
    
    @SuppressLint({ "WrongConstant" })
    @NonNull
    public ViewPropertyAnimatorCompat withLayer() {
        final View view = this.mView.get();
        if (view != null) {
            Api16Impl.withLayer(view.animate());
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat withStartAction(@NonNull final Runnable runnable) {
        final View view = this.mView.get();
        if (view != null) {
            Api16Impl.withStartAction(view.animate(), runnable);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat x(final float n) {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().x(n);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat xBy(final float n) {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().xBy(n);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat y(final float n) {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().y(n);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat yBy(final float n) {
        final View view = this.mView.get();
        if (view != null) {
            view.animate().yBy(n);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat z(final float n) {
        final View view = this.mView.get();
        if (view != null) {
            Api21Impl.z(view.animate(), n);
        }
        return this;
    }
    
    @NonNull
    public ViewPropertyAnimatorCompat zBy(final float n) {
        final View view = this.mView.get();
        if (view != null) {
            Api21Impl.zBy(view.animate(), n);
        }
        return this;
    }
    
    @RequiresApi(16)
    static class Api16Impl
    {
        private Api16Impl() {
        }
        
        @DoNotInline
        static ViewPropertyAnimator withEndAction(final ViewPropertyAnimator viewPropertyAnimator, final Runnable runnable) {
            return viewPropertyAnimator.withEndAction(runnable);
        }
        
        @DoNotInline
        static ViewPropertyAnimator withLayer(final ViewPropertyAnimator viewPropertyAnimator) {
            return viewPropertyAnimator.withLayer();
        }
        
        @DoNotInline
        static ViewPropertyAnimator withStartAction(final ViewPropertyAnimator viewPropertyAnimator, final Runnable runnable) {
            return viewPropertyAnimator.withStartAction(runnable);
        }
    }
    
    @RequiresApi(18)
    static class Api18Impl
    {
        private Api18Impl() {
        }
        
        @DoNotInline
        static Interpolator getInterpolator(final ViewPropertyAnimator viewPropertyAnimator) {
            return (Interpolator)viewPropertyAnimator.getInterpolator();
        }
    }
    
    @RequiresApi(19)
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        @DoNotInline
        static ViewPropertyAnimator setUpdateListener(final ViewPropertyAnimator viewPropertyAnimator, final ValueAnimator$AnimatorUpdateListener updateListener) {
            return viewPropertyAnimator.setUpdateListener(updateListener);
        }
    }
    
    @RequiresApi(21)
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        @DoNotInline
        static ViewPropertyAnimator translationZ(final ViewPropertyAnimator viewPropertyAnimator, final float n) {
            return viewPropertyAnimator.translationZ(n);
        }
        
        @DoNotInline
        static ViewPropertyAnimator translationZBy(final ViewPropertyAnimator viewPropertyAnimator, final float n) {
            return viewPropertyAnimator.translationZBy(n);
        }
        
        @DoNotInline
        static ViewPropertyAnimator z(final ViewPropertyAnimator viewPropertyAnimator, final float n) {
            return viewPropertyAnimator.z(n);
        }
        
        @DoNotInline
        static ViewPropertyAnimator zBy(final ViewPropertyAnimator viewPropertyAnimator, final float n) {
            return viewPropertyAnimator.zBy(n);
        }
    }
    
    static class ViewPropertyAnimatorListenerApi14 implements ViewPropertyAnimatorListener
    {
        boolean mAnimEndCalled;
        ViewPropertyAnimatorCompat mVpa;
        
        ViewPropertyAnimatorListenerApi14(final ViewPropertyAnimatorCompat mVpa) {
            this.mVpa = mVpa;
        }
        
        @Override
        public void onAnimationCancel(@NonNull final View view) {
            final Object tag = view.getTag(2113929216);
            ViewPropertyAnimatorListener viewPropertyAnimatorListener;
            if (tag instanceof ViewPropertyAnimatorListener) {
                viewPropertyAnimatorListener = (ViewPropertyAnimatorListener)tag;
            }
            else {
                viewPropertyAnimatorListener = null;
            }
            if (viewPropertyAnimatorListener != null) {
                viewPropertyAnimatorListener.onAnimationCancel(view);
            }
        }
        
        @SuppressLint({ "WrongConstant" })
        @Override
        public void onAnimationEnd(@NonNull final View view) {
            final int mOldLayerType = this.mVpa.mOldLayerType;
            ViewPropertyAnimatorListener viewPropertyAnimatorListener = null;
            if (mOldLayerType > -1) {
                view.setLayerType(mOldLayerType, (Paint)null);
                this.mVpa.mOldLayerType = -1;
            }
            final ViewPropertyAnimatorCompat mVpa = this.mVpa;
            final Runnable mEndAction = mVpa.mEndAction;
            if (mEndAction != null) {
                mVpa.mEndAction = null;
                mEndAction.run();
            }
            final Object tag = view.getTag(2113929216);
            if (tag instanceof ViewPropertyAnimatorListener) {
                viewPropertyAnimatorListener = (ViewPropertyAnimatorListener)tag;
            }
            if (viewPropertyAnimatorListener != null) {
                viewPropertyAnimatorListener.onAnimationEnd(view);
            }
            this.mAnimEndCalled = true;
        }
        
        @Override
        public void onAnimationStart(@NonNull final View view) {
            this.mAnimEndCalled = false;
            final int mOldLayerType = this.mVpa.mOldLayerType;
            ViewPropertyAnimatorListener viewPropertyAnimatorListener = null;
            if (mOldLayerType > -1) {
                view.setLayerType(2, (Paint)null);
            }
            final ViewPropertyAnimatorCompat mVpa = this.mVpa;
            final Runnable mStartAction = mVpa.mStartAction;
            if (mStartAction != null) {
                mVpa.mStartAction = null;
                mStartAction.run();
            }
            final Object tag = view.getTag(2113929216);
            if (tag instanceof ViewPropertyAnimatorListener) {
                viewPropertyAnimatorListener = (ViewPropertyAnimatorListener)tag;
            }
            if (viewPropertyAnimatorListener != null) {
                viewPropertyAnimatorListener.onAnimationStart(view);
            }
        }
    }
}

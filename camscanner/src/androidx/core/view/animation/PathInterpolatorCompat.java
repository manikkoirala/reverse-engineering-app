// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view.animation;

import androidx.annotation.DoNotInline;
import android.view.animation.PathInterpolator;
import androidx.annotation.RequiresApi;
import android.graphics.Path;
import androidx.annotation.NonNull;
import android.view.animation.Interpolator;

public final class PathInterpolatorCompat
{
    private PathInterpolatorCompat() {
    }
    
    @NonNull
    public static Interpolator create(final float n, final float n2) {
        return (Interpolator)Api21Impl.createPathInterpolator(n, n2);
    }
    
    @NonNull
    public static Interpolator create(final float n, final float n2, final float n3, final float n4) {
        return (Interpolator)Api21Impl.createPathInterpolator(n, n2, n3, n4);
    }
    
    @NonNull
    public static Interpolator create(@NonNull final Path path) {
        return (Interpolator)Api21Impl.createPathInterpolator(path);
    }
    
    @RequiresApi(21)
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        @DoNotInline
        static PathInterpolator createPathInterpolator(final float n, final float n2) {
            return new PathInterpolator(n, n2);
        }
        
        @DoNotInline
        static PathInterpolator createPathInterpolator(final float n, final float n2, final float n3, final float n4) {
            return new PathInterpolator(n, n2, n3, n4);
        }
        
        @DoNotInline
        static PathInterpolator createPathInterpolator(final Path path) {
            return new PathInterpolator(path);
        }
    }
}

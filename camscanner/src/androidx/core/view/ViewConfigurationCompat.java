// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.content.res.Resources;
import androidx.annotation.NonNull;
import android.util.TypedValue;
import android.content.Context;
import android.view.ViewConfiguration;
import android.os.Build$VERSION;
import java.lang.reflect.Method;

public final class ViewConfigurationCompat
{
    private static final String TAG = "ViewConfigCompat";
    private static Method sGetScaledScrollFactorMethod;
    
    static {
        if (Build$VERSION.SDK_INT != 25) {
            return;
        }
        try {
            ViewConfigurationCompat.sGetScaledScrollFactorMethod = ViewConfiguration.class.getDeclaredMethod("getScaledScrollFactor", (Class<?>[])new Class[0]);
        }
        catch (final Exception ex) {}
    }
    
    private ViewConfigurationCompat() {
    }
    
    private static float getLegacyScrollFactor(final ViewConfiguration obj, final Context context) {
        if (Build$VERSION.SDK_INT >= 25) {
            final Method sGetScaledScrollFactorMethod = ViewConfigurationCompat.sGetScaledScrollFactorMethod;
            if (sGetScaledScrollFactorMethod != null) {
                try {
                    return (float)(int)sGetScaledScrollFactorMethod.invoke(obj, new Object[0]);
                }
                catch (final Exception ex) {}
            }
        }
        final TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(16842829, typedValue, true)) {
            return typedValue.getDimension(context.getResources().getDisplayMetrics());
        }
        return 0.0f;
    }
    
    public static float getScaledHorizontalScrollFactor(@NonNull final ViewConfiguration viewConfiguration, @NonNull final Context context) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getScaledHorizontalScrollFactor(viewConfiguration);
        }
        return getLegacyScrollFactor(viewConfiguration, context);
    }
    
    public static int getScaledHoverSlop(@NonNull final ViewConfiguration viewConfiguration) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getScaledHoverSlop(viewConfiguration);
        }
        return viewConfiguration.getScaledTouchSlop() / 2;
    }
    
    @Deprecated
    public static int getScaledPagingTouchSlop(final ViewConfiguration viewConfiguration) {
        return viewConfiguration.getScaledPagingTouchSlop();
    }
    
    public static float getScaledVerticalScrollFactor(@NonNull final ViewConfiguration viewConfiguration, @NonNull final Context context) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getScaledVerticalScrollFactor(viewConfiguration);
        }
        return getLegacyScrollFactor(viewConfiguration, context);
    }
    
    @Deprecated
    public static boolean hasPermanentMenuKey(final ViewConfiguration viewConfiguration) {
        return viewConfiguration.hasPermanentMenuKey();
    }
    
    public static boolean shouldShowMenuShortcutsWhenKeyboardPresent(@NonNull final ViewConfiguration viewConfiguration, @NonNull final Context context) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.shouldShowMenuShortcutsWhenKeyboardPresent(viewConfiguration);
        }
        final Resources resources = context.getResources();
        final int identifier = resources.getIdentifier("config_showMenuShortcutsWhenKeyboardPresent", "bool", "android");
        return identifier != 0 && resources.getBoolean(identifier);
    }
    
    @RequiresApi(26)
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        @DoNotInline
        static float getScaledHorizontalScrollFactor(final ViewConfiguration viewConfiguration) {
            return O0oO008.\u3007080(viewConfiguration);
        }
        
        @DoNotInline
        static float getScaledVerticalScrollFactor(final ViewConfiguration viewConfiguration) {
            return Oo0oO\u3007O\u3007O.\u3007080(viewConfiguration);
        }
    }
    
    @RequiresApi(28)
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        @DoNotInline
        static int getScaledHoverSlop(final ViewConfiguration viewConfiguration) {
            return \u3007\u300700OO.\u3007080(viewConfiguration);
        }
        
        @DoNotInline
        static boolean shouldShowMenuShortcutsWhenKeyboardPresent(final ViewConfiguration viewConfiguration) {
            return o\u30078\u3007.\u3007080(viewConfiguration);
        }
    }
}

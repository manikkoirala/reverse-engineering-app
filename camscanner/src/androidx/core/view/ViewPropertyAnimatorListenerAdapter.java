// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.annotation.NonNull;
import android.view.View;

public class ViewPropertyAnimatorListenerAdapter implements ViewPropertyAnimatorListener
{
    @Override
    public void onAnimationCancel(@NonNull final View view) {
    }
    
    @Override
    public void onAnimationEnd(@NonNull final View view) {
    }
    
    @Override
    public void onAnimationStart(@NonNull final View view) {
    }
}

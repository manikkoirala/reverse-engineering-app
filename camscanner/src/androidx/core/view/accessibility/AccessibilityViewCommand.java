// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view.accessibility;

import android.os.BaseBundle;
import androidx.annotation.RestrictTo;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.view.View;

public interface AccessibilityViewCommand
{
    boolean perform(@NonNull final View p0, @Nullable final CommandArguments p1);
    
    public abstract static class CommandArguments
    {
        Bundle mBundle;
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public void setBundle(@Nullable final Bundle mBundle) {
            this.mBundle = mBundle;
        }
    }
    
    public static final class MoveAtGranularityArguments extends CommandArguments
    {
        public boolean getExtendSelection() {
            return super.mBundle.getBoolean("ACTION_ARGUMENT_EXTEND_SELECTION_BOOLEAN");
        }
        
        public int getGranularity() {
            return ((BaseBundle)super.mBundle).getInt("ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT");
        }
    }
    
    public static final class MoveHtmlArguments extends CommandArguments
    {
        @Nullable
        public String getHTMLElement() {
            return ((BaseBundle)super.mBundle).getString("ACTION_ARGUMENT_HTML_ELEMENT_STRING");
        }
    }
    
    public static final class MoveWindowArguments extends CommandArguments
    {
        public int getX() {
            return ((BaseBundle)super.mBundle).getInt("ACTION_ARGUMENT_MOVE_WINDOW_X");
        }
        
        public int getY() {
            return ((BaseBundle)super.mBundle).getInt("ACTION_ARGUMENT_MOVE_WINDOW_Y");
        }
    }
    
    public static final class ScrollToPositionArguments extends CommandArguments
    {
        public int getColumn() {
            return ((BaseBundle)super.mBundle).getInt("android.view.accessibility.action.ARGUMENT_COLUMN_INT");
        }
        
        public int getRow() {
            return ((BaseBundle)super.mBundle).getInt("android.view.accessibility.action.ARGUMENT_ROW_INT");
        }
    }
    
    public static final class SetProgressArguments extends CommandArguments
    {
        public float getProgress() {
            return super.mBundle.getFloat("android.view.accessibility.action.ARGUMENT_PROGRESS_VALUE");
        }
    }
    
    public static final class SetSelectionArguments extends CommandArguments
    {
        public int getEnd() {
            return ((BaseBundle)super.mBundle).getInt("ACTION_ARGUMENT_SELECTION_END_INT");
        }
        
        public int getStart() {
            return ((BaseBundle)super.mBundle).getInt("ACTION_ARGUMENT_SELECTION_START_INT");
        }
    }
    
    public static final class SetTextArguments extends CommandArguments
    {
        @Nullable
        public CharSequence getText() {
            return super.mBundle.getCharSequence("ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE");
        }
    }
}

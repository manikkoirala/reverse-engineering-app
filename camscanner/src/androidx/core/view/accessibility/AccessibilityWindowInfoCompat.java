// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view.accessibility;

import android.view.accessibility.AccessibilityNodeInfo;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.graphics.Region;
import androidx.annotation.NonNull;
import android.graphics.Rect;
import android.os.Build$VERSION;
import android.view.accessibility.AccessibilityWindowInfo;
import androidx.annotation.Nullable;

public class AccessibilityWindowInfoCompat
{
    public static final int TYPE_ACCESSIBILITY_OVERLAY = 4;
    public static final int TYPE_APPLICATION = 1;
    public static final int TYPE_INPUT_METHOD = 2;
    public static final int TYPE_SPLIT_SCREEN_DIVIDER = 5;
    public static final int TYPE_SYSTEM = 3;
    private static final int UNDEFINED = -1;
    private final Object mInfo;
    
    private AccessibilityWindowInfoCompat(final Object mInfo) {
        this.mInfo = mInfo;
    }
    
    @Nullable
    public static AccessibilityWindowInfoCompat obtain() {
        return wrapNonNullInstance(Api21Impl.obtain());
    }
    
    @Nullable
    public static AccessibilityWindowInfoCompat obtain(@Nullable AccessibilityWindowInfoCompat wrapNonNullInstance) {
        if (wrapNonNullInstance == null) {
            wrapNonNullInstance = null;
        }
        else {
            wrapNonNullInstance = wrapNonNullInstance(Api21Impl.obtain((AccessibilityWindowInfo)wrapNonNullInstance.mInfo));
        }
        return wrapNonNullInstance;
    }
    
    private static String typeToString(final int n) {
        if (n == 1) {
            return "TYPE_APPLICATION";
        }
        if (n == 2) {
            return "TYPE_INPUT_METHOD";
        }
        if (n == 3) {
            return "TYPE_SYSTEM";
        }
        if (n != 4) {
            return "<UNKNOWN>";
        }
        return "TYPE_ACCESSIBILITY_OVERLAY";
    }
    
    static AccessibilityWindowInfoCompat wrapNonNullInstance(final Object o) {
        if (o != null) {
            return new AccessibilityWindowInfoCompat(o);
        }
        return null;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (!(o instanceof AccessibilityWindowInfoCompat)) {
            return false;
        }
        final AccessibilityWindowInfoCompat accessibilityWindowInfoCompat = (AccessibilityWindowInfoCompat)o;
        final Object mInfo = this.mInfo;
        if (mInfo == null) {
            if (accessibilityWindowInfoCompat.mInfo != null) {
                b = false;
            }
            return b;
        }
        return mInfo.equals(accessibilityWindowInfoCompat.mInfo);
    }
    
    @Nullable
    public AccessibilityNodeInfoCompat getAnchor() {
        if (Build$VERSION.SDK_INT >= 24) {
            return AccessibilityNodeInfoCompat.wrapNonNullInstance(Api24Impl.getAnchor((AccessibilityWindowInfo)this.mInfo));
        }
        return null;
    }
    
    public void getBoundsInScreen(@NonNull final Rect rect) {
        Api21Impl.getBoundsInScreen((AccessibilityWindowInfo)this.mInfo, rect);
    }
    
    @Nullable
    public AccessibilityWindowInfoCompat getChild(final int n) {
        return wrapNonNullInstance(Api21Impl.getChild((AccessibilityWindowInfo)this.mInfo, n));
    }
    
    public int getChildCount() {
        return Api21Impl.getChildCount((AccessibilityWindowInfo)this.mInfo);
    }
    
    public int getDisplayId() {
        if (Build$VERSION.SDK_INT >= 33) {
            return Api33Impl.getDisplayId((AccessibilityWindowInfo)this.mInfo);
        }
        return 0;
    }
    
    public int getId() {
        return Api21Impl.getId((AccessibilityWindowInfo)this.mInfo);
    }
    
    public int getLayer() {
        return Api21Impl.getLayer((AccessibilityWindowInfo)this.mInfo);
    }
    
    @Nullable
    public AccessibilityWindowInfoCompat getParent() {
        return wrapNonNullInstance(Api21Impl.getParent((AccessibilityWindowInfo)this.mInfo));
    }
    
    public void getRegionInScreen(@NonNull final Region region) {
        if (Build$VERSION.SDK_INT >= 33) {
            Api33Impl.getRegionInScreen((AccessibilityWindowInfo)this.mInfo, region);
        }
        else {
            final Rect rect = new Rect();
            Api21Impl.getBoundsInScreen((AccessibilityWindowInfo)this.mInfo, rect);
            region.set(rect);
        }
    }
    
    @Nullable
    public AccessibilityNodeInfoCompat getRoot() {
        return AccessibilityNodeInfoCompat.wrapNonNullInstance(Api21Impl.getRoot((AccessibilityWindowInfo)this.mInfo));
    }
    
    @Nullable
    public CharSequence getTitle() {
        if (Build$VERSION.SDK_INT >= 24) {
            return Api24Impl.getTitle((AccessibilityWindowInfo)this.mInfo);
        }
        return null;
    }
    
    public int getType() {
        return Api21Impl.getType((AccessibilityWindowInfo)this.mInfo);
    }
    
    @Override
    public int hashCode() {
        final Object mInfo = this.mInfo;
        int hashCode;
        if (mInfo == null) {
            hashCode = 0;
        }
        else {
            hashCode = mInfo.hashCode();
        }
        return hashCode;
    }
    
    public boolean isAccessibilityFocused() {
        return Api21Impl.isAccessibilityFocused((AccessibilityWindowInfo)this.mInfo);
    }
    
    public boolean isActive() {
        return Api21Impl.isActive((AccessibilityWindowInfo)this.mInfo);
    }
    
    public boolean isFocused() {
        return Api21Impl.isFocused((AccessibilityWindowInfo)this.mInfo);
    }
    
    public boolean isInPictureInPictureMode() {
        return Build$VERSION.SDK_INT >= 33 && Api33Impl.isInPictureInPictureMode((AccessibilityWindowInfo)this.mInfo);
    }
    
    public void recycle() {
        Api21Impl.recycle((AccessibilityWindowInfo)this.mInfo);
    }
    
    @NonNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        final Rect obj = new Rect();
        this.getBoundsInScreen(obj);
        sb.append("AccessibilityWindowInfo[");
        sb.append("id=");
        sb.append(this.getId());
        sb.append(", type=");
        sb.append(typeToString(this.getType()));
        sb.append(", layer=");
        sb.append(this.getLayer());
        sb.append(", bounds=");
        sb.append(obj);
        sb.append(", focused=");
        sb.append(this.isFocused());
        sb.append(", active=");
        sb.append(this.isActive());
        sb.append(", hasParent=");
        final AccessibilityWindowInfoCompat parent = this.getParent();
        final boolean b = true;
        sb.append(parent != null);
        sb.append(", hasChildren=");
        sb.append(this.getChildCount() > 0 && b);
        sb.append(']');
        return sb.toString();
    }
    
    @Nullable
    public AccessibilityWindowInfo unwrap() {
        return (AccessibilityWindowInfo)this.mInfo;
    }
    
    @RequiresApi(21)
    private static class Api21Impl
    {
        @DoNotInline
        static void getBoundsInScreen(final AccessibilityWindowInfo accessibilityWindowInfo, final Rect rect) {
            accessibilityWindowInfo.getBoundsInScreen(rect);
        }
        
        @DoNotInline
        static AccessibilityWindowInfo getChild(final AccessibilityWindowInfo accessibilityWindowInfo, final int n) {
            return accessibilityWindowInfo.getChild(n);
        }
        
        @DoNotInline
        static int getChildCount(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return accessibilityWindowInfo.getChildCount();
        }
        
        @DoNotInline
        static int getId(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return accessibilityWindowInfo.getId();
        }
        
        @DoNotInline
        static int getLayer(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return accessibilityWindowInfo.getLayer();
        }
        
        @DoNotInline
        static AccessibilityWindowInfo getParent(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return accessibilityWindowInfo.getParent();
        }
        
        @DoNotInline
        static AccessibilityNodeInfo getRoot(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return accessibilityWindowInfo.getRoot();
        }
        
        @DoNotInline
        static int getType(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return accessibilityWindowInfo.getType();
        }
        
        @DoNotInline
        static boolean isAccessibilityFocused(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return accessibilityWindowInfo.isAccessibilityFocused();
        }
        
        @DoNotInline
        static boolean isActive(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return accessibilityWindowInfo.isActive();
        }
        
        @DoNotInline
        static boolean isFocused(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return accessibilityWindowInfo.isFocused();
        }
        
        @DoNotInline
        static AccessibilityWindowInfo obtain() {
            return AccessibilityWindowInfo.obtain();
        }
        
        @DoNotInline
        static AccessibilityWindowInfo obtain(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return AccessibilityWindowInfo.obtain(accessibilityWindowInfo);
        }
        
        @DoNotInline
        static void recycle(final AccessibilityWindowInfo accessibilityWindowInfo) {
            accessibilityWindowInfo.recycle();
        }
    }
    
    @RequiresApi(24)
    private static class Api24Impl
    {
        @DoNotInline
        static AccessibilityNodeInfo getAnchor(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return \u3007O\u300780o08O.\u3007080(accessibilityWindowInfo);
        }
        
        @DoNotInline
        static CharSequence getTitle(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return Ooo.\u3007080(accessibilityWindowInfo);
        }
    }
    
    @RequiresApi(33)
    private static class Api33Impl
    {
        @DoNotInline
        static int getDisplayId(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return ooo\u30078oO.\u3007080(accessibilityWindowInfo);
        }
        
        @DoNotInline
        static void getRegionInScreen(final AccessibilityWindowInfo accessibilityWindowInfo, final Region region) {
            O0o\u3007\u3007Oo.\u3007080(accessibilityWindowInfo, region);
        }
        
        @DoNotInline
        static boolean isInPictureInPictureMode(final AccessibilityWindowInfo accessibilityWindowInfo) {
            return OO8oO0o\u3007.\u3007080(accessibilityWindowInfo);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view.accessibility;

import android.text.Spannable;
import android.os.BaseBundle;
import androidx.annotation.IntRange;
import android.graphics.Region;
import java.util.Map;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import androidx.annotation.OptIn;
import android.view.accessibility.AccessibilityNodeInfo$TouchDelegateInfo;
import android.text.SpannableString;
import android.text.TextUtils;
import androidx.core.os.BuildCompat;
import android.view.accessibility.AccessibilityNodeInfo$RangeInfo;
import androidx.annotation.Nullable;
import android.view.accessibility.AccessibilityNodeInfo$ExtraRenderingInfo;
import android.view.accessibility.AccessibilityNodeInfo$CollectionItemInfo;
import android.view.accessibility.AccessibilityNodeInfo$CollectionInfo;
import android.graphics.Rect;
import java.util.Collections;
import java.util.Iterator;
import android.os.Build$VERSION;
import android.view.accessibility.AccessibilityNodeInfo$AccessibilityAction;
import androidx.annotation.NonNull;
import androidx.core.R;
import java.lang.ref.WeakReference;
import android.util.SparseArray;
import android.view.View;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;
import android.text.Spanned;
import android.text.style.ClickableSpan;
import androidx.annotation.RestrictTo;
import android.view.accessibility.AccessibilityNodeInfo;
import android.annotation.SuppressLint;

public class AccessibilityNodeInfoCompat
{
    public static final int ACTION_ACCESSIBILITY_FOCUS = 64;
    public static final String ACTION_ARGUMENT_COLUMN_INT = "android.view.accessibility.action.ARGUMENT_COLUMN_INT";
    public static final String ACTION_ARGUMENT_EXTEND_SELECTION_BOOLEAN = "ACTION_ARGUMENT_EXTEND_SELECTION_BOOLEAN";
    public static final String ACTION_ARGUMENT_HTML_ELEMENT_STRING = "ACTION_ARGUMENT_HTML_ELEMENT_STRING";
    public static final String ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT = "ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT";
    public static final String ACTION_ARGUMENT_MOVE_WINDOW_X = "ACTION_ARGUMENT_MOVE_WINDOW_X";
    public static final String ACTION_ARGUMENT_MOVE_WINDOW_Y = "ACTION_ARGUMENT_MOVE_WINDOW_Y";
    @SuppressLint({ "ActionValue" })
    public static final String ACTION_ARGUMENT_PRESS_AND_HOLD_DURATION_MILLIS_INT = "android.view.accessibility.action.ARGUMENT_PRESS_AND_HOLD_DURATION_MILLIS_INT";
    public static final String ACTION_ARGUMENT_PROGRESS_VALUE = "android.view.accessibility.action.ARGUMENT_PROGRESS_VALUE";
    public static final String ACTION_ARGUMENT_ROW_INT = "android.view.accessibility.action.ARGUMENT_ROW_INT";
    public static final String ACTION_ARGUMENT_SELECTION_END_INT = "ACTION_ARGUMENT_SELECTION_END_INT";
    public static final String ACTION_ARGUMENT_SELECTION_START_INT = "ACTION_ARGUMENT_SELECTION_START_INT";
    public static final String ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE = "ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE";
    public static final int ACTION_CLEAR_ACCESSIBILITY_FOCUS = 128;
    public static final int ACTION_CLEAR_FOCUS = 2;
    public static final int ACTION_CLEAR_SELECTION = 8;
    public static final int ACTION_CLICK = 16;
    public static final int ACTION_COLLAPSE = 524288;
    public static final int ACTION_COPY = 16384;
    public static final int ACTION_CUT = 65536;
    public static final int ACTION_DISMISS = 1048576;
    public static final int ACTION_EXPAND = 262144;
    public static final int ACTION_FOCUS = 1;
    public static final int ACTION_LONG_CLICK = 32;
    public static final int ACTION_NEXT_AT_MOVEMENT_GRANULARITY = 256;
    public static final int ACTION_NEXT_HTML_ELEMENT = 1024;
    public static final int ACTION_PASTE = 32768;
    public static final int ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY = 512;
    public static final int ACTION_PREVIOUS_HTML_ELEMENT = 2048;
    public static final int ACTION_SCROLL_BACKWARD = 8192;
    public static final int ACTION_SCROLL_FORWARD = 4096;
    public static final int ACTION_SELECT = 4;
    public static final int ACTION_SET_SELECTION = 131072;
    public static final int ACTION_SET_TEXT = 2097152;
    private static final int BOOLEAN_PROPERTY_IS_HEADING = 2;
    private static final int BOOLEAN_PROPERTY_IS_SHOWING_HINT = 4;
    private static final int BOOLEAN_PROPERTY_IS_TEXT_ENTRY_KEY = 8;
    private static final String BOOLEAN_PROPERTY_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY";
    private static final int BOOLEAN_PROPERTY_SCREEN_READER_FOCUSABLE = 1;
    public static final String EXTRA_DATA_TEXT_CHARACTER_LOCATION_ARG_LENGTH = "android.core.view.accessibility.extra.DATA_TEXT_CHARACTER_LOCATION_ARG_LENGTH";
    public static final int EXTRA_DATA_TEXT_CHARACTER_LOCATION_ARG_MAX_LENGTH = 20000;
    public static final String EXTRA_DATA_TEXT_CHARACTER_LOCATION_ARG_START_INDEX = "android.core.view.accessibility.extra.DATA_TEXT_CHARACTER_LOCATION_ARG_START_INDEX";
    public static final String EXTRA_DATA_TEXT_CHARACTER_LOCATION_KEY = "android.core.view.accessibility.extra.DATA_TEXT_CHARACTER_LOCATION_KEY";
    public static final int FOCUS_ACCESSIBILITY = 2;
    public static final int FOCUS_INPUT = 1;
    private static final String HINT_TEXT_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.HINT_TEXT_KEY";
    public static final int MOVEMENT_GRANULARITY_CHARACTER = 1;
    public static final int MOVEMENT_GRANULARITY_LINE = 4;
    public static final int MOVEMENT_GRANULARITY_PAGE = 16;
    public static final int MOVEMENT_GRANULARITY_PARAGRAPH = 8;
    public static final int MOVEMENT_GRANULARITY_WORD = 2;
    private static final String PANE_TITLE_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.PANE_TITLE_KEY";
    private static final String ROLE_DESCRIPTION_KEY = "AccessibilityNodeInfo.roleDescription";
    private static final String SPANS_ACTION_ID_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ACTION_ID_KEY";
    private static final String SPANS_END_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY";
    private static final String SPANS_FLAGS_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY";
    private static final String SPANS_ID_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY";
    private static final String SPANS_START_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY";
    private static final String STATE_DESCRIPTION_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.STATE_DESCRIPTION_KEY";
    private static final String TOOLTIP_TEXT_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.TOOLTIP_TEXT_KEY";
    private static final String UNIQUE_ID_KEY = "androidx.view.accessibility.AccessibilityNodeInfoCompat.UNIQUE_ID_KEY";
    private static int sClickableSpanId;
    private final AccessibilityNodeInfo mInfo;
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public int mParentVirtualDescendantId;
    private int mVirtualDescendantId;
    
    private AccessibilityNodeInfoCompat(final AccessibilityNodeInfo mInfo) {
        this.mParentVirtualDescendantId = -1;
        this.mVirtualDescendantId = -1;
        this.mInfo = mInfo;
    }
    
    @Deprecated
    public AccessibilityNodeInfoCompat(final Object o) {
        this.mParentVirtualDescendantId = -1;
        this.mVirtualDescendantId = -1;
        this.mInfo = (AccessibilityNodeInfo)o;
    }
    
    private void addSpanLocationToExtras(final ClickableSpan clickableSpan, final Spanned spanned, final int i) {
        this.extrasIntList("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY").add(spanned.getSpanStart((Object)clickableSpan));
        this.extrasIntList("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY").add(spanned.getSpanEnd((Object)clickableSpan));
        this.extrasIntList("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY").add(spanned.getSpanFlags((Object)clickableSpan));
        this.extrasIntList("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY").add(i);
    }
    
    private void clearExtrasSpans() {
        this.mInfo.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY");
        this.mInfo.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY");
        this.mInfo.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY");
        this.mInfo.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY");
    }
    
    private List<Integer> extrasIntList(final String s) {
        ArrayList integerArrayList;
        if ((integerArrayList = this.mInfo.getExtras().getIntegerArrayList(s)) == null) {
            integerArrayList = new ArrayList();
            this.mInfo.getExtras().putIntegerArrayList(s, integerArrayList);
        }
        return integerArrayList;
    }
    
    private static String getActionSymbolicName(final int n) {
        if (n == 1) {
            return "ACTION_FOCUS";
        }
        if (n == 2) {
            return "ACTION_CLEAR_FOCUS";
        }
        switch (n) {
            default: {
                switch (n) {
                    default: {
                        switch (n) {
                            default: {
                                switch (n) {
                                    default: {
                                        return "ACTION_UNKNOWN";
                                    }
                                    case 16908375: {
                                        return "ACTION_DRAG_CANCEL";
                                    }
                                    case 16908374: {
                                        return "ACTION_DRAG_DROP";
                                    }
                                    case 16908373: {
                                        return "ACTION_DRAG_START";
                                    }
                                    case 16908372: {
                                        return "ACTION_IME_ENTER";
                                    }
                                }
                                break;
                            }
                            case 16908362: {
                                return "ACTION_PRESS_AND_HOLD";
                            }
                            case 16908361: {
                                return "ACTION_PAGE_RIGHT";
                            }
                            case 16908360: {
                                return "ACTION_PAGE_LEFT";
                            }
                            case 16908359: {
                                return "ACTION_PAGE_DOWN";
                            }
                            case 16908358: {
                                return "ACTION_PAGE_UP";
                            }
                            case 16908357: {
                                return "ACTION_HIDE_TOOLTIP";
                            }
                            case 16908356: {
                                return "ACTION_SHOW_TOOLTIP";
                            }
                        }
                        break;
                    }
                    case 16908349: {
                        return "ACTION_SET_PROGRESS";
                    }
                    case 16908348: {
                        return "ACTION_CONTEXT_CLICK";
                    }
                    case 16908347: {
                        return "ACTION_SCROLL_RIGHT";
                    }
                    case 16908346: {
                        return "ACTION_SCROLL_DOWN";
                    }
                    case 16908345: {
                        return "ACTION_SCROLL_LEFT";
                    }
                    case 16908344: {
                        return "ACTION_SCROLL_UP";
                    }
                    case 16908343: {
                        return "ACTION_SCROLL_TO_POSITION";
                    }
                    case 16908342: {
                        return "ACTION_SHOW_ON_SCREEN";
                    }
                }
                break;
            }
            case 16908354: {
                return "ACTION_MOVE_WINDOW";
            }
            case 2097152: {
                return "ACTION_SET_TEXT";
            }
            case 524288: {
                return "ACTION_COLLAPSE";
            }
            case 262144: {
                return "ACTION_EXPAND";
            }
            case 131072: {
                return "ACTION_SET_SELECTION";
            }
            case 65536: {
                return "ACTION_CUT";
            }
            case 32768: {
                return "ACTION_PASTE";
            }
            case 16384: {
                return "ACTION_COPY";
            }
            case 8192: {
                return "ACTION_SCROLL_BACKWARD";
            }
            case 4096: {
                return "ACTION_SCROLL_FORWARD";
            }
            case 2048: {
                return "ACTION_PREVIOUS_HTML_ELEMENT";
            }
            case 1024: {
                return "ACTION_NEXT_HTML_ELEMENT";
            }
            case 512: {
                return "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY";
            }
            case 256: {
                return "ACTION_NEXT_AT_MOVEMENT_GRANULARITY";
            }
            case 128: {
                return "ACTION_CLEAR_ACCESSIBILITY_FOCUS";
            }
            case 64: {
                return "ACTION_ACCESSIBILITY_FOCUS";
            }
            case 32: {
                return "ACTION_LONG_CLICK";
            }
            case 16: {
                return "ACTION_CLICK";
            }
            case 8: {
                return "ACTION_CLEAR_SELECTION";
            }
            case 4: {
                return "ACTION_SELECT";
            }
        }
    }
    
    private boolean getBooleanProperty(final int n) {
        final Bundle extras = this.getExtras();
        boolean b = false;
        if (extras == null) {
            return false;
        }
        if ((((BaseBundle)extras).getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", 0) & n) == n) {
            b = true;
        }
        return b;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static ClickableSpan[] getClickableSpans(final CharSequence charSequence) {
        if (charSequence instanceof Spanned) {
            return (ClickableSpan[])((Spanned)charSequence).getSpans(0, charSequence.length(), (Class)ClickableSpan.class);
        }
        return null;
    }
    
    private SparseArray<WeakReference<ClickableSpan>> getOrCreateSpansFromViewTags(final View view) {
        SparseArray spansFromViewTags;
        if ((spansFromViewTags = this.getSpansFromViewTags(view)) == null) {
            spansFromViewTags = new SparseArray();
            view.setTag(R.id.tag_accessibility_clickable_spans, (Object)spansFromViewTags);
        }
        return (SparseArray<WeakReference<ClickableSpan>>)spansFromViewTags;
    }
    
    private SparseArray<WeakReference<ClickableSpan>> getSpansFromViewTags(final View view) {
        return (SparseArray<WeakReference<ClickableSpan>>)view.getTag(R.id.tag_accessibility_clickable_spans);
    }
    
    private boolean hasSpans() {
        return this.extrasIntList("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY").isEmpty() ^ true;
    }
    
    private int idForClickableSpan(final ClickableSpan clickableSpan, final SparseArray<WeakReference<ClickableSpan>> sparseArray) {
        if (sparseArray != null) {
            for (int i = 0; i < sparseArray.size(); ++i) {
                if (clickableSpan.equals(((WeakReference)sparseArray.valueAt(i)).get())) {
                    return sparseArray.keyAt(i);
                }
            }
        }
        final int sClickableSpanId = AccessibilityNodeInfoCompat.sClickableSpanId;
        AccessibilityNodeInfoCompat.sClickableSpanId = sClickableSpanId + 1;
        return sClickableSpanId;
    }
    
    public static AccessibilityNodeInfoCompat obtain() {
        return wrap(AccessibilityNodeInfo.obtain());
    }
    
    public static AccessibilityNodeInfoCompat obtain(final View view) {
        return wrap(AccessibilityNodeInfo.obtain(view));
    }
    
    public static AccessibilityNodeInfoCompat obtain(final View view, final int n) {
        return wrapNonNullInstance(AccessibilityNodeInfo.obtain(view, n));
    }
    
    public static AccessibilityNodeInfoCompat obtain(final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        return wrap(AccessibilityNodeInfo.obtain(accessibilityNodeInfoCompat.mInfo));
    }
    
    private void removeCollectedSpans(final View view) {
        final SparseArray<WeakReference<ClickableSpan>> spansFromViewTags = this.getSpansFromViewTags(view);
        if (spansFromViewTags != null) {
            final ArrayList list = new ArrayList();
            final int n = 0;
            int i = 0;
            int j;
            while (true) {
                j = n;
                if (i >= spansFromViewTags.size()) {
                    break;
                }
                if (((WeakReference)spansFromViewTags.valueAt(i)).get() == null) {
                    list.add(i);
                }
                ++i;
            }
            while (j < list.size()) {
                spansFromViewTags.remove((int)list.get(j));
                ++j;
            }
        }
    }
    
    private void setBooleanProperty(final int n, final boolean b) {
        final Bundle extras = this.getExtras();
        if (extras != null) {
            final int int1 = ((BaseBundle)extras).getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", 0);
            int n2;
            if (b) {
                n2 = n;
            }
            else {
                n2 = 0;
            }
            ((BaseBundle)extras).putInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", n2 | (int1 & ~n));
        }
    }
    
    public static AccessibilityNodeInfoCompat wrap(@NonNull final AccessibilityNodeInfo accessibilityNodeInfo) {
        return new AccessibilityNodeInfoCompat(accessibilityNodeInfo);
    }
    
    static AccessibilityNodeInfoCompat wrapNonNullInstance(final Object o) {
        if (o != null) {
            return new AccessibilityNodeInfoCompat(o);
        }
        return null;
    }
    
    public void addAction(final int n) {
        this.mInfo.addAction(n);
    }
    
    public void addAction(final AccessibilityActionCompat accessibilityActionCompat) {
        this.mInfo.addAction((AccessibilityNodeInfo$AccessibilityAction)accessibilityActionCompat.mAction);
    }
    
    public void addChild(final View view) {
        this.mInfo.addChild(view);
    }
    
    public void addChild(final View view, final int n) {
        this.mInfo.addChild(view, n);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void addSpansToExtras(final CharSequence charSequence, final View view) {
        if (Build$VERSION.SDK_INT < 26) {
            this.clearExtrasSpans();
            this.removeCollectedSpans(view);
            final ClickableSpan[] clickableSpans = getClickableSpans(charSequence);
            if (clickableSpans != null && clickableSpans.length > 0) {
                ((BaseBundle)this.getExtras()).putInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ACTION_ID_KEY", R.id.accessibility_action_clickable_span);
                final SparseArray<WeakReference<ClickableSpan>> orCreateSpansFromViewTags = this.getOrCreateSpansFromViewTags(view);
                for (int i = 0; i < clickableSpans.length; ++i) {
                    final int idForClickableSpan = this.idForClickableSpan(clickableSpans[i], orCreateSpansFromViewTags);
                    orCreateSpansFromViewTags.put(idForClickableSpan, (Object)new WeakReference(clickableSpans[i]));
                    this.addSpanLocationToExtras(clickableSpans[i], (Spanned)charSequence, idForClickableSpan);
                }
            }
        }
    }
    
    public boolean canOpenPopup() {
        return this.mInfo.canOpenPopup();
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (!(o instanceof AccessibilityNodeInfoCompat)) {
            return false;
        }
        final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat = (AccessibilityNodeInfoCompat)o;
        final AccessibilityNodeInfo mInfo = this.mInfo;
        if (mInfo == null) {
            if (accessibilityNodeInfoCompat.mInfo != null) {
                return false;
            }
        }
        else if (!mInfo.equals((Object)accessibilityNodeInfoCompat.mInfo)) {
            return false;
        }
        return this.mVirtualDescendantId == accessibilityNodeInfoCompat.mVirtualDescendantId && this.mParentVirtualDescendantId == accessibilityNodeInfoCompat.mParentVirtualDescendantId;
    }
    
    public List<AccessibilityNodeInfoCompat> findAccessibilityNodeInfosByText(final String s) {
        final ArrayList list = new ArrayList();
        final List accessibilityNodeInfosByText = this.mInfo.findAccessibilityNodeInfosByText(s);
        for (int size = accessibilityNodeInfosByText.size(), i = 0; i < size; ++i) {
            list.add(wrap((AccessibilityNodeInfo)accessibilityNodeInfosByText.get(i)));
        }
        return list;
    }
    
    public List<AccessibilityNodeInfoCompat> findAccessibilityNodeInfosByViewId(final String s) {
        final List accessibilityNodeInfosByViewId = this.mInfo.findAccessibilityNodeInfosByViewId(s);
        final ArrayList list = new ArrayList();
        final Iterator iterator = accessibilityNodeInfosByViewId.iterator();
        while (iterator.hasNext()) {
            list.add(wrap((AccessibilityNodeInfo)iterator.next()));
        }
        return list;
    }
    
    public AccessibilityNodeInfoCompat findFocus(final int n) {
        return wrapNonNullInstance(this.mInfo.findFocus(n));
    }
    
    public AccessibilityNodeInfoCompat focusSearch(final int n) {
        return wrapNonNullInstance(this.mInfo.focusSearch(n));
    }
    
    public List<AccessibilityActionCompat> getActionList() {
        final List actionList = this.mInfo.getActionList();
        if (actionList != null) {
            final ArrayList list = new ArrayList();
            for (int size = actionList.size(), i = 0; i < size; ++i) {
                list.add(new AccessibilityActionCompat(actionList.get(i)));
            }
            return list;
        }
        return Collections.emptyList();
    }
    
    @Deprecated
    public int getActions() {
        return this.mInfo.getActions();
    }
    
    @NonNull
    public List<String> getAvailableExtraData() {
        if (Build$VERSION.SDK_INT >= 26) {
            return o\u3007O8\u3007\u3007o.\u3007080(this.mInfo);
        }
        return Collections.emptyList();
    }
    
    @Deprecated
    public void getBoundsInParent(final Rect rect) {
        this.mInfo.getBoundsInParent(rect);
    }
    
    public void getBoundsInScreen(final Rect rect) {
        this.mInfo.getBoundsInScreen(rect);
    }
    
    public AccessibilityNodeInfoCompat getChild(final int n) {
        return wrapNonNullInstance(this.mInfo.getChild(n));
    }
    
    public int getChildCount() {
        return this.mInfo.getChildCount();
    }
    
    public CharSequence getClassName() {
        return this.mInfo.getClassName();
    }
    
    public CollectionInfoCompat getCollectionInfo() {
        final AccessibilityNodeInfo$CollectionInfo collectionInfo = this.mInfo.getCollectionInfo();
        if (collectionInfo != null) {
            return new CollectionInfoCompat(collectionInfo);
        }
        return null;
    }
    
    public CollectionItemInfoCompat getCollectionItemInfo() {
        final AccessibilityNodeInfo$CollectionItemInfo collectionItemInfo = this.mInfo.getCollectionItemInfo();
        if (collectionItemInfo != null) {
            return new CollectionItemInfoCompat(collectionItemInfo);
        }
        return null;
    }
    
    public CharSequence getContentDescription() {
        return this.mInfo.getContentDescription();
    }
    
    public int getDrawingOrder() {
        if (Build$VERSION.SDK_INT >= 24) {
            return \u300700.\u3007080(this.mInfo);
        }
        return 0;
    }
    
    public CharSequence getError() {
        return this.mInfo.getError();
    }
    
    @Nullable
    public AccessibilityNodeInfo$ExtraRenderingInfo getExtraRenderingInfo() {
        if (Build$VERSION.SDK_INT >= 33) {
            return Api33Impl.getExtraRenderingInfo(this.mInfo);
        }
        return null;
    }
    
    public Bundle getExtras() {
        return this.mInfo.getExtras();
    }
    
    @Nullable
    public CharSequence getHintText() {
        if (Build$VERSION.SDK_INT >= 26) {
            return \u3007080.\u3007080(this.mInfo);
        }
        return this.mInfo.getExtras().getCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.HINT_TEXT_KEY");
    }
    
    @Deprecated
    public Object getInfo() {
        return this.mInfo;
    }
    
    public int getInputType() {
        return this.mInfo.getInputType();
    }
    
    public AccessibilityNodeInfoCompat getLabelFor() {
        return wrapNonNullInstance(this.mInfo.getLabelFor());
    }
    
    public AccessibilityNodeInfoCompat getLabeledBy() {
        return wrapNonNullInstance(this.mInfo.getLabeledBy());
    }
    
    public int getLiveRegion() {
        return this.mInfo.getLiveRegion();
    }
    
    public int getMaxTextLength() {
        return this.mInfo.getMaxTextLength();
    }
    
    public int getMovementGranularities() {
        return this.mInfo.getMovementGranularities();
    }
    
    public CharSequence getPackageName() {
        return this.mInfo.getPackageName();
    }
    
    @Nullable
    public CharSequence getPaneTitle() {
        if (Build$VERSION.SDK_INT >= 28) {
            return Oooo8o0\u3007.\u3007080(this.mInfo);
        }
        return this.mInfo.getExtras().getCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.PANE_TITLE_KEY");
    }
    
    public AccessibilityNodeInfoCompat getParent() {
        return wrapNonNullInstance(this.mInfo.getParent());
    }
    
    public RangeInfoCompat getRangeInfo() {
        final AccessibilityNodeInfo$RangeInfo rangeInfo = this.mInfo.getRangeInfo();
        if (rangeInfo != null) {
            return new RangeInfoCompat(rangeInfo);
        }
        return null;
    }
    
    @Nullable
    public CharSequence getRoleDescription() {
        return this.mInfo.getExtras().getCharSequence("AccessibilityNodeInfo.roleDescription");
    }
    
    @Nullable
    public CharSequence getStateDescription() {
        if (BuildCompat.isAtLeastR()) {
            return \u3007\u30078O0\u30078.\u3007080(this.mInfo);
        }
        return this.mInfo.getExtras().getCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.STATE_DESCRIPTION_KEY");
    }
    
    public CharSequence getText() {
        if (this.hasSpans()) {
            final List<Integer> extrasIntList = this.extrasIntList("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY");
            final List<Integer> extrasIntList2 = this.extrasIntList("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY");
            final List<Integer> extrasIntList3 = this.extrasIntList("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY");
            final List<Integer> extrasIntList4 = this.extrasIntList("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY");
            final CharSequence text = this.mInfo.getText();
            final int length = this.mInfo.getText().length();
            int i = 0;
            final SpannableString spannableString = new SpannableString((CharSequence)TextUtils.substring(text, 0, length));
            while (i < extrasIntList.size()) {
                ((Spannable)spannableString).setSpan((Object)new AccessibilityClickableSpanCompat(extrasIntList4.get(i), this, ((BaseBundle)this.getExtras()).getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ACTION_ID_KEY")), (int)extrasIntList.get(i), (int)extrasIntList2.get(i), (int)extrasIntList3.get(i));
                ++i;
            }
            return (CharSequence)spannableString;
        }
        return this.mInfo.getText();
    }
    
    public int getTextSelectionEnd() {
        return this.mInfo.getTextSelectionEnd();
    }
    
    public int getTextSelectionStart() {
        return this.mInfo.getTextSelectionStart();
    }
    
    @Nullable
    public CharSequence getTooltipText() {
        if (Build$VERSION.SDK_INT >= 28) {
            return OO0o\u3007\u3007\u3007\u30070.\u3007080(this.mInfo);
        }
        return this.mInfo.getExtras().getCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.TOOLTIP_TEXT_KEY");
    }
    
    @Nullable
    public TouchDelegateInfoCompat getTouchDelegateInfo() {
        if (Build$VERSION.SDK_INT >= 29) {
            final AccessibilityNodeInfo$TouchDelegateInfo \u3007080 = OOO\u3007O0.\u3007080(this.mInfo);
            if (\u3007080 != null) {
                return new TouchDelegateInfoCompat(\u3007080);
            }
        }
        return null;
    }
    
    public AccessibilityNodeInfoCompat getTraversalAfter() {
        if (Build$VERSION.SDK_INT >= 22) {
            return wrapNonNullInstance(oo88o8O.\u3007080(this.mInfo));
        }
        return null;
    }
    
    public AccessibilityNodeInfoCompat getTraversalBefore() {
        if (Build$VERSION.SDK_INT >= 22) {
            return wrapNonNullInstance(\u3007\u3007888.\u3007080(this.mInfo));
        }
        return null;
    }
    
    @Nullable
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    public String getUniqueId() {
        if (BuildCompat.isAtLeastT()) {
            return this.mInfo.getUniqueId();
        }
        return ((BaseBundle)this.mInfo.getExtras()).getString("androidx.view.accessibility.AccessibilityNodeInfoCompat.UNIQUE_ID_KEY");
    }
    
    public String getViewIdResourceName() {
        return this.mInfo.getViewIdResourceName();
    }
    
    public AccessibilityWindowInfoCompat getWindow() {
        return AccessibilityWindowInfoCompat.wrapNonNullInstance(this.mInfo.getWindow());
    }
    
    public int getWindowId() {
        return this.mInfo.getWindowId();
    }
    
    @Override
    public int hashCode() {
        final AccessibilityNodeInfo mInfo = this.mInfo;
        int hashCode;
        if (mInfo == null) {
            hashCode = 0;
        }
        else {
            hashCode = mInfo.hashCode();
        }
        return hashCode;
    }
    
    public boolean isAccessibilityFocused() {
        return this.mInfo.isAccessibilityFocused();
    }
    
    public boolean isCheckable() {
        return this.mInfo.isCheckable();
    }
    
    public boolean isChecked() {
        return this.mInfo.isChecked();
    }
    
    public boolean isClickable() {
        return this.mInfo.isClickable();
    }
    
    public boolean isContentInvalid() {
        return this.mInfo.isContentInvalid();
    }
    
    public boolean isContextClickable() {
        return Build$VERSION.SDK_INT >= 23 && o\u30070.\u3007080(this.mInfo);
    }
    
    public boolean isDismissable() {
        return this.mInfo.isDismissable();
    }
    
    public boolean isEditable() {
        return this.mInfo.isEditable();
    }
    
    public boolean isEnabled() {
        return this.mInfo.isEnabled();
    }
    
    public boolean isFocusable() {
        return this.mInfo.isFocusable();
    }
    
    public boolean isFocused() {
        return this.mInfo.isFocused();
    }
    
    public boolean isHeading() {
        if (Build$VERSION.SDK_INT >= 28) {
            return \u30070000OOO.\u3007080(this.mInfo);
        }
        final boolean booleanProperty = this.getBooleanProperty(2);
        boolean b = true;
        if (booleanProperty) {
            return true;
        }
        final CollectionItemInfoCompat collectionItemInfo = this.getCollectionItemInfo();
        if (collectionItemInfo == null || !collectionItemInfo.isHeading()) {
            b = false;
        }
        return b;
    }
    
    public boolean isImportantForAccessibility() {
        return Build$VERSION.SDK_INT < 24 || \u3007O8o08O.\u3007080(this.mInfo);
    }
    
    public boolean isLongClickable() {
        return this.mInfo.isLongClickable();
    }
    
    public boolean isMultiLine() {
        return this.mInfo.isMultiLine();
    }
    
    public boolean isPassword() {
        return this.mInfo.isPassword();
    }
    
    public boolean isScreenReaderFocusable() {
        if (Build$VERSION.SDK_INT >= 28) {
            return OoO8.\u3007080(this.mInfo);
        }
        return this.getBooleanProperty(1);
    }
    
    public boolean isScrollable() {
        return this.mInfo.isScrollable();
    }
    
    public boolean isSelected() {
        return this.mInfo.isSelected();
    }
    
    public boolean isShowingHintText() {
        if (Build$VERSION.SDK_INT >= 26) {
            return O\u30078O8\u3007008.\u3007080(this.mInfo);
        }
        return this.getBooleanProperty(4);
    }
    
    public boolean isTextEntryKey() {
        if (Build$VERSION.SDK_INT >= 29) {
            return O8.\u3007080(this.mInfo);
        }
        return this.getBooleanProperty(8);
    }
    
    public boolean isTextSelectable() {
        return Build$VERSION.SDK_INT >= 33 && Api33Impl.isTextSelectable(this.mInfo);
    }
    
    public boolean isVisibleToUser() {
        return this.mInfo.isVisibleToUser();
    }
    
    public boolean performAction(final int n) {
        return this.mInfo.performAction(n);
    }
    
    public boolean performAction(final int n, final Bundle bundle) {
        return this.mInfo.performAction(n, bundle);
    }
    
    public void recycle() {
        this.mInfo.recycle();
    }
    
    public boolean refresh() {
        return this.mInfo.refresh();
    }
    
    public boolean removeAction(final AccessibilityActionCompat accessibilityActionCompat) {
        return this.mInfo.removeAction((AccessibilityNodeInfo$AccessibilityAction)accessibilityActionCompat.mAction);
    }
    
    public boolean removeChild(final View view) {
        return this.mInfo.removeChild(view);
    }
    
    public boolean removeChild(final View view, final int n) {
        return this.mInfo.removeChild(view, n);
    }
    
    public void setAccessibilityFocused(final boolean accessibilityFocused) {
        this.mInfo.setAccessibilityFocused(accessibilityFocused);
    }
    
    public void setAvailableExtraData(@NonNull final List<String> list) {
        if (Build$VERSION.SDK_INT >= 26) {
            \u3007O00.\u3007080(this.mInfo, (List)list);
        }
    }
    
    @Deprecated
    public void setBoundsInParent(final Rect boundsInParent) {
        this.mInfo.setBoundsInParent(boundsInParent);
    }
    
    public void setBoundsInScreen(final Rect boundsInScreen) {
        this.mInfo.setBoundsInScreen(boundsInScreen);
    }
    
    public void setCanOpenPopup(final boolean canOpenPopup) {
        this.mInfo.setCanOpenPopup(canOpenPopup);
    }
    
    public void setCheckable(final boolean checkable) {
        this.mInfo.setCheckable(checkable);
    }
    
    public void setChecked(final boolean checked) {
        this.mInfo.setChecked(checked);
    }
    
    public void setClassName(final CharSequence className) {
        this.mInfo.setClassName(className);
    }
    
    public void setClickable(final boolean clickable) {
        this.mInfo.setClickable(clickable);
    }
    
    public void setCollectionInfo(final Object o) {
        final AccessibilityNodeInfo mInfo = this.mInfo;
        AccessibilityNodeInfo$CollectionInfo collectionInfo;
        if (o == null) {
            collectionInfo = null;
        }
        else {
            collectionInfo = (AccessibilityNodeInfo$CollectionInfo)((CollectionInfoCompat)o).mInfo;
        }
        mInfo.setCollectionInfo(collectionInfo);
    }
    
    public void setCollectionItemInfo(final Object o) {
        final AccessibilityNodeInfo mInfo = this.mInfo;
        AccessibilityNodeInfo$CollectionItemInfo collectionItemInfo;
        if (o == null) {
            collectionItemInfo = null;
        }
        else {
            collectionItemInfo = (AccessibilityNodeInfo$CollectionItemInfo)((CollectionItemInfoCompat)o).mInfo;
        }
        mInfo.setCollectionItemInfo(collectionItemInfo);
    }
    
    public void setContentDescription(final CharSequence contentDescription) {
        this.mInfo.setContentDescription(contentDescription);
    }
    
    public void setContentInvalid(final boolean contentInvalid) {
        this.mInfo.setContentInvalid(contentInvalid);
    }
    
    public void setContextClickable(final boolean b) {
        if (Build$VERSION.SDK_INT >= 23) {
            \u300780\u3007808\u3007O.\u3007080(this.mInfo, b);
        }
    }
    
    public void setDismissable(final boolean dismissable) {
        this.mInfo.setDismissable(dismissable);
    }
    
    public void setDrawingOrder(final int n) {
        if (Build$VERSION.SDK_INT >= 24) {
            \u3007o00\u3007\u3007Oo.\u3007080(this.mInfo, n);
        }
    }
    
    public void setEditable(final boolean editable) {
        this.mInfo.setEditable(editable);
    }
    
    public void setEnabled(final boolean enabled) {
        this.mInfo.setEnabled(enabled);
    }
    
    public void setError(final CharSequence error) {
        this.mInfo.setError(error);
    }
    
    public void setFocusable(final boolean focusable) {
        this.mInfo.setFocusable(focusable);
    }
    
    public void setFocused(final boolean focused) {
        this.mInfo.setFocused(focused);
    }
    
    public void setHeading(final boolean b) {
        if (Build$VERSION.SDK_INT >= 28) {
            \u3007O888o0o.\u3007080(this.mInfo, b);
        }
        else {
            this.setBooleanProperty(2, b);
        }
    }
    
    public void setHintText(@Nullable final CharSequence charSequence) {
        if (Build$VERSION.SDK_INT >= 26) {
            \u3007oo\u3007.\u3007080(this.mInfo, charSequence);
        }
        else {
            this.mInfo.getExtras().putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.HINT_TEXT_KEY", charSequence);
        }
    }
    
    public void setImportantForAccessibility(final boolean b) {
        if (Build$VERSION.SDK_INT >= 24) {
            OO0o\u3007\u3007.\u3007080(this.mInfo, b);
        }
    }
    
    public void setInputType(final int inputType) {
        this.mInfo.setInputType(inputType);
    }
    
    public void setLabelFor(final View labelFor) {
        this.mInfo.setLabelFor(labelFor);
    }
    
    public void setLabelFor(final View view, final int n) {
        this.mInfo.setLabelFor(view, n);
    }
    
    public void setLabeledBy(final View labeledBy) {
        this.mInfo.setLabeledBy(labeledBy);
    }
    
    public void setLabeledBy(final View view, final int n) {
        this.mInfo.setLabeledBy(view, n);
    }
    
    public void setLiveRegion(final int liveRegion) {
        this.mInfo.setLiveRegion(liveRegion);
    }
    
    public void setLongClickable(final boolean longClickable) {
        this.mInfo.setLongClickable(longClickable);
    }
    
    public void setMaxTextLength(final int maxTextLength) {
        this.mInfo.setMaxTextLength(maxTextLength);
    }
    
    public void setMovementGranularities(final int movementGranularities) {
        this.mInfo.setMovementGranularities(movementGranularities);
    }
    
    public void setMultiLine(final boolean multiLine) {
        this.mInfo.setMultiLine(multiLine);
    }
    
    public void setPackageName(final CharSequence packageName) {
        this.mInfo.setPackageName(packageName);
    }
    
    public void setPaneTitle(@Nullable final CharSequence charSequence) {
        if (Build$VERSION.SDK_INT >= 28) {
            oO80.\u3007080(this.mInfo, charSequence);
        }
        else {
            this.mInfo.getExtras().putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.PANE_TITLE_KEY", charSequence);
        }
    }
    
    public void setParent(final View parent) {
        this.mParentVirtualDescendantId = -1;
        this.mInfo.setParent(parent);
    }
    
    public void setParent(final View view, final int mParentVirtualDescendantId) {
        this.mParentVirtualDescendantId = mParentVirtualDescendantId;
        this.mInfo.setParent(view, mParentVirtualDescendantId);
    }
    
    public void setPassword(final boolean password) {
        this.mInfo.setPassword(password);
    }
    
    public void setRangeInfo(final RangeInfoCompat rangeInfoCompat) {
        this.mInfo.setRangeInfo((AccessibilityNodeInfo$RangeInfo)rangeInfoCompat.mInfo);
    }
    
    public void setRoleDescription(@Nullable final CharSequence charSequence) {
        this.mInfo.getExtras().putCharSequence("AccessibilityNodeInfo.roleDescription", charSequence);
    }
    
    public void setScreenReaderFocusable(final boolean b) {
        if (Build$VERSION.SDK_INT >= 28) {
            oo\u3007.\u3007080(this.mInfo, b);
        }
        else {
            this.setBooleanProperty(1, b);
        }
    }
    
    public void setScrollable(final boolean scrollable) {
        this.mInfo.setScrollable(scrollable);
    }
    
    public void setSelected(final boolean selected) {
        this.mInfo.setSelected(selected);
    }
    
    public void setShowingHintText(final boolean b) {
        if (Build$VERSION.SDK_INT >= 26) {
            \u30070\u3007O0088o.\u3007080(this.mInfo, b);
        }
        else {
            this.setBooleanProperty(4, b);
        }
    }
    
    public void setSource(final View source) {
        this.mVirtualDescendantId = -1;
        this.mInfo.setSource(source);
    }
    
    public void setSource(final View view, final int mVirtualDescendantId) {
        this.mVirtualDescendantId = mVirtualDescendantId;
        this.mInfo.setSource(view, mVirtualDescendantId);
    }
    
    public void setStateDescription(@Nullable final CharSequence charSequence) {
        if (BuildCompat.isAtLeastR()) {
            \u3007\u3007808\u3007.\u3007080(this.mInfo, charSequence);
        }
        else {
            this.mInfo.getExtras().putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.STATE_DESCRIPTION_KEY", charSequence);
        }
    }
    
    public void setText(final CharSequence text) {
        this.mInfo.setText(text);
    }
    
    public void setTextEntryKey(final boolean b) {
        if (Build$VERSION.SDK_INT >= 29) {
            O8ooOoo\u3007.\u3007080(this.mInfo, b);
        }
        else {
            this.setBooleanProperty(8, b);
        }
    }
    
    public void setTextSelectable(final boolean b) {
        if (Build$VERSION.SDK_INT >= 33) {
            Api33Impl.setTextSelectable(this.mInfo, b);
        }
    }
    
    public void setTextSelection(final int n, final int n2) {
        this.mInfo.setTextSelection(n, n2);
    }
    
    public void setTooltipText(@Nullable final CharSequence charSequence) {
        if (Build$VERSION.SDK_INT >= 28) {
            \u3007oOO8O8.\u3007080(this.mInfo, charSequence);
        }
        else {
            this.mInfo.getExtras().putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.TOOLTIP_TEXT_KEY", charSequence);
        }
    }
    
    public void setTouchDelegateInfo(@NonNull final TouchDelegateInfoCompat touchDelegateInfoCompat) {
        if (Build$VERSION.SDK_INT >= 29) {
            \u3007o\u3007.\u3007080(this.mInfo, touchDelegateInfoCompat.mInfo);
        }
    }
    
    public void setTraversalAfter(final View view) {
        if (Build$VERSION.SDK_INT >= 22) {
            Oo08.\u3007080(this.mInfo, view);
        }
    }
    
    public void setTraversalAfter(final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 22) {
            o\u3007\u30070\u3007.\u3007080(this.mInfo, view, n);
        }
    }
    
    public void setTraversalBefore(final View view) {
        if (Build$VERSION.SDK_INT >= 22) {
            \u30078o8o\u3007.\u3007080(this.mInfo, view);
        }
    }
    
    public void setTraversalBefore(final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 22) {
            o800o8O.\u3007080(this.mInfo, view, n);
        }
    }
    
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    public void setUniqueId(@Nullable final String uniqueId) {
        if (BuildCompat.isAtLeastT()) {
            this.mInfo.setUniqueId(uniqueId);
        }
        else {
            ((BaseBundle)this.mInfo.getExtras()).putString("androidx.view.accessibility.AccessibilityNodeInfoCompat.UNIQUE_ID_KEY", uniqueId);
        }
    }
    
    public void setViewIdResourceName(final String viewIdResourceName) {
        this.mInfo.setViewIdResourceName(viewIdResourceName);
    }
    
    public void setVisibleToUser(final boolean visibleToUser) {
        this.mInfo.setVisibleToUser(visibleToUser);
    }
    
    @NonNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        final Rect rect = new Rect();
        this.getBoundsInParent(rect);
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("; boundsInParent: ");
        sb2.append(rect);
        sb.append(sb2.toString());
        this.getBoundsInScreen(rect);
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("; boundsInScreen: ");
        sb3.append(rect);
        sb.append(sb3.toString());
        sb.append("; packageName: ");
        sb.append(this.getPackageName());
        sb.append("; className: ");
        sb.append(this.getClassName());
        sb.append("; text: ");
        sb.append(this.getText());
        sb.append("; contentDescription: ");
        sb.append(this.getContentDescription());
        sb.append("; viewId: ");
        sb.append(this.getViewIdResourceName());
        sb.append("; uniqueId: ");
        sb.append(this.getUniqueId());
        sb.append("; checkable: ");
        sb.append(this.isCheckable());
        sb.append("; checked: ");
        sb.append(this.isChecked());
        sb.append("; focusable: ");
        sb.append(this.isFocusable());
        sb.append("; focused: ");
        sb.append(this.isFocused());
        sb.append("; selected: ");
        sb.append(this.isSelected());
        sb.append("; clickable: ");
        sb.append(this.isClickable());
        sb.append("; longClickable: ");
        sb.append(this.isLongClickable());
        sb.append("; enabled: ");
        sb.append(this.isEnabled());
        sb.append("; password: ");
        sb.append(this.isPassword());
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("; scrollable: ");
        sb4.append(this.isScrollable());
        sb.append(sb4.toString());
        sb.append("; [");
        final List<AccessibilityActionCompat> actionList = this.getActionList();
        for (int i = 0; i < actionList.size(); ++i) {
            final AccessibilityActionCompat accessibilityActionCompat = actionList.get(i);
            String str;
            final String s = str = getActionSymbolicName(accessibilityActionCompat.getId());
            if (s.equals("ACTION_UNKNOWN")) {
                str = s;
                if (accessibilityActionCompat.getLabel() != null) {
                    str = accessibilityActionCompat.getLabel().toString();
                }
            }
            sb.append(str);
            if (i != actionList.size() - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }
    
    public AccessibilityNodeInfo unwrap() {
        return this.mInfo;
    }
    
    public static class AccessibilityActionCompat
    {
        public static final AccessibilityActionCompat ACTION_ACCESSIBILITY_FOCUS;
        public static final AccessibilityActionCompat ACTION_CLEAR_ACCESSIBILITY_FOCUS;
        public static final AccessibilityActionCompat ACTION_CLEAR_FOCUS;
        public static final AccessibilityActionCompat ACTION_CLEAR_SELECTION;
        public static final AccessibilityActionCompat ACTION_CLICK;
        public static final AccessibilityActionCompat ACTION_COLLAPSE;
        public static final AccessibilityActionCompat ACTION_CONTEXT_CLICK;
        public static final AccessibilityActionCompat ACTION_COPY;
        public static final AccessibilityActionCompat ACTION_CUT;
        public static final AccessibilityActionCompat ACTION_DISMISS;
        @NonNull
        public static final AccessibilityActionCompat ACTION_DRAG_CANCEL;
        @NonNull
        public static final AccessibilityActionCompat ACTION_DRAG_DROP;
        @NonNull
        public static final AccessibilityActionCompat ACTION_DRAG_START;
        public static final AccessibilityActionCompat ACTION_EXPAND;
        public static final AccessibilityActionCompat ACTION_FOCUS;
        public static final AccessibilityActionCompat ACTION_HIDE_TOOLTIP;
        @NonNull
        public static final AccessibilityActionCompat ACTION_IME_ENTER;
        public static final AccessibilityActionCompat ACTION_LONG_CLICK;
        public static final AccessibilityActionCompat ACTION_MOVE_WINDOW;
        public static final AccessibilityActionCompat ACTION_NEXT_AT_MOVEMENT_GRANULARITY;
        public static final AccessibilityActionCompat ACTION_NEXT_HTML_ELEMENT;
        @NonNull
        public static final AccessibilityActionCompat ACTION_PAGE_DOWN;
        @NonNull
        public static final AccessibilityActionCompat ACTION_PAGE_LEFT;
        @NonNull
        public static final AccessibilityActionCompat ACTION_PAGE_RIGHT;
        @NonNull
        public static final AccessibilityActionCompat ACTION_PAGE_UP;
        public static final AccessibilityActionCompat ACTION_PASTE;
        @NonNull
        public static final AccessibilityActionCompat ACTION_PRESS_AND_HOLD;
        public static final AccessibilityActionCompat ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY;
        public static final AccessibilityActionCompat ACTION_PREVIOUS_HTML_ELEMENT;
        public static final AccessibilityActionCompat ACTION_SCROLL_BACKWARD;
        public static final AccessibilityActionCompat ACTION_SCROLL_DOWN;
        public static final AccessibilityActionCompat ACTION_SCROLL_FORWARD;
        public static final AccessibilityActionCompat ACTION_SCROLL_LEFT;
        public static final AccessibilityActionCompat ACTION_SCROLL_RIGHT;
        public static final AccessibilityActionCompat ACTION_SCROLL_TO_POSITION;
        public static final AccessibilityActionCompat ACTION_SCROLL_UP;
        public static final AccessibilityActionCompat ACTION_SELECT;
        public static final AccessibilityActionCompat ACTION_SET_PROGRESS;
        public static final AccessibilityActionCompat ACTION_SET_SELECTION;
        public static final AccessibilityActionCompat ACTION_SET_TEXT;
        public static final AccessibilityActionCompat ACTION_SHOW_ON_SCREEN;
        @NonNull
        public static final AccessibilityActionCompat ACTION_SHOW_TEXT_SUGGESTIONS;
        public static final AccessibilityActionCompat ACTION_SHOW_TOOLTIP;
        private static final String TAG = "A11yActionCompat";
        final Object mAction;
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        protected final AccessibilityViewCommand mCommand;
        private final int mId;
        private final Class<? extends AccessibilityViewCommand.CommandArguments> mViewCommandArgumentClass;
        
        static {
            final Object o = null;
            ACTION_FOCUS = new AccessibilityActionCompat(1, null);
            ACTION_CLEAR_FOCUS = new AccessibilityActionCompat(2, null);
            ACTION_SELECT = new AccessibilityActionCompat(4, null);
            ACTION_CLEAR_SELECTION = new AccessibilityActionCompat(8, null);
            ACTION_CLICK = new AccessibilityActionCompat(16, null);
            ACTION_LONG_CLICK = new AccessibilityActionCompat(32, null);
            ACTION_ACCESSIBILITY_FOCUS = new AccessibilityActionCompat(64, null);
            ACTION_CLEAR_ACCESSIBILITY_FOCUS = new AccessibilityActionCompat(128, null);
            ACTION_NEXT_AT_MOVEMENT_GRANULARITY = new AccessibilityActionCompat(256, null, AccessibilityViewCommand.MoveAtGranularityArguments.class);
            ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY = new AccessibilityActionCompat(512, null, AccessibilityViewCommand.MoveAtGranularityArguments.class);
            ACTION_NEXT_HTML_ELEMENT = new AccessibilityActionCompat(1024, null, AccessibilityViewCommand.MoveHtmlArguments.class);
            ACTION_PREVIOUS_HTML_ELEMENT = new AccessibilityActionCompat(2048, null, AccessibilityViewCommand.MoveHtmlArguments.class);
            ACTION_SCROLL_FORWARD = new AccessibilityActionCompat(4096, null);
            ACTION_SCROLL_BACKWARD = new AccessibilityActionCompat(8192, null);
            ACTION_COPY = new AccessibilityActionCompat(16384, null);
            ACTION_PASTE = new AccessibilityActionCompat(32768, null);
            ACTION_CUT = new AccessibilityActionCompat(65536, null);
            ACTION_SET_SELECTION = new AccessibilityActionCompat(131072, null, AccessibilityViewCommand.SetSelectionArguments.class);
            ACTION_EXPAND = new AccessibilityActionCompat(262144, null);
            ACTION_COLLAPSE = new AccessibilityActionCompat(524288, null);
            ACTION_DISMISS = new AccessibilityActionCompat(1048576, null);
            ACTION_SET_TEXT = new AccessibilityActionCompat(2097152, null, AccessibilityViewCommand.SetTextArguments.class);
            final int sdk_INT = Build$VERSION.SDK_INT;
            AccessibilityNodeInfo$AccessibilityAction \u3007080;
            if (sdk_INT >= 23) {
                \u3007080 = O8\u3007o.\u3007080();
            }
            else {
                \u3007080 = null;
            }
            ACTION_SHOW_ON_SCREEN = new AccessibilityActionCompat(\u3007080, 16908342, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction \u300781;
            if (sdk_INT >= 23) {
                \u300781 = o8oO\u3007.\u3007080();
            }
            else {
                \u300781 = null;
            }
            ACTION_SCROLL_TO_POSITION = new AccessibilityActionCompat(\u300781, 16908343, null, null, AccessibilityViewCommand.ScrollToPositionArguments.class);
            AccessibilityNodeInfo$AccessibilityAction \u300782;
            if (sdk_INT >= 23) {
                \u300782 = o\u30078oOO88.\u3007080();
            }
            else {
                \u300782 = null;
            }
            ACTION_SCROLL_UP = new AccessibilityActionCompat(\u300782, 16908344, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction \u300783;
            if (sdk_INT >= 23) {
                \u300783 = \u300700\u30078.\u3007080();
            }
            else {
                \u300783 = null;
            }
            ACTION_SCROLL_LEFT = new AccessibilityActionCompat(\u300783, 16908345, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction \u300784;
            if (sdk_INT >= 23) {
                \u300784 = \u3007o.\u3007080();
            }
            else {
                \u300784 = null;
            }
            ACTION_SCROLL_DOWN = new AccessibilityActionCompat(\u300784, 16908346, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction \u300785;
            if (sdk_INT >= 23) {
                \u300785 = o0ooO.\u3007080();
            }
            else {
                \u300785 = null;
            }
            ACTION_SCROLL_RIGHT = new AccessibilityActionCompat(\u300785, 16908347, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction \u300786;
            if (sdk_INT >= 29) {
                \u300786 = o\u30078.\u3007080();
            }
            else {
                \u300786 = null;
            }
            ACTION_PAGE_UP = new AccessibilityActionCompat(\u300786, 16908358, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction \u300787;
            if (sdk_INT >= 29) {
                \u300787 = Oo8Oo00oo.\u3007080();
            }
            else {
                \u300787 = null;
            }
            ACTION_PAGE_DOWN = new AccessibilityActionCompat(\u300787, 16908359, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction \u300788;
            if (sdk_INT >= 29) {
                \u300788 = \u3007\u3007\u30070\u3007\u30070.\u3007080();
            }
            else {
                \u300788 = null;
            }
            ACTION_PAGE_LEFT = new AccessibilityActionCompat(\u300788, 16908360, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction \u300789;
            if (sdk_INT >= 29) {
                \u300789 = o\u30070OOo\u30070.\u3007080();
            }
            else {
                \u300789 = null;
            }
            ACTION_PAGE_RIGHT = new AccessibilityActionCompat(\u300789, 16908361, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction \u300790;
            if (sdk_INT >= 23) {
                \u300790 = \u3007\u30070o.\u3007080();
            }
            else {
                \u300790 = null;
            }
            ACTION_CONTEXT_CLICK = new AccessibilityActionCompat(\u300790, 16908348, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction \u300791;
            if (sdk_INT >= 24) {
                \u300791 = \u300708O8o\u30070.\u3007080();
            }
            else {
                \u300791 = null;
            }
            ACTION_SET_PROGRESS = new AccessibilityActionCompat(\u300791, 16908349, null, null, AccessibilityViewCommand.SetProgressArguments.class);
            AccessibilityNodeInfo$AccessibilityAction \u300792;
            if (sdk_INT >= 26) {
                \u300792 = oO.\u3007080();
            }
            else {
                \u300792 = null;
            }
            ACTION_MOVE_WINDOW = new AccessibilityActionCompat(\u300792, 16908354, null, null, AccessibilityViewCommand.MoveWindowArguments.class);
            AccessibilityNodeInfo$AccessibilityAction \u300793;
            if (sdk_INT >= 28) {
                \u300793 = \u30078.\u3007080();
            }
            else {
                \u300793 = null;
            }
            ACTION_SHOW_TOOLTIP = new AccessibilityActionCompat(\u300793, 16908356, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction \u300794;
            if (sdk_INT >= 28) {
                \u300794 = O08000.\u3007080();
            }
            else {
                \u300794 = null;
            }
            ACTION_HIDE_TOOLTIP = new AccessibilityActionCompat(\u300794, 16908357, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction \u300795;
            if (sdk_INT >= 30) {
                \u300795 = \u30078\u30070\u3007o\u3007O.\u3007080();
            }
            else {
                \u300795 = null;
            }
            ACTION_PRESS_AND_HOLD = new AccessibilityActionCompat(\u300795, 16908362, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction \u300796;
            if (sdk_INT >= 30) {
                \u300796 = O\u3007O\u3007oO.\u3007080();
            }
            else {
                \u300796 = null;
            }
            ACTION_IME_ENTER = new AccessibilityActionCompat(\u300796, 16908372, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction action_DRAG_START;
            if (sdk_INT >= 32) {
                action_DRAG_START = AccessibilityNodeInfo$AccessibilityAction.ACTION_DRAG_START;
            }
            else {
                action_DRAG_START = null;
            }
            ACTION_DRAG_START = new AccessibilityActionCompat(action_DRAG_START, 16908373, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction action_DRAG_DROP;
            if (sdk_INT >= 32) {
                action_DRAG_DROP = AccessibilityNodeInfo$AccessibilityAction.ACTION_DRAG_DROP;
            }
            else {
                action_DRAG_DROP = null;
            }
            ACTION_DRAG_DROP = new AccessibilityActionCompat(action_DRAG_DROP, 16908374, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction action_DRAG_CANCEL;
            if (sdk_INT >= 32) {
                action_DRAG_CANCEL = AccessibilityNodeInfo$AccessibilityAction.ACTION_DRAG_CANCEL;
            }
            else {
                action_DRAG_CANCEL = null;
            }
            ACTION_DRAG_CANCEL = new AccessibilityActionCompat(action_DRAG_CANCEL, 16908375, null, null, null);
            Object action_SHOW_TEXT_SUGGESTIONS = o;
            if (sdk_INT >= 33) {
                action_SHOW_TEXT_SUGGESTIONS = AccessibilityNodeInfo$AccessibilityAction.ACTION_SHOW_TEXT_SUGGESTIONS;
            }
            ACTION_SHOW_TEXT_SUGGESTIONS = new AccessibilityActionCompat(action_SHOW_TEXT_SUGGESTIONS, 16908376, null, null, null);
        }
        
        public AccessibilityActionCompat(final int n, final CharSequence charSequence) {
            this(null, n, charSequence, null, null);
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public AccessibilityActionCompat(final int n, final CharSequence charSequence, final AccessibilityViewCommand accessibilityViewCommand) {
            this(null, n, charSequence, accessibilityViewCommand, null);
        }
        
        private AccessibilityActionCompat(final int n, final CharSequence charSequence, final Class<? extends AccessibilityViewCommand.CommandArguments> clazz) {
            this(null, n, charSequence, null, clazz);
        }
        
        AccessibilityActionCompat(final Object o) {
            this(o, 0, null, null, null);
        }
        
        AccessibilityActionCompat(final Object mAction, final int mId, final CharSequence charSequence, final AccessibilityViewCommand mCommand, final Class<? extends AccessibilityViewCommand.CommandArguments> mViewCommandArgumentClass) {
            this.mId = mId;
            this.mCommand = mCommand;
            if (mAction == null) {
                this.mAction = new AccessibilityNodeInfo$AccessibilityAction(mId, charSequence);
            }
            else {
                this.mAction = mAction;
            }
            this.mViewCommandArgumentClass = mViewCommandArgumentClass;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public AccessibilityActionCompat createReplacementAction(final CharSequence charSequence, final AccessibilityViewCommand accessibilityViewCommand) {
            return new AccessibilityActionCompat(null, this.mId, charSequence, accessibilityViewCommand, this.mViewCommandArgumentClass);
        }
        
        @Override
        public boolean equals(@Nullable Object mAction) {
            if (mAction == null) {
                return false;
            }
            if (!(mAction instanceof AccessibilityActionCompat)) {
                return false;
            }
            final AccessibilityActionCompat accessibilityActionCompat = (AccessibilityActionCompat)mAction;
            mAction = this.mAction;
            if (mAction == null) {
                if (accessibilityActionCompat.mAction != null) {
                    return false;
                }
            }
            else if (!mAction.equals(accessibilityActionCompat.mAction)) {
                return false;
            }
            return true;
        }
        
        public int getId() {
            return ((AccessibilityNodeInfo$AccessibilityAction)this.mAction).getId();
        }
        
        public CharSequence getLabel() {
            return ((AccessibilityNodeInfo$AccessibilityAction)this.mAction).getLabel();
        }
        
        @Override
        public int hashCode() {
            final Object mAction = this.mAction;
            int hashCode;
            if (mAction != null) {
                hashCode = mAction.hashCode();
            }
            else {
                hashCode = 0;
            }
            return hashCode;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
        public boolean perform(final View view, final Bundle bundle) {
            if (this.mCommand != null) {
                final Class<? extends AccessibilityViewCommand.CommandArguments> mViewCommandArgumentClass = this.mViewCommandArgumentClass;
                Object o = null;
                final AccessibilityViewCommand.CommandArguments commandArguments = null;
                if (mViewCommandArgumentClass != null) {
                    AccessibilityViewCommand.CommandArguments commandArguments2;
                    try {
                        o = (AccessibilityViewCommand.CommandArguments)mViewCommandArgumentClass.getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
                        try {
                            ((AccessibilityViewCommand.CommandArguments)o).setBundle(bundle);
                        }
                        catch (final Exception ex) {
                            commandArguments2 = (AccessibilityViewCommand.CommandArguments)o;
                        }
                    }
                    catch (final Exception ex2) {
                        commandArguments2 = commandArguments;
                    }
                    final Class<? extends AccessibilityViewCommand.CommandArguments> mViewCommandArgumentClass2 = this.mViewCommandArgumentClass;
                    String name;
                    if (mViewCommandArgumentClass2 == null) {
                        name = "null";
                    }
                    else {
                        name = mViewCommandArgumentClass2.getName();
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to execute command with argument class ViewCommandArgument: ");
                    sb.append(name);
                    o = commandArguments2;
                }
                return this.mCommand.perform(view, (AccessibilityViewCommand.CommandArguments)o);
            }
            return false;
        }
    }
    
    @RequiresApi(33)
    private static class Api33Impl
    {
        @DoNotInline
        public static AccessibilityNodeInfo$ExtraRenderingInfo getExtraRenderingInfo(final AccessibilityNodeInfo accessibilityNodeInfo) {
            return o\u3007O.\u3007080(accessibilityNodeInfo);
        }
        
        @DoNotInline
        public static boolean isTextSelectable(final AccessibilityNodeInfo accessibilityNodeInfo) {
            return accessibilityNodeInfo.isTextSelectable();
        }
        
        @DoNotInline
        public static void setTextSelectable(final AccessibilityNodeInfo accessibilityNodeInfo, final boolean textSelectable) {
            accessibilityNodeInfo.setTextSelectable(textSelectable);
        }
    }
    
    public static class CollectionInfoCompat
    {
        public static final int SELECTION_MODE_MULTIPLE = 2;
        public static final int SELECTION_MODE_NONE = 0;
        public static final int SELECTION_MODE_SINGLE = 1;
        final Object mInfo;
        
        CollectionInfoCompat(final Object mInfo) {
            this.mInfo = mInfo;
        }
        
        public static CollectionInfoCompat obtain(final int n, final int n2, final boolean b) {
            return new CollectionInfoCompat(AccessibilityNodeInfo$CollectionInfo.obtain(n, n2, b));
        }
        
        public static CollectionInfoCompat obtain(final int n, final int n2, final boolean b, final int n3) {
            return new CollectionInfoCompat(AccessibilityNodeInfo$CollectionInfo.obtain(n, n2, b, n3));
        }
        
        public int getColumnCount() {
            return ((AccessibilityNodeInfo$CollectionInfo)this.mInfo).getColumnCount();
        }
        
        public int getRowCount() {
            return ((AccessibilityNodeInfo$CollectionInfo)this.mInfo).getRowCount();
        }
        
        public int getSelectionMode() {
            return ((AccessibilityNodeInfo$CollectionInfo)this.mInfo).getSelectionMode();
        }
        
        public boolean isHierarchical() {
            return ((AccessibilityNodeInfo$CollectionInfo)this.mInfo).isHierarchical();
        }
    }
    
    public static class CollectionItemInfoCompat
    {
        final Object mInfo;
        
        CollectionItemInfoCompat(final Object mInfo) {
            this.mInfo = mInfo;
        }
        
        public static CollectionItemInfoCompat obtain(final int n, final int n2, final int n3, final int n4, final boolean b) {
            return new CollectionItemInfoCompat(AccessibilityNodeInfo$CollectionItemInfo.obtain(n, n2, n3, n4, b));
        }
        
        public static CollectionItemInfoCompat obtain(final int n, final int n2, final int n3, final int n4, final boolean b, final boolean b2) {
            return new CollectionItemInfoCompat(AccessibilityNodeInfo$CollectionItemInfo.obtain(n, n2, n3, n4, b, b2));
        }
        
        public int getColumnIndex() {
            return ((AccessibilityNodeInfo$CollectionItemInfo)this.mInfo).getColumnIndex();
        }
        
        public int getColumnSpan() {
            return ((AccessibilityNodeInfo$CollectionItemInfo)this.mInfo).getColumnSpan();
        }
        
        public int getRowIndex() {
            return ((AccessibilityNodeInfo$CollectionItemInfo)this.mInfo).getRowIndex();
        }
        
        public int getRowSpan() {
            return ((AccessibilityNodeInfo$CollectionItemInfo)this.mInfo).getRowSpan();
        }
        
        @Deprecated
        public boolean isHeading() {
            return ((AccessibilityNodeInfo$CollectionItemInfo)this.mInfo).isHeading();
        }
        
        public boolean isSelected() {
            return ((AccessibilityNodeInfo$CollectionItemInfo)this.mInfo).isSelected();
        }
    }
    
    public static class RangeInfoCompat
    {
        public static final int RANGE_TYPE_FLOAT = 1;
        public static final int RANGE_TYPE_INT = 0;
        public static final int RANGE_TYPE_PERCENT = 2;
        final Object mInfo;
        
        RangeInfoCompat(final Object mInfo) {
            this.mInfo = mInfo;
        }
        
        public static RangeInfoCompat obtain(final int n, final float n2, final float n3, final float n4) {
            return new RangeInfoCompat(AccessibilityNodeInfo$RangeInfo.obtain(n, n2, n3, n4));
        }
        
        public float getCurrent() {
            return ((AccessibilityNodeInfo$RangeInfo)this.mInfo).getCurrent();
        }
        
        public float getMax() {
            return ((AccessibilityNodeInfo$RangeInfo)this.mInfo).getMax();
        }
        
        public float getMin() {
            return ((AccessibilityNodeInfo$RangeInfo)this.mInfo).getMin();
        }
        
        public int getType() {
            return ((AccessibilityNodeInfo$RangeInfo)this.mInfo).getType();
        }
    }
    
    public static final class TouchDelegateInfoCompat
    {
        final AccessibilityNodeInfo$TouchDelegateInfo mInfo;
        
        TouchDelegateInfoCompat(@NonNull final AccessibilityNodeInfo$TouchDelegateInfo mInfo) {
            this.mInfo = mInfo;
        }
        
        public TouchDelegateInfoCompat(@NonNull final Map<Region, View> map) {
            if (Build$VERSION.SDK_INT >= 29) {
                this.mInfo = new AccessibilityNodeInfo$TouchDelegateInfo((Map)map);
            }
            else {
                this.mInfo = null;
            }
        }
        
        @Nullable
        public Region getRegionAt(@IntRange(from = 0L) final int n) {
            if (Build$VERSION.SDK_INT >= 29) {
                return O000.\u3007080(this.mInfo, n);
            }
            return null;
        }
        
        @IntRange(from = 0L)
        public int getRegionCount() {
            if (Build$VERSION.SDK_INT >= 29) {
                return \u300780.\u3007080(this.mInfo);
            }
            return 0;
        }
        
        @Nullable
        public AccessibilityNodeInfoCompat getTargetForRegion(@NonNull final Region region) {
            if (Build$VERSION.SDK_INT >= 29) {
                final AccessibilityNodeInfo \u3007080 = oO00OOO.\u3007080(this.mInfo, region);
                if (\u3007080 != null) {
                    return AccessibilityNodeInfoCompat.wrap(\u3007080);
                }
            }
            return null;
        }
    }
}

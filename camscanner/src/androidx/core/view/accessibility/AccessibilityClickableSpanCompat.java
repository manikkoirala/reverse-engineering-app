// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view.accessibility;

import android.os.BaseBundle;
import android.os.Bundle;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;
import android.text.style.ClickableSpan;

public final class AccessibilityClickableSpanCompat extends ClickableSpan
{
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static final String SPAN_ID = "ACCESSIBILITY_CLICKABLE_SPAN_ID";
    private final int mClickableSpanActionId;
    private final AccessibilityNodeInfoCompat mNodeInfoCompat;
    private final int mOriginalClickableSpanId;
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public AccessibilityClickableSpanCompat(final int mOriginalClickableSpanId, @NonNull final AccessibilityNodeInfoCompat mNodeInfoCompat, final int mClickableSpanActionId) {
        this.mOriginalClickableSpanId = mOriginalClickableSpanId;
        this.mNodeInfoCompat = mNodeInfoCompat;
        this.mClickableSpanActionId = mClickableSpanActionId;
    }
    
    public void onClick(@NonNull final View view) {
        final Bundle bundle = new Bundle();
        ((BaseBundle)bundle).putInt("ACCESSIBILITY_CLICKABLE_SPAN_ID", this.mOriginalClickableSpanId);
        this.mNodeInfoCompat.performAction(this.mClickableSpanActionId, bundle);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view.accessibility;

import androidx.annotation.RestrictTo;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import androidx.annotation.NonNull;
import android.view.accessibility.AccessibilityRecord;
import android.view.accessibility.AccessibilityEvent;

public final class AccessibilityEventCompat
{
    public static final int CONTENT_CHANGE_TYPE_CONTENT_DESCRIPTION = 4;
    public static final int CONTENT_CHANGE_TYPE_DRAG_CANCELLED = 512;
    public static final int CONTENT_CHANGE_TYPE_DRAG_DROPPED = 256;
    public static final int CONTENT_CHANGE_TYPE_DRAG_STARTED = 128;
    public static final int CONTENT_CHANGE_TYPE_PANE_APPEARED = 16;
    public static final int CONTENT_CHANGE_TYPE_PANE_DISAPPEARED = 32;
    public static final int CONTENT_CHANGE_TYPE_PANE_TITLE = 8;
    public static final int CONTENT_CHANGE_TYPE_STATE_DESCRIPTION = 64;
    public static final int CONTENT_CHANGE_TYPE_SUBTREE = 1;
    public static final int CONTENT_CHANGE_TYPE_TEXT = 2;
    public static final int CONTENT_CHANGE_TYPE_UNDEFINED = 0;
    public static final int TYPES_ALL_MASK = -1;
    public static final int TYPE_ANNOUNCEMENT = 16384;
    public static final int TYPE_ASSIST_READING_CONTEXT = 16777216;
    public static final int TYPE_GESTURE_DETECTION_END = 524288;
    public static final int TYPE_GESTURE_DETECTION_START = 262144;
    @Deprecated
    public static final int TYPE_TOUCH_EXPLORATION_GESTURE_END = 1024;
    @Deprecated
    public static final int TYPE_TOUCH_EXPLORATION_GESTURE_START = 512;
    public static final int TYPE_TOUCH_INTERACTION_END = 2097152;
    public static final int TYPE_TOUCH_INTERACTION_START = 1048576;
    public static final int TYPE_VIEW_ACCESSIBILITY_FOCUSED = 32768;
    public static final int TYPE_VIEW_ACCESSIBILITY_FOCUS_CLEARED = 65536;
    public static final int TYPE_VIEW_CONTEXT_CLICKED = 8388608;
    @Deprecated
    public static final int TYPE_VIEW_HOVER_ENTER = 128;
    @Deprecated
    public static final int TYPE_VIEW_HOVER_EXIT = 256;
    @Deprecated
    public static final int TYPE_VIEW_SCROLLED = 4096;
    @Deprecated
    public static final int TYPE_VIEW_TEXT_SELECTION_CHANGED = 8192;
    public static final int TYPE_VIEW_TEXT_TRAVERSED_AT_MOVEMENT_GRANULARITY = 131072;
    public static final int TYPE_WINDOWS_CHANGED = 4194304;
    @Deprecated
    public static final int TYPE_WINDOW_CONTENT_CHANGED = 2048;
    
    private AccessibilityEventCompat() {
    }
    
    @Deprecated
    public static void appendRecord(final AccessibilityEvent accessibilityEvent, final AccessibilityRecordCompat accessibilityRecordCompat) {
        accessibilityEvent.appendRecord((AccessibilityRecord)accessibilityRecordCompat.getImpl());
    }
    
    @Deprecated
    public static AccessibilityRecordCompat asRecord(final AccessibilityEvent accessibilityEvent) {
        return new AccessibilityRecordCompat(accessibilityEvent);
    }
    
    public static int getAction(@NonNull final AccessibilityEvent accessibilityEvent) {
        return Api16Impl.getAction(accessibilityEvent);
    }
    
    public static int getContentChangeTypes(@NonNull final AccessibilityEvent accessibilityEvent) {
        return Api19Impl.getContentChangeTypes(accessibilityEvent);
    }
    
    public static int getMovementGranularity(@NonNull final AccessibilityEvent accessibilityEvent) {
        return Api16Impl.getMovementGranularity(accessibilityEvent);
    }
    
    @Deprecated
    public static AccessibilityRecordCompat getRecord(final AccessibilityEvent accessibilityEvent, final int n) {
        return new AccessibilityRecordCompat(accessibilityEvent.getRecord(n));
    }
    
    @Deprecated
    public static int getRecordCount(final AccessibilityEvent accessibilityEvent) {
        return accessibilityEvent.getRecordCount();
    }
    
    public static void setAction(@NonNull final AccessibilityEvent accessibilityEvent, final int n) {
        Api16Impl.setAction(accessibilityEvent, n);
    }
    
    public static void setContentChangeTypes(@NonNull final AccessibilityEvent accessibilityEvent, final int n) {
        Api19Impl.setContentChangeTypes(accessibilityEvent, n);
    }
    
    public static void setMovementGranularity(@NonNull final AccessibilityEvent accessibilityEvent, final int n) {
        Api16Impl.setMovementGranularity(accessibilityEvent, n);
    }
    
    @RequiresApi(16)
    static class Api16Impl
    {
        private Api16Impl() {
        }
        
        @DoNotInline
        static int getAction(final AccessibilityEvent accessibilityEvent) {
            return accessibilityEvent.getAction();
        }
        
        @DoNotInline
        static int getMovementGranularity(final AccessibilityEvent accessibilityEvent) {
            return accessibilityEvent.getMovementGranularity();
        }
        
        @DoNotInline
        static void setAction(final AccessibilityEvent accessibilityEvent, final int action) {
            accessibilityEvent.setAction(action);
        }
        
        @DoNotInline
        static void setMovementGranularity(final AccessibilityEvent accessibilityEvent, final int movementGranularity) {
            accessibilityEvent.setMovementGranularity(movementGranularity);
        }
    }
    
    @RequiresApi(19)
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        @DoNotInline
        static int getContentChangeTypes(final AccessibilityEvent accessibilityEvent) {
            return accessibilityEvent.getContentChangeTypes();
        }
        
        @DoNotInline
        static void setContentChangeTypes(final AccessibilityEvent accessibilityEvent, final int contentChangeTypes) {
            accessibilityEvent.setContentChangeTypes(contentChangeTypes);
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public @interface ContentChangeType {
    }
}

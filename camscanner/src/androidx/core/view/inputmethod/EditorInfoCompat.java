// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view.inputmethod;

import android.os.BaseBundle;
import androidx.annotation.RequiresApi;
import android.text.SpannableStringBuilder;
import androidx.core.util.Preconditions;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.os.Bundle;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.view.inputmethod.EditorInfo;
import androidx.annotation.VisibleForTesting;
import android.annotation.SuppressLint;

@SuppressLint({ "PrivateConstructorForUtilityClass" })
public final class EditorInfoCompat
{
    private static final String CONTENT_MIME_TYPES_INTEROP_KEY = "android.support.v13.view.inputmethod.EditorInfoCompat.CONTENT_MIME_TYPES";
    private static final String CONTENT_MIME_TYPES_KEY = "androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_MIME_TYPES";
    private static final String CONTENT_SELECTION_END_KEY = "androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SELECTION_END";
    private static final String CONTENT_SELECTION_HEAD_KEY = "androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SELECTION_HEAD";
    private static final String CONTENT_SURROUNDING_TEXT_KEY = "androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SURROUNDING_TEXT";
    private static final String[] EMPTY_STRING_ARRAY;
    public static final int IME_FLAG_FORCE_ASCII = Integer.MIN_VALUE;
    public static final int IME_FLAG_NO_PERSONALIZED_LEARNING = 16777216;
    @VisibleForTesting
    static final int MAX_INITIAL_SELECTION_LENGTH = 1024;
    @VisibleForTesting
    static final int MEMORY_EFFICIENT_TEXT_LENGTH = 2048;
    
    static {
        EMPTY_STRING_ARRAY = new String[0];
    }
    
    @Deprecated
    public EditorInfoCompat() {
    }
    
    @NonNull
    public static String[] getContentMimeTypes(@NonNull final EditorInfo editorInfo) {
        if (Build$VERSION.SDK_INT >= 25) {
            String[] array = \u3007o00\u3007\u3007Oo.\u3007080(editorInfo);
            if (array == null) {
                array = EditorInfoCompat.EMPTY_STRING_ARRAY;
            }
            return array;
        }
        final Bundle extras = editorInfo.extras;
        if (extras == null) {
            return EditorInfoCompat.EMPTY_STRING_ARRAY;
        }
        String[] array2;
        if ((array2 = ((BaseBundle)extras).getStringArray("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_MIME_TYPES")) == null) {
            array2 = ((BaseBundle)editorInfo.extras).getStringArray("android.support.v13.view.inputmethod.EditorInfoCompat.CONTENT_MIME_TYPES");
        }
        if (array2 == null) {
            array2 = EditorInfoCompat.EMPTY_STRING_ARRAY;
        }
        return array2;
    }
    
    @Nullable
    public static CharSequence getInitialSelectedText(@NonNull final EditorInfo editorInfo, final int n) {
        if (Build$VERSION.SDK_INT >= 30) {
            return Api30Impl.getInitialSelectedText(editorInfo, n);
        }
        if (editorInfo.extras == null) {
            return null;
        }
        final int min = Math.min(editorInfo.initialSelStart, editorInfo.initialSelEnd);
        final int max = Math.max(editorInfo.initialSelStart, editorInfo.initialSelEnd);
        final int int1 = ((BaseBundle)editorInfo.extras).getInt("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SELECTION_HEAD");
        final int int2 = ((BaseBundle)editorInfo.extras).getInt("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SELECTION_END");
        if (editorInfo.initialSelStart < 0 || editorInfo.initialSelEnd < 0 || int2 - int1 != max - min) {
            return null;
        }
        final CharSequence charSequence = editorInfo.extras.getCharSequence("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SURROUNDING_TEXT");
        if (charSequence == null) {
            return null;
        }
        CharSequence charSequence2;
        if ((n & 0x1) != 0x0) {
            charSequence2 = charSequence.subSequence(int1, int2);
        }
        else {
            charSequence2 = TextUtils.substring(charSequence, int1, int2);
        }
        return charSequence2;
    }
    
    @Nullable
    public static CharSequence getInitialTextAfterCursor(@NonNull final EditorInfo editorInfo, int min, final int n) {
        if (Build$VERSION.SDK_INT >= 30) {
            return Api30Impl.getInitialTextAfterCursor(editorInfo, min, n);
        }
        final Bundle extras = editorInfo.extras;
        if (extras == null) {
            return null;
        }
        final CharSequence charSequence = extras.getCharSequence("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SURROUNDING_TEXT");
        if (charSequence == null) {
            return null;
        }
        final int int1 = ((BaseBundle)editorInfo.extras).getInt("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SELECTION_END");
        min = Math.min(min, charSequence.length() - int1);
        CharSequence charSequence2;
        if ((n & 0x1) != 0x0) {
            charSequence2 = charSequence.subSequence(int1, min + int1);
        }
        else {
            charSequence2 = TextUtils.substring(charSequence, int1, min + int1);
        }
        return charSequence2;
    }
    
    @Nullable
    public static CharSequence getInitialTextBeforeCursor(@NonNull final EditorInfo editorInfo, int min, final int n) {
        if (Build$VERSION.SDK_INT >= 30) {
            return Api30Impl.getInitialTextBeforeCursor(editorInfo, min, n);
        }
        final Bundle extras = editorInfo.extras;
        if (extras == null) {
            return null;
        }
        final CharSequence charSequence = extras.getCharSequence("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SURROUNDING_TEXT");
        if (charSequence == null) {
            return null;
        }
        final int int1 = ((BaseBundle)editorInfo.extras).getInt("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SELECTION_HEAD");
        min = Math.min(min, int1);
        CharSequence charSequence2;
        if ((n & 0x1) != 0x0) {
            charSequence2 = charSequence.subSequence(int1 - min, int1);
        }
        else {
            charSequence2 = TextUtils.substring(charSequence, int1 - min, int1);
        }
        return charSequence2;
    }
    
    static int getProtocol(final EditorInfo editorInfo) {
        if (Build$VERSION.SDK_INT >= 25) {
            return 1;
        }
        final Bundle extras = editorInfo.extras;
        if (extras == null) {
            return 0;
        }
        final boolean containsKey = ((BaseBundle)extras).containsKey("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_MIME_TYPES");
        final boolean containsKey2 = ((BaseBundle)editorInfo.extras).containsKey("android.support.v13.view.inputmethod.EditorInfoCompat.CONTENT_MIME_TYPES");
        if (containsKey && containsKey2) {
            return 4;
        }
        if (containsKey) {
            return 3;
        }
        if (containsKey2) {
            return 2;
        }
        return 0;
    }
    
    private static boolean isCutOnSurrogate(final CharSequence charSequence, final int n, final int n2) {
        if (n2 != 0) {
            return n2 == 1 && Character.isHighSurrogate(charSequence.charAt(n));
        }
        return Character.isLowSurrogate(charSequence.charAt(n));
    }
    
    private static boolean isPasswordInputType(int n) {
        n &= 0xFFF;
        return n == 129 || n == 225 || n == 18;
    }
    
    public static void setContentMimeTypes(@NonNull final EditorInfo editorInfo, @Nullable final String[] array) {
        if (Build$VERSION.SDK_INT >= 25) {
            \u3007080.\u3007080(editorInfo, array);
        }
        else {
            if (editorInfo.extras == null) {
                editorInfo.extras = new Bundle();
            }
            ((BaseBundle)editorInfo.extras).putStringArray("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_MIME_TYPES", array);
            ((BaseBundle)editorInfo.extras).putStringArray("android.support.v13.view.inputmethod.EditorInfoCompat.CONTENT_MIME_TYPES", array);
        }
    }
    
    public static void setInitialSurroundingSubText(@NonNull final EditorInfo editorInfo, @NonNull final CharSequence charSequence, final int n) {
        Preconditions.checkNotNull(charSequence);
        if (Build$VERSION.SDK_INT >= 30) {
            Api30Impl.setInitialSurroundingSubText(editorInfo, charSequence, n);
            return;
        }
        final int initialSelStart = editorInfo.initialSelStart;
        final int initialSelEnd = editorInfo.initialSelEnd;
        int n2;
        if (initialSelStart > initialSelEnd) {
            n2 = initialSelEnd - n;
        }
        else {
            n2 = initialSelStart - n;
        }
        int n3;
        if (initialSelStart > initialSelEnd) {
            n3 = initialSelStart - n;
        }
        else {
            n3 = initialSelEnd - n;
        }
        final int length = charSequence.length();
        if (n < 0 || n2 < 0 || n3 > length) {
            setSurroundingText(editorInfo, null, 0, 0);
            return;
        }
        if (isPasswordInputType(editorInfo.inputType)) {
            setSurroundingText(editorInfo, null, 0, 0);
            return;
        }
        if (length <= 2048) {
            setSurroundingText(editorInfo, charSequence, n2, n3);
            return;
        }
        trimLongSurroundingText(editorInfo, charSequence, n2, n3);
    }
    
    public static void setInitialSurroundingText(@NonNull final EditorInfo editorInfo, @NonNull final CharSequence charSequence) {
        if (Build$VERSION.SDK_INT >= 30) {
            Api30Impl.setInitialSurroundingSubText(editorInfo, charSequence, 0);
        }
        else {
            setInitialSurroundingSubText(editorInfo, charSequence, 0);
        }
    }
    
    private static void setSurroundingText(final EditorInfo editorInfo, final CharSequence charSequence, final int n, final int n2) {
        if (editorInfo.extras == null) {
            editorInfo.extras = new Bundle();
        }
        Object o;
        if (charSequence != null) {
            o = new SpannableStringBuilder(charSequence);
        }
        else {
            o = null;
        }
        editorInfo.extras.putCharSequence("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SURROUNDING_TEXT", (CharSequence)o);
        ((BaseBundle)editorInfo.extras).putInt("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SELECTION_HEAD", n);
        ((BaseBundle)editorInfo.extras).putInt("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SELECTION_END", n2);
    }
    
    private static void trimLongSurroundingText(final EditorInfo editorInfo, CharSequence charSequence, int n, final int n2) {
        final int n3 = n2 - n;
        int n4;
        if (n3 > 1024) {
            n4 = 0;
        }
        else {
            n4 = n3;
        }
        final int length = charSequence.length();
        final int n5 = 2048 - n4;
        final int min = Math.min(length - n2, n5 - Math.min(n, (int)(n5 * 0.8)));
        final int min2 = Math.min(n, n5 - min);
        final int n6 = n - min2;
        int n7 = min2;
        n = n6;
        if (isCutOnSurrogate(charSequence, n6, 0)) {
            n = n6 + 1;
            n7 = min2 - 1;
        }
        int n8 = min;
        if (isCutOnSurrogate(charSequence, n2 + min - 1, 1)) {
            n8 = min - 1;
        }
        if (n4 != n3) {
            charSequence = TextUtils.concat(new CharSequence[] { charSequence.subSequence(n, n + n7), charSequence.subSequence(n2, n8 + n2) });
        }
        else {
            charSequence = charSequence.subSequence(n, n7 + n4 + n8 + n);
        }
        n = n7 + 0;
        setSurroundingText(editorInfo, charSequence, n, n4 + n);
    }
    
    @RequiresApi(30)
    private static class Api30Impl
    {
        static CharSequence getInitialSelectedText(@NonNull final EditorInfo editorInfo, final int n) {
            return o\u30070.\u3007080(editorInfo, n);
        }
        
        static CharSequence getInitialTextAfterCursor(@NonNull final EditorInfo editorInfo, final int n, final int n2) {
            return \u3007o\u3007.\u3007080(editorInfo, n, n2);
        }
        
        static CharSequence getInitialTextBeforeCursor(@NonNull final EditorInfo editorInfo, final int n, final int n2) {
            return Oo08.\u3007080(editorInfo, n, n2);
        }
        
        static void setInitialSurroundingSubText(@NonNull final EditorInfo editorInfo, final CharSequence charSequence, final int n) {
            O8.\u3007080(editorInfo, charSequence, n);
        }
    }
}

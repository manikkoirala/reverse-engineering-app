// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view.inputmethod;

import android.os.BaseBundle;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import androidx.core.view.ViewCompat;
import androidx.core.view.ContentInfoCompat;
import android.content.ClipData;
import android.content.ClipData$Item;
import android.content.ClipDescription;
import android.net.Uri;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.view.inputmethod.InputConnectionWrapper;
import androidx.core.util.ObjectsCompat;
import androidx.core.util.Preconditions;
import android.view.View;
import android.os.Parcelable;
import android.view.inputmethod.InputContentInfo;
import android.os.Build$VERSION;
import androidx.annotation.Nullable;
import android.os.Bundle;
import android.view.inputmethod.EditorInfo;
import androidx.annotation.NonNull;
import android.view.inputmethod.InputConnection;
import android.annotation.SuppressLint;

@SuppressLint({ "PrivateConstructorForUtilityClass" })
public final class InputConnectionCompat
{
    private static final String COMMIT_CONTENT_ACTION = "androidx.core.view.inputmethod.InputConnectionCompat.COMMIT_CONTENT";
    private static final String COMMIT_CONTENT_CONTENT_URI_INTEROP_KEY = "android.support.v13.view.inputmethod.InputConnectionCompat.CONTENT_URI";
    private static final String COMMIT_CONTENT_CONTENT_URI_KEY = "androidx.core.view.inputmethod.InputConnectionCompat.CONTENT_URI";
    private static final String COMMIT_CONTENT_DESCRIPTION_INTEROP_KEY = "android.support.v13.view.inputmethod.InputConnectionCompat.CONTENT_DESCRIPTION";
    private static final String COMMIT_CONTENT_DESCRIPTION_KEY = "androidx.core.view.inputmethod.InputConnectionCompat.CONTENT_DESCRIPTION";
    private static final String COMMIT_CONTENT_FLAGS_INTEROP_KEY = "android.support.v13.view.inputmethod.InputConnectionCompat.CONTENT_FLAGS";
    private static final String COMMIT_CONTENT_FLAGS_KEY = "androidx.core.view.inputmethod.InputConnectionCompat.CONTENT_FLAGS";
    private static final String COMMIT_CONTENT_INTEROP_ACTION = "android.support.v13.view.inputmethod.InputConnectionCompat.COMMIT_CONTENT";
    private static final String COMMIT_CONTENT_LINK_URI_INTEROP_KEY = "android.support.v13.view.inputmethod.InputConnectionCompat.CONTENT_LINK_URI";
    private static final String COMMIT_CONTENT_LINK_URI_KEY = "androidx.core.view.inputmethod.InputConnectionCompat.CONTENT_LINK_URI";
    private static final String COMMIT_CONTENT_OPTS_INTEROP_KEY = "android.support.v13.view.inputmethod.InputConnectionCompat.CONTENT_OPTS";
    private static final String COMMIT_CONTENT_OPTS_KEY = "androidx.core.view.inputmethod.InputConnectionCompat.CONTENT_OPTS";
    private static final String COMMIT_CONTENT_RESULT_INTEROP_RECEIVER_KEY = "android.support.v13.view.inputmethod.InputConnectionCompat.CONTENT_RESULT_RECEIVER";
    private static final String COMMIT_CONTENT_RESULT_RECEIVER_KEY = "androidx.core.view.inputmethod.InputConnectionCompat.CONTENT_RESULT_RECEIVER";
    private static final String EXTRA_INPUT_CONTENT_INFO = "androidx.core.view.extra.INPUT_CONTENT_INFO";
    public static final int INPUT_CONTENT_GRANT_READ_URI_PERMISSION = 1;
    private static final String LOG_TAG = "InputConnectionCompat";
    
    @Deprecated
    public InputConnectionCompat() {
    }
    
    public static boolean commitContent(@NonNull final InputConnection inputConnection, @NonNull final EditorInfo editorInfo, @NonNull final InputContentInfoCompat inputContentInfoCompat, final int n, @Nullable final Bundle bundle) {
        if (Build$VERSION.SDK_INT >= 25) {
            return Api25Impl.commitContent(inputConnection, (InputContentInfo)inputContentInfoCompat.unwrap(), n, bundle);
        }
        final int protocol = EditorInfoCompat.getProtocol(editorInfo);
        int n3;
        if (protocol != 2) {
            final int n2 = n3 = 0;
            if (protocol != 3) {
                n3 = n2;
                if (protocol != 4) {
                    return false;
                }
            }
        }
        else {
            n3 = 1;
        }
        final Bundle bundle2 = new Bundle();
        String s;
        if (n3 != 0) {
            s = "android.support.v13.view.inputmethod.InputConnectionCompat.CONTENT_URI";
        }
        else {
            s = "androidx.core.view.inputmethod.InputConnectionCompat.CONTENT_URI";
        }
        bundle2.putParcelable(s, (Parcelable)inputContentInfoCompat.getContentUri());
        String s2;
        if (n3 != 0) {
            s2 = "android.support.v13.view.inputmethod.InputConnectionCompat.CONTENT_DESCRIPTION";
        }
        else {
            s2 = "androidx.core.view.inputmethod.InputConnectionCompat.CONTENT_DESCRIPTION";
        }
        bundle2.putParcelable(s2, (Parcelable)inputContentInfoCompat.getDescription());
        String s3;
        if (n3 != 0) {
            s3 = "android.support.v13.view.inputmethod.InputConnectionCompat.CONTENT_LINK_URI";
        }
        else {
            s3 = "androidx.core.view.inputmethod.InputConnectionCompat.CONTENT_LINK_URI";
        }
        bundle2.putParcelable(s3, (Parcelable)inputContentInfoCompat.getLinkUri());
        String s4;
        if (n3 != 0) {
            s4 = "android.support.v13.view.inputmethod.InputConnectionCompat.CONTENT_FLAGS";
        }
        else {
            s4 = "androidx.core.view.inputmethod.InputConnectionCompat.CONTENT_FLAGS";
        }
        ((BaseBundle)bundle2).putInt(s4, n);
        String s5;
        if (n3 != 0) {
            s5 = "android.support.v13.view.inputmethod.InputConnectionCompat.CONTENT_OPTS";
        }
        else {
            s5 = "androidx.core.view.inputmethod.InputConnectionCompat.CONTENT_OPTS";
        }
        bundle2.putParcelable(s5, (Parcelable)bundle);
        String s6;
        if (n3 != 0) {
            s6 = "android.support.v13.view.inputmethod.InputConnectionCompat.COMMIT_CONTENT";
        }
        else {
            s6 = "androidx.core.view.inputmethod.InputConnectionCompat.COMMIT_CONTENT";
        }
        return inputConnection.performPrivateCommand(s6, bundle2);
    }
    
    @NonNull
    private static OnCommitContentListener createOnCommitContentListenerUsingPerformReceiveContent(@NonNull final View view) {
        Preconditions.checkNotNull(view);
        return (OnCommitContentListener)new \u3007\u3007888(view);
    }
    
    @NonNull
    public static InputConnection createWrapper(@NonNull final View view, @NonNull final InputConnection inputConnection, @NonNull final EditorInfo editorInfo) {
        return createWrapper(inputConnection, editorInfo, createOnCommitContentListenerUsingPerformReceiveContent(view));
    }
    
    @Deprecated
    @NonNull
    public static InputConnection createWrapper(@NonNull final InputConnection inputConnection, @NonNull final EditorInfo editorInfo, @NonNull final OnCommitContentListener onCommitContentListener) {
        ObjectsCompat.requireNonNull(inputConnection, "inputConnection must be non-null");
        ObjectsCompat.requireNonNull(editorInfo, "editorInfo must be non-null");
        ObjectsCompat.requireNonNull(onCommitContentListener, "onCommitContentListener must be non-null");
        if (Build$VERSION.SDK_INT >= 25) {
            return (InputConnection)new InputConnectionWrapper(inputConnection, false, onCommitContentListener) {
                final OnCommitContentListener val$listener;
                
                public boolean commitContent(final InputContentInfo inputContentInfo, final int n, final Bundle bundle) {
                    return this.val$listener.onCommitContent(InputContentInfoCompat.wrap(inputContentInfo), n, bundle) || super.commitContent(inputContentInfo, n, bundle);
                }
            };
        }
        if (EditorInfoCompat.getContentMimeTypes(editorInfo).length == 0) {
            return inputConnection;
        }
        return (InputConnection)new InputConnectionWrapper(inputConnection, false, onCommitContentListener) {
            final OnCommitContentListener val$listener;
            
            public boolean performPrivateCommand(final String s, final Bundle bundle) {
                return InputConnectionCompat.handlePerformPrivateCommand(s, bundle, this.val$listener) || super.performPrivateCommand(s, bundle);
            }
        };
    }
    
    static boolean handlePerformPrivateCommand(@Nullable String s, @Nullable Bundle bundle, @NonNull final OnCommitContentListener onCommitContentListener) {
        final int n = 0;
        if (bundle == null) {
            return false;
        }
        boolean b;
        if (TextUtils.equals((CharSequence)"androidx.core.view.inputmethod.InputConnectionCompat.COMMIT_CONTENT", (CharSequence)s)) {
            b = false;
        }
        else {
            if (!TextUtils.equals((CharSequence)"android.support.v13.view.inputmethod.InputConnectionCompat.COMMIT_CONTENT", (CharSequence)s)) {
                return false;
            }
            b = true;
        }
        if (b) {
            s = "android.support.v13.view.inputmethod.InputConnectionCompat.CONTENT_RESULT_RECEIVER";
        }
        else {
            s = "androidx.core.view.inputmethod.InputConnectionCompat.CONTENT_RESULT_RECEIVER";
        }
        ResultReceiver resultReceiver2;
        try {
            final ResultReceiver resultReceiver = (ResultReceiver)bundle.getParcelable(s);
            if (b) {
                s = "android.support.v13.view.inputmethod.InputConnectionCompat.CONTENT_URI";
            }
            else {
                s = "androidx.core.view.inputmethod.InputConnectionCompat.CONTENT_URI";
            }
            try {
                final Uri uri = (Uri)bundle.getParcelable(s);
                if (b) {
                    s = "android.support.v13.view.inputmethod.InputConnectionCompat.CONTENT_DESCRIPTION";
                }
                else {
                    s = "androidx.core.view.inputmethod.InputConnectionCompat.CONTENT_DESCRIPTION";
                }
                final ClipDescription clipDescription = (ClipDescription)bundle.getParcelable(s);
                if (b) {
                    s = "android.support.v13.view.inputmethod.InputConnectionCompat.CONTENT_LINK_URI";
                }
                else {
                    s = "androidx.core.view.inputmethod.InputConnectionCompat.CONTENT_LINK_URI";
                }
                final Uri uri2 = (Uri)bundle.getParcelable(s);
                if (b) {
                    s = "android.support.v13.view.inputmethod.InputConnectionCompat.CONTENT_FLAGS";
                }
                else {
                    s = "androidx.core.view.inputmethod.InputConnectionCompat.CONTENT_FLAGS";
                }
                final int int1 = ((BaseBundle)bundle).getInt(s);
                if (b) {
                    s = "android.support.v13.view.inputmethod.InputConnectionCompat.CONTENT_OPTS";
                }
                else {
                    s = "androidx.core.view.inputmethod.InputConnectionCompat.CONTENT_OPTS";
                }
                bundle = (Bundle)bundle.getParcelable(s);
                int onCommitContent = n;
                if (uri != null) {
                    onCommitContent = n;
                    if (clipDescription != null) {
                        onCommitContent = (onCommitContentListener.onCommitContent(new InputContentInfoCompat(uri, clipDescription, uri2), int1, bundle) ? 1 : 0);
                    }
                }
                if (resultReceiver != null) {
                    resultReceiver.send(onCommitContent, (Bundle)null);
                }
                return onCommitContent != 0;
            }
            finally {}
        }
        finally {
            resultReceiver2 = null;
        }
        if (resultReceiver2 != null) {
            resultReceiver2.send(0, (Bundle)null);
        }
    }
    
    @RequiresApi(25)
    static class Api25Impl
    {
        private Api25Impl() {
        }
        
        @DoNotInline
        static boolean commitContent(final InputConnection inputConnection, final InputContentInfo inputContentInfo, final int n, final Bundle bundle) {
            return oO80.\u3007080(inputConnection, inputContentInfo, n, bundle);
        }
    }
    
    public interface OnCommitContentListener
    {
        boolean onCommitContent(@NonNull final InputContentInfoCompat p0, final int p1, @Nullable final Bundle p2);
    }
}

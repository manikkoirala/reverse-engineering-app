// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.view.MenuItem;
import android.view.MenuInflater;
import androidx.annotation.NonNull;
import android.view.Menu;

public interface MenuProvider
{
    void onCreateMenu(@NonNull final Menu p0, @NonNull final MenuInflater p1);
    
    void onMenuClosed(@NonNull final Menu p0);
    
    boolean onMenuItemSelected(@NonNull final MenuItem p0);
    
    void onPrepareMenu(@NonNull final Menu p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import androidx.annotation.NonNull;
import android.view.ScaleGestureDetector;

public final class ScaleGestureDetectorCompat
{
    private ScaleGestureDetectorCompat() {
    }
    
    public static boolean isQuickScaleEnabled(@NonNull final ScaleGestureDetector scaleGestureDetector) {
        return Api19Impl.isQuickScaleEnabled(scaleGestureDetector);
    }
    
    @Deprecated
    public static boolean isQuickScaleEnabled(final Object o) {
        return isQuickScaleEnabled((ScaleGestureDetector)o);
    }
    
    public static void setQuickScaleEnabled(@NonNull final ScaleGestureDetector scaleGestureDetector, final boolean b) {
        Api19Impl.setQuickScaleEnabled(scaleGestureDetector, b);
    }
    
    @Deprecated
    public static void setQuickScaleEnabled(final Object o, final boolean b) {
        setQuickScaleEnabled((ScaleGestureDetector)o, b);
    }
    
    @RequiresApi(19)
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        @DoNotInline
        static boolean isQuickScaleEnabled(final ScaleGestureDetector scaleGestureDetector) {
            return scaleGestureDetector.isQuickScaleEnabled();
        }
        
        @DoNotInline
        static void setQuickScaleEnabled(final ScaleGestureDetector scaleGestureDetector, final boolean quickScaleEnabled) {
            scaleGestureDetector.setQuickScaleEnabled(quickScaleEnabled);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.os.Build$VERSION;
import androidx.annotation.IdRes;
import android.view.View;
import androidx.annotation.NonNull;
import android.view.Window;

public final class WindowCompat
{
    public static final int FEATURE_ACTION_BAR = 8;
    public static final int FEATURE_ACTION_BAR_OVERLAY = 9;
    public static final int FEATURE_ACTION_MODE_OVERLAY = 10;
    
    private WindowCompat() {
    }
    
    @NonNull
    public static WindowInsetsControllerCompat getInsetsController(@NonNull final Window window, @NonNull final View view) {
        return new WindowInsetsControllerCompat(window, view);
    }
    
    @NonNull
    public static <T extends View> T requireViewById(@NonNull final Window window, @IdRes final int n) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.requireViewById(window, n);
        }
        final View viewById = window.findViewById(n);
        if (viewById != null) {
            return (T)viewById;
        }
        throw new IllegalArgumentException("ID does not reference a View inside this Window");
    }
    
    public static void setDecorFitsSystemWindows(@NonNull final Window window, final boolean b) {
        if (Build$VERSION.SDK_INT >= 30) {
            Api30Impl.setDecorFitsSystemWindows(window, b);
        }
        else {
            Api16Impl.setDecorFitsSystemWindows(window, b);
        }
    }
    
    @RequiresApi(16)
    static class Api16Impl
    {
        private Api16Impl() {
        }
        
        @DoNotInline
        static void setDecorFitsSystemWindows(@NonNull final Window window, final boolean b) {
            final View decorView = window.getDecorView();
            final int systemUiVisibility = decorView.getSystemUiVisibility();
            int systemUiVisibility2;
            if (b) {
                systemUiVisibility2 = (systemUiVisibility & 0xFFFFF8FF);
            }
            else {
                systemUiVisibility2 = (systemUiVisibility | 0x700);
            }
            decorView.setSystemUiVisibility(systemUiVisibility2);
        }
    }
    
    @RequiresApi(28)
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        @DoNotInline
        static <T> T requireViewById(final Window window, final int n) {
            return (T)\u3007o\u30078.\u3007080(window, n);
        }
    }
    
    @RequiresApi(30)
    static class Api30Impl
    {
        private Api30Impl() {
        }
        
        @DoNotInline
        static void setDecorFitsSystemWindows(@NonNull final Window window, final boolean b) {
            ooo\u3007\u3007O\u3007.\u3007080(window, b);
        }
    }
}

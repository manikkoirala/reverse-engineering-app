// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import java.util.Iterator;
import kotlin.sequences.Sequence;
import kotlin.jvm.functions.Function2;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import android.view.MenuItem;
import org.jetbrains.annotations.NotNull;
import android.view.Menu;
import kotlin.Metadata;

@Metadata
public final class MenuKt
{
    public static final boolean contains(@NotNull final Menu menu, @NotNull final MenuItem menuItem) {
        Intrinsics.checkNotNullParameter((Object)menu, "<this>");
        Intrinsics.checkNotNullParameter((Object)menuItem, "item");
        for (int size = menu.size(), i = 0; i < size; ++i) {
            if (Intrinsics.\u3007o\u3007((Object)menu.getItem(i), (Object)menuItem)) {
                return true;
            }
        }
        return false;
    }
    
    public static final void forEach(@NotNull final Menu menu, @NotNull final Function1<? super MenuItem, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)menu, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "action");
        for (int size = menu.size(), i = 0; i < size; ++i) {
            final MenuItem item = menu.getItem(i);
            Intrinsics.checkNotNullExpressionValue((Object)item, "getItem(index)");
            function1.invoke((Object)item);
        }
    }
    
    public static final void forEachIndexed(@NotNull final Menu menu, @NotNull final Function2<? super Integer, ? super MenuItem, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)menu, "<this>");
        Intrinsics.checkNotNullParameter((Object)function2, "action");
        for (int size = menu.size(), i = 0; i < size; ++i) {
            final MenuItem item = menu.getItem(i);
            Intrinsics.checkNotNullExpressionValue((Object)item, "getItem(index)");
            function2.invoke((Object)i, (Object)item);
        }
    }
    
    @NotNull
    public static final MenuItem get(@NotNull final Menu menu, final int n) {
        Intrinsics.checkNotNullParameter((Object)menu, "<this>");
        final MenuItem item = menu.getItem(n);
        Intrinsics.checkNotNullExpressionValue((Object)item, "getItem(index)");
        return item;
    }
    
    @NotNull
    public static final Sequence<MenuItem> getChildren(@NotNull final Menu menu) {
        Intrinsics.checkNotNullParameter((Object)menu, "<this>");
        return (Sequence<MenuItem>)new MenuKt$children.MenuKt$children$1(menu);
    }
    
    public static final int getSize(@NotNull final Menu menu) {
        Intrinsics.checkNotNullParameter((Object)menu, "<this>");
        return menu.size();
    }
    
    public static final boolean isEmpty(@NotNull final Menu menu) {
        Intrinsics.checkNotNullParameter((Object)menu, "<this>");
        return menu.size() == 0;
    }
    
    public static final boolean isNotEmpty(@NotNull final Menu menu) {
        Intrinsics.checkNotNullParameter((Object)menu, "<this>");
        return menu.size() != 0;
    }
    
    @NotNull
    public static final Iterator<MenuItem> iterator(@NotNull final Menu menu) {
        Intrinsics.checkNotNullParameter((Object)menu, "<this>");
        return (Iterator<MenuItem>)new MenuKt$iterator.MenuKt$iterator$1(menu);
    }
    
    public static final void minusAssign(@NotNull final Menu menu, @NotNull final MenuItem menuItem) {
        Intrinsics.checkNotNullParameter((Object)menu, "<this>");
        Intrinsics.checkNotNullParameter((Object)menuItem, "item");
        menu.removeItem(menuItem.getItemId());
    }
    
    public static final void removeItemAt(@NotNull final Menu menu, final int n) {
        Intrinsics.checkNotNullParameter((Object)menu, "<this>");
        final MenuItem item = menu.getItem(n);
        Unit \u3007080;
        if (item != null) {
            menu.removeItem(item.getItemId());
            \u3007080 = Unit.\u3007080;
        }
        else {
            \u3007080 = null;
        }
        if (\u3007080 != null) {
            return;
        }
        throw new IndexOutOfBoundsException();
    }
}

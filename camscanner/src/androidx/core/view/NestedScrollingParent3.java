// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.annotation.NonNull;
import android.view.View;

public interface NestedScrollingParent3 extends NestedScrollingParent2
{
    void onNestedScroll(@NonNull final View p0, final int p1, final int p2, final int p3, final int p4, final int p5, @NonNull final int[] p6);
}

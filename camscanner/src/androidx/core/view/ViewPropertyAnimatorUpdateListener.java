// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.annotation.NonNull;
import android.view.View;

public interface ViewPropertyAnimatorUpdateListener
{
    void onAnimationUpdate(@NonNull final View p0);
}

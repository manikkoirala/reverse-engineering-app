// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.view.Gravity;
import androidx.annotation.NonNull;
import android.graphics.Rect;

public final class GravityCompat
{
    public static final int END = 8388613;
    public static final int RELATIVE_HORIZONTAL_GRAVITY_MASK = 8388615;
    public static final int RELATIVE_LAYOUT_DIRECTION = 8388608;
    public static final int START = 8388611;
    
    private GravityCompat() {
    }
    
    public static void apply(final int n, final int n2, final int n3, @NonNull final Rect rect, final int n4, final int n5, @NonNull final Rect rect2, final int n6) {
        Api17Impl.apply(n, n2, n3, rect, n4, n5, rect2, n6);
    }
    
    public static void apply(final int n, final int n2, final int n3, @NonNull final Rect rect, @NonNull final Rect rect2, final int n4) {
        Api17Impl.apply(n, n2, n3, rect, rect2, n4);
    }
    
    public static void applyDisplay(final int n, @NonNull final Rect rect, @NonNull final Rect rect2, final int n2) {
        Api17Impl.applyDisplay(n, rect, rect2, n2);
    }
    
    public static int getAbsoluteGravity(final int n, final int n2) {
        return Gravity.getAbsoluteGravity(n, n2);
    }
    
    @RequiresApi(17)
    static class Api17Impl
    {
        private Api17Impl() {
        }
        
        @DoNotInline
        static void apply(final int n, final int n2, final int n3, final Rect rect, final int n4, final int n5, final Rect rect2, final int n6) {
            Gravity.apply(n, n2, n3, rect, n4, n5, rect2, n6);
        }
        
        @DoNotInline
        static void apply(final int n, final int n2, final int n3, final Rect rect, final Rect rect2, final int n4) {
            Gravity.apply(n, n2, n3, rect, rect2, n4);
        }
        
        @DoNotInline
        static void applyDisplay(final int n, final Rect rect, final Rect rect2, final int n2) {
            Gravity.applyDisplay(n, rect, rect2, n2);
        }
    }
}

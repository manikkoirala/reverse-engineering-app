// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.annotation.RequiresApi;
import android.annotation.SuppressLint;
import androidx.annotation.Px;
import android.view.ViewGroup$MarginLayoutParams;
import java.util.Iterator;
import kotlin.ranges.RangesKt;
import kotlin.ranges.IntRange;
import kotlin.sequences.SequencesKt;
import kotlin.coroutines.Continuation;
import kotlin.sequences.Sequence;
import kotlin.jvm.functions.Function2;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import android.view.View;
import org.jetbrains.annotations.NotNull;
import android.view.ViewGroup;
import kotlin.Metadata;

@Metadata
public final class ViewGroupKt
{
    public static final boolean contains(@NotNull final ViewGroup viewGroup, @NotNull final View view) {
        Intrinsics.checkNotNullParameter((Object)viewGroup, "<this>");
        Intrinsics.checkNotNullParameter((Object)view, "view");
        return viewGroup.indexOfChild(view) != -1;
    }
    
    public static final void forEach(@NotNull final ViewGroup viewGroup, @NotNull final Function1<? super View, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)viewGroup, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "action");
        for (int childCount = viewGroup.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = viewGroup.getChildAt(i);
            Intrinsics.checkNotNullExpressionValue((Object)child, "getChildAt(index)");
            function1.invoke((Object)child);
        }
    }
    
    public static final void forEachIndexed(@NotNull final ViewGroup viewGroup, @NotNull final Function2<? super Integer, ? super View, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)viewGroup, "<this>");
        Intrinsics.checkNotNullParameter((Object)function2, "action");
        for (int childCount = viewGroup.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = viewGroup.getChildAt(i);
            Intrinsics.checkNotNullExpressionValue((Object)child, "getChildAt(index)");
            function2.invoke((Object)i, (Object)child);
        }
    }
    
    @NotNull
    public static final View get(@NotNull final ViewGroup viewGroup, final int i) {
        Intrinsics.checkNotNullParameter((Object)viewGroup, "<this>");
        final View child = viewGroup.getChildAt(i);
        if (child != null) {
            return child;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Index: ");
        sb.append(i);
        sb.append(", Size: ");
        sb.append(viewGroup.getChildCount());
        throw new IndexOutOfBoundsException(sb.toString());
    }
    
    @NotNull
    public static final Sequence<View> getChildren(@NotNull final ViewGroup viewGroup) {
        Intrinsics.checkNotNullParameter((Object)viewGroup, "<this>");
        return (Sequence<View>)new ViewGroupKt$children.ViewGroupKt$children$1(viewGroup);
    }
    
    @NotNull
    public static final Sequence<View> getDescendants(@NotNull final ViewGroup viewGroup) {
        Intrinsics.checkNotNullParameter((Object)viewGroup, "<this>");
        return (Sequence<View>)SequencesKt.\u3007o00\u3007\u3007Oo((Function2)new ViewGroupKt$descendants.ViewGroupKt$descendants$1(viewGroup, (Continuation)null));
    }
    
    @NotNull
    public static final IntRange getIndices(@NotNull final ViewGroup viewGroup) {
        Intrinsics.checkNotNullParameter((Object)viewGroup, "<this>");
        return RangesKt.OO0o\u3007\u3007(0, viewGroup.getChildCount());
    }
    
    public static final int getSize(@NotNull final ViewGroup viewGroup) {
        Intrinsics.checkNotNullParameter((Object)viewGroup, "<this>");
        return viewGroup.getChildCount();
    }
    
    public static final boolean isEmpty(@NotNull final ViewGroup viewGroup) {
        Intrinsics.checkNotNullParameter((Object)viewGroup, "<this>");
        return viewGroup.getChildCount() == 0;
    }
    
    public static final boolean isNotEmpty(@NotNull final ViewGroup viewGroup) {
        Intrinsics.checkNotNullParameter((Object)viewGroup, "<this>");
        return viewGroup.getChildCount() != 0;
    }
    
    @NotNull
    public static final Iterator<View> iterator(@NotNull final ViewGroup viewGroup) {
        Intrinsics.checkNotNullParameter((Object)viewGroup, "<this>");
        return (Iterator<View>)new ViewGroupKt$iterator.ViewGroupKt$iterator$1(viewGroup);
    }
    
    public static final void minusAssign(@NotNull final ViewGroup viewGroup, @NotNull final View view) {
        Intrinsics.checkNotNullParameter((Object)viewGroup, "<this>");
        Intrinsics.checkNotNullParameter((Object)view, "view");
        viewGroup.removeView(view);
    }
    
    public static final void plusAssign(@NotNull final ViewGroup viewGroup, @NotNull final View view) {
        Intrinsics.checkNotNullParameter((Object)viewGroup, "<this>");
        Intrinsics.checkNotNullParameter((Object)view, "view");
        viewGroup.addView(view);
    }
    
    public static final void setMargins(@NotNull final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, @Px final int n) {
        Intrinsics.checkNotNullParameter((Object)viewGroup$MarginLayoutParams, "<this>");
        viewGroup$MarginLayoutParams.setMargins(n, n, n, n);
    }
    
    public static final void updateMargins(@NotNull final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, @Px final int n, @Px final int n2, @Px final int n3, @Px final int n4) {
        Intrinsics.checkNotNullParameter((Object)viewGroup$MarginLayoutParams, "<this>");
        viewGroup$MarginLayoutParams.setMargins(n, n2, n3, n4);
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(17)
    public static final void updateMarginsRelative(@NotNull final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, @Px final int marginStart, @Px final int topMargin, @Px final int marginEnd, @Px final int bottomMargin) {
        Intrinsics.checkNotNullParameter((Object)viewGroup$MarginLayoutParams, "<this>");
        viewGroup$MarginLayoutParams.setMarginStart(marginStart);
        viewGroup$MarginLayoutParams.topMargin = topMargin;
        viewGroup$MarginLayoutParams.setMarginEnd(marginEnd);
        viewGroup$MarginLayoutParams.bottomMargin = bottomMargin;
    }
}

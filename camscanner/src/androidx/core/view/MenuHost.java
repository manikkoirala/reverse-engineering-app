// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.annotation.SuppressLint;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.annotation.NonNull;

public interface MenuHost
{
    void addMenuProvider(@NonNull final MenuProvider p0);
    
    void addMenuProvider(@NonNull final MenuProvider p0, @NonNull final LifecycleOwner p1);
    
    @SuppressLint({ "LambdaLast" })
    void addMenuProvider(@NonNull final MenuProvider p0, @NonNull final LifecycleOwner p1, @NonNull final Lifecycle.State p2);
    
    void invalidateMenu();
    
    void removeMenuProvider(@NonNull final MenuProvider p0);
}

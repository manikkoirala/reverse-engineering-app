// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.annotation.DoNotInline;
import kotlin.jvm.internal.Intrinsics;
import android.view.View;
import org.jetbrains.annotations.NotNull;
import androidx.annotation.RequiresApi;
import kotlin.Metadata;

@Metadata
@RequiresApi(16)
final class Api16Impl
{
    @NotNull
    public static final Api16Impl INSTANCE;
    
    static {
        INSTANCE = new Api16Impl();
    }
    
    private Api16Impl() {
    }
    
    @DoNotInline
    public static final void postOnAnimationDelayed(@NotNull final View view, @NotNull final Runnable runnable, final long n) {
        Intrinsics.checkNotNullParameter((Object)view, "view");
        Intrinsics.checkNotNullParameter((Object)runnable, "action");
        view.postOnAnimationDelayed(runnable, n);
    }
}

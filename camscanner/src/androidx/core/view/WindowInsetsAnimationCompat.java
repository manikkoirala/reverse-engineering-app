// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.animation.TimeInterpolator;
import java.util.ArrayList;
import java.util.HashMap;
import android.view.WindowInsetsAnimation$Callback;
import android.animation.Animator$AnimatorListener;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import java.util.Collections;
import android.animation.ValueAnimator$AnimatorUpdateListener;
import android.animation.ValueAnimator;
import android.view.animation.DecelerateInterpolator;
import java.util.Objects;
import androidx.core.R;
import android.view.ViewGroup;
import android.view.View$OnApplyWindowInsetsListener;
import android.annotation.SuppressLint;
import androidx.annotation.RestrictTo;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.List;
import android.view.WindowInsets;
import android.view.WindowInsetsAnimation$Bounds;
import androidx.core.graphics.Insets;
import androidx.annotation.FloatRange;
import android.view.View;
import androidx.annotation.RequiresApi;
import androidx.annotation.NonNull;
import android.view.WindowInsetsAnimation;
import android.os.Build$VERSION;
import androidx.annotation.Nullable;
import android.view.animation.Interpolator;

public final class WindowInsetsAnimationCompat
{
    private static final boolean DEBUG = false;
    private static final String TAG = "WindowInsetsAnimCompat";
    private Impl mImpl;
    
    public WindowInsetsAnimationCompat(final int n, @Nullable final Interpolator interpolator, final long n2) {
        if (Build$VERSION.SDK_INT >= 30) {
            this.mImpl = (Impl)new Impl30(n, interpolator, n2);
        }
        else {
            this.mImpl = (Impl)new Impl21(n, interpolator, n2);
        }
    }
    
    @RequiresApi(30)
    private WindowInsetsAnimationCompat(@NonNull final WindowInsetsAnimation windowInsetsAnimation) {
        this(0, null, 0L);
        if (Build$VERSION.SDK_INT >= 30) {
            this.mImpl = (Impl)new Impl30(windowInsetsAnimation);
        }
    }
    
    static void setCallback(@NonNull final View view, @Nullable final Callback callback) {
        if (Build$VERSION.SDK_INT >= 30) {
            Impl30.setCallback(view, callback);
        }
        else {
            Impl21.setCallback(view, callback);
        }
    }
    
    @RequiresApi(30)
    static WindowInsetsAnimationCompat toWindowInsetsAnimationCompat(final WindowInsetsAnimation windowInsetsAnimation) {
        return new WindowInsetsAnimationCompat(windowInsetsAnimation);
    }
    
    @FloatRange(from = 0.0, to = 1.0)
    public float getAlpha() {
        return this.mImpl.getAlpha();
    }
    
    public long getDurationMillis() {
        return this.mImpl.getDurationMillis();
    }
    
    @FloatRange(from = 0.0, to = 1.0)
    public float getFraction() {
        return this.mImpl.getFraction();
    }
    
    public float getInterpolatedFraction() {
        return this.mImpl.getInterpolatedFraction();
    }
    
    @Nullable
    public Interpolator getInterpolator() {
        return this.mImpl.getInterpolator();
    }
    
    public int getTypeMask() {
        return this.mImpl.getTypeMask();
    }
    
    public void setAlpha(@FloatRange(from = 0.0, to = 1.0) final float alpha) {
        this.mImpl.setAlpha(alpha);
    }
    
    public void setFraction(@FloatRange(from = 0.0, to = 1.0) final float fraction) {
        this.mImpl.setFraction(fraction);
    }
    
    public static final class BoundsCompat
    {
        private final Insets mLowerBound;
        private final Insets mUpperBound;
        
        @RequiresApi(30)
        private BoundsCompat(@NonNull final WindowInsetsAnimation$Bounds windowInsetsAnimation$Bounds) {
            this.mLowerBound = Impl30.getLowerBounds(windowInsetsAnimation$Bounds);
            this.mUpperBound = Impl30.getHigherBounds(windowInsetsAnimation$Bounds);
        }
        
        public BoundsCompat(@NonNull final Insets mLowerBound, @NonNull final Insets mUpperBound) {
            this.mLowerBound = mLowerBound;
            this.mUpperBound = mUpperBound;
        }
        
        @NonNull
        @RequiresApi(30)
        public static BoundsCompat toBoundsCompat(@NonNull final WindowInsetsAnimation$Bounds windowInsetsAnimation$Bounds) {
            return new BoundsCompat(windowInsetsAnimation$Bounds);
        }
        
        @NonNull
        public Insets getLowerBound() {
            return this.mLowerBound;
        }
        
        @NonNull
        public Insets getUpperBound() {
            return this.mUpperBound;
        }
        
        @NonNull
        public BoundsCompat inset(@NonNull final Insets insets) {
            return new BoundsCompat(WindowInsetsCompat.insetInsets(this.mLowerBound, insets.left, insets.top, insets.right, insets.bottom), WindowInsetsCompat.insetInsets(this.mUpperBound, insets.left, insets.top, insets.right, insets.bottom));
        }
        
        @NonNull
        @RequiresApi(30)
        public WindowInsetsAnimation$Bounds toBounds() {
            return Impl30.createPlatformBounds(this);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Bounds{lower=");
            sb.append(this.mLowerBound);
            sb.append(" upper=");
            sb.append(this.mUpperBound);
            sb.append("}");
            return sb.toString();
        }
    }
    
    public abstract static class Callback
    {
        public static final int DISPATCH_MODE_CONTINUE_ON_SUBTREE = 1;
        public static final int DISPATCH_MODE_STOP = 0;
        WindowInsets mDispachedInsets;
        private final int mDispatchMode;
        
        public Callback(final int mDispatchMode) {
            this.mDispatchMode = mDispatchMode;
        }
        
        public final int getDispatchMode() {
            return this.mDispatchMode;
        }
        
        public void onEnd(@NonNull final WindowInsetsAnimationCompat windowInsetsAnimationCompat) {
        }
        
        public void onPrepare(@NonNull final WindowInsetsAnimationCompat windowInsetsAnimationCompat) {
        }
        
        @NonNull
        public abstract WindowInsetsCompat onProgress(@NonNull final WindowInsetsCompat p0, @NonNull final List<WindowInsetsAnimationCompat> p1);
        
        @NonNull
        public BoundsCompat onStart(@NonNull final WindowInsetsAnimationCompat windowInsetsAnimationCompat, @NonNull final BoundsCompat boundsCompat) {
            return boundsCompat;
        }
        
        @Retention(RetentionPolicy.SOURCE)
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public @interface DispatchMode {
        }
    }
    
    private static class Impl
    {
        private float mAlpha;
        private final long mDurationMillis;
        private float mFraction;
        @Nullable
        private final Interpolator mInterpolator;
        private final int mTypeMask;
        
        Impl(final int mTypeMask, @Nullable final Interpolator mInterpolator, final long mDurationMillis) {
            this.mTypeMask = mTypeMask;
            this.mInterpolator = mInterpolator;
            this.mDurationMillis = mDurationMillis;
        }
        
        public float getAlpha() {
            return this.mAlpha;
        }
        
        public long getDurationMillis() {
            return this.mDurationMillis;
        }
        
        public float getFraction() {
            return this.mFraction;
        }
        
        public float getInterpolatedFraction() {
            final Interpolator mInterpolator = this.mInterpolator;
            if (mInterpolator != null) {
                return ((TimeInterpolator)mInterpolator).getInterpolation(this.mFraction);
            }
            return this.mFraction;
        }
        
        @Nullable
        public Interpolator getInterpolator() {
            return this.mInterpolator;
        }
        
        public int getTypeMask() {
            return this.mTypeMask;
        }
        
        public void setAlpha(final float mAlpha) {
            this.mAlpha = mAlpha;
        }
        
        public void setFraction(final float mFraction) {
            this.mFraction = mFraction;
        }
    }
    
    @RequiresApi(21)
    private static class Impl21 extends Impl
    {
        Impl21(final int n, @Nullable final Interpolator interpolator, final long n2) {
            super(n, interpolator, n2);
        }
        
        @SuppressLint({ "WrongConstant" })
        static int buildAnimationMask(@NonNull final WindowInsetsCompat windowInsetsCompat, @NonNull final WindowInsetsCompat windowInsetsCompat2) {
            int n = 0;
            int n2;
            for (int i = 1; i <= 256; i <<= 1, n = n2) {
                n2 = n;
                if (!windowInsetsCompat.getInsets(i).equals(windowInsetsCompat2.getInsets(i))) {
                    n2 = (n | i);
                }
            }
            return n;
        }
        
        @NonNull
        static BoundsCompat computeAnimationBounds(@NonNull final WindowInsetsCompat windowInsetsCompat, @NonNull final WindowInsetsCompat windowInsetsCompat2, final int n) {
            final Insets insets = windowInsetsCompat.getInsets(n);
            final Insets insets2 = windowInsetsCompat2.getInsets(n);
            return new BoundsCompat(Insets.of(Math.min(insets.left, insets2.left), Math.min(insets.top, insets2.top), Math.min(insets.right, insets2.right), Math.min(insets.bottom, insets2.bottom)), Insets.of(Math.max(insets.left, insets2.left), Math.max(insets.top, insets2.top), Math.max(insets.right, insets2.right), Math.max(insets.bottom, insets2.bottom)));
        }
        
        @NonNull
        private static View$OnApplyWindowInsetsListener createProxyListener(@NonNull final View view, @NonNull final Callback callback) {
            return (View$OnApplyWindowInsetsListener)new Impl21OnApplyWindowInsetsListener(view, callback);
        }
        
        static void dispatchOnEnd(@NonNull final View view, @NonNull final WindowInsetsAnimationCompat windowInsetsAnimationCompat) {
            final Callback callback = getCallback(view);
            if (callback != null) {
                callback.onEnd(windowInsetsAnimationCompat);
                if (callback.getDispatchMode() == 0) {
                    return;
                }
            }
            if (view instanceof ViewGroup) {
                final ViewGroup viewGroup = (ViewGroup)view;
                for (int i = 0; i < viewGroup.getChildCount(); ++i) {
                    dispatchOnEnd(viewGroup.getChildAt(i), windowInsetsAnimationCompat);
                }
            }
        }
        
        static void dispatchOnPrepare(final View view, final WindowInsetsAnimationCompat windowInsetsAnimationCompat, final WindowInsets mDispachedInsets, final boolean b) {
            final Callback callback = getCallback(view);
            int i = 0;
            boolean b2 = b;
            if (callback != null) {
                callback.mDispachedInsets = mDispachedInsets;
                if (!(b2 = b)) {
                    callback.onPrepare(windowInsetsAnimationCompat);
                    b2 = (callback.getDispatchMode() == 0);
                }
            }
            if (view instanceof ViewGroup) {
                for (ViewGroup viewGroup = (ViewGroup)view; i < viewGroup.getChildCount(); ++i) {
                    dispatchOnPrepare(viewGroup.getChildAt(i), windowInsetsAnimationCompat, mDispachedInsets, b2);
                }
            }
        }
        
        static void dispatchOnProgress(@NonNull final View view, @NonNull final WindowInsetsCompat windowInsetsCompat, @NonNull final List<WindowInsetsAnimationCompat> list) {
            final Callback callback = getCallback(view);
            WindowInsetsCompat onProgress = windowInsetsCompat;
            if (callback != null) {
                onProgress = callback.onProgress(windowInsetsCompat, list);
                if (callback.getDispatchMode() == 0) {
                    return;
                }
            }
            if (view instanceof ViewGroup) {
                final ViewGroup viewGroup = (ViewGroup)view;
                for (int i = 0; i < viewGroup.getChildCount(); ++i) {
                    dispatchOnProgress(viewGroup.getChildAt(i), onProgress, list);
                }
            }
        }
        
        static void dispatchOnStart(final View view, final WindowInsetsAnimationCompat windowInsetsAnimationCompat, final BoundsCompat boundsCompat) {
            final Callback callback = getCallback(view);
            if (callback != null) {
                callback.onStart(windowInsetsAnimationCompat, boundsCompat);
                if (callback.getDispatchMode() == 0) {
                    return;
                }
            }
            if (view instanceof ViewGroup) {
                final ViewGroup viewGroup = (ViewGroup)view;
                for (int i = 0; i < viewGroup.getChildCount(); ++i) {
                    dispatchOnStart(viewGroup.getChildAt(i), windowInsetsAnimationCompat, boundsCompat);
                }
            }
        }
        
        @NonNull
        static WindowInsets forwardToViewIfNeeded(@NonNull final View view, @NonNull final WindowInsets windowInsets) {
            if (view.getTag(R.id.tag_on_apply_window_listener) != null) {
                return windowInsets;
            }
            return view.onApplyWindowInsets(windowInsets);
        }
        
        @Nullable
        static Callback getCallback(final View view) {
            final Object tag = view.getTag(R.id.tag_window_insets_animation_callback);
            Callback mCallback;
            if (tag instanceof Impl21OnApplyWindowInsetsListener) {
                mCallback = ((Impl21OnApplyWindowInsetsListener)tag).mCallback;
            }
            else {
                mCallback = null;
            }
            return mCallback;
        }
        
        @SuppressLint({ "WrongConstant" })
        static WindowInsetsCompat interpolateInsets(final WindowInsetsCompat windowInsetsCompat, final WindowInsetsCompat windowInsetsCompat2, final float n, final int n2) {
            final WindowInsetsCompat.Builder builder = new WindowInsetsCompat.Builder(windowInsetsCompat);
            for (int i = 1; i <= 256; i <<= 1) {
                if ((n2 & i) == 0x0) {
                    builder.setInsets(i, windowInsetsCompat.getInsets(i));
                }
                else {
                    final Insets insets = windowInsetsCompat.getInsets(i);
                    final Insets insets2 = windowInsetsCompat2.getInsets(i);
                    final float n3 = (float)(insets.left - insets2.left);
                    final float n4 = 1.0f - n;
                    builder.setInsets(i, WindowInsetsCompat.insetInsets(insets, (int)(n3 * n4 + 0.5), (int)((insets.top - insets2.top) * n4 + 0.5), (int)((insets.right - insets2.right) * n4 + 0.5), (int)((insets.bottom - insets2.bottom) * n4 + 0.5)));
                }
            }
            return builder.build();
        }
        
        static void setCallback(@NonNull final View view, @Nullable final Callback callback) {
            final Object tag = view.getTag(R.id.tag_on_apply_window_listener);
            if (callback == null) {
                view.setTag(R.id.tag_window_insets_animation_callback, (Object)null);
                if (tag == null) {
                    view.setOnApplyWindowInsetsListener((View$OnApplyWindowInsetsListener)null);
                }
            }
            else {
                final View$OnApplyWindowInsetsListener proxyListener = createProxyListener(view, callback);
                view.setTag(R.id.tag_window_insets_animation_callback, (Object)proxyListener);
                if (tag == null) {
                    view.setOnApplyWindowInsetsListener(proxyListener);
                }
            }
        }
        
        @RequiresApi(21)
        private static class Impl21OnApplyWindowInsetsListener implements View$OnApplyWindowInsetsListener
        {
            private static final int COMPAT_ANIMATION_DURATION = 160;
            final Callback mCallback;
            private WindowInsetsCompat mLastInsets;
            
            Impl21OnApplyWindowInsetsListener(@NonNull final View view, @NonNull final Callback mCallback) {
                this.mCallback = mCallback;
                final WindowInsetsCompat rootWindowInsets = ViewCompat.getRootWindowInsets(view);
                WindowInsetsCompat build;
                if (rootWindowInsets != null) {
                    build = new WindowInsetsCompat.Builder(rootWindowInsets).build();
                }
                else {
                    build = null;
                }
                this.mLastInsets = build;
            }
            
            public WindowInsets onApplyWindowInsets(final View view, final WindowInsets b) {
                if (!view.isLaidOut()) {
                    this.mLastInsets = WindowInsetsCompat.toWindowInsetsCompat(b, view);
                    return Impl21.forwardToViewIfNeeded(view, b);
                }
                final WindowInsetsCompat windowInsetsCompat = WindowInsetsCompat.toWindowInsetsCompat(b, view);
                if (this.mLastInsets == null) {
                    this.mLastInsets = ViewCompat.getRootWindowInsets(view);
                }
                if (this.mLastInsets == null) {
                    this.mLastInsets = windowInsetsCompat;
                    return Impl21.forwardToViewIfNeeded(view, b);
                }
                final Callback callback = Impl21.getCallback(view);
                if (callback != null && Objects.equals(callback.mDispachedInsets, b)) {
                    return Impl21.forwardToViewIfNeeded(view, b);
                }
                final int buildAnimationMask = Impl21.buildAnimationMask(windowInsetsCompat, this.mLastInsets);
                if (buildAnimationMask == 0) {
                    return Impl21.forwardToViewIfNeeded(view, b);
                }
                final WindowInsetsCompat mLastInsets = this.mLastInsets;
                final WindowInsetsAnimationCompat windowInsetsAnimationCompat = new WindowInsetsAnimationCompat(buildAnimationMask, (Interpolator)new DecelerateInterpolator(), 160L);
                windowInsetsAnimationCompat.setFraction(0.0f);
                final ValueAnimator setDuration = ValueAnimator.ofFloat(new float[] { 0.0f, 1.0f }).setDuration(windowInsetsAnimationCompat.getDurationMillis());
                final BoundsCompat computeAnimationBounds = Impl21.computeAnimationBounds(windowInsetsCompat, mLastInsets, buildAnimationMask);
                Impl21.dispatchOnPrepare(view, windowInsetsAnimationCompat, b, false);
                setDuration.addUpdateListener((ValueAnimator$AnimatorUpdateListener)new ValueAnimator$AnimatorUpdateListener(this, windowInsetsAnimationCompat, windowInsetsCompat, mLastInsets, buildAnimationMask, view) {
                    final Impl21OnApplyWindowInsetsListener this$0;
                    final WindowInsetsAnimationCompat val$anim;
                    final int val$animationMask;
                    final WindowInsetsCompat val$startingInsets;
                    final WindowInsetsCompat val$targetInsets;
                    final View val$v;
                    
                    public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                        this.val$anim.setFraction(valueAnimator.getAnimatedFraction());
                        Impl21.dispatchOnProgress(this.val$v, Impl21.interpolateInsets(this.val$targetInsets, this.val$startingInsets, this.val$anim.getInterpolatedFraction(), this.val$animationMask), Collections.singletonList(this.val$anim));
                    }
                });
                ((Animator)setDuration).addListener((Animator$AnimatorListener)new AnimatorListenerAdapter(this, windowInsetsAnimationCompat, view) {
                    final Impl21OnApplyWindowInsetsListener this$0;
                    final WindowInsetsAnimationCompat val$anim;
                    final View val$v;
                    
                    public void onAnimationEnd(final Animator animator) {
                        this.val$anim.setFraction(1.0f);
                        Impl21.dispatchOnEnd(this.val$v, this.val$anim);
                    }
                });
                OneShotPreDrawListener.add(view, new Runnable(this, view, windowInsetsAnimationCompat, computeAnimationBounds, setDuration) {
                    final Impl21OnApplyWindowInsetsListener this$0;
                    final WindowInsetsAnimationCompat val$anim;
                    final BoundsCompat val$animationBounds;
                    final ValueAnimator val$animator;
                    final View val$v;
                    
                    @Override
                    public void run() {
                        Impl21.dispatchOnStart(this.val$v, this.val$anim, this.val$animationBounds);
                        this.val$animator.start();
                    }
                });
                this.mLastInsets = windowInsetsCompat;
                return Impl21.forwardToViewIfNeeded(view, b);
            }
        }
    }
    
    @RequiresApi(30)
    private static class Impl30 extends Impl
    {
        @NonNull
        private final WindowInsetsAnimation mWrapped;
        
        Impl30(final int n, final Interpolator interpolator, final long n2) {
            this(new WindowInsetsAnimation(n, interpolator, n2));
        }
        
        Impl30(@NonNull final WindowInsetsAnimation mWrapped) {
            super(0, null, 0L);
            this.mWrapped = mWrapped;
        }
        
        @NonNull
        public static WindowInsetsAnimation$Bounds createPlatformBounds(@NonNull final BoundsCompat boundsCompat) {
            return new WindowInsetsAnimation$Bounds(boundsCompat.getLowerBound().toPlatformInsets(), boundsCompat.getUpperBound().toPlatformInsets());
        }
        
        @NonNull
        public static Insets getHigherBounds(@NonNull final WindowInsetsAnimation$Bounds windowInsetsAnimation$Bounds) {
            return Insets.toCompatInsets(OoO\u3007.\u3007080(windowInsetsAnimation$Bounds));
        }
        
        @NonNull
        public static Insets getLowerBounds(@NonNull final WindowInsetsAnimation$Bounds windowInsetsAnimation$Bounds) {
            return Insets.toCompatInsets(O\u30070\u3007o808\u3007.\u3007080(windowInsetsAnimation$Bounds));
        }
        
        public static void setCallback(@NonNull final View view, @Nullable final Callback callback) {
            ProxyCallback proxyCallback;
            if (callback != null) {
                proxyCallback = new ProxyCallback(callback);
            }
            else {
                proxyCallback = null;
            }
            O00O.\u3007080(view, (WindowInsetsAnimation$Callback)proxyCallback);
        }
        
        @Override
        public long getDurationMillis() {
            return OO\u30070008O8.\u3007080(this.mWrapped);
        }
        
        @Override
        public float getFraction() {
            return \u3007o\u3007Oo0.\u3007080(this.mWrapped);
        }
        
        @Override
        public float getInterpolatedFraction() {
            return \u30078o.\u3007080(this.mWrapped);
        }
        
        @Nullable
        @Override
        public Interpolator getInterpolator() {
            return Oo08OO8oO.\u3007080(this.mWrapped);
        }
        
        @Override
        public int getTypeMask() {
            return OO0\u3007\u30078.\u3007080(this.mWrapped);
        }
        
        @Override
        public void setFraction(final float n) {
            \u3007Oo\u3007o8.\u3007080(this.mWrapped, n);
        }
        
        @RequiresApi(30)
        private static class ProxyCallback extends WindowInsetsAnimation$Callback
        {
            private final HashMap<WindowInsetsAnimation, WindowInsetsAnimationCompat> mAnimations;
            private final Callback mCompat;
            private List<WindowInsetsAnimationCompat> mRORunningAnimations;
            private ArrayList<WindowInsetsAnimationCompat> mTmpRunningAnimations;
            
            ProxyCallback(@NonNull final Callback mCompat) {
                super(mCompat.getDispatchMode());
                this.mAnimations = new HashMap<WindowInsetsAnimation, WindowInsetsAnimationCompat>();
                this.mCompat = mCompat;
            }
            
            @NonNull
            private WindowInsetsAnimationCompat getWindowInsetsAnimationCompat(@NonNull final WindowInsetsAnimation windowInsetsAnimation) {
                WindowInsetsAnimationCompat windowInsetsAnimationCompat;
                if ((windowInsetsAnimationCompat = this.mAnimations.get(windowInsetsAnimation)) == null) {
                    windowInsetsAnimationCompat = WindowInsetsAnimationCompat.toWindowInsetsAnimationCompat(windowInsetsAnimation);
                    this.mAnimations.put(windowInsetsAnimation, windowInsetsAnimationCompat);
                }
                return windowInsetsAnimationCompat;
            }
            
            public void onEnd(@NonNull final WindowInsetsAnimation key) {
                this.mCompat.onEnd(this.getWindowInsetsAnimationCompat(key));
                this.mAnimations.remove(key);
            }
            
            public void onPrepare(@NonNull final WindowInsetsAnimation windowInsetsAnimation) {
                this.mCompat.onPrepare(this.getWindowInsetsAnimationCompat(windowInsetsAnimation));
            }
            
            @NonNull
            public WindowInsets onProgress(@NonNull final WindowInsets windowInsets, @NonNull final List<WindowInsetsAnimation> list) {
                final ArrayList<WindowInsetsAnimationCompat> mTmpRunningAnimations = this.mTmpRunningAnimations;
                if (mTmpRunningAnimations == null) {
                    final ArrayList list2 = new ArrayList(list.size());
                    this.mTmpRunningAnimations = list2;
                    this.mRORunningAnimations = (List<WindowInsetsAnimationCompat>)Collections.unmodifiableList((List<?>)list2);
                }
                else {
                    mTmpRunningAnimations.clear();
                }
                for (int i = list.size() - 1; i >= 0; --i) {
                    final WindowInsetsAnimation windowInsetsAnimation = list.get(i);
                    final WindowInsetsAnimationCompat windowInsetsAnimationCompat = this.getWindowInsetsAnimationCompat(windowInsetsAnimation);
                    windowInsetsAnimationCompat.setFraction(\u3007o\u3007Oo0.\u3007080(windowInsetsAnimation));
                    this.mTmpRunningAnimations.add(windowInsetsAnimationCompat);
                }
                return this.mCompat.onProgress(WindowInsetsCompat.toWindowInsetsCompat(windowInsets), this.mRORunningAnimations).toWindowInsets();
            }
            
            @NonNull
            public WindowInsetsAnimation$Bounds onStart(@NonNull final WindowInsetsAnimation windowInsetsAnimation, @NonNull final WindowInsetsAnimation$Bounds windowInsetsAnimation$Bounds) {
                return this.mCompat.onStart(this.getWindowInsetsAnimationCompat(windowInsetsAnimation), BoundsCompat.toBoundsCompat(windowInsetsAnimation$Bounds)).toBounds();
            }
        }
    }
}

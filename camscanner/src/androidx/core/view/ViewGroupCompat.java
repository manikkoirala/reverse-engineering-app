// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.view.accessibility.AccessibilityEvent;
import android.view.View;
import androidx.annotation.NonNull;
import android.view.ViewGroup;

public final class ViewGroupCompat
{
    public static final int LAYOUT_MODE_CLIP_BOUNDS = 0;
    public static final int LAYOUT_MODE_OPTICAL_BOUNDS = 1;
    
    private ViewGroupCompat() {
    }
    
    public static int getLayoutMode(@NonNull final ViewGroup viewGroup) {
        return Api18Impl.getLayoutMode(viewGroup);
    }
    
    public static int getNestedScrollAxes(@NonNull final ViewGroup viewGroup) {
        return Api21Impl.getNestedScrollAxes(viewGroup);
    }
    
    public static boolean isTransitionGroup(@NonNull final ViewGroup viewGroup) {
        return Api21Impl.isTransitionGroup(viewGroup);
    }
    
    @Deprecated
    public static boolean onRequestSendAccessibilityEvent(final ViewGroup viewGroup, final View view, final AccessibilityEvent accessibilityEvent) {
        return viewGroup.onRequestSendAccessibilityEvent(view, accessibilityEvent);
    }
    
    public static void setLayoutMode(@NonNull final ViewGroup viewGroup, final int n) {
        Api18Impl.setLayoutMode(viewGroup, n);
    }
    
    @Deprecated
    public static void setMotionEventSplittingEnabled(final ViewGroup viewGroup, final boolean motionEventSplittingEnabled) {
        viewGroup.setMotionEventSplittingEnabled(motionEventSplittingEnabled);
    }
    
    public static void setTransitionGroup(@NonNull final ViewGroup viewGroup, final boolean b) {
        Api21Impl.setTransitionGroup(viewGroup, b);
    }
    
    @RequiresApi(18)
    static class Api18Impl
    {
        private Api18Impl() {
        }
        
        @DoNotInline
        static int getLayoutMode(final ViewGroup viewGroup) {
            return viewGroup.getLayoutMode();
        }
        
        @DoNotInline
        static void setLayoutMode(final ViewGroup viewGroup, final int layoutMode) {
            viewGroup.setLayoutMode(layoutMode);
        }
    }
    
    @RequiresApi(21)
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        @DoNotInline
        static int getNestedScrollAxes(final ViewGroup viewGroup) {
            return viewGroup.getNestedScrollAxes();
        }
        
        @DoNotInline
        static boolean isTransitionGroup(final ViewGroup viewGroup) {
            return viewGroup.isTransitionGroup();
        }
        
        @DoNotInline
        static void setTransitionGroup(final ViewGroup viewGroup, final boolean transitionGroup) {
            viewGroup.setTransitionGroup(transitionGroup);
        }
    }
}

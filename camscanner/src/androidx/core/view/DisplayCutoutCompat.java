// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import java.util.Collections;
import androidx.core.util.ObjectsCompat;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import androidx.core.graphics.Insets;
import android.os.Build$VERSION;
import java.util.List;
import androidx.annotation.Nullable;
import android.graphics.Rect;
import android.view.DisplayCutout;

public final class DisplayCutoutCompat
{
    private final DisplayCutout mDisplayCutout;
    
    public DisplayCutoutCompat(@Nullable final Rect rect, @Nullable final List<Rect> list) {
        DisplayCutout displayCutout;
        if (Build$VERSION.SDK_INT >= 28) {
            displayCutout = Api28Impl.createDisplayCutout(rect, list);
        }
        else {
            displayCutout = null;
        }
        this(displayCutout);
    }
    
    private DisplayCutoutCompat(final DisplayCutout mDisplayCutout) {
        this.mDisplayCutout = mDisplayCutout;
    }
    
    public DisplayCutoutCompat(@NonNull final Insets insets, @Nullable final Rect rect, @Nullable final Rect rect2, @Nullable final Rect rect3, @Nullable final Rect rect4, @NonNull final Insets insets2) {
        this(constructDisplayCutout(insets, rect, rect2, rect3, rect4, insets2));
    }
    
    private static DisplayCutout constructDisplayCutout(@NonNull final Insets insets, @Nullable final Rect e, @Nullable final Rect e2, @Nullable final Rect e3, @Nullable final Rect e4, @NonNull final Insets insets2) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 30) {
            return Api30Impl.createDisplayCutout(insets.toPlatformInsets(), e, e2, e3, e4, insets2.toPlatformInsets());
        }
        if (sdk_INT >= 29) {
            return Api29Impl.createDisplayCutout(insets.toPlatformInsets(), e, e2, e3, e4);
        }
        if (sdk_INT >= 28) {
            final Rect rect = new Rect(insets.left, insets.top, insets.right, insets.bottom);
            final ArrayList list = new ArrayList();
            if (e != null) {
                list.add(e);
            }
            if (e2 != null) {
                list.add(e2);
            }
            if (e3 != null) {
                list.add(e3);
            }
            if (e4 != null) {
                list.add(e4);
            }
            return Api28Impl.createDisplayCutout(rect, list);
        }
        return null;
    }
    
    static DisplayCutoutCompat wrap(final DisplayCutout displayCutout) {
        DisplayCutoutCompat displayCutoutCompat;
        if (displayCutout == null) {
            displayCutoutCompat = null;
        }
        else {
            displayCutoutCompat = new DisplayCutoutCompat(displayCutout);
        }
        return displayCutoutCompat;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o != null && DisplayCutoutCompat.class == o.getClass() && ObjectsCompat.equals(this.mDisplayCutout, ((DisplayCutoutCompat)o).mDisplayCutout));
    }
    
    @NonNull
    public List<Rect> getBoundingRects() {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getBoundingRects(this.mDisplayCutout);
        }
        return Collections.emptyList();
    }
    
    public int getSafeInsetBottom() {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getSafeInsetBottom(this.mDisplayCutout);
        }
        return 0;
    }
    
    public int getSafeInsetLeft() {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getSafeInsetLeft(this.mDisplayCutout);
        }
        return 0;
    }
    
    public int getSafeInsetRight() {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getSafeInsetRight(this.mDisplayCutout);
        }
        return 0;
    }
    
    public int getSafeInsetTop() {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.getSafeInsetTop(this.mDisplayCutout);
        }
        return 0;
    }
    
    @NonNull
    public Insets getWaterfallInsets() {
        if (Build$VERSION.SDK_INT >= 30) {
            return Insets.toCompatInsets(Api30Impl.getWaterfallInsets(this.mDisplayCutout));
        }
        return Insets.NONE;
    }
    
    @Override
    public int hashCode() {
        final DisplayCutout mDisplayCutout = this.mDisplayCutout;
        int \u3007080;
        if (mDisplayCutout == null) {
            \u3007080 = 0;
        }
        else {
            \u3007080 = \u30070\u3007O0088o.\u3007080(mDisplayCutout);
        }
        return \u3007080;
    }
    
    @NonNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DisplayCutoutCompat{");
        sb.append(this.mDisplayCutout);
        sb.append("}");
        return sb.toString();
    }
    
    @RequiresApi(28)
    DisplayCutout unwrap() {
        return this.mDisplayCutout;
    }
    
    @RequiresApi(28)
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        @DoNotInline
        static DisplayCutout createDisplayCutout(final Rect rect, final List<Rect> list) {
            return new DisplayCutout(rect, (List)list);
        }
        
        @DoNotInline
        static List<Rect> getBoundingRects(final DisplayCutout displayCutout) {
            return OoO8.\u3007080(displayCutout);
        }
        
        @DoNotInline
        static int getSafeInsetBottom(final DisplayCutout displayCutout) {
            return o800o8O.\u3007080(displayCutout);
        }
        
        @DoNotInline
        static int getSafeInsetLeft(final DisplayCutout displayCutout) {
            return \u3007O888o0o.\u3007080(displayCutout);
        }
        
        @DoNotInline
        static int getSafeInsetRight(final DisplayCutout displayCutout) {
            return \u3007oo\u3007.\u3007080(displayCutout);
        }
        
        @DoNotInline
        static int getSafeInsetTop(final DisplayCutout displayCutout) {
            return oo88o8O.\u3007080(displayCutout);
        }
    }
    
    @RequiresApi(29)
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        @DoNotInline
        static DisplayCutout createDisplayCutout(final android.graphics.Insets insets, final Rect rect, final Rect rect2, final Rect rect3, final Rect rect4) {
            return new DisplayCutout(insets, rect, rect2, rect3, rect4);
        }
    }
    
    @RequiresApi(30)
    static class Api30Impl
    {
        private Api30Impl() {
        }
        
        @DoNotInline
        static DisplayCutout createDisplayCutout(final android.graphics.Insets insets, final Rect rect, final Rect rect2, final Rect rect3, final Rect rect4, final android.graphics.Insets insets2) {
            return new DisplayCutout(insets, rect, rect2, rect3, rect4, insets2);
        }
        
        @DoNotInline
        static android.graphics.Insets getWaterfallInsets(final DisplayCutout displayCutout) {
            return o\u3007O8\u3007\u3007o.\u3007080(displayCutout);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.appcompat.widget.OoO8;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import androidx.annotation.RestrictTo;
import androidx.annotation.Nullable;
import android.os.Build$VERSION;
import android.view.DragEvent;
import androidx.annotation.NonNull;
import android.app.Activity;
import android.view.DragAndDropPermissions;

public final class DragAndDropPermissionsCompat
{
    private final DragAndDropPermissions mDragAndDropPermissions;
    
    private DragAndDropPermissionsCompat(final DragAndDropPermissions mDragAndDropPermissions) {
        this.mDragAndDropPermissions = mDragAndDropPermissions;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static DragAndDropPermissionsCompat request(@NonNull final Activity activity, @NonNull final DragEvent dragEvent) {
        if (Build$VERSION.SDK_INT >= 24) {
            final DragAndDropPermissions requestDragAndDropPermissions = Api24Impl.requestDragAndDropPermissions(activity, dragEvent);
            if (requestDragAndDropPermissions != null) {
                return new DragAndDropPermissionsCompat(requestDragAndDropPermissions);
            }
        }
        return null;
    }
    
    public void release() {
        if (Build$VERSION.SDK_INT >= 24) {
            Api24Impl.release(this.mDragAndDropPermissions);
        }
    }
    
    @RequiresApi(24)
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        @DoNotInline
        static void release(final DragAndDropPermissions dragAndDropPermissions) {
            \u300700.\u3007080(dragAndDropPermissions);
        }
        
        @DoNotInline
        static DragAndDropPermissions requestDragAndDropPermissions(final Activity activity, final DragEvent dragEvent) {
            return OoO8.\u3007080(activity, dragEvent);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.view.View;

public interface OnReceiveContentListener
{
    @Nullable
    ContentInfoCompat onReceiveContent(@NonNull final View p0, @NonNull final ContentInfoCompat p1);
}

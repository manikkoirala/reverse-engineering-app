// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.annotation.SuppressLint;
import androidx.annotation.Nullable;
import androidx.core.graphics.Insets;
import androidx.annotation.FloatRange;
import androidx.annotation.RequiresApi;
import androidx.annotation.NonNull;
import android.view.WindowInsetsAnimationController;

public final class WindowInsetsAnimationControllerCompat
{
    private final Impl mImpl;
    
    @RequiresApi(30)
    WindowInsetsAnimationControllerCompat(@NonNull final WindowInsetsAnimationController windowInsetsAnimationController) {
        this.mImpl = (Impl)new Impl30(windowInsetsAnimationController);
    }
    
    public void finish(final boolean b) {
        this.mImpl.finish(b);
    }
    
    public float getCurrentAlpha() {
        return this.mImpl.getCurrentAlpha();
    }
    
    @FloatRange(from = 0.0, to = 1.0)
    public float getCurrentFraction() {
        return this.mImpl.getCurrentFraction();
    }
    
    @NonNull
    public Insets getCurrentInsets() {
        return this.mImpl.getCurrentInsets();
    }
    
    @NonNull
    public Insets getHiddenStateInsets() {
        return this.mImpl.getHiddenStateInsets();
    }
    
    @NonNull
    public Insets getShownStateInsets() {
        return this.mImpl.getShownStateInsets();
    }
    
    public int getTypes() {
        return this.mImpl.getTypes();
    }
    
    public boolean isCancelled() {
        return this.mImpl.isCancelled();
    }
    
    public boolean isFinished() {
        return this.mImpl.isFinished();
    }
    
    public boolean isReady() {
        return !this.isFinished() && !this.isCancelled();
    }
    
    public void setInsetsAndAlpha(@Nullable final Insets insets, @FloatRange(from = 0.0, to = 1.0) final float n, @FloatRange(from = 0.0, to = 1.0) final float n2) {
        this.mImpl.setInsetsAndAlpha(insets, n, n2);
    }
    
    private static class Impl
    {
        Impl() {
        }
        
        void finish(final boolean b) {
        }
        
        public float getCurrentAlpha() {
            return 0.0f;
        }
        
        @FloatRange(from = 0.0, to = 1.0)
        public float getCurrentFraction() {
            return 0.0f;
        }
        
        @NonNull
        public Insets getCurrentInsets() {
            return Insets.NONE;
        }
        
        @NonNull
        public Insets getHiddenStateInsets() {
            return Insets.NONE;
        }
        
        @NonNull
        public Insets getShownStateInsets() {
            return Insets.NONE;
        }
        
        public int getTypes() {
            return 0;
        }
        
        boolean isCancelled() {
            return true;
        }
        
        boolean isFinished() {
            return false;
        }
        
        public void setInsetsAndAlpha(@Nullable final Insets insets, @FloatRange(from = 0.0, to = 1.0) final float n, @FloatRange(from = 0.0, to = 1.0) final float n2) {
        }
    }
    
    @RequiresApi(30)
    private static class Impl30 extends Impl
    {
        private final WindowInsetsAnimationController mController;
        
        Impl30(@NonNull final WindowInsetsAnimationController mController) {
            this.mController = mController;
        }
        
        @Override
        void finish(final boolean b) {
            O8oOo80.\u3007080(this.mController, b);
        }
        
        @Override
        public float getCurrentAlpha() {
            return o8o\u3007\u30070O.\u3007080(this.mController);
        }
        
        @Override
        public float getCurrentFraction() {
            return \u30078o8O\u3007O.\u3007080(this.mController);
        }
        
        @NonNull
        @Override
        public Insets getCurrentInsets() {
            return Insets.toCompatInsets(\u3007o8OO0.\u3007080(this.mController));
        }
        
        @NonNull
        @Override
        public Insets getHiddenStateInsets() {
            return Insets.toCompatInsets(\u3007\u30070o8O\u3007\u3007.\u3007080(this.mController));
        }
        
        @NonNull
        @Override
        public Insets getShownStateInsets() {
            return Insets.toCompatInsets(\u30078o\u3007\u30078080.\u3007080(this.mController));
        }
        
        @SuppressLint({ "WrongConstant" })
        @Override
        public int getTypes() {
            return \u3007oo.\u3007080(this.mController);
        }
        
        @Override
        boolean isCancelled() {
            return o\u3007\u30070\u300788.\u3007080(this.mController);
        }
        
        @Override
        boolean isFinished() {
            return O\u30078oOo8O.\u3007080(this.mController);
        }
        
        @Override
        public void setInsetsAndAlpha(@Nullable final Insets insets, final float n, final float n2) {
            final WindowInsetsAnimationController mController = this.mController;
            android.graphics.Insets platformInsets;
            if (insets == null) {
                platformInsets = null;
            }
            else {
                platformInsets = insets.toPlatformInsets();
            }
            Oo0oOo\u30070.\u3007080(mController, platformInsets, n, n2);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.core.util.Preconditions;
import android.os.Build$VERSION;
import androidx.annotation.DoNotInline;
import android.view.ContentInfo$Builder;
import java.util.Objects;
import android.net.Uri;
import androidx.annotation.Nullable;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import android.view.ContentInfo;
import java.util.ArrayList;
import android.util.Pair;
import androidx.core.util.Predicate;
import androidx.annotation.RestrictTo;
import android.content.ClipData;
import android.content.ClipData$Item;
import java.util.List;
import android.content.ClipDescription;
import androidx.annotation.NonNull;

public final class ContentInfoCompat
{
    public static final int FLAG_CONVERT_TO_PLAIN_TEXT = 1;
    public static final int SOURCE_APP = 0;
    public static final int SOURCE_AUTOFILL = 4;
    public static final int SOURCE_CLIPBOARD = 1;
    public static final int SOURCE_DRAG_AND_DROP = 3;
    public static final int SOURCE_INPUT_METHOD = 2;
    public static final int SOURCE_PROCESS_TEXT = 5;
    @NonNull
    private final Compat mCompat;
    
    ContentInfoCompat(@NonNull final Compat mCompat) {
        this.mCompat = mCompat;
    }
    
    @NonNull
    static ClipData buildClipData(@NonNull final ClipDescription clipDescription, @NonNull final List<ClipData$Item> list) {
        final ClipData clipData = new ClipData(new ClipDescription(clipDescription), (ClipData$Item)list.get(0));
        for (int i = 1; i < list.size(); ++i) {
            clipData.addItem((ClipData$Item)list.get(i));
        }
        return clipData;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    static String flagsToString(final int i) {
        if ((i & 0x1) != 0x0) {
            return "FLAG_CONVERT_TO_PLAIN_TEXT";
        }
        return String.valueOf(i);
    }
    
    @NonNull
    static Pair<ClipData, ClipData> partition(@NonNull final ClipData clipData, @NonNull final Predicate<ClipData$Item> predicate) {
        int i = 0;
        List<ClipData$Item> list = null;
        List<ClipData$Item> list2 = null;
        while (i < clipData.getItemCount()) {
            final ClipData$Item item = clipData.getItemAt(i);
            if (predicate.test(item)) {
                ArrayList<ClipData$Item> list3;
                if ((list3 = (ArrayList<ClipData$Item>)list) == null) {
                    list3 = new ArrayList<ClipData$Item>();
                }
                list3.add(item);
                list = list3;
            }
            else {
                ArrayList<ClipData$Item> list4;
                if ((list4 = (ArrayList<ClipData$Item>)list2) == null) {
                    list4 = new ArrayList<ClipData$Item>();
                }
                list4.add(item);
                list2 = list4;
            }
            ++i;
        }
        if (list == null) {
            return (Pair<ClipData, ClipData>)Pair.create((Object)null, (Object)clipData);
        }
        if (list2 == null) {
            return (Pair<ClipData, ClipData>)Pair.create((Object)clipData, (Object)null);
        }
        return (Pair<ClipData, ClipData>)Pair.create((Object)buildClipData(clipData.getDescription(), list), (Object)buildClipData(clipData.getDescription(), list2));
    }
    
    @NonNull
    @RequiresApi(31)
    public static Pair<ContentInfo, ContentInfo> partition(@NonNull final ContentInfo contentInfo, @NonNull final java.util.function.Predicate<ClipData$Item> predicate) {
        return Api31Impl.partition(contentInfo, predicate);
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    static String sourceToString(final int i) {
        if (i == 0) {
            return "SOURCE_APP";
        }
        if (i == 1) {
            return "SOURCE_CLIPBOARD";
        }
        if (i == 2) {
            return "SOURCE_INPUT_METHOD";
        }
        if (i == 3) {
            return "SOURCE_DRAG_AND_DROP";
        }
        if (i == 4) {
            return "SOURCE_AUTOFILL";
        }
        if (i != 5) {
            return String.valueOf(i);
        }
        return "SOURCE_PROCESS_TEXT";
    }
    
    @NonNull
    @RequiresApi(31)
    public static ContentInfoCompat toContentInfoCompat(@NonNull final ContentInfo contentInfo) {
        return new ContentInfoCompat((Compat)new Compat31Impl(contentInfo));
    }
    
    @NonNull
    public ClipData getClip() {
        return this.mCompat.getClip();
    }
    
    @Nullable
    public Bundle getExtras() {
        return this.mCompat.getExtras();
    }
    
    public int getFlags() {
        return this.mCompat.getFlags();
    }
    
    @Nullable
    public Uri getLinkUri() {
        return this.mCompat.getLinkUri();
    }
    
    public int getSource() {
        return this.mCompat.getSource();
    }
    
    @NonNull
    public Pair<ContentInfoCompat, ContentInfoCompat> partition(@NonNull final Predicate<ClipData$Item> predicate) {
        final ClipData clip = this.mCompat.getClip();
        final int itemCount = clip.getItemCount();
        Object o = null;
        if (itemCount == 1) {
            final boolean test = predicate.test(clip.getItemAt(0));
            ContentInfoCompat contentInfoCompat;
            if (test) {
                contentInfoCompat = this;
            }
            else {
                contentInfoCompat = null;
            }
            if (!test) {
                o = this;
            }
            return (Pair<ContentInfoCompat, ContentInfoCompat>)Pair.create((Object)contentInfoCompat, o);
        }
        final Pair<ClipData, ClipData> partition = partition(clip, predicate);
        if (partition.first == null) {
            return (Pair<ContentInfoCompat, ContentInfoCompat>)Pair.create((Object)null, (Object)this);
        }
        if (partition.second == null) {
            return (Pair<ContentInfoCompat, ContentInfoCompat>)Pair.create((Object)this, (Object)null);
        }
        return (Pair<ContentInfoCompat, ContentInfoCompat>)Pair.create((Object)new Builder(this).setClip((ClipData)partition.first).build(), (Object)new Builder(this).setClip((ClipData)partition.second).build());
    }
    
    @NonNull
    @RequiresApi(31)
    public ContentInfo toContentInfo() {
        final ContentInfo wrapped = this.mCompat.getWrapped();
        Objects.requireNonNull(wrapped);
        final ContentInfo contentInfo = wrapped;
        return wrapped;
    }
    
    @NonNull
    @Override
    public String toString() {
        return this.mCompat.toString();
    }
    
    @RequiresApi(31)
    private static final class Api31Impl
    {
        @DoNotInline
        @NonNull
        public static Pair<ContentInfo, ContentInfo> partition(@NonNull ContentInfo contentInfo, @NonNull final java.util.function.Predicate<ClipData$Item> obj) {
            final ClipData \u3007080 = androidx.core.view.\u3007080.\u3007080(contentInfo);
            if (\u3007080.getItemCount() == 1) {
                final boolean \u300781 = \u3007o00\u3007\u3007Oo.\u3007080((java.util.function.Predicate)obj, (Object)\u3007080.getItemAt(0));
                ContentInfo contentInfo2;
                if (\u300781) {
                    contentInfo2 = contentInfo;
                }
                else {
                    contentInfo2 = null;
                }
                if (\u300781) {
                    contentInfo = null;
                }
                return (Pair<ContentInfo, ContentInfo>)Pair.create((Object)contentInfo2, (Object)contentInfo);
            }
            Objects.requireNonNull(obj);
            final Pair<ClipData, ClipData> partition = ContentInfoCompat.partition(\u3007080, new Oo08(obj));
            if (partition.first == null) {
                return (Pair<ContentInfo, ContentInfo>)Pair.create((Object)null, (Object)contentInfo);
            }
            if (partition.second == null) {
                return (Pair<ContentInfo, ContentInfo>)Pair.create((Object)contentInfo, (Object)null);
            }
            return (Pair<ContentInfo, ContentInfo>)Pair.create((Object)O8.\u3007080(\u3007o\u3007.\u3007080(new ContentInfo$Builder(contentInfo), (ClipData)partition.first)), (Object)O8.\u3007080(\u3007o\u3007.\u3007080(new ContentInfo$Builder(contentInfo), (ClipData)partition.second)));
        }
    }
    
    public static final class Builder
    {
        @NonNull
        private final BuilderCompat mBuilderCompat;
        
        public Builder(@NonNull final ClipData clipData, final int n) {
            if (Build$VERSION.SDK_INT >= 31) {
                this.mBuilderCompat = new BuilderCompat31Impl(clipData, n);
            }
            else {
                this.mBuilderCompat = new BuilderCompatImpl(clipData, n);
            }
        }
        
        public Builder(@NonNull final ContentInfoCompat contentInfoCompat) {
            if (Build$VERSION.SDK_INT >= 31) {
                this.mBuilderCompat = new BuilderCompat31Impl(contentInfoCompat);
            }
            else {
                this.mBuilderCompat = new BuilderCompatImpl(contentInfoCompat);
            }
        }
        
        @NonNull
        public ContentInfoCompat build() {
            return this.mBuilderCompat.build();
        }
        
        @NonNull
        public Builder setClip(@NonNull final ClipData clip) {
            this.mBuilderCompat.setClip(clip);
            return this;
        }
        
        @NonNull
        public Builder setExtras(@Nullable final Bundle extras) {
            this.mBuilderCompat.setExtras(extras);
            return this;
        }
        
        @NonNull
        public Builder setFlags(final int flags) {
            this.mBuilderCompat.setFlags(flags);
            return this;
        }
        
        @NonNull
        public Builder setLinkUri(@Nullable final Uri linkUri) {
            this.mBuilderCompat.setLinkUri(linkUri);
            return this;
        }
        
        @NonNull
        public Builder setSource(final int source) {
            this.mBuilderCompat.setSource(source);
            return this;
        }
    }
    
    private interface BuilderCompat
    {
        @NonNull
        ContentInfoCompat build();
        
        void setClip(@NonNull final ClipData p0);
        
        void setExtras(@Nullable final Bundle p0);
        
        void setFlags(final int p0);
        
        void setLinkUri(@Nullable final Uri p0);
        
        void setSource(final int p0);
    }
    
    @RequiresApi(31)
    private static final class BuilderCompat31Impl implements BuilderCompat
    {
        @NonNull
        private final ContentInfo$Builder mPlatformBuilder;
        
        BuilderCompat31Impl(@NonNull final ClipData clipData, final int n) {
            this.mPlatformBuilder = new ContentInfo$Builder(clipData, n);
        }
        
        BuilderCompat31Impl(@NonNull final ContentInfoCompat contentInfoCompat) {
            this.mPlatformBuilder = new ContentInfo$Builder(contentInfoCompat.toContentInfo());
        }
        
        @NonNull
        @Override
        public ContentInfoCompat build() {
            return new ContentInfoCompat((Compat)new Compat31Impl(O8.\u3007080(this.mPlatformBuilder)));
        }
        
        @Override
        public void setClip(@NonNull final ClipData clipData) {
            \u3007o\u3007.\u3007080(this.mPlatformBuilder, clipData);
        }
        
        @Override
        public void setExtras(@Nullable final Bundle bundle) {
            o\u30070.\u3007080(this.mPlatformBuilder, bundle);
        }
        
        @Override
        public void setFlags(final int n) {
            oO80.\u3007080(this.mPlatformBuilder, n);
        }
        
        @Override
        public void setLinkUri(@Nullable final Uri uri) {
            \u300780\u3007808\u3007O.\u3007080(this.mPlatformBuilder, uri);
        }
        
        @Override
        public void setSource(final int n) {
            \u3007\u3007888.\u3007080(this.mPlatformBuilder, n);
        }
    }
    
    private static final class BuilderCompatImpl implements BuilderCompat
    {
        @NonNull
        ClipData mClip;
        @Nullable
        Bundle mExtras;
        int mFlags;
        @Nullable
        Uri mLinkUri;
        int mSource;
        
        BuilderCompatImpl(@NonNull final ClipData mClip, final int mSource) {
            this.mClip = mClip;
            this.mSource = mSource;
        }
        
        BuilderCompatImpl(@NonNull final ContentInfoCompat contentInfoCompat) {
            this.mClip = contentInfoCompat.getClip();
            this.mSource = contentInfoCompat.getSource();
            this.mFlags = contentInfoCompat.getFlags();
            this.mLinkUri = contentInfoCompat.getLinkUri();
            this.mExtras = contentInfoCompat.getExtras();
        }
        
        @NonNull
        @Override
        public ContentInfoCompat build() {
            return new ContentInfoCompat((Compat)new CompatImpl(this));
        }
        
        @Override
        public void setClip(@NonNull final ClipData mClip) {
            this.mClip = mClip;
        }
        
        @Override
        public void setExtras(@Nullable final Bundle mExtras) {
            this.mExtras = mExtras;
        }
        
        @Override
        public void setFlags(final int mFlags) {
            this.mFlags = mFlags;
        }
        
        @Override
        public void setLinkUri(@Nullable final Uri mLinkUri) {
            this.mLinkUri = mLinkUri;
        }
        
        @Override
        public void setSource(final int mSource) {
            this.mSource = mSource;
        }
    }
    
    private interface Compat
    {
        @NonNull
        ClipData getClip();
        
        @Nullable
        Bundle getExtras();
        
        int getFlags();
        
        @Nullable
        Uri getLinkUri();
        
        int getSource();
        
        @Nullable
        ContentInfo getWrapped();
    }
    
    @RequiresApi(31)
    private static final class Compat31Impl implements Compat
    {
        @NonNull
        private final ContentInfo mWrapped;
        
        Compat31Impl(@NonNull final ContentInfo contentInfo) {
            this.mWrapped = Preconditions.checkNotNull(contentInfo);
        }
        
        @NonNull
        @Override
        public ClipData getClip() {
            return \u3007080.\u3007080(this.mWrapped);
        }
        
        @Nullable
        @Override
        public Bundle getExtras() {
            return OO0o\u3007\u3007\u3007\u30070.\u3007080(this.mWrapped);
        }
        
        @Override
        public int getFlags() {
            return \u3007O8o08O.\u3007080(this.mWrapped);
        }
        
        @Nullable
        @Override
        public Uri getLinkUri() {
            return \u30078o8o\u3007.\u3007080(this.mWrapped);
        }
        
        @Override
        public int getSource() {
            return OO0o\u3007\u3007.\u3007080(this.mWrapped);
        }
        
        @NonNull
        @Override
        public ContentInfo getWrapped() {
            return this.mWrapped;
        }
        
        @NonNull
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("ContentInfoCompat{");
            sb.append(this.mWrapped);
            sb.append("}");
            return sb.toString();
        }
    }
    
    private static final class CompatImpl implements Compat
    {
        @NonNull
        private final ClipData mClip;
        @Nullable
        private final Bundle mExtras;
        private final int mFlags;
        @Nullable
        private final Uri mLinkUri;
        private final int mSource;
        
        CompatImpl(final BuilderCompatImpl builderCompatImpl) {
            this.mClip = Preconditions.checkNotNull(builderCompatImpl.mClip);
            this.mSource = Preconditions.checkArgumentInRange(builderCompatImpl.mSource, 0, 5, "source");
            this.mFlags = Preconditions.checkFlagsArgument(builderCompatImpl.mFlags, 1);
            this.mLinkUri = builderCompatImpl.mLinkUri;
            this.mExtras = builderCompatImpl.mExtras;
        }
        
        @NonNull
        @Override
        public ClipData getClip() {
            return this.mClip;
        }
        
        @Nullable
        @Override
        public Bundle getExtras() {
            return this.mExtras;
        }
        
        @Override
        public int getFlags() {
            return this.mFlags;
        }
        
        @Nullable
        @Override
        public Uri getLinkUri() {
            return this.mLinkUri;
        }
        
        @Override
        public int getSource() {
            return this.mSource;
        }
        
        @Nullable
        @Override
        public ContentInfo getWrapped() {
            return null;
        }
        
        @NonNull
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("ContentInfoCompat{clip=");
            sb.append(this.mClip.getDescription());
            sb.append(", source=");
            sb.append(ContentInfoCompat.sourceToString(this.mSource));
            sb.append(", flags=");
            sb.append(ContentInfoCompat.flagsToString(this.mFlags));
            final Uri mLinkUri = this.mLinkUri;
            final String s = "";
            String string;
            if (mLinkUri == null) {
                string = "";
            }
            else {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(", hasLinkUri(");
                sb2.append(this.mLinkUri.toString().length());
                sb2.append(")");
                string = sb2.toString();
            }
            sb.append(string);
            String str;
            if (this.mExtras == null) {
                str = s;
            }
            else {
                str = ", hasExtras";
            }
            sb.append(str);
            sb.append("}");
            return sb.toString();
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public @interface Flags {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public @interface Source {
    }
}

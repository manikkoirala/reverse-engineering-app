// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.os.BaseBundle;
import androidx.annotation.DoNotInline;
import android.view.accessibility.AccessibilityNodeInfo;
import androidx.annotation.RequiresApi;
import android.os.Bundle;
import android.view.ViewGroup;
import androidx.annotation.Nullable;
import android.view.accessibility.AccessibilityNodeProvider;
import androidx.core.view.accessibility.AccessibilityNodeProviderCompat;
import android.view.accessibility.AccessibilityEvent;
import java.lang.ref.WeakReference;
import android.util.SparseArray;
import android.text.style.ClickableSpan;
import java.util.Collections;
import androidx.core.R;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import java.util.List;
import android.view.View;
import androidx.annotation.RestrictTo;
import androidx.annotation.NonNull;
import android.view.View$AccessibilityDelegate;

public class AccessibilityDelegateCompat
{
    private static final View$AccessibilityDelegate DEFAULT_DELEGATE;
    private final View$AccessibilityDelegate mBridge;
    private final View$AccessibilityDelegate mOriginalDelegate;
    
    static {
        DEFAULT_DELEGATE = new View$AccessibilityDelegate();
    }
    
    public AccessibilityDelegateCompat() {
        this(AccessibilityDelegateCompat.DEFAULT_DELEGATE);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public AccessibilityDelegateCompat(@NonNull final View$AccessibilityDelegate mOriginalDelegate) {
        this.mOriginalDelegate = mOriginalDelegate;
        this.mBridge = new AccessibilityDelegateAdapter(this);
    }
    
    static List<AccessibilityNodeInfoCompat.AccessibilityActionCompat> getActionList(final View view) {
        List<Object> emptyList;
        if ((emptyList = (List)view.getTag(R.id.tag_accessibility_actions)) == null) {
            emptyList = Collections.emptyList();
        }
        return (List<AccessibilityNodeInfoCompat.AccessibilityActionCompat>)emptyList;
    }
    
    private boolean isSpanStillValid(final ClickableSpan clickableSpan, final View view) {
        if (clickableSpan != null) {
            final ClickableSpan[] clickableSpans = AccessibilityNodeInfoCompat.getClickableSpans(view.createAccessibilityNodeInfo().getText());
            for (int n = 0; clickableSpans != null && n < clickableSpans.length; ++n) {
                if (clickableSpan.equals(clickableSpans[n])) {
                    return true;
                }
            }
        }
        return false;
    }
    
    private boolean performClickableSpanAction(final int n, final View view) {
        final SparseArray sparseArray = (SparseArray)view.getTag(R.id.tag_accessibility_clickable_spans);
        if (sparseArray != null) {
            final WeakReference weakReference = (WeakReference)sparseArray.get(n);
            if (weakReference != null) {
                final ClickableSpan clickableSpan = (ClickableSpan)weakReference.get();
                if (this.isSpanStillValid(clickableSpan, view)) {
                    clickableSpan.onClick(view);
                    return true;
                }
            }
        }
        return false;
    }
    
    public boolean dispatchPopulateAccessibilityEvent(@NonNull final View view, @NonNull final AccessibilityEvent accessibilityEvent) {
        return this.mOriginalDelegate.dispatchPopulateAccessibilityEvent(view, accessibilityEvent);
    }
    
    @Nullable
    public AccessibilityNodeProviderCompat getAccessibilityNodeProvider(@NonNull final View view) {
        final AccessibilityNodeProvider accessibilityNodeProvider = Api16Impl.getAccessibilityNodeProvider(this.mOriginalDelegate, view);
        if (accessibilityNodeProvider != null) {
            return new AccessibilityNodeProviderCompat(accessibilityNodeProvider);
        }
        return null;
    }
    
    View$AccessibilityDelegate getBridge() {
        return this.mBridge;
    }
    
    public void onInitializeAccessibilityEvent(@NonNull final View view, @NonNull final AccessibilityEvent accessibilityEvent) {
        this.mOriginalDelegate.onInitializeAccessibilityEvent(view, accessibilityEvent);
    }
    
    public void onInitializeAccessibilityNodeInfo(@NonNull final View view, @NonNull final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        this.mOriginalDelegate.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat.unwrap());
    }
    
    public void onPopulateAccessibilityEvent(@NonNull final View view, @NonNull final AccessibilityEvent accessibilityEvent) {
        this.mOriginalDelegate.onPopulateAccessibilityEvent(view, accessibilityEvent);
    }
    
    public boolean onRequestSendAccessibilityEvent(@NonNull final ViewGroup viewGroup, @NonNull final View view, @NonNull final AccessibilityEvent accessibilityEvent) {
        return this.mOriginalDelegate.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
    }
    
    public boolean performAccessibilityAction(@NonNull final View view, final int n, @Nullable final Bundle bundle) {
        final List<AccessibilityNodeInfoCompat.AccessibilityActionCompat> actionList = getActionList(view);
        final int n2 = 0;
        int n3 = 0;
        boolean perform;
        while (true) {
            perform = (n2 != 0);
            if (n3 >= actionList.size()) {
                break;
            }
            final AccessibilityNodeInfoCompat.AccessibilityActionCompat accessibilityActionCompat = actionList.get(n3);
            if (accessibilityActionCompat.getId() == n) {
                perform = accessibilityActionCompat.perform(view, bundle);
                break;
            }
            ++n3;
        }
        boolean performAccessibilityAction = perform;
        if (!perform) {
            performAccessibilityAction = Api16Impl.performAccessibilityAction(this.mOriginalDelegate, view, n, bundle);
        }
        boolean performClickableSpanAction = performAccessibilityAction;
        if (!performAccessibilityAction) {
            performClickableSpanAction = performAccessibilityAction;
            if (n == R.id.accessibility_action_clickable_span) {
                performClickableSpanAction = performAccessibilityAction;
                if (bundle != null) {
                    performClickableSpanAction = this.performClickableSpanAction(((BaseBundle)bundle).getInt("ACCESSIBILITY_CLICKABLE_SPAN_ID", -1), view);
                }
            }
        }
        return performClickableSpanAction;
    }
    
    public void sendAccessibilityEvent(@NonNull final View view, final int n) {
        this.mOriginalDelegate.sendAccessibilityEvent(view, n);
    }
    
    public void sendAccessibilityEventUnchecked(@NonNull final View view, @NonNull final AccessibilityEvent accessibilityEvent) {
        this.mOriginalDelegate.sendAccessibilityEventUnchecked(view, accessibilityEvent);
    }
    
    static final class AccessibilityDelegateAdapter extends View$AccessibilityDelegate
    {
        final AccessibilityDelegateCompat mCompat;
        
        AccessibilityDelegateAdapter(final AccessibilityDelegateCompat mCompat) {
            this.mCompat = mCompat;
        }
        
        public boolean dispatchPopulateAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
            return this.mCompat.dispatchPopulateAccessibilityEvent(view, accessibilityEvent);
        }
        
        @RequiresApi(16)
        public AccessibilityNodeProvider getAccessibilityNodeProvider(final View view) {
            final AccessibilityNodeProviderCompat accessibilityNodeProvider = this.mCompat.getAccessibilityNodeProvider(view);
            AccessibilityNodeProvider accessibilityNodeProvider2;
            if (accessibilityNodeProvider != null) {
                accessibilityNodeProvider2 = (AccessibilityNodeProvider)accessibilityNodeProvider.getProvider();
            }
            else {
                accessibilityNodeProvider2 = null;
            }
            return accessibilityNodeProvider2;
        }
        
        public void onInitializeAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
            this.mCompat.onInitializeAccessibilityEvent(view, accessibilityEvent);
        }
        
        public void onInitializeAccessibilityNodeInfo(final View view, final AccessibilityNodeInfo accessibilityNodeInfo) {
            final AccessibilityNodeInfoCompat wrap = AccessibilityNodeInfoCompat.wrap(accessibilityNodeInfo);
            wrap.setScreenReaderFocusable(ViewCompat.isScreenReaderFocusable(view));
            wrap.setHeading(ViewCompat.isAccessibilityHeading(view));
            wrap.setPaneTitle(ViewCompat.getAccessibilityPaneTitle(view));
            wrap.setStateDescription(ViewCompat.getStateDescription(view));
            this.mCompat.onInitializeAccessibilityNodeInfo(view, wrap);
            wrap.addSpansToExtras(accessibilityNodeInfo.getText(), view);
            final List<AccessibilityNodeInfoCompat.AccessibilityActionCompat> actionList = AccessibilityDelegateCompat.getActionList(view);
            for (int i = 0; i < actionList.size(); ++i) {
                wrap.addAction((AccessibilityNodeInfoCompat.AccessibilityActionCompat)actionList.get(i));
            }
        }
        
        public void onPopulateAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
            this.mCompat.onPopulateAccessibilityEvent(view, accessibilityEvent);
        }
        
        public boolean onRequestSendAccessibilityEvent(final ViewGroup viewGroup, final View view, final AccessibilityEvent accessibilityEvent) {
            return this.mCompat.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
        }
        
        public boolean performAccessibilityAction(final View view, final int n, final Bundle bundle) {
            return this.mCompat.performAccessibilityAction(view, n, bundle);
        }
        
        public void sendAccessibilityEvent(final View view, final int n) {
            this.mCompat.sendAccessibilityEvent(view, n);
        }
        
        public void sendAccessibilityEventUnchecked(final View view, final AccessibilityEvent accessibilityEvent) {
            this.mCompat.sendAccessibilityEventUnchecked(view, accessibilityEvent);
        }
    }
    
    @RequiresApi(16)
    static class Api16Impl
    {
        private Api16Impl() {
        }
        
        @DoNotInline
        static AccessibilityNodeProvider getAccessibilityNodeProvider(final View$AccessibilityDelegate view$AccessibilityDelegate, final View view) {
            return view$AccessibilityDelegate.getAccessibilityNodeProvider(view);
        }
        
        @DoNotInline
        static boolean performAccessibilityAction(final View$AccessibilityDelegate view$AccessibilityDelegate, final View view, final int n, final Bundle bundle) {
            return view$AccessibilityDelegate.performAccessibilityAction(view, n, bundle);
        }
    }
}

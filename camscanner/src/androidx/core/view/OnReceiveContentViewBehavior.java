// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;

public interface OnReceiveContentViewBehavior
{
    @Nullable
    ContentInfoCompat onReceiveContent(@NonNull final ContentInfoCompat p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.view.MenuItem;
import android.os.Build$VERSION;
import androidx.core.internal.view.SupportMenu;
import androidx.annotation.NonNull;
import android.view.Menu;

public final class MenuCompat
{
    private MenuCompat() {
    }
    
    public static void setGroupDividerEnabled(@NonNull final Menu menu, final boolean groupDividerEnabled) {
        if (menu instanceof SupportMenu) {
            ((SupportMenu)menu).setGroupDividerEnabled(groupDividerEnabled);
        }
        else if (Build$VERSION.SDK_INT >= 28) {
            Api28Impl.setGroupDividerEnabled(menu, groupDividerEnabled);
        }
    }
    
    @Deprecated
    public static void setShowAsAction(final MenuItem menuItem, final int showAsAction) {
        menuItem.setShowAsAction(showAsAction);
    }
    
    @RequiresApi(28)
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        @DoNotInline
        static void setGroupDividerEnabled(final Menu menu, final boolean b) {
            \u3007oOO8O8.\u3007080(menu, b);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.annotation.NonNull;
import android.view.View;

public interface OnApplyWindowInsetsListener
{
    @NonNull
    WindowInsetsCompat onApplyWindowInsets(@NonNull final View p0, @NonNull final WindowInsetsCompat p1);
}

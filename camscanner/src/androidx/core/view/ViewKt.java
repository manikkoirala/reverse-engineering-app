// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.annotation.SuppressLint;
import androidx.annotation.Px;
import androidx.annotation.RequiresApi;
import kotlin.jvm.functions.Function0;
import android.view.ViewGroup$LayoutParams;
import android.view.ViewGroup$MarginLayoutParams;
import android.view.ViewParent;
import kotlin.jvm.functions.Function2;
import kotlin.sequences.SequencesKt;
import kotlin.coroutines.Continuation;
import kotlin.sequences.Sequence;
import android.graphics.Canvas;
import android.graphics.Bitmap;
import android.graphics.Bitmap$Config;
import android.view.View$OnLayoutChangeListener;
import android.view.View$OnAttachStateChangeListener;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import org.jetbrains.annotations.NotNull;
import android.view.View;
import kotlin.Metadata;

@Metadata
public final class ViewKt
{
    public static final void doOnAttach(@NotNull final View view, @NotNull final Function1<? super View, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "action");
        if (ViewCompat.isAttachedToWindow(view)) {
            function1.invoke((Object)view);
        }
        else {
            view.addOnAttachStateChangeListener((View$OnAttachStateChangeListener)new ViewKt$doOnAttach.ViewKt$doOnAttach$1(view, (Function1)function1));
        }
    }
    
    public static final void doOnDetach(@NotNull final View view, @NotNull final Function1<? super View, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "action");
        if (!ViewCompat.isAttachedToWindow(view)) {
            function1.invoke((Object)view);
        }
        else {
            view.addOnAttachStateChangeListener((View$OnAttachStateChangeListener)new ViewKt$doOnDetach.ViewKt$doOnDetach$1(view, (Function1)function1));
        }
    }
    
    public static final void doOnLayout(@NotNull final View view, @NotNull final Function1<? super View, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "action");
        if (ViewCompat.isLaidOut(view) && !view.isLayoutRequested()) {
            function1.invoke((Object)view);
        }
        else {
            view.addOnLayoutChangeListener((View$OnLayoutChangeListener)new ViewKt$doOnLayout$$inlined$doOnNextLayout.ViewKt$doOnLayout$$inlined$doOnNextLayout$1((Function1)function1));
        }
    }
    
    public static final void doOnNextLayout(@NotNull final View view, @NotNull final Function1<? super View, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "action");
        view.addOnLayoutChangeListener((View$OnLayoutChangeListener)new ViewKt$doOnNextLayout.ViewKt$doOnNextLayout$1((Function1)function1));
    }
    
    @NotNull
    public static final OneShotPreDrawListener doOnPreDraw(@NotNull final View view, @NotNull final Function1<? super View, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "action");
        final OneShotPreDrawListener add = OneShotPreDrawListener.add(view, (Runnable)new ViewKt$doOnPreDraw.ViewKt$doOnPreDraw$1((Function1)function1, view));
        Intrinsics.checkNotNullExpressionValue((Object)add, "View.doOnPreDraw(\n    cr\u2026dd(this) { action(this) }");
        return add;
    }
    
    @NotNull
    public static final Bitmap drawToBitmap(@NotNull final View view, @NotNull final Bitmap$Config bitmap$Config) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        Intrinsics.checkNotNullParameter((Object)bitmap$Config, "config");
        if (ViewCompat.isLaidOut(view)) {
            final Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), bitmap$Config);
            Intrinsics.checkNotNullExpressionValue((Object)bitmap, "createBitmap(width, height, config)");
            final Canvas canvas = new Canvas(bitmap);
            canvas.translate(-(float)view.getScrollX(), -(float)view.getScrollY());
            view.draw(canvas);
            return bitmap;
        }
        throw new IllegalStateException("View needs to be laid out before calling drawToBitmap()");
    }
    
    @NotNull
    public static final Sequence<View> getAllViews(@NotNull final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        return (Sequence<View>)SequencesKt.\u3007o00\u3007\u3007Oo((Function2)new ViewKt$allViews.ViewKt$allViews$1(view, (Continuation)null));
    }
    
    @NotNull
    public static final Sequence<ViewParent> getAncestors(@NotNull final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        return (Sequence<ViewParent>)SequencesKt.o\u30070((Object)view.getParent(), (Function1)ViewKt$ancestors.ViewKt$ancestors$1.INSTANCE);
    }
    
    public static final int getMarginBottom(@NotNull final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        final ViewGroup$LayoutParams layoutParams = view.getLayoutParams();
        ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams;
        if (layoutParams instanceof ViewGroup$MarginLayoutParams) {
            viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)layoutParams;
        }
        else {
            viewGroup$MarginLayoutParams = null;
        }
        int bottomMargin;
        if (viewGroup$MarginLayoutParams != null) {
            bottomMargin = viewGroup$MarginLayoutParams.bottomMargin;
        }
        else {
            bottomMargin = 0;
        }
        return bottomMargin;
    }
    
    public static final int getMarginEnd(@NotNull final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        final ViewGroup$LayoutParams layoutParams = view.getLayoutParams();
        int marginEnd;
        if (layoutParams instanceof ViewGroup$MarginLayoutParams) {
            marginEnd = MarginLayoutParamsCompat.getMarginEnd((ViewGroup$MarginLayoutParams)layoutParams);
        }
        else {
            marginEnd = 0;
        }
        return marginEnd;
    }
    
    public static final int getMarginLeft(@NotNull final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        final ViewGroup$LayoutParams layoutParams = view.getLayoutParams();
        ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams;
        if (layoutParams instanceof ViewGroup$MarginLayoutParams) {
            viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)layoutParams;
        }
        else {
            viewGroup$MarginLayoutParams = null;
        }
        int leftMargin;
        if (viewGroup$MarginLayoutParams != null) {
            leftMargin = viewGroup$MarginLayoutParams.leftMargin;
        }
        else {
            leftMargin = 0;
        }
        return leftMargin;
    }
    
    public static final int getMarginRight(@NotNull final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        final ViewGroup$LayoutParams layoutParams = view.getLayoutParams();
        ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams;
        if (layoutParams instanceof ViewGroup$MarginLayoutParams) {
            viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)layoutParams;
        }
        else {
            viewGroup$MarginLayoutParams = null;
        }
        int rightMargin;
        if (viewGroup$MarginLayoutParams != null) {
            rightMargin = viewGroup$MarginLayoutParams.rightMargin;
        }
        else {
            rightMargin = 0;
        }
        return rightMargin;
    }
    
    public static final int getMarginStart(@NotNull final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        final ViewGroup$LayoutParams layoutParams = view.getLayoutParams();
        int marginStart;
        if (layoutParams instanceof ViewGroup$MarginLayoutParams) {
            marginStart = MarginLayoutParamsCompat.getMarginStart((ViewGroup$MarginLayoutParams)layoutParams);
        }
        else {
            marginStart = 0;
        }
        return marginStart;
    }
    
    public static final int getMarginTop(@NotNull final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        final ViewGroup$LayoutParams layoutParams = view.getLayoutParams();
        ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams;
        if (layoutParams instanceof ViewGroup$MarginLayoutParams) {
            viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)layoutParams;
        }
        else {
            viewGroup$MarginLayoutParams = null;
        }
        int topMargin;
        if (viewGroup$MarginLayoutParams != null) {
            topMargin = viewGroup$MarginLayoutParams.topMargin;
        }
        else {
            topMargin = 0;
        }
        return topMargin;
    }
    
    public static final boolean isGone(@NotNull final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        return view.getVisibility() == 8;
    }
    
    public static final boolean isInvisible(@NotNull final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        return view.getVisibility() == 4;
    }
    
    public static final boolean isVisible(@NotNull final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        return view.getVisibility() == 0;
    }
    
    @NotNull
    public static final Runnable postDelayed(@NotNull final View view, final long n, @NotNull final Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        Intrinsics.checkNotNullParameter((Object)function0, "action");
        final ViewKt$postDelayed$runnable.ViewKt$postDelayed$runnable$1 viewKt$postDelayed$runnable$1 = new ViewKt$postDelayed$runnable.ViewKt$postDelayed$runnable$1((Function0)function0);
        view.postDelayed((Runnable)viewKt$postDelayed$runnable$1, n);
        return (Runnable)viewKt$postDelayed$runnable$1;
    }
    
    @RequiresApi(16)
    @NotNull
    public static final Runnable postOnAnimationDelayed(@NotNull final View view, final long n, @NotNull final Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        Intrinsics.checkNotNullParameter((Object)function0, "action");
        final \u30070OO8 \u30070OO8 = new \u30070OO8(function0);
        Api16Impl.postOnAnimationDelayed(view, \u30070OO8, n);
        return \u30070OO8;
    }
    
    private static final void postOnAnimationDelayed$lambda-1(final Function0 function0) {
        Intrinsics.checkNotNullParameter((Object)function0, "$action");
        function0.invoke();
    }
    
    public static final void setGone(@NotNull final View view, final boolean b) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        int visibility;
        if (b) {
            visibility = 8;
        }
        else {
            visibility = 0;
        }
        view.setVisibility(visibility);
    }
    
    public static final void setInvisible(@NotNull final View view, final boolean b) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        int visibility;
        if (b) {
            visibility = 4;
        }
        else {
            visibility = 0;
        }
        view.setVisibility(visibility);
    }
    
    public static final void setPadding(@NotNull final View view, @Px final int n) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        view.setPadding(n, n, n, n);
    }
    
    public static final void setVisible(@NotNull final View view, final boolean b) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        int visibility;
        if (b) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        view.setVisibility(visibility);
    }
    
    public static final void updateLayoutParams(@NotNull final View view, @NotNull final Function1<? super ViewGroup$LayoutParams, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "block");
        final ViewGroup$LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams != null) {
            function1.invoke((Object)layoutParams);
            view.setLayoutParams(layoutParams);
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type android.view.ViewGroup.LayoutParams");
    }
    
    public static final void updatePadding(@NotNull final View view, @Px final int n, @Px final int n2, @Px final int n3, @Px final int n4) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        view.setPadding(n, n2, n3, n4);
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @RequiresApi(17)
    public static final void updatePaddingRelative(@NotNull final View view, @Px final int n, @Px final int n2, @Px final int n3, @Px final int n4) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        view.setPaddingRelative(n, n2, n3, n4);
    }
}

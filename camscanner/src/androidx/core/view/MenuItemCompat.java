// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.view.MenuItem$OnActionExpandListener;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.os.Build$VERSION;
import android.view.View;
import androidx.annotation.Nullable;
import androidx.core.internal.view.SupportMenuItem;
import androidx.annotation.NonNull;
import android.view.MenuItem;

public final class MenuItemCompat
{
    @Deprecated
    public static final int SHOW_AS_ACTION_ALWAYS = 2;
    @Deprecated
    public static final int SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW = 8;
    @Deprecated
    public static final int SHOW_AS_ACTION_IF_ROOM = 1;
    @Deprecated
    public static final int SHOW_AS_ACTION_NEVER = 0;
    @Deprecated
    public static final int SHOW_AS_ACTION_WITH_TEXT = 4;
    private static final String TAG = "MenuItemCompat";
    
    private MenuItemCompat() {
    }
    
    @Deprecated
    public static boolean collapseActionView(final MenuItem menuItem) {
        return menuItem.collapseActionView();
    }
    
    @Deprecated
    public static boolean expandActionView(final MenuItem menuItem) {
        return menuItem.expandActionView();
    }
    
    @Nullable
    public static ActionProvider getActionProvider(@NonNull final MenuItem menuItem) {
        if (menuItem instanceof SupportMenuItem) {
            return ((SupportMenuItem)menuItem).getSupportActionProvider();
        }
        return null;
    }
    
    @Deprecated
    public static View getActionView(final MenuItem menuItem) {
        return menuItem.getActionView();
    }
    
    public static int getAlphabeticModifiers(@NonNull final MenuItem menuItem) {
        if (menuItem instanceof SupportMenuItem) {
            return ((SupportMenuItem)menuItem).getAlphabeticModifiers();
        }
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getAlphabeticModifiers(menuItem);
        }
        return 0;
    }
    
    @Nullable
    public static CharSequence getContentDescription(@NonNull final MenuItem menuItem) {
        if (menuItem instanceof SupportMenuItem) {
            return ((SupportMenuItem)menuItem).getContentDescription();
        }
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getContentDescription(menuItem);
        }
        return null;
    }
    
    @Nullable
    public static ColorStateList getIconTintList(@NonNull final MenuItem menuItem) {
        if (menuItem instanceof SupportMenuItem) {
            return ((SupportMenuItem)menuItem).getIconTintList();
        }
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getIconTintList(menuItem);
        }
        return null;
    }
    
    @Nullable
    public static PorterDuff$Mode getIconTintMode(@NonNull final MenuItem menuItem) {
        if (menuItem instanceof SupportMenuItem) {
            return ((SupportMenuItem)menuItem).getIconTintMode();
        }
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getIconTintMode(menuItem);
        }
        return null;
    }
    
    public static int getNumericModifiers(@NonNull final MenuItem menuItem) {
        if (menuItem instanceof SupportMenuItem) {
            return ((SupportMenuItem)menuItem).getNumericModifiers();
        }
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getNumericModifiers(menuItem);
        }
        return 0;
    }
    
    @Nullable
    public static CharSequence getTooltipText(@NonNull final MenuItem menuItem) {
        if (menuItem instanceof SupportMenuItem) {
            return ((SupportMenuItem)menuItem).getTooltipText();
        }
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getTooltipText(menuItem);
        }
        return null;
    }
    
    @Deprecated
    public static boolean isActionViewExpanded(final MenuItem menuItem) {
        return menuItem.isActionViewExpanded();
    }
    
    @Nullable
    public static MenuItem setActionProvider(@NonNull final MenuItem menuItem, @Nullable final ActionProvider supportActionProvider) {
        Object setSupportActionProvider = menuItem;
        if (menuItem instanceof SupportMenuItem) {
            setSupportActionProvider = ((SupportMenuItem)menuItem).setSupportActionProvider(supportActionProvider);
        }
        return (MenuItem)setSupportActionProvider;
    }
    
    @Deprecated
    public static MenuItem setActionView(final MenuItem menuItem, final int actionView) {
        return menuItem.setActionView(actionView);
    }
    
    @Deprecated
    public static MenuItem setActionView(final MenuItem menuItem, final View actionView) {
        return menuItem.setActionView(actionView);
    }
    
    public static void setAlphabeticShortcut(@NonNull final MenuItem menuItem, final char c, final int n) {
        if (menuItem instanceof SupportMenuItem) {
            ((SupportMenuItem)menuItem).setAlphabeticShortcut(c, n);
        }
        else if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setAlphabeticShortcut(menuItem, c, n);
        }
    }
    
    public static void setContentDescription(@NonNull final MenuItem menuItem, @Nullable final CharSequence contentDescription) {
        if (menuItem instanceof SupportMenuItem) {
            ((SupportMenuItem)menuItem).setContentDescription(contentDescription);
        }
        else if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setContentDescription(menuItem, contentDescription);
        }
    }
    
    public static void setIconTintList(@NonNull final MenuItem menuItem, @Nullable final ColorStateList iconTintList) {
        if (menuItem instanceof SupportMenuItem) {
            ((SupportMenuItem)menuItem).setIconTintList(iconTintList);
        }
        else if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setIconTintList(menuItem, iconTintList);
        }
    }
    
    public static void setIconTintMode(@NonNull final MenuItem menuItem, @Nullable final PorterDuff$Mode iconTintMode) {
        if (menuItem instanceof SupportMenuItem) {
            ((SupportMenuItem)menuItem).setIconTintMode(iconTintMode);
        }
        else if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setIconTintMode(menuItem, iconTintMode);
        }
    }
    
    public static void setNumericShortcut(@NonNull final MenuItem menuItem, final char c, final int n) {
        if (menuItem instanceof SupportMenuItem) {
            ((SupportMenuItem)menuItem).setNumericShortcut(c, n);
        }
        else if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setNumericShortcut(menuItem, c, n);
        }
    }
    
    @Deprecated
    public static MenuItem setOnActionExpandListener(final MenuItem menuItem, final OnActionExpandListener onActionExpandListener) {
        return menuItem.setOnActionExpandListener((MenuItem$OnActionExpandListener)new MenuItem$OnActionExpandListener(onActionExpandListener) {
            final OnActionExpandListener val$listener;
            
            public boolean onMenuItemActionCollapse(final MenuItem menuItem) {
                return this.val$listener.onMenuItemActionCollapse(menuItem);
            }
            
            public boolean onMenuItemActionExpand(final MenuItem menuItem) {
                return this.val$listener.onMenuItemActionExpand(menuItem);
            }
        });
    }
    
    public static void setShortcut(@NonNull final MenuItem menuItem, final char c, final char c2, final int n, final int n2) {
        if (menuItem instanceof SupportMenuItem) {
            ((SupportMenuItem)menuItem).setShortcut(c, c2, n, n2);
        }
        else if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setShortcut(menuItem, c, c2, n, n2);
        }
    }
    
    @Deprecated
    public static void setShowAsAction(final MenuItem menuItem, final int showAsAction) {
        menuItem.setShowAsAction(showAsAction);
    }
    
    public static void setTooltipText(@NonNull final MenuItem menuItem, @Nullable final CharSequence tooltipText) {
        if (menuItem instanceof SupportMenuItem) {
            ((SupportMenuItem)menuItem).setTooltipText(tooltipText);
        }
        else if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setTooltipText(menuItem, tooltipText);
        }
    }
    
    @RequiresApi(26)
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        @DoNotInline
        static int getAlphabeticModifiers(final MenuItem menuItem) {
            return oO.\u3007080(menuItem);
        }
        
        @DoNotInline
        static CharSequence getContentDescription(final MenuItem menuItem) {
            return o0ooO.\u3007080(menuItem);
        }
        
        @DoNotInline
        static ColorStateList getIconTintList(final MenuItem menuItem) {
            return \u300700\u30078.\u3007080(menuItem);
        }
        
        @DoNotInline
        static PorterDuff$Mode getIconTintMode(final MenuItem menuItem) {
            return Oo8Oo00oo.\u3007080(menuItem);
        }
        
        @DoNotInline
        static int getNumericModifiers(final MenuItem menuItem) {
            return \u3007\u3007\u30070\u3007\u30070.\u3007080(menuItem);
        }
        
        @DoNotInline
        static CharSequence getTooltipText(final MenuItem menuItem) {
            return \u3007\u30070o.\u3007080(menuItem);
        }
        
        @DoNotInline
        static MenuItem setAlphabeticShortcut(final MenuItem menuItem, final char c, final int n) {
            return O8\u3007o.\u3007080(menuItem, c, n);
        }
        
        @DoNotInline
        static MenuItem setContentDescription(final MenuItem menuItem, final CharSequence charSequence) {
            return \u3007o.\u3007080(menuItem, charSequence);
        }
        
        @DoNotInline
        static MenuItem setIconTintList(final MenuItem menuItem, final ColorStateList list) {
            return oo\u3007.\u3007080(menuItem, list);
        }
        
        @DoNotInline
        static MenuItem setIconTintMode(final MenuItem menuItem, final PorterDuff$Mode porterDuff$Mode) {
            return o\u30070OOo\u30070.\u3007080(menuItem, porterDuff$Mode);
        }
        
        @DoNotInline
        static MenuItem setNumericShortcut(final MenuItem menuItem, final char c, final int n) {
            return OOO\u3007O0.\u3007080(menuItem, c, n);
        }
        
        @DoNotInline
        static MenuItem setShortcut(final MenuItem menuItem, final char c, final char c2, final int n, final int n2) {
            return o\u30078.\u3007080(menuItem, c, c2, n, n2);
        }
        
        @DoNotInline
        static MenuItem setTooltipText(final MenuItem menuItem, final CharSequence charSequence) {
            return \u300708O8o\u30070.\u3007080(menuItem, charSequence);
        }
    }
    
    @Deprecated
    public interface OnActionExpandListener
    {
        boolean onMenuItemActionCollapse(final MenuItem p0);
        
        boolean onMenuItemActionExpand(final MenuItem p0);
    }
}

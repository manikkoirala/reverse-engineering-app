// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface WindowInsetsAnimationControlListenerCompat
{
    void onCancelled(@Nullable final WindowInsetsAnimationControllerCompat p0);
    
    void onFinished(@NonNull final WindowInsetsAnimationControllerCompat p0);
    
    void onReady(@NonNull final WindowInsetsAnimationControllerCompat p0, final int p1);
}

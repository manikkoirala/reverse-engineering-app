// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import androidx.annotation.NonNull;
import android.view.ViewGroup$MarginLayoutParams;

public final class MarginLayoutParamsCompat
{
    private MarginLayoutParamsCompat() {
    }
    
    public static int getLayoutDirection(@NonNull final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
        int layoutDirection;
        final int n = layoutDirection = Api17Impl.getLayoutDirection(viewGroup$MarginLayoutParams);
        if (n != 0 && (layoutDirection = n) != 1) {
            layoutDirection = 0;
        }
        return layoutDirection;
    }
    
    public static int getMarginEnd(@NonNull final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
        return Api17Impl.getMarginEnd(viewGroup$MarginLayoutParams);
    }
    
    public static int getMarginStart(@NonNull final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
        return Api17Impl.getMarginStart(viewGroup$MarginLayoutParams);
    }
    
    public static boolean isMarginRelative(@NonNull final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
        return Api17Impl.isMarginRelative(viewGroup$MarginLayoutParams);
    }
    
    public static void resolveLayoutDirection(@NonNull final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int n) {
        Api17Impl.resolveLayoutDirection(viewGroup$MarginLayoutParams, n);
    }
    
    public static void setLayoutDirection(@NonNull final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int n) {
        Api17Impl.setLayoutDirection(viewGroup$MarginLayoutParams, n);
    }
    
    public static void setMarginEnd(@NonNull final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int n) {
        Api17Impl.setMarginEnd(viewGroup$MarginLayoutParams, n);
    }
    
    public static void setMarginStart(@NonNull final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int n) {
        Api17Impl.setMarginStart(viewGroup$MarginLayoutParams, n);
    }
    
    @RequiresApi(17)
    static class Api17Impl
    {
        private Api17Impl() {
        }
        
        @DoNotInline
        static int getLayoutDirection(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            return viewGroup$MarginLayoutParams.getLayoutDirection();
        }
        
        @DoNotInline
        static int getMarginEnd(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            return viewGroup$MarginLayoutParams.getMarginEnd();
        }
        
        @DoNotInline
        static int getMarginStart(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            return viewGroup$MarginLayoutParams.getMarginStart();
        }
        
        @DoNotInline
        static boolean isMarginRelative(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            return viewGroup$MarginLayoutParams.isMarginRelative();
        }
        
        @DoNotInline
        static void resolveLayoutDirection(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int n) {
            viewGroup$MarginLayoutParams.resolveLayoutDirection(n);
        }
        
        @DoNotInline
        static void setLayoutDirection(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int layoutDirection) {
            viewGroup$MarginLayoutParams.setLayoutDirection(layoutDirection);
        }
        
        @DoNotInline
        static void setMarginEnd(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int marginEnd) {
            viewGroup$MarginLayoutParams.setMarginEnd(marginEnd);
        }
        
        @DoNotInline
        static void setMarginStart(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int marginStart) {
            viewGroup$MarginLayoutParams.setMarginStart(marginStart);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import android.view.accessibility.AccessibilityRecord;
import android.util.SparseArray;
import java.lang.ref.WeakReference;
import androidx.annotation.RestrictTo;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.view.ContentInfo;
import android.view.WindowInsetsController;
import android.view.View$OnUnhandledKeyEventListener;
import java.util.Objects;
import androidx.collection.SimpleArrayMap;
import android.view.View$OnApplyWindowInsetsListener;
import android.view.ViewTreeObserver;
import androidx.annotation.DoNotInline;
import java.util.Iterator;
import java.util.Map;
import android.view.View$OnAttachStateChangeListener;
import android.view.ViewTreeObserver$OnGlobalLayoutListener;
import android.view.View$DragShadowBuilder;
import android.content.ClipData;
import android.view.PointerIcon;
import java.util.Arrays;
import androidx.core.util.Preconditions;
import android.graphics.Paint;
import android.view.ViewGroup;
import android.graphics.drawable.Drawable;
import androidx.annotation.FloatRange;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import androidx.annotation.IdRes;
import android.util.Log;
import android.os.Bundle;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.Window;
import android.content.Context;
import android.app.Activity;
import android.content.ContextWrapper;
import java.util.Collections;
import androidx.annotation.Px;
import android.graphics.Matrix;
import android.view.Display;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.text.TextUtils;
import java.util.List;
import android.view.accessibility.AccessibilityNodeProvider;
import androidx.core.view.accessibility.AccessibilityNodeProviderCompat;
import android.view.View$AccessibilityDelegate;
import androidx.annotation.UiThread;
import android.view.KeyEvent;
import androidx.annotation.Nullable;
import android.view.WindowInsets;
import android.view.ViewParent;
import java.util.ArrayList;
import android.os.Build$VERSION;
import java.util.Collection;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import androidx.core.view.accessibility.AccessibilityViewCommand;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.R;
import android.view.View;
import java.util.WeakHashMap;
import android.graphics.Rect;
import java.util.concurrent.atomic.AtomicInteger;
import java.lang.reflect.Method;
import java.lang.reflect.Field;
import android.annotation.SuppressLint;

@SuppressLint({ "PrivateConstructorForUtilityClass" })
public class ViewCompat
{
    private static final int[] ACCESSIBILITY_ACTIONS_RESOURCE_IDS;
    public static final int ACCESSIBILITY_LIVE_REGION_ASSERTIVE = 2;
    public static final int ACCESSIBILITY_LIVE_REGION_NONE = 0;
    public static final int ACCESSIBILITY_LIVE_REGION_POLITE = 1;
    public static final int IMPORTANT_FOR_ACCESSIBILITY_AUTO = 0;
    public static final int IMPORTANT_FOR_ACCESSIBILITY_NO = 2;
    public static final int IMPORTANT_FOR_ACCESSIBILITY_NO_HIDE_DESCENDANTS = 4;
    public static final int IMPORTANT_FOR_ACCESSIBILITY_YES = 1;
    @Deprecated
    public static final int LAYER_TYPE_HARDWARE = 2;
    @Deprecated
    public static final int LAYER_TYPE_NONE = 0;
    @Deprecated
    public static final int LAYER_TYPE_SOFTWARE = 1;
    public static final int LAYOUT_DIRECTION_INHERIT = 2;
    public static final int LAYOUT_DIRECTION_LOCALE = 3;
    public static final int LAYOUT_DIRECTION_LTR = 0;
    public static final int LAYOUT_DIRECTION_RTL = 1;
    @Deprecated
    public static final int MEASURED_HEIGHT_STATE_SHIFT = 16;
    @Deprecated
    public static final int MEASURED_SIZE_MASK = 16777215;
    @Deprecated
    public static final int MEASURED_STATE_MASK = -16777216;
    @Deprecated
    public static final int MEASURED_STATE_TOO_SMALL = 16777216;
    private static final OnReceiveContentViewBehavior NO_OP_ON_RECEIVE_CONTENT_VIEW_BEHAVIOR;
    @Deprecated
    public static final int OVER_SCROLL_ALWAYS = 0;
    @Deprecated
    public static final int OVER_SCROLL_IF_CONTENT_SCROLLS = 1;
    @Deprecated
    public static final int OVER_SCROLL_NEVER = 2;
    public static final int SCROLL_AXIS_HORIZONTAL = 1;
    public static final int SCROLL_AXIS_NONE = 0;
    public static final int SCROLL_AXIS_VERTICAL = 2;
    public static final int SCROLL_INDICATOR_BOTTOM = 2;
    public static final int SCROLL_INDICATOR_END = 32;
    public static final int SCROLL_INDICATOR_LEFT = 4;
    public static final int SCROLL_INDICATOR_RIGHT = 8;
    public static final int SCROLL_INDICATOR_START = 16;
    public static final int SCROLL_INDICATOR_TOP = 1;
    private static final String TAG = "ViewCompat";
    public static final int TYPE_NON_TOUCH = 1;
    public static final int TYPE_TOUCH = 0;
    private static boolean sAccessibilityDelegateCheckFailed;
    private static Field sAccessibilityDelegateField;
    private static final AccessibilityPaneVisibilityManager sAccessibilityPaneVisibilityManager;
    private static Method sChildrenDrawingOrderMethod;
    private static Method sDispatchFinishTemporaryDetach;
    private static Method sDispatchStartTemporaryDetach;
    private static Field sMinHeightField;
    private static boolean sMinHeightFieldFetched;
    private static Field sMinWidthField;
    private static boolean sMinWidthFieldFetched;
    private static final AtomicInteger sNextGeneratedId;
    private static boolean sTempDetachBound;
    private static ThreadLocal<Rect> sThreadLocalRect;
    private static WeakHashMap<View, String> sTransitionNameMap;
    private static WeakHashMap<View, ViewPropertyAnimatorCompat> sViewPropertyAnimatorMap;
    
    static {
        sNextGeneratedId = new AtomicInteger(1);
        ViewCompat.sViewPropertyAnimatorMap = null;
        ViewCompat.sAccessibilityDelegateCheckFailed = false;
        ACCESSIBILITY_ACTIONS_RESOURCE_IDS = new int[] { R.id.accessibility_custom_action_0, R.id.accessibility_custom_action_1, R.id.accessibility_custom_action_2, R.id.accessibility_custom_action_3, R.id.accessibility_custom_action_4, R.id.accessibility_custom_action_5, R.id.accessibility_custom_action_6, R.id.accessibility_custom_action_7, R.id.accessibility_custom_action_8, R.id.accessibility_custom_action_9, R.id.accessibility_custom_action_10, R.id.accessibility_custom_action_11, R.id.accessibility_custom_action_12, R.id.accessibility_custom_action_13, R.id.accessibility_custom_action_14, R.id.accessibility_custom_action_15, R.id.accessibility_custom_action_16, R.id.accessibility_custom_action_17, R.id.accessibility_custom_action_18, R.id.accessibility_custom_action_19, R.id.accessibility_custom_action_20, R.id.accessibility_custom_action_21, R.id.accessibility_custom_action_22, R.id.accessibility_custom_action_23, R.id.accessibility_custom_action_24, R.id.accessibility_custom_action_25, R.id.accessibility_custom_action_26, R.id.accessibility_custom_action_27, R.id.accessibility_custom_action_28, R.id.accessibility_custom_action_29, R.id.accessibility_custom_action_30, R.id.accessibility_custom_action_31 };
        NO_OP_ON_RECEIVE_CONTENT_VIEW_BEHAVIOR = new o8oO\u3007();
        sAccessibilityPaneVisibilityManager = new AccessibilityPaneVisibilityManager();
    }
    
    @Deprecated
    protected ViewCompat() {
    }
    
    private static AccessibilityViewProperty<Boolean> accessibilityHeadingProperty() {
        return (AccessibilityViewProperty<Boolean>)new AccessibilityViewProperty<Boolean>(R.id.tag_accessibility_heading, Boolean.class, 28) {
            @RequiresApi(28)
            Boolean frameworkGet(final View view) {
                return Api28Impl.isAccessibilityHeading(view);
            }
            
            @RequiresApi(28)
            void frameworkSet(final View view, final Boolean b) {
                Api28Impl.setAccessibilityHeading(view, b);
            }
            
            boolean shouldUpdate(final Boolean b, final Boolean b2) {
                return ((AccessibilityViewProperty)this).booleanNullToFalseEquals(b, b2) ^ true;
            }
        };
    }
    
    public static int addAccessibilityAction(@NonNull final View view, @NonNull final CharSequence charSequence, @NonNull final AccessibilityViewCommand accessibilityViewCommand) {
        final int availableActionIdFromResources = getAvailableActionIdFromResources(view, charSequence);
        if (availableActionIdFromResources != -1) {
            addAccessibilityAction(view, new AccessibilityNodeInfoCompat.AccessibilityActionCompat(availableActionIdFromResources, charSequence, accessibilityViewCommand));
        }
        return availableActionIdFromResources;
    }
    
    private static void addAccessibilityAction(@NonNull final View view, @NonNull final AccessibilityNodeInfoCompat.AccessibilityActionCompat accessibilityActionCompat) {
        ensureAccessibilityDelegateCompat(view);
        removeActionWithId(accessibilityActionCompat.getId(), view);
        getActionList(view).add(accessibilityActionCompat);
        notifyViewAccessibilityStateChangedIfNeeded(view, 0);
    }
    
    public static void addKeyboardNavigationClusters(@NonNull final View view, @NonNull final Collection<View> collection, final int n) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.addKeyboardNavigationClusters(view, collection, n);
        }
    }
    
    public static void addOnUnhandledKeyEventListener(@NonNull final View view, @NonNull final OnUnhandledKeyEventListenerCompat e) {
        if (Build$VERSION.SDK_INT >= 28) {
            Api28Impl.addOnUnhandledKeyEventListener(view, e);
            return;
        }
        final int tag_unhandled_key_listeners = R.id.tag_unhandled_key_listeners;
        ArrayList list;
        if ((list = (ArrayList)view.getTag(tag_unhandled_key_listeners)) == null) {
            list = new ArrayList();
            view.setTag(tag_unhandled_key_listeners, (Object)list);
        }
        list.add(e);
        if (list.size() == 1) {
            UnhandledKeyEventManager.registerListeningView(view);
        }
    }
    
    @NonNull
    public static ViewPropertyAnimatorCompat animate(@NonNull final View view) {
        if (ViewCompat.sViewPropertyAnimatorMap == null) {
            ViewCompat.sViewPropertyAnimatorMap = new WeakHashMap<View, ViewPropertyAnimatorCompat>();
        }
        ViewPropertyAnimatorCompat value;
        if ((value = ViewCompat.sViewPropertyAnimatorMap.get(view)) == null) {
            value = new ViewPropertyAnimatorCompat(view);
            ViewCompat.sViewPropertyAnimatorMap.put(view, value);
        }
        return value;
    }
    
    private static void bindTempDetach() {
        while (true) {
            try {
                ViewCompat.sDispatchStartTemporaryDetach = View.class.getDeclaredMethod("dispatchStartTemporaryDetach", (Class<?>[])new Class[0]);
                ViewCompat.sDispatchFinishTemporaryDetach = View.class.getDeclaredMethod("dispatchFinishTemporaryDetach", (Class<?>[])new Class[0]);
                ViewCompat.sTempDetachBound = true;
            }
            catch (final NoSuchMethodException ex) {
                continue;
            }
            break;
        }
    }
    
    @Deprecated
    public static boolean canScrollHorizontally(final View view, final int n) {
        return view.canScrollHorizontally(n);
    }
    
    @Deprecated
    public static boolean canScrollVertically(final View view, final int n) {
        return view.canScrollVertically(n);
    }
    
    public static void cancelDragAndDrop(@NonNull final View view) {
        if (Build$VERSION.SDK_INT >= 24) {
            Api24Impl.cancelDragAndDrop(view);
        }
    }
    
    @Deprecated
    public static int combineMeasuredStates(final int n, final int n2) {
        return View.combineMeasuredStates(n, n2);
    }
    
    private static void compatOffsetLeftAndRight(final View view, final int n) {
        view.offsetLeftAndRight(n);
        if (view.getVisibility() == 0) {
            tickleInvalidationFlag(view);
            final ViewParent parent = view.getParent();
            if (parent instanceof View) {
                tickleInvalidationFlag((View)parent);
            }
        }
    }
    
    private static void compatOffsetTopAndBottom(final View view, final int n) {
        view.offsetTopAndBottom(n);
        if (view.getVisibility() == 0) {
            tickleInvalidationFlag(view);
            final ViewParent parent = view.getParent();
            if (parent instanceof View) {
                tickleInvalidationFlag((View)parent);
            }
        }
    }
    
    @NonNull
    public static WindowInsetsCompat computeSystemWindowInsets(@NonNull final View view, @NonNull final WindowInsetsCompat windowInsetsCompat, @NonNull final Rect rect) {
        return Api21Impl.computeSystemWindowInsets(view, windowInsetsCompat, rect);
    }
    
    @NonNull
    public static WindowInsetsCompat dispatchApplyWindowInsets(@NonNull final View view, @NonNull final WindowInsetsCompat windowInsetsCompat) {
        final WindowInsets windowInsets = windowInsetsCompat.toWindowInsets();
        if (windowInsets != null) {
            final WindowInsets dispatchApplyWindowInsets = Api20Impl.dispatchApplyWindowInsets(view, windowInsets);
            if (!dispatchApplyWindowInsets.equals((Object)windowInsets)) {
                return WindowInsetsCompat.toWindowInsetsCompat(dispatchApplyWindowInsets, view);
            }
        }
        return windowInsetsCompat;
    }
    
    public static void dispatchFinishTemporaryDetach(@NonNull final View obj) {
        if (Build$VERSION.SDK_INT >= 24) {
            Api24Impl.dispatchFinishTemporaryDetach(obj);
            return;
        }
        if (!ViewCompat.sTempDetachBound) {
            bindTempDetach();
        }
        final Method sDispatchFinishTemporaryDetach = ViewCompat.sDispatchFinishTemporaryDetach;
        Label_0045: {
            if (sDispatchFinishTemporaryDetach == null) {
                break Label_0045;
            }
            try {
                sDispatchFinishTemporaryDetach.invoke(obj, new Object[0]);
                return;
                obj.onFinishTemporaryDetach();
            }
            catch (final Exception ex) {}
        }
    }
    
    public static boolean dispatchNestedFling(@NonNull final View view, final float n, final float n2, final boolean b) {
        return Api21Impl.dispatchNestedFling(view, n, n2, b);
    }
    
    public static boolean dispatchNestedPreFling(@NonNull final View view, final float n, final float n2) {
        return Api21Impl.dispatchNestedPreFling(view, n, n2);
    }
    
    public static boolean dispatchNestedPreScroll(@NonNull final View view, final int n, final int n2, @Nullable final int[] array, @Nullable final int[] array2) {
        return Api21Impl.dispatchNestedPreScroll(view, n, n2, array, array2);
    }
    
    public static boolean dispatchNestedPreScroll(@NonNull final View view, final int n, final int n2, @Nullable final int[] array, @Nullable final int[] array2, final int n3) {
        if (view instanceof NestedScrollingChild2) {
            return ((NestedScrollingChild2)view).dispatchNestedPreScroll(n, n2, array, array2, n3);
        }
        return n3 == 0 && dispatchNestedPreScroll(view, n, n2, array, array2);
    }
    
    public static void dispatchNestedScroll(@NonNull final View view, final int n, final int n2, final int n3, final int n4, @Nullable final int[] array, final int n5, @NonNull final int[] array2) {
        if (view instanceof NestedScrollingChild3) {
            ((NestedScrollingChild3)view).dispatchNestedScroll(n, n2, n3, n4, array, n5, array2);
        }
        else {
            dispatchNestedScroll(view, n, n2, n3, n4, array, n5);
        }
    }
    
    public static boolean dispatchNestedScroll(@NonNull final View view, final int n, final int n2, final int n3, final int n4, @Nullable final int[] array) {
        return Api21Impl.dispatchNestedScroll(view, n, n2, n3, n4, array);
    }
    
    public static boolean dispatchNestedScroll(@NonNull final View view, final int n, final int n2, final int n3, final int n4, @Nullable final int[] array, final int n5) {
        if (view instanceof NestedScrollingChild2) {
            return ((NestedScrollingChild2)view).dispatchNestedScroll(n, n2, n3, n4, array, n5);
        }
        return n5 == 0 && dispatchNestedScroll(view, n, n2, n3, n4, array);
    }
    
    public static void dispatchStartTemporaryDetach(@NonNull final View obj) {
        if (Build$VERSION.SDK_INT >= 24) {
            Api24Impl.dispatchStartTemporaryDetach(obj);
            return;
        }
        if (!ViewCompat.sTempDetachBound) {
            bindTempDetach();
        }
        final Method sDispatchStartTemporaryDetach = ViewCompat.sDispatchStartTemporaryDetach;
        Label_0045: {
            if (sDispatchStartTemporaryDetach == null) {
                break Label_0045;
            }
            try {
                sDispatchStartTemporaryDetach.invoke(obj, new Object[0]);
                return;
                obj.onStartTemporaryDetach();
            }
            catch (final Exception ex) {}
        }
    }
    
    @UiThread
    static boolean dispatchUnhandledKeyEventBeforeCallback(final View view, final KeyEvent keyEvent) {
        return Build$VERSION.SDK_INT < 28 && UnhandledKeyEventManager.at(view).dispatch(view, keyEvent);
    }
    
    @UiThread
    static boolean dispatchUnhandledKeyEventBeforeHierarchy(final View view, final KeyEvent keyEvent) {
        return Build$VERSION.SDK_INT < 28 && UnhandledKeyEventManager.at(view).preDispatch(keyEvent);
    }
    
    public static void enableAccessibleClickableSpanSupport(@NonNull final View view) {
        ensureAccessibilityDelegateCompat(view);
    }
    
    static void ensureAccessibilityDelegateCompat(@NonNull final View view) {
        AccessibilityDelegateCompat accessibilityDelegate;
        if ((accessibilityDelegate = getAccessibilityDelegate(view)) == null) {
            accessibilityDelegate = new AccessibilityDelegateCompat();
        }
        setAccessibilityDelegate(view, accessibilityDelegate);
    }
    
    public static int generateViewId() {
        return Api17Impl.generateViewId();
    }
    
    @Nullable
    public static AccessibilityDelegateCompat getAccessibilityDelegate(@NonNull final View view) {
        final View$AccessibilityDelegate accessibilityDelegateInternal = getAccessibilityDelegateInternal(view);
        if (accessibilityDelegateInternal == null) {
            return null;
        }
        if (accessibilityDelegateInternal instanceof AccessibilityDelegateCompat.AccessibilityDelegateAdapter) {
            return ((AccessibilityDelegateCompat.AccessibilityDelegateAdapter)accessibilityDelegateInternal).mCompat;
        }
        return new AccessibilityDelegateCompat(accessibilityDelegateInternal);
    }
    
    @Nullable
    private static View$AccessibilityDelegate getAccessibilityDelegateInternal(@NonNull final View view) {
        if (Build$VERSION.SDK_INT >= 29) {
            return Api29Impl.getAccessibilityDelegate(view);
        }
        return getAccessibilityDelegateThroughReflection(view);
    }
    
    @Nullable
    private static View$AccessibilityDelegate getAccessibilityDelegateThroughReflection(@NonNull final View obj) {
        if (ViewCompat.sAccessibilityDelegateCheckFailed) {
            return null;
        }
        if (ViewCompat.sAccessibilityDelegateField == null) {
            try {
                (ViewCompat.sAccessibilityDelegateField = View.class.getDeclaredField("mAccessibilityDelegate")).setAccessible(true);
            }
            finally {
                ViewCompat.sAccessibilityDelegateCheckFailed = true;
                return null;
            }
        }
        try {
            final Object value = ViewCompat.sAccessibilityDelegateField.get(obj);
            if (value instanceof View$AccessibilityDelegate) {
                return (View$AccessibilityDelegate)value;
            }
            return null;
        }
        finally {
            ViewCompat.sAccessibilityDelegateCheckFailed = true;
            return null;
        }
    }
    
    public static int getAccessibilityLiveRegion(@NonNull final View view) {
        return Api19Impl.getAccessibilityLiveRegion(view);
    }
    
    @Nullable
    public static AccessibilityNodeProviderCompat getAccessibilityNodeProvider(@NonNull final View view) {
        final AccessibilityNodeProvider accessibilityNodeProvider = Api16Impl.getAccessibilityNodeProvider(view);
        if (accessibilityNodeProvider != null) {
            return new AccessibilityNodeProviderCompat(accessibilityNodeProvider);
        }
        return null;
    }
    
    @Nullable
    @UiThread
    public static CharSequence getAccessibilityPaneTitle(@NonNull final View view) {
        return paneTitleProperty().get(view);
    }
    
    private static List<AccessibilityNodeInfoCompat.AccessibilityActionCompat> getActionList(final View view) {
        final int tag_accessibility_actions = R.id.tag_accessibility_actions;
        ArrayList list;
        if ((list = (ArrayList)view.getTag(tag_accessibility_actions)) == null) {
            list = new ArrayList();
            view.setTag(tag_accessibility_actions, (Object)list);
        }
        return list;
    }
    
    @Deprecated
    public static float getAlpha(final View view) {
        return view.getAlpha();
    }
    
    private static int getAvailableActionIdFromResources(final View view, @NonNull final CharSequence charSequence) {
        final List<AccessibilityNodeInfoCompat.AccessibilityActionCompat> actionList = getActionList(view);
        for (int i = 0; i < actionList.size(); ++i) {
            if (TextUtils.equals(charSequence, ((AccessibilityNodeInfoCompat.AccessibilityActionCompat)actionList.get(i)).getLabel())) {
                return ((AccessibilityNodeInfoCompat.AccessibilityActionCompat)actionList.get(i)).getId();
            }
        }
        int n = 0;
        int n2 = -1;
        while (true) {
            final int[] accessibility_ACTIONS_RESOURCE_IDS = ViewCompat.ACCESSIBILITY_ACTIONS_RESOURCE_IDS;
            if (n >= accessibility_ACTIONS_RESOURCE_IDS.length || n2 != -1) {
                break;
            }
            final int n3 = accessibility_ACTIONS_RESOURCE_IDS[n];
            int j = 0;
            boolean b = true;
            while (j < actionList.size()) {
                b &= (((AccessibilityNodeInfoCompat.AccessibilityActionCompat)actionList.get(j)).getId() != n3);
                ++j;
            }
            if (b) {
                n2 = n3;
            }
            ++n;
        }
        return n2;
    }
    
    @Nullable
    public static ColorStateList getBackgroundTintList(@NonNull final View view) {
        return Api21Impl.getBackgroundTintList(view);
    }
    
    @Nullable
    public static PorterDuff$Mode getBackgroundTintMode(@NonNull final View view) {
        return Api21Impl.getBackgroundTintMode(view);
    }
    
    @Nullable
    public static Rect getClipBounds(@NonNull final View view) {
        return Api18Impl.getClipBounds(view);
    }
    
    @Nullable
    public static Display getDisplay(@NonNull final View view) {
        return Api17Impl.getDisplay(view);
    }
    
    public static float getElevation(@NonNull final View view) {
        return Api21Impl.getElevation(view);
    }
    
    private static Rect getEmptyTempRect() {
        if (ViewCompat.sThreadLocalRect == null) {
            ViewCompat.sThreadLocalRect = new ThreadLocal<Rect>();
        }
        Rect value;
        if ((value = ViewCompat.sThreadLocalRect.get()) == null) {
            value = new Rect();
            ViewCompat.sThreadLocalRect.set(value);
        }
        value.setEmpty();
        return value;
    }
    
    private static OnReceiveContentViewBehavior getFallback(@NonNull final View view) {
        if (view instanceof OnReceiveContentViewBehavior) {
            return (OnReceiveContentViewBehavior)view;
        }
        return ViewCompat.NO_OP_ON_RECEIVE_CONTENT_VIEW_BEHAVIOR;
    }
    
    public static boolean getFitsSystemWindows(@NonNull final View view) {
        return Api16Impl.getFitsSystemWindows(view);
    }
    
    public static int getImportantForAccessibility(@NonNull final View view) {
        return Api16Impl.getImportantForAccessibility(view);
    }
    
    @SuppressLint({ "InlinedApi" })
    public static int getImportantForAutofill(@NonNull final View view) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getImportantForAutofill(view);
        }
        return 0;
    }
    
    public static int getLabelFor(@NonNull final View view) {
        return Api17Impl.getLabelFor(view);
    }
    
    @Deprecated
    public static int getLayerType(final View view) {
        return view.getLayerType();
    }
    
    public static int getLayoutDirection(@NonNull final View view) {
        return Api17Impl.getLayoutDirection(view);
    }
    
    @Deprecated
    @Nullable
    public static Matrix getMatrix(final View view) {
        return view.getMatrix();
    }
    
    @Deprecated
    public static int getMeasuredHeightAndState(final View view) {
        return view.getMeasuredHeightAndState();
    }
    
    @Deprecated
    public static int getMeasuredState(final View view) {
        return view.getMeasuredState();
    }
    
    @Deprecated
    public static int getMeasuredWidthAndState(final View view) {
        return view.getMeasuredWidthAndState();
    }
    
    public static int getMinimumHeight(@NonNull final View view) {
        return Api16Impl.getMinimumHeight(view);
    }
    
    public static int getMinimumWidth(@NonNull final View view) {
        return Api16Impl.getMinimumWidth(view);
    }
    
    public static int getNextClusterForwardId(@NonNull final View view) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.getNextClusterForwardId(view);
        }
        return -1;
    }
    
    @Nullable
    public static String[] getOnReceiveContentMimeTypes(@NonNull final View view) {
        if (Build$VERSION.SDK_INT >= 31) {
            return Api31Impl.getReceiveContentMimeTypes(view);
        }
        return (String[])view.getTag(R.id.tag_on_receive_content_mime_types);
    }
    
    @Deprecated
    public static int getOverScrollMode(final View view) {
        return view.getOverScrollMode();
    }
    
    @Px
    public static int getPaddingEnd(@NonNull final View view) {
        return Api17Impl.getPaddingEnd(view);
    }
    
    @Px
    public static int getPaddingStart(@NonNull final View view) {
        return Api17Impl.getPaddingStart(view);
    }
    
    @Nullable
    public static ViewParent getParentForAccessibility(@NonNull final View view) {
        return Api16Impl.getParentForAccessibility(view);
    }
    
    @Deprecated
    public static float getPivotX(final View view) {
        return view.getPivotX();
    }
    
    @Deprecated
    public static float getPivotY(final View view) {
        return view.getPivotY();
    }
    
    @Nullable
    public static WindowInsetsCompat getRootWindowInsets(@NonNull final View view) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getRootWindowInsets(view);
        }
        return Api21Impl.getRootWindowInsets(view);
    }
    
    @Deprecated
    public static float getRotation(final View view) {
        return view.getRotation();
    }
    
    @Deprecated
    public static float getRotationX(final View view) {
        return view.getRotationX();
    }
    
    @Deprecated
    public static float getRotationY(final View view) {
        return view.getRotationY();
    }
    
    @Deprecated
    public static float getScaleX(final View view) {
        return view.getScaleX();
    }
    
    @Deprecated
    public static float getScaleY(final View view) {
        return view.getScaleY();
    }
    
    public static int getScrollIndicators(@NonNull final View view) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.getScrollIndicators(view);
        }
        return 0;
    }
    
    @Nullable
    @UiThread
    public static CharSequence getStateDescription(@NonNull final View view) {
        return stateDescriptionProperty().get(view);
    }
    
    @NonNull
    public static List<Rect> getSystemGestureExclusionRects(@NonNull final View view) {
        if (Build$VERSION.SDK_INT >= 29) {
            return Api29Impl.getSystemGestureExclusionRects(view);
        }
        return Collections.emptyList();
    }
    
    @Nullable
    public static String getTransitionName(@NonNull final View view) {
        return Api21Impl.getTransitionName(view);
    }
    
    @Deprecated
    public static float getTranslationX(final View view) {
        return view.getTranslationX();
    }
    
    @Deprecated
    public static float getTranslationY(final View view) {
        return view.getTranslationY();
    }
    
    public static float getTranslationZ(@NonNull final View view) {
        return Api21Impl.getTranslationZ(view);
    }
    
    @Deprecated
    @Nullable
    public static WindowInsetsControllerCompat getWindowInsetsController(@NonNull final View view) {
        if (Build$VERSION.SDK_INT >= 30) {
            return Api30Impl.getWindowInsetsController(view);
        }
        Context context = view.getContext();
        while (true) {
            final boolean b = context instanceof ContextWrapper;
            final WindowInsetsControllerCompat windowInsetsControllerCompat = null;
            if (!b) {
                return null;
            }
            if (context instanceof Activity) {
                final Window window = ((Activity)context).getWindow();
                WindowInsetsControllerCompat insetsController = windowInsetsControllerCompat;
                if (window != null) {
                    insetsController = WindowCompat.getInsetsController(window, view);
                }
                return insetsController;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
    }
    
    @Deprecated
    public static int getWindowSystemUiVisibility(@NonNull final View view) {
        return Api16Impl.getWindowSystemUiVisibility(view);
    }
    
    @Deprecated
    public static float getX(final View view) {
        return view.getX();
    }
    
    @Deprecated
    public static float getY(final View view) {
        return view.getY();
    }
    
    public static float getZ(@NonNull final View view) {
        return Api21Impl.getZ(view);
    }
    
    public static boolean hasAccessibilityDelegate(@NonNull final View view) {
        return getAccessibilityDelegateInternal(view) != null;
    }
    
    public static boolean hasExplicitFocusable(@NonNull final View view) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.hasExplicitFocusable(view);
        }
        return view.hasFocusable();
    }
    
    public static boolean hasNestedScrollingParent(@NonNull final View view) {
        return Api21Impl.hasNestedScrollingParent(view);
    }
    
    public static boolean hasNestedScrollingParent(@NonNull final View view, final int n) {
        if (view instanceof NestedScrollingChild2) {
            ((NestedScrollingChild2)view).hasNestedScrollingParent(n);
        }
        else if (n == 0) {
            return hasNestedScrollingParent(view);
        }
        return false;
    }
    
    public static boolean hasOnClickListeners(@NonNull final View view) {
        return Api15Impl.hasOnClickListeners(view);
    }
    
    public static boolean hasOverlappingRendering(@NonNull final View view) {
        return Api16Impl.hasOverlappingRendering(view);
    }
    
    public static boolean hasTransientState(@NonNull final View view) {
        return Api16Impl.hasTransientState(view);
    }
    
    @UiThread
    public static boolean isAccessibilityHeading(@NonNull final View view) {
        final Boolean b = accessibilityHeadingProperty().get(view);
        return b != null && b;
    }
    
    public static boolean isAttachedToWindow(@NonNull final View view) {
        return Api19Impl.isAttachedToWindow(view);
    }
    
    public static boolean isFocusedByDefault(@NonNull final View view) {
        return Build$VERSION.SDK_INT >= 26 && Api26Impl.isFocusedByDefault(view);
    }
    
    public static boolean isImportantForAccessibility(@NonNull final View view) {
        return Api21Impl.isImportantForAccessibility(view);
    }
    
    public static boolean isImportantForAutofill(@NonNull final View view) {
        return Build$VERSION.SDK_INT < 26 || Api26Impl.isImportantForAutofill(view);
    }
    
    public static boolean isInLayout(@NonNull final View view) {
        return Api18Impl.isInLayout(view);
    }
    
    public static boolean isKeyboardNavigationCluster(@NonNull final View view) {
        return Build$VERSION.SDK_INT >= 26 && Api26Impl.isKeyboardNavigationCluster(view);
    }
    
    public static boolean isLaidOut(@NonNull final View view) {
        return Api19Impl.isLaidOut(view);
    }
    
    public static boolean isLayoutDirectionResolved(@NonNull final View view) {
        return Api19Impl.isLayoutDirectionResolved(view);
    }
    
    public static boolean isNestedScrollingEnabled(@NonNull final View view) {
        return Api21Impl.isNestedScrollingEnabled(view);
    }
    
    @Deprecated
    public static boolean isOpaque(final View view) {
        return view.isOpaque();
    }
    
    public static boolean isPaddingRelative(@NonNull final View view) {
        return Api17Impl.isPaddingRelative(view);
    }
    
    @UiThread
    public static boolean isScreenReaderFocusable(@NonNull final View view) {
        final Boolean b = screenReaderFocusableProperty().get(view);
        return b != null && b;
    }
    
    @Deprecated
    public static void jumpDrawablesToCurrentState(final View view) {
        view.jumpDrawablesToCurrentState();
    }
    
    @Nullable
    public static View keyboardNavigationClusterSearch(@NonNull final View view, @Nullable final View view2, final int n) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.keyboardNavigationClusterSearch(view, view2, n);
        }
        return null;
    }
    
    @RequiresApi(19)
    static void notifyViewAccessibilityStateChangedIfNeeded(final View view, final int n) {
        final AccessibilityManager accessibilityManager = (AccessibilityManager)view.getContext().getSystemService("accessibility");
        if (!accessibilityManager.isEnabled()) {
            return;
        }
        final boolean b = getAccessibilityPaneTitle(view) != null && view.isShown() && view.getWindowVisibility() == 0;
        final int accessibilityLiveRegion = getAccessibilityLiveRegion(view);
        int eventType = 32;
        if (accessibilityLiveRegion == 0 && !b) {
            if (n == 32) {
                final AccessibilityEvent obtain = AccessibilityEvent.obtain();
                view.onInitializeAccessibilityEvent(obtain);
                obtain.setEventType(32);
                Api19Impl.setContentChangeTypes(obtain, n);
                ((AccessibilityRecord)obtain).setSource(view);
                view.onPopulateAccessibilityEvent(obtain);
                ((AccessibilityRecord)obtain).getText().add(getAccessibilityPaneTitle(view));
                accessibilityManager.sendAccessibilityEvent(obtain);
            }
            else if (view.getParent() != null) {
                final ViewParent parent = view.getParent();
                try {
                    Api19Impl.notifySubtreeAccessibilityStateChanged(parent, view, view, n);
                }
                catch (final AbstractMethodError abstractMethodError) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(view.getParent().getClass().getSimpleName());
                    sb.append(" does not fully implement ViewParent");
                }
            }
        }
        else {
            final AccessibilityEvent obtain2 = AccessibilityEvent.obtain();
            if (!b) {
                eventType = 2048;
            }
            obtain2.setEventType(eventType);
            Api19Impl.setContentChangeTypes(obtain2, n);
            if (b) {
                ((AccessibilityRecord)obtain2).getText().add(getAccessibilityPaneTitle(view));
                setViewImportanceForAccessibilityIfNeeded(view);
            }
            view.sendAccessibilityEventUnchecked(obtain2);
        }
    }
    
    public static void offsetLeftAndRight(@NonNull final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 23) {
            view.offsetLeftAndRight(n);
        }
        else {
            final Rect emptyTempRect = getEmptyTempRect();
            final ViewParent parent = view.getParent();
            boolean b;
            if (parent instanceof View) {
                final View view2 = (View)parent;
                emptyTempRect.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
                b = (emptyTempRect.intersects(view.getLeft(), view.getTop(), view.getRight(), view.getBottom()) ^ true);
            }
            else {
                b = false;
            }
            compatOffsetLeftAndRight(view, n);
            if (b && emptyTempRect.intersect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())) {
                ((View)parent).invalidate(emptyTempRect);
            }
        }
    }
    
    public static void offsetTopAndBottom(@NonNull final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 23) {
            view.offsetTopAndBottom(n);
        }
        else {
            final Rect emptyTempRect = getEmptyTempRect();
            final ViewParent parent = view.getParent();
            boolean b;
            if (parent instanceof View) {
                final View view2 = (View)parent;
                emptyTempRect.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
                b = (emptyTempRect.intersects(view.getLeft(), view.getTop(), view.getRight(), view.getBottom()) ^ true);
            }
            else {
                b = false;
            }
            compatOffsetTopAndBottom(view, n);
            if (b && emptyTempRect.intersect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())) {
                ((View)parent).invalidate(emptyTempRect);
            }
        }
    }
    
    @NonNull
    public static WindowInsetsCompat onApplyWindowInsets(@NonNull final View view, @NonNull final WindowInsetsCompat windowInsetsCompat) {
        final WindowInsets windowInsets = windowInsetsCompat.toWindowInsets();
        if (windowInsets != null) {
            final WindowInsets onApplyWindowInsets = Api20Impl.onApplyWindowInsets(view, windowInsets);
            if (!onApplyWindowInsets.equals((Object)windowInsets)) {
                return WindowInsetsCompat.toWindowInsetsCompat(onApplyWindowInsets, view);
            }
        }
        return windowInsetsCompat;
    }
    
    @Deprecated
    public static void onInitializeAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
        view.onInitializeAccessibilityEvent(accessibilityEvent);
    }
    
    public static void onInitializeAccessibilityNodeInfo(@NonNull final View view, @NonNull final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        view.onInitializeAccessibilityNodeInfo(accessibilityNodeInfoCompat.unwrap());
    }
    
    @Deprecated
    public static void onPopulateAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
        view.onPopulateAccessibilityEvent(accessibilityEvent);
    }
    
    private static AccessibilityViewProperty<CharSequence> paneTitleProperty() {
        return (AccessibilityViewProperty<CharSequence>)new AccessibilityViewProperty<CharSequence>(R.id.tag_accessibility_pane_title, CharSequence.class, 8, 28) {
            @RequiresApi(28)
            CharSequence frameworkGet(final View view) {
                return Api28Impl.getAccessibilityPaneTitle(view);
            }
            
            @RequiresApi(28)
            void frameworkSet(final View view, final CharSequence charSequence) {
                Api28Impl.setAccessibilityPaneTitle(view, charSequence);
            }
            
            boolean shouldUpdate(final CharSequence charSequence, final CharSequence charSequence2) {
                return TextUtils.equals(charSequence, charSequence2) ^ true;
            }
        };
    }
    
    public static boolean performAccessibilityAction(@NonNull final View view, final int n, @Nullable final Bundle bundle) {
        return Api16Impl.performAccessibilityAction(view, n, bundle);
    }
    
    @Nullable
    public static ContentInfoCompat performReceiveContent(@NonNull final View view, @NonNull ContentInfoCompat onReceiveContent) {
        if (Log.isLoggable("ViewCompat", 3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("performReceiveContent: ");
            sb.append(onReceiveContent);
            sb.append(", view=");
            sb.append(view.getClass().getSimpleName());
            sb.append("[");
            sb.append(view.getId());
            sb.append("]");
        }
        if (Build$VERSION.SDK_INT >= 31) {
            return Api31Impl.performReceiveContent(view, onReceiveContent);
        }
        final OnReceiveContentListener onReceiveContentListener = (OnReceiveContentListener)view.getTag(R.id.tag_on_receive_content_listener);
        if (onReceiveContentListener != null) {
            onReceiveContent = onReceiveContentListener.onReceiveContent(view, onReceiveContent);
            ContentInfoCompat onReceiveContent2;
            if (onReceiveContent == null) {
                onReceiveContent2 = null;
            }
            else {
                onReceiveContent2 = getFallback(view).onReceiveContent(onReceiveContent);
            }
            return onReceiveContent2;
        }
        return getFallback(view).onReceiveContent(onReceiveContent);
    }
    
    public static void postInvalidateOnAnimation(@NonNull final View view) {
        Api16Impl.postInvalidateOnAnimation(view);
    }
    
    public static void postInvalidateOnAnimation(@NonNull final View view, final int n, final int n2, final int n3, final int n4) {
        Api16Impl.postInvalidateOnAnimation(view, n, n2, n3, n4);
    }
    
    public static void postOnAnimation(@NonNull final View view, @NonNull final Runnable runnable) {
        Api16Impl.postOnAnimation(view, runnable);
    }
    
    @SuppressLint({ "LambdaLast" })
    public static void postOnAnimationDelayed(@NonNull final View view, @NonNull final Runnable runnable, final long n) {
        Api16Impl.postOnAnimationDelayed(view, runnable, n);
    }
    
    public static void removeAccessibilityAction(@NonNull final View view, final int n) {
        removeActionWithId(n, view);
        notifyViewAccessibilityStateChangedIfNeeded(view, 0);
    }
    
    private static void removeActionWithId(final int n, final View view) {
        final List<AccessibilityNodeInfoCompat.AccessibilityActionCompat> actionList = getActionList(view);
        for (int i = 0; i < actionList.size(); ++i) {
            if (((AccessibilityNodeInfoCompat.AccessibilityActionCompat)actionList.get(i)).getId() == n) {
                actionList.remove(i);
                break;
            }
        }
    }
    
    public static void removeOnUnhandledKeyEventListener(@NonNull final View view, @NonNull final OnUnhandledKeyEventListenerCompat o) {
        if (Build$VERSION.SDK_INT >= 28) {
            Api28Impl.removeOnUnhandledKeyEventListener(view, o);
            return;
        }
        final ArrayList list = (ArrayList)view.getTag(R.id.tag_unhandled_key_listeners);
        if (list != null) {
            list.remove(o);
            if (list.size() == 0) {
                UnhandledKeyEventManager.unregisterListeningView(view);
            }
        }
    }
    
    public static void replaceAccessibilityAction(@NonNull final View view, @NonNull final AccessibilityNodeInfoCompat.AccessibilityActionCompat accessibilityActionCompat, @Nullable final CharSequence charSequence, @Nullable final AccessibilityViewCommand accessibilityViewCommand) {
        if (accessibilityViewCommand == null && charSequence == null) {
            removeAccessibilityAction(view, accessibilityActionCompat.getId());
        }
        else {
            addAccessibilityAction(view, accessibilityActionCompat.createReplacementAction(charSequence, accessibilityViewCommand));
        }
    }
    
    public static void requestApplyInsets(@NonNull final View view) {
        Api20Impl.requestApplyInsets(view);
    }
    
    @NonNull
    public static <T extends View> T requireViewById(@NonNull View viewById, @IdRes final int n) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Api28Impl.requireViewById(viewById, n);
        }
        viewById = viewById.findViewById(n);
        if (viewById != null) {
            return (T)viewById;
        }
        throw new IllegalArgumentException("ID does not reference a View inside this View");
    }
    
    @Deprecated
    public static int resolveSizeAndState(final int n, final int n2, final int n3) {
        return View.resolveSizeAndState(n, n2, n3);
    }
    
    public static boolean restoreDefaultFocus(@NonNull final View view) {
        if (Build$VERSION.SDK_INT >= 26) {
            return Api26Impl.restoreDefaultFocus(view);
        }
        return view.requestFocus();
    }
    
    public static void saveAttributeDataForStyleable(@NonNull final View view, @SuppressLint({ "ContextFirst" }) @NonNull final Context context, @NonNull final int[] array, @Nullable final AttributeSet set, @NonNull final TypedArray typedArray, final int n, final int n2) {
        if (Build$VERSION.SDK_INT >= 29) {
            Api29Impl.saveAttributeDataForStyleable(view, context, array, set, typedArray, n, n2);
        }
    }
    
    private static AccessibilityViewProperty<Boolean> screenReaderFocusableProperty() {
        return (AccessibilityViewProperty<Boolean>)new AccessibilityViewProperty<Boolean>(R.id.tag_screen_reader_focusable, Boolean.class, 28) {
            @RequiresApi(28)
            Boolean frameworkGet(@NonNull final View view) {
                return Api28Impl.isScreenReaderFocusable(view);
            }
            
            @RequiresApi(28)
            void frameworkSet(@NonNull final View view, final Boolean b) {
                Api28Impl.setScreenReaderFocusable(view, b);
            }
            
            boolean shouldUpdate(final Boolean b, final Boolean b2) {
                return ((AccessibilityViewProperty)this).booleanNullToFalseEquals(b, b2) ^ true;
            }
        };
    }
    
    public static void setAccessibilityDelegate(@NonNull final View view, @Nullable final AccessibilityDelegateCompat accessibilityDelegateCompat) {
        AccessibilityDelegateCompat accessibilityDelegateCompat2 = accessibilityDelegateCompat;
        if (accessibilityDelegateCompat == null) {
            accessibilityDelegateCompat2 = accessibilityDelegateCompat;
            if (getAccessibilityDelegateInternal(view) instanceof AccessibilityDelegateCompat.AccessibilityDelegateAdapter) {
                accessibilityDelegateCompat2 = new AccessibilityDelegateCompat();
            }
        }
        View$AccessibilityDelegate bridge;
        if (accessibilityDelegateCompat2 == null) {
            bridge = null;
        }
        else {
            bridge = accessibilityDelegateCompat2.getBridge();
        }
        view.setAccessibilityDelegate(bridge);
    }
    
    @UiThread
    public static void setAccessibilityHeading(@NonNull final View view, final boolean b) {
        accessibilityHeadingProperty().set(view, b);
    }
    
    public static void setAccessibilityLiveRegion(@NonNull final View view, final int n) {
        Api19Impl.setAccessibilityLiveRegion(view, n);
    }
    
    @UiThread
    public static void setAccessibilityPaneTitle(@NonNull final View view, @Nullable final CharSequence charSequence) {
        paneTitleProperty().set(view, charSequence);
        if (charSequence != null) {
            ViewCompat.sAccessibilityPaneVisibilityManager.addAccessibilityPane(view);
        }
        else {
            ViewCompat.sAccessibilityPaneVisibilityManager.removeAccessibilityPane(view);
        }
    }
    
    @Deprecated
    public static void setActivated(final View view, final boolean activated) {
        view.setActivated(activated);
    }
    
    @Deprecated
    public static void setAlpha(final View view, @FloatRange(from = 0.0, to = 1.0) final float alpha) {
        view.setAlpha(alpha);
    }
    
    public static void setAutofillHints(@NonNull final View view, @Nullable final String... array) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setAutofillHints(view, array);
        }
    }
    
    public static void setBackground(@NonNull final View view, @Nullable final Drawable drawable) {
        Api16Impl.setBackground(view, drawable);
    }
    
    public static void setBackgroundTintList(@NonNull final View view, @Nullable final ColorStateList list) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        Api21Impl.setBackgroundTintList(view, list);
        if (sdk_INT == 21) {
            final Drawable background = view.getBackground();
            final boolean b = Api21Impl.getBackgroundTintList(view) != null || Api21Impl.getBackgroundTintMode(view) != null;
            if (background != null && b) {
                if (background.isStateful()) {
                    background.setState(view.getDrawableState());
                }
                Api16Impl.setBackground(view, background);
            }
        }
    }
    
    public static void setBackgroundTintMode(@NonNull final View view, @Nullable final PorterDuff$Mode porterDuff$Mode) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        Api21Impl.setBackgroundTintMode(view, porterDuff$Mode);
        if (sdk_INT == 21) {
            final Drawable background = view.getBackground();
            final boolean b = Api21Impl.getBackgroundTintList(view) != null || Api21Impl.getBackgroundTintMode(view) != null;
            if (background != null && b) {
                if (background.isStateful()) {
                    background.setState(view.getDrawableState());
                }
                Api16Impl.setBackground(view, background);
            }
        }
    }
    
    @Deprecated
    @SuppressLint({ "BanUncheckedReflection" })
    public static void setChildrenDrawingOrderEnabled(final ViewGroup p0, final boolean p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: ifnonnull       35
        //     6: ldc_w           Landroid/view/ViewGroup;.class
        //     9: ldc_w           "setChildrenDrawingOrderEnabled"
        //    12: iconst_1       
        //    13: anewarray       Ljava/lang/Class;
        //    16: dup            
        //    17: iconst_0       
        //    18: getstatic       java/lang/Boolean.TYPE:Ljava/lang/Class;
        //    21: aastore        
        //    22: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //    25: putstatic       androidx/core/view/ViewCompat.sChildrenDrawingOrderMethod:Ljava/lang/reflect/Method;
        //    28: getstatic       androidx/core/view/ViewCompat.sChildrenDrawingOrderMethod:Ljava/lang/reflect/Method;
        //    31: iconst_1       
        //    32: invokevirtual   java/lang/reflect/AccessibleObject.setAccessible:(Z)V
        //    35: getstatic       androidx/core/view/ViewCompat.sChildrenDrawingOrderMethod:Ljava/lang/reflect/Method;
        //    38: aload_0        
        //    39: iconst_1       
        //    40: anewarray       Ljava/lang/Object;
        //    43: dup            
        //    44: iconst_0       
        //    45: iload_1        
        //    46: invokestatic    java/lang/Boolean.valueOf:(Z)Ljava/lang/Boolean;
        //    49: aastore        
        //    50: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //    53: pop            
        //    54: return         
        //    55: astore_2       
        //    56: goto            28
        //    59: astore_0       
        //    60: goto            54
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                         
        //  -----  -----  -----  -----  ---------------------------------------------
        //  6      28     55     59     Ljava/lang/NoSuchMethodException;
        //  35     54     59     63     Ljava/lang/IllegalAccessException;
        //  35     54     59     63     Ljava/lang/IllegalArgumentException;
        //  35     54     59     63     Ljava/lang/reflect/InvocationTargetException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0035:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static void setClipBounds(@NonNull final View view, @Nullable final Rect rect) {
        Api18Impl.setClipBounds(view, rect);
    }
    
    public static void setElevation(@NonNull final View view, final float n) {
        Api21Impl.setElevation(view, n);
    }
    
    @Deprecated
    public static void setFitsSystemWindows(final View view, final boolean fitsSystemWindows) {
        view.setFitsSystemWindows(fitsSystemWindows);
    }
    
    public static void setFocusedByDefault(@NonNull final View view, final boolean b) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setFocusedByDefault(view, b);
        }
    }
    
    public static void setHasTransientState(@NonNull final View view, final boolean b) {
        Api16Impl.setHasTransientState(view, b);
    }
    
    @UiThread
    public static void setImportantForAccessibility(@NonNull final View view, final int n) {
        Api16Impl.setImportantForAccessibility(view, n);
    }
    
    public static void setImportantForAutofill(@NonNull final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setImportantForAutofill(view, n);
        }
    }
    
    public static void setKeyboardNavigationCluster(@NonNull final View view, final boolean b) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setKeyboardNavigationCluster(view, b);
        }
    }
    
    public static void setLabelFor(@NonNull final View view, @IdRes final int n) {
        Api17Impl.setLabelFor(view, n);
    }
    
    public static void setLayerPaint(@NonNull final View view, @Nullable final Paint paint) {
        Api17Impl.setLayerPaint(view, paint);
    }
    
    @Deprecated
    public static void setLayerType(final View view, final int n, final Paint paint) {
        view.setLayerType(n, paint);
    }
    
    public static void setLayoutDirection(@NonNull final View view, final int n) {
        Api17Impl.setLayoutDirection(view, n);
    }
    
    public static void setNestedScrollingEnabled(@NonNull final View view, final boolean b) {
        Api21Impl.setNestedScrollingEnabled(view, b);
    }
    
    public static void setNextClusterForwardId(@NonNull final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setNextClusterForwardId(view, n);
        }
    }
    
    public static void setOnApplyWindowInsetsListener(@NonNull final View view, @Nullable final OnApplyWindowInsetsListener onApplyWindowInsetsListener) {
        Api21Impl.setOnApplyWindowInsetsListener(view, onApplyWindowInsetsListener);
    }
    
    public static void setOnReceiveContentListener(@NonNull final View view, @Nullable final String[] array, @Nullable final OnReceiveContentListener onReceiveContentListener) {
        if (Build$VERSION.SDK_INT >= 31) {
            Api31Impl.setOnReceiveContentListener(view, array, onReceiveContentListener);
            return;
        }
        String[] a = null;
        Label_0030: {
            if (array != null) {
                a = array;
                if (array.length != 0) {
                    break Label_0030;
                }
            }
            a = null;
        }
        final int n = 0;
        if (onReceiveContentListener != null) {
            Preconditions.checkArgument(a != null, (Object)"When the listener is set, MIME types must also be set");
        }
        if (a != null) {
            final int length = a.length;
            int n2 = 0;
            int n3;
            while (true) {
                n3 = n;
                if (n2 >= length) {
                    break;
                }
                if (a[n2].startsWith("*")) {
                    n3 = 1;
                    break;
                }
                ++n2;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("A MIME type set here must not start with *: ");
            sb.append(Arrays.toString(a));
            Preconditions.checkArgument((boolean)((n3 ^ 0x1) != 0x0), (Object)sb.toString());
        }
        view.setTag(R.id.tag_on_receive_content_mime_types, (Object)a);
        view.setTag(R.id.tag_on_receive_content_listener, (Object)onReceiveContentListener);
    }
    
    @Deprecated
    public static void setOverScrollMode(final View view, final int overScrollMode) {
        view.setOverScrollMode(overScrollMode);
    }
    
    public static void setPaddingRelative(@NonNull final View view, @Px final int n, @Px final int n2, @Px final int n3, @Px final int n4) {
        Api17Impl.setPaddingRelative(view, n, n2, n3, n4);
    }
    
    @Deprecated
    public static void setPivotX(final View view, final float pivotX) {
        view.setPivotX(pivotX);
    }
    
    @Deprecated
    public static void setPivotY(final View view, final float pivotY) {
        view.setPivotY(pivotY);
    }
    
    public static void setPointerIcon(@NonNull final View view, @Nullable final PointerIconCompat pointerIconCompat) {
        if (Build$VERSION.SDK_INT >= 24) {
            Object pointerIcon;
            if (pointerIconCompat != null) {
                pointerIcon = pointerIconCompat.getPointerIcon();
            }
            else {
                pointerIcon = null;
            }
            Api24Impl.setPointerIcon(view, (PointerIcon)pointerIcon);
        }
    }
    
    @Deprecated
    public static void setRotation(final View view, final float rotation) {
        view.setRotation(rotation);
    }
    
    @Deprecated
    public static void setRotationX(final View view, final float rotationX) {
        view.setRotationX(rotationX);
    }
    
    @Deprecated
    public static void setRotationY(final View view, final float rotationY) {
        view.setRotationY(rotationY);
    }
    
    @Deprecated
    public static void setSaveFromParentEnabled(final View view, final boolean saveFromParentEnabled) {
        view.setSaveFromParentEnabled(saveFromParentEnabled);
    }
    
    @Deprecated
    public static void setScaleX(final View view, final float scaleX) {
        view.setScaleX(scaleX);
    }
    
    @Deprecated
    public static void setScaleY(final View view, final float scaleY) {
        view.setScaleY(scaleY);
    }
    
    @UiThread
    public static void setScreenReaderFocusable(@NonNull final View view, final boolean b) {
        screenReaderFocusableProperty().set(view, b);
    }
    
    public static void setScrollIndicators(@NonNull final View view, final int n) {
        if (Build$VERSION.SDK_INT >= 23) {
            Api23Impl.setScrollIndicators(view, n);
        }
    }
    
    public static void setScrollIndicators(@NonNull final View view, final int n, final int n2) {
        if (Build$VERSION.SDK_INT >= 23) {
            Api23Impl.setScrollIndicators(view, n, n2);
        }
    }
    
    @UiThread
    public static void setStateDescription(@NonNull final View view, @Nullable final CharSequence charSequence) {
        stateDescriptionProperty().set(view, charSequence);
    }
    
    public static void setSystemGestureExclusionRects(@NonNull final View view, @NonNull final List<Rect> list) {
        if (Build$VERSION.SDK_INT >= 29) {
            Api29Impl.setSystemGestureExclusionRects(view, list);
        }
    }
    
    public static void setTooltipText(@NonNull final View view, @Nullable final CharSequence charSequence) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setTooltipText(view, charSequence);
        }
    }
    
    public static void setTransitionName(@NonNull final View view, @Nullable final String s) {
        Api21Impl.setTransitionName(view, s);
    }
    
    @Deprecated
    public static void setTranslationX(final View view, final float translationX) {
        view.setTranslationX(translationX);
    }
    
    @Deprecated
    public static void setTranslationY(final View view, final float translationY) {
        view.setTranslationY(translationY);
    }
    
    public static void setTranslationZ(@NonNull final View view, final float n) {
        Api21Impl.setTranslationZ(view, n);
    }
    
    private static void setViewImportanceForAccessibilityIfNeeded(final View view) {
        if (getImportantForAccessibility(view) == 0) {
            setImportantForAccessibility(view, 1);
        }
        for (ViewParent viewParent = view.getParent(); viewParent instanceof View; viewParent = viewParent.getParent()) {
            if (getImportantForAccessibility((View)viewParent) == 4) {
                setImportantForAccessibility(view, 2);
                break;
            }
        }
    }
    
    public static void setWindowInsetsAnimationCallback(@NonNull final View view, @Nullable final WindowInsetsAnimationCompat.Callback callback) {
        WindowInsetsAnimationCompat.setCallback(view, callback);
    }
    
    @Deprecated
    public static void setX(final View view, final float x) {
        view.setX(x);
    }
    
    @Deprecated
    public static void setY(final View view, final float y) {
        view.setY(y);
    }
    
    public static void setZ(@NonNull final View view, final float n) {
        Api21Impl.setZ(view, n);
    }
    
    public static boolean startDragAndDrop(@NonNull final View view, @Nullable final ClipData clipData, @NonNull final View$DragShadowBuilder view$DragShadowBuilder, @Nullable final Object o, final int n) {
        if (Build$VERSION.SDK_INT >= 24) {
            return Api24Impl.startDragAndDrop(view, clipData, view$DragShadowBuilder, o, n);
        }
        return view.startDrag(clipData, view$DragShadowBuilder, o, n);
    }
    
    public static boolean startNestedScroll(@NonNull final View view, final int n) {
        return Api21Impl.startNestedScroll(view, n);
    }
    
    public static boolean startNestedScroll(@NonNull final View view, final int n, final int n2) {
        if (view instanceof NestedScrollingChild2) {
            return ((NestedScrollingChild2)view).startNestedScroll(n, n2);
        }
        return n2 == 0 && startNestedScroll(view, n);
    }
    
    private static AccessibilityViewProperty<CharSequence> stateDescriptionProperty() {
        return (AccessibilityViewProperty<CharSequence>)new AccessibilityViewProperty<CharSequence>(R.id.tag_state_description, CharSequence.class, 64, 30) {
            @RequiresApi(30)
            CharSequence frameworkGet(final View view) {
                return Api30Impl.getStateDescription(view);
            }
            
            @RequiresApi(30)
            void frameworkSet(final View view, final CharSequence charSequence) {
                Api30Impl.setStateDescription(view, charSequence);
            }
            
            boolean shouldUpdate(final CharSequence charSequence, final CharSequence charSequence2) {
                return TextUtils.equals(charSequence, charSequence2) ^ true;
            }
        };
    }
    
    public static void stopNestedScroll(@NonNull final View view) {
        Api21Impl.stopNestedScroll(view);
    }
    
    public static void stopNestedScroll(@NonNull final View view, final int n) {
        if (view instanceof NestedScrollingChild2) {
            ((NestedScrollingChild2)view).stopNestedScroll(n);
        }
        else if (n == 0) {
            stopNestedScroll(view);
        }
    }
    
    private static void tickleInvalidationFlag(final View view) {
        final float translationY = view.getTranslationY();
        view.setTranslationY(1.0f + translationY);
        view.setTranslationY(translationY);
    }
    
    public static void updateDragShadow(@NonNull final View view, @NonNull final View$DragShadowBuilder view$DragShadowBuilder) {
        if (Build$VERSION.SDK_INT >= 24) {
            Api24Impl.updateDragShadow(view, view$DragShadowBuilder);
        }
    }
    
    static class AccessibilityPaneVisibilityManager implements ViewTreeObserver$OnGlobalLayoutListener, View$OnAttachStateChangeListener
    {
        private final WeakHashMap<View, Boolean> mPanesToVisible;
        
        AccessibilityPaneVisibilityManager() {
            this.mPanesToVisible = new WeakHashMap<View, Boolean>();
        }
        
        @RequiresApi(19)
        private void checkPaneVisibility(final View key, final boolean b) {
            final boolean b2 = key.isShown() && key.getWindowVisibility() == 0;
            if (b != b2) {
                int n;
                if (b2) {
                    n = 16;
                }
                else {
                    n = 32;
                }
                ViewCompat.notifyViewAccessibilityStateChangedIfNeeded(key, n);
                this.mPanesToVisible.put(key, b2);
            }
        }
        
        @RequiresApi(19)
        private void registerForLayoutCallback(final View view) {
            view.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver$OnGlobalLayoutListener)this);
        }
        
        @RequiresApi(19)
        private void unregisterForLayoutCallback(final View view) {
            Api16Impl.removeOnGlobalLayoutListener(view.getViewTreeObserver(), (ViewTreeObserver$OnGlobalLayoutListener)this);
        }
        
        @RequiresApi(19)
        void addAccessibilityPane(final View key) {
            this.mPanesToVisible.put(key, key.isShown() && key.getWindowVisibility() == 0);
            key.addOnAttachStateChangeListener((View$OnAttachStateChangeListener)this);
            if (Api19Impl.isAttachedToWindow(key)) {
                this.registerForLayoutCallback(key);
            }
        }
        
        @RequiresApi(19)
        public void onGlobalLayout() {
            if (Build$VERSION.SDK_INT < 28) {
                for (final Map.Entry<View, V> entry : this.mPanesToVisible.entrySet()) {
                    this.checkPaneVisibility(entry.getKey(), (boolean)entry.getValue());
                }
            }
        }
        
        @RequiresApi(19)
        public void onViewAttachedToWindow(final View view) {
            this.registerForLayoutCallback(view);
        }
        
        public void onViewDetachedFromWindow(final View view) {
        }
        
        @RequiresApi(19)
        void removeAccessibilityPane(final View key) {
            this.mPanesToVisible.remove(key);
            key.removeOnAttachStateChangeListener((View$OnAttachStateChangeListener)this);
            this.unregisterForLayoutCallback(key);
        }
    }
    
    abstract static class AccessibilityViewProperty<T>
    {
        private final int mContentChangeType;
        private final int mFrameworkMinimumSdk;
        private final int mTagKey;
        private final Class<T> mType;
        
        AccessibilityViewProperty(final int n, final Class<T> clazz, final int n2) {
            this(n, clazz, 0, n2);
        }
        
        AccessibilityViewProperty(final int mTagKey, final Class<T> mType, final int mContentChangeType, final int mFrameworkMinimumSdk) {
            this.mTagKey = mTagKey;
            this.mType = mType;
            this.mContentChangeType = mContentChangeType;
            this.mFrameworkMinimumSdk = mFrameworkMinimumSdk;
        }
        
        private boolean extrasAvailable() {
            return true;
        }
        
        private boolean frameworkAvailable() {
            return Build$VERSION.SDK_INT >= this.mFrameworkMinimumSdk;
        }
        
        boolean booleanNullToFalseEquals(final Boolean b, final Boolean b2) {
            boolean b3 = true;
            if ((b != null && b) != (b2 != null && b2)) {
                b3 = false;
            }
            return b3;
        }
        
        abstract T frameworkGet(final View p0);
        
        abstract void frameworkSet(final View p0, final T p1);
        
        T get(final View view) {
            if (this.frameworkAvailable()) {
                return this.frameworkGet(view);
            }
            if (this.extrasAvailable()) {
                final Object tag = view.getTag(this.mTagKey);
                if (this.mType.isInstance(tag)) {
                    return (T)tag;
                }
            }
            return null;
        }
        
        void set(final View view, final T t) {
            if (this.frameworkAvailable()) {
                this.frameworkSet(view, t);
            }
            else if (this.extrasAvailable() && this.shouldUpdate(this.get(view), t)) {
                ViewCompat.ensureAccessibilityDelegateCompat(view);
                view.setTag(this.mTagKey, (Object)t);
                ViewCompat.notifyViewAccessibilityStateChangedIfNeeded(view, this.mContentChangeType);
            }
        }
        
        boolean shouldUpdate(final T obj, final T t) {
            return t.equals(obj) ^ true;
        }
    }
    
    @RequiresApi(15)
    static class Api15Impl
    {
        private Api15Impl() {
        }
        
        @DoNotInline
        static boolean hasOnClickListeners(@NonNull final View view) {
            return view.hasOnClickListeners();
        }
    }
    
    @RequiresApi(16)
    static class Api16Impl
    {
        private Api16Impl() {
        }
        
        @DoNotInline
        static AccessibilityNodeProvider getAccessibilityNodeProvider(final View view) {
            return view.getAccessibilityNodeProvider();
        }
        
        @DoNotInline
        static boolean getFitsSystemWindows(final View view) {
            return view.getFitsSystemWindows();
        }
        
        @DoNotInline
        static int getImportantForAccessibility(final View view) {
            return view.getImportantForAccessibility();
        }
        
        @DoNotInline
        static int getMinimumHeight(final View view) {
            return view.getMinimumHeight();
        }
        
        @DoNotInline
        static int getMinimumWidth(final View view) {
            return view.getMinimumWidth();
        }
        
        @DoNotInline
        static ViewParent getParentForAccessibility(final View view) {
            return view.getParentForAccessibility();
        }
        
        @DoNotInline
        static int getWindowSystemUiVisibility(final View view) {
            return view.getWindowSystemUiVisibility();
        }
        
        @DoNotInline
        static boolean hasOverlappingRendering(final View view) {
            return view.hasOverlappingRendering();
        }
        
        @DoNotInline
        static boolean hasTransientState(final View view) {
            return view.hasTransientState();
        }
        
        @DoNotInline
        static boolean performAccessibilityAction(final View view, final int n, final Bundle bundle) {
            return view.performAccessibilityAction(n, bundle);
        }
        
        @DoNotInline
        static void postInvalidateOnAnimation(final View view) {
            view.postInvalidateOnAnimation();
        }
        
        @DoNotInline
        static void postInvalidateOnAnimation(final View view, final int n, final int n2, final int n3, final int n4) {
            view.postInvalidateOnAnimation(n, n2, n3, n4);
        }
        
        @DoNotInline
        static void postOnAnimation(final View view, final Runnable runnable) {
            view.postOnAnimation(runnable);
        }
        
        @DoNotInline
        static void postOnAnimationDelayed(final View view, final Runnable runnable, final long n) {
            view.postOnAnimationDelayed(runnable, n);
        }
        
        @DoNotInline
        static void removeOnGlobalLayoutListener(final ViewTreeObserver viewTreeObserver, final ViewTreeObserver$OnGlobalLayoutListener viewTreeObserver$OnGlobalLayoutListener) {
            viewTreeObserver.removeOnGlobalLayoutListener(viewTreeObserver$OnGlobalLayoutListener);
        }
        
        @DoNotInline
        static void requestFitSystemWindows(final View view) {
            view.requestFitSystemWindows();
        }
        
        @DoNotInline
        static void setBackground(final View view, final Drawable background) {
            view.setBackground(background);
        }
        
        @DoNotInline
        static void setHasTransientState(final View view, final boolean hasTransientState) {
            view.setHasTransientState(hasTransientState);
        }
        
        @DoNotInline
        static void setImportantForAccessibility(final View view, final int importantForAccessibility) {
            view.setImportantForAccessibility(importantForAccessibility);
        }
    }
    
    @RequiresApi(17)
    static class Api17Impl
    {
        private Api17Impl() {
        }
        
        @DoNotInline
        static int generateViewId() {
            return View.generateViewId();
        }
        
        @DoNotInline
        static Display getDisplay(@NonNull final View view) {
            return view.getDisplay();
        }
        
        @DoNotInline
        static int getLabelFor(final View view) {
            return view.getLabelFor();
        }
        
        @DoNotInline
        static int getLayoutDirection(final View view) {
            return view.getLayoutDirection();
        }
        
        @DoNotInline
        static int getPaddingEnd(final View view) {
            return view.getPaddingEnd();
        }
        
        @DoNotInline
        static int getPaddingStart(final View view) {
            return view.getPaddingStart();
        }
        
        @DoNotInline
        static boolean isPaddingRelative(final View view) {
            return view.isPaddingRelative();
        }
        
        @DoNotInline
        static void setLabelFor(final View view, final int labelFor) {
            view.setLabelFor(labelFor);
        }
        
        @DoNotInline
        static void setLayerPaint(final View view, final Paint layerPaint) {
            view.setLayerPaint(layerPaint);
        }
        
        @DoNotInline
        static void setLayoutDirection(final View view, final int layoutDirection) {
            view.setLayoutDirection(layoutDirection);
        }
        
        @DoNotInline
        static void setPaddingRelative(final View view, final int n, final int n2, final int n3, final int n4) {
            view.setPaddingRelative(n, n2, n3, n4);
        }
    }
    
    @RequiresApi(18)
    static class Api18Impl
    {
        private Api18Impl() {
        }
        
        @DoNotInline
        static Rect getClipBounds(@NonNull final View view) {
            return view.getClipBounds();
        }
        
        @DoNotInline
        static boolean isInLayout(@NonNull final View view) {
            return view.isInLayout();
        }
        
        @DoNotInline
        static void setClipBounds(@NonNull final View view, final Rect clipBounds) {
            view.setClipBounds(clipBounds);
        }
    }
    
    @RequiresApi(19)
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        @DoNotInline
        static int getAccessibilityLiveRegion(final View view) {
            return view.getAccessibilityLiveRegion();
        }
        
        @DoNotInline
        static boolean isAttachedToWindow(@NonNull final View view) {
            return view.isAttachedToWindow();
        }
        
        @DoNotInline
        static boolean isLaidOut(@NonNull final View view) {
            return view.isLaidOut();
        }
        
        @DoNotInline
        static boolean isLayoutDirectionResolved(@NonNull final View view) {
            return view.isLayoutDirectionResolved();
        }
        
        @DoNotInline
        static void notifySubtreeAccessibilityStateChanged(final ViewParent viewParent, final View view, final View view2, final int n) {
            viewParent.notifySubtreeAccessibilityStateChanged(view, view2, n);
        }
        
        @DoNotInline
        static void setAccessibilityLiveRegion(final View view, final int accessibilityLiveRegion) {
            view.setAccessibilityLiveRegion(accessibilityLiveRegion);
        }
        
        @DoNotInline
        static void setContentChangeTypes(final AccessibilityEvent accessibilityEvent, final int contentChangeTypes) {
            accessibilityEvent.setContentChangeTypes(contentChangeTypes);
        }
    }
    
    @RequiresApi(20)
    static class Api20Impl
    {
        private Api20Impl() {
        }
        
        @DoNotInline
        static WindowInsets dispatchApplyWindowInsets(final View view, final WindowInsets windowInsets) {
            return view.dispatchApplyWindowInsets(windowInsets);
        }
        
        @DoNotInline
        static WindowInsets onApplyWindowInsets(final View view, final WindowInsets windowInsets) {
            return view.onApplyWindowInsets(windowInsets);
        }
        
        @DoNotInline
        static void requestApplyInsets(final View view) {
            view.requestApplyInsets();
        }
    }
    
    @RequiresApi(21)
    private static class Api21Impl
    {
        @DoNotInline
        static void callCompatInsetAnimationCallback(@NonNull final WindowInsets windowInsets, @NonNull final View view) {
            final View$OnApplyWindowInsetsListener view$OnApplyWindowInsetsListener = (View$OnApplyWindowInsetsListener)view.getTag(R.id.tag_window_insets_animation_callback);
            if (view$OnApplyWindowInsetsListener != null) {
                view$OnApplyWindowInsetsListener.onApplyWindowInsets(view, windowInsets);
            }
        }
        
        @DoNotInline
        static WindowInsetsCompat computeSystemWindowInsets(@NonNull final View view, @NonNull final WindowInsetsCompat windowInsetsCompat, @NonNull final Rect rect) {
            final WindowInsets windowInsets = windowInsetsCompat.toWindowInsets();
            if (windowInsets != null) {
                return WindowInsetsCompat.toWindowInsetsCompat(view.computeSystemWindowInsets(windowInsets, rect), view);
            }
            rect.setEmpty();
            return windowInsetsCompat;
        }
        
        @DoNotInline
        static boolean dispatchNestedFling(@NonNull final View view, final float n, final float n2, final boolean b) {
            return view.dispatchNestedFling(n, n2, b);
        }
        
        @DoNotInline
        static boolean dispatchNestedPreFling(@NonNull final View view, final float n, final float n2) {
            return view.dispatchNestedPreFling(n, n2);
        }
        
        @DoNotInline
        static boolean dispatchNestedPreScroll(final View view, final int n, final int n2, final int[] array, final int[] array2) {
            return view.dispatchNestedPreScroll(n, n2, array, array2);
        }
        
        @DoNotInline
        static boolean dispatchNestedScroll(final View view, final int n, final int n2, final int n3, final int n4, final int[] array) {
            return view.dispatchNestedScroll(n, n2, n3, n4, array);
        }
        
        @DoNotInline
        static ColorStateList getBackgroundTintList(final View view) {
            return view.getBackgroundTintList();
        }
        
        @DoNotInline
        static PorterDuff$Mode getBackgroundTintMode(final View view) {
            return view.getBackgroundTintMode();
        }
        
        @DoNotInline
        static float getElevation(final View view) {
            return view.getElevation();
        }
        
        @DoNotInline
        @Nullable
        public static WindowInsetsCompat getRootWindowInsets(@NonNull final View view) {
            return WindowInsetsCompat.Api21ReflectionHolder.getRootWindowInsets(view);
        }
        
        @DoNotInline
        static String getTransitionName(final View view) {
            return view.getTransitionName();
        }
        
        @DoNotInline
        static float getTranslationZ(final View view) {
            return view.getTranslationZ();
        }
        
        @DoNotInline
        static float getZ(@NonNull final View view) {
            return view.getZ();
        }
        
        @DoNotInline
        static boolean hasNestedScrollingParent(final View view) {
            return view.hasNestedScrollingParent();
        }
        
        @DoNotInline
        static boolean isImportantForAccessibility(final View view) {
            return view.isImportantForAccessibility();
        }
        
        @DoNotInline
        static boolean isNestedScrollingEnabled(final View view) {
            return view.isNestedScrollingEnabled();
        }
        
        @DoNotInline
        static void setBackgroundTintList(final View view, final ColorStateList backgroundTintList) {
            view.setBackgroundTintList(backgroundTintList);
        }
        
        @DoNotInline
        static void setBackgroundTintMode(final View view, final PorterDuff$Mode backgroundTintMode) {
            view.setBackgroundTintMode(backgroundTintMode);
        }
        
        @DoNotInline
        static void setElevation(final View view, final float elevation) {
            view.setElevation(elevation);
        }
        
        @DoNotInline
        static void setNestedScrollingEnabled(final View view, final boolean nestedScrollingEnabled) {
            view.setNestedScrollingEnabled(nestedScrollingEnabled);
        }
        
        @DoNotInline
        static void setOnApplyWindowInsetsListener(@NonNull final View view, @Nullable final OnApplyWindowInsetsListener onApplyWindowInsetsListener) {
            if (Build$VERSION.SDK_INT < 30) {
                view.setTag(R.id.tag_on_apply_window_listener, (Object)onApplyWindowInsetsListener);
            }
            if (onApplyWindowInsetsListener == null) {
                view.setOnApplyWindowInsetsListener((View$OnApplyWindowInsetsListener)view.getTag(R.id.tag_window_insets_animation_callback));
                return;
            }
            view.setOnApplyWindowInsetsListener((View$OnApplyWindowInsetsListener)new View$OnApplyWindowInsetsListener(view, onApplyWindowInsetsListener) {
                WindowInsetsCompat mLastInsets = null;
                final OnApplyWindowInsetsListener val$listener;
                final View val$v;
                
                public WindowInsets onApplyWindowInsets(final View view, final WindowInsets windowInsets) {
                    final WindowInsetsCompat windowInsetsCompat = WindowInsetsCompat.toWindowInsetsCompat(windowInsets, view);
                    final int sdk_INT = Build$VERSION.SDK_INT;
                    if (sdk_INT < 30) {
                        Api21Impl.callCompatInsetAnimationCallback(windowInsets, this.val$v);
                        if (windowInsetsCompat.equals(this.mLastInsets)) {
                            return this.val$listener.onApplyWindowInsets(view, windowInsetsCompat).toWindowInsets();
                        }
                    }
                    this.mLastInsets = windowInsetsCompat;
                    final WindowInsetsCompat onApplyWindowInsets = this.val$listener.onApplyWindowInsets(view, windowInsetsCompat);
                    if (sdk_INT >= 30) {
                        return onApplyWindowInsets.toWindowInsets();
                    }
                    ViewCompat.requestApplyInsets(view);
                    return onApplyWindowInsets.toWindowInsets();
                }
            });
        }
        
        @DoNotInline
        static void setTransitionName(final View view, final String transitionName) {
            view.setTransitionName(transitionName);
        }
        
        @DoNotInline
        static void setTranslationZ(final View view, final float translationZ) {
            view.setTranslationZ(translationZ);
        }
        
        @DoNotInline
        static void setZ(@NonNull final View view, final float z) {
            view.setZ(z);
        }
        
        @DoNotInline
        static boolean startNestedScroll(final View view, final int n) {
            return view.startNestedScroll(n);
        }
        
        @DoNotInline
        static void stopNestedScroll(final View view) {
            view.stopNestedScroll();
        }
    }
    
    @RequiresApi(23)
    private static class Api23Impl
    {
        @Nullable
        public static WindowInsetsCompat getRootWindowInsets(@NonNull final View view) {
            final WindowInsets \u3007080 = o\u3007O.\u3007080(view);
            if (\u3007080 == null) {
                return null;
            }
            final WindowInsetsCompat windowInsetsCompat = WindowInsetsCompat.toWindowInsetsCompat(\u3007080);
            windowInsetsCompat.setRootWindowInsets(windowInsetsCompat);
            windowInsetsCompat.copyRootViewBounds(view.getRootView());
            return windowInsetsCompat;
        }
        
        @DoNotInline
        static int getScrollIndicators(@NonNull final View view) {
            return O000.\u3007080(view);
        }
        
        @DoNotInline
        static void setScrollIndicators(@NonNull final View view, final int n) {
            o\u30078oOO88.\u3007080(view, n);
        }
        
        @DoNotInline
        static void setScrollIndicators(@NonNull final View view, final int n, final int n2) {
            oO00OOO.\u3007080(view, n, n2);
        }
    }
    
    @RequiresApi(24)
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        @DoNotInline
        static void cancelDragAndDrop(@NonNull final View view) {
            ooo\u30078oO.\u3007080(view);
        }
        
        @DoNotInline
        static void dispatchFinishTemporaryDetach(final View view) {
            OO8oO0o\u3007.\u3007080(view);
        }
        
        @DoNotInline
        static void dispatchStartTemporaryDetach(final View view) {
            Ooo.\u3007080(view);
        }
        
        @DoNotInline
        static void setPointerIcon(@NonNull final View view, final PointerIcon pointerIcon) {
            O0o\u3007\u3007Oo.\u3007080(view, pointerIcon);
        }
        
        @DoNotInline
        static boolean startDragAndDrop(@NonNull final View view, @Nullable final ClipData clipData, @NonNull final View$DragShadowBuilder view$DragShadowBuilder, @Nullable final Object o, final int n) {
            return \u3007O\u300780o08O.\u3007080(view, clipData, view$DragShadowBuilder, o, n);
        }
        
        @DoNotInline
        static void updateDragShadow(@NonNull final View view, @NonNull final View$DragShadowBuilder view$DragShadowBuilder) {
            \u300780.\u3007080(view, view$DragShadowBuilder);
        }
    }
    
    @RequiresApi(26)
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        @DoNotInline
        static void addKeyboardNavigationClusters(@NonNull final View view, final Collection<View> collection, final int n) {
            ooOO.\u3007080(view, (Collection)collection, n);
        }
        
        @DoNotInline
        static int getImportantForAutofill(final View view) {
            return o0O0.\u3007080(view);
        }
        
        @DoNotInline
        static int getNextClusterForwardId(@NonNull final View view) {
            return \u3007o0O0O8.\u3007080(view);
        }
        
        @DoNotInline
        static boolean hasExplicitFocusable(@NonNull final View view) {
            return \u300700O0O0.\u3007080(view);
        }
        
        @DoNotInline
        static boolean isFocusedByDefault(@NonNull final View view) {
            return OOO8o\u3007\u3007.\u3007080(view);
        }
        
        @DoNotInline
        static boolean isImportantForAutofill(final View view) {
            return O0O8OO088.\u3007080(view);
        }
        
        @DoNotInline
        static boolean isKeyboardNavigationCluster(@NonNull final View view) {
            return Ooo8\u3007\u3007.\u3007080(view);
        }
        
        @DoNotInline
        static View keyboardNavigationClusterSearch(@NonNull final View view, final View view2, final int n) {
            return O8O\u3007.\u3007080(view, view2, n);
        }
        
        @DoNotInline
        static boolean restoreDefaultFocus(@NonNull final View view) {
            return o88\u3007OO08\u3007.\u3007080(view);
        }
        
        @DoNotInline
        static void setAutofillHints(@NonNull final View view, final String... array) {
            Oo\u3007O.\u3007080(view, array);
        }
        
        @DoNotInline
        static void setFocusedByDefault(@NonNull final View view, final boolean b) {
            \u3007\u3007o8.\u3007080(view, b);
        }
        
        @DoNotInline
        static void setImportantForAutofill(final View view, final int n) {
            \u30070O\u3007Oo.\u3007080(view, n);
        }
        
        @DoNotInline
        static void setKeyboardNavigationCluster(@NonNull final View view, final boolean b) {
            \u30070.\u3007080(view, b);
        }
        
        @DoNotInline
        static void setNextClusterForwardId(final View view, final int n) {
            O0.\u3007080(view, n);
        }
        
        @DoNotInline
        static void setTooltipText(@NonNull final View view, final CharSequence charSequence) {
            androidx.appcompat.widget.OO8oO0o\u3007.\u3007080(view, charSequence);
        }
    }
    
    @RequiresApi(28)
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        @DoNotInline
        static void addOnUnhandledKeyEventListener(@NonNull final View view, @NonNull final OnUnhandledKeyEventListenerCompat obj) {
            final int tag_unhandled_key_listeners = R.id.tag_unhandled_key_listeners;
            SimpleArrayMap simpleArrayMap;
            if ((simpleArrayMap = (SimpleArrayMap)view.getTag(tag_unhandled_key_listeners)) == null) {
                simpleArrayMap = new SimpleArrayMap();
                view.setTag(tag_unhandled_key_listeners, (Object)simpleArrayMap);
            }
            Objects.requireNonNull(obj);
            final ooO\u300700O ooO\u300700O = new ooO\u300700O(obj);
            simpleArrayMap.put(obj, ooO\u300700O);
            OOo8o\u3007O.\u3007080(view, (View$OnUnhandledKeyEventListener)ooO\u300700O);
        }
        
        @DoNotInline
        static CharSequence getAccessibilityPaneTitle(final View view) {
            return O0OO8\u30070.\u3007080(view);
        }
        
        @DoNotInline
        static boolean isAccessibilityHeading(final View view) {
            return O880oOO08.\u3007080(view);
        }
        
        @DoNotInline
        static boolean isScreenReaderFocusable(final View view) {
            return \u3007000O0.\u3007080(view);
        }
        
        @DoNotInline
        static void removeOnUnhandledKeyEventListener(@NonNull final View view, @NonNull final OnUnhandledKeyEventListenerCompat onUnhandledKeyEventListenerCompat) {
            final SimpleArrayMap simpleArrayMap = (SimpleArrayMap)view.getTag(R.id.tag_unhandled_key_listeners);
            if (simpleArrayMap == null) {
                return;
            }
            final View$OnUnhandledKeyEventListener view$OnUnhandledKeyEventListener = simpleArrayMap.get(onUnhandledKeyEventListenerCompat);
            if (view$OnUnhandledKeyEventListener != null) {
                O\u3007OO.\u3007080(view, view$OnUnhandledKeyEventListener);
            }
        }
        
        @DoNotInline
        static <T> T requireViewById(final View view, final int n) {
            return (T)O\u300708.\u3007080(view, n);
        }
        
        @DoNotInline
        static void setAccessibilityHeading(final View view, final boolean b) {
            OOo0O.\u3007080(view, b);
        }
        
        @DoNotInline
        static void setAccessibilityPaneTitle(final View view, final CharSequence charSequence) {
            O\u3007Oooo\u3007\u3007.\u3007080(view, charSequence);
        }
        
        @DoNotInline
        static void setScreenReaderFocusable(final View view, final boolean b) {
            ooo0\u3007O88O.\u3007080(view, b);
        }
    }
    
    @RequiresApi(29)
    private static class Api29Impl
    {
        @DoNotInline
        static View$AccessibilityDelegate getAccessibilityDelegate(final View view) {
            return \u3007000\u3007\u300708.\u3007080(view);
        }
        
        @DoNotInline
        static List<Rect> getSystemGestureExclusionRects(final View view) {
            return O0\u3007OO8.\u3007080(view);
        }
        
        @DoNotInline
        static void saveAttributeDataForStyleable(@NonNull final View view, @NonNull final Context context, @NonNull final int[] array, @Nullable final AttributeSet set, @NonNull final TypedArray typedArray, final int n, final int n2) {
            O8O\u300788oO0.\u3007080(view, context, array, set, typedArray, n, n2);
        }
        
        @DoNotInline
        static void setSystemGestureExclusionRects(final View view, final List<Rect> list) {
            Oo\u3007O8o\u30078.\u3007080(view, (List)list);
        }
    }
    
    @RequiresApi(30)
    private static class Api30Impl
    {
        @DoNotInline
        static CharSequence getStateDescription(final View view) {
            return o8O0.\u3007080(view);
        }
        
        @Nullable
        public static WindowInsetsControllerCompat getWindowInsetsController(@NonNull final View view) {
            final WindowInsetsController \u3007080 = o80ooO.\u3007080(view);
            WindowInsetsControllerCompat windowInsetsControllerCompat;
            if (\u3007080 != null) {
                windowInsetsControllerCompat = WindowInsetsControllerCompat.toWindowInsetsControllerCompat(\u3007080);
            }
            else {
                windowInsetsControllerCompat = null;
            }
            return windowInsetsControllerCompat;
        }
        
        @DoNotInline
        static void setStateDescription(final View view, final CharSequence charSequence) {
            O00.\u3007080(view, charSequence);
        }
    }
    
    @RequiresApi(31)
    private static final class Api31Impl
    {
        @DoNotInline
        @Nullable
        public static String[] getReceiveContentMimeTypes(@NonNull final View view) {
            return O0o\u3007O0\u3007.\u3007080(view);
        }
        
        @DoNotInline
        @Nullable
        public static ContentInfoCompat performReceiveContent(@NonNull final View view, @NonNull final ContentInfoCompat contentInfoCompat) {
            final ContentInfo contentInfo = contentInfoCompat.toContentInfo();
            final ContentInfo \u3007080 = O0o.\u3007080(view, contentInfo);
            if (\u3007080 == null) {
                return null;
            }
            if (\u3007080 == contentInfo) {
                return contentInfoCompat;
            }
            return ContentInfoCompat.toContentInfoCompat(\u3007080);
        }
        
        @DoNotInline
        public static void setOnReceiveContentListener(@NonNull final View view, @Nullable final String[] array, @Nullable final OnReceiveContentListener onReceiveContentListener) {
            if (onReceiveContentListener == null) {
                o0O\u30078o0O.\u3007080(view, array, (android.view.OnReceiveContentListener)null);
            }
            else {
                o0O\u30078o0O.\u3007080(view, array, (android.view.OnReceiveContentListener)new OnReceiveContentListenerAdapter(onReceiveContentListener));
            }
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public @interface FocusDirection {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public @interface FocusRealDirection {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public @interface FocusRelativeDirection {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public @interface NestedScrollType {
    }
    
    @RequiresApi(31)
    private static final class OnReceiveContentListenerAdapter implements android.view.OnReceiveContentListener
    {
        @NonNull
        private final OnReceiveContentListener mJetpackListener;
        
        OnReceiveContentListenerAdapter(@NonNull final OnReceiveContentListener mJetpackListener) {
            this.mJetpackListener = mJetpackListener;
        }
        
        @Nullable
        public ContentInfo onReceiveContent(@NonNull final View view, @NonNull final ContentInfo contentInfo) {
            final ContentInfoCompat contentInfoCompat = ContentInfoCompat.toContentInfoCompat(contentInfo);
            final ContentInfoCompat onReceiveContent = this.mJetpackListener.onReceiveContent(view, contentInfoCompat);
            if (onReceiveContent == null) {
                return null;
            }
            if (onReceiveContent == contentInfoCompat) {
                return contentInfo;
            }
            return onReceiveContent.toContentInfo();
        }
    }
    
    public interface OnUnhandledKeyEventListenerCompat
    {
        boolean onUnhandledKeyEvent(@NonNull final View p0, @NonNull final KeyEvent p1);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public @interface ScrollAxis {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public @interface ScrollIndicators {
    }
    
    static class UnhandledKeyEventManager
    {
        private static final ArrayList<WeakReference<View>> sViewsWithListeners;
        private SparseArray<WeakReference<View>> mCapturedKeys;
        private WeakReference<KeyEvent> mLastDispatchedPreViewKeyEvent;
        @Nullable
        private WeakHashMap<View, Boolean> mViewsContainingListeners;
        
        static {
            sViewsWithListeners = new ArrayList<WeakReference<View>>();
        }
        
        UnhandledKeyEventManager() {
            this.mViewsContainingListeners = null;
            this.mCapturedKeys = null;
            this.mLastDispatchedPreViewKeyEvent = null;
        }
        
        static UnhandledKeyEventManager at(final View view) {
            final int tag_unhandled_key_event_manager = R.id.tag_unhandled_key_event_manager;
            UnhandledKeyEventManager unhandledKeyEventManager;
            if ((unhandledKeyEventManager = (UnhandledKeyEventManager)view.getTag(tag_unhandled_key_event_manager)) == null) {
                unhandledKeyEventManager = new UnhandledKeyEventManager();
                view.setTag(tag_unhandled_key_event_manager, (Object)unhandledKeyEventManager);
            }
            return unhandledKeyEventManager;
        }
        
        @Nullable
        private View dispatchInOrder(final View key, final KeyEvent keyEvent) {
            final WeakHashMap<View, Boolean> mViewsContainingListeners = this.mViewsContainingListeners;
            if (mViewsContainingListeners != null) {
                if (mViewsContainingListeners.containsKey(key)) {
                    if (key instanceof ViewGroup) {
                        final ViewGroup viewGroup = (ViewGroup)key;
                        for (int i = viewGroup.getChildCount() - 1; i >= 0; --i) {
                            final View dispatchInOrder = this.dispatchInOrder(viewGroup.getChildAt(i), keyEvent);
                            if (dispatchInOrder != null) {
                                return dispatchInOrder;
                            }
                        }
                    }
                    if (this.onUnhandledKeyEvent(key, keyEvent)) {
                        return key;
                    }
                }
            }
            return null;
        }
        
        private SparseArray<WeakReference<View>> getCapturedKeys() {
            if (this.mCapturedKeys == null) {
                this.mCapturedKeys = (SparseArray<WeakReference<View>>)new SparseArray();
            }
            return this.mCapturedKeys;
        }
        
        private boolean onUnhandledKeyEvent(@NonNull final View view, @NonNull final KeyEvent keyEvent) {
            final ArrayList list = (ArrayList)view.getTag(R.id.tag_unhandled_key_listeners);
            if (list != null) {
                for (int i = list.size() - 1; i >= 0; --i) {
                    if (((OnUnhandledKeyEventListenerCompat)list.get(i)).onUnhandledKeyEvent(view, keyEvent)) {
                        return true;
                    }
                }
            }
            return false;
        }
        
        private void recalcViewsWithUnhandled() {
            final WeakHashMap<View, Boolean> mViewsContainingListeners = this.mViewsContainingListeners;
            if (mViewsContainingListeners != null) {
                mViewsContainingListeners.clear();
            }
            final ArrayList<WeakReference<View>> sViewsWithListeners = UnhandledKeyEventManager.sViewsWithListeners;
            if (sViewsWithListeners.isEmpty()) {
                return;
            }
            synchronized (sViewsWithListeners) {
                if (this.mViewsContainingListeners == null) {
                    this.mViewsContainingListeners = new WeakHashMap<View, Boolean>();
                }
                for (int i = sViewsWithListeners.size() - 1; i >= 0; --i) {
                    final ArrayList<WeakReference<View>> sViewsWithListeners2 = UnhandledKeyEventManager.sViewsWithListeners;
                    final View key = sViewsWithListeners2.get(i).get();
                    if (key == null) {
                        sViewsWithListeners2.remove(i);
                    }
                    else {
                        this.mViewsContainingListeners.put(key, Boolean.TRUE);
                        for (ViewParent viewParent = key.getParent(); viewParent instanceof View; viewParent = viewParent.getParent()) {
                            this.mViewsContainingListeners.put((View)viewParent, Boolean.TRUE);
                        }
                    }
                }
            }
        }
        
        static void registerListeningView(final View referent) {
            final ArrayList<WeakReference<View>> sViewsWithListeners = UnhandledKeyEventManager.sViewsWithListeners;
            synchronized (sViewsWithListeners) {
                final Iterator<WeakReference<View>> iterator = sViewsWithListeners.iterator();
                while (iterator.hasNext()) {
                    if (iterator.next().get() == referent) {
                        return;
                    }
                }
                UnhandledKeyEventManager.sViewsWithListeners.add(new WeakReference<View>(referent));
            }
        }
        
        static void unregisterListeningView(final View view) {
            final ArrayList<WeakReference<View>> sViewsWithListeners = UnhandledKeyEventManager.sViewsWithListeners;
            monitorenter(sViewsWithListeners);
            int n = 0;
            try {
                while (true) {
                    final ArrayList<WeakReference<View>> sViewsWithListeners2 = UnhandledKeyEventManager.sViewsWithListeners;
                    if (n >= sViewsWithListeners2.size()) {
                        return;
                    }
                    if (((WeakReference)sViewsWithListeners2.get(n)).get() == view) {
                        sViewsWithListeners2.remove(n);
                        return;
                    }
                    ++n;
                }
            }
            finally {
                monitorexit(sViewsWithListeners);
            }
        }
        
        boolean dispatch(View dispatchInOrder, final KeyEvent keyEvent) {
            if (keyEvent.getAction() == 0) {
                this.recalcViewsWithUnhandled();
            }
            dispatchInOrder = this.dispatchInOrder(dispatchInOrder, keyEvent);
            if (keyEvent.getAction() == 0) {
                final int keyCode = keyEvent.getKeyCode();
                if (dispatchInOrder != null && !KeyEvent.isModifierKey(keyCode)) {
                    this.getCapturedKeys().put(keyCode, (Object)new WeakReference(dispatchInOrder));
                }
            }
            return dispatchInOrder != null;
        }
        
        boolean preDispatch(final KeyEvent referent) {
            final WeakReference<KeyEvent> mLastDispatchedPreViewKeyEvent = this.mLastDispatchedPreViewKeyEvent;
            if (mLastDispatchedPreViewKeyEvent != null && mLastDispatchedPreViewKeyEvent.get() == referent) {
                return false;
            }
            this.mLastDispatchedPreViewKeyEvent = new WeakReference<KeyEvent>(referent);
            final SparseArray<WeakReference<View>> capturedKeys = this.getCapturedKeys();
            WeakReference weakReference = null;
            Label_0080: {
                if (referent.getAction() == 1) {
                    final int indexOfKey = capturedKeys.indexOfKey(referent.getKeyCode());
                    if (indexOfKey >= 0) {
                        weakReference = (WeakReference)capturedKeys.valueAt(indexOfKey);
                        capturedKeys.removeAt(indexOfKey);
                        break Label_0080;
                    }
                }
                weakReference = null;
            }
            WeakReference weakReference2 = weakReference;
            if (weakReference == null) {
                weakReference2 = (WeakReference)capturedKeys.get(referent.getKeyCode());
            }
            if (weakReference2 != null) {
                final View view = (View)weakReference2.get();
                if (view != null && ViewCompat.isAttachedToWindow(view)) {
                    this.onUnhandledKeyEvent(view, referent);
                }
                return true;
            }
            return false;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.view;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.annotation.RestrictTo;
import java.util.Objects;
import java.lang.reflect.Method;
import android.view.DisplayCutout;
import android.view.WindowInsets$Builder;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import android.annotation.SuppressLint;
import android.graphics.Rect;
import androidx.annotation.IntRange;
import androidx.core.util.ObjectsCompat;
import androidx.core.util.Preconditions;
import android.view.View;
import androidx.core.graphics.Insets;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import android.view.WindowInsets;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;

public class WindowInsetsCompat
{
    @NonNull
    public static final WindowInsetsCompat CONSUMED;
    private static final String TAG = "WindowInsetsCompat";
    private final Impl mImpl;
    
    static {
        if (Build$VERSION.SDK_INT >= 30) {
            CONSUMED = Impl30.CONSUMED;
        }
        else {
            CONSUMED = Impl.CONSUMED;
        }
    }
    
    @RequiresApi(20)
    private WindowInsetsCompat(@NonNull final WindowInsets windowInsets) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 30) {
            this.mImpl = (Impl)new Impl30(this, windowInsets);
        }
        else if (sdk_INT >= 29) {
            this.mImpl = (Impl)new Impl29(this, windowInsets);
        }
        else if (sdk_INT >= 28) {
            this.mImpl = (Impl)new Impl28(this, windowInsets);
        }
        else {
            this.mImpl = (Impl)new Impl21(this, windowInsets);
        }
    }
    
    public WindowInsetsCompat(@Nullable final WindowInsetsCompat windowInsetsCompat) {
        if (windowInsetsCompat != null) {
            final Impl mImpl = windowInsetsCompat.mImpl;
            final int sdk_INT = Build$VERSION.SDK_INT;
            if (sdk_INT >= 30 && mImpl instanceof Impl30) {
                this.mImpl = (Impl)new Impl30(this, (Impl30)mImpl);
            }
            else if (sdk_INT >= 29 && mImpl instanceof Impl29) {
                this.mImpl = (Impl)new Impl29(this, (Impl29)mImpl);
            }
            else if (sdk_INT >= 28 && mImpl instanceof Impl28) {
                this.mImpl = (Impl)new Impl28(this, (Impl28)mImpl);
            }
            else if (mImpl instanceof Impl21) {
                this.mImpl = (Impl)new Impl21(this, (Impl21)mImpl);
            }
            else if (mImpl instanceof Impl20) {
                this.mImpl = (Impl)new Impl20(this, (Impl20)mImpl);
            }
            else {
                this.mImpl = new Impl(this);
            }
            mImpl.copyWindowDataInto(this);
        }
        else {
            this.mImpl = new Impl(this);
        }
    }
    
    static Insets insetInsets(@NonNull final Insets insets, final int n, final int n2, final int n3, final int n4) {
        final int max = Math.max(0, insets.left - n);
        final int max2 = Math.max(0, insets.top - n2);
        final int max3 = Math.max(0, insets.right - n3);
        final int max4 = Math.max(0, insets.bottom - n4);
        if (max == n && max2 == n2 && max3 == n3 && max4 == n4) {
            return insets;
        }
        return Insets.of(max, max2, max3, max4);
    }
    
    @NonNull
    @RequiresApi(20)
    public static WindowInsetsCompat toWindowInsetsCompat(@NonNull final WindowInsets windowInsets) {
        return toWindowInsetsCompat(windowInsets, null);
    }
    
    @NonNull
    @RequiresApi(20)
    public static WindowInsetsCompat toWindowInsetsCompat(@NonNull final WindowInsets windowInsets, @Nullable final View view) {
        final WindowInsetsCompat windowInsetsCompat = new WindowInsetsCompat(Preconditions.checkNotNull(windowInsets));
        if (view != null && ViewCompat.isAttachedToWindow(view)) {
            windowInsetsCompat.setRootWindowInsets(ViewCompat.getRootWindowInsets(view));
            windowInsetsCompat.copyRootViewBounds(view.getRootView());
        }
        return windowInsetsCompat;
    }
    
    @Deprecated
    @NonNull
    public WindowInsetsCompat consumeDisplayCutout() {
        return this.mImpl.consumeDisplayCutout();
    }
    
    @Deprecated
    @NonNull
    public WindowInsetsCompat consumeStableInsets() {
        return this.mImpl.consumeStableInsets();
    }
    
    @Deprecated
    @NonNull
    public WindowInsetsCompat consumeSystemWindowInsets() {
        return this.mImpl.consumeSystemWindowInsets();
    }
    
    void copyRootViewBounds(@NonNull final View view) {
        this.mImpl.copyRootViewBounds(view);
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof WindowInsetsCompat && ObjectsCompat.equals(this.mImpl, ((WindowInsetsCompat)o).mImpl));
    }
    
    @Nullable
    public DisplayCutoutCompat getDisplayCutout() {
        return this.mImpl.getDisplayCutout();
    }
    
    @NonNull
    public Insets getInsets(final int n) {
        return this.mImpl.getInsets(n);
    }
    
    @NonNull
    public Insets getInsetsIgnoringVisibility(final int n) {
        return this.mImpl.getInsetsIgnoringVisibility(n);
    }
    
    @Deprecated
    @NonNull
    public Insets getMandatorySystemGestureInsets() {
        return this.mImpl.getMandatorySystemGestureInsets();
    }
    
    @Deprecated
    public int getStableInsetBottom() {
        return this.mImpl.getStableInsets().bottom;
    }
    
    @Deprecated
    public int getStableInsetLeft() {
        return this.mImpl.getStableInsets().left;
    }
    
    @Deprecated
    public int getStableInsetRight() {
        return this.mImpl.getStableInsets().right;
    }
    
    @Deprecated
    public int getStableInsetTop() {
        return this.mImpl.getStableInsets().top;
    }
    
    @Deprecated
    @NonNull
    public Insets getStableInsets() {
        return this.mImpl.getStableInsets();
    }
    
    @Deprecated
    @NonNull
    public Insets getSystemGestureInsets() {
        return this.mImpl.getSystemGestureInsets();
    }
    
    @Deprecated
    public int getSystemWindowInsetBottom() {
        return this.mImpl.getSystemWindowInsets().bottom;
    }
    
    @Deprecated
    public int getSystemWindowInsetLeft() {
        return this.mImpl.getSystemWindowInsets().left;
    }
    
    @Deprecated
    public int getSystemWindowInsetRight() {
        return this.mImpl.getSystemWindowInsets().right;
    }
    
    @Deprecated
    public int getSystemWindowInsetTop() {
        return this.mImpl.getSystemWindowInsets().top;
    }
    
    @Deprecated
    @NonNull
    public Insets getSystemWindowInsets() {
        return this.mImpl.getSystemWindowInsets();
    }
    
    @Deprecated
    @NonNull
    public Insets getTappableElementInsets() {
        return this.mImpl.getTappableElementInsets();
    }
    
    public boolean hasInsets() {
        final Insets insets = this.getInsets(Type.all());
        final Insets none = Insets.NONE;
        return !insets.equals(none) || !this.getInsetsIgnoringVisibility(Type.all() ^ Type.ime()).equals(none) || this.getDisplayCutout() != null;
    }
    
    @Deprecated
    public boolean hasStableInsets() {
        return this.mImpl.getStableInsets().equals(Insets.NONE) ^ true;
    }
    
    @Deprecated
    public boolean hasSystemWindowInsets() {
        return this.mImpl.getSystemWindowInsets().equals(Insets.NONE) ^ true;
    }
    
    @Override
    public int hashCode() {
        final Impl mImpl = this.mImpl;
        int hashCode;
        if (mImpl == null) {
            hashCode = 0;
        }
        else {
            hashCode = mImpl.hashCode();
        }
        return hashCode;
    }
    
    @NonNull
    public WindowInsetsCompat inset(@IntRange(from = 0L) final int n, @IntRange(from = 0L) final int n2, @IntRange(from = 0L) final int n3, @IntRange(from = 0L) final int n4) {
        return this.mImpl.inset(n, n2, n3, n4);
    }
    
    @NonNull
    public WindowInsetsCompat inset(@NonNull final Insets insets) {
        return this.inset(insets.left, insets.top, insets.right, insets.bottom);
    }
    
    public boolean isConsumed() {
        return this.mImpl.isConsumed();
    }
    
    public boolean isRound() {
        return this.mImpl.isRound();
    }
    
    public boolean isVisible(final int n) {
        return this.mImpl.isVisible(n);
    }
    
    @Deprecated
    @NonNull
    public WindowInsetsCompat replaceSystemWindowInsets(final int n, final int n2, final int n3, final int n4) {
        return new Builder(this).setSystemWindowInsets(Insets.of(n, n2, n3, n4)).build();
    }
    
    @Deprecated
    @NonNull
    public WindowInsetsCompat replaceSystemWindowInsets(@NonNull final Rect rect) {
        return new Builder(this).setSystemWindowInsets(Insets.of(rect)).build();
    }
    
    void setOverriddenInsets(final Insets[] overriddenInsets) {
        this.mImpl.setOverriddenInsets(overriddenInsets);
    }
    
    void setRootViewData(@NonNull final Insets rootViewData) {
        this.mImpl.setRootViewData(rootViewData);
    }
    
    void setRootWindowInsets(@Nullable final WindowInsetsCompat rootWindowInsets) {
        this.mImpl.setRootWindowInsets(rootWindowInsets);
    }
    
    void setStableInsets(@Nullable final Insets stableInsets) {
        this.mImpl.setStableInsets(stableInsets);
    }
    
    @Nullable
    @RequiresApi(20)
    public WindowInsets toWindowInsets() {
        final Impl mImpl = this.mImpl;
        WindowInsets mPlatformInsets;
        if (mImpl instanceof Impl20) {
            mPlatformInsets = ((Impl20)mImpl).mPlatformInsets;
        }
        else {
            mPlatformInsets = null;
        }
        return mPlatformInsets;
    }
    
    @SuppressLint({ "SoonBlockedPrivateApi" })
    @RequiresApi(21)
    static class Api21ReflectionHolder
    {
        private static Field sContentInsets;
        private static boolean sReflectionSucceeded;
        private static Field sStableInsets;
        private static Field sViewAttachInfoField;
        
        static {
            try {
                (Api21ReflectionHolder.sViewAttachInfoField = View.class.getDeclaredField("mAttachInfo")).setAccessible(true);
                final Class<?> forName = Class.forName("android.view.View$AttachInfo");
                (Api21ReflectionHolder.sStableInsets = forName.getDeclaredField("mStableInsets")).setAccessible(true);
                (Api21ReflectionHolder.sContentInsets = forName.getDeclaredField("mContentInsets")).setAccessible(true);
                Api21ReflectionHolder.sReflectionSucceeded = true;
            }
            catch (final ReflectiveOperationException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to get visible insets from AttachInfo ");
                sb.append(ex.getMessage());
            }
        }
        
        private Api21ReflectionHolder() {
        }
        
        @Nullable
        public static WindowInsetsCompat getRootWindowInsets(@NonNull final View view) {
            if (Api21ReflectionHolder.sReflectionSucceeded) {
                if (view.isAttachedToWindow()) {
                    final View rootView = view.getRootView();
                    try {
                        final Object value = Api21ReflectionHolder.sViewAttachInfoField.get(rootView);
                        if (value != null) {
                            final Rect rect = (Rect)Api21ReflectionHolder.sStableInsets.get(value);
                            final Rect rect2 = (Rect)Api21ReflectionHolder.sContentInsets.get(value);
                            if (rect != null && rect2 != null) {
                                final WindowInsetsCompat build = new Builder().setStableInsets(Insets.of(rect)).setSystemWindowInsets(Insets.of(rect2)).build();
                                build.setRootWindowInsets(build);
                                build.copyRootViewBounds(view.getRootView());
                                return build;
                            }
                        }
                    }
                    catch (final IllegalAccessException ex) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Failed to get insets from AttachInfo. ");
                        sb.append(ex.getMessage());
                    }
                }
            }
            return null;
        }
    }
    
    public static final class Builder
    {
        private final BuilderImpl mImpl;
        
        public Builder() {
            final int sdk_INT = Build$VERSION.SDK_INT;
            if (sdk_INT >= 30) {
                this.mImpl = new BuilderImpl30();
            }
            else if (sdk_INT >= 29) {
                this.mImpl = new BuilderImpl29();
            }
            else {
                this.mImpl = new BuilderImpl20();
            }
        }
        
        public Builder(@NonNull final WindowInsetsCompat windowInsetsCompat) {
            final int sdk_INT = Build$VERSION.SDK_INT;
            if (sdk_INT >= 30) {
                this.mImpl = new BuilderImpl30(windowInsetsCompat);
            }
            else if (sdk_INT >= 29) {
                this.mImpl = new BuilderImpl29(windowInsetsCompat);
            }
            else {
                this.mImpl = new BuilderImpl20(windowInsetsCompat);
            }
        }
        
        @NonNull
        public WindowInsetsCompat build() {
            return this.mImpl.build();
        }
        
        @NonNull
        public Builder setDisplayCutout(@Nullable final DisplayCutoutCompat displayCutout) {
            this.mImpl.setDisplayCutout(displayCutout);
            return this;
        }
        
        @NonNull
        public Builder setInsets(final int n, @NonNull final Insets insets) {
            this.mImpl.setInsets(n, insets);
            return this;
        }
        
        @NonNull
        public Builder setInsetsIgnoringVisibility(final int n, @NonNull final Insets insets) {
            this.mImpl.setInsetsIgnoringVisibility(n, insets);
            return this;
        }
        
        @Deprecated
        @NonNull
        public Builder setMandatorySystemGestureInsets(@NonNull final Insets mandatorySystemGestureInsets) {
            this.mImpl.setMandatorySystemGestureInsets(mandatorySystemGestureInsets);
            return this;
        }
        
        @Deprecated
        @NonNull
        public Builder setStableInsets(@NonNull final Insets stableInsets) {
            this.mImpl.setStableInsets(stableInsets);
            return this;
        }
        
        @Deprecated
        @NonNull
        public Builder setSystemGestureInsets(@NonNull final Insets systemGestureInsets) {
            this.mImpl.setSystemGestureInsets(systemGestureInsets);
            return this;
        }
        
        @Deprecated
        @NonNull
        public Builder setSystemWindowInsets(@NonNull final Insets systemWindowInsets) {
            this.mImpl.setSystemWindowInsets(systemWindowInsets);
            return this;
        }
        
        @Deprecated
        @NonNull
        public Builder setTappableElementInsets(@NonNull final Insets tappableElementInsets) {
            this.mImpl.setTappableElementInsets(tappableElementInsets);
            return this;
        }
        
        @NonNull
        public Builder setVisible(final int n, final boolean b) {
            this.mImpl.setVisible(n, b);
            return this;
        }
    }
    
    private static class BuilderImpl
    {
        private final WindowInsetsCompat mInsets;
        Insets[] mInsetsTypeMask;
        
        BuilderImpl() {
            this(new WindowInsetsCompat((WindowInsetsCompat)null));
        }
        
        BuilderImpl(@NonNull final WindowInsetsCompat mInsets) {
            this.mInsets = mInsets;
        }
        
        protected final void applyInsetTypes() {
            final Insets[] mInsetsTypeMask = this.mInsetsTypeMask;
            if (mInsetsTypeMask != null) {
                final Insets insets = mInsetsTypeMask[Type.indexOf(1)];
                Insets insets2;
                if ((insets2 = this.mInsetsTypeMask[Type.indexOf(2)]) == null) {
                    insets2 = this.mInsets.getInsets(2);
                }
                Insets insets3;
                if ((insets3 = insets) == null) {
                    insets3 = this.mInsets.getInsets(1);
                }
                this.setSystemWindowInsets(Insets.max(insets3, insets2));
                final Insets systemGestureInsets = this.mInsetsTypeMask[Type.indexOf(16)];
                if (systemGestureInsets != null) {
                    this.setSystemGestureInsets(systemGestureInsets);
                }
                final Insets mandatorySystemGestureInsets = this.mInsetsTypeMask[Type.indexOf(32)];
                if (mandatorySystemGestureInsets != null) {
                    this.setMandatorySystemGestureInsets(mandatorySystemGestureInsets);
                }
                final Insets tappableElementInsets = this.mInsetsTypeMask[Type.indexOf(64)];
                if (tappableElementInsets != null) {
                    this.setTappableElementInsets(tappableElementInsets);
                }
            }
        }
        
        @NonNull
        WindowInsetsCompat build() {
            this.applyInsetTypes();
            return this.mInsets;
        }
        
        void setDisplayCutout(@Nullable final DisplayCutoutCompat displayCutoutCompat) {
        }
        
        void setInsets(final int n, @NonNull final Insets insets) {
            if (this.mInsetsTypeMask == null) {
                this.mInsetsTypeMask = new Insets[9];
            }
            for (int i = 1; i <= 256; i <<= 1) {
                if ((n & i) != 0x0) {
                    this.mInsetsTypeMask[Type.indexOf(i)] = insets;
                }
            }
        }
        
        void setInsetsIgnoringVisibility(final int n, @NonNull final Insets insets) {
            if (n != 8) {
                return;
            }
            throw new IllegalArgumentException("Ignoring visibility inset not available for IME");
        }
        
        void setMandatorySystemGestureInsets(@NonNull final Insets insets) {
        }
        
        void setStableInsets(@NonNull final Insets insets) {
        }
        
        void setSystemGestureInsets(@NonNull final Insets insets) {
        }
        
        void setSystemWindowInsets(@NonNull final Insets insets) {
        }
        
        void setTappableElementInsets(@NonNull final Insets insets) {
        }
        
        void setVisible(final int n, final boolean b) {
        }
    }
    
    @RequiresApi(api = 20)
    private static class BuilderImpl20 extends BuilderImpl
    {
        private static Constructor<WindowInsets> sConstructor;
        private static boolean sConstructorFetched = false;
        private static Field sConsumedField;
        private static boolean sConsumedFieldFetched = false;
        private WindowInsets mPlatformInsets;
        private Insets mStableInsets;
        
        BuilderImpl20() {
            this.mPlatformInsets = createWindowInsetsInstance();
        }
        
        BuilderImpl20(@NonNull final WindowInsetsCompat windowInsetsCompat) {
            super(windowInsetsCompat);
            this.mPlatformInsets = windowInsetsCompat.toWindowInsets();
        }
        
        @Nullable
        private static WindowInsets createWindowInsetsInstance() {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     3: ifne            20
            //     6: ldc             Landroid/view/WindowInsets;.class
            //     8: ldc             "CONSUMED"
            //    10: invokevirtual   java/lang/Class.getDeclaredField:(Ljava/lang/String;)Ljava/lang/reflect/Field;
            //    13: putstatic       androidx/core/view/WindowInsetsCompat$BuilderImpl20.sConsumedField:Ljava/lang/reflect/Field;
            //    16: iconst_1       
            //    17: putstatic       androidx/core/view/WindowInsetsCompat$BuilderImpl20.sConsumedFieldFetched:Z
            //    20: getstatic       androidx/core/view/WindowInsetsCompat$BuilderImpl20.sConsumedField:Ljava/lang/reflect/Field;
            //    23: astore_0       
            //    24: aload_0        
            //    25: ifnull          53
            //    28: aload_0        
            //    29: aconst_null    
            //    30: invokevirtual   java/lang/reflect/Field.get:(Ljava/lang/Object;)Ljava/lang/Object;
            //    33: checkcast       Landroid/view/WindowInsets;
            //    36: astore_0       
            //    37: aload_0        
            //    38: ifnull          53
            //    41: new             Landroid/view/WindowInsets;
            //    44: dup            
            //    45: aload_0        
            //    46: invokespecial   android/view/WindowInsets.<init>:(Landroid/view/WindowInsets;)V
            //    49: astore_0       
            //    50: aload_0        
            //    51: areturn        
            //    52: astore_0       
            //    53: getstatic       androidx/core/view/WindowInsetsCompat$BuilderImpl20.sConstructorFetched:Z
            //    56: ifne            80
            //    59: ldc             Landroid/view/WindowInsets;.class
            //    61: iconst_1       
            //    62: anewarray       Ljava/lang/Class;
            //    65: dup            
            //    66: iconst_0       
            //    67: ldc             Landroid/graphics/Rect;.class
            //    69: aastore        
            //    70: invokevirtual   java/lang/Class.getConstructor:([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
            //    73: putstatic       androidx/core/view/WindowInsetsCompat$BuilderImpl20.sConstructor:Ljava/lang/reflect/Constructor;
            //    76: iconst_1       
            //    77: putstatic       androidx/core/view/WindowInsetsCompat$BuilderImpl20.sConstructorFetched:Z
            //    80: getstatic       androidx/core/view/WindowInsetsCompat$BuilderImpl20.sConstructor:Ljava/lang/reflect/Constructor;
            //    83: astore_1       
            //    84: aload_1        
            //    85: ifnull          114
            //    88: new             Landroid/graphics/Rect;
            //    91: astore_0       
            //    92: aload_0        
            //    93: invokespecial   android/graphics/Rect.<init>:()V
            //    96: aload_1        
            //    97: iconst_1       
            //    98: anewarray       Ljava/lang/Object;
            //   101: dup            
            //   102: iconst_0       
            //   103: aload_0        
            //   104: aastore        
            //   105: invokevirtual   java/lang/reflect/Constructor.newInstance:([Ljava/lang/Object;)Ljava/lang/Object;
            //   108: checkcast       Landroid/view/WindowInsets;
            //   111: astore_0       
            //   112: aload_0        
            //   113: areturn        
            //   114: aconst_null    
            //   115: areturn        
            //   116: astore_0       
            //   117: goto            16
            //   120: astore_0       
            //   121: goto            76
            //   124: astore_0       
            //   125: goto            114
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                                    
            //  -----  -----  -----  -----  ----------------------------------------
            //  6      16     116    120    Ljava/lang/ReflectiveOperationException;
            //  28     37     52     53     Ljava/lang/ReflectiveOperationException;
            //  41     50     52     53     Ljava/lang/ReflectiveOperationException;
            //  59     76     120    124    Ljava/lang/ReflectiveOperationException;
            //  88     112    124    128    Ljava/lang/ReflectiveOperationException;
            // 
            // The error that occurred was:
            // 
            // java.lang.IndexOutOfBoundsException: Index 68 out of bounds for length 68
            //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
            //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
            //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
            //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
            //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
            //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
            //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        @NonNull
        @Override
        WindowInsetsCompat build() {
            ((BuilderImpl)this).applyInsetTypes();
            final WindowInsetsCompat windowInsetsCompat = WindowInsetsCompat.toWindowInsetsCompat(this.mPlatformInsets);
            windowInsetsCompat.setOverriddenInsets(super.mInsetsTypeMask);
            windowInsetsCompat.setStableInsets(this.mStableInsets);
            return windowInsetsCompat;
        }
        
        @Override
        void setStableInsets(@Nullable final Insets mStableInsets) {
            this.mStableInsets = mStableInsets;
        }
        
        @Override
        void setSystemWindowInsets(@NonNull final Insets insets) {
            final WindowInsets mPlatformInsets = this.mPlatformInsets;
            if (mPlatformInsets != null) {
                this.mPlatformInsets = mPlatformInsets.replaceSystemWindowInsets(insets.left, insets.top, insets.right, insets.bottom);
            }
        }
    }
    
    @RequiresApi(api = 29)
    private static class BuilderImpl29 extends BuilderImpl
    {
        final WindowInsets$Builder mPlatBuilder;
        
        BuilderImpl29() {
            this.mPlatBuilder = new WindowInsets$Builder();
        }
        
        BuilderImpl29(@NonNull final WindowInsetsCompat windowInsetsCompat) {
            super(windowInsetsCompat);
            final WindowInsets windowInsets = windowInsetsCompat.toWindowInsets();
            WindowInsets$Builder mPlatBuilder;
            if (windowInsets != null) {
                mPlatBuilder = new WindowInsets$Builder(windowInsets);
            }
            else {
                mPlatBuilder = new WindowInsets$Builder();
            }
            this.mPlatBuilder = mPlatBuilder;
        }
        
        @NonNull
        @Override
        WindowInsetsCompat build() {
            ((BuilderImpl)this).applyInsetTypes();
            final WindowInsetsCompat windowInsetsCompat = WindowInsetsCompat.toWindowInsetsCompat(oO8008O.\u3007080(this.mPlatBuilder));
            windowInsetsCompat.setOverriddenInsets(super.mInsetsTypeMask);
            return windowInsetsCompat;
        }
        
        @Override
        void setDisplayCutout(@Nullable final DisplayCutoutCompat displayCutoutCompat) {
            final WindowInsets$Builder mPlatBuilder = this.mPlatBuilder;
            DisplayCutout unwrap;
            if (displayCutoutCompat != null) {
                unwrap = displayCutoutCompat.unwrap();
            }
            else {
                unwrap = null;
            }
            O\u3007\u3007.\u3007080(mPlatBuilder, unwrap);
        }
        
        @Override
        void setMandatorySystemGestureInsets(@NonNull final Insets insets) {
            \u3007008\u3007oo.\u3007080(this.mPlatBuilder, insets.toPlatformInsets());
        }
        
        @Override
        void setStableInsets(@NonNull final Insets insets) {
            \u3007\u30070\u30070o8.\u3007080(this.mPlatBuilder, insets.toPlatformInsets());
        }
        
        @Override
        void setSystemGestureInsets(@NonNull final Insets insets) {
            oo0O\u30070\u3007\u3007\u3007.\u3007080(this.mPlatBuilder, insets.toPlatformInsets());
        }
        
        @Override
        void setSystemWindowInsets(@NonNull final Insets insets) {
            oO8o.\u3007080(this.mPlatBuilder, insets.toPlatformInsets());
        }
        
        @Override
        void setTappableElementInsets(@NonNull final Insets insets) {
            ooo8o\u3007o\u3007.\u3007080(this.mPlatBuilder, insets.toPlatformInsets());
        }
    }
    
    @RequiresApi(30)
    private static class BuilderImpl30 extends BuilderImpl29
    {
        BuilderImpl30() {
        }
        
        BuilderImpl30(@NonNull final WindowInsetsCompat windowInsetsCompat) {
            super(windowInsetsCompat);
        }
        
        @Override
        void setInsets(final int n, @NonNull final Insets insets) {
            \u3007OO8Oo0\u3007.\u3007080(super.mPlatBuilder, TypeImpl30.toPlatformType(n), insets.toPlatformInsets());
        }
        
        @Override
        void setInsetsIgnoringVisibility(final int n, @NonNull final Insets insets) {
            O0\u3007oo.\u3007080(super.mPlatBuilder, TypeImpl30.toPlatformType(n), insets.toPlatformInsets());
        }
        
        @Override
        void setVisible(final int n, final boolean b) {
            o08oOO.\u3007080(super.mPlatBuilder, TypeImpl30.toPlatformType(n), b);
        }
    }
    
    private static class Impl
    {
        @NonNull
        static final WindowInsetsCompat CONSUMED;
        final WindowInsetsCompat mHost;
        
        static {
            CONSUMED = new Builder().build().consumeDisplayCutout().consumeStableInsets().consumeSystemWindowInsets();
        }
        
        Impl(@NonNull final WindowInsetsCompat mHost) {
            this.mHost = mHost;
        }
        
        @NonNull
        WindowInsetsCompat consumeDisplayCutout() {
            return this.mHost;
        }
        
        @NonNull
        WindowInsetsCompat consumeStableInsets() {
            return this.mHost;
        }
        
        @NonNull
        WindowInsetsCompat consumeSystemWindowInsets() {
            return this.mHost;
        }
        
        void copyRootViewBounds(@NonNull final View view) {
        }
        
        void copyWindowDataInto(@NonNull final WindowInsetsCompat windowInsetsCompat) {
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (!(o instanceof Impl)) {
                return false;
            }
            final Impl impl = (Impl)o;
            if (this.isRound() != impl.isRound() || this.isConsumed() != impl.isConsumed() || !ObjectsCompat.equals(this.getSystemWindowInsets(), impl.getSystemWindowInsets()) || !ObjectsCompat.equals(this.getStableInsets(), impl.getStableInsets()) || !ObjectsCompat.equals(this.getDisplayCutout(), impl.getDisplayCutout())) {
                b = false;
            }
            return b;
        }
        
        @Nullable
        DisplayCutoutCompat getDisplayCutout() {
            return null;
        }
        
        @NonNull
        Insets getInsets(final int n) {
            return Insets.NONE;
        }
        
        @NonNull
        Insets getInsetsIgnoringVisibility(final int n) {
            if ((n & 0x8) == 0x0) {
                return Insets.NONE;
            }
            throw new IllegalArgumentException("Unable to query the maximum insets for IME");
        }
        
        @NonNull
        Insets getMandatorySystemGestureInsets() {
            return this.getSystemWindowInsets();
        }
        
        @NonNull
        Insets getStableInsets() {
            return Insets.NONE;
        }
        
        @NonNull
        Insets getSystemGestureInsets() {
            return this.getSystemWindowInsets();
        }
        
        @NonNull
        Insets getSystemWindowInsets() {
            return Insets.NONE;
        }
        
        @NonNull
        Insets getTappableElementInsets() {
            return this.getSystemWindowInsets();
        }
        
        @Override
        public int hashCode() {
            return ObjectsCompat.hash(this.isRound(), this.isConsumed(), this.getSystemWindowInsets(), this.getStableInsets(), this.getDisplayCutout());
        }
        
        @NonNull
        WindowInsetsCompat inset(final int n, final int n2, final int n3, final int n4) {
            return Impl.CONSUMED;
        }
        
        boolean isConsumed() {
            return false;
        }
        
        boolean isRound() {
            return false;
        }
        
        boolean isVisible(final int n) {
            return true;
        }
        
        public void setOverriddenInsets(final Insets[] array) {
        }
        
        void setRootViewData(@NonNull final Insets insets) {
        }
        
        void setRootWindowInsets(@Nullable final WindowInsetsCompat windowInsetsCompat) {
        }
        
        public void setStableInsets(final Insets insets) {
        }
    }
    
    @RequiresApi(20)
    private static class Impl20 extends Impl
    {
        private static Class<?> sAttachInfoClass;
        private static Field sAttachInfoField;
        private static Method sGetViewRootImplMethod;
        private static Field sVisibleInsetsField;
        private static boolean sVisibleRectReflectionFetched = false;
        private Insets[] mOverriddenInsets;
        @NonNull
        final WindowInsets mPlatformInsets;
        Insets mRootViewVisibleInsets;
        private WindowInsetsCompat mRootWindowInsets;
        private Insets mSystemWindowInsets;
        
        Impl20(@NonNull final WindowInsetsCompat windowInsetsCompat, @NonNull final WindowInsets mPlatformInsets) {
            super(windowInsetsCompat);
            this.mSystemWindowInsets = null;
            this.mPlatformInsets = mPlatformInsets;
        }
        
        Impl20(@NonNull final WindowInsetsCompat windowInsetsCompat, @NonNull final Impl20 impl20) {
            this(windowInsetsCompat, new WindowInsets(impl20.mPlatformInsets));
        }
        
        @SuppressLint({ "WrongConstant" })
        @NonNull
        private Insets getInsets(final int n, final boolean b) {
            Insets insets = Insets.NONE;
            for (int i = 1; i <= 256; i <<= 1) {
                if ((n & i) != 0x0) {
                    insets = Insets.max(insets, this.getInsetsForType(i, b));
                }
            }
            return insets;
        }
        
        private Insets getRootStableInsets() {
            final WindowInsetsCompat mRootWindowInsets = this.mRootWindowInsets;
            if (mRootWindowInsets != null) {
                return mRootWindowInsets.getStableInsets();
            }
            return Insets.NONE;
        }
        
        @Nullable
        private Insets getVisibleInsets(@NonNull final View obj) {
            if (Build$VERSION.SDK_INT < 30) {
                if (!Impl20.sVisibleRectReflectionFetched) {
                    loadReflectionField();
                }
                final Method sGetViewRootImplMethod = Impl20.sGetViewRootImplMethod;
                final Insets insets = null;
                if (sGetViewRootImplMethod != null && Impl20.sAttachInfoClass != null) {
                    if (Impl20.sVisibleInsetsField != null) {
                        try {
                            final Object invoke = sGetViewRootImplMethod.invoke(obj, new Object[0]);
                            if (invoke == null) {
                                return null;
                            }
                            final Rect rect = (Rect)Impl20.sVisibleInsetsField.get(Impl20.sAttachInfoField.get(invoke));
                            Insets of = insets;
                            if (rect != null) {
                                of = Insets.of(rect);
                            }
                            return of;
                        }
                        catch (final ReflectiveOperationException ex) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Failed to get visible insets. (Reflection error). ");
                            sb.append(ex.getMessage());
                        }
                    }
                }
                return null;
            }
            throw new UnsupportedOperationException("getVisibleInsets() should not be called on API >= 30. Use WindowInsets.isVisible() instead.");
        }
        
        @SuppressLint({ "PrivateApi" })
        private static void loadReflectionField() {
            try {
                Impl20.sGetViewRootImplMethod = View.class.getDeclaredMethod("getViewRootImpl", (Class<?>[])new Class[0]);
                Impl20.sVisibleInsetsField = (Impl20.sAttachInfoClass = Class.forName("android.view.View$AttachInfo")).getDeclaredField("mVisibleInsets");
                Impl20.sAttachInfoField = Class.forName("android.view.ViewRootImpl").getDeclaredField("mAttachInfo");
                Impl20.sVisibleInsetsField.setAccessible(true);
                Impl20.sAttachInfoField.setAccessible(true);
            }
            catch (final ReflectiveOperationException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to get visible insets. (Reflection error). ");
                sb.append(ex.getMessage());
            }
            Impl20.sVisibleRectReflectionFetched = true;
        }
        
        @Override
        void copyRootViewBounds(@NonNull final View view) {
            Insets rootViewData;
            if ((rootViewData = this.getVisibleInsets(view)) == null) {
                rootViewData = Insets.NONE;
            }
            this.setRootViewData(rootViewData);
        }
        
        @Override
        void copyWindowDataInto(@NonNull final WindowInsetsCompat windowInsetsCompat) {
            windowInsetsCompat.setRootWindowInsets(this.mRootWindowInsets);
            windowInsetsCompat.setRootViewData(this.mRootViewVisibleInsets);
        }
        
        @Override
        public boolean equals(final Object o) {
            return super.equals(o) && Objects.equals(this.mRootViewVisibleInsets, ((Impl20)o).mRootViewVisibleInsets);
        }
        
        @NonNull
        public Insets getInsets(final int n) {
            return this.getInsets(n, false);
        }
        
        @NonNull
        protected Insets getInsetsForType(int n, final boolean b) {
            if (n != 1) {
                Insets stableInsets = null;
                final Insets insets = null;
                if (n != 2) {
                    if (n != 8) {
                        if (n == 16) {
                            return ((Impl)this).getSystemGestureInsets();
                        }
                        if (n == 32) {
                            return ((Impl)this).getMandatorySystemGestureInsets();
                        }
                        if (n == 64) {
                            return ((Impl)this).getTappableElementInsets();
                        }
                        if (n != 128) {
                            return Insets.NONE;
                        }
                        final WindowInsetsCompat mRootWindowInsets = this.mRootWindowInsets;
                        DisplayCutoutCompat displayCutoutCompat;
                        if (mRootWindowInsets != null) {
                            displayCutoutCompat = mRootWindowInsets.getDisplayCutout();
                        }
                        else {
                            displayCutoutCompat = ((Impl)this).getDisplayCutout();
                        }
                        if (displayCutoutCompat != null) {
                            return Insets.of(displayCutoutCompat.getSafeInsetLeft(), displayCutoutCompat.getSafeInsetTop(), displayCutoutCompat.getSafeInsetRight(), displayCutoutCompat.getSafeInsetBottom());
                        }
                        return Insets.NONE;
                    }
                    else {
                        final Insets[] mOverriddenInsets = this.mOverriddenInsets;
                        Insets insets2 = insets;
                        if (mOverriddenInsets != null) {
                            insets2 = mOverriddenInsets[Type.indexOf(8)];
                        }
                        if (insets2 != null) {
                            return insets2;
                        }
                        final Insets systemWindowInsets = this.getSystemWindowInsets();
                        final Insets rootStableInsets = this.getRootStableInsets();
                        n = systemWindowInsets.bottom;
                        if (n > rootStableInsets.bottom) {
                            return Insets.of(0, 0, 0, n);
                        }
                        final Insets mRootViewVisibleInsets = this.mRootViewVisibleInsets;
                        if (mRootViewVisibleInsets != null && !mRootViewVisibleInsets.equals(Insets.NONE)) {
                            n = this.mRootViewVisibleInsets.bottom;
                            if (n > rootStableInsets.bottom) {
                                return Insets.of(0, 0, 0, n);
                            }
                        }
                        return Insets.NONE;
                    }
                }
                else {
                    if (b) {
                        final Insets rootStableInsets2 = this.getRootStableInsets();
                        final Insets stableInsets2 = ((Impl)this).getStableInsets();
                        return Insets.of(Math.max(rootStableInsets2.left, stableInsets2.left), 0, Math.max(rootStableInsets2.right, stableInsets2.right), Math.max(rootStableInsets2.bottom, stableInsets2.bottom));
                    }
                    final Insets systemWindowInsets2 = this.getSystemWindowInsets();
                    final WindowInsetsCompat mRootWindowInsets2 = this.mRootWindowInsets;
                    if (mRootWindowInsets2 != null) {
                        stableInsets = mRootWindowInsets2.getStableInsets();
                    }
                    final int a = n = systemWindowInsets2.bottom;
                    if (stableInsets != null) {
                        n = Math.min(a, stableInsets.bottom);
                    }
                    return Insets.of(systemWindowInsets2.left, 0, systemWindowInsets2.right, n);
                }
            }
            else {
                if (b) {
                    return Insets.of(0, Math.max(this.getRootStableInsets().top, this.getSystemWindowInsets().top), 0, 0);
                }
                return Insets.of(0, this.getSystemWindowInsets().top, 0, 0);
            }
        }
        
        @NonNull
        public Insets getInsetsIgnoringVisibility(final int n) {
            return this.getInsets(n, true);
        }
        
        @NonNull
        @Override
        final Insets getSystemWindowInsets() {
            if (this.mSystemWindowInsets == null) {
                this.mSystemWindowInsets = Insets.of(this.mPlatformInsets.getSystemWindowInsetLeft(), this.mPlatformInsets.getSystemWindowInsetTop(), this.mPlatformInsets.getSystemWindowInsetRight(), this.mPlatformInsets.getSystemWindowInsetBottom());
            }
            return this.mSystemWindowInsets;
        }
        
        @NonNull
        @Override
        WindowInsetsCompat inset(final int n, final int n2, final int n3, final int n4) {
            final Builder builder = new Builder(WindowInsetsCompat.toWindowInsetsCompat(this.mPlatformInsets));
            builder.setSystemWindowInsets(WindowInsetsCompat.insetInsets(this.getSystemWindowInsets(), n, n2, n3, n4));
            builder.setStableInsets(WindowInsetsCompat.insetInsets(((Impl)this).getStableInsets(), n, n2, n3, n4));
            return builder.build();
        }
        
        @Override
        boolean isRound() {
            return this.mPlatformInsets.isRound();
        }
        
        protected boolean isTypeVisible(final int n) {
            if (n != 1 && n != 2) {
                if (n == 4) {
                    return false;
                }
                if (n != 8 && n != 128) {
                    return true;
                }
            }
            return this.getInsetsForType(n, false).equals(Insets.NONE) ^ true;
        }
        
        @SuppressLint({ "WrongConstant" })
        @Override
        boolean isVisible(final int n) {
            for (int i = 1; i <= 256; i <<= 1) {
                if ((n & i) != 0x0) {
                    if (!this.isTypeVisible(i)) {
                        return false;
                    }
                }
            }
            return true;
        }
        
        @Override
        public void setOverriddenInsets(final Insets[] mOverriddenInsets) {
            this.mOverriddenInsets = mOverriddenInsets;
        }
        
        @Override
        void setRootViewData(@NonNull final Insets mRootViewVisibleInsets) {
            this.mRootViewVisibleInsets = mRootViewVisibleInsets;
        }
        
        @Override
        void setRootWindowInsets(@Nullable final WindowInsetsCompat mRootWindowInsets) {
            this.mRootWindowInsets = mRootWindowInsets;
        }
    }
    
    @RequiresApi(21)
    private static class Impl21 extends Impl20
    {
        private Insets mStableInsets;
        
        Impl21(@NonNull final WindowInsetsCompat windowInsetsCompat, @NonNull final WindowInsets windowInsets) {
            super(windowInsetsCompat, windowInsets);
            this.mStableInsets = null;
        }
        
        Impl21(@NonNull final WindowInsetsCompat windowInsetsCompat, @NonNull final Impl21 impl21) {
            super(windowInsetsCompat, (Impl20)impl21);
            this.mStableInsets = null;
            this.mStableInsets = impl21.mStableInsets;
        }
        
        @NonNull
        @Override
        WindowInsetsCompat consumeStableInsets() {
            return WindowInsetsCompat.toWindowInsetsCompat(super.mPlatformInsets.consumeStableInsets());
        }
        
        @NonNull
        @Override
        WindowInsetsCompat consumeSystemWindowInsets() {
            return WindowInsetsCompat.toWindowInsetsCompat(super.mPlatformInsets.consumeSystemWindowInsets());
        }
        
        @NonNull
        @Override
        final Insets getStableInsets() {
            if (this.mStableInsets == null) {
                this.mStableInsets = Insets.of(super.mPlatformInsets.getStableInsetLeft(), super.mPlatformInsets.getStableInsetTop(), super.mPlatformInsets.getStableInsetRight(), super.mPlatformInsets.getStableInsetBottom());
            }
            return this.mStableInsets;
        }
        
        @Override
        boolean isConsumed() {
            return super.mPlatformInsets.isConsumed();
        }
        
        @Override
        public void setStableInsets(@Nullable final Insets mStableInsets) {
            this.mStableInsets = mStableInsets;
        }
    }
    
    @RequiresApi(28)
    private static class Impl28 extends Impl21
    {
        Impl28(@NonNull final WindowInsetsCompat windowInsetsCompat, @NonNull final WindowInsets windowInsets) {
            super(windowInsetsCompat, windowInsets);
        }
        
        Impl28(@NonNull final WindowInsetsCompat windowInsetsCompat, @NonNull final Impl28 impl28) {
            super(windowInsetsCompat, (Impl21)impl28);
        }
        
        @NonNull
        @Override
        WindowInsetsCompat consumeDisplayCutout() {
            return WindowInsetsCompat.toWindowInsetsCompat(o88O8.\u3007080(super.mPlatformInsets));
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (!(o instanceof Impl28)) {
                return false;
            }
            final Impl28 impl28 = (Impl28)o;
            if (!Objects.equals(super.mPlatformInsets, impl28.mPlatformInsets) || !Objects.equals(super.mRootViewVisibleInsets, impl28.mRootViewVisibleInsets)) {
                b = false;
            }
            return b;
        }
        
        @Nullable
        @Override
        DisplayCutoutCompat getDisplayCutout() {
            return DisplayCutoutCompat.wrap(o88O\u30078.\u3007080(super.mPlatformInsets));
        }
        
        @Override
        public int hashCode() {
            return super.mPlatformInsets.hashCode();
        }
    }
    
    @RequiresApi(29)
    private static class Impl29 extends Impl28
    {
        private Insets mMandatorySystemGestureInsets;
        private Insets mSystemGestureInsets;
        private Insets mTappableElementInsets;
        
        Impl29(@NonNull final WindowInsetsCompat windowInsetsCompat, @NonNull final WindowInsets windowInsets) {
            super(windowInsetsCompat, windowInsets);
            this.mSystemGestureInsets = null;
            this.mMandatorySystemGestureInsets = null;
            this.mTappableElementInsets = null;
        }
        
        Impl29(@NonNull final WindowInsetsCompat windowInsetsCompat, @NonNull final Impl29 impl29) {
            super(windowInsetsCompat, (Impl28)impl29);
            this.mSystemGestureInsets = null;
            this.mMandatorySystemGestureInsets = null;
            this.mTappableElementInsets = null;
        }
        
        @NonNull
        @Override
        Insets getMandatorySystemGestureInsets() {
            if (this.mMandatorySystemGestureInsets == null) {
                this.mMandatorySystemGestureInsets = Insets.toCompatInsets(\u30078O0O808\u3007.\u3007080(super.mPlatformInsets));
            }
            return this.mMandatorySystemGestureInsets;
        }
        
        @NonNull
        @Override
        Insets getSystemGestureInsets() {
            if (this.mSystemGestureInsets == null) {
                this.mSystemGestureInsets = Insets.toCompatInsets(\u3007\u300700O\u30070o.\u3007080(super.mPlatformInsets));
            }
            return this.mSystemGestureInsets;
        }
        
        @NonNull
        @Override
        Insets getTappableElementInsets() {
            if (this.mTappableElementInsets == null) {
                this.mTappableElementInsets = Insets.toCompatInsets(O8888.\u3007080(super.mPlatformInsets));
            }
            return this.mTappableElementInsets;
        }
        
        @NonNull
        @Override
        WindowInsetsCompat inset(final int n, final int n2, final int n3, final int n4) {
            return WindowInsetsCompat.toWindowInsetsCompat(OOo88OOo.\u3007080(super.mPlatformInsets, n, n2, n3, n4));
        }
        
        @Override
        public void setStableInsets(@Nullable final Insets insets) {
        }
    }
    
    @RequiresApi(30)
    private static class Impl30 extends Impl29
    {
        @NonNull
        static final WindowInsetsCompat CONSUMED;
        
        static {
            CONSUMED = WindowInsetsCompat.toWindowInsetsCompat(O\u3007oO\u3007oo8o.\u3007080());
        }
        
        Impl30(@NonNull final WindowInsetsCompat windowInsetsCompat, @NonNull final WindowInsets windowInsets) {
            super(windowInsetsCompat, windowInsets);
        }
        
        Impl30(@NonNull final WindowInsetsCompat windowInsetsCompat, @NonNull final Impl30 impl30) {
            super(windowInsetsCompat, (Impl29)impl30);
        }
        
        @Override
        final void copyRootViewBounds(@NonNull final View view) {
        }
        
        @NonNull
        @Override
        public Insets getInsets(final int n) {
            return Insets.toCompatInsets(o08O.\u3007080(super.mPlatformInsets, TypeImpl30.toPlatformType(n)));
        }
        
        @NonNull
        @Override
        public Insets getInsetsIgnoringVisibility(final int n) {
            return Insets.toCompatInsets(O88\u3007\u3007o0O.\u3007080(super.mPlatformInsets, TypeImpl30.toPlatformType(n)));
        }
        
        public boolean isVisible(final int n) {
            return o08\u3007\u30070O.\u3007080(super.mPlatformInsets, TypeImpl30.toPlatformType(n));
        }
    }
    
    public static final class Type
    {
        static final int CAPTION_BAR = 4;
        static final int DISPLAY_CUTOUT = 128;
        static final int FIRST = 1;
        static final int IME = 8;
        static final int LAST = 256;
        static final int MANDATORY_SYSTEM_GESTURES = 32;
        static final int NAVIGATION_BARS = 2;
        static final int SIZE = 9;
        static final int STATUS_BARS = 1;
        static final int SYSTEM_GESTURES = 16;
        static final int TAPPABLE_ELEMENT = 64;
        static final int WINDOW_DECOR = 256;
        
        private Type() {
        }
        
        @SuppressLint({ "WrongConstant" })
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        static int all() {
            return -1;
        }
        
        public static int captionBar() {
            return 4;
        }
        
        public static int displayCutout() {
            return 128;
        }
        
        public static int ime() {
            return 8;
        }
        
        static int indexOf(final int i) {
            if (i == 1) {
                return 0;
            }
            if (i == 2) {
                return 1;
            }
            if (i == 4) {
                return 2;
            }
            if (i == 8) {
                return 3;
            }
            if (i == 16) {
                return 4;
            }
            if (i == 32) {
                return 5;
            }
            if (i == 64) {
                return 6;
            }
            if (i == 128) {
                return 7;
            }
            if (i == 256) {
                return 8;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("type needs to be >= FIRST and <= LAST, type=");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        }
        
        public static int mandatorySystemGestures() {
            return 32;
        }
        
        public static int navigationBars() {
            return 2;
        }
        
        public static int statusBars() {
            return 1;
        }
        
        public static int systemBars() {
            return 7;
        }
        
        public static int systemGestures() {
            return 16;
        }
        
        public static int tappableElement() {
            return 64;
        }
        
        @Retention(RetentionPolicy.SOURCE)
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public @interface InsetsType {
        }
    }
    
    @RequiresApi(30)
    private static final class TypeImpl30
    {
        static int toPlatformType(final int n) {
            int n2 = 0;
            int n3;
            for (int i = 1; i <= 256; i <<= 1, n2 = n3) {
                n3 = n2;
                if ((n & i) != 0x0) {
                    int n4;
                    if (i != 1) {
                        if (i != 2) {
                            if (i != 4) {
                                if (i != 8) {
                                    if (i != 16) {
                                        if (i != 32) {
                                            if (i != 64) {
                                                if (i != 128) {
                                                    n3 = n2;
                                                    continue;
                                                }
                                                n4 = o\u30070o\u3007\u3007.\u3007080();
                                            }
                                            else {
                                                n4 = \u3007\u3007\u3007.\u3007080();
                                            }
                                        }
                                        else {
                                            n4 = O\u3007.\u3007080();
                                        }
                                    }
                                    else {
                                        n4 = O80\u3007O\u3007080.\u3007080();
                                    }
                                }
                                else {
                                    n4 = \u3007008\u3007o0\u3007\u3007.\u3007080();
                                }
                            }
                            else {
                                n4 = \u3007080O0.\u3007080();
                            }
                        }
                        else {
                            n4 = OoOOo8.\u3007080();
                        }
                    }
                    else {
                        n4 = O88o\u3007.\u3007080();
                    }
                    n3 = (n2 | n4);
                }
            }
            return n2;
        }
    }
}

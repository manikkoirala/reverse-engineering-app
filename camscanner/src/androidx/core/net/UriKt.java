// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.net;

import kotlin.jvm.internal.Intrinsics;
import java.io.File;
import org.jetbrains.annotations.NotNull;
import android.net.Uri;
import kotlin.Metadata;

@Metadata
public final class UriKt
{
    @NotNull
    public static final File toFile(@NotNull final Uri uri) {
        Intrinsics.checkNotNullParameter((Object)uri, "<this>");
        if (!Intrinsics.\u3007o\u3007((Object)uri.getScheme(), (Object)"file")) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Uri lacks 'file' scheme: ");
            sb.append(uri);
            throw new IllegalArgumentException(sb.toString().toString());
        }
        final String path = uri.getPath();
        if (path != null) {
            return new File(path);
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Uri path is null: ");
        sb2.append(uri);
        throw new IllegalArgumentException(sb2.toString().toString());
    }
    
    @NotNull
    public static final Uri toUri(@NotNull final File file) {
        Intrinsics.checkNotNullParameter((Object)file, "<this>");
        final Uri fromFile = Uri.fromFile(file);
        Intrinsics.checkNotNullExpressionValue((Object)fromFile, "fromFile(this)");
        return fromFile;
    }
    
    @NotNull
    public static final Uri toUri(@NotNull final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "<this>");
        final Uri parse = Uri.parse(s);
        Intrinsics.checkNotNullExpressionValue((Object)parse, "parse(this)");
        return parse;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.net;

import java.util.Iterator;
import java.util.Map;
import java.util.Locale;
import androidx.core.util.Preconditions;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.net.Uri;
import java.util.HashMap;

public final class MailTo
{
    private static final String BCC = "bcc";
    private static final String BODY = "body";
    private static final String CC = "cc";
    private static final String MAILTO = "mailto";
    public static final String MAILTO_SCHEME = "mailto:";
    private static final String SUBJECT = "subject";
    private static final String TO = "to";
    private HashMap<String, String> mHeaders;
    
    private MailTo() {
        this.mHeaders = new HashMap<String, String>();
    }
    
    public static boolean isMailTo(@Nullable final Uri uri) {
        return uri != null && "mailto".equals(uri.getScheme());
    }
    
    public static boolean isMailTo(@Nullable final String s) {
        return s != null && s.startsWith("mailto:");
    }
    
    @NonNull
    public static MailTo parse(@NonNull final Uri uri) throws ParseException {
        return parse(uri.toString());
    }
    
    @NonNull
    public static MailTo parse(@NonNull String str) throws ParseException {
        Preconditions.checkNotNull(str);
        if (isMailTo(str)) {
            final int index = str.indexOf(35);
            String substring = str;
            if (index != -1) {
                substring = str.substring(0, index);
            }
            final int index2 = substring.indexOf(63);
            String substring2;
            if (index2 == -1) {
                str = Uri.decode(substring.substring(7));
                substring2 = null;
            }
            else {
                str = Uri.decode(substring.substring(7, index2));
                substring2 = substring.substring(index2 + 1);
            }
            final MailTo mailTo = new MailTo();
            if (substring2 != null) {
                final String[] split = substring2.split("&");
                for (int length = split.length, i = 0; i < length; ++i) {
                    final String[] split2 = split[i].split("=", 2);
                    if (split2.length != 0) {
                        final String lowerCase = Uri.decode(split2[0]).toLowerCase(Locale.ROOT);
                        String decode;
                        if (split2.length > 1) {
                            decode = Uri.decode(split2[1]);
                        }
                        else {
                            decode = null;
                        }
                        mailTo.mHeaders.put(lowerCase, decode);
                    }
                }
            }
            final String to = mailTo.getTo();
            String string = str;
            if (to != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(", ");
                sb.append(to);
                string = sb.toString();
            }
            mailTo.mHeaders.put("to", string);
            return mailTo;
        }
        throw new ParseException("Not a mailto scheme");
    }
    
    @Nullable
    public String getBcc() {
        return this.mHeaders.get("bcc");
    }
    
    @Nullable
    public String getBody() {
        return this.mHeaders.get("body");
    }
    
    @Nullable
    public String getCc() {
        return this.mHeaders.get("cc");
    }
    
    @Nullable
    public Map<String, String> getHeaders() {
        return this.mHeaders;
    }
    
    @Nullable
    public String getSubject() {
        return this.mHeaders.get("subject");
    }
    
    @Nullable
    public String getTo() {
        return this.mHeaders.get("to");
    }
    
    @NonNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("mailto:");
        sb.append('?');
        for (final Map.Entry<String, V> entry : this.mHeaders.entrySet()) {
            sb.append(Uri.encode((String)entry.getKey()));
            sb.append('=');
            sb.append(Uri.encode((String)entry.getValue()));
            sb.append('&');
        }
        return sb.toString();
    }
}

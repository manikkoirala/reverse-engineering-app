// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.net;

import androidx.annotation.NonNull;

public class ParseException extends RuntimeException
{
    @NonNull
    public final String response;
    
    ParseException(@NonNull final String s) {
        super(s);
        this.response = s;
    }
}

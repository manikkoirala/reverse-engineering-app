// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.net;

import androidx.annotation.RestrictTo;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.os.Build$VERSION;
import androidx.annotation.RequiresPermission;
import androidx.annotation.Nullable;
import android.annotation.SuppressLint;
import android.net.NetworkInfo;
import android.content.Intent;
import androidx.annotation.NonNull;
import android.net.ConnectivityManager;

public final class ConnectivityManagerCompat
{
    public static final int RESTRICT_BACKGROUND_STATUS_DISABLED = 1;
    public static final int RESTRICT_BACKGROUND_STATUS_ENABLED = 3;
    public static final int RESTRICT_BACKGROUND_STATUS_WHITELISTED = 2;
    
    private ConnectivityManagerCompat() {
    }
    
    @SuppressLint({ "ReferencesDeprecated" })
    @Nullable
    @RequiresPermission("android.permission.ACCESS_NETWORK_STATE")
    public static NetworkInfo getNetworkInfoFromBroadcast(@NonNull final ConnectivityManager connectivityManager, @NonNull final Intent intent) {
        final NetworkInfo networkInfo = (NetworkInfo)intent.getParcelableExtra("networkInfo");
        if (networkInfo != null) {
            return connectivityManager.getNetworkInfo(networkInfo.getType());
        }
        return null;
    }
    
    public static int getRestrictBackgroundStatus(@NonNull final ConnectivityManager connectivityManager) {
        if (Build$VERSION.SDK_INT >= 24) {
            return Api24Impl.getRestrictBackgroundStatus(connectivityManager);
        }
        return 3;
    }
    
    @RequiresPermission("android.permission.ACCESS_NETWORK_STATE")
    public static boolean isActiveNetworkMetered(@NonNull final ConnectivityManager connectivityManager) {
        return Api16Impl.isActiveNetworkMetered(connectivityManager);
    }
    
    @RequiresApi(16)
    static class Api16Impl
    {
        private Api16Impl() {
        }
        
        @DoNotInline
        @RequiresPermission("android.permission.ACCESS_NETWORK_STATE")
        static boolean isActiveNetworkMetered(final ConnectivityManager connectivityManager) {
            return connectivityManager.isActiveNetworkMetered();
        }
    }
    
    @RequiresApi(24)
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        @DoNotInline
        static int getRestrictBackgroundStatus(final ConnectivityManager connectivityManager) {
            return \u3007080.\u3007080(connectivityManager);
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public @interface RestrictBackgroundStatus {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.animation;

import androidx.annotation.RequiresApi;
import android.animation.Animator$AnimatorPauseListener;
import kotlin.jvm.internal.Intrinsics;
import android.animation.Animator$AnimatorListener;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import org.jetbrains.annotations.NotNull;
import android.animation.Animator;
import kotlin.Metadata;

@Metadata
public final class AnimatorKt
{
    @NotNull
    public static final Animator$AnimatorListener addListener(@NotNull final Animator animator, @NotNull final Function1<? super Animator, Unit> function1, @NotNull final Function1<? super Animator, Unit> function2, @NotNull final Function1<? super Animator, Unit> function3, @NotNull final Function1<? super Animator, Unit> function4) {
        Intrinsics.checkNotNullParameter((Object)animator, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "onEnd");
        Intrinsics.checkNotNullParameter((Object)function2, "onStart");
        Intrinsics.checkNotNullParameter((Object)function3, "onCancel");
        Intrinsics.checkNotNullParameter((Object)function4, "onRepeat");
        final AnimatorKt$addListener$listener.AnimatorKt$addListener$listener$1 animatorKt$addListener$listener$1 = new AnimatorKt$addListener$listener.AnimatorKt$addListener$listener$1((Function1)function4, (Function1)function1, (Function1)function3, (Function1)function2);
        animator.addListener((Animator$AnimatorListener)animatorKt$addListener$listener$1);
        return (Animator$AnimatorListener)animatorKt$addListener$listener$1;
    }
    
    @RequiresApi(19)
    @NotNull
    public static final Animator$AnimatorPauseListener addPauseListener(@NotNull final Animator animator, @NotNull final Function1<? super Animator, Unit> function1, @NotNull final Function1<? super Animator, Unit> function2) {
        Intrinsics.checkNotNullParameter((Object)animator, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "onResume");
        Intrinsics.checkNotNullParameter((Object)function2, "onPause");
        final AnimatorKt$addPauseListener$listener.AnimatorKt$addPauseListener$listener$1 animatorKt$addPauseListener$listener$1 = new AnimatorKt$addPauseListener$listener.AnimatorKt$addPauseListener$listener$1((Function1)function2, (Function1)function1);
        Api19Impl.addPauseListener(animator, (Animator$AnimatorPauseListener)animatorKt$addPauseListener$listener$1);
        return (Animator$AnimatorPauseListener)animatorKt$addPauseListener$listener$1;
    }
    
    public static /* synthetic */ Animator$AnimatorPauseListener addPauseListener$default(final Animator animator, Function1 instance, Function1 instance2, final int n, final Object o) {
        if ((n & 0x1) != 0x0) {
            instance = (Function1)AnimatorKt$addPauseListener.AnimatorKt$addPauseListener$1.INSTANCE;
        }
        if ((n & 0x2) != 0x0) {
            instance2 = (Function1)AnimatorKt$addPauseListener.AnimatorKt$addPauseListener$2.INSTANCE;
        }
        return addPauseListener(animator, (Function1<? super Animator, Unit>)instance, (Function1<? super Animator, Unit>)instance2);
    }
    
    @NotNull
    public static final Animator$AnimatorListener doOnCancel(@NotNull final Animator animator, @NotNull final Function1<? super Animator, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)animator, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "action");
        final AnimatorKt$doOnCancel$$inlined$addListener$default.AnimatorKt$doOnCancel$$inlined$addListener$default$1 animatorKt$doOnCancel$$inlined$addListener$default$1 = new AnimatorKt$doOnCancel$$inlined$addListener$default.AnimatorKt$doOnCancel$$inlined$addListener$default$1((Function1)function1);
        animator.addListener((Animator$AnimatorListener)animatorKt$doOnCancel$$inlined$addListener$default$1);
        return (Animator$AnimatorListener)animatorKt$doOnCancel$$inlined$addListener$default$1;
    }
    
    @NotNull
    public static final Animator$AnimatorListener doOnEnd(@NotNull final Animator animator, @NotNull final Function1<? super Animator, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)animator, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "action");
        final AnimatorKt$doOnEnd$$inlined$addListener$default.AnimatorKt$doOnEnd$$inlined$addListener$default$1 animatorKt$doOnEnd$$inlined$addListener$default$1 = new AnimatorKt$doOnEnd$$inlined$addListener$default.AnimatorKt$doOnEnd$$inlined$addListener$default$1((Function1)function1);
        animator.addListener((Animator$AnimatorListener)animatorKt$doOnEnd$$inlined$addListener$default$1);
        return (Animator$AnimatorListener)animatorKt$doOnEnd$$inlined$addListener$default$1;
    }
    
    @RequiresApi(19)
    @NotNull
    public static final Animator$AnimatorPauseListener doOnPause(@NotNull final Animator animator, @NotNull final Function1<? super Animator, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)animator, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "action");
        return addPauseListener$default(animator, null, function1, 1, null);
    }
    
    @NotNull
    public static final Animator$AnimatorListener doOnRepeat(@NotNull final Animator animator, @NotNull final Function1<? super Animator, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)animator, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "action");
        final AnimatorKt$doOnRepeat$$inlined$addListener$default.AnimatorKt$doOnRepeat$$inlined$addListener$default$1 animatorKt$doOnRepeat$$inlined$addListener$default$1 = new AnimatorKt$doOnRepeat$$inlined$addListener$default.AnimatorKt$doOnRepeat$$inlined$addListener$default$1((Function1)function1);
        animator.addListener((Animator$AnimatorListener)animatorKt$doOnRepeat$$inlined$addListener$default$1);
        return (Animator$AnimatorListener)animatorKt$doOnRepeat$$inlined$addListener$default$1;
    }
    
    @RequiresApi(19)
    @NotNull
    public static final Animator$AnimatorPauseListener doOnResume(@NotNull final Animator animator, @NotNull final Function1<? super Animator, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)animator, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "action");
        return addPauseListener$default(animator, function1, null, 2, null);
    }
    
    @NotNull
    public static final Animator$AnimatorListener doOnStart(@NotNull final Animator animator, @NotNull final Function1<? super Animator, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)animator, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "action");
        final AnimatorKt$doOnStart$$inlined$addListener$default.AnimatorKt$doOnStart$$inlined$addListener$default$1 animatorKt$doOnStart$$inlined$addListener$default$1 = new AnimatorKt$doOnStart$$inlined$addListener$default.AnimatorKt$doOnStart$$inlined$addListener$default$1((Function1)function1);
        animator.addListener((Animator$AnimatorListener)animatorKt$doOnStart$$inlined$addListener$default$1);
        return (Animator$AnimatorListener)animatorKt$doOnStart$$inlined$addListener$default$1;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.core.animation;

import androidx.annotation.DoNotInline;
import kotlin.jvm.internal.Intrinsics;
import android.animation.Animator$AnimatorPauseListener;
import android.animation.Animator;
import org.jetbrains.annotations.NotNull;
import androidx.annotation.RequiresApi;
import kotlin.Metadata;

@Metadata
@RequiresApi(19)
final class Api19Impl
{
    @NotNull
    public static final Api19Impl INSTANCE;
    
    static {
        INSTANCE = new Api19Impl();
    }
    
    private Api19Impl() {
    }
    
    @DoNotInline
    public static final void addPauseListener(@NotNull final Animator animator, @NotNull final Animator$AnimatorPauseListener animator$AnimatorPauseListener) {
        Intrinsics.checkNotNullParameter((Object)animator, "animator");
        Intrinsics.checkNotNullParameter((Object)animator$AnimatorPauseListener, "listener");
        animator.addPauseListener(animator$AnimatorPauseListener);
    }
}

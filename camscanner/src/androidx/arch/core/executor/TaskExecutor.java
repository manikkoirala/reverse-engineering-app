// 
// Decompiled by Procyon v0.6.0
// 

package androidx.arch.core.executor;

import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public abstract class TaskExecutor
{
    public abstract void executeOnDiskIO(@NonNull final Runnable p0);
    
    public void executeOnMainThread(@NonNull final Runnable runnable) {
        if (this.isMainThread()) {
            runnable.run();
        }
        else {
            this.postToMainThread(runnable);
        }
    }
    
    public abstract boolean isMainThread();
    
    public abstract void postToMainThread(@NonNull final Runnable p0);
}

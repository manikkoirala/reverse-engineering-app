// 
// Decompiled by Procyon v0.6.0
// 

package androidx.arch.core.util;

public interface Function<I, O>
{
    O apply(final I p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.animation.TypeConverter;
import android.animation.ObjectAnimator;
import android.graphics.Path;
import android.graphics.PointF;
import android.util.Property;

class ObjectAnimatorUtils
{
    private ObjectAnimatorUtils() {
    }
    
    static <T> ObjectAnimator ofPointF(final T t, final Property<T, PointF> property, final Path path) {
        return ObjectAnimator.ofObject((Object)t, (Property)property, (TypeConverter)null, path);
    }
}

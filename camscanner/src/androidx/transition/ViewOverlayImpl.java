// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import androidx.annotation.NonNull;
import android.graphics.drawable.Drawable;

interface ViewOverlayImpl
{
    void add(@NonNull final Drawable p0);
    
    void remove(@NonNull final Drawable p0);
}

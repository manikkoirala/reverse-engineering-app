// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.animation.TypeConverter;
import android.animation.PropertyValuesHolder;
import android.graphics.Path;
import android.graphics.PointF;
import android.util.Property;

class PropertyValuesHolderUtils
{
    private PropertyValuesHolderUtils() {
    }
    
    static PropertyValuesHolder ofPointF(final Property<?, PointF> property, final Path path) {
        return PropertyValuesHolder.ofObject((Property)property, (TypeConverter)null, path);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.annotation.SuppressLint;
import androidx.annotation.NonNull;
import android.view.View;
import androidx.annotation.RequiresApi;

@RequiresApi(22)
class ViewUtilsApi22 extends ViewUtilsApi21
{
    private static boolean sTryHiddenSetLeftTopRightBottom = true;
    
    @SuppressLint({ "NewApi" })
    @Override
    public void setLeftTopRightBottom(@NonNull final View view, final int n, final int n2, final int n3, final int n4) {
        if (ViewUtilsApi22.sTryHiddenSetLeftTopRightBottom) {
            try {
                \u3007O8o08O.\u3007080(view, n, n2, n3, n4);
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                ViewUtilsApi22.sTryHiddenSetLeftTopRightBottom = false;
            }
        }
    }
}

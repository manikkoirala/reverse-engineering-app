// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.os.Build$VERSION;
import androidx.annotation.Nullable;
import android.graphics.Matrix;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import android.view.View;

class GhostViewUtils
{
    private GhostViewUtils() {
    }
    
    @Nullable
    static GhostView addGhost(@NonNull final View view, @NonNull final ViewGroup viewGroup, @Nullable final Matrix matrix) {
        if (Build$VERSION.SDK_INT == 28) {
            return GhostViewPlatform.addGhost(view, viewGroup, matrix);
        }
        return GhostViewPort.addGhost(view, viewGroup, matrix);
    }
    
    static void removeGhost(final View view) {
        if (Build$VERSION.SDK_INT == 28) {
            GhostViewPlatform.removeGhost(view);
        }
        else {
            GhostViewPort.removeGhost(view);
        }
    }
}

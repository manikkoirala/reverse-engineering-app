// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.view.View;
import android.view.ViewGroup;

interface GhostView
{
    void reserveEndViewTransition(final ViewGroup p0, final View p1);
    
    void setVisibility(final int p0);
}

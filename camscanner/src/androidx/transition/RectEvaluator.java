// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.graphics.Rect;
import android.animation.TypeEvaluator;

class RectEvaluator implements TypeEvaluator<Rect>
{
    private Rect mRect;
    
    RectEvaluator() {
    }
    
    RectEvaluator(final Rect mRect) {
        this.mRect = mRect;
    }
    
    public Rect evaluate(final float n, Rect mRect, final Rect rect) {
        final int left = mRect.left;
        final int n2 = left + (int)((rect.left - left) * n);
        final int top = mRect.top;
        final int n3 = top + (int)((rect.top - top) * n);
        final int right = mRect.right;
        final int n4 = right + (int)((rect.right - right) * n);
        final int bottom = mRect.bottom;
        final int n5 = bottom + (int)((rect.bottom - bottom) * n);
        mRect = this.mRect;
        if (mRect == null) {
            return new Rect(n2, n3, n4, n5);
        }
        mRect.set(n2, n3, n4, n5);
        return this.mRect;
    }
}

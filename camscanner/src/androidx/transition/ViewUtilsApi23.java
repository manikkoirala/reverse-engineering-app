// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.annotation.SuppressLint;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.view.View;
import androidx.annotation.RequiresApi;

@RequiresApi(23)
class ViewUtilsApi23 extends ViewUtilsApi22
{
    private static boolean sTryHiddenSetTransitionVisibility = true;
    
    @SuppressLint({ "NewApi" })
    @Override
    public void setTransitionVisibility(@NonNull final View view, final int n) {
        if (Build$VERSION.SDK_INT == 28) {
            super.setTransitionVisibility(view, n);
        }
        else if (ViewUtilsApi23.sTryHiddenSetTransitionVisibility) {
            try {
                OO0o\u3007\u3007.\u3007080(view, n);
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                ViewUtilsApi23.sTryHiddenSetTransitionVisibility = false;
            }
        }
    }
}

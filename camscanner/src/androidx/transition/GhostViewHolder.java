// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.view.ViewParent;
import java.util.ArrayList;
import android.view.View;
import androidx.annotation.NonNull;
import android.view.ViewGroup;
import android.annotation.SuppressLint;
import android.widget.FrameLayout;

@SuppressLint({ "ViewConstructor" })
class GhostViewHolder extends FrameLayout
{
    private boolean mAttached;
    @NonNull
    private ViewGroup mParent;
    
    GhostViewHolder(final ViewGroup mParent) {
        super(((View)mParent).getContext());
        ((ViewGroup)this).setClipChildren(false);
        ((View)(this.mParent = mParent)).setTag(R.id.ghost_view_holder, (Object)this);
        ViewGroupUtils.getOverlay(this.mParent).add((View)this);
        this.mAttached = true;
    }
    
    static GhostViewHolder getHolder(@NonNull final ViewGroup viewGroup) {
        return (GhostViewHolder)((View)viewGroup).getTag(R.id.ghost_view_holder);
    }
    
    private int getInsertIndex(final ArrayList<View> list) {
        final ArrayList list2 = new ArrayList();
        int n = ((ViewGroup)this).getChildCount() - 1;
        int i = 0;
        while (i <= n) {
            final int n2 = (i + n) / 2;
            getParents(((GhostViewPort)((ViewGroup)this).getChildAt(n2)).mView, list2);
            if (isOnTop(list, list2)) {
                i = n2 + 1;
            }
            else {
                n = n2 - 1;
            }
            list2.clear();
        }
        return i;
    }
    
    private static void getParents(final View e, final ArrayList<View> list) {
        final ViewParent parent = e.getParent();
        if (parent instanceof ViewGroup) {
            getParents((View)parent, list);
        }
        list.add(e);
    }
    
    private static boolean isOnTop(final View view, final View view2) {
        final ViewGroup viewGroup = (ViewGroup)view.getParent();
        final int childCount = viewGroup.getChildCount();
        final float z = view.getZ();
        final float z2 = view2.getZ();
        boolean b = false;
        final boolean b2 = false;
        if (z != z2) {
            boolean b3 = b2;
            if (view.getZ() > view2.getZ()) {
                b3 = true;
            }
            return b3;
        }
        for (int i = 0; i < childCount; ++i) {
            final View child = viewGroup.getChildAt(ViewGroupUtils.getChildDrawingOrder(viewGroup, i));
            if (child == view) {
                return b;
            }
            if (child == view2) {
                break;
            }
        }
        b = true;
        return b;
    }
    
    private static boolean isOnTop(final ArrayList<View> list, final ArrayList<View> list2) {
        final boolean empty = list.isEmpty();
        boolean b2;
        final boolean b = b2 = true;
        if (!empty) {
            b2 = b;
            if (!list2.isEmpty()) {
                if (list.get(0) != list2.get(0)) {
                    b2 = b;
                }
                else {
                    final int min = Math.min(list.size(), list2.size());
                    for (int i = 1; i < min; ++i) {
                        final View view = list.get(i);
                        final View view2 = list2.get(i);
                        if (view != view2) {
                            return isOnTop(view, view2);
                        }
                    }
                    b2 = (list2.size() == min && b);
                }
            }
        }
        return b2;
    }
    
    void addGhostView(final GhostViewPort ghostViewPort) {
        final ArrayList list = new ArrayList();
        getParents(ghostViewPort.mView, list);
        final int insertIndex = this.getInsertIndex(list);
        if (insertIndex >= 0 && insertIndex < ((ViewGroup)this).getChildCount()) {
            ((ViewGroup)this).addView((View)ghostViewPort, insertIndex);
        }
        else {
            ((ViewGroup)this).addView((View)ghostViewPort);
        }
    }
    
    public void onViewAdded(final View view) {
        if (this.mAttached) {
            super.onViewAdded(view);
            return;
        }
        throw new IllegalStateException("This GhostViewHolder is detached!");
    }
    
    public void onViewRemoved(final View view) {
        super.onViewRemoved(view);
        if ((((ViewGroup)this).getChildCount() == 1 && ((ViewGroup)this).getChildAt(0) == view) || ((ViewGroup)this).getChildCount() == 0) {
            ((View)this.mParent).setTag(R.id.ghost_view_holder, (Object)null);
            ViewGroupUtils.getOverlay(this.mParent).remove((View)this);
            this.mAttached = false;
        }
    }
    
    void popToOverlayTop() {
        if (this.mAttached) {
            ViewGroupUtils.getOverlay(this.mParent).remove((View)this);
            ViewGroupUtils.getOverlay(this.mParent).add((View)this);
            return;
        }
        throw new IllegalStateException("This GhostViewHolder is detached!");
    }
}

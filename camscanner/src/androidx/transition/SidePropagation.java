// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.graphics.Rect;
import android.view.ViewGroup;
import androidx.core.view.ViewCompat;
import android.view.View;

public class SidePropagation extends VisibilityPropagation
{
    private float mPropagationSpeed;
    private int mSide;
    
    public SidePropagation() {
        this.mPropagationSpeed = 3.0f;
        this.mSide = 80;
    }
    
    private int distance(final View view, int n, final int n2, final int n3, final int n4, final int n5, final int n6, final int n7, final int n8) {
        final int mSide = this.mSide;
        final int n9 = 0;
        final int n10 = 1;
        boolean b = true;
        int n11 = 0;
        Label_0090: {
            Label_0047: {
                if (mSide == 8388611) {
                    if (ViewCompat.getLayoutDirection(view) != 1) {
                        b = false;
                    }
                    if (!b) {
                        break Label_0047;
                    }
                }
                else {
                    if ((n11 = mSide) != 8388613) {
                        break Label_0090;
                    }
                    int n12;
                    if (ViewCompat.getLayoutDirection(view) == 1) {
                        n12 = n10;
                    }
                    else {
                        n12 = 0;
                    }
                    if (n12 != 0) {
                        break Label_0047;
                    }
                }
                n11 = 5;
                break Label_0090;
            }
            n11 = 3;
        }
        if (n11 != 3) {
            if (n11 != 5) {
                if (n11 != 48) {
                    if (n11 != 80) {
                        n = n9;
                    }
                    else {
                        n = n2 - n6 + Math.abs(n3 - n);
                    }
                }
                else {
                    n = n8 - n2 + Math.abs(n3 - n);
                }
            }
            else {
                n = n - n5 + Math.abs(n4 - n2);
            }
        }
        else {
            n = n7 - n + Math.abs(n4 - n2);
        }
        return n;
    }
    
    private int getMaxDistance(final ViewGroup viewGroup) {
        final int mSide = this.mSide;
        if (mSide != 3 && mSide != 5 && mSide != 8388611 && mSide != 8388613) {
            return ((View)viewGroup).getHeight();
        }
        return ((View)viewGroup).getWidth();
    }
    
    @Override
    public long getStartDelay(final ViewGroup viewGroup, final Transition transition, TransitionValues transitionValues, final TransitionValues transitionValues2) {
        if (transitionValues == null && transitionValues2 == null) {
            return 0L;
        }
        final Rect epicenter = transition.getEpicenter();
        int n;
        if (transitionValues2 != null && this.getViewVisibility(transitionValues) != 0) {
            n = 1;
            transitionValues = transitionValues2;
        }
        else {
            n = -1;
        }
        final int viewX = this.getViewX(transitionValues);
        final int viewY = this.getViewY(transitionValues);
        final int[] array = new int[2];
        ((View)viewGroup).getLocationOnScreen(array);
        final int n2 = array[0] + Math.round(((View)viewGroup).getTranslationX());
        final int n3 = array[1] + Math.round(((View)viewGroup).getTranslationY());
        final int n4 = n2 + ((View)viewGroup).getWidth();
        final int n5 = n3 + ((View)viewGroup).getHeight();
        int centerX;
        int centerY;
        if (epicenter != null) {
            centerX = epicenter.centerX();
            centerY = epicenter.centerY();
        }
        else {
            centerX = (n2 + n4) / 2;
            centerY = (n3 + n5) / 2;
        }
        final float n6 = this.distance((View)viewGroup, viewX, viewY, centerX, centerY, n2, n3, n4, n5) / (float)this.getMaxDistance(viewGroup);
        long duration;
        if ((duration = transition.getDuration()) < 0L) {
            duration = 300L;
        }
        return Math.round(duration * n / this.mPropagationSpeed * n6);
    }
    
    public void setPropagationSpeed(final float mPropagationSpeed) {
        if (mPropagationSpeed != 0.0f) {
            this.mPropagationSpeed = mPropagationSpeed;
            return;
        }
        throw new IllegalArgumentException("propagationSpeed may not be 0");
    }
    
    public void setSide(final int mSide) {
        this.mSide = mSide;
    }
}

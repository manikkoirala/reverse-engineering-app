// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import java.lang.reflect.InvocationTargetException;
import android.graphics.Matrix;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import android.view.View;
import java.lang.reflect.Method;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
class GhostViewPlatform implements GhostView
{
    private static final String TAG = "GhostViewApi21";
    private static Method sAddGhostMethod;
    private static boolean sAddGhostMethodFetched;
    private static Class<?> sGhostViewClass;
    private static boolean sGhostViewClassFetched;
    private static Method sRemoveGhostMethod;
    private static boolean sRemoveGhostMethodFetched;
    private final View mGhostView;
    
    private GhostViewPlatform(@NonNull final View mGhostView) {
        this.mGhostView = mGhostView;
    }
    
    static GhostView addGhost(final View view, final ViewGroup viewGroup, final Matrix matrix) {
        fetchAddGhostMethod();
        final Method sAddGhostMethod = GhostViewPlatform.sAddGhostMethod;
        if (sAddGhostMethod == null) {
            goto Label_0058;
        }
        try {
            return new GhostViewPlatform((View)sAddGhostMethod.invoke(null, view, viewGroup, matrix));
        }
        catch (final InvocationTargetException ex) {
            throw new RuntimeException(ex.getCause());
        }
        catch (final IllegalAccessException ex2) {
            goto Label_0058;
        }
    }
    
    private static void fetchAddGhostMethod() {
        if (GhostViewPlatform.sAddGhostMethodFetched) {
            return;
        }
        while (true) {
            try {
                fetchGhostViewClass();
                (GhostViewPlatform.sAddGhostMethod = GhostViewPlatform.sGhostViewClass.getDeclaredMethod("addGhost", View.class, ViewGroup.class, Matrix.class)).setAccessible(true);
                GhostViewPlatform.sAddGhostMethodFetched = true;
            }
            catch (final NoSuchMethodException ex) {
                continue;
            }
            break;
        }
    }
    
    private static void fetchGhostViewClass() {
        if (GhostViewPlatform.sGhostViewClassFetched) {
            return;
        }
        while (true) {
            try {
                GhostViewPlatform.sGhostViewClass = Class.forName("android.view.GhostView");
                GhostViewPlatform.sGhostViewClassFetched = true;
            }
            catch (final ClassNotFoundException ex) {
                continue;
            }
            break;
        }
    }
    
    private static void fetchRemoveGhostMethod() {
        if (GhostViewPlatform.sRemoveGhostMethodFetched) {
            return;
        }
        while (true) {
            try {
                fetchGhostViewClass();
                (GhostViewPlatform.sRemoveGhostMethod = GhostViewPlatform.sGhostViewClass.getDeclaredMethod("removeGhost", View.class)).setAccessible(true);
                GhostViewPlatform.sRemoveGhostMethodFetched = true;
            }
            catch (final NoSuchMethodException ex) {
                continue;
            }
            break;
        }
    }
    
    static void removeGhost(final View view) {
        fetchRemoveGhostMethod();
        final Method sRemoveGhostMethod = GhostViewPlatform.sRemoveGhostMethod;
        if (sRemoveGhostMethod == null) {
            goto Label_0041;
        }
        try {
            sRemoveGhostMethod.invoke(null, view);
            goto Label_0041;
        }
        catch (final InvocationTargetException ex) {
            throw new RuntimeException(ex.getCause());
        }
        catch (final IllegalAccessException ex2) {
            goto Label_0041;
        }
    }
    
    @Override
    public void reserveEndViewTransition(final ViewGroup viewGroup, final View view) {
    }
    
    @Override
    public void setVisibility(final int visibility) {
        this.mGhostView.setVisibility(visibility);
    }
}

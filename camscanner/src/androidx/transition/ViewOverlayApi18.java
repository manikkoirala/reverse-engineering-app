// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import android.view.View;
import android.view.ViewOverlay;
import androidx.annotation.RequiresApi;

@RequiresApi(18)
class ViewOverlayApi18 implements ViewOverlayImpl
{
    private final ViewOverlay mViewOverlay;
    
    ViewOverlayApi18(@NonNull final View view) {
        this.mViewOverlay = view.getOverlay();
    }
    
    @Override
    public void add(@NonNull final Drawable drawable) {
        this.mViewOverlay.add(drawable);
    }
    
    @Override
    public void remove(@NonNull final Drawable drawable) {
        this.mViewOverlay.remove(drawable);
    }
}

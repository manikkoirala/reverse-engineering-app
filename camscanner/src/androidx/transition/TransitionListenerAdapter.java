// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import androidx.annotation.NonNull;

public class TransitionListenerAdapter implements TransitionListener
{
    @Override
    public void onTransitionCancel(@NonNull final Transition transition) {
    }
    
    @Override
    public void onTransitionEnd(@NonNull final Transition transition) {
    }
    
    @Override
    public void onTransitionPause(@NonNull final Transition transition) {
    }
    
    @Override
    public void onTransitionResume(@NonNull final Transition transition) {
    }
    
    @Override
    public void onTransitionStart(@NonNull final Transition transition) {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.annotation.SuppressLint;
import java.lang.reflect.InvocationTargetException;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.graphics.Canvas;
import java.lang.reflect.Method;

class CanvasUtils
{
    private static Method sInorderBarrierMethod;
    private static boolean sOrderMethodsFetched;
    private static Method sReorderBarrierMethod;
    
    private CanvasUtils() {
    }
    
    @SuppressLint({ "SoonBlockedPrivateApi" })
    static void enableZ(@NonNull final Canvas obj, final boolean b) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 29) {
            if (b) {
                \u3007080.\u3007080((Canvas)obj);
                goto Label_0152;
            }
            \u3007o00\u3007\u3007Oo.\u3007080((Canvas)obj);
            goto Label_0152;
        }
        else {
            if (sdk_INT == 28) {
                goto Label_0153;
            }
            Label_0086: {
                if (CanvasUtils.sOrderMethodsFetched) {
                    break Label_0086;
                }
                try {
                    (CanvasUtils.sReorderBarrierMethod = Canvas.class.getDeclaredMethod("insertReorderBarrier", (Class<?>[])new Class[0])).setAccessible(true);
                    (CanvasUtils.sInorderBarrierMethod = Canvas.class.getDeclaredMethod("insertInorderBarrier", (Class<?>[])new Class[0])).setAccessible(true);
                    CanvasUtils.sOrderMethodsFetched = true;
                    if (!b) {
                        goto Label_0115;
                    }
                    try {
                        final Method sReorderBarrierMethod = CanvasUtils.sReorderBarrierMethod;
                        if (sReorderBarrierMethod != null) {
                            sReorderBarrierMethod.invoke(obj, new Object[0]);
                            goto Label_0115;
                        }
                        goto Label_0115;
                    }
                    catch (final InvocationTargetException obj) {}
                    catch (final IllegalAccessException obj) {}
                }
                catch (final NoSuchMethodException ex) {}
            }
        }
    }
}

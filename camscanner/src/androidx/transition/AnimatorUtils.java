// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.animation.Animator$AnimatorPauseListener;
import android.animation.AnimatorListenerAdapter;
import androidx.annotation.NonNull;
import android.animation.Animator;

class AnimatorUtils
{
    private AnimatorUtils() {
    }
    
    static void addPauseListener(@NonNull final Animator animator, @NonNull final AnimatorListenerAdapter animatorListenerAdapter) {
        animator.addPauseListener((Animator$AnimatorPauseListener)animatorListenerAdapter);
    }
    
    static void pause(@NonNull final Animator animator) {
        animator.pause();
    }
    
    static void resume(@NonNull final Animator animator) {
        animator.resume();
    }
    
    interface AnimatorPauseListenerCompat
    {
        void onAnimationPause(final Animator p0);
        
        void onAnimationResume(final Animator p0);
    }
}

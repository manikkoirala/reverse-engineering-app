// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import androidx.annotation.NonNull;
import android.view.View;

interface ViewGroupOverlayImpl extends ViewOverlayImpl
{
    void add(@NonNull final View p0);
    
    void remove(@NonNull final View p0);
}

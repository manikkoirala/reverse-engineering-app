// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import androidx.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.content.Context;

class ViewGroupOverlayApi14 extends ViewOverlayApi14 implements ViewGroupOverlayImpl
{
    ViewGroupOverlayApi14(final Context context, final ViewGroup viewGroup, final View view) {
        super(context, viewGroup, view);
    }
    
    static ViewGroupOverlayApi14 createFrom(final ViewGroup viewGroup) {
        return (ViewGroupOverlayApi14)ViewOverlayApi14.createFrom((View)viewGroup);
    }
    
    @Override
    public void add(@NonNull final View view) {
        super.mOverlayViewGroup.add(view);
    }
    
    @Override
    public void remove(@NonNull final View view) {
        super.mOverlayViewGroup.remove(view);
    }
}

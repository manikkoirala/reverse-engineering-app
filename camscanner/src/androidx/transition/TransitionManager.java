// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import java.util.Iterator;
import android.view.ViewTreeObserver$OnPreDrawListener;
import android.view.View$OnAttachStateChangeListener;
import java.util.Collection;
import android.view.View;
import androidx.core.view.ViewCompat;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.collection.ArrayMap;
import java.lang.ref.WeakReference;
import android.view.ViewGroup;
import java.util.ArrayList;

public class TransitionManager
{
    private static final String LOG_TAG = "TransitionManager";
    private static Transition sDefaultTransition;
    static ArrayList<ViewGroup> sPendingTransitions;
    private static ThreadLocal<WeakReference<ArrayMap<ViewGroup, ArrayList<Transition>>>> sRunningTransitions;
    private ArrayMap<Scene, ArrayMap<Scene, Transition>> mScenePairTransitions;
    private ArrayMap<Scene, Transition> mSceneTransitions;
    
    static {
        TransitionManager.sDefaultTransition = new AutoTransition();
        TransitionManager.sRunningTransitions = new ThreadLocal<WeakReference<ArrayMap<ViewGroup, ArrayList<Transition>>>>();
        TransitionManager.sPendingTransitions = new ArrayList<ViewGroup>();
    }
    
    public TransitionManager() {
        this.mSceneTransitions = new ArrayMap<Scene, Transition>();
        this.mScenePairTransitions = new ArrayMap<Scene, ArrayMap<Scene, Transition>>();
    }
    
    public static void beginDelayedTransition(@NonNull final ViewGroup viewGroup) {
        beginDelayedTransition(viewGroup, null);
    }
    
    public static void beginDelayedTransition(@NonNull final ViewGroup viewGroup, @Nullable Transition clone) {
        if (!TransitionManager.sPendingTransitions.contains(viewGroup) && ViewCompat.isLaidOut((View)viewGroup)) {
            TransitionManager.sPendingTransitions.add(viewGroup);
            Transition sDefaultTransition;
            if ((sDefaultTransition = clone) == null) {
                sDefaultTransition = TransitionManager.sDefaultTransition;
            }
            clone = sDefaultTransition.clone();
            sceneChangeSetup(viewGroup, clone);
            Scene.setCurrentScene(viewGroup, null);
            sceneChangeRunTransition(viewGroup, clone);
        }
    }
    
    private static void changeScene(final Scene scene, Transition clone) {
        final ViewGroup sceneRoot = scene.getSceneRoot();
        if (!TransitionManager.sPendingTransitions.contains(sceneRoot)) {
            final Scene currentScene = Scene.getCurrentScene(sceneRoot);
            if (clone == null) {
                if (currentScene != null) {
                    currentScene.exit();
                }
                scene.enter();
            }
            else {
                TransitionManager.sPendingTransitions.add(sceneRoot);
                clone = clone.clone();
                clone.setSceneRoot(sceneRoot);
                if (currentScene != null && currentScene.isCreatedFromLayoutResource()) {
                    clone.setCanRemoveViews(true);
                }
                sceneChangeSetup(sceneRoot, clone);
                scene.enter();
                sceneChangeRunTransition(sceneRoot, clone);
            }
        }
    }
    
    public static void endTransitions(final ViewGroup o) {
        TransitionManager.sPendingTransitions.remove(o);
        final ArrayList c = getRunningTransitions().get(o);
        if (c != null && !c.isEmpty()) {
            final ArrayList list = new ArrayList(c);
            for (int i = list.size() - 1; i >= 0; --i) {
                ((Transition)list.get(i)).forceToEnd(o);
            }
        }
    }
    
    static ArrayMap<ViewGroup, ArrayList<Transition>> getRunningTransitions() {
        final WeakReference weakReference = TransitionManager.sRunningTransitions.get();
        if (weakReference != null) {
            final ArrayMap arrayMap = (ArrayMap)weakReference.get();
            if (arrayMap != null) {
                return arrayMap;
            }
        }
        final ArrayMap<ViewGroup, ArrayList<Transition>> referent = new ArrayMap<ViewGroup, ArrayList<Transition>>();
        TransitionManager.sRunningTransitions.set(new WeakReference<ArrayMap<ViewGroup, ArrayList<Transition>>>(referent));
        return referent;
    }
    
    private Transition getTransition(final Scene scene) {
        final ViewGroup sceneRoot = scene.getSceneRoot();
        if (sceneRoot != null) {
            final Scene currentScene = Scene.getCurrentScene(sceneRoot);
            if (currentScene != null) {
                final ArrayMap arrayMap = this.mScenePairTransitions.get(scene);
                if (arrayMap != null) {
                    final Transition transition = (Transition)arrayMap.get(currentScene);
                    if (transition != null) {
                        return transition;
                    }
                }
            }
        }
        Transition sDefaultTransition = this.mSceneTransitions.get(scene);
        if (sDefaultTransition == null) {
            sDefaultTransition = TransitionManager.sDefaultTransition;
        }
        return sDefaultTransition;
    }
    
    public static void go(@NonNull final Scene scene) {
        changeScene(scene, TransitionManager.sDefaultTransition);
    }
    
    public static void go(@NonNull final Scene scene, @Nullable final Transition transition) {
        changeScene(scene, transition);
    }
    
    private static void sceneChangeRunTransition(final ViewGroup viewGroup, final Transition transition) {
        if (transition != null && viewGroup != null) {
            final MultiListener multiListener = new MultiListener(transition, viewGroup);
            ((View)viewGroup).addOnAttachStateChangeListener((View$OnAttachStateChangeListener)multiListener);
            ((View)viewGroup).getViewTreeObserver().addOnPreDrawListener((ViewTreeObserver$OnPreDrawListener)multiListener);
        }
    }
    
    private static void sceneChangeSetup(final ViewGroup viewGroup, final Transition transition) {
        final ArrayList list = getRunningTransitions().get(viewGroup);
        if (list != null && list.size() > 0) {
            final Iterator iterator = list.iterator();
            while (iterator.hasNext()) {
                ((Transition)iterator.next()).pause((View)viewGroup);
            }
        }
        if (transition != null) {
            transition.captureValues(viewGroup, true);
        }
        final Scene currentScene = Scene.getCurrentScene(viewGroup);
        if (currentScene != null) {
            currentScene.exit();
        }
    }
    
    public void setTransition(@NonNull final Scene scene, @NonNull final Scene scene2, @Nullable final Transition transition) {
        ArrayMap arrayMap;
        if ((arrayMap = this.mScenePairTransitions.get(scene2)) == null) {
            arrayMap = new ArrayMap();
            this.mScenePairTransitions.put(scene2, arrayMap);
        }
        arrayMap.put(scene, transition);
    }
    
    public void setTransition(@NonNull final Scene scene, @Nullable final Transition transition) {
        this.mSceneTransitions.put(scene, transition);
    }
    
    public void transitionTo(@NonNull final Scene scene) {
        changeScene(scene, this.getTransition(scene));
    }
    
    private static class MultiListener implements ViewTreeObserver$OnPreDrawListener, View$OnAttachStateChangeListener
    {
        ViewGroup mSceneRoot;
        Transition mTransition;
        
        MultiListener(final Transition mTransition, final ViewGroup mSceneRoot) {
            this.mTransition = mTransition;
            this.mSceneRoot = mSceneRoot;
        }
        
        private void removeListeners() {
            ((View)this.mSceneRoot).getViewTreeObserver().removeOnPreDrawListener((ViewTreeObserver$OnPreDrawListener)this);
            ((View)this.mSceneRoot).removeOnAttachStateChangeListener((View$OnAttachStateChangeListener)this);
        }
        
        public boolean onPreDraw() {
            this.removeListeners();
            if (!TransitionManager.sPendingTransitions.remove(this.mSceneRoot)) {
                return true;
            }
            final ArrayMap<ViewGroup, ArrayList<Transition>> runningTransitions = TransitionManager.getRunningTransitions();
            final ArrayList c = runningTransitions.get(this.mSceneRoot);
            ArrayList list = null;
            ArrayList list2;
            if (c == null) {
                list2 = new ArrayList();
                runningTransitions.put(this.mSceneRoot, list2);
            }
            else {
                list2 = c;
                if (c.size() > 0) {
                    list = new ArrayList(c);
                    list2 = c;
                }
            }
            list2.add(this.mTransition);
            this.mTransition.addListener((Transition.TransitionListener)new TransitionListenerAdapter(this, runningTransitions) {
                final MultiListener this$0;
                final ArrayMap val$runningTransitions;
                
                @Override
                public void onTransitionEnd(@NonNull final Transition o) {
                    ((ArrayList)this.val$runningTransitions.get(this.this$0.mSceneRoot)).remove(o);
                    o.removeListener((Transition.TransitionListener)this);
                }
            });
            this.mTransition.captureValues(this.mSceneRoot, false);
            if (list != null) {
                final Iterator iterator = list.iterator();
                while (iterator.hasNext()) {
                    ((Transition)iterator.next()).resume((View)this.mSceneRoot);
                }
            }
            this.mTransition.playTransition(this.mSceneRoot);
            return true;
        }
        
        public void onViewAttachedToWindow(final View view) {
        }
        
        public void onViewDetachedFromWindow(final View view) {
            this.removeListeners();
            TransitionManager.sPendingTransitions.remove(this.mSceneRoot);
            final ArrayList list = TransitionManager.getRunningTransitions().get(this.mSceneRoot);
            if (list != null && list.size() > 0) {
                final Iterator iterator = list.iterator();
                while (iterator.hasNext()) {
                    ((Transition)iterator.next()).resume((View)this.mSceneRoot);
                }
            }
            this.mTransition.clearValues(true);
        }
    }
}

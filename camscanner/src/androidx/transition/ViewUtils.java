// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import androidx.annotation.Nullable;
import android.graphics.Matrix;
import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import android.os.Build$VERSION;
import android.graphics.Rect;
import android.view.View;
import android.util.Property;

class ViewUtils
{
    static final Property<View, Rect> CLIP_BOUNDS;
    private static final ViewUtilsBase IMPL;
    private static final String TAG = "ViewUtils";
    static final Property<View, Float> TRANSITION_ALPHA;
    
    static {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 29) {
            IMPL = new ViewUtilsApi29();
        }
        else if (sdk_INT >= 23) {
            IMPL = new ViewUtilsApi23();
        }
        else if (sdk_INT >= 22) {
            IMPL = new ViewUtilsApi22();
        }
        else {
            IMPL = new ViewUtilsApi21();
        }
        TRANSITION_ALPHA = new Property<View, Float>(Float.class, "translationAlpha") {
            public Float get(final View view) {
                return ViewUtils.getTransitionAlpha(view);
            }
            
            public void set(final View view, final Float n) {
                ViewUtils.setTransitionAlpha(view, n);
            }
        };
        CLIP_BOUNDS = new Property<View, Rect>(Rect.class, "clipBounds") {
            public Rect get(final View view) {
                return ViewCompat.getClipBounds(view);
            }
            
            public void set(final View view, final Rect rect) {
                ViewCompat.setClipBounds(view, rect);
            }
        };
    }
    
    private ViewUtils() {
    }
    
    static void clearNonTransitionAlpha(@NonNull final View view) {
        ViewUtils.IMPL.clearNonTransitionAlpha(view);
    }
    
    static ViewOverlayImpl getOverlay(@NonNull final View view) {
        return new ViewOverlayApi18(view);
    }
    
    static float getTransitionAlpha(@NonNull final View view) {
        return ViewUtils.IMPL.getTransitionAlpha(view);
    }
    
    static WindowIdImpl getWindowId(@NonNull final View view) {
        return new WindowIdApi18(view);
    }
    
    static void saveNonTransitionAlpha(@NonNull final View view) {
        ViewUtils.IMPL.saveNonTransitionAlpha(view);
    }
    
    static void setAnimationMatrix(@NonNull final View view, @Nullable final Matrix matrix) {
        ViewUtils.IMPL.setAnimationMatrix(view, matrix);
    }
    
    static void setLeftTopRightBottom(@NonNull final View view, final int n, final int n2, final int n3, final int n4) {
        ViewUtils.IMPL.setLeftTopRightBottom(view, n, n2, n3, n4);
    }
    
    static void setTransitionAlpha(@NonNull final View view, final float n) {
        ViewUtils.IMPL.setTransitionAlpha(view, n);
    }
    
    static void setTransitionVisibility(@NonNull final View view, final int n) {
        ViewUtils.IMPL.setTransitionVisibility(view, n);
    }
    
    static void transformMatrixToGlobal(@NonNull final View view, @NonNull final Matrix matrix) {
        ViewUtils.IMPL.transformMatrixToGlobal(view, matrix);
    }
    
    static void transformMatrixToLocal(@NonNull final View view, @NonNull final Matrix matrix) {
        ViewUtils.IMPL.transformMatrixToLocal(view, matrix);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.annotation.SuppressLint;
import androidx.annotation.NonNull;
import android.view.View;
import androidx.annotation.RequiresApi;

@RequiresApi(19)
class ViewUtilsApi19 extends ViewUtilsBase
{
    private static boolean sTryHiddenTransitionAlpha = true;
    
    @Override
    public void clearNonTransitionAlpha(@NonNull final View view) {
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public float getTransitionAlpha(@NonNull final View view) {
        if (ViewUtilsApi19.sTryHiddenTransitionAlpha) {
            try {
                return oO80.\u3007080(view);
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                ViewUtilsApi19.sTryHiddenTransitionAlpha = false;
            }
        }
        return view.getAlpha();
    }
    
    @Override
    public void saveNonTransitionAlpha(@NonNull final View view) {
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public void setTransitionAlpha(@NonNull final View view, final float alpha) {
        if (ViewUtilsApi19.sTryHiddenTransitionAlpha) {
            try {
                \u3007\u3007888.\u3007080(view, alpha);
                return;
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                ViewUtilsApi19.sTryHiddenTransitionAlpha = false;
            }
        }
        view.setAlpha(alpha);
    }
}

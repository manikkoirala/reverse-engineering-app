// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.graphics.Canvas;
import androidx.annotation.NonNull;
import android.graphics.Paint;
import androidx.core.view.ViewCompat;
import android.view.View;
import android.view.ViewTreeObserver$OnPreDrawListener;
import androidx.annotation.Nullable;
import android.graphics.Matrix;
import android.annotation.SuppressLint;
import android.view.ViewGroup;

@SuppressLint({ "ViewConstructor" })
class GhostViewPort extends ViewGroup implements GhostView
{
    @Nullable
    private Matrix mMatrix;
    private final ViewTreeObserver$OnPreDrawListener mOnPreDrawListener;
    int mReferences;
    ViewGroup mStartParent;
    View mStartView;
    final View mView;
    
    GhostViewPort(final View mView) {
        super(mView.getContext());
        this.mOnPreDrawListener = (ViewTreeObserver$OnPreDrawListener)new ViewTreeObserver$OnPreDrawListener() {
            final GhostViewPort this$0;
            
            public boolean onPreDraw() {
                ViewCompat.postInvalidateOnAnimation((View)this.this$0);
                final GhostViewPort this$0 = this.this$0;
                final ViewGroup mStartParent = this$0.mStartParent;
                if (mStartParent != null) {
                    final View mStartView = this$0.mStartView;
                    if (mStartView != null) {
                        mStartParent.endViewTransition(mStartView);
                        ViewCompat.postInvalidateOnAnimation((View)this.this$0.mStartParent);
                        final GhostViewPort this$2 = this.this$0;
                        this$2.mStartParent = null;
                        this$2.mStartView = null;
                    }
                }
                return true;
            }
        };
        this.mView = mView;
        ((View)this).setWillNotDraw(false);
        ((View)this).setLayerType(2, (Paint)null);
    }
    
    static GhostViewPort addGhost(final View view, final ViewGroup viewGroup, final Matrix matrix) {
        if (view.getParent() instanceof ViewGroup) {
            final GhostViewHolder holder = GhostViewHolder.getHolder(viewGroup);
            Object ghostView = getGhostView(view);
            int mReferences = 0;
            Label_0065: {
                if (ghostView != null) {
                    final GhostViewHolder ghostViewHolder = (GhostViewHolder)((View)ghostView).getParent();
                    if (ghostViewHolder != holder) {
                        mReferences = ((GhostViewPort)ghostView).mReferences;
                        ((ViewGroup)ghostViewHolder).removeView((View)ghostView);
                        ghostView = null;
                        break Label_0065;
                    }
                }
                mReferences = 0;
            }
            Object o;
            if (ghostView == null) {
                Matrix matrix2;
                if ((matrix2 = matrix) == null) {
                    matrix2 = new Matrix();
                    calculateMatrix(view, viewGroup, matrix2);
                }
                final GhostViewPort ghostViewPort = new GhostViewPort(view);
                ghostViewPort.setMatrix(matrix2);
                GhostViewHolder ghostViewHolder2;
                if (holder == null) {
                    ghostViewHolder2 = new GhostViewHolder(viewGroup);
                }
                else {
                    holder.popToOverlayTop();
                    ghostViewHolder2 = holder;
                }
                copySize((View)viewGroup, (View)ghostViewHolder2);
                copySize((View)viewGroup, (View)ghostViewPort);
                ghostViewHolder2.addGhostView(ghostViewPort);
                ghostViewPort.mReferences = mReferences;
                o = ghostViewPort;
            }
            else {
                o = ghostView;
                if (matrix != null) {
                    ((GhostViewPort)ghostView).setMatrix(matrix);
                    o = ghostView;
                }
            }
            ++((GhostViewPort)o).mReferences;
            return (GhostViewPort)o;
        }
        throw new IllegalArgumentException("Ghosted views must be parented by a ViewGroup");
    }
    
    static void calculateMatrix(final View view, final ViewGroup viewGroup, final Matrix matrix) {
        final ViewGroup viewGroup2 = (ViewGroup)view.getParent();
        matrix.reset();
        ViewUtils.transformMatrixToGlobal((View)viewGroup2, matrix);
        matrix.preTranslate((float)(-((View)viewGroup2).getScrollX()), (float)(-((View)viewGroup2).getScrollY()));
        ViewUtils.transformMatrixToLocal((View)viewGroup, matrix);
    }
    
    static void copySize(final View view, final View view2) {
        ViewUtils.setLeftTopRightBottom(view2, view2.getLeft(), view2.getTop(), view2.getLeft() + view.getWidth(), view2.getTop() + view.getHeight());
    }
    
    static GhostViewPort getGhostView(final View view) {
        return (GhostViewPort)view.getTag(R.id.ghost_view);
    }
    
    static void removeGhost(final View view) {
        final GhostViewPort ghostView = getGhostView(view);
        if (ghostView != null && --ghostView.mReferences <= 0) {
            ((ViewGroup)((View)ghostView).getParent()).removeView((View)ghostView);
        }
    }
    
    static void setGhostView(@NonNull final View view, @Nullable final GhostViewPort ghostViewPort) {
        view.setTag(R.id.ghost_view, (Object)ghostViewPort);
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        setGhostView(this.mView, this);
        this.mView.getViewTreeObserver().addOnPreDrawListener(this.mOnPreDrawListener);
        ViewUtils.setTransitionVisibility(this.mView, 4);
        if (this.mView.getParent() != null) {
            ((View)this.mView.getParent()).invalidate();
        }
    }
    
    protected void onDetachedFromWindow() {
        this.mView.getViewTreeObserver().removeOnPreDrawListener(this.mOnPreDrawListener);
        ViewUtils.setTransitionVisibility(this.mView, 0);
        setGhostView(this.mView, null);
        if (this.mView.getParent() != null) {
            ((View)this.mView.getParent()).invalidate();
        }
        super.onDetachedFromWindow();
    }
    
    protected void onDraw(final Canvas canvas) {
        CanvasUtils.enableZ(canvas, true);
        canvas.setMatrix(this.mMatrix);
        ViewUtils.setTransitionVisibility(this.mView, 0);
        this.mView.invalidate();
        ViewUtils.setTransitionVisibility(this.mView, 4);
        this.drawChild(canvas, this.mView, ((View)this).getDrawingTime());
        CanvasUtils.enableZ(canvas, false);
    }
    
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
    }
    
    public void reserveEndViewTransition(final ViewGroup mStartParent, final View mStartView) {
        this.mStartParent = mStartParent;
        this.mStartView = mStartView;
    }
    
    void setMatrix(@NonNull final Matrix mMatrix) {
        this.mMatrix = mMatrix;
    }
    
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (getGhostView(this.mView) == this) {
            if (visibility == 0) {
                visibility = 4;
            }
            else {
                visibility = 0;
            }
            ViewUtils.setTransitionVisibility(this.mView, visibility);
        }
    }
}

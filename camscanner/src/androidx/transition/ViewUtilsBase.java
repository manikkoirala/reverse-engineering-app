// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.view.ViewParent;
import java.lang.reflect.InvocationTargetException;
import androidx.annotation.Nullable;
import android.graphics.Matrix;
import androidx.annotation.NonNull;
import android.annotation.SuppressLint;
import android.view.View;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

class ViewUtilsBase
{
    private static final String TAG = "ViewUtilsBase";
    private static final int VISIBILITY_MASK = 12;
    private static boolean sSetFrameFetched;
    private static Method sSetFrameMethod;
    private static Field sViewFlagsField;
    private static boolean sViewFlagsFieldFetched;
    private float[] mMatrixValues;
    
    @SuppressLint({ "PrivateApi" })
    private void fetchSetFrame() {
        if (ViewUtilsBase.sSetFrameFetched) {
            return;
        }
        while (true) {
            try {
                final Class<Integer> type = Integer.TYPE;
                (ViewUtilsBase.sSetFrameMethod = View.class.getDeclaredMethod("setFrame", type, type, type, type)).setAccessible(true);
                ViewUtilsBase.sSetFrameFetched = true;
            }
            catch (final NoSuchMethodException ex) {
                continue;
            }
            break;
        }
    }
    
    public void clearNonTransitionAlpha(@NonNull final View view) {
        if (view.getVisibility() == 0) {
            view.setTag(R.id.save_non_transition_alpha, (Object)null);
        }
    }
    
    public float getTransitionAlpha(@NonNull final View view) {
        final Float n = (Float)view.getTag(R.id.save_non_transition_alpha);
        if (n != null) {
            return view.getAlpha() / n;
        }
        return view.getAlpha();
    }
    
    public void saveNonTransitionAlpha(@NonNull final View view) {
        final int save_non_transition_alpha = R.id.save_non_transition_alpha;
        if (view.getTag(save_non_transition_alpha) == null) {
            view.setTag(save_non_transition_alpha, (Object)view.getAlpha());
        }
    }
    
    public void setAnimationMatrix(@NonNull final View view, @Nullable final Matrix matrix) {
        if (matrix != null && !matrix.isIdentity()) {
            float[] mMatrixValues;
            if ((mMatrixValues = this.mMatrixValues) == null) {
                mMatrixValues = new float[9];
                this.mMatrixValues = mMatrixValues;
            }
            matrix.getValues(mMatrixValues);
            final float n = mMatrixValues[3];
            final float n2 = (float)Math.sqrt(1.0f - n * n);
            int n3;
            if (mMatrixValues[0] < 0.0f) {
                n3 = -1;
            }
            else {
                n3 = 1;
            }
            final float n4 = n2 * n3;
            final float rotation = (float)Math.toDegrees(Math.atan2(n, n4));
            final float scaleX = mMatrixValues[0] / n4;
            final float scaleY = mMatrixValues[4] / n4;
            final float translationX = mMatrixValues[2];
            final float translationY = mMatrixValues[5];
            view.setPivotX(0.0f);
            view.setPivotY(0.0f);
            view.setTranslationX(translationX);
            view.setTranslationY(translationY);
            view.setRotation(rotation);
            view.setScaleX(scaleX);
            view.setScaleY(scaleY);
        }
        else {
            view.setPivotX((float)(view.getWidth() / 2));
            view.setPivotY((float)(view.getHeight() / 2));
            view.setTranslationX(0.0f);
            view.setTranslationY(0.0f);
            view.setScaleX(1.0f);
            view.setScaleY(1.0f);
            view.setRotation(0.0f);
        }
    }
    
    public void setLeftTopRightBottom(@NonNull final View obj, final int i, final int j, final int k, final int l) {
        this.fetchSetFrame();
        final Method sSetFrameMethod = ViewUtilsBase.sSetFrameMethod;
        if (sSetFrameMethod == null) {
            goto Label_0071;
        }
        try {
            sSetFrameMethod.invoke(obj, i, j, k, l);
            goto Label_0071;
        }
        catch (final InvocationTargetException ex) {
            throw new RuntimeException(ex.getCause());
        }
        catch (final IllegalAccessException ex2) {
            goto Label_0071;
        }
    }
    
    public void setTransitionAlpha(@NonNull final View view, final float alpha) {
        final Float n = (Float)view.getTag(R.id.save_non_transition_alpha);
        if (n != null) {
            view.setAlpha(n * alpha);
        }
        else {
            view.setAlpha(alpha);
        }
    }
    
    public void setTransitionVisibility(@NonNull final View p0, final int p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: ifne            30
        //     6: ldc             Landroid/view/View;.class
        //     8: ldc             "mViewFlags"
        //    10: invokevirtual   java/lang/Class.getDeclaredField:(Ljava/lang/String;)Ljava/lang/reflect/Field;
        //    13: astore          4
        //    15: aload           4
        //    17: putstatic       androidx/transition/ViewUtilsBase.sViewFlagsField:Ljava/lang/reflect/Field;
        //    20: aload           4
        //    22: iconst_1       
        //    23: invokevirtual   java/lang/reflect/AccessibleObject.setAccessible:(Z)V
        //    26: iconst_1       
        //    27: putstatic       androidx/transition/ViewUtilsBase.sViewFlagsFieldFetched:Z
        //    30: getstatic       androidx/transition/ViewUtilsBase.sViewFlagsField:Ljava/lang/reflect/Field;
        //    33: astore          4
        //    35: aload           4
        //    37: ifnull          60
        //    40: aload           4
        //    42: aload_1        
        //    43: invokevirtual   java/lang/reflect/Field.getInt:(Ljava/lang/Object;)I
        //    46: istore_3       
        //    47: getstatic       androidx/transition/ViewUtilsBase.sViewFlagsField:Ljava/lang/reflect/Field;
        //    50: aload_1        
        //    51: iload_2        
        //    52: iload_3        
        //    53: bipush          -13
        //    55: iand           
        //    56: ior            
        //    57: invokevirtual   java/lang/reflect/Field.setInt:(Ljava/lang/Object;I)V
        //    60: return         
        //    61: astore          4
        //    63: goto            26
        //    66: astore_1       
        //    67: goto            60
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                              
        //  -----  -----  -----  -----  ----------------------------------
        //  6      26     61     66     Ljava/lang/NoSuchFieldException;
        //  40     60     66     70     Ljava/lang/IllegalAccessException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0060:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void transformMatrixToGlobal(@NonNull final View view, @NonNull final Matrix matrix) {
        final ViewParent parent = view.getParent();
        if (parent instanceof View) {
            final View view2 = (View)parent;
            this.transformMatrixToGlobal(view2, matrix);
            matrix.preTranslate((float)(-view2.getScrollX()), (float)(-view2.getScrollY()));
        }
        matrix.preTranslate((float)view.getLeft(), (float)view.getTop());
        final Matrix matrix2 = view.getMatrix();
        if (!matrix2.isIdentity()) {
            matrix.preConcat(matrix2);
        }
    }
    
    public void transformMatrixToLocal(@NonNull final View view, @NonNull final Matrix matrix) {
        final ViewParent parent = view.getParent();
        if (parent instanceof View) {
            final View view2 = (View)parent;
            this.transformMatrixToLocal(view2, matrix);
            matrix.postTranslate((float)view2.getScrollX(), (float)view2.getScrollY());
        }
        matrix.postTranslate((float)(-view.getLeft()), (float)(-view.getTop()));
        final Matrix matrix2 = view.getMatrix();
        if (!matrix2.isIdentity()) {
            final Matrix matrix3 = new Matrix();
            if (matrix2.invert(matrix3)) {
                matrix.postConcat(matrix3);
            }
        }
    }
}

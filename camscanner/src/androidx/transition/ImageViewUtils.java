// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.view.View;
import androidx.annotation.RequiresApi;
import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.os.Build$VERSION;
import androidx.annotation.Nullable;
import android.graphics.Matrix;
import androidx.annotation.NonNull;
import android.widget.ImageView;
import java.lang.reflect.Field;

class ImageViewUtils
{
    private static Field sDrawMatrixField;
    private static boolean sDrawMatrixFieldFetched = false;
    private static boolean sTryHiddenAnimateTransform = true;
    
    private ImageViewUtils() {
    }
    
    static void animateTransform(@NonNull final ImageView imageView, @Nullable final Matrix matrix) {
        if (Build$VERSION.SDK_INT >= 29) {
            \u3007o\u3007.\u3007080(imageView, matrix);
        }
        else if (matrix == null) {
            final Drawable drawable = imageView.getDrawable();
            if (drawable != null) {
                drawable.setBounds(0, 0, ((View)imageView).getWidth() - ((View)imageView).getPaddingLeft() - ((View)imageView).getPaddingRight(), ((View)imageView).getHeight() - ((View)imageView).getPaddingTop() - ((View)imageView).getPaddingBottom());
                ((View)imageView).invalidate();
            }
        }
        else {
            hiddenAnimateTransform(imageView, matrix);
        }
    }
    
    private static void fetchDrawMatrixField() {
        if (ImageViewUtils.sDrawMatrixFieldFetched) {
            return;
        }
        while (true) {
            try {
                (ImageViewUtils.sDrawMatrixField = ImageView.class.getDeclaredField("mDrawMatrix")).setAccessible(true);
                ImageViewUtils.sDrawMatrixFieldFetched = true;
            }
            catch (final NoSuchFieldException ex) {
                continue;
            }
            break;
        }
    }
    
    @SuppressLint({ "NewApi" })
    @RequiresApi(21)
    private static void hiddenAnimateTransform(@NonNull final ImageView imageView, @Nullable final Matrix matrix) {
        if (ImageViewUtils.sTryHiddenAnimateTransform) {
            try {
                \u3007o\u3007.\u3007080(imageView, matrix);
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                ImageViewUtils.sTryHiddenAnimateTransform = false;
            }
        }
    }
}

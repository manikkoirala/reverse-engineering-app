// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.animation.AnimatorListenerAdapter;
import android.animation.Animator$AnimatorListener;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.Animator;
import androidx.annotation.Nullable;
import android.animation.TimeInterpolator;
import androidx.annotation.NonNull;
import android.view.View;

class TranslationAnimationCreator
{
    private TranslationAnimationCreator() {
    }
    
    @Nullable
    static Animator createAnimation(@NonNull final View view, @NonNull final TransitionValues transitionValues, final int n, final int n2, float translationX, float translationY, final float n3, final float n4, @Nullable final TimeInterpolator interpolator, @NonNull final Transition transition) {
        final float translationX2 = view.getTranslationX();
        final float translationY2 = view.getTranslationY();
        final int[] array = (int[])transitionValues.view.getTag(R.id.transition_position);
        if (array != null) {
            translationX = array[0] - n + translationX2;
            translationY = array[1] - n2 + translationY2;
        }
        final int round = Math.round(translationX - translationX2);
        final int round2 = Math.round(translationY - translationY2);
        view.setTranslationX(translationX);
        view.setTranslationY(translationY);
        if (translationX == n3 && translationY == n4) {
            return null;
        }
        final ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder((Object)view, new PropertyValuesHolder[] { PropertyValuesHolder.ofFloat(View.TRANSLATION_X, new float[] { translationX, n3 }), PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, new float[] { translationY, n4 }) });
        final TransitionPositionListener transitionPositionListener = new TransitionPositionListener(view, transitionValues.view, n + round, n2 + round2, translationX2, translationY2);
        transition.addListener((Transition.TransitionListener)transitionPositionListener);
        ((Animator)ofPropertyValuesHolder).addListener((Animator$AnimatorListener)transitionPositionListener);
        AnimatorUtils.addPauseListener((Animator)ofPropertyValuesHolder, transitionPositionListener);
        ((Animator)ofPropertyValuesHolder).setInterpolator(interpolator);
        return (Animator)ofPropertyValuesHolder;
    }
    
    private static class TransitionPositionListener extends AnimatorListenerAdapter implements TransitionListener
    {
        private final View mMovingView;
        private float mPausedX;
        private float mPausedY;
        private final int mStartX;
        private final int mStartY;
        private final float mTerminalX;
        private final float mTerminalY;
        private int[] mTransitionPosition;
        private final View mViewInHierarchy;
        
        TransitionPositionListener(final View mMovingView, final View mViewInHierarchy, int transition_position, final int n, final float mTerminalX, final float mTerminalY) {
            this.mMovingView = mMovingView;
            this.mViewInHierarchy = mViewInHierarchy;
            this.mStartX = transition_position - Math.round(mMovingView.getTranslationX());
            this.mStartY = n - Math.round(mMovingView.getTranslationY());
            this.mTerminalX = mTerminalX;
            this.mTerminalY = mTerminalY;
            transition_position = R.id.transition_position;
            final int[] mTransitionPosition = (int[])mViewInHierarchy.getTag(transition_position);
            this.mTransitionPosition = mTransitionPosition;
            if (mTransitionPosition != null) {
                mViewInHierarchy.setTag(transition_position, (Object)null);
            }
        }
        
        public void onAnimationCancel(final Animator animator) {
            if (this.mTransitionPosition == null) {
                this.mTransitionPosition = new int[2];
            }
            this.mTransitionPosition[0] = Math.round(this.mStartX + this.mMovingView.getTranslationX());
            this.mTransitionPosition[1] = Math.round(this.mStartY + this.mMovingView.getTranslationY());
            this.mViewInHierarchy.setTag(R.id.transition_position, (Object)this.mTransitionPosition);
        }
        
        public void onAnimationPause(final Animator animator) {
            this.mPausedX = this.mMovingView.getTranslationX();
            this.mPausedY = this.mMovingView.getTranslationY();
            this.mMovingView.setTranslationX(this.mTerminalX);
            this.mMovingView.setTranslationY(this.mTerminalY);
        }
        
        public void onAnimationResume(final Animator animator) {
            this.mMovingView.setTranslationX(this.mPausedX);
            this.mMovingView.setTranslationY(this.mPausedY);
        }
        
        public void onTransitionCancel(@NonNull final Transition transition) {
        }
        
        public void onTransitionEnd(@NonNull final Transition transition) {
            this.mMovingView.setTranslationX(this.mTerminalX);
            this.mMovingView.setTranslationY(this.mTerminalY);
            transition.removeListener((Transition.TransitionListener)this);
        }
        
        public void onTransitionPause(@NonNull final Transition transition) {
        }
        
        public void onTransitionResume(@NonNull final Transition transition) {
        }
        
        public void onTransitionStart(@NonNull final Transition transition) {
        }
    }
}

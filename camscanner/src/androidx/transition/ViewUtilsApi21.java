// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.annotation.SuppressLint;
import androidx.annotation.Nullable;
import android.graphics.Matrix;
import androidx.annotation.NonNull;
import android.view.View;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
class ViewUtilsApi21 extends ViewUtilsApi19
{
    private static boolean sTryHiddenSetAnimationMatrix = true;
    private static boolean sTryHiddenTransformMatrixToGlobal = true;
    private static boolean sTryHiddenTransformMatrixToLocal = true;
    
    @SuppressLint({ "NewApi" })
    @Override
    public void setAnimationMatrix(@NonNull final View view, @Nullable final Matrix matrix) {
        if (ViewUtilsApi21.sTryHiddenSetAnimationMatrix) {
            try {
                \u300780\u3007808\u3007O.\u3007080(view, matrix);
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                ViewUtilsApi21.sTryHiddenSetAnimationMatrix = false;
            }
        }
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public void transformMatrixToGlobal(@NonNull final View view, @NonNull final Matrix matrix) {
        if (ViewUtilsApi21.sTryHiddenTransformMatrixToGlobal) {
            try {
                \u30078o8o\u3007.\u3007080(view, matrix);
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                ViewUtilsApi21.sTryHiddenTransformMatrixToGlobal = false;
            }
        }
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public void transformMatrixToLocal(@NonNull final View view, @NonNull final Matrix matrix) {
        if (ViewUtilsApi21.sTryHiddenTransformMatrixToLocal) {
            try {
                OO0o\u3007\u3007\u3007\u30070.\u3007080(view, matrix);
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                ViewUtilsApi21.sTryHiddenTransformMatrixToLocal = false;
            }
        }
    }
}

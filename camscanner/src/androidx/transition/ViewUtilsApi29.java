// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import androidx.annotation.Nullable;
import android.graphics.Matrix;
import androidx.annotation.NonNull;
import android.view.View;
import androidx.annotation.RequiresApi;

@RequiresApi(29)
class ViewUtilsApi29 extends ViewUtilsApi23
{
    @Override
    public float getTransitionAlpha(@NonNull final View view) {
        return oO80.\u3007080(view);
    }
    
    @Override
    public void setAnimationMatrix(@NonNull final View view, @Nullable final Matrix matrix) {
        \u300780\u3007808\u3007O.\u3007080(view, matrix);
    }
    
    @Override
    public void setLeftTopRightBottom(@NonNull final View view, final int n, final int n2, final int n3, final int n4) {
        \u3007O8o08O.\u3007080(view, n, n2, n3, n4);
    }
    
    @Override
    public void setTransitionAlpha(@NonNull final View view, final float n) {
        \u3007\u3007888.\u3007080(view, n);
    }
    
    @Override
    public void setTransitionVisibility(@NonNull final View view, final int n) {
        OO0o\u3007\u3007.\u3007080(view, n);
    }
    
    @Override
    public void transformMatrixToGlobal(@NonNull final View view, @NonNull final Matrix matrix) {
        \u30078o8o\u3007.\u3007080(view, matrix);
    }
    
    @Override
    public void transformMatrixToLocal(@NonNull final View view, @NonNull final Matrix matrix) {
        OO0o\u3007\u3007\u3007\u30070.\u3007080(view, matrix);
    }
}

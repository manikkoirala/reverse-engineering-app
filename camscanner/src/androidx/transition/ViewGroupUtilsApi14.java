// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.view.View;
import android.animation.Animator;
import androidx.annotation.NonNull;
import android.view.ViewGroup;
import java.lang.reflect.Field;
import android.animation.LayoutTransition;
import java.lang.reflect.Method;

class ViewGroupUtilsApi14
{
    private static final int LAYOUT_TRANSITION_CHANGING = 4;
    private static final String TAG = "ViewGroupUtilsApi14";
    private static Method sCancelMethod;
    private static boolean sCancelMethodFetched;
    private static LayoutTransition sEmptyLayoutTransition;
    private static Field sLayoutSuppressedField;
    private static boolean sLayoutSuppressedFieldFetched;
    
    private ViewGroupUtilsApi14() {
    }
    
    private static void cancelLayoutTransition(final LayoutTransition p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: ifne            31
        //     6: ldc             Landroid/animation/LayoutTransition;.class
        //     8: ldc             "cancel"
        //    10: iconst_0       
        //    11: anewarray       Ljava/lang/Class;
        //    14: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //    17: astore_1       
        //    18: aload_1        
        //    19: putstatic       androidx/transition/ViewGroupUtilsApi14.sCancelMethod:Ljava/lang/reflect/Method;
        //    22: aload_1        
        //    23: iconst_1       
        //    24: invokevirtual   java/lang/reflect/AccessibleObject.setAccessible:(Z)V
        //    27: iconst_1       
        //    28: putstatic       androidx/transition/ViewGroupUtilsApi14.sCancelMethodFetched:Z
        //    31: getstatic       androidx/transition/ViewGroupUtilsApi14.sCancelMethod:Ljava/lang/reflect/Method;
        //    34: astore_1       
        //    35: aload_1        
        //    36: ifnull          49
        //    39: aload_1        
        //    40: aload_0        
        //    41: iconst_0       
        //    42: anewarray       Ljava/lang/Object;
        //    45: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //    48: pop            
        //    49: return         
        //    50: astore_1       
        //    51: goto            27
        //    54: astore_0       
        //    55: goto            49
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                         
        //  -----  -----  -----  -----  ---------------------------------------------
        //  6      27     50     54     Ljava/lang/NoSuchMethodException;
        //  39     49     54     58     Ljava/lang/IllegalAccessException;
        //  39     49     54     58     Ljava/lang/reflect/InvocationTargetException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0049:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    static void suppressLayout(@NonNull final ViewGroup viewGroup, boolean boolean1) {
        final LayoutTransition sEmptyLayoutTransition = ViewGroupUtilsApi14.sEmptyLayoutTransition;
        final boolean b = false;
        if (sEmptyLayoutTransition == null) {
            (ViewGroupUtilsApi14.sEmptyLayoutTransition = new LayoutTransition() {
                public boolean isChangingLayout() {
                    return true;
                }
            }).setAnimator(2, (Animator)null);
            ViewGroupUtilsApi14.sEmptyLayoutTransition.setAnimator(0, (Animator)null);
            ViewGroupUtilsApi14.sEmptyLayoutTransition.setAnimator(1, (Animator)null);
            ViewGroupUtilsApi14.sEmptyLayoutTransition.setAnimator(3, (Animator)null);
            ViewGroupUtilsApi14.sEmptyLayoutTransition.setAnimator(4, (Animator)null);
        }
        if (boolean1) {
            final LayoutTransition layoutTransition = viewGroup.getLayoutTransition();
            if (layoutTransition != null) {
                if (layoutTransition.isRunning()) {
                    cancelLayoutTransition(layoutTransition);
                }
                if (layoutTransition != ViewGroupUtilsApi14.sEmptyLayoutTransition) {
                    ((View)viewGroup).setTag(R.id.transition_layout_save, (Object)layoutTransition);
                }
            }
            viewGroup.setLayoutTransition(ViewGroupUtilsApi14.sEmptyLayoutTransition);
            return;
        }
        viewGroup.setLayoutTransition((LayoutTransition)null);
        Label_0155: {
            if (ViewGroupUtilsApi14.sLayoutSuppressedFieldFetched) {
                break Label_0155;
            }
            while (true) {
                try {
                    (ViewGroupUtilsApi14.sLayoutSuppressedField = ViewGroup.class.getDeclaredField("mLayoutSuppressed")).setAccessible(true);
                    ViewGroupUtilsApi14.sLayoutSuppressedFieldFetched = true;
                    final Field sLayoutSuppressedField = ViewGroupUtilsApi14.sLayoutSuppressedField;
                    boolean1 = b;
                    if (sLayoutSuppressedField != null) {
                        try {
                            boolean1 = sLayoutSuppressedField.getBoolean(viewGroup);
                            if (boolean1) {
                                try {
                                    ViewGroupUtilsApi14.sLayoutSuppressedField.setBoolean(viewGroup, false);
                                }
                                catch (final IllegalAccessException ex) {}
                            }
                        }
                        catch (final IllegalAccessException ex2) {
                            boolean1 = b;
                        }
                    }
                    if (boolean1) {
                        ((View)viewGroup).requestLayout();
                    }
                    final int transition_layout_save = R.id.transition_layout_save;
                    final LayoutTransition layoutTransition2 = (LayoutTransition)((View)viewGroup).getTag(transition_layout_save);
                    if (layoutTransition2 != null) {
                        ((View)viewGroup).setTag(transition_layout_save, (Object)null);
                        viewGroup.setLayoutTransition(layoutTransition2);
                    }
                }
                catch (final NoSuchFieldException ex3) {
                    continue;
                }
                break;
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.transition;

import android.view.LayoutInflater;
import android.util.SparseArray;
import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.view.ViewGroup;
import android.view.View;
import android.content.Context;

public class Scene
{
    private Context mContext;
    private Runnable mEnterAction;
    private Runnable mExitAction;
    private View mLayout;
    private int mLayoutId;
    private ViewGroup mSceneRoot;
    
    public Scene(@NonNull final ViewGroup mSceneRoot) {
        this.mLayoutId = -1;
        this.mSceneRoot = mSceneRoot;
    }
    
    private Scene(final ViewGroup mSceneRoot, final int mLayoutId, final Context mContext) {
        this.mContext = mContext;
        this.mSceneRoot = mSceneRoot;
        this.mLayoutId = mLayoutId;
    }
    
    public Scene(@NonNull final ViewGroup mSceneRoot, @NonNull final View mLayout) {
        this.mLayoutId = -1;
        this.mSceneRoot = mSceneRoot;
        this.mLayout = mLayout;
    }
    
    @Nullable
    public static Scene getCurrentScene(@NonNull final ViewGroup viewGroup) {
        return (Scene)((View)viewGroup).getTag(R.id.transition_current_scene);
    }
    
    @NonNull
    public static Scene getSceneForLayout(@NonNull final ViewGroup viewGroup, @LayoutRes final int n, @NonNull final Context context) {
        final int transition_scene_layoutid_cache = R.id.transition_scene_layoutid_cache;
        SparseArray sparseArray;
        if ((sparseArray = (SparseArray)((View)viewGroup).getTag(transition_scene_layoutid_cache)) == null) {
            sparseArray = new SparseArray();
            ((View)viewGroup).setTag(transition_scene_layoutid_cache, (Object)sparseArray);
        }
        final Scene scene = (Scene)sparseArray.get(n);
        if (scene != null) {
            return scene;
        }
        final Scene scene2 = new Scene(viewGroup, n, context);
        sparseArray.put(n, (Object)scene2);
        return scene2;
    }
    
    static void setCurrentScene(@NonNull final ViewGroup viewGroup, @Nullable final Scene scene) {
        ((View)viewGroup).setTag(R.id.transition_current_scene, (Object)scene);
    }
    
    public void enter() {
        if (this.mLayoutId > 0 || this.mLayout != null) {
            this.getSceneRoot().removeAllViews();
            if (this.mLayoutId > 0) {
                LayoutInflater.from(this.mContext).inflate(this.mLayoutId, this.mSceneRoot);
            }
            else {
                this.mSceneRoot.addView(this.mLayout);
            }
        }
        final Runnable mEnterAction = this.mEnterAction;
        if (mEnterAction != null) {
            mEnterAction.run();
        }
        setCurrentScene(this.mSceneRoot, this);
    }
    
    public void exit() {
        if (getCurrentScene(this.mSceneRoot) == this) {
            final Runnable mExitAction = this.mExitAction;
            if (mExitAction != null) {
                mExitAction.run();
            }
        }
    }
    
    @NonNull
    public ViewGroup getSceneRoot() {
        return this.mSceneRoot;
    }
    
    boolean isCreatedFromLayoutResource() {
        return this.mLayoutId > 0;
    }
    
    public void setEnterAction(@Nullable final Runnable mEnterAction) {
        this.mEnterAction = mEnterAction;
    }
    
    public void setExitAction(@Nullable final Runnable mExitAction) {
        this.mExitAction = mExitAction;
    }
}

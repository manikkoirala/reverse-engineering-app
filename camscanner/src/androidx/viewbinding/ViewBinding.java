// 
// Decompiled by Procyon v0.6.0
// 

package androidx.viewbinding;

import androidx.annotation.NonNull;
import android.view.View;

public interface ViewBinding
{
    @NonNull
    View getRoot();
}

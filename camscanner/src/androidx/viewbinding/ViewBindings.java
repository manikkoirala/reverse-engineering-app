// 
// Decompiled by Procyon v0.6.0
// 

package androidx.viewbinding;

import androidx.annotation.Nullable;
import android.view.ViewGroup;
import androidx.annotation.IdRes;
import android.view.View;

public class ViewBindings
{
    private ViewBindings() {
    }
    
    @Nullable
    public static <T extends View> T findChildViewById(final View view, @IdRes final int n) {
        if (!(view instanceof ViewGroup)) {
            return null;
        }
        final ViewGroup viewGroup = (ViewGroup)view;
        for (int childCount = viewGroup.getChildCount(), i = 0; i < childCount; ++i) {
            final View viewById = viewGroup.getChildAt(i).findViewById(n);
            if (viewById != null) {
                return (T)viewById;
            }
        }
        return null;
    }
}

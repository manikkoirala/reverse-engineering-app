// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.webkit.internal.WebViewFeatureInternal;
import androidx.webkit.internal.SafeBrowsingResponseImpl;
import android.webkit.SafeBrowsingResponse;
import android.webkit.WebResourceResponse;
import java.lang.reflect.InvocationHandler;
import androidx.annotation.RequiresApi;
import androidx.webkit.internal.WebResourceErrorImpl;
import android.os.Build$VERSION;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import androidx.annotation.UiThread;
import android.webkit.WebView;
import androidx.annotation.RestrictTo;
import androidx.annotation.NonNull;
import org.chromium.support_lib_boundary.WebViewClientBoundaryInterface;
import android.webkit.WebViewClient;

public class WebViewClientCompat extends WebViewClient implements WebViewClientBoundaryInterface
{
    private static final String[] sSupportedFeatures;
    
    static {
        sSupportedFeatures = new String[] { "VISUAL_STATE_CALLBACK", "RECEIVE_WEB_RESOURCE_ERROR", "RECEIVE_HTTP_ERROR", "SHOULD_OVERRIDE_WITH_REDIRECTS", "SAFE_BROWSING_HIT" };
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public final String[] getSupportedFeatures() {
        return WebViewClientCompat.sSupportedFeatures;
    }
    
    @UiThread
    public void onPageCommitVisible(@NonNull final WebView webView, @NonNull final String s) {
    }
    
    @RequiresApi(23)
    public final void onReceivedError(@NonNull final WebView webView, @NonNull final WebResourceRequest webResourceRequest, @NonNull final WebResourceError webResourceError) {
        if (Build$VERSION.SDK_INT < 23) {
            return;
        }
        this.onReceivedError(webView, webResourceRequest, new WebResourceErrorImpl(webResourceError));
    }
    
    @RequiresApi(21)
    @UiThread
    public void onReceivedError(@NonNull final WebView webView, @NonNull final WebResourceRequest webResourceRequest, @NonNull final WebResourceErrorCompat webResourceErrorCompat) {
        if (WebViewFeature.isFeatureSupported("WEB_RESOURCE_ERROR_GET_CODE")) {
            if (WebViewFeature.isFeatureSupported("WEB_RESOURCE_ERROR_GET_DESCRIPTION")) {
                if (webResourceRequest.isForMainFrame()) {
                    this.onReceivedError(webView, webResourceErrorCompat.getErrorCode(), webResourceErrorCompat.getDescription().toString(), webResourceRequest.getUrl().toString());
                }
            }
        }
    }
    
    @RequiresApi(21)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public final void onReceivedError(@NonNull final WebView webView, @NonNull final WebResourceRequest webResourceRequest, @NonNull final InvocationHandler invocationHandler) {
        this.onReceivedError(webView, webResourceRequest, new WebResourceErrorImpl(invocationHandler));
    }
    
    @UiThread
    public void onReceivedHttpError(@NonNull final WebView webView, @NonNull final WebResourceRequest webResourceRequest, @NonNull final WebResourceResponse webResourceResponse) {
    }
    
    @RequiresApi(27)
    public final void onSafeBrowsingHit(@NonNull final WebView webView, @NonNull final WebResourceRequest webResourceRequest, final int n, @NonNull final SafeBrowsingResponse safeBrowsingResponse) {
        this.onSafeBrowsingHit(webView, webResourceRequest, n, new SafeBrowsingResponseImpl(safeBrowsingResponse));
    }
    
    @UiThread
    public void onSafeBrowsingHit(@NonNull final WebView webView, @NonNull final WebResourceRequest webResourceRequest, final int n, @NonNull final SafeBrowsingResponseCompat safeBrowsingResponseCompat) {
        if (WebViewFeature.isFeatureSupported("SAFE_BROWSING_RESPONSE_SHOW_INTERSTITIAL")) {
            safeBrowsingResponseCompat.showInterstitial(true);
            return;
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public final void onSafeBrowsingHit(@NonNull final WebView webView, @NonNull final WebResourceRequest webResourceRequest, final int n, @NonNull final InvocationHandler invocationHandler) {
        this.onSafeBrowsingHit(webView, webResourceRequest, n, new SafeBrowsingResponseImpl(invocationHandler));
    }
    
    @RequiresApi(21)
    @UiThread
    public boolean shouldOverrideUrlLoading(@NonNull final WebView webView, @NonNull final WebResourceRequest webResourceRequest) {
        return this.shouldOverrideUrlLoading(webView, webResourceRequest.getUrl().toString());
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface SafeBrowsingThreat {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;

public abstract class WebResourceErrorCompat
{
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public WebResourceErrorCompat() {
    }
    
    @NonNull
    public abstract CharSequence getDescription();
    
    public abstract int getErrorCode();
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface NetErrorCode {
    }
}

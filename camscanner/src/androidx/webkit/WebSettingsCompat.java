// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.annotation.RestrictTo;
import android.annotation.SuppressLint;
import androidx.webkit.internal.WebViewFeatureInternal;
import androidx.annotation.NonNull;
import androidx.webkit.internal.WebViewGlueCommunicator;
import androidx.webkit.internal.WebSettingsAdapter;
import android.webkit.WebSettings;

public class WebSettingsCompat
{
    public static final int DARK_STRATEGY_PREFER_WEB_THEME_OVER_USER_AGENT_DARKENING = 2;
    public static final int DARK_STRATEGY_USER_AGENT_DARKENING_ONLY = 0;
    public static final int DARK_STRATEGY_WEB_THEME_DARKENING_ONLY = 1;
    public static final int FORCE_DARK_AUTO = 1;
    public static final int FORCE_DARK_OFF = 0;
    public static final int FORCE_DARK_ON = 2;
    
    private WebSettingsCompat() {
    }
    
    private static WebSettingsAdapter getAdapter(final WebSettings webSettings) {
        return WebViewGlueCommunicator.getCompatConverter().convertSettings(webSettings);
    }
    
    @SuppressLint({ "NewApi" })
    public static int getDisabledActionModeMenuItems(@NonNull final WebSettings webSettings) {
        final WebViewFeatureInternal disabled_ACTION_MODE_MENU_ITEMS = WebViewFeatureInternal.DISABLED_ACTION_MODE_MENU_ITEMS;
        if (disabled_ACTION_MODE_MENU_ITEMS.isSupportedByFramework()) {
            return Oo08.\u3007080(webSettings);
        }
        if (disabled_ACTION_MODE_MENU_ITEMS.isSupportedByWebView()) {
            return getAdapter(webSettings).getDisabledActionModeMenuItems();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @SuppressLint({ "NewApi" })
    public static int getForceDark(@NonNull final WebSettings webSettings) {
        final WebViewFeatureInternal force_DARK = WebViewFeatureInternal.FORCE_DARK;
        if (force_DARK.isSupportedByFramework()) {
            return o\u30070.\u3007080(webSettings);
        }
        if (force_DARK.isSupportedByWebView()) {
            return getAdapter(webSettings).getForceDark();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @SuppressLint({ "NewApi" })
    public static int getForceDarkStrategy(@NonNull final WebSettings webSettings) {
        if (WebViewFeatureInternal.FORCE_DARK_STRATEGY.isSupportedByWebView()) {
            return getAdapter(webSettings).getForceDark();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @SuppressLint({ "NewApi" })
    public static boolean getOffscreenPreRaster(@NonNull final WebSettings webSettings) {
        final WebViewFeatureInternal off_SCREEN_PRERASTER = WebViewFeatureInternal.OFF_SCREEN_PRERASTER;
        if (off_SCREEN_PRERASTER.isSupportedByFramework()) {
            return O8.\u3007080(webSettings);
        }
        if (off_SCREEN_PRERASTER.isSupportedByWebView()) {
            return getAdapter(webSettings).getOffscreenPreRaster();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @SuppressLint({ "NewApi" })
    public static boolean getSafeBrowsingEnabled(@NonNull final WebSettings webSettings) {
        final WebViewFeatureInternal safe_BROWSING_ENABLE = WebViewFeatureInternal.SAFE_BROWSING_ENABLE;
        if (safe_BROWSING_ENABLE.isSupportedByFramework()) {
            return \u3007o\u3007.\u3007080(webSettings);
        }
        if (safe_BROWSING_ENABLE.isSupportedByWebView()) {
            return getAdapter(webSettings).getSafeBrowsingEnabled();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @SuppressLint({ "NewApi" })
    public static void setDisabledActionModeMenuItems(@NonNull final WebSettings webSettings, final int disabledActionModeMenuItems) {
        final WebViewFeatureInternal disabled_ACTION_MODE_MENU_ITEMS = WebViewFeatureInternal.DISABLED_ACTION_MODE_MENU_ITEMS;
        if (disabled_ACTION_MODE_MENU_ITEMS.isSupportedByFramework()) {
            \u300780\u3007808\u3007O.\u3007080(webSettings, disabledActionModeMenuItems);
        }
        else {
            if (!disabled_ACTION_MODE_MENU_ITEMS.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            getAdapter(webSettings).setDisabledActionModeMenuItems(disabledActionModeMenuItems);
        }
    }
    
    @SuppressLint({ "NewApi" })
    public static void setForceDark(@NonNull final WebSettings webSettings, final int forceDark) {
        final WebViewFeatureInternal force_DARK = WebViewFeatureInternal.FORCE_DARK;
        if (force_DARK.isSupportedByFramework()) {
            \u3007o00\u3007\u3007Oo.\u3007080(webSettings, forceDark);
        }
        else {
            if (!force_DARK.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            getAdapter(webSettings).setForceDark(forceDark);
        }
    }
    
    @SuppressLint({ "NewApi" })
    public static void setForceDarkStrategy(@NonNull final WebSettings webSettings, final int forceDarkStrategy) {
        if (WebViewFeatureInternal.FORCE_DARK_STRATEGY.isSupportedByWebView()) {
            getAdapter(webSettings).setForceDarkStrategy(forceDarkStrategy);
            return;
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @SuppressLint({ "NewApi" })
    public static void setOffscreenPreRaster(@NonNull final WebSettings webSettings, final boolean offscreenPreRaster) {
        final WebViewFeatureInternal off_SCREEN_PRERASTER = WebViewFeatureInternal.OFF_SCREEN_PRERASTER;
        if (off_SCREEN_PRERASTER.isSupportedByFramework()) {
            oO80.\u3007080(webSettings, offscreenPreRaster);
        }
        else {
            if (!off_SCREEN_PRERASTER.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            getAdapter(webSettings).setOffscreenPreRaster(offscreenPreRaster);
        }
    }
    
    @SuppressLint({ "NewApi" })
    public static void setSafeBrowsingEnabled(@NonNull final WebSettings webSettings, final boolean safeBrowsingEnabled) {
        final WebViewFeatureInternal safe_BROWSING_ENABLE = WebViewFeatureInternal.SAFE_BROWSING_ENABLE;
        if (safe_BROWSING_ENABLE.isSupportedByFramework()) {
            \u3007\u3007888.\u3007080(webSettings, safeBrowsingEnabled);
        }
        else {
            if (!safe_BROWSING_ENABLE.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            getAdapter(webSettings).setSafeBrowsingEnabled(safeBrowsingEnabled);
        }
    }
    
    @SuppressLint({ "NewApi" })
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static void setWillSuppressErrorPage(@NonNull final WebSettings webSettings, final boolean willSuppressErrorPage) {
        if (WebViewFeatureInternal.SUPPRESS_ERROR_PAGE.isSupportedByWebView()) {
            getAdapter(webSettings).setWillSuppressErrorPage(willSuppressErrorPage);
            return;
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @SuppressLint({ "NewApi" })
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static boolean willSuppressErrorPage(@NonNull final WebSettings webSettings) {
        if (WebViewFeatureInternal.SUPPRESS_ERROR_PAGE.isSupportedByWebView()) {
            return getAdapter(webSettings).willSuppressErrorPage();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @Target({ ElementType.PARAMETER, ElementType.METHOD })
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface ForceDark {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @Target({ ElementType.PARAMETER, ElementType.METHOD })
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface ForceDarkStrategy {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @Target({ ElementType.PARAMETER, ElementType.METHOD })
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface MenuItemFlags {
    }
}

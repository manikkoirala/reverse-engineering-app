// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import androidx.annotation.UiThread;
import java.util.concurrent.Executor;
import java.util.HashSet;
import java.util.List;
import java.util.Collection;
import java.util.ArrayList;
import android.webkit.ValueCallback;
import android.webkit.WebView$VisualStateCallback;
import androidx.webkit.internal.WebViewRenderProcessClientFrameworkAdapter;
import androidx.webkit.internal.WebViewRenderProcessImpl;
import android.webkit.WebViewClient;
import android.webkit.WebChromeClient;
import androidx.webkit.internal.WebViewProviderAdapter;
import android.content.pm.PackageManager$NameNotFoundException;
import androidx.webkit.internal.WebViewGlueCommunicator;
import androidx.webkit.internal.WebViewProviderFactory;
import androidx.annotation.Nullable;
import android.content.pm.PackageInfo;
import android.content.Context;
import android.annotation.SuppressLint;
import androidx.webkit.internal.WebMessagePortImpl;
import org.chromium.support_lib_boundary.WebViewProviderBoundaryInterface;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import android.os.Looper;
import android.os.Build$VERSION;
import androidx.annotation.RestrictTo;
import androidx.webkit.internal.WebViewFeatureInternal;
import java.util.Set;
import androidx.annotation.NonNull;
import android.webkit.WebView;
import android.net.Uri;

public class WebViewCompat
{
    private static final Uri EMPTY_URI;
    private static final Uri WILDCARD_URI;
    
    static {
        WILDCARD_URI = Uri.parse("*");
        EMPTY_URI = Uri.parse("");
    }
    
    private WebViewCompat() {
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static ScriptReferenceCompat addDocumentStartJavaScript(@NonNull final WebView webView, @NonNull final String s, @NonNull final Set<String> set) {
        if (WebViewFeatureInternal.DOCUMENT_START_SCRIPT.isSupportedByWebView()) {
            return getProvider(webView).addDocumentStartJavaScript(s, set.toArray(new String[0]));
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    public static void addWebMessageListener(@NonNull final WebView webView, @NonNull final String s, @NonNull final Set<String> set, @NonNull final WebMessageListener webMessageListener) {
        if (WebViewFeatureInternal.WEB_MESSAGE_LISTENER.isSupportedByWebView()) {
            getProvider(webView).addWebMessageListener(s, set.toArray(new String[0]), webMessageListener);
            return;
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    private static void checkThread(final WebView obj) {
        if (Build$VERSION.SDK_INT >= 28) {
            if (o800o8O.\u3007080(obj) == Looper.myLooper()) {
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("A WebView method was called on thread '");
            sb.append(Thread.currentThread().getName());
            sb.append("'. All WebView methods must be called on the same thread. (Expected Looper ");
            sb.append(o800o8O.\u3007080(obj));
            sb.append(" called on ");
            sb.append(Looper.myLooper());
            sb.append(", FYI main Looper is ");
            sb.append(Looper.getMainLooper());
            sb.append(")");
            throw new RuntimeException(sb.toString());
        }
        try {
            final Method declaredMethod = WebView.class.getDeclaredMethod("checkThread", (Class<?>[])new Class[0]);
            declaredMethod.setAccessible(true);
            declaredMethod.invoke(obj, new Object[0]);
        }
        catch (final InvocationTargetException cause) {
            throw new RuntimeException(cause);
        }
        catch (final IllegalAccessException cause2) {
            throw new RuntimeException(cause2);
        }
        catch (final NoSuchMethodException cause3) {
            throw new RuntimeException(cause3);
        }
    }
    
    private static WebViewProviderBoundaryInterface createProvider(final WebView webView) {
        return getFactory().createWebView(webView);
    }
    
    @SuppressLint({ "NewApi" })
    @NonNull
    public static WebMessagePortCompat[] createWebMessageChannel(@NonNull final WebView webView) {
        final WebViewFeatureInternal create_WEB_MESSAGE_CHANNEL = WebViewFeatureInternal.CREATE_WEB_MESSAGE_CHANNEL;
        if (create_WEB_MESSAGE_CHANNEL.isSupportedByFramework()) {
            return WebMessagePortImpl.portsToCompat(oo88o8O.\u3007080(webView));
        }
        if (create_WEB_MESSAGE_CHANNEL.isSupportedByWebView()) {
            return getProvider(webView).createWebMessageChannel();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @Nullable
    public static PackageInfo getCurrentWebViewPackage(@NonNull final Context context) {
        if (Build$VERSION.SDK_INT >= 26) {
            return \u3007\u3007808\u3007.\u3007080();
        }
        try {
            final PackageInfo loadedWebViewPackageInfo = getLoadedWebViewPackageInfo();
            if (loadedWebViewPackageInfo != null) {
                return loadedWebViewPackageInfo;
            }
            return getNotYetLoadedWebViewPackageInfo(context);
        }
        catch (final ClassNotFoundException | IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
            return null;
        }
    }
    
    private static WebViewProviderFactory getFactory() {
        return WebViewGlueCommunicator.getFactory();
    }
    
    @SuppressLint({ "PrivateApi" })
    private static PackageInfo getLoadedWebViewPackageInfo() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        return (PackageInfo)Class.forName("android.webkit.WebViewFactory").getMethod("getLoadedPackageInfo", (Class<?>[])new Class[0]).invoke(null, new Object[0]);
    }
    
    @SuppressLint({ "PrivateApi" })
    private static PackageInfo getNotYetLoadedWebViewPackageInfo(final Context context) {
        try {
            String s;
            if (Build$VERSION.SDK_INT <= 23) {
                s = (String)Class.forName("android.webkit.WebViewFactory").getMethod("getWebViewPackageName", (Class<?>[])new Class[0]).invoke(null, new Object[0]);
            }
            else {
                s = (String)Class.forName("android.webkit.WebViewUpdateService").getMethod("getCurrentWebViewPackageName", (Class<?>[])new Class[0]).invoke(null, new Object[0]);
            }
            if (s == null) {
                return null;
            }
            return context.getPackageManager().getPackageInfo(s, 0);
        }
        catch (final ClassNotFoundException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | PackageManager$NameNotFoundException ex) {
            return null;
        }
    }
    
    private static WebViewProviderAdapter getProvider(final WebView webView) {
        return new WebViewProviderAdapter(createProvider(webView));
    }
    
    @SuppressLint({ "NewApi" })
    @NonNull
    public static Uri getSafeBrowsingPrivacyPolicyUrl() {
        final WebViewFeatureInternal safe_BROWSING_PRIVACY_POLICY_URL = WebViewFeatureInternal.SAFE_BROWSING_PRIVACY_POLICY_URL;
        if (safe_BROWSING_PRIVACY_POLICY_URL.isSupportedByFramework()) {
            return \u30078o8o\u3007.\u3007080();
        }
        if (safe_BROWSING_PRIVACY_POLICY_URL.isSupportedByWebView()) {
            return getFactory().getStatics().getSafeBrowsingPrivacyPolicyUrl();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @SuppressLint({ "NewApi" })
    @Nullable
    public static WebChromeClient getWebChromeClient(@NonNull final WebView webView) {
        final WebViewFeatureInternal get_WEB_CHROME_CLIENT = WebViewFeatureInternal.GET_WEB_CHROME_CLIENT;
        if (get_WEB_CHROME_CLIENT.isSupportedByFramework()) {
            return \u3007O00.\u3007080(webView);
        }
        if (get_WEB_CHROME_CLIENT.isSupportedByWebView()) {
            return getProvider(webView).getWebChromeClient();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @SuppressLint({ "NewApi" })
    @NonNull
    public static WebViewClient getWebViewClient(@NonNull final WebView webView) {
        final WebViewFeatureInternal get_WEB_VIEW_CLIENT = WebViewFeatureInternal.GET_WEB_VIEW_CLIENT;
        if (get_WEB_VIEW_CLIENT.isSupportedByFramework()) {
            return \u3007\u30078O0\u30078.\u3007080(webView);
        }
        if (get_WEB_VIEW_CLIENT.isSupportedByWebView()) {
            return getProvider(webView).getWebViewClient();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @SuppressLint({ "NewApi" })
    @Nullable
    public static WebViewRenderProcess getWebViewRenderProcess(@NonNull final WebView webView) {
        final WebViewFeatureInternal get_WEB_VIEW_RENDERER = WebViewFeatureInternal.GET_WEB_VIEW_RENDERER;
        if (get_WEB_VIEW_RENDERER.isSupportedByFramework()) {
            final android.webkit.WebViewRenderProcess \u3007080 = OO0o\u3007\u3007\u3007\u30070.\u3007080(webView);
            WebViewRenderProcessImpl forFrameworkObject;
            if (\u3007080 != null) {
                forFrameworkObject = WebViewRenderProcessImpl.forFrameworkObject(\u3007080);
            }
            else {
                forFrameworkObject = null;
            }
            return forFrameworkObject;
        }
        if (get_WEB_VIEW_RENDERER.isSupportedByWebView()) {
            return getProvider(webView).getWebViewRenderProcess();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @SuppressLint({ "NewApi" })
    @Nullable
    public static WebViewRenderProcessClient getWebViewRenderProcessClient(@NonNull final WebView webView) {
        final WebViewFeatureInternal web_VIEW_RENDERER_CLIENT_BASIC_USAGE = WebViewFeatureInternal.WEB_VIEW_RENDERER_CLIENT_BASIC_USAGE;
        if (web_VIEW_RENDERER_CLIENT_BASIC_USAGE.isSupportedByFramework()) {
            final android.webkit.WebViewRenderProcessClient \u3007080 = OO0o\u3007\u3007.\u3007080(webView);
            if (\u3007080 != null && \u3007080 instanceof WebViewRenderProcessClientFrameworkAdapter) {
                return ((WebViewRenderProcessClientFrameworkAdapter)\u3007080).getFrameworkRenderProcessClient();
            }
            return null;
        }
        else {
            if (web_VIEW_RENDERER_CLIENT_BASIC_USAGE.isSupportedByWebView()) {
                return getProvider(webView).getWebViewRenderProcessClient();
            }
            throw WebViewFeatureInternal.getUnsupportedOperationException();
        }
    }
    
    public static boolean isMultiProcessEnabled() {
        if (WebViewFeatureInternal.MULTI_PROCESS.isSupportedByWebView()) {
            return getFactory().getStatics().isMultiProcessEnabled();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    public static void postVisualStateCallback(@NonNull final WebView webView, final long n, @NonNull final VisualStateCallback visualStateCallback) {
        final WebViewFeatureInternal visual_STATE_CALLBACK = WebViewFeatureInternal.VISUAL_STATE_CALLBACK;
        if (visual_STATE_CALLBACK.isSupportedByFramework()) {
            OoO8.\u3007080(webView, n, (WebView$VisualStateCallback)new WebView$VisualStateCallback(visualStateCallback) {
                final VisualStateCallback val$callback;
                
                public void onComplete(final long n) {
                    this.val$callback.onComplete(n);
                }
            });
        }
        else {
            if (!visual_STATE_CALLBACK.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            checkThread(webView);
            getProvider(webView).insertVisualStateCallback(n, visualStateCallback);
        }
    }
    
    @SuppressLint({ "NewApi" })
    public static void postWebMessage(@NonNull final WebView webView, @NonNull final WebMessageCompat webMessageCompat, @NonNull final Uri uri) {
        Uri empty_URI = uri;
        if (WebViewCompat.WILDCARD_URI.equals((Object)uri)) {
            empty_URI = WebViewCompat.EMPTY_URI;
        }
        final WebViewFeatureInternal post_WEB_MESSAGE = WebViewFeatureInternal.POST_WEB_MESSAGE;
        if (post_WEB_MESSAGE.isSupportedByFramework()) {
            Oooo8o0\u3007.\u3007080(webView, WebMessagePortImpl.compatToFrameworkMessage(webMessageCompat), empty_URI);
        }
        else {
            if (!post_WEB_MESSAGE.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            getProvider(webView).postWebMessage(webMessageCompat, empty_URI);
        }
    }
    
    public static void removeWebMessageListener(@NonNull final WebView webView, @NonNull final String s) {
        if (WebViewFeatureInternal.WEB_MESSAGE_LISTENER.isSupportedByWebView()) {
            getProvider(webView).removeWebMessageListener(s);
            return;
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @SuppressLint({ "NewApi" })
    public static void setSafeBrowsingAllowlist(@NonNull final Set<String> c, @Nullable final ValueCallback<Boolean> valueCallback) {
        final WebViewFeatureInternal safe_BROWSING_ALLOWLIST_PREFERRED_TO_PREFERRED = WebViewFeatureInternal.SAFE_BROWSING_ALLOWLIST_PREFERRED_TO_PREFERRED;
        final WebViewFeatureInternal safe_BROWSING_ALLOWLIST_PREFERRED_TO_DEPRECATED = WebViewFeatureInternal.SAFE_BROWSING_ALLOWLIST_PREFERRED_TO_DEPRECATED;
        if (safe_BROWSING_ALLOWLIST_PREFERRED_TO_PREFERRED.isSupportedByWebView()) {
            getFactory().getStatics().setSafeBrowsingAllowlist((Set)c, (ValueCallback)valueCallback);
            return;
        }
        final ArrayList list = new ArrayList((Collection<? extends E>)c);
        if (safe_BROWSING_ALLOWLIST_PREFERRED_TO_DEPRECATED.isSupportedByFramework()) {
            \u3007O8o08O.\u3007080((List)list, (ValueCallback)valueCallback);
        }
        else {
            if (!safe_BROWSING_ALLOWLIST_PREFERRED_TO_DEPRECATED.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            getFactory().getStatics().setSafeBrowsingWhitelist((List)list, (ValueCallback)valueCallback);
        }
    }
    
    @Deprecated
    @SuppressLint({ "NewApi" })
    public static void setSafeBrowsingWhitelist(@NonNull final List<String> c, @Nullable final ValueCallback<Boolean> valueCallback) {
        setSafeBrowsingAllowlist(new HashSet<String>(c), valueCallback);
    }
    
    @SuppressLint({ "NewApi" })
    public static void setWebViewRenderProcessClient(@NonNull final WebView webView, @Nullable final WebViewRenderProcessClient webViewRenderProcessClient) {
        final WebViewFeatureInternal web_VIEW_RENDERER_CLIENT_BASIC_USAGE = WebViewFeatureInternal.WEB_VIEW_RENDERER_CLIENT_BASIC_USAGE;
        final boolean supportedByFramework = web_VIEW_RENDERER_CLIENT_BASIC_USAGE.isSupportedByFramework();
        android.webkit.WebViewRenderProcessClient webViewRenderProcessClient2 = null;
        if (supportedByFramework) {
            if (webViewRenderProcessClient != null) {
                webViewRenderProcessClient2 = new WebViewRenderProcessClientFrameworkAdapter(webViewRenderProcessClient);
            }
            \u30070\u3007O0088o.\u3007080(webView, webViewRenderProcessClient2);
        }
        else {
            if (!web_VIEW_RENDERER_CLIENT_BASIC_USAGE.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            getProvider(webView).setWebViewRenderProcessClient(null, webViewRenderProcessClient);
        }
    }
    
    @SuppressLint({ "LambdaLast", "NewApi" })
    public static void setWebViewRenderProcessClient(@NonNull final WebView webView, @NonNull final Executor executor, @NonNull final WebViewRenderProcessClient webViewRenderProcessClient) {
        final WebViewFeatureInternal web_VIEW_RENDERER_CLIENT_BASIC_USAGE = WebViewFeatureInternal.WEB_VIEW_RENDERER_CLIENT_BASIC_USAGE;
        if (web_VIEW_RENDERER_CLIENT_BASIC_USAGE.isSupportedByFramework()) {
            WebViewRenderProcessClientFrameworkAdapter webViewRenderProcessClientFrameworkAdapter;
            if (webViewRenderProcessClient != null) {
                webViewRenderProcessClientFrameworkAdapter = new WebViewRenderProcessClientFrameworkAdapter(webViewRenderProcessClient);
            }
            else {
                webViewRenderProcessClientFrameworkAdapter = null;
            }
            \u3007oo\u3007.\u3007080(webView, executor, (android.webkit.WebViewRenderProcessClient)webViewRenderProcessClientFrameworkAdapter);
        }
        else {
            if (!web_VIEW_RENDERER_CLIENT_BASIC_USAGE.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            getProvider(webView).setWebViewRenderProcessClient(executor, webViewRenderProcessClient);
        }
    }
    
    @SuppressLint({ "NewApi" })
    public static void startSafeBrowsing(@NonNull final Context context, @Nullable final ValueCallback<Boolean> valueCallback) {
        final WebViewFeatureInternal start_SAFE_BROWSING = WebViewFeatureInternal.START_SAFE_BROWSING;
        if (start_SAFE_BROWSING.isSupportedByFramework()) {
            \u3007O888o0o.\u3007080(context, (ValueCallback)valueCallback);
        }
        else {
            if (!start_SAFE_BROWSING.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            getFactory().getStatics().initSafeBrowsing(context, (ValueCallback)valueCallback);
        }
    }
    
    public interface VisualStateCallback
    {
        @UiThread
        void onComplete(final long p0);
    }
    
    public interface WebMessageListener
    {
        @UiThread
        void onPostMessage(@NonNull final WebView p0, @NonNull final WebMessageCompat p1, @NonNull final Uri p2, final boolean p3, @NonNull final JavaScriptReplyProxy p4);
    }
}

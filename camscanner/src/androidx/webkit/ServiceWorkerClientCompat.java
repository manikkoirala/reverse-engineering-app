// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import androidx.annotation.WorkerThread;
import androidx.annotation.Nullable;
import android.webkit.WebResourceResponse;
import androidx.annotation.NonNull;
import android.webkit.WebResourceRequest;

public abstract class ServiceWorkerClientCompat
{
    @Nullable
    @WorkerThread
    public abstract WebResourceResponse shouldInterceptRequest(@NonNull final WebResourceRequest p0);
}

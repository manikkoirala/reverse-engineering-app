// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import androidx.webkit.internal.ProxyControllerImpl;
import java.util.concurrent.Executor;
import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;

public abstract class ProxyController
{
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public ProxyController() {
    }
    
    @NonNull
    public static ProxyController getInstance() {
        if (WebViewFeature.isFeatureSupported("PROXY_OVERRIDE")) {
            return LAZY_HOLDER.INSTANCE;
        }
        throw new UnsupportedOperationException("Proxy override not supported");
    }
    
    public abstract void clearProxyOverride(@NonNull final Executor p0, @NonNull final Runnable p1);
    
    public abstract void setProxyOverride(@NonNull final ProxyConfig p0, @NonNull final Executor p1, @NonNull final Runnable p2);
    
    private static class LAZY_HOLDER
    {
        static final ProxyController INSTANCE;
        
        static {
            INSTANCE = new ProxyControllerImpl();
        }
    }
}

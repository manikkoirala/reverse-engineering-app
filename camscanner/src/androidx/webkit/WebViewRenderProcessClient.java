// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.webkit.WebView;

public abstract class WebViewRenderProcessClient
{
    public abstract void onRenderProcessResponsive(@NonNull final WebView p0, @Nullable final WebViewRenderProcess p1);
    
    public abstract void onRenderProcessUnresponsive(@NonNull final WebView p0, @Nullable final WebViewRenderProcess p1);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.Arrays;
import androidx.annotation.RestrictTo;
import java.util.Collection;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import java.util.List;

public class TracingConfig
{
    public static final int CATEGORIES_ALL = 1;
    public static final int CATEGORIES_ANDROID_WEBVIEW = 2;
    public static final int CATEGORIES_FRAME_VIEWER = 64;
    public static final int CATEGORIES_INPUT_LATENCY = 8;
    public static final int CATEGORIES_JAVASCRIPT_AND_RENDERING = 32;
    public static final int CATEGORIES_NONE = 0;
    public static final int CATEGORIES_RENDERING = 16;
    public static final int CATEGORIES_WEB_DEVELOPER = 4;
    public static final int RECORD_CONTINUOUSLY = 1;
    public static final int RECORD_UNTIL_FULL = 0;
    private final List<String> mCustomIncludedCategories;
    private int mPredefinedCategories;
    private int mTracingMode;
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public TracingConfig(final int mPredefinedCategories, @NonNull final List<String> list, final int mTracingMode) {
        final ArrayList mCustomIncludedCategories = new ArrayList();
        this.mCustomIncludedCategories = mCustomIncludedCategories;
        this.mPredefinedCategories = mPredefinedCategories;
        mCustomIncludedCategories.addAll(list);
        this.mTracingMode = mTracingMode;
    }
    
    @NonNull
    public List<String> getCustomIncludedCategories() {
        return this.mCustomIncludedCategories;
    }
    
    public int getPredefinedCategories() {
        return this.mPredefinedCategories;
    }
    
    public int getTracingMode() {
        return this.mTracingMode;
    }
    
    public static class Builder
    {
        private final List<String> mCustomIncludedCategories;
        private int mPredefinedCategories;
        private int mTracingMode;
        
        public Builder() {
            this.mPredefinedCategories = 0;
            this.mCustomIncludedCategories = new ArrayList<String>();
            this.mTracingMode = 1;
        }
        
        @NonNull
        public Builder addCategories(@NonNull final Collection<String> collection) {
            this.mCustomIncludedCategories.addAll(collection);
            return this;
        }
        
        @NonNull
        public Builder addCategories(@NonNull final int... array) {
            for (int length = array.length, i = 0; i < length; ++i) {
                this.mPredefinedCategories |= array[i];
            }
            return this;
        }
        
        @NonNull
        public Builder addCategories(@NonNull final String... a) {
            this.mCustomIncludedCategories.addAll(Arrays.asList(a));
            return this;
        }
        
        @NonNull
        public TracingConfig build() {
            return new TracingConfig(this.mPredefinedCategories, this.mCustomIncludedCategories, this.mTracingMode);
        }
        
        @NonNull
        public Builder setTracingMode(final int mTracingMode) {
            this.mTracingMode = mTracingMode;
            return this;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface PredefinedCategories {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface TracingMode {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import androidx.webkit.internal.TracingControllerImpl;
import java.util.concurrent.Executor;
import androidx.annotation.Nullable;
import java.io.OutputStream;
import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;

public abstract class TracingController
{
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public TracingController() {
    }
    
    @NonNull
    public static TracingController getInstance() {
        return LAZY_HOLDER.INSTANCE;
    }
    
    public abstract boolean isTracing();
    
    public abstract void start(@NonNull final TracingConfig p0);
    
    public abstract boolean stop(@Nullable final OutputStream p0, @NonNull final Executor p1);
    
    private static class LAZY_HOLDER
    {
        static final TracingController INSTANCE;
        
        static {
            INSTANCE = new TracingControllerImpl();
        }
    }
}

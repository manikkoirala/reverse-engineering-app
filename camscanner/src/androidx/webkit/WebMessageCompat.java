// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import androidx.annotation.Nullable;

public class WebMessageCompat
{
    private String mData;
    private WebMessagePortCompat[] mPorts;
    
    public WebMessageCompat(@Nullable final String mData) {
        this.mData = mData;
    }
    
    public WebMessageCompat(@Nullable final String mData, @Nullable final WebMessagePortCompat[] mPorts) {
        this.mData = mData;
        this.mPorts = mPorts;
    }
    
    @Nullable
    public String getData() {
        return this.mData;
    }
    
    @Nullable
    public WebMessagePortCompat[] getPorts() {
        return this.mPorts;
    }
}

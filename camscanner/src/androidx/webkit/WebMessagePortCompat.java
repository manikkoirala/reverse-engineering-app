// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import androidx.annotation.Nullable;
import android.os.Handler;
import java.lang.reflect.InvocationHandler;
import androidx.annotation.RequiresApi;
import androidx.annotation.NonNull;
import android.webkit.WebMessagePort;
import androidx.annotation.RestrictTo;

public abstract class WebMessagePortCompat
{
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public WebMessagePortCompat() {
    }
    
    public abstract void close();
    
    @NonNull
    @RequiresApi(23)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public abstract WebMessagePort getFrameworkPort();
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public abstract InvocationHandler getInvocationHandler();
    
    public abstract void postMessage(@NonNull final WebMessageCompat p0);
    
    public abstract void setWebMessageCallback(@Nullable final Handler p0, @NonNull final WebMessageCallbackCompat p1);
    
    public abstract void setWebMessageCallback(@NonNull final WebMessageCallbackCompat p0);
    
    public abstract static class WebMessageCallbackCompat
    {
        public void onMessage(@NonNull final WebMessagePortCompat webMessagePortCompat, @Nullable final WebMessageCompat webMessageCompat) {
        }
    }
}

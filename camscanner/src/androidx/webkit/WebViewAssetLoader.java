// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import android.content.res.Resources$NotFoundException;
import java.io.File;
import java.util.ArrayList;
import java.io.IOException;
import java.io.InputStream;
import androidx.annotation.VisibleForTesting;
import android.content.Context;
import androidx.webkit.internal.AssetHelper;
import androidx.annotation.WorkerThread;
import androidx.annotation.Nullable;
import java.util.Iterator;
import android.webkit.WebResourceResponse;
import android.net.Uri;
import androidx.annotation.NonNull;
import java.util.List;

public final class WebViewAssetLoader
{
    public static final String DEFAULT_DOMAIN = "appassets.androidplatform.net";
    private static final String TAG = "WebViewAssetLoader";
    private final List<PathMatcher> mMatchers;
    
    WebViewAssetLoader(@NonNull final List<PathMatcher> mMatchers) {
        this.mMatchers = mMatchers;
    }
    
    @Nullable
    @WorkerThread
    public WebResourceResponse shouldInterceptRequest(@NonNull final Uri uri) {
        for (final PathMatcher pathMatcher : this.mMatchers) {
            final PathHandler match = pathMatcher.match(uri);
            if (match == null) {
                continue;
            }
            final WebResourceResponse handle = match.handle(pathMatcher.getSuffixPath(uri.getPath()));
            if (handle == null) {
                continue;
            }
            return handle;
        }
        return null;
    }
    
    public static final class AssetsPathHandler implements PathHandler
    {
        private AssetHelper mAssetHelper;
        
        public AssetsPathHandler(@NonNull final Context context) {
            this.mAssetHelper = new AssetHelper(context);
        }
        
        @VisibleForTesting
        AssetsPathHandler(@NonNull final AssetHelper mAssetHelper) {
            this.mAssetHelper = mAssetHelper;
        }
        
        @Nullable
        @WorkerThread
        @Override
        public WebResourceResponse handle(@NonNull final String str) {
            try {
                return new WebResourceResponse(AssetHelper.guessMimeType(str), (String)null, this.mAssetHelper.openAsset(str));
            }
            catch (final IOException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Error opening asset path: ");
                sb.append(str);
                return new WebResourceResponse((String)null, (String)null, (InputStream)null);
            }
        }
    }
    
    public static final class Builder
    {
        @NonNull
        private List<PathMatcher> mBuilderMatcherList;
        private String mDomain;
        private boolean mHttpAllowed;
        
        public Builder() {
            this.mHttpAllowed = false;
            this.mDomain = "appassets.androidplatform.net";
            this.mBuilderMatcherList = new ArrayList<PathMatcher>();
        }
        
        @NonNull
        public Builder addPathHandler(@NonNull final String s, @NonNull final PathHandler pathHandler) {
            this.mBuilderMatcherList.add(new PathMatcher(this.mDomain, s, this.mHttpAllowed, pathHandler));
            return this;
        }
        
        @NonNull
        public WebViewAssetLoader build() {
            return new WebViewAssetLoader(this.mBuilderMatcherList);
        }
        
        @NonNull
        public Builder setDomain(@NonNull final String mDomain) {
            this.mDomain = mDomain;
            return this;
        }
        
        @NonNull
        public Builder setHttpAllowed(final boolean mHttpAllowed) {
            this.mHttpAllowed = mHttpAllowed;
            return this;
        }
    }
    
    public static final class InternalStoragePathHandler implements PathHandler
    {
        private static final String[] FORBIDDEN_DATA_DIRS;
        @NonNull
        private final File mDirectory;
        
        static {
            FORBIDDEN_DATA_DIRS = new String[] { "app_webview/", "databases/", "lib/", "shared_prefs/", "code_cache/" };
        }
        
        public InternalStoragePathHandler(@NonNull final Context context, @NonNull final File obj) {
            try {
                this.mDirectory = new File(AssetHelper.getCanonicalDirPath(obj));
                if (this.isAllowedInternalStorageDir(context)) {
                    return;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("The given directory \"");
                sb.append(obj);
                sb.append("\" doesn't exist under an allowed app internal storage directory");
                throw new IllegalArgumentException(sb.toString());
            }
            catch (final IOException cause) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Failed to resolve the canonical path for the given directory: ");
                sb2.append(obj.getPath());
                throw new IllegalArgumentException(sb2.toString(), cause);
            }
        }
        
        private boolean isAllowedInternalStorageDir(@NonNull final Context context) throws IOException {
            final String canonicalDirPath = AssetHelper.getCanonicalDirPath(this.mDirectory);
            final String canonicalDirPath2 = AssetHelper.getCanonicalDirPath(context.getCacheDir());
            final String canonicalDirPath3 = AssetHelper.getCanonicalDirPath(AssetHelper.getDataDir(context));
            if (!canonicalDirPath.startsWith(canonicalDirPath2) && !canonicalDirPath.startsWith(canonicalDirPath3)) {
                return false;
            }
            if (!canonicalDirPath.equals(canonicalDirPath2) && !canonicalDirPath.equals(canonicalDirPath3)) {
                for (final String str : InternalStoragePathHandler.FORBIDDEN_DATA_DIRS) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(canonicalDirPath3);
                    sb.append(str);
                    if (canonicalDirPath.startsWith(sb.toString())) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
        
        @NonNull
        @WorkerThread
        @Override
        public WebResourceResponse handle(@NonNull final String str) {
            try {
                final File canonicalFileIfChild = AssetHelper.getCanonicalFileIfChild(this.mDirectory, str);
                if (canonicalFileIfChild != null) {
                    return new WebResourceResponse(AssetHelper.guessMimeType(str), (String)null, AssetHelper.openFile(canonicalFileIfChild));
                }
                String.format("The requested file: %s is outside the mounted directory: %s", str, this.mDirectory);
            }
            catch (final IOException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Error opening the requested path: ");
                sb.append(str);
            }
            return new WebResourceResponse((String)null, (String)null, (InputStream)null);
        }
    }
    
    public interface PathHandler
    {
        @Nullable
        @WorkerThread
        WebResourceResponse handle(@NonNull final String p0);
    }
    
    @VisibleForTesting
    static class PathMatcher
    {
        static final String HTTPS_SCHEME = "https";
        static final String HTTP_SCHEME = "http";
        @NonNull
        final String mAuthority;
        @NonNull
        final PathHandler mHandler;
        final boolean mHttpEnabled;
        @NonNull
        final String mPath;
        
        PathMatcher(@NonNull final String mAuthority, @NonNull final String mPath, final boolean mHttpEnabled, @NonNull final PathHandler mHandler) {
            if (mPath.isEmpty() || mPath.charAt(0) != '/') {
                throw new IllegalArgumentException("Path should start with a slash '/'.");
            }
            if (mPath.endsWith("/")) {
                this.mAuthority = mAuthority;
                this.mPath = mPath;
                this.mHttpEnabled = mHttpEnabled;
                this.mHandler = mHandler;
                return;
            }
            throw new IllegalArgumentException("Path should end with a slash '/'");
        }
        
        @NonNull
        @WorkerThread
        public String getSuffixPath(@NonNull final String s) {
            return s.replaceFirst(this.mPath, "");
        }
        
        @Nullable
        @WorkerThread
        public PathHandler match(@NonNull final Uri uri) {
            if (uri.getScheme().equals("http") && !this.mHttpEnabled) {
                return null;
            }
            if (!uri.getScheme().equals("http") && !uri.getScheme().equals("https")) {
                return null;
            }
            if (!uri.getAuthority().equals(this.mAuthority)) {
                return null;
            }
            if (!uri.getPath().startsWith(this.mPath)) {
                return null;
            }
            return this.mHandler;
        }
    }
    
    public static final class ResourcesPathHandler implements PathHandler
    {
        private AssetHelper mAssetHelper;
        
        public ResourcesPathHandler(@NonNull final Context context) {
            this.mAssetHelper = new AssetHelper(context);
        }
        
        @VisibleForTesting
        ResourcesPathHandler(@NonNull final AssetHelper mAssetHelper) {
            this.mAssetHelper = mAssetHelper;
        }
        
        @Nullable
        @WorkerThread
        @Override
        public WebResourceResponse handle(@NonNull final String s) {
            try {
                return new WebResourceResponse(AssetHelper.guessMimeType(s), (String)null, this.mAssetHelper.openResource(s));
            }
            catch (final IOException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Error opening resource from the path: ");
                sb.append(s);
            }
            catch (final Resources$NotFoundException ex2) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Resource not found from the path: ");
                sb2.append(s);
            }
            return new WebResourceResponse((String)null, (String)null, (InputStream)null);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import androidx.annotation.RestrictTo;

public abstract class ServiceWorkerWebSettingsCompat
{
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public ServiceWorkerWebSettingsCompat() {
    }
    
    public abstract boolean getAllowContentAccess();
    
    public abstract boolean getAllowFileAccess();
    
    public abstract boolean getBlockNetworkLoads();
    
    public abstract int getCacheMode();
    
    public abstract void setAllowContentAccess(final boolean p0);
    
    public abstract void setAllowFileAccess(final boolean p0);
    
    public abstract void setBlockNetworkLoads(final boolean p0);
    
    public abstract void setCacheMode(final int p0);
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface CacheMode {
    }
}

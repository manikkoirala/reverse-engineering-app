// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.util.ArrayList;
import java.util.Collections;
import androidx.annotation.RestrictTo;
import androidx.annotation.NonNull;
import java.util.List;

public final class ProxyConfig
{
    private static final String BYPASS_RULE_REMOVE_IMPLICIT = "<-loopback>";
    private static final String BYPASS_RULE_SIMPLE_NAMES = "<local>";
    private static final String DIRECT = "direct://";
    public static final String MATCH_ALL_SCHEMES = "*";
    public static final String MATCH_HTTP = "http";
    public static final String MATCH_HTTPS = "https";
    private List<String> mBypassRules;
    private List<ProxyRule> mProxyRules;
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public ProxyConfig(@NonNull final List<ProxyRule> mProxyRules, @NonNull final List<String> mBypassRules) {
        this.mProxyRules = mProxyRules;
        this.mBypassRules = mBypassRules;
    }
    
    @NonNull
    public List<String> getBypassRules() {
        return Collections.unmodifiableList((List<? extends String>)this.mBypassRules);
    }
    
    @NonNull
    public List<ProxyRule> getProxyRules() {
        return Collections.unmodifiableList((List<? extends ProxyRule>)this.mProxyRules);
    }
    
    public static final class Builder
    {
        private List<String> mBypassRules;
        private List<ProxyRule> mProxyRules;
        
        public Builder() {
            this.mProxyRules = new ArrayList<ProxyRule>();
            this.mBypassRules = new ArrayList<String>();
        }
        
        public Builder(@NonNull final ProxyConfig proxyConfig) {
            this.mProxyRules = proxyConfig.getProxyRules();
            this.mBypassRules = proxyConfig.getBypassRules();
        }
        
        @NonNull
        private List<String> bypassRules() {
            return this.mBypassRules;
        }
        
        @NonNull
        private List<ProxyRule> proxyRules() {
            return this.mProxyRules;
        }
        
        @NonNull
        public Builder addBypassRule(@NonNull final String s) {
            this.mBypassRules.add(s);
            return this;
        }
        
        @NonNull
        public Builder addDirect() {
            return this.addDirect("*");
        }
        
        @NonNull
        public Builder addDirect(@NonNull final String s) {
            this.mProxyRules.add(new ProxyRule(s, "direct://"));
            return this;
        }
        
        @NonNull
        public Builder addProxyRule(@NonNull final String s) {
            this.mProxyRules.add(new ProxyRule(s));
            return this;
        }
        
        @NonNull
        public Builder addProxyRule(@NonNull final String s, @NonNull final String s2) {
            this.mProxyRules.add(new ProxyRule(s2, s));
            return this;
        }
        
        @NonNull
        public ProxyConfig build() {
            return new ProxyConfig(this.proxyRules(), this.bypassRules());
        }
        
        @NonNull
        public Builder bypassSimpleHostnames() {
            return this.addBypassRule("<local>");
        }
        
        @NonNull
        public Builder removeImplicitRules() {
            return this.addBypassRule("<-loopback>");
        }
    }
    
    public static final class ProxyRule
    {
        private String mSchemeFilter;
        private String mUrl;
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY })
        public ProxyRule(@NonNull final String s) {
            this("*", s);
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY })
        public ProxyRule(@NonNull final String mSchemeFilter, @NonNull final String mUrl) {
            this.mSchemeFilter = mSchemeFilter;
            this.mUrl = mUrl;
        }
        
        @NonNull
        public String getSchemeFilter() {
            return this.mSchemeFilter;
        }
        
        @NonNull
        public String getUrl() {
            return this.mUrl;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface ProxyScheme {
    }
}

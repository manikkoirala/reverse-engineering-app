// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import android.annotation.SuppressLint;
import androidx.webkit.internal.WebViewFeatureInternal;
import androidx.annotation.NonNull;
import androidx.webkit.internal.WebViewGlueCommunicator;
import androidx.webkit.internal.WebResourceRequestAdapter;
import android.webkit.WebResourceRequest;

public class WebResourceRequestCompat
{
    private WebResourceRequestCompat() {
    }
    
    private static WebResourceRequestAdapter getAdapter(final WebResourceRequest webResourceRequest) {
        return WebViewGlueCommunicator.getCompatConverter().convertWebResourceRequest(webResourceRequest);
    }
    
    @SuppressLint({ "NewApi" })
    public static boolean isRedirect(@NonNull final WebResourceRequest webResourceRequest) {
        final WebViewFeatureInternal web_RESOURCE_REQUEST_IS_REDIRECT = WebViewFeatureInternal.WEB_RESOURCE_REQUEST_IS_REDIRECT;
        if (web_RESOURCE_REQUEST_IS_REDIRECT.isSupportedByFramework()) {
            return \u3007080.\u3007080(webResourceRequest);
        }
        if (web_RESOURCE_REQUEST_IS_REDIRECT.isSupportedByWebView()) {
            return getAdapter(webResourceRequest).isRedirect();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
}

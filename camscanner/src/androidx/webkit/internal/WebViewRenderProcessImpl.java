// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import org.chromium.support_lib_boundary.IsomorphicObjectBoundaryInterface;
import android.annotation.SuppressLint;
import java.util.concurrent.Callable;
import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import java.lang.reflect.InvocationHandler;
import androidx.annotation.NonNull;
import java.lang.ref.WeakReference;
import org.chromium.support_lib_boundary.WebViewRendererBoundaryInterface;
import java.util.WeakHashMap;
import androidx.webkit.WebViewRenderProcess;

public class WebViewRenderProcessImpl extends WebViewRenderProcess
{
    private static WeakHashMap<android.webkit.WebViewRenderProcess, WebViewRenderProcessImpl> sFrameworkMap;
    private WebViewRendererBoundaryInterface mBoundaryInterface;
    private WeakReference<android.webkit.WebViewRenderProcess> mFrameworkObject;
    
    static {
        WebViewRenderProcessImpl.sFrameworkMap = new WeakHashMap<android.webkit.WebViewRenderProcess, WebViewRenderProcessImpl>();
    }
    
    public WebViewRenderProcessImpl(@NonNull final android.webkit.WebViewRenderProcess referent) {
        this.mFrameworkObject = new WeakReference<android.webkit.WebViewRenderProcess>(referent);
    }
    
    public WebViewRenderProcessImpl(@NonNull final WebViewRendererBoundaryInterface mBoundaryInterface) {
        this.mBoundaryInterface = mBoundaryInterface;
    }
    
    @NonNull
    public static WebViewRenderProcessImpl forFrameworkObject(@NonNull final android.webkit.WebViewRenderProcess webViewRenderProcess) {
        final WebViewRenderProcessImpl webViewRenderProcessImpl = WebViewRenderProcessImpl.sFrameworkMap.get(webViewRenderProcess);
        if (webViewRenderProcessImpl != null) {
            return webViewRenderProcessImpl;
        }
        final WebViewRenderProcessImpl value = new WebViewRenderProcessImpl(webViewRenderProcess);
        WebViewRenderProcessImpl.sFrameworkMap.put(webViewRenderProcess, value);
        return value;
    }
    
    @NonNull
    public static WebViewRenderProcessImpl forInvocationHandler(@NonNull final InvocationHandler invocationHandler) {
        final WebViewRendererBoundaryInterface webViewRendererBoundaryInterface = (WebViewRendererBoundaryInterface)BoundaryInterfaceReflectionUtil.\u3007080((Class)WebViewRendererBoundaryInterface.class, invocationHandler);
        return (WebViewRenderProcessImpl)((IsomorphicObjectBoundaryInterface)webViewRendererBoundaryInterface).getOrCreatePeer((Callable)new Callable<Object>(webViewRendererBoundaryInterface) {
            final WebViewRendererBoundaryInterface val$boundaryInterface;
            
            @Override
            public Object call() {
                return new WebViewRenderProcessImpl(this.val$boundaryInterface);
            }
        });
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public boolean terminate() {
        final WebViewFeatureInternal web_VIEW_RENDERER_TERMINATE = WebViewFeatureInternal.WEB_VIEW_RENDERER_TERMINATE;
        if (web_VIEW_RENDERER_TERMINATE.isSupportedByFramework()) {
            final android.webkit.WebViewRenderProcess webViewRenderProcess = this.mFrameworkObject.get();
            return webViewRenderProcess != null && oo\u3007.\u3007080(webViewRenderProcess);
        }
        if (web_VIEW_RENDERER_TERMINATE.isSupportedByWebView()) {
            return this.mBoundaryInterface.terminate();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import android.webkit.WebViewRenderProcess;
import android.webkit.WebView;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import android.webkit.WebViewRenderProcessClient;

@RequiresApi(29)
public class WebViewRenderProcessClientFrameworkAdapter extends WebViewRenderProcessClient
{
    private androidx.webkit.WebViewRenderProcessClient mWebViewRenderProcessClient;
    
    public WebViewRenderProcessClientFrameworkAdapter(@NonNull final androidx.webkit.WebViewRenderProcessClient mWebViewRenderProcessClient) {
        this.mWebViewRenderProcessClient = mWebViewRenderProcessClient;
    }
    
    @Nullable
    public androidx.webkit.WebViewRenderProcessClient getFrameworkRenderProcessClient() {
        return this.mWebViewRenderProcessClient;
    }
    
    public void onRenderProcessResponsive(@NonNull final WebView webView, @Nullable final WebViewRenderProcess webViewRenderProcess) {
        this.mWebViewRenderProcessClient.onRenderProcessResponsive(webView, WebViewRenderProcessImpl.forFrameworkObject(webViewRenderProcess));
    }
    
    public void onRenderProcessUnresponsive(@NonNull final WebView webView, @Nullable final WebViewRenderProcess webViewRenderProcess) {
        this.mWebViewRenderProcessClient.onRenderProcessUnresponsive(webView, WebViewRenderProcessImpl.forFrameworkObject(webViewRenderProcess));
    }
}

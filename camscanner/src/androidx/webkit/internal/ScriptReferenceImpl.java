// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import java.lang.reflect.InvocationHandler;
import androidx.annotation.NonNull;
import org.chromium.support_lib_boundary.ScriptReferenceBoundaryInterface;
import androidx.webkit.ScriptReferenceCompat;

public class ScriptReferenceImpl extends ScriptReferenceCompat
{
    private ScriptReferenceBoundaryInterface mBoundaryInterface;
    
    private ScriptReferenceImpl(@NonNull final ScriptReferenceBoundaryInterface mBoundaryInterface) {
        this.mBoundaryInterface = mBoundaryInterface;
    }
    
    @NonNull
    public static ScriptReferenceImpl toScriptReferenceCompat(@NonNull final InvocationHandler invocationHandler) {
        return new ScriptReferenceImpl((ScriptReferenceBoundaryInterface)BoundaryInterfaceReflectionUtil.\u3007080((Class)ScriptReferenceBoundaryInterface.class, invocationHandler));
    }
    
    @Override
    public void remove() {
        this.mBoundaryInterface.remove();
    }
}

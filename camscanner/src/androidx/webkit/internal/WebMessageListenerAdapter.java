// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import androidx.webkit.JavaScriptReplyProxy;
import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import org.chromium.support_lib_boundary.WebMessageBoundaryInterface;
import android.net.Uri;
import java.lang.reflect.InvocationHandler;
import android.webkit.WebView;
import androidx.annotation.NonNull;
import androidx.webkit.WebViewCompat;
import org.chromium.support_lib_boundary.WebMessageListenerBoundaryInterface;

public class WebMessageListenerAdapter implements WebMessageListenerBoundaryInterface
{
    private WebViewCompat.WebMessageListener mWebMessageListener;
    
    public WebMessageListenerAdapter(@NonNull final WebViewCompat.WebMessageListener mWebMessageListener) {
        this.mWebMessageListener = mWebMessageListener;
    }
    
    @NonNull
    public String[] getSupportedFeatures() {
        return new String[] { "WEB_MESSAGE_LISTENER" };
    }
    
    public void onPostMessage(@NonNull final WebView webView, @NonNull final InvocationHandler invocationHandler, @NonNull final Uri uri, final boolean b, @NonNull final InvocationHandler invocationHandler2) {
        this.mWebMessageListener.onPostMessage(webView, WebMessageAdapter.webMessageCompatFromBoundaryInterface((WebMessageBoundaryInterface)BoundaryInterfaceReflectionUtil.\u3007080((Class)WebMessageBoundaryInterface.class, invocationHandler)), uri, b, JavaScriptReplyProxyImpl.forInvocationHandler(invocationHandler2));
    }
}

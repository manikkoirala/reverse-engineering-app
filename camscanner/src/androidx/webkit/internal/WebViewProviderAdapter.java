// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import android.annotation.SuppressLint;
import java.util.concurrent.Executor;
import android.net.Uri;
import androidx.webkit.WebMessageCompat;
import androidx.webkit.WebViewRenderProcessClient;
import androidx.webkit.WebViewRenderProcess;
import android.webkit.WebViewClient;
import androidx.annotation.Nullable;
import android.webkit.WebChromeClient;
import java.lang.reflect.InvocationHandler;
import androidx.webkit.WebMessagePortCompat;
import androidx.annotation.RequiresApi;
import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import androidx.webkit.WebViewCompat;
import androidx.annotation.NonNull;
import org.chromium.support_lib_boundary.WebViewProviderBoundaryInterface;

public class WebViewProviderAdapter
{
    WebViewProviderBoundaryInterface mImpl;
    
    public WebViewProviderAdapter(@NonNull final WebViewProviderBoundaryInterface mImpl) {
        this.mImpl = mImpl;
    }
    
    @NonNull
    public ScriptReferenceImpl addDocumentStartJavaScript(@NonNull final String s, @NonNull final String[] array) {
        return ScriptReferenceImpl.toScriptReferenceCompat(this.mImpl.addDocumentStartJavaScript(s, array));
    }
    
    @RequiresApi(19)
    public void addWebMessageListener(@NonNull final String s, @NonNull final String[] array, @NonNull final WebViewCompat.WebMessageListener webMessageListener) {
        this.mImpl.addWebMessageListener(s, array, BoundaryInterfaceReflectionUtil.\u3007o\u3007((Object)new WebMessageListenerAdapter(webMessageListener)));
    }
    
    @NonNull
    public WebMessagePortCompat[] createWebMessageChannel() {
        final InvocationHandler[] webMessageChannel = this.mImpl.createWebMessageChannel();
        final WebMessagePortCompat[] array = new WebMessagePortCompat[webMessageChannel.length];
        for (int i = 0; i < webMessageChannel.length; ++i) {
            array[i] = new WebMessagePortImpl(webMessageChannel[i]);
        }
        return array;
    }
    
    @Nullable
    public WebChromeClient getWebChromeClient() {
        return this.mImpl.getWebChromeClient();
    }
    
    @NonNull
    public WebViewClient getWebViewClient() {
        return this.mImpl.getWebViewClient();
    }
    
    @Nullable
    public WebViewRenderProcess getWebViewRenderProcess() {
        return WebViewRenderProcessImpl.forInvocationHandler(this.mImpl.getWebViewRenderer());
    }
    
    @Nullable
    public WebViewRenderProcessClient getWebViewRenderProcessClient() {
        final InvocationHandler webViewRendererClient = this.mImpl.getWebViewRendererClient();
        if (webViewRendererClient == null) {
            return null;
        }
        return ((WebViewRenderProcessClientAdapter)BoundaryInterfaceReflectionUtil.Oo08(webViewRendererClient)).getWebViewRenderProcessClient();
    }
    
    @RequiresApi(19)
    public void insertVisualStateCallback(final long n, @NonNull final WebViewCompat.VisualStateCallback visualStateCallback) {
        this.mImpl.insertVisualStateCallback(n, BoundaryInterfaceReflectionUtil.\u3007o\u3007((Object)new VisualStateCallbackAdapter(visualStateCallback)));
    }
    
    @RequiresApi(19)
    public void postWebMessage(@NonNull final WebMessageCompat webMessageCompat, @NonNull final Uri uri) {
        this.mImpl.postMessageToMainFrame(BoundaryInterfaceReflectionUtil.\u3007o\u3007((Object)new WebMessageAdapter(webMessageCompat)), uri);
    }
    
    public void removeWebMessageListener(@NonNull final String s) {
        this.mImpl.removeWebMessageListener(s);
    }
    
    @SuppressLint({ "LambdaLast" })
    @RequiresApi(19)
    public void setWebViewRenderProcessClient(@Nullable final Executor executor, @Nullable final WebViewRenderProcessClient webViewRenderProcessClient) {
        InvocationHandler \u3007o\u3007;
        if (webViewRenderProcessClient != null) {
            \u3007o\u3007 = BoundaryInterfaceReflectionUtil.\u3007o\u3007((Object)new WebViewRenderProcessClientAdapter(executor, webViewRenderProcessClient));
        }
        else {
            \u3007o\u3007 = null;
        }
        this.mImpl.setWebViewRendererClient(\u3007o\u3007);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import android.annotation.SuppressLint;
import androidx.annotation.RequiresApi;
import java.lang.reflect.Proxy;
import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import java.lang.reflect.InvocationHandler;
import androidx.annotation.NonNull;
import android.webkit.SafeBrowsingResponse;
import org.chromium.support_lib_boundary.SafeBrowsingResponseBoundaryInterface;
import androidx.webkit.SafeBrowsingResponseCompat;

public class SafeBrowsingResponseImpl extends SafeBrowsingResponseCompat
{
    private SafeBrowsingResponseBoundaryInterface mBoundaryInterface;
    private SafeBrowsingResponse mFrameworksImpl;
    
    public SafeBrowsingResponseImpl(@NonNull final SafeBrowsingResponse mFrameworksImpl) {
        this.mFrameworksImpl = mFrameworksImpl;
    }
    
    public SafeBrowsingResponseImpl(@NonNull final InvocationHandler invocationHandler) {
        this.mBoundaryInterface = (SafeBrowsingResponseBoundaryInterface)BoundaryInterfaceReflectionUtil.\u3007080((Class)SafeBrowsingResponseBoundaryInterface.class, invocationHandler);
    }
    
    private SafeBrowsingResponseBoundaryInterface getBoundaryInterface() {
        if (this.mBoundaryInterface == null) {
            this.mBoundaryInterface = (SafeBrowsingResponseBoundaryInterface)BoundaryInterfaceReflectionUtil.\u3007080((Class)SafeBrowsingResponseBoundaryInterface.class, WebViewGlueCommunicator.getCompatConverter().convertSafeBrowsingResponse(this.mFrameworksImpl));
        }
        return this.mBoundaryInterface;
    }
    
    @RequiresApi(27)
    private SafeBrowsingResponse getFrameworksImpl() {
        if (this.mFrameworksImpl == null) {
            this.mFrameworksImpl = WebViewGlueCommunicator.getCompatConverter().convertSafeBrowsingResponse(Proxy.getInvocationHandler(this.mBoundaryInterface));
        }
        return this.mFrameworksImpl;
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public void backToSafety(final boolean b) {
        final WebViewFeatureInternal safe_BROWSING_RESPONSE_BACK_TO_SAFETY = WebViewFeatureInternal.SAFE_BROWSING_RESPONSE_BACK_TO_SAFETY;
        if (safe_BROWSING_RESPONSE_BACK_TO_SAFETY.isSupportedByFramework()) {
            \u3007080.\u3007080(this.getFrameworksImpl(), b);
        }
        else {
            if (!safe_BROWSING_RESPONSE_BACK_TO_SAFETY.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.getBoundaryInterface().backToSafety(b);
        }
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public void proceed(final boolean b) {
        final WebViewFeatureInternal safe_BROWSING_RESPONSE_PROCEED = WebViewFeatureInternal.SAFE_BROWSING_RESPONSE_PROCEED;
        if (safe_BROWSING_RESPONSE_PROCEED.isSupportedByFramework()) {
            \u3007o00\u3007\u3007Oo.\u3007080(this.getFrameworksImpl(), b);
        }
        else {
            if (!safe_BROWSING_RESPONSE_PROCEED.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.getBoundaryInterface().proceed(b);
        }
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public void showInterstitial(final boolean b) {
        final WebViewFeatureInternal safe_BROWSING_RESPONSE_SHOW_INTERSTITIAL = WebViewFeatureInternal.SAFE_BROWSING_RESPONSE_SHOW_INTERSTITIAL;
        if (safe_BROWSING_RESPONSE_SHOW_INTERSTITIAL.isSupportedByFramework()) {
            \u3007o\u3007.\u3007080(this.getFrameworksImpl(), b);
        }
        else {
            if (!safe_BROWSING_RESPONSE_SHOW_INTERSTITIAL.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.getBoundaryInterface().showInterstitial(b);
        }
    }
}

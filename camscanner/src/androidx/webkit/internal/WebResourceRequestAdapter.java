// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import org.chromium.support_lib_boundary.WebResourceRequestBoundaryInterface;

public class WebResourceRequestAdapter
{
    private final WebResourceRequestBoundaryInterface mBoundaryInterface;
    
    public WebResourceRequestAdapter(final WebResourceRequestBoundaryInterface mBoundaryInterface) {
        this.mBoundaryInterface = mBoundaryInterface;
    }
    
    public boolean isRedirect() {
        return this.mBoundaryInterface.isRedirect();
    }
}

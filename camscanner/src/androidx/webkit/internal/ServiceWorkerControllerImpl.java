// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import android.webkit.ServiceWorkerClient;
import androidx.annotation.Nullable;
import androidx.webkit.ServiceWorkerClientCompat;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import android.annotation.SuppressLint;
import androidx.webkit.ServiceWorkerWebSettingsCompat;
import android.webkit.ServiceWorkerController;
import org.chromium.support_lib_boundary.ServiceWorkerControllerBoundaryInterface;
import androidx.webkit.ServiceWorkerControllerCompat;

public class ServiceWorkerControllerImpl extends ServiceWorkerControllerCompat
{
    private ServiceWorkerControllerBoundaryInterface mBoundaryInterface;
    private ServiceWorkerController mFrameworksImpl;
    private final ServiceWorkerWebSettingsCompat mWebSettings;
    
    @SuppressLint({ "NewApi" })
    public ServiceWorkerControllerImpl() {
        final WebViewFeatureInternal service_WORKER_BASIC_USAGE = WebViewFeatureInternal.SERVICE_WORKER_BASIC_USAGE;
        if (service_WORKER_BASIC_USAGE.isSupportedByFramework()) {
            final ServiceWorkerController \u3007080 = Oo08.\u3007080();
            this.mFrameworksImpl = \u3007080;
            this.mBoundaryInterface = null;
            this.mWebSettings = new ServiceWorkerWebSettingsImpl(o\u30070.\u3007080(\u3007080));
        }
        else {
            if (!service_WORKER_BASIC_USAGE.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.mFrameworksImpl = null;
            final ServiceWorkerControllerBoundaryInterface serviceWorkerController = WebViewGlueCommunicator.getFactory().getServiceWorkerController();
            this.mBoundaryInterface = serviceWorkerController;
            this.mWebSettings = new ServiceWorkerWebSettingsImpl(serviceWorkerController.getServiceWorkerWebSettings());
        }
    }
    
    private ServiceWorkerControllerBoundaryInterface getBoundaryInterface() {
        if (this.mBoundaryInterface == null) {
            this.mBoundaryInterface = WebViewGlueCommunicator.getFactory().getServiceWorkerController();
        }
        return this.mBoundaryInterface;
    }
    
    @RequiresApi(24)
    private ServiceWorkerController getFrameworksImpl() {
        if (this.mFrameworksImpl == null) {
            this.mFrameworksImpl = Oo08.\u3007080();
        }
        return this.mFrameworksImpl;
    }
    
    @NonNull
    @Override
    public ServiceWorkerWebSettingsCompat getServiceWorkerWebSettings() {
        return this.mWebSettings;
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public void setServiceWorkerClient(@Nullable final ServiceWorkerClientCompat serviceWorkerClientCompat) {
        final WebViewFeatureInternal service_WORKER_BASIC_USAGE = WebViewFeatureInternal.SERVICE_WORKER_BASIC_USAGE;
        if (service_WORKER_BASIC_USAGE.isSupportedByFramework()) {
            O8.\u3007080(this.getFrameworksImpl(), (ServiceWorkerClient)new FrameworkServiceWorkerClient(serviceWorkerClientCompat));
        }
        else {
            if (!service_WORKER_BASIC_USAGE.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.getBoundaryInterface().setServiceWorkerClient(BoundaryInterfaceReflectionUtil.\u3007o\u3007((Object)new ServiceWorkerClientAdapter(serviceWorkerClientCompat)));
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import android.annotation.SuppressLint;
import androidx.annotation.RequiresApi;
import java.lang.reflect.Proxy;
import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import java.lang.reflect.InvocationHandler;
import androidx.annotation.NonNull;
import android.webkit.WebResourceError;
import org.chromium.support_lib_boundary.WebResourceErrorBoundaryInterface;
import androidx.webkit.WebResourceErrorCompat;

public class WebResourceErrorImpl extends WebResourceErrorCompat
{
    private WebResourceErrorBoundaryInterface mBoundaryInterface;
    private WebResourceError mFrameworksImpl;
    
    public WebResourceErrorImpl(@NonNull final WebResourceError mFrameworksImpl) {
        this.mFrameworksImpl = mFrameworksImpl;
    }
    
    public WebResourceErrorImpl(@NonNull final InvocationHandler invocationHandler) {
        this.mBoundaryInterface = (WebResourceErrorBoundaryInterface)BoundaryInterfaceReflectionUtil.\u3007080((Class)WebResourceErrorBoundaryInterface.class, invocationHandler);
    }
    
    private WebResourceErrorBoundaryInterface getBoundaryInterface() {
        if (this.mBoundaryInterface == null) {
            this.mBoundaryInterface = (WebResourceErrorBoundaryInterface)BoundaryInterfaceReflectionUtil.\u3007080((Class)WebResourceErrorBoundaryInterface.class, WebViewGlueCommunicator.getCompatConverter().convertWebResourceError(this.mFrameworksImpl));
        }
        return this.mBoundaryInterface;
    }
    
    @RequiresApi(23)
    private WebResourceError getFrameworksImpl() {
        if (this.mFrameworksImpl == null) {
            this.mFrameworksImpl = WebViewGlueCommunicator.getCompatConverter().convertWebResourceError(Proxy.getInvocationHandler(this.mBoundaryInterface));
        }
        return this.mFrameworksImpl;
    }
    
    @SuppressLint({ "NewApi" })
    @NonNull
    @Override
    public CharSequence getDescription() {
        final WebViewFeatureInternal web_RESOURCE_ERROR_GET_DESCRIPTION = WebViewFeatureInternal.WEB_RESOURCE_ERROR_GET_DESCRIPTION;
        if (web_RESOURCE_ERROR_GET_DESCRIPTION.isSupportedByFramework()) {
            return o\u3007\u30070\u3007.\u3007080(this.getFrameworksImpl());
        }
        if (web_RESOURCE_ERROR_GET_DESCRIPTION.isSupportedByWebView()) {
            return this.getBoundaryInterface().getDescription();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public int getErrorCode() {
        final WebViewFeatureInternal web_RESOURCE_ERROR_GET_CODE = WebViewFeatureInternal.WEB_RESOURCE_ERROR_GET_CODE;
        if (web_RESOURCE_ERROR_GET_CODE.isSupportedByFramework()) {
            return \u30070000OOO.\u3007080(this.getFrameworksImpl());
        }
        if (web_RESOURCE_ERROR_GET_CODE.isSupportedByWebView()) {
            return this.getBoundaryInterface().getErrorCode();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import org.chromium.support_lib_boundary.WebkitToCompatConverterBoundaryInterface;
import org.chromium.support_lib_boundary.TracingControllerBoundaryInterface;
import org.chromium.support_lib_boundary.StaticsBoundaryInterface;
import androidx.annotation.NonNull;
import org.chromium.support_lib_boundary.ServiceWorkerControllerBoundaryInterface;
import org.chromium.support_lib_boundary.ProxyControllerBoundaryInterface;
import org.chromium.support_lib_boundary.WebViewProviderBoundaryInterface;
import android.webkit.WebView;

public interface WebViewProviderFactory
{
    WebViewProviderBoundaryInterface createWebView(final WebView p0);
    
    ProxyControllerBoundaryInterface getProxyController();
    
    @NonNull
    ServiceWorkerControllerBoundaryInterface getServiceWorkerController();
    
    StaticsBoundaryInterface getStatics();
    
    TracingControllerBoundaryInterface getTracingController();
    
    String[] getWebViewFeatures();
    
    WebkitToCompatConverterBoundaryInterface getWebkitToCompatConverter();
}

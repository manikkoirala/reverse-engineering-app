// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import java.lang.reflect.Method;
import android.webkit.WebView;
import android.os.Build$VERSION;
import java.lang.reflect.InvocationHandler;
import androidx.annotation.NonNull;
import java.lang.reflect.InvocationTargetException;
import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import org.chromium.support_lib_boundary.WebViewProviderFactoryBoundaryInterface;

public class WebViewGlueCommunicator
{
    private static final String GLUE_FACTORY_PROVIDER_FETCHER_CLASS = "org.chromium.support_lib_glue.SupportLibReflectionUtil";
    private static final String GLUE_FACTORY_PROVIDER_FETCHER_METHOD = "createWebViewProviderFactory";
    
    private WebViewGlueCommunicator() {
    }
    
    @NonNull
    static WebViewProviderFactory createGlueProviderFactory() {
        try {
            return new WebViewProviderFactoryAdapter((WebViewProviderFactoryBoundaryInterface)BoundaryInterfaceReflectionUtil.\u3007080((Class)WebViewProviderFactoryBoundaryInterface.class, fetchGlueProviderFactoryImpl()));
        }
        catch (final NoSuchMethodException cause) {
            throw new RuntimeException(cause);
        }
        catch (final ClassNotFoundException ex) {
            return new IncompatibleApkWebViewProviderFactory();
        }
        catch (final InvocationTargetException cause2) {
            throw new RuntimeException(cause2);
        }
        catch (final IllegalAccessException cause3) {
            throw new RuntimeException(cause3);
        }
    }
    
    private static InvocationHandler fetchGlueProviderFactoryImpl() throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException {
        return (InvocationHandler)Class.forName("org.chromium.support_lib_glue.SupportLibReflectionUtil", false, getWebViewClassLoader()).getDeclaredMethod("createWebViewProviderFactory", (Class<?>[])new Class[0]).invoke(null, new Object[0]);
    }
    
    @NonNull
    public static WebkitToCompatConverter getCompatConverter() {
        return LAZY_COMPAT_CONVERTER_HOLDER.INSTANCE;
    }
    
    @NonNull
    public static WebViewProviderFactory getFactory() {
        return LAZY_FACTORY_HOLDER.INSTANCE;
    }
    
    public static ClassLoader getWebViewClassLoader() {
        if (Build$VERSION.SDK_INT >= 28) {
            return OOO\u3007O0.\u3007080();
        }
        return getWebViewProviderFactory().getClass().getClassLoader();
    }
    
    private static Object getWebViewProviderFactory() {
        try {
            final Method declaredMethod = WebView.class.getDeclaredMethod("getFactory", (Class<?>[])new Class[0]);
            declaredMethod.setAccessible(true);
            return declaredMethod.invoke(null, new Object[0]);
        }
        catch (final IllegalAccessException cause) {
            throw new RuntimeException(cause);
        }
        catch (final InvocationTargetException cause2) {
            throw new RuntimeException(cause2);
        }
        catch (final NoSuchMethodException cause3) {
            throw new RuntimeException(cause3);
        }
    }
    
    private static class LAZY_COMPAT_CONVERTER_HOLDER
    {
        static final WebkitToCompatConverter INSTANCE;
        
        static {
            INSTANCE = new WebkitToCompatConverter(WebViewGlueCommunicator.getFactory().getWebkitToCompatConverter());
        }
    }
    
    private static class LAZY_FACTORY_HOLDER
    {
        static final WebViewProviderFactory INSTANCE;
        
        static {
            INSTANCE = WebViewGlueCommunicator.createGlueProviderFactory();
        }
    }
}

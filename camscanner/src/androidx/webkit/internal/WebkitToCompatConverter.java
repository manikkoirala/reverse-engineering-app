// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import org.chromium.support_lib_boundary.WebResourceRequestBoundaryInterface;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceError;
import android.webkit.WebMessagePort;
import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import org.chromium.support_lib_boundary.WebSettingsBoundaryInterface;
import android.webkit.WebSettings;
import android.webkit.ServiceWorkerWebSettings;
import androidx.annotation.RequiresApi;
import android.webkit.SafeBrowsingResponse;
import androidx.annotation.NonNull;
import java.lang.reflect.InvocationHandler;
import org.chromium.support_lib_boundary.WebkitToCompatConverterBoundaryInterface;

public class WebkitToCompatConverter
{
    private final WebkitToCompatConverterBoundaryInterface mImpl;
    
    public WebkitToCompatConverter(final WebkitToCompatConverterBoundaryInterface mImpl) {
        this.mImpl = mImpl;
    }
    
    @NonNull
    @RequiresApi(27)
    public SafeBrowsingResponse convertSafeBrowsingResponse(@NonNull final InvocationHandler invocationHandler) {
        return (SafeBrowsingResponse)this.mImpl.convertSafeBrowsingResponse(invocationHandler);
    }
    
    @NonNull
    public InvocationHandler convertSafeBrowsingResponse(@NonNull final SafeBrowsingResponse safeBrowsingResponse) {
        return this.mImpl.convertSafeBrowsingResponse((Object)safeBrowsingResponse);
    }
    
    @NonNull
    @RequiresApi(24)
    public ServiceWorkerWebSettings convertServiceWorkerSettings(@NonNull final InvocationHandler invocationHandler) {
        return (ServiceWorkerWebSettings)this.mImpl.convertServiceWorkerSettings(invocationHandler);
    }
    
    @NonNull
    public InvocationHandler convertServiceWorkerSettings(@NonNull final ServiceWorkerWebSettings serviceWorkerWebSettings) {
        return this.mImpl.convertServiceWorkerSettings((Object)serviceWorkerWebSettings);
    }
    
    @NonNull
    public WebSettingsAdapter convertSettings(final WebSettings webSettings) {
        return new WebSettingsAdapter((WebSettingsBoundaryInterface)BoundaryInterfaceReflectionUtil.\u3007080((Class)WebSettingsBoundaryInterface.class, this.mImpl.convertSettings(webSettings)));
    }
    
    @NonNull
    @RequiresApi(23)
    public WebMessagePort convertWebMessagePort(@NonNull final InvocationHandler invocationHandler) {
        return (WebMessagePort)this.mImpl.convertWebMessagePort(invocationHandler);
    }
    
    @NonNull
    public InvocationHandler convertWebMessagePort(@NonNull final WebMessagePort webMessagePort) {
        return this.mImpl.convertWebMessagePort((Object)webMessagePort);
    }
    
    @NonNull
    @RequiresApi(23)
    public WebResourceError convertWebResourceError(@NonNull final InvocationHandler invocationHandler) {
        return (WebResourceError)this.mImpl.convertWebResourceError(invocationHandler);
    }
    
    @NonNull
    public InvocationHandler convertWebResourceError(@NonNull final WebResourceError webResourceError) {
        return this.mImpl.convertWebResourceError((Object)webResourceError);
    }
    
    @NonNull
    public WebResourceRequestAdapter convertWebResourceRequest(final WebResourceRequest webResourceRequest) {
        return new WebResourceRequestAdapter((WebResourceRequestBoundaryInterface)BoundaryInterfaceReflectionUtil.\u3007080((Class)WebResourceRequestBoundaryInterface.class, this.mImpl.convertWebResourceRequest(webResourceRequest)));
    }
}

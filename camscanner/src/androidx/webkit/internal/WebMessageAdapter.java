// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import androidx.annotation.Nullable;
import androidx.webkit.WebMessagePortCompat;
import java.lang.reflect.InvocationHandler;
import androidx.annotation.NonNull;
import androidx.webkit.WebMessageCompat;
import org.chromium.support_lib_boundary.WebMessageBoundaryInterface;

public class WebMessageAdapter implements WebMessageBoundaryInterface
{
    private WebMessageCompat mWebMessageCompat;
    
    public WebMessageAdapter(@NonNull final WebMessageCompat mWebMessageCompat) {
        this.mWebMessageCompat = mWebMessageCompat;
    }
    
    @NonNull
    private static WebMessagePortCompat[] toWebMessagePortCompats(final InvocationHandler[] array) {
        final WebMessagePortCompat[] array2 = new WebMessagePortCompat[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = new WebMessagePortImpl(array[i]);
        }
        return array2;
    }
    
    @NonNull
    public static WebMessageCompat webMessageCompatFromBoundaryInterface(@NonNull final WebMessageBoundaryInterface webMessageBoundaryInterface) {
        return new WebMessageCompat(webMessageBoundaryInterface.getData(), toWebMessagePortCompats(webMessageBoundaryInterface.getPorts()));
    }
    
    @Nullable
    public String getData() {
        return this.mWebMessageCompat.getData();
    }
    
    @Nullable
    public InvocationHandler[] getPorts() {
        final WebMessagePortCompat[] ports = this.mWebMessageCompat.getPorts();
        if (ports == null) {
            return null;
        }
        final InvocationHandler[] array = new InvocationHandler[ports.length];
        for (int i = 0; i < ports.length; ++i) {
            array[i] = ports[i].getInvocationHandler();
        }
        return array;
    }
    
    @NonNull
    public String[] getSupportedFeatures() {
        return new String[0];
    }
}

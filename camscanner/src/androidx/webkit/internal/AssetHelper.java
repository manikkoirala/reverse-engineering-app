// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import android.content.res.Resources$NotFoundException;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.util.zip.GZIPInputStream;
import java.io.InputStream;
import java.net.URLConnection;
import android.util.TypedValue;
import androidx.core.content.Oo08;
import android.os.Build$VERSION;
import androidx.annotation.Nullable;
import java.io.IOException;
import java.io.File;
import androidx.annotation.NonNull;
import android.content.Context;

public class AssetHelper
{
    public static final String DEFAULT_MIME_TYPE = "text/plain";
    private static final String TAG = "AssetHelper";
    @NonNull
    private Context mContext;
    
    public AssetHelper(@NonNull final Context mContext) {
        this.mContext = mContext;
    }
    
    @NonNull
    public static String getCanonicalDirPath(@NonNull final File file) throws IOException {
        String s;
        final String str = s = file.getCanonicalPath();
        if (!str.endsWith("/")) {
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("/");
            s = sb.toString();
        }
        return s;
    }
    
    @Nullable
    public static File getCanonicalFileIfChild(@NonNull final File parent, @NonNull final String child) throws IOException {
        final String canonicalDirPath = getCanonicalDirPath(parent);
        final String canonicalPath = new File(parent, child).getCanonicalPath();
        if (canonicalPath.startsWith(canonicalDirPath)) {
            return new File(canonicalPath);
        }
        return null;
    }
    
    @NonNull
    public static File getDataDir(@NonNull final Context context) {
        if (Build$VERSION.SDK_INT >= 24) {
            return Oo08.\u3007080(context);
        }
        return context.getCacheDir().getParentFile();
    }
    
    private int getFieldId(@NonNull final String s, @NonNull final String s2) {
        return this.mContext.getResources().getIdentifier(s2, s, this.mContext.getPackageName());
    }
    
    private int getValueType(final int n) {
        final TypedValue typedValue = new TypedValue();
        this.mContext.getResources().getValue(n, typedValue, true);
        return typedValue.type;
    }
    
    @NonNull
    public static String guessMimeType(@NonNull String guessContentTypeFromName) {
        if ((guessContentTypeFromName = URLConnection.guessContentTypeFromName(guessContentTypeFromName)) == null) {
            guessContentTypeFromName = "text/plain";
        }
        return guessContentTypeFromName;
    }
    
    @NonNull
    private static InputStream handleSvgzStream(@NonNull final String s, @NonNull final InputStream in) throws IOException {
        InputStream inputStream = in;
        if (s.endsWith(".svgz")) {
            inputStream = new GZIPInputStream(in);
        }
        return inputStream;
    }
    
    @NonNull
    public static InputStream openFile(@NonNull final File file) throws FileNotFoundException, IOException {
        return handleSvgzStream(file.getPath(), new FileInputStream(file));
    }
    
    @NonNull
    private static String removeLeadingSlash(@NonNull final String s) {
        String substring = s;
        if (s.length() > 1) {
            substring = s;
            if (s.charAt(0) == '/') {
                substring = s.substring(1);
            }
        }
        return substring;
    }
    
    @NonNull
    public InputStream openAsset(@NonNull String removeLeadingSlash) throws IOException {
        removeLeadingSlash = removeLeadingSlash(removeLeadingSlash);
        return handleSvgzStream(removeLeadingSlash, this.mContext.getAssets().open(removeLeadingSlash, 2));
    }
    
    @NonNull
    public InputStream openResource(@NonNull String substring) throws Resources$NotFoundException, IOException {
        final String removeLeadingSlash = removeLeadingSlash(substring);
        final String[] split = removeLeadingSlash.split("/", -1);
        if (split.length != 2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Incorrect resource path: ");
            sb.append(removeLeadingSlash);
            throw new IllegalArgumentException(sb.toString());
        }
        final String s = split[0];
        final String s2 = split[1];
        final int lastIndex = s2.lastIndexOf(46);
        substring = s2;
        if (lastIndex != -1) {
            substring = s2.substring(0, lastIndex);
        }
        final int fieldId = this.getFieldId(s, substring);
        final int valueType = this.getValueType(fieldId);
        if (valueType == 3) {
            return handleSvgzStream(removeLeadingSlash, this.mContext.getResources().openRawResource(fieldId));
        }
        throw new IOException(String.format("Expected %s resource to be of TYPE_STRING but was %d", removeLeadingSlash, valueType));
    }
}

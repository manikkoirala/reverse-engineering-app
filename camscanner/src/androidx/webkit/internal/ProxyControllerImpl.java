// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import java.util.concurrent.Executor;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.NonNull;
import androidx.webkit.ProxyConfig;
import java.util.List;
import org.chromium.support_lib_boundary.ProxyControllerBoundaryInterface;
import androidx.webkit.ProxyController;

public class ProxyControllerImpl extends ProxyController
{
    private ProxyControllerBoundaryInterface mBoundaryInterface;
    
    private ProxyControllerBoundaryInterface getBoundaryInterface() {
        if (this.mBoundaryInterface == null) {
            this.mBoundaryInterface = WebViewGlueCommunicator.getFactory().getProxyController();
        }
        return this.mBoundaryInterface;
    }
    
    @NonNull
    @VisibleForTesting
    public static String[][] proxyRulesToStringArray(@NonNull final List<ProxyConfig.ProxyRule> list) {
        final String[][] array = new String[list.size()][2];
        for (int i = 0; i < list.size(); ++i) {
            array[i][0] = ((ProxyConfig.ProxyRule)list.get(i)).getSchemeFilter();
            array[i][1] = ((ProxyConfig.ProxyRule)list.get(i)).getUrl();
        }
        return array;
    }
    
    @Override
    public void clearProxyOverride(@NonNull final Executor executor, @NonNull final Runnable runnable) {
        if (WebViewFeatureInternal.PROXY_OVERRIDE.isSupportedByWebView()) {
            this.getBoundaryInterface().clearProxyOverride(runnable, executor);
            return;
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @Override
    public void setProxyOverride(@NonNull final ProxyConfig proxyConfig, @NonNull final Executor executor, @NonNull final Runnable runnable) {
        if (WebViewFeatureInternal.PROXY_OVERRIDE.isSupportedByWebView()) {
            this.getBoundaryInterface().setProxyOverride(proxyRulesToStringArray(proxyConfig.getProxyRules()), (String[])proxyConfig.getBypassRules().toArray(new String[0]), runnable, executor);
            return;
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
}

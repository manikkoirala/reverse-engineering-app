// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import org.chromium.support_lib_boundary.WebkitToCompatConverterBoundaryInterface;
import org.chromium.support_lib_boundary.TracingControllerBoundaryInterface;
import org.chromium.support_lib_boundary.StaticsBoundaryInterface;
import androidx.annotation.NonNull;
import org.chromium.support_lib_boundary.ServiceWorkerControllerBoundaryInterface;
import org.chromium.support_lib_boundary.ProxyControllerBoundaryInterface;
import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import org.chromium.support_lib_boundary.WebViewProviderBoundaryInterface;
import android.webkit.WebView;
import org.chromium.support_lib_boundary.WebViewProviderFactoryBoundaryInterface;

public class WebViewProviderFactoryAdapter implements WebViewProviderFactory
{
    WebViewProviderFactoryBoundaryInterface mImpl;
    
    public WebViewProviderFactoryAdapter(final WebViewProviderFactoryBoundaryInterface mImpl) {
        this.mImpl = mImpl;
    }
    
    @Override
    public WebViewProviderBoundaryInterface createWebView(final WebView webView) {
        return (WebViewProviderBoundaryInterface)BoundaryInterfaceReflectionUtil.\u3007080((Class)WebViewProviderBoundaryInterface.class, this.mImpl.createWebView(webView));
    }
    
    @Override
    public ProxyControllerBoundaryInterface getProxyController() {
        return (ProxyControllerBoundaryInterface)BoundaryInterfaceReflectionUtil.\u3007080((Class)ProxyControllerBoundaryInterface.class, this.mImpl.getProxyController());
    }
    
    @NonNull
    @Override
    public ServiceWorkerControllerBoundaryInterface getServiceWorkerController() {
        return (ServiceWorkerControllerBoundaryInterface)BoundaryInterfaceReflectionUtil.\u3007080((Class)ServiceWorkerControllerBoundaryInterface.class, this.mImpl.getServiceWorkerController());
    }
    
    @Override
    public StaticsBoundaryInterface getStatics() {
        return (StaticsBoundaryInterface)BoundaryInterfaceReflectionUtil.\u3007080((Class)StaticsBoundaryInterface.class, this.mImpl.getStatics());
    }
    
    @Override
    public TracingControllerBoundaryInterface getTracingController() {
        return (TracingControllerBoundaryInterface)BoundaryInterfaceReflectionUtil.\u3007080((Class)TracingControllerBoundaryInterface.class, this.mImpl.getTracingController());
    }
    
    @Override
    public String[] getWebViewFeatures() {
        return this.mImpl.getSupportedFeatures();
    }
    
    @Override
    public WebkitToCompatConverterBoundaryInterface getWebkitToCompatConverter() {
        return (WebkitToCompatConverterBoundaryInterface)BoundaryInterfaceReflectionUtil.\u3007080((Class)WebkitToCompatConverterBoundaryInterface.class, this.mImpl.getWebkitToCompatConverter());
    }
}

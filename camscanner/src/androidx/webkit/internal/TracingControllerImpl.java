// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import java.util.concurrent.Executor;
import java.io.OutputStream;
import java.util.Collection;
import android.webkit.TracingConfig$Builder;
import androidx.annotation.NonNull;
import androidx.webkit.TracingConfig;
import androidx.annotation.RequiresApi;
import android.annotation.SuppressLint;
import org.chromium.support_lib_boundary.TracingControllerBoundaryInterface;
import androidx.webkit.TracingController;

public class TracingControllerImpl extends TracingController
{
    private TracingControllerBoundaryInterface mBoundaryInterface;
    private android.webkit.TracingController mFrameworksImpl;
    
    @SuppressLint({ "NewApi" })
    public TracingControllerImpl() {
        final WebViewFeatureInternal tracing_CONTROLLER_BASIC_USAGE = WebViewFeatureInternal.TRACING_CONTROLLER_BASIC_USAGE;
        if (tracing_CONTROLLER_BASIC_USAGE.isSupportedByFramework()) {
            this.mFrameworksImpl = oo88o8O.\u3007080();
            this.mBoundaryInterface = null;
        }
        else {
            if (!tracing_CONTROLLER_BASIC_USAGE.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.mFrameworksImpl = null;
            this.mBoundaryInterface = WebViewGlueCommunicator.getFactory().getTracingController();
        }
    }
    
    private TracingControllerBoundaryInterface getBoundaryInterface() {
        if (this.mBoundaryInterface == null) {
            this.mBoundaryInterface = WebViewGlueCommunicator.getFactory().getTracingController();
        }
        return this.mBoundaryInterface;
    }
    
    @RequiresApi(28)
    private android.webkit.TracingController getFrameworksImpl() {
        if (this.mFrameworksImpl == null) {
            this.mFrameworksImpl = oo88o8O.\u3007080();
        }
        return this.mFrameworksImpl;
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public boolean isTracing() {
        final WebViewFeatureInternal tracing_CONTROLLER_BASIC_USAGE = WebViewFeatureInternal.TRACING_CONTROLLER_BASIC_USAGE;
        if (tracing_CONTROLLER_BASIC_USAGE.isSupportedByFramework()) {
            return o800o8O.\u3007080(this.getFrameworksImpl());
        }
        if (tracing_CONTROLLER_BASIC_USAGE.isSupportedByWebView()) {
            return this.getBoundaryInterface().isTracing();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public void start(@NonNull final TracingConfig tracingConfig) {
        if (tracingConfig != null) {
            final WebViewFeatureInternal tracing_CONTROLLER_BASIC_USAGE = WebViewFeatureInternal.TRACING_CONTROLLER_BASIC_USAGE;
            if (tracing_CONTROLLER_BASIC_USAGE.isSupportedByFramework()) {
                OoO8.\u3007080(this.getFrameworksImpl(), \u30070\u3007O0088o.\u3007080(\u3007\u30078O0\u30078.\u3007080(\u3007O00.\u3007080(\u3007\u3007808\u3007.\u3007080(new TracingConfig$Builder(), new int[] { tracingConfig.getPredefinedCategories() }), (Collection)tracingConfig.getCustomIncludedCategories()), tracingConfig.getTracingMode())));
            }
            else {
                if (!tracing_CONTROLLER_BASIC_USAGE.isSupportedByWebView()) {
                    throw WebViewFeatureInternal.getUnsupportedOperationException();
                }
                this.getBoundaryInterface().start(tracingConfig.getPredefinedCategories(), (Collection)tracingConfig.getCustomIncludedCategories(), tracingConfig.getTracingMode());
            }
            return;
        }
        throw new IllegalArgumentException("Tracing config must be non null");
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public boolean stop(final OutputStream outputStream, final Executor executor) {
        final WebViewFeatureInternal tracing_CONTROLLER_BASIC_USAGE = WebViewFeatureInternal.TRACING_CONTROLLER_BASIC_USAGE;
        if (tracing_CONTROLLER_BASIC_USAGE.isSupportedByFramework()) {
            return \u3007O888o0o.\u3007080(this.getFrameworksImpl(), outputStream, executor);
        }
        if (tracing_CONTROLLER_BASIC_USAGE.isSupportedByWebView()) {
            return this.getBoundaryInterface().stop(outputStream, executor);
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
}

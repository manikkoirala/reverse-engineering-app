// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import android.webkit.WebMessagePort$WebMessageCallback;
import android.os.Handler;
import android.annotation.SuppressLint;
import java.lang.reflect.Proxy;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.annotation.NonNull;
import android.webkit.WebMessage;
import androidx.webkit.WebMessageCompat;
import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import java.lang.reflect.InvocationHandler;
import android.webkit.WebMessagePort;
import org.chromium.support_lib_boundary.WebMessagePortBoundaryInterface;
import androidx.webkit.WebMessagePortCompat;

public class WebMessagePortImpl extends WebMessagePortCompat
{
    private WebMessagePortBoundaryInterface mBoundaryInterface;
    private WebMessagePort mFrameworksImpl;
    
    public WebMessagePortImpl(final WebMessagePort mFrameworksImpl) {
        this.mFrameworksImpl = mFrameworksImpl;
    }
    
    public WebMessagePortImpl(final InvocationHandler invocationHandler) {
        this.mBoundaryInterface = (WebMessagePortBoundaryInterface)BoundaryInterfaceReflectionUtil.\u3007080((Class)WebMessagePortBoundaryInterface.class, invocationHandler);
    }
    
    @NonNull
    @RequiresApi(23)
    public static WebMessage compatToFrameworkMessage(final WebMessageCompat webMessageCompat) {
        return new WebMessage(webMessageCompat.getData(), compatToPorts(webMessageCompat.getPorts()));
    }
    
    @Nullable
    @RequiresApi(23)
    public static WebMessagePort[] compatToPorts(final WebMessagePortCompat[] array) {
        if (array == null) {
            return null;
        }
        final int length = array.length;
        final WebMessagePort[] array2 = new WebMessagePort[length];
        for (int i = 0; i < length; ++i) {
            array2[i] = array[i].getFrameworkPort();
        }
        return array2;
    }
    
    @NonNull
    @RequiresApi(23)
    public static WebMessageCompat frameworkMessageToCompat(final WebMessage webMessage) {
        return new WebMessageCompat(O\u30078O8\u3007008.\u3007080(webMessage), portsToCompat(O8ooOoo\u3007.\u3007080(webMessage)));
    }
    
    private WebMessagePortBoundaryInterface getBoundaryInterface() {
        if (this.mBoundaryInterface == null) {
            this.mBoundaryInterface = (WebMessagePortBoundaryInterface)BoundaryInterfaceReflectionUtil.\u3007080((Class)WebMessagePortBoundaryInterface.class, WebViewGlueCommunicator.getCompatConverter().convertWebMessagePort(this.mFrameworksImpl));
        }
        return this.mBoundaryInterface;
    }
    
    @RequiresApi(23)
    private WebMessagePort getFrameworksImpl() {
        if (this.mFrameworksImpl == null) {
            this.mFrameworksImpl = WebViewGlueCommunicator.getCompatConverter().convertWebMessagePort(Proxy.getInvocationHandler(this.mBoundaryInterface));
        }
        return this.mFrameworksImpl;
    }
    
    @Nullable
    public static WebMessagePortCompat[] portsToCompat(final WebMessagePort[] array) {
        if (array == null) {
            return null;
        }
        final WebMessagePortCompat[] array2 = new WebMessagePortCompat[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = new WebMessagePortImpl(array[i]);
        }
        return array2;
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public void close() {
        final WebViewFeatureInternal web_MESSAGE_PORT_CLOSE = WebViewFeatureInternal.WEB_MESSAGE_PORT_CLOSE;
        if (web_MESSAGE_PORT_CLOSE.isSupportedByFramework()) {
            \u3007oOO8O8.\u3007080(this.getFrameworksImpl());
        }
        else {
            if (!web_MESSAGE_PORT_CLOSE.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.getBoundaryInterface().close();
        }
    }
    
    @RequiresApi(23)
    @Override
    public WebMessagePort getFrameworkPort() {
        return this.getFrameworksImpl();
    }
    
    @Override
    public InvocationHandler getInvocationHandler() {
        return Proxy.getInvocationHandler(this.getBoundaryInterface());
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public void postMessage(@NonNull final WebMessageCompat webMessageCompat) {
        final WebViewFeatureInternal web_MESSAGE_PORT_POST_MESSAGE = WebViewFeatureInternal.WEB_MESSAGE_PORT_POST_MESSAGE;
        if (web_MESSAGE_PORT_POST_MESSAGE.isSupportedByFramework()) {
            \u300700.\u3007080(this.getFrameworksImpl(), compatToFrameworkMessage(webMessageCompat));
        }
        else {
            if (!web_MESSAGE_PORT_POST_MESSAGE.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.getBoundaryInterface().postMessage(BoundaryInterfaceReflectionUtil.\u3007o\u3007((Object)new WebMessageAdapter(webMessageCompat)));
        }
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public void setWebMessageCallback(final Handler handler, @NonNull final WebMessageCallbackCompat webMessageCallbackCompat) {
        final WebViewFeatureInternal create_WEB_MESSAGE_CHANNEL = WebViewFeatureInternal.CREATE_WEB_MESSAGE_CHANNEL;
        if (create_WEB_MESSAGE_CHANNEL.isSupportedByFramework()) {
            \u3007oo\u3007.\u3007080(this.getFrameworksImpl(), (WebMessagePort$WebMessageCallback)new WebMessagePort$WebMessageCallback(this, webMessageCallbackCompat) {
                final WebMessagePortImpl this$0;
                final WebMessageCallbackCompat val$callback;
                
                public void onMessage(final WebMessagePort webMessagePort, final WebMessage webMessage) {
                    this.val$callback.onMessage(new WebMessagePortImpl(webMessagePort), WebMessagePortImpl.frameworkMessageToCompat(webMessage));
                }
            }, handler);
        }
        else {
            if (!create_WEB_MESSAGE_CHANNEL.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.getBoundaryInterface().setWebMessageCallback(BoundaryInterfaceReflectionUtil.\u3007o\u3007((Object)new WebMessageCallbackAdapter(webMessageCallbackCompat)), handler);
        }
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public void setWebMessageCallback(@NonNull final WebMessageCallbackCompat webMessageCallbackCompat) {
        final WebViewFeatureInternal web_MESSAGE_PORT_SET_MESSAGE_CALLBACK = WebViewFeatureInternal.WEB_MESSAGE_PORT_SET_MESSAGE_CALLBACK;
        if (web_MESSAGE_PORT_SET_MESSAGE_CALLBACK.isSupportedByFramework()) {
            o\u3007O8\u3007\u3007o.\u3007080(this.getFrameworksImpl(), (WebMessagePort$WebMessageCallback)new WebMessagePort$WebMessageCallback(this, webMessageCallbackCompat) {
                final WebMessagePortImpl this$0;
                final WebMessageCallbackCompat val$callback;
                
                public void onMessage(final WebMessagePort webMessagePort, final WebMessage webMessage) {
                    this.val$callback.onMessage(new WebMessagePortImpl(webMessagePort), WebMessagePortImpl.frameworkMessageToCompat(webMessage));
                }
            });
        }
        else {
            if (!web_MESSAGE_PORT_SET_MESSAGE_CALLBACK.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.getBoundaryInterface().setWebMessageCallback(BoundaryInterfaceReflectionUtil.\u3007o\u3007((Object)new WebMessageCallbackAdapter(webMessageCallbackCompat)));
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import org.chromium.support_lib_boundary.WebSettingsBoundaryInterface;

public class WebSettingsAdapter
{
    private WebSettingsBoundaryInterface mBoundaryInterface;
    
    public WebSettingsAdapter(final WebSettingsBoundaryInterface mBoundaryInterface) {
        this.mBoundaryInterface = mBoundaryInterface;
    }
    
    public int getDisabledActionModeMenuItems() {
        return this.mBoundaryInterface.getDisabledActionModeMenuItems();
    }
    
    public int getForceDark() {
        return this.mBoundaryInterface.getForceDark();
    }
    
    public int getForceDarkStrategy() {
        return this.mBoundaryInterface.getForceDarkBehavior();
    }
    
    public boolean getOffscreenPreRaster() {
        return this.mBoundaryInterface.getOffscreenPreRaster();
    }
    
    public boolean getSafeBrowsingEnabled() {
        return this.mBoundaryInterface.getSafeBrowsingEnabled();
    }
    
    public void setDisabledActionModeMenuItems(final int disabledActionModeMenuItems) {
        this.mBoundaryInterface.setDisabledActionModeMenuItems(disabledActionModeMenuItems);
    }
    
    public void setForceDark(final int forceDark) {
        this.mBoundaryInterface.setForceDark(forceDark);
    }
    
    public void setForceDarkStrategy(final int forceDarkBehavior) {
        this.mBoundaryInterface.setForceDarkBehavior(forceDarkBehavior);
    }
    
    public void setOffscreenPreRaster(final boolean offscreenPreRaster) {
        this.mBoundaryInterface.setOffscreenPreRaster(offscreenPreRaster);
    }
    
    public void setSafeBrowsingEnabled(final boolean safeBrowsingEnabled) {
        this.mBoundaryInterface.setSafeBrowsingEnabled(safeBrowsingEnabled);
    }
    
    public void setWillSuppressErrorPage(final boolean willSuppressErrorPage) {
        this.mBoundaryInterface.setWillSuppressErrorPage(willSuppressErrorPage);
    }
    
    public boolean willSuppressErrorPage() {
        return this.mBoundaryInterface.getWillSuppressErrorPage();
    }
}

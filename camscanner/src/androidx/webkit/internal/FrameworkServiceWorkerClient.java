// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import androidx.annotation.Nullable;
import android.webkit.WebResourceResponse;
import android.webkit.WebResourceRequest;
import androidx.annotation.NonNull;
import androidx.webkit.ServiceWorkerClientCompat;
import androidx.annotation.RequiresApi;
import android.webkit.ServiceWorkerClient;

@RequiresApi(24)
public class FrameworkServiceWorkerClient extends ServiceWorkerClient
{
    private final ServiceWorkerClientCompat mImpl;
    
    public FrameworkServiceWorkerClient(@NonNull final ServiceWorkerClientCompat mImpl) {
        this.mImpl = mImpl;
    }
    
    @Nullable
    public WebResourceResponse shouldInterceptRequest(@NonNull final WebResourceRequest webResourceRequest) {
        return this.mImpl.shouldInterceptRequest(webResourceRequest);
    }
}

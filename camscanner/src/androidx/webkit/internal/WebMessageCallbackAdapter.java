// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import org.chromium.support_lib_boundary.WebMessageBoundaryInterface;
import java.lang.reflect.InvocationHandler;
import androidx.annotation.NonNull;
import androidx.webkit.WebMessagePortCompat;
import org.chromium.support_lib_boundary.WebMessageCallbackBoundaryInterface;

public class WebMessageCallbackAdapter implements WebMessageCallbackBoundaryInterface
{
    WebMessagePortCompat.WebMessageCallbackCompat mImpl;
    
    public WebMessageCallbackAdapter(@NonNull final WebMessagePortCompat.WebMessageCallbackCompat mImpl) {
        this.mImpl = mImpl;
    }
    
    @NonNull
    public String[] getSupportedFeatures() {
        return new String[] { "WEB_MESSAGE_CALLBACK_ON_MESSAGE" };
    }
    
    public void onMessage(final InvocationHandler invocationHandler, final InvocationHandler invocationHandler2) {
        this.mImpl.onMessage(new WebMessagePortImpl(invocationHandler), WebMessageAdapter.webMessageCompatFromBoundaryInterface((WebMessageBoundaryInterface)BoundaryInterfaceReflectionUtil.\u3007080((Class)WebMessageBoundaryInterface.class, invocationHandler2)));
    }
}

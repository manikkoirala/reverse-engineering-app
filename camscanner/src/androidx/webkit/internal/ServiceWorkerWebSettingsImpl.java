// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit.internal;

import android.annotation.SuppressLint;
import androidx.annotation.RequiresApi;
import java.lang.reflect.Proxy;
import org.chromium.support_lib_boundary.util.BoundaryInterfaceReflectionUtil;
import java.lang.reflect.InvocationHandler;
import androidx.annotation.NonNull;
import android.webkit.ServiceWorkerWebSettings;
import org.chromium.support_lib_boundary.ServiceWorkerWebSettingsBoundaryInterface;
import androidx.webkit.ServiceWorkerWebSettingsCompat;

public class ServiceWorkerWebSettingsImpl extends ServiceWorkerWebSettingsCompat
{
    private ServiceWorkerWebSettingsBoundaryInterface mBoundaryInterface;
    private ServiceWorkerWebSettings mFrameworksImpl;
    
    public ServiceWorkerWebSettingsImpl(@NonNull final ServiceWorkerWebSettings mFrameworksImpl) {
        this.mFrameworksImpl = mFrameworksImpl;
    }
    
    public ServiceWorkerWebSettingsImpl(@NonNull final InvocationHandler invocationHandler) {
        this.mBoundaryInterface = (ServiceWorkerWebSettingsBoundaryInterface)BoundaryInterfaceReflectionUtil.\u3007080((Class)ServiceWorkerWebSettingsBoundaryInterface.class, invocationHandler);
    }
    
    private ServiceWorkerWebSettingsBoundaryInterface getBoundaryInterface() {
        if (this.mBoundaryInterface == null) {
            this.mBoundaryInterface = (ServiceWorkerWebSettingsBoundaryInterface)BoundaryInterfaceReflectionUtil.\u3007080((Class)ServiceWorkerWebSettingsBoundaryInterface.class, WebViewGlueCommunicator.getCompatConverter().convertServiceWorkerSettings(this.mFrameworksImpl));
        }
        return this.mBoundaryInterface;
    }
    
    @RequiresApi(24)
    private ServiceWorkerWebSettings getFrameworksImpl() {
        if (this.mFrameworksImpl == null) {
            this.mFrameworksImpl = WebViewGlueCommunicator.getCompatConverter().convertServiceWorkerSettings(Proxy.getInvocationHandler(this.mBoundaryInterface));
        }
        return this.mFrameworksImpl;
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public boolean getAllowContentAccess() {
        final WebViewFeatureInternal service_WORKER_CONTENT_ACCESS = WebViewFeatureInternal.SERVICE_WORKER_CONTENT_ACCESS;
        if (service_WORKER_CONTENT_ACCESS.isSupportedByFramework()) {
            return OO0o\u3007\u3007.\u3007080(this.getFrameworksImpl());
        }
        if (service_WORKER_CONTENT_ACCESS.isSupportedByWebView()) {
            return this.getBoundaryInterface().getAllowContentAccess();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public boolean getAllowFileAccess() {
        final WebViewFeatureInternal service_WORKER_FILE_ACCESS = WebViewFeatureInternal.SERVICE_WORKER_FILE_ACCESS;
        if (service_WORKER_FILE_ACCESS.isSupportedByFramework()) {
            return \u30078o8o\u3007.\u3007080(this.getFrameworksImpl());
        }
        if (service_WORKER_FILE_ACCESS.isSupportedByWebView()) {
            return this.getBoundaryInterface().getAllowFileAccess();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public boolean getBlockNetworkLoads() {
        final WebViewFeatureInternal service_WORKER_BLOCK_NETWORK_LOADS = WebViewFeatureInternal.SERVICE_WORKER_BLOCK_NETWORK_LOADS;
        if (service_WORKER_BLOCK_NETWORK_LOADS.isSupportedByFramework()) {
            return Oooo8o0\u3007.\u3007080(this.getFrameworksImpl());
        }
        if (service_WORKER_BLOCK_NETWORK_LOADS.isSupportedByWebView()) {
            return this.getBoundaryInterface().getBlockNetworkLoads();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public int getCacheMode() {
        final WebViewFeatureInternal service_WORKER_CACHE_MODE = WebViewFeatureInternal.SERVICE_WORKER_CACHE_MODE;
        if (service_WORKER_CACHE_MODE.isSupportedByFramework()) {
            return \u3007\u3007888.\u3007080(this.getFrameworksImpl());
        }
        if (service_WORKER_CACHE_MODE.isSupportedByWebView()) {
            return this.getBoundaryInterface().getCacheMode();
        }
        throw WebViewFeatureInternal.getUnsupportedOperationException();
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public void setAllowContentAccess(final boolean allowContentAccess) {
        final WebViewFeatureInternal service_WORKER_CONTENT_ACCESS = WebViewFeatureInternal.SERVICE_WORKER_CONTENT_ACCESS;
        if (service_WORKER_CONTENT_ACCESS.isSupportedByFramework()) {
            \u3007O8o08O.\u3007080(this.getFrameworksImpl(), allowContentAccess);
        }
        else {
            if (!service_WORKER_CONTENT_ACCESS.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.getBoundaryInterface().setAllowContentAccess(allowContentAccess);
        }
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public void setAllowFileAccess(final boolean allowFileAccess) {
        final WebViewFeatureInternal service_WORKER_FILE_ACCESS = WebViewFeatureInternal.SERVICE_WORKER_FILE_ACCESS;
        if (service_WORKER_FILE_ACCESS.isSupportedByFramework()) {
            \u300780\u3007808\u3007O.\u3007080(this.getFrameworksImpl(), allowFileAccess);
        }
        else {
            if (!service_WORKER_FILE_ACCESS.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.getBoundaryInterface().setAllowFileAccess(allowFileAccess);
        }
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public void setBlockNetworkLoads(final boolean blockNetworkLoads) {
        final WebViewFeatureInternal service_WORKER_BLOCK_NETWORK_LOADS = WebViewFeatureInternal.SERVICE_WORKER_BLOCK_NETWORK_LOADS;
        if (service_WORKER_BLOCK_NETWORK_LOADS.isSupportedByFramework()) {
            oO80.\u3007080(this.getFrameworksImpl(), blockNetworkLoads);
        }
        else {
            if (!service_WORKER_BLOCK_NETWORK_LOADS.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.getBoundaryInterface().setBlockNetworkLoads(blockNetworkLoads);
        }
    }
    
    @SuppressLint({ "NewApi" })
    @Override
    public void setCacheMode(final int cacheMode) {
        final WebViewFeatureInternal service_WORKER_CACHE_MODE = WebViewFeatureInternal.SERVICE_WORKER_CACHE_MODE;
        if (service_WORKER_CACHE_MODE.isSupportedByFramework()) {
            OO0o\u3007\u3007\u3007\u30070.\u3007080(this.getFrameworksImpl(), cacheMode);
        }
        else {
            if (!service_WORKER_CACHE_MODE.isSupportedByWebView()) {
                throw WebViewFeatureInternal.getUnsupportedOperationException();
            }
            this.getBoundaryInterface().setCacheMode(cacheMode);
        }
    }
}

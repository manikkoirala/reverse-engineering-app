// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public abstract class ScriptReferenceCompat
{
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public ScriptReferenceCompat() {
    }
    
    public abstract void remove();
}

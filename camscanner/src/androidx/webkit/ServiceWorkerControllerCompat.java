// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import androidx.webkit.internal.ServiceWorkerControllerImpl;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;

public abstract class ServiceWorkerControllerCompat
{
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public ServiceWorkerControllerCompat() {
    }
    
    @NonNull
    public static ServiceWorkerControllerCompat getInstance() {
        return LAZY_HOLDER.INSTANCE;
    }
    
    @NonNull
    public abstract ServiceWorkerWebSettingsCompat getServiceWorkerWebSettings();
    
    public abstract void setServiceWorkerClient(@Nullable final ServiceWorkerClientCompat p0);
    
    private static class LAZY_HOLDER
    {
        static final ServiceWorkerControllerCompat INSTANCE;
        
        static {
            INSTANCE = new ServiceWorkerControllerImpl();
        }
    }
}

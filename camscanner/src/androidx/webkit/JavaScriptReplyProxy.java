// 
// Decompiled by Procyon v0.6.0
// 

package androidx.webkit;

import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;

public abstract class JavaScriptReplyProxy
{
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public JavaScriptReplyProxy() {
    }
    
    public abstract void postMessage(@NonNull final String p0);
}

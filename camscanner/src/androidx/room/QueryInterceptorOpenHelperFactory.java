// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import androidx.annotation.NonNull;
import java.util.concurrent.Executor;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

final class QueryInterceptorOpenHelperFactory implements Factory
{
    private final Factory mDelegate;
    private final RoomDatabase.QueryCallback mQueryCallback;
    private final Executor mQueryCallbackExecutor;
    
    QueryInterceptorOpenHelperFactory(@NonNull final Factory mDelegate, @NonNull final RoomDatabase.QueryCallback mQueryCallback, @NonNull final Executor mQueryCallbackExecutor) {
        this.mDelegate = mDelegate;
        this.mQueryCallback = mQueryCallback;
        this.mQueryCallbackExecutor = mQueryCallbackExecutor;
    }
    
    @NonNull
    @Override
    public SupportSQLiteOpenHelper create(@NonNull final Configuration configuration) {
        return new QueryInterceptorOpenHelper(this.mDelegate.create(configuration), this.mQueryCallback, this.mQueryCallbackExecutor);
    }
}

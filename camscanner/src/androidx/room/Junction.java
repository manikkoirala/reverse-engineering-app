// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.CLASS)
@Target({})
public @interface Junction {
    String entityColumn() default "";
    
    String parentColumn() default "";
    
    Class<?> value();
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import androidx.annotation.VisibleForTesting;
import androidx.arch.core.util.Function;
import java.io.IOException;
import androidx.room.util.SneakyThrow;
import android.os.SystemClock;
import android.os.Looper;
import java.util.concurrent.TimeUnit;
import android.os.Handler;
import java.util.concurrent.Executor;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.annotation.Nullable;
import androidx.annotation.GuardedBy;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.annotation.NonNull;

final class AutoCloser
{
    final long mAutoCloseTimeoutInMs;
    @NonNull
    final Runnable mAutoCloser;
    @GuardedBy("mLock")
    @Nullable
    SupportSQLiteDatabase mDelegateDatabase;
    @Nullable
    private SupportSQLiteOpenHelper mDelegateOpenHelper;
    private final Runnable mExecuteAutoCloser;
    @NonNull
    final Executor mExecutor;
    @NonNull
    private final Handler mHandler;
    @GuardedBy("mLock")
    long mLastDecrementRefCountTimeStamp;
    @NonNull
    final Object mLock;
    private boolean mManuallyClosed;
    @Nullable
    Runnable mOnAutoCloseCallback;
    @GuardedBy("mLock")
    int mRefCount;
    
    AutoCloser(final long duration, @NonNull final TimeUnit timeUnit, @NonNull final Executor mExecutor) {
        this.mDelegateOpenHelper = null;
        this.mHandler = new Handler(Looper.getMainLooper());
        this.mOnAutoCloseCallback = null;
        this.mLock = new Object();
        this.mRefCount = 0;
        this.mLastDecrementRefCountTimeStamp = SystemClock.uptimeMillis();
        this.mManuallyClosed = false;
        this.mExecuteAutoCloser = new Runnable() {
            final AutoCloser this$0;
            
            @Override
            public void run() {
                final AutoCloser this$0 = this.this$0;
                this$0.mExecutor.execute(this$0.mAutoCloser);
            }
        };
        this.mAutoCloser = new Runnable() {
            final AutoCloser this$0;
            
            @Override
            public void run() {
                synchronized (this.this$0.mLock) {
                    final long uptimeMillis = SystemClock.uptimeMillis();
                    final AutoCloser this$0 = this.this$0;
                    if (uptimeMillis - this$0.mLastDecrementRefCountTimeStamp < this$0.mAutoCloseTimeoutInMs) {
                        return;
                    }
                    if (this$0.mRefCount != 0) {
                        return;
                    }
                    final Runnable mOnAutoCloseCallback = this$0.mOnAutoCloseCallback;
                    if (mOnAutoCloseCallback != null) {
                        mOnAutoCloseCallback.run();
                        final SupportSQLiteDatabase mDelegateDatabase = this.this$0.mDelegateDatabase;
                        if (mDelegateDatabase != null && mDelegateDatabase.isOpen()) {
                            try {
                                this.this$0.mDelegateDatabase.close();
                            }
                            catch (final IOException ex) {
                                SneakyThrow.reThrow(ex);
                            }
                            this.this$0.mDelegateDatabase = null;
                        }
                        return;
                    }
                    throw new IllegalStateException("mOnAutoCloseCallback is null but it should have been set before use. Please file a bug against Room at: https://issuetracker.google.com/issues/new?component=413107&template=1096568");
                }
            }
        };
        this.mAutoCloseTimeoutInMs = timeUnit.toMillis(duration);
        this.mExecutor = mExecutor;
    }
    
    public void closeDatabaseIfOpen() throws IOException {
        synchronized (this.mLock) {
            this.mManuallyClosed = true;
            final SupportSQLiteDatabase mDelegateDatabase = this.mDelegateDatabase;
            if (mDelegateDatabase != null) {
                mDelegateDatabase.close();
            }
            this.mDelegateDatabase = null;
        }
    }
    
    public void decrementCountAndScheduleClose() {
        synchronized (this.mLock) {
            int mRefCount = this.mRefCount;
            if (mRefCount > 0) {
                --mRefCount;
                if ((this.mRefCount = mRefCount) == 0) {
                    if (this.mDelegateDatabase == null) {
                        return;
                    }
                    this.mHandler.postDelayed(this.mExecuteAutoCloser, this.mAutoCloseTimeoutInMs);
                }
                return;
            }
            throw new IllegalStateException("ref count is 0 or lower but we're supposed to decrement");
        }
    }
    
    @Nullable
    public <V> V executeRefCountingFunction(@NonNull final Function<SupportSQLiteDatabase, V> function) {
        try {
            return function.apply(this.incrementCountAndEnsureDbIsOpen());
        }
        finally {
            this.decrementCountAndScheduleClose();
        }
    }
    
    @Nullable
    public SupportSQLiteDatabase getDelegateDatabase() {
        synchronized (this.mLock) {
            return this.mDelegateDatabase;
        }
    }
    
    @VisibleForTesting
    public int getRefCountForTest() {
        synchronized (this.mLock) {
            return this.mRefCount;
        }
    }
    
    @NonNull
    public SupportSQLiteDatabase incrementCountAndEnsureDbIsOpen() {
        synchronized (this.mLock) {
            this.mHandler.removeCallbacks(this.mExecuteAutoCloser);
            ++this.mRefCount;
            if (this.mManuallyClosed) {
                throw new IllegalStateException("Attempting to open already closed database.");
            }
            final SupportSQLiteDatabase mDelegateDatabase = this.mDelegateDatabase;
            if (mDelegateDatabase != null && mDelegateDatabase.isOpen()) {
                return this.mDelegateDatabase;
            }
            final SupportSQLiteOpenHelper mDelegateOpenHelper = this.mDelegateOpenHelper;
            if (mDelegateOpenHelper != null) {
                return this.mDelegateDatabase = mDelegateOpenHelper.getWritableDatabase();
            }
            throw new IllegalStateException("AutoCloser has not been initialized. Please file a bug against Room at: https://issuetracker.google.com/issues/new?component=413107&template=1096568");
        }
    }
    
    public void init(@NonNull final SupportSQLiteOpenHelper mDelegateOpenHelper) {
        if (this.mDelegateOpenHelper != null) {
            return;
        }
        this.mDelegateOpenHelper = mDelegateOpenHelper;
    }
    
    public boolean isActive() {
        return this.mManuallyClosed ^ true;
    }
    
    public void setAutoCloseCallback(final Runnable mOnAutoCloseCallback) {
        this.mOnAutoCloseCallback = mOnAutoCloseCallback;
    }
}

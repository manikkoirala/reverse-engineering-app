// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.RestrictTo;
import androidx.sqlite.db.SupportSQLiteProgram;
import androidx.sqlite.db.SupportSQLiteQuery;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public class RoomSQLiteQuery implements SupportSQLiteQuery, SupportSQLiteProgram
{
    private static final int BLOB = 5;
    @VisibleForTesting
    static final int DESIRED_POOL_SIZE = 10;
    private static final int DOUBLE = 3;
    private static final int LONG = 2;
    private static final int NULL = 1;
    @VisibleForTesting
    static final int POOL_LIMIT = 15;
    private static final int STRING = 4;
    @VisibleForTesting
    static final TreeMap<Integer, RoomSQLiteQuery> sQueryPool;
    @VisibleForTesting
    int mArgCount;
    private final int[] mBindingTypes;
    @VisibleForTesting
    final byte[][] mBlobBindings;
    @VisibleForTesting
    final int mCapacity;
    @VisibleForTesting
    final double[] mDoubleBindings;
    @VisibleForTesting
    final long[] mLongBindings;
    private volatile String mQuery;
    @VisibleForTesting
    final String[] mStringBindings;
    
    static {
        sQueryPool = new TreeMap<Integer, RoomSQLiteQuery>();
    }
    
    private RoomSQLiteQuery(int mCapacity) {
        this.mCapacity = mCapacity;
        ++mCapacity;
        this.mBindingTypes = new int[mCapacity];
        this.mLongBindings = new long[mCapacity];
        this.mDoubleBindings = new double[mCapacity];
        this.mStringBindings = new String[mCapacity];
        this.mBlobBindings = new byte[mCapacity][];
    }
    
    public static RoomSQLiteQuery acquire(final String s, final int i) {
        Object sQueryPool = RoomSQLiteQuery.sQueryPool;
        synchronized (sQueryPool) {
            final Map.Entry<Integer, Object> ceilingEntry = (Map.Entry<Integer, Object>)((TreeMap<Integer, RoomSQLiteQuery>)sQueryPool).ceilingEntry(Integer.valueOf(i));
            if (ceilingEntry != null) {
                ((TreeMap<Integer, RoomSQLiteQuery>)sQueryPool).remove(ceilingEntry.getKey());
                final RoomSQLiteQuery roomSQLiteQuery = ceilingEntry.getValue();
                roomSQLiteQuery.init(s, i);
                return roomSQLiteQuery;
            }
            monitorexit(sQueryPool);
            sQueryPool = new RoomSQLiteQuery(i);
            ((RoomSQLiteQuery)sQueryPool).init(s, i);
            return (RoomSQLiteQuery)sQueryPool;
        }
    }
    
    public static RoomSQLiteQuery copyFrom(final SupportSQLiteQuery supportSQLiteQuery) {
        final RoomSQLiteQuery acquire = acquire(supportSQLiteQuery.getSql(), supportSQLiteQuery.getArgCount());
        supportSQLiteQuery.bindTo(new SupportSQLiteProgram(acquire) {
            final RoomSQLiteQuery val$query;
            
            @Override
            public void bindBlob(final int n, final byte[] array) {
                this.val$query.bindBlob(n, array);
            }
            
            @Override
            public void bindDouble(final int n, final double n2) {
                this.val$query.bindDouble(n, n2);
            }
            
            @Override
            public void bindLong(final int n, final long n2) {
                this.val$query.bindLong(n, n2);
            }
            
            @Override
            public void bindNull(final int n) {
                this.val$query.bindNull(n);
            }
            
            @Override
            public void bindString(final int n, final String s) {
                this.val$query.bindString(n, s);
            }
            
            @Override
            public void clearBindings() {
                this.val$query.clearBindings();
            }
            
            @Override
            public void close() {
            }
        });
        return acquire;
    }
    
    private static void prunePoolLocked() {
        final TreeMap<Integer, RoomSQLiteQuery> sQueryPool = RoomSQLiteQuery.sQueryPool;
        if (sQueryPool.size() > 15) {
            int i = sQueryPool.size() - 10;
            final Iterator iterator = sQueryPool.descendingKeySet().iterator();
            while (i > 0) {
                iterator.next();
                iterator.remove();
                --i;
            }
        }
    }
    
    @Override
    public void bindBlob(final int n, final byte[] array) {
        this.mBindingTypes[n] = 5;
        this.mBlobBindings[n] = array;
    }
    
    @Override
    public void bindDouble(final int n, final double n2) {
        this.mBindingTypes[n] = 3;
        this.mDoubleBindings[n] = n2;
    }
    
    @Override
    public void bindLong(final int n, final long n2) {
        this.mBindingTypes[n] = 2;
        this.mLongBindings[n] = n2;
    }
    
    @Override
    public void bindNull(final int n) {
        this.mBindingTypes[n] = 1;
    }
    
    @Override
    public void bindString(final int n, final String s) {
        this.mBindingTypes[n] = 4;
        this.mStringBindings[n] = s;
    }
    
    @Override
    public void bindTo(final SupportSQLiteProgram supportSQLiteProgram) {
        for (int i = 1; i <= this.mArgCount; ++i) {
            final int n = this.mBindingTypes[i];
            if (n != 1) {
                if (n != 2) {
                    if (n != 3) {
                        if (n != 4) {
                            if (n == 5) {
                                supportSQLiteProgram.bindBlob(i, this.mBlobBindings[i]);
                            }
                        }
                        else {
                            supportSQLiteProgram.bindString(i, this.mStringBindings[i]);
                        }
                    }
                    else {
                        supportSQLiteProgram.bindDouble(i, this.mDoubleBindings[i]);
                    }
                }
                else {
                    supportSQLiteProgram.bindLong(i, this.mLongBindings[i]);
                }
            }
            else {
                supportSQLiteProgram.bindNull(i);
            }
        }
    }
    
    @Override
    public void clearBindings() {
        Arrays.fill(this.mBindingTypes, 1);
        Arrays.fill(this.mStringBindings, null);
        Arrays.fill(this.mBlobBindings, null);
        this.mQuery = null;
    }
    
    @Override
    public void close() {
    }
    
    public void copyArgumentsFrom(final RoomSQLiteQuery roomSQLiteQuery) {
        final int n = roomSQLiteQuery.getArgCount() + 1;
        System.arraycopy(roomSQLiteQuery.mBindingTypes, 0, this.mBindingTypes, 0, n);
        System.arraycopy(roomSQLiteQuery.mLongBindings, 0, this.mLongBindings, 0, n);
        System.arraycopy(roomSQLiteQuery.mStringBindings, 0, this.mStringBindings, 0, n);
        System.arraycopy(roomSQLiteQuery.mBlobBindings, 0, this.mBlobBindings, 0, n);
        System.arraycopy(roomSQLiteQuery.mDoubleBindings, 0, this.mDoubleBindings, 0, n);
    }
    
    @Override
    public int getArgCount() {
        return this.mArgCount;
    }
    
    @Override
    public String getSql() {
        return this.mQuery;
    }
    
    void init(final String mQuery, final int mArgCount) {
        this.mQuery = mQuery;
        this.mArgCount = mArgCount;
    }
    
    public void release() {
        final TreeMap<Integer, RoomSQLiteQuery> sQueryPool = RoomSQLiteQuery.sQueryPool;
        synchronized (sQueryPool) {
            sQueryPool.put(this.mCapacity, this);
            prunePoolLocked();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room.migration;

import androidx.annotation.NonNull;
import androidx.sqlite.db.SupportSQLiteDatabase;

public interface AutoMigrationSpec
{
    void onPostMigrate(@NonNull final SupportSQLiteDatabase p0);
}

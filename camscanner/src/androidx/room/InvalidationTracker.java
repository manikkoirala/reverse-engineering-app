// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.concurrent.locks.Lock;
import android.content.Intent;
import android.content.Context;
import androidx.lifecycle.LiveData;
import java.util.concurrent.Callable;
import androidx.annotation.WorkerThread;
import java.util.Collection;
import java.util.Collections;
import androidx.annotation.RestrictTo;
import java.util.Locale;
import java.util.Iterator;
import androidx.sqlite.db.SupportSQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.Cursor;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.sqlite.db.SimpleSQLiteQuery;
import java.util.HashSet;
import java.util.Set;
import java.util.Map;
import androidx.annotation.NonNull;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import android.annotation.SuppressLint;
import androidx.arch.core.internal.SafeIterableMap;
import androidx.sqlite.db.SupportSQLiteStatement;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

public class InvalidationTracker
{
    private static final String CREATE_TRACKING_TABLE_SQL = "CREATE TEMP TABLE room_table_modification_log(table_id INTEGER PRIMARY KEY, invalidated INTEGER NOT NULL DEFAULT 0)";
    private static final String INVALIDATED_COLUMN_NAME = "invalidated";
    @VisibleForTesting
    static final String RESET_UPDATED_TABLES_SQL = "UPDATE room_table_modification_log SET invalidated = 0 WHERE invalidated = 1 ";
    @VisibleForTesting
    static final String SELECT_UPDATED_TABLES_SQL = "SELECT * FROM room_table_modification_log WHERE invalidated = 1;";
    private static final String TABLE_ID_COLUMN_NAME = "table_id";
    private static final String[] TRIGGERS;
    private static final String UPDATE_TABLE_NAME = "room_table_modification_log";
    @Nullable
    AutoCloser mAutoCloser;
    volatile SupportSQLiteStatement mCleanupStatement;
    final RoomDatabase mDatabase;
    private volatile boolean mInitialized;
    private final InvalidationLiveDataContainer mInvalidationLiveDataContainer;
    private MultiInstanceInvalidationClient mMultiInstanceInvalidationClient;
    private final ObservedTableTracker mObservedTableTracker;
    @SuppressLint({ "RestrictedApi" })
    @VisibleForTesting
    final SafeIterableMap<Observer, ObserverWrapper> mObserverMap;
    AtomicBoolean mPendingRefresh;
    @VisibleForTesting
    Runnable mRefreshRunnable;
    private final Object mSyncTriggersLock;
    @NonNull
    final HashMap<String, Integer> mTableIdLookup;
    final String[] mTableNames;
    @NonNull
    private Map<String, Set<String>> mViewTables;
    
    static {
        TRIGGERS = new String[] { "UPDATE", "DELETE", "INSERT" };
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public InvalidationTracker(final RoomDatabase mDatabase, final Map<String, String> map, final Map<String, Set<String>> mViewTables, final String... array) {
        this.mAutoCloser = null;
        int i = 0;
        this.mPendingRefresh = new AtomicBoolean(false);
        this.mInitialized = false;
        this.mObserverMap = new SafeIterableMap<Observer, ObserverWrapper>();
        this.mSyncTriggersLock = new Object();
        this.mRefreshRunnable = new Runnable() {
            final InvalidationTracker this$0;
            
            private Set<Integer> checkUpdatedTable() {
                final HashSet set = new HashSet();
                final Cursor query = this.this$0.mDatabase.query(new SimpleSQLiteQuery("SELECT * FROM room_table_modification_log WHERE invalidated = 1;"));
                try {
                    while (query.moveToNext()) {
                        set.add(query.getInt(0));
                    }
                    query.close();
                    if (!set.isEmpty()) {
                        this.this$0.mCleanupStatement.executeUpdateDelete();
                    }
                    return set;
                }
                finally {
                    query.close();
                }
            }
            
            @Override
            public void run() {
                Object o = this.this$0.mDatabase.getCloseLock();
                ((Lock)o).lock();
                final Set set = null;
                Set<Integer> checkUpdatedTable = null;
                Set<Integer> set2 = set;
                while (true) {
                    try {
                        if (!this.this$0.ensureInitialization()) {
                            return;
                        }
                        set2 = set;
                        if (!this.this$0.mPendingRefresh.compareAndSet(true, false)) {
                            return;
                        }
                        set2 = set;
                        if (this.this$0.mDatabase.inTransaction()) {
                            return;
                        }
                        set2 = set;
                        final SupportSQLiteDatabase writableDatabase = this.this$0.mDatabase.getOpenHelper().getWritableDatabase();
                        set2 = set;
                        writableDatabase.beginTransactionNonExclusive();
                        try {
                            final Set<Integer> set3 = checkUpdatedTable = this.checkUpdatedTable();
                            writableDatabase.setTransactionSuccessful();
                            set2 = set3;
                            writableDatabase.endTransaction();
                            ((Lock)o).unlock();
                            o = this.this$0.mAutoCloser;
                            checkUpdatedTable = set3;
                            if (o != null) {
                                set2 = set3;
                                final Object mAutoCloser = o;
                                ((AutoCloser)mAutoCloser).decrementCountAndScheduleClose();
                                checkUpdatedTable = set2;
                            }
                        }
                        finally {
                            set2 = checkUpdatedTable;
                            writableDatabase.endTransaction();
                            set2 = checkUpdatedTable;
                        }
                    }
                    catch (final IllegalStateException | SQLiteException ex) {
                        ((Lock)o).unlock();
                        final AutoCloser mAutoCloser2 = this.this$0.mAutoCloser;
                        checkUpdatedTable = set2;
                        if (mAutoCloser2 != null) {
                            continue;
                        }
                    }
                    finally {
                        ((Lock)o).unlock();
                        final Object mAutoCloser = this.this$0.mAutoCloser;
                        if (mAutoCloser != null) {
                            ((AutoCloser)mAutoCloser).decrementCountAndScheduleClose();
                        }
                    }
                    break;
                }
                if (checkUpdatedTable != null && !checkUpdatedTable.isEmpty()) {
                    synchronized (this.this$0.mObserverMap) {
                        final Iterator<Map.Entry<Observer, ObserverWrapper>> iterator = this.this$0.mObserverMap.iterator();
                        while (iterator.hasNext()) {
                            ((Map.Entry<K, ObserverWrapper>)iterator.next()).getValue().notifyByTableInvalidStatus(checkUpdatedTable);
                        }
                    }
                }
            }
        };
        this.mDatabase = mDatabase;
        this.mObservedTableTracker = new ObservedTableTracker(array.length);
        this.mTableIdLookup = new HashMap<String, Integer>();
        this.mViewTables = mViewTables;
        this.mInvalidationLiveDataContainer = new InvalidationLiveDataContainer(mDatabase);
        final int length = array.length;
        this.mTableNames = new String[length];
        while (i < length) {
            final String s = array[i];
            final Locale us = Locale.US;
            final String lowerCase = s.toLowerCase(us);
            this.mTableIdLookup.put(lowerCase, i);
            final String s2 = map.get(array[i]);
            if (s2 != null) {
                this.mTableNames[i] = s2.toLowerCase(us);
            }
            else {
                this.mTableNames[i] = lowerCase;
            }
            ++i;
        }
        for (final Map.Entry<K, String> entry : map.entrySet()) {
            final String s3 = entry.getValue();
            final Locale us2 = Locale.US;
            final String lowerCase2 = s3.toLowerCase(us2);
            if (this.mTableIdLookup.containsKey(lowerCase2)) {
                final String lowerCase3 = ((String)entry.getKey()).toLowerCase(us2);
                final HashMap<String, Integer> mTableIdLookup = this.mTableIdLookup;
                mTableIdLookup.put(lowerCase3, mTableIdLookup.get(lowerCase2));
            }
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public InvalidationTracker(final RoomDatabase roomDatabase, final String... array) {
        this(roomDatabase, new HashMap<String, String>(), Collections.emptyMap(), array);
    }
    
    private static void appendTriggerName(final StringBuilder sb, final String str, final String str2) {
        sb.append("`");
        sb.append("room_table_modification_trigger_");
        sb.append(str);
        sb.append("_");
        sb.append(str2);
        sb.append("`");
    }
    
    private static void beginTransactionInternal(final SupportSQLiteDatabase supportSQLiteDatabase) {
        if (supportSQLiteDatabase.isWriteAheadLoggingEnabled()) {
            supportSQLiteDatabase.beginTransactionNonExclusive();
        }
        else {
            supportSQLiteDatabase.beginTransaction();
        }
    }
    
    private String[] resolveViews(final String[] array) {
        final HashSet set = new HashSet();
        for (final String s : array) {
            final String lowerCase = s.toLowerCase(Locale.US);
            if (this.mViewTables.containsKey(lowerCase)) {
                set.addAll(this.mViewTables.get(lowerCase));
            }
            else {
                set.add(s);
            }
        }
        return (String[])set.toArray(new String[set.size()]);
    }
    
    private void startTrackingTable(final SupportSQLiteDatabase supportSQLiteDatabase, final int n) {
        final StringBuilder sb = new StringBuilder();
        sb.append("INSERT OR IGNORE INTO room_table_modification_log VALUES(");
        sb.append(n);
        sb.append(", 0)");
        supportSQLiteDatabase.execSQL(sb.toString());
        final String str = this.mTableNames[n];
        final StringBuilder sb2 = new StringBuilder();
        for (final String str2 : InvalidationTracker.TRIGGERS) {
            sb2.setLength(0);
            sb2.append("CREATE TEMP TRIGGER IF NOT EXISTS ");
            appendTriggerName(sb2, str, str2);
            sb2.append(" AFTER ");
            sb2.append(str2);
            sb2.append(" ON `");
            sb2.append(str);
            sb2.append("` BEGIN UPDATE ");
            sb2.append("room_table_modification_log");
            sb2.append(" SET ");
            sb2.append("invalidated");
            sb2.append(" = 1");
            sb2.append(" WHERE ");
            sb2.append("table_id");
            sb2.append(" = ");
            sb2.append(n);
            sb2.append(" AND ");
            sb2.append("invalidated");
            sb2.append(" = 0");
            sb2.append("; END");
            supportSQLiteDatabase.execSQL(sb2.toString());
        }
    }
    
    private void stopTrackingTable(final SupportSQLiteDatabase supportSQLiteDatabase, int i) {
        final String s = this.mTableNames[i];
        final StringBuilder sb = new StringBuilder();
        final String[] triggers = InvalidationTracker.TRIGGERS;
        int length;
        String s2;
        for (length = triggers.length, i = 0; i < length; ++i) {
            s2 = triggers[i];
            sb.setLength(0);
            sb.append("DROP TRIGGER IF EXISTS ");
            appendTriggerName(sb, s, s2);
            supportSQLiteDatabase.execSQL(sb.toString());
        }
    }
    
    private String[] validateAndResolveTableNames(final String[] array) {
        final String[] resolveViews = this.resolveViews(array);
        for (final String str : resolveViews) {
            if (!this.mTableIdLookup.containsKey(str.toLowerCase(Locale.US))) {
                final StringBuilder sb = new StringBuilder();
                sb.append("There is no table with name ");
                sb.append(str);
                throw new IllegalArgumentException(sb.toString());
            }
        }
        return resolveViews;
    }
    
    @SuppressLint({ "RestrictedApi" })
    @WorkerThread
    public void addObserver(@NonNull final Observer observer) {
        final String[] resolveViews = this.resolveViews(observer.mTables);
        final int[] array = new int[resolveViews.length];
        for (int length = resolveViews.length, i = 0; i < length; ++i) {
            final Integer n = this.mTableIdLookup.get(resolveViews[i].toLowerCase(Locale.US));
            if (n == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("There is no table with name ");
                sb.append(resolveViews[i]);
                throw new IllegalArgumentException(sb.toString());
            }
            array[i] = n;
        }
        final ObserverWrapper observerWrapper = new ObserverWrapper(observer, array, resolveViews);
        synchronized (this.mObserverMap) {
            final ObserverWrapper observerWrapper2 = this.mObserverMap.putIfAbsent(observer, observerWrapper);
            monitorexit(this.mObserverMap);
            if (observerWrapper2 == null && this.mObservedTableTracker.onAdded(array)) {
                this.syncTriggers();
            }
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void addWeakObserver(final Observer observer) {
        this.addObserver((Observer)new WeakObserver(this, observer));
    }
    
    @Deprecated
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public <T> LiveData<T> createLiveData(final String[] array, final Callable<T> callable) {
        return this.createLiveData(array, false, callable);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public <T> LiveData<T> createLiveData(final String[] array, final boolean b, final Callable<T> callable) {
        return this.mInvalidationLiveDataContainer.create(this.validateAndResolveTableNames(array), b, callable);
    }
    
    boolean ensureInitialization() {
        if (!this.mDatabase.isOpen()) {
            return false;
        }
        if (!this.mInitialized) {
            this.mDatabase.getOpenHelper().getWritableDatabase();
        }
        return this.mInitialized;
    }
    
    void internalInit(final SupportSQLiteDatabase supportSQLiteDatabase) {
        synchronized (this) {
            if (this.mInitialized) {
                return;
            }
            supportSQLiteDatabase.execSQL("PRAGMA temp_store = MEMORY;");
            supportSQLiteDatabase.execSQL("PRAGMA recursive_triggers='ON';");
            supportSQLiteDatabase.execSQL("CREATE TEMP TABLE room_table_modification_log(table_id INTEGER PRIMARY KEY, invalidated INTEGER NOT NULL DEFAULT 0)");
            this.syncTriggers(supportSQLiteDatabase);
            this.mCleanupStatement = supportSQLiteDatabase.compileStatement("UPDATE room_table_modification_log SET invalidated = 0 WHERE invalidated = 1 ");
            this.mInitialized = true;
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    @VisibleForTesting(otherwise = 3)
    public void notifyObserversByTableNames(final String... array) {
        synchronized (this.mObserverMap) {
            for (final Map.Entry<Observer, V> entry : this.mObserverMap) {
                if (!entry.getKey().isRemote()) {
                    ((ObserverWrapper)entry.getValue()).notifyByTableNames(array);
                }
            }
        }
    }
    
    void onAutoCloseCallback() {
        synchronized (this) {
            this.mInitialized = false;
            this.mObservedTableTracker.resetTriggerState();
        }
    }
    
    public void refreshVersionsAsync() {
        if (this.mPendingRefresh.compareAndSet(false, true)) {
            final AutoCloser mAutoCloser = this.mAutoCloser;
            if (mAutoCloser != null) {
                mAutoCloser.incrementCountAndEnsureDbIsOpen();
            }
            this.mDatabase.getQueryExecutor().execute(this.mRefreshRunnable);
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    @WorkerThread
    public void refreshVersionsSync() {
        final AutoCloser mAutoCloser = this.mAutoCloser;
        if (mAutoCloser != null) {
            mAutoCloser.incrementCountAndEnsureDbIsOpen();
        }
        this.syncTriggers();
        this.mRefreshRunnable.run();
    }
    
    @SuppressLint({ "RestrictedApi" })
    @WorkerThread
    public void removeObserver(@NonNull final Observer observer) {
        synchronized (this.mObserverMap) {
            final ObserverWrapper observerWrapper = this.mObserverMap.remove(observer);
            monitorexit(this.mObserverMap);
            if (observerWrapper != null && this.mObservedTableTracker.onRemoved(observerWrapper.mTableIds)) {
                this.syncTriggers();
            }
        }
    }
    
    void setAutoCloser(final AutoCloser mAutoCloser) {
        (this.mAutoCloser = mAutoCloser).setAutoCloseCallback(new OOO\u3007O0(this));
    }
    
    void startMultiInstanceInvalidation(final Context context, final String s, final Intent intent) {
        this.mMultiInstanceInvalidationClient = new MultiInstanceInvalidationClient(context, s, intent, this, this.mDatabase.getQueryExecutor());
    }
    
    void stopMultiInstanceInvalidation() {
        final MultiInstanceInvalidationClient mMultiInstanceInvalidationClient = this.mMultiInstanceInvalidationClient;
        if (mMultiInstanceInvalidationClient != null) {
            mMultiInstanceInvalidationClient.stop();
            this.mMultiInstanceInvalidationClient = null;
        }
    }
    
    void syncTriggers() {
        if (!this.mDatabase.isOpen()) {
            return;
        }
        this.syncTriggers(this.mDatabase.getOpenHelper().getWritableDatabase());
    }
    
    void syncTriggers(final SupportSQLiteDatabase supportSQLiteDatabase) {
        if (supportSQLiteDatabase.inTransaction()) {
            return;
        }
        try {
            final Lock closeLock = this.mDatabase.getCloseLock();
            closeLock.lock();
            try {
                synchronized (this.mSyncTriggersLock) {
                    final int[] tablesToSync = this.mObservedTableTracker.getTablesToSync();
                    if (tablesToSync == null) {
                        return;
                    }
                    final int length = tablesToSync.length;
                    beginTransactionInternal(supportSQLiteDatabase);
                    int n = 0;
                    while (true) {
                        Label_0117: {
                            if (n >= length) {
                                break Label_0117;
                            }
                            final int n2 = tablesToSync[n];
                            Label_0105: {
                                if (n2 == 1) {
                                    break Label_0105;
                                }
                                Label_0111: {
                                    if (n2 != 2) {
                                        break Label_0111;
                                    }
                                    try {
                                        this.stopTrackingTable(supportSQLiteDatabase, n);
                                        ++n;
                                        continue;
                                        supportSQLiteDatabase.setTransactionSuccessful();
                                        return;
                                        this.startTrackingTable(supportSQLiteDatabase, n);
                                    }
                                    finally {
                                        supportSQLiteDatabase.endTransaction();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            finally {
                closeLock.unlock();
            }
        }
        catch (final IllegalStateException | SQLiteException ex) {}
    }
    
    static class ObservedTableTracker
    {
        static final int ADD = 1;
        static final int NO_OP = 0;
        static final int REMOVE = 2;
        boolean mNeedsSync;
        final long[] mTableObservers;
        final int[] mTriggerStateChanges;
        final boolean[] mTriggerStates;
        
        ObservedTableTracker(final int n) {
            final long[] array = new long[n];
            this.mTableObservers = array;
            final boolean[] array2 = new boolean[n];
            this.mTriggerStates = array2;
            this.mTriggerStateChanges = new int[n];
            Arrays.fill(array, 0L);
            Arrays.fill(array2, false);
        }
        
        @Nullable
        int[] getTablesToSync() {
            synchronized (this) {
                if (!this.mNeedsSync) {
                    return null;
                }
                for (int length = this.mTableObservers.length, i = 0; i < length; ++i) {
                    final long n = this.mTableObservers[i];
                    int n2 = 1;
                    final boolean b = n > 0L;
                    final boolean[] mTriggerStates = this.mTriggerStates;
                    if (b != mTriggerStates[i]) {
                        final int[] mTriggerStateChanges = this.mTriggerStateChanges;
                        if (!b) {
                            n2 = 2;
                        }
                        mTriggerStateChanges[i] = n2;
                    }
                    else {
                        this.mTriggerStateChanges[i] = 0;
                    }
                    mTriggerStates[i] = b;
                }
                this.mNeedsSync = false;
                return this.mTriggerStateChanges.clone();
            }
        }
        
        boolean onAdded(final int... array) {
            synchronized (this) {
                final int length = array.length;
                int i = 0;
                boolean b = false;
                while (i < length) {
                    final int n = array[i];
                    final long[] mTableObservers = this.mTableObservers;
                    final long n2 = mTableObservers[n];
                    mTableObservers[n] = 1L + n2;
                    if (n2 == 0L) {
                        b = true;
                        this.mNeedsSync = true;
                    }
                    ++i;
                }
                return b;
            }
        }
        
        boolean onRemoved(final int... array) {
            synchronized (this) {
                final int length = array.length;
                int i = 0;
                boolean b = false;
                while (i < length) {
                    if (this.mTableObservers[array[i]]-- == 1L) {
                        b = true;
                        this.mNeedsSync = true;
                    }
                    ++i;
                }
                return b;
            }
        }
        
        void resetTriggerState() {
            synchronized (this) {
                Arrays.fill(this.mTriggerStates, false);
                this.mNeedsSync = true;
            }
        }
    }
    
    public abstract static class Observer
    {
        final String[] mTables;
        
        protected Observer(@NonNull final String s, final String... original) {
            (this.mTables = Arrays.copyOf(original, original.length + 1))[original.length] = s;
        }
        
        public Observer(@NonNull final String[] original) {
            this.mTables = Arrays.copyOf(original, original.length);
        }
        
        boolean isRemote() {
            return false;
        }
        
        public abstract void onInvalidated(@NonNull final Set<String> p0);
    }
    
    static class ObserverWrapper
    {
        final Observer mObserver;
        private final Set<String> mSingleTableSet;
        final int[] mTableIds;
        private final String[] mTableNames;
        
        ObserverWrapper(final Observer mObserver, final int[] mTableIds, final String[] mTableNames) {
            this.mObserver = mObserver;
            this.mTableIds = mTableIds;
            this.mTableNames = mTableNames;
            if (mTableIds.length == 1) {
                final HashSet s = new HashSet();
                s.add(mTableNames[0]);
                this.mSingleTableSet = (Set<String>)Collections.unmodifiableSet((Set<?>)s);
            }
            else {
                this.mSingleTableSet = null;
            }
        }
        
        void notifyByTableInvalidStatus(final Set<Integer> set) {
            final int length = this.mTableIds.length;
            Set<String> set2 = null;
            Set<String> mSingleTableSet;
            for (int i = 0; i < length; ++i, set2 = mSingleTableSet) {
                mSingleTableSet = set2;
                if (set.contains(this.mTableIds[i])) {
                    if (length == 1) {
                        mSingleTableSet = this.mSingleTableSet;
                    }
                    else {
                        if ((mSingleTableSet = set2) == null) {
                            mSingleTableSet = new HashSet<String>(length);
                        }
                        mSingleTableSet.add(this.mTableNames[i]);
                    }
                }
            }
            if (set2 != null) {
                this.mObserver.onInvalidated(set2);
            }
        }
        
        void notifyByTableNames(final String[] array) {
            final int length = this.mTableNames.length;
            final Set<String> set = null;
            Set<String> mSingleTableSet;
            if (length == 1) {
                final int length2 = array.length;
                int n = 0;
                while (true) {
                    mSingleTableSet = set;
                    if (n >= length2) {
                        break;
                    }
                    if (array[n].equalsIgnoreCase(this.mTableNames[0])) {
                        mSingleTableSet = this.mSingleTableSet;
                        break;
                    }
                    ++n;
                }
            }
            else {
                final HashSet set2 = new HashSet();
                for (final String anotherString : array) {
                    for (final String e : this.mTableNames) {
                        if (e.equalsIgnoreCase(anotherString)) {
                            set2.add(e);
                            break;
                        }
                    }
                }
                mSingleTableSet = set;
                if (set2.size() > 0) {
                    mSingleTableSet = set2;
                }
            }
            if (mSingleTableSet != null) {
                this.mObserver.onInvalidated(mSingleTableSet);
            }
        }
    }
    
    static class WeakObserver extends Observer
    {
        final WeakReference<Observer> mDelegateRef;
        final InvalidationTracker mTracker;
        
        WeakObserver(final InvalidationTracker mTracker, final Observer referent) {
            super(referent.mTables);
            this.mTracker = mTracker;
            this.mDelegateRef = new WeakReference<Observer>(referent);
        }
        
        @Override
        public void onInvalidated(@NonNull final Set<String> set) {
            final Observer observer = this.mDelegateRef.get();
            if (observer == null) {
                this.mTracker.removeObserver((Observer)this);
            }
            else {
                observer.onInvalidated(set);
            }
        }
    }
}

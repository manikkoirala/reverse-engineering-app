// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import android.content.ContentResolver;
import android.database.DataSetObserver;
import android.database.ContentObserver;
import androidx.sqlite.db.SupportSQLiteCompat;
import android.net.Uri;
import android.os.Bundle;
import android.database.CharArrayBuffer;
import java.util.ArrayList;
import android.os.CancellationSignal;
import android.database.Cursor;
import androidx.sqlite.db.SupportSQLiteQuery;
import android.util.Pair;
import java.util.List;
import android.database.SQLException;
import androidx.sqlite.db.O8;
import androidx.arch.core.util.Function;
import androidx.sqlite.db.SupportSQLiteStatement;
import android.database.sqlite.SQLiteTransactionListener;
import java.util.Locale;
import android.content.ContentValues;
import androidx.annotation.RequiresApi;
import androidx.annotation.Nullable;
import androidx.sqlite.db.SupportSQLiteDatabase;
import java.io.IOException;
import androidx.room.util.SneakyThrow;
import androidx.annotation.NonNull;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

final class AutoClosingRoomOpenHelper implements SupportSQLiteOpenHelper, DelegatingOpenHelper
{
    @NonNull
    private final AutoCloser mAutoCloser;
    @NonNull
    private final AutoClosingSupportSQLiteDatabase mAutoClosingDb;
    @NonNull
    private final SupportSQLiteOpenHelper mDelegateOpenHelper;
    
    AutoClosingRoomOpenHelper(@NonNull final SupportSQLiteOpenHelper mDelegateOpenHelper, @NonNull final AutoCloser mAutoCloser) {
        this.mDelegateOpenHelper = mDelegateOpenHelper;
        (this.mAutoCloser = mAutoCloser).init(mDelegateOpenHelper);
        this.mAutoClosingDb = new AutoClosingSupportSQLiteDatabase(mAutoCloser);
    }
    
    @Override
    public void close() {
        try {
            this.mAutoClosingDb.close();
        }
        catch (final IOException ex) {
            SneakyThrow.reThrow(ex);
        }
    }
    
    @NonNull
    AutoCloser getAutoCloser() {
        return this.mAutoCloser;
    }
    
    @NonNull
    SupportSQLiteDatabase getAutoClosingDb() {
        return this.mAutoClosingDb;
    }
    
    @Nullable
    @Override
    public String getDatabaseName() {
        return this.mDelegateOpenHelper.getDatabaseName();
    }
    
    @NonNull
    @Override
    public SupportSQLiteOpenHelper getDelegate() {
        return this.mDelegateOpenHelper;
    }
    
    @NonNull
    @RequiresApi(api = 24)
    @Override
    public SupportSQLiteDatabase getReadableDatabase() {
        this.mAutoClosingDb.pokeOpen();
        return this.mAutoClosingDb;
    }
    
    @NonNull
    @RequiresApi(api = 24)
    @Override
    public SupportSQLiteDatabase getWritableDatabase() {
        this.mAutoClosingDb.pokeOpen();
        return this.mAutoClosingDb;
    }
    
    @RequiresApi(api = 16)
    @Override
    public void setWriteAheadLoggingEnabled(final boolean writeAheadLoggingEnabled) {
        this.mDelegateOpenHelper.setWriteAheadLoggingEnabled(writeAheadLoggingEnabled);
    }
    
    static final class AutoClosingSupportSQLiteDatabase implements SupportSQLiteDatabase
    {
        @NonNull
        private final AutoCloser mAutoCloser;
        
        AutoClosingSupportSQLiteDatabase(@NonNull final AutoCloser mAutoCloser) {
            this.mAutoCloser = mAutoCloser;
        }
        
        @Override
        public void beginTransaction() {
            final SupportSQLiteDatabase incrementCountAndEnsureDbIsOpen = this.mAutoCloser.incrementCountAndEnsureDbIsOpen();
            try {
                incrementCountAndEnsureDbIsOpen.beginTransaction();
            }
            finally {
                this.mAutoCloser.decrementCountAndScheduleClose();
            }
        }
        
        @Override
        public void beginTransactionNonExclusive() {
            final SupportSQLiteDatabase incrementCountAndEnsureDbIsOpen = this.mAutoCloser.incrementCountAndEnsureDbIsOpen();
            try {
                incrementCountAndEnsureDbIsOpen.beginTransactionNonExclusive();
            }
            finally {
                this.mAutoCloser.decrementCountAndScheduleClose();
            }
        }
        
        @Override
        public void beginTransactionWithListener(final SQLiteTransactionListener sqLiteTransactionListener) {
            final SupportSQLiteDatabase incrementCountAndEnsureDbIsOpen = this.mAutoCloser.incrementCountAndEnsureDbIsOpen();
            try {
                incrementCountAndEnsureDbIsOpen.beginTransactionWithListener(sqLiteTransactionListener);
            }
            finally {
                this.mAutoCloser.decrementCountAndScheduleClose();
            }
        }
        
        @Override
        public void beginTransactionWithListenerNonExclusive(final SQLiteTransactionListener sqLiteTransactionListener) {
            final SupportSQLiteDatabase incrementCountAndEnsureDbIsOpen = this.mAutoCloser.incrementCountAndEnsureDbIsOpen();
            try {
                incrementCountAndEnsureDbIsOpen.beginTransactionWithListenerNonExclusive(sqLiteTransactionListener);
            }
            finally {
                this.mAutoCloser.decrementCountAndScheduleClose();
            }
        }
        
        @Override
        public void close() throws IOException {
            this.mAutoCloser.closeDatabaseIfOpen();
        }
        
        @Override
        public SupportSQLiteStatement compileStatement(final String s) {
            return new AutoClosingSupportSqliteStatement(s, this.mAutoCloser);
        }
        
        @Override
        public int delete(final String s, final String s2, final Object[] array) {
            return this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Integer>)new \u3007\u3007888(s, s2, array));
        }
        
        @Override
        public void disableWriteAheadLogging() {
            throw new UnsupportedOperationException("Enable/disable write ahead logging on the OpenHelper instead of on the database directly.");
        }
        
        @Override
        public boolean enableWriteAheadLogging() {
            throw new UnsupportedOperationException("Enable/disable write ahead logging on the OpenHelper instead of on the database directly.");
        }
        
        @Override
        public void endTransaction() {
            if (this.mAutoCloser.getDelegateDatabase() != null) {
                try {
                    this.mAutoCloser.getDelegateDatabase().endTransaction();
                    return;
                }
                finally {
                    this.mAutoCloser.decrementCountAndScheduleClose();
                }
            }
            throw new IllegalStateException("End transaction called but delegateDb is null");
        }
        
        @Override
        public void execSQL(final String s) throws SQLException {
            this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Object>)new \u3007o\u3007(s));
        }
        
        @Override
        public void execSQL(final String s, final Object[] array) throws SQLException {
            this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Object>)new OO0o\u3007\u3007(s, array));
        }
        
        @Override
        public List<Pair<String, String>> getAttachedDbs() {
            return this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, List<Pair<String, String>>>)new o\u3007O8\u3007\u3007o());
        }
        
        @Override
        public long getMaximumSize() {
            return this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Long>)new androidx.room.O8());
        }
        
        @Override
        public long getPageSize() {
            return this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Long>)new \u30078o8o\u3007());
        }
        
        @Override
        public String getPath() {
            return this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, String>)new Oooo8o0\u3007());
        }
        
        @Override
        public int getVersion() {
            return this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Integer>)new \u3007\u30078O0\u30078());
        }
        
        @Override
        public boolean inTransaction() {
            return this.mAutoCloser.getDelegateDatabase() != null && this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Boolean>)new \u3007oo\u3007());
        }
        
        @Override
        public long insert(final String s, final int n, final ContentValues contentValues) throws SQLException {
            return this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Long>)new \u3007O00(s, n, contentValues));
        }
        
        @Override
        public boolean isDatabaseIntegrityOk() {
            return this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Boolean>)new Oo08());
        }
        
        @Override
        public boolean isDbLockedByCurrentThread() {
            return this.mAutoCloser.getDelegateDatabase() != null && this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Boolean>)new \u30070\u3007O0088o());
        }
        
        @Override
        public boolean isOpen() {
            final SupportSQLiteDatabase delegateDatabase = this.mAutoCloser.getDelegateDatabase();
            return delegateDatabase != null && delegateDatabase.isOpen();
        }
        
        @Override
        public boolean isReadOnly() {
            return this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Boolean>)new OoO8());
        }
        
        @RequiresApi(api = 16)
        @Override
        public boolean isWriteAheadLoggingEnabled() {
            return this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Boolean>)new \u3007080());
        }
        
        @Override
        public boolean needUpgrade(final int n) {
            return this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Boolean>)new \u300780\u3007808\u3007O(n));
        }
        
        void pokeOpen() {
            this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Object>)new \u3007o00\u3007\u3007Oo());
        }
        
        @Override
        public Cursor query(final SupportSQLiteQuery supportSQLiteQuery) {
            try {
                return (Cursor)new KeepAliveCursor(this.mAutoCloser.incrementCountAndEnsureDbIsOpen().query(supportSQLiteQuery), this.mAutoCloser);
            }
            finally {
                this.mAutoCloser.decrementCountAndScheduleClose();
            }
        }
        
        @RequiresApi(api = 24)
        @Override
        public Cursor query(final SupportSQLiteQuery supportSQLiteQuery, final CancellationSignal cancellationSignal) {
            try {
                return (Cursor)new KeepAliveCursor(this.mAutoCloser.incrementCountAndEnsureDbIsOpen().query(supportSQLiteQuery, cancellationSignal), this.mAutoCloser);
            }
            finally {
                this.mAutoCloser.decrementCountAndScheduleClose();
            }
        }
        
        @Override
        public Cursor query(final String s) {
            try {
                return (Cursor)new KeepAliveCursor(this.mAutoCloser.incrementCountAndEnsureDbIsOpen().query(s), this.mAutoCloser);
            }
            finally {
                this.mAutoCloser.decrementCountAndScheduleClose();
            }
        }
        
        @Override
        public Cursor query(final String s, final Object[] array) {
            try {
                return (Cursor)new KeepAliveCursor(this.mAutoCloser.incrementCountAndEnsureDbIsOpen().query(s, array), this.mAutoCloser);
            }
            finally {
                this.mAutoCloser.decrementCountAndScheduleClose();
            }
        }
        
        @RequiresApi(api = 16)
        @Override
        public void setForeignKeyConstraintsEnabled(final boolean b) {
            this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Object>)new \u3007O8o08O(b));
        }
        
        @Override
        public void setLocale(final Locale locale) {
            this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Object>)new oo88o8O(locale));
        }
        
        @Override
        public void setMaxSqlCacheSize(final int n) {
            this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Object>)new o800o8O(n));
        }
        
        @Override
        public long setMaximumSize(final long n) {
            return this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Long>)new oO80(n));
        }
        
        @Override
        public void setPageSize(final long n) {
            this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Object>)new OO0o\u3007\u3007\u3007\u30070(n));
        }
        
        @Override
        public void setTransactionSuccessful() {
            final SupportSQLiteDatabase delegateDatabase = this.mAutoCloser.getDelegateDatabase();
            if (delegateDatabase != null) {
                delegateDatabase.setTransactionSuccessful();
                return;
            }
            throw new IllegalStateException("setTransactionSuccessful called but delegateDb is null");
        }
        
        @Override
        public void setVersion(final int n) {
            this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Object>)new o\u30070(n));
        }
        
        @Override
        public int update(final String s, final int n, final ContentValues contentValues, final String s2, final Object[] array) {
            return this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Integer>)new \u3007O888o0o(s, n, contentValues, s2, array));
        }
        
        @Override
        public boolean yieldIfContendedSafely() {
            return this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Boolean>)new \u3007\u3007808\u3007());
        }
        
        @Override
        public boolean yieldIfContendedSafely(final long n) {
            return this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Boolean>)new \u3007\u3007808\u3007());
        }
    }
    
    private static class AutoClosingSupportSqliteStatement implements SupportSQLiteStatement
    {
        private final AutoCloser mAutoCloser;
        private final ArrayList<Object> mBinds;
        private final String mSql;
        
        AutoClosingSupportSqliteStatement(final String mSql, final AutoCloser mAutoCloser) {
            this.mBinds = new ArrayList<Object>();
            this.mSql = mSql;
            this.mAutoCloser = mAutoCloser;
        }
        
        private void doBinds(final SupportSQLiteStatement supportSQLiteStatement) {
            int n;
            for (int i = 0; i < this.mBinds.size(); i = n) {
                n = i + 1;
                final Object value = this.mBinds.get(i);
                if (value == null) {
                    supportSQLiteStatement.bindNull(n);
                }
                else if (value instanceof Long) {
                    supportSQLiteStatement.bindLong(n, (long)value);
                }
                else if (value instanceof Double) {
                    supportSQLiteStatement.bindDouble(n, (double)value);
                }
                else if (value instanceof String) {
                    supportSQLiteStatement.bindString(n, (String)value);
                }
                else if (value instanceof byte[]) {
                    supportSQLiteStatement.bindBlob(n, (byte[])value);
                }
            }
        }
        
        private <T> T executeSqliteStatementWithRefCount(final Function<SupportSQLiteStatement, T> function) {
            return this.mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, T>)new O8ooOoo\u3007(this, function));
        }
        
        private void saveBinds(int i, final Object element) {
            final int index = i - 1;
            if (index >= this.mBinds.size()) {
                for (i = this.mBinds.size(); i <= index; ++i) {
                    this.mBinds.add(null);
                }
            }
            this.mBinds.set(index, element);
        }
        
        @Override
        public void bindBlob(final int n, final byte[] array) {
            this.saveBinds(n, array);
        }
        
        @Override
        public void bindDouble(final int n, final double d) {
            this.saveBinds(n, d);
        }
        
        @Override
        public void bindLong(final int n, final long l) {
            this.saveBinds(n, l);
        }
        
        @Override
        public void bindNull(final int n) {
            this.saveBinds(n, null);
        }
        
        @Override
        public void bindString(final int n, final String s) {
            this.saveBinds(n, s);
        }
        
        @Override
        public void clearBindings() {
            this.mBinds.clear();
        }
        
        @Override
        public void close() throws IOException {
        }
        
        @Override
        public void execute() {
            this.executeSqliteStatementWithRefCount((Function<SupportSQLiteStatement, Object>)new o\u3007\u30070\u3007());
        }
        
        @Override
        public long executeInsert() {
            return this.executeSqliteStatementWithRefCount((Function<SupportSQLiteStatement, Long>)new \u3007oOO8O8());
        }
        
        @Override
        public int executeUpdateDelete() {
            return this.executeSqliteStatementWithRefCount((Function<SupportSQLiteStatement, Integer>)new \u300700());
        }
        
        @Override
        public long simpleQueryForLong() {
            return this.executeSqliteStatementWithRefCount((Function<SupportSQLiteStatement, Long>)new \u30070000OOO());
        }
        
        @Override
        public String simpleQueryForString() {
            return this.executeSqliteStatementWithRefCount((Function<SupportSQLiteStatement, String>)new O\u30078O8\u3007008());
        }
    }
    
    private static final class KeepAliveCursor implements Cursor
    {
        private final AutoCloser mAutoCloser;
        private final Cursor mDelegate;
        
        KeepAliveCursor(final Cursor mDelegate, final AutoCloser mAutoCloser) {
            this.mDelegate = mDelegate;
            this.mAutoCloser = mAutoCloser;
        }
        
        public void close() {
            this.mDelegate.close();
            this.mAutoCloser.decrementCountAndScheduleClose();
        }
        
        public void copyStringToBuffer(final int n, final CharArrayBuffer charArrayBuffer) {
            this.mDelegate.copyStringToBuffer(n, charArrayBuffer);
        }
        
        @Deprecated
        public void deactivate() {
            this.mDelegate.deactivate();
        }
        
        public byte[] getBlob(final int n) {
            return this.mDelegate.getBlob(n);
        }
        
        public int getColumnCount() {
            return this.mDelegate.getColumnCount();
        }
        
        public int getColumnIndex(final String s) {
            return this.mDelegate.getColumnIndex(s);
        }
        
        public int getColumnIndexOrThrow(final String s) throws IllegalArgumentException {
            return this.mDelegate.getColumnIndexOrThrow(s);
        }
        
        public String getColumnName(final int n) {
            return this.mDelegate.getColumnName(n);
        }
        
        public String[] getColumnNames() {
            return this.mDelegate.getColumnNames();
        }
        
        public int getCount() {
            return this.mDelegate.getCount();
        }
        
        public double getDouble(final int n) {
            return this.mDelegate.getDouble(n);
        }
        
        public Bundle getExtras() {
            return this.mDelegate.getExtras();
        }
        
        public float getFloat(final int n) {
            return this.mDelegate.getFloat(n);
        }
        
        public int getInt(final int n) {
            return this.mDelegate.getInt(n);
        }
        
        public long getLong(final int n) {
            return this.mDelegate.getLong(n);
        }
        
        @RequiresApi(api = 19)
        public Uri getNotificationUri() {
            return SupportSQLiteCompat.Api19Impl.getNotificationUri(this.mDelegate);
        }
        
        @Nullable
        @RequiresApi(api = 29)
        public List<Uri> getNotificationUris() {
            return SupportSQLiteCompat.Api29Impl.getNotificationUris(this.mDelegate);
        }
        
        public int getPosition() {
            return this.mDelegate.getPosition();
        }
        
        public short getShort(final int n) {
            return this.mDelegate.getShort(n);
        }
        
        public String getString(final int n) {
            return this.mDelegate.getString(n);
        }
        
        public int getType(final int n) {
            return this.mDelegate.getType(n);
        }
        
        public boolean getWantsAllOnMoveCalls() {
            return this.mDelegate.getWantsAllOnMoveCalls();
        }
        
        public boolean isAfterLast() {
            return this.mDelegate.isAfterLast();
        }
        
        public boolean isBeforeFirst() {
            return this.mDelegate.isBeforeFirst();
        }
        
        public boolean isClosed() {
            return this.mDelegate.isClosed();
        }
        
        public boolean isFirst() {
            return this.mDelegate.isFirst();
        }
        
        public boolean isLast() {
            return this.mDelegate.isLast();
        }
        
        public boolean isNull(final int n) {
            return this.mDelegate.isNull(n);
        }
        
        public boolean move(final int n) {
            return this.mDelegate.move(n);
        }
        
        public boolean moveToFirst() {
            return this.mDelegate.moveToFirst();
        }
        
        public boolean moveToLast() {
            return this.mDelegate.moveToLast();
        }
        
        public boolean moveToNext() {
            return this.mDelegate.moveToNext();
        }
        
        public boolean moveToPosition(final int n) {
            return this.mDelegate.moveToPosition(n);
        }
        
        public boolean moveToPrevious() {
            return this.mDelegate.moveToPrevious();
        }
        
        public void registerContentObserver(final ContentObserver contentObserver) {
            this.mDelegate.registerContentObserver(contentObserver);
        }
        
        public void registerDataSetObserver(final DataSetObserver dataSetObserver) {
            this.mDelegate.registerDataSetObserver(dataSetObserver);
        }
        
        @Deprecated
        public boolean requery() {
            return this.mDelegate.requery();
        }
        
        public Bundle respond(final Bundle bundle) {
            return this.mDelegate.respond(bundle);
        }
        
        @RequiresApi(api = 23)
        public void setExtras(final Bundle bundle) {
            SupportSQLiteCompat.Api23Impl.setExtras(this.mDelegate, bundle);
        }
        
        public void setNotificationUri(final ContentResolver contentResolver, final Uri uri) {
            this.mDelegate.setNotificationUri(contentResolver, uri);
        }
        
        @RequiresApi(api = 29)
        public void setNotificationUris(@NonNull final ContentResolver contentResolver, @NonNull final List<Uri> list) {
            SupportSQLiteCompat.Api29Impl.setNotificationUris(this.mDelegate, contentResolver, list);
        }
        
        public void unregisterContentObserver(final ContentObserver contentObserver) {
            this.mDelegate.unregisterContentObserver(contentObserver);
        }
        
        public void unregisterDataSetObserver(final DataSetObserver dataSetObserver) {
            this.mDelegate.unregisterDataSetObserver(dataSetObserver);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.util.ArrayList;
import java.util.List;
import java.util.Collection;
import java.util.Iterator;
import androidx.sqlite.db.SupportSQLiteStatement;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public abstract class EntityInsertionAdapter<T> extends SharedSQLiteStatement
{
    public EntityInsertionAdapter(final RoomDatabase roomDatabase) {
        super(roomDatabase);
    }
    
    protected abstract void bind(final SupportSQLiteStatement p0, final T p1);
    
    public final void insert(final Iterable<? extends T> iterable) {
        final SupportSQLiteStatement acquire = this.acquire();
        try {
            final Iterator<? extends T> iterator = iterable.iterator();
            while (iterator.hasNext()) {
                this.bind(acquire, iterator.next());
                acquire.executeInsert();
            }
        }
        finally {
            this.release(acquire);
        }
    }
    
    public final void insert(final T t) {
        final SupportSQLiteStatement acquire = this.acquire();
        try {
            this.bind(acquire, t);
            acquire.executeInsert();
        }
        finally {
            this.release(acquire);
        }
    }
    
    public final void insert(final T[] array) {
        final SupportSQLiteStatement acquire = this.acquire();
        try {
            for (int length = array.length, i = 0; i < length; ++i) {
                this.bind(acquire, array[i]);
                acquire.executeInsert();
            }
        }
        finally {
            this.release(acquire);
        }
    }
    
    public final long insertAndReturnId(final T t) {
        final SupportSQLiteStatement acquire = this.acquire();
        try {
            this.bind(acquire, t);
            return acquire.executeInsert();
        }
        finally {
            this.release(acquire);
        }
    }
    
    public final long[] insertAndReturnIdsArray(final Collection<? extends T> collection) {
        final SupportSQLiteStatement acquire = this.acquire();
        try {
            final long[] array = new long[collection.size()];
            final Iterator iterator = collection.iterator();
            int n = 0;
            while (iterator.hasNext()) {
                this.bind(acquire, iterator.next());
                array[n] = acquire.executeInsert();
                ++n;
            }
            return array;
        }
        finally {
            this.release(acquire);
        }
    }
    
    public final long[] insertAndReturnIdsArray(final T[] array) {
        final SupportSQLiteStatement acquire = this.acquire();
        try {
            final long[] array2 = new long[array.length];
            final int length = array.length;
            int i = 0;
            int n = 0;
            while (i < length) {
                this.bind(acquire, array[i]);
                array2[n] = acquire.executeInsert();
                ++n;
                ++i;
            }
            return array2;
        }
        finally {
            this.release(acquire);
        }
    }
    
    public final Long[] insertAndReturnIdsArrayBox(final Collection<? extends T> collection) {
        final SupportSQLiteStatement acquire = this.acquire();
        try {
            final Long[] array = new Long[collection.size()];
            final Iterator iterator = collection.iterator();
            int n = 0;
            while (iterator.hasNext()) {
                this.bind(acquire, iterator.next());
                array[n] = acquire.executeInsert();
                ++n;
            }
            return array;
        }
        finally {
            this.release(acquire);
        }
    }
    
    public final Long[] insertAndReturnIdsArrayBox(final T[] array) {
        final SupportSQLiteStatement acquire = this.acquire();
        try {
            final Long[] array2 = new Long[array.length];
            final int length = array.length;
            int i = 0;
            int n = 0;
            while (i < length) {
                this.bind(acquire, array[i]);
                array2[n] = acquire.executeInsert();
                ++n;
                ++i;
            }
            return array2;
        }
        finally {
            this.release(acquire);
        }
    }
    
    public final List<Long> insertAndReturnIdsList(final Collection<? extends T> collection) {
        final SupportSQLiteStatement acquire = this.acquire();
        try {
            final ArrayList list = new ArrayList(collection.size());
            final Iterator iterator = collection.iterator();
            int n = 0;
            while (iterator.hasNext()) {
                this.bind(acquire, iterator.next());
                list.add(n, acquire.executeInsert());
                ++n;
            }
            return list;
        }
        finally {
            this.release(acquire);
        }
    }
    
    public final List<Long> insertAndReturnIdsList(final T[] array) {
        final SupportSQLiteStatement acquire = this.acquire();
        try {
            final ArrayList list = new ArrayList(array.length);
            final int length = array.length;
            int i = 0;
            int n = 0;
            while (i < length) {
                this.bind(acquire, array[i]);
                list.add(n, acquire.executeInsert());
                ++n;
                ++i;
            }
            return list;
        }
        finally {
            this.release(acquire);
        }
    }
}

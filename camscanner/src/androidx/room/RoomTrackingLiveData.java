// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.util.concurrent.Executor;
import android.annotation.SuppressLint;
import androidx.arch.core.executor.ArchTaskExecutor;
import androidx.annotation.NonNull;
import java.util.Set;
import androidx.annotation.MainThread;
import androidx.annotation.WorkerThread;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.Callable;
import androidx.lifecycle.LiveData;

class RoomTrackingLiveData<T> extends LiveData<T>
{
    final Callable<T> mComputeFunction;
    final AtomicBoolean mComputing;
    private final InvalidationLiveDataContainer mContainer;
    final RoomDatabase mDatabase;
    final boolean mInTransaction;
    final AtomicBoolean mInvalid;
    final Runnable mInvalidationRunnable;
    final InvalidationTracker.Observer mObserver;
    final Runnable mRefreshRunnable;
    final AtomicBoolean mRegisteredObserver;
    
    @SuppressLint({ "RestrictedApi" })
    RoomTrackingLiveData(final RoomDatabase mDatabase, final InvalidationLiveDataContainer mContainer, final boolean mInTransaction, final Callable<T> mComputeFunction, final String[] array) {
        this.mInvalid = new AtomicBoolean(true);
        this.mComputing = new AtomicBoolean(false);
        this.mRegisteredObserver = new AtomicBoolean(false);
        this.mRefreshRunnable = new Runnable() {
            final RoomTrackingLiveData this$0;
            
            @WorkerThread
            @Override
            public void run() {
                if (this.this$0.mRegisteredObserver.compareAndSet(false, true)) {
                    this.this$0.mDatabase.getInvalidationTracker().addWeakObserver(this.this$0.mObserver);
                }
                int n;
                do {
                    if (this.this$0.mComputing.compareAndSet(false, true)) {
                        Object call = null;
                        n = 0;
                        try {
                            while (this.this$0.mInvalid.compareAndSet(true, false)) {
                                try {
                                    call = this.this$0.mComputeFunction.call();
                                    n = 1;
                                    continue;
                                }
                                catch (final Exception cause) {
                                    throw new RuntimeException("Exception while computing database live data.", cause);
                                }
                                break;
                            }
                            if (n == 0) {
                                continue;
                            }
                            this.this$0.postValue(call);
                            continue;
                        }
                        finally {
                            this.this$0.mComputing.set(false);
                        }
                    }
                    n = 0;
                } while (n != 0 && this.this$0.mInvalid.get());
            }
        };
        this.mInvalidationRunnable = new Runnable() {
            final RoomTrackingLiveData this$0;
            
            @MainThread
            @Override
            public void run() {
                final boolean hasActiveObservers = this.this$0.hasActiveObservers();
                if (this.this$0.mInvalid.compareAndSet(false, true) && hasActiveObservers) {
                    this.this$0.getQueryExecutor().execute(this.this$0.mRefreshRunnable);
                }
            }
        };
        this.mDatabase = mDatabase;
        this.mInTransaction = mInTransaction;
        this.mComputeFunction = mComputeFunction;
        this.mContainer = mContainer;
        this.mObserver = new InvalidationTracker.Observer(this, array) {
            final RoomTrackingLiveData this$0;
            
            @Override
            public void onInvalidated(@NonNull final Set<String> set) {
                ArchTaskExecutor.getInstance().executeOnMainThread(this.this$0.mInvalidationRunnable);
            }
        };
    }
    
    Executor getQueryExecutor() {
        if (this.mInTransaction) {
            return this.mDatabase.getTransactionExecutor();
        }
        return this.mDatabase.getQueryExecutor();
    }
    
    @Override
    protected void onActive() {
        super.onActive();
        this.mContainer.onActive(this);
        this.getQueryExecutor().execute(this.mRefreshRunnable);
    }
    
    @Override
    protected void onInactive() {
        super.onInactive();
        this.mContainer.onInactive(this);
    }
}

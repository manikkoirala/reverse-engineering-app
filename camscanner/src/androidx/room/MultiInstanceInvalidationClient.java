// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import androidx.annotation.NonNull;
import java.util.Set;
import android.os.RemoteException;
import android.os.IBinder;
import android.content.ComponentName;
import android.content.Intent;
import java.util.concurrent.atomic.AtomicBoolean;
import android.content.ServiceConnection;
import androidx.annotation.Nullable;
import java.util.concurrent.Executor;
import android.content.Context;

class MultiInstanceInvalidationClient
{
    final Context mAppContext;
    final IMultiInstanceInvalidationCallback mCallback;
    int mClientId;
    final Executor mExecutor;
    final InvalidationTracker mInvalidationTracker;
    final String mName;
    final InvalidationTracker.Observer mObserver;
    final Runnable mRemoveObserverRunnable;
    @Nullable
    IMultiInstanceInvalidationService mService;
    final ServiceConnection mServiceConnection;
    final Runnable mSetUpRunnable;
    final AtomicBoolean mStopped;
    
    MultiInstanceInvalidationClient(Context applicationContext, final String mName, final Intent intent, final InvalidationTracker mInvalidationTracker, final Executor mExecutor) {
        this.mCallback = new IMultiInstanceInvalidationCallback.Stub() {
            final MultiInstanceInvalidationClient this$0;
            
            public void onInvalidation(final String[] array) {
                this.this$0.mExecutor.execute(new Runnable(this, array) {
                    final MultiInstanceInvalidationClient$1 this$1;
                    final String[] val$tables;
                    
                    @Override
                    public void run() {
                        this.this$1.this$0.mInvalidationTracker.notifyObserversByTableNames(this.val$tables);
                    }
                });
            }
        };
        this.mStopped = new AtomicBoolean(false);
        final ServiceConnection mServiceConnection = (ServiceConnection)new ServiceConnection() {
            final MultiInstanceInvalidationClient this$0;
            
            public void onServiceConnected(final ComponentName componentName, final IBinder binder) {
                this.this$0.mService = IMultiInstanceInvalidationService.Stub.asInterface(binder);
                final MultiInstanceInvalidationClient this$0 = this.this$0;
                this$0.mExecutor.execute(this$0.mSetUpRunnable);
            }
            
            public void onServiceDisconnected(final ComponentName componentName) {
                final MultiInstanceInvalidationClient this$0 = this.this$0;
                this$0.mExecutor.execute(this$0.mRemoveObserverRunnable);
                this.this$0.mService = null;
            }
        };
        this.mServiceConnection = (ServiceConnection)mServiceConnection;
        this.mSetUpRunnable = new Runnable() {
            final MultiInstanceInvalidationClient this$0;
            
            @Override
            public void run() {
                try {
                    final MultiInstanceInvalidationClient this$0 = this.this$0;
                    final IMultiInstanceInvalidationService mService = this$0.mService;
                    if (mService != null) {
                        this$0.mClientId = mService.registerCallback(this$0.mCallback, this$0.mName);
                        final MultiInstanceInvalidationClient this$2 = this.this$0;
                        this$2.mInvalidationTracker.addObserver(this$2.mObserver);
                    }
                }
                catch (final RemoteException ex) {}
            }
        };
        this.mRemoveObserverRunnable = new Runnable() {
            final MultiInstanceInvalidationClient this$0;
            
            @Override
            public void run() {
                final MultiInstanceInvalidationClient this$0 = this.this$0;
                this$0.mInvalidationTracker.removeObserver(this$0.mObserver);
            }
        };
        applicationContext = applicationContext.getApplicationContext();
        this.mAppContext = applicationContext;
        this.mName = mName;
        this.mInvalidationTracker = mInvalidationTracker;
        this.mExecutor = mExecutor;
        this.mObserver = new InvalidationTracker.Observer(this, mInvalidationTracker.mTableIdLookup.keySet().toArray(new String[0])) {
            final MultiInstanceInvalidationClient this$0;
            
            @Override
            boolean isRemote() {
                return true;
            }
            
            @Override
            public void onInvalidated(@NonNull final Set<String> set) {
                if (this.this$0.mStopped.get()) {
                    return;
                }
                try {
                    final MultiInstanceInvalidationClient this$0 = this.this$0;
                    final IMultiInstanceInvalidationService mService = this$0.mService;
                    if (mService != null) {
                        mService.broadcastInvalidation(this$0.mClientId, set.toArray(new String[0]));
                    }
                }
                catch (final RemoteException ex) {}
            }
        };
        applicationContext.bindService(intent, (ServiceConnection)mServiceConnection, 1);
    }
    
    void stop() {
        if (!this.mStopped.compareAndSet(false, true)) {
            return;
        }
        this.mInvalidationTracker.removeObserver(this.mObserver);
        while (true) {
            try {
                final IMultiInstanceInvalidationService mService = this.mService;
                if (mService != null) {
                    mService.unregisterCallback(this.mCallback, this.mClientId);
                }
                this.mAppContext.unbindService(this.mServiceConnection);
            }
            catch (final RemoteException ex) {
                continue;
            }
            break;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.CLASS)
@Target({ ElementType.FIELD, ElementType.METHOD })
public @interface Relation {
    Junction associateBy() default @Junction(Object.class);
    
    Class<?> entity() default Object.class;
    
    String entityColumn();
    
    String parentColumn();
    
    String[] projection() default {};
}

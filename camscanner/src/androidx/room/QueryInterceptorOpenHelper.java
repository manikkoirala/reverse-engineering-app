// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import androidx.annotation.RequiresApi;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

final class QueryInterceptorOpenHelper implements SupportSQLiteOpenHelper, DelegatingOpenHelper
{
    private final SupportSQLiteOpenHelper mDelegate;
    private final RoomDatabase.QueryCallback mQueryCallback;
    private final Executor mQueryCallbackExecutor;
    
    QueryInterceptorOpenHelper(@NonNull final SupportSQLiteOpenHelper mDelegate, @NonNull final RoomDatabase.QueryCallback mQueryCallback, @NonNull final Executor mQueryCallbackExecutor) {
        this.mDelegate = mDelegate;
        this.mQueryCallback = mQueryCallback;
        this.mQueryCallbackExecutor = mQueryCallbackExecutor;
    }
    
    @Override
    public void close() {
        this.mDelegate.close();
    }
    
    @Nullable
    @Override
    public String getDatabaseName() {
        return this.mDelegate.getDatabaseName();
    }
    
    @NonNull
    @Override
    public SupportSQLiteOpenHelper getDelegate() {
        return this.mDelegate;
    }
    
    @Override
    public SupportSQLiteDatabase getReadableDatabase() {
        return new QueryInterceptorDatabase(this.mDelegate.getReadableDatabase(), this.mQueryCallback, this.mQueryCallbackExecutor);
    }
    
    @Override
    public SupportSQLiteDatabase getWritableDatabase() {
        return new QueryInterceptorDatabase(this.mDelegate.getWritableDatabase(), this.mQueryCallback, this.mQueryCallbackExecutor);
    }
    
    @RequiresApi(api = 16)
    @Override
    public void setWriteAheadLoggingEnabled(final boolean writeAheadLoggingEnabled) {
        this.mDelegate.setWriteAheadLoggingEnabled(writeAheadLoggingEnabled);
    }
}

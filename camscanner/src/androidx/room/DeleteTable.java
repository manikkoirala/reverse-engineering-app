// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.lang.annotation.Repeatable;

@Repeatable(Entries.class)
@Retention(RetentionPolicy.CLASS)
@Target({ ElementType.TYPE })
public @interface DeleteTable {
    String tableName();
    
    @Retention(RetentionPolicy.CLASS)
    @Target({ ElementType.TYPE })
    public @interface Entries {
        DeleteTable[] value();
    }
}

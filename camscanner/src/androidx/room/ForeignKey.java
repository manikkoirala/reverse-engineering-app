// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.CLASS)
@Target({})
public @interface ForeignKey {
    public static final int CASCADE = 5;
    public static final int NO_ACTION = 1;
    public static final int RESTRICT = 2;
    public static final int SET_DEFAULT = 4;
    public static final int SET_NULL = 3;
    
    String[] childColumns();
    
    boolean deferred() default false;
    
    Class<?> entity();
    
    @Action
    int onDelete() default 1;
    
    @Action
    int onUpdate() default 1;
    
    String[] parentColumns();
    
    @Retention(RetentionPolicy.CLASS)
    public @interface Action {
    }
}

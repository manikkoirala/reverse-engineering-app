// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.io.IOException;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;
import java.util.List;
import androidx.sqlite.db.SupportSQLiteStatement;

final class QueryInterceptorStatement implements SupportSQLiteStatement
{
    private final List<Object> mBindArgsCache;
    private final SupportSQLiteStatement mDelegate;
    private final RoomDatabase.QueryCallback mQueryCallback;
    private final Executor mQueryCallbackExecutor;
    private final String mSqlStatement;
    
    QueryInterceptorStatement(@NonNull final SupportSQLiteStatement mDelegate, @NonNull final RoomDatabase.QueryCallback mQueryCallback, final String mSqlStatement, @NonNull final Executor mQueryCallbackExecutor) {
        this.mBindArgsCache = new ArrayList<Object>();
        this.mDelegate = mDelegate;
        this.mQueryCallback = mQueryCallback;
        this.mSqlStatement = mSqlStatement;
        this.mQueryCallbackExecutor = mQueryCallbackExecutor;
    }
    
    private void saveArgsToCache(int i, final Object o) {
        final int n = i - 1;
        if (n >= this.mBindArgsCache.size()) {
            for (i = this.mBindArgsCache.size(); i <= n; ++i) {
                this.mBindArgsCache.add(null);
            }
        }
        this.mBindArgsCache.set(n, o);
    }
    
    @Override
    public void bindBlob(final int n, final byte[] array) {
        this.saveArgsToCache(n, array);
        this.mDelegate.bindBlob(n, array);
    }
    
    @Override
    public void bindDouble(final int n, final double d) {
        this.saveArgsToCache(n, d);
        this.mDelegate.bindDouble(n, d);
    }
    
    @Override
    public void bindLong(final int n, final long l) {
        this.saveArgsToCache(n, l);
        this.mDelegate.bindLong(n, l);
    }
    
    @Override
    public void bindNull(final int n) {
        this.saveArgsToCache(n, this.mBindArgsCache.toArray());
        this.mDelegate.bindNull(n);
    }
    
    @Override
    public void bindString(final int n, final String s) {
        this.saveArgsToCache(n, s);
        this.mDelegate.bindString(n, s);
    }
    
    @Override
    public void clearBindings() {
        this.mBindArgsCache.clear();
        this.mDelegate.clearBindings();
    }
    
    @Override
    public void close() throws IOException {
        this.mDelegate.close();
    }
    
    @Override
    public void execute() {
        this.mQueryCallbackExecutor.execute(new O08000(this));
        this.mDelegate.execute();
    }
    
    @Override
    public long executeInsert() {
        this.mQueryCallbackExecutor.execute(new \u30078\u30070\u3007o\u3007O(this));
        return this.mDelegate.executeInsert();
    }
    
    @Override
    public int executeUpdateDelete() {
        this.mQueryCallbackExecutor.execute(new \u30078(this));
        return this.mDelegate.executeUpdateDelete();
    }
    
    @Override
    public long simpleQueryForLong() {
        this.mQueryCallbackExecutor.execute(new o8oO\u3007(this));
        return this.mDelegate.simpleQueryForLong();
    }
    
    @Override
    public String simpleQueryForString() {
        this.mQueryCallbackExecutor.execute(new O\u3007O\u3007oO(this));
        return this.mDelegate.simpleQueryForString();
    }
}

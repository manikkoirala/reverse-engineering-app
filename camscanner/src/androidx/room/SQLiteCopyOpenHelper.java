// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import androidx.annotation.RequiresApi;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.framework.FrameworkSQLiteOpenHelperFactory;
import java.nio.channels.ReadableByteChannel;
import java.io.IOException;
import androidx.room.util.FileUtil;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.nio.channels.Channels;
import java.io.InputStream;
import java.util.concurrent.Callable;
import java.io.File;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

class SQLiteCopyOpenHelper implements SupportSQLiteOpenHelper, DelegatingOpenHelper
{
    @NonNull
    private final Context mContext;
    @Nullable
    private final String mCopyFromAssetPath;
    @Nullable
    private final File mCopyFromFile;
    @Nullable
    private final Callable<InputStream> mCopyFromInputStream;
    @Nullable
    private DatabaseConfiguration mDatabaseConfiguration;
    private final int mDatabaseVersion;
    @NonNull
    private final SupportSQLiteOpenHelper mDelegate;
    private boolean mVerified;
    
    SQLiteCopyOpenHelper(@NonNull final Context mContext, @Nullable final String mCopyFromAssetPath, @Nullable final File mCopyFromFile, @Nullable final Callable<InputStream> mCopyFromInputStream, final int mDatabaseVersion, @NonNull final SupportSQLiteOpenHelper mDelegate) {
        this.mContext = mContext;
        this.mCopyFromAssetPath = mCopyFromAssetPath;
        this.mCopyFromFile = mCopyFromFile;
        this.mCopyFromInputStream = mCopyFromInputStream;
        this.mDatabaseVersion = mDatabaseVersion;
        this.mDelegate = mDelegate;
    }
    
    private void copyDatabaseFile(final File dest, final boolean b) throws IOException {
        Label_0077: {
            if (this.mCopyFromAssetPath != null) {
                final ReadableByteChannel readableByteChannel = Channels.newChannel(this.mContext.getAssets().open(this.mCopyFromAssetPath));
                break Label_0077;
            }
            if (this.mCopyFromFile != null) {
                final ReadableByteChannel readableByteChannel = new FileInputStream(this.mCopyFromFile).getChannel();
                break Label_0077;
            }
            final Callable<InputStream> mCopyFromInputStream = this.mCopyFromInputStream;
            if (mCopyFromInputStream == null) {
                throw new IllegalStateException("copyFromAssetPath, copyFromFile and copyFromInputStream are all null!");
            }
            try {
                final ReadableByteChannel readableByteChannel = Channels.newChannel(mCopyFromInputStream.call());
                final File tempFile = File.createTempFile("room-copy-helper", ".tmp", this.mContext.getCacheDir());
                tempFile.deleteOnExit();
                FileUtil.copy(readableByteChannel, new FileOutputStream(tempFile).getChannel());
                final File parentFile = dest.getParentFile();
                if (parentFile != null && !parentFile.exists() && !parentFile.mkdirs()) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to create directories for ");
                    sb.append(dest.getAbsolutePath());
                    throw new IOException(sb.toString());
                }
                this.dispatchOnOpenPrepackagedDatabase(tempFile, b);
                if (tempFile.renameTo(dest)) {
                    return;
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Failed to move intermediate file (");
                sb2.append(tempFile.getAbsolutePath());
                sb2.append(") to destination (");
                sb2.append(dest.getAbsolutePath());
                sb2.append(").");
                throw new IOException(sb2.toString());
            }
            catch (final Exception cause) {
                throw new IOException("inputStreamCallable exception on call", cause);
            }
        }
        throw new IllegalStateException("copyFromAssetPath, copyFromFile and copyFromInputStream are all null!");
    }
    
    private SupportSQLiteOpenHelper createFrameworkOpenHelper(final File file) {
        try {
            return new FrameworkSQLiteOpenHelperFactory().create(Configuration.builder(this.mContext).name(file.getAbsolutePath()).callback(new Callback(this, Math.max(DBUtil.readVersion(file), 1)) {
                final SQLiteCopyOpenHelper this$0;
                
                @Override
                public void onCreate(@NonNull final SupportSQLiteDatabase supportSQLiteDatabase) {
                }
                
                @Override
                public void onOpen(@NonNull final SupportSQLiteDatabase supportSQLiteDatabase) {
                    final int version = super.version;
                    if (version < 1) {
                        supportSQLiteDatabase.setVersion(version);
                    }
                }
                
                @Override
                public void onUpgrade(@NonNull final SupportSQLiteDatabase supportSQLiteDatabase, final int n, final int n2) {
                }
            }).build());
        }
        catch (final IOException cause) {
            throw new RuntimeException("Malformed database file, unable to read version.", cause);
        }
    }
    
    private void dispatchOnOpenPrepackagedDatabase(final File file, final boolean b) {
        final DatabaseConfiguration mDatabaseConfiguration = this.mDatabaseConfiguration;
        if (mDatabaseConfiguration != null) {
            if (mDatabaseConfiguration.prepackagedDatabaseCallback != null) {
                final SupportSQLiteOpenHelper frameworkOpenHelper = this.createFrameworkOpenHelper(file);
                Label_0039: {
                    if (!b) {
                        break Label_0039;
                    }
                    try {
                        SupportSQLiteDatabase supportSQLiteDatabase = frameworkOpenHelper.getWritableDatabase();
                        while (true) {
                            this.mDatabaseConfiguration.prepackagedDatabaseCallback.onOpenPrepackagedDatabase(supportSQLiteDatabase);
                            return;
                            supportSQLiteDatabase = frameworkOpenHelper.getReadableDatabase();
                            continue;
                        }
                    }
                    finally {
                        frameworkOpenHelper.close();
                    }
                }
            }
        }
    }
    
    private void verifyDatabaseFile(final boolean p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokevirtual   androidx/room/SQLiteCopyOpenHelper.getDatabaseName:()Ljava/lang/String;
        //     4: astore          6
        //     6: aload_0        
        //     7: getfield        androidx/room/SQLiteCopyOpenHelper.mContext:Landroid/content/Context;
        //    10: aload           6
        //    12: invokevirtual   android/content/Context.getDatabasePath:(Ljava/lang/String;)Ljava/io/File;
        //    15: astore          7
        //    17: aload_0        
        //    18: getfield        androidx/room/SQLiteCopyOpenHelper.mDatabaseConfiguration:Landroidx/room/DatabaseConfiguration;
        //    21: astore          5
        //    23: aload           5
        //    25: ifnull          45
        //    28: aload           5
        //    30: getfield        androidx/room/DatabaseConfiguration.multiInstanceInvalidation:Z
        //    33: ifeq            39
        //    36: goto            45
        //    39: iconst_0       
        //    40: istore          4
        //    42: goto            48
        //    45: iconst_1       
        //    46: istore          4
        //    48: new             Landroidx/room/util/CopyLock;
        //    51: dup            
        //    52: aload           6
        //    54: aload_0        
        //    55: getfield        androidx/room/SQLiteCopyOpenHelper.mContext:Landroid/content/Context;
        //    58: invokevirtual   android/content/Context.getFilesDir:()Ljava/io/File;
        //    61: iload           4
        //    63: invokespecial   androidx/room/util/CopyLock.<init>:(Ljava/lang/String;Ljava/io/File;Z)V
        //    66: astore          5
        //    68: aload           5
        //    70: invokevirtual   androidx/room/util/CopyLock.lock:()V
        //    73: aload           7
        //    75: invokevirtual   java/io/File.exists:()Z
        //    78: istore          4
        //    80: iload           4
        //    82: ifne            118
        //    85: aload_0        
        //    86: aload           7
        //    88: iload_1        
        //    89: invokespecial   androidx/room/SQLiteCopyOpenHelper.copyDatabaseFile:(Ljava/io/File;Z)V
        //    92: aload           5
        //    94: invokevirtual   androidx/room/util/CopyLock.unlock:()V
        //    97: return         
        //    98: astore          6
        //   100: new             Ljava/lang/RuntimeException;
        //   103: astore          7
        //   105: aload           7
        //   107: ldc_w           "Unable to copy database file."
        //   110: aload           6
        //   112: invokespecial   java/lang/RuntimeException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   115: aload           7
        //   117: athrow         
        //   118: aload_0        
        //   119: getfield        androidx/room/SQLiteCopyOpenHelper.mDatabaseConfiguration:Landroidx/room/DatabaseConfiguration;
        //   122: astore          8
        //   124: aload           8
        //   126: ifnonnull       135
        //   129: aload           5
        //   131: invokevirtual   androidx/room/util/CopyLock.unlock:()V
        //   134: return         
        //   135: aload           7
        //   137: invokestatic    androidx/room/util/DBUtil.readVersion:(Ljava/io/File;)I
        //   140: istore_3       
        //   141: aload_0        
        //   142: getfield        androidx/room/SQLiteCopyOpenHelper.mDatabaseVersion:I
        //   145: istore_2       
        //   146: iload_3        
        //   147: iload_2        
        //   148: if_icmpne       157
        //   151: aload           5
        //   153: invokevirtual   androidx/room/util/CopyLock.unlock:()V
        //   156: return         
        //   157: aload_0        
        //   158: getfield        androidx/room/SQLiteCopyOpenHelper.mDatabaseConfiguration:Landroidx/room/DatabaseConfiguration;
        //   161: iload_3        
        //   162: iload_2        
        //   163: invokevirtual   androidx/room/DatabaseConfiguration.isMigrationRequired:(II)Z
        //   166: istore          4
        //   168: iload           4
        //   170: ifeq            179
        //   173: aload           5
        //   175: invokevirtual   androidx/room/util/CopyLock.unlock:()V
        //   178: return         
        //   179: aload_0        
        //   180: getfield        androidx/room/SQLiteCopyOpenHelper.mContext:Landroid/content/Context;
        //   183: aload           6
        //   185: invokevirtual   android/content/Context.deleteDatabase:(Ljava/lang/String;)Z
        //   188: istore          4
        //   190: iload           4
        //   192: ifeq            205
        //   195: aload_0        
        //   196: aload           7
        //   198: iload_1        
        //   199: invokespecial   androidx/room/SQLiteCopyOpenHelper.copyDatabaseFile:(Ljava/io/File;Z)V
        //   202: goto            241
        //   205: new             Ljava/lang/StringBuilder;
        //   208: astore          7
        //   210: aload           7
        //   212: invokespecial   java/lang/StringBuilder.<init>:()V
        //   215: aload           7
        //   217: ldc_w           "Failed to delete database file ("
        //   220: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   223: pop            
        //   224: aload           7
        //   226: aload           6
        //   228: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   231: pop            
        //   232: aload           7
        //   234: ldc_w           ") for a copy destructive migration."
        //   237: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   240: pop            
        //   241: aload           5
        //   243: invokevirtual   androidx/room/util/CopyLock.unlock:()V
        //   246: return         
        //   247: astore          6
        //   249: aload           5
        //   251: invokevirtual   androidx/room/util/CopyLock.unlock:()V
        //   254: return         
        //   255: astore          6
        //   257: aload           5
        //   259: invokevirtual   androidx/room/util/CopyLock.unlock:()V
        //   262: aload           6
        //   264: athrow         
        //   265: astore          6
        //   267: goto            241
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  68     80     255    265    Any
        //  85     92     98     118    Ljava/io/IOException;
        //  85     92     255    265    Any
        //  100    118    255    265    Any
        //  118    124    255    265    Any
        //  135    141    247    255    Ljava/io/IOException;
        //  135    141    255    265    Any
        //  141    146    255    265    Any
        //  157    168    255    265    Any
        //  179    190    255    265    Any
        //  195    202    265    270    Ljava/io/IOException;
        //  195    202    255    265    Any
        //  205    241    255    265    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0205:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public void close() {
        synchronized (this) {
            this.mDelegate.close();
            this.mVerified = false;
        }
    }
    
    @Override
    public String getDatabaseName() {
        return this.mDelegate.getDatabaseName();
    }
    
    @NonNull
    @Override
    public SupportSQLiteOpenHelper getDelegate() {
        return this.mDelegate;
    }
    
    @Override
    public SupportSQLiteDatabase getReadableDatabase() {
        synchronized (this) {
            if (!this.mVerified) {
                this.verifyDatabaseFile(false);
                this.mVerified = true;
            }
            return this.mDelegate.getReadableDatabase();
        }
    }
    
    @Override
    public SupportSQLiteDatabase getWritableDatabase() {
        synchronized (this) {
            if (!this.mVerified) {
                this.verifyDatabaseFile(true);
                this.mVerified = true;
            }
            return this.mDelegate.getWritableDatabase();
        }
    }
    
    void setDatabaseConfiguration(@Nullable final DatabaseConfiguration mDatabaseConfiguration) {
        this.mDatabaseConfiguration = mDatabaseConfiguration;
    }
    
    @RequiresApi(api = 16)
    @Override
    public void setWriteAheadLoggingEnabled(final boolean writeAheadLoggingEnabled) {
        this.mDelegate.setWriteAheadLoggingEnabled(writeAheadLoggingEnabled);
    }
}

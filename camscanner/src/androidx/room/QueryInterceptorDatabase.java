// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.util.Locale;
import android.os.CancellationSignal;
import androidx.sqlite.db.SupportSQLiteProgram;
import android.database.Cursor;
import android.content.ContentValues;
import android.util.Pair;
import java.util.Collection;
import java.util.Arrays;
import android.database.SQLException;
import androidx.sqlite.db.O8;
import androidx.annotation.RequiresApi;
import androidx.sqlite.db.SupportSQLiteStatement;
import java.io.IOException;
import android.database.sqlite.SQLiteTransactionListener;
import androidx.sqlite.db.SupportSQLiteQuery;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;
import androidx.sqlite.db.SupportSQLiteDatabase;

final class QueryInterceptorDatabase implements SupportSQLiteDatabase
{
    private final SupportSQLiteDatabase mDelegate;
    private final RoomDatabase.QueryCallback mQueryCallback;
    private final Executor mQueryCallbackExecutor;
    
    QueryInterceptorDatabase(@NonNull final SupportSQLiteDatabase mDelegate, @NonNull final RoomDatabase.QueryCallback mQueryCallback, @NonNull final Executor mQueryCallbackExecutor) {
        this.mDelegate = mDelegate;
        this.mQueryCallback = mQueryCallback;
        this.mQueryCallbackExecutor = mQueryCallbackExecutor;
    }
    
    @Override
    public void beginTransaction() {
        this.mQueryCallbackExecutor.execute(new o\u30070OOo\u30070(this));
        this.mDelegate.beginTransaction();
    }
    
    @Override
    public void beginTransactionNonExclusive() {
        this.mQueryCallbackExecutor.execute(new oo\u3007(this));
        this.mDelegate.beginTransactionNonExclusive();
    }
    
    @Override
    public void beginTransactionWithListener(@NonNull final SQLiteTransactionListener sqLiteTransactionListener) {
        this.mQueryCallbackExecutor.execute(new \u300700\u30078(this));
        this.mDelegate.beginTransactionWithListener(sqLiteTransactionListener);
    }
    
    @Override
    public void beginTransactionWithListenerNonExclusive(@NonNull final SQLiteTransactionListener sqLiteTransactionListener) {
        this.mQueryCallbackExecutor.execute(new Oo8Oo00oo(this));
        this.mDelegate.beginTransactionWithListenerNonExclusive(sqLiteTransactionListener);
    }
    
    @Override
    public void close() throws IOException {
        this.mDelegate.close();
    }
    
    @NonNull
    @Override
    public SupportSQLiteStatement compileStatement(@NonNull final String s) {
        return new QueryInterceptorStatement(this.mDelegate.compileStatement(s), this.mQueryCallback, s, this.mQueryCallbackExecutor);
    }
    
    @Override
    public int delete(@NonNull final String s, @NonNull final String s2, @NonNull final Object[] array) {
        return this.mDelegate.delete(s, s2, array);
    }
    
    @RequiresApi(api = 16)
    @Override
    public void disableWriteAheadLogging() {
        this.mDelegate.disableWriteAheadLogging();
    }
    
    @Override
    public boolean enableWriteAheadLogging() {
        return this.mDelegate.enableWriteAheadLogging();
    }
    
    @Override
    public void endTransaction() {
        this.mQueryCallbackExecutor.execute(new \u300708O8o\u30070(this));
        this.mDelegate.endTransaction();
    }
    
    @Override
    public void execSQL(@NonNull final String s) throws SQLException {
        this.mQueryCallbackExecutor.execute(new O8\u3007o(this, s));
        this.mDelegate.execSQL(s);
    }
    
    @Override
    public void execSQL(@NonNull final String s, @NonNull final Object[] a) throws SQLException {
        final ArrayList list = new ArrayList();
        list.addAll(Arrays.asList(a));
        this.mQueryCallbackExecutor.execute(new oO(this, s, list));
        this.mDelegate.execSQL(s, list.toArray());
    }
    
    @NonNull
    @Override
    public List<Pair<String, String>> getAttachedDbs() {
        return this.mDelegate.getAttachedDbs();
    }
    
    @Override
    public long getMaximumSize() {
        return this.mDelegate.getMaximumSize();
    }
    
    @Override
    public long getPageSize() {
        return this.mDelegate.getPageSize();
    }
    
    @NonNull
    @Override
    public String getPath() {
        return this.mDelegate.getPath();
    }
    
    @Override
    public int getVersion() {
        return this.mDelegate.getVersion();
    }
    
    @Override
    public boolean inTransaction() {
        return this.mDelegate.inTransaction();
    }
    
    @Override
    public long insert(@NonNull final String s, final int n, @NonNull final ContentValues contentValues) throws SQLException {
        return this.mDelegate.insert(s, n, contentValues);
    }
    
    @Override
    public boolean isDatabaseIntegrityOk() {
        return this.mDelegate.isDatabaseIntegrityOk();
    }
    
    @Override
    public boolean isDbLockedByCurrentThread() {
        return this.mDelegate.isDbLockedByCurrentThread();
    }
    
    @Override
    public boolean isOpen() {
        return this.mDelegate.isOpen();
    }
    
    @Override
    public boolean isReadOnly() {
        return this.mDelegate.isReadOnly();
    }
    
    @RequiresApi(api = 16)
    @Override
    public boolean isWriteAheadLoggingEnabled() {
        return this.mDelegate.isWriteAheadLoggingEnabled();
    }
    
    @Override
    public boolean needUpgrade(final int n) {
        return this.mDelegate.needUpgrade(n);
    }
    
    @NonNull
    @Override
    public Cursor query(@NonNull final SupportSQLiteQuery supportSQLiteQuery) {
        final QueryInterceptorProgram queryInterceptorProgram = new QueryInterceptorProgram();
        supportSQLiteQuery.bindTo(queryInterceptorProgram);
        this.mQueryCallbackExecutor.execute(new \u3007\u3007\u30070\u3007\u30070(this, supportSQLiteQuery, queryInterceptorProgram));
        return this.mDelegate.query(supportSQLiteQuery);
    }
    
    @NonNull
    @Override
    public Cursor query(@NonNull final SupportSQLiteQuery supportSQLiteQuery, @NonNull final CancellationSignal cancellationSignal) {
        final QueryInterceptorProgram queryInterceptorProgram = new QueryInterceptorProgram();
        supportSQLiteQuery.bindTo(queryInterceptorProgram);
        this.mQueryCallbackExecutor.execute(new \u3007o(this, supportSQLiteQuery, queryInterceptorProgram));
        return this.mDelegate.query(supportSQLiteQuery);
    }
    
    @NonNull
    @Override
    public Cursor query(@NonNull final String s) {
        this.mQueryCallbackExecutor.execute(new \u3007\u30070o(this, s));
        return this.mDelegate.query(s);
    }
    
    @NonNull
    @Override
    public Cursor query(@NonNull final String s, @NonNull final Object[] a) {
        final ArrayList list = new ArrayList();
        list.addAll(Arrays.asList(a));
        this.mQueryCallbackExecutor.execute(new o0ooO(this, s, list));
        return this.mDelegate.query(s, a);
    }
    
    @RequiresApi(api = 16)
    @Override
    public void setForeignKeyConstraintsEnabled(final boolean foreignKeyConstraintsEnabled) {
        this.mDelegate.setForeignKeyConstraintsEnabled(foreignKeyConstraintsEnabled);
    }
    
    @Override
    public void setLocale(@NonNull final Locale locale) {
        this.mDelegate.setLocale(locale);
    }
    
    @Override
    public void setMaxSqlCacheSize(final int maxSqlCacheSize) {
        this.mDelegate.setMaxSqlCacheSize(maxSqlCacheSize);
    }
    
    @Override
    public long setMaximumSize(final long maximumSize) {
        return this.mDelegate.setMaximumSize(maximumSize);
    }
    
    @Override
    public void setPageSize(final long pageSize) {
        this.mDelegate.setPageSize(pageSize);
    }
    
    @Override
    public void setTransactionSuccessful() {
        this.mQueryCallbackExecutor.execute(new o\u30078(this));
        this.mDelegate.setTransactionSuccessful();
    }
    
    @Override
    public void setVersion(final int version) {
        this.mDelegate.setVersion(version);
    }
    
    @Override
    public int update(@NonNull final String s, final int n, @NonNull final ContentValues contentValues, @NonNull final String s2, @NonNull final Object[] array) {
        return this.mDelegate.update(s, n, contentValues, s2, array);
    }
    
    @Override
    public boolean yieldIfContendedSafely() {
        return this.mDelegate.yieldIfContendedSafely();
    }
    
    @Override
    public boolean yieldIfContendedSafely(final long n) {
        return this.mDelegate.yieldIfContendedSafely(n);
    }
}

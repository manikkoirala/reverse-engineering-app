// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import androidx.annotation.Nullable;
import android.os.IBinder;
import androidx.annotation.NonNull;
import android.content.Intent;
import android.os.IInterface;
import java.util.HashMap;
import android.os.RemoteCallbackList;
import android.app.Service;

@ExperimentalRoomApi
public class MultiInstanceInvalidationService extends Service
{
    private final IMultiInstanceInvalidationService.Stub mBinder;
    final RemoteCallbackList<IMultiInstanceInvalidationCallback> mCallbackList;
    final HashMap<Integer, String> mClientNames;
    int mMaxClientId;
    
    public MultiInstanceInvalidationService() {
        this.mMaxClientId = 0;
        this.mClientNames = new HashMap<Integer, String>();
        this.mCallbackList = new RemoteCallbackList<IMultiInstanceInvalidationCallback>() {
            final MultiInstanceInvalidationService this$0;
            
            public void onCallbackDied(final IMultiInstanceInvalidationCallback multiInstanceInvalidationCallback, final Object o) {
                this.this$0.mClientNames.remove((int)o);
            }
        };
        this.mBinder = new IMultiInstanceInvalidationService.Stub() {
            final MultiInstanceInvalidationService this$0;
            
            public void broadcastInvalidation(final int p0, final String[] p1) {
                // 
                // This method could not be decompiled.
                // 
                // Original Bytecode:
                // 
                //     1: getfield        androidx/room/MultiInstanceInvalidationService$2.this$0:Landroidx/room/MultiInstanceInvalidationService;
                //     4: getfield        androidx/room/MultiInstanceInvalidationService.mCallbackList:Landroid/os/RemoteCallbackList;
                //     7: astore          7
                //     9: aload           7
                //    11: monitorenter   
                //    12: aload_0        
                //    13: getfield        androidx/room/MultiInstanceInvalidationService$2.this$0:Landroidx/room/MultiInstanceInvalidationService;
                //    16: getfield        androidx/room/MultiInstanceInvalidationService.mClientNames:Ljava/util/HashMap;
                //    19: iload_1        
                //    20: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
                //    23: invokevirtual   java/util/HashMap.get:(Ljava/lang/Object;)Ljava/lang/Object;
                //    26: checkcast       Ljava/lang/String;
                //    29: astore          8
                //    31: aload           8
                //    33: ifnonnull       40
                //    36: aload           7
                //    38: monitorexit    
                //    39: return         
                //    40: aload_0        
                //    41: getfield        androidx/room/MultiInstanceInvalidationService$2.this$0:Landroidx/room/MultiInstanceInvalidationService;
                //    44: getfield        androidx/room/MultiInstanceInvalidationService.mCallbackList:Landroid/os/RemoteCallbackList;
                //    47: invokevirtual   android/os/RemoteCallbackList.beginBroadcast:()I
                //    50: istore          4
                //    52: iconst_0       
                //    53: istore_3       
                //    54: iload_3        
                //    55: iload           4
                //    57: if_icmpge       161
                //    60: aload_0        
                //    61: getfield        androidx/room/MultiInstanceInvalidationService$2.this$0:Landroidx/room/MultiInstanceInvalidationService;
                //    64: getfield        androidx/room/MultiInstanceInvalidationService.mCallbackList:Landroid/os/RemoteCallbackList;
                //    67: iload_3        
                //    68: invokevirtual   android/os/RemoteCallbackList.getBroadcastCookie:(I)Ljava/lang/Object;
                //    71: checkcast       Ljava/lang/Integer;
                //    74: invokevirtual   java/lang/Integer.intValue:()I
                //    77: istore          5
                //    79: aload_0        
                //    80: getfield        androidx/room/MultiInstanceInvalidationService$2.this$0:Landroidx/room/MultiInstanceInvalidationService;
                //    83: getfield        androidx/room/MultiInstanceInvalidationService.mClientNames:Ljava/util/HashMap;
                //    86: iload           5
                //    88: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
                //    91: invokevirtual   java/util/HashMap.get:(Ljava/lang/Object;)Ljava/lang/Object;
                //    94: checkcast       Ljava/lang/String;
                //    97: astore          9
                //    99: iload_1        
                //   100: iload           5
                //   102: if_icmpeq       142
                //   105: aload           8
                //   107: aload           9
                //   109: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
                //   112: istore          6
                //   114: iload           6
                //   116: ifne            122
                //   119: goto            142
                //   122: aload_0        
                //   123: getfield        androidx/room/MultiInstanceInvalidationService$2.this$0:Landroidx/room/MultiInstanceInvalidationService;
                //   126: getfield        androidx/room/MultiInstanceInvalidationService.mCallbackList:Landroid/os/RemoteCallbackList;
                //   129: iload_3        
                //   130: invokevirtual   android/os/RemoteCallbackList.getBroadcastItem:(I)Landroid/os/IInterface;
                //   133: checkcast       Landroidx/room/IMultiInstanceInvalidationCallback;
                //   136: aload_2        
                //   137: invokeinterface androidx/room/IMultiInstanceInvalidationCallback.onInvalidation:([Ljava/lang/String;)V
                //   142: iinc            3, 1
                //   145: goto            54
                //   148: astore_2       
                //   149: aload_0        
                //   150: getfield        androidx/room/MultiInstanceInvalidationService$2.this$0:Landroidx/room/MultiInstanceInvalidationService;
                //   153: getfield        androidx/room/MultiInstanceInvalidationService.mCallbackList:Landroid/os/RemoteCallbackList;
                //   156: invokevirtual   android/os/RemoteCallbackList.finishBroadcast:()V
                //   159: aload_2        
                //   160: athrow         
                //   161: aload_0        
                //   162: getfield        androidx/room/MultiInstanceInvalidationService$2.this$0:Landroidx/room/MultiInstanceInvalidationService;
                //   165: getfield        androidx/room/MultiInstanceInvalidationService.mCallbackList:Landroid/os/RemoteCallbackList;
                //   168: invokevirtual   android/os/RemoteCallbackList.finishBroadcast:()V
                //   171: aload           7
                //   173: monitorexit    
                //   174: return         
                //   175: astore_2       
                //   176: aload           7
                //   178: monitorexit    
                //   179: aload_2        
                //   180: athrow         
                //   181: astore          9
                //   183: goto            142
                //    Exceptions:
                //  Try           Handler
                //  Start  End    Start  End    Type                        
                //  -----  -----  -----  -----  ----------------------------
                //  12     31     175    181    Any
                //  36     39     175    181    Any
                //  40     52     175    181    Any
                //  60     99     148    161    Any
                //  105    114    148    161    Any
                //  122    142    181    186    Landroid/os/RemoteException;
                //  122    142    148    161    Any
                //  149    161    175    181    Any
                //  161    174    175    181    Any
                //  176    179    175    181    Any
                // 
                // The error that occurred was:
                // 
                // java.lang.IllegalStateException: Expression is linked from several locations: Label_0122:
                //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
                //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
                //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
                //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformCall(AstMethodBodyBuilder.java:1151)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:993)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:534)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:548)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:534)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:377)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:318)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:213)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createConstructor(AstBuilder.java:799)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:635)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
                //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
                //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
                //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
                // 
                throw new IllegalStateException("An error occurred while decompiling this method.");
            }
            
            public int registerCallback(final IMultiInstanceInvalidationCallback multiInstanceInvalidationCallback, final String value) {
                if (value == null) {
                    return 0;
                }
                synchronized (this.this$0.mCallbackList) {
                    final MultiInstanceInvalidationService this$0 = this.this$0;
                    final int i = this$0.mMaxClientId + 1;
                    this$0.mMaxClientId = i;
                    if (this$0.mCallbackList.register((IInterface)multiInstanceInvalidationCallback, (Object)i)) {
                        this.this$0.mClientNames.put(i, value);
                        return i;
                    }
                    final MultiInstanceInvalidationService this$2 = this.this$0;
                    --this$2.mMaxClientId;
                    return 0;
                }
            }
            
            public void unregisterCallback(final IMultiInstanceInvalidationCallback multiInstanceInvalidationCallback, final int i) {
                synchronized (this.this$0.mCallbackList) {
                    this.this$0.mCallbackList.unregister((IInterface)multiInstanceInvalidationCallback);
                    this.this$0.mClientNames.remove(i);
                }
            }
        };
    }
    
    @Nullable
    public IBinder onBind(@NonNull final Intent intent) {
        return (IBinder)this.mBinder;
    }
}

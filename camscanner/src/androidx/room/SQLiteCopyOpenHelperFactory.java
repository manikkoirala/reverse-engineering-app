// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import androidx.annotation.NonNull;
import java.io.InputStream;
import java.util.concurrent.Callable;
import java.io.File;
import androidx.annotation.Nullable;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

class SQLiteCopyOpenHelperFactory implements Factory
{
    @Nullable
    private final String mCopyFromAssetPath;
    @Nullable
    private final File mCopyFromFile;
    @Nullable
    private final Callable<InputStream> mCopyFromInputStream;
    @NonNull
    private final Factory mDelegate;
    
    SQLiteCopyOpenHelperFactory(@Nullable final String mCopyFromAssetPath, @Nullable final File mCopyFromFile, @Nullable final Callable<InputStream> mCopyFromInputStream, @NonNull final Factory mDelegate) {
        this.mCopyFromAssetPath = mCopyFromAssetPath;
        this.mCopyFromFile = mCopyFromFile;
        this.mCopyFromInputStream = mCopyFromInputStream;
        this.mDelegate = mDelegate;
    }
    
    @NonNull
    @Override
    public SupportSQLiteOpenHelper create(final Configuration configuration) {
        return new SQLiteCopyOpenHelper(configuration.context, this.mCopyFromAssetPath, this.mCopyFromFile, this.mCopyFromInputStream, configuration.callback.version, this.mDelegate.create(configuration));
    }
}

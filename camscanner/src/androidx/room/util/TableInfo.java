// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room.util;

import androidx.room.Index;
import java.util.Locale;
import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import java.util.Collection;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.List;
import android.database.Cursor;
import java.util.HashMap;
import androidx.sqlite.db.SupportSQLiteDatabase;
import java.util.Collections;
import androidx.annotation.Nullable;
import java.util.Set;
import java.util.Map;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public final class TableInfo
{
    public static final int CREATED_FROM_DATABASE = 2;
    public static final int CREATED_FROM_ENTITY = 1;
    public static final int CREATED_FROM_UNKNOWN = 0;
    public final Map<String, Column> columns;
    public final Set<ForeignKey> foreignKeys;
    @Nullable
    public final Set<Index> indices;
    public final String name;
    
    public TableInfo(final String s, final Map<String, Column> map, final Set<ForeignKey> set) {
        this(s, map, set, Collections.emptySet());
    }
    
    public TableInfo(final String name, final Map<String, Column> m, final Set<ForeignKey> s, final Set<Index> s2) {
        this.name = name;
        this.columns = Collections.unmodifiableMap((Map<? extends String, ? extends Column>)m);
        this.foreignKeys = Collections.unmodifiableSet((Set<? extends ForeignKey>)s);
        Set<Index> unmodifiableSet;
        if (s2 == null) {
            unmodifiableSet = null;
        }
        else {
            unmodifiableSet = Collections.unmodifiableSet((Set<? extends Index>)s2);
        }
        this.indices = unmodifiableSet;
    }
    
    public static TableInfo read(final SupportSQLiteDatabase supportSQLiteDatabase, final String s) {
        return new TableInfo(s, readColumns(supportSQLiteDatabase, s), readForeignKeys(supportSQLiteDatabase, s), readIndices(supportSQLiteDatabase, s));
    }
    
    private static Map<String, Column> readColumns(SupportSQLiteDatabase query, final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("PRAGMA table_info(`");
        sb.append(str);
        sb.append("`)");
        query = (SupportSQLiteDatabase)query.query(sb.toString());
        final HashMap hashMap = new HashMap();
        try {
            if (((Cursor)query).getColumnCount() > 0) {
                final int columnIndex = ((Cursor)query).getColumnIndex("name");
                final int columnIndex2 = ((Cursor)query).getColumnIndex("type");
                final int columnIndex3 = ((Cursor)query).getColumnIndex("notnull");
                final int columnIndex4 = ((Cursor)query).getColumnIndex("pk");
                final int columnIndex5 = ((Cursor)query).getColumnIndex("dflt_value");
                while (((Cursor)query).moveToNext()) {
                    final String string = ((Cursor)query).getString(columnIndex);
                    hashMap.put(string, new Column(string, ((Cursor)query).getString(columnIndex2), ((Cursor)query).getInt(columnIndex3) != 0, ((Cursor)query).getInt(columnIndex4), ((Cursor)query).getString(columnIndex5), 2));
                }
            }
            return hashMap;
        }
        finally {
            ((Cursor)query).close();
        }
    }
    
    private static List<ForeignKeyWithSequence> readForeignKeyFieldMappings(final Cursor cursor) {
        final int columnIndex = cursor.getColumnIndex("id");
        final int columnIndex2 = cursor.getColumnIndex("seq");
        final int columnIndex3 = cursor.getColumnIndex("from");
        final int columnIndex4 = cursor.getColumnIndex("to");
        final int count = cursor.getCount();
        final ArrayList list = new ArrayList();
        for (int i = 0; i < count; ++i) {
            cursor.moveToPosition(i);
            list.add(new ForeignKeyWithSequence(cursor.getInt(columnIndex), cursor.getInt(columnIndex2), cursor.getString(columnIndex3), cursor.getString(columnIndex4)));
        }
        Collections.sort((List<Comparable>)list);
        return list;
    }
    
    private static Set<ForeignKey> readForeignKeys(SupportSQLiteDatabase query, final String str) {
        final HashSet set = new HashSet();
        final StringBuilder sb = new StringBuilder();
        sb.append("PRAGMA foreign_key_list(`");
        sb.append(str);
        sb.append("`)");
        query = (SupportSQLiteDatabase)query.query(sb.toString());
        try {
            final int columnIndex = ((Cursor)query).getColumnIndex("id");
            final int columnIndex2 = ((Cursor)query).getColumnIndex("seq");
            final int columnIndex3 = ((Cursor)query).getColumnIndex("table");
            final int columnIndex4 = ((Cursor)query).getColumnIndex("on_delete");
            final int columnIndex5 = ((Cursor)query).getColumnIndex("on_update");
            final List<ForeignKeyWithSequence> foreignKeyFieldMappings = readForeignKeyFieldMappings((Cursor)query);
            for (int count = ((Cursor)query).getCount(), i = 0; i < count; ++i) {
                ((Cursor)query).moveToPosition(i);
                if (((Cursor)query).getInt(columnIndex2) == 0) {
                    final int int1 = ((Cursor)query).getInt(columnIndex);
                    final ArrayList list = new ArrayList();
                    final ArrayList<String> list2 = new ArrayList<String>();
                    for (final ForeignKeyWithSequence foreignKeyWithSequence : foreignKeyFieldMappings) {
                        if (foreignKeyWithSequence.mId == int1) {
                            list.add(foreignKeyWithSequence.mFrom);
                            list2.add(foreignKeyWithSequence.mTo);
                        }
                    }
                    set.add(new ForeignKey(((Cursor)query).getString(columnIndex3), ((Cursor)query).getString(columnIndex4), ((Cursor)query).getString(columnIndex5), list, list2));
                }
            }
            return set;
        }
        finally {
            ((Cursor)query).close();
        }
    }
    
    @Nullable
    private static Index readIndex(final SupportSQLiteDatabase supportSQLiteDatabase, final String str, final boolean b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("PRAGMA index_xinfo(`");
        sb.append(str);
        sb.append("`)");
        final Cursor query = supportSQLiteDatabase.query(sb.toString());
        try {
            final int columnIndex = query.getColumnIndex("seqno");
            final int columnIndex2 = query.getColumnIndex("cid");
            final int columnIndex3 = query.getColumnIndex("name");
            final int columnIndex4 = query.getColumnIndex("desc");
            if (columnIndex != -1 && columnIndex2 != -1 && columnIndex3 != -1 && columnIndex4 != -1) {
                final TreeMap<Integer, String> treeMap = new TreeMap<Integer, String>();
                final TreeMap treeMap2 = new TreeMap();
                while (query.moveToNext()) {
                    if (query.getInt(columnIndex2) < 0) {
                        continue;
                    }
                    final int int1 = query.getInt(columnIndex);
                    final String string = query.getString(columnIndex3);
                    String value;
                    if (query.getInt(columnIndex4) > 0) {
                        value = "DESC";
                    }
                    else {
                        value = "ASC";
                    }
                    treeMap.put(int1, string);
                    treeMap2.put(int1, value);
                }
                final ArrayList list = new ArrayList(treeMap.size());
                list.addAll((Collection)treeMap.values());
                final ArrayList list2 = new ArrayList<String>(treeMap2.size());
                list2.addAll((Collection<?>)treeMap2.values());
                return new Index(str, b, (List<String>)list, (List<String>)list2);
            }
            return null;
        }
        finally {
            query.close();
        }
    }
    
    @Nullable
    private static Set<Index> readIndices(final SupportSQLiteDatabase supportSQLiteDatabase, String query) {
        final StringBuilder sb = new StringBuilder();
        sb.append("PRAGMA index_list(`");
        sb.append(query);
        sb.append("`)");
        query = (String)supportSQLiteDatabase.query(sb.toString());
        try {
            final int columnIndex = ((Cursor)query).getColumnIndex("name");
            final int columnIndex2 = ((Cursor)query).getColumnIndex("origin");
            final int columnIndex3 = ((Cursor)query).getColumnIndex("unique");
            if (columnIndex != -1 && columnIndex2 != -1 && columnIndex3 != -1) {
                final HashSet<Index> set = new HashSet<Index>();
                while (((Cursor)query).moveToNext()) {
                    if (!"c".equals(((Cursor)query).getString(columnIndex2))) {
                        continue;
                    }
                    final String string = ((Cursor)query).getString(columnIndex);
                    final int int1 = ((Cursor)query).getInt(columnIndex3);
                    boolean b = true;
                    if (int1 != 1) {
                        b = false;
                    }
                    final Index index = readIndex(supportSQLiteDatabase, string, b);
                    if (index == null) {
                        return null;
                    }
                    set.add(index);
                }
                return set;
            }
            return null;
        }
        finally {
            ((Cursor)query).close();
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TableInfo)) {
            return false;
        }
        final TableInfo tableInfo = (TableInfo)o;
        final String name = this.name;
        Label_0053: {
            if (name != null) {
                if (name.equals(tableInfo.name)) {
                    break Label_0053;
                }
            }
            else if (tableInfo.name == null) {
                break Label_0053;
            }
            return false;
        }
        final Map<String, Column> columns = this.columns;
        Label_0087: {
            if (columns != null) {
                if (columns.equals(tableInfo.columns)) {
                    break Label_0087;
                }
            }
            else if (tableInfo.columns == null) {
                break Label_0087;
            }
            return false;
        }
        final Set<ForeignKey> foreignKeys = this.foreignKeys;
        Label_0121: {
            if (foreignKeys != null) {
                if (foreignKeys.equals(tableInfo.foreignKeys)) {
                    break Label_0121;
                }
            }
            else if (tableInfo.foreignKeys == null) {
                break Label_0121;
            }
            return false;
        }
        final Set<Index> indices = this.indices;
        if (indices != null) {
            final Set<Index> indices2 = tableInfo.indices;
            if (indices2 != null) {
                return indices.equals(indices2);
            }
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        final String name = this.name;
        int hashCode = 0;
        int hashCode2;
        if (name != null) {
            hashCode2 = name.hashCode();
        }
        else {
            hashCode2 = 0;
        }
        final Map<String, Column> columns = this.columns;
        int hashCode3;
        if (columns != null) {
            hashCode3 = columns.hashCode();
        }
        else {
            hashCode3 = 0;
        }
        final Set<ForeignKey> foreignKeys = this.foreignKeys;
        if (foreignKeys != null) {
            hashCode = foreignKeys.hashCode();
        }
        return (hashCode2 * 31 + hashCode3) * 31 + hashCode;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("TableInfo{name='");
        sb.append(this.name);
        sb.append('\'');
        sb.append(", columns=");
        sb.append(this.columns);
        sb.append(", foreignKeys=");
        sb.append(this.foreignKeys);
        sb.append(", indices=");
        sb.append(this.indices);
        sb.append('}');
        return sb.toString();
    }
    
    public static final class Column
    {
        @ColumnInfo.SQLiteTypeAffinity
        public final int affinity;
        public final String defaultValue;
        private final int mCreatedFrom;
        public final String name;
        public final boolean notNull;
        public final int primaryKeyPosition;
        public final String type;
        
        @Deprecated
        public Column(final String s, final String s2, final boolean b, final int n) {
            this(s, s2, b, n, null, 0);
        }
        
        public Column(final String name, final String type, final boolean notNull, final int primaryKeyPosition, final String defaultValue, final int mCreatedFrom) {
            this.name = name;
            this.type = type;
            this.notNull = notNull;
            this.primaryKeyPosition = primaryKeyPosition;
            this.affinity = findAffinity(type);
            this.defaultValue = defaultValue;
            this.mCreatedFrom = mCreatedFrom;
        }
        
        private static boolean containsSurroundingParenthesis(@NonNull final String s) {
            final int length = s.length();
            boolean b = false;
            if (length == 0) {
                return false;
            }
            int i = 0;
            int n = 0;
            while (i < s.length()) {
                final char char1 = s.charAt(i);
                if (i == 0 && char1 != '(') {
                    return false;
                }
                int n2;
                if (char1 == '(') {
                    n2 = n + 1;
                }
                else {
                    n2 = n;
                    if (char1 == ')') {
                        n2 = --n;
                        if (n == 0) {
                            n2 = n;
                            if (i != s.length() - 1) {
                                return false;
                            }
                        }
                    }
                }
                ++i;
                n = n2;
            }
            if (n == 0) {
                b = true;
            }
            return b;
        }
        
        public static boolean defaultValueEquals(@NonNull final String s, @Nullable final String s2) {
            return s2 != null && (s.equals(s2) || (containsSurroundingParenthesis(s) && s.substring(1, s.length() - 1).trim().equals(s2)));
        }
        
        @ColumnInfo.SQLiteTypeAffinity
        private static int findAffinity(@Nullable String upperCase) {
            if (upperCase == null) {
                return 5;
            }
            upperCase = upperCase.toUpperCase(Locale.US);
            if (upperCase.contains("INT")) {
                return 3;
            }
            if (upperCase.contains("CHAR") || upperCase.contains("CLOB") || upperCase.contains("TEXT")) {
                return 2;
            }
            if (upperCase.contains("BLOB")) {
                return 5;
            }
            if (!upperCase.contains("REAL") && !upperCase.contains("FLOA") && !upperCase.contains("DOUB")) {
                return 1;
            }
            return 4;
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (!(o instanceof Column)) {
                return false;
            }
            final Column column = (Column)o;
            if (this.primaryKeyPosition != column.primaryKeyPosition) {
                return false;
            }
            if (!this.name.equals(column.name)) {
                return false;
            }
            if (this.notNull != column.notNull) {
                return false;
            }
            if (this.mCreatedFrom == 1 && column.mCreatedFrom == 2) {
                final String defaultValue = this.defaultValue;
                if (defaultValue != null && !defaultValueEquals(defaultValue, column.defaultValue)) {
                    return false;
                }
            }
            if (this.mCreatedFrom == 2 && column.mCreatedFrom == 1) {
                final String defaultValue2 = column.defaultValue;
                if (defaultValue2 != null && !defaultValueEquals(defaultValue2, this.defaultValue)) {
                    return false;
                }
            }
            final int mCreatedFrom = this.mCreatedFrom;
            Label_0199: {
                if (mCreatedFrom != 0 && mCreatedFrom == column.mCreatedFrom) {
                    final String defaultValue3 = this.defaultValue;
                    if (defaultValue3 != null) {
                        if (defaultValueEquals(defaultValue3, column.defaultValue)) {
                            break Label_0199;
                        }
                    }
                    else if (column.defaultValue == null) {
                        break Label_0199;
                    }
                    return false;
                }
            }
            if (this.affinity != column.affinity) {
                b = false;
            }
            return b;
        }
        
        @Override
        public int hashCode() {
            final int hashCode = this.name.hashCode();
            final int affinity = this.affinity;
            int n;
            if (this.notNull) {
                n = 1231;
            }
            else {
                n = 1237;
            }
            return ((hashCode * 31 + affinity) * 31 + n) * 31 + this.primaryKeyPosition;
        }
        
        public boolean isPrimaryKey() {
            return this.primaryKeyPosition > 0;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Column{name='");
            sb.append(this.name);
            sb.append('\'');
            sb.append(", type='");
            sb.append(this.type);
            sb.append('\'');
            sb.append(", affinity='");
            sb.append(this.affinity);
            sb.append('\'');
            sb.append(", notNull=");
            sb.append(this.notNull);
            sb.append(", primaryKeyPosition=");
            sb.append(this.primaryKeyPosition);
            sb.append(", defaultValue='");
            sb.append(this.defaultValue);
            sb.append('\'');
            sb.append('}');
            return sb.toString();
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static final class ForeignKey
    {
        @NonNull
        public final List<String> columnNames;
        @NonNull
        public final String onDelete;
        @NonNull
        public final String onUpdate;
        @NonNull
        public final List<String> referenceColumnNames;
        @NonNull
        public final String referenceTable;
        
        public ForeignKey(@NonNull final String referenceTable, @NonNull final String onDelete, @NonNull final String onUpdate, @NonNull final List<String> list, @NonNull final List<String> list2) {
            this.referenceTable = referenceTable;
            this.onDelete = onDelete;
            this.onUpdate = onUpdate;
            this.columnNames = Collections.unmodifiableList((List<? extends String>)list);
            this.referenceColumnNames = Collections.unmodifiableList((List<? extends String>)list2);
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof ForeignKey)) {
                return false;
            }
            final ForeignKey foreignKey = (ForeignKey)o;
            return this.referenceTable.equals(foreignKey.referenceTable) && this.onDelete.equals(foreignKey.onDelete) && this.onUpdate.equals(foreignKey.onUpdate) && this.columnNames.equals(foreignKey.columnNames) && this.referenceColumnNames.equals(foreignKey.referenceColumnNames);
        }
        
        @Override
        public int hashCode() {
            return (((this.referenceTable.hashCode() * 31 + this.onDelete.hashCode()) * 31 + this.onUpdate.hashCode()) * 31 + this.columnNames.hashCode()) * 31 + this.referenceColumnNames.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("ForeignKey{referenceTable='");
            sb.append(this.referenceTable);
            sb.append('\'');
            sb.append(", onDelete='");
            sb.append(this.onDelete);
            sb.append('\'');
            sb.append(", onUpdate='");
            sb.append(this.onUpdate);
            sb.append('\'');
            sb.append(", columnNames=");
            sb.append(this.columnNames);
            sb.append(", referenceColumnNames=");
            sb.append(this.referenceColumnNames);
            sb.append('}');
            return sb.toString();
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    static class ForeignKeyWithSequence implements Comparable<ForeignKeyWithSequence>
    {
        final String mFrom;
        final int mId;
        final int mSequence;
        final String mTo;
        
        ForeignKeyWithSequence(final int mId, final int mSequence, final String mFrom, final String mTo) {
            this.mId = mId;
            this.mSequence = mSequence;
            this.mFrom = mFrom;
            this.mTo = mTo;
        }
        
        @Override
        public int compareTo(@NonNull final ForeignKeyWithSequence foreignKeyWithSequence) {
            int n;
            if ((n = this.mId - foreignKeyWithSequence.mId) == 0) {
                n = this.mSequence - foreignKeyWithSequence.mSequence;
            }
            return n;
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static final class Index
    {
        public static final String DEFAULT_PREFIX = "index_";
        public final List<String> columns;
        public final String name;
        public final List<String> orders;
        public final boolean unique;
        
        public Index(final String s, final boolean b, final List<String> list) {
            this(s, b, list, null);
        }
        
        public Index(final String name, final boolean unique, final List<String> columns, final List<String> list) {
            this.name = name;
            this.unique = unique;
            this.columns = columns;
            List<String> nCopies = null;
            Label_0053: {
                if (list != null) {
                    nCopies = list;
                    if (list.size() != 0) {
                        break Label_0053;
                    }
                }
                nCopies = Collections.nCopies(columns.size(), androidx.room.Index.Order.ASC.name());
            }
            this.orders = nCopies;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof Index)) {
                return false;
            }
            final Index index = (Index)o;
            if (this.unique != index.unique) {
                return false;
            }
            if (!this.columns.equals(index.columns)) {
                return false;
            }
            if (!this.orders.equals(index.orders)) {
                return false;
            }
            if (this.name.startsWith("index_")) {
                return index.name.startsWith("index_");
            }
            return this.name.equals(index.name);
        }
        
        @Override
        public int hashCode() {
            int hashCode;
            if (this.name.startsWith("index_")) {
                hashCode = -1184239155;
            }
            else {
                hashCode = this.name.hashCode();
            }
            return ((hashCode * 31 + (this.unique ? 1 : 0)) * 31 + this.columns.hashCode()) * 31 + this.orders.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Index{name='");
            sb.append(this.name);
            sb.append('\'');
            sb.append(", unique=");
            sb.append(this.unique);
            sb.append(", columns=");
            sb.append(this.columns);
            sb.append(", orders=");
            sb.append(this.orders);
            sb.append('}');
            return sb.toString();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room.util;

import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class SneakyThrow
{
    private SneakyThrow() {
    }
    
    public static void reThrow(@NonNull final Exception ex) {
        sneakyThrow(ex);
    }
    
    private static <E extends Throwable> void sneakyThrow(@NonNull final Throwable t) throws E, Throwable {
        throw t;
    }
}

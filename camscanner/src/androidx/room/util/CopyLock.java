// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room.util;

import java.io.IOException;
import java.io.FileOutputStream;
import java.util.concurrent.locks.ReentrantLock;
import androidx.annotation.NonNull;
import java.util.HashMap;
import java.nio.channels.FileChannel;
import java.io.File;
import java.util.concurrent.locks.Lock;
import java.util.Map;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public class CopyLock
{
    private static final Map<String, Lock> sThreadLocks;
    private final File mCopyLockFile;
    private final boolean mFileLevelLock;
    private FileChannel mLockChannel;
    private final Lock mThreadLock;
    
    static {
        sThreadLocks = new HashMap<String, Lock>();
    }
    
    public CopyLock(@NonNull final String str, @NonNull final File parent, final boolean mFileLevelLock) {
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(".lck");
        final File mCopyLockFile = new File(parent, sb.toString());
        this.mCopyLockFile = mCopyLockFile;
        this.mThreadLock = getThreadLock(mCopyLockFile.getAbsolutePath());
        this.mFileLevelLock = mFileLevelLock;
    }
    
    private static Lock getThreadLock(final String s) {
        final Map<String, Lock> sThreadLocks = CopyLock.sThreadLocks;
        synchronized (sThreadLocks) {
            Lock lock;
            if ((lock = sThreadLocks.get(s)) == null) {
                lock = new ReentrantLock();
                sThreadLocks.put(s, lock);
            }
            return lock;
        }
    }
    
    public void lock() {
        this.mThreadLock.lock();
        if (this.mFileLevelLock) {
            try {
                (this.mLockChannel = new FileOutputStream(this.mCopyLockFile).getChannel()).lock();
            }
            catch (final IOException cause) {
                throw new IllegalStateException("Unable to grab copy lock.", cause);
            }
        }
    }
    
    public void unlock() {
        final FileChannel mLockChannel = this.mLockChannel;
        while (true) {
            if (mLockChannel == null) {
                break Label_0013;
            }
            try {
                mLockChannel.close();
                this.mThreadLock.unlock();
            }
            catch (final IOException ex) {
                continue;
            }
            break;
        }
    }
}

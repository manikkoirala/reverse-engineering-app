// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room.util;

import android.database.Cursor;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public final class ViewInfo
{
    public final String name;
    public final String sql;
    
    public ViewInfo(final String name, final String sql) {
        this.name = name;
        this.sql = sql;
    }
    
    public static ViewInfo read(SupportSQLiteDatabase query, final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("SELECT name, sql FROM sqlite_master WHERE type = 'view' AND name = '");
        sb.append(str);
        sb.append("'");
        query = (SupportSQLiteDatabase)query.query(sb.toString());
        try {
            if (((Cursor)query).moveToFirst()) {
                return new ViewInfo(((Cursor)query).getString(0), ((Cursor)query).getString(1));
            }
            return new ViewInfo(str, null);
        }
        finally {
            ((Cursor)query).close();
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof ViewInfo)) {
            return false;
        }
        final ViewInfo viewInfo = (ViewInfo)o;
        final String name = this.name;
        if (name != null) {
            if (!name.equals(viewInfo.name)) {
                return false;
            }
        }
        else if (viewInfo.name != null) {
            return false;
        }
        final String sql = this.sql;
        if (sql != null) {
            if (sql.equals(viewInfo.sql)) {
                return b;
            }
        }
        else if (viewInfo.sql == null) {
            return b;
        }
        b = false;
        return b;
    }
    
    @Override
    public int hashCode() {
        final String name = this.name;
        int hashCode = 0;
        int hashCode2;
        if (name != null) {
            hashCode2 = name.hashCode();
        }
        else {
            hashCode2 = 0;
        }
        final String sql = this.sql;
        if (sql != null) {
            hashCode = sql.hashCode();
        }
        return hashCode2 * 31 + hashCode;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ViewInfo{name='");
        sb.append(this.name);
        sb.append('\'');
        sb.append(", sql='");
        sb.append(this.sql);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}

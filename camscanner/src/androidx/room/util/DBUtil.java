// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room.util;

import android.database.AbstractCursor;
import java.nio.channels.FileChannel;
import java.nio.channels.spi.AbstractInterruptibleChannel;
import java.io.IOException;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.io.File;
import android.os.Build$VERSION;
import android.database.AbstractWindowedCursor;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.room.RoomDatabase;
import java.util.Map;
import java.util.HashMap;
import android.database.Cursor;
import androidx.annotation.NonNull;
import java.util.Iterator;
import java.util.ArrayList;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.annotation.Nullable;
import androidx.sqlite.db.SupportSQLiteCompat;
import android.os.CancellationSignal;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public class DBUtil
{
    private DBUtil() {
    }
    
    @Nullable
    public static CancellationSignal createCancellationSignal() {
        return SupportSQLiteCompat.Api16Impl.createCancellationSignal();
    }
    
    public static void dropFtsSyncTriggers(final SupportSQLiteDatabase supportSQLiteDatabase) {
        final ArrayList list = new ArrayList();
        Object query = supportSQLiteDatabase.query("SELECT name FROM sqlite_master WHERE type = 'trigger'");
        try {
            while (((Cursor)query).moveToNext()) {
                list.add(((Cursor)query).getString(0));
            }
            ((Cursor)query).close();
            for (final String str : list) {
                if (str.startsWith("room_fts_content_sync_")) {
                    query = new StringBuilder();
                    ((StringBuilder)query).append("DROP TRIGGER IF EXISTS ");
                    ((StringBuilder)query).append(str);
                    supportSQLiteDatabase.execSQL(((StringBuilder)query).toString());
                }
            }
        }
        finally {
            ((Cursor)query).close();
        }
    }
    
    public static void foreignKeyCheck(@NonNull SupportSQLiteDatabase query, @NonNull String processForeignKeyCheckFailure) {
        final StringBuilder sb = new StringBuilder();
        sb.append("PRAGMA foreign_key_check(`");
        sb.append(processForeignKeyCheckFailure);
        sb.append("`)");
        query = (SupportSQLiteDatabase)query.query(sb.toString());
        try {
            if (((Cursor)query).getCount() <= 0) {
                return;
            }
            processForeignKeyCheckFailure = processForeignKeyCheckFailure((Cursor)query);
            throw new IllegalStateException(processForeignKeyCheckFailure);
        }
        finally {
            ((Cursor)query).close();
        }
    }
    
    private static String processForeignKeyCheckFailure(final Cursor cursor) {
        final int count = cursor.getCount();
        final HashMap hashMap = new HashMap();
        String str = null;
        while (cursor.moveToNext()) {
            String string;
            if ((string = str) == null) {
                string = cursor.getString(0);
            }
            final String string2 = cursor.getString(3);
            str = string;
            if (!hashMap.containsKey(string2)) {
                hashMap.put(string2, cursor.getString(2));
                str = string;
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Foreign key violation(s) detected in '");
        sb.append(str);
        sb.append("'.\n");
        sb.append("Number of different violations discovered: ");
        sb.append(hashMap.keySet().size());
        sb.append("\n");
        sb.append("Number of rows in violation: ");
        sb.append(count);
        sb.append("\n");
        sb.append("Violation(s) detected in the following constraint(s):\n");
        for (final Map.Entry<K, String> entry : hashMap.entrySet()) {
            sb.append("\tParent Table = ");
            sb.append(entry.getValue());
            sb.append(", Foreign Key Constraint Index = ");
            sb.append((String)entry.getKey());
            sb.append("\n");
        }
        return sb.toString();
    }
    
    @Deprecated
    @NonNull
    public static Cursor query(final RoomDatabase roomDatabase, final SupportSQLiteQuery supportSQLiteQuery, final boolean b) {
        return query(roomDatabase, supportSQLiteQuery, b, null);
    }
    
    @NonNull
    public static Cursor query(@NonNull final RoomDatabase roomDatabase, @NonNull final SupportSQLiteQuery supportSQLiteQuery, final boolean b, @Nullable final CancellationSignal cancellationSignal) {
        Cursor cursor2;
        final Cursor cursor = cursor2 = roomDatabase.query(supportSQLiteQuery, cancellationSignal);
        if (b) {
            cursor2 = cursor;
            if (cursor instanceof AbstractWindowedCursor) {
                final AbstractWindowedCursor abstractWindowedCursor = (AbstractWindowedCursor)cursor;
                final int count = ((AbstractCursor)abstractWindowedCursor).getCount();
                int numRows;
                if (abstractWindowedCursor.hasWindow()) {
                    numRows = abstractWindowedCursor.getWindow().getNumRows();
                }
                else {
                    numRows = count;
                }
                if (Build$VERSION.SDK_INT >= 23) {
                    cursor2 = cursor;
                    if (numRows >= count) {
                        return cursor2;
                    }
                }
                cursor2 = CursorUtil.copyAndClose((Cursor)abstractWindowedCursor);
            }
        }
        return cursor2;
    }
    
    public static int readVersion(@NonNull final File file) throws IOException {
        AbstractInterruptibleChannel channel;
        final AbstractInterruptibleChannel abstractInterruptibleChannel = channel = null;
        try {
            final ByteBuffer allocate = ByteBuffer.allocate(4);
            channel = abstractInterruptibleChannel;
            channel = abstractInterruptibleChannel;
            final FileInputStream fileInputStream = new FileInputStream(file);
            channel = abstractInterruptibleChannel;
            final FileChannel fileChannel = (FileChannel)(channel = fileInputStream.getChannel());
            fileChannel.tryLock(60L, 4L, true);
            channel = fileChannel;
            fileChannel.position(60L);
            channel = fileChannel;
            if (fileChannel.read(allocate) == 4) {
                channel = fileChannel;
                allocate.rewind();
                channel = fileChannel;
                final int int1 = allocate.getInt();
                fileChannel.close();
                return int1;
            }
            channel = fileChannel;
            channel = fileChannel;
            final IOException ex = new IOException("Bad database header, unable to read 4 bytes at offset 60");
            channel = fileChannel;
            throw ex;
        }
        finally {
            if (channel != null) {
                channel.close();
            }
        }
    }
}

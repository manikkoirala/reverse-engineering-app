// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room.util;

import android.database.Cursor;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.annotation.VisibleForTesting;
import java.util.Iterator;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public final class FtsTableInfo
{
    private static final String[] FTS_OPTIONS;
    public final Set<String> columns;
    public final String name;
    public final Set<String> options;
    
    static {
        FTS_OPTIONS = new String[] { "tokenize=", "compress=", "content=", "languageid=", "matchinfo=", "notindexed=", "order=", "prefix=", "uncompress=" };
    }
    
    public FtsTableInfo(final String name, final Set<String> columns, final String s) {
        this.name = name;
        this.columns = columns;
        this.options = parseOptions(s);
    }
    
    public FtsTableInfo(final String name, final Set<String> columns, final Set<String> options) {
        this.name = name;
        this.columns = columns;
        this.options = options;
    }
    
    @VisibleForTesting
    static Set<String> parseOptions(String substring) {
        if (substring.isEmpty()) {
            return new HashSet<String>();
        }
        substring = substring.substring(substring.indexOf(40) + 1, substring.lastIndexOf(41));
        final ArrayList list = new ArrayList();
        final ArrayDeque arrayDeque = new ArrayDeque();
        int n = -1;
        int n2;
        for (int i = 0; i < substring.length(); ++i, n = n2) {
            final char char1 = substring.charAt(i);
            if (char1 != '\"' && char1 != '\'') {
                if (char1 != ',') {
                    if (char1 != '[') {
                        if (char1 != ']') {
                            if (char1 != '`') {
                                n2 = n;
                                continue;
                            }
                        }
                        else {
                            n2 = n;
                            if (arrayDeque.isEmpty()) {
                                continue;
                            }
                            n2 = n;
                            if ((char)arrayDeque.peek() == '[') {
                                arrayDeque.pop();
                                n2 = n;
                            }
                            continue;
                        }
                    }
                    else {
                        n2 = n;
                        if (arrayDeque.isEmpty()) {
                            arrayDeque.push(char1);
                            n2 = n;
                        }
                        continue;
                    }
                }
                else {
                    n2 = n;
                    if (arrayDeque.isEmpty()) {
                        list.add(substring.substring(n + 1, i).trim());
                        n2 = i;
                    }
                    continue;
                }
            }
            if (arrayDeque.isEmpty()) {
                arrayDeque.push(char1);
                n2 = n;
            }
            else {
                n2 = n;
                if (arrayDeque.peek() == char1) {
                    arrayDeque.pop();
                    n2 = n;
                }
            }
        }
        list.add(substring.substring(n + 1).trim());
        final HashSet<String> set = new HashSet<String>();
        for (final String e : list) {
            final String[] fts_OPTIONS = FtsTableInfo.FTS_OPTIONS;
            for (int length = fts_OPTIONS.length, j = 0; j < length; ++j) {
                if (e.startsWith(fts_OPTIONS[j])) {
                    set.add(e);
                }
            }
        }
        return set;
    }
    
    public static FtsTableInfo read(final SupportSQLiteDatabase supportSQLiteDatabase, final String s) {
        return new FtsTableInfo(s, readColumns(supportSQLiteDatabase, s), readOptions(supportSQLiteDatabase, s));
    }
    
    private static Set<String> readColumns(SupportSQLiteDatabase query, final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("PRAGMA table_info(`");
        sb.append(str);
        sb.append("`)");
        query = (SupportSQLiteDatabase)query.query(sb.toString());
        final HashSet set = new HashSet();
        try {
            if (((Cursor)query).getColumnCount() > 0) {
                final int columnIndex = ((Cursor)query).getColumnIndex("name");
                while (((Cursor)query).moveToNext()) {
                    set.add(((Cursor)query).getString(columnIndex));
                }
            }
            return set;
        }
        finally {
            ((Cursor)query).close();
        }
    }
    
    private static Set<String> readOptions(final SupportSQLiteDatabase supportSQLiteDatabase, String query) {
        final StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM sqlite_master WHERE `name` = '");
        sb.append(query);
        sb.append("'");
        query = (String)supportSQLiteDatabase.query(sb.toString());
        try {
            String string;
            if (((Cursor)query).moveToFirst()) {
                string = ((Cursor)query).getString(((Cursor)query).getColumnIndexOrThrow("sql"));
            }
            else {
                string = "";
            }
            ((Cursor)query).close();
            return parseOptions(string);
        }
        finally {
            ((Cursor)query).close();
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean equals = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof FtsTableInfo)) {
            return false;
        }
        final FtsTableInfo ftsTableInfo = (FtsTableInfo)o;
        final String name = this.name;
        Label_0055: {
            if (name != null) {
                if (name.equals(ftsTableInfo.name)) {
                    break Label_0055;
                }
            }
            else if (ftsTableInfo.name == null) {
                break Label_0055;
            }
            return false;
        }
        final Set<String> columns = this.columns;
        Label_0089: {
            if (columns != null) {
                if (columns.equals(ftsTableInfo.columns)) {
                    break Label_0089;
                }
            }
            else if (ftsTableInfo.columns == null) {
                break Label_0089;
            }
            return false;
        }
        final Set<String> options = this.options;
        final Set<String> options2 = ftsTableInfo.options;
        if (options != null) {
            equals = options.equals(options2);
        }
        else if (options2 != null) {
            equals = false;
        }
        return equals;
    }
    
    @Override
    public int hashCode() {
        final String name = this.name;
        int hashCode = 0;
        int hashCode2;
        if (name != null) {
            hashCode2 = name.hashCode();
        }
        else {
            hashCode2 = 0;
        }
        final Set<String> columns = this.columns;
        int hashCode3;
        if (columns != null) {
            hashCode3 = columns.hashCode();
        }
        else {
            hashCode3 = 0;
        }
        final Set<String> options = this.options;
        if (options != null) {
            hashCode = options.hashCode();
        }
        return (hashCode2 * 31 + hashCode3) * 31 + hashCode;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("FtsTableInfo{name='");
        sb.append(this.name);
        sb.append('\'');
        sb.append(", columns=");
        sb.append(this.columns);
        sb.append(", options=");
        sb.append(this.options);
        sb.append('}');
        return sb.toString();
    }
}

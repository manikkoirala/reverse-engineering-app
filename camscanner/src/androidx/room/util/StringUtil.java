// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room.util;

import java.util.StringTokenizer;
import java.util.ArrayList;
import androidx.annotation.Nullable;
import java.util.List;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public class StringUtil
{
    public static final String[] EMPTY_STRING_ARRAY;
    
    static {
        EMPTY_STRING_ARRAY = new String[0];
    }
    
    private StringUtil() {
    }
    
    public static void appendPlaceholders(final StringBuilder sb, final int n) {
        for (int i = 0; i < n; ++i) {
            sb.append("?");
            if (i < n - 1) {
                sb.append(",");
            }
        }
    }
    
    @Nullable
    public static String joinIntoString(@Nullable final List<Integer> list) {
        if (list == null) {
            return null;
        }
        final int size = list.size();
        if (size == 0) {
            return "";
        }
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; ++i) {
            sb.append(Integer.toString((int)list.get(i)));
            if (i < size - 1) {
                sb.append(",");
            }
        }
        return sb.toString();
    }
    
    public static StringBuilder newStringBuilder() {
        return new StringBuilder();
    }
    
    @Nullable
    public static List<Integer> splitToIntList(@Nullable String str) {
        if (str == null) {
            return null;
        }
        final ArrayList list = new ArrayList();
        str = (String)new StringTokenizer(str, ",");
        while (((StringTokenizer)str).hasMoreElements()) {
            final String nextToken = ((StringTokenizer)str).nextToken();
            try {
                list.add(Integer.parseInt(nextToken));
            }
            catch (final NumberFormatException ex) {}
        }
        return list;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room.util;

import java.util.Arrays;
import androidx.annotation.VisibleForTesting;
import android.os.Build$VERSION;
import android.database.MatrixCursor;
import androidx.annotation.NonNull;
import android.database.Cursor;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public class CursorUtil
{
    private CursorUtil() {
    }
    
    @NonNull
    public static Cursor copyAndClose(@NonNull final Cursor cursor) {
        try {
            final MatrixCursor matrixCursor = new MatrixCursor(cursor.getColumnNames(), cursor.getCount());
            while (cursor.moveToNext()) {
                final Object[] array = new Object[cursor.getColumnCount()];
                for (int i = 0; i < cursor.getColumnCount(); ++i) {
                    final int type = cursor.getType(i);
                    if (type != 0) {
                        if (type != 1) {
                            if (type != 2) {
                                if (type != 3) {
                                    if (type != 4) {
                                        throw new IllegalStateException();
                                    }
                                    array[i] = cursor.getBlob(i);
                                }
                                else {
                                    array[i] = cursor.getString(i);
                                }
                            }
                            else {
                                array[i] = cursor.getDouble(i);
                            }
                        }
                        else {
                            array[i] = cursor.getLong(i);
                        }
                    }
                    else {
                        array[i] = null;
                    }
                }
                matrixCursor.addRow(array);
            }
            return (Cursor)matrixCursor;
        }
        finally {
            cursor.close();
        }
    }
    
    private static int findColumnIndexBySuffix(@NonNull final Cursor cursor, @NonNull final String s) {
        if (Build$VERSION.SDK_INT > 25) {
            return -1;
        }
        if (s.length() == 0) {
            return -1;
        }
        return findColumnIndexBySuffix(cursor.getColumnNames(), s);
    }
    
    @VisibleForTesting(otherwise = 2)
    static int findColumnIndexBySuffix(final String[] array, final String s) {
        final StringBuilder sb = new StringBuilder();
        sb.append(".");
        sb.append(s);
        final String string = sb.toString();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(".");
        sb2.append(s);
        sb2.append("`");
        final String string2 = sb2.toString();
        for (int i = 0; i < array.length; ++i) {
            final String s2 = array[i];
            if (s2.length() >= s.length() + 2) {
                if (s2.endsWith(string)) {
                    return i;
                }
                if (s2.charAt(0) == '`' && s2.endsWith(string2)) {
                    return i;
                }
            }
        }
        return -1;
    }
    
    public static int getColumnIndex(@NonNull final Cursor cursor, @NonNull final String str) {
        final int columnIndex = cursor.getColumnIndex(str);
        if (columnIndex >= 0) {
            return columnIndex;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("`");
        sb.append(str);
        sb.append("`");
        final int columnIndex2 = cursor.getColumnIndex(sb.toString());
        if (columnIndex2 >= 0) {
            return columnIndex2;
        }
        return findColumnIndexBySuffix(cursor, str);
    }
    
    public static int getColumnIndexOrThrow(@NonNull final Cursor cursor, @NonNull final String str) {
        final int columnIndex = getColumnIndex(cursor, str);
        if (columnIndex >= 0) {
            return columnIndex;
        }
        String string;
        try {
            string = Arrays.toString(cursor.getColumnNames());
        }
        catch (final Exception ex) {
            string = "";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("column '");
        sb.append(str);
        sb.append("' does not exist. Available columns: ");
        sb.append(string);
        throw new IllegalArgumentException(sb.toString());
    }
}

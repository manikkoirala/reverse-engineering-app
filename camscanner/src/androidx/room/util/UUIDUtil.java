// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room.util;

import java.nio.ByteBuffer;
import java.util.UUID;
import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public final class UUIDUtil
{
    private UUIDUtil() {
    }
    
    @NonNull
    public static UUID convertByteToUUID(@NonNull final byte[] array) {
        final ByteBuffer wrap = ByteBuffer.wrap(array);
        return new UUID(wrap.getLong(), wrap.getLong());
    }
    
    @NonNull
    public static byte[] convertUUIDToByte(@NonNull final UUID uuid) {
        final ByteBuffer wrap = ByteBuffer.wrap(new byte[16]);
        wrap.putLong(uuid.getMostSignificantBits());
        wrap.putLong(uuid.getLeastSignificantBits());
        return wrap.array();
    }
}

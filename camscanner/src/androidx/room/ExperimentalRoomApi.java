// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import androidx.annotation.RequiresOptIn;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target({ ElementType.TYPE, ElementType.METHOD })
@RequiresOptIn
public @interface ExperimentalRoomApi {
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import android.annotation.SuppressLint;
import java.util.Collections;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import java.util.concurrent.Executor;
import androidx.annotation.RestrictTo;
import android.content.Intent;
import java.util.Set;
import java.io.InputStream;
import java.util.concurrent.Callable;
import java.io.File;
import android.content.Context;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.room.migration.AutoMigrationSpec;
import java.util.List;

public class DatabaseConfiguration
{
    public final boolean allowDestructiveMigrationOnDowngrade;
    public final boolean allowMainThreadQueries;
    @NonNull
    public final List<AutoMigrationSpec> autoMigrationSpecs;
    @Nullable
    public final List<RoomDatabase.Callback> callbacks;
    @NonNull
    public final Context context;
    @Nullable
    public final String copyFromAssetPath;
    @Nullable
    public final File copyFromFile;
    @Nullable
    public final Callable<InputStream> copyFromInputStream;
    public final RoomDatabase.JournalMode journalMode;
    private final Set<Integer> mMigrationNotRequiredFrom;
    @NonNull
    public final RoomDatabase.MigrationContainer migrationContainer;
    public final boolean multiInstanceInvalidation;
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public final Intent multiInstanceInvalidationServiceIntent;
    @Nullable
    public final String name;
    @Nullable
    public final RoomDatabase.PrepackagedDatabaseCallback prepackagedDatabaseCallback;
    @NonNull
    public final Executor queryExecutor;
    public final boolean requireMigration;
    @NonNull
    public final SupportSQLiteOpenHelper.Factory sqliteOpenHelperFactory;
    @NonNull
    public final Executor transactionExecutor;
    @NonNull
    public final List<Object> typeConverters;
    
    @SuppressLint({ "LambdaLast" })
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public DatabaseConfiguration(@NonNull final Context context, @Nullable final String name, @NonNull final SupportSQLiteOpenHelper.Factory sqliteOpenHelperFactory, @NonNull final RoomDatabase.MigrationContainer migrationContainer, @Nullable final List<RoomDatabase.Callback> callbacks, final boolean allowMainThreadQueries, @NonNull final RoomDatabase.JournalMode journalMode, @NonNull final Executor queryExecutor, @NonNull final Executor transactionExecutor, @Nullable final Intent multiInstanceInvalidationServiceIntent, final boolean requireMigration, final boolean allowDestructiveMigrationOnDowngrade, @Nullable final Set<Integer> mMigrationNotRequiredFrom, @Nullable final String copyFromAssetPath, @Nullable final File copyFromFile, @Nullable final Callable<InputStream> copyFromInputStream, @Nullable final RoomDatabase.PrepackagedDatabaseCallback prepackagedDatabaseCallback, @Nullable final List<Object> list, @Nullable final List<AutoMigrationSpec> list2) {
        this.sqliteOpenHelperFactory = sqliteOpenHelperFactory;
        this.context = context;
        this.name = name;
        this.migrationContainer = migrationContainer;
        this.callbacks = callbacks;
        this.allowMainThreadQueries = allowMainThreadQueries;
        this.journalMode = journalMode;
        this.queryExecutor = queryExecutor;
        this.transactionExecutor = transactionExecutor;
        this.multiInstanceInvalidationServiceIntent = multiInstanceInvalidationServiceIntent;
        this.multiInstanceInvalidation = (multiInstanceInvalidationServiceIntent != null);
        this.requireMigration = requireMigration;
        this.allowDestructiveMigrationOnDowngrade = allowDestructiveMigrationOnDowngrade;
        this.mMigrationNotRequiredFrom = mMigrationNotRequiredFrom;
        this.copyFromAssetPath = copyFromAssetPath;
        this.copyFromFile = copyFromFile;
        this.copyFromInputStream = copyFromInputStream;
        this.prepackagedDatabaseCallback = prepackagedDatabaseCallback;
        List<Object> emptyList;
        if (list == null) {
            emptyList = Collections.emptyList();
        }
        else {
            emptyList = list;
        }
        this.typeConverters = emptyList;
        Object emptyList2;
        if (list2 == null) {
            emptyList2 = Collections.emptyList();
        }
        else {
            emptyList2 = list2;
        }
        this.autoMigrationSpecs = (List<AutoMigrationSpec>)emptyList2;
    }
    
    @Deprecated
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public DatabaseConfiguration(@NonNull final Context context, @Nullable final String s, @NonNull final SupportSQLiteOpenHelper.Factory factory, @NonNull final RoomDatabase.MigrationContainer migrationContainer, @Nullable final List<RoomDatabase.Callback> list, final boolean b, final RoomDatabase.JournalMode journalMode, @NonNull final Executor executor, @NonNull final Executor executor2, final boolean b2, final boolean b3, final boolean b4, @Nullable final Set<Integer> set) {
        this(context, s, factory, migrationContainer, list, b, journalMode, executor, executor2, b2, b3, b4, set, null, null, null, null, null, null);
    }
    
    @Deprecated
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public DatabaseConfiguration(@NonNull final Context context, @Nullable final String s, @NonNull final SupportSQLiteOpenHelper.Factory factory, @NonNull final RoomDatabase.MigrationContainer migrationContainer, @Nullable final List<RoomDatabase.Callback> list, final boolean b, final RoomDatabase.JournalMode journalMode, @NonNull final Executor executor, @NonNull final Executor executor2, final boolean b2, final boolean b3, final boolean b4, @Nullable final Set<Integer> set, @Nullable final String s2, @Nullable final File file) {
        this(context, s, factory, migrationContainer, list, b, journalMode, executor, executor2, b2, b3, b4, set, s2, file, null, null, null, null);
    }
    
    @Deprecated
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public DatabaseConfiguration(@NonNull final Context context, @Nullable final String s, @NonNull final SupportSQLiteOpenHelper.Factory factory, @NonNull final RoomDatabase.MigrationContainer migrationContainer, @Nullable final List<RoomDatabase.Callback> list, final boolean b, @NonNull final RoomDatabase.JournalMode journalMode, @NonNull final Executor executor, @NonNull final Executor executor2, final boolean b2, final boolean b3, final boolean b4, @Nullable final Set<Integer> set, @Nullable final String s2, @Nullable final File file, @Nullable final Callable<InputStream> callable) {
        this(context, s, factory, migrationContainer, list, b, journalMode, executor, executor2, b2, b3, b4, set, s2, file, callable, null, null, null);
    }
    
    @Deprecated
    @SuppressLint({ "LambdaLast" })
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public DatabaseConfiguration(@NonNull final Context context, @Nullable final String s, @NonNull final SupportSQLiteOpenHelper.Factory factory, @NonNull final RoomDatabase.MigrationContainer migrationContainer, @Nullable final List<RoomDatabase.Callback> list, final boolean b, @NonNull final RoomDatabase.JournalMode journalMode, @NonNull final Executor executor, @NonNull final Executor executor2, final boolean b2, final boolean b3, final boolean b4, @Nullable final Set<Integer> set, @Nullable final String s2, @Nullable final File file, @Nullable final Callable<InputStream> callable, @Nullable final RoomDatabase.PrepackagedDatabaseCallback prepackagedDatabaseCallback) {
        this(context, s, factory, migrationContainer, list, b, journalMode, executor, executor2, b2, b3, b4, set, s2, file, callable, prepackagedDatabaseCallback, null, null);
    }
    
    @Deprecated
    @SuppressLint({ "LambdaLast" })
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public DatabaseConfiguration(@NonNull final Context context, @Nullable final String s, @NonNull final SupportSQLiteOpenHelper.Factory factory, @NonNull final RoomDatabase.MigrationContainer migrationContainer, @Nullable final List<RoomDatabase.Callback> list, final boolean b, @NonNull final RoomDatabase.JournalMode journalMode, @NonNull final Executor executor, @NonNull final Executor executor2, final boolean b2, final boolean b3, final boolean b4, @Nullable final Set<Integer> set, @Nullable final String s2, @Nullable final File file, @Nullable final Callable<InputStream> callable, @Nullable final RoomDatabase.PrepackagedDatabaseCallback prepackagedDatabaseCallback, @Nullable final List<Object> list2) {
        this(context, s, factory, migrationContainer, list, b, journalMode, executor, executor2, b2, b3, b4, set, s2, file, callable, prepackagedDatabaseCallback, list2, null);
    }
    
    @Deprecated
    @SuppressLint({ "LambdaLast" })
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public DatabaseConfiguration(@NonNull final Context context, @Nullable final String s, @NonNull final SupportSQLiteOpenHelper.Factory factory, @NonNull final RoomDatabase.MigrationContainer migrationContainer, @Nullable final List<RoomDatabase.Callback> list, final boolean b, @NonNull final RoomDatabase.JournalMode journalMode, @NonNull final Executor executor, @NonNull final Executor executor2, final boolean b2, final boolean b3, final boolean b4, @Nullable final Set<Integer> set, @Nullable final String s2, @Nullable final File file, @Nullable final Callable<InputStream> callable, @Nullable final RoomDatabase.PrepackagedDatabaseCallback prepackagedDatabaseCallback, @Nullable final List<Object> list2, @Nullable final List<AutoMigrationSpec> list3) {
        Intent intent;
        if (b2) {
            intent = new Intent(context, (Class)MultiInstanceInvalidationService.class);
        }
        else {
            intent = null;
        }
        this(context, s, factory, migrationContainer, list, b, journalMode, executor, executor2, intent, b3, b4, set, s2, file, callable, prepackagedDatabaseCallback, list2, list3);
    }
    
    @Deprecated
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public DatabaseConfiguration(@NonNull final Context context, @Nullable final String s, @NonNull final SupportSQLiteOpenHelper.Factory factory, @NonNull final RoomDatabase.MigrationContainer migrationContainer, @Nullable final List<RoomDatabase.Callback> list, final boolean b, final RoomDatabase.JournalMode journalMode, @NonNull final Executor executor, final boolean b2, @Nullable final Set<Integer> set) {
        this(context, s, factory, migrationContainer, list, b, journalMode, executor, executor, false, b2, false, set, null, null, null, null, null, null);
    }
    
    public boolean isMigrationRequired(final int i, int n) {
        final boolean b = true;
        if (i > n) {
            n = 1;
        }
        else {
            n = 0;
        }
        if (n != 0 && this.allowDestructiveMigrationOnDowngrade) {
            return false;
        }
        if (this.requireMigration) {
            final Set<Integer> mMigrationNotRequiredFrom = this.mMigrationNotRequiredFrom;
            boolean b2 = b;
            if (mMigrationNotRequiredFrom == null) {
                return b2;
            }
            if (!mMigrationNotRequiredFrom.contains(i)) {
                b2 = b;
                return b2;
            }
        }
        return false;
    }
    
    @Deprecated
    public boolean isMigrationRequiredFrom(final int n) {
        return this.isMigrationRequired(n, n + 1);
    }
}

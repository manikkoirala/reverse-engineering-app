// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.CLASS)
@Target({})
public @interface BuiltInTypeConverters {
    State enums() default State.INHERITED;
    
    State uuid() default State.INHERITED;
    
    public enum State
    {
        private static final State[] $VALUES;
        
        DISABLED, 
        ENABLED, 
        INHERITED;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import androidx.annotation.NonNull;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

final class AutoClosingRoomOpenHelperFactory implements Factory
{
    @NonNull
    private final AutoCloser mAutoCloser;
    @NonNull
    private final Factory mDelegate;
    
    AutoClosingRoomOpenHelperFactory(@NonNull final Factory mDelegate, @NonNull final AutoCloser mAutoCloser) {
        this.mDelegate = mDelegate;
        this.mAutoCloser = mAutoCloser;
    }
    
    @NonNull
    public AutoClosingRoomOpenHelper create(@NonNull final Configuration configuration) {
        return new AutoClosingRoomOpenHelper(this.mDelegate.create(configuration), this.mAutoCloser);
    }
}

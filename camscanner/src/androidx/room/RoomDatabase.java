// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.util.TreeMap;
import androidx.sqlite.db.SupportSQLiteCompat;
import android.app.ActivityManager;
import androidx.annotation.RequiresApi;
import androidx.annotation.IntRange;
import android.annotation.SuppressLint;
import androidx.sqlite.db.framework.FrameworkSQLiteOpenHelperFactory;
import androidx.arch.core.executor.ArchTaskExecutor;
import java.util.HashSet;
import java.io.InputStream;
import java.io.File;
import android.content.Context;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import androidx.room.util.SneakyThrow;
import java.util.concurrent.Callable;
import androidx.sqlite.db.SimpleSQLiteQuery;
import android.os.CancellationSignal;
import android.database.Cursor;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.annotation.CallSuper;
import android.content.Intent;
import java.util.Iterator;
import java.util.BitSet;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteStatement;
import androidx.annotation.WorkerThread;
import androidx.arch.core.util.Function;
import android.os.Looper;
import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.Executor;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteDatabase;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.room.migration.AutoMigrationSpec;
import java.util.Map;
import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;

public abstract class RoomDatabase
{
    private static final String DB_IMPL_SUFFIX = "_Impl";
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static final int MAX_BIND_PARAMETER_CNT = 999;
    private boolean mAllowMainThreadQueries;
    @Nullable
    private AutoCloser mAutoCloser;
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected Map<Class<? extends AutoMigrationSpec>, AutoMigrationSpec> mAutoMigrationSpecs;
    private final Map<String, Object> mBackingFieldMap;
    @Deprecated
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    protected List<Callback> mCallbacks;
    private final ReentrantReadWriteLock mCloseLock;
    @Deprecated
    protected volatile SupportSQLiteDatabase mDatabase;
    private final InvalidationTracker mInvalidationTracker;
    private SupportSQLiteOpenHelper mOpenHelper;
    private Executor mQueryExecutor;
    private final ThreadLocal<Integer> mSuspendingTransactionId;
    private Executor mTransactionExecutor;
    private final Map<Class<?>, Object> mTypeConverters;
    boolean mWriteAheadLoggingEnabled;
    
    public RoomDatabase() {
        this.mCloseLock = new ReentrantReadWriteLock();
        this.mSuspendingTransactionId = new ThreadLocal<Integer>();
        this.mBackingFieldMap = Collections.synchronizedMap(new HashMap<String, Object>());
        this.mInvalidationTracker = this.createInvalidationTracker();
        this.mTypeConverters = new HashMap<Class<?>, Object>();
        this.mAutoMigrationSpecs = new HashMap<Class<? extends AutoMigrationSpec>, AutoMigrationSpec>();
    }
    
    private void internalBeginTransaction() {
        this.assertNotMainThread();
        final SupportSQLiteDatabase writableDatabase = this.mOpenHelper.getWritableDatabase();
        this.mInvalidationTracker.syncTriggers(writableDatabase);
        if (writableDatabase.isWriteAheadLoggingEnabled()) {
            writableDatabase.beginTransactionNonExclusive();
        }
        else {
            writableDatabase.beginTransaction();
        }
    }
    
    private void internalEndTransaction() {
        this.mOpenHelper.getWritableDatabase().endTransaction();
        if (!this.inTransaction()) {
            this.mInvalidationTracker.refreshVersionsAsync();
        }
    }
    
    private static boolean isMainThread() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }
    
    @Nullable
    private <T> T unwrapOpenHelper(final Class<T> clazz, final SupportSQLiteOpenHelper supportSQLiteOpenHelper) {
        if (clazz.isInstance(supportSQLiteOpenHelper)) {
            return (T)supportSQLiteOpenHelper;
        }
        if (supportSQLiteOpenHelper instanceof DelegatingOpenHelper) {
            return (T)this.unwrapOpenHelper((Class<Object>)clazz, ((DelegatingOpenHelper)supportSQLiteOpenHelper).getDelegate());
        }
        return null;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void assertNotMainThread() {
        if (this.mAllowMainThreadQueries) {
            return;
        }
        if (!isMainThread()) {
            return;
        }
        throw new IllegalStateException("Cannot access database on the main thread since it may potentially lock the UI for a long period of time.");
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void assertNotSuspendingTransaction() {
        if (!this.inTransaction() && this.mSuspendingTransactionId.get() != null) {
            throw new IllegalStateException("Cannot access database on a different coroutine context inherited from a suspending transaction.");
        }
    }
    
    @Deprecated
    public void beginTransaction() {
        this.assertNotMainThread();
        final AutoCloser mAutoCloser = this.mAutoCloser;
        if (mAutoCloser == null) {
            this.internalBeginTransaction();
        }
        else {
            mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Object>)new o\u30078oOO88(this));
        }
    }
    
    @WorkerThread
    public abstract void clearAllTables();
    
    public void close() {
        if (this.isOpen()) {
            final ReentrantReadWriteLock.WriteLock writeLock = this.mCloseLock.writeLock();
            writeLock.lock();
            try {
                this.mInvalidationTracker.stopMultiInstanceInvalidation();
                this.mOpenHelper.close();
            }
            finally {
                writeLock.unlock();
            }
        }
    }
    
    public SupportSQLiteStatement compileStatement(@NonNull final String s) {
        this.assertNotMainThread();
        this.assertNotSuspendingTransaction();
        return this.mOpenHelper.getWritableDatabase().compileStatement(s);
    }
    
    @NonNull
    protected abstract InvalidationTracker createInvalidationTracker();
    
    @NonNull
    protected abstract SupportSQLiteOpenHelper createOpenHelper(final DatabaseConfiguration p0);
    
    @Deprecated
    public void endTransaction() {
        final AutoCloser mAutoCloser = this.mAutoCloser;
        if (mAutoCloser == null) {
            this.internalEndTransaction();
        }
        else {
            mAutoCloser.executeRefCountingFunction((Function<SupportSQLiteDatabase, Object>)new o\u3007O(this));
        }
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public List<Migration> getAutoMigrations(@NonNull final Map<Class<? extends AutoMigrationSpec>, AutoMigrationSpec> map) {
        return Collections.emptyList();
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    Map<String, Object> getBackingFieldMap() {
        return this.mBackingFieldMap;
    }
    
    Lock getCloseLock() {
        return this.mCloseLock.readLock();
    }
    
    @NonNull
    public InvalidationTracker getInvalidationTracker() {
        return this.mInvalidationTracker;
    }
    
    @NonNull
    public SupportSQLiteOpenHelper getOpenHelper() {
        return this.mOpenHelper;
    }
    
    @NonNull
    public Executor getQueryExecutor() {
        return this.mQueryExecutor;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public Set<Class<? extends AutoMigrationSpec>> getRequiredAutoMigrationSpecs() {
        return Collections.emptySet();
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected Map<Class<?>, List<Class<?>>> getRequiredTypeConverters() {
        return Collections.emptyMap();
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    ThreadLocal<Integer> getSuspendingTransactionId() {
        return this.mSuspendingTransactionId;
    }
    
    @NonNull
    public Executor getTransactionExecutor() {
        return this.mTransactionExecutor;
    }
    
    @Nullable
    public <T> T getTypeConverter(@NonNull final Class<T> clazz) {
        return (T)this.mTypeConverters.get(clazz);
    }
    
    public boolean inTransaction() {
        return this.mOpenHelper.getWritableDatabase().inTransaction();
    }
    
    @CallSuper
    public void init(@NonNull final DatabaseConfiguration databaseConfiguration) {
        this.mOpenHelper = this.createOpenHelper(databaseConfiguration);
        final Set<Class<? extends AutoMigrationSpec>> requiredAutoMigrationSpecs = this.getRequiredAutoMigrationSpecs();
        final BitSet set = new BitSet();
        final Iterator<Class<? extends AutoMigrationSpec>> iterator = requiredAutoMigrationSpecs.iterator();
        while (true) {
            final boolean hasNext = iterator.hasNext();
            final int n = -1;
            if (!hasNext) {
                for (int i = databaseConfiguration.autoMigrationSpecs.size() - 1; i >= 0; --i) {
                    if (!set.get(i)) {
                        throw new IllegalArgumentException("Unexpected auto migration specs found. Annotate AutoMigrationSpec implementation with @ProvidedAutoMigrationSpec annotation or remove this spec from the builder.");
                    }
                }
                final Iterator<Migration> iterator2 = this.getAutoMigrations(this.mAutoMigrationSpecs).iterator();
                boolean b;
                while (true) {
                    final boolean hasNext2 = iterator2.hasNext();
                    b = false;
                    if (!hasNext2) {
                        break;
                    }
                    final Migration migration = iterator2.next();
                    if (databaseConfiguration.migrationContainer.getMigrations().containsKey(migration.startVersion)) {
                        continue;
                    }
                    databaseConfiguration.migrationContainer.addMigrations(migration);
                }
                final SQLiteCopyOpenHelper sqLiteCopyOpenHelper = this.unwrapOpenHelper(SQLiteCopyOpenHelper.class, this.mOpenHelper);
                if (sqLiteCopyOpenHelper != null) {
                    sqLiteCopyOpenHelper.setDatabaseConfiguration(databaseConfiguration);
                }
                final AutoClosingRoomOpenHelper autoClosingRoomOpenHelper = this.unwrapOpenHelper(AutoClosingRoomOpenHelper.class, this.mOpenHelper);
                if (autoClosingRoomOpenHelper != null) {
                    final AutoCloser autoCloser = autoClosingRoomOpenHelper.getAutoCloser();
                    this.mAutoCloser = autoCloser;
                    this.mInvalidationTracker.setAutoCloser(autoCloser);
                }
                if (databaseConfiguration.journalMode == JournalMode.WRITE_AHEAD_LOGGING) {
                    b = true;
                }
                this.mOpenHelper.setWriteAheadLoggingEnabled(b);
                this.mCallbacks = databaseConfiguration.callbacks;
                this.mQueryExecutor = databaseConfiguration.queryExecutor;
                this.mTransactionExecutor = new TransactionExecutor(databaseConfiguration.transactionExecutor);
                this.mAllowMainThreadQueries = databaseConfiguration.allowMainThreadQueries;
                this.mWriteAheadLoggingEnabled = b;
                final Intent multiInstanceInvalidationServiceIntent = databaseConfiguration.multiInstanceInvalidationServiceIntent;
                if (multiInstanceInvalidationServiceIntent != null) {
                    this.mInvalidationTracker.startMultiInstanceInvalidation(databaseConfiguration.context, databaseConfiguration.name, multiInstanceInvalidationServiceIntent);
                }
                final Map<Class<?>, List<Class<?>>> requiredTypeConverters = this.getRequiredTypeConverters();
                final BitSet set2 = new BitSet();
                for (final Map.Entry<Class, V> entry : requiredTypeConverters.entrySet()) {
                    final Class clazz = entry.getKey();
                Label_0572:
                    for (final Class obj : (List)entry.getValue()) {
                        int j = databaseConfiguration.typeConverters.size() - 1;
                        while (true) {
                            while (j >= 0) {
                                if (obj.isAssignableFrom(databaseConfiguration.typeConverters.get(j).getClass())) {
                                    set2.set(j);
                                    if (j >= 0) {
                                        this.mTypeConverters.put(obj, databaseConfiguration.typeConverters.get(j));
                                        continue Label_0572;
                                    }
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append("A required type converter (");
                                    sb.append(obj);
                                    sb.append(") for ");
                                    sb.append(clazz.getCanonicalName());
                                    sb.append(" is missing in the database configuration.");
                                    throw new IllegalArgumentException(sb.toString());
                                }
                                else {
                                    --j;
                                }
                            }
                            j = -1;
                            continue;
                        }
                    }
                }
                for (int k = databaseConfiguration.typeConverters.size() - 1; k >= 0; --k) {
                    if (!set2.get(k)) {
                        final Object value = databaseConfiguration.typeConverters.get(k);
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Unexpected type converter ");
                        sb2.append(value);
                        sb2.append(". Annotate TypeConverter class with @ProvidedTypeConverter annotation or remove this converter from the builder.");
                        throw new IllegalArgumentException(sb2.toString());
                    }
                }
                return;
            }
            final Class clazz2 = iterator.next();
            int bitIndex = databaseConfiguration.autoMigrationSpecs.size() - 1;
            int n2;
            while (true) {
                n2 = n;
                if (bitIndex < 0) {
                    break;
                }
                if (clazz2.isAssignableFrom(databaseConfiguration.autoMigrationSpecs.get(bitIndex).getClass())) {
                    set.set(bitIndex);
                    n2 = bitIndex;
                    break;
                }
                --bitIndex;
            }
            if (n2 < 0) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("A required auto migration spec (");
                sb3.append(clazz2.getCanonicalName());
                sb3.append(") is missing in the database configuration.");
                throw new IllegalArgumentException(sb3.toString());
            }
            this.mAutoMigrationSpecs.put(clazz2, databaseConfiguration.autoMigrationSpecs.get(n2));
        }
    }
    
    protected void internalInitInvalidationTracker(@NonNull final SupportSQLiteDatabase supportSQLiteDatabase) {
        this.mInvalidationTracker.internalInit(supportSQLiteDatabase);
    }
    
    public boolean isOpen() {
        final AutoCloser mAutoCloser = this.mAutoCloser;
        if (mAutoCloser != null) {
            return mAutoCloser.isActive();
        }
        final SupportSQLiteDatabase mDatabase = this.mDatabase;
        return mDatabase != null && mDatabase.isOpen();
    }
    
    @NonNull
    public Cursor query(@NonNull final SupportSQLiteQuery supportSQLiteQuery) {
        return this.query(supportSQLiteQuery, null);
    }
    
    @NonNull
    public Cursor query(@NonNull final SupportSQLiteQuery supportSQLiteQuery, @Nullable final CancellationSignal cancellationSignal) {
        this.assertNotMainThread();
        this.assertNotSuspendingTransaction();
        if (cancellationSignal != null) {
            return this.mOpenHelper.getWritableDatabase().query(supportSQLiteQuery, cancellationSignal);
        }
        return this.mOpenHelper.getWritableDatabase().query(supportSQLiteQuery);
    }
    
    @NonNull
    public Cursor query(@NonNull final String s, @Nullable final Object[] array) {
        return this.mOpenHelper.getWritableDatabase().query(new SimpleSQLiteQuery(s, array));
    }
    
    public <V> V runInTransaction(@NonNull final Callable<V> callable) {
        this.beginTransaction();
        try {
            try {
                final V call = callable.call();
                this.setTransactionSuccessful();
                this.endTransaction();
                return call;
            }
            finally {}
        }
        catch (final Exception ex) {
            SneakyThrow.reThrow(ex);
            this.endTransaction();
            return null;
        }
        catch (final RuntimeException ex2) {
            throw ex2;
        }
        this.endTransaction();
    }
    
    public void runInTransaction(@NonNull final Runnable runnable) {
        this.beginTransaction();
        try {
            runnable.run();
            this.setTransactionSuccessful();
        }
        finally {
            this.endTransaction();
        }
    }
    
    @Deprecated
    public void setTransactionSuccessful() {
        this.mOpenHelper.getWritableDatabase().setTransactionSuccessful();
    }
    
    public static class Builder<T extends RoomDatabase>
    {
        private boolean mAllowDestructiveMigrationOnDowngrade;
        private boolean mAllowMainThreadQueries;
        private TimeUnit mAutoCloseTimeUnit;
        private long mAutoCloseTimeout;
        private List<AutoMigrationSpec> mAutoMigrationSpecs;
        private ArrayList<Callback> mCallbacks;
        private final Context mContext;
        private String mCopyFromAssetPath;
        private File mCopyFromFile;
        private Callable<InputStream> mCopyFromInputStream;
        private final Class<T> mDatabaseClass;
        private SupportSQLiteOpenHelper.Factory mFactory;
        private JournalMode mJournalMode;
        private final MigrationContainer mMigrationContainer;
        private Set<Integer> mMigrationStartAndEndVersions;
        private Set<Integer> mMigrationsNotRequiredFrom;
        private Intent mMultiInstanceInvalidationIntent;
        private final String mName;
        private PrepackagedDatabaseCallback mPrepackagedDatabaseCallback;
        private QueryCallback mQueryCallback;
        private Executor mQueryCallbackExecutor;
        private Executor mQueryExecutor;
        private boolean mRequireMigration;
        private Executor mTransactionExecutor;
        private List<Object> mTypeConverters;
        
        Builder(@NonNull final Context mContext, @NonNull final Class<T> mDatabaseClass, @Nullable final String mName) {
            this.mAutoCloseTimeout = -1L;
            this.mContext = mContext;
            this.mDatabaseClass = mDatabaseClass;
            this.mName = mName;
            this.mJournalMode = JournalMode.AUTOMATIC;
            this.mRequireMigration = true;
            this.mMigrationContainer = new MigrationContainer();
        }
        
        @NonNull
        public Builder<T> addAutoMigrationSpec(@NonNull final AutoMigrationSpec autoMigrationSpec) {
            if (this.mAutoMigrationSpecs == null) {
                this.mAutoMigrationSpecs = new ArrayList<AutoMigrationSpec>();
            }
            this.mAutoMigrationSpecs.add(autoMigrationSpec);
            return this;
        }
        
        @NonNull
        public Builder<T> addCallback(@NonNull final Callback e) {
            if (this.mCallbacks == null) {
                this.mCallbacks = new ArrayList<Callback>();
            }
            this.mCallbacks.add(e);
            return this;
        }
        
        @NonNull
        public Builder<T> addMigrations(@NonNull final Migration... array) {
            if (this.mMigrationStartAndEndVersions == null) {
                this.mMigrationStartAndEndVersions = new HashSet<Integer>();
            }
            for (final Migration migration : array) {
                this.mMigrationStartAndEndVersions.add(migration.startVersion);
                this.mMigrationStartAndEndVersions.add(migration.endVersion);
            }
            this.mMigrationContainer.addMigrations(array);
            return this;
        }
        
        @NonNull
        public Builder<T> addTypeConverter(@NonNull final Object o) {
            if (this.mTypeConverters == null) {
                this.mTypeConverters = new ArrayList<Object>();
            }
            this.mTypeConverters.add(o);
            return this;
        }
        
        @NonNull
        public Builder<T> allowMainThreadQueries() {
            this.mAllowMainThreadQueries = true;
            return this;
        }
        
        @SuppressLint({ "RestrictedApi" })
        @NonNull
        public T build() {
            if (this.mContext == null) {
                throw new IllegalArgumentException("Cannot provide null context for the database.");
            }
            if (this.mDatabaseClass != null) {
                final Executor mQueryExecutor = this.mQueryExecutor;
                if (mQueryExecutor == null && this.mTransactionExecutor == null) {
                    final Executor ioThreadExecutor = ArchTaskExecutor.getIOThreadExecutor();
                    this.mTransactionExecutor = ioThreadExecutor;
                    this.mQueryExecutor = ioThreadExecutor;
                }
                else if (mQueryExecutor != null && this.mTransactionExecutor == null) {
                    this.mTransactionExecutor = mQueryExecutor;
                }
                else if (mQueryExecutor == null) {
                    final Executor mTransactionExecutor = this.mTransactionExecutor;
                    if (mTransactionExecutor != null) {
                        this.mQueryExecutor = mTransactionExecutor;
                    }
                }
                final Set<Integer> mMigrationStartAndEndVersions = this.mMigrationStartAndEndVersions;
                if (mMigrationStartAndEndVersions != null && this.mMigrationsNotRequiredFrom != null) {
                    for (final Integer obj : mMigrationStartAndEndVersions) {
                        if (!this.mMigrationsNotRequiredFrom.contains(obj)) {
                            continue;
                        }
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Inconsistency detected. A Migration was supplied to addMigration(Migration... migrations) that has a start or end version equal to a start version supplied to fallbackToDestructiveMigrationFrom(int... startVersions). Start version: ");
                        sb.append(obj);
                        throw new IllegalArgumentException(sb.toString());
                    }
                }
                SupportSQLiteOpenHelper.Factory mFactory;
                if ((mFactory = this.mFactory) == null) {
                    mFactory = new FrameworkSQLiteOpenHelperFactory();
                }
                final long mAutoCloseTimeout = this.mAutoCloseTimeout;
                SupportSQLiteOpenHelper.Factory factory = mFactory;
                if (mAutoCloseTimeout > 0L) {
                    if (this.mName == null) {
                        throw new IllegalArgumentException("Cannot create auto-closing database for an in-memory database.");
                    }
                    factory = new AutoClosingRoomOpenHelperFactory(mFactory, new AutoCloser(mAutoCloseTimeout, this.mAutoCloseTimeUnit, this.mTransactionExecutor));
                }
                final String mCopyFromAssetPath = this.mCopyFromAssetPath;
                SupportSQLiteOpenHelper.Factory factory2 = null;
                Label_0398: {
                    if (mCopyFromAssetPath == null && this.mCopyFromFile == null) {
                        factory2 = factory;
                        if (this.mCopyFromInputStream == null) {
                            break Label_0398;
                        }
                    }
                    if (this.mName == null) {
                        throw new IllegalArgumentException("Cannot create from asset or file for an in-memory database.");
                    }
                    int n = 0;
                    int n2;
                    if (mCopyFromAssetPath == null) {
                        n2 = 0;
                    }
                    else {
                        n2 = 1;
                    }
                    final File mCopyFromFile = this.mCopyFromFile;
                    int n3;
                    if (mCopyFromFile == null) {
                        n3 = 0;
                    }
                    else {
                        n3 = 1;
                    }
                    final Callable<InputStream> mCopyFromInputStream = this.mCopyFromInputStream;
                    if (mCopyFromInputStream != null) {
                        n = 1;
                    }
                    if (n2 + n3 + n != 1) {
                        throw new IllegalArgumentException("More than one of createFromAsset(), createFromInputStream(), and createFromFile() were called on this Builder, but the database can only be created using one of the three configurations.");
                    }
                    factory2 = new SQLiteCopyOpenHelperFactory(mCopyFromAssetPath, mCopyFromFile, mCopyFromInputStream, factory);
                }
                final QueryCallback mQueryCallback = this.mQueryCallback;
                SupportSQLiteOpenHelper.Factory factory3;
                if (mQueryCallback != null) {
                    factory3 = new QueryInterceptorOpenHelperFactory(factory2, mQueryCallback, this.mQueryCallbackExecutor);
                }
                else {
                    factory3 = factory2;
                }
                final Context mContext = this.mContext;
                final DatabaseConfiguration databaseConfiguration = new DatabaseConfiguration(mContext, this.mName, factory3, this.mMigrationContainer, this.mCallbacks, this.mAllowMainThreadQueries, this.mJournalMode.resolve(mContext), this.mQueryExecutor, this.mTransactionExecutor, this.mMultiInstanceInvalidationIntent, this.mRequireMigration, this.mAllowDestructiveMigrationOnDowngrade, this.mMigrationsNotRequiredFrom, this.mCopyFromAssetPath, this.mCopyFromFile, this.mCopyFromInputStream, this.mPrepackagedDatabaseCallback, this.mTypeConverters, this.mAutoMigrationSpecs);
                final RoomDatabase roomDatabase = Room.getGeneratedImplementation(this.mDatabaseClass, "_Impl");
                roomDatabase.init(databaseConfiguration);
                return (T)roomDatabase;
            }
            throw new IllegalArgumentException("Must provide an abstract class that extends RoomDatabase");
        }
        
        @NonNull
        public Builder<T> createFromAsset(@NonNull final String mCopyFromAssetPath) {
            this.mCopyFromAssetPath = mCopyFromAssetPath;
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder<T> createFromAsset(@NonNull final String mCopyFromAssetPath, @NonNull final PrepackagedDatabaseCallback mPrepackagedDatabaseCallback) {
            this.mPrepackagedDatabaseCallback = mPrepackagedDatabaseCallback;
            this.mCopyFromAssetPath = mCopyFromAssetPath;
            return this;
        }
        
        @NonNull
        public Builder<T> createFromFile(@NonNull final File mCopyFromFile) {
            this.mCopyFromFile = mCopyFromFile;
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle", "StreamFiles" })
        @NonNull
        public Builder<T> createFromFile(@NonNull final File mCopyFromFile, @NonNull final PrepackagedDatabaseCallback mPrepackagedDatabaseCallback) {
            this.mPrepackagedDatabaseCallback = mPrepackagedDatabaseCallback;
            this.mCopyFromFile = mCopyFromFile;
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public Builder<T> createFromInputStream(@NonNull final Callable<InputStream> mCopyFromInputStream) {
            this.mCopyFromInputStream = mCopyFromInputStream;
            return this;
        }
        
        @SuppressLint({ "BuilderSetStyle", "LambdaLast" })
        @NonNull
        public Builder<T> createFromInputStream(@NonNull final Callable<InputStream> mCopyFromInputStream, @NonNull final PrepackagedDatabaseCallback mPrepackagedDatabaseCallback) {
            this.mPrepackagedDatabaseCallback = mPrepackagedDatabaseCallback;
            this.mCopyFromInputStream = mCopyFromInputStream;
            return this;
        }
        
        @NonNull
        public Builder<T> enableMultiInstanceInvalidation() {
            Intent mMultiInstanceInvalidationIntent;
            if (this.mName != null) {
                mMultiInstanceInvalidationIntent = new Intent(this.mContext, (Class)MultiInstanceInvalidationService.class);
            }
            else {
                mMultiInstanceInvalidationIntent = null;
            }
            this.mMultiInstanceInvalidationIntent = mMultiInstanceInvalidationIntent;
            return this;
        }
        
        @NonNull
        public Builder<T> fallbackToDestructiveMigration() {
            this.mRequireMigration = false;
            this.mAllowDestructiveMigrationOnDowngrade = true;
            return this;
        }
        
        @NonNull
        public Builder<T> fallbackToDestructiveMigrationFrom(final int... array) {
            if (this.mMigrationsNotRequiredFrom == null) {
                this.mMigrationsNotRequiredFrom = new HashSet<Integer>(array.length);
            }
            for (int length = array.length, i = 0; i < length; ++i) {
                this.mMigrationsNotRequiredFrom.add(array[i]);
            }
            return this;
        }
        
        @NonNull
        public Builder<T> fallbackToDestructiveMigrationOnDowngrade() {
            this.mRequireMigration = true;
            this.mAllowDestructiveMigrationOnDowngrade = true;
            return this;
        }
        
        @NonNull
        public Builder<T> openHelperFactory(@Nullable final SupportSQLiteOpenHelper.Factory mFactory) {
            this.mFactory = mFactory;
            return this;
        }
        
        @NonNull
        @ExperimentalRoomApi
        public Builder<T> setAutoCloseTimeout(@IntRange(from = 0L) final long mAutoCloseTimeout, @NonNull final TimeUnit mAutoCloseTimeUnit) {
            if (mAutoCloseTimeout >= 0L) {
                this.mAutoCloseTimeout = mAutoCloseTimeout;
                this.mAutoCloseTimeUnit = mAutoCloseTimeUnit;
                return this;
            }
            throw new IllegalArgumentException("autoCloseTimeout must be >= 0");
        }
        
        @NonNull
        public Builder<T> setJournalMode(@NonNull final JournalMode mJournalMode) {
            this.mJournalMode = mJournalMode;
            return this;
        }
        
        @NonNull
        @ExperimentalRoomApi
        public Builder<T> setMultiInstanceInvalidationServiceIntent(@NonNull Intent mMultiInstanceInvalidationIntent) {
            if (this.mName == null) {
                mMultiInstanceInvalidationIntent = null;
            }
            this.mMultiInstanceInvalidationIntent = mMultiInstanceInvalidationIntent;
            return this;
        }
        
        @NonNull
        public Builder<T> setQueryCallback(@NonNull final QueryCallback mQueryCallback, @NonNull final Executor mQueryCallbackExecutor) {
            this.mQueryCallback = mQueryCallback;
            this.mQueryCallbackExecutor = mQueryCallbackExecutor;
            return this;
        }
        
        @NonNull
        public Builder<T> setQueryExecutor(@NonNull final Executor mQueryExecutor) {
            this.mQueryExecutor = mQueryExecutor;
            return this;
        }
        
        @NonNull
        public Builder<T> setTransactionExecutor(@NonNull final Executor mTransactionExecutor) {
            this.mTransactionExecutor = mTransactionExecutor;
            return this;
        }
    }
    
    public abstract static class Callback
    {
        public void onCreate(@NonNull final SupportSQLiteDatabase supportSQLiteDatabase) {
        }
        
        public void onDestructiveMigration(@NonNull final SupportSQLiteDatabase supportSQLiteDatabase) {
        }
        
        public void onOpen(@NonNull final SupportSQLiteDatabase supportSQLiteDatabase) {
        }
    }
    
    public enum JournalMode
    {
        private static final JournalMode[] $VALUES;
        
        AUTOMATIC, 
        TRUNCATE, 
        @RequiresApi(16)
        WRITE_AHEAD_LOGGING;
        
        private static boolean isLowRamDevice(@NonNull final ActivityManager activityManager) {
            return SupportSQLiteCompat.Api19Impl.isLowRamDevice(activityManager);
        }
        
        JournalMode resolve(final Context context) {
            if (this != JournalMode.AUTOMATIC) {
                return this;
            }
            final ActivityManager activityManager = (ActivityManager)context.getSystemService("activity");
            if (activityManager != null && !isLowRamDevice(activityManager)) {
                return JournalMode.WRITE_AHEAD_LOGGING;
            }
            return JournalMode.TRUNCATE;
        }
    }
    
    public static class MigrationContainer
    {
        private HashMap<Integer, TreeMap<Integer, Migration>> mMigrations;
        
        public MigrationContainer() {
            this.mMigrations = new HashMap<Integer, TreeMap<Integer, Migration>>();
        }
        
        private void addMigration(final Migration migration) {
            final int startVersion = migration.startVersion;
            final int endVersion = migration.endVersion;
            TreeMap value;
            if ((value = this.mMigrations.get(startVersion)) == null) {
                value = new TreeMap();
                this.mMigrations.put(startVersion, value);
            }
            final Migration obj = (Migration)value.get(endVersion);
            if (obj != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Overriding migration ");
                sb.append(obj);
                sb.append(" with ");
                sb.append(migration);
            }
            value.put(endVersion, migration);
        }
        
        private List<Migration> findUpMigrationPath(final List<Migration> list, final boolean b, int i, final int n) {
            boolean b2;
        Label_0197_Outer:
            do {
                Label_0000: {
                    if (b) {
                        if (i >= n) {
                            return list;
                        }
                    }
                    else if (i <= n) {
                        return list;
                    }
                }
                final TreeMap treeMap = this.mMigrations.get(i);
                if (treeMap == null) {
                    return null;
                }
                Set set;
                if (b) {
                    set = treeMap.descendingKeySet();
                }
                else {
                    set = treeMap.keySet();
                }
                final Iterator iterator = set.iterator();
                while (true) {
                    int j;
                    int intValue;
                    do {
                        final boolean hasNext = iterator.hasNext();
                        b2 = false;
                        final int n2 = 0;
                        final int n3 = i;
                        if (!hasNext) {
                            i = n3;
                            continue Label_0000;
                        }
                        intValue = (int)iterator.next();
                        if (b) {
                            j = n2;
                            if (intValue > n) {
                                continue Label_0197_Outer;
                            }
                            j = n2;
                            if (intValue <= i) {
                                continue Label_0197_Outer;
                            }
                        }
                        else {
                            j = n2;
                            if (intValue < n) {
                                continue Label_0197_Outer;
                            }
                            j = n2;
                            if (intValue >= i) {
                                continue Label_0197_Outer;
                            }
                        }
                        j = 1;
                    } while (j == 0);
                    list.add((Migration)treeMap.get(intValue));
                    final int n3 = intValue;
                    b2 = true;
                    continue;
                }
            } while (b2);
            return null;
        }
        
        public void addMigrations(@NonNull final List<Migration> list) {
            final Iterator<Migration> iterator = list.iterator();
            while (iterator.hasNext()) {
                this.addMigration(iterator.next());
            }
        }
        
        public void addMigrations(@NonNull final Migration... array) {
            for (int length = array.length, i = 0; i < length; ++i) {
                this.addMigration(array[i]);
            }
        }
        
        @Nullable
        public List<Migration> findMigrationPath(final int n, final int n2) {
            if (n == n2) {
                return Collections.emptyList();
            }
            return this.findUpMigrationPath(new ArrayList<Migration>(), n2 > n, n, n2);
        }
        
        @NonNull
        public Map<Integer, Map<Integer, Migration>> getMigrations() {
            return Collections.unmodifiableMap((Map<? extends Integer, ? extends Map<Integer, Migration>>)this.mMigrations);
        }
    }
    
    public abstract static class PrepackagedDatabaseCallback
    {
        public void onOpenPrepackagedDatabase(@NonNull final SupportSQLiteDatabase supportSQLiteDatabase) {
        }
    }
    
    public interface QueryCallback
    {
        void onQuery(@NonNull final String p0, @NonNull final List<Object> p1);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import androidx.annotation.NonNull;
import java.util.ArrayDeque;
import java.util.concurrent.Executor;

class TransactionExecutor implements Executor
{
    private Runnable mActive;
    private final Executor mExecutor;
    private final ArrayDeque<Runnable> mTasks;
    
    TransactionExecutor(@NonNull final Executor mExecutor) {
        this.mTasks = new ArrayDeque<Runnable>();
        this.mExecutor = mExecutor;
    }
    
    @Override
    public void execute(final Runnable runnable) {
        synchronized (this) {
            this.mTasks.offer(new Runnable(this, runnable) {
                final TransactionExecutor this$0;
                final Runnable val$command;
                
                @Override
                public void run() {
                    try {
                        this.val$command.run();
                    }
                    finally {
                        this.this$0.scheduleNext();
                    }
                }
            });
            if (this.mActive == null) {
                this.scheduleNext();
            }
        }
    }
    
    void scheduleNext() {
        synchronized (this) {
            final Runnable mActive = this.mTasks.poll();
            this.mActive = mActive;
            if (mActive != null) {
                this.mExecutor.execute(mActive);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room.paging;

import androidx.paging.PositionalDataSource$LoadRangeCallback;
import androidx.paging.PositionalDataSource$LoadRangeParams;
import java.util.Collections;
import androidx.paging.PositionalDataSource$LoadInitialCallback;
import androidx.paging.PositionalDataSource$LoadInitialParams;
import java.util.List;
import android.database.Cursor;
import androidx.sqlite.db.SupportSQLiteQuery;
import java.util.Set;
import androidx.annotation.NonNull;
import androidx.room.RoomSQLiteQuery;
import java.util.concurrent.atomic.AtomicBoolean;
import androidx.room.InvalidationTracker;
import androidx.room.RoomDatabase;
import androidx.annotation.RestrictTo;
import androidx.paging.PositionalDataSource;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public abstract class LimitOffsetDataSource<T> extends PositionalDataSource<T>
{
    private final String mCountQuery;
    private final RoomDatabase mDb;
    private final boolean mInTransaction;
    private final String mLimitOffsetQuery;
    private final InvalidationTracker.Observer mObserver;
    private final AtomicBoolean mRegisteredObserver;
    private final RoomSQLiteQuery mSourceQuery;
    
    protected LimitOffsetDataSource(@NonNull final RoomDatabase mDb, @NonNull final RoomSQLiteQuery mSourceQuery, final boolean mInTransaction, final boolean b, @NonNull final String... array) {
        this.mRegisteredObserver = new AtomicBoolean(false);
        this.mDb = mDb;
        this.mSourceQuery = mSourceQuery;
        this.mInTransaction = mInTransaction;
        final StringBuilder sb = new StringBuilder();
        sb.append("SELECT COUNT(*) FROM ( ");
        sb.append(mSourceQuery.getSql());
        sb.append(" )");
        this.mCountQuery = sb.toString();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("SELECT * FROM ( ");
        sb2.append(mSourceQuery.getSql());
        sb2.append(" ) LIMIT ? OFFSET ?");
        this.mLimitOffsetQuery = sb2.toString();
        this.mObserver = new InvalidationTracker.Observer(this, array) {
            final LimitOffsetDataSource this$0;
            
            @Override
            public void onInvalidated(@NonNull final Set<String> set) {
                this.this$0.invalidate();
            }
        };
        if (b) {
            this.registerObserverIfNecessary();
        }
    }
    
    protected LimitOffsetDataSource(@NonNull final RoomDatabase roomDatabase, @NonNull final RoomSQLiteQuery roomSQLiteQuery, final boolean b, @NonNull final String... array) {
        this(roomDatabase, roomSQLiteQuery, b, true, array);
    }
    
    protected LimitOffsetDataSource(@NonNull final RoomDatabase roomDatabase, @NonNull final SupportSQLiteQuery supportSQLiteQuery, final boolean b, final boolean b2, @NonNull final String... array) {
        this(roomDatabase, RoomSQLiteQuery.copyFrom(supportSQLiteQuery), b, b2, array);
    }
    
    protected LimitOffsetDataSource(@NonNull final RoomDatabase roomDatabase, @NonNull final SupportSQLiteQuery supportSQLiteQuery, final boolean b, @NonNull final String... array) {
        this(roomDatabase, RoomSQLiteQuery.copyFrom(supportSQLiteQuery), b, array);
    }
    
    private RoomSQLiteQuery getSQLiteQuery(final int n, final int n2) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire(this.mLimitOffsetQuery, this.mSourceQuery.getArgCount() + 2);
        acquire.copyArgumentsFrom(this.mSourceQuery);
        acquire.bindLong(acquire.getArgCount() - 1, n2);
        acquire.bindLong(acquire.getArgCount(), n);
        return acquire;
    }
    
    private void registerObserverIfNecessary() {
        if (this.mRegisteredObserver.compareAndSet(false, true)) {
            this.mDb.getInvalidationTracker().addWeakObserver(this.mObserver);
        }
    }
    
    @NonNull
    protected abstract List<T> convertRows(@NonNull final Cursor p0);
    
    public int countItems() {
        this.registerObserverIfNecessary();
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire(this.mCountQuery, this.mSourceQuery.getArgCount());
        acquire.copyArgumentsFrom(this.mSourceQuery);
        final Cursor query = this.mDb.query(acquire);
        try {
            if (query.moveToFirst()) {
                return query.getInt(0);
            }
            return 0;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    public boolean isInvalid() {
        this.registerObserverIfNecessary();
        this.mDb.getInvalidationTracker().refreshVersionsSync();
        return super.isInvalid();
    }
    
    public void loadInitial(@NonNull PositionalDataSource$LoadInitialParams query, @NonNull final PositionalDataSource$LoadInitialCallback<T> positionalDataSource$LoadInitialCallback) {
        this.registerObserverIfNecessary();
        final Object emptyList = Collections.emptyList();
        this.mDb.beginTransaction();
        final Cursor cursor = null;
        Object o = null;
        final PositionalDataSource$LoadInitialParams positionalDataSource$LoadInitialParams = null;
        RoomSQLiteQuery roomSQLiteQuery = null;
        Cursor cursor2 = null;
        Label_0159: {
            try {
                final int countItems = this.countItems();
                int computeInitialLoadPosition = 0;
                RoomSQLiteQuery sqLiteQuery = null;
                Label_0114: {
                    if (countItems != 0) {
                        computeInitialLoadPosition = computeInitialLoadPosition(query, countItems);
                        sqLiteQuery = this.getSQLiteQuery(computeInitialLoadPosition, computeInitialLoadSize(query, computeInitialLoadPosition, countItems));
                        query = positionalDataSource$LoadInitialParams;
                        try {
                            o = (query = (PositionalDataSource$LoadInitialParams)this.mDb.query(sqLiteQuery));
                            final Object convertRows = this.convertRows((Cursor)o);
                            query = (PositionalDataSource$LoadInitialParams)o;
                            this.mDb.setTransactionSuccessful();
                            query = (PositionalDataSource$LoadInitialParams)convertRows;
                            break Label_0114;
                        }
                        finally {
                            break Label_0159;
                        }
                    }
                    computeInitialLoadPosition = 0;
                    sqLiteQuery = null;
                    o = cursor;
                    query = (PositionalDataSource$LoadInitialParams)emptyList;
                }
                if (o != null) {
                    ((Cursor)o).close();
                }
                this.mDb.endTransaction();
                if (sqLiteQuery != null) {
                    sqLiteQuery.release();
                }
                positionalDataSource$LoadInitialCallback.onResult((List)query, computeInitialLoadPosition, countItems);
                return;
            }
            finally {
                roomSQLiteQuery = null;
                cursor2 = (Cursor)o;
            }
        }
        if (cursor2 != null) {
            cursor2.close();
        }
        this.mDb.endTransaction();
        if (roomSQLiteQuery != null) {
            roomSQLiteQuery.release();
        }
    }
    
    @NonNull
    public List<T> loadRange(final int n, final int n2) {
        final RoomSQLiteQuery sqLiteQuery = this.getSQLiteQuery(n, n2);
        if (this.mInTransaction) {
            this.mDb.beginTransaction();
            Cursor query = null;
            try {
                final Cursor cursor = query = this.mDb.query(sqLiteQuery);
                final List<T> convertRows = this.convertRows(cursor);
                query = cursor;
                this.mDb.setTransactionSuccessful();
                return convertRows;
            }
            finally {
                if (query != null) {
                    query.close();
                }
                this.mDb.endTransaction();
                sqLiteQuery.release();
            }
        }
        final Cursor query2 = this.mDb.query(sqLiteQuery);
        try {
            return this.convertRows(query2);
        }
        finally {
            query2.close();
            sqLiteQuery.release();
        }
    }
    
    public void loadRange(@NonNull final PositionalDataSource$LoadRangeParams positionalDataSource$LoadRangeParams, @NonNull final PositionalDataSource$LoadRangeCallback<T> positionalDataSource$LoadRangeCallback) {
        positionalDataSource$LoadRangeCallback.onResult((List)this.loadRange(positionalDataSource$LoadRangeParams.startPosition, positionalDataSource$LoadRangeParams.loadSize));
    }
}

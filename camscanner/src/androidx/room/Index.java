// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.CLASS)
@Target({})
public @interface Index {
    String name() default "";
    
    Order[] orders() default {};
    
    boolean unique() default false;
    
    String[] value();
    
    public enum Order
    {
        private static final Order[] $VALUES;
        
        ASC, 
        DESC;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.util.ArrayList;
import java.util.List;
import androidx.sqlite.db.SupportSQLiteProgram;

final class QueryInterceptorProgram implements SupportSQLiteProgram
{
    private List<Object> mBindArgsCache;
    
    QueryInterceptorProgram() {
        this.mBindArgsCache = new ArrayList<Object>();
    }
    
    private void saveArgsToCache(int i, final Object o) {
        final int n = i - 1;
        if (n >= this.mBindArgsCache.size()) {
            for (i = this.mBindArgsCache.size(); i <= n; ++i) {
                this.mBindArgsCache.add(null);
            }
        }
        this.mBindArgsCache.set(n, o);
    }
    
    @Override
    public void bindBlob(final int n, final byte[] array) {
        this.saveArgsToCache(n, array);
    }
    
    @Override
    public void bindDouble(final int n, final double d) {
        this.saveArgsToCache(n, d);
    }
    
    @Override
    public void bindLong(final int n, final long l) {
        this.saveArgsToCache(n, l);
    }
    
    @Override
    public void bindNull(final int n) {
        this.saveArgsToCache(n, null);
    }
    
    @Override
    public void bindString(final int n, final String s) {
        this.saveArgsToCache(n, s);
    }
    
    @Override
    public void clearBindings() {
        this.mBindArgsCache.clear();
    }
    
    @Override
    public void close() {
    }
    
    List<Object> getBindArgs() {
        return this.mBindArgsCache;
    }
}

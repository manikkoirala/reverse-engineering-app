// 
// Decompiled by Procyon v0.6.0
// 

package androidx.room;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.CLASS)
public @interface Update {
    Class<?> entity() default Object.class;
    
    @OnConflictStrategy
    int onConflict() default 3;
}

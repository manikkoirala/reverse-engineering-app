// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.result;

import androidx.annotation.NonNull;

public interface ActivityResultRegistryOwner
{
    @NonNull
    ActivityResultRegistry getActivityResultRegistry();
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.result;

import androidx.annotation.NonNull;
import androidx.activity.result.contract.ActivityResultContract;

public interface ActivityResultCaller
{
    @NonNull
     <I, O> ActivityResultLauncher<I> registerForActivityResult(@NonNull final ActivityResultContract<I, O> p0, @NonNull final ActivityResultCallback<O> p1);
    
    @NonNull
     <I, O> ActivityResultLauncher<I> registerForActivityResult(@NonNull final ActivityResultContract<I, O> p0, @NonNull final ActivityResultRegistry p1, @NonNull final ActivityResultCallback<O> p2);
}

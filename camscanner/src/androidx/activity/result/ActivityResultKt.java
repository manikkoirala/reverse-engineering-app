// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.result;

import android.content.Intent;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class ActivityResultKt
{
    public static final int component1(@NotNull final ActivityResult activityResult) {
        Intrinsics.checkNotNullParameter((Object)activityResult, "<this>");
        return activityResult.getResultCode();
    }
    
    public static final Intent component2(@NotNull final ActivityResult activityResult) {
        Intrinsics.checkNotNullParameter((Object)activityResult, "<this>");
        return activityResult.getData();
    }
}

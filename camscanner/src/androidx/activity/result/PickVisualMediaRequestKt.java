// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.result;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import androidx.activity.result.contract.ActivityResultContracts;
import kotlin.Metadata;

@Metadata
public final class PickVisualMediaRequestKt
{
    @NotNull
    public static final PickVisualMediaRequest PickVisualMediaRequest(@NotNull final ActivityResultContracts.PickVisualMedia.VisualMediaType mediaType) {
        Intrinsics.checkNotNullParameter((Object)mediaType, "mediaType");
        return new PickVisualMediaRequest.Builder().setMediaType(mediaType).build();
    }
}

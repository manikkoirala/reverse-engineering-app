// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.result;

import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;
import androidx.core.app.ActivityOptionsCompat;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class ActivityResultLauncherKt
{
    public static final void launch(@NotNull final ActivityResultLauncher<Void> activityResultLauncher, final ActivityOptionsCompat activityOptionsCompat) {
        Intrinsics.checkNotNullParameter((Object)activityResultLauncher, "<this>");
        activityResultLauncher.launch(null, activityOptionsCompat);
    }
    
    public static final void launchUnit(@NotNull final ActivityResultLauncher<Unit> activityResultLauncher, final ActivityOptionsCompat activityOptionsCompat) {
        Intrinsics.checkNotNullParameter((Object)activityResultLauncher, "<this>");
        activityResultLauncher.launch(Unit.\u3007080, activityOptionsCompat);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.result.contract;

import kotlin.jvm.internal.Intrinsics;
import android.content.Intent;
import org.jetbrains.annotations.NotNull;
import android.content.Context;
import kotlin.Metadata;

@Metadata
public abstract class ActivityResultContract<I, O>
{
    @NotNull
    public abstract Intent createIntent(@NotNull final Context p0, final I p1);
    
    public SynchronousResult<O> getSynchronousResult(@NotNull final Context context, final I n) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        return null;
    }
    
    public abstract O parseResult(final int p0, final Intent p1);
    
    @Metadata
    public static final class SynchronousResult<T>
    {
        private final T value;
        
        public SynchronousResult(final T value) {
            this.value = value;
        }
        
        public final T getValue() {
            return this.value;
        }
    }
}

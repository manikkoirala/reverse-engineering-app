// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.result.contract;

import android.graphics.Bitmap;
import androidx.activity.result.IntentSenderRequest;
import androidx.activity.result.ActivityResult;
import kotlin.collections.ArraysKt;
import kotlin.Pair;
import kotlin.TuplesKt;
import java.util.LinkedHashMap;
import kotlin.ranges.RangesKt;
import androidx.core.content.ContextCompat;
import kotlin.collections.MapsKt;
import java.util.Map;
import android.os.ext.SdkExtensions;
import kotlin.NoWhenBranchMatchedException;
import android.annotation.SuppressLint;
import android.provider.MediaStore;
import androidx.activity.result.PickVisualMediaRequest;
import android.os.Build$VERSION;
import android.content.ClipData;
import java.util.Collection;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import java.util.List;
import androidx.annotation.RequiresApi;
import androidx.annotation.CallSuper;
import android.os.Parcelable;
import kotlin.jvm.internal.Intrinsics;
import android.content.Intent;
import org.jetbrains.annotations.NotNull;
import android.content.Context;
import android.net.Uri;
import kotlin.Metadata;

@Metadata
public final class ActivityResultContracts
{
    private ActivityResultContracts() {
    }
    
    @Metadata
    public static class CaptureVideo extends ActivityResultContract<Uri, Boolean>
    {
        @CallSuper
        @NotNull
        @Override
        public Intent createIntent(@NotNull final Context context, @NotNull final Uri uri) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)uri, "input");
            final Intent putExtra = new Intent("android.media.action.VIDEO_CAPTURE").putExtra("output", (Parcelable)uri);
            Intrinsics.checkNotNullExpressionValue((Object)putExtra, "Intent(MediaStore.ACTION\u2026tore.EXTRA_OUTPUT, input)");
            return putExtra;
        }
        
        public final SynchronousResult<Boolean> getSynchronousResult(@NotNull final Context context, @NotNull final Uri uri) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)uri, "input");
            return null;
        }
        
        @NotNull
        @Override
        public final Boolean parseResult(final int n, final Intent intent) {
            return n == -1;
        }
    }
    
    @Metadata
    @RequiresApi(19)
    public static class CreateDocument extends ActivityResultContract<String, Uri>
    {
        @NotNull
        private final String mimeType;
        
        public CreateDocument() {
            this("*/*");
        }
        
        public CreateDocument(@NotNull final String mimeType) {
            Intrinsics.checkNotNullParameter((Object)mimeType, "mimeType");
            this.mimeType = mimeType;
        }
        
        @CallSuper
        @NotNull
        @Override
        public Intent createIntent(@NotNull final Context context, @NotNull final String s) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)s, "input");
            final Intent putExtra = new Intent("android.intent.action.CREATE_DOCUMENT").setType(this.mimeType).putExtra("android.intent.extra.TITLE", s);
            Intrinsics.checkNotNullExpressionValue((Object)putExtra, "Intent(Intent.ACTION_CRE\u2026ntent.EXTRA_TITLE, input)");
            return putExtra;
        }
        
        public final SynchronousResult<Uri> getSynchronousResult(@NotNull final Context context, @NotNull final String s) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)s, "input");
            return null;
        }
        
        @Override
        public final Uri parseResult(int n, Intent intent) {
            if (n == -1) {
                n = 1;
            }
            else {
                n = 0;
            }
            Uri data = null;
            if (n == 0) {
                intent = null;
            }
            if (intent != null) {
                data = intent.getData();
            }
            return data;
        }
    }
    
    @Metadata
    public static class GetContent extends ActivityResultContract<String, Uri>
    {
        @CallSuper
        @NotNull
        @Override
        public Intent createIntent(@NotNull final Context context, @NotNull final String type) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)type, "input");
            final Intent setType = new Intent("android.intent.action.GET_CONTENT").addCategory("android.intent.category.OPENABLE").setType(type);
            Intrinsics.checkNotNullExpressionValue((Object)setType, "Intent(Intent.ACTION_GET\u2026          .setType(input)");
            return setType;
        }
        
        public final SynchronousResult<Uri> getSynchronousResult(@NotNull final Context context, @NotNull final String s) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)s, "input");
            return null;
        }
        
        @Override
        public final Uri parseResult(int n, Intent intent) {
            if (n == -1) {
                n = 1;
            }
            else {
                n = 0;
            }
            Uri data = null;
            if (n == 0) {
                intent = null;
            }
            if (intent != null) {
                data = intent.getData();
            }
            return data;
        }
    }
    
    @Metadata
    @RequiresApi(18)
    public static class GetMultipleContents extends ActivityResultContract<String, List<Uri>>
    {
        @NotNull
        public static final Companion Companion;
        
        static {
            Companion = new Companion(null);
        }
        
        @CallSuper
        @NotNull
        @Override
        public Intent createIntent(@NotNull final Context context, @NotNull final String type) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)type, "input");
            final Intent putExtra = new Intent("android.intent.action.GET_CONTENT").addCategory("android.intent.category.OPENABLE").setType(type).putExtra("android.intent.extra.ALLOW_MULTIPLE", true);
            Intrinsics.checkNotNullExpressionValue((Object)putExtra, "Intent(Intent.ACTION_GET\u2026TRA_ALLOW_MULTIPLE, true)");
            return putExtra;
        }
        
        public final SynchronousResult<List<Uri>> getSynchronousResult(@NotNull final Context context, @NotNull final String s) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)s, "input");
            return null;
        }
        
        @NotNull
        @Override
        public final List<Uri> parseResult(int n, Intent intent) {
            if (n == -1) {
                n = 1;
            }
            else {
                n = 0;
            }
            if (n == 0) {
                intent = null;
            }
            if (intent != null) {
                final List<Uri> list = GetMultipleContents.Companion.getClipDataUris$activity_release(intent);
                if (list != null) {
                    return list;
                }
            }
            return CollectionsKt.\u300780\u3007808\u3007O();
        }
        
        @Metadata
        @RequiresApi(18)
        public static final class Companion
        {
            private Companion() {
            }
            
            @NotNull
            public final List<Uri> getClipDataUris$activity_release(@NotNull final Intent intent) {
                Intrinsics.checkNotNullParameter((Object)intent, "<this>");
                final LinkedHashSet c = new LinkedHashSet();
                final Uri data = intent.getData();
                if (data != null) {
                    c.add(data);
                }
                final ClipData clipData = intent.getClipData();
                if (clipData == null && c.isEmpty()) {
                    return CollectionsKt.\u300780\u3007808\u3007O();
                }
                if (clipData != null) {
                    for (int itemCount = clipData.getItemCount(), i = 0; i < itemCount; ++i) {
                        final Uri uri = clipData.getItemAt(i).getUri();
                        if (uri != null) {
                            c.add(uri);
                        }
                    }
                }
                return new ArrayList<Uri>(c);
            }
        }
    }
    
    @Metadata
    @RequiresApi(19)
    public static class OpenDocument extends ActivityResultContract<String[], Uri>
    {
        @CallSuper
        @NotNull
        @Override
        public Intent createIntent(@NotNull final Context context, @NotNull final String[] array) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)array, "input");
            final Intent setType = new Intent("android.intent.action.OPEN_DOCUMENT").putExtra("android.intent.extra.MIME_TYPES", array).setType("*/*");
            Intrinsics.checkNotNullExpressionValue((Object)setType, "Intent(Intent.ACTION_OPE\u2026          .setType(\"*/*\")");
            return setType;
        }
        
        public final SynchronousResult<Uri> getSynchronousResult(@NotNull final Context context, @NotNull final String[] array) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)array, "input");
            return null;
        }
        
        @Override
        public final Uri parseResult(int n, Intent intent) {
            if (n == -1) {
                n = 1;
            }
            else {
                n = 0;
            }
            Uri data = null;
            if (n == 0) {
                intent = null;
            }
            if (intent != null) {
                data = intent.getData();
            }
            return data;
        }
    }
    
    @Metadata
    @RequiresApi(21)
    public static class OpenDocumentTree extends ActivityResultContract<Uri, Uri>
    {
        @CallSuper
        @NotNull
        @Override
        public Intent createIntent(@NotNull final Context context, final Uri uri) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            final Intent intent = new Intent("android.intent.action.OPEN_DOCUMENT_TREE");
            if (Build$VERSION.SDK_INT >= 26 && uri != null) {
                intent.putExtra("android.provider.extra.INITIAL_URI", (Parcelable)uri);
            }
            return intent;
        }
        
        public final SynchronousResult<Uri> getSynchronousResult(@NotNull final Context context, final Uri uri) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            return null;
        }
        
        @Override
        public final Uri parseResult(int n, Intent intent) {
            if (n == -1) {
                n = 1;
            }
            else {
                n = 0;
            }
            Uri data = null;
            if (n == 0) {
                intent = null;
            }
            if (intent != null) {
                data = intent.getData();
            }
            return data;
        }
    }
    
    @Metadata
    @RequiresApi(19)
    public static class OpenMultipleDocuments extends ActivityResultContract<String[], List<Uri>>
    {
        @CallSuper
        @NotNull
        @Override
        public Intent createIntent(@NotNull final Context context, @NotNull final String[] array) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)array, "input");
            final Intent setType = new Intent("android.intent.action.OPEN_DOCUMENT").putExtra("android.intent.extra.MIME_TYPES", array).putExtra("android.intent.extra.ALLOW_MULTIPLE", true).setType("*/*");
            Intrinsics.checkNotNullExpressionValue((Object)setType, "Intent(Intent.ACTION_OPE\u2026          .setType(\"*/*\")");
            return setType;
        }
        
        public final SynchronousResult<List<Uri>> getSynchronousResult(@NotNull final Context context, @NotNull final String[] array) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)array, "input");
            return null;
        }
        
        @NotNull
        @Override
        public final List<Uri> parseResult(int n, Intent intent) {
            if (n == -1) {
                n = 1;
            }
            else {
                n = 0;
            }
            if (n == 0) {
                intent = null;
            }
            if (intent != null) {
                final List<Uri> list = GetMultipleContents.Companion.getClipDataUris$activity_release(intent);
                if (list != null) {
                    return list;
                }
            }
            return CollectionsKt.\u300780\u3007808\u3007O();
        }
    }
    
    @Metadata
    public static final class PickContact extends ActivityResultContract<Void, Uri>
    {
        @NotNull
        @Override
        public Intent createIntent(@NotNull final Context context, final Void void1) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            final Intent setType = new Intent("android.intent.action.PICK").setType("vnd.android.cursor.dir/contact");
            Intrinsics.checkNotNullExpressionValue((Object)setType, "Intent(Intent.ACTION_PIC\u2026ct.Contacts.CONTENT_TYPE)");
            return setType;
        }
        
        @Override
        public Uri parseResult(int n, Intent intent) {
            if (n == -1) {
                n = 1;
            }
            else {
                n = 0;
            }
            Uri data = null;
            if (n == 0) {
                intent = null;
            }
            if (intent != null) {
                data = intent.getData();
            }
            return data;
        }
    }
    
    @Metadata
    @RequiresApi(19)
    public static class PickMultipleVisualMedia extends ActivityResultContract<PickVisualMediaRequest, List<Uri>>
    {
        @NotNull
        public static final Companion Companion;
        private final int maxItems;
        
        static {
            Companion = new Companion(null);
        }
        
        public PickMultipleVisualMedia() {
            this(0, 1, null);
        }
        
        public PickMultipleVisualMedia(int maxItems) {
            this.maxItems = maxItems;
            final int n = 1;
            if (maxItems > 1) {
                maxItems = n;
            }
            else {
                maxItems = 0;
            }
            if (maxItems != 0) {
                return;
            }
            throw new IllegalArgumentException("Max items must be higher than 1".toString());
        }
        
        @SuppressLint({ "NewApi", "ClassVerificationFailure" })
        @CallSuper
        @NotNull
        @Override
        public Intent createIntent(@NotNull final Context context, @NotNull final PickVisualMediaRequest pickVisualMediaRequest) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)pickVisualMediaRequest, "input");
            final PickVisualMedia.Companion companion = PickVisualMedia.Companion;
            final boolean photoPickerAvailable = companion.isPhotoPickerAvailable();
            boolean b = true;
            Intent intent;
            if (photoPickerAvailable) {
                intent = new Intent("android.provider.action.PICK_IMAGES");
                intent.setType(companion.getVisualMimeType$activity_release(pickVisualMediaRequest.getMediaType()));
                if (this.maxItems > MediaStore.getPickImagesMaxLimit()) {
                    b = false;
                }
                if (!b) {
                    throw new IllegalArgumentException("Max items must be less or equals MediaStore.getPickImagesMaxLimit()".toString());
                }
                intent.putExtra("android.provider.extra.PICK_IMAGES_MAX", this.maxItems);
            }
            else {
                final Intent intent2 = new Intent("android.intent.action.OPEN_DOCUMENT");
                intent2.setType(companion.getVisualMimeType$activity_release(pickVisualMediaRequest.getMediaType()));
                intent2.putExtra("android.intent.extra.ALLOW_MULTIPLE", true);
                intent = intent2;
                if (intent2.getType() == null) {
                    intent2.setType("*/*");
                    intent2.putExtra("android.intent.extra.MIME_TYPES", new String[] { "image/*", "video/*" });
                    intent = intent2;
                }
            }
            return intent;
        }
        
        public final SynchronousResult<List<Uri>> getSynchronousResult(@NotNull final Context context, @NotNull final PickVisualMediaRequest pickVisualMediaRequest) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)pickVisualMediaRequest, "input");
            return null;
        }
        
        @NotNull
        @Override
        public final List<Uri> parseResult(int n, Intent intent) {
            if (n == -1) {
                n = 1;
            }
            else {
                n = 0;
            }
            if (n == 0) {
                intent = null;
            }
            if (intent != null) {
                final List<Uri> list = GetMultipleContents.Companion.getClipDataUris$activity_release(intent);
                if (list != null) {
                    return list;
                }
            }
            return CollectionsKt.\u300780\u3007808\u3007O();
        }
        
        @Metadata
        public static final class Companion
        {
            private Companion() {
            }
            
            @SuppressLint({ "NewApi", "ClassVerificationFailure" })
            public final int getMaxItems$activity_release() {
                int pickImagesMaxLimit;
                if (PickVisualMedia.Companion.isPhotoPickerAvailable()) {
                    pickImagesMaxLimit = MediaStore.getPickImagesMaxLimit();
                }
                else {
                    pickImagesMaxLimit = Integer.MAX_VALUE;
                }
                return pickImagesMaxLimit;
            }
        }
    }
    
    @Metadata
    public static class PickVisualMedia extends ActivityResultContract<PickVisualMediaRequest, Uri>
    {
        @NotNull
        public static final Companion Companion;
        
        static {
            Companion = new Companion(null);
        }
        
        @SuppressLint({ "ClassVerificationFailure", "NewApi" })
        public static final boolean isPhotoPickerAvailable() {
            return PickVisualMedia.Companion.isPhotoPickerAvailable();
        }
        
        @CallSuper
        @NotNull
        @Override
        public Intent createIntent(@NotNull final Context context, @NotNull final PickVisualMediaRequest pickVisualMediaRequest) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)pickVisualMediaRequest, "input");
            final Companion companion = PickVisualMedia.Companion;
            Intent intent;
            if (companion.isPhotoPickerAvailable()) {
                intent = new Intent("android.provider.action.PICK_IMAGES");
                intent.setType(companion.getVisualMimeType$activity_release(pickVisualMediaRequest.getMediaType()));
            }
            else {
                final Intent intent2 = new Intent("android.intent.action.OPEN_DOCUMENT");
                intent2.setType(companion.getVisualMimeType$activity_release(pickVisualMediaRequest.getMediaType()));
                intent = intent2;
                if (intent2.getType() == null) {
                    intent2.setType("*/*");
                    intent2.putExtra("android.intent.extra.MIME_TYPES", new String[] { "image/*", "video/*" });
                    intent = intent2;
                }
            }
            return intent;
        }
        
        public final SynchronousResult<Uri> getSynchronousResult(@NotNull final Context context, @NotNull final PickVisualMediaRequest pickVisualMediaRequest) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)pickVisualMediaRequest, "input");
            return null;
        }
        
        @Override
        public final Uri parseResult(int n, Intent intent) {
            if (n == -1) {
                n = 1;
            }
            else {
                n = 0;
            }
            Uri data = null;
            if (n == 0) {
                intent = null;
            }
            if (intent != null) {
                data = intent.getData();
            }
            return data;
        }
        
        @Metadata
        public static final class Companion
        {
            private Companion() {
            }
            
            public final String getVisualMimeType$activity_release(@NotNull final VisualMediaType visualMediaType) {
                Intrinsics.checkNotNullParameter((Object)visualMediaType, "input");
                String mimeType;
                if (visualMediaType instanceof ImageOnly) {
                    mimeType = "image/*";
                }
                else if (visualMediaType instanceof VideoOnly) {
                    mimeType = "video/*";
                }
                else if (visualMediaType instanceof SingleMimeType) {
                    mimeType = ((SingleMimeType)visualMediaType).getMimeType();
                }
                else {
                    if (!(visualMediaType instanceof ImageAndVideo)) {
                        throw new NoWhenBranchMatchedException();
                    }
                    mimeType = null;
                }
                return mimeType;
            }
            
            @SuppressLint({ "ClassVerificationFailure", "NewApi" })
            public final boolean isPhotoPickerAvailable() {
                final int sdk_INT = Build$VERSION.SDK_INT;
                boolean b = true;
                if (sdk_INT < 33) {
                    if (sdk_INT < 30 || SdkExtensions.getExtensionVersion(30) < 2) {
                        b = false;
                    }
                }
                return b;
            }
        }
        
        @Metadata
        public static final class ImageAndVideo implements VisualMediaType
        {
            @NotNull
            public static final ImageAndVideo INSTANCE;
            
            static {
                INSTANCE = new ImageAndVideo();
            }
            
            private ImageAndVideo() {
            }
        }
        
        @Metadata
        public static final class ImageOnly implements VisualMediaType
        {
            @NotNull
            public static final ImageOnly INSTANCE;
            
            static {
                INSTANCE = new ImageOnly();
            }
            
            private ImageOnly() {
            }
        }
        
        @Metadata
        public static final class SingleMimeType implements VisualMediaType
        {
            @NotNull
            private final String mimeType;
            
            public SingleMimeType(@NotNull final String mimeType) {
                Intrinsics.checkNotNullParameter((Object)mimeType, "mimeType");
                this.mimeType = mimeType;
            }
            
            @NotNull
            public final String getMimeType() {
                return this.mimeType;
            }
        }
        
        @Metadata
        public static final class VideoOnly implements VisualMediaType
        {
            @NotNull
            public static final VideoOnly INSTANCE;
            
            static {
                INSTANCE = new VideoOnly();
            }
            
            private VideoOnly() {
            }
        }
        
        @Metadata
        public interface VisualMediaType
        {
        }
    }
    
    @Metadata
    public static final class RequestMultiplePermissions extends ActivityResultContract<String[], Map<String, Boolean>>
    {
        @NotNull
        public static final String ACTION_REQUEST_PERMISSIONS = "androidx.activity.result.contract.action.REQUEST_PERMISSIONS";
        @NotNull
        public static final Companion Companion;
        @NotNull
        public static final String EXTRA_PERMISSIONS = "androidx.activity.result.contract.extra.PERMISSIONS";
        @NotNull
        public static final String EXTRA_PERMISSION_GRANT_RESULTS = "androidx.activity.result.contract.extra.PERMISSION_GRANT_RESULTS";
        
        static {
            Companion = new Companion(null);
        }
        
        @NotNull
        @Override
        public Intent createIntent(@NotNull final Context context, @NotNull final String[] array) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)array, "input");
            return RequestMultiplePermissions.Companion.createIntent$activity_release(array);
        }
        
        public SynchronousResult<Map<String, Boolean>> getSynchronousResult(@NotNull final Context context, @NotNull final String[] array) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)array, "input");
            final int length = array.length;
            final int n = 1;
            final int n2 = 0;
            if (length == 0) {
                return (SynchronousResult<Map<String, Boolean>>)new SynchronousResult(MapsKt.\u3007\u3007888());
            }
            final int length2 = array.length;
            int n3 = 0;
            int n4;
            while (true) {
                n4 = n;
                if (n3 >= length2) {
                    break;
                }
                if (ContextCompat.checkSelfPermission(context, array[n3]) != 0) {
                    n4 = 0;
                    break;
                }
                ++n3;
            }
            SynchronousResult<Map<String, Boolean>> synchronousResult;
            if (n4 != 0) {
                final LinkedHashMap linkedHashMap = new LinkedHashMap(RangesKt.\u3007o\u3007(MapsKt.O8(array.length), 16));
                for (int length3 = array.length, i = n2; i < length3; ++i) {
                    final Pair \u3007080 = TuplesKt.\u3007080((Object)array[i], (Object)Boolean.TRUE);
                    linkedHashMap.put(\u3007080.getFirst(), \u3007080.getSecond());
                }
                synchronousResult = (SynchronousResult<Map<String, Boolean>>)new SynchronousResult(linkedHashMap);
            }
            else {
                synchronousResult = null;
            }
            return synchronousResult;
        }
        
        @NotNull
        @Override
        public Map<String, Boolean> parseResult(int i, final Intent intent) {
            if (i != -1) {
                return MapsKt.\u3007\u3007888();
            }
            if (intent == null) {
                return MapsKt.\u3007\u3007888();
            }
            final String[] stringArrayExtra = intent.getStringArrayExtra("androidx.activity.result.contract.extra.PERMISSIONS");
            final int[] intArrayExtra = intent.getIntArrayExtra("androidx.activity.result.contract.extra.PERMISSION_GRANT_RESULTS");
            if (intArrayExtra != null && stringArrayExtra != null) {
                final ArrayList list = new ArrayList(intArrayExtra.length);
                int length;
                for (length = intArrayExtra.length, i = 0; i < length; ++i) {
                    list.add((Object)(intArrayExtra[i] == 0));
                }
                return MapsKt.Oooo8o0\u3007((Iterable)CollectionsKt.o\u3007o((Iterable)ArraysKt.o800o8O((Object[])stringArrayExtra), (Iterable)list));
            }
            return MapsKt.\u3007\u3007888();
        }
        
        @Metadata
        public static final class Companion
        {
            private Companion() {
            }
            
            @NotNull
            public final Intent createIntent$activity_release(@NotNull final String[] array) {
                Intrinsics.checkNotNullParameter((Object)array, "input");
                final Intent putExtra = new Intent("androidx.activity.result.contract.action.REQUEST_PERMISSIONS").putExtra("androidx.activity.result.contract.extra.PERMISSIONS", array);
                Intrinsics.checkNotNullExpressionValue((Object)putExtra, "Intent(ACTION_REQUEST_PE\u2026EXTRA_PERMISSIONS, input)");
                return putExtra;
            }
        }
    }
    
    @Metadata
    public static final class RequestPermission extends ActivityResultContract<String, Boolean>
    {
        @NotNull
        @Override
        public Intent createIntent(@NotNull final Context context, @NotNull final String s) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)s, "input");
            return RequestMultiplePermissions.Companion.createIntent$activity_release(new String[] { s });
        }
        
        public SynchronousResult<Boolean> getSynchronousResult(@NotNull final Context context, @NotNull final String s) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)s, "input");
            SynchronousResult<Boolean> synchronousResult;
            if (ContextCompat.checkSelfPermission(context, s) == 0) {
                synchronousResult = (SynchronousResult<Boolean>)new SynchronousResult(Boolean.TRUE);
            }
            else {
                synchronousResult = null;
            }
            return synchronousResult;
        }
        
        @NotNull
        @Override
        public Boolean parseResult(int i, final Intent intent) {
            if (intent != null && i == -1) {
                final int[] intArrayExtra = intent.getIntArrayExtra("androidx.activity.result.contract.extra.PERMISSION_GRANT_RESULTS");
                boolean b = false;
                if (intArrayExtra != null) {
                    final int length = intArrayExtra.length;
                    i = 0;
                    while (true) {
                        while (i < length) {
                            if (intArrayExtra[i] == 0) {
                                i = 1;
                                b = b;
                                if (i == 1) {
                                    b = true;
                                    return b;
                                }
                                return b;
                            }
                            else {
                                ++i;
                            }
                        }
                        i = 0;
                        continue;
                    }
                }
                return b;
            }
            return Boolean.FALSE;
        }
    }
    
    @Metadata
    public static final class StartActivityForResult extends ActivityResultContract<Intent, ActivityResult>
    {
        @NotNull
        public static final Companion Companion;
        @NotNull
        public static final String EXTRA_ACTIVITY_OPTIONS_BUNDLE = "androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE";
        
        static {
            Companion = new Companion(null);
        }
        
        @NotNull
        @Override
        public Intent createIntent(@NotNull final Context context, @NotNull final Intent intent) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)intent, "input");
            return intent;
        }
        
        @NotNull
        @Override
        public ActivityResult parseResult(final int n, final Intent intent) {
            return new ActivityResult(n, intent);
        }
        
        @Metadata
        public static final class Companion
        {
            private Companion() {
            }
        }
    }
    
    @Metadata
    public static final class StartIntentSenderForResult extends ActivityResultContract<IntentSenderRequest, ActivityResult>
    {
        @NotNull
        public static final String ACTION_INTENT_SENDER_REQUEST = "androidx.activity.result.contract.action.INTENT_SENDER_REQUEST";
        @NotNull
        public static final Companion Companion;
        @NotNull
        public static final String EXTRA_INTENT_SENDER_REQUEST = "androidx.activity.result.contract.extra.INTENT_SENDER_REQUEST";
        @NotNull
        public static final String EXTRA_SEND_INTENT_EXCEPTION = "androidx.activity.result.contract.extra.SEND_INTENT_EXCEPTION";
        
        static {
            Companion = new Companion(null);
        }
        
        @NotNull
        @Override
        public Intent createIntent(@NotNull final Context context, @NotNull final IntentSenderRequest intentSenderRequest) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)intentSenderRequest, "input");
            final Intent putExtra = new Intent("androidx.activity.result.contract.action.INTENT_SENDER_REQUEST").putExtra("androidx.activity.result.contract.extra.INTENT_SENDER_REQUEST", (Parcelable)intentSenderRequest);
            Intrinsics.checkNotNullExpressionValue((Object)putExtra, "Intent(ACTION_INTENT_SEN\u2026NT_SENDER_REQUEST, input)");
            return putExtra;
        }
        
        @NotNull
        @Override
        public ActivityResult parseResult(final int n, final Intent intent) {
            return new ActivityResult(n, intent);
        }
        
        @Metadata
        public static final class Companion
        {
            private Companion() {
            }
        }
    }
    
    @Metadata
    public static class TakePicture extends ActivityResultContract<Uri, Boolean>
    {
        @CallSuper
        @NotNull
        @Override
        public Intent createIntent(@NotNull final Context context, @NotNull final Uri uri) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)uri, "input");
            final Intent putExtra = new Intent("android.media.action.IMAGE_CAPTURE").putExtra("output", (Parcelable)uri);
            Intrinsics.checkNotNullExpressionValue((Object)putExtra, "Intent(MediaStore.ACTION\u2026tore.EXTRA_OUTPUT, input)");
            return putExtra;
        }
        
        public final SynchronousResult<Boolean> getSynchronousResult(@NotNull final Context context, @NotNull final Uri uri) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)uri, "input");
            return null;
        }
        
        @NotNull
        @Override
        public final Boolean parseResult(final int n, final Intent intent) {
            return n == -1;
        }
    }
    
    @Metadata
    public static class TakePicturePreview extends ActivityResultContract<Void, Bitmap>
    {
        @CallSuper
        @NotNull
        @Override
        public Intent createIntent(@NotNull final Context context, final Void void1) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            return new Intent("android.media.action.IMAGE_CAPTURE");
        }
        
        public final SynchronousResult<Bitmap> getSynchronousResult(@NotNull final Context context, final Void void1) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            return null;
        }
        
        @Override
        public final Bitmap parseResult(int n, Intent intent) {
            if (n == -1) {
                n = 1;
            }
            else {
                n = 0;
            }
            Bitmap bitmap = null;
            if (n == 0) {
                intent = null;
            }
            if (intent != null) {
                bitmap = (Bitmap)intent.getParcelableExtra("data");
            }
            return bitmap;
        }
    }
    
    @Metadata
    public static class TakeVideo extends ActivityResultContract<Uri, Bitmap>
    {
        @CallSuper
        @NotNull
        @Override
        public Intent createIntent(@NotNull final Context context, @NotNull final Uri uri) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)uri, "input");
            final Intent putExtra = new Intent("android.media.action.VIDEO_CAPTURE").putExtra("output", (Parcelable)uri);
            Intrinsics.checkNotNullExpressionValue((Object)putExtra, "Intent(MediaStore.ACTION\u2026tore.EXTRA_OUTPUT, input)");
            return putExtra;
        }
        
        public final SynchronousResult<Bitmap> getSynchronousResult(@NotNull final Context context, @NotNull final Uri uri) {
            Intrinsics.checkNotNullParameter((Object)context, "context");
            Intrinsics.checkNotNullParameter((Object)uri, "input");
            return null;
        }
        
        @Override
        public final Bitmap parseResult(int n, Intent intent) {
            if (n == -1) {
                n = 1;
            }
            else {
                n = 0;
            }
            Bitmap bitmap = null;
            if (n == 0) {
                intent = null;
            }
            if (intent != null) {
                bitmap = (Bitmap)intent.getParcelableExtra("data");
            }
            return bitmap;
        }
    }
}

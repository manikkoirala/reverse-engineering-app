// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.result;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import androidx.activity.result.contract.ActivityResultContracts;
import kotlin.Metadata;

@Metadata
public final class PickVisualMediaRequest
{
    @NotNull
    private ActivityResultContracts.PickVisualMedia.VisualMediaType mediaType;
    
    public PickVisualMediaRequest() {
        this.mediaType = ActivityResultContracts.PickVisualMedia.ImageAndVideo.INSTANCE;
    }
    
    @NotNull
    public final ActivityResultContracts.PickVisualMedia.VisualMediaType getMediaType() {
        return this.mediaType;
    }
    
    public final void setMediaType$activity_release(@NotNull final ActivityResultContracts.PickVisualMedia.VisualMediaType mediaType) {
        Intrinsics.checkNotNullParameter((Object)mediaType, "<set-?>");
        this.mediaType = mediaType;
    }
    
    @Metadata
    public static final class Builder
    {
        @NotNull
        private ActivityResultContracts.PickVisualMedia.VisualMediaType mediaType;
        
        public Builder() {
            this.mediaType = ActivityResultContracts.PickVisualMedia.ImageAndVideo.INSTANCE;
        }
        
        @NotNull
        public final PickVisualMediaRequest build() {
            final PickVisualMediaRequest pickVisualMediaRequest = new PickVisualMediaRequest();
            pickVisualMediaRequest.setMediaType$activity_release(this.mediaType);
            return pickVisualMediaRequest;
        }
        
        @NotNull
        public final Builder setMediaType(@NotNull final ActivityResultContracts.PickVisualMedia.VisualMediaType mediaType) {
            Intrinsics.checkNotNullParameter((Object)mediaType, "mediaType");
            this.mediaType = mediaType;
            return this;
        }
    }
}

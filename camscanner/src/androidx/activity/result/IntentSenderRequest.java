// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.result;

import android.app.PendingIntent;
import android.os.Parcel;
import android.content.IntentSender;
import androidx.annotation.Nullable;
import android.content.Intent;
import androidx.annotation.NonNull;
import android.os.Parcelable$Creator;
import android.annotation.SuppressLint;
import android.os.Parcelable;

@SuppressLint({ "BanParcelableUsage" })
public final class IntentSenderRequest implements Parcelable
{
    @NonNull
    public static final Parcelable$Creator<IntentSenderRequest> CREATOR;
    @Nullable
    private final Intent mFillInIntent;
    private final int mFlagsMask;
    private final int mFlagsValues;
    @NonNull
    private final IntentSender mIntentSender;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<IntentSenderRequest>() {
            public IntentSenderRequest createFromParcel(final Parcel parcel) {
                return new IntentSenderRequest(parcel);
            }
            
            public IntentSenderRequest[] newArray(final int n) {
                return new IntentSenderRequest[n];
            }
        };
    }
    
    IntentSenderRequest(@NonNull final IntentSender mIntentSender, @Nullable final Intent mFillInIntent, final int mFlagsMask, final int mFlagsValues) {
        this.mIntentSender = mIntentSender;
        this.mFillInIntent = mFillInIntent;
        this.mFlagsMask = mFlagsMask;
        this.mFlagsValues = mFlagsValues;
    }
    
    IntentSenderRequest(@NonNull final Parcel parcel) {
        this.mIntentSender = (IntentSender)parcel.readParcelable(IntentSender.class.getClassLoader());
        this.mFillInIntent = (Intent)parcel.readParcelable(Intent.class.getClassLoader());
        this.mFlagsMask = parcel.readInt();
        this.mFlagsValues = parcel.readInt();
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Nullable
    public Intent getFillInIntent() {
        return this.mFillInIntent;
    }
    
    public int getFlagsMask() {
        return this.mFlagsMask;
    }
    
    public int getFlagsValues() {
        return this.mFlagsValues;
    }
    
    @NonNull
    public IntentSender getIntentSender() {
        return this.mIntentSender;
    }
    
    public void writeToParcel(@NonNull final Parcel parcel, final int n) {
        parcel.writeParcelable((Parcelable)this.mIntentSender, n);
        parcel.writeParcelable((Parcelable)this.mFillInIntent, n);
        parcel.writeInt(this.mFlagsMask);
        parcel.writeInt(this.mFlagsValues);
    }
    
    public static final class Builder
    {
        private Intent mFillInIntent;
        private int mFlagsMask;
        private int mFlagsValues;
        private IntentSender mIntentSender;
        
        public Builder(@NonNull final PendingIntent pendingIntent) {
            this(pendingIntent.getIntentSender());
        }
        
        public Builder(@NonNull final IntentSender mIntentSender) {
            this.mIntentSender = mIntentSender;
        }
        
        @NonNull
        public IntentSenderRequest build() {
            return new IntentSenderRequest(this.mIntentSender, this.mFillInIntent, this.mFlagsMask, this.mFlagsValues);
        }
        
        @NonNull
        public Builder setFillInIntent(@Nullable final Intent mFillInIntent) {
            this.mFillInIntent = mFillInIntent;
            return this;
        }
        
        @NonNull
        public Builder setFlags(final int mFlagsValues, final int mFlagsMask) {
            this.mFlagsValues = mFlagsValues;
            this.mFlagsMask = mFlagsMask;
            return this;
        }
    }
}

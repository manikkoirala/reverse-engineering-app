// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.result;

import android.os.BaseBundle;
import java.util.Iterator;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleEventObserver;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import java.io.Serializable;
import java.util.Collection;
import androidx.core.app.ActivityOptionsCompat;
import androidx.annotation.NonNull;
import androidx.activity.result.contract.ActivityResultContract;
import android.annotation.SuppressLint;
import androidx.annotation.MainThread;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import android.content.Intent;
import java.util.HashMap;
import java.util.Random;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Map;

public abstract class ActivityResultRegistry
{
    private static final int INITIAL_REQUEST_CODE_VALUE = 65536;
    private static final String KEY_COMPONENT_ACTIVITY_LAUNCHED_KEYS = "KEY_COMPONENT_ACTIVITY_LAUNCHED_KEYS";
    private static final String KEY_COMPONENT_ACTIVITY_PENDING_RESULTS = "KEY_COMPONENT_ACTIVITY_PENDING_RESULT";
    private static final String KEY_COMPONENT_ACTIVITY_RANDOM_OBJECT = "KEY_COMPONENT_ACTIVITY_RANDOM_OBJECT";
    private static final String KEY_COMPONENT_ACTIVITY_REGISTERED_KEYS = "KEY_COMPONENT_ACTIVITY_REGISTERED_KEYS";
    private static final String KEY_COMPONENT_ACTIVITY_REGISTERED_RCS = "KEY_COMPONENT_ACTIVITY_REGISTERED_RCS";
    private static final String LOG_TAG = "ActivityResultRegistry";
    final transient Map<String, CallbackAndContract<?>> mKeyToCallback;
    private final Map<String, LifecycleContainer> mKeyToLifecycleContainers;
    final Map<String, Integer> mKeyToRc;
    ArrayList<String> mLaunchedKeys;
    final Map<String, Object> mParsedPendingResults;
    final Bundle mPendingResults;
    private Random mRandom;
    private final Map<Integer, String> mRcToKey;
    
    public ActivityResultRegistry() {
        this.mRandom = new Random();
        this.mRcToKey = new HashMap<Integer, String>();
        this.mKeyToRc = new HashMap<String, Integer>();
        this.mKeyToLifecycleContainers = new HashMap<String, LifecycleContainer>();
        this.mLaunchedKeys = new ArrayList<String>();
        this.mKeyToCallback = new HashMap<String, CallbackAndContract<?>>();
        this.mParsedPendingResults = new HashMap<String, Object>();
        this.mPendingResults = new Bundle();
    }
    
    private void bindRcKey(final int n, final String s) {
        this.mRcToKey.put(n, s);
        this.mKeyToRc.put(s, n);
    }
    
    private <O> void doDispatch(final String s, final int n, @Nullable final Intent intent, @Nullable final CallbackAndContract<O> callbackAndContract) {
        if (callbackAndContract != null && callbackAndContract.mCallback != null && this.mLaunchedKeys.contains(s)) {
            callbackAndContract.mCallback.onActivityResult(callbackAndContract.mContract.parseResult(n, intent));
            this.mLaunchedKeys.remove(s);
        }
        else {
            this.mParsedPendingResults.remove(s);
            this.mPendingResults.putParcelable(s, (Parcelable)new ActivityResult(n, intent));
        }
    }
    
    private int generateRandomNumber() {
        int n = this.mRandom.nextInt(2147418112);
        int i;
        while (true) {
            i = n + 65536;
            if (!this.mRcToKey.containsKey(i)) {
                break;
            }
            n = this.mRandom.nextInt(2147418112);
        }
        return i;
    }
    
    private void registerKey(final String s) {
        if (this.mKeyToRc.get(s) != null) {
            return;
        }
        this.bindRcKey(this.generateRandomNumber(), s);
    }
    
    @MainThread
    public final boolean dispatchResult(final int i, final int n, @Nullable final Intent intent) {
        final String s = this.mRcToKey.get(i);
        if (s == null) {
            return false;
        }
        this.doDispatch(s, n, intent, this.mKeyToCallback.get(s));
        return true;
    }
    
    @MainThread
    public final <O> boolean dispatchResult(final int i, @SuppressLint({ "UnknownNullness" }) final O o) {
        final String o2 = this.mRcToKey.get(i);
        if (o2 == null) {
            return false;
        }
        final CallbackAndContract callbackAndContract = this.mKeyToCallback.get(o2);
        if (callbackAndContract != null) {
            final ActivityResultCallback<O> mCallback = callbackAndContract.mCallback;
            if (mCallback != null) {
                if (this.mLaunchedKeys.remove(o2)) {
                    mCallback.onActivityResult((O)o);
                    return true;
                }
                return true;
            }
        }
        this.mPendingResults.remove(o2);
        this.mParsedPendingResults.put(o2, o);
        return true;
    }
    
    @MainThread
    public abstract <I, O> void onLaunch(final int p0, @NonNull final ActivityResultContract<I, O> p1, @SuppressLint({ "UnknownNullness" }) final I p2, @Nullable final ActivityOptionsCompat p3);
    
    public final void onRestoreInstanceState(@Nullable final Bundle bundle) {
        if (bundle == null) {
            return;
        }
        final ArrayList integerArrayList = bundle.getIntegerArrayList("KEY_COMPONENT_ACTIVITY_REGISTERED_RCS");
        final ArrayList stringArrayList = bundle.getStringArrayList("KEY_COMPONENT_ACTIVITY_REGISTERED_KEYS");
        if (stringArrayList != null) {
            if (integerArrayList != null) {
                this.mLaunchedKeys = bundle.getStringArrayList("KEY_COMPONENT_ACTIVITY_LAUNCHED_KEYS");
                this.mRandom = (Random)bundle.getSerializable("KEY_COMPONENT_ACTIVITY_RANDOM_OBJECT");
                this.mPendingResults.putAll(bundle.getBundle("KEY_COMPONENT_ACTIVITY_PENDING_RESULT"));
                for (int i = 0; i < stringArrayList.size(); ++i) {
                    final String s = stringArrayList.get(i);
                    if (this.mKeyToRc.containsKey(s)) {
                        final Integer n = this.mKeyToRc.remove(s);
                        if (!((BaseBundle)this.mPendingResults).containsKey(s)) {
                            this.mRcToKey.remove(n);
                        }
                    }
                    this.bindRcKey((int)integerArrayList.get(i), (String)stringArrayList.get(i));
                }
            }
        }
    }
    
    public final void onSaveInstanceState(@NonNull final Bundle bundle) {
        bundle.putIntegerArrayList("KEY_COMPONENT_ACTIVITY_REGISTERED_RCS", new ArrayList((Collection<? extends E>)this.mKeyToRc.values()));
        bundle.putStringArrayList("KEY_COMPONENT_ACTIVITY_REGISTERED_KEYS", new ArrayList((Collection<? extends E>)this.mKeyToRc.keySet()));
        bundle.putStringArrayList("KEY_COMPONENT_ACTIVITY_LAUNCHED_KEYS", new ArrayList((Collection<? extends E>)this.mLaunchedKeys));
        bundle.putBundle("KEY_COMPONENT_ACTIVITY_PENDING_RESULT", (Bundle)this.mPendingResults.clone());
        bundle.putSerializable("KEY_COMPONENT_ACTIVITY_RANDOM_OBJECT", (Serializable)this.mRandom);
    }
    
    @NonNull
    public final <I, O> ActivityResultLauncher<I> register(@NonNull final String s, @NonNull final ActivityResultContract<I, O> activityResultContract, @NonNull final ActivityResultCallback<O> activityResultCallback) {
        this.registerKey(s);
        this.mKeyToCallback.put(s, new CallbackAndContract<Object>((ActivityResultCallback<Object>)activityResultCallback, (ActivityResultContract<?, Object>)activityResultContract));
        if (this.mParsedPendingResults.containsKey(s)) {
            final Object value = this.mParsedPendingResults.get(s);
            this.mParsedPendingResults.remove(s);
            activityResultCallback.onActivityResult((O)value);
        }
        final ActivityResult activityResult = (ActivityResult)this.mPendingResults.getParcelable(s);
        if (activityResult != null) {
            this.mPendingResults.remove(s);
            activityResultCallback.onActivityResult(activityResultContract.parseResult(activityResult.getResultCode(), activityResult.getData()));
        }
        return new ActivityResultLauncher<I>(this, s, activityResultContract) {
            final ActivityResultRegistry this$0;
            final ActivityResultContract val$contract;
            final String val$key;
            
            @NonNull
            @Override
            public ActivityResultContract<I, ?> getContract() {
                return this.val$contract;
            }
            
            @Override
            public void launch(final I obj, @Nullable final ActivityOptionsCompat activityOptionsCompat) {
                final Integer n = this.this$0.mKeyToRc.get(this.val$key);
                if (n != null) {
                    this.this$0.mLaunchedKeys.add(this.val$key);
                    try {
                        this.this$0.onLaunch(n, (ActivityResultContract<I, Object>)this.val$contract, obj, activityOptionsCompat);
                        return;
                    }
                    catch (final Exception ex) {
                        this.this$0.mLaunchedKeys.remove(this.val$key);
                        throw ex;
                    }
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Attempting to launch an unregistered ActivityResultLauncher with contract ");
                sb.append(this.val$contract);
                sb.append(" and input ");
                sb.append(obj);
                sb.append(". You must ensure the ActivityResultLauncher is registered before calling launch().");
                throw new IllegalStateException(sb.toString());
            }
            
            @Override
            public void unregister() {
                this.this$0.unregister(this.val$key);
            }
        };
    }
    
    @NonNull
    public final <I, O> ActivityResultLauncher<I> register(@NonNull final String s, @NonNull final LifecycleOwner obj, @NonNull final ActivityResultContract<I, O> activityResultContract, @NonNull final ActivityResultCallback<O> activityResultCallback) {
        final Lifecycle lifecycle = obj.getLifecycle();
        if (!lifecycle.getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
            this.registerKey(s);
            LifecycleContainer lifecycleContainer;
            if ((lifecycleContainer = this.mKeyToLifecycleContainers.get(s)) == null) {
                lifecycleContainer = new LifecycleContainer(lifecycle);
            }
            lifecycleContainer.addObserver(new LifecycleEventObserver(this, s, activityResultCallback, activityResultContract) {
                final ActivityResultRegistry this$0;
                final ActivityResultCallback val$callback;
                final ActivityResultContract val$contract;
                final String val$key;
                
                @Override
                public void onStateChanged(@NonNull final LifecycleOwner lifecycleOwner, @NonNull final Lifecycle.Event obj) {
                    if (Lifecycle.Event.ON_START.equals(obj)) {
                        this.this$0.mKeyToCallback.put(this.val$key, new CallbackAndContract<Object>(this.val$callback, this.val$contract));
                        if (this.this$0.mParsedPendingResults.containsKey(this.val$key)) {
                            final Object value = this.this$0.mParsedPendingResults.get(this.val$key);
                            this.this$0.mParsedPendingResults.remove(this.val$key);
                            this.val$callback.onActivityResult(value);
                        }
                        final ActivityResult activityResult = (ActivityResult)this.this$0.mPendingResults.getParcelable(this.val$key);
                        if (activityResult != null) {
                            this.this$0.mPendingResults.remove(this.val$key);
                            this.val$callback.onActivityResult(this.val$contract.parseResult(activityResult.getResultCode(), activityResult.getData()));
                        }
                    }
                    else if (Lifecycle.Event.ON_STOP.equals(obj)) {
                        this.this$0.mKeyToCallback.remove(this.val$key);
                    }
                    else if (Lifecycle.Event.ON_DESTROY.equals(obj)) {
                        this.this$0.unregister(this.val$key);
                    }
                }
            });
            this.mKeyToLifecycleContainers.put(s, lifecycleContainer);
            return new ActivityResultLauncher<I>(this, s, activityResultContract) {
                final ActivityResultRegistry this$0;
                final ActivityResultContract val$contract;
                final String val$key;
                
                @NonNull
                @Override
                public ActivityResultContract<I, ?> getContract() {
                    return this.val$contract;
                }
                
                @Override
                public void launch(final I obj, @Nullable final ActivityOptionsCompat activityOptionsCompat) {
                    final Integer n = this.this$0.mKeyToRc.get(this.val$key);
                    if (n != null) {
                        this.this$0.mLaunchedKeys.add(this.val$key);
                        try {
                            this.this$0.onLaunch(n, (ActivityResultContract<I, Object>)this.val$contract, obj, activityOptionsCompat);
                            return;
                        }
                        catch (final Exception ex) {
                            this.this$0.mLaunchedKeys.remove(this.val$key);
                            throw ex;
                        }
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Attempting to launch an unregistered ActivityResultLauncher with contract ");
                    sb.append(this.val$contract);
                    sb.append(" and input ");
                    sb.append(obj);
                    sb.append(". You must ensure the ActivityResultLauncher is registered before calling launch().");
                    throw new IllegalStateException(sb.toString());
                }
                
                @Override
                public void unregister() {
                    this.this$0.unregister(this.val$key);
                }
            };
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("LifecycleOwner ");
        sb.append(obj);
        sb.append(" is attempting to register while current state is ");
        sb.append(lifecycle.getCurrentState());
        sb.append(". LifecycleOwners must call register before they are STARTED.");
        throw new IllegalStateException(sb.toString());
    }
    
    @MainThread
    final void unregister(@NonNull final String str) {
        if (!this.mLaunchedKeys.contains(str)) {
            final Integer n = this.mKeyToRc.remove(str);
            if (n != null) {
                this.mRcToKey.remove(n);
            }
        }
        this.mKeyToCallback.remove(str);
        if (this.mParsedPendingResults.containsKey(str)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Dropping pending result for request ");
            sb.append(str);
            sb.append(": ");
            sb.append(this.mParsedPendingResults.get(str));
            this.mParsedPendingResults.remove(str);
        }
        if (((BaseBundle)this.mPendingResults).containsKey(str)) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Dropping pending result for request ");
            sb2.append(str);
            sb2.append(": ");
            sb2.append(this.mPendingResults.getParcelable(str));
            this.mPendingResults.remove(str);
        }
        final LifecycleContainer lifecycleContainer = this.mKeyToLifecycleContainers.get(str);
        if (lifecycleContainer != null) {
            lifecycleContainer.clearObservers();
            this.mKeyToLifecycleContainers.remove(str);
        }
    }
    
    private static class CallbackAndContract<O>
    {
        final ActivityResultCallback<O> mCallback;
        final ActivityResultContract<?, O> mContract;
        
        CallbackAndContract(final ActivityResultCallback<O> mCallback, final ActivityResultContract<?, O> mContract) {
            this.mCallback = mCallback;
            this.mContract = mContract;
        }
    }
    
    private static class LifecycleContainer
    {
        final Lifecycle mLifecycle;
        private final ArrayList<LifecycleEventObserver> mObservers;
        
        LifecycleContainer(@NonNull final Lifecycle mLifecycle) {
            this.mLifecycle = mLifecycle;
            this.mObservers = new ArrayList<LifecycleEventObserver>();
        }
        
        void addObserver(@NonNull final LifecycleEventObserver e) {
            this.mLifecycle.addObserver(e);
            this.mObservers.add(e);
        }
        
        void clearObservers() {
            final Iterator<LifecycleEventObserver> iterator = this.mObservers.iterator();
            while (iterator.hasNext()) {
                this.mLifecycle.removeObserver(iterator.next());
            }
            this.mObservers.clear();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.result;

import androidx.core.app.ActivityOptionsCompat;
import kotlin.jvm.functions.Function0;
import kotlin.LazyKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Lazy;
import org.jetbrains.annotations.NotNull;
import androidx.activity.result.contract.ActivityResultContract;
import kotlin.Metadata;
import kotlin.Unit;

@Metadata
public final class ActivityResultCallerLauncher<I, O> extends ActivityResultLauncher<Unit>
{
    @NotNull
    private final ActivityResultContract<I, O> callerContract;
    private final I callerInput;
    @NotNull
    private final ActivityResultLauncher<I> launcher;
    @NotNull
    private final Lazy resultContract$delegate;
    
    public ActivityResultCallerLauncher(@NotNull final ActivityResultLauncher<I> launcher, @NotNull final ActivityResultContract<I, O> callerContract, final I callerInput) {
        Intrinsics.checkNotNullParameter((Object)launcher, "launcher");
        Intrinsics.checkNotNullParameter((Object)callerContract, "callerContract");
        this.launcher = launcher;
        this.callerContract = callerContract;
        this.callerInput = callerInput;
        this.resultContract$delegate = LazyKt.\u3007o00\u3007\u3007Oo((Function0)new ActivityResultCallerLauncher$resultContract.ActivityResultCallerLauncher$resultContract$2(this));
    }
    
    @NotNull
    public final ActivityResultContract<I, O> getCallerContract() {
        return this.callerContract;
    }
    
    public final I getCallerInput() {
        return this.callerInput;
    }
    
    @NotNull
    @Override
    public ActivityResultContract<Unit, O> getContract() {
        return this.getResultContract();
    }
    
    @NotNull
    public final ActivityResultLauncher<I> getLauncher() {
        return this.launcher;
    }
    
    @NotNull
    public final ActivityResultContract<Unit, O> getResultContract() {
        return (ActivityResultContract)this.resultContract$delegate.getValue();
    }
    
    @Override
    public void launch(@NotNull final Unit unit, final ActivityOptionsCompat activityOptionsCompat) {
        Intrinsics.checkNotNullParameter((Object)unit, "input");
        this.launcher.launch(this.callerInput, activityOptionsCompat);
    }
    
    @Override
    public void unregister() {
        this.launcher.unregister();
    }
}

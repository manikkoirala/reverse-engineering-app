// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.result;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import androidx.activity.result.contract.ActivityResultContract;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class ActivityResultCallerKt
{
    @NotNull
    public static final <I, O> ActivityResultLauncher<Unit> registerForActivityResult(@NotNull final ActivityResultCaller activityResultCaller, @NotNull final ActivityResultContract<I, O> activityResultContract, final I n, @NotNull final ActivityResultRegistry activityResultRegistry, @NotNull final Function1<? super O, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)activityResultCaller, "<this>");
        Intrinsics.checkNotNullParameter((Object)activityResultContract, "contract");
        Intrinsics.checkNotNullParameter((Object)activityResultRegistry, "registry");
        Intrinsics.checkNotNullParameter((Object)function1, "callback");
        final ActivityResultLauncher<I> registerForActivityResult = activityResultCaller.registerForActivityResult(activityResultContract, activityResultRegistry, new \u3007o00\u3007\u3007Oo(function1));
        Intrinsics.checkNotNullExpressionValue((Object)registerForActivityResult, "registerForActivityResul\u2026egistry) { callback(it) }");
        return new ActivityResultCallerLauncher<Object, Object>((ActivityResultLauncher<Object>)registerForActivityResult, (ActivityResultContract<Object, Object>)activityResultContract, n);
    }
    
    @NotNull
    public static final <I, O> ActivityResultLauncher<Unit> registerForActivityResult(@NotNull final ActivityResultCaller activityResultCaller, @NotNull final ActivityResultContract<I, O> activityResultContract, final I n, @NotNull final Function1<? super O, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)activityResultCaller, "<this>");
        Intrinsics.checkNotNullParameter((Object)activityResultContract, "contract");
        Intrinsics.checkNotNullParameter((Object)function1, "callback");
        final ActivityResultLauncher<I> registerForActivityResult = activityResultCaller.registerForActivityResult(activityResultContract, new \u3007080(function1));
        Intrinsics.checkNotNullExpressionValue((Object)registerForActivityResult, "registerForActivityResul\u2026ontract) { callback(it) }");
        return new ActivityResultCallerLauncher<Object, Object>((ActivityResultLauncher<Object>)registerForActivityResult, (ActivityResultContract<Object, Object>)activityResultContract, n);
    }
    
    private static final void registerForActivityResult$lambda-0(final Function1 function1, final Object o) {
        Intrinsics.checkNotNullParameter((Object)function1, "$callback");
        function1.invoke(o);
    }
    
    private static final void registerForActivityResult$lambda-1(final Function1 function1, final Object o) {
        Intrinsics.checkNotNullParameter((Object)function1, "$callback");
        function1.invoke(o);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.result;

import androidx.annotation.MainThread;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;
import android.annotation.SuppressLint;
import androidx.annotation.NonNull;
import androidx.activity.result.contract.ActivityResultContract;

public abstract class ActivityResultLauncher<I>
{
    @NonNull
    public abstract ActivityResultContract<I, ?> getContract();
    
    public void launch(@SuppressLint({ "UnknownNullness" }) final I n) {
        this.launch(n, null);
    }
    
    public abstract void launch(@SuppressLint({ "UnknownNullness" }) final I p0, @Nullable final ActivityOptionsCompat p1);
    
    @MainThread
    public abstract void unregister();
}

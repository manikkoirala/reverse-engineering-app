// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import kotlin.jvm.functions.Function1;
import kotlin.sequences.SequencesKt;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.view.View;
import kotlin.Metadata;

@Metadata
public final class ViewTreeOnBackPressedDispatcherOwner
{
    public static final OnBackPressedDispatcherOwner get(@NotNull final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        return (OnBackPressedDispatcherOwner)SequencesKt.OO0o\u3007\u3007(SequencesKt.\u3007\u30078O0\u30078(SequencesKt.o\u30070((Object)view, (Function1)ViewTreeOnBackPressedDispatcherOwner$findViewTreeOnBackPressedDispatcherOwner.ViewTreeOnBackPressedDispatcherOwner$findViewTreeOnBackPressedDispatcherOwner$1.INSTANCE), (Function1)ViewTreeOnBackPressedDispatcherOwner$findViewTreeOnBackPressedDispatcherOwner.ViewTreeOnBackPressedDispatcherOwner$findViewTreeOnBackPressedDispatcherOwner$2.INSTANCE));
    }
    
    public static final void set(@NotNull final View view, @NotNull final OnBackPressedDispatcherOwner onBackPressedDispatcherOwner) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        Intrinsics.checkNotNullParameter((Object)onBackPressedDispatcherOwner, "onBackPressedDispatcherOwner");
        view.setTag(R.id.view_tree_on_back_pressed_dispatcher_owner, (Object)onBackPressedDispatcherOwner);
    }
}

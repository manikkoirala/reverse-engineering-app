// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import androidx.lifecycle.LifecycleOwner;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class OnBackPressedDispatcherKt
{
    @NotNull
    public static final OnBackPressedCallback addCallback(@NotNull final OnBackPressedDispatcher onBackPressedDispatcher, final LifecycleOwner lifecycleOwner, final boolean b, @NotNull final Function1<? super OnBackPressedCallback, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)onBackPressedDispatcher, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "onBackPressed");
        final OnBackPressedDispatcherKt$addCallback$callback.OnBackPressedDispatcherKt$addCallback$callback$1 onBackPressedDispatcherKt$addCallback$callback$1 = new OnBackPressedDispatcherKt$addCallback$callback.OnBackPressedDispatcherKt$addCallback$callback$1(b, (Function1)function1);
        if (lifecycleOwner != null) {
            onBackPressedDispatcher.addCallback(lifecycleOwner, (OnBackPressedCallback)onBackPressedDispatcherKt$addCallback$callback$1);
        }
        else {
            onBackPressedDispatcher.addCallback((OnBackPressedCallback)onBackPressedDispatcherKt$addCallback$callback$1);
        }
        return (OnBackPressedCallback)onBackPressedDispatcherKt$addCallback$callback$1;
    }
}

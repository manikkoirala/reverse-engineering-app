// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import android.os.Build$VERSION;
import android.os.Bundle;
import androidx.annotation.CallSuper;
import androidx.lifecycle.Lifecycle;
import android.view.ViewGroup$LayoutParams;
import android.view.View;
import android.view.Window;
import androidx.lifecycle.ViewTreeLifecycleOwner;
import androidx.annotation.StyleRes;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import android.content.Context;
import org.jetbrains.annotations.NotNull;
import androidx.lifecycle.LifecycleRegistry;
import kotlin.Metadata;
import androidx.lifecycle.LifecycleOwner;
import android.app.Dialog;

@Metadata
public class ComponentDialog extends Dialog implements LifecycleOwner, OnBackPressedDispatcherOwner
{
    private LifecycleRegistry _lifecycleRegistry;
    @NotNull
    private final OnBackPressedDispatcher onBackPressedDispatcher;
    
    public ComponentDialog(@NotNull final Context context) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        this(context, 0, 2, null);
    }
    
    public ComponentDialog(@NotNull final Context context, @StyleRes final int n) {
        Intrinsics.checkNotNullParameter((Object)context, "context");
        super(context, n);
        this.onBackPressedDispatcher = new OnBackPressedDispatcher(new \u3007\u3007888(this));
    }
    
    private final LifecycleRegistry getLifecycleRegistry() {
        LifecycleRegistry lifecycleRegistry;
        if ((lifecycleRegistry = this._lifecycleRegistry) == null) {
            lifecycleRegistry = new LifecycleRegistry(this);
            this._lifecycleRegistry = lifecycleRegistry;
        }
        return lifecycleRegistry;
    }
    
    private final void initViewTreeOwners() {
        final Window window = this.getWindow();
        Intrinsics.Oo08((Object)window);
        ViewTreeLifecycleOwner.set(window.getDecorView(), this);
        final Window window2 = this.getWindow();
        Intrinsics.Oo08((Object)window2);
        final View decorView = window2.getDecorView();
        Intrinsics.checkNotNullExpressionValue((Object)decorView, "window!!.decorView");
        ViewTreeOnBackPressedDispatcherOwner.set(decorView, this);
    }
    
    private static final void onBackPressedDispatcher$lambda-1(final ComponentDialog componentDialog) {
        Intrinsics.checkNotNullParameter((Object)componentDialog, "this$0");
        componentDialog.onBackPressed();
    }
    
    public void addContentView(@NotNull final View view, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        Intrinsics.checkNotNullParameter((Object)view, "view");
        this.initViewTreeOwners();
        super.addContentView(view, viewGroup$LayoutParams);
    }
    
    @NotNull
    public final Lifecycle getLifecycle() {
        return this.getLifecycleRegistry();
    }
    
    @NotNull
    public final OnBackPressedDispatcher getOnBackPressedDispatcher() {
        return this.onBackPressedDispatcher;
    }
    
    @CallSuper
    public void onBackPressed() {
        this.onBackPressedDispatcher.onBackPressed();
    }
    
    @CallSuper
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (Build$VERSION.SDK_INT >= 33) {
            this.onBackPressedDispatcher.setOnBackInvokedDispatcher(this.getOnBackInvokedDispatcher());
        }
        this.getLifecycleRegistry().handleLifecycleEvent(Lifecycle.Event.ON_CREATE);
    }
    
    @CallSuper
    protected void onStart() {
        super.onStart();
        this.getLifecycleRegistry().handleLifecycleEvent(Lifecycle.Event.ON_RESUME);
    }
    
    @CallSuper
    protected void onStop() {
        this.getLifecycleRegistry().handleLifecycleEvent(Lifecycle.Event.ON_DESTROY);
        this._lifecycleRegistry = null;
        super.onStop();
    }
    
    public void setContentView(final int contentView) {
        this.initViewTreeOwners();
        super.setContentView(contentView);
    }
    
    public void setContentView(@NotNull final View contentView) {
        Intrinsics.checkNotNullParameter((Object)contentView, "view");
        this.initViewTreeOwners();
        super.setContentView(contentView);
    }
    
    public void setContentView(@NotNull final View view, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        Intrinsics.checkNotNullParameter((Object)view, "view");
        this.initViewTreeOwners();
        super.setContentView(view, viewGroup$LayoutParams);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import androidx.annotation.DoNotInline;
import android.window.OnBackInvokedDispatcher;
import android.content.IntentSender;
import androidx.tracing.Trace;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.ActivityResultCallback;
import androidx.annotation.RequiresApi;
import android.view.MenuItem;
import android.view.Menu;
import androidx.annotation.OptIn;
import androidx.core.os.BuildCompat;
import androidx.lifecycle.ReportFragment;
import java.util.Iterator;
import androidx.annotation.MainThread;
import android.app.Application;
import androidx.lifecycle.SavedStateViewModelFactory;
import androidx.annotation.CallSuper;
import androidx.lifecycle.viewmodel.MutableCreationExtras;
import androidx.lifecycle.viewmodel.CreationExtras;
import androidx.core.view.MenuProvider;
import android.view.ViewGroup$LayoutParams;
import android.annotation.SuppressLint;
import androidx.savedstate.ViewTreeSavedStateRegistryOwner;
import androidx.lifecycle.ViewTreeViewModelStoreOwner;
import androidx.lifecycle.ViewTreeLifecycleOwner;
import androidx.annotation.ContentView;
import androidx.activity.contextaware.OnContextAvailableListener;
import androidx.savedstate.SavedStateRegistry;
import androidx.lifecycle.SavedStateHandleSupport;
import androidx.lifecycle.LifecycleObserver;
import android.view.View;
import android.view.Window;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleEventObserver;
import android.os.Build$VERSION;
import android.os.Bundle;
import java.io.Serializable;
import android.content.IntentSender$SendIntentException;
import androidx.activity.result.IntentSenderRequest;
import android.app.Activity;
import androidx.core.app.ActivityCompat;
import android.os.Handler;
import android.os.Looper;
import android.content.Context;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;
import androidx.annotation.NonNull;
import androidx.activity.result.contract.ActivityResultContract;
import android.text.TextUtils;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelStore;
import androidx.savedstate.SavedStateRegistryController;
import androidx.core.app.PictureInPictureModeChangedInfo;
import android.content.Intent;
import androidx.core.app.MultiWindowModeChangedInfo;
import android.content.res.Configuration;
import androidx.core.util.Consumer;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import androidx.core.view.MenuHostHelper;
import androidx.lifecycle.LifecycleRegistry;
import androidx.lifecycle.ViewModelProvider;
import androidx.activity.contextaware.ContextAwareHelper;
import androidx.annotation.LayoutRes;
import androidx.activity.result.ActivityResultRegistry;
import androidx.core.view.MenuHost;
import androidx.core.app.OnPictureInPictureModeChangedProvider;
import androidx.core.app.OnMultiWindowModeChangedProvider;
import androidx.core.app.OnNewIntentProvider;
import androidx.core.content.OnTrimMemoryProvider;
import androidx.core.content.OnConfigurationChangedProvider;
import androidx.activity.result.ActivityResultCaller;
import androidx.activity.result.ActivityResultRegistryOwner;
import androidx.savedstate.SavedStateRegistryOwner;
import androidx.lifecycle.HasDefaultViewModelProviderFactory;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.activity.contextaware.ContextAware;

public class ComponentActivity extends androidx.core.app.ComponentActivity implements ContextAware, ViewModelStoreOwner, HasDefaultViewModelProviderFactory, SavedStateRegistryOwner, OnBackPressedDispatcherOwner, ActivityResultRegistryOwner, ActivityResultCaller, OnConfigurationChangedProvider, OnTrimMemoryProvider, OnNewIntentProvider, OnMultiWindowModeChangedProvider, OnPictureInPictureModeChangedProvider, MenuHost
{
    private static final String ACTIVITY_RESULT_TAG = "android:support:activity-result";
    private final ActivityResultRegistry mActivityResultRegistry;
    @LayoutRes
    private int mContentLayoutId;
    final ContextAwareHelper mContextAwareHelper;
    private ViewModelProvider.Factory mDefaultFactory;
    private boolean mDispatchingOnMultiWindowModeChanged;
    private boolean mDispatchingOnPictureInPictureModeChanged;
    private final LifecycleRegistry mLifecycleRegistry;
    private final MenuHostHelper mMenuHostHelper;
    private final AtomicInteger mNextLocalRequestCode;
    private final OnBackPressedDispatcher mOnBackPressedDispatcher;
    private final CopyOnWriteArrayList<Consumer<Configuration>> mOnConfigurationChangedListeners;
    private final CopyOnWriteArrayList<Consumer<MultiWindowModeChangedInfo>> mOnMultiWindowModeChangedListeners;
    private final CopyOnWriteArrayList<Consumer<Intent>> mOnNewIntentListeners;
    private final CopyOnWriteArrayList<Consumer<PictureInPictureModeChangedInfo>> mOnPictureInPictureModeChangedListeners;
    private final CopyOnWriteArrayList<Consumer<Integer>> mOnTrimMemoryListeners;
    final SavedStateRegistryController mSavedStateRegistryController;
    private ViewModelStore mViewModelStore;
    
    public ComponentActivity() {
        this.mContextAwareHelper = new ContextAwareHelper();
        this.mMenuHostHelper = new MenuHostHelper(new O8(this));
        this.mLifecycleRegistry = new LifecycleRegistry(this);
        final SavedStateRegistryController create = SavedStateRegistryController.create(this);
        this.mSavedStateRegistryController = create;
        this.mOnBackPressedDispatcher = new OnBackPressedDispatcher(new Runnable() {
            final ComponentActivity this$0;
            
            @Override
            public void run() {
                try {
                    ComponentActivity.access$001(this.this$0);
                }
                catch (final IllegalStateException ex) {
                    if (!TextUtils.equals((CharSequence)ex.getMessage(), (CharSequence)"Can not perform this action after onSaveInstanceState")) {
                        throw ex;
                    }
                }
            }
        });
        this.mNextLocalRequestCode = new AtomicInteger();
        this.mActivityResultRegistry = new ActivityResultRegistry() {
            final ComponentActivity this$0;
            
            @Override
            public <I, O> void onLaunch(final int n, @NonNull final ActivityResultContract<I, O> activityResultContract, final I n2, @Nullable final ActivityOptionsCompat activityOptionsCompat) {
                final ComponentActivity this$0 = this.this$0;
                final ActivityResultContract.SynchronousResult<O> synchronousResult = activityResultContract.getSynchronousResult((Context)this$0, n2);
                if (synchronousResult != null) {
                    new Handler(Looper.getMainLooper()).post((Runnable)new Runnable(this, n, synchronousResult) {
                        final ComponentActivity$2 this$1;
                        final int val$requestCode;
                        final ActivityResultContract.SynchronousResult val$synchronousResult;
                        
                        @Override
                        public void run() {
                            this.this$1.dispatchResult(this.val$requestCode, this.val$synchronousResult.getValue());
                        }
                    });
                    return;
                }
                final Intent intent = activityResultContract.createIntent((Context)this$0, n2);
                if (intent.getExtras() != null && intent.getExtras().getClassLoader() == null) {
                    intent.setExtrasClassLoader(((Context)this$0).getClassLoader());
                }
                Bundle bundle;
                if (intent.hasExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE")) {
                    bundle = intent.getBundleExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE");
                    intent.removeExtra("androidx.activity.result.contract.extra.ACTIVITY_OPTIONS_BUNDLE");
                }
                else if (activityOptionsCompat != null) {
                    bundle = activityOptionsCompat.toBundle();
                }
                else {
                    bundle = null;
                }
                if ("androidx.activity.result.contract.action.REQUEST_PERMISSIONS".equals(intent.getAction())) {
                    String[] stringArrayExtra;
                    if ((stringArrayExtra = intent.getStringArrayExtra("androidx.activity.result.contract.extra.PERMISSIONS")) == null) {
                        stringArrayExtra = new String[0];
                    }
                    ActivityCompat.requestPermissions(this$0, stringArrayExtra, n);
                }
                else if ("androidx.activity.result.contract.action.INTENT_SENDER_REQUEST".equals(intent.getAction())) {
                    final IntentSenderRequest intentSenderRequest = (IntentSenderRequest)intent.getParcelableExtra("androidx.activity.result.contract.extra.INTENT_SENDER_REQUEST");
                    try {
                        ActivityCompat.startIntentSenderForResult(this$0, intentSenderRequest.getIntentSender(), n, intentSenderRequest.getFillInIntent(), intentSenderRequest.getFlagsMask(), intentSenderRequest.getFlagsValues(), 0, bundle);
                    }
                    catch (final IntentSender$SendIntentException ex) {
                        new Handler(Looper.getMainLooper()).post((Runnable)new Runnable(this, n, ex) {
                            final ComponentActivity$2 this$1;
                            final IntentSender$SendIntentException val$e;
                            final int val$requestCode;
                            
                            @Override
                            public void run() {
                                this.this$1.dispatchResult(this.val$requestCode, 0, new Intent().setAction("androidx.activity.result.contract.action.INTENT_SENDER_REQUEST").putExtra("androidx.activity.result.contract.extra.SEND_INTENT_EXCEPTION", (Serializable)this.val$e));
                            }
                        });
                    }
                }
                else {
                    ActivityCompat.startActivityForResult(this$0, intent, n, bundle);
                }
            }
        };
        this.mOnConfigurationChangedListeners = new CopyOnWriteArrayList<Consumer<Configuration>>();
        this.mOnTrimMemoryListeners = new CopyOnWriteArrayList<Consumer<Integer>>();
        this.mOnNewIntentListeners = new CopyOnWriteArrayList<Consumer<Intent>>();
        this.mOnMultiWindowModeChangedListeners = new CopyOnWriteArrayList<Consumer<MultiWindowModeChangedInfo>>();
        this.mOnPictureInPictureModeChangedListeners = new CopyOnWriteArrayList<Consumer<PictureInPictureModeChangedInfo>>();
        this.mDispatchingOnMultiWindowModeChanged = false;
        this.mDispatchingOnPictureInPictureModeChanged = false;
        if (this.getLifecycle() != null) {
            final int sdk_INT = Build$VERSION.SDK_INT;
            this.getLifecycle().addObserver(new LifecycleEventObserver(this) {
                final ComponentActivity this$0;
                
                @Override
                public void onStateChanged(@NonNull final LifecycleOwner lifecycleOwner, @NonNull final Lifecycle.Event event) {
                    if (event == Lifecycle.Event.ON_STOP) {
                        final Window window = this.this$0.getWindow();
                        View peekDecorView;
                        if (window != null) {
                            peekDecorView = window.peekDecorView();
                        }
                        else {
                            peekDecorView = null;
                        }
                        if (peekDecorView != null) {
                            Api19Impl.cancelPendingInputEvents(peekDecorView);
                        }
                    }
                }
            });
            this.getLifecycle().addObserver(new LifecycleEventObserver(this) {
                final ComponentActivity this$0;
                
                @Override
                public void onStateChanged(@NonNull final LifecycleOwner lifecycleOwner, @NonNull final Lifecycle.Event event) {
                    if (event == Lifecycle.Event.ON_DESTROY) {
                        this.this$0.mContextAwareHelper.clearAvailableContext();
                        if (!this.this$0.isChangingConfigurations()) {
                            this.this$0.getViewModelStore().clear();
                        }
                    }
                }
            });
            this.getLifecycle().addObserver(new LifecycleEventObserver(this) {
                final ComponentActivity this$0;
                
                @Override
                public void onStateChanged(@NonNull final LifecycleOwner lifecycleOwner, @NonNull final Lifecycle.Event event) {
                    this.this$0.ensureViewModelStore();
                    this.this$0.getLifecycle().removeObserver(this);
                }
            });
            create.performAttach();
            SavedStateHandleSupport.enableSavedStateHandles(this);
            if (sdk_INT <= 23) {
                this.getLifecycle().addObserver(new ImmLeaksCleaner(this));
            }
            this.getSavedStateRegistry().registerSavedStateProvider("android:support:activity-result", (SavedStateRegistry.SavedStateProvider)new Oo08(this));
            this.addOnContextAvailableListener(new o\u30070(this));
            return;
        }
        throw new IllegalStateException("getLifecycle() returned null in ComponentActivity's constructor. Please make sure you are lazily constructing your Lifecycle in the first call to getLifecycle() rather than relying on field initialization.");
    }
    
    @ContentView
    public ComponentActivity(@LayoutRes final int mContentLayoutId) {
        this();
        this.mContentLayoutId = mContentLayoutId;
    }
    
    static /* synthetic */ void access$001(final ComponentActivity componentActivity) {
        componentActivity.onBackPressed();
    }
    
    private void initViewTreeOwners() {
        ViewTreeLifecycleOwner.set(this.getWindow().getDecorView(), this);
        ViewTreeViewModelStoreOwner.set(this.getWindow().getDecorView(), this);
        ViewTreeSavedStateRegistryOwner.set(this.getWindow().getDecorView(), this);
        ViewTreeOnBackPressedDispatcherOwner.set(this.getWindow().getDecorView(), this);
    }
    
    public void addContentView(@SuppressLint({ "UnknownNullness", "MissingNullability" }) final View view, @SuppressLint({ "UnknownNullness", "MissingNullability" }) final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        this.initViewTreeOwners();
        super.addContentView(view, viewGroup$LayoutParams);
    }
    
    @Override
    public void addMenuProvider(@NonNull final MenuProvider menuProvider) {
        this.mMenuHostHelper.addMenuProvider(menuProvider);
    }
    
    @Override
    public void addMenuProvider(@NonNull final MenuProvider menuProvider, @NonNull final LifecycleOwner lifecycleOwner) {
        this.mMenuHostHelper.addMenuProvider(menuProvider, lifecycleOwner);
    }
    
    @SuppressLint({ "LambdaLast" })
    @Override
    public void addMenuProvider(@NonNull final MenuProvider menuProvider, @NonNull final LifecycleOwner lifecycleOwner, @NonNull final Lifecycle.State state) {
        this.mMenuHostHelper.addMenuProvider(menuProvider, lifecycleOwner, state);
    }
    
    @Override
    public final void addOnConfigurationChangedListener(@NonNull final Consumer<Configuration> e) {
        this.mOnConfigurationChangedListeners.add(e);
    }
    
    @Override
    public final void addOnContextAvailableListener(@NonNull final OnContextAvailableListener onContextAvailableListener) {
        this.mContextAwareHelper.addOnContextAvailableListener(onContextAvailableListener);
    }
    
    @Override
    public final void addOnMultiWindowModeChangedListener(@NonNull final Consumer<MultiWindowModeChangedInfo> e) {
        this.mOnMultiWindowModeChangedListeners.add(e);
    }
    
    @Override
    public final void addOnNewIntentListener(@NonNull final Consumer<Intent> e) {
        this.mOnNewIntentListeners.add(e);
    }
    
    @Override
    public final void addOnPictureInPictureModeChangedListener(@NonNull final Consumer<PictureInPictureModeChangedInfo> e) {
        this.mOnPictureInPictureModeChangedListeners.add(e);
    }
    
    @Override
    public final void addOnTrimMemoryListener(@NonNull final Consumer<Integer> e) {
        this.mOnTrimMemoryListeners.add(e);
    }
    
    void ensureViewModelStore() {
        if (this.mViewModelStore == null) {
            final NonConfigurationInstances nonConfigurationInstances = (NonConfigurationInstances)this.getLastNonConfigurationInstance();
            if (nonConfigurationInstances != null) {
                this.mViewModelStore = nonConfigurationInstances.viewModelStore;
            }
            if (this.mViewModelStore == null) {
                this.mViewModelStore = new ViewModelStore();
            }
        }
    }
    
    @NonNull
    @Override
    public final ActivityResultRegistry getActivityResultRegistry() {
        return this.mActivityResultRegistry;
    }
    
    @CallSuper
    @NonNull
    @Override
    public CreationExtras getDefaultViewModelCreationExtras() {
        final MutableCreationExtras mutableCreationExtras = new MutableCreationExtras();
        if (this.getApplication() != null) {
            mutableCreationExtras.set(ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY, this.getApplication());
        }
        mutableCreationExtras.set(SavedStateHandleSupport.SAVED_STATE_REGISTRY_OWNER_KEY, this);
        mutableCreationExtras.set(SavedStateHandleSupport.VIEW_MODEL_STORE_OWNER_KEY, this);
        if (this.getIntent() != null && this.getIntent().getExtras() != null) {
            mutableCreationExtras.set(SavedStateHandleSupport.DEFAULT_ARGS_KEY, this.getIntent().getExtras());
        }
        return mutableCreationExtras;
    }
    
    @NonNull
    @Override
    public ViewModelProvider.Factory getDefaultViewModelProviderFactory() {
        if (this.mDefaultFactory == null) {
            final Application application = this.getApplication();
            Bundle extras;
            if (this.getIntent() != null) {
                extras = this.getIntent().getExtras();
            }
            else {
                extras = null;
            }
            this.mDefaultFactory = new SavedStateViewModelFactory(application, this, extras);
        }
        return this.mDefaultFactory;
    }
    
    @Deprecated
    @Nullable
    public Object getLastCustomNonConfigurationInstance() {
        final NonConfigurationInstances nonConfigurationInstances = (NonConfigurationInstances)this.getLastNonConfigurationInstance();
        Object custom;
        if (nonConfigurationInstances != null) {
            custom = nonConfigurationInstances.custom;
        }
        else {
            custom = null;
        }
        return custom;
    }
    
    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return this.mLifecycleRegistry;
    }
    
    @NonNull
    @Override
    public final OnBackPressedDispatcher getOnBackPressedDispatcher() {
        return this.mOnBackPressedDispatcher;
    }
    
    @NonNull
    @Override
    public final SavedStateRegistry getSavedStateRegistry() {
        return this.mSavedStateRegistryController.getSavedStateRegistry();
    }
    
    @NonNull
    @Override
    public ViewModelStore getViewModelStore() {
        if (this.getApplication() != null) {
            this.ensureViewModelStore();
            return this.mViewModelStore;
        }
        throw new IllegalStateException("Your activity is not yet attached to the Application instance. You can't request ViewModel before onCreate call.");
    }
    
    @Override
    public void invalidateMenu() {
        this.invalidateOptionsMenu();
    }
    
    @Deprecated
    @CallSuper
    protected void onActivityResult(final int n, final int n2, @Nullable final Intent intent) {
        if (!this.mActivityResultRegistry.dispatchResult(n, n2, intent)) {
            super.onActivityResult(n, n2, intent);
        }
    }
    
    @MainThread
    public void onBackPressed() {
        this.mOnBackPressedDispatcher.onBackPressed();
    }
    
    @CallSuper
    public void onConfigurationChanged(@NonNull final Configuration configuration) {
        super.onConfigurationChanged(configuration);
        final Iterator<Consumer<Configuration>> iterator = this.mOnConfigurationChangedListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().accept(configuration);
        }
    }
    
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    @Override
    protected void onCreate(@Nullable final Bundle bundle) {
        this.mSavedStateRegistryController.performRestore(bundle);
        this.mContextAwareHelper.dispatchOnContextAvailable((Context)this);
        super.onCreate(bundle);
        ReportFragment.injectIfNeededIn(this);
        if (BuildCompat.isAtLeastT()) {
            this.mOnBackPressedDispatcher.setOnBackInvokedDispatcher(Api33Impl.getOnBackInvokedDispatcher(this));
        }
        final int mContentLayoutId = this.mContentLayoutId;
        if (mContentLayoutId != 0) {
            this.setContentView(mContentLayoutId);
        }
    }
    
    public boolean onCreatePanelMenu(final int n, @NonNull final Menu menu) {
        if (n == 0) {
            super.onCreatePanelMenu(n, menu);
            this.mMenuHostHelper.onCreateMenu(menu, this.getMenuInflater());
        }
        return true;
    }
    
    public boolean onMenuItemSelected(final int n, @NonNull final MenuItem menuItem) {
        return super.onMenuItemSelected(n, menuItem) || (n == 0 && this.mMenuHostHelper.onMenuItemSelected(menuItem));
    }
    
    @CallSuper
    public void onMultiWindowModeChanged(final boolean b) {
        if (this.mDispatchingOnMultiWindowModeChanged) {
            return;
        }
        final Iterator<Consumer<MultiWindowModeChangedInfo>> iterator = this.mOnMultiWindowModeChangedListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().accept(new MultiWindowModeChangedInfo(b));
        }
    }
    
    @CallSuper
    @RequiresApi(api = 26)
    public void onMultiWindowModeChanged(final boolean b, @NonNull final Configuration configuration) {
        this.mDispatchingOnMultiWindowModeChanged = true;
        try {
            super.onMultiWindowModeChanged(b, configuration);
            this.mDispatchingOnMultiWindowModeChanged = false;
            final Iterator<Consumer<MultiWindowModeChangedInfo>> iterator = this.mOnMultiWindowModeChangedListeners.iterator();
            while (iterator.hasNext()) {
                iterator.next().accept(new MultiWindowModeChangedInfo(b, configuration));
            }
        }
        finally {
            this.mDispatchingOnMultiWindowModeChanged = false;
        }
    }
    
    @CallSuper
    protected void onNewIntent(@SuppressLint({ "UnknownNullness", "MissingNullability" }) final Intent intent) {
        super.onNewIntent(intent);
        final Iterator<Consumer<Intent>> iterator = this.mOnNewIntentListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().accept(intent);
        }
    }
    
    public void onPanelClosed(final int n, @NonNull final Menu menu) {
        this.mMenuHostHelper.onMenuClosed(menu);
        super.onPanelClosed(n, menu);
    }
    
    @CallSuper
    public void onPictureInPictureModeChanged(final boolean b) {
        if (this.mDispatchingOnPictureInPictureModeChanged) {
            return;
        }
        final Iterator<Consumer<PictureInPictureModeChangedInfo>> iterator = this.mOnPictureInPictureModeChangedListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().accept(new PictureInPictureModeChangedInfo(b));
        }
    }
    
    @CallSuper
    @RequiresApi(api = 26)
    public void onPictureInPictureModeChanged(final boolean b, @NonNull final Configuration configuration) {
        this.mDispatchingOnPictureInPictureModeChanged = true;
        try {
            super.onPictureInPictureModeChanged(b, configuration);
            this.mDispatchingOnPictureInPictureModeChanged = false;
            final Iterator<Consumer<PictureInPictureModeChangedInfo>> iterator = this.mOnPictureInPictureModeChangedListeners.iterator();
            while (iterator.hasNext()) {
                iterator.next().accept(new PictureInPictureModeChangedInfo(b, configuration));
            }
        }
        finally {
            this.mDispatchingOnPictureInPictureModeChanged = false;
        }
    }
    
    public boolean onPreparePanel(final int n, @Nullable final View view, @NonNull final Menu menu) {
        if (n == 0) {
            super.onPreparePanel(n, view, menu);
            this.mMenuHostHelper.onPrepareMenu(menu);
        }
        return true;
    }
    
    @Deprecated
    @CallSuper
    public void onRequestPermissionsResult(final int n, @NonNull final String[] array, @NonNull final int[] array2) {
        if (!this.mActivityResultRegistry.dispatchResult(n, -1, new Intent().putExtra("androidx.activity.result.contract.extra.PERMISSIONS", array).putExtra("androidx.activity.result.contract.extra.PERMISSION_GRANT_RESULTS", array2)) && Build$VERSION.SDK_INT >= 23) {
            super.onRequestPermissionsResult(n, array, array2);
        }
    }
    
    @Deprecated
    @Nullable
    public Object onRetainCustomNonConfigurationInstance() {
        return null;
    }
    
    @Nullable
    public final Object onRetainNonConfigurationInstance() {
        final Object onRetainCustomNonConfigurationInstance = this.onRetainCustomNonConfigurationInstance();
        ViewModelStore viewModelStore2;
        final ViewModelStore viewModelStore = viewModelStore2 = this.mViewModelStore;
        if (viewModelStore == null) {
            final NonConfigurationInstances nonConfigurationInstances = (NonConfigurationInstances)this.getLastNonConfigurationInstance();
            viewModelStore2 = viewModelStore;
            if (nonConfigurationInstances != null) {
                viewModelStore2 = nonConfigurationInstances.viewModelStore;
            }
        }
        if (viewModelStore2 == null && onRetainCustomNonConfigurationInstance == null) {
            return null;
        }
        final NonConfigurationInstances nonConfigurationInstances2 = new NonConfigurationInstances();
        nonConfigurationInstances2.custom = onRetainCustomNonConfigurationInstance;
        nonConfigurationInstances2.viewModelStore = viewModelStore2;
        return nonConfigurationInstances2;
    }
    
    @CallSuper
    @Override
    protected void onSaveInstanceState(@NonNull final Bundle bundle) {
        final Lifecycle lifecycle = this.getLifecycle();
        if (lifecycle instanceof LifecycleRegistry) {
            ((LifecycleRegistry)lifecycle).setCurrentState(Lifecycle.State.CREATED);
        }
        super.onSaveInstanceState(bundle);
        this.mSavedStateRegistryController.performSave(bundle);
    }
    
    @CallSuper
    public void onTrimMemory(final int i) {
        super.onTrimMemory(i);
        final Iterator<Consumer<Integer>> iterator = this.mOnTrimMemoryListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().accept(i);
        }
    }
    
    @Nullable
    @Override
    public Context peekAvailableContext() {
        return this.mContextAwareHelper.peekAvailableContext();
    }
    
    @NonNull
    @Override
    public final <I, O> ActivityResultLauncher<I> registerForActivityResult(@NonNull final ActivityResultContract<I, O> activityResultContract, @NonNull final ActivityResultCallback<O> activityResultCallback) {
        return this.registerForActivityResult(activityResultContract, this.mActivityResultRegistry, activityResultCallback);
    }
    
    @NonNull
    @Override
    public final <I, O> ActivityResultLauncher<I> registerForActivityResult(@NonNull final ActivityResultContract<I, O> activityResultContract, @NonNull final ActivityResultRegistry activityResultRegistry, @NonNull final ActivityResultCallback<O> activityResultCallback) {
        final StringBuilder sb = new StringBuilder();
        sb.append("activity_rq#");
        sb.append(this.mNextLocalRequestCode.getAndIncrement());
        return activityResultRegistry.register(sb.toString(), this, activityResultContract, activityResultCallback);
    }
    
    @Override
    public void removeMenuProvider(@NonNull final MenuProvider menuProvider) {
        this.mMenuHostHelper.removeMenuProvider(menuProvider);
    }
    
    @Override
    public final void removeOnConfigurationChangedListener(@NonNull final Consumer<Configuration> o) {
        this.mOnConfigurationChangedListeners.remove(o);
    }
    
    @Override
    public final void removeOnContextAvailableListener(@NonNull final OnContextAvailableListener onContextAvailableListener) {
        this.mContextAwareHelper.removeOnContextAvailableListener(onContextAvailableListener);
    }
    
    @Override
    public final void removeOnMultiWindowModeChangedListener(@NonNull final Consumer<MultiWindowModeChangedInfo> o) {
        this.mOnMultiWindowModeChangedListeners.remove(o);
    }
    
    @Override
    public final void removeOnNewIntentListener(@NonNull final Consumer<Intent> o) {
        this.mOnNewIntentListeners.remove(o);
    }
    
    @Override
    public final void removeOnPictureInPictureModeChangedListener(@NonNull final Consumer<PictureInPictureModeChangedInfo> o) {
        this.mOnPictureInPictureModeChangedListeners.remove(o);
    }
    
    @Override
    public final void removeOnTrimMemoryListener(@NonNull final Consumer<Integer> o) {
        this.mOnTrimMemoryListeners.remove(o);
    }
    
    public void reportFullyDrawn() {
        try {
            if (Trace.isEnabled()) {
                Trace.beginSection("reportFullyDrawn() for ComponentActivity");
            }
            super.reportFullyDrawn();
        }
        finally {
            Trace.endSection();
        }
    }
    
    public void setContentView(@LayoutRes final int contentView) {
        this.initViewTreeOwners();
        super.setContentView(contentView);
    }
    
    public void setContentView(@SuppressLint({ "UnknownNullness", "MissingNullability" }) final View contentView) {
        this.initViewTreeOwners();
        super.setContentView(contentView);
    }
    
    public void setContentView(@SuppressLint({ "UnknownNullness", "MissingNullability" }) final View view, @SuppressLint({ "UnknownNullness", "MissingNullability" }) final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        this.initViewTreeOwners();
        super.setContentView(view, viewGroup$LayoutParams);
    }
    
    @Deprecated
    public void startActivityForResult(@NonNull final Intent intent, final int n) {
        super.startActivityForResult(intent, n);
    }
    
    @Deprecated
    public void startActivityForResult(@NonNull final Intent intent, final int n, @Nullable final Bundle bundle) {
        super.startActivityForResult(intent, n, bundle);
    }
    
    @Deprecated
    public void startIntentSenderForResult(@NonNull final IntentSender intentSender, final int n, @Nullable final Intent intent, final int n2, final int n3, final int n4) throws IntentSender$SendIntentException {
        super.startIntentSenderForResult(intentSender, n, intent, n2, n3, n4);
    }
    
    @Deprecated
    public void startIntentSenderForResult(@NonNull final IntentSender intentSender, final int n, @Nullable final Intent intent, final int n2, final int n3, final int n4, @Nullable final Bundle bundle) throws IntentSender$SendIntentException {
        super.startIntentSenderForResult(intentSender, n, intent, n2, n3, n4, bundle);
    }
    
    @RequiresApi(19)
    static class Api19Impl
    {
        private Api19Impl() {
        }
        
        static void cancelPendingInputEvents(final View view) {
            view.cancelPendingInputEvents();
        }
    }
    
    @RequiresApi(33)
    static class Api33Impl
    {
        private Api33Impl() {
        }
        
        @DoNotInline
        static OnBackInvokedDispatcher getOnBackInvokedDispatcher(final Activity activity) {
            return activity.getOnBackInvokedDispatcher();
        }
    }
    
    static final class NonConfigurationInstances
    {
        Object custom;
        ViewModelStore viewModelStore;
    }
}

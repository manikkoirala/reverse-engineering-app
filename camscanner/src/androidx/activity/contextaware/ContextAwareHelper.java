// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.contextaware;

import androidx.annotation.Nullable;
import java.util.Iterator;
import androidx.annotation.NonNull;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.Set;
import android.content.Context;

public final class ContextAwareHelper
{
    private volatile Context mContext;
    private final Set<OnContextAvailableListener> mListeners;
    
    public ContextAwareHelper() {
        this.mListeners = new CopyOnWriteArraySet<OnContextAvailableListener>();
    }
    
    public void addOnContextAvailableListener(@NonNull final OnContextAvailableListener onContextAvailableListener) {
        if (this.mContext != null) {
            onContextAvailableListener.onContextAvailable(this.mContext);
        }
        this.mListeners.add(onContextAvailableListener);
    }
    
    public void clearAvailableContext() {
        this.mContext = null;
    }
    
    public void dispatchOnContextAvailable(@NonNull final Context mContext) {
        this.mContext = mContext;
        final Iterator<OnContextAvailableListener> iterator = this.mListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().onContextAvailable(mContext);
        }
    }
    
    @Nullable
    public Context peekAvailableContext() {
        return this.mContext;
    }
    
    public void removeOnContextAvailableListener(@NonNull final OnContextAvailableListener onContextAvailableListener) {
        this.mListeners.remove(onContextAvailableListener);
    }
}

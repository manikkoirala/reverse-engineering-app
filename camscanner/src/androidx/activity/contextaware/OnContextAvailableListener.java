// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.contextaware;

import androidx.annotation.NonNull;
import android.content.Context;

public interface OnContextAvailableListener
{
    void onContextAvailable(@NonNull final Context p0);
}

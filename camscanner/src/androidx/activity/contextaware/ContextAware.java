// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.contextaware;

import androidx.annotation.Nullable;
import android.content.Context;
import androidx.annotation.NonNull;

public interface ContextAware
{
    void addOnContextAvailableListener(@NonNull final OnContextAvailableListener p0);
    
    @Nullable
    Context peekAvailableContext();
    
    void removeOnContextAvailableListener(@NonNull final OnContextAvailableListener p0);
}

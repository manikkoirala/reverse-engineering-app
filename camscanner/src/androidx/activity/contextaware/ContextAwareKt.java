// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity.contextaware;

import kotlin.jvm.internal.InlineMarker;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlinx.coroutines.CancellableContinuation;
import kotlinx.coroutines.CancellableContinuationImpl;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.Continuation;
import android.content.Context;
import kotlin.jvm.functions.Function1;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class ContextAwareKt
{
    public static final <R> Object withContextAvailable(@NotNull final ContextAware contextAware, @NotNull final Function1<? super Context, ? extends R> function1, @NotNull final Continuation<? super R> continuation) {
        final Context peekAvailableContext = contextAware.peekAvailableContext();
        if (peekAvailableContext != null) {
            return function1.invoke((Object)peekAvailableContext);
        }
        final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.\u3007o\u3007((Continuation)continuation), 1);
        cancellableContinuationImpl.O8ooOoo\u3007();
        final ContextAwareKt$withContextAvailable$2$listener.ContextAwareKt$withContextAvailable$2$listener$1 contextAwareKt$withContextAvailable$2$listener$1 = new ContextAwareKt$withContextAvailable$2$listener.ContextAwareKt$withContextAvailable$2$listener$1((CancellableContinuation)cancellableContinuationImpl, (Function1)function1);
        contextAware.addOnContextAvailableListener((OnContextAvailableListener)contextAwareKt$withContextAvailable$2$listener$1);
        ((CancellableContinuation)cancellableContinuationImpl).\u30070\u3007O0088o((Function1)new ContextAwareKt$withContextAvailable$2.ContextAwareKt$withContextAvailable$2$1(contextAware, contextAwareKt$withContextAvailable$2$listener$1));
        final Object o\u3007O8\u3007\u3007o = cancellableContinuationImpl.o\u3007O8\u3007\u3007o();
        if (o\u3007O8\u3007\u3007o == IntrinsicsKt.O8()) {
            DebugProbesKt.\u3007o\u3007((Continuation)continuation);
        }
        return o\u3007O8\u3007\u3007o;
    }
    
    private static final <R> Object withContextAvailable$$forInline(final ContextAware contextAware, final Function1<? super Context, ? extends R> function1, final Continuation<? super R> continuation) {
        final Context peekAvailableContext = contextAware.peekAvailableContext();
        if (peekAvailableContext != null) {
            return function1.invoke((Object)peekAvailableContext);
        }
        InlineMarker.\u3007o\u3007(0);
        final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.\u3007o\u3007((Continuation)continuation), 1);
        cancellableContinuationImpl.O8ooOoo\u3007();
        final ContextAwareKt$withContextAvailable$2$listener.ContextAwareKt$withContextAvailable$2$listener$1 contextAwareKt$withContextAvailable$2$listener$1 = new ContextAwareKt$withContextAvailable$2$listener.ContextAwareKt$withContextAvailable$2$listener$1((CancellableContinuation)cancellableContinuationImpl, (Function1)function1);
        contextAware.addOnContextAvailableListener((OnContextAvailableListener)contextAwareKt$withContextAvailable$2$listener$1);
        ((CancellableContinuation)cancellableContinuationImpl).\u30070\u3007O0088o((Function1)new ContextAwareKt$withContextAvailable$2.ContextAwareKt$withContextAvailable$2$1(contextAware, contextAwareKt$withContextAvailable$2$listener$1));
        final Object o\u3007O8\u3007\u3007o = cancellableContinuationImpl.o\u3007O8\u3007\u3007o();
        if (o\u3007O8\u3007\u3007o == IntrinsicsKt.O8()) {
            DebugProbesKt.\u3007o\u3007((Continuation)continuation);
        }
        InlineMarker.\u3007o\u3007(1);
        return o\u3007O8\u3007\u3007o;
    }
}

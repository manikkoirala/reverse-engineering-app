// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;

public interface OnBackPressedDispatcherOwner extends LifecycleOwner
{
    @NonNull
    OnBackPressedDispatcher getOnBackPressedDispatcher();
}

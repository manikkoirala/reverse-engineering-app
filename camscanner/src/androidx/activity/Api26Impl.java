// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import android.app.PictureInPictureParams$Builder;
import kotlin.jvm.internal.Intrinsics;
import android.graphics.Rect;
import android.app.Activity;
import org.jetbrains.annotations.NotNull;
import androidx.annotation.RequiresApi;
import kotlin.Metadata;

@Metadata
@RequiresApi(26)
public final class Api26Impl
{
    @NotNull
    public static final Api26Impl INSTANCE;
    
    static {
        INSTANCE = new Api26Impl();
    }
    
    private Api26Impl() {
    }
    
    public final void setPipParamsSourceRectHint(@NotNull final Activity activity, @NotNull final Rect rect) {
        Intrinsics.checkNotNullParameter((Object)activity, "activity");
        Intrinsics.checkNotNullParameter((Object)rect, "hint");
        \u3007o\u3007.\u3007080(activity, \u3007o00\u3007\u3007Oo.\u3007080(\u3007080.\u3007080(new PictureInPictureParams$Builder(), rect)));
    }
}

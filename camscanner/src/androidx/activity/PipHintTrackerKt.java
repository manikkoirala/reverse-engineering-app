// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import androidx.annotation.RequiresApi;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlinx.coroutines.flow.FlowCollector;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.flow.FlowKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import org.jetbrains.annotations.NotNull;
import android.app.Activity;
import android.graphics.Rect;
import android.view.View;
import kotlin.Metadata;

@Metadata
public final class PipHintTrackerKt
{
    @RequiresApi(26)
    public static final Object trackPipAnimationHintView(@NotNull final Activity activity, @NotNull final View view, @NotNull final Continuation<? super Unit> continuation) {
        final Object \u3007080 = FlowKt.Oo08((Function2)new PipHintTrackerKt$trackPipAnimationHintView$flow.PipHintTrackerKt$trackPipAnimationHintView$flow$1(view, (Continuation)null)).\u3007080((FlowCollector)new PipHintTrackerKt$trackPipAnimationHintView.PipHintTrackerKt$trackPipAnimationHintView$2(activity), (Continuation)continuation);
        if (\u3007080 == IntrinsicsKt.O8()) {
            return \u3007080;
        }
        return Unit.\u3007080;
    }
    
    private static final Rect trackPipAnimationHintView$positionInWindow(final View view) {
        final Rect rect = new Rect();
        view.getGlobalVisibleRect(rect);
        return rect;
    }
}

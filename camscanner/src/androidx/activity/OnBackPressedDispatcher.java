// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleEventObserver;
import androidx.annotation.DoNotInline;
import java.util.Objects;
import androidx.annotation.RequiresApi;
import java.util.Iterator;
import android.annotation.SuppressLint;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.OptIn;
import androidx.core.os.BuildCompat;
import java.util.ArrayDeque;
import android.window.OnBackInvokedCallback;
import android.window.OnBackInvokedDispatcher;
import androidx.annotation.Nullable;
import androidx.core.util.Consumer;

public final class OnBackPressedDispatcher
{
    private boolean mBackInvokedCallbackRegistered;
    private Consumer<Boolean> mEnabledConsumer;
    @Nullable
    private final Runnable mFallbackOnBackPressed;
    private OnBackInvokedDispatcher mInvokedDispatcher;
    private OnBackInvokedCallback mOnBackInvokedCallback;
    final ArrayDeque<OnBackPressedCallback> mOnBackPressedCallbacks;
    
    public OnBackPressedDispatcher() {
        this(null);
    }
    
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    public OnBackPressedDispatcher(@Nullable final Runnable mFallbackOnBackPressed) {
        this.mOnBackPressedCallbacks = new ArrayDeque<OnBackPressedCallback>();
        this.mBackInvokedCallbackRegistered = false;
        this.mFallbackOnBackPressed = mFallbackOnBackPressed;
        if (BuildCompat.isAtLeastT()) {
            this.mEnabledConsumer = new oO80(this);
            this.mOnBackInvokedCallback = Api33Impl.createOnBackInvokedCallback(new \u300780\u3007808\u3007O(this));
        }
    }
    
    @MainThread
    public void addCallback(@NonNull final OnBackPressedCallback onBackPressedCallback) {
        this.addCancellableCallback(onBackPressedCallback);
    }
    
    @SuppressLint({ "LambdaLast" })
    @MainThread
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    public void addCallback(@NonNull final LifecycleOwner lifecycleOwner, @NonNull final OnBackPressedCallback onBackPressedCallback) {
        final Lifecycle lifecycle = lifecycleOwner.getLifecycle();
        if (lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
            return;
        }
        onBackPressedCallback.addCancellable(new LifecycleOnBackPressedCancellable(lifecycle, onBackPressedCallback));
        if (BuildCompat.isAtLeastT()) {
            this.updateBackInvokedCallbackState();
            onBackPressedCallback.setIsEnabledConsumer(this.mEnabledConsumer);
        }
    }
    
    @MainThread
    @NonNull
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    Cancellable addCancellableCallback(@NonNull final OnBackPressedCallback e) {
        this.mOnBackPressedCallbacks.add(e);
        final OnBackPressedCancellable onBackPressedCancellable = new OnBackPressedCancellable(e);
        e.addCancellable(onBackPressedCancellable);
        if (BuildCompat.isAtLeastT()) {
            this.updateBackInvokedCallbackState();
            e.setIsEnabledConsumer(this.mEnabledConsumer);
        }
        return onBackPressedCancellable;
    }
    
    @MainThread
    public boolean hasEnabledCallbacks() {
        final Iterator<OnBackPressedCallback> descendingIterator = this.mOnBackPressedCallbacks.descendingIterator();
        while (descendingIterator.hasNext()) {
            if (descendingIterator.next().isEnabled()) {
                return true;
            }
        }
        return false;
    }
    
    @MainThread
    public void onBackPressed() {
        final Iterator<OnBackPressedCallback> descendingIterator = this.mOnBackPressedCallbacks.descendingIterator();
        while (descendingIterator.hasNext()) {
            final OnBackPressedCallback onBackPressedCallback = descendingIterator.next();
            if (onBackPressedCallback.isEnabled()) {
                onBackPressedCallback.handleOnBackPressed();
                return;
            }
        }
        final Runnable mFallbackOnBackPressed = this.mFallbackOnBackPressed;
        if (mFallbackOnBackPressed != null) {
            mFallbackOnBackPressed.run();
        }
    }
    
    @RequiresApi(33)
    public void setOnBackInvokedDispatcher(@NonNull final OnBackInvokedDispatcher mInvokedDispatcher) {
        this.mInvokedDispatcher = mInvokedDispatcher;
        this.updateBackInvokedCallbackState();
    }
    
    @RequiresApi(33)
    void updateBackInvokedCallbackState() {
        final boolean hasEnabledCallbacks = this.hasEnabledCallbacks();
        final OnBackInvokedDispatcher mInvokedDispatcher = this.mInvokedDispatcher;
        if (mInvokedDispatcher != null) {
            if (hasEnabledCallbacks && !this.mBackInvokedCallbackRegistered) {
                Api33Impl.registerOnBackInvokedCallback(mInvokedDispatcher, 0, this.mOnBackInvokedCallback);
                this.mBackInvokedCallbackRegistered = true;
            }
            else if (!hasEnabledCallbacks && this.mBackInvokedCallbackRegistered) {
                Api33Impl.unregisterOnBackInvokedCallback(mInvokedDispatcher, this.mOnBackInvokedCallback);
                this.mBackInvokedCallbackRegistered = false;
            }
        }
    }
    
    @RequiresApi(33)
    static class Api33Impl
    {
        private Api33Impl() {
        }
        
        @DoNotInline
        static OnBackInvokedCallback createOnBackInvokedCallback(final Runnable obj) {
            Objects.requireNonNull(obj);
            return (OnBackInvokedCallback)new OO0o\u3007\u3007\u3007\u30070(obj);
        }
        
        @DoNotInline
        static void registerOnBackInvokedCallback(final Object o, final int n, final Object o2) {
            ((OnBackInvokedDispatcher)o).registerOnBackInvokedCallback(n, (OnBackInvokedCallback)o2);
        }
        
        @DoNotInline
        static void unregisterOnBackInvokedCallback(final Object o, final Object o2) {
            ((OnBackInvokedDispatcher)o).unregisterOnBackInvokedCallback((OnBackInvokedCallback)o2);
        }
    }
    
    private class LifecycleOnBackPressedCancellable implements LifecycleEventObserver, Cancellable
    {
        @Nullable
        private Cancellable mCurrentCancellable;
        private final Lifecycle mLifecycle;
        private final OnBackPressedCallback mOnBackPressedCallback;
        final OnBackPressedDispatcher this$0;
        
        LifecycleOnBackPressedCancellable(@NonNull final OnBackPressedDispatcher this$0, @NonNull final Lifecycle mLifecycle, final OnBackPressedCallback mOnBackPressedCallback) {
            this.this$0 = this$0;
            this.mLifecycle = mLifecycle;
            this.mOnBackPressedCallback = mOnBackPressedCallback;
            mLifecycle.addObserver(this);
        }
        
        @Override
        public void cancel() {
            this.mLifecycle.removeObserver(this);
            this.mOnBackPressedCallback.removeCancellable(this);
            final Cancellable mCurrentCancellable = this.mCurrentCancellable;
            if (mCurrentCancellable != null) {
                mCurrentCancellable.cancel();
                this.mCurrentCancellable = null;
            }
        }
        
        @Override
        public void onStateChanged(@NonNull final LifecycleOwner lifecycleOwner, @NonNull final Lifecycle.Event event) {
            if (event == Lifecycle.Event.ON_START) {
                this.mCurrentCancellable = this.this$0.addCancellableCallback(this.mOnBackPressedCallback);
            }
            else if (event == Lifecycle.Event.ON_STOP) {
                final Cancellable mCurrentCancellable = this.mCurrentCancellable;
                if (mCurrentCancellable != null) {
                    mCurrentCancellable.cancel();
                }
            }
            else if (event == Lifecycle.Event.ON_DESTROY) {
                this.cancel();
            }
        }
    }
    
    private class OnBackPressedCancellable implements Cancellable
    {
        private final OnBackPressedCallback mOnBackPressedCallback;
        final OnBackPressedDispatcher this$0;
        
        OnBackPressedCancellable(final OnBackPressedDispatcher this$0, final OnBackPressedCallback mOnBackPressedCallback) {
            this.this$0 = this$0;
            this.mOnBackPressedCallback = mOnBackPressedCallback;
        }
        
        @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
        @Override
        public void cancel() {
            this.this$0.mOnBackPressedCallbacks.remove(this.mOnBackPressedCallback);
            this.mOnBackPressedCallback.removeCancellable(this);
            if (BuildCompat.isAtLeastT()) {
                this.mOnBackPressedCallback.setIsEnabledConsumer(null);
                this.this$0.updateBackInvokedCallbackState();
            }
        }
    }
}

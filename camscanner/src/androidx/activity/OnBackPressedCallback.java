// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import androidx.annotation.Nullable;
import androidx.core.os.BuildCompat;
import androidx.annotation.OptIn;
import java.util.Iterator;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.core.util.Consumer;
import java.util.concurrent.CopyOnWriteArrayList;

public abstract class OnBackPressedCallback
{
    private CopyOnWriteArrayList<Cancellable> mCancellables;
    private boolean mEnabled;
    private Consumer<Boolean> mEnabledConsumer;
    
    public OnBackPressedCallback(final boolean mEnabled) {
        this.mCancellables = new CopyOnWriteArrayList<Cancellable>();
        this.mEnabled = mEnabled;
    }
    
    void addCancellable(@NonNull final Cancellable e) {
        this.mCancellables.add(e);
    }
    
    @MainThread
    public abstract void handleOnBackPressed();
    
    @MainThread
    public final boolean isEnabled() {
        return this.mEnabled;
    }
    
    @MainThread
    public final void remove() {
        final Iterator<Cancellable> iterator = this.mCancellables.iterator();
        while (iterator.hasNext()) {
            iterator.next().cancel();
        }
    }
    
    void removeCancellable(@NonNull final Cancellable o) {
        this.mCancellables.remove(o);
    }
    
    @MainThread
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    public final void setEnabled(final boolean b) {
        this.mEnabled = b;
        final Consumer<Boolean> mEnabledConsumer = this.mEnabledConsumer;
        if (mEnabledConsumer != null) {
            mEnabledConsumer.accept(b);
        }
    }
    
    void setIsEnabledConsumer(@Nullable final Consumer<Boolean> mEnabledConsumer) {
        this.mEnabledConsumer = mEnabledConsumer;
    }
}

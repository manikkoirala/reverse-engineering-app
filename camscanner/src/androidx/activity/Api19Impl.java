// 
// Decompiled by Procyon v0.6.0
// 

package androidx.activity;

import kotlin.jvm.internal.Intrinsics;
import android.view.View;
import org.jetbrains.annotations.NotNull;
import androidx.annotation.RequiresApi;
import kotlin.Metadata;

@Metadata
@RequiresApi(19)
public final class Api19Impl
{
    @NotNull
    public static final Api19Impl INSTANCE;
    
    static {
        INSTANCE = new Api19Impl();
    }
    
    private Api19Impl() {
    }
    
    public final boolean isAttachedToWindow(@NotNull final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "view");
        return view.isAttachedToWindow();
    }
}

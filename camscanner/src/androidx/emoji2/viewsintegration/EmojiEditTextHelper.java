// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.viewsintegration;

import android.widget.TextView;
import android.text.method.NumberKeyListener;
import android.text.TextWatcher;
import androidx.annotation.RequiresApi;
import androidx.annotation.IntRange;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import androidx.annotation.Nullable;
import android.text.method.KeyListener;
import androidx.annotation.RestrictTo;
import androidx.core.util.Preconditions;
import androidx.annotation.NonNull;
import android.widget.EditText;

public final class EmojiEditTextHelper
{
    private int mEmojiReplaceStrategy;
    private final HelperInternal mHelper;
    private int mMaxEmojiCount;
    
    public EmojiEditTextHelper(@NonNull final EditText editText) {
        this(editText, true);
    }
    
    public EmojiEditTextHelper(@NonNull final EditText editText, final boolean b) {
        this.mMaxEmojiCount = Integer.MAX_VALUE;
        this.mEmojiReplaceStrategy = 0;
        Preconditions.checkNotNull(editText, "editText cannot be null");
        this.mHelper = (HelperInternal)new HelperInternal19(editText, b);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public int getEmojiReplaceStrategy() {
        return this.mEmojiReplaceStrategy;
    }
    
    @Nullable
    public KeyListener getKeyListener(@Nullable final KeyListener keyListener) {
        return this.mHelper.getKeyListener(keyListener);
    }
    
    public int getMaxEmojiCount() {
        return this.mMaxEmojiCount;
    }
    
    public boolean isEnabled() {
        return this.mHelper.isEnabled();
    }
    
    @Nullable
    public InputConnection onCreateInputConnection(@Nullable final InputConnection inputConnection, @NonNull final EditorInfo editorInfo) {
        if (inputConnection == null) {
            return null;
        }
        return this.mHelper.onCreateInputConnection(inputConnection, editorInfo);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void setEmojiReplaceStrategy(final int n) {
        this.mEmojiReplaceStrategy = n;
        this.mHelper.setEmojiReplaceStrategy(n);
    }
    
    public void setEnabled(final boolean enabled) {
        this.mHelper.setEnabled(enabled);
    }
    
    public void setMaxEmojiCount(@IntRange(from = 0L) final int n) {
        Preconditions.checkArgumentNonnegative(n, "maxEmojiCount should be greater than 0");
        this.mMaxEmojiCount = n;
        this.mHelper.setMaxEmojiCount(n);
    }
    
    static class HelperInternal
    {
        @Nullable
        KeyListener getKeyListener(@Nullable final KeyListener keyListener) {
            return keyListener;
        }
        
        boolean isEnabled() {
            return false;
        }
        
        InputConnection onCreateInputConnection(@NonNull final InputConnection inputConnection, @NonNull final EditorInfo editorInfo) {
            return inputConnection;
        }
        
        void setEmojiReplaceStrategy(final int n) {
        }
        
        void setEnabled(final boolean b) {
        }
        
        void setMaxEmojiCount(final int n) {
        }
    }
    
    @RequiresApi(19)
    private static class HelperInternal19 extends HelperInternal
    {
        private final EditText mEditText;
        private final EmojiTextWatcher mTextWatcher;
        
        HelperInternal19(@NonNull final EditText mEditText, final boolean b) {
            ((TextView)(this.mEditText = mEditText)).addTextChangedListener((TextWatcher)(this.mTextWatcher = new EmojiTextWatcher(mEditText, b)));
            ((TextView)mEditText).setEditableFactory(EmojiEditableFactory.getInstance());
        }
        
        @Override
        KeyListener getKeyListener(@Nullable final KeyListener keyListener) {
            if (keyListener instanceof EmojiKeyListener) {
                return keyListener;
            }
            if (keyListener == null) {
                return null;
            }
            if (keyListener instanceof NumberKeyListener) {
                return keyListener;
            }
            return (KeyListener)new EmojiKeyListener(keyListener);
        }
        
        @Override
        boolean isEnabled() {
            return this.mTextWatcher.isEnabled();
        }
        
        @Override
        InputConnection onCreateInputConnection(@NonNull final InputConnection inputConnection, @NonNull final EditorInfo editorInfo) {
            if (inputConnection instanceof EmojiInputConnection) {
                return inputConnection;
            }
            return (InputConnection)new EmojiInputConnection((TextView)this.mEditText, inputConnection, editorInfo);
        }
        
        @Override
        void setEmojiReplaceStrategy(final int emojiReplaceStrategy) {
            this.mTextWatcher.setEmojiReplaceStrategy(emojiReplaceStrategy);
        }
        
        @Override
        void setEnabled(final boolean enabled) {
            this.mTextWatcher.setEnabled(enabled);
        }
        
        @Override
        void setMaxEmojiCount(final int maxEmojiCount) {
            this.mTextWatcher.setMaxEmojiCount(maxEmojiCount);
        }
    }
}

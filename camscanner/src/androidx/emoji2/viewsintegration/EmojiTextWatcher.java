// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.viewsintegration;

import android.widget.TextView;
import android.view.View;
import java.lang.ref.WeakReference;
import java.lang.ref.Reference;
import android.text.Editable;
import android.text.Spannable;
import android.text.Selection;
import androidx.annotation.Nullable;
import androidx.emoji2.text.EmojiCompat;
import android.widget.EditText;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;
import android.text.TextWatcher;

@RequiresApi(19)
@RestrictTo({ RestrictTo.Scope.LIBRARY })
final class EmojiTextWatcher implements TextWatcher
{
    private final EditText mEditText;
    private int mEmojiReplaceStrategy;
    private boolean mEnabled;
    private final boolean mExpectInitializedEmojiCompat;
    private EmojiCompat.InitCallback mInitCallback;
    private int mMaxEmojiCount;
    
    EmojiTextWatcher(final EditText mEditText, final boolean mExpectInitializedEmojiCompat) {
        this.mMaxEmojiCount = Integer.MAX_VALUE;
        this.mEmojiReplaceStrategy = 0;
        this.mEditText = mEditText;
        this.mExpectInitializedEmojiCompat = mExpectInitializedEmojiCompat;
        this.mEnabled = true;
    }
    
    private EmojiCompat.InitCallback getInitCallback() {
        if (this.mInitCallback == null) {
            this.mInitCallback = new InitCallbackImpl(this.mEditText);
        }
        return this.mInitCallback;
    }
    
    static void processTextOnEnablingEvent(@Nullable final EditText editText, int selectionEnd) {
        if (selectionEnd == 1 && editText != null && ((View)editText).isAttachedToWindow()) {
            final Editable editableText = ((TextView)editText).getEditableText();
            final int selectionStart = Selection.getSelectionStart((CharSequence)editableText);
            selectionEnd = Selection.getSelectionEnd((CharSequence)editableText);
            EmojiCompat.get().process((CharSequence)editableText);
            EmojiInputFilter.updateSelection((Spannable)editableText, selectionStart, selectionEnd);
        }
    }
    
    private boolean shouldSkipForDisabledOrNotConfigured() {
        return !this.mEnabled || (!this.mExpectInitializedEmojiCompat && !EmojiCompat.isConfigured());
    }
    
    public void afterTextChanged(final Editable editable) {
    }
    
    public void beforeTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
    }
    
    int getEmojiReplaceStrategy() {
        return this.mEmojiReplaceStrategy;
    }
    
    int getMaxEmojiCount() {
        return this.mMaxEmojiCount;
    }
    
    public boolean isEnabled() {
        return this.mEnabled;
    }
    
    public void onTextChanged(final CharSequence charSequence, final int n, int loadState, final int n2) {
        if (!((View)this.mEditText).isInEditMode()) {
            if (!this.shouldSkipForDisabledOrNotConfigured()) {
                if (loadState <= n2 && charSequence instanceof Spannable) {
                    loadState = EmojiCompat.get().getLoadState();
                    if (loadState != 0) {
                        if (loadState == 1) {
                            EmojiCompat.get().process(charSequence, n, n + n2, this.mMaxEmojiCount, this.mEmojiReplaceStrategy);
                            return;
                        }
                        if (loadState != 3) {
                            return;
                        }
                    }
                    EmojiCompat.get().registerInitCallback(this.getInitCallback());
                }
            }
        }
    }
    
    void setEmojiReplaceStrategy(final int mEmojiReplaceStrategy) {
        this.mEmojiReplaceStrategy = mEmojiReplaceStrategy;
    }
    
    public void setEnabled(final boolean mEnabled) {
        if (this.mEnabled != mEnabled) {
            if (this.mInitCallback != null) {
                EmojiCompat.get().unregisterInitCallback(this.mInitCallback);
            }
            this.mEnabled = mEnabled;
            if (mEnabled) {
                processTextOnEnablingEvent(this.mEditText, EmojiCompat.get().getLoadState());
            }
        }
    }
    
    void setMaxEmojiCount(final int mMaxEmojiCount) {
        this.mMaxEmojiCount = mMaxEmojiCount;
    }
    
    @RequiresApi(19)
    private static class InitCallbackImpl extends InitCallback
    {
        private final Reference<EditText> mViewRef;
        
        InitCallbackImpl(final EditText referent) {
            this.mViewRef = new WeakReference<EditText>(referent);
        }
        
        @Override
        public void onInitialized() {
            super.onInitialized();
            EmojiTextWatcher.processTextOnEnablingEvent(this.mViewRef.get(), 1);
        }
    }
}

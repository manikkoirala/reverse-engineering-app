// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.viewsintegration;

import androidx.emoji2.text.EmojiCompat;
import androidx.annotation.IntRange;
import android.text.Editable;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import androidx.annotation.NonNull;
import android.widget.TextView;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;
import android.view.inputmethod.InputConnectionWrapper;

@RequiresApi(19)
@RestrictTo({ RestrictTo.Scope.LIBRARY })
final class EmojiInputConnection extends InputConnectionWrapper
{
    private final EmojiCompatDeleteHelper mEmojiCompatDeleteHelper;
    private final TextView mTextView;
    
    EmojiInputConnection(@NonNull final TextView textView, @NonNull final InputConnection inputConnection, @NonNull final EditorInfo editorInfo) {
        this(textView, inputConnection, editorInfo, new EmojiCompatDeleteHelper());
    }
    
    EmojiInputConnection(@NonNull final TextView mTextView, @NonNull final InputConnection inputConnection, @NonNull final EditorInfo editorInfo, @NonNull final EmojiCompatDeleteHelper mEmojiCompatDeleteHelper) {
        super(inputConnection, false);
        this.mTextView = mTextView;
        (this.mEmojiCompatDeleteHelper = mEmojiCompatDeleteHelper).updateEditorInfoAttrs(editorInfo);
    }
    
    private Editable getEditable() {
        return this.mTextView.getEditableText();
    }
    
    public boolean deleteSurroundingText(final int n, final int n2) {
        return this.mEmojiCompatDeleteHelper.handleDeleteSurroundingText((InputConnection)this, this.getEditable(), n, n2, false) || super.deleteSurroundingText(n, n2);
    }
    
    public boolean deleteSurroundingTextInCodePoints(final int n, final int n2) {
        return this.mEmojiCompatDeleteHelper.handleDeleteSurroundingText((InputConnection)this, this.getEditable(), n, n2, true) || super.deleteSurroundingTextInCodePoints(n, n2);
    }
    
    public static class EmojiCompatDeleteHelper
    {
        public boolean handleDeleteSurroundingText(@NonNull final InputConnection inputConnection, @NonNull final Editable editable, @IntRange(from = 0L) final int n, @IntRange(from = 0L) final int n2, final boolean b) {
            return EmojiCompat.handleDeleteSurroundingText(inputConnection, editable, n, n2, b);
        }
        
        public void updateEditorInfoAttrs(@NonNull final EditorInfo editorInfo) {
            if (EmojiCompat.isConfigured()) {
                EmojiCompat.get().updateEditorInfo(editorInfo);
            }
        }
    }
}

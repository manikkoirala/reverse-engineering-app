// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.viewsintegration;

import androidx.emoji2.text.EmojiCompat;
import androidx.annotation.NonNull;
import android.view.KeyEvent;
import android.text.Editable;
import android.view.View;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;
import android.text.method.KeyListener;

@RequiresApi(19)
@RestrictTo({ RestrictTo.Scope.LIBRARY })
final class EmojiKeyListener implements KeyListener
{
    private final EmojiCompatHandleKeyDownHelper mEmojiCompatHandleKeyDownHelper;
    private final KeyListener mKeyListener;
    
    EmojiKeyListener(final KeyListener keyListener) {
        this(keyListener, new EmojiCompatHandleKeyDownHelper());
    }
    
    EmojiKeyListener(final KeyListener mKeyListener, final EmojiCompatHandleKeyDownHelper mEmojiCompatHandleKeyDownHelper) {
        this.mKeyListener = mKeyListener;
        this.mEmojiCompatHandleKeyDownHelper = mEmojiCompatHandleKeyDownHelper;
    }
    
    public void clearMetaKeyState(final View view, final Editable editable, final int n) {
        this.mKeyListener.clearMetaKeyState(view, editable, n);
    }
    
    public int getInputType() {
        return this.mKeyListener.getInputType();
    }
    
    public boolean onKeyDown(final View view, final Editable editable, final int n, final KeyEvent keyEvent) {
        return this.mEmojiCompatHandleKeyDownHelper.handleKeyDown(editable, n, keyEvent) || this.mKeyListener.onKeyDown(view, editable, n, keyEvent);
    }
    
    public boolean onKeyOther(final View view, final Editable editable, final KeyEvent keyEvent) {
        return this.mKeyListener.onKeyOther(view, editable, keyEvent);
    }
    
    public boolean onKeyUp(final View view, final Editable editable, final int n, final KeyEvent keyEvent) {
        return this.mKeyListener.onKeyUp(view, editable, n, keyEvent);
    }
    
    public static class EmojiCompatHandleKeyDownHelper
    {
        public boolean handleKeyDown(@NonNull final Editable editable, final int n, @NonNull final KeyEvent keyEvent) {
            return EmojiCompat.handleOnKeyDown(editable, n, keyEvent);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.viewsintegration;

import android.graphics.Rect;
import androidx.emoji2.text.EmojiCompat;
import androidx.annotation.NonNull;
import android.view.View;
import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;
import android.text.method.TransformationMethod;

@RequiresApi(19)
@RestrictTo({ RestrictTo.Scope.LIBRARY })
class EmojiTransformationMethod implements TransformationMethod
{
    @Nullable
    private final TransformationMethod mTransformationMethod;
    
    EmojiTransformationMethod(@Nullable final TransformationMethod mTransformationMethod) {
        this.mTransformationMethod = mTransformationMethod;
    }
    
    public TransformationMethod getOriginalTransformationMethod() {
        return this.mTransformationMethod;
    }
    
    public CharSequence getTransformation(@Nullable CharSequence process, @NonNull final View view) {
        if (view.isInEditMode()) {
            return process;
        }
        final TransformationMethod mTransformationMethod = this.mTransformationMethod;
        CharSequence transformation = process;
        if (mTransformationMethod != null) {
            transformation = mTransformationMethod.getTransformation(process, view);
        }
        if ((process = transformation) != null) {
            if (EmojiCompat.get().getLoadState() != 1) {
                process = transformation;
            }
            else {
                process = EmojiCompat.get().process(transformation);
            }
        }
        return process;
    }
    
    public void onFocusChanged(final View view, final CharSequence charSequence, final boolean b, final int n, final Rect rect) {
        final TransformationMethod mTransformationMethod = this.mTransformationMethod;
        if (mTransformationMethod != null) {
            mTransformationMethod.onFocusChanged(view, charSequence, b, n, rect);
        }
    }
}

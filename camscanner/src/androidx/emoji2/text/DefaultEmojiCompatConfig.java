// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import androidx.annotation.RequiresApi;
import androidx.annotation.VisibleForTesting;
import androidx.core.util.Preconditions;
import java.util.Iterator;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Build$VERSION;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import android.content.pm.Signature;
import androidx.core.provider.FontRequest;
import androidx.annotation.RestrictTo;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.content.Context;

public final class DefaultEmojiCompatConfig
{
    private DefaultEmojiCompatConfig() {
    }
    
    @Nullable
    public static FontRequestEmojiCompatConfig create(@NonNull final Context context) {
        return (FontRequestEmojiCompatConfig)new DefaultEmojiCompatConfigFactory(null).create(context);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static class DefaultEmojiCompatConfigFactory
    {
        @NonNull
        private static final String DEFAULT_EMOJI_QUERY = "emojicompat-emoji-font";
        @NonNull
        private static final String INTENT_LOAD_EMOJI_FONT = "androidx.content.action.LOAD_EMOJI_FONT";
        @NonNull
        private static final String TAG = "emoji2.text.DefaultEmojiConfig";
        private final DefaultEmojiCompatConfigHelper mHelper;
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY })
        public DefaultEmojiCompatConfigFactory(@Nullable DefaultEmojiCompatConfigHelper helperForApi) {
            if (helperForApi == null) {
                helperForApi = getHelperForApi();
            }
            this.mHelper = helperForApi;
        }
        
        @Nullable
        private EmojiCompat.Config configOrNull(@NonNull final Context context, @Nullable final FontRequest fontRequest) {
            if (fontRequest == null) {
                return null;
            }
            return new FontRequestEmojiCompatConfig(context, fontRequest);
        }
        
        @NonNull
        private List<List<byte[]>> convertToByteArray(@NonNull final Signature[] array) {
            final ArrayList o = new ArrayList();
            for (int length = array.length, i = 0; i < length; ++i) {
                o.add(array[i].toByteArray());
            }
            return (List<List<byte[]>>)Collections.singletonList(o);
        }
        
        @NonNull
        private FontRequest generateFontRequestFrom(@NonNull final ProviderInfo providerInfo, @NonNull final PackageManager packageManager) throws PackageManager$NameNotFoundException {
            final String authority = providerInfo.authority;
            final String packageName = providerInfo.packageName;
            return new FontRequest(authority, packageName, "emojicompat-emoji-font", this.convertToByteArray(this.mHelper.getSigningSignatures(packageManager, packageName)));
        }
        
        @NonNull
        private static DefaultEmojiCompatConfigHelper getHelperForApi() {
            if (Build$VERSION.SDK_INT >= 28) {
                return new DefaultEmojiCompatConfigHelper_API28();
            }
            return new DefaultEmojiCompatConfigHelper_API19();
        }
        
        private boolean hasFlagSystem(@Nullable final ProviderInfo providerInfo) {
            if (providerInfo != null) {
                final ApplicationInfo applicationInfo = providerInfo.applicationInfo;
                if (applicationInfo != null) {
                    final int flags = applicationInfo.flags;
                    final boolean b = true;
                    if ((flags & 0x1) == 0x1) {
                        return b;
                    }
                }
            }
            return false;
        }
        
        @Nullable
        private ProviderInfo queryDefaultInstalledContentProvider(@NonNull final PackageManager packageManager) {
            final Iterator<ResolveInfo> iterator = this.mHelper.queryIntentContentProviders(packageManager, new Intent("androidx.content.action.LOAD_EMOJI_FONT"), 0).iterator();
            while (iterator.hasNext()) {
                final ProviderInfo providerInfo = this.mHelper.getProviderInfo(iterator.next());
                if (this.hasFlagSystem(providerInfo)) {
                    return providerInfo;
                }
            }
            return null;
        }
        
        @Nullable
        @RestrictTo({ RestrictTo.Scope.LIBRARY })
        public EmojiCompat.Config create(@NonNull final Context context) {
            return this.configOrNull(context, this.queryForDefaultFontRequest(context));
        }
        
        @Nullable
        @RestrictTo({ RestrictTo.Scope.LIBRARY })
        @VisibleForTesting
        FontRequest queryForDefaultFontRequest(@NonNull final Context context) {
            final PackageManager packageManager = context.getPackageManager();
            Preconditions.checkNotNull(packageManager, "Package manager required to locate emoji font provider");
            final ProviderInfo queryDefaultInstalledContentProvider = this.queryDefaultInstalledContentProvider(packageManager);
            if (queryDefaultInstalledContentProvider == null) {
                return null;
            }
            try {
                return this.generateFontRequestFrom(queryDefaultInstalledContentProvider, packageManager);
            }
            catch (final PackageManager$NameNotFoundException ex) {
                return null;
            }
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static class DefaultEmojiCompatConfigHelper
    {
        @Nullable
        public ProviderInfo getProviderInfo(@NonNull final ResolveInfo resolveInfo) {
            throw new IllegalStateException("Unable to get provider info prior to API 19");
        }
        
        @NonNull
        public Signature[] getSigningSignatures(@NonNull final PackageManager packageManager, @NonNull final String s) throws PackageManager$NameNotFoundException {
            return packageManager.getPackageInfo(s, 64).signatures;
        }
        
        @NonNull
        public List<ResolveInfo> queryIntentContentProviders(@NonNull final PackageManager packageManager, @NonNull final Intent intent, final int n) {
            return Collections.emptyList();
        }
    }
    
    @RequiresApi(19)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static class DefaultEmojiCompatConfigHelper_API19 extends DefaultEmojiCompatConfigHelper
    {
        @Nullable
        @Override
        public ProviderInfo getProviderInfo(@NonNull final ResolveInfo resolveInfo) {
            return resolveInfo.providerInfo;
        }
        
        @NonNull
        @Override
        public List<ResolveInfo> queryIntentContentProviders(@NonNull final PackageManager packageManager, @NonNull final Intent intent, final int n) {
            return packageManager.queryIntentContentProviders(intent, n);
        }
    }
    
    @RequiresApi(28)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static class DefaultEmojiCompatConfigHelper_API28 extends DefaultEmojiCompatConfigHelper_API19
    {
        @NonNull
        @Override
        public Signature[] getSigningSignatures(@NonNull final PackageManager packageManager, @NonNull final String s) throws PackageManager$NameNotFoundException {
            return packageManager.getPackageInfo(s, 64).signatures;
        }
    }
}

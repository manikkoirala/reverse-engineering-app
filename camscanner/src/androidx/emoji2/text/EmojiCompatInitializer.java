// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import androidx.core.os.TraceCompat;
import androidx.annotation.WorkerThread;
import androidx.annotation.Nullable;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.Collections;
import java.util.List;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.\u3007o00\u3007\u3007Oo;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.ProcessLifecycleInitializer;
import androidx.startup.AppInitializer;
import androidx.lifecycle.LifecycleOwner;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.startup.Initializer;

public class EmojiCompatInitializer implements Initializer<Boolean>
{
    private static final long STARTUP_THREAD_CREATION_DELAY_MS = 500L;
    private static final String S_INITIALIZER_THREAD_NAME = "EmojiCompatInitializer";
    
    @NonNull
    @Override
    public Boolean create(@NonNull final Context context) {
        EmojiCompat.init((EmojiCompat.Config)new BackgroundDefaultConfig(context));
        this.delayUntilFirstResume(context);
        return Boolean.TRUE;
    }
    
    @RequiresApi(19)
    void delayUntilFirstResume(@NonNull final Context context) {
        final Lifecycle lifecycle = AppInitializer.getInstance(context).initializeComponent((Class<? extends Initializer<LifecycleOwner>>)ProcessLifecycleInitializer.class).getLifecycle();
        lifecycle.addObserver(new DefaultLifecycleObserver(this, lifecycle) {
            final EmojiCompatInitializer this$0;
            final Lifecycle val$lifecycle;
            
            @Override
            public void onResume(@NonNull final LifecycleOwner lifecycleOwner) {
                this.this$0.loadEmojiCompatAfterDelay();
                this.val$lifecycle.removeObserver(this);
            }
        });
    }
    
    @NonNull
    @Override
    public List<Class<? extends Initializer<?>>> dependencies() {
        return (List<Class<? extends Initializer<?>>>)Collections.singletonList(ProcessLifecycleInitializer.class);
    }
    
    @RequiresApi(19)
    void loadEmojiCompatAfterDelay() {
        ConcurrencyHelpers.mainHandlerAsync().postDelayed((Runnable)new LoadEmojiCompatRunnable(), 500L);
    }
    
    @RequiresApi(19)
    static class BackgroundDefaultConfig extends Config
    {
        protected BackgroundDefaultConfig(final Context context) {
            super(new BackgroundDefaultLoader(context));
            ((EmojiCompat.Config)this).setMetadataLoadStrategy(1);
        }
    }
    
    @RequiresApi(19)
    static class BackgroundDefaultLoader implements MetadataRepoLoader
    {
        private final Context mContext;
        
        BackgroundDefaultLoader(final Context context) {
            this.mContext = context.getApplicationContext();
        }
        
        @WorkerThread
        void doLoad(@NonNull final MetadataRepoLoaderCallback metadataRepoLoaderCallback, @NonNull final ThreadPoolExecutor loadingExecutor) {
            try {
                final FontRequestEmojiCompatConfig create = DefaultEmojiCompatConfig.create(this.mContext);
                if (create == null) {
                    throw new RuntimeException("EmojiCompat font provider not available on this device.");
                }
                create.setLoadingExecutor(loadingExecutor);
                ((EmojiCompat.Config)create).getMetadataRepoLoader().load(new MetadataRepoLoaderCallback(this, metadataRepoLoaderCallback, loadingExecutor) {
                    final BackgroundDefaultLoader this$0;
                    final ThreadPoolExecutor val$executor;
                    final MetadataRepoLoaderCallback val$loaderCallback;
                    
                    @Override
                    public void onFailed(@Nullable final Throwable t) {
                        try {
                            this.val$loaderCallback.onFailed(t);
                        }
                        finally {
                            this.val$executor.shutdown();
                        }
                    }
                    
                    @Override
                    public void onLoaded(@NonNull final MetadataRepo metadataRepo) {
                        try {
                            this.val$loaderCallback.onLoaded(metadataRepo);
                        }
                        finally {
                            this.val$executor.shutdown();
                        }
                    }
                });
            }
            finally {
                final Throwable t;
                metadataRepoLoaderCallback.onFailed(t);
                loadingExecutor.shutdown();
            }
        }
        
        @Override
        public void load(@NonNull final MetadataRepoLoaderCallback metadataRepoLoaderCallback) {
            final ThreadPoolExecutor backgroundPriorityExecutor = ConcurrencyHelpers.createBackgroundPriorityExecutor("EmojiCompatInitializer");
            backgroundPriorityExecutor.execute(new \u3007o\u3007(this, metadataRepoLoaderCallback, backgroundPriorityExecutor));
        }
    }
    
    static class LoadEmojiCompatRunnable implements Runnable
    {
        @Override
        public void run() {
            try {
                TraceCompat.beginSection("EmojiCompat.EmojiCompatInitializer.run");
                if (EmojiCompat.isConfigured()) {
                    EmojiCompat.get().load();
                }
            }
            finally {
                TraceCompat.endSection();
            }
        }
    }
}

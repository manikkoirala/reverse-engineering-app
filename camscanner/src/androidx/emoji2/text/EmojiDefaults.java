// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
public class EmojiDefaults
{
    public static final int MAX_EMOJI_COUNT = Integer.MAX_VALUE;
    
    private EmojiDefaults() {
    }
}

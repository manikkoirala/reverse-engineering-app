// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.os.Looper;
import android.os.Build$VERSION;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.Objects;
import java.util.concurrent.Executor;
import androidx.annotation.NonNull;
import android.os.Handler;

class ConcurrencyHelpers
{
    private static final int FONT_LOAD_TIMEOUT_SECONDS = 15;
    
    private ConcurrencyHelpers() {
    }
    
    @Deprecated
    @NonNull
    static Executor convertHandlerToExecutor(@NonNull final Handler obj) {
        Objects.requireNonNull(obj);
        return new \u3007o00\u3007\u3007Oo(obj);
    }
    
    static ThreadPoolExecutor createBackgroundPriorityExecutor(@NonNull final String s) {
        final ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(0, 1, 15L, TimeUnit.SECONDS, new LinkedBlockingDeque<Runnable>(), new \u3007080(s));
        threadPoolExecutor.allowCoreThreadTimeOut(true);
        return threadPoolExecutor;
    }
    
    static Handler mainHandlerAsync() {
        if (Build$VERSION.SDK_INT >= 28) {
            return Handler28Impl.createAsync(Looper.getMainLooper());
        }
        return new Handler(Looper.getMainLooper());
    }
    
    @RequiresApi(28)
    static class Handler28Impl
    {
        private Handler28Impl() {
        }
        
        @DoNotInline
        public static Handler createAsync(final Looper looper) {
            return androidx.arch.core.executor.\u3007080.\u3007080(looper);
        }
    }
}

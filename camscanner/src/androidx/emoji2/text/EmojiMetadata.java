// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.graphics.Paint;
import android.graphics.Canvas;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.emoji2.text.flatbuffer.MetadataItem;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;
import androidx.annotation.AnyThread;

@AnyThread
@RequiresApi(19)
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class EmojiMetadata
{
    public static final int HAS_GLYPH_ABSENT = 1;
    public static final int HAS_GLYPH_EXISTS = 2;
    public static final int HAS_GLYPH_UNKNOWN = 0;
    private static final ThreadLocal<MetadataItem> sMetadataItem;
    private volatile int mHasGlyph;
    private final int mIndex;
    @NonNull
    private final MetadataRepo mMetadataRepo;
    
    static {
        sMetadataItem = new ThreadLocal<MetadataItem>();
    }
    
    EmojiMetadata(@NonNull final MetadataRepo mMetadataRepo, @IntRange(from = 0L) final int mIndex) {
        this.mHasGlyph = 0;
        this.mMetadataRepo = mMetadataRepo;
        this.mIndex = mIndex;
    }
    
    private MetadataItem getMetadataItem() {
        final ThreadLocal<MetadataItem> sMetadataItem = EmojiMetadata.sMetadataItem;
        MetadataItem value;
        if ((value = sMetadataItem.get()) == null) {
            value = new MetadataItem();
            sMetadataItem.set(value);
        }
        this.mMetadataRepo.getMetadataList().list(value, this.mIndex);
        return value;
    }
    
    public void draw(@NonNull final Canvas canvas, final float n, final float n2, @NonNull final Paint paint) {
        final Typeface typeface = this.mMetadataRepo.getTypeface();
        final Typeface typeface2 = paint.getTypeface();
        paint.setTypeface(typeface);
        canvas.drawText(this.mMetadataRepo.getEmojiCharArray(), this.mIndex * 2, 2, n, n2, paint);
        paint.setTypeface(typeface2);
    }
    
    public int getCodepointAt(final int n) {
        return this.getMetadataItem().codepoints(n);
    }
    
    public int getCodepointsLength() {
        return this.getMetadataItem().codepointsLength();
    }
    
    public short getCompatAdded() {
        return this.getMetadataItem().compatAdded();
    }
    
    @SuppressLint({ "KotlinPropertyAccess" })
    public int getHasGlyph() {
        return this.mHasGlyph;
    }
    
    public short getHeight() {
        return this.getMetadataItem().height();
    }
    
    public int getId() {
        return this.getMetadataItem().id();
    }
    
    public short getSdkAdded() {
        return this.getMetadataItem().sdkAdded();
    }
    
    @NonNull
    public Typeface getTypeface() {
        return this.mMetadataRepo.getTypeface();
    }
    
    public short getWidth() {
        return this.getMetadataItem().width();
    }
    
    public boolean isDefaultEmoji() {
        return this.getMetadataItem().emojiStyle();
    }
    
    @RestrictTo({ RestrictTo.Scope.TESTS })
    public void resetHasGlyphCache() {
        this.mHasGlyph = 0;
    }
    
    @SuppressLint({ "KotlinPropertyAccess" })
    public void setHasGlyph(final boolean b) {
        int mHasGlyph;
        if (b) {
            mHasGlyph = 2;
        }
        else {
            mHasGlyph = 1;
        }
        this.mHasGlyph = mHasGlyph;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append(", id:");
        sb.append(Integer.toHexString(this.getId()));
        sb.append(", codepoints:");
        for (int codepointsLength = this.getCodepointsLength(), i = 0; i < codepointsLength; ++i) {
            sb.append(Integer.toHexString(this.getCodepointAt(i)));
            sb.append(" ");
        }
        return sb.toString();
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface HasGlyph {
    }
}

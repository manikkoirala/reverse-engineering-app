// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import android.util.SparseArray;
import androidx.annotation.VisibleForTesting;
import androidx.core.util.Preconditions;
import java.nio.ByteBuffer;
import java.io.InputStream;
import androidx.annotation.RestrictTo;
import java.io.IOException;
import androidx.core.os.TraceCompat;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import androidx.emoji2.text.flatbuffer.MetadataList;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.annotation.AnyThread;

@AnyThread
@RequiresApi(19)
public final class MetadataRepo
{
    private static final int DEFAULT_ROOT_SIZE = 1024;
    private static final String S_TRACE_CREATE_REPO = "EmojiCompat.MetadataRepo.create";
    @NonNull
    private final char[] mEmojiCharArray;
    @NonNull
    private final MetadataList mMetadataList;
    @NonNull
    private final Node mRootNode;
    @NonNull
    private final Typeface mTypeface;
    
    private MetadataRepo(@NonNull final Typeface mTypeface, @NonNull final MetadataList mMetadataList) {
        this.mTypeface = mTypeface;
        this.mMetadataList = mMetadataList;
        this.mRootNode = new Node(1024);
        this.mEmojiCharArray = new char[mMetadataList.listLength() * 2];
        this.constructIndex(mMetadataList);
    }
    
    private void constructIndex(final MetadataList list) {
        for (int listLength = list.listLength(), i = 0; i < listLength; ++i) {
            final EmojiMetadata emojiMetadata = new EmojiMetadata(this, i);
            Character.toChars(emojiMetadata.getId(), this.mEmojiCharArray, i * 2);
            this.put(emojiMetadata);
        }
    }
    
    @NonNull
    public static MetadataRepo create(@NonNull final AssetManager assetManager, @NonNull final String s) throws IOException {
        try {
            TraceCompat.beginSection("EmojiCompat.MetadataRepo.create");
            return new MetadataRepo(Typeface.createFromAsset(assetManager, s), MetadataListReader.read(assetManager, s));
        }
        finally {
            TraceCompat.endSection();
        }
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.TESTS })
    public static MetadataRepo create(@NonNull final Typeface typeface) {
        try {
            TraceCompat.beginSection("EmojiCompat.MetadataRepo.create");
            return new MetadataRepo(typeface, new MetadataList());
        }
        finally {
            TraceCompat.endSection();
        }
    }
    
    @NonNull
    public static MetadataRepo create(@NonNull final Typeface typeface, @NonNull final InputStream inputStream) throws IOException {
        try {
            TraceCompat.beginSection("EmojiCompat.MetadataRepo.create");
            return new MetadataRepo(typeface, MetadataListReader.read(inputStream));
        }
        finally {
            TraceCompat.endSection();
        }
    }
    
    @NonNull
    public static MetadataRepo create(@NonNull final Typeface typeface, @NonNull final ByteBuffer byteBuffer) throws IOException {
        try {
            TraceCompat.beginSection("EmojiCompat.MetadataRepo.create");
            return new MetadataRepo(typeface, MetadataListReader.read(byteBuffer));
        }
        finally {
            TraceCompat.endSection();
        }
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public char[] getEmojiCharArray() {
        return this.mEmojiCharArray;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public MetadataList getMetadataList() {
        return this.mMetadataList;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    int getMetadataVersion() {
        return this.mMetadataList.version();
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    Node getRootNode() {
        return this.mRootNode;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    Typeface getTypeface() {
        return this.mTypeface;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    @VisibleForTesting
    void put(@NonNull final EmojiMetadata emojiMetadata) {
        Preconditions.checkNotNull(emojiMetadata, "emoji metadata cannot be null");
        Preconditions.checkArgument(emojiMetadata.getCodepointsLength() > 0, (Object)"invalid metadata codepoint length");
        this.mRootNode.put(emojiMetadata, 0, emojiMetadata.getCodepointsLength() - 1);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    static class Node
    {
        private final SparseArray<Node> mChildren;
        private EmojiMetadata mData;
        
        private Node() {
            this(1);
        }
        
        Node(final int n) {
            this.mChildren = (SparseArray<Node>)new SparseArray(n);
        }
        
        Node get(final int n) {
            final SparseArray<Node> mChildren = this.mChildren;
            Node node;
            if (mChildren == null) {
                node = null;
            }
            else {
                node = (Node)mChildren.get(n);
            }
            return node;
        }
        
        final EmojiMetadata getData() {
            return this.mData;
        }
        
        void put(@NonNull final EmojiMetadata mData, final int n, final int n2) {
            Node value;
            if ((value = this.get(mData.getCodepointAt(n))) == null) {
                value = new Node();
                this.mChildren.put(mData.getCodepointAt(n), (Object)value);
            }
            if (n2 > n) {
                value.put(mData, n + 1, n2);
            }
            else {
                value.mData = mData;
            }
        }
    }
}

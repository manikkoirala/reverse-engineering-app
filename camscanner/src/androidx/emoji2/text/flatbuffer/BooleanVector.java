// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

import java.nio.ByteBuffer;

public final class BooleanVector extends BaseVector
{
    public BooleanVector __assign(final int n, final ByteBuffer byteBuffer) {
        this.__reset(n, 1, byteBuffer);
        return this;
    }
    
    public boolean get(final int n) {
        return super.bb.get(this.__element(n)) != 0;
    }
}

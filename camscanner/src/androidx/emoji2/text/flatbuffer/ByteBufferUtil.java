// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

import java.nio.ByteBuffer;

public class ByteBufferUtil
{
    public static int getSizePrefix(final ByteBuffer byteBuffer) {
        return byteBuffer.getInt(byteBuffer.position());
    }
    
    public static ByteBuffer removeSizePrefix(ByteBuffer duplicate) {
        duplicate = duplicate.duplicate();
        duplicate.position(duplicate.position() + 4);
        return duplicate;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

import java.nio.ByteBuffer;

public final class Utf8Safe extends Utf8
{
    private static int computeEncodedLength(final CharSequence charSequence) {
        int length;
        int n;
        for (length = charSequence.length(), n = 0; n < length && charSequence.charAt(n) < '\u0080'; ++n) {}
        int n2 = length;
        int n3;
        while (true) {
            n3 = n2;
            if (n >= length) {
                break;
            }
            final char char1 = charSequence.charAt(n);
            if (char1 >= '\u0800') {
                n3 = n2 + encodedLengthGeneral(charSequence, n);
                break;
            }
            n2 += '\u007f' - char1 >>> 31;
            ++n;
        }
        if (n3 >= length) {
            return n3;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("UTF-8 length does not fit in int: ");
        sb.append(n3 + 4294967296L);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static String decodeUtf8Array(final byte[] array, int i, int j) {
        if ((i | j | array.length - i - j) >= 0) {
            final int n = i + j;
            final char[] value = new char[j];
            j = 0;
            while (i < n) {
                final byte b = array[i];
                if (!DecodeUtil.isOneByte(b)) {
                    break;
                }
                ++i;
                DecodeUtil.handleOneByte(b, value, j);
                ++j;
            }
            final int n2 = j;
            j = i;
            i = n2;
            while (j < n) {
                final int n3 = j + 1;
                final byte b2 = array[j];
                if (DecodeUtil.isOneByte(b2)) {
                    j = i + 1;
                    DecodeUtil.handleOneByte(b2, value, i);
                    i = j;
                    j = n3;
                    while (j < n) {
                        final byte b3 = array[j];
                        if (!DecodeUtil.isOneByte(b3)) {
                            break;
                        }
                        ++j;
                        DecodeUtil.handleOneByte(b3, value, i);
                        ++i;
                    }
                }
                else if (DecodeUtil.isTwoBytes(b2)) {
                    if (n3 >= n) {
                        throw new IllegalArgumentException("Invalid UTF-8");
                    }
                    DecodeUtil.handleTwoBytes(b2, array[n3], value, i);
                    j = n3 + 1;
                    ++i;
                }
                else if (DecodeUtil.isThreeBytes(b2)) {
                    if (n3 >= n - 1) {
                        throw new IllegalArgumentException("Invalid UTF-8");
                    }
                    j = n3 + 1;
                    DecodeUtil.handleThreeBytes(b2, array[n3], array[j], value, i);
                    ++j;
                    ++i;
                }
                else {
                    if (n3 >= n - 2) {
                        throw new IllegalArgumentException("Invalid UTF-8");
                    }
                    j = n3 + 1;
                    final byte b4 = array[n3];
                    final int n4 = j + 1;
                    DecodeUtil.handleFourBytes(b2, b4, array[j], array[n4], value, i);
                    j = n4 + 1;
                    i = i + 1 + 1;
                }
            }
            return new String(value, 0, i);
        }
        throw new ArrayIndexOutOfBoundsException(String.format("buffer length=%d, index=%d, size=%d", array.length, i, j));
    }
    
    public static String decodeUtf8Buffer(final ByteBuffer byteBuffer, int i, int j) {
        if ((i | j | byteBuffer.limit() - i - j) >= 0) {
            final int n = i + j;
            final char[] value = new char[j];
            j = 0;
            while (i < n) {
                final byte value2 = byteBuffer.get(i);
                if (!DecodeUtil.isOneByte(value2)) {
                    break;
                }
                ++i;
                DecodeUtil.handleOneByte(value2, value, j);
                ++j;
            }
            final int n2 = j;
            j = i;
            i = n2;
            while (j < n) {
                final int n3 = j + 1;
                final byte value3 = byteBuffer.get(j);
                if (DecodeUtil.isOneByte(value3)) {
                    j = i + 1;
                    DecodeUtil.handleOneByte(value3, value, i);
                    i = j;
                    j = n3;
                    while (j < n) {
                        final byte value4 = byteBuffer.get(j);
                        if (!DecodeUtil.isOneByte(value4)) {
                            break;
                        }
                        ++j;
                        DecodeUtil.handleOneByte(value4, value, i);
                        ++i;
                    }
                }
                else if (DecodeUtil.isTwoBytes(value3)) {
                    if (n3 >= n) {
                        throw new IllegalArgumentException("Invalid UTF-8");
                    }
                    DecodeUtil.handleTwoBytes(value3, byteBuffer.get(n3), value, i);
                    j = n3 + 1;
                    ++i;
                }
                else if (DecodeUtil.isThreeBytes(value3)) {
                    if (n3 >= n - 1) {
                        throw new IllegalArgumentException("Invalid UTF-8");
                    }
                    j = n3 + 1;
                    DecodeUtil.handleThreeBytes(value3, byteBuffer.get(n3), byteBuffer.get(j), value, i);
                    ++j;
                    ++i;
                }
                else {
                    if (n3 >= n - 2) {
                        throw new IllegalArgumentException("Invalid UTF-8");
                    }
                    j = n3 + 1;
                    final byte value5 = byteBuffer.get(n3);
                    final int n4 = j + 1;
                    DecodeUtil.handleFourBytes(value3, value5, byteBuffer.get(j), byteBuffer.get(n4), value, i);
                    j = n4 + 1;
                    i = i + 1 + 1;
                }
            }
            return new String(value, 0, i);
        }
        throw new ArrayIndexOutOfBoundsException(String.format("buffer limit=%d, index=%d, limit=%d", byteBuffer.limit(), i, j));
    }
    
    private static int encodeUtf8Array(final CharSequence charSequence, final byte[] array, int i, int j) {
        final int length = charSequence.length();
        final int n = j + i;
        int n2;
        char char1;
        for (j = 0; j < length; ++j) {
            n2 = j + i;
            if (n2 >= n) {
                break;
            }
            char1 = charSequence.charAt(j);
            if (char1 >= '\u0080') {
                break;
            }
            array[n2] = (byte)char1;
        }
        if (j == length) {
            return i + length;
        }
        int k = i + j;
        char char2;
        int n3;
        int n4;
        char char3;
        int n5;
        int n6;
        int n7;
        int n8;
        StringBuilder sb;
        for (i = j; i < length; ++i, k = j) {
            char2 = charSequence.charAt(i);
            if (char2 < '\u0080' && k < n) {
                j = k + 1;
                array[k] = (byte)char2;
            }
            else if (char2 < '\u0800' && k <= n - 2) {
                n3 = k + 1;
                array[k] = (byte)(char2 >>> 6 | 0x3C0);
                j = n3 + 1;
                array[n3] = (byte)((char2 & '?') | 0x80);
            }
            else if ((char2 < '\ud800' || '\udfff' < char2) && k <= n - 3) {
                j = k + 1;
                array[k] = (byte)(char2 >>> 12 | 0x1E0);
                n4 = j + 1;
                array[j] = (byte)((char2 >>> 6 & 0x3F) | 0x80);
                j = n4 + 1;
                array[n4] = (byte)((char2 & '?') | 0x80);
            }
            else {
                if (k <= n - 4) {
                    j = i + 1;
                    if (j != charSequence.length()) {
                        char3 = charSequence.charAt(j);
                        if (Character.isSurrogatePair(char2, char3)) {
                            i = Character.toCodePoint(char2, char3);
                            n5 = k + 1;
                            array[k] = (byte)(i >>> 18 | 0xF0);
                            n6 = n5 + 1;
                            array[n5] = (byte)((i >>> 12 & 0x3F) | 0x80);
                            n7 = n6 + 1;
                            array[n6] = (byte)((i >>> 6 & 0x3F) | 0x80);
                            n8 = n7 + 1;
                            array[n7] = (byte)((i & 0x3F) | 0x80);
                            i = j;
                            j = n8;
                            continue;
                        }
                        i = j;
                    }
                    throw new UnpairedSurrogateException(i - 1, length);
                }
                if ('\ud800' <= char2 && char2 <= '\udfff') {
                    j = i + 1;
                    if (j == charSequence.length() || !Character.isSurrogatePair(char2, charSequence.charAt(j))) {
                        throw new UnpairedSurrogateException(i, length);
                    }
                }
                sb = new StringBuilder();
                sb.append("Failed writing ");
                sb.append(char2);
                sb.append(" at index ");
                sb.append(k);
                throw new ArrayIndexOutOfBoundsException(sb.toString());
            }
        }
        return k;
    }
    
    private static void encodeUtf8Buffer(final CharSequence p0, final ByteBuffer p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokeinterface java/lang/CharSequence.length:()I
        //     6: istore          10
        //     8: aload_1        
        //     9: invokevirtual   java/nio/Buffer.position:()I
        //    12: istore          6
        //    14: iconst_0       
        //    15: istore          5
        //    17: iload           5
        //    19: iload           10
        //    21: if_icmpge       77
        //    24: iload           6
        //    26: istore          8
        //    28: iload           5
        //    30: istore          7
        //    32: aload_0        
        //    33: iload           5
        //    35: invokeinterface java/lang/CharSequence.charAt:(I)C
        //    40: istore          9
        //    42: iload           9
        //    44: sipush          128
        //    47: if_icmpge       77
        //    50: iload           6
        //    52: istore          8
        //    54: iload           5
        //    56: istore          7
        //    58: aload_1        
        //    59: iload           6
        //    61: iload           5
        //    63: iadd           
        //    64: iload           9
        //    66: i2b            
        //    67: invokevirtual   java/nio/ByteBuffer.put:(IB)Ljava/nio/ByteBuffer;
        //    70: pop            
        //    71: iinc            5, 1
        //    74: goto            17
        //    77: iload           5
        //    79: iload           10
        //    81: if_icmpne       103
        //    84: iload           6
        //    86: istore          8
        //    88: iload           5
        //    90: istore          7
        //    92: aload_1        
        //    93: iload           6
        //    95: iload           5
        //    97: iadd           
        //    98: invokevirtual   java/nio/ByteBuffer.position:(I)Ljava/nio/Buffer;
        //   101: pop            
        //   102: return         
        //   103: iload           6
        //   105: iload           5
        //   107: iadd           
        //   108: istore          6
        //   110: iload           5
        //   112: iload           10
        //   114: if_icmpge       590
        //   117: iload           6
        //   119: istore          8
        //   121: iload           5
        //   123: istore          7
        //   125: aload_0        
        //   126: iload           5
        //   128: invokeinterface java/lang/CharSequence.charAt:(I)C
        //   133: istore          4
        //   135: iload           4
        //   137: sipush          128
        //   140: if_icmpge       164
        //   143: iload           6
        //   145: istore          8
        //   147: iload           5
        //   149: istore          7
        //   151: aload_1        
        //   152: iload           6
        //   154: iload           4
        //   156: i2b            
        //   157: invokevirtual   java/nio/ByteBuffer.put:(IB)Ljava/nio/ByteBuffer;
        //   160: pop            
        //   161: goto            581
        //   164: iload           4
        //   166: sipush          2048
        //   169: if_icmpge       238
        //   172: iload           6
        //   174: iconst_1       
        //   175: iadd           
        //   176: istore          8
        //   178: iload           4
        //   180: bipush          6
        //   182: iushr          
        //   183: sipush          192
        //   186: ior            
        //   187: i2b            
        //   188: istore_2       
        //   189: iload           8
        //   191: istore          7
        //   193: aload_1        
        //   194: iload           6
        //   196: iload_2        
        //   197: invokevirtual   java/nio/ByteBuffer.put:(IB)Ljava/nio/ByteBuffer;
        //   200: pop            
        //   201: iload           8
        //   203: istore          7
        //   205: aload_1        
        //   206: iload           8
        //   208: iload           4
        //   210: bipush          63
        //   212: iand           
        //   213: sipush          128
        //   216: ior            
        //   217: i2b            
        //   218: invokevirtual   java/nio/ByteBuffer.put:(IB)Ljava/nio/ByteBuffer;
        //   221: pop            
        //   222: iload           8
        //   224: istore          6
        //   226: goto            581
        //   229: astore          12
        //   231: iload           7
        //   233: istore          8
        //   235: goto            606
        //   238: iload           4
        //   240: ldc             55296
        //   242: if_icmplt       491
        //   245: ldc             57343
        //   247: iload           4
        //   249: if_icmpge       255
        //   252: goto            491
        //   255: iload           5
        //   257: iconst_1       
        //   258: iadd           
        //   259: istore          7
        //   261: iload           7
        //   263: iload           10
        //   265: if_icmpeq       450
        //   268: iload           6
        //   270: istore          5
        //   272: aload_0        
        //   273: iload           7
        //   275: invokeinterface java/lang/CharSequence.charAt:(I)C
        //   280: istore_3       
        //   281: iload           6
        //   283: istore          5
        //   285: iload           4
        //   287: iload_3        
        //   288: invokestatic    java/lang/Character.isSurrogatePair:(CC)Z
        //   291: ifeq            432
        //   294: iload           6
        //   296: istore          5
        //   298: iload           4
        //   300: iload_3        
        //   301: invokestatic    java/lang/Character.toCodePoint:(CC)I
        //   304: istore          11
        //   306: iload           6
        //   308: iconst_1       
        //   309: iadd           
        //   310: istore          8
        //   312: iload           11
        //   314: bipush          18
        //   316: iushr          
        //   317: sipush          240
        //   320: ior            
        //   321: i2b            
        //   322: istore_2       
        //   323: iload           8
        //   325: istore          5
        //   327: aload_1        
        //   328: iload           6
        //   330: iload_2        
        //   331: invokevirtual   java/nio/ByteBuffer.put:(IB)Ljava/nio/ByteBuffer;
        //   334: pop            
        //   335: iload           8
        //   337: iconst_1       
        //   338: iadd           
        //   339: istore          9
        //   341: iload           11
        //   343: bipush          12
        //   345: iushr          
        //   346: bipush          63
        //   348: iand           
        //   349: sipush          128
        //   352: ior            
        //   353: i2b            
        //   354: istore_2       
        //   355: iload           9
        //   357: istore          5
        //   359: aload_1        
        //   360: iload           8
        //   362: iload_2        
        //   363: invokevirtual   java/nio/ByteBuffer.put:(IB)Ljava/nio/ByteBuffer;
        //   366: pop            
        //   367: iload           9
        //   369: iconst_1       
        //   370: iadd           
        //   371: istore          6
        //   373: iload           11
        //   375: bipush          6
        //   377: iushr          
        //   378: bipush          63
        //   380: iand           
        //   381: sipush          128
        //   384: ior            
        //   385: i2b            
        //   386: istore_2       
        //   387: iload           6
        //   389: istore          5
        //   391: aload_1        
        //   392: iload           9
        //   394: iload_2        
        //   395: invokevirtual   java/nio/ByteBuffer.put:(IB)Ljava/nio/ByteBuffer;
        //   398: pop            
        //   399: iload           6
        //   401: istore          5
        //   403: aload_1        
        //   404: iload           6
        //   406: iload           11
        //   408: bipush          63
        //   410: iand           
        //   411: sipush          128
        //   414: ior            
        //   415: i2b            
        //   416: invokevirtual   java/nio/ByteBuffer.put:(IB)Ljava/nio/ByteBuffer;
        //   419: pop            
        //   420: iload           7
        //   422: istore          5
        //   424: goto            581
        //   427: astore          12
        //   429: goto            439
        //   432: iload           7
        //   434: istore          5
        //   436: goto            450
        //   439: iload           5
        //   441: istore          8
        //   443: iload           7
        //   445: istore          5
        //   447: goto            606
        //   450: iload           6
        //   452: istore          8
        //   454: iload           5
        //   456: istore          7
        //   458: new             Landroidx/emoji2/text/flatbuffer/Utf8Safe$UnpairedSurrogateException;
        //   461: astore          12
        //   463: iload           6
        //   465: istore          8
        //   467: iload           5
        //   469: istore          7
        //   471: aload           12
        //   473: iload           5
        //   475: iload           10
        //   477: invokespecial   androidx/emoji2/text/flatbuffer/Utf8Safe$UnpairedSurrogateException.<init>:(II)V
        //   480: iload           6
        //   482: istore          8
        //   484: iload           5
        //   486: istore          7
        //   488: aload           12
        //   490: athrow         
        //   491: iload           6
        //   493: iconst_1       
        //   494: iadd           
        //   495: istore          9
        //   497: iload           4
        //   499: bipush          12
        //   501: iushr          
        //   502: sipush          224
        //   505: ior            
        //   506: i2b            
        //   507: istore_2       
        //   508: iload           9
        //   510: istore          7
        //   512: aload_1        
        //   513: iload           6
        //   515: iload_2        
        //   516: invokevirtual   java/nio/ByteBuffer.put:(IB)Ljava/nio/ByteBuffer;
        //   519: pop            
        //   520: iload           9
        //   522: iconst_1       
        //   523: iadd           
        //   524: istore          6
        //   526: iload           4
        //   528: bipush          6
        //   530: iushr          
        //   531: bipush          63
        //   533: iand           
        //   534: sipush          128
        //   537: ior            
        //   538: i2b            
        //   539: istore_2       
        //   540: iload           6
        //   542: istore          8
        //   544: iload           5
        //   546: istore          7
        //   548: aload_1        
        //   549: iload           9
        //   551: iload_2        
        //   552: invokevirtual   java/nio/ByteBuffer.put:(IB)Ljava/nio/ByteBuffer;
        //   555: pop            
        //   556: iload           6
        //   558: istore          8
        //   560: iload           5
        //   562: istore          7
        //   564: aload_1        
        //   565: iload           6
        //   567: iload           4
        //   569: bipush          63
        //   571: iand           
        //   572: sipush          128
        //   575: ior            
        //   576: i2b            
        //   577: invokevirtual   java/nio/ByteBuffer.put:(IB)Ljava/nio/ByteBuffer;
        //   580: pop            
        //   581: iinc            5, 1
        //   584: iinc            6, 1
        //   587: goto            110
        //   590: iload           6
        //   592: istore          8
        //   594: iload           5
        //   596: istore          7
        //   598: aload_1        
        //   599: iload           6
        //   601: invokevirtual   java/nio/ByteBuffer.position:(I)Ljava/nio/Buffer;
        //   604: pop            
        //   605: return         
        //   606: aload_1        
        //   607: invokevirtual   java/nio/Buffer.position:()I
        //   610: istore          6
        //   612: iload           5
        //   614: iload           8
        //   616: aload_1        
        //   617: invokevirtual   java/nio/Buffer.position:()I
        //   620: isub           
        //   621: iconst_1       
        //   622: iadd           
        //   623: invokestatic    java/lang/Math.max:(II)I
        //   626: istore          7
        //   628: new             Ljava/lang/StringBuilder;
        //   631: dup            
        //   632: invokespecial   java/lang/StringBuilder.<init>:()V
        //   635: astore_1       
        //   636: aload_1        
        //   637: ldc             "Failed writing "
        //   639: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   642: pop            
        //   643: aload_1        
        //   644: aload_0        
        //   645: iload           5
        //   647: invokeinterface java/lang/CharSequence.charAt:(I)C
        //   652: invokevirtual   java/lang/StringBuilder.append:(C)Ljava/lang/StringBuilder;
        //   655: pop            
        //   656: aload_1        
        //   657: ldc             " at index "
        //   659: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   662: pop            
        //   663: aload_1        
        //   664: iload           6
        //   666: iload           7
        //   668: iadd           
        //   669: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   672: pop            
        //   673: new             Ljava/lang/ArrayIndexOutOfBoundsException;
        //   676: dup            
        //   677: aload_1        
        //   678: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   681: invokespecial   java/lang/ArrayIndexOutOfBoundsException.<init>:(Ljava/lang/String;)V
        //   684: athrow         
        //   685: astore          12
        //   687: iload           7
        //   689: istore          5
        //   691: goto            606
        //   694: astore          12
        //   696: goto            439
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                 
        //  -----  -----  -----  -----  -------------------------------------
        //  32     42     685    694    Ljava/lang/IndexOutOfBoundsException;
        //  58     71     685    694    Ljava/lang/IndexOutOfBoundsException;
        //  92     102    685    694    Ljava/lang/IndexOutOfBoundsException;
        //  125    135    685    694    Ljava/lang/IndexOutOfBoundsException;
        //  151    161    685    694    Ljava/lang/IndexOutOfBoundsException;
        //  193    201    229    238    Ljava/lang/IndexOutOfBoundsException;
        //  205    222    229    238    Ljava/lang/IndexOutOfBoundsException;
        //  272    281    694    699    Ljava/lang/IndexOutOfBoundsException;
        //  285    294    694    699    Ljava/lang/IndexOutOfBoundsException;
        //  298    306    694    699    Ljava/lang/IndexOutOfBoundsException;
        //  327    335    427    432    Ljava/lang/IndexOutOfBoundsException;
        //  359    367    694    699    Ljava/lang/IndexOutOfBoundsException;
        //  391    399    427    432    Ljava/lang/IndexOutOfBoundsException;
        //  403    420    427    432    Ljava/lang/IndexOutOfBoundsException;
        //  458    463    685    694    Ljava/lang/IndexOutOfBoundsException;
        //  471    480    685    694    Ljava/lang/IndexOutOfBoundsException;
        //  488    491    685    694    Ljava/lang/IndexOutOfBoundsException;
        //  512    520    229    238    Ljava/lang/IndexOutOfBoundsException;
        //  548    556    685    694    Ljava/lang/IndexOutOfBoundsException;
        //  564    581    685    694    Ljava/lang/IndexOutOfBoundsException;
        //  598    605    685    694    Ljava/lang/IndexOutOfBoundsException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0432:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static int encodedLengthGeneral(final CharSequence seq, int i) {
        final int length = seq.length();
        int n = 0;
        while (i < length) {
            final char char1 = seq.charAt(i);
            int n2;
            if (char1 < '\u0800') {
                n += '\u007f' - char1 >>> 31;
                n2 = i;
            }
            else {
                final int n3 = n += 2;
                n2 = i;
                if ('\ud800' <= char1) {
                    n = n3;
                    n2 = i;
                    if (char1 <= '\udfff') {
                        if (Character.codePointAt(seq, i) < 65536) {
                            throw new UnpairedSurrogateException(i, length);
                        }
                        n2 = i + 1;
                        n = n3;
                    }
                }
            }
            i = n2 + 1;
        }
        return n;
    }
    
    @Override
    public String decodeUtf8(final ByteBuffer byteBuffer, final int n, final int n2) throws IllegalArgumentException {
        if (byteBuffer.hasArray()) {
            return decodeUtf8Array(byteBuffer.array(), byteBuffer.arrayOffset() + n, n2);
        }
        return decodeUtf8Buffer(byteBuffer, n, n2);
    }
    
    @Override
    public void encodeUtf8(final CharSequence charSequence, final ByteBuffer byteBuffer) {
        if (byteBuffer.hasArray()) {
            final int arrayOffset = byteBuffer.arrayOffset();
            byteBuffer.position(encodeUtf8Array(charSequence, byteBuffer.array(), byteBuffer.position() + arrayOffset, byteBuffer.remaining()) - arrayOffset);
        }
        else {
            encodeUtf8Buffer(charSequence, byteBuffer);
        }
    }
    
    @Override
    public int encodedLength(final CharSequence charSequence) {
        return computeEncodedLength(charSequence);
    }
    
    static class UnpairedSurrogateException extends IllegalArgumentException
    {
        UnpairedSurrogateException(final int i, final int j) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unpaired surrogate at index ");
            sb.append(i);
            sb.append(" of ");
            sb.append(j);
            super(sb.toString());
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

import java.nio.ByteBuffer;

public final class StringVector extends BaseVector
{
    private Utf8 utf8;
    
    public StringVector() {
        this.utf8 = Utf8.getDefault();
    }
    
    public StringVector __assign(final int n, final int n2, final ByteBuffer byteBuffer) {
        this.__reset(n, n2, byteBuffer);
        return this;
    }
    
    public String get(final int n) {
        return Table.__string(this.__element(n), super.bb, this.utf8);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.io.InputStream;
import java.util.Arrays;
import java.nio.ByteOrder;
import java.nio.ByteBuffer;

public class FlatBufferBuilder
{
    static final boolean $assertionsDisabled = false;
    ByteBuffer bb;
    ByteBufferFactory bb_factory;
    boolean finished;
    boolean force_defaults;
    int minalign;
    boolean nested;
    int num_vtables;
    int object_start;
    int space;
    final Utf8 utf8;
    int vector_num_elems;
    int[] vtable;
    int vtable_in_use;
    int[] vtables;
    
    public FlatBufferBuilder() {
        this(1024);
    }
    
    public FlatBufferBuilder(final int n) {
        this(n, (ByteBufferFactory)HeapByteBufferFactory.INSTANCE, null, Utf8.getDefault());
    }
    
    public FlatBufferBuilder(final int n, final ByteBufferFactory byteBufferFactory) {
        this(n, byteBufferFactory, null, Utf8.getDefault());
    }
    
    public FlatBufferBuilder(final int n, final ByteBufferFactory bb_factory, final ByteBuffer bb, final Utf8 utf8) {
        this.minalign = 1;
        this.vtable = null;
        this.vtable_in_use = 0;
        this.nested = false;
        this.finished = false;
        this.vtables = new int[16];
        this.num_vtables = 0;
        this.vector_num_elems = 0;
        this.force_defaults = false;
        int n2 = n;
        if (n <= 0) {
            n2 = 1;
        }
        this.bb_factory = bb_factory;
        if (bb != null) {
            (this.bb = bb).clear();
            this.bb.order(ByteOrder.LITTLE_ENDIAN);
        }
        else {
            this.bb = bb_factory.newByteBuffer(n2);
        }
        this.utf8 = utf8;
        this.space = this.bb.capacity();
    }
    
    public FlatBufferBuilder(final ByteBuffer byteBuffer) {
        this(byteBuffer, (ByteBufferFactory)new HeapByteBufferFactory());
    }
    
    public FlatBufferBuilder(final ByteBuffer byteBuffer, final ByteBufferFactory byteBufferFactory) {
        this(byteBuffer.capacity(), byteBufferFactory, byteBuffer, Utf8.getDefault());
    }
    
    @Deprecated
    private int dataStart() {
        this.finished();
        return this.space;
    }
    
    static ByteBuffer growByteBuffer(final ByteBuffer src, final ByteBufferFactory byteBufferFactory) {
        final int capacity = src.capacity();
        if ((0xC0000000 & capacity) == 0x0) {
            int n;
            if (capacity == 0) {
                n = 1;
            }
            else {
                n = capacity << 1;
            }
            src.position(0);
            final ByteBuffer byteBuffer = byteBufferFactory.newByteBuffer(n);
            byteBuffer.position(byteBuffer.clear().capacity() - capacity);
            byteBuffer.put(src);
            return byteBuffer;
        }
        throw new AssertionError((Object)"FlatBuffers: cannot grow buffer beyond 2 gigabytes.");
    }
    
    public static boolean isFieldPresent(final Table table, final int n) {
        return table.__offset(n) != 0;
    }
    
    public void Nested(final int n) {
        if (n == this.offset()) {
            return;
        }
        throw new AssertionError((Object)"FlatBuffers: struct must be serialized inline.");
    }
    
    public void addBoolean(final int n, final boolean b, final boolean b2) {
        if (this.force_defaults || b != b2) {
            this.addBoolean(b);
            this.slot(n);
        }
    }
    
    public void addBoolean(final boolean b) {
        this.prep(1, 0);
        this.putBoolean(b);
    }
    
    public void addByte(final byte b) {
        this.prep(1, 0);
        this.putByte(b);
    }
    
    public void addByte(final int n, final byte b, final int n2) {
        if (this.force_defaults || b != n2) {
            this.addByte(b);
            this.slot(n);
        }
    }
    
    public void addDouble(final double n) {
        this.prep(8, 0);
        this.putDouble(n);
    }
    
    public void addDouble(final int n, final double n2, final double n3) {
        if (this.force_defaults || n2 != n3) {
            this.addDouble(n2);
            this.slot(n);
        }
    }
    
    public void addFloat(final float n) {
        this.prep(4, 0);
        this.putFloat(n);
    }
    
    public void addFloat(final int n, final float n2, final double n3) {
        if (this.force_defaults || n2 != n3) {
            this.addFloat(n2);
            this.slot(n);
        }
    }
    
    public void addInt(final int n) {
        this.prep(4, 0);
        this.putInt(n);
    }
    
    public void addInt(final int n, final int n2, final int n3) {
        if (this.force_defaults || n2 != n3) {
            this.addInt(n2);
            this.slot(n);
        }
    }
    
    public void addLong(final int n, final long n2, final long n3) {
        if (this.force_defaults || n2 != n3) {
            this.addLong(n2);
            this.slot(n);
        }
    }
    
    public void addLong(final long n) {
        this.prep(8, 0);
        this.putLong(n);
    }
    
    public void addOffset(final int n) {
        this.prep(4, 0);
        this.putInt(this.offset() - n + 4);
    }
    
    public void addOffset(final int n, final int n2, final int n3) {
        if (this.force_defaults || n2 != n3) {
            this.addOffset(n2);
            this.slot(n);
        }
    }
    
    public void addShort(final int n, final short n2, final int n3) {
        if (this.force_defaults || n2 != n3) {
            this.addShort(n2);
            this.slot(n);
        }
    }
    
    public void addShort(final short n) {
        this.prep(2, 0);
        this.putShort(n);
    }
    
    public void addStruct(final int n, final int n2, final int n3) {
        if (n2 != n3) {
            this.Nested(n2);
            this.slot(n);
        }
    }
    
    public void clear() {
        this.space = this.bb.capacity();
        this.bb.clear();
        this.minalign = 1;
        while (true) {
            int vtable_in_use = this.vtable_in_use;
            if (vtable_in_use <= 0) {
                break;
            }
            final int[] vtable = this.vtable;
            --vtable_in_use;
            vtable[this.vtable_in_use = vtable_in_use] = 0;
        }
        this.vtable_in_use = 0;
        this.nested = false;
        this.finished = false;
        this.object_start = 0;
        this.num_vtables = 0;
        this.vector_num_elems = 0;
    }
    
    public int createByteVector(final ByteBuffer src) {
        final int remaining = src.remaining();
        this.startVector(1, remaining, 1);
        this.bb.position(this.space -= remaining);
        this.bb.put(src);
        return this.endVector();
    }
    
    public int createByteVector(final byte[] src) {
        final int length = src.length;
        this.startVector(1, length, 1);
        this.bb.position(this.space -= length);
        this.bb.put(src);
        return this.endVector();
    }
    
    public int createByteVector(final byte[] src, final int offset, final int length) {
        this.startVector(1, length, 1);
        this.bb.position(this.space -= length);
        this.bb.put(src, offset, length);
        return this.endVector();
    }
    
    public <T extends Table> int createSortedVectorOfTables(final T t, final int[] array) {
        t.sortTables(array, this.bb);
        return this.createVectorOfTables(array);
    }
    
    public int createString(final CharSequence charSequence) {
        final int encodedLength = this.utf8.encodedLength(charSequence);
        this.addByte((byte)0);
        this.startVector(1, encodedLength, 1);
        this.bb.position(this.space -= encodedLength);
        this.utf8.encodeUtf8(charSequence, this.bb);
        return this.endVector();
    }
    
    public int createString(final ByteBuffer src) {
        final int remaining = src.remaining();
        this.addByte((byte)0);
        this.startVector(1, remaining, 1);
        this.bb.position(this.space -= remaining);
        this.bb.put(src);
        return this.endVector();
    }
    
    public ByteBuffer createUnintializedVector(int space, final int n, final int n2) {
        final int n3 = space * n;
        this.startVector(space, n, n2);
        final ByteBuffer bb = this.bb;
        space = this.space - n3;
        bb.position(this.space = space);
        final ByteBuffer order = this.bb.slice().order(ByteOrder.LITTLE_ENDIAN);
        order.limit(n3);
        return order;
    }
    
    public int createVectorOfTables(final int[] array) {
        this.notNested();
        this.startVector(4, array.length, 4);
        for (int i = array.length - 1; i >= 0; --i) {
            this.addOffset(array[i]);
        }
        return this.endVector();
    }
    
    public ByteBuffer dataBuffer() {
        this.finished();
        return this.bb;
    }
    
    public int endTable() {
        if (this.vtable != null && this.nested) {
            this.addInt(0);
            final int offset = this.offset();
            int n;
            for (n = this.vtable_in_use - 1; n >= 0 && this.vtable[n] == 0; --n) {}
            int n2 = n;
            while (true) {
                final int n3 = n2;
                if (n3 < 0) {
                    break;
                }
                final int n4 = this.vtable[n3];
                int n5;
                if (n4 != 0) {
                    n5 = offset - n4;
                }
                else {
                    n5 = 0;
                }
                this.addShort((short)n5);
                n2 = n3 - 1;
            }
            this.addShort((short)(offset - this.object_start));
            this.addShort((short)((n + 1 + 2) * 2));
            while (true) {
            Label_0223:
                for (int i = 0; i < this.num_vtables; ++i) {
                    final int n6 = this.bb.capacity() - this.vtables[i];
                    final int space = this.space;
                    final short short1 = this.bb.getShort(n6);
                    if (short1 == this.bb.getShort(space)) {
                        for (short n7 = 2; n7 < short1; n7 += 2) {
                            if (this.bb.getShort(n6 + n7) != this.bb.getShort(space + n7)) {
                                continue Label_0223;
                            }
                        }
                        final int n8 = this.vtables[i];
                        if (n8 != 0) {
                            final int space2 = this.bb.capacity() - offset;
                            this.space = space2;
                            this.bb.putInt(space2, n8 - offset);
                        }
                        else {
                            final int num_vtables = this.num_vtables;
                            final int[] vtables = this.vtables;
                            if (num_vtables == vtables.length) {
                                this.vtables = Arrays.copyOf(vtables, num_vtables * 2);
                            }
                            this.vtables[this.num_vtables++] = this.offset();
                            final ByteBuffer bb = this.bb;
                            bb.putInt(bb.capacity() - offset, this.offset() - offset);
                        }
                        this.nested = false;
                        return offset;
                    }
                }
                final int n8 = 0;
                continue;
            }
        }
        throw new AssertionError((Object)"FlatBuffers: endTable called without startTable");
    }
    
    public int endVector() {
        if (this.nested) {
            this.nested = false;
            this.putInt(this.vector_num_elems);
            return this.offset();
        }
        throw new AssertionError((Object)"FlatBuffers: endVector called without startVector");
    }
    
    public void finish(final int n) {
        this.finish(n, false);
    }
    
    public void finish(final int n, final String s) {
        this.finish(n, s, false);
    }
    
    protected void finish(final int n, final String s, final boolean b) {
        final int minalign = this.minalign;
        int n2;
        if (b) {
            n2 = 4;
        }
        else {
            n2 = 0;
        }
        this.prep(minalign, n2 + 8);
        if (s.length() == 4) {
            for (int i = 3; i >= 0; --i) {
                this.addByte((byte)s.charAt(i));
            }
            this.finish(n, b);
            return;
        }
        throw new AssertionError((Object)"FlatBuffers: file identifier must be length 4");
    }
    
    protected void finish(final int n, final boolean b) {
        final int minalign = this.minalign;
        int n2;
        if (b) {
            n2 = 4;
        }
        else {
            n2 = 0;
        }
        this.prep(minalign, n2 + 4);
        this.addOffset(n);
        if (b) {
            this.addInt(this.bb.capacity() - this.space);
        }
        this.bb.position(this.space);
        this.finished = true;
    }
    
    public void finishSizePrefixed(final int n) {
        this.finish(n, true);
    }
    
    public void finishSizePrefixed(final int n, final String s) {
        this.finish(n, s, true);
    }
    
    public void finished() {
        if (this.finished) {
            return;
        }
        throw new AssertionError((Object)"FlatBuffers: you can only access the serialized buffer after it has been finished by FlatBufferBuilder.finish().");
    }
    
    public FlatBufferBuilder forceDefaults(final boolean force_defaults) {
        this.force_defaults = force_defaults;
        return this;
    }
    
    public FlatBufferBuilder init(final ByteBuffer bb, final ByteBufferFactory bb_factory) {
        this.bb_factory = bb_factory;
        (this.bb = bb).clear();
        this.bb.order(ByteOrder.LITTLE_ENDIAN);
        this.minalign = 1;
        this.space = this.bb.capacity();
        this.vtable_in_use = 0;
        this.nested = false;
        this.finished = false;
        this.object_start = 0;
        this.num_vtables = 0;
        this.vector_num_elems = 0;
        return this;
    }
    
    public void notNested() {
        if (!this.nested) {
            return;
        }
        throw new AssertionError((Object)"FlatBuffers: object serialization must not be nested.");
    }
    
    public int offset() {
        return this.bb.capacity() - this.space;
    }
    
    public void pad(final int n) {
        for (int i = 0; i < n; ++i) {
            this.bb.put(--this.space, (byte)0);
        }
    }
    
    public void prep(final int minalign, final int n) {
        if (minalign > this.minalign) {
            this.minalign = minalign;
        }
        final int n2 = ~(this.bb.capacity() - this.space + n) + 1 & minalign - 1;
        while (this.space < n2 + minalign + n) {
            final int capacity = this.bb.capacity();
            final ByteBuffer bb = this.bb;
            if (bb != (this.bb = growByteBuffer(bb, this.bb_factory))) {
                this.bb_factory.releaseByteBuffer(bb);
            }
            this.space += this.bb.capacity() - capacity;
        }
        this.pad(n2);
    }
    
    public void putBoolean(final boolean b) {
        this.bb.put(--this.space, (byte)(b ? 1 : 0));
    }
    
    public void putByte(final byte b) {
        this.bb.put(--this.space, b);
    }
    
    public void putDouble(final double n) {
        this.bb.putDouble(this.space -= 8, n);
    }
    
    public void putFloat(final float n) {
        this.bb.putFloat(this.space -= 4, n);
    }
    
    public void putInt(final int n) {
        this.bb.putInt(this.space -= 4, n);
    }
    
    public void putLong(final long n) {
        this.bb.putLong(this.space -= 8, n);
    }
    
    public void putShort(final short n) {
        this.bb.putShort(this.space -= 2, n);
    }
    
    public void required(int n, final int i) {
        n = this.bb.capacity() - n;
        if (this.bb.getShort(n - this.bb.getInt(n) + i) != 0) {
            n = 1;
        }
        else {
            n = 0;
        }
        if (n != 0) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("FlatBuffers: field ");
        sb.append(i);
        sb.append(" must be set");
        throw new AssertionError((Object)sb.toString());
    }
    
    public byte[] sizedByteArray() {
        return this.sizedByteArray(this.space, this.bb.capacity() - this.space);
    }
    
    public byte[] sizedByteArray(final int n, final int n2) {
        this.finished();
        final byte[] dst = new byte[n2];
        this.bb.position(n);
        this.bb.get(dst);
        return dst;
    }
    
    public InputStream sizedInputStream() {
        this.finished();
        final ByteBuffer duplicate = this.bb.duplicate();
        duplicate.position(this.space);
        duplicate.limit(this.bb.capacity());
        return new ByteBufferBackedInputStream(duplicate);
    }
    
    public void slot(final int n) {
        this.vtable[n] = this.offset();
    }
    
    public void startTable(final int n) {
        this.notNested();
        final int[] vtable = this.vtable;
        if (vtable == null || vtable.length < n) {
            this.vtable = new int[n];
        }
        this.vtable_in_use = n;
        Arrays.fill(this.vtable, 0, n, 0);
        this.nested = true;
        this.object_start = this.offset();
    }
    
    public void startVector(int n, final int vector_num_elems, final int n2) {
        this.notNested();
        this.vector_num_elems = vector_num_elems;
        n *= vector_num_elems;
        this.prep(4, n);
        this.prep(n2, n);
        this.nested = true;
    }
    
    static class ByteBufferBackedInputStream extends InputStream
    {
        ByteBuffer buf;
        
        public ByteBufferBackedInputStream(final ByteBuffer buf) {
            this.buf = buf;
        }
        
        @Override
        public int read() throws IOException {
            try {
                return this.buf.get() & 0xFF;
            }
            catch (final BufferUnderflowException ex) {
                return -1;
            }
        }
    }
    
    public abstract static class ByteBufferFactory
    {
        public abstract ByteBuffer newByteBuffer(final int p0);
        
        public void releaseByteBuffer(final ByteBuffer byteBuffer) {
        }
    }
    
    public static final class HeapByteBufferFactory extends ByteBufferFactory
    {
        public static final HeapByteBufferFactory INSTANCE;
        
        static {
            INSTANCE = new HeapByteBufferFactory();
        }
        
        @Override
        public ByteBuffer newByteBuffer(final int capacity) {
            return ByteBuffer.allocate(capacity).order(ByteOrder.LITTLE_ENDIAN);
        }
    }
}

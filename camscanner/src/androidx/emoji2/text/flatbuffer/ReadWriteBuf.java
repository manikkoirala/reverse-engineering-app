// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

interface ReadWriteBuf extends ReadBuf
{
    int limit();
    
    void put(final byte p0);
    
    void put(final byte[] p0, final int p1, final int p2);
    
    void putBoolean(final boolean p0);
    
    void putDouble(final double p0);
    
    void putFloat(final float p0);
    
    void putInt(final int p0);
    
    void putLong(final long p0);
    
    void putShort(final short p0);
    
    boolean requestCapacity(final int p0);
    
    void set(final int p0, final byte p1);
    
    void set(final int p0, final byte[] p1, final int p2, final int p3);
    
    void setBoolean(final int p0, final boolean p1);
    
    void setDouble(final int p0, final double p1);
    
    void setFloat(final int p0, final float p1);
    
    void setInt(final int p0, final int p1);
    
    void setLong(final int p0, final long p1);
    
    void setShort(final int p0, final short p1);
    
    int writePosition();
}

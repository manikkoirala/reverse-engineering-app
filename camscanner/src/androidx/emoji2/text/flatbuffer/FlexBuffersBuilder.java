// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

import java.math.BigInteger;
import java.util.Collections;
import java.nio.charset.StandardCharsets;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Comparator;

public class FlexBuffersBuilder
{
    static final boolean $assertionsDisabled = false;
    public static final int BUILDER_FLAG_NONE = 0;
    public static final int BUILDER_FLAG_SHARE_ALL = 7;
    public static final int BUILDER_FLAG_SHARE_KEYS = 1;
    public static final int BUILDER_FLAG_SHARE_KEYS_AND_STRINGS = 3;
    public static final int BUILDER_FLAG_SHARE_KEY_VECTORS = 4;
    public static final int BUILDER_FLAG_SHARE_STRINGS = 2;
    private static final int WIDTH_16 = 1;
    private static final int WIDTH_32 = 2;
    private static final int WIDTH_64 = 3;
    private static final int WIDTH_8 = 0;
    private final ReadWriteBuf bb;
    private boolean finished;
    private final int flags;
    private Comparator<Value> keyComparator;
    private final HashMap<String, Integer> keyPool;
    private final ArrayList<Value> stack;
    private final HashMap<String, Integer> stringPool;
    
    public FlexBuffersBuilder() {
        this(256);
    }
    
    public FlexBuffersBuilder(final int n) {
        this(new ArrayReadWriteBuf(n), 1);
    }
    
    public FlexBuffersBuilder(final ReadWriteBuf bb, final int flags) {
        this.stack = new ArrayList<Value>();
        this.keyPool = new HashMap<String, Integer>();
        this.stringPool = new HashMap<String, Integer>();
        this.finished = false;
        this.keyComparator = new Comparator<Value>() {
            final FlexBuffersBuilder this$0;
            
            @Override
            public int compare(final Value value, final Value value2) {
                int key = value.key;
                int key2 = value2.key;
                byte value3;
                byte value4;
                do {
                    value3 = this.this$0.bb.get(key);
                    value4 = this.this$0.bb.get(key2);
                    if (value3 == 0) {
                        return value3 - value4;
                    }
                    ++key;
                    ++key2;
                } while (value3 == value4);
                return value3 - value4;
            }
        };
        this.bb = bb;
        this.flags = flags;
    }
    
    public FlexBuffersBuilder(final ByteBuffer byteBuffer) {
        this(byteBuffer, 1);
    }
    
    @Deprecated
    public FlexBuffersBuilder(final ByteBuffer byteBuffer, final int n) {
        this(new ArrayReadWriteBuf(byteBuffer.array()), n);
    }
    
    private int align(int i) {
        final int n = 1 << i;
        for (i = paddingBytes(this.bb.writePosition(), n); i != 0; --i) {
            this.bb.put((byte)0);
        }
        return n;
    }
    
    private Value createKeyVector(int i, int a) {
        final long n = a;
        a = Math.max(0, widthUInBits(n));
        long n2;
        int writePosition;
        for (int j = i; j < this.stack.size(); ++j, a = Math.max(a, elemWidth(4, 0, n2, writePosition, j))) {
            n2 = this.stack.get(j).key;
            writePosition = this.bb.writePosition();
        }
        final int align = this.align(a);
        this.writeInt(n, align);
        final int writePosition2 = this.bb.writePosition();
        while (i < this.stack.size()) {
            final int key = this.stack.get(i).key;
            this.writeOffset(this.stack.get(i).key, align);
            ++i;
        }
        return new Value(-1, FlexBuffers.toTypedVector(4, 0), a, writePosition2);
    }
    
    private Value createVector(final int n, int i, int n2, final boolean b, final boolean b2, final Value value) {
        int n3 = n2;
        final long n4 = n3;
        n2 = Math.max(0, widthUInBits(n4));
        int n5;
        if (value != null) {
            n2 = Math.max(n2, value.elemWidth(this.bb.writePosition(), 0));
            n5 = 3;
        }
        else {
            n5 = 1;
        }
        int n6 = 4;
        int max;
        for (int j = i; j < this.stack.size(); ++j, n6 = n2, n2 = max) {
            max = Math.max(n2, this.stack.get(j).elemWidth(this.bb.writePosition(), j + n5));
            n2 = n6;
            if (b) {
                n2 = n6;
                if (j == i) {
                    n2 = this.stack.get(j).type;
                    if (!FlexBuffers.isTypedVectorElementType(n2)) {
                        throw new FlexBuffers.FlexBufferException("TypedVector does not support this element type");
                    }
                }
            }
        }
        final int align = this.align(n2);
        if (value != null) {
            this.writeOffset(value.iValue, align);
            this.writeInt(1L << value.minBitWidth, align);
        }
        if (!b2) {
            this.writeInt(n4, align);
        }
        final int writePosition = this.bb.writePosition();
        for (int k = i; k < this.stack.size(); ++k) {
            this.writeAny(this.stack.get(k), align);
        }
        if (!b) {
            while (i < this.stack.size()) {
                this.bb.put(this.stack.get(i).storedPackedType(n2));
                ++i;
            }
        }
        if (value != null) {
            i = 9;
        }
        else if (b) {
            if (!b2) {
                n3 = 0;
            }
            i = FlexBuffers.toTypedVector(n6, n3);
        }
        else {
            i = 10;
        }
        return new Value(n, i, n2, writePosition);
    }
    
    private int putKey(final String key) {
        if (key == null) {
            return -1;
        }
        int n = this.bb.writePosition();
        if ((this.flags & 0x1) != 0x0) {
            final Integer n2 = this.keyPool.get(key);
            if (n2 == null) {
                final byte[] bytes = key.getBytes(StandardCharsets.UTF_8);
                this.bb.put(bytes, 0, bytes.length);
                this.bb.put((byte)0);
                this.keyPool.put(key, n);
            }
            else {
                n = n2;
            }
        }
        else {
            final byte[] bytes2 = key.getBytes(StandardCharsets.UTF_8);
            this.bb.put(bytes2, 0, bytes2.length);
            this.bb.put((byte)0);
            this.keyPool.put(key, n);
        }
        return n;
    }
    
    private void putUInt(final String s, final long n) {
        final int putKey = this.putKey(s);
        final int widthUInBits = widthUInBits(n);
        Value e;
        if (widthUInBits == 0) {
            e = Value.uInt8(putKey, (int)n);
        }
        else if (widthUInBits == 1) {
            e = Value.uInt16(putKey, (int)n);
        }
        else if (widthUInBits == 2) {
            e = Value.uInt32(putKey, (int)n);
        }
        else {
            e = Value.uInt64(putKey, n);
        }
        this.stack.add(e);
    }
    
    private void putUInt64(final String s, final long n) {
        this.stack.add(Value.uInt64(this.putKey(s), n));
    }
    
    static int widthUInBits(final long n) {
        if (n <= FlexBuffers.Unsigned.byteToUnsignedInt((byte)(-1))) {
            return 0;
        }
        if (n <= FlexBuffers.Unsigned.shortToUnsignedInt((short)(-1))) {
            return 1;
        }
        if (n <= FlexBuffers.Unsigned.intToUnsignedLong(-1)) {
            return 2;
        }
        return 3;
    }
    
    private void writeAny(final Value value, final int n) {
        final int type = value.type;
        if (type != 0 && type != 1 && type != 2) {
            if (type == 3) {
                this.writeDouble(value.dValue, n);
                return;
            }
            if (type != 26) {
                this.writeOffset(value.iValue, n);
                return;
            }
        }
        this.writeInt(value.iValue, n);
    }
    
    private Value writeBlob(final int n, final byte[] array, final int n2, final boolean b) {
        final int widthUInBits = widthUInBits(array.length);
        this.writeInt(array.length, this.align(widthUInBits));
        final int writePosition = this.bb.writePosition();
        this.bb.put(array, 0, array.length);
        if (b) {
            this.bb.put((byte)0);
        }
        return Value.blob(n, writePosition, n2, widthUInBits);
    }
    
    private void writeDouble(final double n, final int n2) {
        if (n2 == 4) {
            this.bb.putFloat((float)n);
        }
        else if (n2 == 8) {
            this.bb.putDouble(n);
        }
    }
    
    private void writeInt(final long n, final int n2) {
        if (n2 != 1) {
            if (n2 != 2) {
                if (n2 != 4) {
                    if (n2 == 8) {
                        this.bb.putLong(n);
                    }
                }
                else {
                    this.bb.putInt((int)n);
                }
            }
            else {
                this.bb.putShort((short)n);
            }
        }
        else {
            this.bb.put((byte)n);
        }
    }
    
    private void writeOffset(final long n, final int n2) {
        this.writeInt((int)(this.bb.writePosition() - n), n2);
    }
    
    private Value writeString(final int n, final String s) {
        return this.writeBlob(n, s.getBytes(StandardCharsets.UTF_8), 5, true);
    }
    
    public int endMap(final String s, final int fromIndex) {
        final int putKey = this.putKey(s);
        final ArrayList<Value> stack = this.stack;
        Collections.sort(stack.subList(fromIndex, stack.size()), this.keyComparator);
        final Value vector = this.createVector(putKey, fromIndex, this.stack.size() - fromIndex, false, false, this.createKeyVector(fromIndex, this.stack.size() - fromIndex));
        while (this.stack.size() > fromIndex) {
            final ArrayList<Value> stack2 = this.stack;
            stack2.remove(stack2.size() - 1);
        }
        this.stack.add(vector);
        return (int)vector.iValue;
    }
    
    public int endVector(final String s, final int n, final boolean b, final boolean b2) {
        final Value vector = this.createVector(this.putKey(s), n, this.stack.size() - n, b, b2, null);
        while (this.stack.size() > n) {
            final ArrayList<Value> stack = this.stack;
            stack.remove(stack.size() - 1);
        }
        this.stack.add(vector);
        return (int)vector.iValue;
    }
    
    public ByteBuffer finish() {
        final int align = this.align(this.stack.get(0).elemWidth(this.bb.writePosition(), 0));
        this.writeAny(this.stack.get(0), align);
        this.bb.put(this.stack.get(0).storedPackedType());
        this.bb.put((byte)align);
        this.finished = true;
        return ByteBuffer.wrap(this.bb.data(), 0, this.bb.writePosition());
    }
    
    public ReadWriteBuf getBuffer() {
        return this.bb;
    }
    
    public int putBlob(final String s, final byte[] array) {
        final Value writeBlob = this.writeBlob(this.putKey(s), array, 25, false);
        this.stack.add(writeBlob);
        return (int)writeBlob.iValue;
    }
    
    public int putBlob(final byte[] array) {
        return this.putBlob(null, array);
    }
    
    public void putBoolean(final String s, final boolean b) {
        this.stack.add(Value.bool(this.putKey(s), b));
    }
    
    public void putBoolean(final boolean b) {
        this.putBoolean(null, b);
    }
    
    public void putFloat(final double n) {
        this.putFloat(null, n);
    }
    
    public void putFloat(final float n) {
        this.putFloat(null, n);
    }
    
    public void putFloat(final String s, final double n) {
        this.stack.add(Value.float64(this.putKey(s), n));
    }
    
    public void putFloat(final String s, final float n) {
        this.stack.add(Value.float32(this.putKey(s), n));
    }
    
    public void putInt(final int n) {
        this.putInt(null, n);
    }
    
    public void putInt(final long n) {
        this.putInt(null, n);
    }
    
    public void putInt(final String s, final int n) {
        this.putInt(s, (long)n);
    }
    
    public void putInt(final String s, final long n) {
        final int putKey = this.putKey(s);
        if (-128L <= n && n <= 127L) {
            this.stack.add(Value.int8(putKey, (int)n));
        }
        else if (-32768L <= n && n <= 32767L) {
            this.stack.add(Value.int16(putKey, (int)n));
        }
        else if (-2147483648L <= n && n <= 2147483647L) {
            this.stack.add(Value.int32(putKey, (int)n));
        }
        else {
            this.stack.add(Value.int64(putKey, n));
        }
    }
    
    public int putString(final String s) {
        return this.putString(null, s);
    }
    
    public int putString(final String s, final String s2) {
        final int putKey = this.putKey(s);
        long n2;
        if ((this.flags & 0x2) != 0x0) {
            final Integer n = this.stringPool.get(s2);
            if (n != null) {
                this.stack.add(Value.blob(putKey, n, 5, widthUInBits(s2.length())));
                return n;
            }
            final Value writeString = this.writeString(putKey, s2);
            this.stringPool.put(s2, (int)writeString.iValue);
            this.stack.add(writeString);
            n2 = writeString.iValue;
        }
        else {
            final Value writeString2 = this.writeString(putKey, s2);
            this.stack.add(writeString2);
            n2 = writeString2.iValue;
        }
        return (int)n2;
    }
    
    public void putUInt(final int n) {
        this.putUInt(null, n);
    }
    
    public void putUInt(final long n) {
        this.putUInt(null, n);
    }
    
    public void putUInt64(final BigInteger bigInteger) {
        this.putUInt64(null, bigInteger.longValue());
    }
    
    public int startMap() {
        return this.stack.size();
    }
    
    public int startVector() {
        return this.stack.size();
    }
    
    private static class Value
    {
        static final boolean $assertionsDisabled = false;
        final double dValue;
        long iValue;
        int key;
        final int minBitWidth;
        final int type;
        
        Value(final int key, final int type, final int minBitWidth, final double dValue) {
            this.key = key;
            this.type = type;
            this.minBitWidth = minBitWidth;
            this.dValue = dValue;
            this.iValue = Long.MIN_VALUE;
        }
        
        Value(final int key, final int type, final int minBitWidth, final long iValue) {
            this.key = key;
            this.type = type;
            this.minBitWidth = minBitWidth;
            this.iValue = iValue;
            this.dValue = Double.MIN_VALUE;
        }
        
        static Value blob(final int n, final int n2, final int n3, final int n4) {
            return new Value(n, n3, n4, n2);
        }
        
        static Value bool(final int n, final boolean b) {
            long n2;
            if (b) {
                n2 = 1L;
            }
            else {
                n2 = 0L;
            }
            return new Value(n, 26, 0, n2);
        }
        
        private int elemWidth(final int n, final int n2) {
            return elemWidth(this.type, this.minBitWidth, this.iValue, n, n2);
        }
        
        private static int elemWidth(int i, int widthUInBits, final long n, final int n2, final int n3) {
            if (FlexBuffers.isTypeInline(i)) {
                return widthUInBits;
            }
            for (i = 1; i <= 32; i *= 2) {
                widthUInBits = FlexBuffersBuilder.widthUInBits((int)(paddingBytes(n2, i) + n2 + n3 * i - n));
                if (1L << widthUInBits == i) {
                    return widthUInBits;
                }
            }
            return 3;
        }
        
        static Value float32(final int n, final float n2) {
            return new Value(n, 3, 2, n2);
        }
        
        static Value float64(final int n, final double n2) {
            return new Value(n, 3, 3, n2);
        }
        
        static Value int16(final int n, final int n2) {
            return new Value(n, 1, 1, n2);
        }
        
        static Value int32(final int n, final int n2) {
            return new Value(n, 1, 2, n2);
        }
        
        static Value int64(final int n, final long n2) {
            return new Value(n, 1, 3, n2);
        }
        
        static Value int8(final int n, final int n2) {
            return new Value(n, 1, 0, n2);
        }
        
        private static byte packedType(final int n, final int n2) {
            return (byte)(n | n2 << 2);
        }
        
        private static int paddingBytes(final int n, final int n2) {
            return ~n + 1 & n2 - 1;
        }
        
        private byte storedPackedType() {
            return this.storedPackedType(0);
        }
        
        private byte storedPackedType(final int n) {
            return packedType(this.storedWidth(n), this.type);
        }
        
        private int storedWidth(final int b) {
            if (FlexBuffers.isTypeInline(this.type)) {
                return Math.max(this.minBitWidth, b);
            }
            return this.minBitWidth;
        }
        
        static Value uInt16(final int n, final int n2) {
            return new Value(n, 2, 1, n2);
        }
        
        static Value uInt32(final int n, final int n2) {
            return new Value(n, 2, 2, n2);
        }
        
        static Value uInt64(final int n, final long n2) {
            return new Value(n, 2, 3, n2);
        }
        
        static Value uInt8(final int n, final int n2) {
            return new Value(n, 2, 0, n2);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

import java.nio.ByteBuffer;

public class BaseVector
{
    protected ByteBuffer bb;
    private int element_size;
    private int length;
    private int vector;
    
    protected int __element(final int n) {
        return this.vector + n * this.element_size;
    }
    
    protected void __reset(final int vector, final int element_size, final ByteBuffer bb) {
        this.bb = bb;
        if (bb != null) {
            this.vector = vector;
            this.length = bb.getInt(vector - 4);
            this.element_size = element_size;
        }
        else {
            this.vector = 0;
            this.length = 0;
            this.element_size = 0;
        }
    }
    
    protected int __vector() {
        return this.vector;
    }
    
    public int length() {
        return this.length;
    }
    
    public void reset() {
        this.__reset(0, 0, null);
    }
}

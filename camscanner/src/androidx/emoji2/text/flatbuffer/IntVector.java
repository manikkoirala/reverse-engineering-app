// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

import java.nio.ByteBuffer;

public final class IntVector extends BaseVector
{
    public IntVector __assign(final int n, final ByteBuffer byteBuffer) {
        this.__reset(n, 4, byteBuffer);
        return this;
    }
    
    public int get(final int n) {
        return super.bb.getInt(this.__element(n));
    }
    
    public long getAsUnsigned(final int n) {
        return (long)this.get(n) & 0xFFFFFFFFL;
    }
}

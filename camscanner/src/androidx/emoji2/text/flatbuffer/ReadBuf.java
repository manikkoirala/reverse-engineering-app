// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

interface ReadBuf
{
    byte[] data();
    
    byte get(final int p0);
    
    boolean getBoolean(final int p0);
    
    double getDouble(final int p0);
    
    float getFloat(final int p0);
    
    int getInt(final int p0);
    
    long getLong(final int p0);
    
    short getShort(final int p0);
    
    String getString(final int p0, final int p1);
    
    int limit();
}

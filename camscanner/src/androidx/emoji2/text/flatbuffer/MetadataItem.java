// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text.flatbuffer;

import java.nio.ByteOrder;
import java.nio.ByteBuffer;

public final class MetadataItem extends Table
{
    public static void ValidateVersion() {
        Constants.FLATBUFFERS_1_12_0();
    }
    
    public static void addCodepoints(final FlatBufferBuilder flatBufferBuilder, final int n) {
        flatBufferBuilder.addOffset(6, n, 0);
    }
    
    public static void addCompatAdded(final FlatBufferBuilder flatBufferBuilder, final short n) {
        flatBufferBuilder.addShort(3, n, 0);
    }
    
    public static void addEmojiStyle(final FlatBufferBuilder flatBufferBuilder, final boolean b) {
        flatBufferBuilder.addBoolean(1, b, false);
    }
    
    public static void addHeight(final FlatBufferBuilder flatBufferBuilder, final short n) {
        flatBufferBuilder.addShort(5, n, 0);
    }
    
    public static void addId(final FlatBufferBuilder flatBufferBuilder, final int n) {
        flatBufferBuilder.addInt(0, n, 0);
    }
    
    public static void addSdkAdded(final FlatBufferBuilder flatBufferBuilder, final short n) {
        flatBufferBuilder.addShort(2, n, 0);
    }
    
    public static void addWidth(final FlatBufferBuilder flatBufferBuilder, final short n) {
        flatBufferBuilder.addShort(4, n, 0);
    }
    
    public static int createCodepointsVector(final FlatBufferBuilder flatBufferBuilder, final int[] array) {
        flatBufferBuilder.startVector(4, array.length, 4);
        for (int i = array.length - 1; i >= 0; --i) {
            flatBufferBuilder.addInt(array[i]);
        }
        return flatBufferBuilder.endVector();
    }
    
    public static int createMetadataItem(final FlatBufferBuilder flatBufferBuilder, final int n, final boolean b, final short n2, final short n3, final short n4, final short n5, final int n6) {
        flatBufferBuilder.startTable(7);
        addCodepoints(flatBufferBuilder, n6);
        addId(flatBufferBuilder, n);
        addHeight(flatBufferBuilder, n5);
        addWidth(flatBufferBuilder, n4);
        addCompatAdded(flatBufferBuilder, n3);
        addSdkAdded(flatBufferBuilder, n2);
        addEmojiStyle(flatBufferBuilder, b);
        return endMetadataItem(flatBufferBuilder);
    }
    
    public static int endMetadataItem(final FlatBufferBuilder flatBufferBuilder) {
        return flatBufferBuilder.endTable();
    }
    
    public static MetadataItem getRootAsMetadataItem(final ByteBuffer byteBuffer) {
        return getRootAsMetadataItem(byteBuffer, new MetadataItem());
    }
    
    public static MetadataItem getRootAsMetadataItem(final ByteBuffer byteBuffer, final MetadataItem metadataItem) {
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        return metadataItem.__assign(byteBuffer.getInt(byteBuffer.position()) + byteBuffer.position(), byteBuffer);
    }
    
    public static void startCodepointsVector(final FlatBufferBuilder flatBufferBuilder, final int n) {
        flatBufferBuilder.startVector(4, n, 4);
    }
    
    public static void startMetadataItem(final FlatBufferBuilder flatBufferBuilder) {
        flatBufferBuilder.startTable(7);
    }
    
    public MetadataItem __assign(final int n, final ByteBuffer byteBuffer) {
        this.__init(n, byteBuffer);
        return this;
    }
    
    public void __init(final int n, final ByteBuffer byteBuffer) {
        this.__reset(n, byteBuffer);
    }
    
    public int codepoints(int int1) {
        final int _offset = this.__offset(16);
        if (_offset != 0) {
            int1 = super.bb.getInt(this.__vector(_offset) + int1 * 4);
        }
        else {
            int1 = 0;
        }
        return int1;
    }
    
    public ByteBuffer codepointsAsByteBuffer() {
        return this.__vector_as_bytebuffer(16, 4);
    }
    
    public ByteBuffer codepointsInByteBuffer(final ByteBuffer byteBuffer) {
        return this.__vector_in_bytebuffer(byteBuffer, 16, 4);
    }
    
    public int codepointsLength() {
        final int _offset = this.__offset(16);
        int _vector_len;
        if (_offset != 0) {
            _vector_len = this.__vector_len(_offset);
        }
        else {
            _vector_len = 0;
        }
        return _vector_len;
    }
    
    public IntVector codepointsVector() {
        return this.codepointsVector(new IntVector());
    }
    
    public IntVector codepointsVector(IntVector _assign) {
        final int _offset = this.__offset(16);
        if (_offset != 0) {
            _assign = _assign.__assign(this.__vector(_offset), super.bb);
        }
        else {
            _assign = null;
        }
        return _assign;
    }
    
    public short compatAdded() {
        final int _offset = this.__offset(10);
        short short1;
        if (_offset != 0) {
            short1 = super.bb.getShort(_offset + super.bb_pos);
        }
        else {
            short1 = 0;
        }
        return short1;
    }
    
    public boolean emojiStyle() {
        final int _offset = this.__offset(6);
        boolean b = false;
        if (_offset != 0) {
            b = b;
            if (super.bb.get(_offset + super.bb_pos) != 0) {
                b = true;
            }
        }
        return b;
    }
    
    public short height() {
        final int _offset = this.__offset(14);
        short short1;
        if (_offset != 0) {
            short1 = super.bb.getShort(_offset + super.bb_pos);
        }
        else {
            short1 = 0;
        }
        return short1;
    }
    
    public int id() {
        final int _offset = this.__offset(4);
        int int1;
        if (_offset != 0) {
            int1 = super.bb.getInt(_offset + super.bb_pos);
        }
        else {
            int1 = 0;
        }
        return int1;
    }
    
    public short sdkAdded() {
        final int _offset = this.__offset(8);
        short short1;
        if (_offset != 0) {
            short1 = super.bb.getShort(_offset + super.bb_pos);
        }
        else {
            short1 = 0;
        }
        return short1;
    }
    
    public short width() {
        final int _offset = this.__offset(12);
        short short1;
        if (_offset != 0) {
            short1 = super.bb.getShort(_offset + super.bb_pos);
        }
        else {
            short1 = 0;
        }
        return short1;
    }
    
    public static final class Vector extends BaseVector
    {
        public Vector __assign(final int n, final int n2, final ByteBuffer byteBuffer) {
            this.__reset(n, n2, byteBuffer);
            return this;
        }
        
        public MetadataItem get(final int n) {
            return this.get(new MetadataItem(), n);
        }
        
        public MetadataItem get(final MetadataItem metadataItem, final int n) {
            return metadataItem.__assign(Table.__indirect(this.__element(n), super.bb), super.bb);
        }
    }
}

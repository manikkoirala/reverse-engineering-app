// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import android.graphics.Paint;
import androidx.core.graphics.PaintCompat;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.text.TextPaint;
import androidx.annotation.RestrictTo;
import androidx.annotation.AnyThread;

@AnyThread
@RestrictTo({ RestrictTo.Scope.LIBRARY })
class DefaultGlyphChecker implements GlyphChecker
{
    private static final int PAINT_TEXT_SIZE = 10;
    private static final ThreadLocal<StringBuilder> sStringBuilder;
    private final TextPaint mTextPaint;
    
    static {
        sStringBuilder = new ThreadLocal<StringBuilder>();
    }
    
    DefaultGlyphChecker() {
        ((Paint)(this.mTextPaint = new TextPaint())).setTextSize(10.0f);
    }
    
    private static StringBuilder getStringBuilder() {
        final ThreadLocal<StringBuilder> sStringBuilder = DefaultGlyphChecker.sStringBuilder;
        if (sStringBuilder.get() == null) {
            sStringBuilder.set(new StringBuilder());
        }
        return sStringBuilder.get();
    }
    
    @Override
    public boolean hasGlyph(@NonNull final CharSequence charSequence, int i, final int n, final int n2) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT < 23 && n2 > sdk_INT) {
            return false;
        }
        final StringBuilder stringBuilder = getStringBuilder();
        stringBuilder.setLength(0);
        while (i < n) {
            stringBuilder.append(charSequence.charAt(i));
            ++i;
        }
        return PaintCompat.hasGlyph((Paint)this.mTextPaint, stringBuilder.toString());
    }
}

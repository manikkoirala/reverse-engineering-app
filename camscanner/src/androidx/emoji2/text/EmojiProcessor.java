// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import java.util.Arrays;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.MetaKeyKeyListener;
import androidx.annotation.IntRange;
import android.view.inputmethod.InputConnection;
import android.text.Selection;
import android.view.KeyEvent;
import android.text.Editable;
import android.text.Spannable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;
import androidx.annotation.AnyThread;

@AnyThread
@RequiresApi(19)
@RestrictTo({ RestrictTo.Scope.LIBRARY })
final class EmojiProcessor
{
    private static final int ACTION_ADVANCE_BOTH = 1;
    private static final int ACTION_ADVANCE_END = 2;
    private static final int ACTION_FLUSH = 3;
    @Nullable
    private final int[] mEmojiAsDefaultStyleExceptions;
    @NonNull
    private EmojiCompat.GlyphChecker mGlyphChecker;
    @NonNull
    private final MetadataRepo mMetadataRepo;
    @NonNull
    private final EmojiCompat.SpanFactory mSpanFactory;
    private final boolean mUseEmojiAsDefaultStyle;
    
    EmojiProcessor(@NonNull final MetadataRepo mMetadataRepo, @NonNull final EmojiCompat.SpanFactory mSpanFactory, @NonNull final EmojiCompat.GlyphChecker mGlyphChecker, final boolean mUseEmojiAsDefaultStyle, @Nullable final int[] mEmojiAsDefaultStyleExceptions) {
        this.mSpanFactory = mSpanFactory;
        this.mMetadataRepo = mMetadataRepo;
        this.mGlyphChecker = mGlyphChecker;
        this.mUseEmojiAsDefaultStyle = mUseEmojiAsDefaultStyle;
        this.mEmojiAsDefaultStyleExceptions = mEmojiAsDefaultStyleExceptions;
    }
    
    private void addEmoji(@NonNull final Spannable spannable, final EmojiMetadata emojiMetadata, final int n, final int n2) {
        spannable.setSpan((Object)this.mSpanFactory.createSpan(emojiMetadata), n, n2, 33);
    }
    
    private static boolean delete(@NonNull final Editable editable, @NonNull final KeyEvent keyEvent, final boolean b) {
        if (hasModifiers(keyEvent)) {
            return false;
        }
        final int selectionStart = Selection.getSelectionStart((CharSequence)editable);
        final int selectionEnd = Selection.getSelectionEnd((CharSequence)editable);
        if (hasInvalidSelection(selectionStart, selectionEnd)) {
            return false;
        }
        final EmojiSpan[] array = (EmojiSpan[])((Spanned)editable).getSpans(selectionStart, selectionEnd, (Class)EmojiSpan.class);
        if (array != null && array.length > 0) {
            for (final EmojiSpan emojiSpan : array) {
                final int spanStart = ((Spanned)editable).getSpanStart((Object)emojiSpan);
                final int spanEnd = ((Spanned)editable).getSpanEnd((Object)emojiSpan);
                if ((b && spanStart == selectionStart) || (!b && spanEnd == selectionStart) || (selectionStart > spanStart && selectionStart < spanEnd)) {
                    editable.delete(spanStart, spanEnd);
                    return true;
                }
            }
        }
        return false;
    }
    
    static boolean handleDeleteSurroundingText(@NonNull final InputConnection inputConnection, @NonNull final Editable editable, @IntRange(from = 0L) int a, @IntRange(from = 0L) int a2, final boolean b) {
        if (editable != null) {
            if (inputConnection != null) {
                if (a >= 0) {
                    if (a2 >= 0) {
                        final int selectionStart = Selection.getSelectionStart((CharSequence)editable);
                        final int selectionEnd = Selection.getSelectionEnd((CharSequence)editable);
                        if (hasInvalidSelection(selectionStart, selectionEnd)) {
                            return false;
                        }
                        if (b) {
                            a = CodepointIndexFinder.findIndexBackward((CharSequence)editable, selectionStart, Math.max(a, 0));
                            final int indexForward = CodepointIndexFinder.findIndexForward((CharSequence)editable, selectionEnd, Math.max(a2, 0));
                            if (a == -1 || (a2 = indexForward) == -1) {
                                return false;
                            }
                        }
                        else {
                            a = Math.max(selectionStart - a, 0);
                            a2 = Math.min(selectionEnd + a2, ((CharSequence)editable).length());
                        }
                        final EmojiSpan[] array = (EmojiSpan[])((Spanned)editable).getSpans(a, a2, (Class)EmojiSpan.class);
                        if (array != null && array.length > 0) {
                            for (final EmojiSpan emojiSpan : array) {
                                final int spanStart = ((Spanned)editable).getSpanStart((Object)emojiSpan);
                                final int spanEnd = ((Spanned)editable).getSpanEnd((Object)emojiSpan);
                                a = Math.min(spanStart, a);
                                a2 = Math.max(spanEnd, a2);
                            }
                            a = Math.max(a, 0);
                            a2 = Math.min(a2, ((CharSequence)editable).length());
                            inputConnection.beginBatchEdit();
                            editable.delete(a, a2);
                            inputConnection.endBatchEdit();
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
    
    static boolean handleOnKeyDown(@NonNull final Editable editable, final int n, @NonNull final KeyEvent keyEvent) {
        boolean delete;
        if (n != 67) {
            delete = (n == 112 && delete(editable, keyEvent, true));
        }
        else {
            delete = delete(editable, keyEvent, false);
        }
        if (delete) {
            MetaKeyKeyListener.adjustMetaAfterKeypress((Spannable)editable);
            return true;
        }
        return false;
    }
    
    private boolean hasGlyph(final CharSequence charSequence, final int n, final int n2, final EmojiMetadata emojiMetadata) {
        if (emojiMetadata.getHasGlyph() == 0) {
            emojiMetadata.setHasGlyph(this.mGlyphChecker.hasGlyph(charSequence, n, n2, emojiMetadata.getSdkAdded()));
        }
        return emojiMetadata.getHasGlyph() == 2;
    }
    
    private static boolean hasInvalidSelection(final int n, final int n2) {
        return n == -1 || n2 == -1 || n != n2;
    }
    
    private static boolean hasModifiers(@NonNull final KeyEvent keyEvent) {
        return KeyEvent.metaStateHasNoModifiers(keyEvent.getMetaState()) ^ true;
    }
    
    int getEmojiMatch(@NonNull final CharSequence charSequence) {
        return this.getEmojiMatch(charSequence, this.mMetadataRepo.getMetadataVersion());
    }
    
    int getEmojiMatch(@NonNull final CharSequence seq, final int n) {
        final ProcessorSm processorSm = new ProcessorSm(this.mMetadataRepo.getRootNode(), this.mUseEmojiAsDefaultStyle, this.mEmojiAsDefaultStyleExceptions);
        final int length = seq.length();
        int i = 0;
        int n2 = 0;
        int n3 = 0;
        while (i < length) {
            final int codePoint = Character.codePointAt(seq, i);
            final int check = processorSm.check(codePoint);
            EmojiMetadata currentMetadata = processorSm.getCurrentMetadata();
            int n4;
            int n5;
            int n6;
            if (check != 1) {
                if (check != 2) {
                    if (check != 3) {
                        n4 = i;
                        n5 = n2;
                        n6 = n3;
                    }
                    else {
                        final EmojiMetadata flushMetadata = processorSm.getFlushMetadata();
                        n4 = i;
                        n5 = n2;
                        n6 = n3;
                        currentMetadata = flushMetadata;
                        if (flushMetadata.getCompatAdded() <= n) {
                            n5 = n2 + 1;
                            n4 = i;
                            n6 = n3;
                            currentMetadata = flushMetadata;
                        }
                    }
                }
                else {
                    n4 = i + Character.charCount(codePoint);
                    n5 = n2;
                    n6 = n3;
                }
            }
            else {
                n4 = i + Character.charCount(codePoint);
                n6 = 0;
                n5 = n2;
            }
            i = n4;
            n2 = n5;
            n3 = n6;
            if (currentMetadata != null) {
                i = n4;
                n2 = n5;
                n3 = n6;
                if (currentMetadata.getCompatAdded() > n) {
                    continue;
                }
                n3 = n6 + 1;
                i = n4;
                n2 = n5;
            }
        }
        if (n2 != 0) {
            return 2;
        }
        if (processorSm.isInFlushableState() && processorSm.getCurrentMetadata().getCompatAdded() <= n) {
            return 1;
        }
        if (n3 == 0) {
            return 0;
        }
        return 2;
    }
    
    CharSequence process(@NonNull final CharSequence charSequence, @IntRange(from = 0L) int n, @IntRange(from = 0L) int max, @IntRange(from = 0L) int n2, final boolean b) {
        final boolean b2 = charSequence instanceof SpannableBuilder;
        if (b2) {
            ((SpannableBuilder)charSequence).beginBatchEdit();
        }
        Label_0080: {
            if (b2) {
                break Label_0080;
            }
            try {
                Object o;
                if (charSequence instanceof Spannable) {
                    o = new UnprecomputeTextOnModificationSpannable((Spannable)charSequence);
                }
                else if (charSequence instanceof Spanned && ((Spanned)charSequence).nextSpanTransition(n - 1, max + 1, (Class)EmojiSpan.class) <= max) {
                    o = new UnprecomputeTextOnModificationSpannable(charSequence);
                }
                else {
                    o = null;
                }
                int index = n;
                int n3 = max;
                if (o != null) {
                    final EmojiSpan[] array = ((UnprecomputeTextOnModificationSpannable)o).getSpans(n, max, EmojiSpan.class);
                    index = n;
                    n3 = max;
                    if (array != null) {
                        index = n;
                        n3 = max;
                        if (array.length > 0) {
                            final int length = array.length;
                            int n4 = 0;
                            while (true) {
                                index = n;
                                n3 = max;
                                if (n4 >= length) {
                                    break;
                                }
                                final EmojiSpan emojiSpan = array[n4];
                                final int spanStart = ((UnprecomputeTextOnModificationSpannable)o).getSpanStart(emojiSpan);
                                final int spanEnd = ((UnprecomputeTextOnModificationSpannable)o).getSpanEnd(emojiSpan);
                                if (spanStart != max) {
                                    ((UnprecomputeTextOnModificationSpannable)o).removeSpan(emojiSpan);
                                }
                                n = Math.min(spanStart, n);
                                max = Math.max(spanEnd, max);
                                ++n4;
                            }
                        }
                    }
                }
                if (index == n3 || index >= charSequence.length()) {
                    return charSequence;
                }
                int n5;
                if ((n5 = n2) != Integer.MAX_VALUE) {
                    n5 = n2;
                    if (o != null) {
                        n5 = n2 - ((UnprecomputeTextOnModificationSpannable)o).getSpans(0, ((UnprecomputeTextOnModificationSpannable)o).length(), EmojiSpan.class).length;
                    }
                }
                final ProcessorSm processorSm = new ProcessorSm(this.mMetadataRepo.getRootNode(), this.mUseEmojiAsDefaultStyle, this.mEmojiAsDefaultStyleExceptions);
                int codePoint = Character.codePointAt(charSequence, index);
                n2 = 0;
                int n6 = 0;
            Label_0320:
                while (true) {
                    n6 = index;
                    max = index;
                    n = codePoint;
                    while (max < n3 && n2 < n5) {
                        final int check = processorSm.check(n);
                        if (check != 1) {
                            if (check != 2) {
                                if (check != 3) {
                                    continue;
                                }
                                if (!b) {
                                    codePoint = n;
                                    index = max;
                                    if (this.hasGlyph(charSequence, n6, max, processorSm.getFlushMetadata())) {
                                        continue Label_0320;
                                    }
                                }
                                UnprecomputeTextOnModificationSpannable unprecomputeTextOnModificationSpannable;
                                if ((unprecomputeTextOnModificationSpannable = (UnprecomputeTextOnModificationSpannable)o) == null) {
                                    unprecomputeTextOnModificationSpannable = new UnprecomputeTextOnModificationSpannable((Spannable)new SpannableString(charSequence));
                                }
                                this.addEmoji((Spannable)unprecomputeTextOnModificationSpannable, processorSm.getFlushMetadata(), n6, max);
                                ++n2;
                                o = unprecomputeTextOnModificationSpannable;
                                codePoint = n;
                                index = max;
                                continue Label_0320;
                            }
                            else {
                                final int index2 = max + Character.charCount(n);
                                if ((max = index2) >= n3) {
                                    continue;
                                }
                                n = Character.codePointAt(charSequence, index2);
                                max = index2;
                            }
                        }
                        else {
                            n6 += Character.charCount(Character.codePointAt(charSequence, n6));
                            if (n6 < n3) {
                                n = Character.codePointAt(charSequence, n6);
                            }
                            max = n6;
                        }
                    }
                    break;
                }
                UnprecomputeTextOnModificationSpannable unprecomputeTextOnModificationSpannable2 = (UnprecomputeTextOnModificationSpannable)o;
                Label_0608: {
                    if (processorSm.isInFlushableState()) {
                        unprecomputeTextOnModificationSpannable2 = (UnprecomputeTextOnModificationSpannable)o;
                        if (n2 < n5) {
                            if (!b) {
                                unprecomputeTextOnModificationSpannable2 = (UnprecomputeTextOnModificationSpannable)o;
                                if (this.hasGlyph(charSequence, n6, max, processorSm.getCurrentMetadata())) {
                                    break Label_0608;
                                }
                            }
                            if ((unprecomputeTextOnModificationSpannable2 = (UnprecomputeTextOnModificationSpannable)o) == null) {
                                unprecomputeTextOnModificationSpannable2 = new UnprecomputeTextOnModificationSpannable(charSequence);
                            }
                            this.addEmoji((Spannable)unprecomputeTextOnModificationSpannable2, processorSm.getCurrentMetadata(), n6, max);
                        }
                    }
                }
                if (unprecomputeTextOnModificationSpannable2 != null) {
                    return (CharSequence)unprecomputeTextOnModificationSpannable2.getUnwrappedSpannable();
                }
                return charSequence;
            }
            finally {
                if (b2) {
                    ((SpannableBuilder)charSequence).endBatchEdit();
                }
            }
        }
    }
    
    @RequiresApi(19)
    private static final class CodepointIndexFinder
    {
        private static final int INVALID_INDEX = -1;
        
        static int findIndexBackward(final CharSequence charSequence, int n, int i) {
            final int length = charSequence.length();
            if (n < 0 || length < n) {
                return -1;
            }
            if (i < 0) {
                return -1;
            }
        Label_0027:
            while (true) {
                int n2 = 0;
                while (i != 0) {
                    if (--n < 0) {
                        if (n2 != 0) {
                            return -1;
                        }
                        return 0;
                    }
                    else {
                        final char char1 = charSequence.charAt(n);
                        if (n2 != 0) {
                            if (!Character.isHighSurrogate(char1)) {
                                return -1;
                            }
                            --i;
                            continue Label_0027;
                        }
                        else if (!Character.isSurrogate(char1)) {
                            --i;
                        }
                        else {
                            if (Character.isHighSurrogate(char1)) {
                                return -1;
                            }
                            n2 = 1;
                        }
                    }
                }
                return n;
            }
        }
        
        static int findIndexForward(final CharSequence charSequence, int n, int i) {
            final int length = charSequence.length();
            if (n < 0 || length < n) {
                return -1;
            }
            if (i < 0) {
                return -1;
            }
        Label_0027:
            while (true) {
                int n2 = 0;
                while (i != 0) {
                    if (n >= length) {
                        if (n2 != 0) {
                            return -1;
                        }
                        return length;
                    }
                    else {
                        final char char1 = charSequence.charAt(n);
                        if (n2 != 0) {
                            if (!Character.isLowSurrogate(char1)) {
                                return -1;
                            }
                            --i;
                            ++n;
                            continue Label_0027;
                        }
                        else if (!Character.isSurrogate(char1)) {
                            --i;
                            ++n;
                        }
                        else {
                            if (Character.isLowSurrogate(char1)) {
                                return -1;
                            }
                            ++n;
                            n2 = 1;
                        }
                    }
                }
                return n;
            }
        }
    }
    
    static final class ProcessorSm
    {
        private static final int STATE_DEFAULT = 1;
        private static final int STATE_WALKING = 2;
        private int mCurrentDepth;
        private MetadataRepo.Node mCurrentNode;
        private final int[] mEmojiAsDefaultStyleExceptions;
        private MetadataRepo.Node mFlushNode;
        private int mLastCodepoint;
        private final MetadataRepo.Node mRootNode;
        private int mState;
        private final boolean mUseEmojiAsDefaultStyle;
        
        ProcessorSm(final MetadataRepo.Node node, final boolean mUseEmojiAsDefaultStyle, final int[] mEmojiAsDefaultStyleExceptions) {
            this.mState = 1;
            this.mRootNode = node;
            this.mCurrentNode = node;
            this.mUseEmojiAsDefaultStyle = mUseEmojiAsDefaultStyle;
            this.mEmojiAsDefaultStyleExceptions = mEmojiAsDefaultStyleExceptions;
        }
        
        private static boolean isEmojiStyle(final int n) {
            return n == 65039;
        }
        
        private static boolean isTextStyle(final int n) {
            return n == 65038;
        }
        
        private int reset() {
            this.mState = 1;
            this.mCurrentNode = this.mRootNode;
            this.mCurrentDepth = 0;
            return 1;
        }
        
        private boolean shouldUseEmojiPresentationStyleForSingleCodepoint() {
            if (this.mCurrentNode.getData().isDefaultEmoji()) {
                return true;
            }
            if (isEmojiStyle(this.mLastCodepoint)) {
                return true;
            }
            if (this.mUseEmojiAsDefaultStyle) {
                if (this.mEmojiAsDefaultStyleExceptions == null) {
                    return true;
                }
                if (Arrays.binarySearch(this.mEmojiAsDefaultStyleExceptions, this.mCurrentNode.getData().getCodepointAt(0)) < 0) {
                    return true;
                }
            }
            return false;
        }
        
        int check(final int mLastCodepoint) {
            final MetadataRepo.Node value = this.mCurrentNode.get(mLastCodepoint);
            final int mState = this.mState;
            int n = 2;
            if (mState != 2) {
                if (value == null) {
                    n = this.reset();
                }
                else {
                    this.mState = 2;
                    this.mCurrentNode = value;
                    this.mCurrentDepth = 1;
                }
            }
            else if (value != null) {
                this.mCurrentNode = value;
                ++this.mCurrentDepth;
            }
            else if (isTextStyle(mLastCodepoint)) {
                n = this.reset();
            }
            else if (!isEmojiStyle(mLastCodepoint)) {
                if (this.mCurrentNode.getData() != null) {
                    final int mCurrentDepth = this.mCurrentDepth;
                    n = 3;
                    if (mCurrentDepth == 1) {
                        if (this.shouldUseEmojiPresentationStyleForSingleCodepoint()) {
                            this.mFlushNode = this.mCurrentNode;
                            this.reset();
                        }
                        else {
                            n = this.reset();
                        }
                    }
                    else {
                        this.mFlushNode = this.mCurrentNode;
                        this.reset();
                    }
                }
                else {
                    n = this.reset();
                }
            }
            this.mLastCodepoint = mLastCodepoint;
            return n;
        }
        
        EmojiMetadata getCurrentMetadata() {
            return this.mCurrentNode.getData();
        }
        
        EmojiMetadata getFlushMetadata() {
            return this.mFlushNode.getData();
        }
        
        boolean isInFlushableState() {
            if (this.mState == 2 && this.mCurrentNode.getData() != null) {
                final int mCurrentDepth = this.mCurrentDepth;
                boolean b = true;
                if (mCurrentDepth > 1) {
                    return b;
                }
                if (this.shouldUseEmojiPresentationStyleForSingleCodepoint()) {
                    b = b;
                    return b;
                }
            }
            return false;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import androidx.annotation.Nullable;
import android.annotation.SuppressLint;
import android.graphics.Paint;
import androidx.annotation.RestrictTo;
import androidx.core.util.Preconditions;
import android.graphics.Paint$FontMetricsInt;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import android.text.style.ReplacementSpan;

@RequiresApi(19)
public abstract class EmojiSpan extends ReplacementSpan
{
    private short mHeight;
    @NonNull
    private final EmojiMetadata mMetadata;
    private float mRatio;
    private final Paint$FontMetricsInt mTmpFontMetrics;
    private short mWidth;
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    EmojiSpan(@NonNull final EmojiMetadata mMetadata) {
        this.mTmpFontMetrics = new Paint$FontMetricsInt();
        this.mWidth = -1;
        this.mHeight = -1;
        this.mRatio = 1.0f;
        Preconditions.checkNotNull(mMetadata, "metadata cannot be null");
        this.mMetadata = mMetadata;
    }
    
    @RestrictTo({ RestrictTo.Scope.TESTS })
    public final int getHeight() {
        return this.mHeight;
    }
    
    @RestrictTo({ RestrictTo.Scope.TESTS })
    public final int getId() {
        return this.getMetadata().getId();
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public final EmojiMetadata getMetadata() {
        return this.mMetadata;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    final float getRatio() {
        return this.mRatio;
    }
    
    public int getSize(@NonNull final Paint paint, @SuppressLint({ "UnknownNullness" }) final CharSequence charSequence, final int n, final int n2, @Nullable final Paint$FontMetricsInt paint$FontMetricsInt) {
        paint.getFontMetricsInt(this.mTmpFontMetrics);
        final Paint$FontMetricsInt mTmpFontMetrics = this.mTmpFontMetrics;
        this.mRatio = Math.abs(mTmpFontMetrics.descent - mTmpFontMetrics.ascent) * 1.0f / this.mMetadata.getHeight();
        this.mHeight = (short)(this.mMetadata.getHeight() * this.mRatio);
        final short mWidth = (short)(this.mMetadata.getWidth() * this.mRatio);
        this.mWidth = mWidth;
        if (paint$FontMetricsInt != null) {
            final Paint$FontMetricsInt mTmpFontMetrics2 = this.mTmpFontMetrics;
            paint$FontMetricsInt.ascent = mTmpFontMetrics2.ascent;
            paint$FontMetricsInt.descent = mTmpFontMetrics2.descent;
            paint$FontMetricsInt.top = mTmpFontMetrics2.top;
            paint$FontMetricsInt.bottom = mTmpFontMetrics2.bottom;
        }
        return mWidth;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    final int getWidth() {
        return this.mWidth;
    }
}

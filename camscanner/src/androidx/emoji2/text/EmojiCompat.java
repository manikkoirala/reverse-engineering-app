// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import android.os.BaseBundle;
import java.util.Iterator;
import java.util.Arrays;
import java.util.List;
import androidx.annotation.RequiresApi;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.os.Bundle;
import android.view.inputmethod.EditorInfo;
import androidx.annotation.CheckResult;
import java.util.ArrayList;
import androidx.annotation.ColorInt;
import android.content.Context;
import android.view.KeyEvent;
import androidx.annotation.IntRange;
import android.text.Editable;
import android.view.inputmethod.InputConnection;
import androidx.core.util.Preconditions;
import java.util.Collection;
import androidx.collection.ArraySet;
import android.os.Looper;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import android.os.Handler;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.Set;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.GuardedBy;
import androidx.annotation.RestrictTo;
import androidx.annotation.AnyThread;

@AnyThread
public class EmojiCompat
{
    private static final Object CONFIG_LOCK;
    public static final String EDITOR_INFO_METAVERSION_KEY = "android.support.text.emoji.emojiCompat_metadataVersion";
    public static final String EDITOR_INFO_REPLACE_ALL_KEY = "android.support.text.emoji.emojiCompat_replaceAll";
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    static final int EMOJI_COUNT_UNLIMITED = Integer.MAX_VALUE;
    public static final int EMOJI_FALLBACK = 2;
    public static final int EMOJI_SUPPORTED = 1;
    public static final int EMOJI_UNSUPPORTED = 0;
    private static final Object INSTANCE_LOCK;
    public static final int LOAD_STATE_DEFAULT = 3;
    public static final int LOAD_STATE_FAILED = 2;
    public static final int LOAD_STATE_LOADING = 0;
    public static final int LOAD_STATE_SUCCEEDED = 1;
    public static final int LOAD_STRATEGY_DEFAULT = 0;
    public static final int LOAD_STRATEGY_MANUAL = 1;
    private static final String NOT_INITIALIZED_ERROR_TEXT = "EmojiCompat is not initialized.\n\nYou must initialize EmojiCompat prior to referencing the EmojiCompat instance.\n\nThe most likely cause of this error is disabling the EmojiCompatInitializer\neither explicitly in AndroidManifest.xml, or by including\nandroidx.emoji2:emoji2-bundled.\n\nAutomatic initialization is typically performed by EmojiCompatInitializer. If\nyou are not expecting to initialize EmojiCompat manually in your application,\nplease check to ensure it has not been removed from your APK's manifest. You can\ndo this in Android Studio using Build > Analyze APK.\n\nIn the APK Analyzer, ensure that the startup entry for\nEmojiCompatInitializer and InitializationProvider is present in\n AndroidManifest.xml. If it is missing or contains tools:node=\"remove\", and you\nintend to use automatic configuration, verify:\n\n  1. Your application does not include emoji2-bundled\n  2. All modules do not contain an exclusion manifest rule for\n     EmojiCompatInitializer or InitializationProvider. For more information\n     about manifest exclusions see the documentation for the androidx startup\n     library.\n\nIf you intend to use emoji2-bundled, please call EmojiCompat.init. You can\nlearn more in the documentation for BundledEmojiCompatConfig.\n\nIf you intended to perform manual configuration, it is recommended that you call\nEmojiCompat.init immediately on application startup.\n\nIf you still cannot resolve this issue, please open a bug with your specific\nconfiguration to help improve error message.";
    public static final int REPLACE_STRATEGY_ALL = 1;
    public static final int REPLACE_STRATEGY_DEFAULT = 0;
    public static final int REPLACE_STRATEGY_NON_EXISTENT = 2;
    @GuardedBy("CONFIG_LOCK")
    private static volatile boolean sHasDoneDefaultConfigLookup;
    @GuardedBy("INSTANCE_LOCK")
    @Nullable
    private static volatile EmojiCompat sInstance;
    @Nullable
    final int[] mEmojiAsDefaultStyleExceptions;
    private final int mEmojiSpanIndicatorColor;
    private final boolean mEmojiSpanIndicatorEnabled;
    private final GlyphChecker mGlyphChecker;
    @NonNull
    private final CompatInternal mHelper;
    @GuardedBy("mInitLock")
    @NonNull
    private final Set<InitCallback> mInitCallbacks;
    @NonNull
    private final ReadWriteLock mInitLock;
    @GuardedBy("mInitLock")
    private volatile int mLoadState;
    @NonNull
    private final Handler mMainHandler;
    private final int mMetadataLoadStrategy;
    @NonNull
    final MetadataRepoLoader mMetadataLoader;
    final boolean mReplaceAll;
    final boolean mUseEmojiAsDefaultStyle;
    
    static {
        INSTANCE_LOCK = new Object();
        CONFIG_LOCK = new Object();
    }
    
    private EmojiCompat(@NonNull final Config config) {
        this.mInitLock = new ReentrantReadWriteLock();
        this.mLoadState = 3;
        this.mReplaceAll = config.mReplaceAll;
        this.mUseEmojiAsDefaultStyle = config.mUseEmojiAsDefaultStyle;
        this.mEmojiAsDefaultStyleExceptions = config.mEmojiAsDefaultStyleExceptions;
        this.mEmojiSpanIndicatorEnabled = config.mEmojiSpanIndicatorEnabled;
        this.mEmojiSpanIndicatorColor = config.mEmojiSpanIndicatorColor;
        this.mMetadataLoader = config.mMetadataLoader;
        this.mMetadataLoadStrategy = config.mMetadataLoadStrategy;
        this.mGlyphChecker = config.mGlyphChecker;
        this.mMainHandler = new Handler(Looper.getMainLooper());
        final ArraySet mInitCallbacks = new ArraySet();
        this.mInitCallbacks = mInitCallbacks;
        final Set<InitCallback> mInitCallbacks2 = config.mInitCallbacks;
        if (mInitCallbacks2 != null && !mInitCallbacks2.isEmpty()) {
            mInitCallbacks.addAll(config.mInitCallbacks);
        }
        this.mHelper = (CompatInternal)new CompatInternal19(this);
        this.loadMetadata();
    }
    
    @NonNull
    public static EmojiCompat get() {
        synchronized (EmojiCompat.INSTANCE_LOCK) {
            final EmojiCompat sInstance = EmojiCompat.sInstance;
            Preconditions.checkState(sInstance != null, "EmojiCompat is not initialized.\n\nYou must initialize EmojiCompat prior to referencing the EmojiCompat instance.\n\nThe most likely cause of this error is disabling the EmojiCompatInitializer\neither explicitly in AndroidManifest.xml, or by including\nandroidx.emoji2:emoji2-bundled.\n\nAutomatic initialization is typically performed by EmojiCompatInitializer. If\nyou are not expecting to initialize EmojiCompat manually in your application,\nplease check to ensure it has not been removed from your APK's manifest. You can\ndo this in Android Studio using Build > Analyze APK.\n\nIn the APK Analyzer, ensure that the startup entry for\nEmojiCompatInitializer and InitializationProvider is present in\n AndroidManifest.xml. If it is missing or contains tools:node=\"remove\", and you\nintend to use automatic configuration, verify:\n\n  1. Your application does not include emoji2-bundled\n  2. All modules do not contain an exclusion manifest rule for\n     EmojiCompatInitializer or InitializationProvider. For more information\n     about manifest exclusions see the documentation for the androidx startup\n     library.\n\nIf you intend to use emoji2-bundled, please call EmojiCompat.init. You can\nlearn more in the documentation for BundledEmojiCompatConfig.\n\nIf you intended to perform manual configuration, it is recommended that you call\nEmojiCompat.init immediately on application startup.\n\nIf you still cannot resolve this issue, please open a bug with your specific\nconfiguration to help improve error message.");
            return sInstance;
        }
    }
    
    public static boolean handleDeleteSurroundingText(@NonNull final InputConnection inputConnection, @NonNull final Editable editable, @IntRange(from = 0L) final int n, @IntRange(from = 0L) final int n2, final boolean b) {
        return EmojiProcessor.handleDeleteSurroundingText(inputConnection, editable, n, n2, b);
    }
    
    public static boolean handleOnKeyDown(@NonNull final Editable editable, final int n, @NonNull final KeyEvent keyEvent) {
        return EmojiProcessor.handleOnKeyDown(editable, n, keyEvent);
    }
    
    @Nullable
    public static EmojiCompat init(@NonNull final Context context) {
        return init(context, null);
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public static EmojiCompat init(@NonNull final Context context, @Nullable DefaultEmojiCompatConfig.DefaultEmojiCompatConfigFactory defaultEmojiCompatConfigFactory) {
        if (EmojiCompat.sHasDoneDefaultConfigLookup) {
            return EmojiCompat.sInstance;
        }
        if (defaultEmojiCompatConfigFactory == null) {
            defaultEmojiCompatConfigFactory = new DefaultEmojiCompatConfig.DefaultEmojiCompatConfigFactory(null);
        }
        final Config create = defaultEmojiCompatConfigFactory.create(context);
        synchronized (EmojiCompat.CONFIG_LOCK) {
            if (!EmojiCompat.sHasDoneDefaultConfigLookup) {
                if (create != null) {
                    init(create);
                }
                EmojiCompat.sHasDoneDefaultConfigLookup = true;
            }
            return EmojiCompat.sInstance;
        }
    }
    
    @NonNull
    public static EmojiCompat init(@NonNull final Config config) {
        EmojiCompat emojiCompat;
        if ((emojiCompat = EmojiCompat.sInstance) == null) {
            synchronized (EmojiCompat.INSTANCE_LOCK) {
                if ((emojiCompat = EmojiCompat.sInstance) == null) {
                    emojiCompat = (EmojiCompat.sInstance = new EmojiCompat(config));
                }
            }
        }
        return emojiCompat;
    }
    
    public static boolean isConfigured() {
        return EmojiCompat.sInstance != null;
    }
    
    private boolean isInitialized() {
        final int loadState = this.getLoadState();
        boolean b = true;
        if (loadState != 1) {
            b = false;
        }
        return b;
    }
    
    private void loadMetadata() {
        this.mInitLock.writeLock().lock();
        try {
            if (this.mMetadataLoadStrategy == 0) {
                this.mLoadState = 0;
            }
            this.mInitLock.writeLock().unlock();
            if (this.getLoadState() == 0) {
                this.mHelper.loadMetadata();
            }
        }
        finally {
            this.mInitLock.writeLock().unlock();
        }
    }
    
    @NonNull
    public static EmojiCompat reset(@NonNull final Config config) {
        synchronized (EmojiCompat.INSTANCE_LOCK) {
            return EmojiCompat.sInstance = new EmojiCompat(config);
        }
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.TESTS })
    public static EmojiCompat reset(@Nullable EmojiCompat sInstance) {
        synchronized (EmojiCompat.INSTANCE_LOCK) {
            EmojiCompat.sInstance = sInstance;
            sInstance = EmojiCompat.sInstance;
            return sInstance;
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.TESTS })
    public static void skipDefaultConfigurationLookup(final boolean sHasDoneDefaultConfigLookup) {
        synchronized (EmojiCompat.CONFIG_LOCK) {
            EmojiCompat.sHasDoneDefaultConfigLookup = sHasDoneDefaultConfigLookup;
        }
    }
    
    @NonNull
    public String getAssetSignature() {
        Preconditions.checkState(this.isInitialized(), "Not initialized yet");
        return this.mHelper.getAssetSignature();
    }
    
    public int getEmojiMatch(@NonNull final CharSequence charSequence, @IntRange(from = 0L) final int n) {
        Preconditions.checkState(this.isInitialized(), "Not initialized yet");
        Preconditions.checkNotNull(charSequence, "sequence cannot be null");
        return this.mHelper.getEmojiMatch(charSequence, n);
    }
    
    @ColorInt
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public int getEmojiSpanIndicatorColor() {
        return this.mEmojiSpanIndicatorColor;
    }
    
    public int getLoadState() {
        this.mInitLock.readLock().lock();
        try {
            return this.mLoadState;
        }
        finally {
            this.mInitLock.readLock().unlock();
        }
    }
    
    @Deprecated
    public boolean hasEmojiGlyph(@NonNull final CharSequence charSequence) {
        Preconditions.checkState(this.isInitialized(), "Not initialized yet");
        Preconditions.checkNotNull(charSequence, "sequence cannot be null");
        return this.mHelper.hasEmojiGlyph(charSequence);
    }
    
    @Deprecated
    public boolean hasEmojiGlyph(@NonNull final CharSequence charSequence, @IntRange(from = 0L) final int n) {
        Preconditions.checkState(this.isInitialized(), "Not initialized yet");
        Preconditions.checkNotNull(charSequence, "sequence cannot be null");
        return this.mHelper.hasEmojiGlyph(charSequence, n);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public boolean isEmojiSpanIndicatorEnabled() {
        return this.mEmojiSpanIndicatorEnabled;
    }
    
    public void load() {
        final int mMetadataLoadStrategy = this.mMetadataLoadStrategy;
        boolean b = true;
        if (mMetadataLoadStrategy != 1) {
            b = false;
        }
        Preconditions.checkState(b, "Set metadataLoadStrategy to LOAD_STRATEGY_MANUAL to execute manual loading");
        if (this.isInitialized()) {
            return;
        }
        this.mInitLock.writeLock().lock();
        try {
            if (this.mLoadState == 0) {
                return;
            }
            this.mLoadState = 0;
            this.mInitLock.writeLock().unlock();
            this.mHelper.loadMetadata();
        }
        finally {
            this.mInitLock.writeLock().unlock();
        }
    }
    
    void onMetadataLoadFailed(@Nullable final Throwable t) {
        final ArrayList list = new ArrayList();
        this.mInitLock.writeLock().lock();
        try {
            this.mLoadState = 2;
            list.addAll(this.mInitCallbacks);
            this.mInitCallbacks.clear();
            this.mInitLock.writeLock().unlock();
            this.mMainHandler.post((Runnable)new ListenerDispatcher(list, this.mLoadState, t));
        }
        finally {
            this.mInitLock.writeLock().unlock();
        }
    }
    
    void onMetadataLoadSuccess() {
        final ArrayList list = new ArrayList();
        this.mInitLock.writeLock().lock();
        try {
            this.mLoadState = 1;
            list.addAll(this.mInitCallbacks);
            this.mInitCallbacks.clear();
            this.mInitLock.writeLock().unlock();
            this.mMainHandler.post((Runnable)new ListenerDispatcher(list, this.mLoadState));
        }
        finally {
            this.mInitLock.writeLock().unlock();
        }
    }
    
    @CheckResult
    @Nullable
    public CharSequence process(@Nullable final CharSequence charSequence) {
        int length;
        if (charSequence == null) {
            length = 0;
        }
        else {
            length = charSequence.length();
        }
        return this.process(charSequence, 0, length);
    }
    
    @CheckResult
    @Nullable
    public CharSequence process(@Nullable final CharSequence charSequence, @IntRange(from = 0L) final int n, @IntRange(from = 0L) final int n2) {
        return this.process(charSequence, n, n2, Integer.MAX_VALUE);
    }
    
    @CheckResult
    @Nullable
    public CharSequence process(@Nullable final CharSequence charSequence, @IntRange(from = 0L) final int n, @IntRange(from = 0L) final int n2, @IntRange(from = 0L) final int n3) {
        return this.process(charSequence, n, n2, n3, 0);
    }
    
    @CheckResult
    @Nullable
    public CharSequence process(@Nullable final CharSequence charSequence, @IntRange(from = 0L) final int n, @IntRange(from = 0L) final int n2, @IntRange(from = 0L) final int n3, final int n4) {
        Preconditions.checkState(this.isInitialized(), "Not initialized yet");
        Preconditions.checkArgumentNonnegative(n, "start cannot be negative");
        Preconditions.checkArgumentNonnegative(n2, "end cannot be negative");
        Preconditions.checkArgumentNonnegative(n3, "maxEmojiCount cannot be negative");
        Preconditions.checkArgument(n <= n2, (Object)"start should be <= than end");
        if (charSequence == null) {
            return null;
        }
        Preconditions.checkArgument(n <= charSequence.length(), (Object)"start should be < than charSequence length");
        Preconditions.checkArgument(n2 <= charSequence.length(), (Object)"end should be < than charSequence length");
        CharSequence process = charSequence;
        if (charSequence.length() != 0) {
            if (n == n2) {
                process = charSequence;
            }
            else {
                process = this.mHelper.process(charSequence, n, n2, n3, n4 == 1 || (n4 != 2 && this.mReplaceAll));
            }
        }
        return process;
    }
    
    public void registerInitCallback(@NonNull final InitCallback initCallback) {
        Preconditions.checkNotNull(initCallback, "initCallback cannot be null");
        this.mInitLock.writeLock().lock();
        try {
            if (this.mLoadState != 1 && this.mLoadState != 2) {
                this.mInitCallbacks.add(initCallback);
            }
            else {
                this.mMainHandler.post((Runnable)new ListenerDispatcher(initCallback, this.mLoadState));
            }
        }
        finally {
            this.mInitLock.writeLock().unlock();
        }
    }
    
    public void unregisterInitCallback(@NonNull final InitCallback initCallback) {
        Preconditions.checkNotNull(initCallback, "initCallback cannot be null");
        this.mInitLock.writeLock().lock();
        try {
            this.mInitCallbacks.remove(initCallback);
        }
        finally {
            this.mInitLock.writeLock().unlock();
        }
    }
    
    public void updateEditorInfo(@NonNull final EditorInfo editorInfo) {
        if (this.isInitialized()) {
            if (editorInfo != null) {
                if (editorInfo.extras == null) {
                    editorInfo.extras = new Bundle();
                }
                this.mHelper.updateEditorInfoAttrs(editorInfo);
            }
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface CodepointSequenceMatchResult {
    }
    
    private static class CompatInternal
    {
        final EmojiCompat mEmojiCompat;
        
        CompatInternal(final EmojiCompat mEmojiCompat) {
            this.mEmojiCompat = mEmojiCompat;
        }
        
        String getAssetSignature() {
            return "";
        }
        
        public int getEmojiMatch(final CharSequence charSequence, final int n) {
            return 0;
        }
        
        boolean hasEmojiGlyph(@NonNull final CharSequence charSequence) {
            return false;
        }
        
        boolean hasEmojiGlyph(@NonNull final CharSequence charSequence, final int n) {
            return false;
        }
        
        void loadMetadata() {
            this.mEmojiCompat.onMetadataLoadSuccess();
        }
        
        CharSequence process(@NonNull final CharSequence charSequence, @IntRange(from = 0L) final int n, @IntRange(from = 0L) final int n2, @IntRange(from = 0L) final int n3, final boolean b) {
            return charSequence;
        }
        
        void updateEditorInfoAttrs(@NonNull final EditorInfo editorInfo) {
        }
    }
    
    @RequiresApi(19)
    private static final class CompatInternal19 extends CompatInternal
    {
        private volatile MetadataRepo mMetadataRepo;
        private volatile EmojiProcessor mProcessor;
        
        CompatInternal19(final EmojiCompat emojiCompat) {
            super(emojiCompat);
        }
        
        @Override
        String getAssetSignature() {
            String sourceSha;
            if ((sourceSha = this.mMetadataRepo.getMetadataList().sourceSha()) == null) {
                sourceSha = "";
            }
            return sourceSha;
        }
        
        @Override
        public int getEmojiMatch(final CharSequence charSequence, final int n) {
            return this.mProcessor.getEmojiMatch(charSequence, n);
        }
        
        @Override
        boolean hasEmojiGlyph(@NonNull final CharSequence charSequence) {
            final int emojiMatch = this.mProcessor.getEmojiMatch(charSequence);
            boolean b = true;
            if (emojiMatch != 1) {
                b = false;
            }
            return b;
        }
        
        @Override
        boolean hasEmojiGlyph(@NonNull final CharSequence charSequence, int emojiMatch) {
            emojiMatch = this.mProcessor.getEmojiMatch(charSequence, emojiMatch);
            boolean b = true;
            if (emojiMatch != 1) {
                b = false;
            }
            return b;
        }
        
        @Override
        void loadMetadata() {
            try {
                super.mEmojiCompat.mMetadataLoader.load(new MetadataRepoLoaderCallback(this) {
                    final CompatInternal19 this$0;
                    
                    @Override
                    public void onFailed(@Nullable final Throwable t) {
                        this.this$0.mEmojiCompat.onMetadataLoadFailed(t);
                    }
                    
                    @Override
                    public void onLoaded(@NonNull final MetadataRepo metadataRepo) {
                        this.this$0.onMetadataLoadSuccess(metadataRepo);
                    }
                });
            }
            finally {
                final Throwable t;
                super.mEmojiCompat.onMetadataLoadFailed(t);
            }
        }
        
        void onMetadataLoadSuccess(@NonNull final MetadataRepo mMetadataRepo) {
            if (mMetadataRepo == null) {
                super.mEmojiCompat.onMetadataLoadFailed(new IllegalArgumentException("metadataRepo cannot be null"));
                return;
            }
            this.mMetadataRepo = mMetadataRepo;
            final MetadataRepo mMetadataRepo2 = this.mMetadataRepo;
            final SpanFactory spanFactory = new SpanFactory();
            final GlyphChecker access$000 = super.mEmojiCompat.mGlyphChecker;
            final EmojiCompat mEmojiCompat = super.mEmojiCompat;
            this.mProcessor = new EmojiProcessor(mMetadataRepo2, spanFactory, access$000, mEmojiCompat.mUseEmojiAsDefaultStyle, mEmojiCompat.mEmojiAsDefaultStyleExceptions);
            super.mEmojiCompat.onMetadataLoadSuccess();
        }
        
        @Override
        CharSequence process(@NonNull final CharSequence charSequence, final int n, final int n2, final int n3, final boolean b) {
            return this.mProcessor.process(charSequence, n, n2, n3, b);
        }
        
        @Override
        void updateEditorInfoAttrs(@NonNull final EditorInfo editorInfo) {
            ((BaseBundle)editorInfo.extras).putInt("android.support.text.emoji.emojiCompat_metadataVersion", this.mMetadataRepo.getMetadataVersion());
            editorInfo.extras.putBoolean("android.support.text.emoji.emojiCompat_replaceAll", super.mEmojiCompat.mReplaceAll);
        }
    }
    
    public abstract static class MetadataRepoLoaderCallback
    {
        public abstract void onFailed(@Nullable final Throwable p0);
        
        public abstract void onLoaded(@NonNull final MetadataRepo p0);
    }
    
    public abstract static class Config
    {
        @Nullable
        int[] mEmojiAsDefaultStyleExceptions;
        int mEmojiSpanIndicatorColor;
        boolean mEmojiSpanIndicatorEnabled;
        @NonNull
        GlyphChecker mGlyphChecker;
        @Nullable
        Set<InitCallback> mInitCallbacks;
        int mMetadataLoadStrategy;
        @NonNull
        final MetadataRepoLoader mMetadataLoader;
        boolean mReplaceAll;
        boolean mUseEmojiAsDefaultStyle;
        
        protected Config(@NonNull final MetadataRepoLoader mMetadataLoader) {
            this.mEmojiSpanIndicatorColor = -16711936;
            this.mMetadataLoadStrategy = 0;
            this.mGlyphChecker = new DefaultGlyphChecker();
            Preconditions.checkNotNull(mMetadataLoader, "metadataLoader cannot be null.");
            this.mMetadataLoader = mMetadataLoader;
        }
        
        @NonNull
        protected final MetadataRepoLoader getMetadataRepoLoader() {
            return this.mMetadataLoader;
        }
        
        @NonNull
        public Config registerInitCallback(@NonNull final InitCallback initCallback) {
            Preconditions.checkNotNull(initCallback, "initCallback cannot be null");
            if (this.mInitCallbacks == null) {
                this.mInitCallbacks = new ArraySet<InitCallback>();
            }
            this.mInitCallbacks.add(initCallback);
            return this;
        }
        
        @NonNull
        public Config setEmojiSpanIndicatorColor(@ColorInt final int mEmojiSpanIndicatorColor) {
            this.mEmojiSpanIndicatorColor = mEmojiSpanIndicatorColor;
            return this;
        }
        
        @NonNull
        public Config setEmojiSpanIndicatorEnabled(final boolean mEmojiSpanIndicatorEnabled) {
            this.mEmojiSpanIndicatorEnabled = mEmojiSpanIndicatorEnabled;
            return this;
        }
        
        @NonNull
        public Config setGlyphChecker(@NonNull final GlyphChecker mGlyphChecker) {
            Preconditions.checkNotNull(mGlyphChecker, "GlyphChecker cannot be null");
            this.mGlyphChecker = mGlyphChecker;
            return this;
        }
        
        @NonNull
        public Config setMetadataLoadStrategy(final int mMetadataLoadStrategy) {
            this.mMetadataLoadStrategy = mMetadataLoadStrategy;
            return this;
        }
        
        @NonNull
        public Config setReplaceAll(final boolean mReplaceAll) {
            this.mReplaceAll = mReplaceAll;
            return this;
        }
        
        @NonNull
        public Config setUseEmojiAsDefaultStyle(final boolean b) {
            return this.setUseEmojiAsDefaultStyle(b, null);
        }
        
        @NonNull
        public Config setUseEmojiAsDefaultStyle(final boolean mUseEmojiAsDefaultStyle, @Nullable final List<Integer> list) {
            this.mUseEmojiAsDefaultStyle = mUseEmojiAsDefaultStyle;
            if (mUseEmojiAsDefaultStyle && list != null) {
                this.mEmojiAsDefaultStyleExceptions = new int[list.size()];
                final Iterator iterator = list.iterator();
                int n = 0;
                while (iterator.hasNext()) {
                    this.mEmojiAsDefaultStyleExceptions[n] = (int)iterator.next();
                    ++n;
                }
                Arrays.sort(this.mEmojiAsDefaultStyleExceptions);
            }
            else {
                this.mEmojiAsDefaultStyleExceptions = null;
            }
            return this;
        }
        
        @NonNull
        public Config unregisterInitCallback(@NonNull final InitCallback initCallback) {
            Preconditions.checkNotNull(initCallback, "initCallback cannot be null");
            final Set<InitCallback> mInitCallbacks = this.mInitCallbacks;
            if (mInitCallbacks != null) {
                mInitCallbacks.remove(initCallback);
            }
            return this;
        }
    }
    
    public interface GlyphChecker
    {
        boolean hasGlyph(@NonNull final CharSequence p0, @IntRange(from = 0L) final int p1, @IntRange(from = 0L) final int p2, @IntRange(from = 0L) final int p3);
    }
    
    public abstract static class InitCallback
    {
        public void onFailed(@Nullable final Throwable t) {
        }
        
        public void onInitialized() {
        }
    }
    
    private static class ListenerDispatcher implements Runnable
    {
        private final List<InitCallback> mInitCallbacks;
        private final int mLoadState;
        private final Throwable mThrowable;
        
        ListenerDispatcher(@NonNull final InitCallback initCallback, final int n) {
            this(Arrays.asList(Preconditions.checkNotNull(initCallback, "initCallback cannot be null")), n, null);
        }
        
        ListenerDispatcher(@NonNull final Collection<InitCallback> collection, final int n) {
            this(collection, n, null);
        }
        
        ListenerDispatcher(@NonNull final Collection<InitCallback> c, final int mLoadState, @Nullable final Throwable mThrowable) {
            Preconditions.checkNotNull(c, "initCallbacks cannot be null");
            this.mInitCallbacks = new ArrayList<InitCallback>(c);
            this.mLoadState = mLoadState;
            this.mThrowable = mThrowable;
        }
        
        @Override
        public void run() {
            final int size = this.mInitCallbacks.size();
            final int mLoadState = this.mLoadState;
            int i = 0;
            final int n = 0;
            if (mLoadState != 1) {
                for (int j = n; j < size; ++j) {
                    this.mInitCallbacks.get(j).onFailed(this.mThrowable);
                }
            }
            else {
                while (i < size) {
                    this.mInitCallbacks.get(i).onInitialized();
                    ++i;
                }
            }
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface LoadStrategy {
    }
    
    public interface MetadataRepoLoader
    {
        void load(@NonNull final MetadataRepoLoaderCallback p0);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public @interface ReplaceStrategy {
    }
    
    @RequiresApi(19)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    static class SpanFactory
    {
        EmojiSpan createSpan(@NonNull final EmojiMetadata emojiMetadata) {
            return new TypefaceEmojiSpan(emojiMetadata);
        }
    }
}

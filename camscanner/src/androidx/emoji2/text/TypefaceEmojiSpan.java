// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import androidx.annotation.IntRange;
import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.Paint$Style;
import android.text.TextPaint;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.graphics.Paint;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;

@RequiresApi(19)
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public final class TypefaceEmojiSpan extends EmojiSpan
{
    @Nullable
    private static Paint sDebugPaint;
    
    public TypefaceEmojiSpan(@NonNull final EmojiMetadata emojiMetadata) {
        super(emojiMetadata);
    }
    
    @NonNull
    private static Paint getDebugPaint() {
        if (TypefaceEmojiSpan.sDebugPaint == null) {
            (TypefaceEmojiSpan.sDebugPaint = (Paint)new TextPaint()).setColor(EmojiCompat.get().getEmojiSpanIndicatorColor());
            TypefaceEmojiSpan.sDebugPaint.setStyle(Paint$Style.FILL);
        }
        return TypefaceEmojiSpan.sDebugPaint;
    }
    
    public void draw(@NonNull final Canvas canvas, @SuppressLint({ "UnknownNullness" }) final CharSequence charSequence, @IntRange(from = 0L) final int n, @IntRange(from = 0L) final int n2, final float n3, final int n4, final int n5, final int n6, @NonNull final Paint paint) {
        if (EmojiCompat.get().isEmojiSpanIndicatorEnabled()) {
            canvas.drawRect(n3, (float)n4, n3 + this.getWidth(), (float)n6, getDebugPaint());
        }
        this.getMetadata().draw(canvas, n3, (float)n5, paint);
    }
}

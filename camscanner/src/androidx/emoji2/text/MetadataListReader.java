// 
// Decompiled by Procyon v0.6.0
// 

package androidx.emoji2.text;

import androidx.annotation.IntRange;
import java.nio.ByteOrder;
import androidx.annotation.NonNull;
import java.nio.ByteBuffer;
import java.io.InputStream;
import androidx.emoji2.text.flatbuffer.MetadataList;
import android.content.res.AssetManager;
import java.io.IOException;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;
import androidx.annotation.AnyThread;

@AnyThread
@RequiresApi(19)
@RestrictTo({ RestrictTo.Scope.LIBRARY })
class MetadataListReader
{
    private static final int EMJI_TAG = 1164798569;
    private static final int EMJI_TAG_DEPRECATED = 1701669481;
    private static final int META_TABLE_NAME = 1835365473;
    
    private MetadataListReader() {
    }
    
    private static OffsetInfo findOffsetInfo(final OpenTypeReader openTypeReader) throws IOException {
        openTypeReader.skip(4);
        final int unsignedShort = openTypeReader.readUnsignedShort();
        if (unsignedShort <= 100) {
            openTypeReader.skip(6);
            final int n = 0;
            while (true) {
                for (int i = 0; i < unsignedShort; ++i) {
                    final int tag = openTypeReader.readTag();
                    openTypeReader.skip(4);
                    final long unsignedInt = openTypeReader.readUnsignedInt();
                    openTypeReader.skip(4);
                    if (1835365473 == tag) {
                        if (unsignedInt != -1L) {
                            openTypeReader.skip((int)(unsignedInt - openTypeReader.getPosition()));
                            openTypeReader.skip(12);
                            final long unsignedInt2 = openTypeReader.readUnsignedInt();
                            for (int n2 = n; n2 < unsignedInt2; ++n2) {
                                final int tag2 = openTypeReader.readTag();
                                final long unsignedInt3 = openTypeReader.readUnsignedInt();
                                final long unsignedInt4 = openTypeReader.readUnsignedInt();
                                if (1164798569 == tag2 || 1701669481 == tag2) {
                                    return new OffsetInfo(unsignedInt3 + unsignedInt, unsignedInt4);
                                }
                            }
                        }
                        throw new IOException("Cannot read metadata.");
                    }
                }
                final long unsignedInt = -1L;
                continue;
            }
        }
        throw new IOException("Cannot read metadata.");
    }
    
    static MetadataList read(AssetManager open, final String s) throws IOException {
        open = (AssetManager)open.open(s);
        try {
            final MetadataList read = read((InputStream)open);
            if (open != null) {
                ((InputStream)open).close();
            }
            return read;
        }
        finally {
            if (open != null) {
                try {
                    ((InputStream)open).close();
                }
                finally {
                    final Throwable exception;
                    ((Throwable)s).addSuppressed(exception);
                }
            }
        }
    }
    
    static MetadataList read(final InputStream inputStream) throws IOException {
        final InputStreamOpenTypeReader inputStreamOpenTypeReader = new InputStreamOpenTypeReader(inputStream);
        final OffsetInfo offsetInfo = findOffsetInfo((OpenTypeReader)inputStreamOpenTypeReader);
        ((OpenTypeReader)inputStreamOpenTypeReader).skip((int)(offsetInfo.getStartOffset() - ((OpenTypeReader)inputStreamOpenTypeReader).getPosition()));
        final ByteBuffer allocate = ByteBuffer.allocate((int)offsetInfo.getLength());
        final int read = inputStream.read(allocate.array());
        if (read == offsetInfo.getLength()) {
            return MetadataList.getRootAsMetadataList(allocate);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Needed ");
        sb.append(offsetInfo.getLength());
        sb.append(" bytes, got ");
        sb.append(read);
        throw new IOException(sb.toString());
    }
    
    static MetadataList read(ByteBuffer duplicate) throws IOException {
        duplicate = duplicate.duplicate();
        duplicate.position((int)findOffsetInfo((OpenTypeReader)new ByteBufferReader(duplicate)).getStartOffset());
        return MetadataList.getRootAsMetadataList(duplicate);
    }
    
    static long toUnsignedInt(final int n) {
        return (long)n & 0xFFFFFFFFL;
    }
    
    static int toUnsignedShort(final short n) {
        return n & 0xFFFF;
    }
    
    private static class ByteBufferReader implements OpenTypeReader
    {
        @NonNull
        private final ByteBuffer mByteBuffer;
        
        ByteBufferReader(@NonNull final ByteBuffer mByteBuffer) {
            (this.mByteBuffer = mByteBuffer).order(ByteOrder.BIG_ENDIAN);
        }
        
        @Override
        public long getPosition() {
            return this.mByteBuffer.position();
        }
        
        @Override
        public int readTag() throws IOException {
            return this.mByteBuffer.getInt();
        }
        
        @Override
        public long readUnsignedInt() throws IOException {
            return MetadataListReader.toUnsignedInt(this.mByteBuffer.getInt());
        }
        
        @Override
        public int readUnsignedShort() throws IOException {
            return MetadataListReader.toUnsignedShort(this.mByteBuffer.getShort());
        }
        
        @Override
        public void skip(final int n) throws IOException {
            final ByteBuffer mByteBuffer = this.mByteBuffer;
            mByteBuffer.position(mByteBuffer.position() + n);
        }
    }
    
    private interface OpenTypeReader
    {
        public static final int UINT16_BYTE_COUNT = 2;
        public static final int UINT32_BYTE_COUNT = 4;
        
        long getPosition();
        
        int readTag() throws IOException;
        
        long readUnsignedInt() throws IOException;
        
        int readUnsignedShort() throws IOException;
        
        void skip(final int p0) throws IOException;
    }
    
    private static class InputStreamOpenTypeReader implements OpenTypeReader
    {
        @NonNull
        private final byte[] mByteArray;
        @NonNull
        private final ByteBuffer mByteBuffer;
        @NonNull
        private final InputStream mInputStream;
        private long mPosition;
        
        InputStreamOpenTypeReader(@NonNull final InputStream mInputStream) {
            this.mPosition = 0L;
            this.mInputStream = mInputStream;
            final byte[] array = new byte[4];
            this.mByteArray = array;
            (this.mByteBuffer = ByteBuffer.wrap(array)).order(ByteOrder.BIG_ENDIAN);
        }
        
        private void read(@IntRange(from = 0L, to = 4L) final int len) throws IOException {
            if (this.mInputStream.read(this.mByteArray, 0, len) == len) {
                this.mPosition += len;
                return;
            }
            throw new IOException("read failed");
        }
        
        @Override
        public long getPosition() {
            return this.mPosition;
        }
        
        @Override
        public int readTag() throws IOException {
            this.mByteBuffer.position(0);
            this.read(4);
            return this.mByteBuffer.getInt();
        }
        
        @Override
        public long readUnsignedInt() throws IOException {
            this.mByteBuffer.position(0);
            this.read(4);
            return MetadataListReader.toUnsignedInt(this.mByteBuffer.getInt());
        }
        
        @Override
        public int readUnsignedShort() throws IOException {
            this.mByteBuffer.position(0);
            this.read(2);
            return MetadataListReader.toUnsignedShort(this.mByteBuffer.getShort());
        }
        
        @Override
        public void skip(int i) throws IOException {
            while (i > 0) {
                final int n = (int)this.mInputStream.skip(i);
                if (n < 1) {
                    throw new IOException("Skip didn't move at least 1 byte forward");
                }
                i -= n;
                this.mPosition += n;
            }
        }
    }
    
    private static class OffsetInfo
    {
        private final long mLength;
        private final long mStartOffset;
        
        OffsetInfo(final long mStartOffset, final long mLength) {
            this.mStartOffset = mStartOffset;
            this.mLength = mLength;
        }
        
        long getLength() {
            return this.mLength;
        }
        
        long getStartOffset() {
            return this.mStartOffset;
        }
    }
}

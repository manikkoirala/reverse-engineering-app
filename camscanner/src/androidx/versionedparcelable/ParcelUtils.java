// 
// Decompiled by Procyon v0.6.0
// 

package androidx.versionedparcelable;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.RestrictTo;
import java.io.OutputStream;
import java.io.InputStream;

public class ParcelUtils
{
    private static final String INNER_BUNDLE_KEY = "a";
    
    private ParcelUtils() {
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static <T extends VersionedParcelable> T fromInputStream(final InputStream inputStream) {
        return new VersionedParcelStream(inputStream, null).readVersionedParcelable();
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static <T extends VersionedParcelable> T fromParcelable(final Parcelable parcelable) {
        if (parcelable instanceof ParcelImpl) {
            return ((ParcelImpl)parcelable).getVersionedParcel();
        }
        throw new IllegalArgumentException("Invalid parcel");
    }
    
    @Nullable
    public static <T extends VersionedParcelable> T getVersionedParcelable(@NonNull Bundle bundle, @NonNull final String s) {
        try {
            bundle = (Bundle)bundle.getParcelable(s);
            if (bundle == null) {
                return null;
            }
            bundle.setClassLoader(ParcelUtils.class.getClassLoader());
            return fromParcelable(bundle.getParcelable("a"));
        }
        catch (final RuntimeException ex) {
            return null;
        }
    }
    
    @Nullable
    public static <T extends VersionedParcelable> List<T> getVersionedParcelableList(Bundle bundle, final String s) {
        final ArrayList list = new ArrayList();
        try {
            bundle = (Bundle)bundle.getParcelable(s);
            bundle.setClassLoader(ParcelUtils.class.getClassLoader());
            final Iterator iterator = bundle.getParcelableArrayList("a").iterator();
            while (iterator.hasNext()) {
                list.add(fromParcelable((Parcelable)iterator.next()));
            }
            return list;
        }
        catch (final RuntimeException ex) {
            return null;
        }
    }
    
    public static void putVersionedParcelable(@NonNull final Bundle bundle, @NonNull final String s, @Nullable final VersionedParcelable versionedParcelable) {
        if (versionedParcelable == null) {
            return;
        }
        final Bundle bundle2 = new Bundle();
        bundle2.putParcelable("a", toParcelable(versionedParcelable));
        bundle.putParcelable(s, (Parcelable)bundle2);
    }
    
    public static void putVersionedParcelableList(@NonNull final Bundle bundle, @NonNull final String s, @NonNull final List<? extends VersionedParcelable> list) {
        final Bundle bundle2 = new Bundle();
        final ArrayList list2 = new ArrayList();
        final Iterator<? extends VersionedParcelable> iterator = list.iterator();
        while (iterator.hasNext()) {
            list2.add(toParcelable((VersionedParcelable)iterator.next()));
        }
        bundle2.putParcelableArrayList("a", list2);
        bundle.putParcelable(s, (Parcelable)bundle2);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static void toOutputStream(final VersionedParcelable versionedParcelable, final OutputStream outputStream) {
        final VersionedParcelStream versionedParcelStream = new VersionedParcelStream(null, outputStream);
        versionedParcelStream.writeVersionedParcelable(versionedParcelable);
        versionedParcelStream.closeField();
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static Parcelable toParcelable(final VersionedParcelable versionedParcelable) {
        return (Parcelable)new ParcelImpl(versionedParcelable);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.versionedparcelable;

import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public abstract class CustomVersionedParcelable implements VersionedParcelable
{
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void onPostParceling() {
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void onPreParceling(final boolean b) {
    }
}

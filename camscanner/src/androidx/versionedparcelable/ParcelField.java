// 
// Decompiled by Procyon v0.6.0
// 

package androidx.versionedparcelable;

import androidx.annotation.RestrictTo;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.SOURCE)
@Target({ ElementType.FIELD })
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public @interface ParcelField {
    String defaultValue() default "";
    
    int value();
}

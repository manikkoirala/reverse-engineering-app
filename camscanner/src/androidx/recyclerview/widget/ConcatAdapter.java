// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview.widget;

import android.view.ViewGroup;
import java.util.Collections;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import androidx.annotation.NonNull;

public final class ConcatAdapter extends Adapter<ViewHolder>
{
    static final String TAG = "ConcatAdapter";
    private final ConcatAdapterController mController;
    
    public ConcatAdapter(@NonNull final Config config, @NonNull final List<? extends Adapter<? extends ViewHolder>> list) {
        this.mController = new ConcatAdapterController(this, config);
        final Iterator<? extends Adapter<? extends ViewHolder>> iterator = list.iterator();
        while (iterator.hasNext()) {
            this.addAdapter((Adapter<? extends ViewHolder>)iterator.next());
        }
        super.setHasStableIds(this.mController.hasStableIds());
    }
    
    @SafeVarargs
    public ConcatAdapter(@NonNull final Config config, @NonNull final Adapter<? extends ViewHolder>... a) {
        this(config, Arrays.asList(a));
    }
    
    public ConcatAdapter(@NonNull final List<? extends Adapter<? extends ViewHolder>> list) {
        this(Config.DEFAULT, list);
    }
    
    @SafeVarargs
    public ConcatAdapter(@NonNull final Adapter<? extends ViewHolder>... array) {
        this(Config.DEFAULT, array);
    }
    
    public boolean addAdapter(final int n, @NonNull final Adapter<? extends ViewHolder> adapter) {
        return this.mController.addAdapter(n, (Adapter<ViewHolder>)adapter);
    }
    
    public boolean addAdapter(@NonNull final Adapter<? extends ViewHolder> adapter) {
        return this.mController.addAdapter((Adapter<ViewHolder>)adapter);
    }
    
    @Override
    public int findRelativeAdapterPositionIn(@NonNull final Adapter<? extends ViewHolder> adapter, @NonNull final ViewHolder viewHolder, final int n) {
        return this.mController.getLocalAdapterPosition(adapter, viewHolder, n);
    }
    
    @NonNull
    public List<? extends Adapter<? extends ViewHolder>> getAdapters() {
        return Collections.unmodifiableList((List<? extends Adapter<? extends ViewHolder>>)this.mController.getCopyOfAdapters());
    }
    
    @Override
    public int getItemCount() {
        return this.mController.getTotalCount();
    }
    
    @Override
    public long getItemId(final int n) {
        return this.mController.getItemId(n);
    }
    
    @Override
    public int getItemViewType(final int n) {
        return this.mController.getItemViewType(n);
    }
    
    void internalSetStateRestorationPolicy(@NonNull final StateRestorationPolicy stateRestorationPolicy) {
        super.setStateRestorationPolicy(stateRestorationPolicy);
    }
    
    @Override
    public void onAttachedToRecyclerView(@NonNull final RecyclerView recyclerView) {
        this.mController.onAttachedToRecyclerView(recyclerView);
    }
    
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int n) {
        this.mController.onBindViewHolder(viewHolder, n);
    }
    
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, final int n) {
        return this.mController.onCreateViewHolder(viewGroup, n);
    }
    
    @Override
    public void onDetachedFromRecyclerView(@NonNull final RecyclerView recyclerView) {
        this.mController.onDetachedFromRecyclerView(recyclerView);
    }
    
    @Override
    public boolean onFailedToRecycleView(@NonNull final ViewHolder viewHolder) {
        return this.mController.onFailedToRecycleView(viewHolder);
    }
    
    @Override
    public void onViewAttachedToWindow(@NonNull final ViewHolder viewHolder) {
        this.mController.onViewAttachedToWindow(viewHolder);
    }
    
    @Override
    public void onViewDetachedFromWindow(@NonNull final ViewHolder viewHolder) {
        this.mController.onViewDetachedFromWindow(viewHolder);
    }
    
    @Override
    public void onViewRecycled(@NonNull final ViewHolder viewHolder) {
        this.mController.onViewRecycled(viewHolder);
    }
    
    public boolean removeAdapter(@NonNull final Adapter<? extends ViewHolder> adapter) {
        return this.mController.removeAdapter((Adapter<ViewHolder>)adapter);
    }
    
    @Override
    public void setHasStableIds(final boolean b) {
        throw new UnsupportedOperationException("Calling setHasStableIds is not allowed on the ConcatAdapter. Use the Config object passed in the constructor to control this behavior");
    }
    
    @Override
    public void setStateRestorationPolicy(@NonNull final StateRestorationPolicy stateRestorationPolicy) {
        throw new UnsupportedOperationException("Calling setStateRestorationPolicy is not allowed on the ConcatAdapter. This value is inferred from added adapters");
    }
    
    public static final class Config
    {
        @NonNull
        public static final Config DEFAULT;
        public final boolean isolateViewTypes;
        @NonNull
        public final StableIdMode stableIdMode;
        
        static {
            DEFAULT = new Config(true, StableIdMode.NO_STABLE_IDS);
        }
        
        Config(final boolean isolateViewTypes, @NonNull final StableIdMode stableIdMode) {
            this.isolateViewTypes = isolateViewTypes;
            this.stableIdMode = stableIdMode;
        }
        
        public static final class Builder
        {
            private boolean mIsolateViewTypes;
            private StableIdMode mStableIdMode;
            
            public Builder() {
                final Config default1 = Config.DEFAULT;
                this.mIsolateViewTypes = default1.isolateViewTypes;
                this.mStableIdMode = default1.stableIdMode;
            }
            
            @NonNull
            public Config build() {
                return new Config(this.mIsolateViewTypes, this.mStableIdMode);
            }
            
            @NonNull
            public Builder setIsolateViewTypes(final boolean mIsolateViewTypes) {
                this.mIsolateViewTypes = mIsolateViewTypes;
                return this;
            }
            
            @NonNull
            public Builder setStableIdMode(@NonNull final StableIdMode mStableIdMode) {
                this.mStableIdMode = mStableIdMode;
                return this;
            }
        }
        
        public enum StableIdMode
        {
            private static final StableIdMode[] $VALUES;
            
            ISOLATED_STABLE_IDS, 
            NO_STABLE_IDS, 
            SHARED_STABLE_IDS;
        }
    }
}

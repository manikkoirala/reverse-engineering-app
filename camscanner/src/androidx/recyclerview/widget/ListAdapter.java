// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview.widget;

import androidx.annotation.Nullable;
import java.util.List;
import androidx.annotation.NonNull;

public abstract class ListAdapter<T, VH extends ViewHolder> extends Adapter<VH>
{
    final AsyncListDiffer<T> mDiffer;
    private final AsyncListDiffer.ListListener<T> mListener;
    
    protected ListAdapter(@NonNull final AsyncDifferConfig<T> asyncDifferConfig) {
        final AsyncListDiffer.ListListener<T> mListener = new AsyncListDiffer.ListListener<T>() {
            final ListAdapter this$0;
            
            @Override
            public void onCurrentListChanged(@NonNull final List<T> list, @NonNull final List<T> list2) {
                this.this$0.onCurrentListChanged(list, list2);
            }
        };
        this.mListener = mListener;
        (this.mDiffer = new AsyncListDiffer<T>(new AdapterListUpdateCallback(this), asyncDifferConfig)).addListListener((AsyncListDiffer.ListListener<T>)mListener);
    }
    
    protected ListAdapter(@NonNull final DiffUtil.ItemCallback<T> itemCallback) {
        final AsyncListDiffer.ListListener<T> mListener = new AsyncListDiffer.ListListener<T>() {
            final ListAdapter this$0;
            
            @Override
            public void onCurrentListChanged(@NonNull final List<T> list, @NonNull final List<T> list2) {
                this.this$0.onCurrentListChanged(list, list2);
            }
        };
        this.mListener = mListener;
        (this.mDiffer = new AsyncListDiffer<T>(new AdapterListUpdateCallback(this), new AsyncDifferConfig.Builder<T>(itemCallback).build())).addListListener((AsyncListDiffer.ListListener<T>)mListener);
    }
    
    @NonNull
    public List<T> getCurrentList() {
        return this.mDiffer.getCurrentList();
    }
    
    protected T getItem(final int n) {
        return this.mDiffer.getCurrentList().get(n);
    }
    
    @Override
    public int getItemCount() {
        return this.mDiffer.getCurrentList().size();
    }
    
    public void onCurrentListChanged(@NonNull final List<T> list, @NonNull final List<T> list2) {
    }
    
    public void submitList(@Nullable final List<T> list) {
        this.mDiffer.submitList(list);
    }
    
    public void submitList(@Nullable final List<T> list, @Nullable final Runnable runnable) {
        this.mDiffer.submitList(list, runnable);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview.widget;

import androidx.collection.LongSparseArray;
import androidx.annotation.NonNull;

interface StableIdStorage
{
    @NonNull
    StableIdLookup createStableIdLookup();
    
    public static class IsolatedStableIdStorage implements StableIdStorage
    {
        long mNextStableId;
        
        public IsolatedStableIdStorage() {
            this.mNextStableId = 0L;
        }
        
        @NonNull
        @Override
        public StableIdLookup createStableIdLookup() {
            return new WrapperStableIdLookup();
        }
        
        long obtainId() {
            final long mNextStableId = this.mNextStableId;
            this.mNextStableId = 1L + mNextStableId;
            return mNextStableId;
        }
        
        class WrapperStableIdLookup implements StableIdLookup
        {
            private final LongSparseArray<Long> mLocalToGlobalLookup;
            final IsolatedStableIdStorage this$0;
            
            WrapperStableIdLookup(final IsolatedStableIdStorage this$0) {
                this.this$0 = this$0;
                this.mLocalToGlobalLookup = new LongSparseArray<Long>();
            }
            
            @Override
            public long localToGlobal(final long n) {
                Long value;
                if ((value = this.mLocalToGlobalLookup.get(n)) == null) {
                    value = this.this$0.obtainId();
                    this.mLocalToGlobalLookup.put(n, value);
                }
                return value;
            }
        }
    }
    
    public static class NoStableIdStorage implements StableIdStorage
    {
        private final StableIdLookup mNoIdLookup;
        
        public NoStableIdStorage() {
            this.mNoIdLookup = new StableIdLookup() {
                final NoStableIdStorage this$0;
                
                @Override
                public long localToGlobal(final long n) {
                    return -1L;
                }
            };
        }
        
        @NonNull
        @Override
        public StableIdLookup createStableIdLookup() {
            return this.mNoIdLookup;
        }
    }
    
    public static class SharedPoolStableIdStorage implements StableIdStorage
    {
        private final StableIdLookup mSameIdLookup;
        
        public SharedPoolStableIdStorage() {
            this.mSameIdLookup = new StableIdLookup() {
                final SharedPoolStableIdStorage this$0;
                
                @Override
                public long localToGlobal(final long n) {
                    return n;
                }
            };
        }
        
        @NonNull
        @Override
        public StableIdLookup createStableIdLookup() {
            return this.mSameIdLookup;
        }
    }
    
    public interface StableIdLookup
    {
        long localToGlobal(final long p0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.recyclerview.widget;

import android.view.ViewGroup;
import androidx.core.util.Preconditions;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;

class NestedAdapterWrapper
{
    public final RecyclerView.Adapter<RecyclerView.ViewHolder> adapter;
    private RecyclerView.AdapterDataObserver mAdapterObserver;
    int mCachedItemCount;
    final Callback mCallback;
    @NonNull
    private final StableIdStorage.StableIdLookup mStableIdLookup;
    @NonNull
    private final ViewTypeStorage.ViewTypeLookup mViewTypeLookup;
    
    NestedAdapterWrapper(final RecyclerView.Adapter<RecyclerView.ViewHolder> adapter, final Callback mCallback, final ViewTypeStorage viewTypeStorage, final StableIdStorage.StableIdLookup mStableIdLookup) {
        this.mAdapterObserver = new RecyclerView.AdapterDataObserver() {
            final NestedAdapterWrapper this$0;
            
            @Override
            public void onChanged() {
                final NestedAdapterWrapper this$0 = this.this$0;
                this$0.mCachedItemCount = this$0.adapter.getItemCount();
                final NestedAdapterWrapper this$2 = this.this$0;
                this$2.mCallback.onChanged(this$2);
            }
            
            @Override
            public void onItemRangeChanged(final int n, final int n2) {
                final NestedAdapterWrapper this$0 = this.this$0;
                this$0.mCallback.onItemRangeChanged(this$0, n, n2, null);
            }
            
            @Override
            public void onItemRangeChanged(final int n, final int n2, @Nullable final Object o) {
                final NestedAdapterWrapper this$0 = this.this$0;
                this$0.mCallback.onItemRangeChanged(this$0, n, n2, o);
            }
            
            @Override
            public void onItemRangeInserted(final int n, final int n2) {
                final NestedAdapterWrapper this$0 = this.this$0;
                this$0.mCachedItemCount += n2;
                this$0.mCallback.onItemRangeInserted(this$0, n, n2);
                final NestedAdapterWrapper this$2 = this.this$0;
                if (this$2.mCachedItemCount > 0 && this$2.adapter.getStateRestorationPolicy() == StateRestorationPolicy.PREVENT_WHEN_EMPTY) {
                    final NestedAdapterWrapper this$3 = this.this$0;
                    this$3.mCallback.onStateRestorationPolicyChanged(this$3);
                }
            }
            
            @Override
            public void onItemRangeMoved(final int n, final int n2, final int n3) {
                boolean b = true;
                if (n3 != 1) {
                    b = false;
                }
                Preconditions.checkArgument(b, (Object)"moving more than 1 item is not supported in RecyclerView");
                final NestedAdapterWrapper this$0 = this.this$0;
                this$0.mCallback.onItemRangeMoved(this$0, n, n2);
            }
            
            @Override
            public void onItemRangeRemoved(final int n, final int n2) {
                final NestedAdapterWrapper this$0 = this.this$0;
                this$0.mCachedItemCount -= n2;
                this$0.mCallback.onItemRangeRemoved(this$0, n, n2);
                final NestedAdapterWrapper this$2 = this.this$0;
                if (this$2.mCachedItemCount < 1 && this$2.adapter.getStateRestorationPolicy() == StateRestorationPolicy.PREVENT_WHEN_EMPTY) {
                    final NestedAdapterWrapper this$3 = this.this$0;
                    this$3.mCallback.onStateRestorationPolicyChanged(this$3);
                }
            }
            
            @Override
            public void onStateRestorationPolicyChanged() {
                final NestedAdapterWrapper this$0 = this.this$0;
                this$0.mCallback.onStateRestorationPolicyChanged(this$0);
            }
        };
        this.adapter = adapter;
        this.mCallback = mCallback;
        this.mViewTypeLookup = viewTypeStorage.createViewTypeWrapper(this);
        this.mStableIdLookup = mStableIdLookup;
        this.mCachedItemCount = adapter.getItemCount();
        adapter.registerAdapterDataObserver(this.mAdapterObserver);
    }
    
    void dispose() {
        this.adapter.unregisterAdapterDataObserver(this.mAdapterObserver);
        this.mViewTypeLookup.dispose();
    }
    
    int getCachedItemCount() {
        return this.mCachedItemCount;
    }
    
    public long getItemId(final int n) {
        return this.mStableIdLookup.localToGlobal(this.adapter.getItemId(n));
    }
    
    int getItemViewType(final int n) {
        return this.mViewTypeLookup.localToGlobal(this.adapter.getItemViewType(n));
    }
    
    void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int n) {
        this.adapter.bindViewHolder(viewHolder, n);
    }
    
    RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int globalToLocal) {
        globalToLocal = this.mViewTypeLookup.globalToLocal(globalToLocal);
        return this.adapter.onCreateViewHolder(viewGroup, globalToLocal);
    }
    
    interface Callback
    {
        void onChanged(@NonNull final NestedAdapterWrapper p0);
        
        void onItemRangeChanged(@NonNull final NestedAdapterWrapper p0, final int p1, final int p2);
        
        void onItemRangeChanged(@NonNull final NestedAdapterWrapper p0, final int p1, final int p2, @Nullable final Object p3);
        
        void onItemRangeInserted(@NonNull final NestedAdapterWrapper p0, final int p1, final int p2);
        
        void onItemRangeMoved(@NonNull final NestedAdapterWrapper p0, final int p1, final int p2);
        
        void onItemRangeRemoved(@NonNull final NestedAdapterWrapper p0, final int p1, final int p2);
        
        void onStateRestorationPolicyChanged(final NestedAdapterWrapper p0);
    }
}

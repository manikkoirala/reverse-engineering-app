// 
// Decompiled by Procyon v0.6.0
// 

package androidx.customview.widget;

public interface Openable
{
    void close();
    
    boolean isOpen();
    
    void open();
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.graphics.Bitmap;
import android.widget.ProgressBar;
import android.view.View;
import androidx.appcompat.R;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import androidx.annotation.NonNull;
import android.content.Context;
import android.widget.RatingBar;

public class AppCompatRatingBar extends RatingBar
{
    private final AppCompatProgressBarHelper mAppCompatProgressBarHelper;
    
    public AppCompatRatingBar(@NonNull final Context context) {
        this(context, null);
    }
    
    public AppCompatRatingBar(@NonNull final Context context, @Nullable final AttributeSet set) {
        this(context, set, R.attr.ratingBarStyle);
    }
    
    public AppCompatRatingBar(@NonNull final Context context, @Nullable final AttributeSet set, final int n) {
        super(context, set, n);
        ThemeUtils.checkAppCompatTheme((View)this, ((View)this).getContext());
        (this.mAppCompatProgressBarHelper = new AppCompatProgressBarHelper((ProgressBar)this)).loadFromAttributes(set, n);
    }
    
    protected void onMeasure(final int n, final int n2) {
        synchronized (this) {
            super.onMeasure(n, n2);
            final Bitmap sampleTile = this.mAppCompatProgressBarHelper.getSampleTile();
            if (sampleTile != null) {
                ((View)this).setMeasuredDimension(View.resolveSizeAndState(sampleTile.getWidth() * this.getNumStars(), n, 0), ((View)this).getMeasuredHeight());
            }
        }
    }
}

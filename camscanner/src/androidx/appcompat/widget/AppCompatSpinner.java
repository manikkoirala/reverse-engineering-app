// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.widget.AbsListView;
import android.view.AbsSavedState;
import android.widget.AbsSpinner;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import android.view.View$BaseSavedState;
import android.widget.PopupWindow$OnDismissListener;
import androidx.core.view.ViewCompat;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemClickListener;
import android.database.DataSetObserver;
import android.os.Build$VERSION;
import android.widget.ListView;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import android.content.DialogInterface$OnClickListener;
import androidx.core.util.ObjectsCompat;
import android.widget.ThemedSpinnerAdapter;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.annotation.DrawableRes;
import android.widget.ListAdapter;
import android.widget.Adapter;
import android.view.MotionEvent;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver$OnGlobalLayoutListener;
import android.os.Parcelable;
import android.graphics.PorterDuff$Mode;
import androidx.annotation.RestrictTo;
import android.content.res.ColorStateList;
import androidx.annotation.VisibleForTesting;
import android.view.ViewGroup$LayoutParams;
import android.view.ViewGroup;
import android.view.View$MeasureSpec;
import android.graphics.drawable.Drawable;
import androidx.appcompat.view.ContextThemeWrapper;
import android.view.View;
import android.content.res.Resources$Theme;
import androidx.annotation.Nullable;
import androidx.appcompat.R;
import android.util.AttributeSet;
import androidx.annotation.NonNull;
import android.graphics.Rect;
import android.widget.SpinnerAdapter;
import android.content.Context;
import androidx.annotation.StyleableRes;
import android.annotation.SuppressLint;
import androidx.core.view.TintableBackgroundView;
import android.widget.Spinner;

public class AppCompatSpinner extends Spinner implements TintableBackgroundView
{
    @SuppressLint({ "ResourceType" })
    @StyleableRes
    private static final int[] ATTRS_ANDROID_SPINNERMODE;
    private static final int MAX_ITEMS_MEASURED = 15;
    private static final int MODE_DIALOG = 0;
    private static final int MODE_DROPDOWN = 1;
    private static final int MODE_THEME = -1;
    private static final String TAG = "AppCompatSpinner";
    private final AppCompatBackgroundHelper mBackgroundTintHelper;
    int mDropDownWidth;
    private ForwardingListener mForwardingListener;
    private SpinnerPopup mPopup;
    private final Context mPopupContext;
    private final boolean mPopupSet;
    private SpinnerAdapter mTempAdapter;
    final Rect mTempRect;
    
    static {
        ATTRS_ANDROID_SPINNERMODE = new int[] { 16843505 };
    }
    
    public AppCompatSpinner(@NonNull final Context context) {
        this(context, null);
    }
    
    public AppCompatSpinner(@NonNull final Context context, final int n) {
        this(context, null, R.attr.spinnerStyle, n);
    }
    
    public AppCompatSpinner(@NonNull final Context context, @Nullable final AttributeSet set) {
        this(context, set, R.attr.spinnerStyle);
    }
    
    public AppCompatSpinner(@NonNull final Context context, @Nullable final AttributeSet set, final int n) {
        this(context, set, n, -1);
    }
    
    public AppCompatSpinner(@NonNull final Context context, @Nullable final AttributeSet set, final int n, final int n2) {
        this(context, set, n, n2, null);
    }
    
    public AppCompatSpinner(@NonNull final Context mPopupContext, @Nullable final AttributeSet set, final int n, final int n2, Resources$Theme resources$Theme) {
        super(mPopupContext, set, n);
        this.mTempRect = new Rect();
        ThemeUtils.checkAppCompatTheme((View)this, ((View)this).getContext());
        final TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(mPopupContext, set, R.styleable.Spinner, n, 0);
        this.mBackgroundTintHelper = new AppCompatBackgroundHelper((View)this);
        if (resources$Theme != null) {
            this.mPopupContext = (Context)new ContextThemeWrapper(mPopupContext, resources$Theme);
        }
        else {
            final int resourceId = obtainStyledAttributes.getResourceId(R.styleable.Spinner_popupTheme, 0);
            if (resourceId != 0) {
                this.mPopupContext = (Context)new ContextThemeWrapper(mPopupContext, resourceId);
            }
            else {
                this.mPopupContext = mPopupContext;
            }
        }
        Object obtainStyledAttributes2 = null;
        if (n2 != -1) {
            goto Label_0219;
        }
        try {
            resources$Theme = (Resources$Theme)(obtainStyledAttributes2 = mPopupContext.obtainStyledAttributes(set, AppCompatSpinner.ATTRS_ANDROID_SPINNERMODE, n, 0));
            try {
                if (((TypedArray)resources$Theme).hasValue(0)) {
                    ((TypedArray)resources$Theme).getInt(0, 0);
                    obtainStyledAttributes2 = resources$Theme;
                }
                ((TypedArray)obtainStyledAttributes2).recycle();
            }
            catch (final Exception obtainStyledAttributes2) {}
        }
        catch (final Exception ex) {}
    }
    
    int compatMeasureContentWidth(final SpinnerAdapter spinnerAdapter, final Drawable drawable) {
        int n = 0;
        if (spinnerAdapter == null) {
            return 0;
        }
        final int measureSpec = View$MeasureSpec.makeMeasureSpec(((View)this).getMeasuredWidth(), 0);
        final int measureSpec2 = View$MeasureSpec.makeMeasureSpec(((View)this).getMeasuredHeight(), 0);
        final int max = Math.max(0, ((AdapterView)this).getSelectedItemPosition());
        final int min = Math.min(((Adapter)spinnerAdapter).getCount(), max + 15);
        int i = Math.max(0, max - (15 - (min - max)));
        View view = null;
        int max2 = 0;
        while (i < min) {
            final int itemViewType = ((Adapter)spinnerAdapter).getItemViewType(i);
            int n2;
            if (itemViewType != (n2 = n)) {
                view = null;
                n2 = itemViewType;
            }
            view = ((Adapter)spinnerAdapter).getView(i, view, (ViewGroup)this);
            if (view.getLayoutParams() == null) {
                view.setLayoutParams(new ViewGroup$LayoutParams(-2, -2));
            }
            view.measure(measureSpec, measureSpec2);
            max2 = Math.max(max2, view.getMeasuredWidth());
            ++i;
            n = n2;
        }
        int n3 = max2;
        if (drawable != null) {
            drawable.getPadding(this.mTempRect);
            final Rect mTempRect = this.mTempRect;
            n3 = max2 + (mTempRect.left + mTempRect.right);
        }
        return n3;
    }
    
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.applySupportBackgroundTint();
        }
    }
    
    public int getDropDownHorizontalOffset() {
        final SpinnerPopup mPopup = this.mPopup;
        if (mPopup != null) {
            return mPopup.getHorizontalOffset();
        }
        return super.getDropDownHorizontalOffset();
    }
    
    public int getDropDownVerticalOffset() {
        final SpinnerPopup mPopup = this.mPopup;
        if (mPopup != null) {
            return mPopup.getVerticalOffset();
        }
        return super.getDropDownVerticalOffset();
    }
    
    public int getDropDownWidth() {
        if (this.mPopup != null) {
            return this.mDropDownWidth;
        }
        return super.getDropDownWidth();
    }
    
    @VisibleForTesting
    final SpinnerPopup getInternalPopup() {
        return this.mPopup;
    }
    
    public Drawable getPopupBackground() {
        final SpinnerPopup mPopup = this.mPopup;
        if (mPopup != null) {
            return mPopup.getBackground();
        }
        return super.getPopupBackground();
    }
    
    public Context getPopupContext() {
        return this.mPopupContext;
    }
    
    public CharSequence getPrompt() {
        final SpinnerPopup mPopup = this.mPopup;
        CharSequence charSequence;
        if (mPopup != null) {
            charSequence = mPopup.getHintText();
        }
        else {
            charSequence = super.getPrompt();
        }
        return charSequence;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public ColorStateList getSupportBackgroundTintList() {
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        ColorStateList supportBackgroundTintList;
        if (mBackgroundTintHelper != null) {
            supportBackgroundTintList = mBackgroundTintHelper.getSupportBackgroundTintList();
        }
        else {
            supportBackgroundTintList = null;
        }
        return supportBackgroundTintList;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public PorterDuff$Mode getSupportBackgroundTintMode() {
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        PorterDuff$Mode supportBackgroundTintMode;
        if (mBackgroundTintHelper != null) {
            supportBackgroundTintMode = mBackgroundTintHelper.getSupportBackgroundTintMode();
        }
        else {
            supportBackgroundTintMode = null;
        }
        return supportBackgroundTintMode;
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        final SpinnerPopup mPopup = this.mPopup;
        if (mPopup != null && mPopup.isShowing()) {
            this.mPopup.dismiss();
        }
    }
    
    protected void onMeasure(final int n, final int n2) {
        super.onMeasure(n, n2);
        if (this.mPopup != null && View$MeasureSpec.getMode(n) == Integer.MIN_VALUE) {
            ((View)this).setMeasuredDimension(Math.min(Math.max(((View)this).getMeasuredWidth(), this.compatMeasureContentWidth(((AbsSpinner)this).getAdapter(), ((View)this).getBackground())), View$MeasureSpec.getSize(n)), ((View)this).getMeasuredHeight());
        }
    }
    
    public void onRestoreInstanceState(final Parcelable parcelable) {
        final SavedState savedState = (SavedState)parcelable;
        super.onRestoreInstanceState(((AbsSavedState)savedState).getSuperState());
        if (savedState.mShowDropdown) {
            final ViewTreeObserver viewTreeObserver = ((View)this).getViewTreeObserver();
            if (viewTreeObserver != null) {
                viewTreeObserver.addOnGlobalLayoutListener((ViewTreeObserver$OnGlobalLayoutListener)new ViewTreeObserver$OnGlobalLayoutListener(this) {
                    final AppCompatSpinner this$0;
                    
                    public void onGlobalLayout() {
                        if (!this.this$0.getInternalPopup().isShowing()) {
                            this.this$0.showPopup();
                        }
                        final ViewTreeObserver viewTreeObserver = ((View)this.this$0).getViewTreeObserver();
                        if (viewTreeObserver != null) {
                            Api16Impl.removeOnGlobalLayoutListener(viewTreeObserver, (ViewTreeObserver$OnGlobalLayoutListener)this);
                        }
                    }
                });
            }
        }
    }
    
    public Parcelable onSaveInstanceState() {
        final SavedState savedState = new SavedState(super.onSaveInstanceState());
        final SpinnerPopup mPopup = this.mPopup;
        savedState.mShowDropdown = (mPopup != null && mPopup.isShowing());
        return (Parcelable)savedState;
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        final ForwardingListener mForwardingListener = this.mForwardingListener;
        return (mForwardingListener != null && mForwardingListener.onTouch((View)this, motionEvent)) || super.onTouchEvent(motionEvent);
    }
    
    public boolean performClick() {
        final SpinnerPopup mPopup = this.mPopup;
        if (mPopup != null) {
            if (!mPopup.isShowing()) {
                this.showPopup();
            }
            return true;
        }
        return super.performClick();
    }
    
    public void setAdapter(final SpinnerAdapter spinnerAdapter) {
        if (!this.mPopupSet) {
            this.mTempAdapter = spinnerAdapter;
            return;
        }
        super.setAdapter(spinnerAdapter);
        if (this.mPopup != null) {
            Context context;
            if ((context = this.mPopupContext) == null) {
                context = ((View)this).getContext();
            }
            this.mPopup.setAdapter((ListAdapter)new DropDownAdapter(spinnerAdapter, context.getTheme()));
        }
    }
    
    public void setBackgroundDrawable(@Nullable final Drawable backgroundDrawable) {
        super.setBackgroundDrawable(backgroundDrawable);
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.onSetBackgroundDrawable(backgroundDrawable);
        }
    }
    
    public void setBackgroundResource(@DrawableRes final int backgroundResource) {
        super.setBackgroundResource(backgroundResource);
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.onSetBackgroundResource(backgroundResource);
        }
    }
    
    public void setDropDownHorizontalOffset(final int dropDownHorizontalOffset) {
        final SpinnerPopup mPopup = this.mPopup;
        if (mPopup != null) {
            mPopup.setHorizontalOriginalOffset(dropDownHorizontalOffset);
            this.mPopup.setHorizontalOffset(dropDownHorizontalOffset);
        }
        else {
            super.setDropDownHorizontalOffset(dropDownHorizontalOffset);
        }
    }
    
    public void setDropDownVerticalOffset(final int n) {
        final SpinnerPopup mPopup = this.mPopup;
        if (mPopup != null) {
            mPopup.setVerticalOffset(n);
        }
        else {
            super.setDropDownVerticalOffset(n);
        }
    }
    
    public void setDropDownWidth(final int n) {
        if (this.mPopup != null) {
            this.mDropDownWidth = n;
        }
        else {
            super.setDropDownWidth(n);
        }
    }
    
    public void setPopupBackgroundDrawable(final Drawable drawable) {
        final SpinnerPopup mPopup = this.mPopup;
        if (mPopup != null) {
            mPopup.setBackgroundDrawable(drawable);
        }
        else {
            super.setPopupBackgroundDrawable(drawable);
        }
    }
    
    public void setPopupBackgroundResource(@DrawableRes final int n) {
        this.setPopupBackgroundDrawable(AppCompatResources.getDrawable(this.getPopupContext(), n));
    }
    
    public void setPrompt(final CharSequence charSequence) {
        final SpinnerPopup mPopup = this.mPopup;
        if (mPopup != null) {
            mPopup.setPromptText(charSequence);
        }
        else {
            super.setPrompt(charSequence);
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void setSupportBackgroundTintList(@Nullable final ColorStateList supportBackgroundTintList) {
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.setSupportBackgroundTintList(supportBackgroundTintList);
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void setSupportBackgroundTintMode(@Nullable final PorterDuff$Mode supportBackgroundTintMode) {
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.setSupportBackgroundTintMode(supportBackgroundTintMode);
        }
    }
    
    void showPopup() {
        this.mPopup.show(Api17Impl.getTextDirection((View)this), Api17Impl.getTextAlignment((View)this));
    }
    
    @RequiresApi(16)
    private static final class Api16Impl
    {
        @DoNotInline
        static void removeOnGlobalLayoutListener(@NonNull final ViewTreeObserver viewTreeObserver, @Nullable final ViewTreeObserver$OnGlobalLayoutListener viewTreeObserver$OnGlobalLayoutListener) {
            viewTreeObserver.removeOnGlobalLayoutListener(viewTreeObserver$OnGlobalLayoutListener);
        }
    }
    
    @RequiresApi(17)
    private static final class Api17Impl
    {
        @DoNotInline
        static int getTextAlignment(@NonNull final View view) {
            return view.getTextAlignment();
        }
        
        @DoNotInline
        static int getTextDirection(@NonNull final View view) {
            return view.getTextDirection();
        }
        
        @DoNotInline
        static void setTextAlignment(@NonNull final View view, final int textAlignment) {
            view.setTextAlignment(textAlignment);
        }
        
        @DoNotInline
        static void setTextDirection(@NonNull final View view, final int textDirection) {
            view.setTextDirection(textDirection);
        }
    }
    
    @RequiresApi(23)
    private static final class Api23Impl
    {
        @DoNotInline
        static void setDropDownViewTheme(@NonNull final ThemedSpinnerAdapter themedSpinnerAdapter, @Nullable final Resources$Theme resources$Theme) {
            if (!ObjectsCompat.equals(o800o8O.\u3007080(themedSpinnerAdapter), resources$Theme)) {
                \u3007O888o0o.\u3007080(themedSpinnerAdapter, resources$Theme);
            }
        }
    }
    
    @VisibleForTesting
    class DialogPopup implements SpinnerPopup, DialogInterface$OnClickListener
    {
        private ListAdapter mListAdapter;
        @VisibleForTesting
        AlertDialog mPopup;
        private CharSequence mPrompt;
        final AppCompatSpinner this$0;
        
        DialogPopup(final AppCompatSpinner this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public void dismiss() {
            final AlertDialog mPopup = this.mPopup;
            if (mPopup != null) {
                mPopup.dismiss();
                this.mPopup = null;
            }
        }
        
        @Override
        public Drawable getBackground() {
            return null;
        }
        
        @Override
        public CharSequence getHintText() {
            return this.mPrompt;
        }
        
        @Override
        public int getHorizontalOffset() {
            return 0;
        }
        
        @Override
        public int getHorizontalOriginalOffset() {
            return 0;
        }
        
        @Override
        public int getVerticalOffset() {
            return 0;
        }
        
        @Override
        public boolean isShowing() {
            final AlertDialog mPopup = this.mPopup;
            return mPopup != null && mPopup.isShowing();
        }
        
        public void onClick(final DialogInterface dialogInterface, final int selection) {
            ((AdapterView)this.this$0).setSelection(selection);
            if (((AdapterView)this.this$0).getOnItemClickListener() != null) {
                ((AdapterView)this.this$0).performItemClick((View)null, selection, ((Adapter)this.mListAdapter).getItemId(selection));
            }
            this.dismiss();
        }
        
        @Override
        public void setAdapter(final ListAdapter mListAdapter) {
            this.mListAdapter = mListAdapter;
        }
        
        @Override
        public void setBackgroundDrawable(final Drawable drawable) {
        }
        
        @Override
        public void setHorizontalOffset(final int n) {
        }
        
        @Override
        public void setHorizontalOriginalOffset(final int n) {
        }
        
        @Override
        public void setPromptText(final CharSequence mPrompt) {
            this.mPrompt = mPrompt;
        }
        
        @Override
        public void setVerticalOffset(final int n) {
        }
        
        @Override
        public void show(final int n, final int n2) {
            if (this.mListAdapter == null) {
                return;
            }
            final AlertDialog.Builder builder = new AlertDialog.Builder(this.this$0.getPopupContext());
            final CharSequence mPrompt = this.mPrompt;
            if (mPrompt != null) {
                builder.setTitle(mPrompt);
            }
            final AlertDialog create = builder.setSingleChoiceItems(this.mListAdapter, ((AdapterView)this.this$0).getSelectedItemPosition(), (DialogInterface$OnClickListener)this).create();
            this.mPopup = create;
            final ListView listView = create.getListView();
            Api17Impl.setTextDirection((View)listView, n);
            Api17Impl.setTextAlignment((View)listView, n2);
            this.mPopup.show();
        }
    }
    
    @VisibleForTesting
    interface SpinnerPopup
    {
        void dismiss();
        
        Drawable getBackground();
        
        CharSequence getHintText();
        
        int getHorizontalOffset();
        
        int getHorizontalOriginalOffset();
        
        int getVerticalOffset();
        
        boolean isShowing();
        
        void setAdapter(final ListAdapter p0);
        
        void setBackgroundDrawable(final Drawable p0);
        
        void setHorizontalOffset(final int p0);
        
        void setHorizontalOriginalOffset(final int p0);
        
        void setPromptText(final CharSequence p0);
        
        void setVerticalOffset(final int p0);
        
        void show(final int p0, final int p1);
    }
    
    private static class DropDownAdapter implements ListAdapter, SpinnerAdapter
    {
        private SpinnerAdapter mAdapter;
        private ListAdapter mListAdapter;
        
        public DropDownAdapter(@Nullable final SpinnerAdapter mAdapter, @Nullable final Resources$Theme dropDownViewTheme) {
            this.mAdapter = mAdapter;
            if (mAdapter instanceof ListAdapter) {
                this.mListAdapter = (ListAdapter)mAdapter;
            }
            if (dropDownViewTheme != null) {
                if (Build$VERSION.SDK_INT >= 23 && mAdapter instanceof ThemedSpinnerAdapter) {
                    Api23Impl.setDropDownViewTheme((ThemedSpinnerAdapter)mAdapter, dropDownViewTheme);
                }
                else if (mAdapter instanceof androidx.appcompat.widget.ThemedSpinnerAdapter) {
                    final androidx.appcompat.widget.ThemedSpinnerAdapter themedSpinnerAdapter = (androidx.appcompat.widget.ThemedSpinnerAdapter)mAdapter;
                    if (themedSpinnerAdapter.getDropDownViewTheme() == null) {
                        themedSpinnerAdapter.setDropDownViewTheme(dropDownViewTheme);
                    }
                }
            }
        }
        
        public boolean areAllItemsEnabled() {
            final ListAdapter mListAdapter = this.mListAdapter;
            return mListAdapter == null || mListAdapter.areAllItemsEnabled();
        }
        
        public int getCount() {
            final SpinnerAdapter mAdapter = this.mAdapter;
            int count;
            if (mAdapter == null) {
                count = 0;
            }
            else {
                count = ((Adapter)mAdapter).getCount();
            }
            return count;
        }
        
        public View getDropDownView(final int n, View dropDownView, final ViewGroup viewGroup) {
            final SpinnerAdapter mAdapter = this.mAdapter;
            if (mAdapter == null) {
                dropDownView = null;
            }
            else {
                dropDownView = mAdapter.getDropDownView(n, dropDownView, viewGroup);
            }
            return dropDownView;
        }
        
        public Object getItem(final int n) {
            final SpinnerAdapter mAdapter = this.mAdapter;
            Object item;
            if (mAdapter == null) {
                item = null;
            }
            else {
                item = ((Adapter)mAdapter).getItem(n);
            }
            return item;
        }
        
        public long getItemId(final int n) {
            final SpinnerAdapter mAdapter = this.mAdapter;
            long itemId;
            if (mAdapter == null) {
                itemId = -1L;
            }
            else {
                itemId = ((Adapter)mAdapter).getItemId(n);
            }
            return itemId;
        }
        
        public int getItemViewType(final int n) {
            return 0;
        }
        
        public View getView(final int n, final View view, final ViewGroup viewGroup) {
            return this.getDropDownView(n, view, viewGroup);
        }
        
        public int getViewTypeCount() {
            return 1;
        }
        
        public boolean hasStableIds() {
            final SpinnerAdapter mAdapter = this.mAdapter;
            return mAdapter != null && ((Adapter)mAdapter).hasStableIds();
        }
        
        public boolean isEmpty() {
            return this.getCount() == 0;
        }
        
        public boolean isEnabled(final int n) {
            final ListAdapter mListAdapter = this.mListAdapter;
            return mListAdapter == null || mListAdapter.isEnabled(n);
        }
        
        public void registerDataSetObserver(final DataSetObserver dataSetObserver) {
            final SpinnerAdapter mAdapter = this.mAdapter;
            if (mAdapter != null) {
                ((Adapter)mAdapter).registerDataSetObserver(dataSetObserver);
            }
        }
        
        public void unregisterDataSetObserver(final DataSetObserver dataSetObserver) {
            final SpinnerAdapter mAdapter = this.mAdapter;
            if (mAdapter != null) {
                ((Adapter)mAdapter).unregisterDataSetObserver(dataSetObserver);
            }
        }
    }
    
    @VisibleForTesting
    class DropdownPopup extends ListPopupWindow implements SpinnerPopup
    {
        ListAdapter mAdapter;
        private CharSequence mHintText;
        private int mOriginalHorizontalOffset;
        private final Rect mVisibleRect;
        final AppCompatSpinner this$0;
        
        public DropdownPopup(final AppCompatSpinner appCompatSpinner, final Context context, final AttributeSet set, final int n) {
            this.this$0 = appCompatSpinner;
            super(context, set, n);
            this.mVisibleRect = new Rect();
            this.setAnchorView((View)appCompatSpinner);
            this.setModal(true);
            this.setPromptPosition(0);
            this.setOnItemClickListener((AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener(this, appCompatSpinner) {
                final DropdownPopup this$1;
                final AppCompatSpinner val$this$0;
                
                public void onItemClick(final AdapterView<?> adapterView, final View view, final int selection, final long n) {
                    ((AdapterView)this.this$1.this$0).setSelection(selection);
                    if (((AdapterView)this.this$1.this$0).getOnItemClickListener() != null) {
                        final DropdownPopup this$1 = this.this$1;
                        ((AdapterView)this$1.this$0).performItemClick(view, selection, ((Adapter)this$1.mAdapter).getItemId(selection));
                    }
                    this.this$1.dismiss();
                }
            });
        }
        
        void computeContentWidth() {
            final Drawable background = this.getBackground();
            int right;
            if (background != null) {
                background.getPadding(this.this$0.mTempRect);
                if (ViewUtils.isLayoutRtl((View)this.this$0)) {
                    right = this.this$0.mTempRect.right;
                }
                else {
                    right = -this.this$0.mTempRect.left;
                }
            }
            else {
                final Rect mTempRect = this.this$0.mTempRect;
                mTempRect.right = 0;
                mTempRect.left = 0;
                right = 0;
            }
            final int paddingLeft = ((View)this.this$0).getPaddingLeft();
            final int paddingRight = ((View)this.this$0).getPaddingRight();
            final int width = ((View)this.this$0).getWidth();
            final AppCompatSpinner this$0 = this.this$0;
            final int mDropDownWidth = this$0.mDropDownWidth;
            if (mDropDownWidth == -2) {
                final int compatMeasureContentWidth = this$0.compatMeasureContentWidth((SpinnerAdapter)this.mAdapter, this.getBackground());
                final int widthPixels = ((View)this.this$0).getContext().getResources().getDisplayMetrics().widthPixels;
                final Rect mTempRect2 = this.this$0.mTempRect;
                final int n = widthPixels - mTempRect2.left - mTempRect2.right;
                int a;
                if ((a = compatMeasureContentWidth) > n) {
                    a = n;
                }
                this.setContentWidth(Math.max(a, width - paddingLeft - paddingRight));
            }
            else if (mDropDownWidth == -1) {
                this.setContentWidth(width - paddingLeft - paddingRight);
            }
            else {
                this.setContentWidth(mDropDownWidth);
            }
            int horizontalOffset;
            if (ViewUtils.isLayoutRtl((View)this.this$0)) {
                horizontalOffset = right + (width - paddingRight - this.getWidth() - this.getHorizontalOriginalOffset());
            }
            else {
                horizontalOffset = right + (paddingLeft + this.getHorizontalOriginalOffset());
            }
            this.setHorizontalOffset(horizontalOffset);
        }
        
        @Override
        public CharSequence getHintText() {
            return this.mHintText;
        }
        
        @Override
        public int getHorizontalOriginalOffset() {
            return this.mOriginalHorizontalOffset;
        }
        
        boolean isVisibleToUser(final View view) {
            return ViewCompat.isAttachedToWindow(view) && view.getGlobalVisibleRect(this.mVisibleRect);
        }
        
        @Override
        public void setAdapter(final ListAdapter listAdapter) {
            super.setAdapter(listAdapter);
            this.mAdapter = listAdapter;
        }
        
        @Override
        public void setHorizontalOriginalOffset(final int mOriginalHorizontalOffset) {
            this.mOriginalHorizontalOffset = mOriginalHorizontalOffset;
        }
        
        @Override
        public void setPromptText(final CharSequence mHintText) {
            this.mHintText = mHintText;
        }
        
        @Override
        public void show(final int n, final int n2) {
            final boolean showing = this.isShowing();
            this.computeContentWidth();
            this.setInputMethodMode(2);
            super.show();
            final ListView listView = this.getListView();
            ((AbsListView)listView).setChoiceMode(1);
            Api17Impl.setTextDirection((View)listView, n);
            Api17Impl.setTextAlignment((View)listView, n2);
            this.setSelection(((AdapterView)this.this$0).getSelectedItemPosition());
            if (showing) {
                return;
            }
            final ViewTreeObserver viewTreeObserver = ((View)this.this$0).getViewTreeObserver();
            if (viewTreeObserver != null) {
                final ViewTreeObserver$OnGlobalLayoutListener viewTreeObserver$OnGlobalLayoutListener = (ViewTreeObserver$OnGlobalLayoutListener)new ViewTreeObserver$OnGlobalLayoutListener(this) {
                    final DropdownPopup this$1;
                    
                    public void onGlobalLayout() {
                        final DropdownPopup this$1 = this.this$1;
                        if (!this$1.isVisibleToUser((View)this$1.this$0)) {
                            this.this$1.dismiss();
                        }
                        else {
                            this.this$1.computeContentWidth();
                            this.this$1.show();
                        }
                    }
                };
                viewTreeObserver.addOnGlobalLayoutListener((ViewTreeObserver$OnGlobalLayoutListener)viewTreeObserver$OnGlobalLayoutListener);
                this.setOnDismissListener((PopupWindow$OnDismissListener)new PopupWindow$OnDismissListener(this, viewTreeObserver$OnGlobalLayoutListener) {
                    final DropdownPopup this$1;
                    final ViewTreeObserver$OnGlobalLayoutListener val$layoutListener;
                    
                    public void onDismiss() {
                        final ViewTreeObserver viewTreeObserver = ((View)this.this$1.this$0).getViewTreeObserver();
                        if (viewTreeObserver != null) {
                            viewTreeObserver.removeGlobalOnLayoutListener(this.val$layoutListener);
                        }
                    }
                });
            }
        }
    }
    
    static class SavedState extends View$BaseSavedState
    {
        public static final Parcelable$Creator<SavedState> CREATOR;
        boolean mShowDropdown;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$Creator<SavedState>() {
                public SavedState createFromParcel(final Parcel parcel) {
                    return new SavedState(parcel);
                }
                
                public SavedState[] newArray(final int n) {
                    return new SavedState[n];
                }
            };
        }
        
        SavedState(final Parcel parcel) {
            super(parcel);
            this.mShowDropdown = (parcel.readByte() != 0);
        }
        
        SavedState(final Parcelable parcelable) {
            super(parcelable);
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeByte((byte)(byte)(this.mShowDropdown ? 1 : 0));
        }
    }
}

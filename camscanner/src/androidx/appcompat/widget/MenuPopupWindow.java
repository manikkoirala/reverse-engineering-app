// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.widget.AbsListView;
import android.widget.AdapterView;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.view.View;
import androidx.appcompat.view.menu.ListMenuItemView;
import android.view.KeyEvent;
import android.widget.ListAdapter;
import androidx.appcompat.view.menu.MenuAdapter;
import android.widget.HeaderViewListAdapter;
import android.view.MotionEvent;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.transition.Transition;
import android.view.MenuItem;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import androidx.annotation.NonNull;
import android.content.Context;
import android.widget.PopupWindow;
import android.os.Build$VERSION;
import java.lang.reflect.Method;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public class MenuPopupWindow extends ListPopupWindow implements MenuItemHoverListener
{
    private static final String TAG = "MenuPopupWindow";
    private static Method sSetTouchModalMethod;
    private MenuItemHoverListener mHoverListener;
    
    static {
        try {
            if (Build$VERSION.SDK_INT <= 28) {
                MenuPopupWindow.sSetTouchModalMethod = PopupWindow.class.getDeclaredMethod("setTouchModal", Boolean.TYPE);
            }
        }
        catch (final NoSuchMethodException ex) {}
    }
    
    public MenuPopupWindow(@NonNull final Context context, @Nullable final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
    }
    
    @NonNull
    @Override
    DropDownListView createDropDownListView(final Context context, final boolean b) {
        final MenuDropDownListView menuDropDownListView = new MenuDropDownListView(context, b);
        menuDropDownListView.setHoverListener(this);
        return menuDropDownListView;
    }
    
    @Override
    public void onItemHoverEnter(@NonNull final MenuBuilder menuBuilder, @NonNull final MenuItem menuItem) {
        final MenuItemHoverListener mHoverListener = this.mHoverListener;
        if (mHoverListener != null) {
            mHoverListener.onItemHoverEnter(menuBuilder, menuItem);
        }
    }
    
    @Override
    public void onItemHoverExit(@NonNull final MenuBuilder menuBuilder, @NonNull final MenuItem menuItem) {
        final MenuItemHoverListener mHoverListener = this.mHoverListener;
        if (mHoverListener != null) {
            mHoverListener.onItemHoverExit(menuBuilder, menuItem);
        }
    }
    
    public void setEnterTransition(final Object o) {
        if (Build$VERSION.SDK_INT >= 23) {
            Api23Impl.setEnterTransition(super.mPopup, (Transition)o);
        }
    }
    
    public void setExitTransition(final Object o) {
        if (Build$VERSION.SDK_INT >= 23) {
            Api23Impl.setExitTransition(super.mPopup, (Transition)o);
        }
    }
    
    public void setHoverListener(final MenuItemHoverListener mHoverListener) {
        this.mHoverListener = mHoverListener;
    }
    
    public void setTouchModal(final boolean b) {
        Label_0039: {
            if (Build$VERSION.SDK_INT > 28) {
                break Label_0039;
            }
            final Method sSetTouchModalMethod = MenuPopupWindow.sSetTouchModalMethod;
            if (sSetTouchModalMethod == null) {
                return;
            }
            try {
                sSetTouchModalMethod.invoke(super.mPopup, b);
                return;
                Api29Impl.setTouchModal(super.mPopup, b);
            }
            catch (final Exception ex) {}
        }
    }
    
    @RequiresApi(23)
    static class Api23Impl
    {
        private Api23Impl() {
        }
        
        @DoNotInline
        static void setEnterTransition(final PopupWindow popupWindow, final Transition transition) {
            \u300780.\u3007080(popupWindow, transition);
        }
        
        @DoNotInline
        static void setExitTransition(final PopupWindow popupWindow, final Transition transition) {
            O000.\u3007080(popupWindow, transition);
        }
    }
    
    @RequiresApi(29)
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        @DoNotInline
        static void setTouchModal(final PopupWindow popupWindow, final boolean b) {
            Ooo.\u3007080(popupWindow, b);
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static class MenuDropDownListView extends DropDownListView
    {
        final int mAdvanceKey;
        private MenuItemHoverListener mHoverListener;
        private MenuItem mHoveredMenuItem;
        final int mRetreatKey;
        
        public MenuDropDownListView(final Context context, final boolean b) {
            super(context, b);
            if (1 == Api17Impl.getLayoutDirection(context.getResources().getConfiguration())) {
                this.mAdvanceKey = 21;
                this.mRetreatKey = 22;
            }
            else {
                this.mAdvanceKey = 22;
                this.mRetreatKey = 21;
            }
        }
        
        public void clearSelection() {
            ((AdapterView)this).setSelection(-1);
        }
        
        @Override
        public boolean onHoverEvent(final MotionEvent motionEvent) {
            if (this.mHoverListener != null) {
                final ListAdapter adapter = this.getAdapter();
                int headersCount;
                MenuAdapter menuAdapter;
                if (adapter instanceof HeaderViewListAdapter) {
                    final HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter)adapter;
                    headersCount = headerViewListAdapter.getHeadersCount();
                    menuAdapter = (MenuAdapter)headerViewListAdapter.getWrappedAdapter();
                }
                else {
                    menuAdapter = (MenuAdapter)adapter;
                    headersCount = 0;
                }
                Object item = null;
                Label_0116: {
                    if (motionEvent.getAction() != 10) {
                        final int pointToPosition = ((AbsListView)this).pointToPosition((int)motionEvent.getX(), (int)motionEvent.getY());
                        if (pointToPosition != -1) {
                            final int n = pointToPosition - headersCount;
                            if (n >= 0 && n < menuAdapter.getCount()) {
                                item = menuAdapter.getItem(n);
                                break Label_0116;
                            }
                        }
                    }
                    item = null;
                }
                final MenuItem mHoveredMenuItem = this.mHoveredMenuItem;
                if (mHoveredMenuItem != item) {
                    final MenuBuilder adapterMenu = menuAdapter.getAdapterMenu();
                    if (mHoveredMenuItem != null) {
                        this.mHoverListener.onItemHoverExit(adapterMenu, mHoveredMenuItem);
                    }
                    if ((this.mHoveredMenuItem = (MenuItem)item) != null) {
                        this.mHoverListener.onItemHoverEnter(adapterMenu, (MenuItem)item);
                    }
                }
            }
            return super.onHoverEvent(motionEvent);
        }
        
        public boolean onKeyDown(final int n, final KeyEvent keyEvent) {
            final ListMenuItemView listMenuItemView = (ListMenuItemView)((AdapterView)this).getSelectedView();
            if (listMenuItemView != null && n == this.mAdvanceKey) {
                if (((View)listMenuItemView).isEnabled() && listMenuItemView.getItemData().hasSubMenu()) {
                    ((AdapterView)this).performItemClick((View)listMenuItemView, ((AdapterView)this).getSelectedItemPosition(), ((AdapterView)this).getSelectedItemId());
                }
                return true;
            }
            if (listMenuItemView != null && n == this.mRetreatKey) {
                ((AdapterView)this).setSelection(-1);
                final ListAdapter adapter = this.getAdapter();
                MenuAdapter menuAdapter;
                if (adapter instanceof HeaderViewListAdapter) {
                    menuAdapter = (MenuAdapter)((HeaderViewListAdapter)adapter).getWrappedAdapter();
                }
                else {
                    menuAdapter = (MenuAdapter)adapter;
                }
                menuAdapter.getAdapterMenu().close(false);
                return true;
            }
            return super.onKeyDown(n, keyEvent);
        }
        
        public void setHoverListener(final MenuItemHoverListener mHoverListener) {
            this.mHoverListener = mHoverListener;
        }
        
        @RequiresApi(17)
        static class Api17Impl
        {
            private Api17Impl() {
            }
            
            @DoNotInline
            static int getLayoutDirection(final Configuration configuration) {
                return configuration.getLayoutDirection();
            }
        }
    }
}

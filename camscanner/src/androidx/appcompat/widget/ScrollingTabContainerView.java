// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.view.accessibility.AccessibilityRecord;
import android.widget.AbsSpinner;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.ViewParent;
import android.text.TextUtils$TruncateAt;
import android.text.TextUtils;
import android.widget.LinearLayout$LayoutParams;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityEvent;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.view.View$MeasureSpec;
import android.widget.AdapterView;
import android.content.res.Configuration;
import android.view.View$OnClickListener;
import android.widget.AbsListView$LayoutParams;
import android.graphics.drawable.Drawable;
import android.animation.Animator$AnimatorListener;
import android.animation.TimeInterpolator;
import androidx.appcompat.app.ActionBar;
import android.widget.SpinnerAdapter;
import android.util.AttributeSet;
import androidx.appcompat.R;
import android.view.View;
import android.view.ViewGroup$LayoutParams;
import androidx.appcompat.view.ActionBarPolicy;
import androidx.annotation.NonNull;
import android.content.Context;
import android.view.animation.DecelerateInterpolator;
import android.view.ViewPropertyAnimator;
import android.widget.Spinner;
import android.view.animation.Interpolator;
import androidx.annotation.RestrictTo;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.HorizontalScrollView;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public class ScrollingTabContainerView extends HorizontalScrollView implements AdapterView$OnItemSelectedListener
{
    private static final int FADE_DURATION = 200;
    private static final String TAG = "ScrollingTabContainerView";
    private static final Interpolator sAlphaInterpolator;
    private boolean mAllowCollapse;
    private int mContentHeight;
    int mMaxTabWidth;
    private int mSelectedTabIndex;
    int mStackedTabMaxWidth;
    private TabClickListener mTabClickListener;
    LinearLayoutCompat mTabLayout;
    Runnable mTabSelector;
    private Spinner mTabSpinner;
    protected final VisibilityAnimListener mVisAnimListener;
    protected ViewPropertyAnimator mVisibilityAnim;
    
    static {
        sAlphaInterpolator = (Interpolator)new DecelerateInterpolator();
    }
    
    public ScrollingTabContainerView(@NonNull final Context context) {
        super(context);
        this.mVisAnimListener = new VisibilityAnimListener();
        ((View)this).setHorizontalScrollBarEnabled(false);
        final ActionBarPolicy value = ActionBarPolicy.get(context);
        this.setContentHeight(value.getTabContainerHeight());
        this.mStackedTabMaxWidth = value.getStackedTabMaxWidth();
        ((ViewGroup)this).addView((View)(this.mTabLayout = this.createTabLayout()), new ViewGroup$LayoutParams(-2, -1));
    }
    
    private Spinner createSpinner() {
        final AppCompatSpinner appCompatSpinner = new AppCompatSpinner(((View)this).getContext(), null, R.attr.actionDropDownStyle);
        ((View)appCompatSpinner).setLayoutParams((ViewGroup$LayoutParams)new LinearLayoutCompat.LayoutParams(-2, -1));
        ((AdapterView)appCompatSpinner).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)this);
        return appCompatSpinner;
    }
    
    private LinearLayoutCompat createTabLayout() {
        final LinearLayoutCompat linearLayoutCompat = new LinearLayoutCompat(((View)this).getContext(), null, R.attr.actionBarTabBarStyle);
        linearLayoutCompat.setMeasureWithLargestChildEnabled(true);
        linearLayoutCompat.setGravity(17);
        ((View)linearLayoutCompat).setLayoutParams((ViewGroup$LayoutParams)new LinearLayoutCompat.LayoutParams(-2, -1));
        return linearLayoutCompat;
    }
    
    private boolean isCollapsed() {
        final Spinner mTabSpinner = this.mTabSpinner;
        return mTabSpinner != null && ((View)mTabSpinner).getParent() == this;
    }
    
    private void performCollapse() {
        if (this.isCollapsed()) {
            return;
        }
        if (this.mTabSpinner == null) {
            this.mTabSpinner = this.createSpinner();
        }
        ((ViewGroup)this).removeView((View)this.mTabLayout);
        ((ViewGroup)this).addView((View)this.mTabSpinner, new ViewGroup$LayoutParams(-2, -1));
        if (((AbsSpinner)this.mTabSpinner).getAdapter() == null) {
            this.mTabSpinner.setAdapter((SpinnerAdapter)new TabAdapter());
        }
        final Runnable mTabSelector = this.mTabSelector;
        if (mTabSelector != null) {
            ((View)this).removeCallbacks(mTabSelector);
            this.mTabSelector = null;
        }
        ((AdapterView)this.mTabSpinner).setSelection(this.mSelectedTabIndex);
    }
    
    private boolean performExpand() {
        if (!this.isCollapsed()) {
            return false;
        }
        ((ViewGroup)this).removeView((View)this.mTabSpinner);
        ((ViewGroup)this).addView((View)this.mTabLayout, new ViewGroup$LayoutParams(-2, -1));
        this.setTabSelected(((AdapterView)this.mTabSpinner).getSelectedItemPosition());
        return false;
    }
    
    public void addTab(final ActionBar.Tab tab, final int n, final boolean b) {
        final TabView tabView = this.createTabView(tab, false);
        this.mTabLayout.addView((View)tabView, n, (ViewGroup$LayoutParams)new LinearLayoutCompat.LayoutParams(0, -1, 1.0f));
        final Spinner mTabSpinner = this.mTabSpinner;
        if (mTabSpinner != null) {
            ((TabAdapter)((AbsSpinner)mTabSpinner).getAdapter()).notifyDataSetChanged();
        }
        if (b) {
            tabView.setSelected(true);
        }
        if (this.mAllowCollapse) {
            ((View)this).requestLayout();
        }
    }
    
    public void addTab(final ActionBar.Tab tab, final boolean b) {
        final TabView tabView = this.createTabView(tab, false);
        this.mTabLayout.addView((View)tabView, (ViewGroup$LayoutParams)new LinearLayoutCompat.LayoutParams(0, -1, 1.0f));
        final Spinner mTabSpinner = this.mTabSpinner;
        if (mTabSpinner != null) {
            ((TabAdapter)((AbsSpinner)mTabSpinner).getAdapter()).notifyDataSetChanged();
        }
        if (b) {
            tabView.setSelected(true);
        }
        if (this.mAllowCollapse) {
            ((View)this).requestLayout();
        }
    }
    
    public void animateToTab(final int n) {
        final View child = this.mTabLayout.getChildAt(n);
        final Runnable mTabSelector = this.mTabSelector;
        if (mTabSelector != null) {
            ((View)this).removeCallbacks(mTabSelector);
        }
        ((View)this).post(this.mTabSelector = new Runnable(this, child) {
            final ScrollingTabContainerView this$0;
            final View val$tabView;
            
            @Override
            public void run() {
                this.this$0.smoothScrollTo(this.val$tabView.getLeft() - (((View)this.this$0).getWidth() - this.val$tabView.getWidth()) / 2, 0);
                this.this$0.mTabSelector = null;
            }
        });
    }
    
    public void animateToVisibility(final int n) {
        final ViewPropertyAnimator mVisibilityAnim = this.mVisibilityAnim;
        if (mVisibilityAnim != null) {
            mVisibilityAnim.cancel();
        }
        if (n == 0) {
            if (((View)this).getVisibility() != 0) {
                ((View)this).setAlpha(0.0f);
            }
            final ViewPropertyAnimator alpha = ((View)this).animate().alpha(1.0f);
            alpha.setDuration(200L);
            alpha.setInterpolator((TimeInterpolator)ScrollingTabContainerView.sAlphaInterpolator);
            alpha.setListener((Animator$AnimatorListener)this.mVisAnimListener.withFinalVisibility(alpha, n));
            alpha.start();
        }
        else {
            final ViewPropertyAnimator alpha2 = ((View)this).animate().alpha(0.0f);
            alpha2.setDuration(200L);
            alpha2.setInterpolator((TimeInterpolator)ScrollingTabContainerView.sAlphaInterpolator);
            alpha2.setListener((Animator$AnimatorListener)this.mVisAnimListener.withFinalVisibility(alpha2, n));
            alpha2.start();
        }
    }
    
    TabView createTabView(final ActionBar.Tab tab, final boolean b) {
        final TabView tabView = new TabView(((View)this).getContext(), tab, b);
        if (b) {
            ((View)tabView).setBackgroundDrawable((Drawable)null);
            ((View)tabView).setLayoutParams((ViewGroup$LayoutParams)new AbsListView$LayoutParams(-1, this.mContentHeight));
        }
        else {
            ((View)tabView).setFocusable(true);
            if (this.mTabClickListener == null) {
                this.mTabClickListener = new TabClickListener();
            }
            ((View)tabView).setOnClickListener((View$OnClickListener)this.mTabClickListener);
        }
        return tabView;
    }
    
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        final Runnable mTabSelector = this.mTabSelector;
        if (mTabSelector != null) {
            ((View)this).post(mTabSelector);
        }
    }
    
    protected void onConfigurationChanged(final Configuration configuration) {
        super.onConfigurationChanged(configuration);
        final ActionBarPolicy value = ActionBarPolicy.get(((View)this).getContext());
        this.setContentHeight(value.getTabContainerHeight());
        this.mStackedTabMaxWidth = value.getStackedTabMaxWidth();
    }
    
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        final Runnable mTabSelector = this.mTabSelector;
        if (mTabSelector != null) {
            ((View)this).removeCallbacks(mTabSelector);
        }
    }
    
    public void onItemSelected(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
        ((TabView)view).getTab().select();
    }
    
    public void onMeasure(int measuredWidth, int measuredWidth2) {
        final int mode = View$MeasureSpec.getMode(measuredWidth);
        measuredWidth2 = 1;
        final boolean fillViewport = mode == 1073741824;
        this.setFillViewport(fillViewport);
        final int childCount = this.mTabLayout.getChildCount();
        if (childCount > 1 && (mode == 1073741824 || mode == Integer.MIN_VALUE)) {
            if (childCount > 2) {
                this.mMaxTabWidth = (int)(View$MeasureSpec.getSize(measuredWidth) * 0.4f);
            }
            else {
                this.mMaxTabWidth = View$MeasureSpec.getSize(measuredWidth) / 2;
            }
            this.mMaxTabWidth = Math.min(this.mMaxTabWidth, this.mStackedTabMaxWidth);
        }
        else {
            this.mMaxTabWidth = -1;
        }
        final int measureSpec = View$MeasureSpec.makeMeasureSpec(this.mContentHeight, 1073741824);
        if (fillViewport || !this.mAllowCollapse) {
            measuredWidth2 = 0;
        }
        if (measuredWidth2 != 0) {
            ((View)this.mTabLayout).measure(0, measureSpec);
            if (((View)this.mTabLayout).getMeasuredWidth() > View$MeasureSpec.getSize(measuredWidth)) {
                this.performCollapse();
            }
            else {
                this.performExpand();
            }
        }
        else {
            this.performExpand();
        }
        measuredWidth2 = ((View)this).getMeasuredWidth();
        super.onMeasure(measuredWidth, measureSpec);
        measuredWidth = ((View)this).getMeasuredWidth();
        if (fillViewport && measuredWidth2 != measuredWidth) {
            this.setTabSelected(this.mSelectedTabIndex);
        }
    }
    
    public void onNothingSelected(final AdapterView<?> adapterView) {
    }
    
    public void removeAllTabs() {
        this.mTabLayout.removeAllViews();
        final Spinner mTabSpinner = this.mTabSpinner;
        if (mTabSpinner != null) {
            ((TabAdapter)((AbsSpinner)mTabSpinner).getAdapter()).notifyDataSetChanged();
        }
        if (this.mAllowCollapse) {
            ((View)this).requestLayout();
        }
    }
    
    public void removeTabAt(final int n) {
        this.mTabLayout.removeViewAt(n);
        final Spinner mTabSpinner = this.mTabSpinner;
        if (mTabSpinner != null) {
            ((TabAdapter)((AbsSpinner)mTabSpinner).getAdapter()).notifyDataSetChanged();
        }
        if (this.mAllowCollapse) {
            ((View)this).requestLayout();
        }
    }
    
    public void setAllowCollapse(final boolean mAllowCollapse) {
        this.mAllowCollapse = mAllowCollapse;
    }
    
    public void setContentHeight(final int mContentHeight) {
        this.mContentHeight = mContentHeight;
        ((View)this).requestLayout();
    }
    
    public void setTabSelected(final int n) {
        this.mSelectedTabIndex = n;
        for (int childCount = this.mTabLayout.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = this.mTabLayout.getChildAt(i);
            final boolean selected = i == n;
            child.setSelected(selected);
            if (selected) {
                this.animateToTab(n);
            }
        }
        final Spinner mTabSpinner = this.mTabSpinner;
        if (mTabSpinner != null && n >= 0) {
            ((AdapterView)mTabSpinner).setSelection(n);
        }
    }
    
    public void updateTab(final int n) {
        ((TabView)this.mTabLayout.getChildAt(n)).update();
        final Spinner mTabSpinner = this.mTabSpinner;
        if (mTabSpinner != null) {
            ((TabAdapter)((AbsSpinner)mTabSpinner).getAdapter()).notifyDataSetChanged();
        }
        if (this.mAllowCollapse) {
            ((View)this).requestLayout();
        }
    }
    
    private class TabAdapter extends BaseAdapter
    {
        final ScrollingTabContainerView this$0;
        
        TabAdapter(final ScrollingTabContainerView this$0) {
            this.this$0 = this$0;
        }
        
        public int getCount() {
            return this.this$0.mTabLayout.getChildCount();
        }
        
        public Object getItem(final int n) {
            return ((TabView)this.this$0.mTabLayout.getChildAt(n)).getTab();
        }
        
        public long getItemId(final int n) {
            return n;
        }
        
        public View getView(final int n, View tabView, final ViewGroup viewGroup) {
            if (tabView == null) {
                tabView = (View)this.this$0.createTabView((ActionBar.Tab)this.getItem(n), true);
            }
            else {
                ((TabView)tabView).bindTab((ActionBar.Tab)this.getItem(n));
            }
            return tabView;
        }
    }
    
    private class TabClickListener implements View$OnClickListener
    {
        final ScrollingTabContainerView this$0;
        
        TabClickListener(final ScrollingTabContainerView this$0) {
            this.this$0 = this$0;
        }
        
        public void onClick(final View view) {
            ((TabView)view).getTab().select();
            for (int childCount = this.this$0.mTabLayout.getChildCount(), i = 0; i < childCount; ++i) {
                final View child = this.this$0.mTabLayout.getChildAt(i);
                child.setSelected(child == view);
            }
        }
    }
    
    private class TabView extends LinearLayout
    {
        private static final String ACCESSIBILITY_CLASS_NAME = "androidx.appcompat.app.ActionBar$Tab";
        private final int[] BG_ATTRS;
        private View mCustomView;
        private ImageView mIconView;
        private ActionBar.Tab mTab;
        private TextView mTextView;
        final ScrollingTabContainerView this$0;
        
        public TabView(final ScrollingTabContainerView this$0, final Context context, final ActionBar.Tab mTab, final boolean b) {
            this.this$0 = this$0;
            final int actionBarTabStyle = R.attr.actionBarTabStyle;
            super(context, (AttributeSet)null, actionBarTabStyle);
            final int[] bg_ATTRS = { 16842964 };
            this.BG_ATTRS = bg_ATTRS;
            this.mTab = mTab;
            final TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(context, null, bg_ATTRS, actionBarTabStyle, 0);
            if (obtainStyledAttributes.hasValue(0)) {
                ((View)this).setBackgroundDrawable(obtainStyledAttributes.getDrawable(0));
            }
            obtainStyledAttributes.recycle();
            if (b) {
                this.setGravity(8388627);
            }
            this.update();
        }
        
        public void bindTab(final ActionBar.Tab mTab) {
            this.mTab = mTab;
            this.update();
        }
        
        public ActionBar.Tab getTab() {
            return this.mTab;
        }
        
        public void onInitializeAccessibilityEvent(final AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(accessibilityEvent);
            ((AccessibilityRecord)accessibilityEvent).setClassName((CharSequence)"androidx.appcompat.app.ActionBar$Tab");
        }
        
        public void onInitializeAccessibilityNodeInfo(final AccessibilityNodeInfo accessibilityNodeInfo) {
            super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
            accessibilityNodeInfo.setClassName((CharSequence)"androidx.appcompat.app.ActionBar$Tab");
        }
        
        public void onMeasure(int mMaxTabWidth, final int n) {
            super.onMeasure(mMaxTabWidth, n);
            if (this.this$0.mMaxTabWidth > 0) {
                final int measuredWidth = ((View)this).getMeasuredWidth();
                mMaxTabWidth = this.this$0.mMaxTabWidth;
                if (measuredWidth > mMaxTabWidth) {
                    super.onMeasure(View$MeasureSpec.makeMeasureSpec(mMaxTabWidth, 1073741824), n);
                }
            }
        }
        
        public void setSelected(final boolean selected) {
            final boolean b = ((View)this).isSelected() != selected;
            super.setSelected(selected);
            if (b && selected) {
                ((View)this).sendAccessibilityEvent(4);
            }
        }
        
        public void update() {
            final ActionBar.Tab mTab = this.mTab;
            final View customView = mTab.getCustomView();
            CharSequence contentDescription = null;
            if (customView != null) {
                final ViewParent parent = customView.getParent();
                if (parent != this) {
                    if (parent != null) {
                        ((ViewGroup)parent).removeView(customView);
                    }
                    ((ViewGroup)this).addView(customView);
                }
                this.mCustomView = customView;
                final TextView mTextView = this.mTextView;
                if (mTextView != null) {
                    ((View)mTextView).setVisibility(8);
                }
                final ImageView mIconView = this.mIconView;
                if (mIconView != null) {
                    mIconView.setVisibility(8);
                    this.mIconView.setImageDrawable((Drawable)null);
                }
            }
            else {
                final View mCustomView = this.mCustomView;
                if (mCustomView != null) {
                    ((ViewGroup)this).removeView(mCustomView);
                    this.mCustomView = null;
                }
                final Drawable icon = mTab.getIcon();
                final CharSequence text = mTab.getText();
                if (icon != null) {
                    if (this.mIconView == null) {
                        final AppCompatImageView mIconView2 = new AppCompatImageView(((View)this).getContext());
                        final LinearLayout$LayoutParams layoutParams = new LinearLayout$LayoutParams(-2, -2);
                        layoutParams.gravity = 16;
                        ((View)mIconView2).setLayoutParams((ViewGroup$LayoutParams)layoutParams);
                        ((ViewGroup)this).addView((View)mIconView2, 0);
                        this.mIconView = mIconView2;
                    }
                    this.mIconView.setImageDrawable(icon);
                    this.mIconView.setVisibility(0);
                }
                else {
                    final ImageView mIconView3 = this.mIconView;
                    if (mIconView3 != null) {
                        mIconView3.setVisibility(8);
                        this.mIconView.setImageDrawable((Drawable)null);
                    }
                }
                final boolean b = TextUtils.isEmpty(text) ^ true;
                if (b) {
                    if (this.mTextView == null) {
                        final AppCompatTextView mTextView2 = new AppCompatTextView(((View)this).getContext(), null, R.attr.actionBarTabTextStyle);
                        mTextView2.setEllipsize(TextUtils$TruncateAt.END);
                        final LinearLayout$LayoutParams layoutParams2 = new LinearLayout$LayoutParams(-2, -2);
                        layoutParams2.gravity = 16;
                        ((View)mTextView2).setLayoutParams((ViewGroup$LayoutParams)layoutParams2);
                        ((ViewGroup)this).addView((View)mTextView2);
                        this.mTextView = mTextView2;
                    }
                    this.mTextView.setText(text);
                    ((View)this.mTextView).setVisibility(0);
                }
                else {
                    final TextView mTextView3 = this.mTextView;
                    if (mTextView3 != null) {
                        ((View)mTextView3).setVisibility(8);
                        this.mTextView.setText((CharSequence)null);
                    }
                }
                final ImageView mIconView4 = this.mIconView;
                if (mIconView4 != null) {
                    ((View)mIconView4).setContentDescription(mTab.getContentDescription());
                }
                if (!b) {
                    contentDescription = mTab.getContentDescription();
                }
                TooltipCompat.setTooltipText((View)this, contentDescription);
            }
        }
    }
    
    protected class VisibilityAnimListener extends AnimatorListenerAdapter
    {
        private boolean mCanceled;
        private int mFinalVisibility;
        final ScrollingTabContainerView this$0;
        
        protected VisibilityAnimListener(final ScrollingTabContainerView this$0) {
            this.this$0 = this$0;
            this.mCanceled = false;
        }
        
        public void onAnimationCancel(final Animator animator) {
            this.mCanceled = true;
        }
        
        public void onAnimationEnd(final Animator animator) {
            if (this.mCanceled) {
                return;
            }
            final ScrollingTabContainerView this$0 = this.this$0;
            this$0.mVisibilityAnim = null;
            ((View)this$0).setVisibility(this.mFinalVisibility);
        }
        
        public void onAnimationStart(final Animator animator) {
            ((View)this.this$0).setVisibility(0);
            this.mCanceled = false;
        }
        
        public VisibilityAnimListener withFinalVisibility(final ViewPropertyAnimator mVisibilityAnim, final int mFinalVisibility) {
            this.mFinalVisibility = mFinalVisibility;
            this.this$0.mVisibilityAnim = mVisibilityAnim;
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import androidx.core.graphics.TypefaceCompat;
import android.graphics.Typeface;
import androidx.annotation.Px;
import androidx.annotation.IntRange;
import android.text.InputFilter;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.annotation.DrawableRes;
import android.graphics.drawable.Drawable;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.EditorInfo;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import androidx.annotation.UiThread;
import androidx.annotation.RequiresApi;
import android.os.Build$VERSION;
import android.view.ActionMode$Callback;
import android.annotation.SuppressLint;
import androidx.annotation.RestrictTo;
import java.util.concurrent.ExecutionException;
import androidx.core.widget.TextViewCompat;
import android.view.textclassifier.TextClassifier;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import androidx.annotation.Nullable;
import androidx.core.text.PrecomputedTextCompat;
import java.util.concurrent.Future;
import androidx.annotation.NonNull;
import androidx.core.widget.AutoSizeableTextView;
import androidx.core.widget.TintableCompoundDrawablesView;
import androidx.core.view.TintableBackgroundView;
import android.widget.TextView;

public class AppCompatTextView extends TextView implements TintableBackgroundView, TintableCompoundDrawablesView, AutoSizeableTextView, EmojiCompatConfigurationView
{
    private final AppCompatBackgroundHelper mBackgroundTintHelper;
    @NonNull
    private AppCompatEmojiTextHelper mEmojiTextViewHelper;
    private boolean mIsSetTypefaceProcessing;
    @Nullable
    private Future<PrecomputedTextCompat> mPrecomputedTextFuture;
    @Nullable
    private SuperCaller mSuperCaller;
    private final AppCompatTextClassifierHelper mTextClassifierHelper;
    private final AppCompatTextHelper mTextHelper;
    
    public AppCompatTextView(@NonNull final Context context) {
        this(context, null);
    }
    
    public AppCompatTextView(@NonNull final Context context, @Nullable final AttributeSet set) {
        this(context, set, 16842884);
    }
    
    public AppCompatTextView(@NonNull final Context context, @Nullable final AttributeSet set, final int n) {
        super(TintContextWrapper.wrap(context), set, n);
        this.mIsSetTypefaceProcessing = false;
        this.mSuperCaller = null;
        ThemeUtils.checkAppCompatTheme((View)this, ((View)this).getContext());
        (this.mBackgroundTintHelper = new AppCompatBackgroundHelper((View)this)).loadFromAttributes(set, n);
        final AppCompatTextHelper mTextHelper = new AppCompatTextHelper(this);
        (this.mTextHelper = mTextHelper).loadFromAttributes(set, n);
        mTextHelper.applyCompoundDrawablesTints();
        this.mTextClassifierHelper = new AppCompatTextClassifierHelper(this);
        this.getEmojiTextViewHelper().loadFromAttributes(set, n);
    }
    
    static /* synthetic */ int access$001(final AppCompatTextView appCompatTextView) {
        return appCompatTextView.getAutoSizeMaxTextSize();
    }
    
    static /* synthetic */ void access$1001(final AppCompatTextView appCompatTextView, final int firstBaselineToTopHeight) {
        appCompatTextView.setFirstBaselineToTopHeight(firstBaselineToTopHeight);
    }
    
    static /* synthetic */ int access$101(final AppCompatTextView appCompatTextView) {
        return appCompatTextView.getAutoSizeMinTextSize();
    }
    
    static /* synthetic */ void access$1101(final AppCompatTextView appCompatTextView, final int lastBaselineToBottomHeight) {
        appCompatTextView.setLastBaselineToBottomHeight(lastBaselineToBottomHeight);
    }
    
    static /* synthetic */ int access$201(final AppCompatTextView appCompatTextView) {
        return appCompatTextView.getAutoSizeStepGranularity();
    }
    
    static /* synthetic */ int[] access$301(final AppCompatTextView appCompatTextView) {
        return appCompatTextView.getAutoSizeTextAvailableSizes();
    }
    
    static /* synthetic */ int access$401(final AppCompatTextView appCompatTextView) {
        return appCompatTextView.getAutoSizeTextType();
    }
    
    static /* synthetic */ TextClassifier access$501(final AppCompatTextView appCompatTextView) {
        return appCompatTextView.getTextClassifier();
    }
    
    static /* synthetic */ void access$601(final AppCompatTextView appCompatTextView, final int n, final int n2, final int n3, final int n4) {
        appCompatTextView.setAutoSizeTextTypeUniformWithConfiguration(n, n2, n3, n4);
    }
    
    static /* synthetic */ void access$701(final AppCompatTextView appCompatTextView, final int[] array, final int n) {
        appCompatTextView.setAutoSizeTextTypeUniformWithPresetSizes(array, n);
    }
    
    static /* synthetic */ void access$801(final AppCompatTextView appCompatTextView, final int autoSizeTextTypeWithDefaults) {
        appCompatTextView.setAutoSizeTextTypeWithDefaults(autoSizeTextTypeWithDefaults);
    }
    
    static /* synthetic */ void access$901(final AppCompatTextView appCompatTextView, final TextClassifier textClassifier) {
        appCompatTextView.setTextClassifier(textClassifier);
    }
    
    private void consumeTextFutureAndSetBlocking() {
        final Future<PrecomputedTextCompat> mPrecomputedTextFuture = this.mPrecomputedTextFuture;
        if (mPrecomputedTextFuture == null) {
            return;
        }
        try {
            this.mPrecomputedTextFuture = null;
            TextViewCompat.setPrecomputedText(this, mPrecomputedTextFuture.get());
        }
        catch (final InterruptedException | ExecutionException ex) {}
    }
    
    @NonNull
    private AppCompatEmojiTextHelper getEmojiTextViewHelper() {
        if (this.mEmojiTextViewHelper == null) {
            this.mEmojiTextViewHelper = new AppCompatEmojiTextHelper(this);
        }
        return this.mEmojiTextViewHelper;
    }
    
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.applySupportBackgroundTint();
        }
        final AppCompatTextHelper mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.applyCompoundDrawablesTints();
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public int getAutoSizeMaxTextSize() {
        if (ViewUtils.SDK_LEVEL_SUPPORTS_AUTOSIZE) {
            return this.getSuperCaller().getAutoSizeMaxTextSize();
        }
        final AppCompatTextHelper mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            return mTextHelper.getAutoSizeMaxTextSize();
        }
        return -1;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public int getAutoSizeMinTextSize() {
        if (ViewUtils.SDK_LEVEL_SUPPORTS_AUTOSIZE) {
            return this.getSuperCaller().getAutoSizeMinTextSize();
        }
        final AppCompatTextHelper mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            return mTextHelper.getAutoSizeMinTextSize();
        }
        return -1;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public int getAutoSizeStepGranularity() {
        if (ViewUtils.SDK_LEVEL_SUPPORTS_AUTOSIZE) {
            return this.getSuperCaller().getAutoSizeStepGranularity();
        }
        final AppCompatTextHelper mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            return mTextHelper.getAutoSizeStepGranularity();
        }
        return -1;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public int[] getAutoSizeTextAvailableSizes() {
        if (ViewUtils.SDK_LEVEL_SUPPORTS_AUTOSIZE) {
            return this.getSuperCaller().getAutoSizeTextAvailableSizes();
        }
        final AppCompatTextHelper mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            return mTextHelper.getAutoSizeTextAvailableSizes();
        }
        return new int[0];
    }
    
    @SuppressLint({ "WrongConstant" })
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public int getAutoSizeTextType() {
        final boolean sdk_LEVEL_SUPPORTS_AUTOSIZE = ViewUtils.SDK_LEVEL_SUPPORTS_AUTOSIZE;
        int n = 0;
        if (sdk_LEVEL_SUPPORTS_AUTOSIZE) {
            if (this.getSuperCaller().getAutoSizeTextType() == 1) {
                n = 1;
            }
            return n;
        }
        final AppCompatTextHelper mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            return mTextHelper.getAutoSizeTextType();
        }
        return 0;
    }
    
    @Nullable
    public ActionMode$Callback getCustomSelectionActionModeCallback() {
        return TextViewCompat.unwrapCustomSelectionActionModeCallback(super.getCustomSelectionActionModeCallback());
    }
    
    public int getFirstBaselineToTopHeight() {
        return TextViewCompat.getFirstBaselineToTopHeight(this);
    }
    
    public int getLastBaselineToBottomHeight() {
        return TextViewCompat.getLastBaselineToBottomHeight(this);
    }
    
    @RequiresApi(api = 26)
    @UiThread
    SuperCaller getSuperCaller() {
        if (this.mSuperCaller == null) {
            final int sdk_INT = Build$VERSION.SDK_INT;
            if (sdk_INT >= 28) {
                this.mSuperCaller = (SuperCaller)new SuperCallerApi28();
            }
            else if (sdk_INT >= 26) {
                this.mSuperCaller = (SuperCaller)new SuperCallerApi26();
            }
        }
        return this.mSuperCaller;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public ColorStateList getSupportBackgroundTintList() {
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        ColorStateList supportBackgroundTintList;
        if (mBackgroundTintHelper != null) {
            supportBackgroundTintList = mBackgroundTintHelper.getSupportBackgroundTintList();
        }
        else {
            supportBackgroundTintList = null;
        }
        return supportBackgroundTintList;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public PorterDuff$Mode getSupportBackgroundTintMode() {
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        PorterDuff$Mode supportBackgroundTintMode;
        if (mBackgroundTintHelper != null) {
            supportBackgroundTintMode = mBackgroundTintHelper.getSupportBackgroundTintMode();
        }
        else {
            supportBackgroundTintMode = null;
        }
        return supportBackgroundTintMode;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public ColorStateList getSupportCompoundDrawablesTintList() {
        return this.mTextHelper.getCompoundDrawableTintList();
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public PorterDuff$Mode getSupportCompoundDrawablesTintMode() {
        return this.mTextHelper.getCompoundDrawableTintMode();
    }
    
    public CharSequence getText() {
        this.consumeTextFutureAndSetBlocking();
        return super.getText();
    }
    
    @NonNull
    @RequiresApi(api = 26)
    public TextClassifier getTextClassifier() {
        if (Build$VERSION.SDK_INT < 28) {
            final AppCompatTextClassifierHelper mTextClassifierHelper = this.mTextClassifierHelper;
            if (mTextClassifierHelper != null) {
                return mTextClassifierHelper.getTextClassifier();
            }
        }
        return this.getSuperCaller().getTextClassifier();
    }
    
    @NonNull
    public PrecomputedTextCompat.Params getTextMetricsParamsCompat() {
        return TextViewCompat.getTextMetricsParams(this);
    }
    
    public boolean isEmojiCompatEnabled() {
        return this.getEmojiTextViewHelper().isEnabled();
    }
    
    public InputConnection onCreateInputConnection(final EditorInfo editorInfo) {
        final InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
        this.mTextHelper.populateSurroundingTextIfNeeded(this, onCreateInputConnection, editorInfo);
        return AppCompatHintHelper.onCreateInputConnection(onCreateInputConnection, editorInfo, (View)this);
    }
    
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        super.onLayout(b, n, n2, n3, n4);
        final AppCompatTextHelper mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.onLayout(b, n, n2, n3, n4);
        }
    }
    
    protected void onMeasure(final int n, final int n2) {
        this.consumeTextFutureAndSetBlocking();
        super.onMeasure(n, n2);
    }
    
    protected void onTextChanged(final CharSequence charSequence, int n, final int n2, final int n3) {
        super.onTextChanged(charSequence, n, n2, n3);
        final AppCompatTextHelper mTextHelper = this.mTextHelper;
        if (mTextHelper != null && !ViewUtils.SDK_LEVEL_SUPPORTS_AUTOSIZE && mTextHelper.isAutoSizeEnabled()) {
            n = 1;
        }
        else {
            n = 0;
        }
        if (n != 0) {
            this.mTextHelper.autoSizeText();
        }
    }
    
    public void setAllCaps(final boolean b) {
        super.setAllCaps(b);
        this.getEmojiTextViewHelper().setAllCaps(b);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void setAutoSizeTextTypeUniformWithConfiguration(final int n, final int n2, final int n3, final int n4) throws IllegalArgumentException {
        if (ViewUtils.SDK_LEVEL_SUPPORTS_AUTOSIZE) {
            this.getSuperCaller().setAutoSizeTextTypeUniformWithConfiguration(n, n2, n3, n4);
        }
        else {
            final AppCompatTextHelper mTextHelper = this.mTextHelper;
            if (mTextHelper != null) {
                mTextHelper.setAutoSizeTextTypeUniformWithConfiguration(n, n2, n3, n4);
            }
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void setAutoSizeTextTypeUniformWithPresetSizes(@NonNull final int[] array, final int n) throws IllegalArgumentException {
        if (ViewUtils.SDK_LEVEL_SUPPORTS_AUTOSIZE) {
            this.getSuperCaller().setAutoSizeTextTypeUniformWithPresetSizes(array, n);
        }
        else {
            final AppCompatTextHelper mTextHelper = this.mTextHelper;
            if (mTextHelper != null) {
                mTextHelper.setAutoSizeTextTypeUniformWithPresetSizes(array, n);
            }
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void setAutoSizeTextTypeWithDefaults(final int n) {
        if (ViewUtils.SDK_LEVEL_SUPPORTS_AUTOSIZE) {
            this.getSuperCaller().setAutoSizeTextTypeWithDefaults(n);
        }
        else {
            final AppCompatTextHelper mTextHelper = this.mTextHelper;
            if (mTextHelper != null) {
                mTextHelper.setAutoSizeTextTypeWithDefaults(n);
            }
        }
    }
    
    public void setBackgroundDrawable(@Nullable final Drawable backgroundDrawable) {
        super.setBackgroundDrawable(backgroundDrawable);
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.onSetBackgroundDrawable(backgroundDrawable);
        }
    }
    
    public void setBackgroundResource(@DrawableRes final int backgroundResource) {
        super.setBackgroundResource(backgroundResource);
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.onSetBackgroundResource(backgroundResource);
        }
    }
    
    public void setCompoundDrawables(@Nullable final Drawable drawable, @Nullable final Drawable drawable2, @Nullable final Drawable drawable3, @Nullable final Drawable drawable4) {
        super.setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
        final AppCompatTextHelper mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.onSetCompoundDrawables();
        }
    }
    
    @RequiresApi(17)
    public void setCompoundDrawablesRelative(@Nullable final Drawable drawable, @Nullable final Drawable drawable2, @Nullable final Drawable drawable3, @Nullable final Drawable drawable4) {
        super.setCompoundDrawablesRelative(drawable, drawable2, drawable3, drawable4);
        final AppCompatTextHelper mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.onSetCompoundDrawables();
        }
    }
    
    @RequiresApi(17)
    public void setCompoundDrawablesRelativeWithIntrinsicBounds(final int n, final int n2, final int n3, final int n4) {
        final Context context = ((View)this).getContext();
        Drawable drawable = null;
        Drawable drawable2;
        if (n != 0) {
            drawable2 = AppCompatResources.getDrawable(context, n);
        }
        else {
            drawable2 = null;
        }
        Drawable drawable3;
        if (n2 != 0) {
            drawable3 = AppCompatResources.getDrawable(context, n2);
        }
        else {
            drawable3 = null;
        }
        Drawable drawable4;
        if (n3 != 0) {
            drawable4 = AppCompatResources.getDrawable(context, n3);
        }
        else {
            drawable4 = null;
        }
        if (n4 != 0) {
            drawable = AppCompatResources.getDrawable(context, n4);
        }
        this.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable2, drawable3, drawable4, drawable);
        final AppCompatTextHelper mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.onSetCompoundDrawables();
        }
    }
    
    @RequiresApi(17)
    public void setCompoundDrawablesRelativeWithIntrinsicBounds(@Nullable final Drawable drawable, @Nullable final Drawable drawable2, @Nullable final Drawable drawable3, @Nullable final Drawable drawable4) {
        super.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
        final AppCompatTextHelper mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.onSetCompoundDrawables();
        }
    }
    
    public void setCompoundDrawablesWithIntrinsicBounds(final int n, final int n2, final int n3, final int n4) {
        final Context context = ((View)this).getContext();
        Drawable drawable = null;
        Drawable drawable2;
        if (n != 0) {
            drawable2 = AppCompatResources.getDrawable(context, n);
        }
        else {
            drawable2 = null;
        }
        Drawable drawable3;
        if (n2 != 0) {
            drawable3 = AppCompatResources.getDrawable(context, n2);
        }
        else {
            drawable3 = null;
        }
        Drawable drawable4;
        if (n3 != 0) {
            drawable4 = AppCompatResources.getDrawable(context, n3);
        }
        else {
            drawable4 = null;
        }
        if (n4 != 0) {
            drawable = AppCompatResources.getDrawable(context, n4);
        }
        this.setCompoundDrawablesWithIntrinsicBounds(drawable2, drawable3, drawable4, drawable);
        final AppCompatTextHelper mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.onSetCompoundDrawables();
        }
    }
    
    public void setCompoundDrawablesWithIntrinsicBounds(@Nullable final Drawable drawable, @Nullable final Drawable drawable2, @Nullable final Drawable drawable3, @Nullable final Drawable drawable4) {
        super.setCompoundDrawablesWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
        final AppCompatTextHelper mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.onSetCompoundDrawables();
        }
    }
    
    public void setCustomSelectionActionModeCallback(@Nullable final ActionMode$Callback actionMode$Callback) {
        super.setCustomSelectionActionModeCallback(TextViewCompat.wrapCustomSelectionActionModeCallback(this, actionMode$Callback));
    }
    
    public void setEmojiCompatEnabled(final boolean enabled) {
        this.getEmojiTextViewHelper().setEnabled(enabled);
    }
    
    public void setFilters(@NonNull final InputFilter[] array) {
        super.setFilters(this.getEmojiTextViewHelper().getFilters(array));
    }
    
    public void setFirstBaselineToTopHeight(@IntRange(from = 0L) @Px final int firstBaselineToTopHeight) {
        if (Build$VERSION.SDK_INT >= 28) {
            this.getSuperCaller().setFirstBaselineToTopHeight(firstBaselineToTopHeight);
        }
        else {
            TextViewCompat.setFirstBaselineToTopHeight(this, firstBaselineToTopHeight);
        }
    }
    
    public void setLastBaselineToBottomHeight(@IntRange(from = 0L) @Px final int lastBaselineToBottomHeight) {
        if (Build$VERSION.SDK_INT >= 28) {
            this.getSuperCaller().setLastBaselineToBottomHeight(lastBaselineToBottomHeight);
        }
        else {
            TextViewCompat.setLastBaselineToBottomHeight(this, lastBaselineToBottomHeight);
        }
    }
    
    public void setLineHeight(@IntRange(from = 0L) @Px final int n) {
        TextViewCompat.setLineHeight(this, n);
    }
    
    public void setPrecomputedText(@NonNull final PrecomputedTextCompat precomputedTextCompat) {
        TextViewCompat.setPrecomputedText(this, precomputedTextCompat);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void setSupportBackgroundTintList(@Nullable final ColorStateList supportBackgroundTintList) {
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.setSupportBackgroundTintList(supportBackgroundTintList);
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void setSupportBackgroundTintMode(@Nullable final PorterDuff$Mode supportBackgroundTintMode) {
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.setSupportBackgroundTintMode(supportBackgroundTintMode);
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void setSupportCompoundDrawablesTintList(@Nullable final ColorStateList compoundDrawableTintList) {
        this.mTextHelper.setCompoundDrawableTintList(compoundDrawableTintList);
        this.mTextHelper.applyCompoundDrawablesTints();
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void setSupportCompoundDrawablesTintMode(@Nullable final PorterDuff$Mode compoundDrawableTintMode) {
        this.mTextHelper.setCompoundDrawableTintMode(compoundDrawableTintMode);
        this.mTextHelper.applyCompoundDrawablesTints();
    }
    
    public void setTextAppearance(final Context context, final int n) {
        super.setTextAppearance(context, n);
        final AppCompatTextHelper mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.onSetTextAppearance(context, n);
        }
    }
    
    @RequiresApi(api = 26)
    public void setTextClassifier(@Nullable final TextClassifier textClassifier) {
        if (Build$VERSION.SDK_INT < 28) {
            final AppCompatTextClassifierHelper mTextClassifierHelper = this.mTextClassifierHelper;
            if (mTextClassifierHelper != null) {
                mTextClassifierHelper.setTextClassifier(textClassifier);
                return;
            }
        }
        this.getSuperCaller().setTextClassifier(textClassifier);
    }
    
    public void setTextFuture(@Nullable final Future<PrecomputedTextCompat> mPrecomputedTextFuture) {
        this.mPrecomputedTextFuture = mPrecomputedTextFuture;
        if (mPrecomputedTextFuture != null) {
            ((View)this).requestLayout();
        }
    }
    
    public void setTextMetricsParamsCompat(@NonNull final PrecomputedTextCompat.Params params) {
        TextViewCompat.setTextMetricsParams(this, params);
    }
    
    public void setTextSize(final int n, final float n2) {
        if (ViewUtils.SDK_LEVEL_SUPPORTS_AUTOSIZE) {
            super.setTextSize(n, n2);
        }
        else {
            final AppCompatTextHelper mTextHelper = this.mTextHelper;
            if (mTextHelper != null) {
                mTextHelper.setTextSize(n, n2);
            }
        }
    }
    
    public void setTypeface(@Nullable Typeface typeface, final int n) {
        if (this.mIsSetTypefaceProcessing) {
            return;
        }
        Typeface create;
        if (typeface != null && n > 0) {
            create = TypefaceCompat.create(((View)this).getContext(), typeface, n);
        }
        else {
            create = null;
        }
        this.mIsSetTypefaceProcessing = true;
        if (create != null) {
            typeface = create;
        }
        try {
            super.setTypeface(typeface, n);
        }
        finally {
            this.mIsSetTypefaceProcessing = false;
        }
    }
    
    private interface SuperCaller
    {
        int getAutoSizeMaxTextSize();
        
        int getAutoSizeMinTextSize();
        
        int getAutoSizeStepGranularity();
        
        int[] getAutoSizeTextAvailableSizes();
        
        int getAutoSizeTextType();
        
        TextClassifier getTextClassifier();
        
        void setAutoSizeTextTypeUniformWithConfiguration(final int p0, final int p1, final int p2, final int p3);
        
        void setAutoSizeTextTypeUniformWithPresetSizes(final int[] p0, final int p1);
        
        void setAutoSizeTextTypeWithDefaults(final int p0);
        
        void setFirstBaselineToTopHeight(@Px final int p0);
        
        void setLastBaselineToBottomHeight(@Px final int p0);
        
        void setTextClassifier(@Nullable final TextClassifier p0);
    }
    
    @RequiresApi(api = 26)
    class SuperCallerApi26 implements SuperCaller
    {
        final AppCompatTextView this$0;
        
        SuperCallerApi26(final AppCompatTextView this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public int getAutoSizeMaxTextSize() {
            return AppCompatTextView.access$001(this.this$0);
        }
        
        @Override
        public int getAutoSizeMinTextSize() {
            return AppCompatTextView.access$101(this.this$0);
        }
        
        @Override
        public int getAutoSizeStepGranularity() {
            return AppCompatTextView.access$201(this.this$0);
        }
        
        @Override
        public int[] getAutoSizeTextAvailableSizes() {
            return AppCompatTextView.access$301(this.this$0);
        }
        
        @Override
        public int getAutoSizeTextType() {
            return AppCompatTextView.access$401(this.this$0);
        }
        
        @Override
        public TextClassifier getTextClassifier() {
            return AppCompatTextView.access$501(this.this$0);
        }
        
        @Override
        public void setAutoSizeTextTypeUniformWithConfiguration(final int n, final int n2, final int n3, final int n4) {
            AppCompatTextView.access$601(this.this$0, n, n2, n3, n4);
        }
        
        @Override
        public void setAutoSizeTextTypeUniformWithPresetSizes(final int[] array, final int n) {
            AppCompatTextView.access$701(this.this$0, array, n);
        }
        
        @Override
        public void setAutoSizeTextTypeWithDefaults(final int n) {
            AppCompatTextView.access$801(this.this$0, n);
        }
        
        @Override
        public void setFirstBaselineToTopHeight(final int n) {
        }
        
        @Override
        public void setLastBaselineToBottomHeight(final int n) {
        }
        
        @Override
        public void setTextClassifier(@Nullable final TextClassifier textClassifier) {
            AppCompatTextView.access$901(this.this$0, textClassifier);
        }
    }
    
    @RequiresApi(api = 28)
    class SuperCallerApi28 extends SuperCallerApi26
    {
        final AppCompatTextView this$0;
        
        SuperCallerApi28(final AppCompatTextView this$0) {
            this.this$0 = this$0.super();
        }
        
        @Override
        public void setFirstBaselineToTopHeight(@Px final int n) {
            AppCompatTextView.access$1001(this.this$0, n);
        }
        
        @Override
        public void setLastBaselineToBottomHeight(@Px final int n) {
            AppCompatTextView.access$1101(this.this$0, n);
        }
    }
}

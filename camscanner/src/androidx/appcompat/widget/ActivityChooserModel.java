// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import java.math.BigDecimal;
import android.content.pm.ActivityInfo;
import android.content.ComponentName;
import java.util.Collections;
import java.util.Collection;
import android.os.AsyncTask;
import android.content.pm.ResolveInfo;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashMap;
import android.content.Intent;
import android.content.Context;
import java.util.List;
import java.util.Map;
import android.database.DataSetObservable;

class ActivityChooserModel extends DataSetObservable
{
    static final String ATTRIBUTE_ACTIVITY = "activity";
    static final String ATTRIBUTE_TIME = "time";
    static final String ATTRIBUTE_WEIGHT = "weight";
    static final boolean DEBUG = false;
    private static final int DEFAULT_ACTIVITY_INFLATION = 5;
    private static final float DEFAULT_HISTORICAL_RECORD_WEIGHT = 1.0f;
    public static final String DEFAULT_HISTORY_FILE_NAME = "activity_choser_model_history.xml";
    public static final int DEFAULT_HISTORY_MAX_LENGTH = 50;
    private static final String HISTORY_FILE_EXTENSION = ".xml";
    private static final int INVALID_INDEX = -1;
    static final String LOG_TAG = "ActivityChooserModel";
    static final String TAG_HISTORICAL_RECORD = "historical-record";
    static final String TAG_HISTORICAL_RECORDS = "historical-records";
    private static final Map<String, ActivityChooserModel> sDataModelRegistry;
    private static final Object sRegistryLock;
    private final List<ActivityResolveInfo> mActivities;
    private OnChooseActivityListener mActivityChoserModelPolicy;
    private ActivitySorter mActivitySorter;
    boolean mCanReadHistoricalData;
    final Context mContext;
    private final List<HistoricalRecord> mHistoricalRecords;
    private boolean mHistoricalRecordsChanged;
    final String mHistoryFileName;
    private int mHistoryMaxSize;
    private final Object mInstanceLock;
    private Intent mIntent;
    private boolean mReadShareHistoryCalled;
    private boolean mReloadActivities;
    
    static {
        sRegistryLock = new Object();
        sDataModelRegistry = new HashMap<String, ActivityChooserModel>();
    }
    
    private ActivityChooserModel(final Context context, final String s) {
        this.mInstanceLock = new Object();
        this.mActivities = new ArrayList<ActivityResolveInfo>();
        this.mHistoricalRecords = new ArrayList<HistoricalRecord>();
        this.mActivitySorter = (ActivitySorter)new DefaultSorter();
        this.mHistoryMaxSize = 50;
        this.mCanReadHistoricalData = true;
        this.mReadShareHistoryCalled = false;
        this.mHistoricalRecordsChanged = true;
        this.mReloadActivities = false;
        this.mContext = context.getApplicationContext();
        if (!TextUtils.isEmpty((CharSequence)s) && !s.endsWith(".xml")) {
            final StringBuilder sb = new StringBuilder();
            sb.append(s);
            sb.append(".xml");
            this.mHistoryFileName = sb.toString();
        }
        else {
            this.mHistoryFileName = s;
        }
    }
    
    private boolean addHistoricalRecord(final HistoricalRecord historicalRecord) {
        final boolean add = this.mHistoricalRecords.add(historicalRecord);
        if (add) {
            this.mHistoricalRecordsChanged = true;
            this.pruneExcessiveHistoricalRecordsIfNeeded();
            this.persistHistoricalDataIfNeeded();
            this.sortActivitiesIfNeeded();
            this.notifyChanged();
        }
        return add;
    }
    
    private void ensureConsistentState() {
        final boolean loadActivitiesIfNeeded = this.loadActivitiesIfNeeded();
        final boolean historicalDataIfNeeded = this.readHistoricalDataIfNeeded();
        this.pruneExcessiveHistoricalRecordsIfNeeded();
        if (loadActivitiesIfNeeded | historicalDataIfNeeded) {
            this.sortActivitiesIfNeeded();
            this.notifyChanged();
        }
    }
    
    public static ActivityChooserModel get(final Context context, final String s) {
        synchronized (ActivityChooserModel.sRegistryLock) {
            final Map<String, ActivityChooserModel> sDataModelRegistry = ActivityChooserModel.sDataModelRegistry;
            ActivityChooserModel activityChooserModel;
            if ((activityChooserModel = sDataModelRegistry.get(s)) == null) {
                activityChooserModel = new ActivityChooserModel(context, s);
                sDataModelRegistry.put(s, activityChooserModel);
            }
            return activityChooserModel;
        }
    }
    
    private boolean loadActivitiesIfNeeded() {
        final boolean mReloadActivities = this.mReloadActivities;
        int i = 0;
        if (mReloadActivities && this.mIntent != null) {
            this.mReloadActivities = false;
            this.mActivities.clear();
            for (List queryIntentActivities = this.mContext.getPackageManager().queryIntentActivities(this.mIntent, 0); i < queryIntentActivities.size(); ++i) {
                this.mActivities.add(new ActivityResolveInfo((ResolveInfo)queryIntentActivities.get(i)));
            }
            return true;
        }
        return false;
    }
    
    private void persistHistoricalDataIfNeeded() {
        if (!this.mReadShareHistoryCalled) {
            throw new IllegalStateException("No preceding call to #readHistoricalData");
        }
        if (!this.mHistoricalRecordsChanged) {
            return;
        }
        this.mHistoricalRecordsChanged = false;
        if (!TextUtils.isEmpty((CharSequence)this.mHistoryFileName)) {
            new PersistHistoryAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Object[] { new ArrayList(this.mHistoricalRecords), this.mHistoryFileName });
        }
    }
    
    private void pruneExcessiveHistoricalRecordsIfNeeded() {
        final int n = this.mHistoricalRecords.size() - this.mHistoryMaxSize;
        if (n <= 0) {
            return;
        }
        this.mHistoricalRecordsChanged = true;
        for (int i = 0; i < n; ++i) {
            final HistoricalRecord historicalRecord = this.mHistoricalRecords.remove(0);
        }
    }
    
    private boolean readHistoricalDataIfNeeded() {
        if (this.mCanReadHistoricalData && this.mHistoricalRecordsChanged && !TextUtils.isEmpty((CharSequence)this.mHistoryFileName)) {
            this.mCanReadHistoricalData = false;
            this.mReadShareHistoryCalled = true;
            this.readHistoricalDataImpl();
            return true;
        }
        return false;
    }
    
    private void readHistoricalDataImpl() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        androidx/appcompat/widget/ActivityChooserModel.mContext:Landroid/content/Context;
        //     4: aload_0        
        //     5: getfield        androidx/appcompat/widget/ActivityChooserModel.mHistoryFileName:Ljava/lang/String;
        //     8: invokevirtual   android/content/Context.openFileInput:(Ljava/lang/String;)Ljava/io/FileInputStream;
        //    11: astore          5
        //    13: invokestatic    android/util/Xml.newPullParser:()Lorg/xmlpull/v1/XmlPullParser;
        //    16: astore          8
        //    18: aload           8
        //    20: aload           5
        //    22: ldc_w           "UTF-8"
        //    25: invokeinterface org/xmlpull/v1/XmlPullParser.setInput:(Ljava/io/InputStream;Ljava/lang/String;)V
        //    30: iconst_0       
        //    31: istore_2       
        //    32: iload_2        
        //    33: iconst_1       
        //    34: if_icmpeq       53
        //    37: iload_2        
        //    38: iconst_2       
        //    39: if_icmpeq       53
        //    42: aload           8
        //    44: invokeinterface org/xmlpull/v1/XmlPullParser.next:()I
        //    49: istore_2       
        //    50: goto            32
        //    53: ldc             "historical-records"
        //    55: aload           8
        //    57: invokeinterface org/xmlpull/v1/XmlPullParser.getName:()Ljava/lang/String;
        //    62: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    65: ifeq            218
        //    68: aload_0        
        //    69: getfield        androidx/appcompat/widget/ActivityChooserModel.mHistoricalRecords:Ljava/util/List;
        //    72: astore          6
        //    74: aload           6
        //    76: invokeinterface java/util/List.clear:()V
        //    81: aload           8
        //    83: invokeinterface org/xmlpull/v1/XmlPullParser.next:()I
        //    88: istore_2       
        //    89: iload_2        
        //    90: iconst_1       
        //    91: if_icmpne       107
        //    94: aload           5
        //    96: ifnull          317
        //    99: aload           5
        //   101: invokevirtual   java/io/FileInputStream.close:()V
        //   104: goto            317
        //   107: iload_2        
        //   108: iconst_3       
        //   109: if_icmpeq       81
        //   112: iload_2        
        //   113: iconst_4       
        //   114: if_icmpne       120
        //   117: goto            81
        //   120: ldc             "historical-record"
        //   122: aload           8
        //   124: invokeinterface org/xmlpull/v1/XmlPullParser.getName:()Ljava/lang/String;
        //   129: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   132: ifeq            202
        //   135: aload           8
        //   137: aconst_null    
        //   138: ldc             "activity"
        //   140: invokeinterface org/xmlpull/v1/XmlPullParser.getAttributeValue:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   145: astore          9
        //   147: aload           8
        //   149: aconst_null    
        //   150: ldc             "time"
        //   152: invokeinterface org/xmlpull/v1/XmlPullParser.getAttributeValue:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   157: invokestatic    java/lang/Long.parseLong:(Ljava/lang/String;)J
        //   160: lstore_3       
        //   161: aload           8
        //   163: aconst_null    
        //   164: ldc             "weight"
        //   166: invokeinterface org/xmlpull/v1/XmlPullParser.getAttributeValue:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   171: invokestatic    java/lang/Float.parseFloat:(Ljava/lang/String;)F
        //   174: fstore_1       
        //   175: new             Landroidx/appcompat/widget/ActivityChooserModel$HistoricalRecord;
        //   178: astore          7
        //   180: aload           7
        //   182: aload           9
        //   184: lload_3        
        //   185: fload_1        
        //   186: invokespecial   androidx/appcompat/widget/ActivityChooserModel$HistoricalRecord.<init>:(Ljava/lang/String;JF)V
        //   189: aload           6
        //   191: aload           7
        //   193: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   198: pop            
        //   199: goto            81
        //   202: new             Lorg/xmlpull/v1/XmlPullParserException;
        //   205: astore          6
        //   207: aload           6
        //   209: ldc_w           "Share records file not well-formed."
        //   212: invokespecial   org/xmlpull/v1/XmlPullParserException.<init>:(Ljava/lang/String;)V
        //   215: aload           6
        //   217: athrow         
        //   218: new             Lorg/xmlpull/v1/XmlPullParserException;
        //   221: astore          6
        //   223: aload           6
        //   225: ldc_w           "Share records file does not start with historical-records tag."
        //   228: invokespecial   org/xmlpull/v1/XmlPullParserException.<init>:(Ljava/lang/String;)V
        //   231: aload           6
        //   233: athrow         
        //   234: astore          6
        //   236: goto            318
        //   239: astore          6
        //   241: new             Ljava/lang/StringBuilder;
        //   244: astore          6
        //   246: aload           6
        //   248: invokespecial   java/lang/StringBuilder.<init>:()V
        //   251: aload           6
        //   253: ldc_w           "Error reading historical recrod file: "
        //   256: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   259: pop            
        //   260: aload           6
        //   262: aload_0        
        //   263: getfield        androidx/appcompat/widget/ActivityChooserModel.mHistoryFileName:Ljava/lang/String;
        //   266: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   269: pop            
        //   270: aload           5
        //   272: ifnull          317
        //   275: goto            99
        //   278: astore          6
        //   280: new             Ljava/lang/StringBuilder;
        //   283: astore          6
        //   285: aload           6
        //   287: invokespecial   java/lang/StringBuilder.<init>:()V
        //   290: aload           6
        //   292: ldc_w           "Error reading historical recrod file: "
        //   295: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   298: pop            
        //   299: aload           6
        //   301: aload_0        
        //   302: getfield        androidx/appcompat/widget/ActivityChooserModel.mHistoryFileName:Ljava/lang/String;
        //   305: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   308: pop            
        //   309: aload           5
        //   311: ifnull          317
        //   314: goto            99
        //   317: return         
        //   318: aload           5
        //   320: ifnull          328
        //   323: aload           5
        //   325: invokevirtual   java/io/FileInputStream.close:()V
        //   328: aload           6
        //   330: athrow         
        //   331: astore          5
        //   333: return         
        //   334: astore          5
        //   336: goto            317
        //   339: astore          5
        //   341: goto            328
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                   
        //  -----  -----  -----  -----  ---------------------------------------
        //  0      13     331    334    Ljava/io/FileNotFoundException;
        //  13     30     278    317    Lorg/xmlpull/v1/XmlPullParserException;
        //  13     30     239    278    Ljava/io/IOException;
        //  13     30     234    331    Any
        //  42     50     278    317    Lorg/xmlpull/v1/XmlPullParserException;
        //  42     50     239    278    Ljava/io/IOException;
        //  42     50     234    331    Any
        //  53     81     278    317    Lorg/xmlpull/v1/XmlPullParserException;
        //  53     81     239    278    Ljava/io/IOException;
        //  53     81     234    331    Any
        //  81     89     278    317    Lorg/xmlpull/v1/XmlPullParserException;
        //  81     89     239    278    Ljava/io/IOException;
        //  81     89     234    331    Any
        //  99     104    334    339    Ljava/io/IOException;
        //  120    199    278    317    Lorg/xmlpull/v1/XmlPullParserException;
        //  120    199    239    278    Ljava/io/IOException;
        //  120    199    234    331    Any
        //  202    218    278    317    Lorg/xmlpull/v1/XmlPullParserException;
        //  202    218    239    278    Ljava/io/IOException;
        //  202    218    234    331    Any
        //  218    234    278    317    Lorg/xmlpull/v1/XmlPullParserException;
        //  218    234    239    278    Ljava/io/IOException;
        //  218    234    234    331    Any
        //  241    270    234    331    Any
        //  280    309    234    331    Any
        //  323    328    339    344    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 149 out of bounds for length 149
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private boolean sortActivitiesIfNeeded() {
        if (this.mActivitySorter != null && this.mIntent != null && !this.mActivities.isEmpty() && !this.mHistoricalRecords.isEmpty()) {
            this.mActivitySorter.sort(this.mIntent, this.mActivities, Collections.unmodifiableList((List<? extends HistoricalRecord>)this.mHistoricalRecords));
            return true;
        }
        return false;
    }
    
    public Intent chooseActivity(final int n) {
        synchronized (this.mInstanceLock) {
            if (this.mIntent == null) {
                return null;
            }
            this.ensureConsistentState();
            final ActivityInfo activityInfo = this.mActivities.get(n).resolveInfo.activityInfo;
            final ComponentName component = new ComponentName(activityInfo.packageName, activityInfo.name);
            final Intent intent = new Intent(this.mIntent);
            intent.setComponent(component);
            if (this.mActivityChoserModelPolicy != null && this.mActivityChoserModelPolicy.onChooseActivity(this, new Intent(intent))) {
                return null;
            }
            this.addHistoricalRecord(new HistoricalRecord(component, System.currentTimeMillis(), 1.0f));
            return intent;
        }
    }
    
    public ResolveInfo getActivity(final int n) {
        synchronized (this.mInstanceLock) {
            this.ensureConsistentState();
            return this.mActivities.get(n).resolveInfo;
        }
    }
    
    public int getActivityCount() {
        synchronized (this.mInstanceLock) {
            this.ensureConsistentState();
            return this.mActivities.size();
        }
    }
    
    public int getActivityIndex(final ResolveInfo resolveInfo) {
        synchronized (this.mInstanceLock) {
            this.ensureConsistentState();
            final List<ActivityResolveInfo> mActivities = this.mActivities;
            for (int size = mActivities.size(), i = 0; i < size; ++i) {
                if (((ActivityResolveInfo)mActivities.get(i)).resolveInfo == resolveInfo) {
                    return i;
                }
            }
            return -1;
        }
    }
    
    public ResolveInfo getDefaultActivity() {
        synchronized (this.mInstanceLock) {
            this.ensureConsistentState();
            if (!this.mActivities.isEmpty()) {
                return this.mActivities.get(0).resolveInfo;
            }
            return null;
        }
    }
    
    public int getHistoryMaxSize() {
        synchronized (this.mInstanceLock) {
            return this.mHistoryMaxSize;
        }
    }
    
    public int getHistorySize() {
        synchronized (this.mInstanceLock) {
            this.ensureConsistentState();
            return this.mHistoricalRecords.size();
        }
    }
    
    public Intent getIntent() {
        synchronized (this.mInstanceLock) {
            return this.mIntent;
        }
    }
    
    public void setActivitySorter(final ActivitySorter mActivitySorter) {
        synchronized (this.mInstanceLock) {
            if (this.mActivitySorter == mActivitySorter) {
                return;
            }
            this.mActivitySorter = mActivitySorter;
            if (this.sortActivitiesIfNeeded()) {
                this.notifyChanged();
            }
        }
    }
    
    public void setDefaultActivity(final int n) {
        synchronized (this.mInstanceLock) {
            this.ensureConsistentState();
            final ActivityResolveInfo activityResolveInfo = this.mActivities.get(n);
            final ActivityResolveInfo activityResolveInfo2 = this.mActivities.get(0);
            float n2;
            if (activityResolveInfo2 != null) {
                n2 = activityResolveInfo2.weight - activityResolveInfo.weight + 5.0f;
            }
            else {
                n2 = 1.0f;
            }
            final ActivityInfo activityInfo = activityResolveInfo.resolveInfo.activityInfo;
            this.addHistoricalRecord(new HistoricalRecord(new ComponentName(activityInfo.packageName, activityInfo.name), System.currentTimeMillis(), n2));
        }
    }
    
    public void setHistoryMaxSize(final int mHistoryMaxSize) {
        synchronized (this.mInstanceLock) {
            if (this.mHistoryMaxSize == mHistoryMaxSize) {
                return;
            }
            this.mHistoryMaxSize = mHistoryMaxSize;
            this.pruneExcessiveHistoricalRecordsIfNeeded();
            if (this.sortActivitiesIfNeeded()) {
                this.notifyChanged();
            }
        }
    }
    
    public void setIntent(final Intent mIntent) {
        synchronized (this.mInstanceLock) {
            if (this.mIntent == mIntent) {
                return;
            }
            this.mIntent = mIntent;
            this.mReloadActivities = true;
            this.ensureConsistentState();
        }
    }
    
    public void setOnChooseActivityListener(final OnChooseActivityListener mActivityChoserModelPolicy) {
        synchronized (this.mInstanceLock) {
            this.mActivityChoserModelPolicy = mActivityChoserModelPolicy;
        }
    }
    
    public interface ActivityChooserModelClient
    {
        void setActivityChooserModel(final ActivityChooserModel p0);
    }
    
    public static final class ActivityResolveInfo implements Comparable<ActivityResolveInfo>
    {
        public final ResolveInfo resolveInfo;
        public float weight;
        
        public ActivityResolveInfo(final ResolveInfo resolveInfo) {
            this.resolveInfo = resolveInfo;
        }
        
        @Override
        public int compareTo(final ActivityResolveInfo activityResolveInfo) {
            return Float.floatToIntBits(activityResolveInfo.weight) - Float.floatToIntBits(this.weight);
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o != null && ActivityResolveInfo.class == o.getClass() && Float.floatToIntBits(this.weight) == Float.floatToIntBits(((ActivityResolveInfo)o).weight));
        }
        
        @Override
        public int hashCode() {
            return Float.floatToIntBits(this.weight) + 31;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append("resolveInfo:");
            sb.append(this.resolveInfo.toString());
            sb.append("; weight:");
            sb.append(new BigDecimal(this.weight));
            sb.append("]");
            return sb.toString();
        }
    }
    
    public interface ActivitySorter
    {
        void sort(final Intent p0, final List<ActivityResolveInfo> p1, final List<HistoricalRecord> p2);
    }
    
    private static final class DefaultSorter implements ActivitySorter
    {
        private static final float WEIGHT_DECAY_COEFFICIENT = 0.95f;
        private final Map<ComponentName, ActivityResolveInfo> mPackageNameToActivityMap;
        
        DefaultSorter() {
            this.mPackageNameToActivityMap = new HashMap<ComponentName, ActivityResolveInfo>();
        }
        
        @Override
        public void sort(final Intent intent, final List<ActivityResolveInfo> list, final List<HistoricalRecord> list2) {
            final Map<ComponentName, ActivityResolveInfo> mPackageNameToActivityMap = this.mPackageNameToActivityMap;
            mPackageNameToActivityMap.clear();
            for (int size = list.size(), i = 0; i < size; ++i) {
                final ActivityResolveInfo activityResolveInfo = list.get(i);
                activityResolveInfo.weight = 0.0f;
                final ActivityInfo activityInfo = activityResolveInfo.resolveInfo.activityInfo;
                mPackageNameToActivityMap.put(new ComponentName(activityInfo.packageName, activityInfo.name), activityResolveInfo);
            }
            int j = list2.size() - 1;
            float n = 1.0f;
            while (j >= 0) {
                final HistoricalRecord historicalRecord = list2.get(j);
                final ActivityResolveInfo activityResolveInfo2 = mPackageNameToActivityMap.get(historicalRecord.activity);
                float n2 = n;
                if (activityResolveInfo2 != null) {
                    activityResolveInfo2.weight += historicalRecord.weight * n;
                    n2 = n * 0.95f;
                }
                --j;
                n = n2;
            }
            Collections.sort((List<Comparable>)list);
        }
    }
    
    public static final class HistoricalRecord
    {
        public final ComponentName activity;
        public final long time;
        public final float weight;
        
        public HistoricalRecord(final ComponentName activity, final long time, final float weight) {
            this.activity = activity;
            this.time = time;
            this.weight = weight;
        }
        
        public HistoricalRecord(final String s, final long n, final float n2) {
            this(ComponentName.unflattenFromString(s), n, n2);
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null) {
                return false;
            }
            if (HistoricalRecord.class != o.getClass()) {
                return false;
            }
            final HistoricalRecord historicalRecord = (HistoricalRecord)o;
            final ComponentName activity = this.activity;
            if (activity == null) {
                if (historicalRecord.activity != null) {
                    return false;
                }
            }
            else if (!activity.equals((Object)historicalRecord.activity)) {
                return false;
            }
            return this.time == historicalRecord.time && Float.floatToIntBits(this.weight) == Float.floatToIntBits(historicalRecord.weight);
        }
        
        @Override
        public int hashCode() {
            final ComponentName activity = this.activity;
            int hashCode;
            if (activity == null) {
                hashCode = 0;
            }
            else {
                hashCode = activity.hashCode();
            }
            final long time = this.time;
            return ((hashCode + 31) * 31 + (int)(time ^ time >>> 32)) * 31 + Float.floatToIntBits(this.weight);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append("; activity:");
            sb.append(this.activity);
            sb.append("; time:");
            sb.append(this.time);
            sb.append("; weight:");
            sb.append(new BigDecimal(this.weight));
            sb.append("]");
            return sb.toString();
        }
    }
    
    public interface OnChooseActivityListener
    {
        boolean onChooseActivity(final ActivityChooserModel p0, final Intent p1);
    }
    
    private final class PersistHistoryAsyncTask extends AsyncTask<Object, Void, Void>
    {
        final ActivityChooserModel this$0;
        
        PersistHistoryAsyncTask(final ActivityChooserModel this$0) {
            this.this$0 = this$0;
        }
        
        public Void doInBackground(final Object... p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: iconst_0       
            //     2: aaload         
            //     3: checkcast       Ljava/util/List;
            //     6: astore          5
            //     8: aload_1        
            //     9: iconst_1       
            //    10: aaload         
            //    11: checkcast       Ljava/lang/String;
            //    14: astore          4
            //    16: aload_0        
            //    17: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //    20: getfield        androidx/appcompat/widget/ActivityChooserModel.mContext:Landroid/content/Context;
            //    23: aload           4
            //    25: iconst_0       
            //    26: invokevirtual   android/content/Context.openFileOutput:(Ljava/lang/String;I)Ljava/io/FileOutputStream;
            //    29: astore_1       
            //    30: invokestatic    android/util/Xml.newSerializer:()Lorg/xmlpull/v1/XmlSerializer;
            //    33: astore          6
            //    35: aload           6
            //    37: aload_1        
            //    38: aconst_null    
            //    39: invokeinterface org/xmlpull/v1/XmlSerializer.setOutput:(Ljava/io/OutputStream;Ljava/lang/String;)V
            //    44: aload           6
            //    46: ldc             "UTF-8"
            //    48: getstatic       java/lang/Boolean.TRUE:Ljava/lang/Boolean;
            //    51: invokeinterface org/xmlpull/v1/XmlSerializer.startDocument:(Ljava/lang/String;Ljava/lang/Boolean;)V
            //    56: aload           6
            //    58: aconst_null    
            //    59: ldc             "historical-records"
            //    61: invokeinterface org/xmlpull/v1/XmlSerializer.startTag:(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //    66: pop            
            //    67: aload           5
            //    69: invokeinterface java/util/List.size:()I
            //    74: istore_3       
            //    75: iconst_0       
            //    76: istore_2       
            //    77: iload_2        
            //    78: iload_3        
            //    79: if_icmpge       180
            //    82: aload           5
            //    84: iconst_0       
            //    85: invokeinterface java/util/List.remove:(I)Ljava/lang/Object;
            //    90: checkcast       Landroidx/appcompat/widget/ActivityChooserModel$HistoricalRecord;
            //    93: astore          4
            //    95: aload           6
            //    97: aconst_null    
            //    98: ldc             "historical-record"
            //   100: invokeinterface org/xmlpull/v1/XmlSerializer.startTag:(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //   105: pop            
            //   106: aload           6
            //   108: aconst_null    
            //   109: ldc             "activity"
            //   111: aload           4
            //   113: getfield        androidx/appcompat/widget/ActivityChooserModel$HistoricalRecord.activity:Landroid/content/ComponentName;
            //   116: invokevirtual   android/content/ComponentName.flattenToString:()Ljava/lang/String;
            //   119: invokeinterface org/xmlpull/v1/XmlSerializer.attribute:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //   124: pop            
            //   125: aload           6
            //   127: aconst_null    
            //   128: ldc             "time"
            //   130: aload           4
            //   132: getfield        androidx/appcompat/widget/ActivityChooserModel$HistoricalRecord.time:J
            //   135: invokestatic    java/lang/String.valueOf:(J)Ljava/lang/String;
            //   138: invokeinterface org/xmlpull/v1/XmlSerializer.attribute:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //   143: pop            
            //   144: aload           6
            //   146: aconst_null    
            //   147: ldc             "weight"
            //   149: aload           4
            //   151: getfield        androidx/appcompat/widget/ActivityChooserModel$HistoricalRecord.weight:F
            //   154: invokestatic    java/lang/String.valueOf:(F)Ljava/lang/String;
            //   157: invokeinterface org/xmlpull/v1/XmlSerializer.attribute:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //   162: pop            
            //   163: aload           6
            //   165: aconst_null    
            //   166: ldc             "historical-record"
            //   168: invokeinterface org/xmlpull/v1/XmlSerializer.endTag:(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //   173: pop            
            //   174: iinc            2, 1
            //   177: goto            77
            //   180: aload           6
            //   182: aconst_null    
            //   183: ldc             "historical-records"
            //   185: invokeinterface org/xmlpull/v1/XmlSerializer.endTag:(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //   190: pop            
            //   191: aload           6
            //   193: invokeinterface org/xmlpull/v1/XmlSerializer.endDocument:()V
            //   198: aload_0        
            //   199: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //   202: iconst_1       
            //   203: putfield        androidx/appcompat/widget/ActivityChooserModel.mCanReadHistoricalData:Z
            //   206: aload_1        
            //   207: ifnull          381
            //   210: aload_1        
            //   211: invokevirtual   java/io/FileOutputStream.close:()V
            //   214: goto            381
            //   217: astore          4
            //   219: goto            383
            //   222: astore          4
            //   224: getstatic       androidx/appcompat/widget/ActivityChooserModel.DEFAULT_HISTORY_FILE_NAME:Ljava/lang/String;
            //   227: astore          4
            //   229: new             Ljava/lang/StringBuilder;
            //   232: astore          4
            //   234: aload           4
            //   236: invokespecial   java/lang/StringBuilder.<init>:()V
            //   239: aload           4
            //   241: ldc             "Error writing historical record file: "
            //   243: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   246: pop            
            //   247: aload           4
            //   249: aload_0        
            //   250: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //   253: getfield        androidx/appcompat/widget/ActivityChooserModel.mHistoryFileName:Ljava/lang/String;
            //   256: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   259: pop            
            //   260: aload_0        
            //   261: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //   264: iconst_1       
            //   265: putfield        androidx/appcompat/widget/ActivityChooserModel.mCanReadHistoricalData:Z
            //   268: aload_1        
            //   269: ifnull          381
            //   272: goto            210
            //   275: astore          4
            //   277: getstatic       androidx/appcompat/widget/ActivityChooserModel.DEFAULT_HISTORY_FILE_NAME:Ljava/lang/String;
            //   280: astore          4
            //   282: new             Ljava/lang/StringBuilder;
            //   285: astore          4
            //   287: aload           4
            //   289: invokespecial   java/lang/StringBuilder.<init>:()V
            //   292: aload           4
            //   294: ldc             "Error writing historical record file: "
            //   296: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   299: pop            
            //   300: aload           4
            //   302: aload_0        
            //   303: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //   306: getfield        androidx/appcompat/widget/ActivityChooserModel.mHistoryFileName:Ljava/lang/String;
            //   309: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   312: pop            
            //   313: aload_0        
            //   314: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //   317: iconst_1       
            //   318: putfield        androidx/appcompat/widget/ActivityChooserModel.mCanReadHistoricalData:Z
            //   321: aload_1        
            //   322: ifnull          381
            //   325: goto            210
            //   328: astore          4
            //   330: getstatic       androidx/appcompat/widget/ActivityChooserModel.DEFAULT_HISTORY_FILE_NAME:Ljava/lang/String;
            //   333: astore          4
            //   335: new             Ljava/lang/StringBuilder;
            //   338: astore          4
            //   340: aload           4
            //   342: invokespecial   java/lang/StringBuilder.<init>:()V
            //   345: aload           4
            //   347: ldc             "Error writing historical record file: "
            //   349: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   352: pop            
            //   353: aload           4
            //   355: aload_0        
            //   356: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //   359: getfield        androidx/appcompat/widget/ActivityChooserModel.mHistoryFileName:Ljava/lang/String;
            //   362: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   365: pop            
            //   366: aload_0        
            //   367: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //   370: iconst_1       
            //   371: putfield        androidx/appcompat/widget/ActivityChooserModel.mCanReadHistoricalData:Z
            //   374: aload_1        
            //   375: ifnull          381
            //   378: goto            210
            //   381: aconst_null    
            //   382: areturn        
            //   383: aload_0        
            //   384: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //   387: iconst_1       
            //   388: putfield        androidx/appcompat/widget/ActivityChooserModel.mCanReadHistoricalData:Z
            //   391: aload_1        
            //   392: ifnull          399
            //   395: aload_1        
            //   396: invokevirtual   java/io/FileOutputStream.close:()V
            //   399: aload           4
            //   401: athrow         
            //   402: astore_1       
            //   403: getstatic       androidx/appcompat/widget/ActivityChooserModel.DEFAULT_HISTORY_FILE_NAME:Ljava/lang/String;
            //   406: astore_1       
            //   407: new             Ljava/lang/StringBuilder;
            //   410: dup            
            //   411: invokespecial   java/lang/StringBuilder.<init>:()V
            //   414: astore_1       
            //   415: aload_1        
            //   416: ldc             "Error writing historical record file: "
            //   418: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   421: pop            
            //   422: aload_1        
            //   423: aload           4
            //   425: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   428: pop            
            //   429: aconst_null    
            //   430: areturn        
            //   431: astore_1       
            //   432: goto            381
            //   435: astore_1       
            //   436: goto            399
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                                
            //  -----  -----  -----  -----  ------------------------------------
            //  16     30     402    431    Ljava/io/FileNotFoundException;
            //  35     75     328    381    Ljava/lang/IllegalArgumentException;
            //  35     75     275    328    Ljava/lang/IllegalStateException;
            //  35     75     222    275    Ljava/io/IOException;
            //  35     75     217    402    Any
            //  82     174    328    381    Ljava/lang/IllegalArgumentException;
            //  82     174    275    328    Ljava/lang/IllegalStateException;
            //  82     174    222    275    Ljava/io/IOException;
            //  82     174    217    402    Any
            //  180    198    328    381    Ljava/lang/IllegalArgumentException;
            //  180    198    275    328    Ljava/lang/IllegalStateException;
            //  180    198    222    275    Ljava/io/IOException;
            //  180    198    217    402    Any
            //  210    214    431    435    Ljava/io/IOException;
            //  224    260    217    402    Any
            //  277    313    217    402    Any
            //  330    366    217    402    Any
            //  395    399    435    439    Ljava/io/IOException;
            // 
            // The error that occurred was:
            // 
            // java.lang.IndexOutOfBoundsException: Index 204 out of bounds for length 204
            //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
            //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
            //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
            //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
            //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
            //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
            //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
    }
}

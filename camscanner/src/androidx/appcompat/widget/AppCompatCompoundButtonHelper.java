// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.Resources$NotFoundException;
import androidx.appcompat.content.res.AppCompatResources;
import android.view.View;
import androidx.core.view.ViewCompat;
import androidx.appcompat.R;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.graphics.drawable.Drawable;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.widget.CompoundButtonCompat;
import androidx.annotation.NonNull;
import android.widget.CompoundButton;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;

class AppCompatCompoundButtonHelper
{
    private ColorStateList mButtonTintList;
    private PorterDuff$Mode mButtonTintMode;
    private boolean mHasButtonTint;
    private boolean mHasButtonTintMode;
    private boolean mSkipNextApply;
    @NonNull
    private final CompoundButton mView;
    
    AppCompatCompoundButtonHelper(@NonNull final CompoundButton mView) {
        this.mButtonTintList = null;
        this.mButtonTintMode = null;
        this.mHasButtonTint = false;
        this.mHasButtonTintMode = false;
        this.mView = mView;
    }
    
    void applyButtonTint() {
        final Drawable buttonDrawable = CompoundButtonCompat.getButtonDrawable(this.mView);
        if (buttonDrawable != null && (this.mHasButtonTint || this.mHasButtonTintMode)) {
            final Drawable mutate = DrawableCompat.wrap(buttonDrawable).mutate();
            if (this.mHasButtonTint) {
                DrawableCompat.setTintList(mutate, this.mButtonTintList);
            }
            if (this.mHasButtonTintMode) {
                DrawableCompat.setTintMode(mutate, this.mButtonTintMode);
            }
            if (mutate.isStateful()) {
                mutate.setState(((View)this.mView).getDrawableState());
            }
            this.mView.setButtonDrawable(mutate);
        }
    }
    
    int getCompoundPaddingLeft(final int n) {
        return n;
    }
    
    ColorStateList getSupportButtonTintList() {
        return this.mButtonTintList;
    }
    
    PorterDuff$Mode getSupportButtonTintMode() {
        return this.mButtonTintMode;
    }
    
    void loadFromAttributes(@Nullable final AttributeSet set, int n) {
        final Context context = ((View)this.mView).getContext();
        final int[] compoundButton = R.styleable.CompoundButton;
        final TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(context, set, compoundButton, n, 0);
        final CompoundButton mView = this.mView;
        ViewCompat.saveAttributeDataForStyleable((View)mView, ((View)mView).getContext(), compoundButton, set, obtainStyledAttributes.getWrappedTypeArray(), n, 0);
        try {
            n = R.styleable.CompoundButton_buttonCompat;
            Label_0096: {
                if (obtainStyledAttributes.hasValue(n)) {
                    n = obtainStyledAttributes.getResourceId(n, 0);
                    if (n != 0) {
                        try {
                            final CompoundButton mView2 = this.mView;
                            mView2.setButtonDrawable(AppCompatResources.getDrawable(((View)mView2).getContext(), n));
                            n = 1;
                            break Label_0096;
                        }
                        catch (final Resources$NotFoundException ex) {}
                    }
                }
                n = 0;
            }
            if (n == 0) {
                n = R.styleable.CompoundButton_android_button;
                if (obtainStyledAttributes.hasValue(n)) {
                    n = obtainStyledAttributes.getResourceId(n, 0);
                    if (n != 0) {
                        final CompoundButton mView3 = this.mView;
                        mView3.setButtonDrawable(AppCompatResources.getDrawable(((View)mView3).getContext(), n));
                    }
                }
            }
            n = R.styleable.CompoundButton_buttonTint;
            if (obtainStyledAttributes.hasValue(n)) {
                CompoundButtonCompat.setButtonTintList(this.mView, obtainStyledAttributes.getColorStateList(n));
            }
            n = R.styleable.CompoundButton_buttonTintMode;
            if (obtainStyledAttributes.hasValue(n)) {
                CompoundButtonCompat.setButtonTintMode(this.mView, DrawableUtils.parseTintMode(obtainStyledAttributes.getInt(n, -1), null));
            }
        }
        finally {
            obtainStyledAttributes.recycle();
        }
    }
    
    void onSetButtonDrawable() {
        if (this.mSkipNextApply) {
            this.mSkipNextApply = false;
            return;
        }
        this.mSkipNextApply = true;
        this.applyButtonTint();
    }
    
    void setSupportButtonTintList(final ColorStateList mButtonTintList) {
        this.mButtonTintList = mButtonTintList;
        this.mHasButtonTint = true;
        this.applyButtonTint();
    }
    
    void setSupportButtonTintMode(@Nullable final PorterDuff$Mode mButtonTintMode) {
        this.mButtonTintMode = mButtonTintMode;
        this.mHasButtonTintMode = true;
        this.applyButtonTint();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.content.res.TypedArray;
import android.view.View;
import android.text.method.TransformationMethod;
import androidx.appcompat.R;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.text.InputFilter;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.emoji2.viewsintegration.EmojiTextViewHelper;

class AppCompatEmojiTextHelper
{
    @NonNull
    private final EmojiTextViewHelper mEmojiTextViewHelper;
    @NonNull
    private final TextView mView;
    
    AppCompatEmojiTextHelper(@NonNull final TextView mView) {
        this.mView = mView;
        this.mEmojiTextViewHelper = new EmojiTextViewHelper(mView, false);
    }
    
    @NonNull
    InputFilter[] getFilters(@NonNull final InputFilter[] array) {
        return this.mEmojiTextViewHelper.getFilters(array);
    }
    
    public boolean isEnabled() {
        return this.mEmojiTextViewHelper.isEnabled();
    }
    
    void loadFromAttributes(@Nullable AttributeSet obtainStyledAttributes, int appCompatTextView_emojiCompatEnabled) {
        obtainStyledAttributes = (AttributeSet)((View)this.mView).getContext().obtainStyledAttributes(obtainStyledAttributes, R.styleable.AppCompatTextView, appCompatTextView_emojiCompatEnabled, 0);
        try {
            appCompatTextView_emojiCompatEnabled = R.styleable.AppCompatTextView_emojiCompatEnabled;
            final boolean hasValue = ((TypedArray)obtainStyledAttributes).hasValue(appCompatTextView_emojiCompatEnabled);
            boolean boolean1 = true;
            if (hasValue) {
                boolean1 = ((TypedArray)obtainStyledAttributes).getBoolean(appCompatTextView_emojiCompatEnabled, true);
            }
            ((TypedArray)obtainStyledAttributes).recycle();
            this.setEnabled(boolean1);
        }
        finally {
            ((TypedArray)obtainStyledAttributes).recycle();
        }
    }
    
    void setAllCaps(final boolean allCaps) {
        this.mEmojiTextViewHelper.setAllCaps(allCaps);
    }
    
    void setEnabled(final boolean enabled) {
        this.mEmojiTextViewHelper.setEnabled(enabled);
    }
    
    @Nullable
    public TransformationMethod wrapTransformationMethod(@Nullable final TransformationMethod transformationMethod) {
        return this.mEmojiTextViewHelper.wrapTransformationMethod(transformationMethod);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.widget.AbsSeekBar;
import android.graphics.drawable.Drawable$Callback;
import android.content.Context;
import android.view.View;
import androidx.core.view.ViewCompat;
import androidx.appcompat.R;
import android.util.AttributeSet;
import androidx.annotation.Nullable;
import android.graphics.Canvas;
import androidx.core.graphics.drawable.DrawableCompat;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;

class AppCompatSeekBarHelper extends AppCompatProgressBarHelper
{
    private boolean mHasTickMarkTint;
    private boolean mHasTickMarkTintMode;
    private Drawable mTickMark;
    private ColorStateList mTickMarkTintList;
    private PorterDuff$Mode mTickMarkTintMode;
    private final SeekBar mView;
    
    AppCompatSeekBarHelper(final SeekBar mView) {
        super((ProgressBar)mView);
        this.mTickMarkTintList = null;
        this.mTickMarkTintMode = null;
        this.mHasTickMarkTint = false;
        this.mHasTickMarkTintMode = false;
        this.mView = mView;
    }
    
    private void applyTickMarkTint() {
        final Drawable mTickMark = this.mTickMark;
        if (mTickMark != null && (this.mHasTickMarkTint || this.mHasTickMarkTintMode)) {
            final Drawable wrap = DrawableCompat.wrap(mTickMark.mutate());
            this.mTickMark = wrap;
            if (this.mHasTickMarkTint) {
                DrawableCompat.setTintList(wrap, this.mTickMarkTintList);
            }
            if (this.mHasTickMarkTintMode) {
                DrawableCompat.setTintMode(this.mTickMark, this.mTickMarkTintMode);
            }
            if (this.mTickMark.isStateful()) {
                this.mTickMark.setState(((View)this.mView).getDrawableState());
            }
        }
    }
    
    void drawTickMarks(final Canvas canvas) {
        if (this.mTickMark != null) {
            final int max = ((ProgressBar)this.mView).getMax();
            int n = 1;
            if (max > 1) {
                final int intrinsicWidth = this.mTickMark.getIntrinsicWidth();
                final int intrinsicHeight = this.mTickMark.getIntrinsicHeight();
                int n2;
                if (intrinsicWidth >= 0) {
                    n2 = intrinsicWidth / 2;
                }
                else {
                    n2 = 1;
                }
                if (intrinsicHeight >= 0) {
                    n = intrinsicHeight / 2;
                }
                this.mTickMark.setBounds(-n2, -n, n2, n);
                final float n3 = (((View)this.mView).getWidth() - ((View)this.mView).getPaddingLeft() - ((View)this.mView).getPaddingRight()) / (float)max;
                final int save = canvas.save();
                canvas.translate((float)((View)this.mView).getPaddingLeft(), (float)(((View)this.mView).getHeight() / 2));
                for (int i = 0; i <= max; ++i) {
                    this.mTickMark.draw(canvas);
                    canvas.translate(n3, 0.0f);
                }
                canvas.restoreToCount(save);
            }
        }
    }
    
    void drawableStateChanged() {
        final Drawable mTickMark = this.mTickMark;
        if (mTickMark != null && mTickMark.isStateful() && mTickMark.setState(((View)this.mView).getDrawableState())) {
            ((View)this.mView).invalidateDrawable(mTickMark);
        }
    }
    
    @Nullable
    Drawable getTickMark() {
        return this.mTickMark;
    }
    
    @Nullable
    ColorStateList getTickMarkTintList() {
        return this.mTickMarkTintList;
    }
    
    @Nullable
    PorterDuff$Mode getTickMarkTintMode() {
        return this.mTickMarkTintMode;
    }
    
    void jumpDrawablesToCurrentState() {
        final Drawable mTickMark = this.mTickMark;
        if (mTickMark != null) {
            mTickMark.jumpToCurrentState();
        }
    }
    
    @Override
    void loadFromAttributes(final AttributeSet set, int n) {
        super.loadFromAttributes(set, n);
        final Context context = ((View)this.mView).getContext();
        final int[] appCompatSeekBar = R.styleable.AppCompatSeekBar;
        final TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(context, set, appCompatSeekBar, n, 0);
        final SeekBar mView = this.mView;
        ViewCompat.saveAttributeDataForStyleable((View)mView, ((View)mView).getContext(), appCompatSeekBar, set, obtainStyledAttributes.getWrappedTypeArray(), n, 0);
        final Drawable drawableIfKnown = obtainStyledAttributes.getDrawableIfKnown(R.styleable.AppCompatSeekBar_android_thumb);
        if (drawableIfKnown != null) {
            ((AbsSeekBar)this.mView).setThumb(drawableIfKnown);
        }
        this.setTickMark(obtainStyledAttributes.getDrawable(R.styleable.AppCompatSeekBar_tickMark));
        n = R.styleable.AppCompatSeekBar_tickMarkTintMode;
        if (obtainStyledAttributes.hasValue(n)) {
            this.mTickMarkTintMode = DrawableUtils.parseTintMode(obtainStyledAttributes.getInt(n, -1), this.mTickMarkTintMode);
            this.mHasTickMarkTintMode = true;
        }
        n = R.styleable.AppCompatSeekBar_tickMarkTint;
        if (obtainStyledAttributes.hasValue(n)) {
            this.mTickMarkTintList = obtainStyledAttributes.getColorStateList(n);
            this.mHasTickMarkTint = true;
        }
        obtainStyledAttributes.recycle();
        this.applyTickMarkTint();
    }
    
    void setTickMark(@Nullable final Drawable mTickMark) {
        final Drawable mTickMark2 = this.mTickMark;
        if (mTickMark2 != null) {
            mTickMark2.setCallback((Drawable$Callback)null);
        }
        if ((this.mTickMark = mTickMark) != null) {
            mTickMark.setCallback((Drawable$Callback)this.mView);
            DrawableCompat.setLayoutDirection(mTickMark, ViewCompat.getLayoutDirection((View)this.mView));
            if (mTickMark.isStateful()) {
                mTickMark.setState(((View)this.mView).getDrawableState());
            }
            this.applyTickMarkTint();
        }
        ((View)this.mView).invalidate();
    }
    
    void setTickMarkTintList(@Nullable final ColorStateList mTickMarkTintList) {
        this.mTickMarkTintList = mTickMarkTintList;
        this.mHasTickMarkTint = true;
        this.applyTickMarkTint();
    }
    
    void setTickMarkTintMode(@Nullable final PorterDuff$Mode mTickMarkTintMode) {
        this.mTickMarkTintMode = mTickMarkTintMode;
        this.mHasTickMarkTintMode = true;
        this.applyTickMarkTint();
    }
}

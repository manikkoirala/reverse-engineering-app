// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.content.res.TypedArray;
import androidx.appcompat.R;
import android.util.AttributeSet;
import android.text.method.NumberKeyListener;
import androidx.annotation.Nullable;
import android.text.method.KeyListener;
import android.widget.EditText;
import androidx.annotation.NonNull;
import androidx.emoji2.viewsintegration.EmojiEditTextHelper;

class AppCompatEmojiEditTextHelper
{
    @NonNull
    private final EmojiEditTextHelper mEmojiEditTextHelper;
    @NonNull
    private final EditText mView;
    
    AppCompatEmojiEditTextHelper(@NonNull final EditText mView) {
        this.mView = mView;
        this.mEmojiEditTextHelper = new EmojiEditTextHelper(mView, false);
    }
    
    @Nullable
    KeyListener getKeyListener(@Nullable final KeyListener keyListener) {
        KeyListener keyListener2 = keyListener;
        if (this.isEmojiCapableKeyListener(keyListener)) {
            keyListener2 = this.mEmojiEditTextHelper.getKeyListener(keyListener);
        }
        return keyListener2;
    }
    
    boolean isEmojiCapableKeyListener(final KeyListener keyListener) {
        return keyListener instanceof NumberKeyListener ^ true;
    }
    
    boolean isEnabled() {
        return this.mEmojiEditTextHelper.isEnabled();
    }
    
    void loadFromAttributes(@Nullable final AttributeSet set, int appCompatTextView_emojiCompatEnabled) {
        final TypedArray obtainStyledAttributes = ((View)this.mView).getContext().obtainStyledAttributes(set, R.styleable.AppCompatTextView, appCompatTextView_emojiCompatEnabled, 0);
        try {
            appCompatTextView_emojiCompatEnabled = R.styleable.AppCompatTextView_emojiCompatEnabled;
            final boolean hasValue = obtainStyledAttributes.hasValue(appCompatTextView_emojiCompatEnabled);
            boolean boolean1 = true;
            if (hasValue) {
                boolean1 = obtainStyledAttributes.getBoolean(appCompatTextView_emojiCompatEnabled, true);
            }
            obtainStyledAttributes.recycle();
            this.setEnabled(boolean1);
        }
        finally {
            obtainStyledAttributes.recycle();
        }
    }
    
    @Nullable
    InputConnection onCreateInputConnection(@Nullable final InputConnection inputConnection, @NonNull final EditorInfo editorInfo) {
        return this.mEmojiEditTextHelper.onCreateInputConnection(inputConnection, editorInfo);
    }
    
    void setEnabled(final boolean enabled) {
        this.mEmojiEditTextHelper.setEnabled(enabled);
    }
}

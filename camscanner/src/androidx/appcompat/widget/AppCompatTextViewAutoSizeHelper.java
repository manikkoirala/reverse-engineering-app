// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.text.Layout;
import android.graphics.Paint;
import android.text.TextDirectionHeuristics;
import android.text.TextDirectionHeuristic;
import android.text.StaticLayout$Builder;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import androidx.core.view.ViewCompat;
import androidx.appcompat.R;
import android.util.AttributeSet;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.RestrictTo;
import android.text.method.TransformationMethod;
import android.content.res.TypedArray;
import android.view.View;
import androidx.annotation.Nullable;
import android.text.StaticLayout;
import android.text.Layout$Alignment;
import java.util.List;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Arrays;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.widget.TextView;
import android.text.TextPaint;
import android.content.Context;
import java.lang.reflect.Method;
import android.annotation.SuppressLint;
import java.lang.reflect.Field;
import java.util.concurrent.ConcurrentHashMap;
import android.graphics.RectF;

class AppCompatTextViewAutoSizeHelper
{
    private static final int DEFAULT_AUTO_SIZE_GRANULARITY_IN_PX = 1;
    private static final int DEFAULT_AUTO_SIZE_MAX_TEXT_SIZE_IN_SP = 112;
    private static final int DEFAULT_AUTO_SIZE_MIN_TEXT_SIZE_IN_SP = 12;
    private static final String TAG = "ACTVAutoSizeHelper";
    private static final RectF TEMP_RECTF;
    static final float UNSET_AUTO_SIZE_UNIFORM_CONFIGURATION_VALUE = -1.0f;
    private static final int VERY_WIDE = 1048576;
    @SuppressLint({ "BanConcurrentHashMap" })
    private static ConcurrentHashMap<String, Field> sTextViewFieldByNameCache;
    @SuppressLint({ "BanConcurrentHashMap" })
    private static ConcurrentHashMap<String, Method> sTextViewMethodByNameCache;
    private float mAutoSizeMaxTextSizeInPx;
    private float mAutoSizeMinTextSizeInPx;
    private float mAutoSizeStepGranularityInPx;
    private int[] mAutoSizeTextSizesInPx;
    private int mAutoSizeTextType;
    private final Context mContext;
    private boolean mHasPresetAutoSizeValues;
    private final Impl mImpl;
    private boolean mNeedsAutoSizeText;
    private TextPaint mTempTextPaint;
    @NonNull
    private final TextView mTextView;
    
    static {
        TEMP_RECTF = new RectF();
        AppCompatTextViewAutoSizeHelper.sTextViewMethodByNameCache = new ConcurrentHashMap<String, Method>();
        AppCompatTextViewAutoSizeHelper.sTextViewFieldByNameCache = new ConcurrentHashMap<String, Field>();
    }
    
    AppCompatTextViewAutoSizeHelper(@NonNull final TextView mTextView) {
        this.mAutoSizeTextType = 0;
        this.mNeedsAutoSizeText = false;
        this.mAutoSizeStepGranularityInPx = -1.0f;
        this.mAutoSizeMinTextSizeInPx = -1.0f;
        this.mAutoSizeMaxTextSizeInPx = -1.0f;
        this.mAutoSizeTextSizesInPx = new int[0];
        this.mHasPresetAutoSizeValues = false;
        this.mTextView = mTextView;
        this.mContext = ((View)mTextView).getContext();
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 29) {
            this.mImpl = (Impl)new Impl29();
        }
        else if (sdk_INT >= 23) {
            this.mImpl = (Impl)new Impl23();
        }
        else {
            this.mImpl = new Impl();
        }
    }
    
    private static <T> T accessAndReturnWithDefault(@NonNull Object value, @NonNull final String str, @NonNull final T t) {
        try {
            final Field textViewField = getTextViewField(str);
            if (textViewField == null) {
                return t;
            }
            value = textViewField.get(value);
            return (T)value;
        }
        catch (final IllegalAccessException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to access TextView#");
            sb.append(str);
            sb.append(" member");
            return t;
        }
    }
    
    private int[] cleanupAutoSizePresetSizes(int[] a) {
        final int length = a.length;
        if (length == 0) {
            return a;
        }
        Arrays.sort(a);
        final ArrayList list = new ArrayList();
        final int n = 0;
        for (final int n2 : a) {
            if (n2 > 0 && Collections.binarySearch(list, n2) < 0) {
                list.add(n2);
            }
        }
        if (length == list.size()) {
            return a;
        }
        final int size = list.size();
        a = new int[size];
        for (int j = n; j < size; ++j) {
            a[j] = (int)list.get(j);
        }
        return a;
    }
    
    private void clearAutoSizeConfiguration() {
        this.mAutoSizeTextType = 0;
        this.mAutoSizeMinTextSizeInPx = -1.0f;
        this.mAutoSizeMaxTextSizeInPx = -1.0f;
        this.mAutoSizeStepGranularityInPx = -1.0f;
        this.mAutoSizeTextSizesInPx = new int[0];
        this.mNeedsAutoSizeText = false;
    }
    
    private StaticLayout createStaticLayoutForMeasuringPre16(final CharSequence charSequence, final Layout$Alignment layout$Alignment, final int n) {
        return new StaticLayout(charSequence, this.mTempTextPaint, n, layout$Alignment, (float)accessAndReturnWithDefault(this.mTextView, "mSpacingMult", 1.0f), (float)accessAndReturnWithDefault(this.mTextView, "mSpacingAdd", 0.0f), (boolean)accessAndReturnWithDefault(this.mTextView, "mIncludePad", Boolean.TRUE));
    }
    
    private int findLargestTextSizeWhichFits(final RectF rectF) {
        final int length = this.mAutoSizeTextSizesInPx.length;
        if (length != 0) {
            int i = 1;
            int n = length - 1;
            int n2 = 0;
            while (i <= n) {
                final int n3 = (i + n) / 2;
                if (this.suggestedSizeFitsInSpace(this.mAutoSizeTextSizesInPx[n3], rectF)) {
                    n2 = i;
                    i = n3 + 1;
                }
                else {
                    n2 = (n = n3 - 1);
                }
            }
            return this.mAutoSizeTextSizesInPx[n2];
        }
        throw new IllegalStateException("No available text sizes to choose from.");
    }
    
    @Nullable
    private static Field getTextViewField(@NonNull final String s) {
        try {
            Field field;
            if ((field = AppCompatTextViewAutoSizeHelper.sTextViewFieldByNameCache.get(s)) == null) {
                final Field declaredField = TextView.class.getDeclaredField(s);
                if ((field = declaredField) != null) {
                    declaredField.setAccessible(true);
                    AppCompatTextViewAutoSizeHelper.sTextViewFieldByNameCache.put(s, declaredField);
                    field = declaredField;
                }
            }
            return field;
        }
        catch (final NoSuchFieldException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to access TextView#");
            sb.append(s);
            sb.append(" member");
            return null;
        }
    }
    
    @Nullable
    private static Method getTextViewMethod(@NonNull final String s) {
        try {
            Method method;
            if ((method = AppCompatTextViewAutoSizeHelper.sTextViewMethodByNameCache.get(s)) == null) {
                final Method declaredMethod = TextView.class.getDeclaredMethod(s, (Class<?>[])new Class[0]);
                if ((method = declaredMethod) != null) {
                    declaredMethod.setAccessible(true);
                    AppCompatTextViewAutoSizeHelper.sTextViewMethodByNameCache.put(s, declaredMethod);
                    method = declaredMethod;
                }
            }
            return method;
        }
        catch (final Exception ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to retrieve TextView#");
            sb.append(s);
            sb.append("() method");
            return null;
        }
    }
    
    static <T> T invokeAndReturnWithDefault(@NonNull Object obj, @NonNull final String str, @NonNull T invoke) {
        try {
            try {
                obj = (invoke = (T)getTextViewMethod(str).invoke(obj, new Object[0]));
            }
            finally {}
        }
        catch (final Exception ex) {
            obj = new StringBuilder();
            ((StringBuilder)obj).append("Failed to invoke TextView#");
            ((StringBuilder)obj).append(str);
            ((StringBuilder)obj).append("() method");
        }
        return invoke;
    }
    
    private void setRawTextSize(final float textSize) {
        if (textSize != ((Paint)this.mTextView.getPaint()).getTextSize()) {
            ((Paint)this.mTextView.getPaint()).setTextSize(textSize);
            final boolean inLayout = Api18Impl.isInLayout((View)this.mTextView);
            if (this.mTextView.getLayout() != null) {
                this.mNeedsAutoSizeText = false;
                try {
                    final Method textViewMethod = getTextViewMethod("nullLayouts");
                    if (textViewMethod != null) {
                        textViewMethod.invoke(this.mTextView, new Object[0]);
                    }
                }
                catch (final Exception ex) {}
                if (!inLayout) {
                    ((View)this.mTextView).requestLayout();
                }
                else {
                    ((View)this.mTextView).forceLayout();
                }
                ((View)this.mTextView).invalidate();
            }
        }
    }
    
    private boolean setupAutoSizeText() {
        final boolean supportsAutoSizeText = this.supportsAutoSizeText();
        int i = 0;
        if (supportsAutoSizeText && this.mAutoSizeTextType == 1) {
            if (!this.mHasPresetAutoSizeValues || this.mAutoSizeTextSizesInPx.length == 0) {
                final int n = (int)Math.floor((this.mAutoSizeMaxTextSizeInPx - this.mAutoSizeMinTextSizeInPx) / this.mAutoSizeStepGranularityInPx) + 1;
                final int[] array = new int[n];
                while (i < n) {
                    array[i] = Math.round(this.mAutoSizeMinTextSizeInPx + i * this.mAutoSizeStepGranularityInPx);
                    ++i;
                }
                this.mAutoSizeTextSizesInPx = this.cleanupAutoSizePresetSizes(array);
            }
            this.mNeedsAutoSizeText = true;
        }
        else {
            this.mNeedsAutoSizeText = false;
        }
        return this.mNeedsAutoSizeText;
    }
    
    private void setupAutoSizeUniformPresetSizes(final TypedArray typedArray) {
        final int length = typedArray.length();
        final int[] array = new int[length];
        if (length > 0) {
            for (int i = 0; i < length; ++i) {
                array[i] = typedArray.getDimensionPixelSize(i, -1);
            }
            this.mAutoSizeTextSizesInPx = this.cleanupAutoSizePresetSizes(array);
            this.setupAutoSizeUniformPresetSizesConfiguration();
        }
    }
    
    private boolean setupAutoSizeUniformPresetSizesConfiguration() {
        final int[] mAutoSizeTextSizesInPx = this.mAutoSizeTextSizesInPx;
        final int length = mAutoSizeTextSizesInPx.length;
        final boolean mHasPresetAutoSizeValues = length > 0;
        this.mHasPresetAutoSizeValues = mHasPresetAutoSizeValues;
        if (mHasPresetAutoSizeValues) {
            this.mAutoSizeTextType = 1;
            this.mAutoSizeMinTextSizeInPx = (float)mAutoSizeTextSizesInPx[0];
            this.mAutoSizeMaxTextSizeInPx = (float)mAutoSizeTextSizesInPx[length - 1];
            this.mAutoSizeStepGranularityInPx = -1.0f;
        }
        return mHasPresetAutoSizeValues;
    }
    
    private boolean suggestedSizeFitsInSpace(final int n, final RectF rectF) {
        final CharSequence text = this.mTextView.getText();
        final TransformationMethod transformationMethod = this.mTextView.getTransformationMethod();
        CharSequence charSequence = text;
        if (transformationMethod != null) {
            final CharSequence transformation = transformationMethod.getTransformation(text, (View)this.mTextView);
            charSequence = text;
            if (transformation != null) {
                charSequence = transformation;
            }
        }
        final int maxLines = Api16Impl.getMaxLines(this.mTextView);
        this.initTempTextPaint(n);
        final StaticLayout layout = this.createLayout(charSequence, invokeAndReturnWithDefault(this.mTextView, "getLayoutAlignment", Layout$Alignment.ALIGN_NORMAL), Math.round(rectF.right), maxLines);
        return (maxLines == -1 || (layout.getLineCount() <= maxLines && ((Layout)layout).getLineEnd(layout.getLineCount() - 1) == charSequence.length())) && ((Layout)layout).getHeight() <= rectF.bottom;
    }
    
    private boolean supportsAutoSizeText() {
        return this.mTextView instanceof AppCompatEditText ^ true;
    }
    
    private void validateAndSetAutoSizeTextTypeUniformConfiguration(final float mAutoSizeMinTextSizeInPx, final float n, final float n2) throws IllegalArgumentException {
        if (mAutoSizeMinTextSizeInPx <= 0.0f) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Minimum auto-size text size (");
            sb.append(mAutoSizeMinTextSizeInPx);
            sb.append("px) is less or equal to (0px)");
            throw new IllegalArgumentException(sb.toString());
        }
        if (n <= mAutoSizeMinTextSizeInPx) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Maximum auto-size text size (");
            sb2.append(n);
            sb2.append("px) is less or equal to minimum auto-size text size (");
            sb2.append(mAutoSizeMinTextSizeInPx);
            sb2.append("px)");
            throw new IllegalArgumentException(sb2.toString());
        }
        if (n2 > 0.0f) {
            this.mAutoSizeTextType = 1;
            this.mAutoSizeMinTextSizeInPx = mAutoSizeMinTextSizeInPx;
            this.mAutoSizeMaxTextSizeInPx = n;
            this.mAutoSizeStepGranularityInPx = n2;
            this.mHasPresetAutoSizeValues = false;
            return;
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("The auto-size step granularity (");
        sb3.append(n2);
        sb3.append("px) is less or equal to (0px)");
        throw new IllegalArgumentException(sb3.toString());
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    void autoSizeText() {
        if (!this.isAutoSizeEnabled()) {
            return;
        }
        Label_0185: {
            if (this.mNeedsAutoSizeText) {
                if (((View)this.mTextView).getMeasuredHeight() > 0) {
                    if (((View)this.mTextView).getMeasuredWidth() > 0) {
                        int n;
                        if (this.mImpl.isHorizontallyScrollable(this.mTextView)) {
                            n = 1048576;
                        }
                        else {
                            n = ((View)this.mTextView).getMeasuredWidth() - this.mTextView.getTotalPaddingLeft() - this.mTextView.getTotalPaddingRight();
                        }
                        final int n2 = ((View)this.mTextView).getHeight() - this.mTextView.getCompoundPaddingBottom() - this.mTextView.getCompoundPaddingTop();
                        if (n > 0) {
                            if (n2 > 0) {
                                final RectF temp_RECTF = AppCompatTextViewAutoSizeHelper.TEMP_RECTF;
                                synchronized (temp_RECTF) {
                                    temp_RECTF.setEmpty();
                                    temp_RECTF.right = (float)n;
                                    temp_RECTF.bottom = (float)n2;
                                    final float n3 = (float)this.findLargestTextSizeWhichFits(temp_RECTF);
                                    if (n3 != this.mTextView.getTextSize()) {
                                        this.setTextSizeInternal(0, n3);
                                    }
                                    break Label_0185;
                                }
                            }
                        }
                    }
                }
                return;
            }
        }
        this.mNeedsAutoSizeText = true;
    }
    
    @NonNull
    @VisibleForTesting
    StaticLayout createLayout(@NonNull final CharSequence charSequence, @NonNull final Layout$Alignment layout$Alignment, final int n, final int n2) {
        if (Build$VERSION.SDK_INT >= 23) {
            return Api23Impl.createStaticLayoutForMeasuring(charSequence, layout$Alignment, n, n2, this.mTextView, this.mTempTextPaint, this.mImpl);
        }
        return Api16Impl.createStaticLayoutForMeasuring(charSequence, layout$Alignment, n, this.mTextView, this.mTempTextPaint);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    int getAutoSizeMaxTextSize() {
        return Math.round(this.mAutoSizeMaxTextSizeInPx);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    int getAutoSizeMinTextSize() {
        return Math.round(this.mAutoSizeMinTextSizeInPx);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    int getAutoSizeStepGranularity() {
        return Math.round(this.mAutoSizeStepGranularityInPx);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    int[] getAutoSizeTextAvailableSizes() {
        return this.mAutoSizeTextSizesInPx;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    int getAutoSizeTextType() {
        return this.mAutoSizeTextType;
    }
    
    @VisibleForTesting
    void initTempTextPaint(final int n) {
        final TextPaint mTempTextPaint = this.mTempTextPaint;
        if (mTempTextPaint == null) {
            this.mTempTextPaint = new TextPaint();
        }
        else {
            ((Paint)mTempTextPaint).reset();
        }
        this.mTempTextPaint.set(this.mTextView.getPaint());
        ((Paint)this.mTempTextPaint).setTextSize((float)n);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    boolean isAutoSizeEnabled() {
        return this.supportsAutoSizeText() && this.mAutoSizeTextType != 0;
    }
    
    void loadFromAttributes(@Nullable final AttributeSet set, int n) {
        final Context mContext = this.mContext;
        final int[] appCompatTextView = R.styleable.AppCompatTextView;
        final TypedArray obtainStyledAttributes = mContext.obtainStyledAttributes(set, appCompatTextView, n, 0);
        final TextView mTextView = this.mTextView;
        ViewCompat.saveAttributeDataForStyleable((View)mTextView, ((View)mTextView).getContext(), appCompatTextView, set, obtainStyledAttributes, n, 0);
        n = R.styleable.AppCompatTextView_autoSizeTextType;
        if (obtainStyledAttributes.hasValue(n)) {
            this.mAutoSizeTextType = obtainStyledAttributes.getInt(n, 0);
        }
        n = R.styleable.AppCompatTextView_autoSizeStepGranularity;
        float dimension;
        if (obtainStyledAttributes.hasValue(n)) {
            dimension = obtainStyledAttributes.getDimension(n, -1.0f);
        }
        else {
            dimension = -1.0f;
        }
        n = R.styleable.AppCompatTextView_autoSizeMinTextSize;
        float dimension2;
        if (obtainStyledAttributes.hasValue(n)) {
            dimension2 = obtainStyledAttributes.getDimension(n, -1.0f);
        }
        else {
            dimension2 = -1.0f;
        }
        n = R.styleable.AppCompatTextView_autoSizeMaxTextSize;
        float dimension3;
        if (obtainStyledAttributes.hasValue(n)) {
            dimension3 = obtainStyledAttributes.getDimension(n, -1.0f);
        }
        else {
            dimension3 = -1.0f;
        }
        n = R.styleable.AppCompatTextView_autoSizePresetSizes;
        if (obtainStyledAttributes.hasValue(n)) {
            n = obtainStyledAttributes.getResourceId(n, 0);
            if (n > 0) {
                final TypedArray obtainTypedArray = obtainStyledAttributes.getResources().obtainTypedArray(n);
                this.setupAutoSizeUniformPresetSizes(obtainTypedArray);
                obtainTypedArray.recycle();
            }
        }
        obtainStyledAttributes.recycle();
        if (this.supportsAutoSizeText()) {
            if (this.mAutoSizeTextType == 1) {
                if (!this.mHasPresetAutoSizeValues) {
                    final DisplayMetrics displayMetrics = this.mContext.getResources().getDisplayMetrics();
                    float applyDimension = dimension2;
                    if (dimension2 == -1.0f) {
                        applyDimension = TypedValue.applyDimension(2, 12.0f, displayMetrics);
                    }
                    float applyDimension2 = dimension3;
                    if (dimension3 == -1.0f) {
                        applyDimension2 = TypedValue.applyDimension(2, 112.0f, displayMetrics);
                    }
                    float n2 = dimension;
                    if (dimension == -1.0f) {
                        n2 = 1.0f;
                    }
                    this.validateAndSetAutoSizeTextTypeUniformConfiguration(applyDimension, applyDimension2, n2);
                }
                this.setupAutoSizeText();
            }
        }
        else {
            this.mAutoSizeTextType = 0;
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    void setAutoSizeTextTypeUniformWithConfiguration(final int n, final int n2, final int n3, final int n4) throws IllegalArgumentException {
        if (this.supportsAutoSizeText()) {
            final DisplayMetrics displayMetrics = this.mContext.getResources().getDisplayMetrics();
            this.validateAndSetAutoSizeTextTypeUniformConfiguration(TypedValue.applyDimension(n4, (float)n, displayMetrics), TypedValue.applyDimension(n4, (float)n2, displayMetrics), TypedValue.applyDimension(n4, (float)n3, displayMetrics));
            if (this.setupAutoSizeText()) {
                this.autoSizeText();
            }
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    void setAutoSizeTextTypeUniformWithPresetSizes(@NonNull final int[] array, final int n) throws IllegalArgumentException {
        if (this.supportsAutoSizeText()) {
            final int length = array.length;
            int n2 = 0;
            if (length > 0) {
                final int[] array2 = new int[length];
                int[] copy;
                if (n == 0) {
                    copy = Arrays.copyOf(array, length);
                }
                else {
                    final DisplayMetrics displayMetrics = this.mContext.getResources().getDisplayMetrics();
                    while (true) {
                        copy = array2;
                        if (n2 >= length) {
                            break;
                        }
                        array2[n2] = Math.round(TypedValue.applyDimension(n, (float)array[n2], displayMetrics));
                        ++n2;
                    }
                }
                this.mAutoSizeTextSizesInPx = this.cleanupAutoSizePresetSizes(copy);
                if (!this.setupAutoSizeUniformPresetSizesConfiguration()) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("None of the preset sizes is valid: ");
                    sb.append(Arrays.toString(array));
                    throw new IllegalArgumentException(sb.toString());
                }
            }
            else {
                this.mHasPresetAutoSizeValues = false;
            }
            if (this.setupAutoSizeText()) {
                this.autoSizeText();
            }
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    void setAutoSizeTextTypeWithDefaults(final int i) {
        if (this.supportsAutoSizeText()) {
            if (i != 0) {
                if (i != 1) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unknown auto-size text type: ");
                    sb.append(i);
                    throw new IllegalArgumentException(sb.toString());
                }
                final DisplayMetrics displayMetrics = this.mContext.getResources().getDisplayMetrics();
                this.validateAndSetAutoSizeTextTypeUniformConfiguration(TypedValue.applyDimension(2, 12.0f, displayMetrics), TypedValue.applyDimension(2, 112.0f, displayMetrics), 1.0f);
                if (this.setupAutoSizeText()) {
                    this.autoSizeText();
                }
            }
            else {
                this.clearAutoSizeConfiguration();
            }
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    void setTextSizeInternal(final int n, final float n2) {
        final Context mContext = this.mContext;
        Resources resources;
        if (mContext == null) {
            resources = Resources.getSystem();
        }
        else {
            resources = mContext.getResources();
        }
        this.setRawTextSize(TypedValue.applyDimension(n, n2, resources.getDisplayMetrics()));
    }
    
    @RequiresApi(16)
    private static final class Api16Impl
    {
        @DoNotInline
        @NonNull
        static StaticLayout createStaticLayoutForMeasuring(@NonNull final CharSequence charSequence, @NonNull final Layout$Alignment layout$Alignment, final int n, @NonNull final TextView textView, @NonNull final TextPaint textPaint) {
            return new StaticLayout(charSequence, textPaint, n, layout$Alignment, textView.getLineSpacingMultiplier(), textView.getLineSpacingExtra(), textView.getIncludeFontPadding());
        }
        
        @DoNotInline
        static int getMaxLines(@NonNull final TextView textView) {
            return textView.getMaxLines();
        }
    }
    
    @RequiresApi(18)
    private static final class Api18Impl
    {
        @DoNotInline
        static boolean isInLayout(@NonNull final View view) {
            return view.isInLayout();
        }
    }
    
    @RequiresApi(23)
    private static final class Api23Impl
    {
        @DoNotInline
        @NonNull
        static StaticLayout createStaticLayoutForMeasuring(@NonNull CharSequence \u3007080, @NonNull final Layout$Alignment layout$Alignment, int n, final int n2, @NonNull final TextView textView, @NonNull final TextPaint textPaint, @NonNull final Impl impl) {
            \u3007080 = (CharSequence)OOO\u3007O0.\u3007080(\u3007080, 0, \u3007080.length(), textPaint, n);
            final StaticLayout$Builder \u300781 = Oo8Oo00oo.\u3007080(o0ooO.\u3007080(\u300700\u30078.\u3007080(O8\u3007o.\u3007080(oo\u3007.\u3007080((StaticLayout$Builder)\u3007080, layout$Alignment), textView.getLineSpacingExtra(), textView.getLineSpacingMultiplier()), textView.getIncludeFontPadding()), \u3007o.\u3007080(textView)), o\u30078.\u3007080(textView));
            n = n2;
            if (n2 == -1) {
                n = Integer.MAX_VALUE;
            }
            \u3007\u3007\u30070\u3007\u30070.\u3007080(\u300781, n);
            try {
                impl.computeAndSetTextDirection((StaticLayout$Builder)\u3007080, textView);
                return o\u30070OOo\u30070.\u3007080((StaticLayout$Builder)\u3007080);
            }
            catch (final ClassCastException ex) {
                return o\u30070OOo\u30070.\u3007080((StaticLayout$Builder)\u3007080);
            }
        }
    }
    
    private static class Impl
    {
        Impl() {
        }
        
        void computeAndSetTextDirection(final StaticLayout$Builder staticLayout$Builder, final TextView textView) {
        }
        
        boolean isHorizontallyScrollable(final TextView textView) {
            return AppCompatTextViewAutoSizeHelper.invokeAndReturnWithDefault(textView, "getHorizontallyScrolling", Boolean.FALSE);
        }
    }
    
    @RequiresApi(23)
    private static class Impl23 extends Impl
    {
        Impl23() {
        }
        
        @Override
        void computeAndSetTextDirection(final StaticLayout$Builder staticLayout$Builder, final TextView textView) {
            \u3007\u30070o.\u3007080(staticLayout$Builder, (TextDirectionHeuristic)AppCompatTextViewAutoSizeHelper.invokeAndReturnWithDefault(textView, "getTextDirectionHeuristic", TextDirectionHeuristics.FIRSTSTRONG_LTR));
        }
    }
    
    @RequiresApi(29)
    private static class Impl29 extends Impl23
    {
        Impl29() {
        }
        
        @Override
        void computeAndSetTextDirection(final StaticLayout$Builder staticLayout$Builder, final TextView textView) {
            \u3007\u30070o.\u3007080(staticLayout$Builder, oO.\u3007080(textView));
        }
        
        @Override
        boolean isHorizontallyScrollable(final TextView textView) {
            return \u300708O8o\u30070.\u3007080(textView);
        }
    }
}

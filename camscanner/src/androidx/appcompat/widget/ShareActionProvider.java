// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.pm.PackageManager;
import android.view.MenuItem$OnMenuItemClickListener;
import android.view.SubMenu;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.R;
import android.util.TypedValue;
import android.view.View;
import android.content.Context;
import androidx.core.view.ActionProvider;

public class ShareActionProvider extends ActionProvider
{
    private static final int DEFAULT_INITIAL_ACTIVITY_COUNT = 4;
    public static final String DEFAULT_SHARE_HISTORY_FILE_NAME = "share_history.xml";
    final Context mContext;
    private int mMaxShownActivityCount;
    private ActivityChooserModel.OnChooseActivityListener mOnChooseActivityListener;
    private final ShareMenuItemOnMenuItemClickListener mOnMenuItemClickListener;
    OnShareTargetSelectedListener mOnShareTargetSelectedListener;
    String mShareHistoryFileName;
    
    public ShareActionProvider(final Context mContext) {
        super(mContext);
        this.mMaxShownActivityCount = 4;
        this.mOnMenuItemClickListener = new ShareMenuItemOnMenuItemClickListener();
        this.mShareHistoryFileName = "share_history.xml";
        this.mContext = mContext;
    }
    
    private void setActivityChooserPolicyIfNeeded() {
        if (this.mOnShareTargetSelectedListener == null) {
            return;
        }
        if (this.mOnChooseActivityListener == null) {
            this.mOnChooseActivityListener = new ShareActivityChooserModelPolicy();
        }
        ActivityChooserModel.get(this.mContext, this.mShareHistoryFileName).setOnChooseActivityListener(this.mOnChooseActivityListener);
    }
    
    @Override
    public boolean hasSubMenu() {
        return true;
    }
    
    @Override
    public View onCreateActionView() {
        final ActivityChooserView activityChooserView = new ActivityChooserView(this.mContext);
        if (!((View)activityChooserView).isInEditMode()) {
            activityChooserView.setActivityChooserModel(ActivityChooserModel.get(this.mContext, this.mShareHistoryFileName));
        }
        final TypedValue typedValue = new TypedValue();
        this.mContext.getTheme().resolveAttribute(R.attr.actionModeShareDrawable, typedValue, true);
        activityChooserView.setExpandActivityOverflowButtonDrawable(AppCompatResources.getDrawable(this.mContext, typedValue.resourceId));
        activityChooserView.setProvider(this);
        activityChooserView.setDefaultActionButtonContentDescription(R.string.abc_shareactionprovider_share_with_application);
        activityChooserView.setExpandActivityOverflowButtonContentDescription(R.string.abc_shareactionprovider_share_with);
        return (View)activityChooserView;
    }
    
    @Override
    public void onPrepareSubMenu(final SubMenu subMenu) {
        ((Menu)subMenu).clear();
        final ActivityChooserModel value = ActivityChooserModel.get(this.mContext, this.mShareHistoryFileName);
        final PackageManager packageManager = this.mContext.getPackageManager();
        final int activityCount = value.getActivityCount();
        final int min = Math.min(activityCount, this.mMaxShownActivityCount);
        for (int i = 0; i < min; ++i) {
            final ResolveInfo activity = value.getActivity(i);
            ((Menu)subMenu).add(0, i, i, activity.loadLabel(packageManager)).setIcon(activity.loadIcon(packageManager)).setOnMenuItemClickListener((MenuItem$OnMenuItemClickListener)this.mOnMenuItemClickListener);
        }
        if (min < activityCount) {
            final SubMenu addSubMenu = ((Menu)subMenu).addSubMenu(0, min, min, (CharSequence)this.mContext.getString(R.string.abc_activity_chooser_view_see_all));
            for (int j = 0; j < activityCount; ++j) {
                final ResolveInfo activity2 = value.getActivity(j);
                ((Menu)addSubMenu).add(0, j, j, activity2.loadLabel(packageManager)).setIcon(activity2.loadIcon(packageManager)).setOnMenuItemClickListener((MenuItem$OnMenuItemClickListener)this.mOnMenuItemClickListener);
            }
        }
    }
    
    public void setOnShareTargetSelectedListener(final OnShareTargetSelectedListener mOnShareTargetSelectedListener) {
        this.mOnShareTargetSelectedListener = mOnShareTargetSelectedListener;
        this.setActivityChooserPolicyIfNeeded();
    }
    
    public void setShareHistoryFileName(final String mShareHistoryFileName) {
        this.mShareHistoryFileName = mShareHistoryFileName;
        this.setActivityChooserPolicyIfNeeded();
    }
    
    public void setShareIntent(final Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if ("android.intent.action.SEND".equals(action) || "android.intent.action.SEND_MULTIPLE".equals(action)) {
                this.updateIntent(intent);
            }
        }
        ActivityChooserModel.get(this.mContext, this.mShareHistoryFileName).setIntent(intent);
    }
    
    void updateIntent(final Intent intent) {
        intent.addFlags(134742016);
    }
    
    public interface OnShareTargetSelectedListener
    {
        boolean onShareTargetSelected(final ShareActionProvider p0, final Intent p1);
    }
    
    private class ShareActivityChooserModelPolicy implements OnChooseActivityListener
    {
        final ShareActionProvider this$0;
        
        ShareActivityChooserModelPolicy(final ShareActionProvider this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public boolean onChooseActivity(final ActivityChooserModel activityChooserModel, final Intent intent) {
            final ShareActionProvider this$0 = this.this$0;
            final OnShareTargetSelectedListener mOnShareTargetSelectedListener = this$0.mOnShareTargetSelectedListener;
            if (mOnShareTargetSelectedListener != null) {
                mOnShareTargetSelectedListener.onShareTargetSelected(this$0, intent);
            }
            return false;
        }
    }
    
    private class ShareMenuItemOnMenuItemClickListener implements MenuItem$OnMenuItemClickListener
    {
        final ShareActionProvider this$0;
        
        ShareMenuItemOnMenuItemClickListener(final ShareActionProvider this$0) {
            this.this$0 = this$0;
        }
        
        public boolean onMenuItemClick(final MenuItem menuItem) {
            final ShareActionProvider this$0 = this.this$0;
            final Intent chooseActivity = ActivityChooserModel.get(this$0.mContext, this$0.mShareHistoryFileName).chooseActivity(menuItem.getItemId());
            if (chooseActivity != null) {
                final String action = chooseActivity.getAction();
                if ("android.intent.action.SEND".equals(action) || "android.intent.action.SEND_MULTIPLE".equals(action)) {
                    this.this$0.updateIntent(chooseActivity);
                }
                this.this$0.mContext.startActivity(chooseActivity);
            }
            return true;
        }
    }
}

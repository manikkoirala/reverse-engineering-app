// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.content.Context;
import androidx.appcompat.R;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.os.Build$VERSION;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import androidx.core.view.ViewCompat;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import android.view.View;

class AppCompatBackgroundHelper
{
    private int mBackgroundResId;
    private TintInfo mBackgroundTint;
    private final AppCompatDrawableManager mDrawableManager;
    private TintInfo mInternalBackgroundTint;
    private TintInfo mTmpInfo;
    @NonNull
    private final View mView;
    
    AppCompatBackgroundHelper(@NonNull final View mView) {
        this.mBackgroundResId = -1;
        this.mView = mView;
        this.mDrawableManager = AppCompatDrawableManager.get();
    }
    
    private boolean applyFrameworkTintUsingColorFilter(@NonNull final Drawable drawable) {
        if (this.mTmpInfo == null) {
            this.mTmpInfo = new TintInfo();
        }
        final TintInfo mTmpInfo = this.mTmpInfo;
        mTmpInfo.clear();
        final ColorStateList backgroundTintList = ViewCompat.getBackgroundTintList(this.mView);
        if (backgroundTintList != null) {
            mTmpInfo.mHasTintList = true;
            mTmpInfo.mTintList = backgroundTintList;
        }
        final PorterDuff$Mode backgroundTintMode = ViewCompat.getBackgroundTintMode(this.mView);
        if (backgroundTintMode != null) {
            mTmpInfo.mHasTintMode = true;
            mTmpInfo.mTintMode = backgroundTintMode;
        }
        if (!mTmpInfo.mHasTintList && !mTmpInfo.mHasTintMode) {
            return false;
        }
        AppCompatDrawableManager.tintDrawable(drawable, mTmpInfo, this.mView.getDrawableState());
        return true;
    }
    
    private boolean shouldApplyFrameworkTintUsingColorFilter() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        boolean b = true;
        if (sdk_INT > 21) {
            if (this.mInternalBackgroundTint == null) {
                b = false;
            }
            return b;
        }
        return sdk_INT == 21;
    }
    
    void applySupportBackgroundTint() {
        final Drawable background = this.mView.getBackground();
        if (background != null) {
            if (this.shouldApplyFrameworkTintUsingColorFilter() && this.applyFrameworkTintUsingColorFilter(background)) {
                return;
            }
            final TintInfo mBackgroundTint = this.mBackgroundTint;
            if (mBackgroundTint != null) {
                AppCompatDrawableManager.tintDrawable(background, mBackgroundTint, this.mView.getDrawableState());
            }
            else {
                final TintInfo mInternalBackgroundTint = this.mInternalBackgroundTint;
                if (mInternalBackgroundTint != null) {
                    AppCompatDrawableManager.tintDrawable(background, mInternalBackgroundTint, this.mView.getDrawableState());
                }
            }
        }
    }
    
    ColorStateList getSupportBackgroundTintList() {
        final TintInfo mBackgroundTint = this.mBackgroundTint;
        ColorStateList mTintList;
        if (mBackgroundTint != null) {
            mTintList = mBackgroundTint.mTintList;
        }
        else {
            mTintList = null;
        }
        return mTintList;
    }
    
    PorterDuff$Mode getSupportBackgroundTintMode() {
        final TintInfo mBackgroundTint = this.mBackgroundTint;
        PorterDuff$Mode mTintMode;
        if (mBackgroundTint != null) {
            mTintMode = mBackgroundTint.mTintMode;
        }
        else {
            mTintMode = null;
        }
        return mTintMode;
    }
    
    void loadFromAttributes(@Nullable final AttributeSet set, int n) {
        final Context context = this.mView.getContext();
        final int[] viewBackgroundHelper = R.styleable.ViewBackgroundHelper;
        final TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(context, set, viewBackgroundHelper, n, 0);
        final View mView = this.mView;
        ViewCompat.saveAttributeDataForStyleable(mView, mView.getContext(), viewBackgroundHelper, set, obtainStyledAttributes.getWrappedTypeArray(), n, 0);
        try {
            n = R.styleable.ViewBackgroundHelper_android_background;
            if (obtainStyledAttributes.hasValue(n)) {
                this.mBackgroundResId = obtainStyledAttributes.getResourceId(n, -1);
                final ColorStateList tintList = this.mDrawableManager.getTintList(this.mView.getContext(), this.mBackgroundResId);
                if (tintList != null) {
                    this.setInternalBackgroundTint(tintList);
                }
            }
            n = R.styleable.ViewBackgroundHelper_backgroundTint;
            if (obtainStyledAttributes.hasValue(n)) {
                ViewCompat.setBackgroundTintList(this.mView, obtainStyledAttributes.getColorStateList(n));
            }
            n = R.styleable.ViewBackgroundHelper_backgroundTintMode;
            if (obtainStyledAttributes.hasValue(n)) {
                ViewCompat.setBackgroundTintMode(this.mView, DrawableUtils.parseTintMode(obtainStyledAttributes.getInt(n, -1), null));
            }
        }
        finally {
            obtainStyledAttributes.recycle();
        }
    }
    
    void onSetBackgroundDrawable(final Drawable drawable) {
        this.mBackgroundResId = -1;
        this.setInternalBackgroundTint(null);
        this.applySupportBackgroundTint();
    }
    
    void onSetBackgroundResource(final int mBackgroundResId) {
        this.mBackgroundResId = mBackgroundResId;
        final AppCompatDrawableManager mDrawableManager = this.mDrawableManager;
        ColorStateList tintList;
        if (mDrawableManager != null) {
            tintList = mDrawableManager.getTintList(this.mView.getContext(), mBackgroundResId);
        }
        else {
            tintList = null;
        }
        this.setInternalBackgroundTint(tintList);
        this.applySupportBackgroundTint();
    }
    
    void setInternalBackgroundTint(final ColorStateList mTintList) {
        if (mTintList != null) {
            if (this.mInternalBackgroundTint == null) {
                this.mInternalBackgroundTint = new TintInfo();
            }
            final TintInfo mInternalBackgroundTint = this.mInternalBackgroundTint;
            mInternalBackgroundTint.mTintList = mTintList;
            mInternalBackgroundTint.mHasTintList = true;
        }
        else {
            this.mInternalBackgroundTint = null;
        }
        this.applySupportBackgroundTint();
    }
    
    void setSupportBackgroundTintList(final ColorStateList mTintList) {
        if (this.mBackgroundTint == null) {
            this.mBackgroundTint = new TintInfo();
        }
        final TintInfo mBackgroundTint = this.mBackgroundTint;
        mBackgroundTint.mTintList = mTintList;
        mBackgroundTint.mHasTintList = true;
        this.applySupportBackgroundTint();
    }
    
    void setSupportBackgroundTintMode(final PorterDuff$Mode mTintMode) {
        if (this.mBackgroundTint == null) {
            this.mBackgroundTint = new TintInfo();
        }
        final TintInfo mBackgroundTint = this.mBackgroundTint;
        mBackgroundTint.mTintMode = mTintMode;
        mBackgroundTint.mHasTintMode = true;
        this.applySupportBackgroundTint();
    }
}

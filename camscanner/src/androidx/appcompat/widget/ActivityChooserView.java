// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.widget.AbsListView;
import android.database.Observable;
import android.widget.LinearLayout;
import android.widget.AdapterView;
import android.content.Intent;
import android.widget.TextView;
import android.widget.BaseAdapter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.ColorDrawable;
import android.view.View$MeasureSpec;
import android.widget.AdapterView$OnItemClickListener;
import android.widget.ListAdapter;
import android.view.ViewTreeObserver;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.view.View$OnTouchListener;
import androidx.appcompat.view.menu.ShowableListMenu;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.View$AccessibilityDelegate;
import android.view.View$OnLongClickListener;
import android.view.View$OnClickListener;
import android.view.LayoutInflater;
import androidx.core.view.ViewCompat;
import androidx.appcompat.R;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.core.view.ActionProvider;
import android.view.ViewTreeObserver$OnGlobalLayoutListener;
import android.widget.PopupWindow$OnDismissListener;
import android.database.DataSetObserver;
import android.widget.ImageView;
import android.widget.FrameLayout;
import android.graphics.drawable.Drawable;
import android.view.View;
import androidx.annotation.RestrictTo;
import android.view.ViewGroup;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public class ActivityChooserView extends ViewGroup implements ActivityChooserModelClient
{
    private final View mActivityChooserContent;
    private final Drawable mActivityChooserContentBackground;
    final ActivityChooserViewAdapter mAdapter;
    private final Callbacks mCallbacks;
    private int mDefaultActionButtonContentDescription;
    final FrameLayout mDefaultActivityButton;
    private final ImageView mDefaultActivityButtonImage;
    final FrameLayout mExpandActivityOverflowButton;
    private final ImageView mExpandActivityOverflowButtonImage;
    int mInitialActivityCount;
    private boolean mIsAttachedToWindow;
    boolean mIsSelectingDefaultActivity;
    private final int mListPopupMaxWidth;
    private ListPopupWindow mListPopupWindow;
    final DataSetObserver mModelDataSetObserver;
    PopupWindow$OnDismissListener mOnDismissListener;
    private final ViewTreeObserver$OnGlobalLayoutListener mOnGlobalLayoutListener;
    ActionProvider mProvider;
    
    public ActivityChooserView(@NonNull final Context context) {
        this(context, null);
    }
    
    public ActivityChooserView(@NonNull final Context context, @Nullable final AttributeSet set) {
        this(context, set, 0);
    }
    
    public ActivityChooserView(@NonNull final Context context, @Nullable final AttributeSet set, int image) {
        super(context, set, image);
        this.mModelDataSetObserver = new DataSetObserver() {
            final ActivityChooserView this$0;
            
            public void onChanged() {
                super.onChanged();
                this.this$0.mAdapter.notifyDataSetChanged();
            }
            
            public void onInvalidated() {
                super.onInvalidated();
                this.this$0.mAdapter.notifyDataSetInvalidated();
            }
        };
        this.mOnGlobalLayoutListener = (ViewTreeObserver$OnGlobalLayoutListener)new ViewTreeObserver$OnGlobalLayoutListener() {
            final ActivityChooserView this$0;
            
            public void onGlobalLayout() {
                if (this.this$0.isShowingPopup()) {
                    if (!((View)this.this$0).isShown()) {
                        this.this$0.getListPopupWindow().dismiss();
                    }
                    else {
                        this.this$0.getListPopupWindow().show();
                        final ActionProvider mProvider = this.this$0.mProvider;
                        if (mProvider != null) {
                            mProvider.subUiVisibilityChanged(true);
                        }
                    }
                }
            }
        };
        this.mInitialActivityCount = 4;
        final int[] activityChooserView = R.styleable.ActivityChooserView;
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, activityChooserView, image, 0);
        ViewCompat.saveAttributeDataForStyleable((View)this, context, activityChooserView, set, obtainStyledAttributes, image, 0);
        this.mInitialActivityCount = obtainStyledAttributes.getInt(R.styleable.ActivityChooserView_initialActivityCount, 4);
        final Drawable drawable = obtainStyledAttributes.getDrawable(R.styleable.ActivityChooserView_expandActivityOverflowButtonDrawable);
        obtainStyledAttributes.recycle();
        LayoutInflater.from(((View)this).getContext()).inflate(R.layout.abc_activity_chooser_view, (ViewGroup)this, true);
        final Callbacks callbacks = new Callbacks();
        this.mCallbacks = callbacks;
        final View viewById = ((View)this).findViewById(R.id.activity_chooser_view_content);
        this.mActivityChooserContent = viewById;
        this.mActivityChooserContentBackground = viewById.getBackground();
        final FrameLayout mDefaultActivityButton = (FrameLayout)((View)this).findViewById(R.id.default_activity_button);
        ((View)(this.mDefaultActivityButton = mDefaultActivityButton)).setOnClickListener((View$OnClickListener)callbacks);
        ((View)mDefaultActivityButton).setOnLongClickListener((View$OnLongClickListener)callbacks);
        image = R.id.image;
        this.mDefaultActivityButtonImage = (ImageView)((View)mDefaultActivityButton).findViewById(image);
        final FrameLayout mExpandActivityOverflowButton = (FrameLayout)((View)this).findViewById(R.id.expand_activities_button);
        ((View)mExpandActivityOverflowButton).setOnClickListener((View$OnClickListener)callbacks);
        ((View)mExpandActivityOverflowButton).setAccessibilityDelegate((View$AccessibilityDelegate)new View$AccessibilityDelegate(this) {
            final ActivityChooserView this$0;
            
            public void onInitializeAccessibilityNodeInfo(final View view, final AccessibilityNodeInfo accessibilityNodeInfo) {
                super.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfo);
                AccessibilityNodeInfoCompat.wrap(accessibilityNodeInfo).setCanOpenPopup(true);
            }
        });
        ((View)mExpandActivityOverflowButton).setOnTouchListener((View$OnTouchListener)new ForwardingListener(this, mExpandActivityOverflowButton) {
            final ActivityChooserView this$0;
            
            @Override
            public ShowableListMenu getPopup() {
                return this.this$0.getListPopupWindow();
            }
            
            @Override
            protected boolean onForwardingStarted() {
                this.this$0.showPopup();
                return true;
            }
            
            @Override
            protected boolean onForwardingStopped() {
                this.this$0.dismissPopup();
                return true;
            }
        });
        this.mExpandActivityOverflowButton = mExpandActivityOverflowButton;
        (this.mExpandActivityOverflowButtonImage = (ImageView)((View)mExpandActivityOverflowButton).findViewById(image)).setImageDrawable(drawable);
        (this.mAdapter = new ActivityChooserViewAdapter()).registerDataSetObserver((DataSetObserver)new DataSetObserver(this) {
            final ActivityChooserView this$0;
            
            public void onChanged() {
                super.onChanged();
                this.this$0.updateAppearance();
            }
        });
        final Resources resources = context.getResources();
        this.mListPopupMaxWidth = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(R.dimen.abc_config_prefDialogWidth));
    }
    
    public boolean dismissPopup() {
        if (this.isShowingPopup()) {
            this.getListPopupWindow().dismiss();
            final ViewTreeObserver viewTreeObserver = ((View)this).getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeGlobalOnLayoutListener(this.mOnGlobalLayoutListener);
            }
        }
        return true;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public ActivityChooserModel getDataModel() {
        return this.mAdapter.getDataModel();
    }
    
    ListPopupWindow getListPopupWindow() {
        if (this.mListPopupWindow == null) {
            (this.mListPopupWindow = new ListPopupWindow(((View)this).getContext())).setAdapter((ListAdapter)this.mAdapter);
            this.mListPopupWindow.setAnchorView((View)this);
            this.mListPopupWindow.setModal(true);
            this.mListPopupWindow.setOnItemClickListener((AdapterView$OnItemClickListener)this.mCallbacks);
            this.mListPopupWindow.setOnDismissListener((PopupWindow$OnDismissListener)this.mCallbacks);
        }
        return this.mListPopupWindow;
    }
    
    public boolean isShowingPopup() {
        return this.getListPopupWindow().isShowing();
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        final ActivityChooserModel dataModel = this.mAdapter.getDataModel();
        if (dataModel != null) {
            ((Observable)dataModel).registerObserver((Object)this.mModelDataSetObserver);
        }
        this.mIsAttachedToWindow = true;
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        final ActivityChooserModel dataModel = this.mAdapter.getDataModel();
        if (dataModel != null) {
            ((Observable)dataModel).unregisterObserver((Object)this.mModelDataSetObserver);
        }
        final ViewTreeObserver viewTreeObserver = ((View)this).getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.removeGlobalOnLayoutListener(this.mOnGlobalLayoutListener);
        }
        if (this.isShowingPopup()) {
            this.dismissPopup();
        }
        this.mIsAttachedToWindow = false;
    }
    
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        this.mActivityChooserContent.layout(0, 0, n3 - n, n4 - n2);
        if (!this.isShowingPopup()) {
            this.dismissPopup();
        }
    }
    
    protected void onMeasure(final int n, final int n2) {
        final View mActivityChooserContent = this.mActivityChooserContent;
        int measureSpec = n2;
        if (((View)this.mDefaultActivityButton).getVisibility() != 0) {
            measureSpec = View$MeasureSpec.makeMeasureSpec(View$MeasureSpec.getSize(n2), 1073741824);
        }
        this.measureChild(mActivityChooserContent, n, measureSpec);
        ((View)this).setMeasuredDimension(mActivityChooserContent.getMeasuredWidth(), mActivityChooserContent.getMeasuredHeight());
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public void setActivityChooserModel(final ActivityChooserModel dataModel) {
        this.mAdapter.setDataModel(dataModel);
        if (this.isShowingPopup()) {
            this.dismissPopup();
            this.showPopup();
        }
    }
    
    public void setDefaultActionButtonContentDescription(final int mDefaultActionButtonContentDescription) {
        this.mDefaultActionButtonContentDescription = mDefaultActionButtonContentDescription;
    }
    
    public void setExpandActivityOverflowButtonContentDescription(final int n) {
        ((View)this.mExpandActivityOverflowButtonImage).setContentDescription((CharSequence)((View)this).getContext().getString(n));
    }
    
    public void setExpandActivityOverflowButtonDrawable(final Drawable imageDrawable) {
        this.mExpandActivityOverflowButtonImage.setImageDrawable(imageDrawable);
    }
    
    public void setInitialActivityCount(final int mInitialActivityCount) {
        this.mInitialActivityCount = mInitialActivityCount;
    }
    
    public void setOnDismissListener(final PopupWindow$OnDismissListener mOnDismissListener) {
        this.mOnDismissListener = mOnDismissListener;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void setProvider(final ActionProvider mProvider) {
        this.mProvider = mProvider;
    }
    
    public boolean showPopup() {
        if (!this.isShowingPopup() && this.mIsAttachedToWindow) {
            this.mIsSelectingDefaultActivity = false;
            this.showPopupUnchecked(this.mInitialActivityCount);
            return true;
        }
        return false;
    }
    
    void showPopupUnchecked(final int maxActivityCount) {
        if (this.mAdapter.getDataModel() != null) {
            ((View)this).getViewTreeObserver().addOnGlobalLayoutListener(this.mOnGlobalLayoutListener);
            int n;
            if (((View)this.mDefaultActivityButton).getVisibility() == 0) {
                n = 1;
            }
            else {
                n = 0;
            }
            final int activityCount = this.mAdapter.getActivityCount();
            if (maxActivityCount != Integer.MAX_VALUE && activityCount > maxActivityCount + n) {
                this.mAdapter.setShowFooterView(true);
                this.mAdapter.setMaxActivityCount(maxActivityCount - 1);
            }
            else {
                this.mAdapter.setShowFooterView(false);
                this.mAdapter.setMaxActivityCount(maxActivityCount);
            }
            final ListPopupWindow listPopupWindow = this.getListPopupWindow();
            if (!listPopupWindow.isShowing()) {
                if (!this.mIsSelectingDefaultActivity && n != 0) {
                    this.mAdapter.setShowDefaultActivity(false, false);
                }
                else {
                    this.mAdapter.setShowDefaultActivity(true, (boolean)(n != 0));
                }
                listPopupWindow.setContentWidth(Math.min(this.mAdapter.measureContentWidth(), this.mListPopupMaxWidth));
                listPopupWindow.show();
                final ActionProvider mProvider = this.mProvider;
                if (mProvider != null) {
                    mProvider.subUiVisibilityChanged(true);
                }
                ((View)listPopupWindow.getListView()).setContentDescription((CharSequence)((View)this).getContext().getString(R.string.abc_activitychooserview_choose_application));
                ((AbsListView)listPopupWindow.getListView()).setSelector((Drawable)new ColorDrawable(0));
            }
            return;
        }
        throw new IllegalStateException("No data model. Did you call #setDataModel?");
    }
    
    void updateAppearance() {
        if (this.mAdapter.getCount() > 0) {
            ((View)this.mExpandActivityOverflowButton).setEnabled(true);
        }
        else {
            ((View)this.mExpandActivityOverflowButton).setEnabled(false);
        }
        final int activityCount = this.mAdapter.getActivityCount();
        final int historySize = this.mAdapter.getHistorySize();
        if (activityCount != 1 && (activityCount <= 1 || historySize <= 0)) {
            ((View)this.mDefaultActivityButton).setVisibility(8);
        }
        else {
            ((View)this.mDefaultActivityButton).setVisibility(0);
            final ResolveInfo defaultActivity = this.mAdapter.getDefaultActivity();
            final PackageManager packageManager = ((View)this).getContext().getPackageManager();
            this.mDefaultActivityButtonImage.setImageDrawable(defaultActivity.loadIcon(packageManager));
            if (this.mDefaultActionButtonContentDescription != 0) {
                ((View)this.mDefaultActivityButton).setContentDescription((CharSequence)((View)this).getContext().getString(this.mDefaultActionButtonContentDescription, new Object[] { defaultActivity.loadLabel(packageManager) }));
            }
        }
        if (((View)this.mDefaultActivityButton).getVisibility() == 0) {
            this.mActivityChooserContent.setBackgroundDrawable(this.mActivityChooserContentBackground);
        }
        else {
            this.mActivityChooserContent.setBackgroundDrawable((Drawable)null);
        }
    }
    
    private class ActivityChooserViewAdapter extends BaseAdapter
    {
        private static final int ITEM_VIEW_TYPE_ACTIVITY = 0;
        private static final int ITEM_VIEW_TYPE_COUNT = 3;
        private static final int ITEM_VIEW_TYPE_FOOTER = 1;
        public static final int MAX_ACTIVITY_COUNT_DEFAULT = 4;
        public static final int MAX_ACTIVITY_COUNT_UNLIMITED = Integer.MAX_VALUE;
        private ActivityChooserModel mDataModel;
        private boolean mHighlightDefaultActivity;
        private int mMaxActivityCount;
        private boolean mShowDefaultActivity;
        private boolean mShowFooterView;
        final ActivityChooserView this$0;
        
        ActivityChooserViewAdapter(final ActivityChooserView this$0) {
            this.this$0 = this$0;
            this.mMaxActivityCount = 4;
        }
        
        public int getActivityCount() {
            return this.mDataModel.getActivityCount();
        }
        
        public int getCount() {
            int activityCount;
            final int n = activityCount = this.mDataModel.getActivityCount();
            if (!this.mShowDefaultActivity) {
                activityCount = n;
                if (this.mDataModel.getDefaultActivity() != null) {
                    activityCount = n - 1;
                }
            }
            int min = Math.min(activityCount, this.mMaxActivityCount);
            if (this.mShowFooterView) {
                ++min;
            }
            return min;
        }
        
        public ActivityChooserModel getDataModel() {
            return this.mDataModel;
        }
        
        public ResolveInfo getDefaultActivity() {
            return this.mDataModel.getDefaultActivity();
        }
        
        public int getHistorySize() {
            return this.mDataModel.getHistorySize();
        }
        
        public Object getItem(final int n) {
            final int itemViewType = this.getItemViewType(n);
            if (itemViewType == 0) {
                int n2 = n;
                if (!this.mShowDefaultActivity) {
                    n2 = n;
                    if (this.mDataModel.getDefaultActivity() != null) {
                        n2 = n + 1;
                    }
                }
                return this.mDataModel.getActivity(n2);
            }
            if (itemViewType == 1) {
                return null;
            }
            throw new IllegalArgumentException();
        }
        
        public long getItemId(final int n) {
            return n;
        }
        
        public int getItemViewType(final int n) {
            if (this.mShowFooterView && n == this.getCount() - 1) {
                return 1;
            }
            return 0;
        }
        
        public boolean getShowDefaultActivity() {
            return this.mShowDefaultActivity;
        }
        
        public View getView(final int n, final View view, final ViewGroup viewGroup) {
            final int itemViewType = this.getItemViewType(n);
            if (itemViewType == 0) {
                View inflate = null;
                Label_0134: {
                    if (view != null) {
                        inflate = view;
                        if (view.getId() == R.id.list_item) {
                            break Label_0134;
                        }
                    }
                    inflate = LayoutInflater.from(((View)this.this$0).getContext()).inflate(R.layout.abc_activity_chooser_view_list_item, viewGroup, false);
                }
                final PackageManager packageManager = ((View)this.this$0).getContext().getPackageManager();
                final ImageView imageView = (ImageView)inflate.findViewById(R.id.icon);
                final ResolveInfo resolveInfo = (ResolveInfo)this.getItem(n);
                imageView.setImageDrawable(resolveInfo.loadIcon(packageManager));
                ((TextView)inflate.findViewById(R.id.title)).setText(resolveInfo.loadLabel(packageManager));
                if (this.mShowDefaultActivity && n == 0 && this.mHighlightDefaultActivity) {
                    inflate.setActivated(true);
                }
                else {
                    inflate.setActivated(false);
                }
                return inflate;
            }
            if (itemViewType == 1) {
                if (view != null) {
                    final View inflate2 = view;
                    if (view.getId() == 1) {
                        return inflate2;
                    }
                }
                final View inflate2 = LayoutInflater.from(((View)this.this$0).getContext()).inflate(R.layout.abc_activity_chooser_view_list_item, viewGroup, false);
                inflate2.setId(1);
                ((TextView)inflate2.findViewById(R.id.title)).setText((CharSequence)((View)this.this$0).getContext().getString(R.string.abc_activity_chooser_view_see_all));
                return inflate2;
            }
            throw new IllegalArgumentException();
        }
        
        public int getViewTypeCount() {
            return 3;
        }
        
        public int measureContentWidth() {
            final int mMaxActivityCount = this.mMaxActivityCount;
            this.mMaxActivityCount = Integer.MAX_VALUE;
            int i = 0;
            final int measureSpec = View$MeasureSpec.makeMeasureSpec(0, 0);
            final int measureSpec2 = View$MeasureSpec.makeMeasureSpec(0, 0);
            final int count = this.getCount();
            View view = null;
            int max = 0;
            while (i < count) {
                view = this.getView(i, view, null);
                view.measure(measureSpec, measureSpec2);
                max = Math.max(max, view.getMeasuredWidth());
                ++i;
            }
            this.mMaxActivityCount = mMaxActivityCount;
            return max;
        }
        
        public void setDataModel(final ActivityChooserModel mDataModel) {
            final ActivityChooserModel dataModel = this.this$0.mAdapter.getDataModel();
            if (dataModel != null && ((View)this.this$0).isShown()) {
                ((Observable)dataModel).unregisterObserver((Object)this.this$0.mModelDataSetObserver);
            }
            if ((this.mDataModel = mDataModel) != null && ((View)this.this$0).isShown()) {
                ((Observable)mDataModel).registerObserver((Object)this.this$0.mModelDataSetObserver);
            }
            this.notifyDataSetChanged();
        }
        
        public void setMaxActivityCount(final int mMaxActivityCount) {
            if (this.mMaxActivityCount != mMaxActivityCount) {
                this.mMaxActivityCount = mMaxActivityCount;
                this.notifyDataSetChanged();
            }
        }
        
        public void setShowDefaultActivity(final boolean mShowDefaultActivity, final boolean mHighlightDefaultActivity) {
            if (this.mShowDefaultActivity != mShowDefaultActivity || this.mHighlightDefaultActivity != mHighlightDefaultActivity) {
                this.mShowDefaultActivity = mShowDefaultActivity;
                this.mHighlightDefaultActivity = mHighlightDefaultActivity;
                this.notifyDataSetChanged();
            }
        }
        
        public void setShowFooterView(final boolean mShowFooterView) {
            if (this.mShowFooterView != mShowFooterView) {
                this.mShowFooterView = mShowFooterView;
                this.notifyDataSetChanged();
            }
        }
    }
    
    private class Callbacks implements AdapterView$OnItemClickListener, View$OnClickListener, View$OnLongClickListener, PopupWindow$OnDismissListener
    {
        final ActivityChooserView this$0;
        
        Callbacks(final ActivityChooserView this$0) {
            this.this$0 = this$0;
        }
        
        private void notifyOnDismissListener() {
            final PopupWindow$OnDismissListener mOnDismissListener = this.this$0.mOnDismissListener;
            if (mOnDismissListener != null) {
                mOnDismissListener.onDismiss();
            }
        }
        
        public void onClick(final View view) {
            final ActivityChooserView this$0 = this.this$0;
            if (view == this$0.mDefaultActivityButton) {
                this$0.dismissPopup();
                final Intent chooseActivity = this.this$0.mAdapter.getDataModel().chooseActivity(this.this$0.mAdapter.getDataModel().getActivityIndex(this.this$0.mAdapter.getDefaultActivity()));
                if (chooseActivity != null) {
                    chooseActivity.addFlags(524288);
                    ((View)this.this$0).getContext().startActivity(chooseActivity);
                }
            }
            else {
                if (view != this$0.mExpandActivityOverflowButton) {
                    throw new IllegalArgumentException();
                }
                this$0.mIsSelectingDefaultActivity = false;
                this$0.showPopupUnchecked(this$0.mInitialActivityCount);
            }
        }
        
        public void onDismiss() {
            this.notifyOnDismissListener();
            final ActionProvider mProvider = this.this$0.mProvider;
            if (mProvider != null) {
                mProvider.subUiVisibilityChanged(false);
            }
        }
        
        public void onItemClick(final AdapterView<?> adapterView, final View view, int defaultActivity, final long n) {
            final int itemViewType = ((ActivityChooserViewAdapter)adapterView.getAdapter()).getItemViewType(defaultActivity);
            if (itemViewType != 0) {
                if (itemViewType != 1) {
                    throw new IllegalArgumentException();
                }
                this.this$0.showPopupUnchecked(Integer.MAX_VALUE);
            }
            else {
                this.this$0.dismissPopup();
                final ActivityChooserView this$0 = this.this$0;
                if (this$0.mIsSelectingDefaultActivity) {
                    if (defaultActivity > 0) {
                        this$0.mAdapter.getDataModel().setDefaultActivity(defaultActivity);
                    }
                }
                else {
                    if (!this$0.mAdapter.getShowDefaultActivity()) {
                        ++defaultActivity;
                    }
                    final Intent chooseActivity = this.this$0.mAdapter.getDataModel().chooseActivity(defaultActivity);
                    if (chooseActivity != null) {
                        chooseActivity.addFlags(524288);
                        ((View)this.this$0).getContext().startActivity(chooseActivity);
                    }
                }
            }
        }
        
        public boolean onLongClick(final View view) {
            final ActivityChooserView this$0 = this.this$0;
            if (view == this$0.mDefaultActivityButton) {
                if (this$0.mAdapter.getCount() > 0) {
                    final ActivityChooserView this$2 = this.this$0;
                    this$2.mIsSelectingDefaultActivity = true;
                    this$2.showPopupUnchecked(this$2.mInitialActivityCount);
                }
                return true;
            }
            throw new IllegalArgumentException();
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static class InnerLayout extends LinearLayout
    {
        private static final int[] TINT_ATTRS;
        
        static {
            TINT_ATTRS = new int[] { 16842964 };
        }
        
        public InnerLayout(final Context context, final AttributeSet set) {
            super(context, set);
            final TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(context, set, InnerLayout.TINT_ATTRS);
            ((View)this).setBackgroundDrawable(obtainStyledAttributes.getDrawable(0));
            obtainStyledAttributes.recycle();
        }
    }
}

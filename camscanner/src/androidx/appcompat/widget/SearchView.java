// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.widget.EditText;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.view.inputmethod.InputMethodManager;
import android.view.KeyEvent$DispatcherState;
import android.util.TypedValue;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.EditorInfo;
import android.content.res.Configuration;
import android.os.Parcel;
import android.os.Parcelable$ClassLoaderCreator;
import android.os.Parcelable$Creator;
import androidx.customview.view.AbsSavedState;
import android.annotation.SuppressLint;
import java.lang.reflect.Method;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import androidx.annotation.RestrictTo;
import android.content.ActivityNotFoundException;
import android.view.View$MeasureSpec;
import android.view.TouchDelegate;
import android.widget.AutoCompleteTextView;
import android.widget.ListAdapter;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.text.SpannableStringBuilder;
import android.content.res.Resources;
import android.content.ComponentName;
import android.os.Parcelable;
import android.app.PendingIntent;
import android.net.Uri;
import android.view.View$OnLayoutChangeListener;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import androidx.core.view.ViewCompat;
import android.text.Editable;
import android.widget.AdapterView;
import android.widget.TextView;
import android.view.KeyEvent;
import android.database.Cursor;
import androidx.appcompat.R;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import androidx.annotation.NonNull;
import android.content.Context;
import android.os.Build$VERSION;
import android.content.Intent;
import android.text.TextWatcher;
import android.view.View$OnKeyListener;
import androidx.cursoradapter.widget.CursorAdapter;
import android.app.SearchableInfo;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable$ConstantState;
import java.util.WeakHashMap;
import android.view.View$OnFocusChangeListener;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.AdapterView$OnItemClickListener;
import android.widget.TextView$OnEditorActionListener;
import android.view.View$OnClickListener;
import android.view.View;
import android.widget.ImageView;
import android.os.Bundle;
import androidx.appcompat.view.CollapsibleActionView;

public class SearchView extends LinearLayoutCompat implements CollapsibleActionView
{
    static final boolean DBG = false;
    private static final String IME_OPTION_NO_MICROPHONE = "nm";
    static final String LOG_TAG = "SearchView";
    static final PreQAutoCompleteTextViewReflector PRE_API_29_HIDDEN_METHOD_INVOKER;
    private Bundle mAppSearchData;
    private boolean mClearingFocus;
    final ImageView mCloseButton;
    private final ImageView mCollapsedIcon;
    private int mCollapsedImeOptions;
    private final CharSequence mDefaultQueryHint;
    private final View mDropDownAnchor;
    private boolean mExpandedInActionView;
    final ImageView mGoButton;
    private boolean mIconified;
    private boolean mIconifiedByDefault;
    private int mMaxWidth;
    private CharSequence mOldQueryText;
    private final View$OnClickListener mOnClickListener;
    private OnCloseListener mOnCloseListener;
    private final TextView$OnEditorActionListener mOnEditorActionListener;
    private final AdapterView$OnItemClickListener mOnItemClickListener;
    private final AdapterView$OnItemSelectedListener mOnItemSelectedListener;
    private OnQueryTextListener mOnQueryChangeListener;
    View$OnFocusChangeListener mOnQueryTextFocusChangeListener;
    private View$OnClickListener mOnSearchClickListener;
    private OnSuggestionListener mOnSuggestionListener;
    private final WeakHashMap<String, Drawable$ConstantState> mOutsideDrawablesCache;
    private CharSequence mQueryHint;
    private boolean mQueryRefinement;
    private Runnable mReleaseCursorRunnable;
    final ImageView mSearchButton;
    private final View mSearchEditFrame;
    private final Drawable mSearchHintIcon;
    private final View mSearchPlate;
    final SearchAutoComplete mSearchSrcTextView;
    private Rect mSearchSrcTextViewBounds;
    private Rect mSearchSrtTextViewBoundsExpanded;
    SearchableInfo mSearchable;
    private final View mSubmitArea;
    private boolean mSubmitButtonEnabled;
    private final int mSuggestionCommitIconResId;
    private final int mSuggestionRowLayout;
    CursorAdapter mSuggestionsAdapter;
    private int[] mTemp;
    private int[] mTemp2;
    View$OnKeyListener mTextKeyListener;
    private TextWatcher mTextWatcher;
    private UpdatableTouchDelegate mTouchDelegate;
    private final Runnable mUpdateDrawableStateRunnable;
    private CharSequence mUserQuery;
    private final Intent mVoiceAppSearchIntent;
    final ImageView mVoiceButton;
    private boolean mVoiceButtonEnabled;
    private final Intent mVoiceWebSearchIntent;
    
    static {
        PreQAutoCompleteTextViewReflector pre_API_29_HIDDEN_METHOD_INVOKER;
        if (Build$VERSION.SDK_INT < 29) {
            pre_API_29_HIDDEN_METHOD_INVOKER = new PreQAutoCompleteTextViewReflector();
        }
        else {
            pre_API_29_HIDDEN_METHOD_INVOKER = null;
        }
        PRE_API_29_HIDDEN_METHOD_INVOKER = pre_API_29_HIDDEN_METHOD_INVOKER;
    }
    
    public SearchView(@NonNull final Context context) {
        this(context, null);
    }
    
    public SearchView(@NonNull final Context context, @Nullable final AttributeSet set) {
        this(context, set, R.attr.searchViewStyle);
    }
    
    public SearchView(@NonNull final Context context, @Nullable final AttributeSet set, int inputType) {
        super(context, set, inputType);
        this.mSearchSrcTextViewBounds = new Rect();
        this.mSearchSrtTextViewBoundsExpanded = new Rect();
        this.mTemp = new int[2];
        this.mTemp2 = new int[2];
        this.mUpdateDrawableStateRunnable = new Runnable() {
            final SearchView this$0;
            
            @Override
            public void run() {
                this.this$0.updateFocusedState();
            }
        };
        this.mReleaseCursorRunnable = new Runnable() {
            final SearchView this$0;
            
            @Override
            public void run() {
                final CursorAdapter mSuggestionsAdapter = this.this$0.mSuggestionsAdapter;
                if (mSuggestionsAdapter instanceof SuggestionsAdapter) {
                    mSuggestionsAdapter.changeCursor(null);
                }
            }
        };
        this.mOutsideDrawablesCache = new WeakHashMap<String, Drawable$ConstantState>();
        final View$OnClickListener view$OnClickListener = (View$OnClickListener)new View$OnClickListener() {
            final SearchView this$0;
            
            public void onClick(final View view) {
                final SearchView this$0 = this.this$0;
                if (view == this$0.mSearchButton) {
                    this$0.onSearchClicked();
                }
                else if (view == this$0.mCloseButton) {
                    this$0.onCloseClicked();
                }
                else if (view == this$0.mGoButton) {
                    this$0.onSubmitQuery();
                }
                else if (view == this$0.mVoiceButton) {
                    this$0.onVoiceClicked();
                }
                else if (view == this$0.mSearchSrcTextView) {
                    this$0.forceSuggestionQuery();
                }
            }
        };
        this.mOnClickListener = (View$OnClickListener)view$OnClickListener;
        this.mTextKeyListener = (View$OnKeyListener)new View$OnKeyListener() {
            final SearchView this$0;
            
            public boolean onKey(final View view, final int n, final KeyEvent keyEvent) {
                final SearchView this$0 = this.this$0;
                if (this$0.mSearchable == null) {
                    return false;
                }
                if (this$0.mSearchSrcTextView.isPopupShowing() && this.this$0.mSearchSrcTextView.getListSelection() != -1) {
                    return this.this$0.onSuggestionsKey(view, n, keyEvent);
                }
                if (!this.this$0.mSearchSrcTextView.isEmpty() && keyEvent.hasNoModifiers() && keyEvent.getAction() == 1 && n == 66) {
                    view.cancelLongPress();
                    final SearchView this$2 = this.this$0;
                    this$2.launchQuerySearch(0, null, ((EditText)this$2.mSearchSrcTextView).getText().toString());
                    return true;
                }
                return false;
            }
        };
        final TextView$OnEditorActionListener textView$OnEditorActionListener = (TextView$OnEditorActionListener)new TextView$OnEditorActionListener() {
            final SearchView this$0;
            
            public boolean onEditorAction(final TextView textView, final int n, final KeyEvent keyEvent) {
                this.this$0.onSubmitQuery();
                return true;
            }
        };
        this.mOnEditorActionListener = (TextView$OnEditorActionListener)textView$OnEditorActionListener;
        final AdapterView$OnItemClickListener adapterView$OnItemClickListener = (AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener() {
            final SearchView this$0;
            
            public void onItemClick(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
                this.this$0.onItemClicked(n, 0, null);
            }
        };
        this.mOnItemClickListener = (AdapterView$OnItemClickListener)adapterView$OnItemClickListener;
        final AdapterView$OnItemSelectedListener adapterView$OnItemSelectedListener = (AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener() {
            final SearchView this$0;
            
            public void onItemSelected(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
                this.this$0.onItemSelected(n);
            }
            
            public void onNothingSelected(final AdapterView<?> adapterView) {
            }
        };
        this.mOnItemSelectedListener = (AdapterView$OnItemSelectedListener)adapterView$OnItemSelectedListener;
        this.mTextWatcher = (TextWatcher)new TextWatcher() {
            final SearchView this$0;
            
            public void afterTextChanged(final Editable editable) {
            }
            
            public void beforeTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
            }
            
            public void onTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
                this.this$0.onTextChanged(charSequence);
            }
        };
        final int[] searchView = R.styleable.SearchView;
        final TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(context, set, searchView, inputType, 0);
        ViewCompat.saveAttributeDataForStyleable((View)this, context, searchView, set, obtainStyledAttributes.getWrappedTypeArray(), inputType, 0);
        LayoutInflater.from(context).inflate(obtainStyledAttributes.getResourceId(R.styleable.SearchView_layout, R.layout.abc_search_view), (ViewGroup)this, true);
        final SearchAutoComplete mSearchSrcTextView = (SearchAutoComplete)((View)this).findViewById(R.id.search_src_text);
        (this.mSearchSrcTextView = mSearchSrcTextView).setSearchView(this);
        this.mSearchEditFrame = ((View)this).findViewById(R.id.search_edit_frame);
        final View viewById = ((View)this).findViewById(R.id.search_plate);
        this.mSearchPlate = viewById;
        final View viewById2 = ((View)this).findViewById(R.id.submit_area);
        this.mSubmitArea = viewById2;
        final ImageView mSearchButton = (ImageView)((View)this).findViewById(R.id.search_button);
        this.mSearchButton = mSearchButton;
        final ImageView mGoButton = (ImageView)((View)this).findViewById(R.id.search_go_btn);
        this.mGoButton = mGoButton;
        final ImageView mCloseButton = (ImageView)((View)this).findViewById(R.id.search_close_btn);
        this.mCloseButton = mCloseButton;
        final ImageView mVoiceButton = (ImageView)((View)this).findViewById(R.id.search_voice_btn);
        this.mVoiceButton = mVoiceButton;
        final ImageView mCollapsedIcon = (ImageView)((View)this).findViewById(R.id.search_mag_icon);
        this.mCollapsedIcon = mCollapsedIcon;
        ViewCompat.setBackground(viewById, obtainStyledAttributes.getDrawable(R.styleable.SearchView_queryBackground));
        ViewCompat.setBackground(viewById2, obtainStyledAttributes.getDrawable(R.styleable.SearchView_submitBackground));
        inputType = R.styleable.SearchView_searchIcon;
        mSearchButton.setImageDrawable(obtainStyledAttributes.getDrawable(inputType));
        mGoButton.setImageDrawable(obtainStyledAttributes.getDrawable(R.styleable.SearchView_goIcon));
        mCloseButton.setImageDrawable(obtainStyledAttributes.getDrawable(R.styleable.SearchView_closeIcon));
        mVoiceButton.setImageDrawable(obtainStyledAttributes.getDrawable(R.styleable.SearchView_voiceIcon));
        mCollapsedIcon.setImageDrawable(obtainStyledAttributes.getDrawable(inputType));
        this.mSearchHintIcon = obtainStyledAttributes.getDrawable(R.styleable.SearchView_searchHintIcon);
        TooltipCompat.setTooltipText((View)mSearchButton, ((View)this).getResources().getString(R.string.abc_searchview_description_search));
        this.mSuggestionRowLayout = obtainStyledAttributes.getResourceId(R.styleable.SearchView_suggestionRowLayout, R.layout.abc_search_dropdown_item_icons_2line);
        this.mSuggestionCommitIconResId = obtainStyledAttributes.getResourceId(R.styleable.SearchView_commitIcon, 0);
        ((View)mSearchButton).setOnClickListener((View$OnClickListener)view$OnClickListener);
        ((View)mCloseButton).setOnClickListener((View$OnClickListener)view$OnClickListener);
        ((View)mGoButton).setOnClickListener((View$OnClickListener)view$OnClickListener);
        ((View)mVoiceButton).setOnClickListener((View$OnClickListener)view$OnClickListener);
        ((View)mSearchSrcTextView).setOnClickListener((View$OnClickListener)view$OnClickListener);
        ((TextView)mSearchSrcTextView).addTextChangedListener(this.mTextWatcher);
        ((TextView)mSearchSrcTextView).setOnEditorActionListener((TextView$OnEditorActionListener)textView$OnEditorActionListener);
        mSearchSrcTextView.setOnItemClickListener((AdapterView$OnItemClickListener)adapterView$OnItemClickListener);
        mSearchSrcTextView.setOnItemSelectedListener((AdapterView$OnItemSelectedListener)adapterView$OnItemSelectedListener);
        ((View)mSearchSrcTextView).setOnKeyListener(this.mTextKeyListener);
        ((View)mSearchSrcTextView).setOnFocusChangeListener((View$OnFocusChangeListener)new View$OnFocusChangeListener(this) {
            final SearchView this$0;
            
            public void onFocusChange(final View view, final boolean b) {
                final SearchView this$0 = this.this$0;
                final View$OnFocusChangeListener mOnQueryTextFocusChangeListener = this$0.mOnQueryTextFocusChangeListener;
                if (mOnQueryTextFocusChangeListener != null) {
                    mOnQueryTextFocusChangeListener.onFocusChange((View)this$0, b);
                }
            }
        });
        this.setIconifiedByDefault(obtainStyledAttributes.getBoolean(R.styleable.SearchView_iconifiedByDefault, true));
        inputType = obtainStyledAttributes.getDimensionPixelSize(R.styleable.SearchView_android_maxWidth, -1);
        if (inputType != -1) {
            this.setMaxWidth(inputType);
        }
        this.mDefaultQueryHint = obtainStyledAttributes.getText(R.styleable.SearchView_defaultQueryHint);
        this.mQueryHint = obtainStyledAttributes.getText(R.styleable.SearchView_queryHint);
        inputType = obtainStyledAttributes.getInt(R.styleable.SearchView_android_imeOptions, -1);
        if (inputType != -1) {
            this.setImeOptions(inputType);
        }
        inputType = obtainStyledAttributes.getInt(R.styleable.SearchView_android_inputType, -1);
        if (inputType != -1) {
            this.setInputType(inputType);
        }
        ((View)this).setFocusable(obtainStyledAttributes.getBoolean(R.styleable.SearchView_android_focusable, true));
        obtainStyledAttributes.recycle();
        final Intent mVoiceWebSearchIntent = new Intent("android.speech.action.WEB_SEARCH");
        (this.mVoiceWebSearchIntent = mVoiceWebSearchIntent).addFlags(268435456);
        mVoiceWebSearchIntent.putExtra("android.speech.extra.LANGUAGE_MODEL", "web_search");
        (this.mVoiceAppSearchIntent = new Intent("android.speech.action.RECOGNIZE_SPEECH")).addFlags(268435456);
        final View viewById3 = ((View)this).findViewById(mSearchSrcTextView.getDropDownAnchor());
        if ((this.mDropDownAnchor = viewById3) != null) {
            viewById3.addOnLayoutChangeListener((View$OnLayoutChangeListener)new View$OnLayoutChangeListener(this) {
                final SearchView this$0;
                
                public void onLayoutChange(final View view, final int n, final int n2, final int n3, final int n4, final int n5, final int n6, final int n7, final int n8) {
                    this.this$0.adjustDropDownSizeAndPosition();
                }
            });
        }
        this.updateViewsVisibility(this.mIconifiedByDefault);
        this.updateQueryHint();
    }
    
    private Intent createIntent(final String s, final Uri data, final String s2, final String s3, final int n, final String s4) {
        final Intent intent = new Intent(s);
        intent.addFlags(268435456);
        if (data != null) {
            intent.setData(data);
        }
        intent.putExtra("user_query", this.mUserQuery);
        if (s3 != null) {
            intent.putExtra("query", s3);
        }
        if (s2 != null) {
            intent.putExtra("intent_extra_data_key", s2);
        }
        final Bundle mAppSearchData = this.mAppSearchData;
        if (mAppSearchData != null) {
            intent.putExtra("app_data", mAppSearchData);
        }
        if (n != 0) {
            intent.putExtra("action_key", n);
            intent.putExtra("action_msg", s4);
        }
        intent.setComponent(this.mSearchable.getSearchActivity());
        return intent;
    }
    
    private Intent createIntentFromSuggestion(final Cursor cursor, int position, final String s) {
        try {
            String s2;
            if ((s2 = SuggestionsAdapter.getColumnString(cursor, "suggest_intent_action")) == null) {
                s2 = this.mSearchable.getSuggestIntentAction();
            }
            String s3;
            if ((s3 = s2) == null) {
                s3 = "android.intent.action.SEARCH";
            }
            String str;
            if ((str = SuggestionsAdapter.getColumnString(cursor, "suggest_intent_data")) == null) {
                str = this.mSearchable.getSuggestIntentData();
            }
            String string;
            if ((string = str) != null) {
                final String columnString = SuggestionsAdapter.getColumnString(cursor, "suggest_intent_data_id");
                string = str;
                if (columnString != null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append("/");
                    sb.append(Uri.encode(columnString));
                    string = sb.toString();
                }
            }
            Uri parse;
            if (string == null) {
                parse = null;
            }
            else {
                parse = Uri.parse(string);
            }
            return this.createIntent(s3, parse, SuggestionsAdapter.getColumnString(cursor, "suggest_intent_extra_data"), SuggestionsAdapter.getColumnString(cursor, "suggest_intent_query"), position, s);
        }
        catch (final RuntimeException ex) {
            try {
                position = cursor.getPosition();
            }
            catch (final RuntimeException ex2) {
                position = -1;
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Search suggestions cursor at row ");
            sb2.append(position);
            sb2.append(" returned exception.");
            return null;
        }
    }
    
    private Intent createVoiceAppSearchIntent(final Intent intent, final SearchableInfo searchableInfo) {
        final ComponentName searchActivity = searchableInfo.getSearchActivity();
        final Intent intent2 = new Intent("android.intent.action.SEARCH");
        intent2.setComponent(searchActivity);
        final PendingIntent activity = PendingIntent.getActivity(((View)this).getContext(), 0, intent2, 1107296256);
        final Bundle bundle = new Bundle();
        final Bundle mAppSearchData = this.mAppSearchData;
        if (mAppSearchData != null) {
            bundle.putParcelable("app_data", (Parcelable)mAppSearchData);
        }
        final Intent intent3 = new Intent(intent);
        final Resources resources = ((View)this).getResources();
        String string;
        if (searchableInfo.getVoiceLanguageModeId() != 0) {
            string = resources.getString(searchableInfo.getVoiceLanguageModeId());
        }
        else {
            string = "free_form";
        }
        final int voicePromptTextId = searchableInfo.getVoicePromptTextId();
        final String s = null;
        String string2;
        if (voicePromptTextId != 0) {
            string2 = resources.getString(searchableInfo.getVoicePromptTextId());
        }
        else {
            string2 = null;
        }
        String string3;
        if (searchableInfo.getVoiceLanguageId() != 0) {
            string3 = resources.getString(searchableInfo.getVoiceLanguageId());
        }
        else {
            string3 = null;
        }
        int voiceMaxResults;
        if (searchableInfo.getVoiceMaxResults() != 0) {
            voiceMaxResults = searchableInfo.getVoiceMaxResults();
        }
        else {
            voiceMaxResults = 1;
        }
        intent3.putExtra("android.speech.extra.LANGUAGE_MODEL", string);
        intent3.putExtra("android.speech.extra.PROMPT", string2);
        intent3.putExtra("android.speech.extra.LANGUAGE", string3);
        intent3.putExtra("android.speech.extra.MAX_RESULTS", voiceMaxResults);
        String flattenToShortString;
        if (searchActivity == null) {
            flattenToShortString = s;
        }
        else {
            flattenToShortString = searchActivity.flattenToShortString();
        }
        intent3.putExtra("calling_package", flattenToShortString);
        intent3.putExtra("android.speech.extra.RESULTS_PENDINGINTENT", (Parcelable)activity);
        intent3.putExtra("android.speech.extra.RESULTS_PENDINGINTENT_BUNDLE", bundle);
        return intent3;
    }
    
    private Intent createVoiceWebSearchIntent(final Intent intent, final SearchableInfo searchableInfo) {
        final Intent intent2 = new Intent(intent);
        final ComponentName searchActivity = searchableInfo.getSearchActivity();
        String flattenToShortString;
        if (searchActivity == null) {
            flattenToShortString = null;
        }
        else {
            flattenToShortString = searchActivity.flattenToShortString();
        }
        intent2.putExtra("calling_package", flattenToShortString);
        return intent2;
    }
    
    private void dismissSuggestions() {
        this.mSearchSrcTextView.dismissDropDown();
    }
    
    private void getChildBoundsWithinSearchView(final View view, final Rect rect) {
        view.getLocationInWindow(this.mTemp);
        ((View)this).getLocationInWindow(this.mTemp2);
        final int[] mTemp = this.mTemp;
        final int n = mTemp[1];
        final int[] mTemp2 = this.mTemp2;
        final int n2 = n - mTemp2[1];
        final int n3 = mTemp[0] - mTemp2[0];
        rect.set(n3, n2, view.getWidth() + n3, view.getHeight() + n2);
    }
    
    private CharSequence getDecoratedHint(final CharSequence charSequence) {
        if (this.mIconifiedByDefault && this.mSearchHintIcon != null) {
            final int n = (int)(((TextView)this.mSearchSrcTextView).getTextSize() * 1.25);
            this.mSearchHintIcon.setBounds(0, 0, n, n);
            final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder((CharSequence)"   ");
            spannableStringBuilder.setSpan((Object)new ImageSpan(this.mSearchHintIcon), 1, 2, 33);
            spannableStringBuilder.append(charSequence);
            return (CharSequence)spannableStringBuilder;
        }
        return charSequence;
    }
    
    private int getPreferredHeight() {
        return ((View)this).getContext().getResources().getDimensionPixelSize(R.dimen.abc_search_view_preferred_height);
    }
    
    private int getPreferredWidth() {
        return ((View)this).getContext().getResources().getDimensionPixelSize(R.dimen.abc_search_view_preferred_width);
    }
    
    private boolean hasVoiceSearch() {
        final SearchableInfo mSearchable = this.mSearchable;
        boolean b2;
        final boolean b = b2 = false;
        if (mSearchable != null) {
            b2 = b;
            if (mSearchable.getVoiceSearchEnabled()) {
                Intent intent;
                if (this.mSearchable.getVoiceSearchLaunchWebSearch()) {
                    intent = this.mVoiceWebSearchIntent;
                }
                else if (this.mSearchable.getVoiceSearchLaunchRecognizer()) {
                    intent = this.mVoiceAppSearchIntent;
                }
                else {
                    intent = null;
                }
                b2 = b;
                if (intent != null) {
                    b2 = b;
                    if (((View)this).getContext().getPackageManager().resolveActivity(intent, 65536) != null) {
                        b2 = true;
                    }
                }
            }
        }
        return b2;
    }
    
    static boolean isLandscapeMode(final Context context) {
        return context.getResources().getConfiguration().orientation == 2;
    }
    
    private boolean isSubmitAreaEnabled() {
        return (this.mSubmitButtonEnabled || this.mVoiceButtonEnabled) && !this.isIconified();
    }
    
    private void launchIntent(final Intent obj) {
        if (obj == null) {
            return;
        }
        try {
            ((View)this).getContext().startActivity(obj);
        }
        catch (final RuntimeException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed launch activity: ");
            sb.append(obj);
        }
    }
    
    private boolean launchSuggestion(final int n, final int n2, final String s) {
        final Cursor cursor = this.mSuggestionsAdapter.getCursor();
        if (cursor != null && cursor.moveToPosition(n)) {
            this.launchIntent(this.createIntentFromSuggestion(cursor, n2, s));
            return true;
        }
        return false;
    }
    
    private void postUpdateFocusedState() {
        ((View)this).post(this.mUpdateDrawableStateRunnable);
    }
    
    private void rewriteQueryFromSuggestion(final int n) {
        final Editable text = ((EditText)this.mSearchSrcTextView).getText();
        final Cursor cursor = this.mSuggestionsAdapter.getCursor();
        if (cursor == null) {
            return;
        }
        if (cursor.moveToPosition(n)) {
            final CharSequence convertToString = this.mSuggestionsAdapter.convertToString(cursor);
            if (convertToString != null) {
                this.setQuery(convertToString);
            }
            else {
                this.setQuery((CharSequence)text);
            }
        }
        else {
            this.setQuery((CharSequence)text);
        }
    }
    
    private void setQuery(final CharSequence text) {
        ((TextView)this.mSearchSrcTextView).setText(text);
        final SearchAutoComplete mSearchSrcTextView = this.mSearchSrcTextView;
        int length;
        if (TextUtils.isEmpty(text)) {
            length = 0;
        }
        else {
            length = text.length();
        }
        ((EditText)mSearchSrcTextView).setSelection(length);
    }
    
    private void updateCloseButton() {
        final boolean empty = TextUtils.isEmpty((CharSequence)((EditText)this.mSearchSrcTextView).getText());
        final boolean b = true;
        final boolean b2 = empty ^ true;
        final int n = 0;
        int n2 = b ? 1 : 0;
        if (!b2) {
            if (this.mIconifiedByDefault && !this.mExpandedInActionView) {
                n2 = (b ? 1 : 0);
            }
            else {
                n2 = 0;
            }
        }
        final ImageView mCloseButton = this.mCloseButton;
        int visibility;
        if (n2 != 0) {
            visibility = n;
        }
        else {
            visibility = 8;
        }
        mCloseButton.setVisibility(visibility);
        final Drawable drawable = this.mCloseButton.getDrawable();
        if (drawable != null) {
            int[] state;
            if (b2) {
                state = ViewGroup.ENABLED_STATE_SET;
            }
            else {
                state = ViewGroup.EMPTY_STATE_SET;
            }
            drawable.setState(state);
        }
    }
    
    private void updateQueryHint() {
        final CharSequence queryHint = this.getQueryHint();
        final SearchAutoComplete mSearchSrcTextView = this.mSearchSrcTextView;
        CharSequence charSequence = queryHint;
        if (queryHint == null) {
            charSequence = "";
        }
        ((TextView)mSearchSrcTextView).setHint(this.getDecoratedHint(charSequence));
    }
    
    private void updateSearchAutoComplete() {
        this.mSearchSrcTextView.setThreshold(this.mSearchable.getSuggestThreshold());
        ((TextView)this.mSearchSrcTextView).setImeOptions(this.mSearchable.getImeOptions());
        final int inputType = this.mSearchable.getInputType();
        final int n = 1;
        int inputType2 = inputType;
        if ((inputType & 0xF) == 0x1) {
            inputType2 = (inputType & 0xFFFEFFFF);
            if (this.mSearchable.getSuggestAuthority() != null) {
                inputType2 = (inputType2 | 0x10000 | 0x80000);
            }
        }
        ((TextView)this.mSearchSrcTextView).setInputType(inputType2);
        final CursorAdapter mSuggestionsAdapter = this.mSuggestionsAdapter;
        if (mSuggestionsAdapter != null) {
            mSuggestionsAdapter.changeCursor(null);
        }
        if (this.mSearchable.getSuggestAuthority() != null) {
            final SuggestionsAdapter suggestionsAdapter = new SuggestionsAdapter(((View)this).getContext(), this, this.mSearchable, this.mOutsideDrawablesCache);
            this.mSuggestionsAdapter = suggestionsAdapter;
            this.mSearchSrcTextView.setAdapter((ListAdapter)suggestionsAdapter);
            final SuggestionsAdapter suggestionsAdapter2 = (SuggestionsAdapter)this.mSuggestionsAdapter;
            int queryRefinement = n;
            if (this.mQueryRefinement) {
                queryRefinement = 2;
            }
            suggestionsAdapter2.setQueryRefinement(queryRefinement);
        }
    }
    
    private void updateSubmitArea() {
        int visibility;
        if (this.isSubmitAreaEnabled() && (((View)this.mGoButton).getVisibility() == 0 || ((View)this.mVoiceButton).getVisibility() == 0)) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        this.mSubmitArea.setVisibility(visibility);
    }
    
    private void updateSubmitButton(final boolean b) {
        int visibility;
        if (this.mSubmitButtonEnabled && this.isSubmitAreaEnabled() && ((View)this).hasFocus() && (b || !this.mVoiceButtonEnabled)) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        this.mGoButton.setVisibility(visibility);
    }
    
    private void updateViewsVisibility(final boolean mIconified) {
        this.mIconified = mIconified;
        final int n = 0;
        int visibility;
        if (mIconified) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        final boolean b = TextUtils.isEmpty((CharSequence)((EditText)this.mSearchSrcTextView).getText()) ^ true;
        this.mSearchButton.setVisibility(visibility);
        this.updateSubmitButton(b);
        final View mSearchEditFrame = this.mSearchEditFrame;
        int visibility2;
        if (mIconified) {
            visibility2 = 8;
        }
        else {
            visibility2 = 0;
        }
        mSearchEditFrame.setVisibility(visibility2);
        int visibility3 = 0;
        Label_0093: {
            if (this.mCollapsedIcon.getDrawable() != null) {
                visibility3 = n;
                if (!this.mIconifiedByDefault) {
                    break Label_0093;
                }
            }
            visibility3 = 8;
        }
        this.mCollapsedIcon.setVisibility(visibility3);
        this.updateCloseButton();
        this.updateVoiceButton(b ^ true);
        this.updateSubmitArea();
    }
    
    private void updateVoiceButton(final boolean b) {
        final boolean mVoiceButtonEnabled = this.mVoiceButtonEnabled;
        int visibility;
        final int n = visibility = 8;
        if (mVoiceButtonEnabled) {
            visibility = n;
            if (!this.isIconified()) {
                visibility = n;
                if (b) {
                    this.mGoButton.setVisibility(8);
                    visibility = 0;
                }
            }
        }
        this.mVoiceButton.setVisibility(visibility);
    }
    
    void adjustDropDownSizeAndPosition() {
        if (this.mDropDownAnchor.getWidth() > 1) {
            final Resources resources = ((View)this).getContext().getResources();
            final int paddingLeft = this.mSearchPlate.getPaddingLeft();
            final Rect rect = new Rect();
            final boolean layoutRtl = ViewUtils.isLayoutRtl((View)this);
            int n;
            if (this.mIconifiedByDefault) {
                n = resources.getDimensionPixelSize(R.dimen.abc_dropdownitem_icon_width) + resources.getDimensionPixelSize(R.dimen.abc_dropdownitem_text_padding_left);
            }
            else {
                n = 0;
            }
            this.mSearchSrcTextView.getDropDownBackground().getPadding(rect);
            int dropDownHorizontalOffset;
            if (layoutRtl) {
                dropDownHorizontalOffset = -rect.left;
            }
            else {
                dropDownHorizontalOffset = paddingLeft - (rect.left + n);
            }
            this.mSearchSrcTextView.setDropDownHorizontalOffset(dropDownHorizontalOffset);
            this.mSearchSrcTextView.setDropDownWidth(this.mDropDownAnchor.getWidth() + rect.left + rect.right + n - paddingLeft);
        }
    }
    
    public void clearFocus() {
        this.mClearingFocus = true;
        super.clearFocus();
        ((View)this.mSearchSrcTextView).clearFocus();
        this.mSearchSrcTextView.setImeVisibility(false);
        this.mClearingFocus = false;
    }
    
    void forceSuggestionQuery() {
        if (Build$VERSION.SDK_INT >= 29) {
            Api29Impl.refreshAutoCompleteResults(this.mSearchSrcTextView);
        }
        else {
            final PreQAutoCompleteTextViewReflector pre_API_29_HIDDEN_METHOD_INVOKER = SearchView.PRE_API_29_HIDDEN_METHOD_INVOKER;
            pre_API_29_HIDDEN_METHOD_INVOKER.doBeforeTextChanged(this.mSearchSrcTextView);
            pre_API_29_HIDDEN_METHOD_INVOKER.doAfterTextChanged(this.mSearchSrcTextView);
        }
    }
    
    public int getImeOptions() {
        return ((TextView)this.mSearchSrcTextView).getImeOptions();
    }
    
    public int getInputType() {
        return ((TextView)this.mSearchSrcTextView).getInputType();
    }
    
    public int getMaxWidth() {
        return this.mMaxWidth;
    }
    
    public CharSequence getQuery() {
        return (CharSequence)((EditText)this.mSearchSrcTextView).getText();
    }
    
    @Nullable
    public CharSequence getQueryHint() {
        CharSequence charSequence = this.mQueryHint;
        if (charSequence == null) {
            final SearchableInfo mSearchable = this.mSearchable;
            if (mSearchable != null && mSearchable.getHintId() != 0) {
                charSequence = ((View)this).getContext().getText(this.mSearchable.getHintId());
            }
            else {
                charSequence = this.mDefaultQueryHint;
            }
        }
        return charSequence;
    }
    
    int getSuggestionCommitIconResId() {
        return this.mSuggestionCommitIconResId;
    }
    
    int getSuggestionRowLayout() {
        return this.mSuggestionRowLayout;
    }
    
    public CursorAdapter getSuggestionsAdapter() {
        return this.mSuggestionsAdapter;
    }
    
    public boolean isIconfiedByDefault() {
        return this.mIconifiedByDefault;
    }
    
    public boolean isIconified() {
        return this.mIconified;
    }
    
    public boolean isQueryRefinementEnabled() {
        return this.mQueryRefinement;
    }
    
    public boolean isSubmitButtonEnabled() {
        return this.mSubmitButtonEnabled;
    }
    
    void launchQuerySearch(final int n, final String s, final String s2) {
        ((View)this).getContext().startActivity(this.createIntent("android.intent.action.SEARCH", null, null, s2, n, s));
    }
    
    @Override
    public void onActionViewCollapsed() {
        this.setQuery("", false);
        this.clearFocus();
        this.updateViewsVisibility(true);
        ((TextView)this.mSearchSrcTextView).setImeOptions(this.mCollapsedImeOptions);
        this.mExpandedInActionView = false;
    }
    
    @Override
    public void onActionViewExpanded() {
        if (this.mExpandedInActionView) {
            return;
        }
        this.mExpandedInActionView = true;
        final int imeOptions = ((TextView)this.mSearchSrcTextView).getImeOptions();
        this.mCollapsedImeOptions = imeOptions;
        ((TextView)this.mSearchSrcTextView).setImeOptions(imeOptions | 0x2000000);
        ((TextView)this.mSearchSrcTextView).setText((CharSequence)"");
        this.setIconified(false);
    }
    
    void onCloseClicked() {
        if (TextUtils.isEmpty((CharSequence)((EditText)this.mSearchSrcTextView).getText())) {
            if (this.mIconifiedByDefault) {
                final OnCloseListener mOnCloseListener = this.mOnCloseListener;
                if (mOnCloseListener == null || !mOnCloseListener.onClose()) {
                    this.clearFocus();
                    this.updateViewsVisibility(true);
                }
            }
        }
        else {
            ((TextView)this.mSearchSrcTextView).setText((CharSequence)"");
            ((View)this.mSearchSrcTextView).requestFocus();
            this.mSearchSrcTextView.setImeVisibility(true);
        }
    }
    
    protected void onDetachedFromWindow() {
        ((View)this).removeCallbacks(this.mUpdateDrawableStateRunnable);
        ((View)this).post(this.mReleaseCursorRunnable);
        super.onDetachedFromWindow();
    }
    
    boolean onItemClicked(final int n, final int n2, final String s) {
        final OnSuggestionListener mOnSuggestionListener = this.mOnSuggestionListener;
        if (mOnSuggestionListener != null && mOnSuggestionListener.onSuggestionClick(n)) {
            return false;
        }
        this.launchSuggestion(n, 0, null);
        this.mSearchSrcTextView.setImeVisibility(false);
        this.dismissSuggestions();
        return true;
    }
    
    boolean onItemSelected(final int n) {
        final OnSuggestionListener mOnSuggestionListener = this.mOnSuggestionListener;
        if (mOnSuggestionListener != null && mOnSuggestionListener.onSuggestionSelect(n)) {
            return false;
        }
        this.rewriteQueryFromSuggestion(n);
        return true;
    }
    
    @Override
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        super.onLayout(b, n, n2, n3, n4);
        if (b) {
            this.getChildBoundsWithinSearchView((View)this.mSearchSrcTextView, this.mSearchSrcTextViewBounds);
            final Rect mSearchSrtTextViewBoundsExpanded = this.mSearchSrtTextViewBoundsExpanded;
            final Rect mSearchSrcTextViewBounds = this.mSearchSrcTextViewBounds;
            mSearchSrtTextViewBoundsExpanded.set(mSearchSrcTextViewBounds.left, 0, mSearchSrcTextViewBounds.right, n4 - n2);
            final UpdatableTouchDelegate mTouchDelegate = this.mTouchDelegate;
            if (mTouchDelegate == null) {
                ((View)this).setTouchDelegate((TouchDelegate)(this.mTouchDelegate = new UpdatableTouchDelegate(this.mSearchSrtTextViewBoundsExpanded, this.mSearchSrcTextViewBounds, (View)this.mSearchSrcTextView)));
            }
            else {
                mTouchDelegate.setBounds(this.mSearchSrtTextViewBoundsExpanded, this.mSearchSrcTextViewBounds);
            }
        }
    }
    
    @Override
    protected void onMeasure(int a, int b) {
        if (this.isIconified()) {
            super.onMeasure(a, b);
            return;
        }
        final int mode = View$MeasureSpec.getMode(a);
        final int size = View$MeasureSpec.getSize(a);
        if (mode != Integer.MIN_VALUE) {
            if (mode != 0) {
                if (mode != 1073741824) {
                    a = size;
                }
                else {
                    final int mMaxWidth = this.mMaxWidth;
                    a = size;
                    if (mMaxWidth > 0) {
                        a = Math.min(mMaxWidth, size);
                    }
                }
            }
            else {
                a = this.mMaxWidth;
                if (a <= 0) {
                    a = this.getPreferredWidth();
                }
            }
        }
        else {
            a = this.mMaxWidth;
            if (a > 0) {
                a = Math.min(a, size);
            }
            else {
                a = Math.min(this.getPreferredWidth(), size);
            }
        }
        final int mode2 = View$MeasureSpec.getMode(b);
        b = View$MeasureSpec.getSize(b);
        if (mode2 != Integer.MIN_VALUE) {
            if (mode2 == 0) {
                b = this.getPreferredHeight();
            }
        }
        else {
            b = Math.min(this.getPreferredHeight(), b);
        }
        super.onMeasure(View$MeasureSpec.makeMeasureSpec(a, 1073741824), View$MeasureSpec.makeMeasureSpec(b, 1073741824));
    }
    
    protected void onQueryRefine(@Nullable final CharSequence query) {
        this.setQuery(query);
    }
    
    protected void onRestoreInstanceState(final Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        final SavedState savedState = (SavedState)parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.updateViewsVisibility(savedState.isIconified);
        ((View)this).requestLayout();
    }
    
    protected Parcelable onSaveInstanceState() {
        final SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.isIconified = this.isIconified();
        return (Parcelable)savedState;
    }
    
    void onSearchClicked() {
        this.updateViewsVisibility(false);
        ((View)this.mSearchSrcTextView).requestFocus();
        this.mSearchSrcTextView.setImeVisibility(true);
        final View$OnClickListener mOnSearchClickListener = this.mOnSearchClickListener;
        if (mOnSearchClickListener != null) {
            mOnSearchClickListener.onClick((View)this);
        }
    }
    
    void onSubmitQuery() {
        final Editable text = ((EditText)this.mSearchSrcTextView).getText();
        if (text != null && TextUtils.getTrimmedLength((CharSequence)text) > 0) {
            final OnQueryTextListener mOnQueryChangeListener = this.mOnQueryChangeListener;
            if (mOnQueryChangeListener == null || !mOnQueryChangeListener.onQueryTextSubmit(((CharSequence)text).toString())) {
                if (this.mSearchable != null) {
                    this.launchQuerySearch(0, null, ((CharSequence)text).toString());
                }
                this.mSearchSrcTextView.setImeVisibility(false);
                this.dismissSuggestions();
            }
        }
    }
    
    boolean onSuggestionsKey(final View view, int length, final KeyEvent keyEvent) {
        if (this.mSearchable == null) {
            return false;
        }
        if (this.mSuggestionsAdapter == null) {
            return false;
        }
        if (keyEvent.getAction() == 0 && keyEvent.hasNoModifiers()) {
            if (length == 66 || length == 84 || length == 61) {
                return this.onItemClicked(this.mSearchSrcTextView.getListSelection(), 0, null);
            }
            if (length == 21 || length == 22) {
                if (length == 21) {
                    length = 0;
                }
                else {
                    length = ((TextView)this.mSearchSrcTextView).length();
                }
                ((EditText)this.mSearchSrcTextView).setSelection(length);
                this.mSearchSrcTextView.setListSelection(0);
                this.mSearchSrcTextView.clearListSelection();
                this.mSearchSrcTextView.ensureImeVisible();
                return true;
            }
            if (length == 19) {
                this.mSearchSrcTextView.getListSelection();
                return false;
            }
        }
        return false;
    }
    
    void onTextChanged(final CharSequence charSequence) {
        final Editable text = ((EditText)this.mSearchSrcTextView).getText();
        this.mUserQuery = (CharSequence)text;
        final boolean b = TextUtils.isEmpty((CharSequence)text) ^ true;
        this.updateSubmitButton(b);
        this.updateVoiceButton(b ^ true);
        this.updateCloseButton();
        this.updateSubmitArea();
        if (this.mOnQueryChangeListener != null && !TextUtils.equals(charSequence, this.mOldQueryText)) {
            this.mOnQueryChangeListener.onQueryTextChange(charSequence.toString());
        }
        this.mOldQueryText = charSequence.toString();
    }
    
    void onTextFocusChanged() {
        this.updateViewsVisibility(this.isIconified());
        this.postUpdateFocusedState();
        if (((View)this.mSearchSrcTextView).hasFocus()) {
            this.forceSuggestionQuery();
        }
    }
    
    void onVoiceClicked() {
        final SearchableInfo mSearchable = this.mSearchable;
        if (mSearchable == null) {
            return;
        }
        try {
            if (mSearchable.getVoiceSearchLaunchWebSearch()) {
                ((View)this).getContext().startActivity(this.createVoiceWebSearchIntent(this.mVoiceWebSearchIntent, mSearchable));
            }
            else if (mSearchable.getVoiceSearchLaunchRecognizer()) {
                ((View)this).getContext().startActivity(this.createVoiceAppSearchIntent(this.mVoiceAppSearchIntent, mSearchable));
            }
        }
        catch (final ActivityNotFoundException ex) {}
    }
    
    public void onWindowFocusChanged(final boolean b) {
        super.onWindowFocusChanged(b);
        this.postUpdateFocusedState();
    }
    
    public boolean requestFocus(final int n, final Rect rect) {
        if (this.mClearingFocus) {
            return false;
        }
        if (!((View)this).isFocusable()) {
            return false;
        }
        if (!this.isIconified()) {
            final boolean requestFocus = ((View)this.mSearchSrcTextView).requestFocus(n, rect);
            if (requestFocus) {
                this.updateViewsVisibility(false);
            }
            return requestFocus;
        }
        return super.requestFocus(n, rect);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void setAppSearchData(final Bundle mAppSearchData) {
        this.mAppSearchData = mAppSearchData;
    }
    
    public void setIconified(final boolean b) {
        if (b) {
            this.onCloseClicked();
        }
        else {
            this.onSearchClicked();
        }
    }
    
    public void setIconifiedByDefault(final boolean mIconifiedByDefault) {
        if (this.mIconifiedByDefault == mIconifiedByDefault) {
            return;
        }
        this.updateViewsVisibility(this.mIconifiedByDefault = mIconifiedByDefault);
        this.updateQueryHint();
    }
    
    public void setImeOptions(final int imeOptions) {
        ((TextView)this.mSearchSrcTextView).setImeOptions(imeOptions);
    }
    
    public void setInputType(final int inputType) {
        ((TextView)this.mSearchSrcTextView).setInputType(inputType);
    }
    
    public void setMaxWidth(final int mMaxWidth) {
        this.mMaxWidth = mMaxWidth;
        ((View)this).requestLayout();
    }
    
    public void setOnCloseListener(final OnCloseListener mOnCloseListener) {
        this.mOnCloseListener = mOnCloseListener;
    }
    
    public void setOnQueryTextFocusChangeListener(final View$OnFocusChangeListener mOnQueryTextFocusChangeListener) {
        this.mOnQueryTextFocusChangeListener = mOnQueryTextFocusChangeListener;
    }
    
    public void setOnQueryTextListener(final OnQueryTextListener mOnQueryChangeListener) {
        this.mOnQueryChangeListener = mOnQueryChangeListener;
    }
    
    public void setOnSearchClickListener(final View$OnClickListener mOnSearchClickListener) {
        this.mOnSearchClickListener = mOnSearchClickListener;
    }
    
    public void setOnSuggestionListener(final OnSuggestionListener mOnSuggestionListener) {
        this.mOnSuggestionListener = mOnSuggestionListener;
    }
    
    public void setQuery(final CharSequence charSequence, final boolean b) {
        ((TextView)this.mSearchSrcTextView).setText(charSequence);
        if (charSequence != null) {
            final SearchAutoComplete mSearchSrcTextView = this.mSearchSrcTextView;
            ((EditText)mSearchSrcTextView).setSelection(((TextView)mSearchSrcTextView).length());
            this.mUserQuery = charSequence;
        }
        if (b && !TextUtils.isEmpty(charSequence)) {
            this.onSubmitQuery();
        }
    }
    
    public void setQueryHint(@Nullable final CharSequence mQueryHint) {
        this.mQueryHint = mQueryHint;
        this.updateQueryHint();
    }
    
    public void setQueryRefinementEnabled(final boolean mQueryRefinement) {
        this.mQueryRefinement = mQueryRefinement;
        final CursorAdapter mSuggestionsAdapter = this.mSuggestionsAdapter;
        if (mSuggestionsAdapter instanceof SuggestionsAdapter) {
            final SuggestionsAdapter suggestionsAdapter = (SuggestionsAdapter)mSuggestionsAdapter;
            int queryRefinement;
            if (mQueryRefinement) {
                queryRefinement = 2;
            }
            else {
                queryRefinement = 1;
            }
            suggestionsAdapter.setQueryRefinement(queryRefinement);
        }
    }
    
    public void setSearchableInfo(final SearchableInfo mSearchable) {
        this.mSearchable = mSearchable;
        if (mSearchable != null) {
            this.updateSearchAutoComplete();
            this.updateQueryHint();
        }
        final boolean hasVoiceSearch = this.hasVoiceSearch();
        this.mVoiceButtonEnabled = hasVoiceSearch;
        if (hasVoiceSearch) {
            ((TextView)this.mSearchSrcTextView).setPrivateImeOptions("nm");
        }
        this.updateViewsVisibility(this.isIconified());
    }
    
    public void setSubmitButtonEnabled(final boolean mSubmitButtonEnabled) {
        this.mSubmitButtonEnabled = mSubmitButtonEnabled;
        this.updateViewsVisibility(this.isIconified());
    }
    
    public void setSuggestionsAdapter(final CursorAdapter cursorAdapter) {
        this.mSuggestionsAdapter = cursorAdapter;
        this.mSearchSrcTextView.setAdapter((ListAdapter)cursorAdapter);
    }
    
    void updateFocusedState() {
        int[] array;
        if (((View)this.mSearchSrcTextView).hasFocus()) {
            array = ViewGroup.FOCUSED_STATE_SET;
        }
        else {
            array = ViewGroup.EMPTY_STATE_SET;
        }
        final Drawable background = this.mSearchPlate.getBackground();
        if (background != null) {
            background.setState(array);
        }
        final Drawable background2 = this.mSubmitArea.getBackground();
        if (background2 != null) {
            background2.setState(array);
        }
        ((View)this).invalidate();
    }
    
    @RequiresApi(29)
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        @DoNotInline
        static void refreshAutoCompleteResults(final AutoCompleteTextView autoCompleteTextView) {
            \u3007O\u300780o08O.\u3007080(autoCompleteTextView);
        }
        
        @DoNotInline
        static void setInputMethodMode(final SearchAutoComplete searchAutoComplete, final int inputMethodMode) {
            searchAutoComplete.setInputMethodMode(inputMethodMode);
        }
    }
    
    public interface OnCloseListener
    {
        boolean onClose();
    }
    
    public interface OnQueryTextListener
    {
        boolean onQueryTextChange(final String p0);
        
        boolean onQueryTextSubmit(final String p0);
    }
    
    public interface OnSuggestionListener
    {
        boolean onSuggestionClick(final int p0);
        
        boolean onSuggestionSelect(final int p0);
    }
    
    private static class PreQAutoCompleteTextViewReflector
    {
        private Method mDoAfterTextChanged;
        private Method mDoBeforeTextChanged;
        private Method mEnsureImeVisible;
        
        @SuppressLint({ "DiscouragedPrivateApi", "SoonBlockedPrivateApi" })
        PreQAutoCompleteTextViewReflector() {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     4: aload_0        
            //     5: aconst_null    
            //     6: putfield        androidx/appcompat/widget/SearchView$PreQAutoCompleteTextViewReflector.mDoBeforeTextChanged:Ljava/lang/reflect/Method;
            //     9: aload_0        
            //    10: aconst_null    
            //    11: putfield        androidx/appcompat/widget/SearchView$PreQAutoCompleteTextViewReflector.mDoAfterTextChanged:Ljava/lang/reflect/Method;
            //    14: aload_0        
            //    15: aconst_null    
            //    16: putfield        androidx/appcompat/widget/SearchView$PreQAutoCompleteTextViewReflector.mEnsureImeVisible:Ljava/lang/reflect/Method;
            //    19: invokestatic    androidx/appcompat/widget/SearchView$PreQAutoCompleteTextViewReflector.preApi29Check:()V
            //    22: ldc             Landroid/widget/AutoCompleteTextView;.class
            //    24: ldc             "doBeforeTextChanged"
            //    26: iconst_0       
            //    27: anewarray       Ljava/lang/Class;
            //    30: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
            //    33: astore_1       
            //    34: aload_0        
            //    35: aload_1        
            //    36: putfield        androidx/appcompat/widget/SearchView$PreQAutoCompleteTextViewReflector.mDoBeforeTextChanged:Ljava/lang/reflect/Method;
            //    39: aload_1        
            //    40: iconst_1       
            //    41: invokevirtual   java/lang/reflect/AccessibleObject.setAccessible:(Z)V
            //    44: ldc             Landroid/widget/AutoCompleteTextView;.class
            //    46: ldc             "doAfterTextChanged"
            //    48: iconst_0       
            //    49: anewarray       Ljava/lang/Class;
            //    52: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
            //    55: astore_1       
            //    56: aload_0        
            //    57: aload_1        
            //    58: putfield        androidx/appcompat/widget/SearchView$PreQAutoCompleteTextViewReflector.mDoAfterTextChanged:Ljava/lang/reflect/Method;
            //    61: aload_1        
            //    62: iconst_1       
            //    63: invokevirtual   java/lang/reflect/AccessibleObject.setAccessible:(Z)V
            //    66: ldc             Landroid/widget/AutoCompleteTextView;.class
            //    68: ldc             "ensureImeVisible"
            //    70: iconst_1       
            //    71: anewarray       Ljava/lang/Class;
            //    74: dup            
            //    75: iconst_0       
            //    76: getstatic       java/lang/Boolean.TYPE:Ljava/lang/Class;
            //    79: aastore        
            //    80: invokevirtual   java/lang/Class.getMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
            //    83: astore_1       
            //    84: aload_0        
            //    85: aload_1        
            //    86: putfield        androidx/appcompat/widget/SearchView$PreQAutoCompleteTextViewReflector.mEnsureImeVisible:Ljava/lang/reflect/Method;
            //    89: aload_1        
            //    90: iconst_1       
            //    91: invokevirtual   java/lang/reflect/AccessibleObject.setAccessible:(Z)V
            //    94: return         
            //    95: astore_1       
            //    96: goto            44
            //    99: astore_1       
            //   100: goto            66
            //   103: astore_1       
            //   104: goto            94
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                             
            //  -----  -----  -----  -----  ---------------------------------
            //  22     44     95     99     Ljava/lang/NoSuchMethodException;
            //  44     66     99     103    Ljava/lang/NoSuchMethodException;
            //  66     94     103    107    Ljava/lang/NoSuchMethodException;
            // 
            // The error that occurred was:
            // 
            // java.lang.IndexOutOfBoundsException: Index 59 out of bounds for length 59
            //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
            //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
            //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
            //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
            //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
            //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
            //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createConstructor(AstBuilder.java:799)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:635)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        private static void preApi29Check() {
            if (Build$VERSION.SDK_INT < 29) {
                return;
            }
            throw new UnsupportedClassVersionError("This function can only be used for API Level < 29.");
        }
        
        void doAfterTextChanged(final AutoCompleteTextView obj) {
            preApi29Check();
            final Method mDoAfterTextChanged = this.mDoAfterTextChanged;
            if (mDoAfterTextChanged == null) {
                return;
            }
            try {
                mDoAfterTextChanged.invoke(obj, new Object[0]);
            }
            catch (final Exception ex) {}
        }
        
        void doBeforeTextChanged(final AutoCompleteTextView obj) {
            preApi29Check();
            final Method mDoBeforeTextChanged = this.mDoBeforeTextChanged;
            if (mDoBeforeTextChanged == null) {
                return;
            }
            try {
                mDoBeforeTextChanged.invoke(obj, new Object[0]);
            }
            catch (final Exception ex) {}
        }
        
        void ensureImeVisible(final AutoCompleteTextView obj) {
            preApi29Check();
            final Method mEnsureImeVisible = this.mEnsureImeVisible;
            if (mEnsureImeVisible == null) {
                return;
            }
            try {
                mEnsureImeVisible.invoke(obj, Boolean.TRUE);
            }
            catch (final Exception ex) {}
        }
    }
    
    static class SavedState extends AbsSavedState
    {
        public static final Parcelable$Creator<SavedState> CREATOR;
        boolean isIconified;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$ClassLoaderCreator<SavedState>() {
                public SavedState createFromParcel(final Parcel parcel) {
                    return new SavedState(parcel, null);
                }
                
                public SavedState createFromParcel(final Parcel parcel, final ClassLoader classLoader) {
                    return new SavedState(parcel, classLoader);
                }
                
                public SavedState[] newArray(final int n) {
                    return new SavedState[n];
                }
            };
        }
        
        public SavedState(final Parcel parcel, final ClassLoader classLoader) {
            super(parcel, classLoader);
            this.isIconified = (boolean)parcel.readValue((ClassLoader)null);
        }
        
        SavedState(final Parcelable parcelable) {
            super(parcelable);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("SearchView.SavedState{");
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            sb.append(" isIconified=");
            sb.append(this.isIconified);
            sb.append("}");
            return sb.toString();
        }
        
        @Override
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeValue((Object)this.isIconified);
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public static class SearchAutoComplete extends AppCompatAutoCompleteTextView
    {
        private boolean mHasPendingShowSoftInputRequest;
        final Runnable mRunShowSoftInputIfNecessary;
        private SearchView mSearchView;
        private int mThreshold;
        
        public SearchAutoComplete(final Context context) {
            this(context, null);
        }
        
        public SearchAutoComplete(final Context context, final AttributeSet set) {
            this(context, set, R.attr.autoCompleteTextViewStyle);
        }
        
        public SearchAutoComplete(final Context context, final AttributeSet set, final int n) {
            super(context, set, n);
            this.mRunShowSoftInputIfNecessary = new Runnable() {
                final SearchAutoComplete this$0;
                
                @Override
                public void run() {
                    this.this$0.showSoftInputIfNecessary();
                }
            };
            this.mThreshold = this.getThreshold();
        }
        
        private int getSearchViewTextMinWidthDp() {
            final Configuration configuration = ((View)this).getResources().getConfiguration();
            final int screenWidthDp = configuration.screenWidthDp;
            final int screenHeightDp = configuration.screenHeightDp;
            if (screenWidthDp >= 960 && screenHeightDp >= 720 && configuration.orientation == 2) {
                return 256;
            }
            if (screenWidthDp < 600 && (screenWidthDp < 640 || screenHeightDp < 480)) {
                return 160;
            }
            return 192;
        }
        
        public boolean enoughToFilter() {
            return this.mThreshold <= 0 || super.enoughToFilter();
        }
        
        void ensureImeVisible() {
            if (Build$VERSION.SDK_INT >= 29) {
                Api29Impl.setInputMethodMode(this, 1);
                if (this.enoughToFilter()) {
                    this.showDropDown();
                }
            }
            else {
                SearchView.PRE_API_29_HIDDEN_METHOD_INVOKER.ensureImeVisible(this);
            }
        }
        
        boolean isEmpty() {
            return TextUtils.getTrimmedLength((CharSequence)((EditText)this).getText()) == 0;
        }
        
        @Override
        public InputConnection onCreateInputConnection(final EditorInfo editorInfo) {
            final InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
            if (this.mHasPendingShowSoftInputRequest) {
                ((View)this).removeCallbacks(this.mRunShowSoftInputIfNecessary);
                ((View)this).post(this.mRunShowSoftInputIfNecessary);
            }
            return onCreateInputConnection;
        }
        
        protected void onFinishInflate() {
            super.onFinishInflate();
            ((TextView)this).setMinWidth((int)TypedValue.applyDimension(1, (float)this.getSearchViewTextMinWidthDp(), ((View)this).getResources().getDisplayMetrics()));
        }
        
        protected void onFocusChanged(final boolean b, final int n, final Rect rect) {
            super.onFocusChanged(b, n, rect);
            this.mSearchView.onTextFocusChanged();
        }
        
        public boolean onKeyPreIme(final int n, final KeyEvent keyEvent) {
            if (n == 4) {
                if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                    final KeyEvent$DispatcherState keyDispatcherState = ((View)this).getKeyDispatcherState();
                    if (keyDispatcherState != null) {
                        keyDispatcherState.startTracking(keyEvent, (Object)this);
                    }
                    return true;
                }
                if (keyEvent.getAction() == 1) {
                    final KeyEvent$DispatcherState keyDispatcherState2 = ((View)this).getKeyDispatcherState();
                    if (keyDispatcherState2 != null) {
                        keyDispatcherState2.handleUpEvent(keyEvent);
                    }
                    if (keyEvent.isTracking() && !keyEvent.isCanceled()) {
                        this.mSearchView.clearFocus();
                        this.setImeVisibility(false);
                        return true;
                    }
                }
            }
            return super.onKeyPreIme(n, keyEvent);
        }
        
        public void onWindowFocusChanged(final boolean b) {
            super.onWindowFocusChanged(b);
            if (b && ((View)this.mSearchView).hasFocus() && ((View)this).getVisibility() == 0) {
                this.mHasPendingShowSoftInputRequest = true;
                if (SearchView.isLandscapeMode(((View)this).getContext())) {
                    this.ensureImeVisible();
                }
            }
        }
        
        public void performCompletion() {
        }
        
        protected void replaceText(final CharSequence charSequence) {
        }
        
        void setImeVisibility(final boolean b) {
            final InputMethodManager inputMethodManager = (InputMethodManager)((View)this).getContext().getSystemService("input_method");
            if (!b) {
                this.mHasPendingShowSoftInputRequest = false;
                ((View)this).removeCallbacks(this.mRunShowSoftInputIfNecessary);
                inputMethodManager.hideSoftInputFromWindow(((View)this).getWindowToken(), 0);
                return;
            }
            if (inputMethodManager.isActive((View)this)) {
                this.mHasPendingShowSoftInputRequest = false;
                ((View)this).removeCallbacks(this.mRunShowSoftInputIfNecessary);
                inputMethodManager.showSoftInput((View)this, 0);
                return;
            }
            this.mHasPendingShowSoftInputRequest = true;
        }
        
        void setSearchView(final SearchView mSearchView) {
            this.mSearchView = mSearchView;
        }
        
        public void setThreshold(final int n) {
            super.setThreshold(n);
            this.mThreshold = n;
        }
        
        void showSoftInputIfNecessary() {
            if (this.mHasPendingShowSoftInputRequest) {
                ((InputMethodManager)((View)this).getContext().getSystemService("input_method")).showSoftInput((View)this, 0);
                this.mHasPendingShowSoftInputRequest = false;
            }
        }
    }
    
    private static class UpdatableTouchDelegate extends TouchDelegate
    {
        private final Rect mActualBounds;
        private boolean mDelegateTargeted;
        private final View mDelegateView;
        private final int mSlop;
        private final Rect mSlopBounds;
        private final Rect mTargetBounds;
        
        public UpdatableTouchDelegate(final Rect rect, final Rect rect2, final View mDelegateView) {
            super(rect, mDelegateView);
            this.mSlop = ViewConfiguration.get(mDelegateView.getContext()).getScaledTouchSlop();
            this.mTargetBounds = new Rect();
            this.mSlopBounds = new Rect();
            this.mActualBounds = new Rect();
            this.setBounds(rect, rect2);
            this.mDelegateView = mDelegateView;
        }
        
        public boolean onTouchEvent(final MotionEvent motionEvent) {
            final int n = (int)motionEvent.getX();
            final int n2 = (int)motionEvent.getY();
            final int action = motionEvent.getAction();
            final boolean b = false;
            int mDelegateTargeted = 1;
            boolean b2 = false;
            Label_0132: {
                Label_0127: {
                    if (action != 0) {
                        if (action != 1 && action != 2) {
                            if (action != 3) {
                                break Label_0127;
                            }
                            mDelegateTargeted = (this.mDelegateTargeted ? 1 : 0);
                            this.mDelegateTargeted = false;
                        }
                        else {
                            final boolean mDelegateTargeted2 = this.mDelegateTargeted;
                            if ((mDelegateTargeted = (mDelegateTargeted2 ? 1 : 0)) != 0) {
                                mDelegateTargeted = (mDelegateTargeted2 ? 1 : 0);
                                if (!this.mSlopBounds.contains(n, n2)) {
                                    mDelegateTargeted = (mDelegateTargeted2 ? 1 : 0);
                                    b2 = false;
                                    break Label_0132;
                                }
                            }
                        }
                    }
                    else {
                        if (!this.mTargetBounds.contains(n, n2)) {
                            break Label_0127;
                        }
                        this.mDelegateTargeted = true;
                    }
                    b2 = true;
                    break Label_0132;
                }
                b2 = true;
                mDelegateTargeted = 0;
            }
            boolean dispatchTouchEvent = b;
            if (mDelegateTargeted != 0) {
                if (b2 && !this.mActualBounds.contains(n, n2)) {
                    motionEvent.setLocation((float)(this.mDelegateView.getWidth() / 2), (float)(this.mDelegateView.getHeight() / 2));
                }
                else {
                    final Rect mActualBounds = this.mActualBounds;
                    motionEvent.setLocation((float)(n - mActualBounds.left), (float)(n2 - mActualBounds.top));
                }
                dispatchTouchEvent = this.mDelegateView.dispatchTouchEvent(motionEvent);
            }
            return dispatchTouchEvent;
        }
        
        public void setBounds(Rect mSlopBounds, final Rect rect) {
            this.mTargetBounds.set(mSlopBounds);
            this.mSlopBounds.set(mSlopBounds);
            mSlopBounds = this.mSlopBounds;
            final int mSlop = this.mSlop;
            mSlopBounds.inset(-mSlop, -mSlop);
            this.mActualBounds.set(rect);
        }
    }
}

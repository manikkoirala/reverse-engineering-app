// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.graphics.Canvas;
import android.view.View;
import androidx.appcompat.R;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import androidx.annotation.NonNull;
import android.content.Context;
import android.widget.SeekBar;

public class AppCompatSeekBar extends SeekBar
{
    private final AppCompatSeekBarHelper mAppCompatSeekBarHelper;
    
    public AppCompatSeekBar(@NonNull final Context context) {
        this(context, null);
    }
    
    public AppCompatSeekBar(@NonNull final Context context, @Nullable final AttributeSet set) {
        this(context, set, R.attr.seekBarStyle);
    }
    
    public AppCompatSeekBar(@NonNull final Context context, @Nullable final AttributeSet set, final int n) {
        super(context, set, n);
        ThemeUtils.checkAppCompatTheme((View)this, ((View)this).getContext());
        (this.mAppCompatSeekBarHelper = new AppCompatSeekBarHelper(this)).loadFromAttributes(set, n);
    }
    
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        this.mAppCompatSeekBarHelper.drawableStateChanged();
    }
    
    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        this.mAppCompatSeekBarHelper.jumpDrawablesToCurrentState();
    }
    
    protected void onDraw(final Canvas canvas) {
        synchronized (this) {
            super.onDraw(canvas);
            this.mAppCompatSeekBarHelper.drawTickMarks(canvas);
        }
    }
}

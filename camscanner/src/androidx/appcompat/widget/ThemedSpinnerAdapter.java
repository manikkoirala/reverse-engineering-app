// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import androidx.appcompat.view.ContextThemeWrapper;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.content.Context;
import androidx.annotation.Nullable;
import android.content.res.Resources$Theme;
import android.widget.SpinnerAdapter;

public interface ThemedSpinnerAdapter extends SpinnerAdapter
{
    @Nullable
    Resources$Theme getDropDownViewTheme();
    
    void setDropDownViewTheme(@Nullable final Resources$Theme p0);
    
    public static final class Helper
    {
        private final Context mContext;
        private LayoutInflater mDropDownInflater;
        private final LayoutInflater mInflater;
        
        public Helper(@NonNull final Context mContext) {
            this.mContext = mContext;
            this.mInflater = LayoutInflater.from(mContext);
        }
        
        @NonNull
        public LayoutInflater getDropDownViewInflater() {
            LayoutInflater layoutInflater = this.mDropDownInflater;
            if (layoutInflater == null) {
                layoutInflater = this.mInflater;
            }
            return layoutInflater;
        }
        
        @Nullable
        public Resources$Theme getDropDownViewTheme() {
            final LayoutInflater mDropDownInflater = this.mDropDownInflater;
            Resources$Theme theme;
            if (mDropDownInflater == null) {
                theme = null;
            }
            else {
                theme = mDropDownInflater.getContext().getTheme();
            }
            return theme;
        }
        
        public void setDropDownViewTheme(@Nullable final Resources$Theme resources$Theme) {
            if (resources$Theme == null) {
                this.mDropDownInflater = null;
            }
            else if (resources$Theme.equals((Object)this.mContext.getTheme())) {
                this.mDropDownInflater = this.mInflater;
            }
            else {
                this.mDropDownInflater = LayoutInflater.from((Context)new ContextThemeWrapper(this.mContext, resources$Theme));
            }
        }
    }
}

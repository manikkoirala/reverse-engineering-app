// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import androidx.annotation.DoNotInline;
import android.text.Selection;
import android.text.Spannable;
import androidx.annotation.RequiresApi;
import androidx.annotation.Nullable;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.ClipData;
import androidx.core.view.ContentInfoCompat;
import android.content.ClipboardManager;
import android.app.Activity;
import android.widget.TextView;
import androidx.core.view.ViewCompat;
import android.os.Build$VERSION;
import android.view.DragEvent;
import androidx.annotation.NonNull;
import android.view.View;

final class AppCompatReceiveContentHelper
{
    private static final String LOG_TAG = "ReceiveContent";
    
    private AppCompatReceiveContentHelper() {
    }
    
    static boolean maybeHandleDragEventViaPerformReceiveContent(@NonNull final View obj, @NonNull final DragEvent dragEvent) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT < 31 && sdk_INT >= 24 && dragEvent.getLocalState() == null) {
            if (ViewCompat.getOnReceiveContentMimeTypes(obj) != null) {
                final Activity tryGetActivity = tryGetActivity(obj);
                if (tryGetActivity == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Can't handle drop: no activity: view=");
                    sb.append(obj);
                    return false;
                }
                if (dragEvent.getAction() == 1) {
                    return obj instanceof TextView ^ true;
                }
                if (dragEvent.getAction() == 3) {
                    boolean b;
                    if (obj instanceof TextView) {
                        b = OnDropApi24Impl.onDropForTextView(dragEvent, (TextView)obj, tryGetActivity);
                    }
                    else {
                        b = OnDropApi24Impl.onDropForView(dragEvent, obj, tryGetActivity);
                    }
                    return b;
                }
            }
        }
        return false;
    }
    
    static boolean maybeHandleMenuActionViaPerformReceiveContent(@NonNull final TextView textView, int flags) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        final int n = 0;
        if (sdk_INT < 31 && ViewCompat.getOnReceiveContentMimeTypes((View)textView) != null && (flags == 16908322 || flags == 16908337)) {
            final ClipboardManager clipboardManager = (ClipboardManager)((View)textView).getContext().getSystemService("clipboard");
            ClipData primaryClip;
            if (clipboardManager == null) {
                primaryClip = null;
            }
            else {
                primaryClip = clipboardManager.getPrimaryClip();
            }
            if (primaryClip != null && primaryClip.getItemCount() > 0) {
                final ContentInfoCompat.Builder builder = new ContentInfoCompat.Builder(primaryClip, 1);
                if (flags == 16908322) {
                    flags = n;
                }
                else {
                    flags = 1;
                }
                ViewCompat.performReceiveContent((View)textView, builder.setFlags(flags).build());
            }
            return true;
        }
        return false;
    }
    
    @Nullable
    static Activity tryGetActivity(@NonNull final View view) {
        for (Context context = view.getContext(); context instanceof ContextWrapper; context = ((ContextWrapper)context).getBaseContext()) {
            if (context instanceof Activity) {
                return (Activity)context;
            }
        }
        return null;
    }
    
    @RequiresApi(24)
    private static final class OnDropApi24Impl
    {
        @DoNotInline
        static boolean onDropForTextView(@NonNull final DragEvent dragEvent, @NonNull final TextView textView, @NonNull final Activity activity) {
            OoO8.\u3007080(activity, dragEvent);
            final int offsetForPosition = textView.getOffsetForPosition(dragEvent.getX(), dragEvent.getY());
            textView.beginBatchEdit();
            try {
                Selection.setSelection((Spannable)textView.getText(), offsetForPosition);
                ViewCompat.performReceiveContent((View)textView, new ContentInfoCompat.Builder(dragEvent.getClipData(), 3).build());
                return true;
            }
            finally {
                textView.endBatchEdit();
            }
        }
        
        @DoNotInline
        static boolean onDropForView(@NonNull final DragEvent dragEvent, @NonNull final View view, @NonNull final Activity activity) {
            OoO8.\u3007080(activity, dragEvent);
            ViewCompat.performReceiveContent(view, new ContentInfoCompat.Builder(dragEvent.getClipData(), 3).build());
            return true;
        }
    }
}

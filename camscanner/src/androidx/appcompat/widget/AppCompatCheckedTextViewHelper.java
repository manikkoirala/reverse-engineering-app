// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.content.Context;
import android.content.res.Resources$NotFoundException;
import androidx.appcompat.content.res.AppCompatResources;
import android.view.View;
import androidx.core.view.ViewCompat;
import androidx.appcompat.R;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.graphics.drawable.Drawable;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.widget.CheckedTextViewCompat;
import androidx.annotation.NonNull;
import android.widget.CheckedTextView;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
class AppCompatCheckedTextViewHelper
{
    private ColorStateList mCheckMarkTintList;
    private PorterDuff$Mode mCheckMarkTintMode;
    private boolean mHasCheckMarkTint;
    private boolean mHasCheckMarkTintMode;
    private boolean mSkipNextApply;
    @NonNull
    private final CheckedTextView mView;
    
    AppCompatCheckedTextViewHelper(@NonNull final CheckedTextView mView) {
        this.mCheckMarkTintList = null;
        this.mCheckMarkTintMode = null;
        this.mHasCheckMarkTint = false;
        this.mHasCheckMarkTintMode = false;
        this.mView = mView;
    }
    
    void applyCheckMarkTint() {
        final Drawable checkMarkDrawable = CheckedTextViewCompat.getCheckMarkDrawable(this.mView);
        if (checkMarkDrawable != null && (this.mHasCheckMarkTint || this.mHasCheckMarkTintMode)) {
            final Drawable mutate = DrawableCompat.wrap(checkMarkDrawable).mutate();
            if (this.mHasCheckMarkTint) {
                DrawableCompat.setTintList(mutate, this.mCheckMarkTintList);
            }
            if (this.mHasCheckMarkTintMode) {
                DrawableCompat.setTintMode(mutate, this.mCheckMarkTintMode);
            }
            if (mutate.isStateful()) {
                mutate.setState(((View)this.mView).getDrawableState());
            }
            this.mView.setCheckMarkDrawable(mutate);
        }
    }
    
    ColorStateList getSupportCheckMarkTintList() {
        return this.mCheckMarkTintList;
    }
    
    PorterDuff$Mode getSupportCheckMarkTintMode() {
        return this.mCheckMarkTintMode;
    }
    
    void loadFromAttributes(@Nullable final AttributeSet set, int n) {
        final Context context = ((View)this.mView).getContext();
        final int[] checkedTextView = R.styleable.CheckedTextView;
        final TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(context, set, checkedTextView, n, 0);
        final CheckedTextView mView = this.mView;
        ViewCompat.saveAttributeDataForStyleable((View)mView, ((View)mView).getContext(), checkedTextView, set, obtainStyledAttributes.getWrappedTypeArray(), n, 0);
        try {
            n = R.styleable.CheckedTextView_checkMarkCompat;
            Label_0096: {
                if (obtainStyledAttributes.hasValue(n)) {
                    n = obtainStyledAttributes.getResourceId(n, 0);
                    if (n != 0) {
                        try {
                            final CheckedTextView mView2 = this.mView;
                            mView2.setCheckMarkDrawable(AppCompatResources.getDrawable(((View)mView2).getContext(), n));
                            n = 1;
                            break Label_0096;
                        }
                        catch (final Resources$NotFoundException ex) {}
                    }
                }
                n = 0;
            }
            if (n == 0) {
                n = R.styleable.CheckedTextView_android_checkMark;
                if (obtainStyledAttributes.hasValue(n)) {
                    n = obtainStyledAttributes.getResourceId(n, 0);
                    if (n != 0) {
                        final CheckedTextView mView3 = this.mView;
                        mView3.setCheckMarkDrawable(AppCompatResources.getDrawable(((View)mView3).getContext(), n));
                    }
                }
            }
            n = R.styleable.CheckedTextView_checkMarkTint;
            if (obtainStyledAttributes.hasValue(n)) {
                CheckedTextViewCompat.setCheckMarkTintList(this.mView, obtainStyledAttributes.getColorStateList(n));
            }
            n = R.styleable.CheckedTextView_checkMarkTintMode;
            if (obtainStyledAttributes.hasValue(n)) {
                CheckedTextViewCompat.setCheckMarkTintMode(this.mView, DrawableUtils.parseTintMode(obtainStyledAttributes.getInt(n, -1), null));
            }
        }
        finally {
            obtainStyledAttributes.recycle();
        }
    }
    
    void onSetCheckMarkDrawable() {
        if (this.mSkipNextApply) {
            this.mSkipNextApply = false;
            return;
        }
        this.mSkipNextApply = true;
        this.applyCheckMarkTint();
    }
    
    void setSupportCheckMarkTintList(final ColorStateList mCheckMarkTintList) {
        this.mCheckMarkTintList = mCheckMarkTintList;
        this.mHasCheckMarkTint = true;
        this.applyCheckMarkTint();
    }
    
    void setSupportCheckMarkTintMode(@Nullable final PorterDuff$Mode mCheckMarkTintMode) {
        this.mCheckMarkTintMode = mCheckMarkTintMode;
        this.mHasCheckMarkTintMode = true;
        this.applyCheckMarkTint();
    }
}

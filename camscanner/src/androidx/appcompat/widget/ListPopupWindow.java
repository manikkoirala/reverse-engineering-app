// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.widget.Adapter;
import android.view.MotionEvent;
import android.widget.AbsListView;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import androidx.core.view.ViewCompat;
import androidx.core.widget.PopupWindowCompat;
import android.widget.PopupWindow$OnDismissListener;
import android.view.KeyEvent$DispatcherState;
import android.view.KeyEvent;
import androidx.annotation.RestrictTo;
import android.widget.ListView;
import android.view.View$OnTouchListener;
import android.view.ViewParent;
import android.os.Build$VERSION;
import android.view.ViewGroup;
import android.view.View$MeasureSpec;
import android.view.ViewGroup$LayoutParams;
import android.widget.LinearLayout$LayoutParams;
import android.widget.LinearLayout;
import android.widget.AbsListView$OnScrollListener;
import android.widget.AdapterView;
import android.content.res.TypedArray;
import androidx.annotation.StyleRes;
import androidx.annotation.AttrRes;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import androidx.appcompat.R;
import androidx.annotation.NonNull;
import android.widget.PopupWindow;
import android.database.DataSetObserver;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.AdapterView$OnItemClickListener;
import android.os.Handler;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.content.Context;
import android.widget.ListAdapter;
import java.lang.reflect.Method;
import androidx.appcompat.view.menu.ShowableListMenu;

public class ListPopupWindow implements ShowableListMenu
{
    private static final boolean DEBUG = false;
    static final int EXPAND_LIST_TIMEOUT = 250;
    public static final int INPUT_METHOD_FROM_FOCUSABLE = 0;
    public static final int INPUT_METHOD_NEEDED = 1;
    public static final int INPUT_METHOD_NOT_NEEDED = 2;
    public static final int MATCH_PARENT = -1;
    public static final int POSITION_PROMPT_ABOVE = 0;
    public static final int POSITION_PROMPT_BELOW = 1;
    private static final String TAG = "ListPopupWindow";
    public static final int WRAP_CONTENT = -2;
    private static Method sGetMaxAvailableHeightMethod;
    private static Method sSetClipToWindowEnabledMethod;
    private static Method sSetEpicenterBoundsMethod;
    private ListAdapter mAdapter;
    private Context mContext;
    private boolean mDropDownAlwaysVisible;
    private View mDropDownAnchorView;
    private int mDropDownGravity;
    private int mDropDownHeight;
    private int mDropDownHorizontalOffset;
    DropDownListView mDropDownList;
    private Drawable mDropDownListHighlight;
    private int mDropDownVerticalOffset;
    private boolean mDropDownVerticalOffsetSet;
    private int mDropDownWidth;
    private int mDropDownWindowLayoutType;
    private Rect mEpicenterBounds;
    private boolean mForceIgnoreOutsideTouch;
    final Handler mHandler;
    private final ListSelectorHider mHideSelector;
    private AdapterView$OnItemClickListener mItemClickListener;
    private AdapterView$OnItemSelectedListener mItemSelectedListener;
    int mListItemExpandMaximum;
    private boolean mModal;
    private DataSetObserver mObserver;
    private boolean mOverlapAnchor;
    private boolean mOverlapAnchorSet;
    PopupWindow mPopup;
    private int mPromptPosition;
    private View mPromptView;
    final ResizePopupRunnable mResizePopupRunnable;
    private final PopupScrollListener mScrollListener;
    private Runnable mShowDropDownRunnable;
    private final Rect mTempRect;
    private final PopupTouchInterceptor mTouchInterceptor;
    
    static {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: bipush          28
        //     5: if_icmpgt       51
        //     8: ldc             Landroid/widget/PopupWindow;.class
        //    10: ldc             "setClipToScreenEnabled"
        //    12: iconst_1       
        //    13: anewarray       Ljava/lang/Class;
        //    16: dup            
        //    17: iconst_0       
        //    18: getstatic       java/lang/Boolean.TYPE:Ljava/lang/Class;
        //    21: aastore        
        //    22: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //    25: putstatic       androidx/appcompat/widget/ListPopupWindow.sSetClipToWindowEnabledMethod:Ljava/lang/reflect/Method;
        //    28: ldc             Landroid/widget/PopupWindow;.class
        //    30: ldc             "setEpicenterBounds"
        //    32: iconst_1       
        //    33: anewarray       Ljava/lang/Class;
        //    36: dup            
        //    37: iconst_0       
        //    38: ldc             Landroid/graphics/Rect;.class
        //    40: aastore        
        //    41: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //    44: putstatic       androidx/appcompat/widget/ListPopupWindow.sSetEpicenterBoundsMethod:Ljava/lang/reflect/Method;
        //    47: goto            51
        //    50: astore_0       
        //    51: getstatic       android/os/Build$VERSION.SDK_INT:I
        //    54: bipush          23
        //    56: if_icmpgt       90
        //    59: ldc             Landroid/widget/PopupWindow;.class
        //    61: ldc             "getMaxAvailableHeight"
        //    63: iconst_3       
        //    64: anewarray       Ljava/lang/Class;
        //    67: dup            
        //    68: iconst_0       
        //    69: ldc             Landroid/view/View;.class
        //    71: aastore        
        //    72: dup            
        //    73: iconst_1       
        //    74: getstatic       java/lang/Integer.TYPE:Ljava/lang/Class;
        //    77: aastore        
        //    78: dup            
        //    79: iconst_2       
        //    80: getstatic       java/lang/Boolean.TYPE:Ljava/lang/Class;
        //    83: aastore        
        //    84: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //    87: putstatic       androidx/appcompat/widget/ListPopupWindow.sGetMaxAvailableHeightMethod:Ljava/lang/reflect/Method;
        //    90: return         
        //    91: astore_0       
        //    92: goto            28
        //    95: astore_0       
        //    96: goto            90
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                             
        //  -----  -----  -----  -----  ---------------------------------
        //  8      28     91     95     Ljava/lang/NoSuchMethodException;
        //  28     47     50     51     Ljava/lang/NoSuchMethodException;
        //  59     90     95     99     Ljava/lang/NoSuchMethodException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0090:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public ListPopupWindow(@NonNull final Context context) {
        this(context, null, R.attr.listPopupWindowStyle);
    }
    
    public ListPopupWindow(@NonNull final Context context, @Nullable final AttributeSet set) {
        this(context, set, R.attr.listPopupWindowStyle);
    }
    
    public ListPopupWindow(@NonNull final Context context, @Nullable final AttributeSet set, @AttrRes final int n) {
        this(context, set, n, 0);
    }
    
    public ListPopupWindow(@NonNull final Context mContext, @Nullable final AttributeSet set, @AttrRes final int n, @StyleRes final int n2) {
        this.mDropDownHeight = -2;
        this.mDropDownWidth = -2;
        this.mDropDownWindowLayoutType = 1002;
        this.mDropDownGravity = 0;
        this.mDropDownAlwaysVisible = false;
        this.mForceIgnoreOutsideTouch = false;
        this.mListItemExpandMaximum = Integer.MAX_VALUE;
        this.mPromptPosition = 0;
        this.mResizePopupRunnable = new ResizePopupRunnable();
        this.mTouchInterceptor = new PopupTouchInterceptor();
        this.mScrollListener = new PopupScrollListener();
        this.mHideSelector = new ListSelectorHider();
        this.mTempRect = new Rect();
        this.mContext = mContext;
        this.mHandler = new Handler(mContext.getMainLooper());
        final TypedArray obtainStyledAttributes = mContext.obtainStyledAttributes(set, R.styleable.ListPopupWindow, n, n2);
        this.mDropDownHorizontalOffset = obtainStyledAttributes.getDimensionPixelOffset(R.styleable.ListPopupWindow_android_dropDownHorizontalOffset, 0);
        final int dimensionPixelOffset = obtainStyledAttributes.getDimensionPixelOffset(R.styleable.ListPopupWindow_android_dropDownVerticalOffset, 0);
        this.mDropDownVerticalOffset = dimensionPixelOffset;
        if (dimensionPixelOffset != 0) {
            this.mDropDownVerticalOffsetSet = true;
        }
        obtainStyledAttributes.recycle();
        (this.mPopup = new AppCompatPopupWindow(mContext, set, n, n2)).setInputMethodMode(1);
    }
    
    private int buildDropDown() {
        final DropDownListView mDropDownList = this.mDropDownList;
        boolean b = true;
        int n2;
        if (mDropDownList == null) {
            final Context mContext = this.mContext;
            this.mShowDropDownRunnable = new Runnable(this) {
                final ListPopupWindow this$0;
                
                @Override
                public void run() {
                    final View anchorView = this.this$0.getAnchorView();
                    if (anchorView != null && anchorView.getWindowToken() != null) {
                        this.this$0.show();
                    }
                }
            };
            final DropDownListView dropDownListView = this.createDropDownListView(mContext, this.mModal ^ true);
            this.mDropDownList = dropDownListView;
            final Drawable mDropDownListHighlight = this.mDropDownListHighlight;
            if (mDropDownListHighlight != null) {
                dropDownListView.setSelector(mDropDownListHighlight);
            }
            ((AbsListView)this.mDropDownList).setAdapter(this.mAdapter);
            ((AdapterView)this.mDropDownList).setOnItemClickListener(this.mItemClickListener);
            ((View)this.mDropDownList).setFocusable(true);
            ((View)this.mDropDownList).setFocusableInTouchMode(true);
            ((AdapterView)this.mDropDownList).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this) {
                final ListPopupWindow this$0;
                
                public void onItemSelected(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
                    if (n != -1) {
                        final DropDownListView mDropDownList = this.this$0.mDropDownList;
                        if (mDropDownList != null) {
                            mDropDownList.setListSelectionHidden(false);
                        }
                    }
                }
                
                public void onNothingSelected(final AdapterView<?> adapterView) {
                }
            });
            ((AbsListView)this.mDropDownList).setOnScrollListener((AbsListView$OnScrollListener)this.mScrollListener);
            final AdapterView$OnItemSelectedListener mItemSelectedListener = this.mItemSelectedListener;
            if (mItemSelectedListener != null) {
                ((AdapterView)this.mDropDownList).setOnItemSelectedListener(mItemSelectedListener);
            }
            final DropDownListView mDropDownList2 = this.mDropDownList;
            final View mPromptView = this.mPromptView;
            Object contentView;
            if (mPromptView != null) {
                contentView = new LinearLayout(mContext);
                ((LinearLayout)contentView).setOrientation(1);
                final LinearLayout$LayoutParams linearLayout$LayoutParams = new LinearLayout$LayoutParams(-1, 0, 1.0f);
                final int mPromptPosition = this.mPromptPosition;
                if (mPromptPosition != 0) {
                    if (mPromptPosition != 1) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Invalid hint position ");
                        sb.append(this.mPromptPosition);
                    }
                    else {
                        ((ViewGroup)contentView).addView((View)mDropDownList2, (ViewGroup$LayoutParams)linearLayout$LayoutParams);
                        ((ViewGroup)contentView).addView(mPromptView);
                    }
                }
                else {
                    ((ViewGroup)contentView).addView(mPromptView);
                    ((ViewGroup)contentView).addView((View)mDropDownList2, (ViewGroup$LayoutParams)linearLayout$LayoutParams);
                }
                int mDropDownWidth = this.mDropDownWidth;
                int n;
                if (mDropDownWidth >= 0) {
                    n = Integer.MIN_VALUE;
                }
                else {
                    mDropDownWidth = 0;
                    n = 0;
                }
                mPromptView.measure(View$MeasureSpec.makeMeasureSpec(mDropDownWidth, n), 0);
                final LinearLayout$LayoutParams linearLayout$LayoutParams2 = (LinearLayout$LayoutParams)mPromptView.getLayoutParams();
                n2 = mPromptView.getMeasuredHeight() + linearLayout$LayoutParams2.topMargin + linearLayout$LayoutParams2.bottomMargin;
            }
            else {
                n2 = 0;
                contentView = mDropDownList2;
            }
            this.mPopup.setContentView((View)contentView);
        }
        else {
            final ViewGroup viewGroup = (ViewGroup)this.mPopup.getContentView();
            final View mPromptView2 = this.mPromptView;
            if (mPromptView2 != null) {
                final LinearLayout$LayoutParams linearLayout$LayoutParams3 = (LinearLayout$LayoutParams)mPromptView2.getLayoutParams();
                n2 = mPromptView2.getMeasuredHeight() + linearLayout$LayoutParams3.topMargin + linearLayout$LayoutParams3.bottomMargin;
            }
            else {
                n2 = 0;
            }
        }
        final Drawable background = this.mPopup.getBackground();
        int n4;
        if (background != null) {
            background.getPadding(this.mTempRect);
            final Rect mTempRect = this.mTempRect;
            final int top = mTempRect.top;
            final int n3 = n4 = mTempRect.bottom + top;
            if (!this.mDropDownVerticalOffsetSet) {
                this.mDropDownVerticalOffset = -top;
                n4 = n3;
            }
        }
        else {
            this.mTempRect.setEmpty();
            n4 = 0;
        }
        if (this.mPopup.getInputMethodMode() != 2) {
            b = false;
        }
        final int maxAvailableHeight = this.getMaxAvailableHeight(this.getAnchorView(), this.mDropDownVerticalOffset, b);
        if (!this.mDropDownAlwaysVisible && this.mDropDownHeight != -1) {
            final int mDropDownWidth2 = this.mDropDownWidth;
            int n5;
            if (mDropDownWidth2 != -2) {
                if (mDropDownWidth2 != -1) {
                    n5 = View$MeasureSpec.makeMeasureSpec(mDropDownWidth2, 1073741824);
                }
                else {
                    final int widthPixels = this.mContext.getResources().getDisplayMetrics().widthPixels;
                    final Rect mTempRect2 = this.mTempRect;
                    n5 = View$MeasureSpec.makeMeasureSpec(widthPixels - (mTempRect2.left + mTempRect2.right), 1073741824);
                }
            }
            else {
                final int widthPixels2 = this.mContext.getResources().getDisplayMetrics().widthPixels;
                final Rect mTempRect3 = this.mTempRect;
                n5 = View$MeasureSpec.makeMeasureSpec(widthPixels2 - (mTempRect3.left + mTempRect3.right), Integer.MIN_VALUE);
            }
            final int measureHeightOfChildrenCompat = this.mDropDownList.measureHeightOfChildrenCompat(n5, 0, -1, maxAvailableHeight - n2, -1);
            int n6 = n2;
            if (measureHeightOfChildrenCompat > 0) {
                n6 = n2 + (n4 + (((View)this.mDropDownList).getPaddingTop() + ((View)this.mDropDownList).getPaddingBottom()));
            }
            return measureHeightOfChildrenCompat + n6;
        }
        return maxAvailableHeight + n4;
    }
    
    private int getMaxAvailableHeight(final View view, final int i, final boolean b) {
        Label_0070: {
            if (Build$VERSION.SDK_INT > 23) {
                break Label_0070;
            }
            final Method sGetMaxAvailableHeightMethod = ListPopupWindow.sGetMaxAvailableHeightMethod;
            Label_0060: {
                if (sGetMaxAvailableHeightMethod == null) {
                    break Label_0060;
                }
                try {
                    return (int)sGetMaxAvailableHeightMethod.invoke(this.mPopup, view, i, b);
                    return Api24Impl.getMaxAvailableHeight(this.mPopup, view, i, b);
                    return this.mPopup.getMaxAvailableHeight(view, i);
                }
                catch (final Exception ex) {
                    return this.mPopup.getMaxAvailableHeight(view, i);
                }
            }
        }
    }
    
    private static boolean isConfirmKey(final int n) {
        return n == 66 || n == 23;
    }
    
    private void removePromptView() {
        final View mPromptView = this.mPromptView;
        if (mPromptView != null) {
            final ViewParent parent = mPromptView.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup)parent).removeView(this.mPromptView);
            }
        }
    }
    
    private void setPopupClipToScreenEnabled(final boolean b) {
        Label_0039: {
            if (Build$VERSION.SDK_INT > 28) {
                break Label_0039;
            }
            final Method sSetClipToWindowEnabledMethod = ListPopupWindow.sSetClipToWindowEnabledMethod;
            if (sSetClipToWindowEnabledMethod == null) {
                return;
            }
            try {
                sSetClipToWindowEnabledMethod.invoke(this.mPopup, b);
                return;
                Api29Impl.setIsClippedToScreen(this.mPopup, b);
            }
            catch (final Exception ex) {}
        }
    }
    
    public void clearListSelection() {
        final DropDownListView mDropDownList = this.mDropDownList;
        if (mDropDownList != null) {
            mDropDownList.setListSelectionHidden(true);
            ((View)mDropDownList).requestLayout();
        }
    }
    
    public View$OnTouchListener createDragToOpenListener(final View view) {
        return (View$OnTouchListener)new ForwardingListener(this, view) {
            final ListPopupWindow this$0;
            
            @Override
            public ListPopupWindow getPopup() {
                return this.this$0;
            }
        };
    }
    
    @NonNull
    DropDownListView createDropDownListView(final Context context, final boolean b) {
        return new DropDownListView(context, b);
    }
    
    @Override
    public void dismiss() {
        this.mPopup.dismiss();
        this.removePromptView();
        this.mPopup.setContentView((View)null);
        this.mDropDownList = null;
        this.mHandler.removeCallbacks((Runnable)this.mResizePopupRunnable);
    }
    
    @Nullable
    public View getAnchorView() {
        return this.mDropDownAnchorView;
    }
    
    @StyleRes
    public int getAnimationStyle() {
        return this.mPopup.getAnimationStyle();
    }
    
    @Nullable
    public Drawable getBackground() {
        return this.mPopup.getBackground();
    }
    
    @Nullable
    public Rect getEpicenterBounds() {
        Rect rect;
        if (this.mEpicenterBounds != null) {
            rect = new Rect(this.mEpicenterBounds);
        }
        else {
            rect = null;
        }
        return rect;
    }
    
    public int getHeight() {
        return this.mDropDownHeight;
    }
    
    public int getHorizontalOffset() {
        return this.mDropDownHorizontalOffset;
    }
    
    public int getInputMethodMode() {
        return this.mPopup.getInputMethodMode();
    }
    
    @Nullable
    @Override
    public ListView getListView() {
        return this.mDropDownList;
    }
    
    public int getPromptPosition() {
        return this.mPromptPosition;
    }
    
    @Nullable
    public Object getSelectedItem() {
        if (!this.isShowing()) {
            return null;
        }
        return ((AdapterView)this.mDropDownList).getSelectedItem();
    }
    
    public long getSelectedItemId() {
        if (!this.isShowing()) {
            return Long.MIN_VALUE;
        }
        return ((AdapterView)this.mDropDownList).getSelectedItemId();
    }
    
    public int getSelectedItemPosition() {
        if (!this.isShowing()) {
            return -1;
        }
        return ((AdapterView)this.mDropDownList).getSelectedItemPosition();
    }
    
    @Nullable
    public View getSelectedView() {
        if (!this.isShowing()) {
            return null;
        }
        return ((AdapterView)this.mDropDownList).getSelectedView();
    }
    
    public int getSoftInputMode() {
        return this.mPopup.getSoftInputMode();
    }
    
    public int getVerticalOffset() {
        if (!this.mDropDownVerticalOffsetSet) {
            return 0;
        }
        return this.mDropDownVerticalOffset;
    }
    
    public int getWidth() {
        return this.mDropDownWidth;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public boolean isDropDownAlwaysVisible() {
        return this.mDropDownAlwaysVisible;
    }
    
    public boolean isInputMethodNotNeeded() {
        return this.mPopup.getInputMethodMode() == 2;
    }
    
    public boolean isModal() {
        return this.mModal;
    }
    
    @Override
    public boolean isShowing() {
        return this.mPopup.isShowing();
    }
    
    public boolean onKeyDown(final int n, @NonNull final KeyEvent keyEvent) {
        if (this.isShowing() && n != 62 && (((AdapterView)this.mDropDownList).getSelectedItemPosition() >= 0 || !isConfirmKey(n))) {
            final int selectedItemPosition = ((AdapterView)this.mDropDownList).getSelectedItemPosition();
            final boolean b = this.mPopup.isAboveAnchor() ^ true;
            final ListAdapter mAdapter = this.mAdapter;
            int lookForSelectablePosition;
            int lookForSelectablePosition2;
            if (mAdapter != null) {
                final boolean allItemsEnabled = mAdapter.areAllItemsEnabled();
                if (allItemsEnabled) {
                    lookForSelectablePosition = 0;
                }
                else {
                    lookForSelectablePosition = this.mDropDownList.lookForSelectablePosition(0, true);
                }
                if (allItemsEnabled) {
                    lookForSelectablePosition2 = ((Adapter)mAdapter).getCount() - 1;
                }
                else {
                    lookForSelectablePosition2 = this.mDropDownList.lookForSelectablePosition(((Adapter)mAdapter).getCount() - 1, false);
                }
            }
            else {
                lookForSelectablePosition = Integer.MAX_VALUE;
                lookForSelectablePosition2 = Integer.MIN_VALUE;
            }
            if ((b && n == 19 && selectedItemPosition <= lookForSelectablePosition) || (!b && n == 20 && selectedItemPosition >= lookForSelectablePosition2)) {
                this.clearListSelection();
                this.mPopup.setInputMethodMode(1);
                this.show();
                return true;
            }
            this.mDropDownList.setListSelectionHidden(false);
            if (((View)this.mDropDownList).onKeyDown(n, keyEvent)) {
                this.mPopup.setInputMethodMode(2);
                ((View)this.mDropDownList).requestFocusFromTouch();
                this.show();
                if (n == 19 || n == 20 || n == 23 || n == 66) {
                    return true;
                }
            }
            else if (b && n == 20) {
                if (selectedItemPosition == lookForSelectablePosition2) {
                    return true;
                }
            }
            else if (!b && n == 19 && selectedItemPosition == lookForSelectablePosition) {
                return true;
            }
        }
        return false;
    }
    
    public boolean onKeyPreIme(final int n, @NonNull final KeyEvent keyEvent) {
        if (n == 4 && this.isShowing()) {
            final View mDropDownAnchorView = this.mDropDownAnchorView;
            if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                final KeyEvent$DispatcherState keyDispatcherState = mDropDownAnchorView.getKeyDispatcherState();
                if (keyDispatcherState != null) {
                    keyDispatcherState.startTracking(keyEvent, (Object)this);
                }
                return true;
            }
            if (keyEvent.getAction() == 1) {
                final KeyEvent$DispatcherState keyDispatcherState2 = mDropDownAnchorView.getKeyDispatcherState();
                if (keyDispatcherState2 != null) {
                    keyDispatcherState2.handleUpEvent(keyEvent);
                }
                if (keyEvent.isTracking() && !keyEvent.isCanceled()) {
                    this.dismiss();
                    return true;
                }
            }
        }
        return false;
    }
    
    public boolean onKeyUp(final int n, @NonNull final KeyEvent keyEvent) {
        if (this.isShowing() && ((AdapterView)this.mDropDownList).getSelectedItemPosition() >= 0) {
            final boolean onKeyUp = ((View)this.mDropDownList).onKeyUp(n, keyEvent);
            if (onKeyUp && isConfirmKey(n)) {
                this.dismiss();
            }
            return onKeyUp;
        }
        return false;
    }
    
    public boolean performItemClick(final int n) {
        if (this.isShowing()) {
            if (this.mItemClickListener != null) {
                final DropDownListView mDropDownList = this.mDropDownList;
                this.mItemClickListener.onItemClick((AdapterView)mDropDownList, ((ViewGroup)mDropDownList).getChildAt(n - ((AdapterView)mDropDownList).getFirstVisiblePosition()), n, ((Adapter)mDropDownList.getAdapter()).getItemId(n));
            }
            return true;
        }
        return false;
    }
    
    public void postShow() {
        this.mHandler.post(this.mShowDropDownRunnable);
    }
    
    public void setAdapter(@Nullable final ListAdapter mAdapter) {
        final DataSetObserver mObserver = this.mObserver;
        if (mObserver == null) {
            this.mObserver = new PopupDataSetObserver();
        }
        else {
            final ListAdapter mAdapter2 = this.mAdapter;
            if (mAdapter2 != null) {
                ((Adapter)mAdapter2).unregisterDataSetObserver(mObserver);
            }
        }
        if ((this.mAdapter = mAdapter) != null) {
            ((Adapter)mAdapter).registerDataSetObserver(this.mObserver);
        }
        final DropDownListView mDropDownList = this.mDropDownList;
        if (mDropDownList != null) {
            ((AbsListView)mDropDownList).setAdapter(this.mAdapter);
        }
    }
    
    public void setAnchorView(@Nullable final View mDropDownAnchorView) {
        this.mDropDownAnchorView = mDropDownAnchorView;
    }
    
    public void setAnimationStyle(@StyleRes final int animationStyle) {
        this.mPopup.setAnimationStyle(animationStyle);
    }
    
    public void setBackgroundDrawable(@Nullable final Drawable backgroundDrawable) {
        this.mPopup.setBackgroundDrawable(backgroundDrawable);
    }
    
    public void setContentWidth(final int width) {
        final Drawable background = this.mPopup.getBackground();
        if (background != null) {
            background.getPadding(this.mTempRect);
            final Rect mTempRect = this.mTempRect;
            this.mDropDownWidth = mTempRect.left + mTempRect.right + width;
        }
        else {
            this.setWidth(width);
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void setDropDownAlwaysVisible(final boolean mDropDownAlwaysVisible) {
        this.mDropDownAlwaysVisible = mDropDownAlwaysVisible;
    }
    
    public void setDropDownGravity(final int mDropDownGravity) {
        this.mDropDownGravity = mDropDownGravity;
    }
    
    public void setEpicenterBounds(@Nullable Rect mEpicenterBounds) {
        if (mEpicenterBounds != null) {
            mEpicenterBounds = new Rect(mEpicenterBounds);
        }
        else {
            mEpicenterBounds = null;
        }
        this.mEpicenterBounds = mEpicenterBounds;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void setForceIgnoreOutsideTouch(final boolean mForceIgnoreOutsideTouch) {
        this.mForceIgnoreOutsideTouch = mForceIgnoreOutsideTouch;
    }
    
    public void setHeight(final int mDropDownHeight) {
        if (mDropDownHeight < 0 && -2 != mDropDownHeight && -1 != mDropDownHeight) {
            throw new IllegalArgumentException("Invalid height. Must be a positive value, MATCH_PARENT, or WRAP_CONTENT.");
        }
        this.mDropDownHeight = mDropDownHeight;
    }
    
    public void setHorizontalOffset(final int mDropDownHorizontalOffset) {
        this.mDropDownHorizontalOffset = mDropDownHorizontalOffset;
    }
    
    public void setInputMethodMode(final int inputMethodMode) {
        this.mPopup.setInputMethodMode(inputMethodMode);
    }
    
    void setListItemExpandMax(final int mListItemExpandMaximum) {
        this.mListItemExpandMaximum = mListItemExpandMaximum;
    }
    
    public void setListSelector(final Drawable mDropDownListHighlight) {
        this.mDropDownListHighlight = mDropDownListHighlight;
    }
    
    public void setModal(final boolean b) {
        this.mModal = b;
        this.mPopup.setFocusable(b);
    }
    
    public void setOnDismissListener(@Nullable final PopupWindow$OnDismissListener onDismissListener) {
        this.mPopup.setOnDismissListener(onDismissListener);
    }
    
    public void setOnItemClickListener(@Nullable final AdapterView$OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }
    
    public void setOnItemSelectedListener(@Nullable final AdapterView$OnItemSelectedListener mItemSelectedListener) {
        this.mItemSelectedListener = mItemSelectedListener;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void setOverlapAnchor(final boolean mOverlapAnchor) {
        this.mOverlapAnchorSet = true;
        this.mOverlapAnchor = mOverlapAnchor;
    }
    
    public void setPromptPosition(final int mPromptPosition) {
        this.mPromptPosition = mPromptPosition;
    }
    
    public void setPromptView(@Nullable final View mPromptView) {
        final boolean showing = this.isShowing();
        if (showing) {
            this.removePromptView();
        }
        this.mPromptView = mPromptView;
        if (showing) {
            this.show();
        }
    }
    
    public void setSelection(final int selection) {
        final DropDownListView mDropDownList = this.mDropDownList;
        if (this.isShowing() && mDropDownList != null) {
            mDropDownList.setListSelectionHidden(false);
            ((AdapterView)mDropDownList).setSelection(selection);
            if (((AbsListView)mDropDownList).getChoiceMode() != 0) {
                ((AbsListView)mDropDownList).setItemChecked(selection, true);
            }
        }
    }
    
    public void setSoftInputMode(final int softInputMode) {
        this.mPopup.setSoftInputMode(softInputMode);
    }
    
    public void setVerticalOffset(final int mDropDownVerticalOffset) {
        this.mDropDownVerticalOffset = mDropDownVerticalOffset;
        this.mDropDownVerticalOffsetSet = true;
    }
    
    public void setWidth(final int mDropDownWidth) {
        this.mDropDownWidth = mDropDownWidth;
    }
    
    public void setWindowLayoutType(final int mDropDownWindowLayoutType) {
        this.mDropDownWindowLayoutType = mDropDownWindowLayoutType;
    }
    
    @Override
    public void show() {
        int buildDropDown = this.buildDropDown();
        final boolean inputMethodNotNeeded = this.isInputMethodNotNeeded();
        PopupWindowCompat.setWindowLayoutType(this.mPopup, this.mDropDownWindowLayoutType);
        final boolean showing = this.mPopup.isShowing();
        boolean outsideTouchable = true;
        if (showing) {
            if (!ViewCompat.isAttachedToWindow(this.getAnchorView())) {
                return;
            }
            final int mDropDownWidth = this.mDropDownWidth;
            int width;
            if (mDropDownWidth == -1) {
                width = -1;
            }
            else if ((width = mDropDownWidth) == -2) {
                width = this.getAnchorView().getWidth();
            }
            final int mDropDownHeight = this.mDropDownHeight;
            if (mDropDownHeight == -1) {
                if (!inputMethodNotNeeded) {
                    buildDropDown = -1;
                }
                if (inputMethodNotNeeded) {
                    final PopupWindow mPopup = this.mPopup;
                    int width2;
                    if (this.mDropDownWidth == -1) {
                        width2 = -1;
                    }
                    else {
                        width2 = 0;
                    }
                    mPopup.setWidth(width2);
                    this.mPopup.setHeight(0);
                }
                else {
                    final PopupWindow mPopup2 = this.mPopup;
                    int width3;
                    if (this.mDropDownWidth == -1) {
                        width3 = -1;
                    }
                    else {
                        width3 = 0;
                    }
                    mPopup2.setWidth(width3);
                    this.mPopup.setHeight(-1);
                }
            }
            else if (mDropDownHeight != -2) {
                buildDropDown = mDropDownHeight;
            }
            final PopupWindow mPopup3 = this.mPopup;
            if (this.mForceIgnoreOutsideTouch || this.mDropDownAlwaysVisible) {
                outsideTouchable = false;
            }
            mPopup3.setOutsideTouchable(outsideTouchable);
            final PopupWindow mPopup4 = this.mPopup;
            final View anchorView = this.getAnchorView();
            final int mDropDownHorizontalOffset = this.mDropDownHorizontalOffset;
            final int mDropDownVerticalOffset = this.mDropDownVerticalOffset;
            if (width < 0) {
                width = -1;
            }
            if (buildDropDown < 0) {
                buildDropDown = -1;
            }
            mPopup4.update(anchorView, mDropDownHorizontalOffset, mDropDownVerticalOffset, width, buildDropDown);
        }
        else {
            final int mDropDownWidth2 = this.mDropDownWidth;
            int width4;
            if (mDropDownWidth2 == -1) {
                width4 = -1;
            }
            else if ((width4 = mDropDownWidth2) == -2) {
                width4 = this.getAnchorView().getWidth();
            }
            final int mDropDownHeight2 = this.mDropDownHeight;
            if (mDropDownHeight2 == -1) {
                buildDropDown = -1;
            }
            else if (mDropDownHeight2 != -2) {
                buildDropDown = mDropDownHeight2;
            }
            this.mPopup.setWidth(width4);
            this.mPopup.setHeight(buildDropDown);
            this.setPopupClipToScreenEnabled(true);
            this.mPopup.setOutsideTouchable(!this.mForceIgnoreOutsideTouch && !this.mDropDownAlwaysVisible);
            this.mPopup.setTouchInterceptor((View$OnTouchListener)this.mTouchInterceptor);
            if (this.mOverlapAnchorSet) {
                PopupWindowCompat.setOverlapAnchor(this.mPopup, this.mOverlapAnchor);
            }
            if (Build$VERSION.SDK_INT <= 28) {
                final Method sSetEpicenterBoundsMethod = ListPopupWindow.sSetEpicenterBoundsMethod;
                if (sSetEpicenterBoundsMethod != null) {
                    try {
                        sSetEpicenterBoundsMethod.invoke(this.mPopup, this.mEpicenterBounds);
                    }
                    catch (final Exception ex) {}
                }
            }
            else {
                Api29Impl.setEpicenterBounds(this.mPopup, this.mEpicenterBounds);
            }
            PopupWindowCompat.showAsDropDown(this.mPopup, this.getAnchorView(), this.mDropDownHorizontalOffset, this.mDropDownVerticalOffset, this.mDropDownGravity);
            ((AdapterView)this.mDropDownList).setSelection(-1);
            if (!this.mModal || this.mDropDownList.isInTouchMode()) {
                this.clearListSelection();
            }
            if (!this.mModal) {
                this.mHandler.post((Runnable)this.mHideSelector);
            }
        }
    }
    
    @RequiresApi(24)
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        @DoNotInline
        static int getMaxAvailableHeight(final PopupWindow popupWindow, final View view, final int n, final boolean b) {
            return o\u30078oOO88.\u3007080(popupWindow, view, n, b);
        }
    }
    
    @RequiresApi(29)
    static class Api29Impl
    {
        private Api29Impl() {
        }
        
        @DoNotInline
        static void setEpicenterBounds(final PopupWindow popupWindow, final Rect rect) {
            o\u3007O.\u3007080(popupWindow, rect);
        }
        
        @DoNotInline
        static void setIsClippedToScreen(final PopupWindow popupWindow, final boolean b) {
            oO00OOO.\u3007080(popupWindow, b);
        }
    }
    
    private class ListSelectorHider implements Runnable
    {
        final ListPopupWindow this$0;
        
        ListSelectorHider(final ListPopupWindow this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public void run() {
            this.this$0.clearListSelection();
        }
    }
    
    private class PopupDataSetObserver extends DataSetObserver
    {
        final ListPopupWindow this$0;
        
        PopupDataSetObserver(final ListPopupWindow this$0) {
            this.this$0 = this$0;
        }
        
        public void onChanged() {
            if (this.this$0.isShowing()) {
                this.this$0.show();
            }
        }
        
        public void onInvalidated() {
            this.this$0.dismiss();
        }
    }
    
    private class PopupScrollListener implements AbsListView$OnScrollListener
    {
        final ListPopupWindow this$0;
        
        PopupScrollListener(final ListPopupWindow this$0) {
            this.this$0 = this$0;
        }
        
        public void onScroll(final AbsListView absListView, final int n, final int n2, final int n3) {
        }
        
        public void onScrollStateChanged(final AbsListView absListView, final int n) {
            if (n == 1 && !this.this$0.isInputMethodNotNeeded() && this.this$0.mPopup.getContentView() != null) {
                final ListPopupWindow this$0 = this.this$0;
                this$0.mHandler.removeCallbacks((Runnable)this$0.mResizePopupRunnable);
                this.this$0.mResizePopupRunnable.run();
            }
        }
    }
    
    private class PopupTouchInterceptor implements View$OnTouchListener
    {
        final ListPopupWindow this$0;
        
        PopupTouchInterceptor(final ListPopupWindow this$0) {
            this.this$0 = this$0;
        }
        
        public boolean onTouch(final View view, final MotionEvent motionEvent) {
            final int action = motionEvent.getAction();
            final int n = (int)motionEvent.getX();
            final int n2 = (int)motionEvent.getY();
            if (action == 0) {
                final PopupWindow mPopup = this.this$0.mPopup;
                if (mPopup != null && mPopup.isShowing() && n >= 0 && n < this.this$0.mPopup.getWidth() && n2 >= 0 && n2 < this.this$0.mPopup.getHeight()) {
                    final ListPopupWindow this$0 = this.this$0;
                    this$0.mHandler.postDelayed((Runnable)this$0.mResizePopupRunnable, 250L);
                    return false;
                }
            }
            if (action == 1) {
                final ListPopupWindow this$2 = this.this$0;
                this$2.mHandler.removeCallbacks((Runnable)this$2.mResizePopupRunnable);
            }
            return false;
        }
    }
    
    private class ResizePopupRunnable implements Runnable
    {
        final ListPopupWindow this$0;
        
        ResizePopupRunnable(final ListPopupWindow this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public void run() {
            final DropDownListView mDropDownList = this.this$0.mDropDownList;
            if (mDropDownList != null && ViewCompat.isAttachedToWindow((View)mDropDownList) && ((AdapterView)this.this$0.mDropDownList).getCount() > ((ViewGroup)this.this$0.mDropDownList).getChildCount()) {
                final int childCount = ((ViewGroup)this.this$0.mDropDownList).getChildCount();
                final ListPopupWindow this$0 = this.this$0;
                if (childCount <= this$0.mListItemExpandMaximum) {
                    this$0.mPopup.setInputMethodMode(2);
                    this.this$0.show();
                }
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.view.View;
import androidx.annotation.DoNotInline;
import android.view.textclassifier.TextClassificationManager;
import androidx.annotation.RequiresApi;
import androidx.core.util.Preconditions;
import androidx.annotation.NonNull;
import android.widget.TextView;
import androidx.annotation.Nullable;
import android.view.textclassifier.TextClassifier;

final class AppCompatTextClassifierHelper
{
    @Nullable
    private TextClassifier mTextClassifier;
    @NonNull
    private TextView mTextView;
    
    AppCompatTextClassifierHelper(@NonNull final TextView textView) {
        this.mTextView = Preconditions.checkNotNull(textView);
    }
    
    @NonNull
    @RequiresApi(api = 26)
    public TextClassifier getTextClassifier() {
        TextClassifier textClassifier;
        if ((textClassifier = this.mTextClassifier) == null) {
            textClassifier = Api26Impl.getTextClassifier(this.mTextView);
        }
        return textClassifier;
    }
    
    @RequiresApi(api = 26)
    public void setTextClassifier(@Nullable final TextClassifier mTextClassifier) {
        this.mTextClassifier = mTextClassifier;
    }
    
    @RequiresApi(26)
    private static final class Api26Impl
    {
        @DoNotInline
        @NonNull
        static TextClassifier getTextClassifier(@NonNull final TextView textView) {
            final TextClassificationManager textClassificationManager = (TextClassificationManager)oo88o8O.\u3007080(((View)textView).getContext(), (Class)TextClassificationManager.class);
            if (textClassificationManager != null) {
                return \u3007oo\u3007.\u3007080(textClassificationManager);
            }
            return o\u3007O8\u3007\u3007o.\u3007080();
        }
    }
}

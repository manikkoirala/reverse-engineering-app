// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import androidx.appcompat.app.\u3007o\u3007;
import android.os.LocaleList;
import java.util.Locale;
import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import androidx.core.view.inputmethod.EditorInfoCompat;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.annotation.SuppressLint;
import androidx.core.widget.TextViewCompat;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import androidx.core.view.ViewCompat;
import android.util.AttributeSet;
import android.graphics.PorterDuff$Mode;
import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
import android.content.res.Resources$NotFoundException;
import androidx.core.content.res.ResourcesCompat;
import java.lang.ref.WeakReference;
import android.os.Build$VERSION;
import androidx.appcompat.R;
import android.content.res.ColorStateList;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.TextView;
import android.graphics.Typeface;
import androidx.annotation.NonNull;

class AppCompatTextHelper
{
    private static final int MONOSPACE = 3;
    private static final int SANS = 1;
    private static final int SERIF = 2;
    private static final int TEXT_FONT_WEIGHT_UNSPECIFIED = -1;
    private boolean mAsyncFontPending;
    @NonNull
    private final AppCompatTextViewAutoSizeHelper mAutoSizeTextHelper;
    private TintInfo mDrawableBottomTint;
    private TintInfo mDrawableEndTint;
    private TintInfo mDrawableLeftTint;
    private TintInfo mDrawableRightTint;
    private TintInfo mDrawableStartTint;
    private TintInfo mDrawableTint;
    private TintInfo mDrawableTopTint;
    private Typeface mFontTypeface;
    private int mFontWeight;
    private int mStyle;
    @NonNull
    private final TextView mView;
    
    AppCompatTextHelper(@NonNull final TextView mView) {
        this.mStyle = 0;
        this.mFontWeight = -1;
        this.mView = mView;
        this.mAutoSizeTextHelper = new AppCompatTextViewAutoSizeHelper(mView);
    }
    
    private void applyCompoundDrawableTint(final Drawable drawable, final TintInfo tintInfo) {
        if (drawable != null && tintInfo != null) {
            AppCompatDrawableManager.tintDrawable(drawable, tintInfo, ((View)this.mView).getDrawableState());
        }
    }
    
    private static TintInfo createTintInfo(final Context context, final AppCompatDrawableManager appCompatDrawableManager, final int n) {
        final ColorStateList tintList = appCompatDrawableManager.getTintList(context, n);
        if (tintList != null) {
            final TintInfo tintInfo = new TintInfo();
            tintInfo.mHasTintList = true;
            tintInfo.mTintList = tintList;
            return tintInfo;
        }
        return null;
    }
    
    private void setCompoundDrawables(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4, Drawable drawable5, Drawable drawable6) {
        if (drawable5 == null && drawable6 == null) {
            if (drawable != null || drawable2 != null || drawable3 != null || drawable4 != null) {
                final Drawable[] compoundDrawablesRelative = Api17Impl.getCompoundDrawablesRelative(this.mView);
                drawable5 = compoundDrawablesRelative[0];
                if (drawable5 != null || compoundDrawablesRelative[2] != null) {
                    final TextView mView = this.mView;
                    if (drawable2 == null) {
                        drawable2 = compoundDrawablesRelative[1];
                    }
                    drawable3 = compoundDrawablesRelative[2];
                    if (drawable4 == null) {
                        drawable4 = compoundDrawablesRelative[3];
                    }
                    Api17Impl.setCompoundDrawablesRelativeWithIntrinsicBounds(mView, drawable5, drawable2, drawable3, drawable4);
                    return;
                }
                final Drawable[] compoundDrawables = this.mView.getCompoundDrawables();
                final TextView mView2 = this.mView;
                if (drawable == null) {
                    drawable = compoundDrawables[0];
                }
                if (drawable2 == null) {
                    drawable2 = compoundDrawables[1];
                }
                if (drawable3 == null) {
                    drawable3 = compoundDrawables[2];
                }
                if (drawable4 == null) {
                    drawable4 = compoundDrawables[3];
                }
                mView2.setCompoundDrawablesWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
            }
        }
        else {
            final Drawable[] compoundDrawablesRelative2 = Api17Impl.getCompoundDrawablesRelative(this.mView);
            final TextView mView3 = this.mView;
            if (drawable5 == null) {
                drawable5 = compoundDrawablesRelative2[0];
            }
            if (drawable2 == null) {
                drawable2 = compoundDrawablesRelative2[1];
            }
            if (drawable6 == null) {
                drawable6 = compoundDrawablesRelative2[2];
            }
            if (drawable4 == null) {
                drawable4 = compoundDrawablesRelative2[3];
            }
            Api17Impl.setCompoundDrawablesRelativeWithIntrinsicBounds(mView3, drawable5, drawable2, drawable6, drawable4);
        }
    }
    
    private void setCompoundTints() {
        final TintInfo mDrawableTint = this.mDrawableTint;
        this.mDrawableLeftTint = mDrawableTint;
        this.mDrawableTopTint = mDrawableTint;
        this.mDrawableRightTint = mDrawableTint;
        this.mDrawableBottomTint = mDrawableTint;
        this.mDrawableStartTint = mDrawableTint;
        this.mDrawableEndTint = mDrawableTint;
    }
    
    private void setTextSizeInternal(final int n, final float n2) {
        this.mAutoSizeTextHelper.setTextSizeInternal(n, n2);
    }
    
    private void updateTypefaceAndStyle(final Context context, final TintTypedArray tintTypedArray) {
        this.mStyle = tintTypedArray.getInt(R.styleable.TextAppearance_android_textStyle, this.mStyle);
        final int sdk_INT = Build$VERSION.SDK_INT;
        final boolean b = false;
        if (sdk_INT >= 28 && (this.mFontWeight = tintTypedArray.getInt(R.styleable.TextAppearance_android_textFontWeight, -1)) != -1) {
            this.mStyle = ((this.mStyle & 0x2) | 0x0);
        }
        int textAppearance_android_fontFamily = R.styleable.TextAppearance_android_fontFamily;
        if (!tintTypedArray.hasValue(textAppearance_android_fontFamily) && !tintTypedArray.hasValue(R.styleable.TextAppearance_fontFamily)) {
            final int textAppearance_android_typeface = R.styleable.TextAppearance_android_typeface;
            if (tintTypedArray.hasValue(textAppearance_android_typeface)) {
                this.mAsyncFontPending = false;
                final int int1 = tintTypedArray.getInt(textAppearance_android_typeface, 1);
                if (int1 != 1) {
                    if (int1 != 2) {
                        if (int1 == 3) {
                            this.mFontTypeface = Typeface.MONOSPACE;
                        }
                    }
                    else {
                        this.mFontTypeface = Typeface.SERIF;
                    }
                }
                else {
                    this.mFontTypeface = Typeface.SANS_SERIF;
                }
            }
            return;
        }
        this.mFontTypeface = null;
        final int textAppearance_fontFamily = R.styleable.TextAppearance_fontFamily;
        if (tintTypedArray.hasValue(textAppearance_fontFamily)) {
            textAppearance_android_fontFamily = textAppearance_fontFamily;
        }
        final int mFontWeight = this.mFontWeight;
        final int mStyle = this.mStyle;
        if (!context.isRestricted()) {
            final ResourcesCompat.FontCallback fontCallback = new ResourcesCompat.FontCallback(this, mFontWeight, mStyle, new WeakReference((T)this.mView)) {
                final AppCompatTextHelper this$0;
                final int val$fontWeight;
                final int val$style;
                final WeakReference val$textViewWeak;
                
                @Override
                public void onFontRetrievalFailed(final int n) {
                }
                
                @Override
                public void onFontRetrieved(@NonNull final Typeface typeface) {
                    Typeface create = typeface;
                    if (Build$VERSION.SDK_INT >= 28) {
                        final int val$fontWeight = this.val$fontWeight;
                        create = typeface;
                        if (val$fontWeight != -1) {
                            create = Api28Impl.create(typeface, val$fontWeight, (this.val$style & 0x2) != 0x0);
                        }
                    }
                    this.this$0.onAsyncTypefaceReceived(this.val$textViewWeak, create);
                }
            };
            try {
                final Typeface font = tintTypedArray.getFont(textAppearance_android_fontFamily, this.mStyle, fontCallback);
                if (font != null) {
                    if (sdk_INT >= 28 && this.mFontWeight != -1) {
                        this.mFontTypeface = Api28Impl.create(Typeface.create(font, 0), this.mFontWeight, (this.mStyle & 0x2) != 0x0);
                    }
                    else {
                        this.mFontTypeface = font;
                    }
                }
                this.mAsyncFontPending = (this.mFontTypeface == null);
            }
            catch (final UnsupportedOperationException | Resources$NotFoundException ex) {}
        }
        if (this.mFontTypeface == null) {
            final String string = tintTypedArray.getString(textAppearance_android_fontFamily);
            if (string != null) {
                if (Build$VERSION.SDK_INT >= 28 && this.mFontWeight != -1) {
                    final Typeface create = Typeface.create(string, 0);
                    final int mFontWeight2 = this.mFontWeight;
                    boolean b2 = b;
                    if ((this.mStyle & 0x2) != 0x0) {
                        b2 = true;
                    }
                    this.mFontTypeface = Api28Impl.create(create, mFontWeight2, b2);
                }
                else {
                    this.mFontTypeface = Typeface.create(string, this.mStyle);
                }
            }
        }
    }
    
    void applyCompoundDrawablesTints() {
        if (this.mDrawableLeftTint != null || this.mDrawableTopTint != null || this.mDrawableRightTint != null || this.mDrawableBottomTint != null) {
            final Drawable[] compoundDrawables = this.mView.getCompoundDrawables();
            this.applyCompoundDrawableTint(compoundDrawables[0], this.mDrawableLeftTint);
            this.applyCompoundDrawableTint(compoundDrawables[1], this.mDrawableTopTint);
            this.applyCompoundDrawableTint(compoundDrawables[2], this.mDrawableRightTint);
            this.applyCompoundDrawableTint(compoundDrawables[3], this.mDrawableBottomTint);
        }
        if (this.mDrawableStartTint != null || this.mDrawableEndTint != null) {
            final Drawable[] compoundDrawablesRelative = Api17Impl.getCompoundDrawablesRelative(this.mView);
            this.applyCompoundDrawableTint(compoundDrawablesRelative[0], this.mDrawableStartTint);
            this.applyCompoundDrawableTint(compoundDrawablesRelative[2], this.mDrawableEndTint);
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    void autoSizeText() {
        this.mAutoSizeTextHelper.autoSizeText();
    }
    
    int getAutoSizeMaxTextSize() {
        return this.mAutoSizeTextHelper.getAutoSizeMaxTextSize();
    }
    
    int getAutoSizeMinTextSize() {
        return this.mAutoSizeTextHelper.getAutoSizeMinTextSize();
    }
    
    int getAutoSizeStepGranularity() {
        return this.mAutoSizeTextHelper.getAutoSizeStepGranularity();
    }
    
    int[] getAutoSizeTextAvailableSizes() {
        return this.mAutoSizeTextHelper.getAutoSizeTextAvailableSizes();
    }
    
    int getAutoSizeTextType() {
        return this.mAutoSizeTextHelper.getAutoSizeTextType();
    }
    
    @Nullable
    ColorStateList getCompoundDrawableTintList() {
        final TintInfo mDrawableTint = this.mDrawableTint;
        ColorStateList mTintList;
        if (mDrawableTint != null) {
            mTintList = mDrawableTint.mTintList;
        }
        else {
            mTintList = null;
        }
        return mTintList;
    }
    
    @Nullable
    PorterDuff$Mode getCompoundDrawableTintMode() {
        final TintInfo mDrawableTint = this.mDrawableTint;
        PorterDuff$Mode mTintMode;
        if (mDrawableTint != null) {
            mTintMode = mDrawableTint.mTintMode;
        }
        else {
            mTintMode = null;
        }
        return mTintMode;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    boolean isAutoSizeEnabled() {
        return this.mAutoSizeTextHelper.isAutoSizeEnabled();
    }
    
    @SuppressLint({ "NewApi" })
    void loadFromAttributes(@Nullable final AttributeSet set, int n) {
        final Context context = ((View)this.mView).getContext();
        final AppCompatDrawableManager value = AppCompatDrawableManager.get();
        final int[] appCompatTextHelper = R.styleable.AppCompatTextHelper;
        final TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(context, set, appCompatTextHelper, n, 0);
        final TextView mView = this.mView;
        ViewCompat.saveAttributeDataForStyleable((View)mView, ((View)mView).getContext(), appCompatTextHelper, set, obtainStyledAttributes.getWrappedTypeArray(), n, 0);
        final int resourceId = obtainStyledAttributes.getResourceId(R.styleable.AppCompatTextHelper_android_textAppearance, -1);
        final int appCompatTextHelper_android_drawableLeft = R.styleable.AppCompatTextHelper_android_drawableLeft;
        if (obtainStyledAttributes.hasValue(appCompatTextHelper_android_drawableLeft)) {
            this.mDrawableLeftTint = createTintInfo(context, value, obtainStyledAttributes.getResourceId(appCompatTextHelper_android_drawableLeft, 0));
        }
        final int appCompatTextHelper_android_drawableTop = R.styleable.AppCompatTextHelper_android_drawableTop;
        if (obtainStyledAttributes.hasValue(appCompatTextHelper_android_drawableTop)) {
            this.mDrawableTopTint = createTintInfo(context, value, obtainStyledAttributes.getResourceId(appCompatTextHelper_android_drawableTop, 0));
        }
        final int appCompatTextHelper_android_drawableRight = R.styleable.AppCompatTextHelper_android_drawableRight;
        if (obtainStyledAttributes.hasValue(appCompatTextHelper_android_drawableRight)) {
            this.mDrawableRightTint = createTintInfo(context, value, obtainStyledAttributes.getResourceId(appCompatTextHelper_android_drawableRight, 0));
        }
        final int appCompatTextHelper_android_drawableBottom = R.styleable.AppCompatTextHelper_android_drawableBottom;
        if (obtainStyledAttributes.hasValue(appCompatTextHelper_android_drawableBottom)) {
            this.mDrawableBottomTint = createTintInfo(context, value, obtainStyledAttributes.getResourceId(appCompatTextHelper_android_drawableBottom, 0));
        }
        final int sdk_INT = Build$VERSION.SDK_INT;
        final int appCompatTextHelper_android_drawableStart = R.styleable.AppCompatTextHelper_android_drawableStart;
        if (obtainStyledAttributes.hasValue(appCompatTextHelper_android_drawableStart)) {
            this.mDrawableStartTint = createTintInfo(context, value, obtainStyledAttributes.getResourceId(appCompatTextHelper_android_drawableStart, 0));
        }
        final int appCompatTextHelper_android_drawableEnd = R.styleable.AppCompatTextHelper_android_drawableEnd;
        if (obtainStyledAttributes.hasValue(appCompatTextHelper_android_drawableEnd)) {
            this.mDrawableEndTint = createTintInfo(context, value, obtainStyledAttributes.getResourceId(appCompatTextHelper_android_drawableEnd, 0));
        }
        obtainStyledAttributes.recycle();
        final boolean b = this.mView.getTransformationMethod() instanceof PasswordTransformationMethod;
        boolean allCaps;
        boolean b2;
        ColorStateList colorStateList3;
        ColorStateList colorStateList4;
        String string2;
        ColorStateList colorStateList5;
        String string3;
        if (resourceId != -1) {
            final TintTypedArray obtainStyledAttributes2 = TintTypedArray.obtainStyledAttributes(context, resourceId, R.styleable.TextAppearance);
            Label_0346: {
                if (!b) {
                    final int textAppearance_textAllCaps = R.styleable.TextAppearance_textAllCaps;
                    if (obtainStyledAttributes2.hasValue(textAppearance_textAllCaps)) {
                        allCaps = obtainStyledAttributes2.getBoolean(textAppearance_textAllCaps, false);
                        b2 = true;
                        break Label_0346;
                    }
                }
                allCaps = false;
                b2 = false;
            }
            this.updateTypefaceAndStyle(context, obtainStyledAttributes2);
            ColorStateList list = null;
            Label_0477: {
                ColorStateList list2;
                if (sdk_INT < 23) {
                    final int textAppearance_android_textColor = R.styleable.TextAppearance_android_textColor;
                    ColorStateList colorStateList;
                    if (obtainStyledAttributes2.hasValue(textAppearance_android_textColor)) {
                        colorStateList = obtainStyledAttributes2.getColorStateList(textAppearance_android_textColor);
                    }
                    else {
                        colorStateList = null;
                    }
                    final int textAppearance_android_textColorHint = R.styleable.TextAppearance_android_textColorHint;
                    ColorStateList colorStateList2;
                    if (obtainStyledAttributes2.hasValue(textAppearance_android_textColorHint)) {
                        colorStateList2 = obtainStyledAttributes2.getColorStateList(textAppearance_android_textColorHint);
                    }
                    else {
                        colorStateList2 = null;
                    }
                    final int textAppearance_android_textColorLink = R.styleable.TextAppearance_android_textColorLink;
                    list = colorStateList;
                    list2 = colorStateList2;
                    if (obtainStyledAttributes2.hasValue(textAppearance_android_textColorLink)) {
                        colorStateList3 = obtainStyledAttributes2.getColorStateList(textAppearance_android_textColorLink);
                        list = colorStateList;
                        colorStateList4 = colorStateList2;
                        break Label_0477;
                    }
                }
                else {
                    list = null;
                    list2 = null;
                }
                colorStateList3 = null;
                colorStateList4 = list2;
            }
            final int textAppearance_textLocale = R.styleable.TextAppearance_textLocale;
            String string;
            if (obtainStyledAttributes2.hasValue(textAppearance_textLocale)) {
                string = obtainStyledAttributes2.getString(textAppearance_textLocale);
            }
            else {
                string = null;
            }
            Label_0544: {
                if (sdk_INT >= 26) {
                    final int textAppearance_fontVariationSettings = R.styleable.TextAppearance_fontVariationSettings;
                    if (obtainStyledAttributes2.hasValue(textAppearance_fontVariationSettings)) {
                        string2 = obtainStyledAttributes2.getString(textAppearance_fontVariationSettings);
                        break Label_0544;
                    }
                }
                string2 = null;
            }
            obtainStyledAttributes2.recycle();
            colorStateList5 = list;
            string3 = string;
        }
        else {
            string2 = null;
            colorStateList5 = null;
            string3 = null;
            allCaps = false;
            colorStateList4 = null;
            colorStateList3 = null;
            b2 = false;
        }
        final TintTypedArray obtainStyledAttributes3 = TintTypedArray.obtainStyledAttributes(context, set, R.styleable.TextAppearance, n, 0);
        if (!b) {
            final int textAppearance_textAllCaps2 = R.styleable.TextAppearance_textAllCaps;
            if (obtainStyledAttributes3.hasValue(textAppearance_textAllCaps2)) {
                allCaps = obtainStyledAttributes3.getBoolean(textAppearance_textAllCaps2, false);
                b2 = true;
            }
        }
        ColorStateList textColor = colorStateList5;
        ColorStateList hintTextColor = colorStateList4;
        ColorStateList colorStateList6 = colorStateList3;
        if (sdk_INT < 23) {
            final int textAppearance_android_textColor2 = R.styleable.TextAppearance_android_textColor;
            if (obtainStyledAttributes3.hasValue(textAppearance_android_textColor2)) {
                colorStateList5 = obtainStyledAttributes3.getColorStateList(textAppearance_android_textColor2);
            }
            final int textAppearance_android_textColorHint2 = R.styleable.TextAppearance_android_textColorHint;
            if (obtainStyledAttributes3.hasValue(textAppearance_android_textColorHint2)) {
                colorStateList4 = obtainStyledAttributes3.getColorStateList(textAppearance_android_textColorHint2);
            }
            final int textAppearance_android_textColorLink2 = R.styleable.TextAppearance_android_textColorLink;
            textColor = colorStateList5;
            hintTextColor = colorStateList4;
            colorStateList6 = colorStateList3;
            if (obtainStyledAttributes3.hasValue(textAppearance_android_textColorLink2)) {
                colorStateList6 = obtainStyledAttributes3.getColorStateList(textAppearance_android_textColorLink2);
                hintTextColor = colorStateList4;
                textColor = colorStateList5;
            }
        }
        final int textAppearance_textLocale2 = R.styleable.TextAppearance_textLocale;
        if (obtainStyledAttributes3.hasValue(textAppearance_textLocale2)) {
            string3 = obtainStyledAttributes3.getString(textAppearance_textLocale2);
        }
        String string4 = null;
        Label_0801: {
            if (sdk_INT >= 26) {
                final int textAppearance_fontVariationSettings2 = R.styleable.TextAppearance_fontVariationSettings;
                if (obtainStyledAttributes3.hasValue(textAppearance_fontVariationSettings2)) {
                    string4 = obtainStyledAttributes3.getString(textAppearance_fontVariationSettings2);
                    break Label_0801;
                }
            }
            string4 = string2;
        }
        if (sdk_INT >= 28) {
            final int textAppearance_android_textSize = R.styleable.TextAppearance_android_textSize;
            if (obtainStyledAttributes3.hasValue(textAppearance_android_textSize) && obtainStyledAttributes3.getDimensionPixelSize(textAppearance_android_textSize, -1) == 0) {
                this.mView.setTextSize(0, 0.0f);
            }
        }
        this.updateTypefaceAndStyle(context, obtainStyledAttributes3);
        obtainStyledAttributes3.recycle();
        if (textColor != null) {
            this.mView.setTextColor(textColor);
        }
        if (hintTextColor != null) {
            this.mView.setHintTextColor(hintTextColor);
        }
        if (colorStateList6 != null) {
            this.mView.setLinkTextColor(colorStateList6);
        }
        if (!b && b2) {
            this.setAllCaps(allCaps);
        }
        final Typeface mFontTypeface = this.mFontTypeface;
        if (mFontTypeface != null) {
            if (this.mFontWeight == -1) {
                this.mView.setTypeface(mFontTypeface, this.mStyle);
            }
            else {
                this.mView.setTypeface(mFontTypeface);
            }
        }
        if (string4 != null) {
            Api26Impl.setFontVariationSettings(this.mView, string4);
        }
        if (string3 != null) {
            if (sdk_INT >= 24) {
                Api24Impl.setTextLocales(this.mView, Api24Impl.forLanguageTags(string3));
            }
            else {
                Api17Impl.setTextLocale(this.mView, Api21Impl.forLanguageTag(string3.split(",")[0]));
            }
        }
        this.mAutoSizeTextHelper.loadFromAttributes(set, n);
        if (ViewUtils.SDK_LEVEL_SUPPORTS_AUTOSIZE && this.mAutoSizeTextHelper.getAutoSizeTextType() != 0) {
            final int[] autoSizeTextAvailableSizes = this.mAutoSizeTextHelper.getAutoSizeTextAvailableSizes();
            if (autoSizeTextAvailableSizes.length > 0) {
                if (Api26Impl.getAutoSizeStepGranularity(this.mView) != -1.0f) {
                    Api26Impl.setAutoSizeTextTypeUniformWithConfiguration(this.mView, this.mAutoSizeTextHelper.getAutoSizeMinTextSize(), this.mAutoSizeTextHelper.getAutoSizeMaxTextSize(), this.mAutoSizeTextHelper.getAutoSizeStepGranularity(), 0);
                }
                else {
                    Api26Impl.setAutoSizeTextTypeUniformWithPresetSizes(this.mView, autoSizeTextAvailableSizes, 0);
                }
            }
        }
        final TintTypedArray obtainStyledAttributes4 = TintTypedArray.obtainStyledAttributes(context, set, R.styleable.AppCompatTextView);
        n = obtainStyledAttributes4.getResourceId(R.styleable.AppCompatTextView_drawableLeftCompat, -1);
        Drawable drawable;
        if (n != -1) {
            drawable = value.getDrawable(context, n);
        }
        else {
            drawable = null;
        }
        n = obtainStyledAttributes4.getResourceId(R.styleable.AppCompatTextView_drawableTopCompat, -1);
        Drawable drawable2;
        if (n != -1) {
            drawable2 = value.getDrawable(context, n);
        }
        else {
            drawable2 = null;
        }
        n = obtainStyledAttributes4.getResourceId(R.styleable.AppCompatTextView_drawableRightCompat, -1);
        Drawable drawable3;
        if (n != -1) {
            drawable3 = value.getDrawable(context, n);
        }
        else {
            drawable3 = null;
        }
        n = obtainStyledAttributes4.getResourceId(R.styleable.AppCompatTextView_drawableBottomCompat, -1);
        Drawable drawable4;
        if (n != -1) {
            drawable4 = value.getDrawable(context, n);
        }
        else {
            drawable4 = null;
        }
        n = obtainStyledAttributes4.getResourceId(R.styleable.AppCompatTextView_drawableStartCompat, -1);
        Drawable drawable5;
        if (n != -1) {
            drawable5 = value.getDrawable(context, n);
        }
        else {
            drawable5 = null;
        }
        n = obtainStyledAttributes4.getResourceId(R.styleable.AppCompatTextView_drawableEndCompat, -1);
        Drawable drawable6;
        if (n != -1) {
            drawable6 = value.getDrawable(context, n);
        }
        else {
            drawable6 = null;
        }
        this.setCompoundDrawables(drawable, drawable2, drawable3, drawable4, drawable5, drawable6);
        n = R.styleable.AppCompatTextView_drawableTint;
        if (obtainStyledAttributes4.hasValue(n)) {
            TextViewCompat.setCompoundDrawableTintList(this.mView, obtainStyledAttributes4.getColorStateList(n));
        }
        n = R.styleable.AppCompatTextView_drawableTintMode;
        if (obtainStyledAttributes4.hasValue(n)) {
            TextViewCompat.setCompoundDrawableTintMode(this.mView, DrawableUtils.parseTintMode(obtainStyledAttributes4.getInt(n, -1), null));
        }
        final int dimensionPixelSize = obtainStyledAttributes4.getDimensionPixelSize(R.styleable.AppCompatTextView_firstBaselineToTopHeight, -1);
        n = obtainStyledAttributes4.getDimensionPixelSize(R.styleable.AppCompatTextView_lastBaselineToBottomHeight, -1);
        final int dimensionPixelSize2 = obtainStyledAttributes4.getDimensionPixelSize(R.styleable.AppCompatTextView_lineHeight, -1);
        obtainStyledAttributes4.recycle();
        if (dimensionPixelSize != -1) {
            TextViewCompat.setFirstBaselineToTopHeight(this.mView, dimensionPixelSize);
        }
        if (n != -1) {
            TextViewCompat.setLastBaselineToBottomHeight(this.mView, n);
        }
        if (dimensionPixelSize2 != -1) {
            TextViewCompat.setLineHeight(this.mView, dimensionPixelSize2);
        }
    }
    
    void onAsyncTypefaceReceived(final WeakReference<TextView> weakReference, final Typeface mFontTypeface) {
        if (this.mAsyncFontPending) {
            this.mFontTypeface = mFontTypeface;
            final TextView textView = weakReference.get();
            if (textView != null) {
                if (ViewCompat.isAttachedToWindow((View)textView)) {
                    ((View)textView).post((Runnable)new Runnable(this, textView, mFontTypeface, this.mStyle) {
                        final AppCompatTextHelper this$0;
                        final int val$style;
                        final TextView val$textView;
                        final Typeface val$typeface;
                        
                        @Override
                        public void run() {
                            this.val$textView.setTypeface(this.val$typeface, this.val$style);
                        }
                    });
                }
                else {
                    textView.setTypeface(mFontTypeface, this.mStyle);
                }
            }
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        if (!ViewUtils.SDK_LEVEL_SUPPORTS_AUTOSIZE) {
            this.autoSizeText();
        }
    }
    
    void onSetCompoundDrawables() {
        this.applyCompoundDrawablesTints();
    }
    
    void onSetTextAppearance(final Context context, int n) {
        final TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(context, n, R.styleable.TextAppearance);
        n = R.styleable.TextAppearance_textAllCaps;
        if (obtainStyledAttributes.hasValue(n)) {
            this.setAllCaps(obtainStyledAttributes.getBoolean(n, false));
        }
        n = Build$VERSION.SDK_INT;
        if (n < 23) {
            final int textAppearance_android_textColor = R.styleable.TextAppearance_android_textColor;
            if (obtainStyledAttributes.hasValue(textAppearance_android_textColor)) {
                final ColorStateList colorStateList = obtainStyledAttributes.getColorStateList(textAppearance_android_textColor);
                if (colorStateList != null) {
                    this.mView.setTextColor(colorStateList);
                }
            }
            final int textAppearance_android_textColorLink = R.styleable.TextAppearance_android_textColorLink;
            if (obtainStyledAttributes.hasValue(textAppearance_android_textColorLink)) {
                final ColorStateList colorStateList2 = obtainStyledAttributes.getColorStateList(textAppearance_android_textColorLink);
                if (colorStateList2 != null) {
                    this.mView.setLinkTextColor(colorStateList2);
                }
            }
            final int textAppearance_android_textColorHint = R.styleable.TextAppearance_android_textColorHint;
            if (obtainStyledAttributes.hasValue(textAppearance_android_textColorHint)) {
                final ColorStateList colorStateList3 = obtainStyledAttributes.getColorStateList(textAppearance_android_textColorHint);
                if (colorStateList3 != null) {
                    this.mView.setHintTextColor(colorStateList3);
                }
            }
        }
        final int textAppearance_android_textSize = R.styleable.TextAppearance_android_textSize;
        if (obtainStyledAttributes.hasValue(textAppearance_android_textSize) && obtainStyledAttributes.getDimensionPixelSize(textAppearance_android_textSize, -1) == 0) {
            this.mView.setTextSize(0, 0.0f);
        }
        this.updateTypefaceAndStyle(context, obtainStyledAttributes);
        if (n >= 26) {
            n = R.styleable.TextAppearance_fontVariationSettings;
            if (obtainStyledAttributes.hasValue(n)) {
                final String string = obtainStyledAttributes.getString(n);
                if (string != null) {
                    Api26Impl.setFontVariationSettings(this.mView, string);
                }
            }
        }
        obtainStyledAttributes.recycle();
        final Typeface mFontTypeface = this.mFontTypeface;
        if (mFontTypeface != null) {
            this.mView.setTypeface(mFontTypeface, this.mStyle);
        }
    }
    
    void populateSurroundingTextIfNeeded(@NonNull final TextView textView, @Nullable final InputConnection inputConnection, @NonNull final EditorInfo editorInfo) {
        if (Build$VERSION.SDK_INT < 30 && inputConnection != null) {
            EditorInfoCompat.setInitialSurroundingText(editorInfo, textView.getText());
        }
    }
    
    void setAllCaps(final boolean allCaps) {
        this.mView.setAllCaps(allCaps);
    }
    
    void setAutoSizeTextTypeUniformWithConfiguration(final int n, final int n2, final int n3, final int n4) throws IllegalArgumentException {
        this.mAutoSizeTextHelper.setAutoSizeTextTypeUniformWithConfiguration(n, n2, n3, n4);
    }
    
    void setAutoSizeTextTypeUniformWithPresetSizes(@NonNull final int[] array, final int n) throws IllegalArgumentException {
        this.mAutoSizeTextHelper.setAutoSizeTextTypeUniformWithPresetSizes(array, n);
    }
    
    void setAutoSizeTextTypeWithDefaults(final int autoSizeTextTypeWithDefaults) {
        this.mAutoSizeTextHelper.setAutoSizeTextTypeWithDefaults(autoSizeTextTypeWithDefaults);
    }
    
    void setCompoundDrawableTintList(@Nullable final ColorStateList mTintList) {
        if (this.mDrawableTint == null) {
            this.mDrawableTint = new TintInfo();
        }
        final TintInfo mDrawableTint = this.mDrawableTint;
        mDrawableTint.mTintList = mTintList;
        mDrawableTint.mHasTintList = (mTintList != null);
        this.setCompoundTints();
    }
    
    void setCompoundDrawableTintMode(@Nullable final PorterDuff$Mode mTintMode) {
        if (this.mDrawableTint == null) {
            this.mDrawableTint = new TintInfo();
        }
        final TintInfo mDrawableTint = this.mDrawableTint;
        mDrawableTint.mTintMode = mTintMode;
        mDrawableTint.mHasTintMode = (mTintMode != null);
        this.setCompoundTints();
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    void setTextSize(final int n, final float n2) {
        if (!ViewUtils.SDK_LEVEL_SUPPORTS_AUTOSIZE && !this.isAutoSizeEnabled()) {
            this.setTextSizeInternal(n, n2);
        }
    }
    
    @RequiresApi(17)
    static class Api17Impl
    {
        private Api17Impl() {
        }
        
        @DoNotInline
        static Drawable[] getCompoundDrawablesRelative(final TextView textView) {
            return textView.getCompoundDrawablesRelative();
        }
        
        @DoNotInline
        static void setCompoundDrawablesRelativeWithIntrinsicBounds(final TextView textView, final Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
        }
        
        @DoNotInline
        static void setTextLocale(final TextView textView, final Locale textLocale) {
            textView.setTextLocale(textLocale);
        }
    }
    
    @RequiresApi(21)
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        @DoNotInline
        static Locale forLanguageTag(final String languageTag) {
            return Locale.forLanguageTag(languageTag);
        }
    }
    
    @RequiresApi(24)
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        @DoNotInline
        static LocaleList forLanguageTags(final String s) {
            return \u3007o\u3007.\u3007080(s);
        }
        
        @DoNotInline
        static void setTextLocales(final TextView textView, final LocaleList list) {
            \u300700.\u3007080(textView, list);
        }
    }
    
    @RequiresApi(26)
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        @DoNotInline
        static int getAutoSizeStepGranularity(final TextView textView) {
            return \u3007oOO8O8.\u3007080(textView);
        }
        
        @DoNotInline
        static void setAutoSizeTextTypeUniformWithConfiguration(final TextView textView, final int n, final int n2, final int n3, final int n4) {
            O8ooOoo\u3007.\u3007080(textView, n, n2, n3, n4);
        }
        
        @DoNotInline
        static void setAutoSizeTextTypeUniformWithPresetSizes(final TextView textView, final int[] array, final int n) {
            \u30070000OOO.\u3007080(textView, array, n);
        }
        
        @DoNotInline
        static boolean setFontVariationSettings(final TextView textView, final String s) {
            return O\u30078O8\u3007008.\u3007080(textView, s);
        }
    }
    
    @RequiresApi(28)
    static class Api28Impl
    {
        private Api28Impl() {
        }
        
        @DoNotInline
        static Typeface create(final Typeface typeface, final int n, final boolean b) {
            return o\u3007\u30070\u3007.\u3007080(typeface, n, b);
        }
    }
}

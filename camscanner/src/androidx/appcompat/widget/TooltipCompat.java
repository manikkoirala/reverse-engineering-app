// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.os.Build$VERSION;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.view.View;

public class TooltipCompat
{
    private TooltipCompat() {
    }
    
    public static void setTooltipText(@NonNull final View view, @Nullable final CharSequence charSequence) {
        if (Build$VERSION.SDK_INT >= 26) {
            Api26Impl.setTooltipText(view, charSequence);
        }
        else {
            TooltipCompatHandler.setTooltipText(view, charSequence);
        }
    }
    
    @RequiresApi(26)
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        @DoNotInline
        static void setTooltipText(final View view, final CharSequence charSequence) {
            OO8oO0o\u3007.\u3007080(view, charSequence);
        }
    }
}

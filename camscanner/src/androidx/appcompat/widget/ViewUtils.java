// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import java.lang.reflect.InvocationTargetException;
import androidx.core.view.ViewCompat;
import android.graphics.Rect;
import android.view.View;
import android.os.Build$VERSION;
import java.lang.reflect.Method;
import androidx.annotation.ChecksSdkIntAtLeast;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public class ViewUtils
{
    @ChecksSdkIntAtLeast(api = 27)
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    static final boolean SDK_LEVEL_SUPPORTS_AUTOSIZE;
    private static final String TAG = "ViewUtils";
    private static Method sComputeFitSystemWindowsMethod;
    
    static {
        SDK_LEVEL_SUPPORTS_AUTOSIZE = (Build$VERSION.SDK_INT >= 27);
        try {
            if (!(ViewUtils.sComputeFitSystemWindowsMethod = View.class.getDeclaredMethod("computeFitSystemWindows", Rect.class, Rect.class)).isAccessible()) {
                ViewUtils.sComputeFitSystemWindowsMethod.setAccessible(true);
            }
        }
        catch (final NoSuchMethodException ex) {}
    }
    
    private ViewUtils() {
    }
    
    public static void computeFitSystemWindows(final View obj, final Rect rect, final Rect rect2) {
        final Method sComputeFitSystemWindowsMethod = ViewUtils.sComputeFitSystemWindowsMethod;
        if (sComputeFitSystemWindowsMethod == null) {
            return;
        }
        try {
            sComputeFitSystemWindowsMethod.invoke(obj, rect, rect2);
        }
        catch (final Exception ex) {}
    }
    
    public static boolean isLayoutRtl(final View view) {
        final int layoutDirection = ViewCompat.getLayoutDirection(view);
        boolean b = true;
        if (layoutDirection != 1) {
            b = false;
        }
        return b;
    }
    
    public static void makeOptionalFitsSystemWindows(final View obj) {
        try {
            final Method method = obj.getClass().getMethod("makeOptionalFitsSystemWindows", (Class<?>[])new Class[0]);
            if (!method.isAccessible()) {
                method.setAccessible(true);
            }
            method.invoke(obj, new Object[0]);
        }
        catch (final NoSuchMethodException | InvocationTargetException | IllegalAccessException ex) {}
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.content.Context;
import androidx.appcompat.content.res.AppCompatResources;
import android.view.View;
import androidx.core.view.ViewCompat;
import androidx.appcompat.R;
import android.util.AttributeSet;
import android.graphics.drawable.RippleDrawable;
import android.os.Build$VERSION;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import androidx.core.widget.ImageViewCompat;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import android.widget.ImageView;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public class AppCompatImageHelper
{
    private TintInfo mImageTint;
    private TintInfo mInternalImageTint;
    private int mLevel;
    private TintInfo mTmpInfo;
    @NonNull
    private final ImageView mView;
    
    public AppCompatImageHelper(@NonNull final ImageView mView) {
        this.mLevel = 0;
        this.mView = mView;
    }
    
    private boolean applyFrameworkTintUsingColorFilter(@NonNull final Drawable drawable) {
        if (this.mTmpInfo == null) {
            this.mTmpInfo = new TintInfo();
        }
        final TintInfo mTmpInfo = this.mTmpInfo;
        mTmpInfo.clear();
        final ColorStateList imageTintList = ImageViewCompat.getImageTintList(this.mView);
        if (imageTintList != null) {
            mTmpInfo.mHasTintList = true;
            mTmpInfo.mTintList = imageTintList;
        }
        final PorterDuff$Mode imageTintMode = ImageViewCompat.getImageTintMode(this.mView);
        if (imageTintMode != null) {
            mTmpInfo.mHasTintMode = true;
            mTmpInfo.mTintMode = imageTintMode;
        }
        if (!mTmpInfo.mHasTintList && !mTmpInfo.mHasTintMode) {
            return false;
        }
        AppCompatDrawableManager.tintDrawable(drawable, mTmpInfo, ((View)this.mView).getDrawableState());
        return true;
    }
    
    private boolean shouldApplyFrameworkTintUsingColorFilter() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        boolean b = true;
        if (sdk_INT > 21) {
            if (this.mInternalImageTint == null) {
                b = false;
            }
            return b;
        }
        return sdk_INT == 21;
    }
    
    void applyImageLevel() {
        if (this.mView.getDrawable() != null) {
            this.mView.getDrawable().setLevel(this.mLevel);
        }
    }
    
    void applySupportImageTint() {
        final Drawable drawable = this.mView.getDrawable();
        if (drawable != null) {
            DrawableUtils.fixDrawable(drawable);
        }
        if (drawable != null) {
            if (this.shouldApplyFrameworkTintUsingColorFilter() && this.applyFrameworkTintUsingColorFilter(drawable)) {
                return;
            }
            final TintInfo mImageTint = this.mImageTint;
            if (mImageTint != null) {
                AppCompatDrawableManager.tintDrawable(drawable, mImageTint, ((View)this.mView).getDrawableState());
            }
            else {
                final TintInfo mInternalImageTint = this.mInternalImageTint;
                if (mInternalImageTint != null) {
                    AppCompatDrawableManager.tintDrawable(drawable, mInternalImageTint, ((View)this.mView).getDrawableState());
                }
            }
        }
    }
    
    ColorStateList getSupportImageTintList() {
        final TintInfo mImageTint = this.mImageTint;
        ColorStateList mTintList;
        if (mImageTint != null) {
            mTintList = mImageTint.mTintList;
        }
        else {
            mTintList = null;
        }
        return mTintList;
    }
    
    PorterDuff$Mode getSupportImageTintMode() {
        final TintInfo mImageTint = this.mImageTint;
        PorterDuff$Mode mTintMode;
        if (mImageTint != null) {
            mTintMode = mImageTint.mTintMode;
        }
        else {
            mTintMode = null;
        }
        return mTintMode;
    }
    
    boolean hasOverlappingRendering() {
        return !(((View)this.mView).getBackground() instanceof RippleDrawable);
    }
    
    public void loadFromAttributes(final AttributeSet set, int n) {
        final Context context = ((View)this.mView).getContext();
        final int[] appCompatImageView = R.styleable.AppCompatImageView;
        final TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(context, set, appCompatImageView, n, 0);
        final ImageView mView = this.mView;
        ViewCompat.saveAttributeDataForStyleable((View)mView, ((View)mView).getContext(), appCompatImageView, set, obtainStyledAttributes.getWrappedTypeArray(), n, 0);
        try {
            Drawable drawable2;
            final Drawable drawable = drawable2 = this.mView.getDrawable();
            if (drawable == null) {
                n = obtainStyledAttributes.getResourceId(R.styleable.AppCompatImageView_srcCompat, -1);
                drawable2 = drawable;
                if (n != -1) {
                    final Drawable drawable3 = AppCompatResources.getDrawable(((View)this.mView).getContext(), n);
                    if ((drawable2 = drawable3) != null) {
                        this.mView.setImageDrawable(drawable3);
                        drawable2 = drawable3;
                    }
                }
            }
            if (drawable2 != null) {
                DrawableUtils.fixDrawable(drawable2);
            }
            n = R.styleable.AppCompatImageView_tint;
            if (obtainStyledAttributes.hasValue(n)) {
                ImageViewCompat.setImageTintList(this.mView, obtainStyledAttributes.getColorStateList(n));
            }
            n = R.styleable.AppCompatImageView_tintMode;
            if (obtainStyledAttributes.hasValue(n)) {
                ImageViewCompat.setImageTintMode(this.mView, DrawableUtils.parseTintMode(obtainStyledAttributes.getInt(n, -1), null));
            }
        }
        finally {
            obtainStyledAttributes.recycle();
        }
    }
    
    void obtainLevelFromDrawable(@NonNull final Drawable drawable) {
        this.mLevel = drawable.getLevel();
    }
    
    public void setImageResource(final int n) {
        if (n != 0) {
            final Drawable drawable = AppCompatResources.getDrawable(((View)this.mView).getContext(), n);
            if (drawable != null) {
                DrawableUtils.fixDrawable(drawable);
            }
            this.mView.setImageDrawable(drawable);
        }
        else {
            this.mView.setImageDrawable((Drawable)null);
        }
        this.applySupportImageTint();
    }
    
    void setInternalImageTint(final ColorStateList mTintList) {
        if (mTintList != null) {
            if (this.mInternalImageTint == null) {
                this.mInternalImageTint = new TintInfo();
            }
            final TintInfo mInternalImageTint = this.mInternalImageTint;
            mInternalImageTint.mTintList = mTintList;
            mInternalImageTint.mHasTintList = true;
        }
        else {
            this.mInternalImageTint = null;
        }
        this.applySupportImageTint();
    }
    
    void setSupportImageTintList(final ColorStateList mTintList) {
        if (this.mImageTint == null) {
            this.mImageTint = new TintInfo();
        }
        final TintInfo mImageTint = this.mImageTint;
        mImageTint.mTintList = mTintList;
        mImageTint.mHasTintList = true;
        this.applySupportImageTint();
    }
    
    void setSupportImageTintMode(final PorterDuff$Mode mTintMode) {
        if (this.mImageTint == null) {
            this.mImageTint = new TintInfo();
        }
        final TintInfo mImageTint = this.mImageTint;
        mImageTint.mTintMode = mTintMode;
        mImageTint.mHasTintMode = true;
        this.applySupportImageTint();
    }
}

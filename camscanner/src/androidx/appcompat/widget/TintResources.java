// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.content.res.Resources$NotFoundException;
import android.graphics.drawable.Drawable;
import android.content.res.Resources;
import androidx.annotation.NonNull;
import android.content.Context;
import java.lang.ref.WeakReference;

class TintResources extends ResourcesWrapper
{
    private final WeakReference<Context> mContextRef;
    
    public TintResources(@NonNull final Context referent, @NonNull final Resources resources) {
        super(resources);
        this.mContextRef = new WeakReference<Context>(referent);
    }
    
    @Override
    public Drawable getDrawable(final int n) throws Resources$NotFoundException {
        final Drawable drawableCanonical = this.getDrawableCanonical(n);
        final Context context = this.mContextRef.get();
        if (drawableCanonical != null && context != null) {
            ResourceManagerInternal.get().tintDrawableUsingColorFilter(context, n, drawableCanonical);
        }
        return drawableCanonical;
    }
}

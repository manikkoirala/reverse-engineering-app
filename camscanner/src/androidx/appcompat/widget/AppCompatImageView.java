// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.net.Uri;
import android.graphics.Bitmap;
import androidx.annotation.DrawableRes;
import android.graphics.drawable.Drawable;
import android.graphics.PorterDuff$Mode;
import androidx.annotation.RestrictTo;
import android.content.res.ColorStateList;
import android.view.View;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.core.widget.TintableImageSourceView;
import androidx.core.view.TintableBackgroundView;
import android.widget.ImageView;

public class AppCompatImageView extends ImageView implements TintableBackgroundView, TintableImageSourceView
{
    private final AppCompatBackgroundHelper mBackgroundTintHelper;
    private boolean mHasLevel;
    private final AppCompatImageHelper mImageHelper;
    
    public AppCompatImageView(@NonNull final Context context) {
        this(context, null);
    }
    
    public AppCompatImageView(@NonNull final Context context, @Nullable final AttributeSet set) {
        this(context, set, 0);
    }
    
    public AppCompatImageView(@NonNull final Context context, @Nullable final AttributeSet set, final int n) {
        super(TintContextWrapper.wrap(context), set, n);
        this.mHasLevel = false;
        ThemeUtils.checkAppCompatTheme((View)this, ((View)this).getContext());
        (this.mBackgroundTintHelper = new AppCompatBackgroundHelper((View)this)).loadFromAttributes(set, n);
        (this.mImageHelper = new AppCompatImageHelper(this)).loadFromAttributes(set, n);
    }
    
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.applySupportBackgroundTint();
        }
        final AppCompatImageHelper mImageHelper = this.mImageHelper;
        if (mImageHelper != null) {
            mImageHelper.applySupportImageTint();
        }
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public ColorStateList getSupportBackgroundTintList() {
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        ColorStateList supportBackgroundTintList;
        if (mBackgroundTintHelper != null) {
            supportBackgroundTintList = mBackgroundTintHelper.getSupportBackgroundTintList();
        }
        else {
            supportBackgroundTintList = null;
        }
        return supportBackgroundTintList;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public PorterDuff$Mode getSupportBackgroundTintMode() {
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        PorterDuff$Mode supportBackgroundTintMode;
        if (mBackgroundTintHelper != null) {
            supportBackgroundTintMode = mBackgroundTintHelper.getSupportBackgroundTintMode();
        }
        else {
            supportBackgroundTintMode = null;
        }
        return supportBackgroundTintMode;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public ColorStateList getSupportImageTintList() {
        final AppCompatImageHelper mImageHelper = this.mImageHelper;
        ColorStateList supportImageTintList;
        if (mImageHelper != null) {
            supportImageTintList = mImageHelper.getSupportImageTintList();
        }
        else {
            supportImageTintList = null;
        }
        return supportImageTintList;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public PorterDuff$Mode getSupportImageTintMode() {
        final AppCompatImageHelper mImageHelper = this.mImageHelper;
        PorterDuff$Mode supportImageTintMode;
        if (mImageHelper != null) {
            supportImageTintMode = mImageHelper.getSupportImageTintMode();
        }
        else {
            supportImageTintMode = null;
        }
        return supportImageTintMode;
    }
    
    public boolean hasOverlappingRendering() {
        return this.mImageHelper.hasOverlappingRendering() && super.hasOverlappingRendering();
    }
    
    public void setBackgroundDrawable(@Nullable final Drawable backgroundDrawable) {
        super.setBackgroundDrawable(backgroundDrawable);
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.onSetBackgroundDrawable(backgroundDrawable);
        }
    }
    
    public void setBackgroundResource(@DrawableRes final int backgroundResource) {
        super.setBackgroundResource(backgroundResource);
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.onSetBackgroundResource(backgroundResource);
        }
    }
    
    public void setImageBitmap(final Bitmap imageBitmap) {
        super.setImageBitmap(imageBitmap);
        final AppCompatImageHelper mImageHelper = this.mImageHelper;
        if (mImageHelper != null) {
            mImageHelper.applySupportImageTint();
        }
    }
    
    public void setImageDrawable(@Nullable final Drawable imageDrawable) {
        final AppCompatImageHelper mImageHelper = this.mImageHelper;
        if (mImageHelper != null && imageDrawable != null && !this.mHasLevel) {
            mImageHelper.obtainLevelFromDrawable(imageDrawable);
        }
        super.setImageDrawable(imageDrawable);
        final AppCompatImageHelper mImageHelper2 = this.mImageHelper;
        if (mImageHelper2 != null) {
            mImageHelper2.applySupportImageTint();
            if (!this.mHasLevel) {
                this.mImageHelper.applyImageLevel();
            }
        }
    }
    
    public void setImageLevel(final int imageLevel) {
        super.setImageLevel(imageLevel);
        this.mHasLevel = true;
    }
    
    public void setImageResource(@DrawableRes final int imageResource) {
        final AppCompatImageHelper mImageHelper = this.mImageHelper;
        if (mImageHelper != null) {
            mImageHelper.setImageResource(imageResource);
        }
    }
    
    public void setImageURI(@Nullable final Uri imageURI) {
        super.setImageURI(imageURI);
        final AppCompatImageHelper mImageHelper = this.mImageHelper;
        if (mImageHelper != null) {
            mImageHelper.applySupportImageTint();
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void setSupportBackgroundTintList(@Nullable final ColorStateList supportBackgroundTintList) {
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.setSupportBackgroundTintList(supportBackgroundTintList);
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void setSupportBackgroundTintMode(@Nullable final PorterDuff$Mode supportBackgroundTintMode) {
        final AppCompatBackgroundHelper mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.setSupportBackgroundTintMode(supportBackgroundTintMode);
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void setSupportImageTintList(@Nullable final ColorStateList supportImageTintList) {
        final AppCompatImageHelper mImageHelper = this.mImageHelper;
        if (mImageHelper != null) {
            mImageHelper.setSupportImageTintList(supportImageTintList);
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void setSupportImageTintMode(@Nullable final PorterDuff$Mode supportImageTintMode) {
        final AppCompatImageHelper mImageHelper = this.mImageHelper;
        if (mImageHelper != null) {
            mImageHelper.setSupportImageTintMode(supportImageTintMode);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.graphics.ColorFilter;
import androidx.annotation.RequiresApi;
import androidx.annotation.NonNull;
import android.graphics.Outline;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

class ActionBarBackgroundDrawable extends Drawable
{
    final ActionBarContainer mContainer;
    
    public ActionBarBackgroundDrawable(final ActionBarContainer mContainer) {
        this.mContainer = mContainer;
    }
    
    public void draw(final Canvas canvas) {
        final ActionBarContainer mContainer = this.mContainer;
        if (mContainer.mIsSplit) {
            final Drawable mSplitBackground = mContainer.mSplitBackground;
            if (mSplitBackground != null) {
                mSplitBackground.draw(canvas);
            }
        }
        else {
            final Drawable mBackground = mContainer.mBackground;
            if (mBackground != null) {
                mBackground.draw(canvas);
            }
            final ActionBarContainer mContainer2 = this.mContainer;
            final Drawable mStackedBackground = mContainer2.mStackedBackground;
            if (mStackedBackground != null && mContainer2.mIsStacked) {
                mStackedBackground.draw(canvas);
            }
        }
    }
    
    public int getOpacity() {
        return 0;
    }
    
    @RequiresApi(21)
    public void getOutline(@NonNull final Outline outline) {
        final ActionBarContainer mContainer = this.mContainer;
        if (mContainer.mIsSplit) {
            if (mContainer.mSplitBackground != null) {
                Api21Impl.getOutline(mContainer.mBackground, outline);
            }
        }
        else {
            final Drawable mBackground = mContainer.mBackground;
            if (mBackground != null) {
                Api21Impl.getOutline(mBackground, outline);
            }
        }
    }
    
    public void setAlpha(final int n) {
    }
    
    public void setColorFilter(final ColorFilter colorFilter) {
    }
    
    @RequiresApi(21)
    private static class Api21Impl
    {
        public static void getOutline(final Drawable drawable, final Outline outline) {
            drawable.getOutline(outline);
        }
    }
}

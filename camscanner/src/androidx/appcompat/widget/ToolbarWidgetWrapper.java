// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.widget;

import android.widget.AdapterView;
import androidx.core.view.ViewPropertyAnimatorListener;
import androidx.core.view.ViewPropertyAnimatorListenerAdapter;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPresenter;
import androidx.appcompat.content.res.AppCompatResources;
import android.view.ViewParent;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.SpinnerAdapter;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.Menu;
import android.content.Context;
import androidx.core.view.ViewPropertyAnimatorCompat;
import androidx.core.view.ViewCompat;
import android.view.ViewGroup$LayoutParams;
import android.view.MenuItem;
import androidx.appcompat.view.menu.ActionMenuItem;
import android.view.View$OnClickListener;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.text.TextUtils;
import android.util.AttributeSet;
import androidx.appcompat.R;
import android.view.Window$Callback;
import android.widget.Spinner;
import android.graphics.drawable.Drawable;
import android.view.View;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public class ToolbarWidgetWrapper implements DecorToolbar
{
    private static final int AFFECTS_LOGO_MASK = 3;
    private static final long DEFAULT_FADE_DURATION_MS = 200L;
    private static final String TAG = "ToolbarWidgetWrapper";
    private ActionMenuPresenter mActionMenuPresenter;
    private View mCustomView;
    private int mDefaultNavigationContentDescription;
    private Drawable mDefaultNavigationIcon;
    private int mDisplayOpts;
    private CharSequence mHomeDescription;
    private Drawable mIcon;
    private Drawable mLogo;
    boolean mMenuPrepared;
    private Drawable mNavIcon;
    private int mNavigationMode;
    private Spinner mSpinner;
    private CharSequence mSubtitle;
    private View mTabView;
    CharSequence mTitle;
    private boolean mTitleSet;
    Toolbar mToolbar;
    Window$Callback mWindowCallback;
    
    public ToolbarWidgetWrapper(final Toolbar toolbar, final boolean b) {
        this(toolbar, b, R.string.abc_action_bar_up_description, R.drawable.abc_ic_ab_back_material);
    }
    
    public ToolbarWidgetWrapper(final Toolbar mToolbar, final boolean b, final int defaultNavigationContentDescription, int popupTheme) {
        this.mNavigationMode = 0;
        this.mDefaultNavigationContentDescription = 0;
        this.mToolbar = mToolbar;
        this.mTitle = mToolbar.getTitle();
        this.mSubtitle = mToolbar.getSubtitle();
        this.mTitleSet = (this.mTitle != null);
        this.mNavIcon = mToolbar.getNavigationIcon();
        final TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(((View)mToolbar).getContext(), null, R.styleable.ActionBar, R.attr.actionBarStyle, 0);
        this.mDefaultNavigationIcon = obtainStyledAttributes.getDrawable(R.styleable.ActionBar_homeAsUpIndicator);
        if (b) {
            final CharSequence text = obtainStyledAttributes.getText(R.styleable.ActionBar_title);
            if (!TextUtils.isEmpty(text)) {
                this.setTitle(text);
            }
            final CharSequence text2 = obtainStyledAttributes.getText(R.styleable.ActionBar_subtitle);
            if (!TextUtils.isEmpty(text2)) {
                this.setSubtitle(text2);
            }
            final Drawable drawable = obtainStyledAttributes.getDrawable(R.styleable.ActionBar_logo);
            if (drawable != null) {
                this.setLogo(drawable);
            }
            final Drawable drawable2 = obtainStyledAttributes.getDrawable(R.styleable.ActionBar_icon);
            if (drawable2 != null) {
                this.setIcon(drawable2);
            }
            if (this.mNavIcon == null) {
                final Drawable mDefaultNavigationIcon = this.mDefaultNavigationIcon;
                if (mDefaultNavigationIcon != null) {
                    this.setNavigationIcon(mDefaultNavigationIcon);
                }
            }
            this.setDisplayOptions(obtainStyledAttributes.getInt(R.styleable.ActionBar_displayOptions, 0));
            popupTheme = obtainStyledAttributes.getResourceId(R.styleable.ActionBar_customNavigationLayout, 0);
            if (popupTheme != 0) {
                this.setCustomView(LayoutInflater.from(((View)this.mToolbar).getContext()).inflate(popupTheme, (ViewGroup)this.mToolbar, false));
                this.setDisplayOptions(this.mDisplayOpts | 0x10);
            }
            popupTheme = obtainStyledAttributes.getLayoutDimension(R.styleable.ActionBar_height, 0);
            if (popupTheme > 0) {
                final ViewGroup$LayoutParams layoutParams = ((View)this.mToolbar).getLayoutParams();
                layoutParams.height = popupTheme;
                ((View)this.mToolbar).setLayoutParams(layoutParams);
            }
            final int dimensionPixelOffset = obtainStyledAttributes.getDimensionPixelOffset(R.styleable.ActionBar_contentInsetStart, -1);
            popupTheme = obtainStyledAttributes.getDimensionPixelOffset(R.styleable.ActionBar_contentInsetEnd, -1);
            if (dimensionPixelOffset >= 0 || popupTheme >= 0) {
                this.mToolbar.setContentInsetsRelative(Math.max(dimensionPixelOffset, 0), Math.max(popupTheme, 0));
            }
            popupTheme = obtainStyledAttributes.getResourceId(R.styleable.ActionBar_titleTextStyle, 0);
            if (popupTheme != 0) {
                final Toolbar mToolbar2 = this.mToolbar;
                mToolbar2.setTitleTextAppearance(((View)mToolbar2).getContext(), popupTheme);
            }
            popupTheme = obtainStyledAttributes.getResourceId(R.styleable.ActionBar_subtitleTextStyle, 0);
            if (popupTheme != 0) {
                final Toolbar mToolbar3 = this.mToolbar;
                mToolbar3.setSubtitleTextAppearance(((View)mToolbar3).getContext(), popupTheme);
            }
            popupTheme = obtainStyledAttributes.getResourceId(R.styleable.ActionBar_popupTheme, 0);
            if (popupTheme != 0) {
                this.mToolbar.setPopupTheme(popupTheme);
            }
        }
        else {
            this.mDisplayOpts = this.detectDisplayOptions();
        }
        obtainStyledAttributes.recycle();
        this.setDefaultNavigationContentDescription(defaultNavigationContentDescription);
        this.mHomeDescription = this.mToolbar.getNavigationContentDescription();
        this.mToolbar.setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            final ActionMenuItem mNavItem = new ActionMenuItem(((View)this$0.mToolbar).getContext(), 0, 16908332, 0, 0, this$0.mTitle);
            final ToolbarWidgetWrapper this$0;
            
            public void onClick(final View view) {
                final ToolbarWidgetWrapper this$0 = this.this$0;
                final Window$Callback mWindowCallback = this$0.mWindowCallback;
                if (mWindowCallback != null && this$0.mMenuPrepared) {
                    mWindowCallback.onMenuItemSelected(0, (MenuItem)this.mNavItem);
                }
            }
        });
    }
    
    private int detectDisplayOptions() {
        int n;
        if (this.mToolbar.getNavigationIcon() != null) {
            this.mDefaultNavigationIcon = this.mToolbar.getNavigationIcon();
            n = 15;
        }
        else {
            n = 11;
        }
        return n;
    }
    
    private void ensureSpinner() {
        if (this.mSpinner == null) {
            ((View)(this.mSpinner = new AppCompatSpinner(this.getContext(), null, R.attr.actionDropDownStyle))).setLayoutParams((ViewGroup$LayoutParams)new Toolbar.LayoutParams(-2, -2, 8388627));
        }
    }
    
    private void setTitleInt(final CharSequence charSequence) {
        this.mTitle = charSequence;
        if ((this.mDisplayOpts & 0x8) != 0x0) {
            this.mToolbar.setTitle(charSequence);
            if (this.mTitleSet) {
                ViewCompat.setAccessibilityPaneTitle(((View)this.mToolbar).getRootView(), charSequence);
            }
        }
    }
    
    private void updateHomeAccessibility() {
        if ((this.mDisplayOpts & 0x4) != 0x0) {
            if (TextUtils.isEmpty(this.mHomeDescription)) {
                this.mToolbar.setNavigationContentDescription(this.mDefaultNavigationContentDescription);
            }
            else {
                this.mToolbar.setNavigationContentDescription(this.mHomeDescription);
            }
        }
    }
    
    private void updateNavigationIcon() {
        if ((this.mDisplayOpts & 0x4) != 0x0) {
            final Toolbar mToolbar = this.mToolbar;
            Drawable navigationIcon = this.mNavIcon;
            if (navigationIcon == null) {
                navigationIcon = this.mDefaultNavigationIcon;
            }
            mToolbar.setNavigationIcon(navigationIcon);
        }
        else {
            this.mToolbar.setNavigationIcon(null);
        }
    }
    
    private void updateToolbarLogo() {
        final int mDisplayOpts = this.mDisplayOpts;
        Drawable logo;
        if ((mDisplayOpts & 0x2) != 0x0) {
            if ((mDisplayOpts & 0x1) != 0x0) {
                logo = this.mLogo;
                if (logo == null) {
                    logo = this.mIcon;
                }
            }
            else {
                logo = this.mIcon;
            }
        }
        else {
            logo = null;
        }
        this.mToolbar.setLogo(logo);
    }
    
    @Override
    public void animateToVisibility(final int n) {
        final ViewPropertyAnimatorCompat setupAnimatorToVisibility = this.setupAnimatorToVisibility(n, 200L);
        if (setupAnimatorToVisibility != null) {
            setupAnimatorToVisibility.start();
        }
    }
    
    @Override
    public boolean canShowOverflowMenu() {
        return this.mToolbar.canShowOverflowMenu();
    }
    
    @Override
    public void collapseActionView() {
        this.mToolbar.collapseActionView();
    }
    
    @Override
    public void dismissPopupMenus() {
        this.mToolbar.dismissPopupMenus();
    }
    
    @Override
    public Context getContext() {
        return ((View)this.mToolbar).getContext();
    }
    
    @Override
    public View getCustomView() {
        return this.mCustomView;
    }
    
    @Override
    public int getDisplayOptions() {
        return this.mDisplayOpts;
    }
    
    @Override
    public int getDropdownItemCount() {
        final Spinner mSpinner = this.mSpinner;
        int count;
        if (mSpinner != null) {
            count = ((AdapterView)mSpinner).getCount();
        }
        else {
            count = 0;
        }
        return count;
    }
    
    @Override
    public int getDropdownSelectedPosition() {
        final Spinner mSpinner = this.mSpinner;
        int selectedItemPosition;
        if (mSpinner != null) {
            selectedItemPosition = ((AdapterView)mSpinner).getSelectedItemPosition();
        }
        else {
            selectedItemPosition = 0;
        }
        return selectedItemPosition;
    }
    
    @Override
    public int getHeight() {
        return ((View)this.mToolbar).getHeight();
    }
    
    @Override
    public Menu getMenu() {
        return this.mToolbar.getMenu();
    }
    
    @Override
    public int getNavigationMode() {
        return this.mNavigationMode;
    }
    
    @Override
    public CharSequence getSubtitle() {
        return this.mToolbar.getSubtitle();
    }
    
    @Override
    public CharSequence getTitle() {
        return this.mToolbar.getTitle();
    }
    
    @Override
    public ViewGroup getViewGroup() {
        return this.mToolbar;
    }
    
    @Override
    public int getVisibility() {
        return ((View)this.mToolbar).getVisibility();
    }
    
    @Override
    public boolean hasEmbeddedTabs() {
        return this.mTabView != null;
    }
    
    @Override
    public boolean hasExpandedActionView() {
        return this.mToolbar.hasExpandedActionView();
    }
    
    @Override
    public boolean hasIcon() {
        return this.mIcon != null;
    }
    
    @Override
    public boolean hasLogo() {
        return this.mLogo != null;
    }
    
    @Override
    public boolean hideOverflowMenu() {
        return this.mToolbar.hideOverflowMenu();
    }
    
    @Override
    public void initIndeterminateProgress() {
    }
    
    @Override
    public void initProgress() {
    }
    
    @Override
    public boolean isOverflowMenuShowPending() {
        return this.mToolbar.isOverflowMenuShowPending();
    }
    
    @Override
    public boolean isOverflowMenuShowing() {
        return this.mToolbar.isOverflowMenuShowing();
    }
    
    @Override
    public boolean isTitleTruncated() {
        return this.mToolbar.isTitleTruncated();
    }
    
    @Override
    public void restoreHierarchyState(final SparseArray<Parcelable> sparseArray) {
        ((View)this.mToolbar).restoreHierarchyState((SparseArray)sparseArray);
    }
    
    @Override
    public void saveHierarchyState(final SparseArray<Parcelable> sparseArray) {
        ((View)this.mToolbar).saveHierarchyState((SparseArray)sparseArray);
    }
    
    @Override
    public void setBackgroundDrawable(final Drawable drawable) {
        ViewCompat.setBackground((View)this.mToolbar, drawable);
    }
    
    @Override
    public void setCollapsible(final boolean collapsible) {
        this.mToolbar.setCollapsible(collapsible);
    }
    
    @Override
    public void setCustomView(final View mCustomView) {
        final View mCustomView2 = this.mCustomView;
        if (mCustomView2 != null && (this.mDisplayOpts & 0x10) != 0x0) {
            this.mToolbar.removeView(mCustomView2);
        }
        if ((this.mCustomView = mCustomView) != null && (this.mDisplayOpts & 0x10) != 0x0) {
            this.mToolbar.addView(mCustomView);
        }
    }
    
    @Override
    public void setDefaultNavigationContentDescription(final int mDefaultNavigationContentDescription) {
        if (mDefaultNavigationContentDescription == this.mDefaultNavigationContentDescription) {
            return;
        }
        this.mDefaultNavigationContentDescription = mDefaultNavigationContentDescription;
        if (TextUtils.isEmpty(this.mToolbar.getNavigationContentDescription())) {
            this.setNavigationContentDescription(this.mDefaultNavigationContentDescription);
        }
    }
    
    @Override
    public void setDefaultNavigationIcon(final Drawable mDefaultNavigationIcon) {
        if (this.mDefaultNavigationIcon != mDefaultNavigationIcon) {
            this.mDefaultNavigationIcon = mDefaultNavigationIcon;
            this.updateNavigationIcon();
        }
    }
    
    @Override
    public void setDisplayOptions(final int mDisplayOpts) {
        final int n = this.mDisplayOpts ^ mDisplayOpts;
        this.mDisplayOpts = mDisplayOpts;
        if (n != 0) {
            if ((n & 0x4) != 0x0) {
                if ((mDisplayOpts & 0x4) != 0x0) {
                    this.updateHomeAccessibility();
                }
                this.updateNavigationIcon();
            }
            if ((n & 0x3) != 0x0) {
                this.updateToolbarLogo();
            }
            if ((n & 0x8) != 0x0) {
                if ((mDisplayOpts & 0x8) != 0x0) {
                    this.mToolbar.setTitle(this.mTitle);
                    this.mToolbar.setSubtitle(this.mSubtitle);
                }
                else {
                    this.mToolbar.setTitle(null);
                    this.mToolbar.setSubtitle(null);
                }
            }
            if ((n & 0x10) != 0x0) {
                final View mCustomView = this.mCustomView;
                if (mCustomView != null) {
                    if ((mDisplayOpts & 0x10) != 0x0) {
                        this.mToolbar.addView(mCustomView);
                    }
                    else {
                        this.mToolbar.removeView(mCustomView);
                    }
                }
            }
        }
    }
    
    @Override
    public void setDropdownParams(final SpinnerAdapter adapter, final AdapterView$OnItemSelectedListener onItemSelectedListener) {
        this.ensureSpinner();
        this.mSpinner.setAdapter(adapter);
        ((AdapterView)this.mSpinner).setOnItemSelectedListener(onItemSelectedListener);
    }
    
    @Override
    public void setDropdownSelectedPosition(final int selection) {
        final Spinner mSpinner = this.mSpinner;
        if (mSpinner != null) {
            ((AdapterView)mSpinner).setSelection(selection);
            return;
        }
        throw new IllegalStateException("Can't set dropdown selected position without an adapter");
    }
    
    @Override
    public void setEmbeddedTabView(final ScrollingTabContainerView mTabView) {
        final View mTabView2 = this.mTabView;
        if (mTabView2 != null) {
            final ViewParent parent = mTabView2.getParent();
            final Toolbar mToolbar = this.mToolbar;
            if (parent == mToolbar) {
                mToolbar.removeView(this.mTabView);
            }
        }
        if ((this.mTabView = (View)mTabView) != null && this.mNavigationMode == 2) {
            this.mToolbar.addView((View)mTabView, 0);
            final Toolbar.LayoutParams layoutParams = (Toolbar.LayoutParams)this.mTabView.getLayoutParams();
            layoutParams.width = -2;
            layoutParams.height = -2;
            layoutParams.gravity = 8388691;
            mTabView.setAllowCollapse(true);
        }
    }
    
    @Override
    public void setHomeButtonEnabled(final boolean b) {
    }
    
    @Override
    public void setIcon(final int n) {
        Drawable drawable;
        if (n != 0) {
            drawable = AppCompatResources.getDrawable(this.getContext(), n);
        }
        else {
            drawable = null;
        }
        this.setIcon(drawable);
    }
    
    @Override
    public void setIcon(final Drawable mIcon) {
        this.mIcon = mIcon;
        this.updateToolbarLogo();
    }
    
    @Override
    public void setLogo(final int n) {
        Drawable drawable;
        if (n != 0) {
            drawable = AppCompatResources.getDrawable(this.getContext(), n);
        }
        else {
            drawable = null;
        }
        this.setLogo(drawable);
    }
    
    @Override
    public void setLogo(final Drawable mLogo) {
        this.mLogo = mLogo;
        this.updateToolbarLogo();
    }
    
    @Override
    public void setMenu(final Menu menu, final MenuPresenter.Callback callback) {
        if (this.mActionMenuPresenter == null) {
            (this.mActionMenuPresenter = new ActionMenuPresenter(((View)this.mToolbar).getContext())).setId(R.id.action_menu_presenter);
        }
        this.mActionMenuPresenter.setCallback(callback);
        this.mToolbar.setMenu((MenuBuilder)menu, this.mActionMenuPresenter);
    }
    
    @Override
    public void setMenuCallbacks(final MenuPresenter.Callback callback, final MenuBuilder.Callback callback2) {
        this.mToolbar.setMenuCallbacks(callback, callback2);
    }
    
    @Override
    public void setMenuPrepared() {
        this.mMenuPrepared = true;
    }
    
    @Override
    public void setNavigationContentDescription(final int n) {
        CharSequence string;
        if (n == 0) {
            string = null;
        }
        else {
            string = this.getContext().getString(n);
        }
        this.setNavigationContentDescription(string);
    }
    
    @Override
    public void setNavigationContentDescription(final CharSequence mHomeDescription) {
        this.mHomeDescription = mHomeDescription;
        this.updateHomeAccessibility();
    }
    
    @Override
    public void setNavigationIcon(final int n) {
        Drawable drawable;
        if (n != 0) {
            drawable = AppCompatResources.getDrawable(this.getContext(), n);
        }
        else {
            drawable = null;
        }
        this.setNavigationIcon(drawable);
    }
    
    @Override
    public void setNavigationIcon(final Drawable mNavIcon) {
        this.mNavIcon = mNavIcon;
        this.updateNavigationIcon();
    }
    
    @Override
    public void setNavigationMode(final int n) {
        final int mNavigationMode = this.mNavigationMode;
        if (n != mNavigationMode) {
            if (mNavigationMode != 1) {
                if (mNavigationMode == 2) {
                    final View mTabView = this.mTabView;
                    if (mTabView != null) {
                        final ViewParent parent = mTabView.getParent();
                        final Toolbar mToolbar = this.mToolbar;
                        if (parent == mToolbar) {
                            mToolbar.removeView(this.mTabView);
                        }
                    }
                }
            }
            else {
                final Spinner mSpinner = this.mSpinner;
                if (mSpinner != null) {
                    final ViewParent parent2 = ((View)mSpinner).getParent();
                    final Toolbar mToolbar2 = this.mToolbar;
                    if (parent2 == mToolbar2) {
                        mToolbar2.removeView((View)this.mSpinner);
                    }
                }
            }
            if ((this.mNavigationMode = n) != 0) {
                if (n != 1) {
                    if (n != 2) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Invalid navigation mode ");
                        sb.append(n);
                        throw new IllegalArgumentException(sb.toString());
                    }
                    final View mTabView2 = this.mTabView;
                    if (mTabView2 != null) {
                        this.mToolbar.addView(mTabView2, 0);
                        final Toolbar.LayoutParams layoutParams = (Toolbar.LayoutParams)this.mTabView.getLayoutParams();
                        layoutParams.width = -2;
                        layoutParams.height = -2;
                        layoutParams.gravity = 8388691;
                    }
                }
                else {
                    this.ensureSpinner();
                    this.mToolbar.addView((View)this.mSpinner, 0);
                }
            }
        }
    }
    
    @Override
    public void setSubtitle(final CharSequence charSequence) {
        this.mSubtitle = charSequence;
        if ((this.mDisplayOpts & 0x8) != 0x0) {
            this.mToolbar.setSubtitle(charSequence);
        }
    }
    
    @Override
    public void setTitle(final CharSequence titleInt) {
        this.mTitleSet = true;
        this.setTitleInt(titleInt);
    }
    
    @Override
    public void setVisibility(final int visibility) {
        ((View)this.mToolbar).setVisibility(visibility);
    }
    
    @Override
    public void setWindowCallback(final Window$Callback mWindowCallback) {
        this.mWindowCallback = mWindowCallback;
    }
    
    @Override
    public void setWindowTitle(final CharSequence titleInt) {
        if (!this.mTitleSet) {
            this.setTitleInt(titleInt);
        }
    }
    
    @Override
    public ViewPropertyAnimatorCompat setupAnimatorToVisibility(final int n, final long duration) {
        final ViewPropertyAnimatorCompat animate = ViewCompat.animate((View)this.mToolbar);
        float n2;
        if (n == 0) {
            n2 = 1.0f;
        }
        else {
            n2 = 0.0f;
        }
        return animate.alpha(n2).setDuration(duration).setListener(new ViewPropertyAnimatorListenerAdapter(this, n) {
            private boolean mCanceled = false;
            final ToolbarWidgetWrapper this$0;
            final int val$visibility;
            
            @Override
            public void onAnimationCancel(final View view) {
                this.mCanceled = true;
            }
            
            @Override
            public void onAnimationEnd(final View view) {
                if (!this.mCanceled) {
                    ((View)this.this$0.mToolbar).setVisibility(this.val$visibility);
                }
            }
            
            @Override
            public void onAnimationStart(final View view) {
                ((View)this.this$0.mToolbar).setVisibility(0);
            }
        });
    }
    
    @Override
    public boolean showOverflowMenu() {
        return this.mToolbar.showOverflowMenu();
    }
}

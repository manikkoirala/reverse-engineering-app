// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.app;

import android.widget.Adapter;
import android.os.BaseBundle;
import android.view.ViewManager;
import android.os.Parcel;
import android.os.Parcelable$ClassLoaderCreator;
import android.os.Parcelable$Creator;
import android.annotation.SuppressLint;
import android.os.Parcelable;
import androidx.appcompat.view.menu.MenuView;
import androidx.appcompat.view.menu.ListMenuPresenter;
import androidx.appcompat.content.res.AppCompatResources;
import android.view.MotionEvent;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import androidx.appcompat.view.SupportActionModeWrapper;
import android.view.ActionMode$Callback;
import android.view.KeyboardShortcutGroup;
import java.util.List;
import androidx.appcompat.view.WindowCallbackWrapper;
import java.util.Objects;
import android.os.LocaleList;
import android.os.PowerManager;
import androidx.annotation.DoNotInline;
import android.widget.FrameLayout$LayoutParams;
import android.view.ViewGroup$MarginLayoutParams;
import androidx.appcompat.view.StandaloneActionMode;
import androidx.appcompat.widget.ViewStubCompat;
import androidx.core.view.ViewPropertyAnimatorListener;
import androidx.core.view.ViewPropertyAnimatorListenerAdapter;
import androidx.core.widget.PopupWindowCompat;
import androidx.annotation.StyleRes;
import androidx.appcompat.widget.Toolbar;
import androidx.annotation.RequiresApi;
import java.util.Locale;
import android.view.MenuItem;
import androidx.core.app.NavUtils;
import android.app.UiModeManager;
import androidx.core.view.LayoutInflaterCompat;
import androidx.appcompat.view.SupportMenuInflater;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.IdRes;
import androidx.core.view.KeyEventDispatcher;
import androidx.appcompat.widget.VectorEnabledTintResources;
import org.xmlpull.v1.XmlPullParser;
import androidx.annotation.CallSuper;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.content.ContextCompat;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import android.content.ContextWrapper;
import android.util.AndroidRuntimeException;
import android.os.Bundle;
import android.view.KeyCharacterMap;
import android.view.ViewParent;
import android.view.WindowManager$LayoutParams;
import android.view.ViewGroup$LayoutParams;
import android.view.WindowManager;
import android.view.Menu;
import android.media.AudioManager;
import android.view.ViewConfiguration;
import android.view.KeyEvent;
import android.content.res.Resources$Theme;
import androidx.appcompat.view.menu.MenuPresenter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.ComponentName;
import androidx.core.util.ObjectsCompat;
import android.text.TextUtils;
import android.widget.FrameLayout;
import androidx.appcompat.widget.ViewUtils;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.core.view.OnApplyWindowInsetsListener;
import androidx.appcompat.view.ContextThemeWrapper;
import android.util.TypedValue;
import android.view.LayoutInflater;
import androidx.annotation.Nullable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import androidx.appcompat.widget.TintTypedArray;
import android.view.Window$Callback;
import androidx.annotation.NonNull;
import android.content.res.TypedArray;
import androidx.appcompat.R;
import androidx.appcompat.widget.ContentFrameLayout;
import androidx.core.os.LocaleListCompat;
import android.os.Build$VERSION;
import androidx.appcompat.widget.AppCompatDrawableManager;
import android.app.Dialog;
import android.app.Activity;
import android.os.Build;
import android.view.Window;
import android.widget.TextView;
import android.graphics.Rect;
import android.view.ViewGroup;
import android.view.View;
import android.view.MenuInflater;
import androidx.core.view.ViewPropertyAnimatorCompat;
import android.content.res.Configuration;
import android.window.OnBackInvokedDispatcher;
import androidx.appcompat.widget.DecorContentParent;
import android.content.Context;
import android.window.OnBackInvokedCallback;
import androidx.appcompat.widget.ActionBarContextView;
import android.widget.PopupWindow;
import androidx.appcompat.view.ActionMode;
import androidx.collection.SimpleArrayMap;
import androidx.annotation.RestrictTo;
import android.view.LayoutInflater$Factory2;
import androidx.appcompat.view.menu.MenuBuilder;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
class AppCompatDelegateImpl extends AppCompatDelegate implements Callback, LayoutInflater$Factory2
{
    static final String EXCEPTION_HANDLER_MESSAGE_SUFFIX = ". If the resource you are trying to use is a vector resource, you may be referencing it in an unsupported way. See AppCompatDelegate.setCompatVectorFromResourcesEnabled() for more info.";
    private static final boolean IS_PRE_LOLLIPOP;
    private static final boolean sCanApplyOverrideConfiguration;
    private static final boolean sCanReturnDifferentContext;
    private static boolean sInstalledExceptionHandler;
    private static final SimpleArrayMap<String, Integer> sLocalNightModes;
    private static final int[] sWindowBackgroundStyleable;
    ActionBar mActionBar;
    private ActionMenuPresenterCallback mActionMenuPresenterCallback;
    ActionMode mActionMode;
    PopupWindow mActionModePopup;
    ActionBarContextView mActionModeView;
    private int mActivityHandlesConfigFlags;
    private boolean mActivityHandlesConfigFlagsChecked;
    final AppCompatCallback mAppCompatCallback;
    private AppCompatViewInflater mAppCompatViewInflater;
    private AppCompatWindowCallback mAppCompatWindowCallback;
    private AutoNightModeManager mAutoBatteryNightModeManager;
    private AutoNightModeManager mAutoTimeNightModeManager;
    private OnBackInvokedCallback mBackCallback;
    private boolean mBaseContextAttached;
    private boolean mClosingActionMenu;
    final Context mContext;
    private boolean mCreated;
    private DecorContentParent mDecorContentParent;
    boolean mDestroyed;
    private OnBackInvokedDispatcher mDispatcher;
    private Configuration mEffectiveConfiguration;
    private boolean mEnableDefaultActionBarUp;
    ViewPropertyAnimatorCompat mFadeAnim;
    private boolean mFeatureIndeterminateProgress;
    private boolean mFeatureProgress;
    private boolean mHandleNativeActionModes;
    boolean mHasActionBar;
    final Object mHost;
    int mInvalidatePanelMenuFeatures;
    boolean mInvalidatePanelMenuPosted;
    private final Runnable mInvalidatePanelMenuRunnable;
    boolean mIsFloating;
    private LayoutIncludeDetector mLayoutIncludeDetector;
    private int mLocalNightMode;
    private boolean mLongPressBackDown;
    MenuInflater mMenuInflater;
    boolean mOverlayActionBar;
    boolean mOverlayActionMode;
    private PanelMenuPresenterCallback mPanelMenuPresenterCallback;
    private PanelFeatureState[] mPanels;
    private PanelFeatureState mPreparedPanel;
    Runnable mShowActionModePopup;
    private View mStatusGuard;
    ViewGroup mSubDecor;
    private boolean mSubDecorInstalled;
    private Rect mTempRect1;
    private Rect mTempRect2;
    private int mThemeResId;
    private CharSequence mTitle;
    private TextView mTitleView;
    Window mWindow;
    boolean mWindowNoTitle;
    
    static {
        sLocalNightModes = new SimpleArrayMap<String, Integer>();
        IS_PRE_LOLLIPOP = false;
        sWindowBackgroundStyleable = new int[] { 16842836 };
        sCanReturnDifferentContext = ("robolectric".equals(Build.FINGERPRINT) ^ true);
        sCanApplyOverrideConfiguration = true;
    }
    
    AppCompatDelegateImpl(final Activity activity, final AppCompatCallback appCompatCallback) {
        this((Context)activity, null, appCompatCallback, activity);
    }
    
    AppCompatDelegateImpl(final Dialog dialog, final AppCompatCallback appCompatCallback) {
        this(dialog.getContext(), dialog.getWindow(), appCompatCallback, dialog);
    }
    
    AppCompatDelegateImpl(final Context context, final Activity activity, final AppCompatCallback appCompatCallback) {
        this(context, null, appCompatCallback, activity);
    }
    
    AppCompatDelegateImpl(final Context context, final Window window, final AppCompatCallback appCompatCallback) {
        this(context, window, appCompatCallback, context);
    }
    
    private AppCompatDelegateImpl(final Context mContext, final Window window, final AppCompatCallback mAppCompatCallback, final Object mHost) {
        this.mFadeAnim = null;
        this.mHandleNativeActionModes = true;
        this.mLocalNightMode = -100;
        this.mInvalidatePanelMenuRunnable = new Runnable() {
            final AppCompatDelegateImpl this$0;
            
            @Override
            public void run() {
                final AppCompatDelegateImpl this$0 = this.this$0;
                if ((this$0.mInvalidatePanelMenuFeatures & 0x1) != 0x0) {
                    this$0.doInvalidatePanelMenu(0);
                }
                final AppCompatDelegateImpl this$2 = this.this$0;
                if ((this$2.mInvalidatePanelMenuFeatures & 0x1000) != 0x0) {
                    this$2.doInvalidatePanelMenu(108);
                }
                final AppCompatDelegateImpl this$3 = this.this$0;
                this$3.mInvalidatePanelMenuPosted = false;
                this$3.mInvalidatePanelMenuFeatures = 0;
            }
        };
        this.mContext = mContext;
        this.mAppCompatCallback = mAppCompatCallback;
        this.mHost = mHost;
        if (this.mLocalNightMode == -100 && mHost instanceof Dialog) {
            final AppCompatActivity tryUnwrapContext = this.tryUnwrapContext();
            if (tryUnwrapContext != null) {
                this.mLocalNightMode = tryUnwrapContext.getDelegate().getLocalNightMode();
            }
        }
        if (this.mLocalNightMode == -100) {
            final SimpleArrayMap<String, Integer> sLocalNightModes = AppCompatDelegateImpl.sLocalNightModes;
            final Integer n = sLocalNightModes.get(mHost.getClass().getName());
            if (n != null) {
                this.mLocalNightMode = n;
                sLocalNightModes.remove(mHost.getClass().getName());
            }
        }
        if (window != null) {
            this.attachToWindow(window);
        }
        AppCompatDrawableManager.preload();
    }
    
    private boolean applyApplicationSpecificConfig(final boolean b) {
        return this.applyApplicationSpecificConfig(b, true);
    }
    
    private boolean applyApplicationSpecificConfig(final boolean b, final boolean b2) {
        if (this.mDestroyed) {
            return false;
        }
        final int calculateNightMode = this.calculateNightMode();
        final int mapNightMode = this.mapNightMode(this.mContext, calculateNightMode);
        LocaleListCompat calculateApplicationLocales;
        if (Build$VERSION.SDK_INT < 33) {
            calculateApplicationLocales = this.calculateApplicationLocales(this.mContext);
        }
        else {
            calculateApplicationLocales = null;
        }
        LocaleListCompat configurationLocales = calculateApplicationLocales;
        if (!b2 && (configurationLocales = calculateApplicationLocales) != null) {
            configurationLocales = this.getConfigurationLocales(this.mContext.getResources().getConfiguration());
        }
        final boolean updateAppConfiguration = this.updateAppConfiguration(mapNightMode, configurationLocales, b);
        if (calculateNightMode == 0) {
            this.getAutoTimeNightModeManager(this.mContext).setup();
        }
        else {
            final AutoNightModeManager mAutoTimeNightModeManager = this.mAutoTimeNightModeManager;
            if (mAutoTimeNightModeManager != null) {
                mAutoTimeNightModeManager.cleanup();
            }
        }
        if (calculateNightMode == 3) {
            this.getAutoBatteryNightModeManager(this.mContext).setup();
        }
        else {
            final AutoNightModeManager mAutoBatteryNightModeManager = this.mAutoBatteryNightModeManager;
            if (mAutoBatteryNightModeManager != null) {
                mAutoBatteryNightModeManager.cleanup();
            }
        }
        return updateAppConfiguration;
    }
    
    private void applyFixedSizeWindow() {
        final ContentFrameLayout contentFrameLayout = (ContentFrameLayout)((View)this.mSubDecor).findViewById(16908290);
        final View decorView = this.mWindow.getDecorView();
        contentFrameLayout.setDecorPadding(decorView.getPaddingLeft(), decorView.getPaddingTop(), decorView.getPaddingRight(), decorView.getPaddingBottom());
        final TypedArray obtainStyledAttributes = this.mContext.obtainStyledAttributes(R.styleable.AppCompatTheme);
        obtainStyledAttributes.getValue(R.styleable.AppCompatTheme_windowMinWidthMajor, contentFrameLayout.getMinWidthMajor());
        obtainStyledAttributes.getValue(R.styleable.AppCompatTheme_windowMinWidthMinor, contentFrameLayout.getMinWidthMinor());
        final int appCompatTheme_windowFixedWidthMajor = R.styleable.AppCompatTheme_windowFixedWidthMajor;
        if (obtainStyledAttributes.hasValue(appCompatTheme_windowFixedWidthMajor)) {
            obtainStyledAttributes.getValue(appCompatTheme_windowFixedWidthMajor, contentFrameLayout.getFixedWidthMajor());
        }
        final int appCompatTheme_windowFixedWidthMinor = R.styleable.AppCompatTheme_windowFixedWidthMinor;
        if (obtainStyledAttributes.hasValue(appCompatTheme_windowFixedWidthMinor)) {
            obtainStyledAttributes.getValue(appCompatTheme_windowFixedWidthMinor, contentFrameLayout.getFixedWidthMinor());
        }
        final int appCompatTheme_windowFixedHeightMajor = R.styleable.AppCompatTheme_windowFixedHeightMajor;
        if (obtainStyledAttributes.hasValue(appCompatTheme_windowFixedHeightMajor)) {
            obtainStyledAttributes.getValue(appCompatTheme_windowFixedHeightMajor, contentFrameLayout.getFixedHeightMajor());
        }
        final int appCompatTheme_windowFixedHeightMinor = R.styleable.AppCompatTheme_windowFixedHeightMinor;
        if (obtainStyledAttributes.hasValue(appCompatTheme_windowFixedHeightMinor)) {
            obtainStyledAttributes.getValue(appCompatTheme_windowFixedHeightMinor, contentFrameLayout.getFixedHeightMinor());
        }
        obtainStyledAttributes.recycle();
        ((View)contentFrameLayout).requestLayout();
    }
    
    private void attachToWindow(@NonNull final Window mWindow) {
        if (this.mWindow != null) {
            throw new IllegalStateException("AppCompat has already installed itself into the Window");
        }
        final Window$Callback callback = mWindow.getCallback();
        if (!(callback instanceof AppCompatWindowCallback)) {
            mWindow.setCallback((Window$Callback)(this.mAppCompatWindowCallback = new AppCompatWindowCallback(callback)));
            final TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(this.mContext, null, AppCompatDelegateImpl.sWindowBackgroundStyleable);
            final Drawable drawableIfKnown = obtainStyledAttributes.getDrawableIfKnown(0);
            if (drawableIfKnown != null) {
                mWindow.setBackgroundDrawable(drawableIfKnown);
            }
            obtainStyledAttributes.recycle();
            this.mWindow = mWindow;
            if (Build$VERSION.SDK_INT >= 33 && this.mDispatcher == null) {
                this.setOnBackInvokedDispatcher(null);
            }
            return;
        }
        throw new IllegalStateException("AppCompat has already installed itself into the Window");
    }
    
    private int calculateNightMode() {
        int n = this.mLocalNightMode;
        if (n == -100) {
            n = AppCompatDelegate.getDefaultNightMode();
        }
        return n;
    }
    
    private void cleanupAutoManagers() {
        final AutoNightModeManager mAutoTimeNightModeManager = this.mAutoTimeNightModeManager;
        if (mAutoTimeNightModeManager != null) {
            mAutoTimeNightModeManager.cleanup();
        }
        final AutoNightModeManager mAutoBatteryNightModeManager = this.mAutoBatteryNightModeManager;
        if (mAutoBatteryNightModeManager != null) {
            mAutoBatteryNightModeManager.cleanup();
        }
    }
    
    @NonNull
    private Configuration createOverrideAppConfiguration(@NonNull final Context context, int n, @Nullable final LocaleListCompat localeListCompat, @Nullable final Configuration to, final boolean b) {
        if (n != 1) {
            if (n != 2) {
                if (b) {
                    n = 0;
                }
                else {
                    n = (context.getApplicationContext().getResources().getConfiguration().uiMode & 0x30);
                }
            }
            else {
                n = 32;
            }
        }
        else {
            n = 16;
        }
        final Configuration configuration = new Configuration();
        configuration.fontScale = 0.0f;
        if (to != null) {
            configuration.setTo(to);
        }
        configuration.uiMode = (n | (configuration.uiMode & 0xFFFFFFCF));
        if (localeListCompat != null) {
            this.setConfigurationLocales(configuration, localeListCompat);
        }
        return configuration;
    }
    
    private ViewGroup createSubDecor() {
        final TypedArray obtainStyledAttributes = this.mContext.obtainStyledAttributes(R.styleable.AppCompatTheme);
        final int appCompatTheme_windowActionBar = R.styleable.AppCompatTheme_windowActionBar;
        if (!obtainStyledAttributes.hasValue(appCompatTheme_windowActionBar)) {
            obtainStyledAttributes.recycle();
            throw new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
        }
        if (obtainStyledAttributes.getBoolean(R.styleable.AppCompatTheme_windowNoTitle, false)) {
            this.requestWindowFeature(1);
        }
        else if (obtainStyledAttributes.getBoolean(appCompatTheme_windowActionBar, false)) {
            this.requestWindowFeature(108);
        }
        if (obtainStyledAttributes.getBoolean(R.styleable.AppCompatTheme_windowActionBarOverlay, false)) {
            this.requestWindowFeature(109);
        }
        if (obtainStyledAttributes.getBoolean(R.styleable.AppCompatTheme_windowActionModeOverlay, false)) {
            this.requestWindowFeature(10);
        }
        this.mIsFloating = obtainStyledAttributes.getBoolean(R.styleable.AppCompatTheme_android_windowIsFloating, false);
        obtainStyledAttributes.recycle();
        this.ensureWindow();
        this.mWindow.getDecorView();
        final LayoutInflater from = LayoutInflater.from(this.mContext);
        Object contentView;
        if (!this.mWindowNoTitle) {
            if (this.mIsFloating) {
                contentView = from.inflate(R.layout.abc_dialog_title_material, (ViewGroup)null);
                this.mOverlayActionBar = false;
                this.mHasActionBar = false;
            }
            else if (this.mHasActionBar) {
                final TypedValue typedValue = new TypedValue();
                this.mContext.getTheme().resolveAttribute(R.attr.actionBarTheme, typedValue, true);
                Object mContext;
                if (typedValue.resourceId != 0) {
                    mContext = new ContextThemeWrapper(this.mContext, typedValue.resourceId);
                }
                else {
                    mContext = this.mContext;
                }
                final ViewGroup viewGroup = (ViewGroup)LayoutInflater.from((Context)mContext).inflate(R.layout.abc_screen_toolbar, (ViewGroup)null);
                (this.mDecorContentParent = (DecorContentParent)((View)viewGroup).findViewById(R.id.decor_content_parent)).setWindowCallback(this.getWindowCallback());
                if (this.mOverlayActionBar) {
                    this.mDecorContentParent.initFeature(109);
                }
                if (this.mFeatureProgress) {
                    this.mDecorContentParent.initFeature(2);
                }
                contentView = viewGroup;
                if (this.mFeatureIndeterminateProgress) {
                    this.mDecorContentParent.initFeature(5);
                    contentView = viewGroup;
                }
            }
            else {
                contentView = null;
            }
        }
        else if (this.mOverlayActionMode) {
            contentView = from.inflate(R.layout.abc_screen_simple_overlay_action_mode, (ViewGroup)null);
        }
        else {
            contentView = from.inflate(R.layout.abc_screen_simple, (ViewGroup)null);
        }
        if (contentView != null) {
            ViewCompat.setOnApplyWindowInsetsListener((View)contentView, new OnApplyWindowInsetsListener(this) {
                final AppCompatDelegateImpl this$0;
                
                @Override
                public WindowInsetsCompat onApplyWindowInsets(final View view, final WindowInsetsCompat windowInsetsCompat) {
                    final int systemWindowInsetTop = windowInsetsCompat.getSystemWindowInsetTop();
                    final int updateStatusGuard = this.this$0.updateStatusGuard(windowInsetsCompat, null);
                    WindowInsetsCompat replaceSystemWindowInsets = windowInsetsCompat;
                    if (systemWindowInsetTop != updateStatusGuard) {
                        replaceSystemWindowInsets = windowInsetsCompat.replaceSystemWindowInsets(windowInsetsCompat.getSystemWindowInsetLeft(), updateStatusGuard, windowInsetsCompat.getSystemWindowInsetRight(), windowInsetsCompat.getSystemWindowInsetBottom());
                    }
                    return ViewCompat.onApplyWindowInsets(view, replaceSystemWindowInsets);
                }
            });
            if (this.mDecorContentParent == null) {
                this.mTitleView = (TextView)((View)contentView).findViewById(R.id.title);
            }
            ViewUtils.makeOptionalFitsSystemWindows((View)contentView);
            final ContentFrameLayout contentFrameLayout = (ContentFrameLayout)((View)contentView).findViewById(R.id.action_bar_activity_content);
            final ViewGroup viewGroup2 = (ViewGroup)this.mWindow.findViewById(16908290);
            if (viewGroup2 != null) {
                while (viewGroup2.getChildCount() > 0) {
                    final View child = viewGroup2.getChildAt(0);
                    viewGroup2.removeViewAt(0);
                    ((ViewGroup)contentFrameLayout).addView(child);
                }
                ((View)viewGroup2).setId(-1);
                ((View)contentFrameLayout).setId(16908290);
                if (viewGroup2 instanceof FrameLayout) {
                    ((FrameLayout)viewGroup2).setForeground((Drawable)null);
                }
            }
            this.mWindow.setContentView((View)contentView);
            contentFrameLayout.setAttachListener((ContentFrameLayout.OnAttachListener)new ContentFrameLayout.OnAttachListener(this) {
                final AppCompatDelegateImpl this$0;
                
                @Override
                public void onAttachedFromWindow() {
                }
                
                @Override
                public void onDetachedFromWindow() {
                    this.this$0.dismissPopups();
                }
            });
            return (ViewGroup)contentView;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("AppCompat does not support the current theme features: { windowActionBar: ");
        sb.append(this.mHasActionBar);
        sb.append(", windowActionBarOverlay: ");
        sb.append(this.mOverlayActionBar);
        sb.append(", android:windowIsFloating: ");
        sb.append(this.mIsFloating);
        sb.append(", windowActionModeOverlay: ");
        sb.append(this.mOverlayActionMode);
        sb.append(", windowNoTitle: ");
        sb.append(this.mWindowNoTitle);
        sb.append(" }");
        throw new IllegalArgumentException(sb.toString());
    }
    
    private void ensureSubDecor() {
        if (!this.mSubDecorInstalled) {
            this.mSubDecor = this.createSubDecor();
            final CharSequence title = this.getTitle();
            if (!TextUtils.isEmpty(title)) {
                final DecorContentParent mDecorContentParent = this.mDecorContentParent;
                if (mDecorContentParent != null) {
                    mDecorContentParent.setWindowTitle(title);
                }
                else if (this.peekSupportActionBar() != null) {
                    this.peekSupportActionBar().setWindowTitle(title);
                }
                else {
                    final TextView mTitleView = this.mTitleView;
                    if (mTitleView != null) {
                        mTitleView.setText(title);
                    }
                }
            }
            this.applyFixedSizeWindow();
            this.onSubDecorInstalled(this.mSubDecor);
            this.mSubDecorInstalled = true;
            final PanelFeatureState panelState = this.getPanelState(0, false);
            if (!this.mDestroyed && (panelState == null || panelState.menu == null)) {
                this.invalidatePanelMenu(108);
            }
        }
    }
    
    private void ensureWindow() {
        if (this.mWindow == null) {
            final Object mHost = this.mHost;
            if (mHost instanceof Activity) {
                this.attachToWindow(((Activity)mHost).getWindow());
            }
        }
        if (this.mWindow != null) {
            return;
        }
        throw new IllegalStateException("We have not been given a Window");
    }
    
    @NonNull
    private static Configuration generateConfigDelta(@NonNull final Configuration configuration, @Nullable final Configuration configuration2) {
        final Configuration configuration3 = new Configuration();
        configuration3.fontScale = 0.0f;
        if (configuration2 != null) {
            if (configuration.diff(configuration2) != 0) {
                final float fontScale = configuration.fontScale;
                final float fontScale2 = configuration2.fontScale;
                if (fontScale != fontScale2) {
                    configuration3.fontScale = fontScale2;
                }
                final int mcc = configuration.mcc;
                final int mcc2 = configuration2.mcc;
                if (mcc != mcc2) {
                    configuration3.mcc = mcc2;
                }
                final int mnc = configuration.mnc;
                final int mnc2 = configuration2.mnc;
                if (mnc != mnc2) {
                    configuration3.mnc = mnc2;
                }
                final int sdk_INT = Build$VERSION.SDK_INT;
                if (sdk_INT >= 24) {
                    Api24Impl.generateConfigDelta_locale(configuration, configuration2, configuration3);
                }
                else if (!ObjectsCompat.equals(configuration.locale, configuration2.locale)) {
                    configuration3.locale = configuration2.locale;
                }
                final int touchscreen = configuration.touchscreen;
                final int touchscreen2 = configuration2.touchscreen;
                if (touchscreen != touchscreen2) {
                    configuration3.touchscreen = touchscreen2;
                }
                final int keyboard = configuration.keyboard;
                final int keyboard2 = configuration2.keyboard;
                if (keyboard != keyboard2) {
                    configuration3.keyboard = keyboard2;
                }
                final int keyboardHidden = configuration.keyboardHidden;
                final int keyboardHidden2 = configuration2.keyboardHidden;
                if (keyboardHidden != keyboardHidden2) {
                    configuration3.keyboardHidden = keyboardHidden2;
                }
                final int navigation = configuration.navigation;
                final int navigation2 = configuration2.navigation;
                if (navigation != navigation2) {
                    configuration3.navigation = navigation2;
                }
                final int navigationHidden = configuration.navigationHidden;
                final int navigationHidden2 = configuration2.navigationHidden;
                if (navigationHidden != navigationHidden2) {
                    configuration3.navigationHidden = navigationHidden2;
                }
                final int orientation = configuration.orientation;
                final int orientation2 = configuration2.orientation;
                if (orientation != orientation2) {
                    configuration3.orientation = orientation2;
                }
                final int screenLayout = configuration.screenLayout;
                final int screenLayout2 = configuration2.screenLayout;
                if ((screenLayout & 0xF) != (screenLayout2 & 0xF)) {
                    configuration3.screenLayout |= (screenLayout2 & 0xF);
                }
                final int screenLayout3 = configuration.screenLayout;
                final int screenLayout4 = configuration2.screenLayout;
                if ((screenLayout3 & 0xC0) != (screenLayout4 & 0xC0)) {
                    configuration3.screenLayout |= (screenLayout4 & 0xC0);
                }
                final int screenLayout5 = configuration.screenLayout;
                final int screenLayout6 = configuration2.screenLayout;
                if ((screenLayout5 & 0x30) != (screenLayout6 & 0x30)) {
                    configuration3.screenLayout |= (screenLayout6 & 0x30);
                }
                final int screenLayout7 = configuration.screenLayout;
                final int screenLayout8 = configuration2.screenLayout;
                if ((screenLayout7 & 0x300) != (screenLayout8 & 0x300)) {
                    configuration3.screenLayout |= (screenLayout8 & 0x300);
                }
                if (sdk_INT >= 26) {
                    Api26Impl.generateConfigDelta_colorMode(configuration, configuration2, configuration3);
                }
                final int uiMode = configuration.uiMode;
                final int uiMode2 = configuration2.uiMode;
                if ((uiMode & 0xF) != (uiMode2 & 0xF)) {
                    configuration3.uiMode |= (uiMode2 & 0xF);
                }
                final int uiMode3 = configuration.uiMode;
                final int uiMode4 = configuration2.uiMode;
                if ((uiMode3 & 0x30) != (uiMode4 & 0x30)) {
                    configuration3.uiMode |= (uiMode4 & 0x30);
                }
                final int screenWidthDp = configuration.screenWidthDp;
                final int screenWidthDp2 = configuration2.screenWidthDp;
                if (screenWidthDp != screenWidthDp2) {
                    configuration3.screenWidthDp = screenWidthDp2;
                }
                final int screenHeightDp = configuration.screenHeightDp;
                final int screenHeightDp2 = configuration2.screenHeightDp;
                if (screenHeightDp != screenHeightDp2) {
                    configuration3.screenHeightDp = screenHeightDp2;
                }
                final int smallestScreenWidthDp = configuration.smallestScreenWidthDp;
                final int smallestScreenWidthDp2 = configuration2.smallestScreenWidthDp;
                if (smallestScreenWidthDp != smallestScreenWidthDp2) {
                    configuration3.smallestScreenWidthDp = smallestScreenWidthDp2;
                }
                Api17Impl.generateConfigDelta_densityDpi(configuration, configuration2, configuration3);
            }
        }
        return configuration3;
    }
    
    private int getActivityHandlesConfigChangesFlags(final Context context) {
        if (!this.mActivityHandlesConfigFlagsChecked && this.mHost instanceof Activity) {
            final PackageManager packageManager = context.getPackageManager();
            if (packageManager == null) {
                return 0;
            }
            try {
                final int sdk_INT = Build$VERSION.SDK_INT;
                int n;
                if (sdk_INT >= 29) {
                    n = 269221888;
                }
                else if (sdk_INT >= 24) {
                    n = 786432;
                }
                else {
                    n = 0;
                }
                final ActivityInfo activityInfo = packageManager.getActivityInfo(new ComponentName(context, (Class)this.mHost.getClass()), n);
                if (activityInfo != null) {
                    this.mActivityHandlesConfigFlags = activityInfo.configChanges;
                }
            }
            catch (final PackageManager$NameNotFoundException ex) {
                this.mActivityHandlesConfigFlags = 0;
            }
        }
        this.mActivityHandlesConfigFlagsChecked = true;
        return this.mActivityHandlesConfigFlags;
    }
    
    private AutoNightModeManager getAutoBatteryNightModeManager(@NonNull final Context context) {
        if (this.mAutoBatteryNightModeManager == null) {
            this.mAutoBatteryNightModeManager = (AutoNightModeManager)new AutoBatteryNightModeManager(context);
        }
        return this.mAutoBatteryNightModeManager;
    }
    
    private AutoNightModeManager getAutoTimeNightModeManager(@NonNull final Context context) {
        if (this.mAutoTimeNightModeManager == null) {
            this.mAutoTimeNightModeManager = (AutoNightModeManager)new AutoTimeNightModeManager(TwilightManager.getInstance(context));
        }
        return this.mAutoTimeNightModeManager;
    }
    
    private void initWindowDecorActionBar() {
        this.ensureSubDecor();
        if (this.mHasActionBar) {
            if (this.mActionBar == null) {
                final Object mHost = this.mHost;
                if (mHost instanceof Activity) {
                    this.mActionBar = new WindowDecorActionBar((Activity)this.mHost, this.mOverlayActionBar);
                }
                else if (mHost instanceof Dialog) {
                    this.mActionBar = new WindowDecorActionBar((Dialog)this.mHost);
                }
                final ActionBar mActionBar = this.mActionBar;
                if (mActionBar != null) {
                    mActionBar.setDefaultDisplayHomeAsUpEnabled(this.mEnableDefaultActionBarUp);
                }
            }
        }
    }
    
    private boolean initializePanelContent(final PanelFeatureState panelFeatureState) {
        final View createdPanelView = panelFeatureState.createdPanelView;
        boolean b = true;
        if (createdPanelView != null) {
            panelFeatureState.shownPanelView = createdPanelView;
            return true;
        }
        if (panelFeatureState.menu == null) {
            return false;
        }
        if (this.mPanelMenuPresenterCallback == null) {
            this.mPanelMenuPresenterCallback = new PanelMenuPresenterCallback();
        }
        if ((panelFeatureState.shownPanelView = (View)panelFeatureState.getListMenuView(this.mPanelMenuPresenterCallback)) == null) {
            b = false;
        }
        return b;
    }
    
    private boolean initializePanelDecor(final PanelFeatureState panelFeatureState) {
        panelFeatureState.setStyle(this.getActionBarThemedContext());
        panelFeatureState.decorView = (ViewGroup)new ListMenuDecorView(panelFeatureState.listPresenterContext);
        panelFeatureState.gravity = 81;
        return true;
    }
    
    private boolean initializePanelMenu(final PanelFeatureState panelFeatureState) {
        final Context mContext = this.mContext;
        final int featureId = panelFeatureState.featureId;
        Object o = null;
        Label_0197: {
            if (featureId != 0) {
                o = mContext;
                if (featureId != 108) {
                    break Label_0197;
                }
            }
            o = mContext;
            if (this.mDecorContentParent != null) {
                final TypedValue typedValue = new TypedValue();
                final Resources$Theme theme = mContext.getTheme();
                theme.resolveAttribute(R.attr.actionBarTheme, typedValue, true);
                Resources$Theme theme2;
                if (typedValue.resourceId != 0) {
                    theme2 = mContext.getResources().newTheme();
                    theme2.setTo(theme);
                    theme2.applyStyle(typedValue.resourceId, true);
                    theme2.resolveAttribute(R.attr.actionBarWidgetTheme, typedValue, true);
                }
                else {
                    theme.resolveAttribute(R.attr.actionBarWidgetTheme, typedValue, true);
                    theme2 = null;
                }
                Resources$Theme theme3 = theme2;
                if (typedValue.resourceId != 0) {
                    if ((theme3 = theme2) == null) {
                        theme3 = mContext.getResources().newTheme();
                        theme3.setTo(theme);
                    }
                    theme3.applyStyle(typedValue.resourceId, true);
                }
                o = mContext;
                if (theme3 != null) {
                    o = new ContextThemeWrapper(mContext, 0);
                    ((Context)o).getTheme().setTo(theme3);
                }
            }
        }
        final MenuBuilder menu = new MenuBuilder((Context)o);
        menu.setCallback((MenuBuilder.Callback)this);
        panelFeatureState.setMenu(menu);
        return true;
    }
    
    private void invalidatePanelMenu(final int n) {
        this.mInvalidatePanelMenuFeatures |= 1 << n;
        if (!this.mInvalidatePanelMenuPosted) {
            ViewCompat.postOnAnimation(this.mWindow.getDecorView(), this.mInvalidatePanelMenuRunnable);
            this.mInvalidatePanelMenuPosted = true;
        }
    }
    
    private boolean onKeyDownPanel(final int n, final KeyEvent keyEvent) {
        if (keyEvent.getRepeatCount() == 0) {
            final PanelFeatureState panelState = this.getPanelState(n, true);
            if (!panelState.isOpen) {
                return this.preparePanel(panelState, keyEvent);
            }
        }
        return false;
    }
    
    private boolean onKeyUpPanel(final int n, final KeyEvent keyEvent) {
        if (this.mActionMode != null) {
            return false;
        }
        final boolean b = true;
        final PanelFeatureState panelState = this.getPanelState(n, true);
        boolean b2 = false;
        Label_0198: {
            Label_0186: {
                if (n == 0) {
                    final DecorContentParent mDecorContentParent = this.mDecorContentParent;
                    if (mDecorContentParent != null && mDecorContentParent.canShowOverflowMenu() && !ViewConfiguration.get(this.mContext).hasPermanentMenuKey()) {
                        if (this.mDecorContentParent.isOverflowMenuShowing()) {
                            b2 = this.mDecorContentParent.hideOverflowMenu();
                            break Label_0198;
                        }
                        if (!this.mDestroyed && this.preparePanel(panelState, keyEvent)) {
                            b2 = this.mDecorContentParent.showOverflowMenu();
                            break Label_0198;
                        }
                        break Label_0186;
                    }
                }
                b2 = panelState.isOpen;
                if (b2 || panelState.isHandled) {
                    this.closePanel(panelState, true);
                    break Label_0198;
                }
                if (panelState.isPrepared) {
                    boolean preparePanel;
                    if (panelState.refreshMenuContent) {
                        panelState.isPrepared = false;
                        preparePanel = this.preparePanel(panelState, keyEvent);
                    }
                    else {
                        preparePanel = true;
                    }
                    if (preparePanel) {
                        this.openPanel(panelState, keyEvent);
                        b2 = b;
                        break Label_0198;
                    }
                }
            }
            b2 = false;
        }
        if (b2) {
            final AudioManager audioManager = (AudioManager)this.mContext.getApplicationContext().getSystemService("audio");
            if (audioManager != null) {
                audioManager.playSoundEffect(0);
            }
        }
        return b2;
    }
    
    private void openPanel(final PanelFeatureState panelFeatureState, final KeyEvent keyEvent) {
        if (!panelFeatureState.isOpen) {
            if (!this.mDestroyed) {
                if (panelFeatureState.featureId == 0 && (this.mContext.getResources().getConfiguration().screenLayout & 0xF) == 0x4) {
                    return;
                }
                final Window$Callback windowCallback = this.getWindowCallback();
                if (windowCallback != null && !windowCallback.onMenuOpened(panelFeatureState.featureId, (Menu)panelFeatureState.menu)) {
                    this.closePanel(panelFeatureState, true);
                    return;
                }
                final WindowManager windowManager = (WindowManager)this.mContext.getSystemService("window");
                if (windowManager == null) {
                    return;
                }
                if (!this.preparePanel(panelFeatureState, keyEvent)) {
                    return;
                }
                final ViewGroup decorView = panelFeatureState.decorView;
                int n = 0;
                Label_0336: {
                    if (decorView != null && !panelFeatureState.refreshDecorView) {
                        final View createdPanelView = panelFeatureState.createdPanelView;
                        if (createdPanelView != null) {
                            final ViewGroup$LayoutParams layoutParams = createdPanelView.getLayoutParams();
                            if (layoutParams != null && layoutParams.width == -1) {
                                n = -1;
                                break Label_0336;
                            }
                        }
                    }
                    else {
                        if (decorView == null) {
                            if (!this.initializePanelDecor(panelFeatureState) || panelFeatureState.decorView == null) {
                                return;
                            }
                        }
                        else if (panelFeatureState.refreshDecorView && decorView.getChildCount() > 0) {
                            panelFeatureState.decorView.removeAllViews();
                        }
                        if (!this.initializePanelContent(panelFeatureState) || !panelFeatureState.hasPanelItems()) {
                            panelFeatureState.refreshDecorView = true;
                            return;
                        }
                        ViewGroup$LayoutParams layoutParams2;
                        if ((layoutParams2 = panelFeatureState.shownPanelView.getLayoutParams()) == null) {
                            layoutParams2 = new ViewGroup$LayoutParams(-2, -2);
                        }
                        ((View)panelFeatureState.decorView).setBackgroundResource(panelFeatureState.background);
                        final ViewParent parent = panelFeatureState.shownPanelView.getParent();
                        if (parent instanceof ViewGroup) {
                            ((ViewGroup)parent).removeView(panelFeatureState.shownPanelView);
                        }
                        panelFeatureState.decorView.addView(panelFeatureState.shownPanelView, layoutParams2);
                        if (!panelFeatureState.shownPanelView.hasFocus()) {
                            panelFeatureState.shownPanelView.requestFocus();
                        }
                    }
                    n = -2;
                }
                panelFeatureState.isHandled = false;
                final WindowManager$LayoutParams windowManager$LayoutParams = new WindowManager$LayoutParams(n, -2, panelFeatureState.x, panelFeatureState.y, 1002, 8519680, -3);
                windowManager$LayoutParams.gravity = panelFeatureState.gravity;
                windowManager$LayoutParams.windowAnimations = panelFeatureState.windowAnimations;
                ((ViewManager)windowManager).addView((View)panelFeatureState.decorView, (ViewGroup$LayoutParams)windowManager$LayoutParams);
                panelFeatureState.isOpen = true;
                if (panelFeatureState.featureId == 0) {
                    this.updateBackInvokedCallbackState();
                }
            }
        }
    }
    
    private boolean performPanelShortcut(final PanelFeatureState panelFeatureState, final int n, final KeyEvent keyEvent, final int n2) {
        final boolean system = keyEvent.isSystem();
        final boolean b = false;
        if (system) {
            return false;
        }
        boolean performShortcut = false;
        Label_0062: {
            if (!panelFeatureState.isPrepared) {
                performShortcut = b;
                if (!this.preparePanel(panelFeatureState, keyEvent)) {
                    break Label_0062;
                }
            }
            final MenuBuilder menu = panelFeatureState.menu;
            performShortcut = b;
            if (menu != null) {
                performShortcut = menu.performShortcut(n, keyEvent, n2);
            }
        }
        if (performShortcut && (n2 & 0x1) == 0x0 && this.mDecorContentParent == null) {
            this.closePanel(panelFeatureState, true);
        }
        return performShortcut;
    }
    
    private boolean preparePanel(final PanelFeatureState mPreparedPanel, final KeyEvent keyEvent) {
        if (this.mDestroyed) {
            return false;
        }
        if (mPreparedPanel.isPrepared) {
            return true;
        }
        final PanelFeatureState mPreparedPanel2 = this.mPreparedPanel;
        if (mPreparedPanel2 != null && mPreparedPanel2 != mPreparedPanel) {
            this.closePanel(mPreparedPanel2, false);
        }
        final Window$Callback windowCallback = this.getWindowCallback();
        if (windowCallback != null) {
            mPreparedPanel.createdPanelView = windowCallback.onCreatePanelView(mPreparedPanel.featureId);
        }
        final int featureId = mPreparedPanel.featureId;
        final boolean b = featureId == 0 || featureId == 108;
        if (b) {
            final DecorContentParent mDecorContentParent = this.mDecorContentParent;
            if (mDecorContentParent != null) {
                mDecorContentParent.setMenuPrepared();
            }
        }
        if (mPreparedPanel.createdPanelView == null && (!b || !(this.peekSupportActionBar() instanceof ToolbarActionBar))) {
            final MenuBuilder menu = mPreparedPanel.menu;
            if (menu == null || mPreparedPanel.refreshMenuContent) {
                if (menu == null && (!this.initializePanelMenu(mPreparedPanel) || mPreparedPanel.menu == null)) {
                    return false;
                }
                if (b && this.mDecorContentParent != null) {
                    if (this.mActionMenuPresenterCallback == null) {
                        this.mActionMenuPresenterCallback = new ActionMenuPresenterCallback();
                    }
                    this.mDecorContentParent.setMenu((Menu)mPreparedPanel.menu, this.mActionMenuPresenterCallback);
                }
                mPreparedPanel.menu.stopDispatchingItemsChanged();
                if (!windowCallback.onCreatePanelMenu(mPreparedPanel.featureId, (Menu)mPreparedPanel.menu)) {
                    mPreparedPanel.setMenu(null);
                    if (b) {
                        final DecorContentParent mDecorContentParent2 = this.mDecorContentParent;
                        if (mDecorContentParent2 != null) {
                            mDecorContentParent2.setMenu(null, this.mActionMenuPresenterCallback);
                        }
                    }
                    return false;
                }
                mPreparedPanel.refreshMenuContent = false;
            }
            mPreparedPanel.menu.stopDispatchingItemsChanged();
            final Bundle frozenActionViewState = mPreparedPanel.frozenActionViewState;
            if (frozenActionViewState != null) {
                mPreparedPanel.menu.restoreActionViewStates(frozenActionViewState);
                mPreparedPanel.frozenActionViewState = null;
            }
            if (!windowCallback.onPreparePanel(0, mPreparedPanel.createdPanelView, (Menu)mPreparedPanel.menu)) {
                if (b) {
                    final DecorContentParent mDecorContentParent3 = this.mDecorContentParent;
                    if (mDecorContentParent3 != null) {
                        mDecorContentParent3.setMenu(null, this.mActionMenuPresenterCallback);
                    }
                }
                mPreparedPanel.menu.startDispatchingItemsChanged();
                return false;
            }
            int deviceId;
            if (keyEvent != null) {
                deviceId = keyEvent.getDeviceId();
            }
            else {
                deviceId = -1;
            }
            final boolean b2 = KeyCharacterMap.load(deviceId).getKeyboardType() != 1;
            mPreparedPanel.qwertyMode = b2;
            mPreparedPanel.menu.setQwertyMode(b2);
            mPreparedPanel.menu.startDispatchingItemsChanged();
        }
        mPreparedPanel.isPrepared = true;
        mPreparedPanel.isHandled = false;
        this.mPreparedPanel = mPreparedPanel;
        return true;
    }
    
    private void reopenMenu(final boolean b) {
        final DecorContentParent mDecorContentParent = this.mDecorContentParent;
        if (mDecorContentParent != null && mDecorContentParent.canShowOverflowMenu() && (!ViewConfiguration.get(this.mContext).hasPermanentMenuKey() || this.mDecorContentParent.isOverflowMenuShowPending())) {
            final Window$Callback windowCallback = this.getWindowCallback();
            if (this.mDecorContentParent.isOverflowMenuShowing() && b) {
                this.mDecorContentParent.hideOverflowMenu();
                if (!this.mDestroyed) {
                    windowCallback.onPanelClosed(108, (Menu)this.getPanelState(0, true).menu);
                }
            }
            else if (windowCallback != null && !this.mDestroyed) {
                if (this.mInvalidatePanelMenuPosted && (this.mInvalidatePanelMenuFeatures & 0x1) != 0x0) {
                    this.mWindow.getDecorView().removeCallbacks(this.mInvalidatePanelMenuRunnable);
                    this.mInvalidatePanelMenuRunnable.run();
                }
                final PanelFeatureState panelState = this.getPanelState(0, true);
                final MenuBuilder menu = panelState.menu;
                if (menu != null && !panelState.refreshMenuContent && windowCallback.onPreparePanel(0, panelState.createdPanelView, (Menu)menu)) {
                    windowCallback.onMenuOpened(108, (Menu)panelState.menu);
                    this.mDecorContentParent.showOverflowMenu();
                }
            }
            return;
        }
        final PanelFeatureState panelState2 = this.getPanelState(0, true);
        panelState2.refreshDecorView = true;
        this.closePanel(panelState2, false);
        this.openPanel(panelState2, null);
    }
    
    private int sanitizeWindowFeatureId(final int n) {
        if (n == 8) {
            return 108;
        }
        int n2;
        if ((n2 = n) == 9) {
            n2 = 109;
        }
        return n2;
    }
    
    private boolean shouldInheritContext(ViewParent parent) {
        if (parent == null) {
            return false;
        }
        final View decorView = this.mWindow.getDecorView();
        while (parent != null) {
            if (parent == decorView || !(parent instanceof View) || ViewCompat.isAttachedToWindow((View)parent)) {
                return false;
            }
            parent = parent.getParent();
        }
        return true;
    }
    
    private void throwFeatureRequestIfSubDecorInstalled() {
        if (!this.mSubDecorInstalled) {
            return;
        }
        throw new AndroidRuntimeException("Window feature must be requested before adding content");
    }
    
    @Nullable
    private AppCompatActivity tryUnwrapContext() {
        for (Context context = this.mContext; context != null; context = ((ContextWrapper)context).getBaseContext()) {
            if (context instanceof AppCompatActivity) {
                return (AppCompatActivity)context;
            }
            if (!(context instanceof ContextWrapper)) {
                break;
            }
        }
        return null;
    }
    
    private void updateActivityConfiguration(final Configuration configuration) {
        final Activity activity = (Activity)this.mHost;
        if (activity instanceof LifecycleOwner) {
            if (((LifecycleOwner)activity).getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.CREATED)) {
                activity.onConfigurationChanged(configuration);
            }
        }
        else if (this.mCreated && !this.mDestroyed) {
            activity.onConfigurationChanged(configuration);
        }
    }
    
    private boolean updateAppConfiguration(final int n, @Nullable final LocaleListCompat localeListCompat, final boolean b) {
        final Configuration overrideAppConfiguration = this.createOverrideAppConfiguration(this.mContext, n, localeListCompat, null, false);
        final int activityHandlesConfigChangesFlags = this.getActivityHandlesConfigChangesFlags(this.mContext);
        Configuration configuration;
        if ((configuration = this.mEffectiveConfiguration) == null) {
            configuration = this.mContext.getResources().getConfiguration();
        }
        final int uiMode = configuration.uiMode;
        final int n2 = overrideAppConfiguration.uiMode & 0x30;
        final LocaleListCompat configurationLocales = this.getConfigurationLocales(configuration);
        LocaleListCompat configurationLocales2;
        if (localeListCompat == null) {
            configurationLocales2 = null;
        }
        else {
            configurationLocales2 = this.getConfigurationLocales(overrideAppConfiguration);
        }
        final boolean b2 = false;
        int n3;
        if ((uiMode & 0x30) != n2) {
            n3 = 512;
        }
        else {
            n3 = 0;
        }
        int n4 = n3;
        if (configurationLocales2 != null) {
            n4 = n3;
            if (!configurationLocales.equals(configurationLocales2)) {
                n4 = (n3 | 0x4 | 0x2000);
            }
        }
        final boolean b3 = true;
        boolean b4 = false;
        Label_0230: {
            if ((~activityHandlesConfigChangesFlags & n4) != 0x0 && b && this.mBaseContextAttached && (AppCompatDelegateImpl.sCanReturnDifferentContext || this.mCreated)) {
                final Object mHost = this.mHost;
                if (mHost instanceof Activity && !((Activity)mHost).isChild()) {
                    ActivityCompat.recreate((Activity)this.mHost);
                    b4 = true;
                    break Label_0230;
                }
            }
            b4 = false;
        }
        if (!b4 && n4 != 0) {
            boolean b5 = b2;
            if ((n4 & activityHandlesConfigChangesFlags) == n4) {
                b5 = true;
            }
            this.updateResourcesConfiguration(n2, configurationLocales2, b5, null);
            b4 = b3;
        }
        if (b4) {
            final Object mHost2 = this.mHost;
            if (mHost2 instanceof AppCompatActivity) {
                if ((n4 & 0x200) != 0x0) {
                    ((AppCompatActivity)mHost2).onNightModeChanged(n);
                }
                if ((n4 & 0x4) != 0x0) {
                    ((AppCompatActivity)this.mHost).onLocalesChanged(localeListCompat);
                }
            }
        }
        if (b4 && configurationLocales2 != null) {
            this.setDefaultLocalesForLocaleList(this.getConfigurationLocales(this.mContext.getResources().getConfiguration()));
        }
        return b4;
    }
    
    private void updateResourcesConfiguration(int sdk_INT, @Nullable final LocaleListCompat localeListCompat, final boolean b, @Nullable final Configuration configuration) {
        final Resources resources = this.mContext.getResources();
        final Configuration configuration2 = new Configuration(resources.getConfiguration());
        if (configuration != null) {
            configuration2.updateFrom(configuration);
        }
        configuration2.uiMode = (sdk_INT | (resources.getConfiguration().uiMode & 0xFFFFFFCF));
        if (localeListCompat != null) {
            this.setConfigurationLocales(configuration2, localeListCompat);
        }
        resources.updateConfiguration(configuration2, (DisplayMetrics)null);
        sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT < 26) {
            ResourcesFlusher.flush(resources);
        }
        final int mThemeResId = this.mThemeResId;
        if (mThemeResId != 0) {
            this.mContext.setTheme(mThemeResId);
            if (sdk_INT >= 23) {
                this.mContext.getTheme().applyStyle(this.mThemeResId, true);
            }
        }
        if (b && this.mHost instanceof Activity) {
            this.updateActivityConfiguration(configuration2);
        }
    }
    
    private void updateStatusGuardColor(final View view) {
        int backgroundColor;
        if ((ViewCompat.getWindowSystemUiVisibility(view) & 0x2000) != 0x0) {
            backgroundColor = ContextCompat.getColor(this.mContext, R.color.abc_decor_view_status_guard_light);
        }
        else {
            backgroundColor = ContextCompat.getColor(this.mContext, R.color.abc_decor_view_status_guard);
        }
        view.setBackgroundColor(backgroundColor);
    }
    
    @Override
    public void addContentView(final View view, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        this.ensureSubDecor();
        ((ViewGroup)((View)this.mSubDecor).findViewById(16908290)).addView(view, viewGroup$LayoutParams);
        this.mAppCompatWindowCallback.bypassOnContentChanged(this.mWindow.getCallback());
    }
    
    @Override
    boolean applyAppLocales() {
        if (AppCompatDelegate.isAutoStorageOptedIn(this.mContext) && AppCompatDelegate.getRequestedAppLocales() != null && !AppCompatDelegate.getRequestedAppLocales().equals(AppCompatDelegate.getStoredAppLocales())) {
            this.asyncExecuteSyncRequestedAndStoredLocales(this.mContext);
        }
        return this.applyApplicationSpecificConfig(true);
    }
    
    @Override
    public boolean applyDayNight() {
        return this.applyApplicationSpecificConfig(true);
    }
    
    @CallSuper
    @NonNull
    @Override
    public Context attachBaseContext2(@NonNull final Context context) {
        int n = 1;
        this.mBaseContextAttached = true;
        final int mapNightMode = this.mapNightMode(context, this.calculateNightMode());
        if (AppCompatDelegate.isAutoStorageOptedIn(context)) {
            AppCompatDelegate.syncRequestedAndStoredLocales(context);
        }
        final LocaleListCompat calculateApplicationLocales = this.calculateApplicationLocales(context);
        if (AppCompatDelegateImpl.sCanApplyOverrideConfiguration && context instanceof android.view.ContextThemeWrapper) {
            final Configuration overrideAppConfiguration = this.createOverrideAppConfiguration(context, mapNightMode, calculateApplicationLocales, null, false);
            try {
                ContextThemeWrapperCompatApi17Impl.applyOverrideConfiguration((android.view.ContextThemeWrapper)context, overrideAppConfiguration);
                return context;
            }
            catch (final IllegalStateException ex) {}
        }
        if (context instanceof ContextThemeWrapper) {
            final Configuration overrideAppConfiguration2 = this.createOverrideAppConfiguration(context, mapNightMode, calculateApplicationLocales, null, false);
            try {
                ((ContextThemeWrapper)context).applyOverrideConfiguration(overrideAppConfiguration2);
                return context;
            }
            catch (final IllegalStateException ex2) {}
        }
        if (!AppCompatDelegateImpl.sCanReturnDifferentContext) {
            return super.attachBaseContext2(context);
        }
        final Configuration configuration = new Configuration();
        configuration.uiMode = -1;
        configuration.fontScale = 0.0f;
        final Configuration configuration2 = Api17Impl.createConfigurationContext(context, configuration).getResources().getConfiguration();
        final Configuration configuration3 = context.getResources().getConfiguration();
        configuration2.uiMode = configuration3.uiMode;
        Configuration generateConfigDelta;
        if (!configuration2.equals(configuration3)) {
            generateConfigDelta = generateConfigDelta(configuration2, configuration3);
        }
        else {
            generateConfigDelta = null;
        }
        final Configuration overrideAppConfiguration3 = this.createOverrideAppConfiguration(context, mapNightMode, calculateApplicationLocales, generateConfigDelta, true);
        final ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(context, R.style.Theme_AppCompat_Empty);
        contextThemeWrapper.applyOverrideConfiguration(overrideAppConfiguration3);
        final int n2 = 0;
        try {
            if (context.getTheme() == null) {
                n = 0;
            }
        }
        catch (final NullPointerException ex3) {
            n = n2;
        }
        if (n != 0) {
            ResourcesCompat.ThemeCompat.rebase(contextThemeWrapper.getTheme());
        }
        return super.attachBaseContext2((Context)contextThemeWrapper);
    }
    
    @Nullable
    LocaleListCompat calculateApplicationLocales(@NonNull final Context context) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 33) {
            return null;
        }
        final LocaleListCompat requestedAppLocales = AppCompatDelegate.getRequestedAppLocales();
        if (requestedAppLocales == null) {
            return null;
        }
        final LocaleListCompat configurationLocales = this.getConfigurationLocales(context.getApplicationContext().getResources().getConfiguration());
        LocaleListCompat localeListCompat;
        if (sdk_INT >= 24) {
            localeListCompat = LocaleOverlayHelper.combineLocalesIfOverlayExists(requestedAppLocales, configurationLocales);
        }
        else if (requestedAppLocales.isEmpty()) {
            localeListCompat = LocaleListCompat.getEmptyLocaleList();
        }
        else {
            localeListCompat = LocaleListCompat.forLanguageTags(requestedAppLocales.get(0).toString());
        }
        if (localeListCompat.isEmpty()) {
            localeListCompat = configurationLocales;
        }
        return localeListCompat;
    }
    
    void callOnPanelClosed(final int n, final PanelFeatureState panelFeatureState, final Menu menu) {
        PanelFeatureState panelFeatureState2 = panelFeatureState;
        Object menu2 = menu;
        if (menu == null) {
            PanelFeatureState panelFeatureState3;
            if ((panelFeatureState3 = panelFeatureState) == null) {
                panelFeatureState3 = panelFeatureState;
                if (n >= 0) {
                    final PanelFeatureState[] mPanels = this.mPanels;
                    panelFeatureState3 = panelFeatureState;
                    if (n < mPanels.length) {
                        panelFeatureState3 = mPanels[n];
                    }
                }
            }
            panelFeatureState2 = panelFeatureState3;
            menu2 = menu;
            if (panelFeatureState3 != null) {
                menu2 = panelFeatureState3.menu;
                panelFeatureState2 = panelFeatureState3;
            }
        }
        if (panelFeatureState2 != null && !panelFeatureState2.isOpen) {
            return;
        }
        if (!this.mDestroyed) {
            this.mAppCompatWindowCallback.bypassOnPanelClosed(this.mWindow.getCallback(), n, (Menu)menu2);
        }
    }
    
    void checkCloseActionMenu(@NonNull final MenuBuilder menuBuilder) {
        if (this.mClosingActionMenu) {
            return;
        }
        this.mClosingActionMenu = true;
        this.mDecorContentParent.dismissPopups();
        final Window$Callback windowCallback = this.getWindowCallback();
        if (windowCallback != null && !this.mDestroyed) {
            windowCallback.onPanelClosed(108, (Menu)menuBuilder);
        }
        this.mClosingActionMenu = false;
    }
    
    void closePanel(final int n) {
        this.closePanel(this.getPanelState(n, true), true);
    }
    
    void closePanel(final PanelFeatureState panelFeatureState, final boolean b) {
        if (b && panelFeatureState.featureId == 0) {
            final DecorContentParent mDecorContentParent = this.mDecorContentParent;
            if (mDecorContentParent != null && mDecorContentParent.isOverflowMenuShowing()) {
                this.checkCloseActionMenu(panelFeatureState.menu);
                return;
            }
        }
        final WindowManager windowManager = (WindowManager)this.mContext.getSystemService("window");
        if (windowManager != null && panelFeatureState.isOpen) {
            final ViewGroup decorView = panelFeatureState.decorView;
            if (decorView != null) {
                ((ViewManager)windowManager).removeView((View)decorView);
                if (b) {
                    this.callOnPanelClosed(panelFeatureState.featureId, panelFeatureState, null);
                }
            }
        }
        panelFeatureState.isPrepared = false;
        panelFeatureState.isHandled = false;
        panelFeatureState.isOpen = false;
        panelFeatureState.shownPanelView = null;
        panelFeatureState.refreshDecorView = true;
        if (this.mPreparedPanel == panelFeatureState) {
            this.mPreparedPanel = null;
        }
        if (panelFeatureState.featureId == 0) {
            this.updateBackInvokedCallbackState();
        }
    }
    
    @Override
    public View createView(final View view, final String s, @NonNull final Context context, @NonNull final AttributeSet set) {
        final AppCompatViewInflater mAppCompatViewInflater = this.mAppCompatViewInflater;
        boolean shouldInheritContext = false;
        if (mAppCompatViewInflater == null) {
            final String string = this.mContext.obtainStyledAttributes(R.styleable.AppCompatTheme).getString(R.styleable.AppCompatTheme_viewInflaterClass);
            if (string == null) {
                this.mAppCompatViewInflater = new AppCompatViewInflater();
            }
            else {
                try {
                    this.mAppCompatViewInflater = (AppCompatViewInflater)this.mContext.getClassLoader().loadClass(string).getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
                }
                finally {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to instantiate custom view inflater ");
                    sb.append(string);
                    sb.append(". Falling back to default.");
                    this.mAppCompatViewInflater = new AppCompatViewInflater();
                }
            }
        }
        final boolean is_PRE_LOLLIPOP = AppCompatDelegateImpl.IS_PRE_LOLLIPOP;
        if (is_PRE_LOLLIPOP) {
            if (this.mLayoutIncludeDetector == null) {
                this.mLayoutIncludeDetector = new LayoutIncludeDetector();
            }
            if (this.mLayoutIncludeDetector.detect(set)) {
                shouldInheritContext = true;
            }
            else if (set instanceof XmlPullParser) {
                if (((XmlPullParser)set).getDepth() > 1) {
                    shouldInheritContext = true;
                }
            }
            else {
                shouldInheritContext = this.shouldInheritContext((ViewParent)view);
            }
        }
        else {
            shouldInheritContext = false;
        }
        return this.mAppCompatViewInflater.createView(view, s, context, set, shouldInheritContext, is_PRE_LOLLIPOP, true, VectorEnabledTintResources.shouldBeUsed());
    }
    
    void dismissPopups() {
        final DecorContentParent mDecorContentParent = this.mDecorContentParent;
        if (mDecorContentParent != null) {
            mDecorContentParent.dismissPopups();
        }
        Label_0059: {
            if (this.mActionModePopup == null) {
                break Label_0059;
            }
            this.mWindow.getDecorView().removeCallbacks(this.mShowActionModePopup);
            while (true) {
                if (!this.mActionModePopup.isShowing()) {
                    break Label_0054;
                }
                try {
                    this.mActionModePopup.dismiss();
                    this.mActionModePopup = null;
                    this.endOnGoingFadeAnimation();
                    final PanelFeatureState panelState = this.getPanelState(0, false);
                    if (panelState != null) {
                        final MenuBuilder menu = panelState.menu;
                        if (menu != null) {
                            menu.close();
                        }
                    }
                }
                catch (final IllegalArgumentException ex) {
                    continue;
                }
                break;
            }
        }
    }
    
    boolean dispatchKeyEvent(final KeyEvent keyEvent) {
        final Object mHost = this.mHost;
        final boolean b = mHost instanceof KeyEventDispatcher.Component;
        boolean b2 = true;
        if (b || mHost instanceof AppCompatDialog) {
            final View decorView = this.mWindow.getDecorView();
            if (decorView != null && KeyEventDispatcher.dispatchBeforeHierarchy(decorView, keyEvent)) {
                return true;
            }
        }
        if (keyEvent.getKeyCode() == 82 && this.mAppCompatWindowCallback.bypassDispatchKeyEvent(this.mWindow.getCallback(), keyEvent)) {
            return true;
        }
        final int keyCode = keyEvent.getKeyCode();
        if (keyEvent.getAction() != 0) {
            b2 = false;
        }
        boolean b3;
        if (b2) {
            b3 = this.onKeyDown(keyCode, keyEvent);
        }
        else {
            b3 = this.onKeyUp(keyCode, keyEvent);
        }
        return b3;
    }
    
    void doInvalidatePanelMenu(final int n) {
        final PanelFeatureState panelState = this.getPanelState(n, true);
        if (panelState.menu != null) {
            final Bundle frozenActionViewState = new Bundle();
            panelState.menu.saveActionViewStates(frozenActionViewState);
            if (((BaseBundle)frozenActionViewState).size() > 0) {
                panelState.frozenActionViewState = frozenActionViewState;
            }
            panelState.menu.stopDispatchingItemsChanged();
            panelState.menu.clear();
        }
        panelState.refreshMenuContent = true;
        panelState.refreshDecorView = true;
        if ((n == 108 || n == 0) && this.mDecorContentParent != null) {
            final PanelFeatureState panelState2 = this.getPanelState(0, false);
            if (panelState2 != null) {
                panelState2.isPrepared = false;
                this.preparePanel(panelState2, null);
            }
        }
    }
    
    void endOnGoingFadeAnimation() {
        final ViewPropertyAnimatorCompat mFadeAnim = this.mFadeAnim;
        if (mFadeAnim != null) {
            mFadeAnim.cancel();
        }
    }
    
    PanelFeatureState findMenuPanel(final Menu menu) {
        final PanelFeatureState[] mPanels = this.mPanels;
        int i = 0;
        int length;
        if (mPanels != null) {
            length = mPanels.length;
        }
        else {
            length = 0;
        }
        while (i < length) {
            final PanelFeatureState panelFeatureState = mPanels[i];
            if (panelFeatureState != null && panelFeatureState.menu == menu) {
                return panelFeatureState;
            }
            ++i;
        }
        return null;
    }
    
    @Nullable
    @Override
    public <T extends View> T findViewById(@IdRes final int n) {
        this.ensureSubDecor();
        return (T)this.mWindow.findViewById(n);
    }
    
    final Context getActionBarThemedContext() {
        final ActionBar supportActionBar = this.getSupportActionBar();
        Context themedContext;
        if (supportActionBar != null) {
            themedContext = supportActionBar.getThemedContext();
        }
        else {
            themedContext = null;
        }
        Context mContext = themedContext;
        if (themedContext == null) {
            mContext = this.mContext;
        }
        return mContext;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    @VisibleForTesting
    final AutoNightModeManager getAutoTimeNightModeManager() {
        return this.getAutoTimeNightModeManager(this.mContext);
    }
    
    LocaleListCompat getConfigurationLocales(final Configuration configuration) {
        if (Build$VERSION.SDK_INT >= 24) {
            return Api24Impl.getLocales(configuration);
        }
        return LocaleListCompat.forLanguageTags(Api21Impl.toLanguageTag(configuration.locale));
    }
    
    @Override
    public Context getContextForDelegate() {
        return this.mContext;
    }
    
    @Override
    public final ActionBarDrawerToggle.Delegate getDrawerToggleDelegate() {
        return new ActionBarDrawableToggleImpl();
    }
    
    @Override
    public int getLocalNightMode() {
        return this.mLocalNightMode;
    }
    
    @Override
    public MenuInflater getMenuInflater() {
        if (this.mMenuInflater == null) {
            this.initWindowDecorActionBar();
            final ActionBar mActionBar = this.mActionBar;
            Context context;
            if (mActionBar != null) {
                context = mActionBar.getThemedContext();
            }
            else {
                context = this.mContext;
            }
            this.mMenuInflater = new SupportMenuInflater(context);
        }
        return this.mMenuInflater;
    }
    
    protected PanelFeatureState getPanelState(final int n, final boolean b) {
        final PanelFeatureState[] mPanels = this.mPanels;
        PanelFeatureState[] mPanels2 = null;
        Label_0049: {
            if (mPanels != null) {
                mPanels2 = mPanels;
                if (mPanels.length > n) {
                    break Label_0049;
                }
            }
            mPanels2 = new PanelFeatureState[n + 1];
            if (mPanels != null) {
                System.arraycopy(mPanels, 0, mPanels2, 0, mPanels.length);
            }
            this.mPanels = mPanels2;
        }
        PanelFeatureState panelFeatureState;
        if ((panelFeatureState = mPanels2[n]) == null) {
            panelFeatureState = new PanelFeatureState(n);
            mPanels2[n] = panelFeatureState;
        }
        return panelFeatureState;
    }
    
    ViewGroup getSubDecor() {
        return this.mSubDecor;
    }
    
    @Override
    public ActionBar getSupportActionBar() {
        this.initWindowDecorActionBar();
        return this.mActionBar;
    }
    
    final CharSequence getTitle() {
        final Object mHost = this.mHost;
        if (mHost instanceof Activity) {
            return ((Activity)mHost).getTitle();
        }
        return this.mTitle;
    }
    
    final Window$Callback getWindowCallback() {
        return this.mWindow.getCallback();
    }
    
    @Override
    public boolean hasWindowFeature(final int n) {
        final int sanitizeWindowFeatureId = this.sanitizeWindowFeatureId(n);
        final boolean b = true;
        boolean b2;
        if (sanitizeWindowFeatureId != 1) {
            if (sanitizeWindowFeatureId != 2) {
                if (sanitizeWindowFeatureId != 5) {
                    if (sanitizeWindowFeatureId != 10) {
                        if (sanitizeWindowFeatureId != 108) {
                            b2 = (sanitizeWindowFeatureId == 109 && this.mOverlayActionBar);
                        }
                        else {
                            b2 = this.mHasActionBar;
                        }
                    }
                    else {
                        b2 = this.mOverlayActionMode;
                    }
                }
                else {
                    b2 = this.mFeatureIndeterminateProgress;
                }
            }
            else {
                b2 = this.mFeatureProgress;
            }
        }
        else {
            b2 = this.mWindowNoTitle;
        }
        boolean b3 = b;
        if (!b2) {
            b3 = (this.mWindow.hasFeature(n) && b);
        }
        return b3;
    }
    
    @Override
    public void installViewFactory() {
        final LayoutInflater from = LayoutInflater.from(this.mContext);
        if (from.getFactory() == null) {
            LayoutInflaterCompat.setFactory2(from, (LayoutInflater$Factory2)this);
        }
        else {
            final boolean b = from.getFactory2() instanceof AppCompatDelegateImpl;
        }
    }
    
    @Override
    public void invalidateOptionsMenu() {
        if (this.peekSupportActionBar() != null) {
            if (!this.getSupportActionBar().invalidateOptionsMenu()) {
                this.invalidatePanelMenu(0);
            }
        }
    }
    
    @Override
    public boolean isHandleNativeActionModesEnabled() {
        return this.mHandleNativeActionModes;
    }
    
    int mapNightMode(@NonNull final Context context, final int n) {
        if (n != -100) {
            if (n != -1) {
                if (n != 0) {
                    if (n != 1 && n != 2) {
                        if (n == 3) {
                            return this.getAutoBatteryNightModeManager(context).getApplyableNightMode();
                        }
                        throw new IllegalStateException("Unknown value set for night mode. Please use one of the MODE_NIGHT values from AppCompatDelegate.");
                    }
                }
                else {
                    if (Build$VERSION.SDK_INT >= 23 && ((UiModeManager)context.getApplicationContext().getSystemService("uimode")).getNightMode() == 0) {
                        return -1;
                    }
                    return this.getAutoTimeNightModeManager(context).getApplyableNightMode();
                }
            }
            return n;
        }
        return -1;
    }
    
    boolean onBackPressed() {
        final boolean mLongPressBackDown = this.mLongPressBackDown;
        this.mLongPressBackDown = false;
        final PanelFeatureState panelState = this.getPanelState(0, false);
        if (panelState != null && panelState.isOpen) {
            if (!mLongPressBackDown) {
                this.closePanel(panelState, true);
            }
            return true;
        }
        final ActionMode mActionMode = this.mActionMode;
        if (mActionMode != null) {
            mActionMode.finish();
            return true;
        }
        final ActionBar supportActionBar = this.getSupportActionBar();
        return supportActionBar != null && supportActionBar.collapseActionView();
    }
    
    @Override
    public void onConfigurationChanged(final Configuration configuration) {
        if (this.mHasActionBar && this.mSubDecorInstalled) {
            final ActionBar supportActionBar = this.getSupportActionBar();
            if (supportActionBar != null) {
                supportActionBar.onConfigurationChanged(configuration);
            }
        }
        AppCompatDrawableManager.get().onConfigurationChanged(this.mContext);
        this.mEffectiveConfiguration = new Configuration(this.mContext.getResources().getConfiguration());
        this.applyApplicationSpecificConfig(false, false);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        this.mBaseContextAttached = true;
        this.applyApplicationSpecificConfig(false);
        this.ensureWindow();
        final Object mHost = this.mHost;
        if (mHost instanceof Activity) {
            String parentActivityName;
            try {
                parentActivityName = NavUtils.getParentActivityName((Activity)mHost);
            }
            catch (final IllegalArgumentException ex) {
                parentActivityName = null;
            }
            if (parentActivityName != null) {
                final ActionBar peekSupportActionBar = this.peekSupportActionBar();
                if (peekSupportActionBar == null) {
                    this.mEnableDefaultActionBarUp = true;
                }
                else {
                    peekSupportActionBar.setDefaultDisplayHomeAsUpEnabled(true);
                }
            }
            AppCompatDelegate.addActiveDelegate(this);
        }
        this.mEffectiveConfiguration = new Configuration(this.mContext.getResources().getConfiguration());
        this.mCreated = true;
    }
    
    public final View onCreateView(final View view, final String s, final Context context, final AttributeSet set) {
        return this.createView(view, s, context, set);
    }
    
    public View onCreateView(final String s, final Context context, final AttributeSet set) {
        return this.onCreateView(null, s, context, set);
    }
    
    @Override
    public void onDestroy() {
        if (this.mHost instanceof Activity) {
            AppCompatDelegate.removeActivityDelegate(this);
        }
        if (this.mInvalidatePanelMenuPosted) {
            this.mWindow.getDecorView().removeCallbacks(this.mInvalidatePanelMenuRunnable);
        }
        this.mDestroyed = true;
        Label_0116: {
            if (this.mLocalNightMode != -100) {
                final Object mHost = this.mHost;
                if (mHost instanceof Activity && ((Activity)mHost).isChangingConfigurations()) {
                    AppCompatDelegateImpl.sLocalNightModes.put(this.mHost.getClass().getName(), this.mLocalNightMode);
                    break Label_0116;
                }
            }
            AppCompatDelegateImpl.sLocalNightModes.remove(this.mHost.getClass().getName());
        }
        final ActionBar mActionBar = this.mActionBar;
        if (mActionBar != null) {
            mActionBar.onDestroy();
        }
        this.cleanupAutoManagers();
    }
    
    boolean onKeyDown(final int n, final KeyEvent keyEvent) {
        boolean mLongPressBackDown = true;
        if (n != 4) {
            if (n == 82) {
                this.onKeyDownPanel(0, keyEvent);
                return true;
            }
        }
        else {
            if ((keyEvent.getFlags() & 0x80) == 0x0) {
                mLongPressBackDown = false;
            }
            this.mLongPressBackDown = mLongPressBackDown;
        }
        return false;
    }
    
    boolean onKeyShortcut(final int n, final KeyEvent keyEvent) {
        final ActionBar supportActionBar = this.getSupportActionBar();
        if (supportActionBar != null && supportActionBar.onKeyShortcut(n, keyEvent)) {
            return true;
        }
        final PanelFeatureState mPreparedPanel = this.mPreparedPanel;
        if (mPreparedPanel != null && this.performPanelShortcut(mPreparedPanel, keyEvent.getKeyCode(), keyEvent, 1)) {
            final PanelFeatureState mPreparedPanel2 = this.mPreparedPanel;
            if (mPreparedPanel2 != null) {
                mPreparedPanel2.isHandled = true;
            }
            return true;
        }
        if (this.mPreparedPanel == null) {
            final PanelFeatureState panelState = this.getPanelState(0, true);
            this.preparePanel(panelState, keyEvent);
            final boolean performPanelShortcut = this.performPanelShortcut(panelState, keyEvent.getKeyCode(), keyEvent, 1);
            panelState.isPrepared = false;
            if (performPanelShortcut) {
                return true;
            }
        }
        return false;
    }
    
    boolean onKeyUp(final int n, final KeyEvent keyEvent) {
        if (n != 4) {
            if (n == 82) {
                this.onKeyUpPanel(0, keyEvent);
                return true;
            }
        }
        else if (this.onBackPressed()) {
            return true;
        }
        return false;
    }
    
    @Override
    public boolean onMenuItemSelected(@NonNull final MenuBuilder menuBuilder, @NonNull final MenuItem menuItem) {
        final Window$Callback windowCallback = this.getWindowCallback();
        if (windowCallback != null && !this.mDestroyed) {
            final PanelFeatureState menuPanel = this.findMenuPanel((Menu)menuBuilder.getRootMenu());
            if (menuPanel != null) {
                return windowCallback.onMenuItemSelected(menuPanel.featureId, menuItem);
            }
        }
        return false;
    }
    
    @Override
    public void onMenuModeChange(@NonNull final MenuBuilder menuBuilder) {
        this.reopenMenu(true);
    }
    
    void onMenuOpened(final int n) {
        if (n == 108) {
            final ActionBar supportActionBar = this.getSupportActionBar();
            if (supportActionBar != null) {
                supportActionBar.dispatchMenuVisibilityChanged(true);
            }
        }
    }
    
    void onPanelClosed(final int n) {
        if (n == 108) {
            final ActionBar supportActionBar = this.getSupportActionBar();
            if (supportActionBar != null) {
                supportActionBar.dispatchMenuVisibilityChanged(false);
            }
        }
        else if (n == 0) {
            final PanelFeatureState panelState = this.getPanelState(n, true);
            if (panelState.isOpen) {
                this.closePanel(panelState, false);
            }
        }
    }
    
    @Override
    public void onPostCreate(final Bundle bundle) {
        this.ensureSubDecor();
    }
    
    @Override
    public void onPostResume() {
        final ActionBar supportActionBar = this.getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setShowHideAnimationEnabled(true);
        }
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
    }
    
    @Override
    public void onStart() {
        this.applyApplicationSpecificConfig(true, false);
    }
    
    @Override
    public void onStop() {
        final ActionBar supportActionBar = this.getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setShowHideAnimationEnabled(false);
        }
    }
    
    void onSubDecorInstalled(final ViewGroup viewGroup) {
    }
    
    final ActionBar peekSupportActionBar() {
        return this.mActionBar;
    }
    
    @Override
    public boolean requestWindowFeature(int sanitizeWindowFeatureId) {
        sanitizeWindowFeatureId = this.sanitizeWindowFeatureId(sanitizeWindowFeatureId);
        if (this.mWindowNoTitle && sanitizeWindowFeatureId == 108) {
            return false;
        }
        if (this.mHasActionBar && sanitizeWindowFeatureId == 1) {
            this.mHasActionBar = false;
        }
        if (sanitizeWindowFeatureId == 1) {
            this.throwFeatureRequestIfSubDecorInstalled();
            return this.mWindowNoTitle = true;
        }
        if (sanitizeWindowFeatureId == 2) {
            this.throwFeatureRequestIfSubDecorInstalled();
            return this.mFeatureProgress = true;
        }
        if (sanitizeWindowFeatureId == 5) {
            this.throwFeatureRequestIfSubDecorInstalled();
            return this.mFeatureIndeterminateProgress = true;
        }
        if (sanitizeWindowFeatureId == 10) {
            this.throwFeatureRequestIfSubDecorInstalled();
            return this.mOverlayActionMode = true;
        }
        if (sanitizeWindowFeatureId == 108) {
            this.throwFeatureRequestIfSubDecorInstalled();
            return this.mHasActionBar = true;
        }
        if (sanitizeWindowFeatureId != 109) {
            return this.mWindow.requestFeature(sanitizeWindowFeatureId);
        }
        this.throwFeatureRequestIfSubDecorInstalled();
        return this.mOverlayActionBar = true;
    }
    
    void setConfigurationLocales(final Configuration configuration, @NonNull final LocaleListCompat localeListCompat) {
        if (Build$VERSION.SDK_INT >= 24) {
            Api24Impl.setLocales(configuration, localeListCompat);
        }
        else {
            Api17Impl.setLocale(configuration, localeListCompat.get(0));
            Api17Impl.setLayoutDirection(configuration, localeListCompat.get(0));
        }
    }
    
    @Override
    public void setContentView(final int n) {
        this.ensureSubDecor();
        final ViewGroup viewGroup = (ViewGroup)((View)this.mSubDecor).findViewById(16908290);
        viewGroup.removeAllViews();
        LayoutInflater.from(this.mContext).inflate(n, viewGroup);
        this.mAppCompatWindowCallback.bypassOnContentChanged(this.mWindow.getCallback());
    }
    
    @Override
    public void setContentView(final View view) {
        this.ensureSubDecor();
        final ViewGroup viewGroup = (ViewGroup)((View)this.mSubDecor).findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
        this.mAppCompatWindowCallback.bypassOnContentChanged(this.mWindow.getCallback());
    }
    
    @Override
    public void setContentView(final View view, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        this.ensureSubDecor();
        final ViewGroup viewGroup = (ViewGroup)((View)this.mSubDecor).findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view, viewGroup$LayoutParams);
        this.mAppCompatWindowCallback.bypassOnContentChanged(this.mWindow.getCallback());
    }
    
    void setDefaultLocalesForLocaleList(final LocaleListCompat defaultLocales) {
        if (Build$VERSION.SDK_INT >= 24) {
            Api24Impl.setDefaultLocales(defaultLocales);
        }
        else {
            Locale.setDefault(defaultLocales.get(0));
        }
    }
    
    @Override
    public void setHandleNativeActionModesEnabled(final boolean mHandleNativeActionModes) {
        this.mHandleNativeActionModes = mHandleNativeActionModes;
    }
    
    @RequiresApi(17)
    @Override
    public void setLocalNightMode(final int mLocalNightMode) {
        if (this.mLocalNightMode != mLocalNightMode) {
            this.mLocalNightMode = mLocalNightMode;
            if (this.mBaseContextAttached) {
                this.applyDayNight();
            }
        }
    }
    
    @RequiresApi(33)
    @Override
    public void setOnBackInvokedDispatcher(@Nullable final OnBackInvokedDispatcher onBackInvokedDispatcher) {
        super.setOnBackInvokedDispatcher(onBackInvokedDispatcher);
        final OnBackInvokedDispatcher mDispatcher = this.mDispatcher;
        if (mDispatcher != null) {
            final OnBackInvokedCallback mBackCallback = this.mBackCallback;
            if (mBackCallback != null) {
                Api33Impl.unregisterOnBackInvokedCallback(mDispatcher, mBackCallback);
                this.mBackCallback = null;
            }
        }
        Label_0081: {
            if (onBackInvokedDispatcher == null) {
                final Object mHost = this.mHost;
                if (mHost instanceof Activity && ((Activity)mHost).getWindow() != null) {
                    this.mDispatcher = Api33Impl.getOnBackInvokedDispatcher((Activity)this.mHost);
                    break Label_0081;
                }
            }
            this.mDispatcher = onBackInvokedDispatcher;
        }
        this.updateBackInvokedCallbackState();
    }
    
    @Override
    public void setSupportActionBar(final Toolbar toolbar) {
        if (!(this.mHost instanceof Activity)) {
            return;
        }
        final ActionBar supportActionBar = this.getSupportActionBar();
        if (!(supportActionBar instanceof WindowDecorActionBar)) {
            this.mMenuInflater = null;
            if (supportActionBar != null) {
                supportActionBar.onDestroy();
            }
            this.mActionBar = null;
            if (toolbar != null) {
                final ToolbarActionBar mActionBar = new ToolbarActionBar(toolbar, this.getTitle(), (Window$Callback)this.mAppCompatWindowCallback);
                this.mActionBar = mActionBar;
                this.mAppCompatWindowCallback.setActionBarCallback(mActionBar.mMenuCallback);
                toolbar.setBackInvokedCallbackEnabled(true);
            }
            else {
                this.mAppCompatWindowCallback.setActionBarCallback(null);
            }
            this.invalidateOptionsMenu();
            return;
        }
        throw new IllegalStateException("This Activity already has an action bar supplied by the window decor. Do not request Window.FEATURE_SUPPORT_ACTION_BAR and set windowActionBar to false in your theme to use a Toolbar instead.");
    }
    
    @Override
    public void setTheme(@StyleRes final int mThemeResId) {
        this.mThemeResId = mThemeResId;
    }
    
    @Override
    public final void setTitle(final CharSequence charSequence) {
        this.mTitle = charSequence;
        final DecorContentParent mDecorContentParent = this.mDecorContentParent;
        if (mDecorContentParent != null) {
            mDecorContentParent.setWindowTitle(charSequence);
        }
        else if (this.peekSupportActionBar() != null) {
            this.peekSupportActionBar().setWindowTitle(charSequence);
        }
        else {
            final TextView mTitleView = this.mTitleView;
            if (mTitleView != null) {
                mTitleView.setText(charSequence);
            }
        }
    }
    
    final boolean shouldAnimateActionModeView() {
        if (this.mSubDecorInstalled) {
            final ViewGroup mSubDecor = this.mSubDecor;
            if (mSubDecor != null && ViewCompat.isLaidOut((View)mSubDecor)) {
                return true;
            }
        }
        return false;
    }
    
    boolean shouldRegisterBackInvokedCallback() {
        if (this.mDispatcher == null) {
            return false;
        }
        final PanelFeatureState panelState = this.getPanelState(0, false);
        return (panelState != null && panelState.isOpen) || this.mActionMode != null;
    }
    
    @Override
    public ActionMode startSupportActionMode(@NonNull final ActionMode.Callback callback) {
        if (callback != null) {
            final ActionMode mActionMode = this.mActionMode;
            if (mActionMode != null) {
                mActionMode.finish();
            }
            final ActionModeCallbackWrapperV9 actionModeCallbackWrapperV9 = new ActionModeCallbackWrapperV9(callback);
            final ActionBar supportActionBar = this.getSupportActionBar();
            if (supportActionBar != null) {
                final ActionMode startActionMode = supportActionBar.startActionMode(actionModeCallbackWrapperV9);
                if ((this.mActionMode = startActionMode) != null) {
                    final AppCompatCallback mAppCompatCallback = this.mAppCompatCallback;
                    if (mAppCompatCallback != null) {
                        mAppCompatCallback.onSupportActionModeStarted(startActionMode);
                    }
                }
            }
            if (this.mActionMode == null) {
                this.mActionMode = this.startSupportActionModeFromWindow(actionModeCallbackWrapperV9);
            }
            this.updateBackInvokedCallbackState();
            return this.mActionMode;
        }
        throw new IllegalArgumentException("ActionMode callback can not be null.");
    }
    
    ActionMode startSupportActionModeFromWindow(@NonNull final ActionMode.Callback callback) {
        this.endOnGoingFadeAnimation();
        final ActionMode mActionMode = this.mActionMode;
        if (mActionMode != null) {
            mActionMode.finish();
        }
        ActionMode.Callback callback2 = callback;
        if (!(callback instanceof ActionModeCallbackWrapperV9)) {
            callback2 = new ActionModeCallbackWrapperV9(callback);
        }
        final AppCompatCallback mAppCompatCallback = this.mAppCompatCallback;
        ActionMode onWindowStartingSupportActionMode = null;
        Label_0072: {
            if (mAppCompatCallback != null && !this.mDestroyed) {
                try {
                    onWindowStartingSupportActionMode = mAppCompatCallback.onWindowStartingSupportActionMode(callback2);
                    break Label_0072;
                }
                catch (final AbstractMethodError abstractMethodError) {}
            }
            onWindowStartingSupportActionMode = null;
        }
        if (onWindowStartingSupportActionMode != null) {
            this.mActionMode = onWindowStartingSupportActionMode;
        }
        else {
            final ActionBarContextView mActionModeView = this.mActionModeView;
            boolean b = true;
            if (mActionModeView == null) {
                if (this.mIsFloating) {
                    final TypedValue typedValue = new TypedValue();
                    final Resources$Theme theme = this.mContext.getTheme();
                    theme.resolveAttribute(R.attr.actionBarTheme, typedValue, true);
                    Object mContext;
                    if (typedValue.resourceId != 0) {
                        final Resources$Theme theme2 = this.mContext.getResources().newTheme();
                        theme2.setTo(theme);
                        theme2.applyStyle(typedValue.resourceId, true);
                        mContext = new ContextThemeWrapper(this.mContext, 0);
                        ((Context)mContext).getTheme().setTo(theme2);
                    }
                    else {
                        mContext = this.mContext;
                    }
                    this.mActionModeView = new ActionBarContextView((Context)mContext);
                    PopupWindowCompat.setWindowLayoutType(this.mActionModePopup = new PopupWindow((Context)mContext, (AttributeSet)null, R.attr.actionModePopupWindowStyle), 2);
                    this.mActionModePopup.setContentView((View)this.mActionModeView);
                    this.mActionModePopup.setWidth(-1);
                    ((Context)mContext).getTheme().resolveAttribute(R.attr.actionBarSize, typedValue, true);
                    this.mActionModeView.setContentHeight(TypedValue.complexToDimensionPixelSize(typedValue.data, ((Context)mContext).getResources().getDisplayMetrics()));
                    this.mActionModePopup.setHeight(-2);
                    this.mShowActionModePopup = new Runnable(this) {
                        final AppCompatDelegateImpl this$0;
                        
                        @Override
                        public void run() {
                            final AppCompatDelegateImpl this$0 = this.this$0;
                            this$0.mActionModePopup.showAtLocation((View)this$0.mActionModeView, 55, 0, 0);
                            this.this$0.endOnGoingFadeAnimation();
                            if (this.this$0.shouldAnimateActionModeView()) {
                                ((View)this.this$0.mActionModeView).setAlpha(0.0f);
                                final AppCompatDelegateImpl this$2 = this.this$0;
                                this$2.mFadeAnim = ViewCompat.animate((View)this$2.mActionModeView).alpha(1.0f);
                                this.this$0.mFadeAnim.setListener(new ViewPropertyAnimatorListenerAdapter(this) {
                                    final AppCompatDelegateImpl$6 this$1;
                                    
                                    @Override
                                    public void onAnimationEnd(final View view) {
                                        ((View)this.this$1.this$0.mActionModeView).setAlpha(1.0f);
                                        this.this$1.this$0.mFadeAnim.setListener(null);
                                        this.this$1.this$0.mFadeAnim = null;
                                    }
                                    
                                    @Override
                                    public void onAnimationStart(final View view) {
                                        this.this$1.this$0.mActionModeView.setVisibility(0);
                                    }
                                });
                            }
                            else {
                                ((View)this.this$0.mActionModeView).setAlpha(1.0f);
                                this.this$0.mActionModeView.setVisibility(0);
                            }
                        }
                    };
                }
                else {
                    final ViewStubCompat viewStubCompat = (ViewStubCompat)((View)this.mSubDecor).findViewById(R.id.action_mode_bar_stub);
                    if (viewStubCompat != null) {
                        viewStubCompat.setLayoutInflater(LayoutInflater.from(this.getActionBarThemedContext()));
                        this.mActionModeView = (ActionBarContextView)viewStubCompat.inflate();
                    }
                }
            }
            if (this.mActionModeView != null) {
                this.endOnGoingFadeAnimation();
                this.mActionModeView.killMode();
                final Context context = ((View)this.mActionModeView).getContext();
                final ActionBarContextView mActionModeView2 = this.mActionModeView;
                if (this.mActionModePopup != null) {
                    b = false;
                }
                final StandaloneActionMode mActionMode2 = new StandaloneActionMode(context, mActionModeView2, callback2, b);
                if (callback2.onCreateActionMode(mActionMode2, mActionMode2.getMenu())) {
                    mActionMode2.invalidate();
                    this.mActionModeView.initForMode(mActionMode2);
                    this.mActionMode = mActionMode2;
                    if (this.shouldAnimateActionModeView()) {
                        ((View)this.mActionModeView).setAlpha(0.0f);
                        (this.mFadeAnim = ViewCompat.animate((View)this.mActionModeView).alpha(1.0f)).setListener(new ViewPropertyAnimatorListenerAdapter(this) {
                            final AppCompatDelegateImpl this$0;
                            
                            @Override
                            public void onAnimationEnd(final View view) {
                                ((View)this.this$0.mActionModeView).setAlpha(1.0f);
                                this.this$0.mFadeAnim.setListener(null);
                                this.this$0.mFadeAnim = null;
                            }
                            
                            @Override
                            public void onAnimationStart(final View view) {
                                this.this$0.mActionModeView.setVisibility(0);
                                if (((View)this.this$0.mActionModeView).getParent() instanceof View) {
                                    ViewCompat.requestApplyInsets((View)((View)this.this$0.mActionModeView).getParent());
                                }
                            }
                        });
                    }
                    else {
                        ((View)this.mActionModeView).setAlpha(1.0f);
                        this.mActionModeView.setVisibility(0);
                        if (((View)this.mActionModeView).getParent() instanceof View) {
                            ViewCompat.requestApplyInsets((View)((View)this.mActionModeView).getParent());
                        }
                    }
                    if (this.mActionModePopup != null) {
                        this.mWindow.getDecorView().post(this.mShowActionModePopup);
                    }
                }
                else {
                    this.mActionMode = null;
                }
            }
        }
        final ActionMode mActionMode3 = this.mActionMode;
        if (mActionMode3 != null) {
            final AppCompatCallback mAppCompatCallback2 = this.mAppCompatCallback;
            if (mAppCompatCallback2 != null) {
                mAppCompatCallback2.onSupportActionModeStarted(mActionMode3);
            }
        }
        this.updateBackInvokedCallbackState();
        return this.mActionMode;
    }
    
    void updateBackInvokedCallbackState() {
        if (Build$VERSION.SDK_INT >= 33) {
            final boolean shouldRegisterBackInvokedCallback = this.shouldRegisterBackInvokedCallback();
            if (shouldRegisterBackInvokedCallback && this.mBackCallback == null) {
                this.mBackCallback = Api33Impl.registerOnBackPressedCallback(this.mDispatcher, this);
            }
            else if (!shouldRegisterBackInvokedCallback) {
                final OnBackInvokedCallback mBackCallback = this.mBackCallback;
                if (mBackCallback != null) {
                    Api33Impl.unregisterOnBackInvokedCallback(this.mDispatcher, mBackCallback);
                }
            }
        }
    }
    
    final int updateStatusGuard(@Nullable WindowInsetsCompat rootWindowInsets, @Nullable final Rect rect) {
        final int n = 0;
        int n2;
        if (rootWindowInsets != null) {
            n2 = rootWindowInsets.getSystemWindowInsetTop();
        }
        else if (rect != null) {
            n2 = rect.top;
        }
        else {
            n2 = 0;
        }
        final ActionBarContextView mActionModeView = this.mActionModeView;
        int n9;
        int n10;
        if (mActionModeView != null && ((View)mActionModeView).getLayoutParams() instanceof ViewGroup$MarginLayoutParams) {
            final ViewGroup$MarginLayoutParams layoutParams = (ViewGroup$MarginLayoutParams)((View)this.mActionModeView).getLayoutParams();
            final boolean shown = ((View)this.mActionModeView).isShown();
            int n3 = 1;
            final int n4 = 1;
            int n8;
            if (shown) {
                if (this.mTempRect1 == null) {
                    this.mTempRect1 = new Rect();
                    this.mTempRect2 = new Rect();
                }
                final Rect mTempRect1 = this.mTempRect1;
                final Rect mTempRect2 = this.mTempRect2;
                if (rootWindowInsets == null) {
                    mTempRect1.set(rect);
                }
                else {
                    mTempRect1.set(rootWindowInsets.getSystemWindowInsetLeft(), rootWindowInsets.getSystemWindowInsetTop(), rootWindowInsets.getSystemWindowInsetRight(), rootWindowInsets.getSystemWindowInsetBottom());
                }
                ViewUtils.computeFitSystemWindows((View)this.mSubDecor, mTempRect1, mTempRect2);
                final int top = mTempRect1.top;
                final int left = mTempRect1.left;
                final int right = mTempRect1.right;
                rootWindowInsets = ViewCompat.getRootWindowInsets((View)this.mSubDecor);
                int systemWindowInsetLeft;
                if (rootWindowInsets == null) {
                    systemWindowInsetLeft = 0;
                }
                else {
                    systemWindowInsetLeft = rootWindowInsets.getSystemWindowInsetLeft();
                }
                int systemWindowInsetRight;
                if (rootWindowInsets == null) {
                    systemWindowInsetRight = 0;
                }
                else {
                    systemWindowInsetRight = rootWindowInsets.getSystemWindowInsetRight();
                }
                int n5;
                if (layoutParams.topMargin == top && layoutParams.leftMargin == left && layoutParams.rightMargin == right) {
                    n5 = 0;
                }
                else {
                    layoutParams.topMargin = top;
                    layoutParams.leftMargin = left;
                    layoutParams.rightMargin = right;
                    n5 = 1;
                }
                if (top > 0 && this.mStatusGuard == null) {
                    (this.mStatusGuard = new View(this.mContext)).setVisibility(8);
                    final FrameLayout$LayoutParams frameLayout$LayoutParams = new FrameLayout$LayoutParams(-1, layoutParams.topMargin, 51);
                    frameLayout$LayoutParams.leftMargin = systemWindowInsetLeft;
                    frameLayout$LayoutParams.rightMargin = systemWindowInsetRight;
                    this.mSubDecor.addView(this.mStatusGuard, -1, (ViewGroup$LayoutParams)frameLayout$LayoutParams);
                }
                else {
                    final View mStatusGuard = this.mStatusGuard;
                    if (mStatusGuard != null) {
                        final ViewGroup$MarginLayoutParams layoutParams2 = (ViewGroup$MarginLayoutParams)mStatusGuard.getLayoutParams();
                        final int height = layoutParams2.height;
                        final int topMargin = layoutParams.topMargin;
                        if (height != topMargin || layoutParams2.leftMargin != systemWindowInsetLeft || layoutParams2.rightMargin != systemWindowInsetRight) {
                            layoutParams2.height = topMargin;
                            layoutParams2.leftMargin = systemWindowInsetLeft;
                            layoutParams2.rightMargin = systemWindowInsetRight;
                            this.mStatusGuard.setLayoutParams((ViewGroup$LayoutParams)layoutParams2);
                        }
                    }
                }
                final View mStatusGuard2 = this.mStatusGuard;
                int n6;
                if (mStatusGuard2 != null) {
                    n6 = n4;
                }
                else {
                    n6 = 0;
                }
                if (n6 != 0 && mStatusGuard2.getVisibility() != 0) {
                    this.updateStatusGuardColor(this.mStatusGuard);
                }
                int n7 = n2;
                if (!this.mOverlayActionMode) {
                    n7 = n2;
                    if (n6 != 0) {
                        n7 = 0;
                    }
                }
                n2 = n7;
                n3 = n5;
                n8 = n6;
            }
            else if (layoutParams.topMargin != 0) {
                layoutParams.topMargin = 0;
                n8 = 0;
            }
            else {
                n8 = 0;
                n3 = 0;
            }
            n9 = n2;
            n10 = n8;
            if (n3 != 0) {
                ((View)this.mActionModeView).setLayoutParams((ViewGroup$LayoutParams)layoutParams);
                n9 = n2;
                n10 = n8;
            }
        }
        else {
            n10 = 0;
            n9 = n2;
        }
        final View mStatusGuard3 = this.mStatusGuard;
        if (mStatusGuard3 != null) {
            int visibility;
            if (n10 != 0) {
                visibility = n;
            }
            else {
                visibility = 8;
            }
            mStatusGuard3.setVisibility(visibility);
        }
        return n9;
    }
    
    private class ActionBarDrawableToggleImpl implements Delegate
    {
        final AppCompatDelegateImpl this$0;
        
        ActionBarDrawableToggleImpl(final AppCompatDelegateImpl this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public Context getActionBarThemedContext() {
            return this.this$0.getActionBarThemedContext();
        }
        
        @Override
        public Drawable getThemeUpIndicator() {
            final TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(this.getActionBarThemedContext(), null, new int[] { R.attr.homeAsUpIndicator });
            final Drawable drawable = obtainStyledAttributes.getDrawable(0);
            obtainStyledAttributes.recycle();
            return drawable;
        }
        
        @Override
        public boolean isNavigationVisible() {
            final ActionBar supportActionBar = this.this$0.getSupportActionBar();
            return supportActionBar != null && (supportActionBar.getDisplayOptions() & 0x4) != 0x0;
        }
        
        @Override
        public void setActionBarDescription(final int homeActionContentDescription) {
            final ActionBar supportActionBar = this.this$0.getSupportActionBar();
            if (supportActionBar != null) {
                supportActionBar.setHomeActionContentDescription(homeActionContentDescription);
            }
        }
        
        @Override
        public void setActionBarUpIndicator(final Drawable homeAsUpIndicator, final int homeActionContentDescription) {
            final ActionBar supportActionBar = this.this$0.getSupportActionBar();
            if (supportActionBar != null) {
                supportActionBar.setHomeAsUpIndicator(homeAsUpIndicator);
                supportActionBar.setHomeActionContentDescription(homeActionContentDescription);
            }
        }
    }
    
    interface ActionBarMenuCallback
    {
        @Nullable
        View onCreatePanelView(final int p0);
        
        boolean onPreparePanel(final int p0);
    }
    
    private final class ActionMenuPresenterCallback implements MenuPresenter.Callback
    {
        final AppCompatDelegateImpl this$0;
        
        ActionMenuPresenterCallback(final AppCompatDelegateImpl this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public void onCloseMenu(@NonNull final MenuBuilder menuBuilder, final boolean b) {
            this.this$0.checkCloseActionMenu(menuBuilder);
        }
        
        @Override
        public boolean onOpenSubMenu(@NonNull final MenuBuilder menuBuilder) {
            final Window$Callback windowCallback = this.this$0.getWindowCallback();
            if (windowCallback != null) {
                windowCallback.onMenuOpened(108, (Menu)menuBuilder);
            }
            return true;
        }
    }
    
    class ActionModeCallbackWrapperV9 implements ActionMode.Callback
    {
        private ActionMode.Callback mWrapped;
        final AppCompatDelegateImpl this$0;
        
        public ActionModeCallbackWrapperV9(final AppCompatDelegateImpl this$0, final ActionMode.Callback mWrapped) {
            this.this$0 = this$0;
            this.mWrapped = mWrapped;
        }
        
        @Override
        public boolean onActionItemClicked(final ActionMode actionMode, final MenuItem menuItem) {
            return this.mWrapped.onActionItemClicked(actionMode, menuItem);
        }
        
        @Override
        public boolean onCreateActionMode(final ActionMode actionMode, final Menu menu) {
            return this.mWrapped.onCreateActionMode(actionMode, menu);
        }
        
        @Override
        public void onDestroyActionMode(final ActionMode actionMode) {
            this.mWrapped.onDestroyActionMode(actionMode);
            final AppCompatDelegateImpl this$0 = this.this$0;
            if (this$0.mActionModePopup != null) {
                this$0.mWindow.getDecorView().removeCallbacks(this.this$0.mShowActionModePopup);
            }
            final AppCompatDelegateImpl this$2 = this.this$0;
            if (this$2.mActionModeView != null) {
                this$2.endOnGoingFadeAnimation();
                final AppCompatDelegateImpl this$3 = this.this$0;
                this$3.mFadeAnim = ViewCompat.animate((View)this$3.mActionModeView).alpha(0.0f);
                this.this$0.mFadeAnim.setListener(new ViewPropertyAnimatorListenerAdapter(this) {
                    final ActionModeCallbackWrapperV9 this$1;
                    
                    @Override
                    public void onAnimationEnd(final View view) {
                        this.this$1.this$0.mActionModeView.setVisibility(8);
                        final AppCompatDelegateImpl this$0 = this.this$1.this$0;
                        final PopupWindow mActionModePopup = this$0.mActionModePopup;
                        if (mActionModePopup != null) {
                            mActionModePopup.dismiss();
                        }
                        else if (((View)this$0.mActionModeView).getParent() instanceof View) {
                            ViewCompat.requestApplyInsets((View)((View)this.this$1.this$0.mActionModeView).getParent());
                        }
                        this.this$1.this$0.mActionModeView.killMode();
                        this.this$1.this$0.mFadeAnim.setListener(null);
                        final AppCompatDelegateImpl this$2 = this.this$1.this$0;
                        this$2.mFadeAnim = null;
                        ViewCompat.requestApplyInsets((View)this$2.mSubDecor);
                    }
                });
            }
            final AppCompatDelegateImpl this$4 = this.this$0;
            final AppCompatCallback mAppCompatCallback = this$4.mAppCompatCallback;
            if (mAppCompatCallback != null) {
                mAppCompatCallback.onSupportActionModeFinished(this$4.mActionMode);
            }
            final AppCompatDelegateImpl this$5 = this.this$0;
            this$5.mActionMode = null;
            ViewCompat.requestApplyInsets((View)this$5.mSubDecor);
            this.this$0.updateBackInvokedCallbackState();
        }
        
        @Override
        public boolean onPrepareActionMode(final ActionMode actionMode, final Menu menu) {
            ViewCompat.requestApplyInsets((View)this.this$0.mSubDecor);
            return this.mWrapped.onPrepareActionMode(actionMode, menu);
        }
    }
    
    @RequiresApi(17)
    static class Api17Impl
    {
        private Api17Impl() {
        }
        
        static Context createConfigurationContext(@NonNull final Context context, @NonNull final Configuration configuration) {
            return context.createConfigurationContext(configuration);
        }
        
        static void generateConfigDelta_densityDpi(@NonNull final Configuration configuration, @NonNull final Configuration configuration2, @NonNull final Configuration configuration3) {
            final int densityDpi = configuration.densityDpi;
            final int densityDpi2 = configuration2.densityDpi;
            if (densityDpi != densityDpi2) {
                configuration3.densityDpi = densityDpi2;
            }
        }
        
        @DoNotInline
        static void setLayoutDirection(final Configuration configuration, final Locale layoutDirection) {
            configuration.setLayoutDirection(layoutDirection);
        }
        
        @DoNotInline
        static void setLocale(final Configuration configuration, final Locale locale) {
            configuration.setLocale(locale);
        }
    }
    
    @RequiresApi(21)
    static class Api21Impl
    {
        private Api21Impl() {
        }
        
        static boolean isPowerSaveMode(final PowerManager powerManager) {
            return powerManager.isPowerSaveMode();
        }
        
        @DoNotInline
        static String toLanguageTag(final Locale locale) {
            return locale.toLanguageTag();
        }
    }
    
    @RequiresApi(24)
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        @DoNotInline
        static void generateConfigDelta_locale(@NonNull final Configuration configuration, @NonNull final Configuration configuration2, @NonNull final Configuration configuration3) {
            final LocaleList \u3007080 = Oo08.\u3007080(configuration);
            final LocaleList \u300781 = Oo08.\u3007080(configuration2);
            if (!oO80.\u3007080(\u3007080, (Object)\u300781)) {
                \u3007\u3007888.\u3007080(configuration3, \u300781);
                configuration3.locale = configuration2.locale;
            }
        }
        
        @DoNotInline
        static LocaleListCompat getLocales(final Configuration configuration) {
            return LocaleListCompat.forLanguageTags(o\u30070.\u3007080(Oo08.\u3007080(configuration)));
        }
        
        @DoNotInline
        public static void setDefaultLocales(final LocaleListCompat localeListCompat) {
            O8.\u3007080(\u3007o\u3007.\u3007080(localeListCompat.toLanguageTags()));
        }
        
        @DoNotInline
        static void setLocales(final Configuration configuration, final LocaleListCompat localeListCompat) {
            \u3007\u3007888.\u3007080(configuration, \u3007o\u3007.\u3007080(localeListCompat.toLanguageTags()));
        }
    }
    
    @RequiresApi(26)
    static class Api26Impl
    {
        private Api26Impl() {
        }
        
        static void generateConfigDelta_colorMode(@NonNull final Configuration configuration, @NonNull final Configuration configuration2, @NonNull final Configuration configuration3) {
            if ((\u300780\u3007808\u3007O.\u3007080(configuration) & 0x3) != (\u300780\u3007808\u3007O.\u3007080(configuration2) & 0x3)) {
                OO0o\u3007\u3007\u3007\u30070.\u3007080(configuration3, \u300780\u3007808\u3007O.\u3007080(configuration3) | (\u300780\u3007808\u3007O.\u3007080(configuration2) & 0x3));
            }
            if ((\u300780\u3007808\u3007O.\u3007080(configuration) & 0xC) != (\u300780\u3007808\u3007O.\u3007080(configuration2) & 0xC)) {
                OO0o\u3007\u3007\u3007\u30070.\u3007080(configuration3, \u300780\u3007808\u3007O.\u3007080(configuration3) | (\u300780\u3007808\u3007O.\u3007080(configuration2) & 0xC));
            }
        }
    }
    
    @RequiresApi(33)
    static class Api33Impl
    {
        private Api33Impl() {
        }
        
        @DoNotInline
        static OnBackInvokedDispatcher getOnBackInvokedDispatcher(final Activity activity) {
            return activity.getOnBackInvokedDispatcher();
        }
        
        @DoNotInline
        static OnBackInvokedCallback registerOnBackPressedCallback(final Object o, final AppCompatDelegateImpl obj) {
            Objects.requireNonNull(obj);
            final \u30078o8o\u3007 \u30078o8o\u3007 = new \u30078o8o\u3007(obj);
            ((OnBackInvokedDispatcher)o).registerOnBackInvokedCallback(1000000, (OnBackInvokedCallback)\u30078o8o\u3007);
            return (OnBackInvokedCallback)\u30078o8o\u3007;
        }
        
        @DoNotInline
        static void unregisterOnBackInvokedCallback(final Object o, final Object o2) {
            ((OnBackInvokedDispatcher)o).unregisterOnBackInvokedCallback((OnBackInvokedCallback)o2);
        }
    }
    
    class AppCompatWindowCallback extends WindowCallbackWrapper
    {
        private ActionBarMenuCallback mActionBarCallback;
        private boolean mDispatchKeyEventBypassEnabled;
        private boolean mOnContentChangedBypassEnabled;
        private boolean mOnPanelClosedBypassEnabled;
        final AppCompatDelegateImpl this$0;
        
        AppCompatWindowCallback(final AppCompatDelegateImpl this$0, final Window$Callback window$Callback) {
            this.this$0 = this$0;
            super(window$Callback);
        }
        
        public boolean bypassDispatchKeyEvent(final Window$Callback window$Callback, final KeyEvent keyEvent) {
            try {
                this.mDispatchKeyEventBypassEnabled = true;
                return window$Callback.dispatchKeyEvent(keyEvent);
            }
            finally {
                this.mDispatchKeyEventBypassEnabled = false;
            }
        }
        
        public void bypassOnContentChanged(final Window$Callback window$Callback) {
            try {
                this.mOnContentChangedBypassEnabled = true;
                window$Callback.onContentChanged();
            }
            finally {
                this.mOnContentChangedBypassEnabled = false;
            }
        }
        
        public void bypassOnPanelClosed(final Window$Callback window$Callback, final int n, final Menu menu) {
            try {
                this.mOnPanelClosedBypassEnabled = true;
                window$Callback.onPanelClosed(n, menu);
            }
            finally {
                this.mOnPanelClosedBypassEnabled = false;
            }
        }
        
        @Override
        public boolean dispatchKeyEvent(final KeyEvent keyEvent) {
            if (this.mDispatchKeyEventBypassEnabled) {
                return this.getWrapped().dispatchKeyEvent(keyEvent);
            }
            return this.this$0.dispatchKeyEvent(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }
        
        @Override
        public boolean dispatchKeyShortcutEvent(final KeyEvent keyEvent) {
            return super.dispatchKeyShortcutEvent(keyEvent) || this.this$0.onKeyShortcut(keyEvent.getKeyCode(), keyEvent);
        }
        
        @Override
        public void onContentChanged() {
            if (this.mOnContentChangedBypassEnabled) {
                this.getWrapped().onContentChanged();
            }
        }
        
        @Override
        public boolean onCreatePanelMenu(final int n, final Menu menu) {
            return (n != 0 || menu instanceof MenuBuilder) && super.onCreatePanelMenu(n, menu);
        }
        
        @Override
        public View onCreatePanelView(final int n) {
            final ActionBarMenuCallback mActionBarCallback = this.mActionBarCallback;
            if (mActionBarCallback != null) {
                final View onCreatePanelView = mActionBarCallback.onCreatePanelView(n);
                if (onCreatePanelView != null) {
                    return onCreatePanelView;
                }
            }
            return super.onCreatePanelView(n);
        }
        
        @Override
        public boolean onMenuOpened(final int n, final Menu menu) {
            super.onMenuOpened(n, menu);
            this.this$0.onMenuOpened(n);
            return true;
        }
        
        @Override
        public void onPanelClosed(final int n, final Menu menu) {
            if (this.mOnPanelClosedBypassEnabled) {
                this.getWrapped().onPanelClosed(n, menu);
                return;
            }
            super.onPanelClosed(n, menu);
            this.this$0.onPanelClosed(n);
        }
        
        @Override
        public boolean onPreparePanel(final int n, final View view, final Menu menu) {
            MenuBuilder menuBuilder;
            if (menu instanceof MenuBuilder) {
                menuBuilder = (MenuBuilder)menu;
            }
            else {
                menuBuilder = null;
            }
            if (n == 0 && menuBuilder == null) {
                return false;
            }
            int n2 = 1;
            if (menuBuilder != null) {
                menuBuilder.setOverrideVisibleItems(true);
            }
            final ActionBarMenuCallback mActionBarCallback = this.mActionBarCallback;
            if (mActionBarCallback == null || !mActionBarCallback.onPreparePanel(n)) {
                n2 = 0;
            }
            boolean onPreparePanel = n2 != 0;
            if (n2 == 0) {
                onPreparePanel = super.onPreparePanel(n, view, menu);
            }
            if (menuBuilder != null) {
                menuBuilder.setOverrideVisibleItems(false);
            }
            return onPreparePanel;
        }
        
        @RequiresApi(24)
        @Override
        public void onProvideKeyboardShortcuts(final List<KeyboardShortcutGroup> list, final Menu menu, final int n) {
            final PanelFeatureState panelState = this.this$0.getPanelState(0, true);
            if (panelState != null) {
                final MenuBuilder menu2 = panelState.menu;
                if (menu2 != null) {
                    super.onProvideKeyboardShortcuts(list, (Menu)menu2, n);
                    return;
                }
            }
            super.onProvideKeyboardShortcuts(list, menu, n);
        }
        
        @Override
        public android.view.ActionMode onWindowStartingActionMode(final ActionMode$Callback actionMode$Callback) {
            if (Build$VERSION.SDK_INT >= 23) {
                return null;
            }
            if (this.this$0.isHandleNativeActionModesEnabled()) {
                return this.startAsSupportActionMode(actionMode$Callback);
            }
            return super.onWindowStartingActionMode(actionMode$Callback);
        }
        
        @RequiresApi(23)
        @Override
        public android.view.ActionMode onWindowStartingActionMode(final ActionMode$Callback actionMode$Callback, final int n) {
            if (this.this$0.isHandleNativeActionModesEnabled() && n == 0) {
                return this.startAsSupportActionMode(actionMode$Callback);
            }
            return super.onWindowStartingActionMode(actionMode$Callback, n);
        }
        
        void setActionBarCallback(@Nullable final ActionBarMenuCallback mActionBarCallback) {
            this.mActionBarCallback = mActionBarCallback;
        }
        
        final android.view.ActionMode startAsSupportActionMode(final ActionMode$Callback actionMode$Callback) {
            final SupportActionModeWrapper.CallbackWrapper callbackWrapper = new SupportActionModeWrapper.CallbackWrapper(this.this$0.mContext, actionMode$Callback);
            final ActionMode startSupportActionMode = this.this$0.startSupportActionMode(callbackWrapper);
            if (startSupportActionMode != null) {
                return callbackWrapper.getActionModeWrapper(startSupportActionMode);
            }
            return null;
        }
    }
    
    private class AutoBatteryNightModeManager extends AutoNightModeManager
    {
        private final PowerManager mPowerManager;
        final AppCompatDelegateImpl this$0;
        
        AutoBatteryNightModeManager(@NonNull final AppCompatDelegateImpl this$0, final Context context) {
            this.this$0 = this$0.super();
            this.mPowerManager = (PowerManager)context.getApplicationContext().getSystemService("power");
        }
        
        @Override
        IntentFilter createIntentFilterForBroadcastReceiver() {
            final IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.os.action.POWER_SAVE_MODE_CHANGED");
            return intentFilter;
        }
        
        public int getApplyableNightMode() {
            int n;
            if (Api21Impl.isPowerSaveMode(this.mPowerManager)) {
                n = 2;
            }
            else {
                n = 1;
            }
            return n;
        }
        
        public void onChange() {
            this.this$0.applyDayNight();
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    @VisibleForTesting
    abstract class AutoNightModeManager
    {
        private BroadcastReceiver mReceiver;
        final AppCompatDelegateImpl this$0;
        
        AutoNightModeManager(final AppCompatDelegateImpl this$0) {
            this.this$0 = this$0;
        }
        
        void cleanup() {
            final BroadcastReceiver mReceiver = this.mReceiver;
            if (mReceiver == null) {
                return;
            }
            while (true) {
                try {
                    this.this$0.mContext.unregisterReceiver(mReceiver);
                    this.mReceiver = null;
                }
                catch (final IllegalArgumentException ex) {
                    continue;
                }
                break;
            }
        }
        
        @Nullable
        abstract IntentFilter createIntentFilterForBroadcastReceiver();
        
        abstract int getApplyableNightMode();
        
        boolean isListening() {
            return this.mReceiver != null;
        }
        
        abstract void onChange();
        
        void setup() {
            this.cleanup();
            final IntentFilter intentFilterForBroadcastReceiver = this.createIntentFilterForBroadcastReceiver();
            if (intentFilterForBroadcastReceiver != null) {
                if (intentFilterForBroadcastReceiver.countActions() != 0) {
                    if (this.mReceiver == null) {
                        this.mReceiver = new BroadcastReceiver(this) {
                            final AutoNightModeManager this$1;
                            
                            public void onReceive(final Context context, final Intent intent) {
                                this.this$1.onChange();
                            }
                        };
                    }
                    this.this$0.mContext.registerReceiver(this.mReceiver, intentFilterForBroadcastReceiver);
                }
            }
        }
    }
    
    private class AutoTimeNightModeManager extends AutoNightModeManager
    {
        private final TwilightManager mTwilightManager;
        final AppCompatDelegateImpl this$0;
        
        AutoTimeNightModeManager(@NonNull final AppCompatDelegateImpl this$0, final TwilightManager mTwilightManager) {
            this.this$0 = this$0.super();
            this.mTwilightManager = mTwilightManager;
        }
        
        @Override
        IntentFilter createIntentFilterForBroadcastReceiver() {
            final IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.TIME_SET");
            intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
            intentFilter.addAction("android.intent.action.TIME_TICK");
            return intentFilter;
        }
        
        public int getApplyableNightMode() {
            int n;
            if (this.mTwilightManager.isNight()) {
                n = 2;
            }
            else {
                n = 1;
            }
            return n;
        }
        
        public void onChange() {
            this.this$0.applyDayNight();
        }
    }
    
    @RequiresApi(17)
    private static class ContextThemeWrapperCompatApi17Impl
    {
        static void applyOverrideConfiguration(final android.view.ContextThemeWrapper contextThemeWrapper, final Configuration configuration) {
            contextThemeWrapper.applyOverrideConfiguration(configuration);
        }
    }
    
    private class ListMenuDecorView extends ContentFrameLayout
    {
        final AppCompatDelegateImpl this$0;
        
        public ListMenuDecorView(final AppCompatDelegateImpl this$0, final Context context) {
            this.this$0 = this$0;
            super(context);
        }
        
        private boolean isOutOfBounds(final int n, final int n2) {
            return n < -5 || n2 < -5 || n > ((View)this).getWidth() + 5 || n2 > ((View)this).getHeight() + 5;
        }
        
        public boolean dispatchKeyEvent(final KeyEvent keyEvent) {
            return this.this$0.dispatchKeyEvent(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }
        
        public boolean onInterceptTouchEvent(final MotionEvent motionEvent) {
            if (motionEvent.getAction() == 0 && this.isOutOfBounds((int)motionEvent.getX(), (int)motionEvent.getY())) {
                this.this$0.closePanel(0);
                return true;
            }
            return super.onInterceptTouchEvent(motionEvent);
        }
        
        public void setBackgroundResource(final int n) {
            ((View)this).setBackgroundDrawable(AppCompatResources.getDrawable(((View)this).getContext(), n));
        }
    }
    
    protected static final class PanelFeatureState
    {
        int background;
        View createdPanelView;
        ViewGroup decorView;
        int featureId;
        Bundle frozenActionViewState;
        Bundle frozenMenuState;
        int gravity;
        boolean isHandled;
        boolean isOpen;
        boolean isPrepared;
        ListMenuPresenter listMenuPresenter;
        Context listPresenterContext;
        MenuBuilder menu;
        public boolean qwertyMode;
        boolean refreshDecorView;
        boolean refreshMenuContent;
        View shownPanelView;
        boolean wasLastOpen;
        int windowAnimations;
        int x;
        int y;
        
        PanelFeatureState(final int featureId) {
            this.featureId = featureId;
            this.refreshDecorView = false;
        }
        
        void applyFrozenState() {
            final MenuBuilder menu = this.menu;
            if (menu != null) {
                final Bundle frozenMenuState = this.frozenMenuState;
                if (frozenMenuState != null) {
                    menu.restorePresenterStates(frozenMenuState);
                    this.frozenMenuState = null;
                }
            }
        }
        
        public void clearMenuPresenters() {
            final MenuBuilder menu = this.menu;
            if (menu != null) {
                menu.removeMenuPresenter(this.listMenuPresenter);
            }
            this.listMenuPresenter = null;
        }
        
        MenuView getListMenuView(final MenuPresenter.Callback callback) {
            if (this.menu == null) {
                return null;
            }
            if (this.listMenuPresenter == null) {
                (this.listMenuPresenter = new ListMenuPresenter(this.listPresenterContext, R.layout.abc_list_menu_item_layout)).setCallback(callback);
                this.menu.addMenuPresenter(this.listMenuPresenter);
            }
            return this.listMenuPresenter.getMenuView(this.decorView);
        }
        
        public boolean hasPanelItems() {
            final View shownPanelView = this.shownPanelView;
            boolean b = false;
            if (shownPanelView == null) {
                return false;
            }
            if (this.createdPanelView != null) {
                return true;
            }
            if (((Adapter)this.listMenuPresenter.getAdapter()).getCount() > 0) {
                b = true;
            }
            return b;
        }
        
        void onRestoreInstanceState(final Parcelable parcelable) {
            final SavedState savedState = (SavedState)parcelable;
            this.featureId = savedState.featureId;
            this.wasLastOpen = savedState.isOpen;
            this.frozenMenuState = savedState.menuState;
            this.shownPanelView = null;
            this.decorView = null;
        }
        
        Parcelable onSaveInstanceState() {
            final SavedState savedState = new SavedState();
            savedState.featureId = this.featureId;
            savedState.isOpen = this.isOpen;
            if (this.menu != null) {
                final Bundle menuState = new Bundle();
                savedState.menuState = menuState;
                this.menu.savePresenterStates(menuState);
            }
            return (Parcelable)savedState;
        }
        
        void setMenu(final MenuBuilder menu) {
            final MenuBuilder menu2 = this.menu;
            if (menu == menu2) {
                return;
            }
            if (menu2 != null) {
                menu2.removeMenuPresenter(this.listMenuPresenter);
            }
            if ((this.menu = menu) != null) {
                final ListMenuPresenter listMenuPresenter = this.listMenuPresenter;
                if (listMenuPresenter != null) {
                    menu.addMenuPresenter(listMenuPresenter);
                }
            }
        }
        
        void setStyle(final Context context) {
            final TypedValue typedValue = new TypedValue();
            final Resources$Theme theme = context.getResources().newTheme();
            theme.setTo(context.getTheme());
            theme.resolveAttribute(R.attr.actionBarPopupTheme, typedValue, true);
            final int resourceId = typedValue.resourceId;
            if (resourceId != 0) {
                theme.applyStyle(resourceId, true);
            }
            theme.resolveAttribute(R.attr.panelMenuListTheme, typedValue, true);
            final int resourceId2 = typedValue.resourceId;
            if (resourceId2 != 0) {
                theme.applyStyle(resourceId2, true);
            }
            else {
                theme.applyStyle(R.style.Theme_AppCompat_CompactMenu, true);
            }
            final ContextThemeWrapper listPresenterContext = new ContextThemeWrapper(context, 0);
            ((Context)listPresenterContext).getTheme().setTo(theme);
            this.listPresenterContext = (Context)listPresenterContext;
            final TypedArray obtainStyledAttributes = ((Context)listPresenterContext).obtainStyledAttributes(R.styleable.AppCompatTheme);
            this.background = obtainStyledAttributes.getResourceId(R.styleable.AppCompatTheme_panelBackground, 0);
            this.windowAnimations = obtainStyledAttributes.getResourceId(R.styleable.AppCompatTheme_android_windowAnimationStyle, 0);
            obtainStyledAttributes.recycle();
        }
        
        @SuppressLint({ "BanParcelableUsage" })
        private static class SavedState implements Parcelable
        {
            public static final Parcelable$Creator<SavedState> CREATOR;
            int featureId;
            boolean isOpen;
            Bundle menuState;
            
            static {
                CREATOR = (Parcelable$Creator)new Parcelable$ClassLoaderCreator<SavedState>() {
                    public SavedState createFromParcel(final Parcel parcel) {
                        return SavedState.readFromParcel(parcel, null);
                    }
                    
                    public SavedState createFromParcel(final Parcel parcel, final ClassLoader classLoader) {
                        return SavedState.readFromParcel(parcel, classLoader);
                    }
                    
                    public SavedState[] newArray(final int n) {
                        return new SavedState[n];
                    }
                };
            }
            
            SavedState() {
            }
            
            static SavedState readFromParcel(final Parcel parcel, final ClassLoader classLoader) {
                final SavedState savedState = new SavedState();
                savedState.featureId = parcel.readInt();
                final int int1 = parcel.readInt();
                boolean isOpen = true;
                if (int1 != 1) {
                    isOpen = false;
                }
                savedState.isOpen = isOpen;
                if (isOpen) {
                    savedState.menuState = parcel.readBundle(classLoader);
                }
                return savedState;
            }
            
            public int describeContents() {
                return 0;
            }
            
            public void writeToParcel(final Parcel parcel, final int n) {
                parcel.writeInt(this.featureId);
                parcel.writeInt((int)(this.isOpen ? 1 : 0));
                if (this.isOpen) {
                    parcel.writeBundle(this.menuState);
                }
            }
        }
    }
    
    private final class PanelMenuPresenterCallback implements MenuPresenter.Callback
    {
        final AppCompatDelegateImpl this$0;
        
        PanelMenuPresenterCallback(final AppCompatDelegateImpl this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public void onCloseMenu(@NonNull MenuBuilder menuBuilder, final boolean b) {
            final Object rootMenu = menuBuilder.getRootMenu();
            final boolean b2 = rootMenu != menuBuilder;
            final AppCompatDelegateImpl this$0 = this.this$0;
            if (b2) {
                menuBuilder = (MenuBuilder)rootMenu;
            }
            final PanelFeatureState menuPanel = this$0.findMenuPanel((Menu)menuBuilder);
            if (menuPanel != null) {
                if (b2) {
                    this.this$0.callOnPanelClosed(menuPanel.featureId, menuPanel, (Menu)rootMenu);
                    this.this$0.closePanel(menuPanel, true);
                }
                else {
                    this.this$0.closePanel(menuPanel, b);
                }
            }
        }
        
        @Override
        public boolean onOpenSubMenu(@NonNull final MenuBuilder menuBuilder) {
            if (menuBuilder == menuBuilder.getRootMenu()) {
                final AppCompatDelegateImpl this$0 = this.this$0;
                if (this$0.mHasActionBar) {
                    final Window$Callback windowCallback = this$0.getWindowCallback();
                    if (windowCallback != null && !this.this$0.mDestroyed) {
                        windowCallback.onMenuOpened(108, (Menu)menuBuilder);
                    }
                }
            }
            return true;
        }
    }
}

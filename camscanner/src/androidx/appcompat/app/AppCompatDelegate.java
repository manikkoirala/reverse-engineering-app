// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.app;

import androidx.annotation.RestrictTo;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.app.LocaleManager;
import androidx.annotation.DoNotInline;
import android.os.LocaleList;
import androidx.appcompat.view.ActionMode;
import androidx.annotation.StyleRes;
import androidx.appcompat.widget.Toolbar;
import android.window.OnBackInvokedDispatcher;
import androidx.annotation.LayoutRes;
import android.content.res.Configuration;
import android.view.MenuInflater;
import androidx.annotation.IdRes;
import android.util.AttributeSet;
import androidx.annotation.CallSuper;
import android.view.ViewGroup$LayoutParams;
import android.view.View;
import java.util.Objects;
import androidx.annotation.VisibleForTesting;
import androidx.appcompat.widget.VectorEnabledTintResources;
import android.os.Bundle;
import android.content.pm.PackageManager$NameNotFoundException;
import androidx.annotation.RequiresApi;
import androidx.annotation.OptIn;
import androidx.annotation.AnyThread;
import androidx.core.os.BuildCompat;
import android.view.Window;
import android.content.Context;
import android.app.Dialog;
import androidx.annotation.Nullable;
import android.app.Activity;
import java.util.Iterator;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;
import androidx.core.os.LocaleListCompat;
import java.lang.ref.WeakReference;
import androidx.collection.ArraySet;

public abstract class AppCompatDelegate
{
    static final boolean DEBUG = false;
    public static final int FEATURE_ACTION_MODE_OVERLAY = 10;
    public static final int FEATURE_SUPPORT_ACTION_BAR = 108;
    public static final int FEATURE_SUPPORT_ACTION_BAR_OVERLAY = 109;
    @Deprecated
    public static final int MODE_NIGHT_AUTO = 0;
    public static final int MODE_NIGHT_AUTO_BATTERY = 3;
    @Deprecated
    public static final int MODE_NIGHT_AUTO_TIME = 0;
    public static final int MODE_NIGHT_FOLLOW_SYSTEM = -1;
    public static final int MODE_NIGHT_NO = 1;
    public static final int MODE_NIGHT_UNSPECIFIED = -100;
    public static final int MODE_NIGHT_YES = 2;
    static final String TAG = "AppCompatDelegate";
    private static final ArraySet<WeakReference<AppCompatDelegate>> sActivityDelegates;
    private static final Object sActivityDelegatesLock;
    private static final Object sAppLocalesStorageSyncLock;
    private static int sDefaultNightMode;
    private static Boolean sIsAutoStoreLocalesOptedIn;
    private static boolean sIsFrameworkSyncChecked;
    private static LocaleListCompat sRequestedAppLocales;
    static AppLocalesStorageHelper.SerialExecutor sSerialExecutorForLocalesStorage;
    private static LocaleListCompat sStoredAppLocales;
    
    static {
        AppCompatDelegate.sSerialExecutorForLocalesStorage = new AppLocalesStorageHelper.SerialExecutor(new AppLocalesStorageHelper.ThreadPerTaskExecutor());
        AppCompatDelegate.sDefaultNightMode = -100;
        AppCompatDelegate.sRequestedAppLocales = null;
        AppCompatDelegate.sStoredAppLocales = null;
        AppCompatDelegate.sIsAutoStoreLocalesOptedIn = null;
        AppCompatDelegate.sIsFrameworkSyncChecked = false;
        sActivityDelegates = new ArraySet<WeakReference<AppCompatDelegate>>();
        sActivityDelegatesLock = new Object();
        sAppLocalesStorageSyncLock = new Object();
    }
    
    AppCompatDelegate() {
    }
    
    static void addActiveDelegate(@NonNull final AppCompatDelegate referent) {
        synchronized (AppCompatDelegate.sActivityDelegatesLock) {
            removeDelegateFromActives(referent);
            AppCompatDelegate.sActivityDelegates.add(new WeakReference<AppCompatDelegate>(referent));
        }
    }
    
    private static void applyDayNightToActiveDelegates() {
        synchronized (AppCompatDelegate.sActivityDelegatesLock) {
            final Iterator<WeakReference<AppCompatDelegate>> iterator = AppCompatDelegate.sActivityDelegates.iterator();
            while (iterator.hasNext()) {
                final AppCompatDelegate appCompatDelegate = iterator.next().get();
                if (appCompatDelegate != null) {
                    appCompatDelegate.applyDayNight();
                }
            }
        }
    }
    
    private static void applyLocalesToActiveDelegates() {
        final Iterator<WeakReference<AppCompatDelegate>> iterator = AppCompatDelegate.sActivityDelegates.iterator();
        while (iterator.hasNext()) {
            final AppCompatDelegate appCompatDelegate = iterator.next().get();
            if (appCompatDelegate != null) {
                appCompatDelegate.applyAppLocales();
            }
        }
    }
    
    @NonNull
    public static AppCompatDelegate create(@NonNull final Activity activity, @Nullable final AppCompatCallback appCompatCallback) {
        return new AppCompatDelegateImpl(activity, appCompatCallback);
    }
    
    @NonNull
    public static AppCompatDelegate create(@NonNull final Dialog dialog, @Nullable final AppCompatCallback appCompatCallback) {
        return new AppCompatDelegateImpl(dialog, appCompatCallback);
    }
    
    @NonNull
    public static AppCompatDelegate create(@NonNull final Context context, @NonNull final Activity activity, @Nullable final AppCompatCallback appCompatCallback) {
        return new AppCompatDelegateImpl(context, activity, appCompatCallback);
    }
    
    @NonNull
    public static AppCompatDelegate create(@NonNull final Context context, @NonNull final Window window, @Nullable final AppCompatCallback appCompatCallback) {
        return new AppCompatDelegateImpl(context, window, appCompatCallback);
    }
    
    @AnyThread
    @NonNull
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    public static LocaleListCompat getApplicationLocales() {
        if (BuildCompat.isAtLeastT()) {
            final Object localeManagerForApplication = getLocaleManagerForApplication();
            if (localeManagerForApplication != null) {
                return LocaleListCompat.wrap(Api33Impl.localeManagerGetApplicationLocales(localeManagerForApplication));
            }
        }
        else {
            final LocaleListCompat sRequestedAppLocales = AppCompatDelegate.sRequestedAppLocales;
            if (sRequestedAppLocales != null) {
                return sRequestedAppLocales;
            }
        }
        return LocaleListCompat.getEmptyLocaleList();
    }
    
    public static int getDefaultNightMode() {
        return AppCompatDelegate.sDefaultNightMode;
    }
    
    @RequiresApi(33)
    static Object getLocaleManagerForApplication() {
        final Iterator<WeakReference<AppCompatDelegate>> iterator = AppCompatDelegate.sActivityDelegates.iterator();
        while (iterator.hasNext()) {
            final AppCompatDelegate appCompatDelegate = iterator.next().get();
            if (appCompatDelegate != null) {
                final Context contextForDelegate = appCompatDelegate.getContextForDelegate();
                if (contextForDelegate != null) {
                    return contextForDelegate.getSystemService("locale");
                }
                continue;
            }
        }
        return null;
    }
    
    @Nullable
    static LocaleListCompat getRequestedAppLocales() {
        return AppCompatDelegate.sRequestedAppLocales;
    }
    
    @Nullable
    static LocaleListCompat getStoredAppLocales() {
        return AppCompatDelegate.sStoredAppLocales;
    }
    
    static boolean isAutoStorageOptedIn(final Context context) {
        if (AppCompatDelegate.sIsAutoStoreLocalesOptedIn == null) {
            try {
                final Bundle metaData = AppLocalesMetadataHolderService.getServiceInfo(context).metaData;
                if (metaData != null) {
                    AppCompatDelegate.sIsAutoStoreLocalesOptedIn = metaData.getBoolean("autoStoreLocales");
                }
            }
            catch (final PackageManager$NameNotFoundException ex) {
                AppCompatDelegate.sIsAutoStoreLocalesOptedIn = Boolean.FALSE;
            }
        }
        return AppCompatDelegate.sIsAutoStoreLocalesOptedIn;
    }
    
    public static boolean isCompatVectorFromResourcesEnabled() {
        return VectorEnabledTintResources.isCompatVectorFromResourcesEnabled();
    }
    
    static void removeActivityDelegate(@NonNull final AppCompatDelegate appCompatDelegate) {
        synchronized (AppCompatDelegate.sActivityDelegatesLock) {
            removeDelegateFromActives(appCompatDelegate);
        }
    }
    
    private static void removeDelegateFromActives(@NonNull final AppCompatDelegate appCompatDelegate) {
        synchronized (AppCompatDelegate.sActivityDelegatesLock) {
            final Iterator<WeakReference<AppCompatDelegate>> iterator = AppCompatDelegate.sActivityDelegates.iterator();
            while (iterator.hasNext()) {
                final AppCompatDelegate appCompatDelegate2 = iterator.next().get();
                if (appCompatDelegate2 == appCompatDelegate || appCompatDelegate2 == null) {
                    iterator.remove();
                }
            }
        }
    }
    
    @VisibleForTesting
    static void resetStaticRequestedAndStoredLocales() {
        AppCompatDelegate.sRequestedAppLocales = null;
        AppCompatDelegate.sStoredAppLocales = null;
    }
    
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    public static void setApplicationLocales(@NonNull final LocaleListCompat localeListCompat) {
        Objects.requireNonNull(localeListCompat);
        if (BuildCompat.isAtLeastT()) {
            final Object localeManagerForApplication = getLocaleManagerForApplication();
            if (localeManagerForApplication != null) {
                Api33Impl.localeManagerSetApplicationLocales(localeManagerForApplication, Api24Impl.localeListForLanguageTags(localeListCompat.toLanguageTags()));
            }
        }
        else if (!localeListCompat.equals(AppCompatDelegate.sRequestedAppLocales)) {
            synchronized (AppCompatDelegate.sActivityDelegatesLock) {
                AppCompatDelegate.sRequestedAppLocales = localeListCompat;
                applyLocalesToActiveDelegates();
            }
        }
    }
    
    public static void setCompatVectorFromResourcesEnabled(final boolean compatVectorFromResourcesEnabled) {
        VectorEnabledTintResources.setCompatVectorFromResourcesEnabled(compatVectorFromResourcesEnabled);
    }
    
    public static void setDefaultNightMode(final int sDefaultNightMode) {
        if (sDefaultNightMode == -1 || sDefaultNightMode == 0 || sDefaultNightMode == 1 || sDefaultNightMode == 2 || sDefaultNightMode == 3) {
            if (AppCompatDelegate.sDefaultNightMode != sDefaultNightMode) {
                AppCompatDelegate.sDefaultNightMode = sDefaultNightMode;
                applyDayNightToActiveDelegates();
            }
        }
    }
    
    @VisibleForTesting
    static void setIsAutoStoreLocalesOptedIn(final boolean b) {
        AppCompatDelegate.sIsAutoStoreLocalesOptedIn = b;
    }
    
    @OptIn(markerClass = { BuildCompat.PrereleaseSdkCheck.class })
    static void syncRequestedAndStoredLocales(final Context context) {
        if (!isAutoStorageOptedIn(context)) {
            return;
        }
        if (BuildCompat.isAtLeastT()) {
            if (!AppCompatDelegate.sIsFrameworkSyncChecked) {
                AppCompatDelegate.sSerialExecutorForLocalesStorage.execute(new \u3007o00\u3007\u3007Oo(context));
            }
            return;
        }
        synchronized (AppCompatDelegate.sAppLocalesStorageSyncLock) {
            final LocaleListCompat sRequestedAppLocales = AppCompatDelegate.sRequestedAppLocales;
            if (sRequestedAppLocales == null) {
                if (AppCompatDelegate.sStoredAppLocales == null) {
                    AppCompatDelegate.sStoredAppLocales = LocaleListCompat.forLanguageTags(AppLocalesStorageHelper.readLocales(context));
                }
                if (AppCompatDelegate.sStoredAppLocales.isEmpty()) {
                    return;
                }
                AppCompatDelegate.sRequestedAppLocales = AppCompatDelegate.sStoredAppLocales;
            }
            else if (!sRequestedAppLocales.equals(AppCompatDelegate.sStoredAppLocales)) {
                AppLocalesStorageHelper.persistLocales(context, (AppCompatDelegate.sStoredAppLocales = AppCompatDelegate.sRequestedAppLocales).toLanguageTags());
            }
        }
    }
    
    public abstract void addContentView(final View p0, final ViewGroup$LayoutParams p1);
    
    boolean applyAppLocales() {
        return false;
    }
    
    public abstract boolean applyDayNight();
    
    void asyncExecuteSyncRequestedAndStoredLocales(final Context context) {
        AppCompatDelegate.sSerialExecutorForLocalesStorage.execute(new \u3007080(context));
    }
    
    @Deprecated
    public void attachBaseContext(final Context context) {
    }
    
    @CallSuper
    @NonNull
    public Context attachBaseContext2(@NonNull final Context context) {
        this.attachBaseContext(context);
        return context;
    }
    
    public abstract View createView(@Nullable final View p0, final String p1, @NonNull final Context p2, @NonNull final AttributeSet p3);
    
    @Nullable
    public abstract <T extends View> T findViewById(@IdRes final int p0);
    
    @Nullable
    public Context getContextForDelegate() {
        return null;
    }
    
    @Nullable
    public abstract ActionBarDrawerToggle.Delegate getDrawerToggleDelegate();
    
    public int getLocalNightMode() {
        return -100;
    }
    
    public abstract MenuInflater getMenuInflater();
    
    @Nullable
    public abstract ActionBar getSupportActionBar();
    
    public abstract boolean hasWindowFeature(final int p0);
    
    public abstract void installViewFactory();
    
    public abstract void invalidateOptionsMenu();
    
    public abstract boolean isHandleNativeActionModesEnabled();
    
    public abstract void onConfigurationChanged(final Configuration p0);
    
    public abstract void onCreate(final Bundle p0);
    
    public abstract void onDestroy();
    
    public abstract void onPostCreate(final Bundle p0);
    
    public abstract void onPostResume();
    
    public abstract void onSaveInstanceState(final Bundle p0);
    
    public abstract void onStart();
    
    public abstract void onStop();
    
    public abstract boolean requestWindowFeature(final int p0);
    
    public abstract void setContentView(@LayoutRes final int p0);
    
    public abstract void setContentView(final View p0);
    
    public abstract void setContentView(final View p0, final ViewGroup$LayoutParams p1);
    
    public abstract void setHandleNativeActionModesEnabled(final boolean p0);
    
    @RequiresApi(17)
    public abstract void setLocalNightMode(final int p0);
    
    @CallSuper
    @RequiresApi(33)
    public void setOnBackInvokedDispatcher(@Nullable final OnBackInvokedDispatcher onBackInvokedDispatcher) {
    }
    
    public abstract void setSupportActionBar(@Nullable final Toolbar p0);
    
    public void setTheme(@StyleRes final int n) {
    }
    
    public abstract void setTitle(@Nullable final CharSequence p0);
    
    @Nullable
    public abstract ActionMode startSupportActionMode(@NonNull final ActionMode.Callback p0);
    
    @RequiresApi(24)
    static class Api24Impl
    {
        private Api24Impl() {
        }
        
        @DoNotInline
        static LocaleList localeListForLanguageTags(final String s) {
            return \u3007o\u3007.\u3007080(s);
        }
    }
    
    @RequiresApi(33)
    static class Api33Impl
    {
        private Api33Impl() {
        }
        
        @DoNotInline
        static LocaleList localeManagerGetApplicationLocales(final Object o) {
            return ((LocaleManager)o).getApplicationLocales();
        }
        
        @DoNotInline
        static void localeManagerSetApplicationLocales(final Object o, final LocaleList applicationLocales) {
            ((LocaleManager)o).setApplicationLocales(applicationLocales);
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public @interface NightMode {
    }
}

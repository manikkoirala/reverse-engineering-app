// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.app;

import androidx.annotation.DoNotInline;
import androidx.annotation.RequiresApi;
import android.os.IBinder;
import android.content.Intent;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.ComponentName;
import android.os.Build$VERSION;
import android.content.pm.ServiceInfo;
import androidx.annotation.NonNull;
import android.content.Context;
import android.app.Service;

public final class AppLocalesMetadataHolderService extends Service
{
    @NonNull
    public static ServiceInfo getServiceInfo(@NonNull final Context context) throws PackageManager$NameNotFoundException {
        int n;
        if (Build$VERSION.SDK_INT >= 24) {
            n = (Api24Impl.getDisabledComponentFlag() | 0x80);
        }
        else {
            n = 640;
        }
        return context.getPackageManager().getServiceInfo(new ComponentName(context, (Class)AppLocalesMetadataHolderService.class), n);
    }
    
    @NonNull
    public IBinder onBind(@NonNull final Intent intent) {
        throw new UnsupportedOperationException();
    }
    
    @RequiresApi(24)
    private static class Api24Impl
    {
        @DoNotInline
        static int getDisabledComponentFlag() {
            return 512;
        }
    }
}

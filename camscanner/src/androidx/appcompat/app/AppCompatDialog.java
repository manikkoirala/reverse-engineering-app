// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.app;

import androidx.annotation.LayoutRes;
import androidx.appcompat.view.ActionMode;
import androidx.annotation.RestrictTo;
import android.app.Dialog;
import androidx.annotation.IdRes;
import android.view.Window$Callback;
import android.view.KeyEvent;
import android.view.ViewGroup$LayoutParams;
import android.view.View;
import androidx.appcompat.R;
import android.util.TypedValue;
import androidx.annotation.Nullable;
import android.content.DialogInterface$OnCancelListener;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.core.view.KeyEventDispatcher;
import androidx.activity.ComponentDialog;

public class AppCompatDialog extends ComponentDialog implements AppCompatCallback
{
    private AppCompatDelegate mDelegate;
    private final KeyEventDispatcher.Component mKeyDispatcher;
    
    public AppCompatDialog(@NonNull final Context context) {
        this(context, 0);
    }
    
    public AppCompatDialog(@NonNull final Context context, final int n) {
        super(context, getThemeResId(context, n));
        this.mKeyDispatcher = new \u3007O8o08O(this);
        final AppCompatDelegate delegate = this.getDelegate();
        delegate.setTheme(getThemeResId(context, n));
        delegate.onCreate(null);
    }
    
    protected AppCompatDialog(@NonNull final Context context, final boolean cancelable, @Nullable final DialogInterface$OnCancelListener onCancelListener) {
        super(context);
        this.mKeyDispatcher = new \u3007O8o08O(this);
        this.setCancelable(cancelable);
        this.setOnCancelListener(onCancelListener);
    }
    
    private static int getThemeResId(final Context context, final int n) {
        int resourceId = n;
        if (n == 0) {
            final TypedValue typedValue = new TypedValue();
            context.getTheme().resolveAttribute(R.attr.dialogTheme, typedValue, true);
            resourceId = typedValue.resourceId;
        }
        return resourceId;
    }
    
    @Override
    public void addContentView(@NonNull final View view, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        this.getDelegate().addContentView(view, viewGroup$LayoutParams);
    }
    
    public void dismiss() {
        super.dismiss();
        this.getDelegate().onDestroy();
    }
    
    public boolean dispatchKeyEvent(final KeyEvent keyEvent) {
        return KeyEventDispatcher.dispatchKeyEvent(this.mKeyDispatcher, this.getWindow().getDecorView(), (Window$Callback)this, keyEvent);
    }
    
    @Nullable
    public <T extends View> T findViewById(@IdRes final int n) {
        return this.getDelegate().findViewById(n);
    }
    
    @NonNull
    public AppCompatDelegate getDelegate() {
        if (this.mDelegate == null) {
            this.mDelegate = AppCompatDelegate.create(this, this);
        }
        return this.mDelegate;
    }
    
    public ActionBar getSupportActionBar() {
        return this.getDelegate().getSupportActionBar();
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    public void invalidateOptionsMenu() {
        this.getDelegate().invalidateOptionsMenu();
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        this.getDelegate().installViewFactory();
        super.onCreate(bundle);
        this.getDelegate().onCreate(bundle);
    }
    
    @Override
    protected void onStop() {
        super.onStop();
        this.getDelegate().onStop();
    }
    
    @Override
    public void onSupportActionModeFinished(final ActionMode actionMode) {
    }
    
    @Override
    public void onSupportActionModeStarted(final ActionMode actionMode) {
    }
    
    @Nullable
    @Override
    public ActionMode onWindowStartingSupportActionMode(final ActionMode.Callback callback) {
        return null;
    }
    
    @Override
    public void setContentView(@LayoutRes final int contentView) {
        this.getDelegate().setContentView(contentView);
    }
    
    @Override
    public void setContentView(@NonNull final View contentView) {
        this.getDelegate().setContentView(contentView);
    }
    
    @Override
    public void setContentView(@NonNull final View view, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        this.getDelegate().setContentView(view, viewGroup$LayoutParams);
    }
    
    public void setTitle(final int title) {
        super.setTitle(title);
        this.getDelegate().setTitle(this.getContext().getString(title));
    }
    
    public void setTitle(final CharSequence charSequence) {
        super.setTitle(charSequence);
        this.getDelegate().setTitle(charSequence);
    }
    
    boolean superDispatchKeyEvent(final KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent);
    }
    
    public boolean supportRequestWindowFeature(final int n) {
        return this.getDelegate().requestWindowFeature(n);
    }
}

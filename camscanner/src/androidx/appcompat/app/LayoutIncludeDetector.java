// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.app;

import android.util.AttributeSet;
import org.xmlpull.v1.XmlPullParserException;
import androidx.annotation.Nullable;
import java.util.ArrayDeque;
import androidx.annotation.NonNull;
import org.xmlpull.v1.XmlPullParser;
import java.lang.ref.WeakReference;
import java.util.Deque;

class LayoutIncludeDetector
{
    @NonNull
    private final Deque<WeakReference<XmlPullParser>> mXmlParserStack;
    
    LayoutIncludeDetector() {
        this.mXmlParserStack = new ArrayDeque<WeakReference<XmlPullParser>>();
    }
    
    private static boolean isParserOutdated(@Nullable final XmlPullParser xmlPullParser) {
        boolean b2;
        final boolean b = b2 = true;
        if (xmlPullParser == null) {
            return b2;
        }
        b2 = b;
        try {
            if (xmlPullParser.getEventType() != 3) {
                b2 = (xmlPullParser.getEventType() == 1 && b);
            }
            return b2;
        }
        catch (final XmlPullParserException ex) {
            b2 = b;
            return b2;
        }
    }
    
    @Nullable
    private static XmlPullParser popOutdatedAttrHolders(@NonNull final Deque<WeakReference<XmlPullParser>> deque) {
        while (!deque.isEmpty()) {
            final XmlPullParser xmlPullParser = deque.peek().get();
            if (!isParserOutdated(xmlPullParser)) {
                return xmlPullParser;
            }
            deque.pop();
        }
        return null;
    }
    
    private static boolean shouldInheritContext(@NonNull final XmlPullParser xmlPullParser, @Nullable final XmlPullParser xmlPullParser2) {
        if (xmlPullParser2 == null || xmlPullParser == xmlPullParser2) {
            return false;
        }
        try {
            if (xmlPullParser2.getEventType() == 2) {
                return "include".equals(xmlPullParser2.getName());
            }
            return false;
        }
        catch (final XmlPullParserException ex) {
            return false;
        }
    }
    
    boolean detect(@NonNull final AttributeSet set) {
        if (set instanceof XmlPullParser) {
            final XmlPullParser referent = (XmlPullParser)set;
            if (referent.getDepth() == 1) {
                final XmlPullParser popOutdatedAttrHolders = popOutdatedAttrHolders(this.mXmlParserStack);
                this.mXmlParserStack.push(new WeakReference<XmlPullParser>(referent));
                if (shouldInheritContext(referent, popOutdatedAttrHolders)) {
                    return true;
                }
            }
        }
        return false;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.app;

import java.util.Map;
import androidx.annotation.DoNotInline;
import android.util.LongSparseArray;
import androidx.annotation.RequiresApi;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.content.res.Resources;
import java.lang.reflect.Field;

class ResourcesFlusher
{
    private static final String TAG = "ResourcesFlusher";
    private static Field sDrawableCacheField;
    private static boolean sDrawableCacheFieldFetched;
    private static Field sResourcesImplField;
    private static boolean sResourcesImplFieldFetched;
    private static Class<?> sThemedResourceCacheClazz;
    private static boolean sThemedResourceCacheClazzFetched;
    private static Field sThemedResourceCache_mUnthemedEntriesField;
    private static boolean sThemedResourceCache_mUnthemedEntriesFieldFetched;
    
    private ResourcesFlusher() {
    }
    
    static void flush(@NonNull final Resources resources) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 28) {
            return;
        }
        if (sdk_INT >= 24) {
            flushNougats(resources);
        }
        else if (sdk_INT >= 23) {
            flushMarshmallows(resources);
        }
        else {
            flushLollipops(resources);
        }
    }
    
    @RequiresApi(21)
    private static void flushLollipops(@NonNull Resources obj) {
        Label_0027: {
            if (ResourcesFlusher.sDrawableCacheFieldFetched) {
                break Label_0027;
            }
            while (true) {
                try {
                    (ResourcesFlusher.sDrawableCacheField = Resources.class.getDeclaredField("mDrawableCache")).setAccessible(true);
                    ResourcesFlusher.sDrawableCacheFieldFetched = true;
                    final Field sDrawableCacheField = ResourcesFlusher.sDrawableCacheField;
                    if (sDrawableCacheField != null) {
                        try {
                            obj = (IllegalAccessException)sDrawableCacheField.get(obj);
                        }
                        catch (IllegalAccessException obj) {
                            obj = null;
                        }
                        if (obj != null) {
                            ((Map)obj).clear();
                        }
                    }
                }
                catch (final NoSuchFieldException ex) {
                    continue;
                }
                break;
            }
        }
    }
    
    @RequiresApi(23)
    private static void flushMarshmallows(@NonNull final Resources p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: ifne            27
        //     6: ldc             Landroid/content/res/Resources;.class
        //     8: ldc             "mDrawableCache"
        //    10: invokevirtual   java/lang/Class.getDeclaredField:(Ljava/lang/String;)Ljava/lang/reflect/Field;
        //    13: astore_1       
        //    14: aload_1        
        //    15: putstatic       androidx/appcompat/app/ResourcesFlusher.sDrawableCacheField:Ljava/lang/reflect/Field;
        //    18: aload_1        
        //    19: iconst_1       
        //    20: invokevirtual   java/lang/reflect/AccessibleObject.setAccessible:(Z)V
        //    23: iconst_1       
        //    24: putstatic       androidx/appcompat/app/ResourcesFlusher.sDrawableCacheFieldFetched:Z
        //    27: getstatic       androidx/appcompat/app/ResourcesFlusher.sDrawableCacheField:Ljava/lang/reflect/Field;
        //    30: astore_1       
        //    31: aload_1        
        //    32: ifnull          44
        //    35: aload_1        
        //    36: aload_0        
        //    37: invokevirtual   java/lang/reflect/Field.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //    40: astore_0       
        //    41: goto            46
        //    44: aconst_null    
        //    45: astore_0       
        //    46: aload_0        
        //    47: ifnonnull       51
        //    50: return         
        //    51: aload_0        
        //    52: invokestatic    androidx/appcompat/app/ResourcesFlusher.flushThemedResourcesCache:(Ljava/lang/Object;)V
        //    55: return         
        //    56: astore_1       
        //    57: goto            23
        //    60: astore_0       
        //    61: goto            44
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                              
        //  -----  -----  -----  -----  ----------------------------------
        //  6      23     56     60     Ljava/lang/NoSuchFieldException;
        //  35     41     60     64     Ljava/lang/IllegalAccessException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0044:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @RequiresApi(24)
    private static void flushNougats(@NonNull final Resources p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: ifne            27
        //     6: ldc             Landroid/content/res/Resources;.class
        //     8: ldc             "mResourcesImpl"
        //    10: invokevirtual   java/lang/Class.getDeclaredField:(Ljava/lang/String;)Ljava/lang/reflect/Field;
        //    13: astore_1       
        //    14: aload_1        
        //    15: putstatic       androidx/appcompat/app/ResourcesFlusher.sResourcesImplField:Ljava/lang/reflect/Field;
        //    18: aload_1        
        //    19: iconst_1       
        //    20: invokevirtual   java/lang/reflect/AccessibleObject.setAccessible:(Z)V
        //    23: iconst_1       
        //    24: putstatic       androidx/appcompat/app/ResourcesFlusher.sResourcesImplFieldFetched:Z
        //    27: getstatic       androidx/appcompat/app/ResourcesFlusher.sResourcesImplField:Ljava/lang/reflect/Field;
        //    30: astore_1       
        //    31: aload_1        
        //    32: ifnonnull       36
        //    35: return         
        //    36: aconst_null    
        //    37: astore_2       
        //    38: aload_1        
        //    39: aload_0        
        //    40: invokevirtual   java/lang/reflect/Field.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //    43: astore_0       
        //    44: goto            50
        //    47: astore_0       
        //    48: aconst_null    
        //    49: astore_0       
        //    50: aload_0        
        //    51: ifnonnull       55
        //    54: return         
        //    55: getstatic       androidx/appcompat/app/ResourcesFlusher.sDrawableCacheFieldFetched:Z
        //    58: ifne            84
        //    61: aload_0        
        //    62: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
        //    65: ldc             "mDrawableCache"
        //    67: invokevirtual   java/lang/Class.getDeclaredField:(Ljava/lang/String;)Ljava/lang/reflect/Field;
        //    70: astore_1       
        //    71: aload_1        
        //    72: putstatic       androidx/appcompat/app/ResourcesFlusher.sDrawableCacheField:Ljava/lang/reflect/Field;
        //    75: aload_1        
        //    76: iconst_1       
        //    77: invokevirtual   java/lang/reflect/AccessibleObject.setAccessible:(Z)V
        //    80: iconst_1       
        //    81: putstatic       androidx/appcompat/app/ResourcesFlusher.sDrawableCacheFieldFetched:Z
        //    84: getstatic       androidx/appcompat/app/ResourcesFlusher.sDrawableCacheField:Ljava/lang/reflect/Field;
        //    87: astore_3       
        //    88: aload_2        
        //    89: astore_1       
        //    90: aload_3        
        //    91: ifnull          106
        //    94: aload_3        
        //    95: aload_0        
        //    96: invokevirtual   java/lang/reflect/Field.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //    99: astore_1       
        //   100: goto            106
        //   103: astore_0       
        //   104: aload_2        
        //   105: astore_1       
        //   106: aload_1        
        //   107: ifnull          114
        //   110: aload_1        
        //   111: invokestatic    androidx/appcompat/app/ResourcesFlusher.flushThemedResourcesCache:(Ljava/lang/Object;)V
        //   114: return         
        //   115: astore_1       
        //   116: goto            23
        //   119: astore_1       
        //   120: goto            80
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                              
        //  -----  -----  -----  -----  ----------------------------------
        //  6      23     115    119    Ljava/lang/NoSuchFieldException;
        //  38     44     47     50     Ljava/lang/IllegalAccessException;
        //  61     80     119    123    Ljava/lang/NoSuchFieldException;
        //  94     100    103    106    Ljava/lang/IllegalAccessException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0080:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @RequiresApi(16)
    private static void flushThemedResourcesCache(@NonNull final Object p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: ifne            18
        //     6: ldc             "android.content.res.ThemedResourceCache"
        //     8: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
        //    11: putstatic       androidx/appcompat/app/ResourcesFlusher.sThemedResourceCacheClazz:Ljava/lang/Class;
        //    14: iconst_1       
        //    15: putstatic       androidx/appcompat/app/ResourcesFlusher.sThemedResourceCacheClazzFetched:Z
        //    18: getstatic       androidx/appcompat/app/ResourcesFlusher.sThemedResourceCacheClazz:Ljava/lang/Class;
        //    21: astore_1       
        //    22: aload_1        
        //    23: ifnonnull       27
        //    26: return         
        //    27: getstatic       androidx/appcompat/app/ResourcesFlusher.sThemedResourceCache_mUnthemedEntriesFieldFetched:Z
        //    30: ifne            53
        //    33: aload_1        
        //    34: ldc             "mUnthemedEntries"
        //    36: invokevirtual   java/lang/Class.getDeclaredField:(Ljava/lang/String;)Ljava/lang/reflect/Field;
        //    39: astore_1       
        //    40: aload_1        
        //    41: putstatic       androidx/appcompat/app/ResourcesFlusher.sThemedResourceCache_mUnthemedEntriesField:Ljava/lang/reflect/Field;
        //    44: aload_1        
        //    45: iconst_1       
        //    46: invokevirtual   java/lang/reflect/AccessibleObject.setAccessible:(Z)V
        //    49: iconst_1       
        //    50: putstatic       androidx/appcompat/app/ResourcesFlusher.sThemedResourceCache_mUnthemedEntriesFieldFetched:Z
        //    53: getstatic       androidx/appcompat/app/ResourcesFlusher.sThemedResourceCache_mUnthemedEntriesField:Ljava/lang/reflect/Field;
        //    56: astore_1       
        //    57: aload_1        
        //    58: ifnonnull       62
        //    61: return         
        //    62: aload_1        
        //    63: aload_0        
        //    64: invokevirtual   java/lang/reflect/Field.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //    67: checkcast       Landroid/util/LongSparseArray;
        //    70: astore_0       
        //    71: goto            77
        //    74: astore_0       
        //    75: aconst_null    
        //    76: astore_0       
        //    77: aload_0        
        //    78: ifnull          85
        //    81: aload_0        
        //    82: invokestatic    androidx/appcompat/app/ResourcesFlusher$Api16Impl.clear:(Landroid/util/LongSparseArray;)V
        //    85: return         
        //    86: astore_1       
        //    87: goto            14
        //    90: astore_1       
        //    91: goto            49
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                              
        //  -----  -----  -----  -----  ----------------------------------
        //  6      14     86     90     Ljava/lang/ClassNotFoundException;
        //  33     49     90     94     Ljava/lang/NoSuchFieldException;
        //  62     71     74     77     Ljava/lang/IllegalAccessException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0049:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @RequiresApi(16)
    static class Api16Impl
    {
        private Api16Impl() {
        }
        
        @DoNotInline
        static void clear(final LongSparseArray longSparseArray) {
            longSparseArray.clear();
        }
    }
}

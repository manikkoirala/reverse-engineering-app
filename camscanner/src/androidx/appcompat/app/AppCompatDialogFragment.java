// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.app;

import androidx.annotation.RestrictTo;
import androidx.annotation.NonNull;
import android.app.Dialog;
import androidx.annotation.Nullable;
import android.os.Bundle;
import androidx.annotation.LayoutRes;
import androidx.fragment.app.DialogFragment;

public class AppCompatDialogFragment extends DialogFragment
{
    public AppCompatDialogFragment() {
    }
    
    public AppCompatDialogFragment(@LayoutRes final int n) {
        super(n);
    }
    
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable final Bundle bundle) {
        return new AppCompatDialog(this.getContext(), this.getTheme());
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
    @Override
    public void setupDialog(@NonNull final Dialog dialog, final int n) {
        if (dialog instanceof AppCompatDialog) {
            final AppCompatDialog appCompatDialog = (AppCompatDialog)dialog;
            if (n != 1 && n != 2) {
                if (n != 3) {
                    return;
                }
                dialog.getWindow().addFlags(24);
            }
            appCompatDialog.supportRequestWindowFeature(1);
        }
        else {
            super.setupDialog(dialog, n);
        }
    }
}

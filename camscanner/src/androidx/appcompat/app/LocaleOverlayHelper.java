// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.app;

import android.os.LocaleList;
import java.util.Locale;
import java.util.LinkedHashSet;
import androidx.core.os.LocaleListCompat;
import androidx.annotation.RequiresApi;

@RequiresApi(24)
final class LocaleOverlayHelper
{
    private LocaleOverlayHelper() {
    }
    
    private static LocaleListCompat combineLocales(final LocaleListCompat localeListCompat, final LocaleListCompat localeListCompat2) {
        final LinkedHashSet set = new LinkedHashSet();
        for (int i = 0; i < localeListCompat.size() + localeListCompat2.size(); ++i) {
            Locale locale;
            if (i < localeListCompat.size()) {
                locale = localeListCompat.get(i);
            }
            else {
                locale = localeListCompat2.get(i - localeListCompat.size());
            }
            if (locale != null) {
                set.add(locale);
            }
        }
        return LocaleListCompat.create((Locale[])set.toArray(new Locale[set.size()]));
    }
    
    static LocaleListCompat combineLocalesIfOverlayExists(final LocaleList list, final LocaleList list2) {
        if (list != null && !Oooo8o0\u3007.\u3007080(list)) {
            return combineLocales(LocaleListCompat.wrap(list), LocaleListCompat.wrap(list2));
        }
        return LocaleListCompat.getEmptyLocaleList();
    }
    
    static LocaleListCompat combineLocalesIfOverlayExists(final LocaleListCompat localeListCompat, final LocaleListCompat localeListCompat2) {
        if (localeListCompat != null && !localeListCompat.isEmpty()) {
            return combineLocales(localeListCompat, localeListCompat2);
        }
        return LocaleListCompat.getEmptyLocaleList();
    }
}

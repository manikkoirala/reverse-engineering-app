// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.app;

import androidx.annotation.StyleRes;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.annotation.CallSuper;
import androidx.appcompat.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import androidx.core.os.LocaleListCompat;
import android.content.res.Configuration;
import androidx.core.app.NavUtils;
import android.content.Intent;
import androidx.appcompat.widget.VectorEnabledTintResources;
import android.view.MenuInflater;
import androidx.annotation.Nullable;
import android.app.Activity;
import androidx.annotation.IdRes;
import android.view.ViewGroup$LayoutParams;
import android.view.View;
import android.view.Window;
import android.os.Build$VERSION;
import android.view.KeyEvent;
import androidx.activity.OnBackPressedDispatcherOwner;
import androidx.activity.ViewTreeOnBackPressedDispatcherOwner;
import androidx.savedstate.SavedStateRegistryOwner;
import androidx.savedstate.ViewTreeSavedStateRegistryOwner;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.lifecycle.ViewTreeViewModelStoreOwner;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewTreeLifecycleOwner;
import android.content.Context;
import androidx.activity.contextaware.OnContextAvailableListener;
import androidx.annotation.NonNull;
import android.os.Bundle;
import androidx.savedstate.SavedStateRegistry;
import androidx.annotation.ContentView;
import androidx.annotation.LayoutRes;
import android.content.res.Resources;
import androidx.core.app.TaskStackBuilder;
import androidx.fragment.app.FragmentActivity;

public class AppCompatActivity extends FragmentActivity implements AppCompatCallback, SupportParentable, DelegateProvider
{
    private static final String DELEGATE_TAG = "androidx:appcompat";
    private AppCompatDelegate mDelegate;
    private Resources mResources;
    
    public AppCompatActivity() {
        this.initDelegate();
    }
    
    @ContentView
    public AppCompatActivity(@LayoutRes final int n) {
        super(n);
        this.initDelegate();
    }
    
    private void initDelegate() {
        this.getSavedStateRegistry().registerSavedStateProvider("androidx:appcompat", (SavedStateRegistry.SavedStateProvider)new SavedStateRegistry.SavedStateProvider(this) {
            final AppCompatActivity this$0;
            
            @NonNull
            @Override
            public Bundle saveState() {
                final Bundle bundle = new Bundle();
                this.this$0.getDelegate().onSaveInstanceState(bundle);
                return bundle;
            }
        });
        this.addOnContextAvailableListener(new OnContextAvailableListener(this) {
            final AppCompatActivity this$0;
            
            @Override
            public void onContextAvailable(@NonNull final Context context) {
                final AppCompatDelegate delegate = this.this$0.getDelegate();
                delegate.installViewFactory();
                delegate.onCreate(this.this$0.getSavedStateRegistry().consumeRestoredStateForKey("androidx:appcompat"));
            }
        });
    }
    
    private void initViewTreeOwners() {
        ViewTreeLifecycleOwner.set(this.getWindow().getDecorView(), this);
        ViewTreeViewModelStoreOwner.set(this.getWindow().getDecorView(), this);
        ViewTreeSavedStateRegistryOwner.set(this.getWindow().getDecorView(), this);
        ViewTreeOnBackPressedDispatcherOwner.set(this.getWindow().getDecorView(), this);
    }
    
    private boolean performMenuItemShortcut(final KeyEvent keyEvent) {
        if (Build$VERSION.SDK_INT < 26 && !keyEvent.isCtrlPressed() && !KeyEvent.metaStateHasNoModifiers(keyEvent.getMetaState()) && keyEvent.getRepeatCount() == 0 && !KeyEvent.isModifierKey(keyEvent.getKeyCode())) {
            final Window window = this.getWindow();
            if (window != null && window.getDecorView() != null && window.getDecorView().dispatchKeyShortcutEvent(keyEvent)) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public void addContentView(final View view, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        this.initViewTreeOwners();
        this.getDelegate().addContentView(view, viewGroup$LayoutParams);
    }
    
    protected void attachBaseContext(final Context context) {
        super.attachBaseContext(this.getDelegate().attachBaseContext2(context));
    }
    
    public void closeOptionsMenu() {
        final ActionBar supportActionBar = this.getSupportActionBar();
        if (this.getWindow().hasFeature(0) && (supportActionBar == null || !supportActionBar.closeOptionsMenu())) {
            super.closeOptionsMenu();
        }
    }
    
    @Override
    public boolean dispatchKeyEvent(final KeyEvent keyEvent) {
        final int keyCode = keyEvent.getKeyCode();
        final ActionBar supportActionBar = this.getSupportActionBar();
        return (keyCode == 82 && supportActionBar != null && supportActionBar.onMenuKeyEvent(keyEvent)) || super.dispatchKeyEvent(keyEvent);
    }
    
    public <T extends View> T findViewById(@IdRes final int n) {
        return this.getDelegate().findViewById(n);
    }
    
    @NonNull
    public AppCompatDelegate getDelegate() {
        if (this.mDelegate == null) {
            this.mDelegate = AppCompatDelegate.create(this, this);
        }
        return this.mDelegate;
    }
    
    @Nullable
    @Override
    public Delegate getDrawerToggleDelegate() {
        return this.getDelegate().getDrawerToggleDelegate();
    }
    
    @NonNull
    public MenuInflater getMenuInflater() {
        return this.getDelegate().getMenuInflater();
    }
    
    public Resources getResources() {
        if (this.mResources == null && VectorEnabledTintResources.shouldBeUsed()) {
            this.mResources = new VectorEnabledTintResources((Context)this, super.getResources());
        }
        Resources resources;
        if ((resources = this.mResources) == null) {
            resources = super.getResources();
        }
        return resources;
    }
    
    @Nullable
    public ActionBar getSupportActionBar() {
        return this.getDelegate().getSupportActionBar();
    }
    
    @Nullable
    @Override
    public Intent getSupportParentActivityIntent() {
        return NavUtils.getParentActivityIntent(this);
    }
    
    public void invalidateOptionsMenu() {
        this.getDelegate().invalidateOptionsMenu();
    }
    
    @Override
    public void onConfigurationChanged(@NonNull Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.getDelegate().onConfigurationChanged(configuration);
        if (this.mResources != null) {
            configuration = super.getResources().getConfiguration();
            this.mResources.updateConfiguration(configuration, super.getResources().getDisplayMetrics());
        }
    }
    
    public void onContentChanged() {
        this.onSupportContentChanged();
    }
    
    public void onCreateSupportNavigateUpTaskStack(@NonNull final TaskStackBuilder taskStackBuilder) {
        taskStackBuilder.addParentStack(this);
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.getDelegate().onDestroy();
    }
    
    public boolean onKeyDown(final int n, final KeyEvent keyEvent) {
        return this.performMenuItemShortcut(keyEvent) || super.onKeyDown(n, keyEvent);
    }
    
    protected void onLocalesChanged(@NonNull final LocaleListCompat localeListCompat) {
    }
    
    @Override
    public final boolean onMenuItemSelected(final int n, @NonNull final MenuItem menuItem) {
        if (super.onMenuItemSelected(n, menuItem)) {
            return true;
        }
        final ActionBar supportActionBar = this.getSupportActionBar();
        return menuItem.getItemId() == 16908332 && supportActionBar != null && (supportActionBar.getDisplayOptions() & 0x4) != 0x0 && this.onSupportNavigateUp();
    }
    
    public boolean onMenuOpened(final int n, final Menu menu) {
        return super.onMenuOpened(n, menu);
    }
    
    protected void onNightModeChanged(final int n) {
    }
    
    @Override
    public void onPanelClosed(final int n, @NonNull final Menu menu) {
        super.onPanelClosed(n, menu);
    }
    
    protected void onPostCreate(@Nullable final Bundle bundle) {
        super.onPostCreate(bundle);
        this.getDelegate().onPostCreate(bundle);
    }
    
    @Override
    protected void onPostResume() {
        super.onPostResume();
        this.getDelegate().onPostResume();
    }
    
    public void onPrepareSupportNavigateUpTaskStack(@NonNull final TaskStackBuilder taskStackBuilder) {
    }
    
    @Override
    protected void onStart() {
        super.onStart();
        this.getDelegate().onStart();
    }
    
    @Override
    protected void onStop() {
        super.onStop();
        this.getDelegate().onStop();
    }
    
    @CallSuper
    @Override
    public void onSupportActionModeFinished(@NonNull final ActionMode actionMode) {
    }
    
    @CallSuper
    @Override
    public void onSupportActionModeStarted(@NonNull final ActionMode actionMode) {
    }
    
    @Deprecated
    public void onSupportContentChanged() {
    }
    
    public boolean onSupportNavigateUp() {
        final Intent supportParentActivityIntent = this.getSupportParentActivityIntent();
        if (supportParentActivityIntent != null) {
            if (this.supportShouldUpRecreateTask(supportParentActivityIntent)) {
                final TaskStackBuilder create = TaskStackBuilder.create((Context)this);
                this.onCreateSupportNavigateUpTaskStack(create);
                this.onPrepareSupportNavigateUpTaskStack(create);
                create.startActivities();
                try {
                    ActivityCompat.finishAffinity(this);
                }
                catch (final IllegalStateException ex) {
                    this.finish();
                }
            }
            else {
                this.supportNavigateUpTo(supportParentActivityIntent);
            }
            return true;
        }
        return false;
    }
    
    protected void onTitleChanged(final CharSequence title, final int n) {
        super.onTitleChanged(title, n);
        this.getDelegate().setTitle(title);
    }
    
    @Nullable
    @Override
    public ActionMode onWindowStartingSupportActionMode(@NonNull final ActionMode.Callback callback) {
        return null;
    }
    
    public void openOptionsMenu() {
        final ActionBar supportActionBar = this.getSupportActionBar();
        if (this.getWindow().hasFeature(0) && (supportActionBar == null || !supportActionBar.openOptionsMenu())) {
            super.openOptionsMenu();
        }
    }
    
    @Override
    public void setContentView(@LayoutRes final int contentView) {
        this.initViewTreeOwners();
        this.getDelegate().setContentView(contentView);
    }
    
    @Override
    public void setContentView(final View contentView) {
        this.initViewTreeOwners();
        this.getDelegate().setContentView(contentView);
    }
    
    @Override
    public void setContentView(final View view, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        this.initViewTreeOwners();
        this.getDelegate().setContentView(view, viewGroup$LayoutParams);
    }
    
    public void setSupportActionBar(@Nullable final Toolbar supportActionBar) {
        this.getDelegate().setSupportActionBar(supportActionBar);
    }
    
    @Deprecated
    public void setSupportProgress(final int n) {
    }
    
    @Deprecated
    public void setSupportProgressBarIndeterminate(final boolean b) {
    }
    
    @Deprecated
    public void setSupportProgressBarIndeterminateVisibility(final boolean b) {
    }
    
    @Deprecated
    public void setSupportProgressBarVisibility(final boolean b) {
    }
    
    public void setTheme(@StyleRes final int n) {
        super.setTheme(n);
        this.getDelegate().setTheme(n);
    }
    
    @Nullable
    public ActionMode startSupportActionMode(@NonNull final ActionMode.Callback callback) {
        return this.getDelegate().startSupportActionMode(callback);
    }
    
    @Override
    public void supportInvalidateOptionsMenu() {
        this.getDelegate().invalidateOptionsMenu();
    }
    
    public void supportNavigateUpTo(@NonNull final Intent intent) {
        NavUtils.navigateUpTo(this, intent);
    }
    
    public boolean supportRequestWindowFeature(final int n) {
        return this.getDelegate().requestWindowFeature(n);
    }
    
    public boolean supportShouldUpRecreateTask(@NonNull final Intent intent) {
        return NavUtils.shouldUpRecreateTask(this, intent);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.app;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.Executor;
import android.content.ComponentName;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.content.Context;

class AppLocalesStorageHelper
{
    static final String APPLICATION_LOCALES_RECORD_FILE = "androidx.appcompat.app.AppCompatDelegate.application_locales_record_file";
    static final String APP_LOCALES_META_DATA_HOLDER_SERVICE_NAME = "androidx.appcompat.app.AppLocalesMetadataHolderService";
    static final String LOCALE_RECORD_ATTRIBUTE_TAG = "application_locales";
    static final String LOCALE_RECORD_FILE_TAG = "locales";
    static final String TAG = "AppLocalesStorageHelper";
    
    private AppLocalesStorageHelper() {
    }
    
    static void persistLocales(@NonNull final Context p0, @NonNull final String p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: ldc             ""
        //     3: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //     6: ifeq            17
        //     9: aload_0        
        //    10: ldc             "androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //    12: invokevirtual   android/content/Context.deleteFile:(Ljava/lang/String;)Z
        //    15: pop            
        //    16: return         
        //    17: aload_0        
        //    18: ldc             "androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //    20: iconst_0       
        //    21: invokevirtual   android/content/Context.openFileOutput:(Ljava/lang/String;I)Ljava/io/FileOutputStream;
        //    24: astore_0       
        //    25: invokestatic    android/util/Xml.newSerializer:()Lorg/xmlpull/v1/XmlSerializer;
        //    28: astore_2       
        //    29: aload_2        
        //    30: aload_0        
        //    31: aconst_null    
        //    32: invokeinterface org/xmlpull/v1/XmlSerializer.setOutput:(Ljava/io/OutputStream;Ljava/lang/String;)V
        //    37: aload_2        
        //    38: ldc             "UTF-8"
        //    40: getstatic       java/lang/Boolean.TRUE:Ljava/lang/Boolean;
        //    43: invokeinterface org/xmlpull/v1/XmlSerializer.startDocument:(Ljava/lang/String;Ljava/lang/Boolean;)V
        //    48: aload_2        
        //    49: aconst_null    
        //    50: ldc             "locales"
        //    52: invokeinterface org/xmlpull/v1/XmlSerializer.startTag:(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
        //    57: pop            
        //    58: aload_2        
        //    59: aconst_null    
        //    60: ldc             "application_locales"
        //    62: aload_1        
        //    63: invokeinterface org/xmlpull/v1/XmlSerializer.attribute:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
        //    68: pop            
        //    69: aload_2        
        //    70: aconst_null    
        //    71: ldc             "locales"
        //    73: invokeinterface org/xmlpull/v1/XmlSerializer.endTag:(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
        //    78: pop            
        //    79: aload_2        
        //    80: invokeinterface org/xmlpull/v1/XmlSerializer.endDocument:()V
        //    85: new             Ljava/lang/StringBuilder;
        //    88: astore_2       
        //    89: aload_2        
        //    90: invokespecial   java/lang/StringBuilder.<init>:()V
        //    93: aload_2        
        //    94: ldc             "Storing App Locales : app-locales: "
        //    96: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    99: pop            
        //   100: aload_2        
        //   101: aload_1        
        //   102: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   105: pop            
        //   106: aload_2        
        //   107: ldc             " persisted successfully."
        //   109: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   112: pop            
        //   113: aload_0        
        //   114: ifnull          157
        //   117: aload_0        
        //   118: invokevirtual   java/io/FileOutputStream.close:()V
        //   121: goto            157
        //   124: astore_1       
        //   125: goto            158
        //   128: astore_2       
        //   129: new             Ljava/lang/StringBuilder;
        //   132: astore_2       
        //   133: aload_2        
        //   134: invokespecial   java/lang/StringBuilder.<init>:()V
        //   137: aload_2        
        //   138: ldc             "Storing App Locales : Failed to persist app-locales: "
        //   140: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   143: pop            
        //   144: aload_2        
        //   145: aload_1        
        //   146: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   149: pop            
        //   150: aload_0        
        //   151: ifnull          157
        //   154: goto            117
        //   157: return         
        //   158: aload_0        
        //   159: ifnull          166
        //   162: aload_0        
        //   163: invokevirtual   java/io/FileOutputStream.close:()V
        //   166: aload_1        
        //   167: athrow         
        //   168: astore_0       
        //   169: ldc             "Storing App Locales : FileNotFoundException: Cannot open file %s for writing "
        //   171: iconst_1       
        //   172: anewarray       Ljava/lang/Object;
        //   175: dup            
        //   176: iconst_0       
        //   177: ldc             "androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //   179: aastore        
        //   180: invokestatic    java/lang/String.format:(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //   183: pop            
        //   184: return         
        //   185: astore_0       
        //   186: goto            157
        //   189: astore_0       
        //   190: goto            166
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                           
        //  -----  -----  -----  -----  -------------------------------
        //  17     25     168    185    Ljava/io/FileNotFoundException;
        //  29     113    128    157    Ljava/lang/Exception;
        //  29     113    124    168    Any
        //  117    121    185    189    Ljava/io/IOException;
        //  129    150    124    168    Any
        //  162    166    189    193    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 103 out of bounds for length 103
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @NonNull
    static String readLocales(@NonNull final Context p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     2: astore_3       
        //     3: aload_0        
        //     4: ldc             "androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //     6: invokevirtual   android/content/Context.openFileInput:(Ljava/lang/String;)Ljava/io/FileInputStream;
        //     9: astore          6
        //    11: invokestatic    android/util/Xml.newPullParser:()Lorg/xmlpull/v1/XmlPullParser;
        //    14: astore          5
        //    16: aload           5
        //    18: aload           6
        //    20: ldc             "UTF-8"
        //    22: invokeinterface org/xmlpull/v1/XmlPullParser.setInput:(Ljava/io/InputStream;Ljava/lang/String;)V
        //    27: aload           5
        //    29: invokeinterface org/xmlpull/v1/XmlPullParser.getDepth:()I
        //    34: istore_2       
        //    35: aload           5
        //    37: invokeinterface org/xmlpull/v1/XmlPullParser.next:()I
        //    42: istore_1       
        //    43: aload_3        
        //    44: astore          4
        //    46: iload_1        
        //    47: iconst_1       
        //    48: if_icmpeq       110
        //    51: iload_1        
        //    52: iconst_3       
        //    53: if_icmpne       70
        //    56: aload_3        
        //    57: astore          4
        //    59: aload           5
        //    61: invokeinterface org/xmlpull/v1/XmlPullParser.getDepth:()I
        //    66: iload_2        
        //    67: if_icmple       110
        //    70: iload_1        
        //    71: iconst_3       
        //    72: if_icmpeq       35
        //    75: iload_1        
        //    76: iconst_4       
        //    77: if_icmpne       83
        //    80: goto            35
        //    83: aload           5
        //    85: invokeinterface org/xmlpull/v1/XmlPullParser.getName:()Ljava/lang/String;
        //    90: ldc             "locales"
        //    92: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    95: ifeq            35
        //    98: aload           5
        //   100: aconst_null    
        //   101: ldc             "application_locales"
        //   103: invokeinterface org/xmlpull/v1/XmlPullParser.getAttributeValue:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   108: astore          4
        //   110: aload           4
        //   112: astore          5
        //   114: aload           6
        //   116: ifnull          167
        //   119: aload           4
        //   121: astore_3       
        //   122: aload           6
        //   124: invokevirtual   java/io/FileInputStream.close:()V
        //   127: aload_3        
        //   128: astore          5
        //   130: goto            167
        //   133: astore          4
        //   135: aload_3        
        //   136: astore          5
        //   138: goto            167
        //   141: astore_0       
        //   142: aload           6
        //   144: ifnull          152
        //   147: aload           6
        //   149: invokevirtual   java/io/FileInputStream.close:()V
        //   152: aload_0        
        //   153: athrow         
        //   154: astore          4
        //   156: aload_3        
        //   157: astore          5
        //   159: aload           6
        //   161: ifnull          167
        //   164: goto            122
        //   167: aload           5
        //   169: invokevirtual   java/lang/String.isEmpty:()Z
        //   172: ifne            203
        //   175: new             Ljava/lang/StringBuilder;
        //   178: dup            
        //   179: invokespecial   java/lang/StringBuilder.<init>:()V
        //   182: astore_0       
        //   183: aload_0        
        //   184: ldc             "Reading app Locales : Locales read from file: androidx.appcompat.app.AppCompatDelegate.application_locales_record_file , appLocales: "
        //   186: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   189: pop            
        //   190: aload_0        
        //   191: aload           5
        //   193: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   196: pop            
        //   197: aload           5
        //   199: astore_3       
        //   200: goto            213
        //   203: aload_0        
        //   204: ldc             "androidx.appcompat.app.AppCompatDelegate.application_locales_record_file"
        //   206: invokevirtual   android/content/Context.deleteFile:(Ljava/lang/String;)Z
        //   209: pop            
        //   210: aload           5
        //   212: astore_3       
        //   213: aload_3        
        //   214: areturn        
        //   215: astore_0       
        //   216: goto            213
        //   219: astore_3       
        //   220: goto            152
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                   
        //  -----  -----  -----  -----  ---------------------------------------
        //  3      11     215    219    Ljava/io/FileNotFoundException;
        //  11     35     154    167    Lorg/xmlpull/v1/XmlPullParserException;
        //  11     35     154    167    Ljava/io/IOException;
        //  11     35     141    154    Any
        //  35     43     154    167    Lorg/xmlpull/v1/XmlPullParserException;
        //  35     43     154    167    Ljava/io/IOException;
        //  35     43     141    154    Any
        //  59     70     154    167    Lorg/xmlpull/v1/XmlPullParserException;
        //  59     70     154    167    Ljava/io/IOException;
        //  59     70     141    154    Any
        //  83     110    154    167    Lorg/xmlpull/v1/XmlPullParserException;
        //  83     110    154    167    Ljava/io/IOException;
        //  83     110    141    154    Any
        //  122    127    133    141    Ljava/io/IOException;
        //  147    152    219    223    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0152:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    static void syncLocalesToFramework(final Context context) {
        if (Build$VERSION.SDK_INT >= 33) {
            final ComponentName componentName = new ComponentName(context, "androidx.appcompat.app.AppLocalesMetadataHolderService");
            if (context.getPackageManager().getComponentEnabledSetting(componentName) != 1) {
                if (AppCompatDelegate.getApplicationLocales().isEmpty()) {
                    final String locales = readLocales(context);
                    final Object systemService = context.getSystemService("locale");
                    if (systemService != null) {
                        AppCompatDelegate.Api33Impl.localeManagerSetApplicationLocales(systemService, AppCompatDelegate.Api24Impl.localeListForLanguageTags(locales));
                    }
                }
                context.getPackageManager().setComponentEnabledSetting(componentName, 1, 1);
            }
        }
    }
    
    static class SerialExecutor implements Executor
    {
        Runnable mActive;
        final Executor mExecutor;
        private final Object mLock;
        final Queue<Runnable> mTasks;
        
        SerialExecutor(final Executor mExecutor) {
            this.mLock = new Object();
            this.mTasks = new ArrayDeque<Runnable>();
            this.mExecutor = mExecutor;
        }
        
        @Override
        public void execute(final Runnable runnable) {
            synchronized (this.mLock) {
                this.mTasks.add(new OO0o\u3007\u3007(this, runnable));
                if (this.mActive == null) {
                    this.scheduleNext();
                }
            }
        }
        
        protected void scheduleNext() {
            synchronized (this.mLock) {
                final Runnable mActive = this.mTasks.poll();
                this.mActive = mActive;
                if (mActive != null) {
                    this.mExecutor.execute(mActive);
                }
            }
        }
    }
    
    static class ThreadPerTaskExecutor implements Executor
    {
        @Override
        public void execute(final Runnable target) {
            new Thread(target).start();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.app;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import java.lang.reflect.Method;
import android.widget.ImageView;
import android.app.ActionBar;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.app.Activity;

class ActionBarDrawerToggleHoneycomb
{
    private static final String TAG = "ActionBarDrawerToggleHC";
    private static final int[] THEME_ATTRS;
    
    static {
        THEME_ATTRS = new int[] { 16843531 };
    }
    
    private ActionBarDrawerToggleHoneycomb() {
    }
    
    public static Drawable getThemeUpIndicator(final Activity activity) {
        final TypedArray obtainStyledAttributes = ((Context)activity).obtainStyledAttributes(ActionBarDrawerToggleHoneycomb.THEME_ATTRS);
        final Drawable drawable = obtainStyledAttributes.getDrawable(0);
        obtainStyledAttributes.recycle();
        return drawable;
    }
    
    public static SetIndicatorInfo setActionBarDescription(final SetIndicatorInfo setIndicatorInfo, final Activity activity, final int i) {
        SetIndicatorInfo setIndicatorInfo2 = setIndicatorInfo;
        if (setIndicatorInfo == null) {
            setIndicatorInfo2 = new SetIndicatorInfo(activity);
        }
        if (setIndicatorInfo2.setHomeAsUpIndicator == null) {
            return setIndicatorInfo2;
        }
        try {
            setIndicatorInfo2.setHomeActionContentDescription.invoke(activity.getActionBar(), i);
            return setIndicatorInfo2;
        }
        catch (final Exception ex) {
            return setIndicatorInfo2;
        }
    }
    
    public static SetIndicatorInfo setActionBarUpIndicator(final Activity activity, final Drawable imageDrawable, final int i) {
        final SetIndicatorInfo setIndicatorInfo = new SetIndicatorInfo(activity);
        Label_0061: {
            if (setIndicatorInfo.setHomeAsUpIndicator == null) {
                break Label_0061;
            }
            try {
                final ActionBar actionBar = activity.getActionBar();
                setIndicatorInfo.setHomeAsUpIndicator.invoke(actionBar, imageDrawable);
                setIndicatorInfo.setHomeActionContentDescription.invoke(actionBar, i);
                Label_0075: {
                    return setIndicatorInfo;
                }
                while (true) {
                    final ImageView upIndicatorView;
                    upIndicatorView.setImageDrawable(imageDrawable);
                    return setIndicatorInfo;
                    upIndicatorView = setIndicatorInfo.upIndicatorView;
                    iftrue(Label_0075:)(upIndicatorView == null);
                    continue;
                }
            }
            catch (final Exception ex) {
                return setIndicatorInfo;
            }
        }
    }
    
    static class SetIndicatorInfo
    {
        public Method setHomeActionContentDescription;
        public Method setHomeAsUpIndicator;
        public ImageView upIndicatorView;
        
        SetIndicatorInfo(final Activity activity) {
            try {
                this.setHomeAsUpIndicator = ActionBar.class.getDeclaredMethod("setHomeAsUpIndicator", Drawable.class);
                this.setHomeActionContentDescription = ActionBar.class.getDeclaredMethod("setHomeActionContentDescription", Integer.TYPE);
            }
            catch (final NoSuchMethodException ex) {
                final View viewById = activity.findViewById(16908332);
                if (viewById == null) {
                    return;
                }
                final ViewGroup viewGroup = (ViewGroup)viewById.getParent();
                if (viewGroup.getChildCount() != 2) {
                    return;
                }
                final View child = viewGroup.getChildAt(0);
                final View child2 = viewGroup.getChildAt(1);
                Object o = child;
                if (child.getId() == 16908332) {
                    o = child2;
                }
                if (o instanceof ImageView) {
                    this.upIndicatorView = (ImageView)o;
                }
            }
        }
    }
}

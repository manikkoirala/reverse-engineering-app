// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.view;

@Deprecated
public interface CollapsibleActionView
{
    void onActionViewCollapsed();
    
    void onActionViewExpanded();
}

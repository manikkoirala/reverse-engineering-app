// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.graphics.drawable;

import android.util.SparseArray;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import androidx.annotation.ColorInt;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.drawable.Drawable$ConstantState;
import android.graphics.Canvas;
import androidx.annotation.RequiresApi;
import androidx.annotation.NonNull;
import android.content.res.Resources$Theme;
import android.os.SystemClock;
import androidx.annotation.Nullable;
import android.content.res.Resources;
import android.os.Build$VERSION;
import androidx.core.graphics.drawable.DrawableCompat;
import android.graphics.Rect;
import android.graphics.drawable.Drawable$Callback;
import android.graphics.drawable.Drawable;

public class DrawableContainerCompat extends Drawable implements Drawable$Callback
{
    private static final boolean DEBUG = false;
    private static final boolean DEFAULT_DITHER = true;
    private static final String TAG = "DrawableContainerCompat";
    private int mAlpha;
    private Runnable mAnimationRunnable;
    private BlockInvalidateCallback mBlockInvalidateCallback;
    private int mCurIndex;
    private Drawable mCurrDrawable;
    private DrawableContainerState mDrawableContainerState;
    private long mEnterAnimationEnd;
    private long mExitAnimationEnd;
    private boolean mHasAlpha;
    private Rect mHotspotBounds;
    private Drawable mLastDrawable;
    private boolean mMutated;
    
    public DrawableContainerCompat() {
        this.mAlpha = 255;
        this.mCurIndex = -1;
    }
    
    private void initializeDrawableForDisplay(final Drawable drawable) {
        if (this.mBlockInvalidateCallback == null) {
            this.mBlockInvalidateCallback = new BlockInvalidateCallback();
        }
        drawable.setCallback((Drawable$Callback)this.mBlockInvalidateCallback.wrap(drawable.getCallback()));
        try {
            if (this.mDrawableContainerState.mEnterFadeDuration <= 0 && this.mHasAlpha) {
                drawable.setAlpha(this.mAlpha);
            }
            final DrawableContainerState mDrawableContainerState = this.mDrawableContainerState;
            if (mDrawableContainerState.mHasColorFilter) {
                drawable.setColorFilter(mDrawableContainerState.mColorFilter);
            }
            else {
                if (mDrawableContainerState.mHasTintList) {
                    DrawableCompat.setTintList(drawable, mDrawableContainerState.mTintList);
                }
                final DrawableContainerState mDrawableContainerState2 = this.mDrawableContainerState;
                if (mDrawableContainerState2.mHasTintMode) {
                    DrawableCompat.setTintMode(drawable, mDrawableContainerState2.mTintMode);
                }
            }
            drawable.setVisible(this.isVisible(), true);
            drawable.setDither(this.mDrawableContainerState.mDither);
            drawable.setState(this.getState());
            drawable.setLevel(this.getLevel());
            drawable.setBounds(this.getBounds());
            if (Build$VERSION.SDK_INT >= 23) {
                DrawableCompat.setLayoutDirection(drawable, DrawableCompat.getLayoutDirection(this));
            }
            DrawableCompat.setAutoMirrored(drawable, this.mDrawableContainerState.mAutoMirrored);
            final Rect mHotspotBounds = this.mHotspotBounds;
            if (mHotspotBounds != null) {
                DrawableCompat.setHotspotBounds(drawable, mHotspotBounds.left, mHotspotBounds.top, mHotspotBounds.right, mHotspotBounds.bottom);
            }
        }
        finally {
            drawable.setCallback(this.mBlockInvalidateCallback.unwrap());
        }
    }
    
    private boolean needsMirroring() {
        if (this.isAutoMirrored()) {
            final int layoutDirection = DrawableCompat.getLayoutDirection(this);
            final boolean b = true;
            if (layoutDirection == 1) {
                return b;
            }
        }
        return false;
    }
    
    static int resolveDensity(@Nullable final Resources resources, int densityDpi) {
        if (resources != null) {
            densityDpi = resources.getDisplayMetrics().densityDpi;
        }
        int n = densityDpi;
        if (densityDpi == 0) {
            n = 160;
        }
        return n;
    }
    
    void animate(final boolean b) {
        final int n = 1;
        this.mHasAlpha = true;
        final long uptimeMillis = SystemClock.uptimeMillis();
        final Drawable mCurrDrawable = this.mCurrDrawable;
        int n2 = 0;
        Label_0109: {
            if (mCurrDrawable != null) {
                final long mEnterAnimationEnd = this.mEnterAnimationEnd;
                if (mEnterAnimationEnd != 0L) {
                    if (mEnterAnimationEnd > uptimeMillis) {
                        mCurrDrawable.setAlpha((255 - (int)((mEnterAnimationEnd - uptimeMillis) * 255L) / this.mDrawableContainerState.mEnterFadeDuration) * this.mAlpha / 255);
                        n2 = 1;
                        break Label_0109;
                    }
                    mCurrDrawable.setAlpha(this.mAlpha);
                    this.mEnterAnimationEnd = 0L;
                }
            }
            else {
                this.mEnterAnimationEnd = 0L;
            }
            n2 = 0;
        }
        final Drawable mLastDrawable = this.mLastDrawable;
        if (mLastDrawable != null) {
            final long mExitAnimationEnd = this.mExitAnimationEnd;
            if (mExitAnimationEnd != 0L) {
                if (mExitAnimationEnd <= uptimeMillis) {
                    mLastDrawable.setVisible(false, false);
                    this.mLastDrawable = null;
                    this.mExitAnimationEnd = 0L;
                }
                else {
                    mLastDrawable.setAlpha((int)((mExitAnimationEnd - uptimeMillis) * 255L) / this.mDrawableContainerState.mExitFadeDuration * this.mAlpha / 255);
                    n2 = n;
                }
            }
        }
        else {
            this.mExitAnimationEnd = 0L;
        }
        if (b && n2 != 0) {
            this.scheduleSelf(this.mAnimationRunnable, uptimeMillis + 16L);
        }
    }
    
    @RequiresApi(21)
    public void applyTheme(@NonNull final Resources$Theme resources$Theme) {
        this.mDrawableContainerState.applyTheme(resources$Theme);
    }
    
    @RequiresApi(21)
    public boolean canApplyTheme() {
        return this.mDrawableContainerState.canApplyTheme();
    }
    
    void clearMutated() {
        this.mDrawableContainerState.clearMutated();
        this.mMutated = false;
    }
    
    DrawableContainerState cloneConstantState() {
        return this.mDrawableContainerState;
    }
    
    public void draw(@NonNull final Canvas canvas) {
        final Drawable mCurrDrawable = this.mCurrDrawable;
        if (mCurrDrawable != null) {
            mCurrDrawable.draw(canvas);
        }
        final Drawable mLastDrawable = this.mLastDrawable;
        if (mLastDrawable != null) {
            mLastDrawable.draw(canvas);
        }
    }
    
    public int getAlpha() {
        return this.mAlpha;
    }
    
    public int getChangingConfigurations() {
        return super.getChangingConfigurations() | this.mDrawableContainerState.getChangingConfigurations();
    }
    
    public final Drawable$ConstantState getConstantState() {
        if (this.mDrawableContainerState.canConstantState()) {
            this.mDrawableContainerState.mChangingConfigurations = this.getChangingConfigurations();
            return this.mDrawableContainerState;
        }
        return null;
    }
    
    @NonNull
    public Drawable getCurrent() {
        return this.mCurrDrawable;
    }
    
    int getCurrentIndex() {
        return this.mCurIndex;
    }
    
    public void getHotspotBounds(@NonNull final Rect rect) {
        final Rect mHotspotBounds = this.mHotspotBounds;
        if (mHotspotBounds != null) {
            rect.set(mHotspotBounds);
        }
        else {
            super.getHotspotBounds(rect);
        }
    }
    
    public int getIntrinsicHeight() {
        if (this.mDrawableContainerState.isConstantSize()) {
            return this.mDrawableContainerState.getConstantHeight();
        }
        final Drawable mCurrDrawable = this.mCurrDrawable;
        int intrinsicHeight;
        if (mCurrDrawable != null) {
            intrinsicHeight = mCurrDrawable.getIntrinsicHeight();
        }
        else {
            intrinsicHeight = -1;
        }
        return intrinsicHeight;
    }
    
    public int getIntrinsicWidth() {
        if (this.mDrawableContainerState.isConstantSize()) {
            return this.mDrawableContainerState.getConstantWidth();
        }
        final Drawable mCurrDrawable = this.mCurrDrawable;
        int intrinsicWidth;
        if (mCurrDrawable != null) {
            intrinsicWidth = mCurrDrawable.getIntrinsicWidth();
        }
        else {
            intrinsicWidth = -1;
        }
        return intrinsicWidth;
    }
    
    public int getMinimumHeight() {
        if (this.mDrawableContainerState.isConstantSize()) {
            return this.mDrawableContainerState.getConstantMinimumHeight();
        }
        final Drawable mCurrDrawable = this.mCurrDrawable;
        int minimumHeight;
        if (mCurrDrawable != null) {
            minimumHeight = mCurrDrawable.getMinimumHeight();
        }
        else {
            minimumHeight = 0;
        }
        return minimumHeight;
    }
    
    public int getMinimumWidth() {
        if (this.mDrawableContainerState.isConstantSize()) {
            return this.mDrawableContainerState.getConstantMinimumWidth();
        }
        final Drawable mCurrDrawable = this.mCurrDrawable;
        int minimumWidth;
        if (mCurrDrawable != null) {
            minimumWidth = mCurrDrawable.getMinimumWidth();
        }
        else {
            minimumWidth = 0;
        }
        return minimumWidth;
    }
    
    public int getOpacity() {
        final Drawable mCurrDrawable = this.mCurrDrawable;
        int opacity;
        if (mCurrDrawable != null && mCurrDrawable.isVisible()) {
            opacity = this.mDrawableContainerState.getOpacity();
        }
        else {
            opacity = -2;
        }
        return opacity;
    }
    
    @RequiresApi(21)
    public void getOutline(@NonNull final Outline outline) {
        final Drawable mCurrDrawable = this.mCurrDrawable;
        if (mCurrDrawable != null) {
            Api21Impl.getOutline(mCurrDrawable, outline);
        }
    }
    
    public boolean getPadding(@NonNull final Rect rect) {
        final Rect constantPadding = this.mDrawableContainerState.getConstantPadding();
        boolean b;
        if (constantPadding != null) {
            rect.set(constantPadding);
            b = ((constantPadding.right | (constantPadding.left | constantPadding.top | constantPadding.bottom)) != 0x0);
        }
        else {
            final Drawable mCurrDrawable = this.mCurrDrawable;
            if (mCurrDrawable != null) {
                b = mCurrDrawable.getPadding(rect);
            }
            else {
                b = super.getPadding(rect);
            }
        }
        if (this.needsMirroring()) {
            final int left = rect.left;
            rect.left = rect.right;
            rect.right = left;
        }
        return b;
    }
    
    public void invalidateDrawable(@NonNull final Drawable drawable) {
        final DrawableContainerState mDrawableContainerState = this.mDrawableContainerState;
        if (mDrawableContainerState != null) {
            mDrawableContainerState.invalidateCache();
        }
        if (drawable == this.mCurrDrawable && this.getCallback() != null) {
            this.getCallback().invalidateDrawable((Drawable)this);
        }
    }
    
    public boolean isAutoMirrored() {
        return this.mDrawableContainerState.mAutoMirrored;
    }
    
    public boolean isStateful() {
        return this.mDrawableContainerState.isStateful();
    }
    
    public void jumpToCurrentState() {
        final Drawable mLastDrawable = this.mLastDrawable;
        final int n = 1;
        int n2;
        if (mLastDrawable != null) {
            mLastDrawable.jumpToCurrentState();
            this.mLastDrawable = null;
            n2 = 1;
        }
        else {
            n2 = 0;
        }
        final Drawable mCurrDrawable = this.mCurrDrawable;
        if (mCurrDrawable != null) {
            mCurrDrawable.jumpToCurrentState();
            if (this.mHasAlpha) {
                this.mCurrDrawable.setAlpha(this.mAlpha);
            }
        }
        if (this.mExitAnimationEnd != 0L) {
            this.mExitAnimationEnd = 0L;
            n2 = 1;
        }
        if (this.mEnterAnimationEnd != 0L) {
            this.mEnterAnimationEnd = 0L;
            n2 = n;
        }
        if (n2 != 0) {
            this.invalidateSelf();
        }
    }
    
    public Drawable mutate() {
        if (!this.mMutated && super.mutate() == this) {
            final DrawableContainerState cloneConstantState = this.cloneConstantState();
            cloneConstantState.mutate();
            this.setConstantState(cloneConstantState);
            this.mMutated = true;
        }
        return this;
    }
    
    protected void onBoundsChange(final Rect rect) {
        final Drawable mLastDrawable = this.mLastDrawable;
        if (mLastDrawable != null) {
            mLastDrawable.setBounds(rect);
        }
        final Drawable mCurrDrawable = this.mCurrDrawable;
        if (mCurrDrawable != null) {
            mCurrDrawable.setBounds(rect);
        }
    }
    
    public boolean onLayoutDirectionChanged(final int n) {
        return this.mDrawableContainerState.setLayoutDirection(n, this.getCurrentIndex());
    }
    
    protected boolean onLevelChange(final int n) {
        final Drawable mLastDrawable = this.mLastDrawable;
        if (mLastDrawable != null) {
            return mLastDrawable.setLevel(n);
        }
        final Drawable mCurrDrawable = this.mCurrDrawable;
        return mCurrDrawable != null && mCurrDrawable.setLevel(n);
    }
    
    protected boolean onStateChange(@NonNull final int[] array) {
        final Drawable mLastDrawable = this.mLastDrawable;
        if (mLastDrawable != null) {
            return mLastDrawable.setState(array);
        }
        final Drawable mCurrDrawable = this.mCurrDrawable;
        return mCurrDrawable != null && mCurrDrawable.setState(array);
    }
    
    public void scheduleDrawable(@NonNull final Drawable drawable, @NonNull final Runnable runnable, final long n) {
        if (drawable == this.mCurrDrawable && this.getCallback() != null) {
            this.getCallback().scheduleDrawable((Drawable)this, runnable, n);
        }
    }
    
    boolean selectDrawable(int mEnterFadeDuration) {
        if (mEnterFadeDuration == this.mCurIndex) {
            return false;
        }
        final long uptimeMillis = SystemClock.uptimeMillis();
        if (this.mDrawableContainerState.mExitFadeDuration > 0) {
            final Drawable mLastDrawable = this.mLastDrawable;
            if (mLastDrawable != null) {
                mLastDrawable.setVisible(false, false);
            }
            final Drawable mCurrDrawable = this.mCurrDrawable;
            if (mCurrDrawable != null) {
                this.mLastDrawable = mCurrDrawable;
                this.mExitAnimationEnd = this.mDrawableContainerState.mExitFadeDuration + uptimeMillis;
            }
            else {
                this.mLastDrawable = null;
                this.mExitAnimationEnd = 0L;
            }
        }
        else {
            final Drawable mCurrDrawable2 = this.mCurrDrawable;
            if (mCurrDrawable2 != null) {
                mCurrDrawable2.setVisible(false, false);
            }
        }
        Label_0191: {
            if (mEnterFadeDuration >= 0) {
                final DrawableContainerState mDrawableContainerState = this.mDrawableContainerState;
                if (mEnterFadeDuration < mDrawableContainerState.mNumChildren) {
                    final Drawable child = mDrawableContainerState.getChild(mEnterFadeDuration);
                    this.mCurrDrawable = child;
                    this.mCurIndex = mEnterFadeDuration;
                    if (child != null) {
                        mEnterFadeDuration = this.mDrawableContainerState.mEnterFadeDuration;
                        if (mEnterFadeDuration > 0) {
                            this.mEnterAnimationEnd = uptimeMillis + mEnterFadeDuration;
                        }
                        this.initializeDrawableForDisplay(child);
                    }
                    break Label_0191;
                }
            }
            this.mCurrDrawable = null;
            this.mCurIndex = -1;
        }
        if (this.mEnterAnimationEnd != 0L || this.mExitAnimationEnd != 0L) {
            final Runnable mAnimationRunnable = this.mAnimationRunnable;
            if (mAnimationRunnable == null) {
                this.mAnimationRunnable = new Runnable(this) {
                    final DrawableContainerCompat this$0;
                    
                    @Override
                    public void run() {
                        this.this$0.animate(true);
                        this.this$0.invalidateSelf();
                    }
                };
            }
            else {
                this.unscheduleSelf(mAnimationRunnable);
            }
            this.animate(true);
        }
        this.invalidateSelf();
        return true;
    }
    
    public void setAlpha(final int n) {
        if (!this.mHasAlpha || this.mAlpha != n) {
            this.mHasAlpha = true;
            this.mAlpha = n;
            final Drawable mCurrDrawable = this.mCurrDrawable;
            if (mCurrDrawable != null) {
                if (this.mEnterAnimationEnd == 0L) {
                    mCurrDrawable.setAlpha(n);
                }
                else {
                    this.animate(false);
                }
            }
        }
    }
    
    public void setAutoMirrored(final boolean mAutoMirrored) {
        final DrawableContainerState mDrawableContainerState = this.mDrawableContainerState;
        if (mDrawableContainerState.mAutoMirrored != mAutoMirrored) {
            mDrawableContainerState.mAutoMirrored = mAutoMirrored;
            final Drawable mCurrDrawable = this.mCurrDrawable;
            if (mCurrDrawable != null) {
                DrawableCompat.setAutoMirrored(mCurrDrawable, mAutoMirrored);
            }
        }
    }
    
    public void setColorFilter(final ColorFilter colorFilter) {
        final DrawableContainerState mDrawableContainerState = this.mDrawableContainerState;
        mDrawableContainerState.mHasColorFilter = true;
        if (mDrawableContainerState.mColorFilter != colorFilter) {
            mDrawableContainerState.mColorFilter = colorFilter;
            final Drawable mCurrDrawable = this.mCurrDrawable;
            if (mCurrDrawable != null) {
                mCurrDrawable.setColorFilter(colorFilter);
            }
        }
    }
    
    void setConstantState(final DrawableContainerState mDrawableContainerState) {
        this.mDrawableContainerState = mDrawableContainerState;
        final int mCurIndex = this.mCurIndex;
        if (mCurIndex >= 0) {
            final Drawable child = mDrawableContainerState.getChild(mCurIndex);
            if ((this.mCurrDrawable = child) != null) {
                this.initializeDrawableForDisplay(child);
            }
        }
        this.mLastDrawable = null;
    }
    
    void setCurrentIndex(final int n) {
        this.selectDrawable(n);
    }
    
    public void setDither(final boolean b) {
        final DrawableContainerState mDrawableContainerState = this.mDrawableContainerState;
        if (mDrawableContainerState.mDither != b) {
            mDrawableContainerState.mDither = b;
            final Drawable mCurrDrawable = this.mCurrDrawable;
            if (mCurrDrawable != null) {
                mCurrDrawable.setDither(b);
            }
        }
    }
    
    public void setEnterFadeDuration(final int mEnterFadeDuration) {
        this.mDrawableContainerState.mEnterFadeDuration = mEnterFadeDuration;
    }
    
    public void setExitFadeDuration(final int mExitFadeDuration) {
        this.mDrawableContainerState.mExitFadeDuration = mExitFadeDuration;
    }
    
    public void setHotspot(final float n, final float n2) {
        final Drawable mCurrDrawable = this.mCurrDrawable;
        if (mCurrDrawable != null) {
            DrawableCompat.setHotspot(mCurrDrawable, n, n2);
        }
    }
    
    public void setHotspotBounds(final int n, final int n2, final int n3, final int n4) {
        final Rect mHotspotBounds = this.mHotspotBounds;
        if (mHotspotBounds == null) {
            this.mHotspotBounds = new Rect(n, n2, n3, n4);
        }
        else {
            mHotspotBounds.set(n, n2, n3, n4);
        }
        final Drawable mCurrDrawable = this.mCurrDrawable;
        if (mCurrDrawable != null) {
            DrawableCompat.setHotspotBounds(mCurrDrawable, n, n2, n3, n4);
        }
    }
    
    public void setTint(@ColorInt final int n) {
        this.setTintList(ColorStateList.valueOf(n));
    }
    
    public void setTintList(final ColorStateList mTintList) {
        final DrawableContainerState mDrawableContainerState = this.mDrawableContainerState;
        mDrawableContainerState.mHasTintList = true;
        if (mDrawableContainerState.mTintList != mTintList) {
            mDrawableContainerState.mTintList = mTintList;
            DrawableCompat.setTintList(this.mCurrDrawable, mTintList);
        }
    }
    
    public void setTintMode(@NonNull final PorterDuff$Mode mTintMode) {
        final DrawableContainerState mDrawableContainerState = this.mDrawableContainerState;
        mDrawableContainerState.mHasTintMode = true;
        if (mDrawableContainerState.mTintMode != mTintMode) {
            mDrawableContainerState.mTintMode = mTintMode;
            DrawableCompat.setTintMode(this.mCurrDrawable, mTintMode);
        }
    }
    
    public boolean setVisible(final boolean b, final boolean b2) {
        final boolean setVisible = super.setVisible(b, b2);
        final Drawable mLastDrawable = this.mLastDrawable;
        if (mLastDrawable != null) {
            mLastDrawable.setVisible(b, b2);
        }
        final Drawable mCurrDrawable = this.mCurrDrawable;
        if (mCurrDrawable != null) {
            mCurrDrawable.setVisible(b, b2);
        }
        return setVisible;
    }
    
    public void unscheduleDrawable(@NonNull final Drawable drawable, @NonNull final Runnable runnable) {
        if (drawable == this.mCurrDrawable && this.getCallback() != null) {
            this.getCallback().unscheduleDrawable((Drawable)this, runnable);
        }
    }
    
    final void updateDensity(final Resources resources) {
        this.mDrawableContainerState.updateDensity(resources);
    }
    
    @RequiresApi(21)
    private static class Api21Impl
    {
        public static boolean canApplyTheme(final Drawable$ConstantState drawable$ConstantState) {
            return drawable$ConstantState.canApplyTheme();
        }
        
        public static void getOutline(final Drawable drawable, final Outline outline) {
            drawable.getOutline(outline);
        }
        
        public static Resources getResources(final Resources$Theme resources$Theme) {
            return resources$Theme.getResources();
        }
    }
    
    static class BlockInvalidateCallback implements Drawable$Callback
    {
        private Drawable$Callback mCallback;
        
        public void invalidateDrawable(@NonNull final Drawable drawable) {
        }
        
        public void scheduleDrawable(@NonNull final Drawable drawable, @NonNull final Runnable runnable, final long n) {
            final Drawable$Callback mCallback = this.mCallback;
            if (mCallback != null) {
                mCallback.scheduleDrawable(drawable, runnable, n);
            }
        }
        
        public void unscheduleDrawable(@NonNull final Drawable drawable, @NonNull final Runnable runnable) {
            final Drawable$Callback mCallback = this.mCallback;
            if (mCallback != null) {
                mCallback.unscheduleDrawable(drawable, runnable);
            }
        }
        
        public Drawable$Callback unwrap() {
            final Drawable$Callback mCallback = this.mCallback;
            this.mCallback = null;
            return mCallback;
        }
        
        public BlockInvalidateCallback wrap(final Drawable$Callback mCallback) {
            this.mCallback = mCallback;
            return this;
        }
    }
    
    abstract static class DrawableContainerState extends Drawable$ConstantState
    {
        boolean mAutoMirrored;
        boolean mCanConstantState;
        int mChangingConfigurations;
        boolean mCheckedConstantSize;
        boolean mCheckedConstantState;
        boolean mCheckedOpacity;
        boolean mCheckedPadding;
        boolean mCheckedStateful;
        int mChildrenChangingConfigurations;
        ColorFilter mColorFilter;
        int mConstantHeight;
        int mConstantMinimumHeight;
        int mConstantMinimumWidth;
        Rect mConstantPadding;
        boolean mConstantSize;
        int mConstantWidth;
        int mDensity;
        boolean mDither;
        SparseArray<Drawable$ConstantState> mDrawableFutures;
        Drawable[] mDrawables;
        int mEnterFadeDuration;
        int mExitFadeDuration;
        boolean mHasColorFilter;
        boolean mHasTintList;
        boolean mHasTintMode;
        int mLayoutDirection;
        boolean mMutated;
        int mNumChildren;
        int mOpacity;
        final DrawableContainerCompat mOwner;
        Resources mSourceRes;
        boolean mStateful;
        ColorStateList mTintList;
        PorterDuff$Mode mTintMode;
        boolean mVariablePadding;
        
        DrawableContainerState(final DrawableContainerState drawableContainerState, final DrawableContainerCompat mOwner, final Resources resources) {
            final int n = 0;
            this.mVariablePadding = false;
            this.mConstantSize = false;
            this.mDither = true;
            this.mEnterFadeDuration = 0;
            this.mExitFadeDuration = 0;
            this.mOwner = mOwner;
            final Rect rect = null;
            Resources mSourceRes;
            if (resources != null) {
                mSourceRes = resources;
            }
            else if (drawableContainerState != null) {
                mSourceRes = drawableContainerState.mSourceRes;
            }
            else {
                mSourceRes = null;
            }
            this.mSourceRes = mSourceRes;
            int mDensity;
            if (drawableContainerState != null) {
                mDensity = drawableContainerState.mDensity;
            }
            else {
                mDensity = 0;
            }
            final int resolveDensity = DrawableContainerCompat.resolveDensity(resources, mDensity);
            this.mDensity = resolveDensity;
            if (drawableContainerState != null) {
                this.mChangingConfigurations = drawableContainerState.mChangingConfigurations;
                this.mChildrenChangingConfigurations = drawableContainerState.mChildrenChangingConfigurations;
                this.mCheckedConstantState = true;
                this.mCanConstantState = true;
                this.mVariablePadding = drawableContainerState.mVariablePadding;
                this.mConstantSize = drawableContainerState.mConstantSize;
                this.mDither = drawableContainerState.mDither;
                this.mMutated = drawableContainerState.mMutated;
                this.mLayoutDirection = drawableContainerState.mLayoutDirection;
                this.mEnterFadeDuration = drawableContainerState.mEnterFadeDuration;
                this.mExitFadeDuration = drawableContainerState.mExitFadeDuration;
                this.mAutoMirrored = drawableContainerState.mAutoMirrored;
                this.mColorFilter = drawableContainerState.mColorFilter;
                this.mHasColorFilter = drawableContainerState.mHasColorFilter;
                this.mTintList = drawableContainerState.mTintList;
                this.mTintMode = drawableContainerState.mTintMode;
                this.mHasTintList = drawableContainerState.mHasTintList;
                this.mHasTintMode = drawableContainerState.mHasTintMode;
                if (drawableContainerState.mDensity == resolveDensity) {
                    if (drawableContainerState.mCheckedPadding) {
                        Rect mConstantPadding = rect;
                        if (drawableContainerState.mConstantPadding != null) {
                            mConstantPadding = new Rect(drawableContainerState.mConstantPadding);
                        }
                        this.mConstantPadding = mConstantPadding;
                        this.mCheckedPadding = true;
                    }
                    if (drawableContainerState.mCheckedConstantSize) {
                        this.mConstantWidth = drawableContainerState.mConstantWidth;
                        this.mConstantHeight = drawableContainerState.mConstantHeight;
                        this.mConstantMinimumWidth = drawableContainerState.mConstantMinimumWidth;
                        this.mConstantMinimumHeight = drawableContainerState.mConstantMinimumHeight;
                        this.mCheckedConstantSize = true;
                    }
                }
                if (drawableContainerState.mCheckedOpacity) {
                    this.mOpacity = drawableContainerState.mOpacity;
                    this.mCheckedOpacity = true;
                }
                if (drawableContainerState.mCheckedStateful) {
                    this.mStateful = drawableContainerState.mStateful;
                    this.mCheckedStateful = true;
                }
                final Drawable[] mDrawables = drawableContainerState.mDrawables;
                this.mDrawables = new Drawable[mDrawables.length];
                this.mNumChildren = drawableContainerState.mNumChildren;
                final SparseArray<Drawable$ConstantState> mDrawableFutures = drawableContainerState.mDrawableFutures;
                if (mDrawableFutures != null) {
                    this.mDrawableFutures = (SparseArray<Drawable$ConstantState>)mDrawableFutures.clone();
                }
                else {
                    this.mDrawableFutures = (SparseArray<Drawable$ConstantState>)new SparseArray(this.mNumChildren);
                }
                for (int mNumChildren = this.mNumChildren, i = n; i < mNumChildren; ++i) {
                    final Drawable drawable = mDrawables[i];
                    if (drawable != null) {
                        final Drawable$ConstantState constantState = drawable.getConstantState();
                        if (constantState != null) {
                            this.mDrawableFutures.put(i, (Object)constantState);
                        }
                        else {
                            this.mDrawables[i] = mDrawables[i];
                        }
                    }
                }
            }
            else {
                this.mDrawables = new Drawable[10];
                this.mNumChildren = 0;
            }
        }
        
        private void createAllFutures() {
            final SparseArray<Drawable$ConstantState> mDrawableFutures = this.mDrawableFutures;
            if (mDrawableFutures != null) {
                for (int size = mDrawableFutures.size(), i = 0; i < size; ++i) {
                    this.mDrawables[this.mDrawableFutures.keyAt(i)] = this.prepareDrawable(((Drawable$ConstantState)this.mDrawableFutures.valueAt(i)).newDrawable(this.mSourceRes));
                }
                this.mDrawableFutures = null;
            }
        }
        
        private Drawable prepareDrawable(Drawable mutate) {
            if (Build$VERSION.SDK_INT >= 23) {
                DrawableCompat.setLayoutDirection(mutate, this.mLayoutDirection);
            }
            mutate = mutate.mutate();
            mutate.setCallback((Drawable$Callback)this.mOwner);
            return mutate;
        }
        
        public final int addChild(final Drawable drawable) {
            final int mNumChildren = this.mNumChildren;
            if (mNumChildren >= this.mDrawables.length) {
                this.growArray(mNumChildren, mNumChildren + 10);
            }
            drawable.mutate();
            drawable.setVisible(false, true);
            drawable.setCallback((Drawable$Callback)this.mOwner);
            this.mDrawables[mNumChildren] = drawable;
            ++this.mNumChildren;
            this.mChildrenChangingConfigurations |= drawable.getChangingConfigurations();
            this.invalidateCache();
            this.mConstantPadding = null;
            this.mCheckedPadding = false;
            this.mCheckedConstantSize = false;
            this.mCheckedConstantState = false;
            return mNumChildren;
        }
        
        @RequiresApi(21)
        final void applyTheme(final Resources$Theme resources$Theme) {
            if (resources$Theme != null) {
                this.createAllFutures();
                final int mNumChildren = this.mNumChildren;
                final Drawable[] mDrawables = this.mDrawables;
                for (int i = 0; i < mNumChildren; ++i) {
                    final Drawable drawable = mDrawables[i];
                    if (drawable != null && DrawableCompat.canApplyTheme(drawable)) {
                        DrawableCompat.applyTheme(mDrawables[i], resources$Theme);
                        this.mChildrenChangingConfigurations |= mDrawables[i].getChangingConfigurations();
                    }
                }
                this.updateDensity(Api21Impl.getResources(resources$Theme));
            }
        }
        
        @RequiresApi(21)
        public boolean canApplyTheme() {
            final int mNumChildren = this.mNumChildren;
            final Drawable[] mDrawables = this.mDrawables;
            for (int i = 0; i < mNumChildren; ++i) {
                final Drawable drawable = mDrawables[i];
                if (drawable != null) {
                    if (DrawableCompat.canApplyTheme(drawable)) {
                        return true;
                    }
                }
                else {
                    final Drawable$ConstantState drawable$ConstantState = (Drawable$ConstantState)this.mDrawableFutures.get(i);
                    if (drawable$ConstantState != null && Api21Impl.canApplyTheme(drawable$ConstantState)) {
                        return true;
                    }
                }
            }
            return false;
        }
        
        public boolean canConstantState() {
            if (this.mCheckedConstantState) {
                return this.mCanConstantState;
            }
            this.createAllFutures();
            this.mCheckedConstantState = true;
            final int mNumChildren = this.mNumChildren;
            final Drawable[] mDrawables = this.mDrawables;
            for (int i = 0; i < mNumChildren; ++i) {
                if (mDrawables[i].getConstantState() == null) {
                    return this.mCanConstantState = false;
                }
            }
            return this.mCanConstantState = true;
        }
        
        final void clearMutated() {
            this.mMutated = false;
        }
        
        protected void computeConstantSize() {
            this.mCheckedConstantSize = true;
            this.createAllFutures();
            final int mNumChildren = this.mNumChildren;
            final Drawable[] mDrawables = this.mDrawables;
            this.mConstantHeight = -1;
            this.mConstantWidth = -1;
            int i = 0;
            this.mConstantMinimumHeight = 0;
            this.mConstantMinimumWidth = 0;
            while (i < mNumChildren) {
                final Drawable drawable = mDrawables[i];
                final int intrinsicWidth = drawable.getIntrinsicWidth();
                if (intrinsicWidth > this.mConstantWidth) {
                    this.mConstantWidth = intrinsicWidth;
                }
                final int intrinsicHeight = drawable.getIntrinsicHeight();
                if (intrinsicHeight > this.mConstantHeight) {
                    this.mConstantHeight = intrinsicHeight;
                }
                final int minimumWidth = drawable.getMinimumWidth();
                if (minimumWidth > this.mConstantMinimumWidth) {
                    this.mConstantMinimumWidth = minimumWidth;
                }
                final int minimumHeight = drawable.getMinimumHeight();
                if (minimumHeight > this.mConstantMinimumHeight) {
                    this.mConstantMinimumHeight = minimumHeight;
                }
                ++i;
            }
        }
        
        final int getCapacity() {
            return this.mDrawables.length;
        }
        
        public int getChangingConfigurations() {
            return this.mChangingConfigurations | this.mChildrenChangingConfigurations;
        }
        
        public final Drawable getChild(final int n) {
            final Drawable drawable = this.mDrawables[n];
            if (drawable != null) {
                return drawable;
            }
            final SparseArray<Drawable$ConstantState> mDrawableFutures = this.mDrawableFutures;
            if (mDrawableFutures != null) {
                final int indexOfKey = mDrawableFutures.indexOfKey(n);
                if (indexOfKey >= 0) {
                    final Drawable prepareDrawable = this.prepareDrawable(((Drawable$ConstantState)this.mDrawableFutures.valueAt(indexOfKey)).newDrawable(this.mSourceRes));
                    this.mDrawables[n] = prepareDrawable;
                    this.mDrawableFutures.removeAt(indexOfKey);
                    if (this.mDrawableFutures.size() == 0) {
                        this.mDrawableFutures = null;
                    }
                    return prepareDrawable;
                }
            }
            return null;
        }
        
        public final int getChildCount() {
            return this.mNumChildren;
        }
        
        public final int getConstantHeight() {
            if (!this.mCheckedConstantSize) {
                this.computeConstantSize();
            }
            return this.mConstantHeight;
        }
        
        public final int getConstantMinimumHeight() {
            if (!this.mCheckedConstantSize) {
                this.computeConstantSize();
            }
            return this.mConstantMinimumHeight;
        }
        
        public final int getConstantMinimumWidth() {
            if (!this.mCheckedConstantSize) {
                this.computeConstantSize();
            }
            return this.mConstantMinimumWidth;
        }
        
        public final Rect getConstantPadding() {
            final boolean mVariablePadding = this.mVariablePadding;
            Rect mConstantPadding = null;
            if (mVariablePadding) {
                return null;
            }
            final Rect mConstantPadding2 = this.mConstantPadding;
            if (mConstantPadding2 == null && !this.mCheckedPadding) {
                this.createAllFutures();
                final Rect rect = new Rect();
                final int mNumChildren = this.mNumChildren;
                final Drawable[] mDrawables = this.mDrawables;
                Rect rect2;
                for (int i = 0; i < mNumChildren; ++i, mConstantPadding = rect2) {
                    rect2 = mConstantPadding;
                    if (mDrawables[i].getPadding(rect)) {
                        Rect rect3;
                        if ((rect3 = mConstantPadding) == null) {
                            rect3 = new Rect(0, 0, 0, 0);
                        }
                        final int left = rect.left;
                        if (left > rect3.left) {
                            rect3.left = left;
                        }
                        final int top = rect.top;
                        if (top > rect3.top) {
                            rect3.top = top;
                        }
                        final int right = rect.right;
                        if (right > rect3.right) {
                            rect3.right = right;
                        }
                        final int bottom = rect.bottom;
                        rect2 = rect3;
                        if (bottom > rect3.bottom) {
                            rect3.bottom = bottom;
                            rect2 = rect3;
                        }
                    }
                }
                this.mCheckedPadding = true;
                return this.mConstantPadding = mConstantPadding;
            }
            return mConstantPadding2;
        }
        
        public final int getConstantWidth() {
            if (!this.mCheckedConstantSize) {
                this.computeConstantSize();
            }
            return this.mConstantWidth;
        }
        
        public final int getEnterFadeDuration() {
            return this.mEnterFadeDuration;
        }
        
        public final int getExitFadeDuration() {
            return this.mExitFadeDuration;
        }
        
        public final int getOpacity() {
            if (this.mCheckedOpacity) {
                return this.mOpacity;
            }
            this.createAllFutures();
            final int mNumChildren = this.mNumChildren;
            final Drawable[] mDrawables = this.mDrawables;
            int mOpacity;
            if (mNumChildren > 0) {
                mOpacity = mDrawables[0].getOpacity();
            }
            else {
                mOpacity = -2;
            }
            for (int i = 1; i < mNumChildren; ++i) {
                mOpacity = Drawable.resolveOpacity(mOpacity, mDrawables[i].getOpacity());
            }
            this.mOpacity = mOpacity;
            this.mCheckedOpacity = true;
            return mOpacity;
        }
        
        public void growArray(final int n, final int n2) {
            final Drawable[] mDrawables = new Drawable[n2];
            final Drawable[] mDrawables2 = this.mDrawables;
            if (mDrawables2 != null) {
                System.arraycopy(mDrawables2, 0, mDrawables, 0, n);
            }
            this.mDrawables = mDrawables;
        }
        
        void invalidateCache() {
            this.mCheckedOpacity = false;
            this.mCheckedStateful = false;
        }
        
        public final boolean isConstantSize() {
            return this.mConstantSize;
        }
        
        public final boolean isStateful() {
            if (this.mCheckedStateful) {
                return this.mStateful;
            }
            this.createAllFutures();
            final int mNumChildren = this.mNumChildren;
            final Drawable[] mDrawables = this.mDrawables;
            final boolean b = false;
            int n = 0;
            boolean mStateful;
            while (true) {
                mStateful = b;
                if (n >= mNumChildren) {
                    break;
                }
                if (mDrawables[n].isStateful()) {
                    mStateful = true;
                    break;
                }
                ++n;
            }
            this.mStateful = mStateful;
            this.mCheckedStateful = true;
            return mStateful;
        }
        
        void mutate() {
            final int mNumChildren = this.mNumChildren;
            for (final Drawable drawable : this.mDrawables) {
                if (drawable != null) {
                    drawable.mutate();
                }
            }
            this.mMutated = true;
        }
        
        public final void setConstantSize(final boolean mConstantSize) {
            this.mConstantSize = mConstantSize;
        }
        
        public final void setEnterFadeDuration(final int mEnterFadeDuration) {
            this.mEnterFadeDuration = mEnterFadeDuration;
        }
        
        public final void setExitFadeDuration(final int mExitFadeDuration) {
            this.mExitFadeDuration = mExitFadeDuration;
        }
        
        final boolean setLayoutDirection(final int mLayoutDirection, final int n) {
            final int mNumChildren = this.mNumChildren;
            final Drawable[] mDrawables = this.mDrawables;
            int i = 0;
            boolean b = false;
            while (i < mNumChildren) {
                final Drawable drawable = mDrawables[i];
                boolean b2 = b;
                if (drawable != null) {
                    final boolean b3 = Build$VERSION.SDK_INT >= 23 && DrawableCompat.setLayoutDirection(drawable, mLayoutDirection);
                    b2 = b;
                    if (i == n) {
                        b2 = b3;
                    }
                }
                ++i;
                b = b2;
            }
            this.mLayoutDirection = mLayoutDirection;
            return b;
        }
        
        public final void setVariablePadding(final boolean mVariablePadding) {
            this.mVariablePadding = mVariablePadding;
        }
        
        final void updateDensity(final Resources mSourceRes) {
            if (mSourceRes != null) {
                this.mSourceRes = mSourceRes;
                if (this.mDensity != (this.mDensity = DrawableContainerCompat.resolveDensity(mSourceRes, this.mDensity))) {
                    this.mCheckedConstantSize = false;
                    this.mCheckedPadding = false;
                }
            }
        }
    }
}

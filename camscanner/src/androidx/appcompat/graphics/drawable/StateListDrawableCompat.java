// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.graphics.drawable;

import android.util.StateSet;
import androidx.annotation.RequiresApi;
import androidx.annotation.NonNull;
import java.io.IOException;
import android.graphics.drawable.Drawable;
import android.content.res.TypedArray;
import androidx.appcompat.resources.Compatibility;
import org.xmlpull.v1.XmlPullParserException;
import androidx.appcompat.widget.ResourceManagerInternal;
import androidx.core.content.res.TypedArrayUtils;
import androidx.appcompat.resources.R;
import android.content.res.Resources$Theme;
import android.util.AttributeSet;
import org.xmlpull.v1.XmlPullParser;
import android.content.Context;
import androidx.annotation.Nullable;
import android.content.res.Resources;

public class StateListDrawableCompat extends DrawableContainerCompat
{
    private static final boolean DEBUG = false;
    private static final String TAG = "StateListDrawableCompat";
    private boolean mMutated;
    private StateListState mStateListState;
    
    public StateListDrawableCompat() {
        this(null, null);
    }
    
    StateListDrawableCompat(@Nullable final StateListState constantState) {
        if (constantState != null) {
            this.setConstantState(constantState);
        }
    }
    
    StateListDrawableCompat(final StateListState stateListState, final Resources resources) {
        this.setConstantState(new StateListState(stateListState, this, resources));
        this.onStateChange(this.getState());
    }
    
    private void inflateChildElements(final Context context, final Resources resources, final XmlPullParser xmlPullParser, final AttributeSet set, final Resources$Theme resources$Theme) throws XmlPullParserException, IOException {
        final StateListState mStateListState = this.mStateListState;
        final int n = xmlPullParser.getDepth() + 1;
        while (true) {
            final int next = xmlPullParser.next();
            if (next == 1) {
                break;
            }
            final int depth = xmlPullParser.getDepth();
            if (depth < n && next == 3) {
                break;
            }
            if (next != 2) {
                continue;
            }
            if (depth > n) {
                continue;
            }
            if (!xmlPullParser.getName().equals("item")) {
                continue;
            }
            final TypedArray obtainAttributes = TypedArrayUtils.obtainAttributes(resources, resources$Theme, set, R.styleable.StateListDrawableItem);
            final int resourceId = obtainAttributes.getResourceId(R.styleable.StateListDrawableItem_android_drawable, -1);
            Drawable drawable;
            if (resourceId > 0) {
                drawable = ResourceManagerInternal.get().getDrawable(context, resourceId);
            }
            else {
                drawable = null;
            }
            obtainAttributes.recycle();
            final int[] stateSet = this.extractStateSet(set);
            Drawable fromXmlInner = drawable;
            if (drawable == null) {
                int next2;
                do {
                    next2 = xmlPullParser.next();
                } while (next2 == 4);
                if (next2 != 2) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(xmlPullParser.getPositionDescription());
                    sb.append(": <item> tag requires a 'drawable' attribute or child tag defining a drawable");
                    throw new XmlPullParserException(sb.toString());
                }
                fromXmlInner = Compatibility.Api21Impl.createFromXmlInner(resources, xmlPullParser, set, resources$Theme);
            }
            mStateListState.addStateSet(stateSet, fromXmlInner);
        }
    }
    
    private void updateStateFromTypedArray(final TypedArray typedArray) {
        final StateListState mStateListState = this.mStateListState;
        mStateListState.mChangingConfigurations |= Compatibility.Api21Impl.getChangingConfigurations(typedArray);
        mStateListState.mVariablePadding = typedArray.getBoolean(R.styleable.StateListDrawable_android_variablePadding, mStateListState.mVariablePadding);
        mStateListState.mConstantSize = typedArray.getBoolean(R.styleable.StateListDrawable_android_constantSize, mStateListState.mConstantSize);
        mStateListState.mEnterFadeDuration = typedArray.getInt(R.styleable.StateListDrawable_android_enterFadeDuration, mStateListState.mEnterFadeDuration);
        mStateListState.mExitFadeDuration = typedArray.getInt(R.styleable.StateListDrawable_android_exitFadeDuration, mStateListState.mExitFadeDuration);
        mStateListState.mDither = typedArray.getBoolean(R.styleable.StateListDrawable_android_dither, mStateListState.mDither);
    }
    
    public void addState(final int[] array, final Drawable drawable) {
        if (drawable != null) {
            this.mStateListState.addStateSet(array, drawable);
            this.onStateChange(this.getState());
        }
    }
    
    @RequiresApi(21)
    @Override
    public void applyTheme(@NonNull final Resources$Theme resources$Theme) {
        super.applyTheme(resources$Theme);
        this.onStateChange(this.getState());
    }
    
    @Override
    void clearMutated() {
        super.clearMutated();
        this.mMutated = false;
    }
    
    StateListState cloneConstantState() {
        return new StateListState(this.mStateListState, this, null);
    }
    
    int[] extractStateSet(final AttributeSet set) {
        final int attributeCount = set.getAttributeCount();
        final int[] array = new int[attributeCount];
        int i = 0;
        int n = 0;
        while (i < attributeCount) {
            final int attributeNameResource = set.getAttributeNameResource(i);
            int n2 = n;
            if (attributeNameResource != 0) {
                n2 = n;
                if (attributeNameResource != 16842960) {
                    n2 = n;
                    if (attributeNameResource != 16843161) {
                        int n3;
                        if (set.getAttributeBooleanValue(i, false)) {
                            n3 = attributeNameResource;
                        }
                        else {
                            n3 = -attributeNameResource;
                        }
                        array[n] = n3;
                        n2 = n + 1;
                    }
                }
            }
            ++i;
            n = n2;
        }
        return StateSet.trimStateSet(array, n);
    }
    
    int getStateCount() {
        return ((DrawableContainerState)this.mStateListState).getChildCount();
    }
    
    Drawable getStateDrawable(final int n) {
        return ((DrawableContainerState)this.mStateListState).getChild(n);
    }
    
    int getStateDrawableIndex(final int[] array) {
        return this.mStateListState.indexOfStateSet(array);
    }
    
    StateListState getStateListState() {
        return this.mStateListState;
    }
    
    int[] getStateSet(final int n) {
        return this.mStateListState.mStateSets[n];
    }
    
    public void inflate(@NonNull final Context context, @NonNull final Resources resources, @NonNull final XmlPullParser xmlPullParser, @NonNull final AttributeSet set, @Nullable final Resources$Theme resources$Theme) throws XmlPullParserException, IOException {
        final TypedArray obtainAttributes = TypedArrayUtils.obtainAttributes(resources, resources$Theme, set, R.styleable.StateListDrawable);
        this.setVisible(obtainAttributes.getBoolean(R.styleable.StateListDrawable_android_visible, true), true);
        this.updateStateFromTypedArray(obtainAttributes);
        this.updateDensity(resources);
        obtainAttributes.recycle();
        this.inflateChildElements(context, resources, xmlPullParser, set, resources$Theme);
        this.onStateChange(this.getState());
    }
    
    @Override
    public boolean isStateful() {
        return true;
    }
    
    @NonNull
    @Override
    public Drawable mutate() {
        if (!this.mMutated && super.mutate() == this) {
            this.mStateListState.mutate();
            this.mMutated = true;
        }
        return this;
    }
    
    @Override
    protected boolean onStateChange(@NonNull final int[] array) {
        final boolean onStateChange = super.onStateChange(array);
        int n;
        if ((n = this.mStateListState.indexOfStateSet(array)) < 0) {
            n = this.mStateListState.indexOfStateSet(StateSet.WILD_CARD);
        }
        return this.selectDrawable(n) || onStateChange;
    }
    
    @Override
    void setConstantState(@NonNull final DrawableContainerState constantState) {
        super.setConstantState(constantState);
        if (constantState instanceof StateListState) {
            this.mStateListState = (StateListState)constantState;
        }
    }
    
    static class StateListState extends DrawableContainerState
    {
        int[][] mStateSets;
        
        StateListState(final StateListState stateListState, final StateListDrawableCompat stateListDrawableCompat, final Resources resources) {
            super((DrawableContainerState)stateListState, stateListDrawableCompat, resources);
            if (stateListState != null) {
                this.mStateSets = stateListState.mStateSets;
            }
            else {
                this.mStateSets = new int[((DrawableContainerState)this).getCapacity()][];
            }
        }
        
        int addStateSet(final int[] array, final Drawable drawable) {
            final int addChild = ((DrawableContainerState)this).addChild(drawable);
            this.mStateSets[addChild] = array;
            return addChild;
        }
        
        @Override
        public void growArray(final int n, final int n2) {
            super.growArray(n, n2);
            final int[][] mStateSets = new int[n2][];
            System.arraycopy(this.mStateSets, 0, mStateSets, 0, n);
            this.mStateSets = mStateSets;
        }
        
        int indexOfStateSet(final int[] array) {
            final int[][] mStateSets = this.mStateSets;
            for (int childCount = ((DrawableContainerState)this).getChildCount(), i = 0; i < childCount; ++i) {
                if (StateSet.stateSetMatches(mStateSets[i], array)) {
                    return i;
                }
            }
            return -1;
        }
        
        @Override
        void mutate() {
            final int[][] mStateSets = this.mStateSets;
            final int[][] mStateSets2 = new int[mStateSets.length][];
            for (int i = mStateSets.length - 1; i >= 0; --i) {
                final int[] array = this.mStateSets[i];
                int[] array2;
                if (array != null) {
                    array2 = array.clone();
                }
                else {
                    array2 = null;
                }
                mStateSets2[i] = array2;
            }
            this.mStateSets = mStateSets2;
        }
        
        @NonNull
        public Drawable newDrawable() {
            return new StateListDrawableCompat(this, null);
        }
        
        @NonNull
        public Drawable newDrawable(final Resources resources) {
            return new StateListDrawableCompat(this, resources);
        }
    }
}

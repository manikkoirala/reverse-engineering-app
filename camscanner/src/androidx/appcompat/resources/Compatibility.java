// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.resources;

import android.content.res.TypedArray;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;
import android.content.res.Resources$Theme;
import android.util.AttributeSet;
import org.xmlpull.v1.XmlPullParser;
import android.animation.ObjectAnimator;
import androidx.annotation.DoNotInline;
import android.util.TypedValue;
import androidx.annotation.NonNull;
import android.content.res.Resources;
import androidx.annotation.RequiresApi;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
public final class Compatibility
{
    private Compatibility() {
    }
    
    @RequiresApi(15)
    public static class Api15Impl
    {
        private Api15Impl() {
        }
        
        @DoNotInline
        public static void getValueForDensity(@NonNull final Resources resources, final int n, final int n2, @NonNull final TypedValue typedValue, final boolean b) {
            resources.getValueForDensity(n, n2, typedValue, b);
        }
    }
    
    @RequiresApi(18)
    public static class Api18Impl
    {
        private Api18Impl() {
        }
        
        @DoNotInline
        public static void setAutoCancel(@NonNull final ObjectAnimator objectAnimator, final boolean autoCancel) {
            objectAnimator.setAutoCancel(autoCancel);
        }
    }
    
    @RequiresApi(21)
    public static class Api21Impl
    {
        private Api21Impl() {
        }
        
        @DoNotInline
        @NonNull
        public static Drawable createFromXmlInner(@NonNull final Resources resources, @NonNull final XmlPullParser xmlPullParser, @NonNull final AttributeSet set, @Nullable final Resources$Theme resources$Theme) throws IOException, XmlPullParserException {
            return Drawable.createFromXmlInner(resources, xmlPullParser, set, resources$Theme);
        }
        
        @DoNotInline
        public static int getChangingConfigurations(@NonNull final TypedArray typedArray) {
            return typedArray.getChangingConfigurations();
        }
        
        @DoNotInline
        public static void inflate(@NonNull final Drawable drawable, @NonNull final Resources resources, @NonNull final XmlPullParser xmlPullParser, @NonNull final AttributeSet set, @Nullable final Resources$Theme resources$Theme) throws IOException, XmlPullParserException {
            drawable.inflate(resources, xmlPullParser, set, resources$Theme);
        }
    }
}

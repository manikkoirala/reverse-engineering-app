// 
// Decompiled by Procyon v0.6.0
// 

package androidx.appcompat.content.res;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.ResourceManagerInternal;
import android.graphics.drawable.Drawable;
import androidx.annotation.DrawableRes;
import androidx.core.content.ContextCompat;
import android.content.res.ColorStateList;
import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import android.content.Context;
import android.annotation.SuppressLint;

@SuppressLint({ "RestrictedAPI" })
public final class AppCompatResources
{
    private AppCompatResources() {
    }
    
    public static ColorStateList getColorStateList(@NonNull final Context context, @ColorRes final int n) {
        return ContextCompat.getColorStateList(context, n);
    }
    
    @Nullable
    public static Drawable getDrawable(@NonNull final Context context, @DrawableRes final int n) {
        return ResourceManagerInternal.get().getDrawable(context, n);
    }
}

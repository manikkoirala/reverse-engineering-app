// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import java.util.Collections;
import androidx.lifecycle.LiveData;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.annotation.RestrictTo;
import androidx.annotation.NonNull;
import java.util.List;

public abstract class WorkContinuation
{
    @NonNull
    public static WorkContinuation combine(@NonNull final List<WorkContinuation> list) {
        return list.get(0).combineInternal(list);
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected abstract WorkContinuation combineInternal(@NonNull final List<WorkContinuation> p0);
    
    @NonNull
    public abstract Operation enqueue();
    
    @NonNull
    public abstract ListenableFuture<List<WorkInfo>> getWorkInfos();
    
    @NonNull
    public abstract LiveData<List<WorkInfo>> getWorkInfosLiveData();
    
    @NonNull
    public final WorkContinuation then(@NonNull final OneTimeWorkRequest o) {
        return this.then(Collections.singletonList(o));
    }
    
    @NonNull
    public abstract WorkContinuation then(@NonNull final List<OneTimeWorkRequest> p0);
}

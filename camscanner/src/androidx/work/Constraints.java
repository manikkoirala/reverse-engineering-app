// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import androidx.lifecycle.\u3007080;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import android.net.Uri;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.annotation.NonNull;
import android.os.Build$VERSION;
import androidx.annotation.RestrictTo;
import androidx.room.ColumnInfo;

public final class Constraints
{
    public static final Constraints NONE;
    @ColumnInfo(name = "content_uri_triggers")
    private ContentUriTriggers mContentUriTriggers;
    @ColumnInfo(name = "required_network_type")
    private NetworkType mRequiredNetworkType;
    @ColumnInfo(name = "requires_battery_not_low")
    private boolean mRequiresBatteryNotLow;
    @ColumnInfo(name = "requires_charging")
    private boolean mRequiresCharging;
    @ColumnInfo(name = "requires_device_idle")
    private boolean mRequiresDeviceIdle;
    @ColumnInfo(name = "requires_storage_not_low")
    private boolean mRequiresStorageNotLow;
    @ColumnInfo(name = "trigger_content_update_delay")
    private long mTriggerContentUpdateDelay;
    @ColumnInfo(name = "trigger_max_content_delay")
    private long mTriggerMaxContentDelay;
    
    static {
        NONE = new Builder().build();
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public Constraints() {
        this.mRequiredNetworkType = NetworkType.NOT_REQUIRED;
        this.mTriggerContentUpdateDelay = -1L;
        this.mTriggerMaxContentDelay = -1L;
        this.mContentUriTriggers = new ContentUriTriggers();
    }
    
    Constraints(final Builder builder) {
        this.mRequiredNetworkType = NetworkType.NOT_REQUIRED;
        this.mTriggerContentUpdateDelay = -1L;
        this.mTriggerMaxContentDelay = -1L;
        this.mContentUriTriggers = new ContentUriTriggers();
        this.mRequiresCharging = builder.mRequiresCharging;
        final int sdk_INT = Build$VERSION.SDK_INT;
        this.mRequiresDeviceIdle = (sdk_INT >= 23 && builder.mRequiresDeviceIdle);
        this.mRequiredNetworkType = builder.mRequiredNetworkType;
        this.mRequiresBatteryNotLow = builder.mRequiresBatteryNotLow;
        this.mRequiresStorageNotLow = builder.mRequiresStorageNotLow;
        if (sdk_INT >= 24) {
            this.mContentUriTriggers = builder.mContentUriTriggers;
            this.mTriggerContentUpdateDelay = builder.mTriggerContentUpdateDelay;
            this.mTriggerMaxContentDelay = builder.mTriggerContentMaxDelay;
        }
    }
    
    public Constraints(@NonNull final Constraints constraints) {
        this.mRequiredNetworkType = NetworkType.NOT_REQUIRED;
        this.mTriggerContentUpdateDelay = -1L;
        this.mTriggerMaxContentDelay = -1L;
        this.mContentUriTriggers = new ContentUriTriggers();
        this.mRequiresCharging = constraints.mRequiresCharging;
        this.mRequiresDeviceIdle = constraints.mRequiresDeviceIdle;
        this.mRequiredNetworkType = constraints.mRequiredNetworkType;
        this.mRequiresBatteryNotLow = constraints.mRequiresBatteryNotLow;
        this.mRequiresStorageNotLow = constraints.mRequiresStorageNotLow;
        this.mContentUriTriggers = constraints.mContentUriTriggers;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && Constraints.class == o.getClass()) {
            final Constraints constraints = (Constraints)o;
            return this.mRequiresCharging == constraints.mRequiresCharging && this.mRequiresDeviceIdle == constraints.mRequiresDeviceIdle && this.mRequiresBatteryNotLow == constraints.mRequiresBatteryNotLow && this.mRequiresStorageNotLow == constraints.mRequiresStorageNotLow && this.mTriggerContentUpdateDelay == constraints.mTriggerContentUpdateDelay && this.mTriggerMaxContentDelay == constraints.mTriggerMaxContentDelay && this.mRequiredNetworkType == constraints.mRequiredNetworkType && this.mContentUriTriggers.equals(constraints.mContentUriTriggers);
        }
        return false;
    }
    
    @NonNull
    @RequiresApi(24)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public ContentUriTriggers getContentUriTriggers() {
        return this.mContentUriTriggers;
    }
    
    @NonNull
    public NetworkType getRequiredNetworkType() {
        return this.mRequiredNetworkType;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public long getTriggerContentUpdateDelay() {
        return this.mTriggerContentUpdateDelay;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public long getTriggerMaxContentDelay() {
        return this.mTriggerMaxContentDelay;
    }
    
    @RequiresApi(24)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public boolean hasContentUriTriggers() {
        return this.mContentUriTriggers.size() > 0;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.mRequiredNetworkType.hashCode();
        final int mRequiresCharging = this.mRequiresCharging ? 1 : 0;
        final int mRequiresDeviceIdle = this.mRequiresDeviceIdle ? 1 : 0;
        final int mRequiresBatteryNotLow = this.mRequiresBatteryNotLow ? 1 : 0;
        final int mRequiresStorageNotLow = this.mRequiresStorageNotLow ? 1 : 0;
        final long mTriggerContentUpdateDelay = this.mTriggerContentUpdateDelay;
        final int n = (int)(mTriggerContentUpdateDelay ^ mTriggerContentUpdateDelay >>> 32);
        final long mTriggerMaxContentDelay = this.mTriggerMaxContentDelay;
        return ((((((hashCode * 31 + mRequiresCharging) * 31 + mRequiresDeviceIdle) * 31 + mRequiresBatteryNotLow) * 31 + mRequiresStorageNotLow) * 31 + n) * 31 + (int)(mTriggerMaxContentDelay ^ mTriggerMaxContentDelay >>> 32)) * 31 + this.mContentUriTriggers.hashCode();
    }
    
    public boolean requiresBatteryNotLow() {
        return this.mRequiresBatteryNotLow;
    }
    
    public boolean requiresCharging() {
        return this.mRequiresCharging;
    }
    
    @RequiresApi(23)
    public boolean requiresDeviceIdle() {
        return this.mRequiresDeviceIdle;
    }
    
    public boolean requiresStorageNotLow() {
        return this.mRequiresStorageNotLow;
    }
    
    @RequiresApi(24)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void setContentUriTriggers(@Nullable final ContentUriTriggers mContentUriTriggers) {
        this.mContentUriTriggers = mContentUriTriggers;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void setRequiredNetworkType(@NonNull final NetworkType mRequiredNetworkType) {
        this.mRequiredNetworkType = mRequiredNetworkType;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void setRequiresBatteryNotLow(final boolean mRequiresBatteryNotLow) {
        this.mRequiresBatteryNotLow = mRequiresBatteryNotLow;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void setRequiresCharging(final boolean mRequiresCharging) {
        this.mRequiresCharging = mRequiresCharging;
    }
    
    @RequiresApi(23)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void setRequiresDeviceIdle(final boolean mRequiresDeviceIdle) {
        this.mRequiresDeviceIdle = mRequiresDeviceIdle;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void setRequiresStorageNotLow(final boolean mRequiresStorageNotLow) {
        this.mRequiresStorageNotLow = mRequiresStorageNotLow;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void setTriggerContentUpdateDelay(final long mTriggerContentUpdateDelay) {
        this.mTriggerContentUpdateDelay = mTriggerContentUpdateDelay;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void setTriggerMaxContentDelay(final long mTriggerMaxContentDelay) {
        this.mTriggerMaxContentDelay = mTriggerMaxContentDelay;
    }
    
    public static final class Builder
    {
        ContentUriTriggers mContentUriTriggers;
        NetworkType mRequiredNetworkType;
        boolean mRequiresBatteryNotLow;
        boolean mRequiresCharging;
        boolean mRequiresDeviceIdle;
        boolean mRequiresStorageNotLow;
        long mTriggerContentMaxDelay;
        long mTriggerContentUpdateDelay;
        
        public Builder() {
            this.mRequiresCharging = false;
            this.mRequiresDeviceIdle = false;
            this.mRequiredNetworkType = NetworkType.NOT_REQUIRED;
            this.mRequiresBatteryNotLow = false;
            this.mRequiresStorageNotLow = false;
            this.mTriggerContentUpdateDelay = -1L;
            this.mTriggerContentMaxDelay = -1L;
            this.mContentUriTriggers = new ContentUriTriggers();
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder(@NonNull final Constraints constraints) {
            final boolean b = false;
            this.mRequiresCharging = false;
            this.mRequiresDeviceIdle = false;
            this.mRequiredNetworkType = NetworkType.NOT_REQUIRED;
            this.mRequiresBatteryNotLow = false;
            this.mRequiresStorageNotLow = false;
            this.mTriggerContentUpdateDelay = -1L;
            this.mTriggerContentMaxDelay = -1L;
            this.mContentUriTriggers = new ContentUriTriggers();
            this.mRequiresCharging = constraints.requiresCharging();
            final int sdk_INT = Build$VERSION.SDK_INT;
            boolean mRequiresDeviceIdle = b;
            if (sdk_INT >= 23) {
                mRequiresDeviceIdle = b;
                if (constraints.requiresDeviceIdle()) {
                    mRequiresDeviceIdle = true;
                }
            }
            this.mRequiresDeviceIdle = mRequiresDeviceIdle;
            this.mRequiredNetworkType = constraints.getRequiredNetworkType();
            this.mRequiresBatteryNotLow = constraints.requiresBatteryNotLow();
            this.mRequiresStorageNotLow = constraints.requiresStorageNotLow();
            if (sdk_INT >= 24) {
                this.mTriggerContentUpdateDelay = constraints.getTriggerContentUpdateDelay();
                this.mTriggerContentMaxDelay = constraints.getTriggerMaxContentDelay();
                this.mContentUriTriggers = constraints.getContentUriTriggers();
            }
        }
        
        @NonNull
        @RequiresApi(24)
        public Builder addContentUriTrigger(@NonNull final Uri uri, final boolean b) {
            this.mContentUriTriggers.add(uri, b);
            return this;
        }
        
        @NonNull
        public Constraints build() {
            return new Constraints(this);
        }
        
        @NonNull
        public Builder setRequiredNetworkType(@NonNull final NetworkType mRequiredNetworkType) {
            this.mRequiredNetworkType = mRequiredNetworkType;
            return this;
        }
        
        @NonNull
        public Builder setRequiresBatteryNotLow(final boolean mRequiresBatteryNotLow) {
            this.mRequiresBatteryNotLow = mRequiresBatteryNotLow;
            return this;
        }
        
        @NonNull
        public Builder setRequiresCharging(final boolean mRequiresCharging) {
            this.mRequiresCharging = mRequiresCharging;
            return this;
        }
        
        @NonNull
        @RequiresApi(23)
        public Builder setRequiresDeviceIdle(final boolean mRequiresDeviceIdle) {
            this.mRequiresDeviceIdle = mRequiresDeviceIdle;
            return this;
        }
        
        @NonNull
        public Builder setRequiresStorageNotLow(final boolean mRequiresStorageNotLow) {
            this.mRequiresStorageNotLow = mRequiresStorageNotLow;
            return this;
        }
        
        @NonNull
        @RequiresApi(24)
        public Builder setTriggerContentMaxDelay(final long duration, @NonNull final TimeUnit timeUnit) {
            this.mTriggerContentMaxDelay = timeUnit.toMillis(duration);
            return this;
        }
        
        @NonNull
        @RequiresApi(26)
        public Builder setTriggerContentMaxDelay(final Duration duration) {
            this.mTriggerContentMaxDelay = \u3007080.\u3007080(duration);
            return this;
        }
        
        @NonNull
        @RequiresApi(24)
        public Builder setTriggerContentUpdateDelay(final long duration, @NonNull final TimeUnit timeUnit) {
            this.mTriggerContentUpdateDelay = timeUnit.toMillis(duration);
            return this;
        }
        
        @NonNull
        @RequiresApi(26)
        public Builder setTriggerContentUpdateDelay(final Duration duration) {
            this.mTriggerContentUpdateDelay = \u3007080.\u3007080(duration);
            return this;
        }
    }
}

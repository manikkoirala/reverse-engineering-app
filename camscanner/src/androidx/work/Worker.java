// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import com.google.common.util.concurrent.ListenableFuture;
import androidx.annotation.WorkerThread;
import androidx.annotation.Keep;
import android.annotation.SuppressLint;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.work.impl.utils.futures.SettableFuture;

public abstract class Worker extends ListenableWorker
{
    SettableFuture<Result> mFuture;
    
    @SuppressLint({ "BanKeepAnnotation" })
    @Keep
    public Worker(@NonNull final Context context, @NonNull final WorkerParameters workerParameters) {
        super(context, workerParameters);
    }
    
    @NonNull
    @WorkerThread
    public abstract Result doWork();
    
    @NonNull
    @Override
    public final ListenableFuture<Result> startWork() {
        this.mFuture = SettableFuture.create();
        this.getBackgroundExecutor().execute(new Runnable(this) {
            final Worker this$0;
            
            @Override
            public void run() {
                try {
                    this.this$0.mFuture.set(this.this$0.doWork());
                }
                finally {
                    final Throwable exception;
                    this.this$0.mFuture.setException(exception);
                }
            }
        });
        return (ListenableFuture<Result>)this.mFuture;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import java.util.Collections;
import android.net.Uri;
import java.util.List;
import androidx.annotation.RequiresApi;
import androidx.annotation.Nullable;
import android.net.Network;
import androidx.annotation.RestrictTo;
import java.util.HashSet;
import androidx.annotation.IntRange;
import java.util.Collection;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import java.util.Set;
import java.util.UUID;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;

public final class WorkerParameters
{
    @NonNull
    private Executor mBackgroundExecutor;
    @NonNull
    private ForegroundUpdater mForegroundUpdater;
    @NonNull
    private UUID mId;
    @NonNull
    private Data mInputData;
    @NonNull
    private ProgressUpdater mProgressUpdater;
    private int mRunAttemptCount;
    @NonNull
    private RuntimeExtras mRuntimeExtras;
    @NonNull
    private Set<String> mTags;
    @NonNull
    private TaskExecutor mWorkTaskExecutor;
    @NonNull
    private WorkerFactory mWorkerFactory;
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public WorkerParameters(@NonNull final UUID mId, @NonNull final Data mInputData, @NonNull final Collection<String> c, @NonNull final RuntimeExtras mRuntimeExtras, @IntRange(from = 0L) final int mRunAttemptCount, @NonNull final Executor mBackgroundExecutor, @NonNull final TaskExecutor mWorkTaskExecutor, @NonNull final WorkerFactory mWorkerFactory, @NonNull final ProgressUpdater mProgressUpdater, @NonNull final ForegroundUpdater mForegroundUpdater) {
        this.mId = mId;
        this.mInputData = mInputData;
        this.mTags = new HashSet<String>(c);
        this.mRuntimeExtras = mRuntimeExtras;
        this.mRunAttemptCount = mRunAttemptCount;
        this.mBackgroundExecutor = mBackgroundExecutor;
        this.mWorkTaskExecutor = mWorkTaskExecutor;
        this.mWorkerFactory = mWorkerFactory;
        this.mProgressUpdater = mProgressUpdater;
        this.mForegroundUpdater = mForegroundUpdater;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public Executor getBackgroundExecutor() {
        return this.mBackgroundExecutor;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public ForegroundUpdater getForegroundUpdater() {
        return this.mForegroundUpdater;
    }
    
    @NonNull
    public UUID getId() {
        return this.mId;
    }
    
    @NonNull
    public Data getInputData() {
        return this.mInputData;
    }
    
    @Nullable
    @RequiresApi(28)
    public Network getNetwork() {
        return this.mRuntimeExtras.network;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public ProgressUpdater getProgressUpdater() {
        return this.mProgressUpdater;
    }
    
    @IntRange(from = 0L)
    public int getRunAttemptCount() {
        return this.mRunAttemptCount;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public RuntimeExtras getRuntimeExtras() {
        return this.mRuntimeExtras;
    }
    
    @NonNull
    public Set<String> getTags() {
        return this.mTags;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public TaskExecutor getTaskExecutor() {
        return this.mWorkTaskExecutor;
    }
    
    @NonNull
    @RequiresApi(24)
    public List<String> getTriggeredContentAuthorities() {
        return this.mRuntimeExtras.triggeredContentAuthorities;
    }
    
    @NonNull
    @RequiresApi(24)
    public List<Uri> getTriggeredContentUris() {
        return this.mRuntimeExtras.triggeredContentUris;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public WorkerFactory getWorkerFactory() {
        return this.mWorkerFactory;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static class RuntimeExtras
    {
        @RequiresApi(28)
        public Network network;
        @NonNull
        public List<String> triggeredContentAuthorities;
        @NonNull
        public List<Uri> triggeredContentUris;
        
        public RuntimeExtras() {
            this.triggeredContentAuthorities = Collections.emptyList();
            this.triggeredContentUris = Collections.emptyList();
        }
    }
}

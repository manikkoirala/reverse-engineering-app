// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import android.os.Build$VERSION;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import androidx.annotation.NonNull;

public final class OneTimeWorkRequest extends WorkRequest
{
    OneTimeWorkRequest(final Builder builder) {
        super(builder.mId, builder.mWorkSpec, builder.mTags);
    }
    
    @NonNull
    public static OneTimeWorkRequest from(@NonNull final Class<? extends ListenableWorker> clazz) {
        return ((WorkRequest.Builder<B, OneTimeWorkRequest>)new Builder(clazz)).build();
    }
    
    @NonNull
    public static List<OneTimeWorkRequest> from(@NonNull final List<Class<? extends ListenableWorker>> list) {
        final ArrayList list2 = new ArrayList(list.size());
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            list2.add(new Builder((Class<? extends ListenableWorker>)iterator.next()).build());
        }
        return list2;
    }
    
    public static final class Builder extends WorkRequest.Builder<Builder, OneTimeWorkRequest>
    {
        public Builder(@NonNull final Class<? extends ListenableWorker> clazz) {
            super(clazz);
            super.mWorkSpec.inputMergerClassName = OverwritingInputMerger.class.getName();
        }
        
        @NonNull
        OneTimeWorkRequest buildInternal() {
            if (super.mBackoffCriteriaSet && Build$VERSION.SDK_INT >= 23 && super.mWorkSpec.constraints.requiresDeviceIdle()) {
                throw new IllegalArgumentException("Cannot set backoff criteria on an idle mode job");
            }
            return new OneTimeWorkRequest(this);
        }
        
        @NonNull
        Builder getThis() {
            return this;
        }
        
        @NonNull
        public Builder setInputMerger(@NonNull final Class<? extends InputMerger> clazz) {
            super.mWorkSpec.inputMergerClassName = clazz.getName();
            return this;
        }
    }
}

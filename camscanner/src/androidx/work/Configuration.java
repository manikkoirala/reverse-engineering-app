// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import androidx.annotation.IntRange;
import android.os.Build$VERSION;
import androidx.annotation.RestrictTo;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.Executors;
import androidx.work.impl.DefaultRunnableScheduler;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;
import androidx.annotation.Nullable;
import android.annotation.SuppressLint;

public final class Configuration
{
    @SuppressLint({ "MinMaxConstant" })
    public static final int MIN_SCHEDULER_LIMIT = 20;
    @Nullable
    final String mDefaultProcessName;
    @Nullable
    final InitializationExceptionHandler mExceptionHandler;
    @NonNull
    final Executor mExecutor;
    @NonNull
    final InputMergerFactory mInputMergerFactory;
    private final boolean mIsUsingDefaultTaskExecutor;
    final int mLoggingLevel;
    final int mMaxJobSchedulerId;
    final int mMaxSchedulerLimit;
    final int mMinJobSchedulerId;
    @NonNull
    final RunnableScheduler mRunnableScheduler;
    @NonNull
    final Executor mTaskExecutor;
    @NonNull
    final WorkerFactory mWorkerFactory;
    
    Configuration(@NonNull final Builder builder) {
        final Executor mExecutor = builder.mExecutor;
        if (mExecutor == null) {
            this.mExecutor = this.createDefaultExecutor(false);
        }
        else {
            this.mExecutor = mExecutor;
        }
        final Executor mTaskExecutor = builder.mTaskExecutor;
        if (mTaskExecutor == null) {
            this.mIsUsingDefaultTaskExecutor = true;
            this.mTaskExecutor = this.createDefaultExecutor(true);
        }
        else {
            this.mIsUsingDefaultTaskExecutor = false;
            this.mTaskExecutor = mTaskExecutor;
        }
        final WorkerFactory mWorkerFactory = builder.mWorkerFactory;
        if (mWorkerFactory == null) {
            this.mWorkerFactory = WorkerFactory.getDefaultWorkerFactory();
        }
        else {
            this.mWorkerFactory = mWorkerFactory;
        }
        final InputMergerFactory mInputMergerFactory = builder.mInputMergerFactory;
        if (mInputMergerFactory == null) {
            this.mInputMergerFactory = InputMergerFactory.getDefaultInputMergerFactory();
        }
        else {
            this.mInputMergerFactory = mInputMergerFactory;
        }
        final RunnableScheduler mRunnableScheduler = builder.mRunnableScheduler;
        if (mRunnableScheduler == null) {
            this.mRunnableScheduler = new DefaultRunnableScheduler();
        }
        else {
            this.mRunnableScheduler = mRunnableScheduler;
        }
        this.mLoggingLevel = builder.mLoggingLevel;
        this.mMinJobSchedulerId = builder.mMinJobSchedulerId;
        this.mMaxJobSchedulerId = builder.mMaxJobSchedulerId;
        this.mMaxSchedulerLimit = builder.mMaxSchedulerLimit;
        this.mExceptionHandler = builder.mExceptionHandler;
        this.mDefaultProcessName = builder.mDefaultProcessName;
    }
    
    @NonNull
    private Executor createDefaultExecutor(final boolean b) {
        return Executors.newFixedThreadPool(Math.max(2, Math.min(Runtime.getRuntime().availableProcessors() - 1, 4)), this.createDefaultThreadFactory(b));
    }
    
    @NonNull
    private ThreadFactory createDefaultThreadFactory(final boolean b) {
        return new ThreadFactory(this, b) {
            private final AtomicInteger mThreadCount = new AtomicInteger(0);
            final Configuration this$0;
            final boolean val$isTaskExecutor;
            
            @Override
            public Thread newThread(final Runnable target) {
                String str;
                if (this.val$isTaskExecutor) {
                    str = "WM.task-";
                }
                else {
                    str = "androidx.work-";
                }
                final StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(this.mThreadCount.incrementAndGet());
                return new Thread(target, sb.toString());
            }
        };
    }
    
    @Nullable
    public String getDefaultProcessName() {
        return this.mDefaultProcessName;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public InitializationExceptionHandler getExceptionHandler() {
        return this.mExceptionHandler;
    }
    
    @NonNull
    public Executor getExecutor() {
        return this.mExecutor;
    }
    
    @NonNull
    public InputMergerFactory getInputMergerFactory() {
        return this.mInputMergerFactory;
    }
    
    public int getMaxJobSchedulerId() {
        return this.mMaxJobSchedulerId;
    }
    
    @IntRange(from = 20L, to = 50L)
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public int getMaxSchedulerLimit() {
        if (Build$VERSION.SDK_INT == 23) {
            return this.mMaxSchedulerLimit / 2;
        }
        return this.mMaxSchedulerLimit;
    }
    
    public int getMinJobSchedulerId() {
        return this.mMinJobSchedulerId;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public int getMinimumLoggingLevel() {
        return this.mLoggingLevel;
    }
    
    @NonNull
    public RunnableScheduler getRunnableScheduler() {
        return this.mRunnableScheduler;
    }
    
    @NonNull
    public Executor getTaskExecutor() {
        return this.mTaskExecutor;
    }
    
    @NonNull
    public WorkerFactory getWorkerFactory() {
        return this.mWorkerFactory;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public boolean isUsingDefaultTaskExecutor() {
        return this.mIsUsingDefaultTaskExecutor;
    }
    
    public static final class Builder
    {
        @Nullable
        String mDefaultProcessName;
        @Nullable
        InitializationExceptionHandler mExceptionHandler;
        Executor mExecutor;
        InputMergerFactory mInputMergerFactory;
        int mLoggingLevel;
        int mMaxJobSchedulerId;
        int mMaxSchedulerLimit;
        int mMinJobSchedulerId;
        RunnableScheduler mRunnableScheduler;
        Executor mTaskExecutor;
        WorkerFactory mWorkerFactory;
        
        public Builder() {
            this.mLoggingLevel = 4;
            this.mMinJobSchedulerId = 0;
            this.mMaxJobSchedulerId = Integer.MAX_VALUE;
            this.mMaxSchedulerLimit = 20;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder(@NonNull final Configuration configuration) {
            this.mExecutor = configuration.mExecutor;
            this.mWorkerFactory = configuration.mWorkerFactory;
            this.mInputMergerFactory = configuration.mInputMergerFactory;
            this.mTaskExecutor = configuration.mTaskExecutor;
            this.mLoggingLevel = configuration.mLoggingLevel;
            this.mMinJobSchedulerId = configuration.mMinJobSchedulerId;
            this.mMaxJobSchedulerId = configuration.mMaxJobSchedulerId;
            this.mMaxSchedulerLimit = configuration.mMaxSchedulerLimit;
            this.mRunnableScheduler = configuration.mRunnableScheduler;
            this.mExceptionHandler = configuration.mExceptionHandler;
            this.mDefaultProcessName = configuration.mDefaultProcessName;
        }
        
        @NonNull
        public Configuration build() {
            return new Configuration(this);
        }
        
        @NonNull
        public Builder setDefaultProcessName(@NonNull final String mDefaultProcessName) {
            this.mDefaultProcessName = mDefaultProcessName;
            return this;
        }
        
        @NonNull
        public Builder setExecutor(@NonNull final Executor mExecutor) {
            this.mExecutor = mExecutor;
            return this;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder setInitializationExceptionHandler(@NonNull final InitializationExceptionHandler mExceptionHandler) {
            this.mExceptionHandler = mExceptionHandler;
            return this;
        }
        
        @NonNull
        public Builder setInputMergerFactory(@NonNull final InputMergerFactory mInputMergerFactory) {
            this.mInputMergerFactory = mInputMergerFactory;
            return this;
        }
        
        @NonNull
        public Builder setJobSchedulerJobIdRange(final int mMinJobSchedulerId, final int mMaxJobSchedulerId) {
            if (mMaxJobSchedulerId - mMinJobSchedulerId >= 1000) {
                this.mMinJobSchedulerId = mMinJobSchedulerId;
                this.mMaxJobSchedulerId = mMaxJobSchedulerId;
                return this;
            }
            throw new IllegalArgumentException("WorkManager needs a range of at least 1000 job ids.");
        }
        
        @NonNull
        public Builder setMaxSchedulerLimit(final int a) {
            if (a >= 20) {
                this.mMaxSchedulerLimit = Math.min(a, 50);
                return this;
            }
            throw new IllegalArgumentException("WorkManager needs to be able to schedule at least 20 jobs in JobScheduler.");
        }
        
        @NonNull
        public Builder setMinimumLoggingLevel(final int mLoggingLevel) {
            this.mLoggingLevel = mLoggingLevel;
            return this;
        }
        
        @NonNull
        public Builder setRunnableScheduler(@NonNull final RunnableScheduler mRunnableScheduler) {
            this.mRunnableScheduler = mRunnableScheduler;
            return this;
        }
        
        @NonNull
        public Builder setTaskExecutor(@NonNull final Executor mTaskExecutor) {
            this.mTaskExecutor = mTaskExecutor;
            return this;
        }
        
        @NonNull
        public Builder setWorkerFactory(@NonNull final WorkerFactory mWorkerFactory) {
            this.mWorkerFactory = mWorkerFactory;
            return this;
        }
    }
    
    public interface Provider
    {
        @NonNull
        Configuration getWorkManagerConfiguration();
    }
}

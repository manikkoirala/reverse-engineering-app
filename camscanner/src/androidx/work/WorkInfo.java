// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import androidx.annotation.IntRange;
import androidx.annotation.RestrictTo;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import androidx.annotation.NonNull;
import java.util.UUID;

public final class WorkInfo
{
    @NonNull
    private UUID mId;
    @NonNull
    private Data mOutputData;
    @NonNull
    private Data mProgress;
    private int mRunAttemptCount;
    @NonNull
    private State mState;
    @NonNull
    private Set<String> mTags;
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public WorkInfo(@NonNull final UUID mId, @NonNull final State mState, @NonNull final Data mOutputData, @NonNull final List<String> c, @NonNull final Data mProgress, final int mRunAttemptCount) {
        this.mId = mId;
        this.mState = mState;
        this.mOutputData = mOutputData;
        this.mTags = new HashSet<String>(c);
        this.mProgress = mProgress;
        this.mRunAttemptCount = mRunAttemptCount;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && WorkInfo.class == o.getClass()) {
            final WorkInfo workInfo = (WorkInfo)o;
            return this.mRunAttemptCount == workInfo.mRunAttemptCount && this.mId.equals(workInfo.mId) && this.mState == workInfo.mState && this.mOutputData.equals(workInfo.mOutputData) && this.mTags.equals(workInfo.mTags) && this.mProgress.equals(workInfo.mProgress);
        }
        return false;
    }
    
    @NonNull
    public UUID getId() {
        return this.mId;
    }
    
    @NonNull
    public Data getOutputData() {
        return this.mOutputData;
    }
    
    @NonNull
    public Data getProgress() {
        return this.mProgress;
    }
    
    @IntRange(from = 0L)
    public int getRunAttemptCount() {
        return this.mRunAttemptCount;
    }
    
    @NonNull
    public State getState() {
        return this.mState;
    }
    
    @NonNull
    public Set<String> getTags() {
        return this.mTags;
    }
    
    @Override
    public int hashCode() {
        return ((((this.mId.hashCode() * 31 + this.mState.hashCode()) * 31 + this.mOutputData.hashCode()) * 31 + this.mTags.hashCode()) * 31 + this.mProgress.hashCode()) * 31 + this.mRunAttemptCount;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("WorkInfo{mId='");
        sb.append(this.mId);
        sb.append('\'');
        sb.append(", mState=");
        sb.append(this.mState);
        sb.append(", mOutputData=");
        sb.append(this.mOutputData);
        sb.append(", mTags=");
        sb.append(this.mTags);
        sb.append(", mProgress=");
        sb.append(this.mProgress);
        sb.append('}');
        return sb.toString();
    }
    
    public enum State
    {
        private static final State[] $VALUES;
        
        BLOCKED, 
        CANCELLED, 
        ENQUEUED, 
        FAILED, 
        RUNNING, 
        SUCCEEDED;
        
        public boolean isFinished() {
            return this == State.SUCCEEDED || this == State.FAILED || this == State.CANCELLED;
        }
    }
}

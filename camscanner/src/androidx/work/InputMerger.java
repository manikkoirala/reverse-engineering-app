// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import androidx.annotation.NonNull;
import java.util.List;
import androidx.annotation.RestrictTo;

public abstract class InputMerger
{
    private static final String TAG;
    
    static {
        TAG = Logger.tagWithPrefix("InputMerger");
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static InputMerger fromClassName(final String s) {
        try {
            return (InputMerger)Class.forName(s).newInstance();
        }
        catch (final Exception ex) {
            final Logger value = Logger.get();
            final String tag = InputMerger.TAG;
            final StringBuilder sb = new StringBuilder();
            sb.append("Trouble instantiating + ");
            sb.append(s);
            value.error(tag, sb.toString(), ex);
            return null;
        }
    }
    
    @NonNull
    public abstract Data merge(@NonNull final List<Data> p0);
}

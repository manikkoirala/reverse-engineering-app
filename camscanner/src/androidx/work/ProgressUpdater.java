// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import com.google.common.util.concurrent.ListenableFuture;
import java.util.UUID;
import androidx.annotation.NonNull;
import android.content.Context;

public interface ProgressUpdater
{
    @NonNull
    ListenableFuture<Void> updateProgress(@NonNull final Context p0, @NonNull final UUID p1, @NonNull final Data p2);
}

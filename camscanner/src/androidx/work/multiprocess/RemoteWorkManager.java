// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.multiprocess;

import androidx.work.Data;
import androidx.work.WorkInfo;
import androidx.work.WorkQuery;
import androidx.work.PeriodicWorkRequest;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.WorkRequest;
import androidx.work.WorkContinuation;
import java.util.UUID;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.List;
import java.util.Collections;
import androidx.work.OneTimeWorkRequest;
import androidx.work.ExistingWorkPolicy;
import androidx.work.impl.WorkManagerImpl;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.annotation.RestrictTo;

public abstract class RemoteWorkManager
{
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected RemoteWorkManager() {
    }
    
    @NonNull
    public static RemoteWorkManager getInstance(@NonNull final Context context) {
        final RemoteWorkManager remoteWorkManager = WorkManagerImpl.getInstance(context).getRemoteWorkManager();
        if (remoteWorkManager != null) {
            return remoteWorkManager;
        }
        throw new IllegalStateException("Unable to initialize RemoteWorkManager");
    }
    
    @NonNull
    public final RemoteWorkContinuation beginUniqueWork(@NonNull final String s, @NonNull final ExistingWorkPolicy existingWorkPolicy, @NonNull final OneTimeWorkRequest o) {
        return this.beginUniqueWork(s, existingWorkPolicy, Collections.singletonList(o));
    }
    
    @NonNull
    public abstract RemoteWorkContinuation beginUniqueWork(@NonNull final String p0, @NonNull final ExistingWorkPolicy p1, @NonNull final List<OneTimeWorkRequest> p2);
    
    @NonNull
    public final RemoteWorkContinuation beginWith(@NonNull final OneTimeWorkRequest o) {
        return this.beginWith(Collections.singletonList(o));
    }
    
    @NonNull
    public abstract RemoteWorkContinuation beginWith(@NonNull final List<OneTimeWorkRequest> p0);
    
    @NonNull
    public abstract ListenableFuture<Void> cancelAllWork();
    
    @NonNull
    public abstract ListenableFuture<Void> cancelAllWorkByTag(@NonNull final String p0);
    
    @NonNull
    public abstract ListenableFuture<Void> cancelUniqueWork(@NonNull final String p0);
    
    @NonNull
    public abstract ListenableFuture<Void> cancelWorkById(@NonNull final UUID p0);
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public abstract ListenableFuture<Void> enqueue(@NonNull final WorkContinuation p0);
    
    @NonNull
    public abstract ListenableFuture<Void> enqueue(@NonNull final WorkRequest p0);
    
    @NonNull
    public abstract ListenableFuture<Void> enqueue(@NonNull final List<WorkRequest> p0);
    
    @NonNull
    public abstract ListenableFuture<Void> enqueueUniquePeriodicWork(@NonNull final String p0, @NonNull final ExistingPeriodicWorkPolicy p1, @NonNull final PeriodicWorkRequest p2);
    
    @NonNull
    public final ListenableFuture<Void> enqueueUniqueWork(@NonNull final String s, @NonNull final ExistingWorkPolicy existingWorkPolicy, @NonNull final OneTimeWorkRequest o) {
        return this.enqueueUniqueWork(s, existingWorkPolicy, Collections.singletonList(o));
    }
    
    @NonNull
    public abstract ListenableFuture<Void> enqueueUniqueWork(@NonNull final String p0, @NonNull final ExistingWorkPolicy p1, @NonNull final List<OneTimeWorkRequest> p2);
    
    @NonNull
    public abstract ListenableFuture<List<WorkInfo>> getWorkInfos(@NonNull final WorkQuery p0);
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public abstract ListenableFuture<Void> setProgress(@NonNull final UUID p0, @NonNull final Data p1);
}

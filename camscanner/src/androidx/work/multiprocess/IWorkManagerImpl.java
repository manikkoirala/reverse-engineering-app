// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.multiprocess;

import android.os.Parcel;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.IInterface;

public interface IWorkManagerImpl extends IInterface
{
    void cancelAllWork(final IWorkManagerImplCallback p0) throws RemoteException;
    
    void cancelAllWorkByTag(final String p0, final IWorkManagerImplCallback p1) throws RemoteException;
    
    void cancelUniqueWork(final String p0, final IWorkManagerImplCallback p1) throws RemoteException;
    
    void cancelWorkById(final String p0, final IWorkManagerImplCallback p1) throws RemoteException;
    
    void enqueueContinuation(final byte[] p0, final IWorkManagerImplCallback p1) throws RemoteException;
    
    void enqueueWorkRequests(final byte[] p0, final IWorkManagerImplCallback p1) throws RemoteException;
    
    void queryWorkInfo(final byte[] p0, final IWorkManagerImplCallback p1) throws RemoteException;
    
    void setProgress(final byte[] p0, final IWorkManagerImplCallback p1) throws RemoteException;
    
    public static class Default implements IWorkManagerImpl
    {
        public IBinder asBinder() {
            return null;
        }
        
        @Override
        public void cancelAllWork(final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
        }
        
        @Override
        public void cancelAllWorkByTag(final String s, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
        }
        
        @Override
        public void cancelUniqueWork(final String s, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
        }
        
        @Override
        public void cancelWorkById(final String s, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
        }
        
        @Override
        public void enqueueContinuation(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
        }
        
        @Override
        public void enqueueWorkRequests(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
        }
        
        @Override
        public void queryWorkInfo(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
        }
        
        @Override
        public void setProgress(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
        }
    }
    
    public abstract static class Stub extends Binder implements IWorkManagerImpl
    {
        private static final String DESCRIPTOR = "androidx.work.multiprocess.IWorkManagerImpl";
        static final int TRANSACTION_cancelAllWork = 6;
        static final int TRANSACTION_cancelAllWorkByTag = 4;
        static final int TRANSACTION_cancelUniqueWork = 5;
        static final int TRANSACTION_cancelWorkById = 3;
        static final int TRANSACTION_enqueueContinuation = 2;
        static final int TRANSACTION_enqueueWorkRequests = 1;
        static final int TRANSACTION_queryWorkInfo = 7;
        static final int TRANSACTION_setProgress = 8;
        
        public Stub() {
            this.attachInterface((IInterface)this, "androidx.work.multiprocess.IWorkManagerImpl");
        }
        
        public static IWorkManagerImpl asInterface(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("androidx.work.multiprocess.IWorkManagerImpl");
            if (queryLocalInterface != null && queryLocalInterface instanceof IWorkManagerImpl) {
                return (IWorkManagerImpl)queryLocalInterface;
            }
            return new Proxy(binder);
        }
        
        public static IWorkManagerImpl getDefaultImpl() {
            return Proxy.sDefaultImpl;
        }
        
        public static boolean setDefaultImpl(final IWorkManagerImpl sDefaultImpl) {
            if (Proxy.sDefaultImpl != null) {
                throw new IllegalStateException("setDefaultImpl() called twice");
            }
            if (sDefaultImpl != null) {
                Proxy.sDefaultImpl = sDefaultImpl;
                return true;
            }
            return false;
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            if (n == 1598968902) {
                parcel2.writeString("androidx.work.multiprocess.IWorkManagerImpl");
                return true;
            }
            switch (n) {
                default: {
                    return super.onTransact(n, parcel, parcel2, n2);
                }
                case 8: {
                    parcel.enforceInterface("androidx.work.multiprocess.IWorkManagerImpl");
                    this.setProgress(parcel.createByteArray(), IWorkManagerImplCallback.Stub.asInterface(parcel.readStrongBinder()));
                    return true;
                }
                case 7: {
                    parcel.enforceInterface("androidx.work.multiprocess.IWorkManagerImpl");
                    this.queryWorkInfo(parcel.createByteArray(), IWorkManagerImplCallback.Stub.asInterface(parcel.readStrongBinder()));
                    return true;
                }
                case 6: {
                    parcel.enforceInterface("androidx.work.multiprocess.IWorkManagerImpl");
                    this.cancelAllWork(IWorkManagerImplCallback.Stub.asInterface(parcel.readStrongBinder()));
                    return true;
                }
                case 5: {
                    parcel.enforceInterface("androidx.work.multiprocess.IWorkManagerImpl");
                    this.cancelUniqueWork(parcel.readString(), IWorkManagerImplCallback.Stub.asInterface(parcel.readStrongBinder()));
                    return true;
                }
                case 4: {
                    parcel.enforceInterface("androidx.work.multiprocess.IWorkManagerImpl");
                    this.cancelAllWorkByTag(parcel.readString(), IWorkManagerImplCallback.Stub.asInterface(parcel.readStrongBinder()));
                    return true;
                }
                case 3: {
                    parcel.enforceInterface("androidx.work.multiprocess.IWorkManagerImpl");
                    this.cancelWorkById(parcel.readString(), IWorkManagerImplCallback.Stub.asInterface(parcel.readStrongBinder()));
                    return true;
                }
                case 2: {
                    parcel.enforceInterface("androidx.work.multiprocess.IWorkManagerImpl");
                    this.enqueueContinuation(parcel.createByteArray(), IWorkManagerImplCallback.Stub.asInterface(parcel.readStrongBinder()));
                    return true;
                }
                case 1: {
                    parcel.enforceInterface("androidx.work.multiprocess.IWorkManagerImpl");
                    this.enqueueWorkRequests(parcel.createByteArray(), IWorkManagerImplCallback.Stub.asInterface(parcel.readStrongBinder()));
                    return true;
                }
            }
        }
        
        private static class Proxy implements IWorkManagerImpl
        {
            public static IWorkManagerImpl sDefaultImpl;
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            @Override
            public void cancelAllWork(final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.work.multiprocess.IWorkManagerImpl");
                    IBinder binder;
                    if (workManagerImplCallback != null) {
                        binder = ((IInterface)workManagerImplCallback).asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    if (!this.mRemote.transact(6, obtain, (Parcel)null, 1) && Stub.getDefaultImpl() != null) {
                        Stub.getDefaultImpl().cancelAllWork(workManagerImplCallback);
                    }
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void cancelAllWorkByTag(final String s, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.work.multiprocess.IWorkManagerImpl");
                    obtain.writeString(s);
                    IBinder binder;
                    if (workManagerImplCallback != null) {
                        binder = ((IInterface)workManagerImplCallback).asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    if (!this.mRemote.transact(4, obtain, (Parcel)null, 1) && Stub.getDefaultImpl() != null) {
                        Stub.getDefaultImpl().cancelAllWorkByTag(s, workManagerImplCallback);
                    }
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void cancelUniqueWork(final String s, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.work.multiprocess.IWorkManagerImpl");
                    obtain.writeString(s);
                    IBinder binder;
                    if (workManagerImplCallback != null) {
                        binder = ((IInterface)workManagerImplCallback).asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    if (!this.mRemote.transact(5, obtain, (Parcel)null, 1) && Stub.getDefaultImpl() != null) {
                        Stub.getDefaultImpl().cancelUniqueWork(s, workManagerImplCallback);
                    }
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void cancelWorkById(final String s, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.work.multiprocess.IWorkManagerImpl");
                    obtain.writeString(s);
                    IBinder binder;
                    if (workManagerImplCallback != null) {
                        binder = ((IInterface)workManagerImplCallback).asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    if (!this.mRemote.transact(3, obtain, (Parcel)null, 1) && Stub.getDefaultImpl() != null) {
                        Stub.getDefaultImpl().cancelWorkById(s, workManagerImplCallback);
                    }
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void enqueueContinuation(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.work.multiprocess.IWorkManagerImpl");
                    obtain.writeByteArray(array);
                    IBinder binder;
                    if (workManagerImplCallback != null) {
                        binder = ((IInterface)workManagerImplCallback).asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    if (!this.mRemote.transact(2, obtain, (Parcel)null, 1) && Stub.getDefaultImpl() != null) {
                        Stub.getDefaultImpl().enqueueContinuation(array, workManagerImplCallback);
                    }
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void enqueueWorkRequests(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.work.multiprocess.IWorkManagerImpl");
                    obtain.writeByteArray(array);
                    IBinder binder;
                    if (workManagerImplCallback != null) {
                        binder = ((IInterface)workManagerImplCallback).asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    if (!this.mRemote.transact(1, obtain, (Parcel)null, 1) && Stub.getDefaultImpl() != null) {
                        Stub.getDefaultImpl().enqueueWorkRequests(array, workManagerImplCallback);
                    }
                }
                finally {
                    obtain.recycle();
                }
            }
            
            public String getInterfaceDescriptor() {
                return "androidx.work.multiprocess.IWorkManagerImpl";
            }
            
            @Override
            public void queryWorkInfo(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.work.multiprocess.IWorkManagerImpl");
                    obtain.writeByteArray(array);
                    IBinder binder;
                    if (workManagerImplCallback != null) {
                        binder = ((IInterface)workManagerImplCallback).asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    if (!this.mRemote.transact(7, obtain, (Parcel)null, 1) && Stub.getDefaultImpl() != null) {
                        Stub.getDefaultImpl().queryWorkInfo(array, workManagerImplCallback);
                    }
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void setProgress(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.work.multiprocess.IWorkManagerImpl");
                    obtain.writeByteArray(array);
                    IBinder binder;
                    if (workManagerImplCallback != null) {
                        binder = ((IInterface)workManagerImplCallback).asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    if (!this.mRemote.transact(8, obtain, (Parcel)null, 1) && Stub.getDefaultImpl() != null) {
                        Stub.getDefaultImpl().setProgress(array, workManagerImplCallback);
                    }
                }
                finally {
                    obtain.recycle();
                }
            }
        }
    }
}

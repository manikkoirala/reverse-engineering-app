// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.multiprocess;

import android.os.Parcel;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.IInterface;

public interface IListenableWorkerImpl extends IInterface
{
    void interrupt(final byte[] p0, final IWorkManagerImplCallback p1) throws RemoteException;
    
    void startWork(final byte[] p0, final IWorkManagerImplCallback p1) throws RemoteException;
    
    public static class Default implements IListenableWorkerImpl
    {
        public IBinder asBinder() {
            return null;
        }
        
        @Override
        public void interrupt(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
        }
        
        @Override
        public void startWork(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
        }
    }
    
    public abstract static class Stub extends Binder implements IListenableWorkerImpl
    {
        private static final String DESCRIPTOR = "androidx.work.multiprocess.IListenableWorkerImpl";
        static final int TRANSACTION_interrupt = 2;
        static final int TRANSACTION_startWork = 1;
        
        public Stub() {
            this.attachInterface((IInterface)this, "androidx.work.multiprocess.IListenableWorkerImpl");
        }
        
        public static IListenableWorkerImpl asInterface(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("androidx.work.multiprocess.IListenableWorkerImpl");
            if (queryLocalInterface != null && queryLocalInterface instanceof IListenableWorkerImpl) {
                return (IListenableWorkerImpl)queryLocalInterface;
            }
            return new Proxy(binder);
        }
        
        public static IListenableWorkerImpl getDefaultImpl() {
            return Proxy.sDefaultImpl;
        }
        
        public static boolean setDefaultImpl(final IListenableWorkerImpl sDefaultImpl) {
            if (Proxy.sDefaultImpl != null) {
                throw new IllegalStateException("setDefaultImpl() called twice");
            }
            if (sDefaultImpl != null) {
                Proxy.sDefaultImpl = sDefaultImpl;
                return true;
            }
            return false;
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            if (n == 1) {
                parcel.enforceInterface("androidx.work.multiprocess.IListenableWorkerImpl");
                this.startWork(parcel.createByteArray(), IWorkManagerImplCallback.Stub.asInterface(parcel.readStrongBinder()));
                return true;
            }
            if (n == 2) {
                parcel.enforceInterface("androidx.work.multiprocess.IListenableWorkerImpl");
                this.interrupt(parcel.createByteArray(), IWorkManagerImplCallback.Stub.asInterface(parcel.readStrongBinder()));
                return true;
            }
            if (n != 1598968902) {
                return super.onTransact(n, parcel, parcel2, n2);
            }
            parcel2.writeString("androidx.work.multiprocess.IListenableWorkerImpl");
            return true;
        }
        
        private static class Proxy implements IListenableWorkerImpl
        {
            public static IListenableWorkerImpl sDefaultImpl;
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            public String getInterfaceDescriptor() {
                return "androidx.work.multiprocess.IListenableWorkerImpl";
            }
            
            @Override
            public void interrupt(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.work.multiprocess.IListenableWorkerImpl");
                    obtain.writeByteArray(array);
                    IBinder binder;
                    if (workManagerImplCallback != null) {
                        binder = ((IInterface)workManagerImplCallback).asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    if (!this.mRemote.transact(2, obtain, (Parcel)null, 1) && Stub.getDefaultImpl() != null) {
                        Stub.getDefaultImpl().interrupt(array, workManagerImplCallback);
                    }
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void startWork(final byte[] array, final IWorkManagerImplCallback workManagerImplCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.work.multiprocess.IListenableWorkerImpl");
                    obtain.writeByteArray(array);
                    IBinder binder;
                    if (workManagerImplCallback != null) {
                        binder = ((IInterface)workManagerImplCallback).asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    if (!this.mRemote.transact(1, obtain, (Parcel)null, 1) && Stub.getDefaultImpl() != null) {
                        Stub.getDefaultImpl().startWork(array, workManagerImplCallback);
                    }
                }
                finally {
                    obtain.recycle();
                }
            }
        }
    }
}

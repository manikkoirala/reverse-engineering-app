// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.multiprocess;

import java.util.Collections;
import androidx.work.OneTimeWorkRequest;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.annotation.NonNull;
import java.util.List;
import androidx.annotation.RestrictTo;

public abstract class RemoteWorkContinuation
{
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected RemoteWorkContinuation() {
    }
    
    @NonNull
    public static RemoteWorkContinuation combine(@NonNull final List<RemoteWorkContinuation> list) {
        return list.get(0).combineInternal(list);
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected abstract RemoteWorkContinuation combineInternal(@NonNull final List<RemoteWorkContinuation> p0);
    
    @NonNull
    public abstract ListenableFuture<Void> enqueue();
    
    @NonNull
    public final RemoteWorkContinuation then(@NonNull final OneTimeWorkRequest o) {
        return this.then(Collections.singletonList(o));
    }
    
    @NonNull
    public abstract RemoteWorkContinuation then(@NonNull final List<OneTimeWorkRequest> p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public abstract class Logger
{
    private static final int MAX_PREFIXED_TAG_LENGTH = 20;
    private static final int MAX_TAG_LENGTH = 23;
    private static final String TAG_PREFIX = "WM-";
    private static Logger sLogger;
    
    public Logger(final int n) {
    }
    
    public static Logger get() {
        synchronized (Logger.class) {
            if (Logger.sLogger == null) {
                Logger.sLogger = new LogcatLogger(3);
            }
            return Logger.sLogger;
        }
    }
    
    public static void setLogger(final Logger sLogger) {
        synchronized (Logger.class) {
            Logger.sLogger = sLogger;
        }
    }
    
    public static String tagWithPrefix(@NonNull final String str) {
        final int length = str.length();
        final StringBuilder sb = new StringBuilder(23);
        sb.append("WM-");
        final int max_PREFIXED_TAG_LENGTH = Logger.MAX_PREFIXED_TAG_LENGTH;
        if (length >= max_PREFIXED_TAG_LENGTH) {
            sb.append(str.substring(0, max_PREFIXED_TAG_LENGTH));
        }
        else {
            sb.append(str);
        }
        return sb.toString();
    }
    
    public abstract void debug(final String p0, final String p1, final Throwable... p2);
    
    public abstract void error(final String p0, final String p1, final Throwable... p2);
    
    public abstract void info(final String p0, final String p1, final Throwable... p2);
    
    public abstract void verbose(final String p0, final String p1, final Throwable... p2);
    
    public abstract void warning(final String p0, final String p1, final Throwable... p2);
    
    public static class LogcatLogger extends Logger
    {
        private int mLoggingLevel;
        
        public LogcatLogger(final int mLoggingLevel) {
            super(mLoggingLevel);
            this.mLoggingLevel = mLoggingLevel;
        }
        
        @Override
        public void debug(final String s, final String s2, final Throwable... array) {
            if (this.mLoggingLevel <= 3 && array != null && array.length >= 1) {
                final Throwable t = array[0];
            }
        }
        
        @Override
        public void error(final String s, final String s2, final Throwable... array) {
            if (this.mLoggingLevel <= 6 && array != null && array.length >= 1) {
                final Throwable t = array[0];
            }
        }
        
        @Override
        public void info(final String s, final String s2, final Throwable... array) {
            if (this.mLoggingLevel <= 4 && array != null && array.length >= 1) {
                final Throwable t = array[0];
            }
        }
        
        @Override
        public void verbose(final String s, final String s2, final Throwable... array) {
            if (this.mLoggingLevel <= 2 && array != null && array.length >= 1) {
                final Throwable t = array[0];
            }
        }
        
        @Override
        public void warning(final String s, final String s2, final Throwable... array) {
            if (this.mLoggingLevel <= 5 && array != null && array.length >= 1) {
                final Throwable t = array[0];
            }
        }
    }
}

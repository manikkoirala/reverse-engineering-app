// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import androidx.annotation.VisibleForTesting;
import java.util.Collections;
import androidx.annotation.Nullable;
import java.util.Iterator;
import java.util.Set;
import java.util.Arrays;
import androidx.room.TypeConverter;
import androidx.annotation.RestrictTo;
import java.util.HashMap;
import androidx.annotation.NonNull;
import java.util.Map;
import android.annotation.SuppressLint;

public final class Data
{
    public static final Data EMPTY;
    @SuppressLint({ "MinMaxConstant" })
    public static final int MAX_DATA_BYTES = 10240;
    private static final String TAG;
    Map<String, Object> mValues;
    
    static {
        TAG = Logger.tagWithPrefix("Data");
        EMPTY = new Builder().build();
    }
    
    Data() {
    }
    
    public Data(@NonNull final Data data) {
        this.mValues = new HashMap<String, Object>(data.mValues);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public Data(@NonNull final Map<String, ?> m) {
        this.mValues = new HashMap<String, Object>(m);
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static Boolean[] convertPrimitiveBooleanArray(@NonNull final boolean[] array) {
        final Boolean[] array2 = new Boolean[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static Byte[] convertPrimitiveByteArray(@NonNull final byte[] array) {
        final Byte[] array2 = new Byte[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static Double[] convertPrimitiveDoubleArray(@NonNull final double[] array) {
        final Double[] array2 = new Double[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static Float[] convertPrimitiveFloatArray(@NonNull final float[] array) {
        final Float[] array2 = new Float[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static Integer[] convertPrimitiveIntArray(@NonNull final int[] array) {
        final Integer[] array2 = new Integer[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static Long[] convertPrimitiveLongArray(@NonNull final long[] array) {
        final Long[] array2 = new Long[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static byte[] convertToPrimitiveArray(@NonNull final Byte[] array) {
        final byte[] array2 = new byte[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static double[] convertToPrimitiveArray(@NonNull final Double[] array) {
        final double[] array2 = new double[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static float[] convertToPrimitiveArray(@NonNull final Float[] array) {
        final float[] array2 = new float[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static int[] convertToPrimitiveArray(@NonNull final Integer[] array) {
        final int[] array2 = new int[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static long[] convertToPrimitiveArray(@NonNull final Long[] array) {
        final long[] array2 = new long[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static boolean[] convertToPrimitiveArray(@NonNull final Boolean[] array) {
        final boolean[] array2 = new boolean[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    @NonNull
    @TypeConverter
    public static Data fromByteArray(@NonNull final byte[] p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: arraylength    
        //     2: sipush          10240
        //     5: if_icmpgt       130
        //     8: new             Ljava/util/HashMap;
        //    11: dup            
        //    12: invokespecial   java/util/HashMap.<init>:()V
        //    15: astore_3       
        //    16: new             Ljava/io/ByteArrayInputStream;
        //    19: dup            
        //    20: aload_0        
        //    21: invokespecial   java/io/ByteArrayInputStream.<init>:([B)V
        //    24: astore          4
        //    26: aconst_null    
        //    27: astore_2       
        //    28: new             Ljava/io/ObjectInputStream;
        //    31: astore_0       
        //    32: aload_0        
        //    33: aload           4
        //    35: invokespecial   java/io/ObjectInputStream.<init>:(Ljava/io/InputStream;)V
        //    38: aload_0        
        //    39: invokevirtual   java/io/ObjectInputStream.readInt:()I
        //    42: istore_1       
        //    43: iload_1        
        //    44: ifle            68
        //    47: aload_3        
        //    48: aload_0        
        //    49: invokevirtual   java/io/ObjectInputStream.readUTF:()Ljava/lang/String;
        //    52: aload_0        
        //    53: invokevirtual   java/io/ObjectInputStream.readObject:()Ljava/lang/Object;
        //    56: invokeinterface java/util/Map.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //    61: pop            
        //    62: iinc            1, -1
        //    65: goto            43
        //    68: aload_0        
        //    69: invokevirtual   java/io/ObjectInputStream.close:()V
        //    72: aload           4
        //    74: invokevirtual   java/io/ByteArrayInputStream.close:()V
        //    77: goto            121
        //    80: astore_3       
        //    81: aload_0        
        //    82: astore_2       
        //    83: aload_3        
        //    84: astore_0       
        //    85: goto            95
        //    88: astore_2       
        //    89: goto            110
        //    92: astore_0       
        //    93: aconst_null    
        //    94: astore_2       
        //    95: aload_2        
        //    96: ifnull          103
        //    99: aload_2        
        //   100: invokevirtual   java/io/ObjectInputStream.close:()V
        //   103: aload           4
        //   105: invokevirtual   java/io/ByteArrayInputStream.close:()V
        //   108: aload_0        
        //   109: athrow         
        //   110: aload_0        
        //   111: ifnull          72
        //   114: aload_0        
        //   115: invokevirtual   java/io/ObjectInputStream.close:()V
        //   118: goto            72
        //   121: new             Landroidx/work/Data;
        //   124: dup            
        //   125: aload_3        
        //   126: invokespecial   androidx/work/Data.<init>:(Ljava/util/Map;)V
        //   129: areturn        
        //   130: new             Ljava/lang/IllegalStateException;
        //   133: dup            
        //   134: ldc             "Data cannot occupy more than 10240 bytes when serialized"
        //   136: invokespecial   java/lang/IllegalStateException.<init>:(Ljava/lang/String;)V
        //   139: athrow         
        //   140: astore_0       
        //   141: aload_2        
        //   142: astore_0       
        //   143: goto            110
        //   146: astore_0       
        //   147: goto            72
        //   150: astore_0       
        //   151: goto            121
        //   154: astore_2       
        //   155: goto            103
        //   158: astore_2       
        //   159: goto            108
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                              
        //  -----  -----  -----  -----  ----------------------------------
        //  28     38     140    146    Ljava/io/IOException;
        //  28     38     140    146    Ljava/lang/ClassNotFoundException;
        //  28     38     92     95     Any
        //  38     43     88     92     Ljava/io/IOException;
        //  38     43     88     92     Ljava/lang/ClassNotFoundException;
        //  38     43     80     88     Any
        //  47     62     88     92     Ljava/io/IOException;
        //  47     62     88     92     Ljava/lang/ClassNotFoundException;
        //  47     62     80     88     Any
        //  68     72     146    150    Ljava/io/IOException;
        //  72     77     150    154    Ljava/io/IOException;
        //  99     103    154    158    Ljava/io/IOException;
        //  103    108    158    162    Ljava/io/IOException;
        //  114    118    146    150    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 85 out of bounds for length 85
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @TypeConverter
    public static byte[] toByteArrayInternal(@NonNull final Data p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   java/io/ByteArrayOutputStream.<init>:()V
        //     7: astore          5
        //     9: aconst_null    
        //    10: astore_3       
        //    11: aconst_null    
        //    12: astore          4
        //    14: aload           4
        //    16: astore_1       
        //    17: new             Ljava/io/ObjectOutputStream;
        //    20: astore_2       
        //    21: aload           4
        //    23: astore_1       
        //    24: aload_2        
        //    25: aload           5
        //    27: invokespecial   java/io/ObjectOutputStream.<init>:(Ljava/io/OutputStream;)V
        //    30: aload_2        
        //    31: aload_0        
        //    32: invokevirtual   androidx/work/Data.size:()I
        //    35: invokevirtual   java/io/ObjectOutputStream.writeInt:(I)V
        //    38: aload_0        
        //    39: getfield        androidx/work/Data.mValues:Ljava/util/Map;
        //    42: invokeinterface java/util/Map.entrySet:()Ljava/util/Set;
        //    47: invokeinterface java/util/Set.iterator:()Ljava/util/Iterator;
        //    52: astore_1       
        //    53: aload_1        
        //    54: invokeinterface java/util/Iterator.hasNext:()Z
        //    59: ifeq            98
        //    62: aload_1        
        //    63: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //    68: checkcast       Ljava/util/Map$Entry;
        //    71: astore_0       
        //    72: aload_2        
        //    73: aload_0        
        //    74: invokeinterface java/util/Map$Entry.getKey:()Ljava/lang/Object;
        //    79: checkcast       Ljava/lang/String;
        //    82: invokevirtual   java/io/ObjectOutputStream.writeUTF:(Ljava/lang/String;)V
        //    85: aload_2        
        //    86: aload_0        
        //    87: invokeinterface java/util/Map$Entry.getValue:()Ljava/lang/Object;
        //    92: invokevirtual   java/io/ObjectOutputStream.writeObject:(Ljava/lang/Object;)V
        //    95: goto            53
        //    98: aload_2        
        //    99: invokevirtual   java/io/ObjectOutputStream.close:()V
        //   102: aload           5
        //   104: invokevirtual   java/io/ByteArrayOutputStream.close:()V
        //   107: goto            111
        //   110: astore_0       
        //   111: aload           5
        //   113: invokevirtual   java/io/ByteArrayOutputStream.size:()I
        //   116: sipush          10240
        //   119: if_icmpgt       128
        //   122: aload           5
        //   124: invokevirtual   java/io/ByteArrayOutputStream.toByteArray:()[B
        //   127: areturn        
        //   128: new             Ljava/lang/IllegalStateException;
        //   131: dup            
        //   132: ldc             "Data cannot occupy more than 10240 bytes when serialized"
        //   134: invokespecial   java/lang/IllegalStateException.<init>:(Ljava/lang/String;)V
        //   137: athrow         
        //   138: astore_0       
        //   139: aload_2        
        //   140: astore_1       
        //   141: goto            177
        //   144: astore_0       
        //   145: aload_2        
        //   146: astore_0       
        //   147: goto            154
        //   150: astore_0       
        //   151: goto            177
        //   154: aload_0        
        //   155: astore_1       
        //   156: aload           5
        //   158: invokevirtual   java/io/ByteArrayOutputStream.toByteArray:()[B
        //   161: astore_2       
        //   162: aload_0        
        //   163: ifnull          170
        //   166: aload_0        
        //   167: invokevirtual   java/io/ObjectOutputStream.close:()V
        //   170: aload           5
        //   172: invokevirtual   java/io/ByteArrayOutputStream.close:()V
        //   175: aload_2        
        //   176: areturn        
        //   177: aload_1        
        //   178: ifnull          185
        //   181: aload_1        
        //   182: invokevirtual   java/io/ObjectOutputStream.close:()V
        //   185: aload           5
        //   187: invokevirtual   java/io/ByteArrayOutputStream.close:()V
        //   190: aload_0        
        //   191: athrow         
        //   192: astore_0       
        //   193: aload_3        
        //   194: astore_0       
        //   195: goto            154
        //   198: astore_0       
        //   199: goto            102
        //   202: astore_0       
        //   203: goto            170
        //   206: astore_0       
        //   207: goto            175
        //   210: astore_1       
        //   211: goto            185
        //   214: astore_1       
        //   215: goto            190
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  17     21     192    198    Ljava/io/IOException;
        //  17     21     150    154    Any
        //  24     30     192    198    Ljava/io/IOException;
        //  24     30     150    154    Any
        //  30     53     144    150    Ljava/io/IOException;
        //  30     53     138    144    Any
        //  53     95     144    150    Ljava/io/IOException;
        //  53     95     138    144    Any
        //  98     102    198    202    Ljava/io/IOException;
        //  102    107    110    111    Ljava/io/IOException;
        //  156    162    150    154    Any
        //  166    170    202    206    Ljava/io/IOException;
        //  170    175    206    210    Ljava/io/IOException;
        //  181    185    210    214    Ljava/io/IOException;
        //  185    190    214    218    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 106 out of bounds for length 106
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || Data.class != o.getClass()) {
            return false;
        }
        final Data data = (Data)o;
        final Set<String> keySet = this.mValues.keySet();
        if (!keySet.equals(data.mValues.keySet())) {
            return false;
        }
        for (final String s : keySet) {
            final Object value = this.mValues.get(s);
            final Object value2 = data.mValues.get(s);
            boolean b;
            if (value != null && value2 != null) {
                if (value instanceof Object[] && value2 instanceof Object[]) {
                    b = Arrays.deepEquals((Object[])value, (Object[])value2);
                }
                else {
                    b = value.equals(value2);
                }
            }
            else {
                b = (value == value2);
            }
            if (!b) {
                return false;
            }
        }
        return true;
    }
    
    public boolean getBoolean(@NonNull final String s, final boolean b) {
        final Boolean value = this.mValues.get(s);
        if (value instanceof Boolean) {
            return value;
        }
        return b;
    }
    
    @Nullable
    public boolean[] getBooleanArray(@NonNull final String s) {
        final Boolean[] value = this.mValues.get(s);
        if (value instanceof Boolean[]) {
            return convertToPrimitiveArray(value);
        }
        return null;
    }
    
    public byte getByte(@NonNull final String s, final byte b) {
        final Byte value = this.mValues.get(s);
        if (value instanceof Byte) {
            return value;
        }
        return b;
    }
    
    @Nullable
    public byte[] getByteArray(@NonNull final String s) {
        final Byte[] value = this.mValues.get(s);
        if (value instanceof Byte[]) {
            return convertToPrimitiveArray(value);
        }
        return null;
    }
    
    public double getDouble(@NonNull final String s, final double n) {
        final Double value = this.mValues.get(s);
        if (value instanceof Double) {
            return value;
        }
        return n;
    }
    
    @Nullable
    public double[] getDoubleArray(@NonNull final String s) {
        final Double[] value = this.mValues.get(s);
        if (value instanceof Double[]) {
            return convertToPrimitiveArray(value);
        }
        return null;
    }
    
    public float getFloat(@NonNull final String s, final float n) {
        final Float value = this.mValues.get(s);
        if (value instanceof Float) {
            return value;
        }
        return n;
    }
    
    @Nullable
    public float[] getFloatArray(@NonNull final String s) {
        final Float[] value = this.mValues.get(s);
        if (value instanceof Float[]) {
            return convertToPrimitiveArray(value);
        }
        return null;
    }
    
    public int getInt(@NonNull final String s, final int n) {
        final Integer value = this.mValues.get(s);
        if (value instanceof Integer) {
            return value;
        }
        return n;
    }
    
    @Nullable
    public int[] getIntArray(@NonNull final String s) {
        final Integer[] value = this.mValues.get(s);
        if (value instanceof Integer[]) {
            return convertToPrimitiveArray(value);
        }
        return null;
    }
    
    @NonNull
    public Map<String, Object> getKeyValueMap() {
        return Collections.unmodifiableMap((Map<? extends String, ?>)this.mValues);
    }
    
    public long getLong(@NonNull final String s, final long n) {
        final Long value = this.mValues.get(s);
        if (value instanceof Long) {
            return value;
        }
        return n;
    }
    
    @Nullable
    public long[] getLongArray(@NonNull final String s) {
        final Long[] value = this.mValues.get(s);
        if (value instanceof Long[]) {
            return convertToPrimitiveArray(value);
        }
        return null;
    }
    
    @Nullable
    public String getString(@NonNull final String s) {
        final String value = this.mValues.get(s);
        if (value instanceof String) {
            return value;
        }
        return null;
    }
    
    @Nullable
    public String[] getStringArray(@NonNull final String s) {
        final String[] value = this.mValues.get(s);
        if (value instanceof String[]) {
            return value;
        }
        return null;
    }
    
    public <T> boolean hasKeyWithValueOfType(@NonNull final String s, @NonNull final Class<T> clazz) {
        final Object value = this.mValues.get(s);
        return value != null && clazz.isAssignableFrom(value.getClass());
    }
    
    @Override
    public int hashCode() {
        return this.mValues.hashCode() * 31;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @VisibleForTesting
    public int size() {
        return this.mValues.size();
    }
    
    @NonNull
    public byte[] toByteArray() {
        return toByteArrayInternal(this);
    }
    
    @NonNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Data {");
        if (!this.mValues.isEmpty()) {
            for (final String str : this.mValues.keySet()) {
                sb.append(str);
                sb.append(" : ");
                final Object value = this.mValues.get(str);
                if (value instanceof Object[]) {
                    sb.append(Arrays.toString((Object[])value));
                }
                else {
                    sb.append(value);
                }
                sb.append(", ");
            }
        }
        sb.append("}");
        return sb.toString();
    }
    
    public static final class Builder
    {
        private Map<String, Object> mValues;
        
        public Builder() {
            this.mValues = new HashMap<String, Object>();
        }
        
        @NonNull
        public Data build() {
            final Data data = new Data(this.mValues);
            Data.toByteArrayInternal(data);
            return data;
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public Builder put(@NonNull final String s, @Nullable final Object o) {
            if (o == null) {
                this.mValues.put(s, null);
            }
            else {
                final Class<?> class1 = o.getClass();
                if (class1 != Boolean.class && class1 != Byte.class && class1 != Integer.class && class1 != Long.class && class1 != Float.class && class1 != Double.class && class1 != String.class && class1 != Boolean[].class && class1 != Byte[].class && class1 != Integer[].class && class1 != Long[].class && class1 != Float[].class && class1 != Double[].class && class1 != String[].class) {
                    if (class1 == boolean[].class) {
                        this.mValues.put(s, Data.convertPrimitiveBooleanArray((boolean[])o));
                    }
                    else if (class1 == byte[].class) {
                        this.mValues.put(s, Data.convertPrimitiveByteArray((byte[])o));
                    }
                    else if (class1 == int[].class) {
                        this.mValues.put(s, Data.convertPrimitiveIntArray((int[])o));
                    }
                    else if (class1 == long[].class) {
                        this.mValues.put(s, Data.convertPrimitiveLongArray((long[])o));
                    }
                    else if (class1 == float[].class) {
                        this.mValues.put(s, Data.convertPrimitiveFloatArray((float[])o));
                    }
                    else {
                        if (class1 != double[].class) {
                            throw new IllegalArgumentException(String.format("Key %s has invalid type %s", s, class1));
                        }
                        this.mValues.put(s, Data.convertPrimitiveDoubleArray((double[])o));
                    }
                }
                else {
                    this.mValues.put(s, o);
                }
            }
            return this;
        }
        
        @NonNull
        public Builder putAll(@NonNull final Data data) {
            this.putAll(data.mValues);
            return this;
        }
        
        @NonNull
        public Builder putAll(@NonNull final Map<String, Object> map) {
            for (final Map.Entry<String, V> entry : map.entrySet()) {
                this.put(entry.getKey(), entry.getValue());
            }
            return this;
        }
        
        @NonNull
        public Builder putBoolean(@NonNull final String s, final boolean b) {
            this.mValues.put(s, b);
            return this;
        }
        
        @NonNull
        public Builder putBooleanArray(@NonNull final String s, @NonNull final boolean[] array) {
            this.mValues.put(s, Data.convertPrimitiveBooleanArray(array));
            return this;
        }
        
        @NonNull
        public Builder putByte(@NonNull final String s, final byte b) {
            this.mValues.put(s, b);
            return this;
        }
        
        @NonNull
        public Builder putByteArray(@NonNull final String s, @NonNull final byte[] array) {
            this.mValues.put(s, Data.convertPrimitiveByteArray(array));
            return this;
        }
        
        @NonNull
        public Builder putDouble(@NonNull final String s, final double d) {
            this.mValues.put(s, d);
            return this;
        }
        
        @NonNull
        public Builder putDoubleArray(@NonNull final String s, @NonNull final double[] array) {
            this.mValues.put(s, Data.convertPrimitiveDoubleArray(array));
            return this;
        }
        
        @NonNull
        public Builder putFloat(@NonNull final String s, final float f) {
            this.mValues.put(s, f);
            return this;
        }
        
        @NonNull
        public Builder putFloatArray(@NonNull final String s, @NonNull final float[] array) {
            this.mValues.put(s, Data.convertPrimitiveFloatArray(array));
            return this;
        }
        
        @NonNull
        public Builder putInt(@NonNull final String s, final int i) {
            this.mValues.put(s, i);
            return this;
        }
        
        @NonNull
        public Builder putIntArray(@NonNull final String s, @NonNull final int[] array) {
            this.mValues.put(s, Data.convertPrimitiveIntArray(array));
            return this;
        }
        
        @NonNull
        public Builder putLong(@NonNull final String s, final long l) {
            this.mValues.put(s, l);
            return this;
        }
        
        @NonNull
        public Builder putLongArray(@NonNull final String s, @NonNull final long[] array) {
            this.mValues.put(s, Data.convertPrimitiveLongArray(array));
            return this;
        }
        
        @NonNull
        public Builder putString(@NonNull final String s, @Nullable final String s2) {
            this.mValues.put(s, s2);
            return this;
        }
        
        @NonNull
        public Builder putStringArray(@NonNull final String s, @NonNull final String[] array) {
            this.mValues.put(s, array);
            return this;
        }
    }
}

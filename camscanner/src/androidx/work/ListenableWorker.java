// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import androidx.annotation.MainThread;
import android.net.Uri;
import java.util.List;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import java.util.Set;
import androidx.annotation.IntRange;
import androidx.annotation.RequiresApi;
import androidx.annotation.Nullable;
import android.net.Network;
import java.util.UUID;
import androidx.work.impl.utils.futures.SettableFuture;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.annotation.RestrictTo;
import java.util.concurrent.Executor;
import androidx.annotation.Keep;
import android.annotation.SuppressLint;
import androidx.annotation.NonNull;
import android.content.Context;

public abstract class ListenableWorker
{
    @NonNull
    private Context mAppContext;
    private boolean mRunInForeground;
    private volatile boolean mStopped;
    private boolean mUsed;
    @NonNull
    private WorkerParameters mWorkerParams;
    
    @SuppressLint({ "BanKeepAnnotation" })
    @Keep
    public ListenableWorker(@NonNull final Context mAppContext, @NonNull final WorkerParameters mWorkerParams) {
        if (mAppContext == null) {
            throw new IllegalArgumentException("Application Context is null");
        }
        if (mWorkerParams != null) {
            this.mAppContext = mAppContext;
            this.mWorkerParams = mWorkerParams;
            return;
        }
        throw new IllegalArgumentException("WorkerParameters is null");
    }
    
    @NonNull
    public final Context getApplicationContext() {
        return this.mAppContext;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public Executor getBackgroundExecutor() {
        return this.mWorkerParams.getBackgroundExecutor();
    }
    
    @NonNull
    public ListenableFuture<ForegroundInfo> getForegroundInfoAsync() {
        final SettableFuture<Object> create = SettableFuture.create();
        create.setException(new IllegalStateException("Expedited WorkRequests require a ListenableWorker to provide an implementation for `getForegroundInfoAsync()`"));
        return (ListenableFuture<ForegroundInfo>)create;
    }
    
    @NonNull
    public final UUID getId() {
        return this.mWorkerParams.getId();
    }
    
    @NonNull
    public final Data getInputData() {
        return this.mWorkerParams.getInputData();
    }
    
    @Nullable
    @RequiresApi(28)
    public final Network getNetwork() {
        return this.mWorkerParams.getNetwork();
    }
    
    @IntRange(from = 0L)
    public final int getRunAttemptCount() {
        return this.mWorkerParams.getRunAttemptCount();
    }
    
    @NonNull
    public final Set<String> getTags() {
        return this.mWorkerParams.getTags();
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public TaskExecutor getTaskExecutor() {
        return this.mWorkerParams.getTaskExecutor();
    }
    
    @NonNull
    @RequiresApi(24)
    public final List<String> getTriggeredContentAuthorities() {
        return this.mWorkerParams.getTriggeredContentAuthorities();
    }
    
    @NonNull
    @RequiresApi(24)
    public final List<Uri> getTriggeredContentUris() {
        return this.mWorkerParams.getTriggeredContentUris();
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public WorkerFactory getWorkerFactory() {
        return this.mWorkerParams.getWorkerFactory();
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public boolean isRunInForeground() {
        return this.mRunInForeground;
    }
    
    public final boolean isStopped() {
        return this.mStopped;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public final boolean isUsed() {
        return this.mUsed;
    }
    
    public void onStopped() {
    }
    
    @NonNull
    public final ListenableFuture<Void> setForegroundAsync(@NonNull final ForegroundInfo foregroundInfo) {
        this.mRunInForeground = true;
        return this.mWorkerParams.getForegroundUpdater().setForegroundAsync(this.getApplicationContext(), this.getId(), foregroundInfo);
    }
    
    @NonNull
    public ListenableFuture<Void> setProgressAsync(@NonNull final Data data) {
        return this.mWorkerParams.getProgressUpdater().updateProgress(this.getApplicationContext(), this.getId(), data);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void setRunInForeground(final boolean mRunInForeground) {
        this.mRunInForeground = mRunInForeground;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public final void setUsed() {
        this.mUsed = true;
    }
    
    @MainThread
    @NonNull
    public abstract ListenableFuture<Result> startWork();
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public final void stop() {
        this.mStopped = true;
        this.onStopped();
    }
    
    public abstract static class Result
    {
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        Result() {
        }
        
        @NonNull
        public static Result failure() {
            return new Failure();
        }
        
        @NonNull
        public static Result failure(@NonNull final Data data) {
            return new Failure(data);
        }
        
        @NonNull
        public static Result retry() {
            return new Retry();
        }
        
        @NonNull
        public static Result success() {
            return new Success();
        }
        
        @NonNull
        public static Result success(@NonNull final Data data) {
            return new Success(data);
        }
        
        @NonNull
        public abstract Data getOutputData();
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public static final class Failure extends Result
        {
            private final Data mOutputData;
            
            public Failure() {
                this(Data.EMPTY);
            }
            
            public Failure(@NonNull final Data mOutputData) {
                this.mOutputData = mOutputData;
            }
            
            @Override
            public boolean equals(final Object o) {
                return this == o || (o != null && Failure.class == o.getClass() && this.mOutputData.equals(((Failure)o).mOutputData));
            }
            
            @NonNull
            @Override
            public Data getOutputData() {
                return this.mOutputData;
            }
            
            @Override
            public int hashCode() {
                return Failure.class.getName().hashCode() * 31 + this.mOutputData.hashCode();
            }
            
            @Override
            public String toString() {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failure {mOutputData=");
                sb.append(this.mOutputData);
                sb.append('}');
                return sb.toString();
            }
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public static final class Retry extends Result
        {
            @Override
            public boolean equals(final Object o) {
                boolean b = true;
                if (this == o) {
                    return true;
                }
                if (o == null || Retry.class != o.getClass()) {
                    b = false;
                }
                return b;
            }
            
            @NonNull
            @Override
            public Data getOutputData() {
                return Data.EMPTY;
            }
            
            @Override
            public int hashCode() {
                return Retry.class.getName().hashCode();
            }
            
            @Override
            public String toString() {
                return "Retry";
            }
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public static final class Success extends Result
        {
            private final Data mOutputData;
            
            public Success() {
                this(Data.EMPTY);
            }
            
            public Success(@NonNull final Data mOutputData) {
                this.mOutputData = mOutputData;
            }
            
            @Override
            public boolean equals(final Object o) {
                return this == o || (o != null && Success.class == o.getClass() && this.mOutputData.equals(((Success)o).mOutputData));
            }
            
            @NonNull
            @Override
            public Data getOutputData() {
                return this.mOutputData;
            }
            
            @Override
            public int hashCode() {
                return Success.class.getName().hashCode() * 31 + this.mOutputData.hashCode();
            }
            
            @Override
            public String toString() {
                final StringBuilder sb = new StringBuilder();
                sb.append("Success {mOutputData=");
                sb.append(this.mOutputData);
                sb.append('}');
                return sb.toString();
            }
        }
    }
}

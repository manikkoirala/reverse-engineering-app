// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import androidx.annotation.VisibleForTesting;
import androidx.annotation.Nullable;
import java.util.Iterator;
import android.content.Context;
import androidx.annotation.NonNull;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.List;

public class DelegatingWorkerFactory extends WorkerFactory
{
    private static final String TAG;
    private final List<WorkerFactory> mFactories;
    
    static {
        TAG = Logger.tagWithPrefix("DelegatingWkrFctry");
    }
    
    public DelegatingWorkerFactory() {
        this.mFactories = new CopyOnWriteArrayList<WorkerFactory>();
    }
    
    public final void addFactory(@NonNull final WorkerFactory workerFactory) {
        this.mFactories.add(workerFactory);
    }
    
    @Nullable
    @Override
    public final ListenableWorker createWorker(@NonNull final Context context, @NonNull String format, @NonNull final WorkerParameters workerParameters) {
        for (final WorkerFactory workerFactory : this.mFactories) {
            try {
                final ListenableWorker worker = workerFactory.createWorker(context, format, workerParameters);
                if (worker != null) {
                    return worker;
                }
                continue;
            }
            finally {
                format = String.format("Unable to instantiate a ListenableWorker (%s)", format);
                Logger.get().error(DelegatingWorkerFactory.TAG, format, (Throwable)context);
            }
            break;
        }
        return null;
    }
    
    @NonNull
    @VisibleForTesting
    List<WorkerFactory> getFactories() {
        return this.mFactories;
    }
}

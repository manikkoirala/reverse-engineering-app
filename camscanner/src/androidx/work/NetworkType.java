// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import androidx.annotation.RequiresApi;

public enum NetworkType
{
    private static final NetworkType[] $VALUES;
    
    CONNECTED, 
    METERED, 
    NOT_REQUIRED, 
    NOT_ROAMING, 
    @RequiresApi(30)
    TEMPORARILY_UNMETERED, 
    UNMETERED;
}

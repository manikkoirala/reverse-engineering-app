// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import androidx.lifecycle.LiveData;
import com.google.common.util.concurrent.ListenableFuture;
import android.app.PendingIntent;
import java.util.UUID;
import java.util.List;
import java.util.Collections;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.work.impl.WorkManagerImpl;
import androidx.annotation.RestrictTo;
import android.annotation.SuppressLint;

@SuppressLint({ "AddedAbstractMethod" })
public abstract class WorkManager
{
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected WorkManager() {
    }
    
    @Deprecated
    @NonNull
    public static WorkManager getInstance() {
        final WorkManagerImpl instance = WorkManagerImpl.getInstance();
        if (instance != null) {
            return instance;
        }
        throw new IllegalStateException("WorkManager is not initialized properly.  The most likely cause is that you disabled WorkManagerInitializer in your manifest but forgot to call WorkManager#initialize in your Application#onCreate or a ContentProvider.");
    }
    
    @NonNull
    public static WorkManager getInstance(@NonNull final Context context) {
        return WorkManagerImpl.getInstance(context);
    }
    
    public static void initialize(@NonNull final Context context, @NonNull final Configuration configuration) {
        WorkManagerImpl.initialize(context, configuration);
    }
    
    @NonNull
    public final WorkContinuation beginUniqueWork(@NonNull final String s, @NonNull final ExistingWorkPolicy existingWorkPolicy, @NonNull final OneTimeWorkRequest o) {
        return this.beginUniqueWork(s, existingWorkPolicy, Collections.singletonList(o));
    }
    
    @NonNull
    public abstract WorkContinuation beginUniqueWork(@NonNull final String p0, @NonNull final ExistingWorkPolicy p1, @NonNull final List<OneTimeWorkRequest> p2);
    
    @NonNull
    public final WorkContinuation beginWith(@NonNull final OneTimeWorkRequest o) {
        return this.beginWith(Collections.singletonList(o));
    }
    
    @NonNull
    public abstract WorkContinuation beginWith(@NonNull final List<OneTimeWorkRequest> p0);
    
    @NonNull
    public abstract Operation cancelAllWork();
    
    @NonNull
    public abstract Operation cancelAllWorkByTag(@NonNull final String p0);
    
    @NonNull
    public abstract Operation cancelUniqueWork(@NonNull final String p0);
    
    @NonNull
    public abstract Operation cancelWorkById(@NonNull final UUID p0);
    
    @NonNull
    public abstract PendingIntent createCancelPendingIntent(@NonNull final UUID p0);
    
    @NonNull
    public final Operation enqueue(@NonNull final WorkRequest o) {
        return this.enqueue(Collections.singletonList(o));
    }
    
    @NonNull
    public abstract Operation enqueue(@NonNull final List<? extends WorkRequest> p0);
    
    @NonNull
    public abstract Operation enqueueUniquePeriodicWork(@NonNull final String p0, @NonNull final ExistingPeriodicWorkPolicy p1, @NonNull final PeriodicWorkRequest p2);
    
    @NonNull
    public Operation enqueueUniqueWork(@NonNull final String s, @NonNull final ExistingWorkPolicy existingWorkPolicy, @NonNull final OneTimeWorkRequest o) {
        return this.enqueueUniqueWork(s, existingWorkPolicy, Collections.singletonList(o));
    }
    
    @NonNull
    public abstract Operation enqueueUniqueWork(@NonNull final String p0, @NonNull final ExistingWorkPolicy p1, @NonNull final List<OneTimeWorkRequest> p2);
    
    @NonNull
    public abstract ListenableFuture<Long> getLastCancelAllTimeMillis();
    
    @NonNull
    public abstract LiveData<Long> getLastCancelAllTimeMillisLiveData();
    
    @NonNull
    public abstract ListenableFuture<WorkInfo> getWorkInfoById(@NonNull final UUID p0);
    
    @NonNull
    public abstract LiveData<WorkInfo> getWorkInfoByIdLiveData(@NonNull final UUID p0);
    
    @NonNull
    public abstract ListenableFuture<List<WorkInfo>> getWorkInfos(@NonNull final WorkQuery p0);
    
    @NonNull
    public abstract ListenableFuture<List<WorkInfo>> getWorkInfosByTag(@NonNull final String p0);
    
    @NonNull
    public abstract LiveData<List<WorkInfo>> getWorkInfosByTagLiveData(@NonNull final String p0);
    
    @NonNull
    public abstract ListenableFuture<List<WorkInfo>> getWorkInfosForUniqueWork(@NonNull final String p0);
    
    @NonNull
    public abstract LiveData<List<WorkInfo>> getWorkInfosForUniqueWorkLiveData(@NonNull final String p0);
    
    @NonNull
    public abstract LiveData<List<WorkInfo>> getWorkInfosLiveData(@NonNull final WorkQuery p0);
    
    @NonNull
    public abstract Operation pruneWork();
}

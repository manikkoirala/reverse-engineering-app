// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import androidx.annotation.RestrictTo;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.content.Context;

public abstract class WorkerFactory
{
    private static final String TAG;
    
    static {
        TAG = Logger.tagWithPrefix("WorkerFactory");
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static WorkerFactory getDefaultWorkerFactory() {
        return new WorkerFactory() {
            @Nullable
            @Override
            public ListenableWorker createWorker(@NonNull final Context context, @NonNull final String s, @NonNull final WorkerParameters workerParameters) {
                return null;
            }
        };
    }
    
    @Nullable
    public abstract ListenableWorker createWorker(@NonNull final Context p0, @NonNull final String p1, @NonNull final WorkerParameters p2);
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public final ListenableWorker createWorkerWithDefaultFallback(@NonNull final Context context, @NonNull final String str, @NonNull final WorkerParameters workerParameters) {
        ListenableWorker worker;
        final ListenableWorker listenableWorker = worker = this.createWorker(context, str, workerParameters);
        if (listenableWorker == null) {
            Class clazz = null;
            try {
                Class.forName(str).asSubclass(ListenableWorker.class);
            }
            finally {
                final Logger value = Logger.get();
                final String tag = WorkerFactory.TAG;
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid class: ");
                sb.append(str);
                final Throwable t;
                value.error(tag, sb.toString(), t);
                clazz = null;
            }
            worker = listenableWorker;
            if (clazz != null) {
                try {
                    final ListenableWorker listenableWorker2 = (ListenableWorker)clazz.getDeclaredConstructor(Context.class, WorkerParameters.class).newInstance(context, workerParameters);
                }
                finally {
                    final Logger value2 = Logger.get();
                    final String tag2 = WorkerFactory.TAG;
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Could not instantiate ");
                    sb2.append(str);
                    final Throwable t2;
                    value2.error(tag2, sb2.toString(), t2);
                    worker = listenableWorker;
                }
            }
        }
        if (worker != null && worker.isUsed()) {
            throw new IllegalStateException(String.format("WorkerFactory (%s) returned an instance of a ListenableWorker (%s) which has already been invoked. createWorker() must always return a new instance of a ListenableWorker.", this.getClass().getName(), str));
        }
        return worker;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.annotation.RestrictTo;
import android.annotation.SuppressLint;

public interface Operation
{
    @SuppressLint({ "SyntheticAccessor" })
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final IN_PROGRESS IN_PROGRESS = new IN_PROGRESS();
    @SuppressLint({ "SyntheticAccessor" })
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static final SUCCESS SUCCESS = new SUCCESS();
    
    @NonNull
    ListenableFuture<SUCCESS> getResult();
    
    @NonNull
    LiveData<State> getState();
    
    public abstract static class State
    {
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        State() {
        }
        
        public static final class FAILURE extends State
        {
            private final Throwable mThrowable;
            
            public FAILURE(@NonNull final Throwable mThrowable) {
                this.mThrowable = mThrowable;
            }
            
            @NonNull
            public Throwable getThrowable() {
                return this.mThrowable;
            }
            
            @NonNull
            @Override
            public String toString() {
                return String.format("FAILURE (%s)", this.mThrowable.getMessage());
            }
        }
        
        public static final class IN_PROGRESS extends State
        {
            private IN_PROGRESS() {
            }
            
            @NonNull
            @Override
            public String toString() {
                return "IN_PROGRESS";
            }
        }
        
        public static final class SUCCESS extends State
        {
            private SUCCESS() {
            }
            
            @NonNull
            @Override
            public String toString() {
                return "SUCCESS";
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import androidx.annotation.NonNull;
import java.util.List;

public final class OverwritingInputMerger extends InputMerger
{
    @NonNull
    @Override
    public Data merge(@NonNull final List<Data> list) {
        final Data.Builder builder = new Data.Builder();
        final HashMap hashMap = new HashMap();
        final Iterator<Data> iterator = list.iterator();
        while (iterator.hasNext()) {
            hashMap.putAll(iterator.next().getKeyValueMap());
        }
        builder.putAll(hashMap);
        return builder.build();
    }
}

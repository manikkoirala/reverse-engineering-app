// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import java.util.Collection;
import android.annotation.SuppressLint;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import java.util.UUID;
import java.util.List;

public final class WorkQuery
{
    private final List<UUID> mIds;
    private final List<WorkInfo.State> mStates;
    private final List<String> mTags;
    private final List<String> mUniqueWorkNames;
    
    WorkQuery(@NonNull final Builder builder) {
        this.mIds = builder.mIds;
        this.mUniqueWorkNames = builder.mUniqueWorkNames;
        this.mTags = builder.mTags;
        this.mStates = builder.mStates;
    }
    
    @NonNull
    public List<UUID> getIds() {
        return this.mIds;
    }
    
    @NonNull
    public List<WorkInfo.State> getStates() {
        return this.mStates;
    }
    
    @NonNull
    public List<String> getTags() {
        return this.mTags;
    }
    
    @NonNull
    public List<String> getUniqueWorkNames() {
        return this.mUniqueWorkNames;
    }
    
    public static final class Builder
    {
        List<UUID> mIds;
        List<WorkInfo.State> mStates;
        List<String> mTags;
        List<String> mUniqueWorkNames;
        
        private Builder() {
            this.mIds = new ArrayList<UUID>();
            this.mUniqueWorkNames = new ArrayList<String>();
            this.mTags = new ArrayList<String>();
            this.mStates = new ArrayList<WorkInfo.State>();
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public static Builder fromIds(@NonNull final List<UUID> list) {
            final Builder builder = new Builder();
            builder.addIds(list);
            return builder;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public static Builder fromStates(@NonNull final List<WorkInfo.State> list) {
            final Builder builder = new Builder();
            builder.addStates(list);
            return builder;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public static Builder fromTags(@NonNull final List<String> list) {
            final Builder builder = new Builder();
            builder.addTags(list);
            return builder;
        }
        
        @SuppressLint({ "BuilderSetStyle" })
        @NonNull
        public static Builder fromUniqueWorkNames(@NonNull final List<String> list) {
            final Builder builder = new Builder();
            builder.addUniqueWorkNames(list);
            return builder;
        }
        
        @NonNull
        public Builder addIds(@NonNull final List<UUID> list) {
            this.mIds.addAll(list);
            return this;
        }
        
        @NonNull
        public Builder addStates(@NonNull final List<WorkInfo.State> list) {
            this.mStates.addAll(list);
            return this;
        }
        
        @NonNull
        public Builder addTags(@NonNull final List<String> list) {
            this.mTags.addAll(list);
            return this;
        }
        
        @NonNull
        public Builder addUniqueWorkNames(@NonNull final List<String> list) {
            this.mUniqueWorkNames.addAll(list);
            return this;
        }
        
        @NonNull
        public WorkQuery build() {
            if (this.mIds.isEmpty() && this.mUniqueWorkNames.isEmpty() && this.mTags.isEmpty() && this.mStates.isEmpty()) {
                throw new IllegalArgumentException("Must specify ids, uniqueNames, tags or states when building a WorkQuery");
            }
            return new WorkQuery(this);
        }
    }
}

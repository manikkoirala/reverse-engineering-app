// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;

public interface RunnableScheduler
{
    void cancel(@NonNull final Runnable p0);
    
    void scheduleWithDelay(@IntRange(from = 0L) final long p0, @NonNull final Runnable p1);
}

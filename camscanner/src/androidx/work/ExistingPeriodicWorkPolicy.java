// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

public enum ExistingPeriodicWorkPolicy
{
    private static final ExistingPeriodicWorkPolicy[] $VALUES;
    
    KEEP, 
    REPLACE;
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import androidx.annotation.VisibleForTesting;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.\u3007080;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import android.os.Build$VERSION;
import java.util.HashSet;
import androidx.annotation.RestrictTo;
import androidx.work.impl.model.WorkSpec;
import java.util.Set;
import androidx.annotation.NonNull;
import java.util.UUID;
import android.annotation.SuppressLint;

public abstract class WorkRequest
{
    public static final long DEFAULT_BACKOFF_DELAY_MILLIS = 30000L;
    @SuppressLint({ "MinMaxConstant" })
    public static final long MAX_BACKOFF_MILLIS = 18000000L;
    @SuppressLint({ "MinMaxConstant" })
    public static final long MIN_BACKOFF_MILLIS = 10000L;
    @NonNull
    private UUID mId;
    @NonNull
    private Set<String> mTags;
    @NonNull
    private WorkSpec mWorkSpec;
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    protected WorkRequest(@NonNull final UUID mId, @NonNull final WorkSpec mWorkSpec, @NonNull final Set<String> mTags) {
        this.mId = mId;
        this.mWorkSpec = mWorkSpec;
        this.mTags = mTags;
    }
    
    @NonNull
    public UUID getId() {
        return this.mId;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public String getStringId() {
        return this.mId.toString();
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public Set<String> getTags() {
        return this.mTags;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public WorkSpec getWorkSpec() {
        return this.mWorkSpec;
    }
    
    public abstract static class Builder<B extends Builder<?, ?>, W extends WorkRequest>
    {
        boolean mBackoffCriteriaSet;
        UUID mId;
        Set<String> mTags;
        WorkSpec mWorkSpec;
        Class<? extends ListenableWorker> mWorkerClass;
        
        Builder(@NonNull final Class<? extends ListenableWorker> mWorkerClass) {
            this.mBackoffCriteriaSet = false;
            this.mTags = new HashSet<String>();
            this.mId = UUID.randomUUID();
            this.mWorkerClass = mWorkerClass;
            this.mWorkSpec = new WorkSpec(this.mId.toString(), mWorkerClass.getName());
            this.addTag(mWorkerClass.getName());
        }
        
        @NonNull
        public final B addTag(@NonNull final String s) {
            this.mTags.add(s);
            return this.getThis();
        }
        
        @NonNull
        public final W build() {
            final WorkRequest buildInternal = this.buildInternal();
            final Constraints constraints = this.mWorkSpec.constraints;
            final int sdk_INT = Build$VERSION.SDK_INT;
            final boolean b = (sdk_INT >= 24 && constraints.hasContentUriTriggers()) || constraints.requiresBatteryNotLow() || constraints.requiresCharging() || (sdk_INT >= 23 && constraints.requiresDeviceIdle());
            if (this.mWorkSpec.expedited && b) {
                throw new IllegalArgumentException("Expedited jobs only support network and storage constraints");
            }
            this.mId = UUID.randomUUID();
            final WorkSpec mWorkSpec = new WorkSpec(this.mWorkSpec);
            this.mWorkSpec = mWorkSpec;
            mWorkSpec.id = this.mId.toString();
            return (W)buildInternal;
        }
        
        @NonNull
        abstract W buildInternal();
        
        @NonNull
        abstract B getThis();
        
        @NonNull
        public final B keepResultsForAtLeast(final long duration, @NonNull final TimeUnit timeUnit) {
            this.mWorkSpec.minimumRetentionDuration = timeUnit.toMillis(duration);
            return this.getThis();
        }
        
        @NonNull
        @RequiresApi(26)
        public final B keepResultsForAtLeast(@NonNull final Duration duration) {
            this.mWorkSpec.minimumRetentionDuration = \u3007080.\u3007080(duration);
            return this.getThis();
        }
        
        @NonNull
        public final B setBackoffCriteria(@NonNull final BackoffPolicy backoffPolicy, final long duration, @NonNull final TimeUnit timeUnit) {
            this.mBackoffCriteriaSet = true;
            final WorkSpec mWorkSpec = this.mWorkSpec;
            mWorkSpec.backoffPolicy = backoffPolicy;
            mWorkSpec.setBackoffDelayDuration(timeUnit.toMillis(duration));
            return this.getThis();
        }
        
        @NonNull
        @RequiresApi(26)
        public final B setBackoffCriteria(@NonNull final BackoffPolicy backoffPolicy, @NonNull final Duration duration) {
            this.mBackoffCriteriaSet = true;
            final WorkSpec mWorkSpec = this.mWorkSpec;
            mWorkSpec.backoffPolicy = backoffPolicy;
            mWorkSpec.setBackoffDelayDuration(\u3007080.\u3007080(duration));
            return this.getThis();
        }
        
        @NonNull
        public final B setConstraints(@NonNull final Constraints constraints) {
            this.mWorkSpec.constraints = constraints;
            return this.getThis();
        }
        
        @SuppressLint({ "MissingGetterMatchingBuilder" })
        @NonNull
        public B setExpedited(@NonNull final OutOfQuotaPolicy outOfQuotaPolicy) {
            final WorkSpec mWorkSpec = this.mWorkSpec;
            mWorkSpec.expedited = true;
            mWorkSpec.outOfQuotaPolicy = outOfQuotaPolicy;
            return this.getThis();
        }
        
        @NonNull
        public B setInitialDelay(final long duration, @NonNull final TimeUnit timeUnit) {
            this.mWorkSpec.initialDelay = timeUnit.toMillis(duration);
            if (Long.MAX_VALUE - System.currentTimeMillis() > this.mWorkSpec.initialDelay) {
                return this.getThis();
            }
            throw new IllegalArgumentException("The given initial delay is too large and will cause an overflow!");
        }
        
        @NonNull
        @RequiresApi(26)
        public B setInitialDelay(@NonNull final Duration duration) {
            this.mWorkSpec.initialDelay = \u3007080.\u3007080(duration);
            if (Long.MAX_VALUE - System.currentTimeMillis() > this.mWorkSpec.initialDelay) {
                return this.getThis();
            }
            throw new IllegalArgumentException("The given initial delay is too large and will cause an overflow!");
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        @VisibleForTesting
        public final B setInitialRunAttemptCount(final int runAttemptCount) {
            this.mWorkSpec.runAttemptCount = runAttemptCount;
            return this.getThis();
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        @VisibleForTesting
        public final B setInitialState(@NonNull final WorkInfo.State state) {
            this.mWorkSpec.state = state;
            return this.getThis();
        }
        
        @NonNull
        public final B setInputData(@NonNull final Data input) {
            this.mWorkSpec.input = input;
            return this.getThis();
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        @VisibleForTesting
        public final B setPeriodStartTime(final long duration, @NonNull final TimeUnit timeUnit) {
            this.mWorkSpec.periodStartTime = timeUnit.toMillis(duration);
            return this.getThis();
        }
        
        @NonNull
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        @VisibleForTesting
        public final B setScheduleRequestedAt(final long duration, @NonNull final TimeUnit timeUnit) {
            this.mWorkSpec.scheduleRequestedAt = timeUnit.toMillis(duration);
            return this.getThis();
        }
    }
}

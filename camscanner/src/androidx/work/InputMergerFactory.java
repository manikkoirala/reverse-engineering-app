// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import androidx.annotation.RestrictTo;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;

public abstract class InputMergerFactory
{
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static InputMergerFactory getDefaultInputMergerFactory() {
        return new InputMergerFactory() {
            @Nullable
            @Override
            public InputMerger createInputMerger(@NonNull final String s) {
                return null;
            }
        };
    }
    
    @Nullable
    public abstract InputMerger createInputMerger(@NonNull final String p0);
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public final InputMerger createInputMergerWithDefaultFallback(@NonNull final String s) {
        InputMerger inputMerger;
        if ((inputMerger = this.createInputMerger(s)) == null) {
            inputMerger = InputMerger.fromClassName(s);
        }
        return inputMerger;
    }
}

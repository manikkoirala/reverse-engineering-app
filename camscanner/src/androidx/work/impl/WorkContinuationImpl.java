// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import androidx.lifecycle.LiveData;
import androidx.work.impl.utils.StatusRunnable;
import androidx.work.WorkInfo;
import com.google.common.util.concurrent.ListenableFuture;
import android.text.TextUtils;
import androidx.work.impl.utils.EnqueueRunnable;
import java.util.Collections;
import androidx.work.InputMerger;
import androidx.work.ArrayCreatingInputMerger;
import androidx.work.ListenableWorker;
import androidx.work.impl.workers.CombineContinuationsWorker;
import androidx.work.OneTimeWorkRequest;
import java.util.HashSet;
import java.util.Set;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.work.Logger;
import androidx.work.WorkRequest;
import androidx.work.Operation;
import androidx.work.ExistingWorkPolicy;
import java.util.List;
import androidx.annotation.RestrictTo;
import androidx.work.WorkContinuation;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class WorkContinuationImpl extends WorkContinuation
{
    private static final String TAG;
    private final List<String> mAllIds;
    private boolean mEnqueued;
    private final ExistingWorkPolicy mExistingWorkPolicy;
    private final List<String> mIds;
    private final String mName;
    private Operation mOperation;
    private final List<WorkContinuationImpl> mParents;
    private final List<? extends WorkRequest> mWork;
    private final WorkManagerImpl mWorkManagerImpl;
    
    static {
        TAG = Logger.tagWithPrefix("WorkContinuationImpl");
    }
    
    public WorkContinuationImpl(@NonNull final WorkManagerImpl workManagerImpl, @Nullable final String s, @NonNull final ExistingWorkPolicy existingWorkPolicy, @NonNull final List<? extends WorkRequest> list) {
        this(workManagerImpl, s, existingWorkPolicy, list, null);
    }
    
    public WorkContinuationImpl(@NonNull final WorkManagerImpl mWorkManagerImpl, @Nullable final String mName, @NonNull final ExistingWorkPolicy mExistingWorkPolicy, @NonNull final List<? extends WorkRequest> mWork, @Nullable final List<WorkContinuationImpl> mParents) {
        this.mWorkManagerImpl = mWorkManagerImpl;
        this.mName = mName;
        this.mExistingWorkPolicy = mExistingWorkPolicy;
        this.mWork = mWork;
        this.mParents = mParents;
        this.mIds = new ArrayList<String>(mWork.size());
        this.mAllIds = new ArrayList<String>();
        if (mParents != null) {
            final Iterator<WorkContinuationImpl> iterator = mParents.iterator();
            while (iterator.hasNext()) {
                this.mAllIds.addAll(iterator.next().mAllIds);
            }
        }
        for (int i = 0; i < mWork.size(); ++i) {
            final String stringId = mWork.get(i).getStringId();
            this.mIds.add(stringId);
            this.mAllIds.add(stringId);
        }
    }
    
    public WorkContinuationImpl(@NonNull final WorkManagerImpl workManagerImpl, @NonNull final List<? extends WorkRequest> list) {
        this(workManagerImpl, null, ExistingWorkPolicy.KEEP, list, null);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    private static boolean hasCycles(@NonNull final WorkContinuationImpl workContinuationImpl, @NonNull final Set<String> set) {
        set.addAll(workContinuationImpl.getIds());
        final Set<String> prerequisites = prerequisitesFor(workContinuationImpl);
        final Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            if (prerequisites.contains(iterator.next())) {
                return true;
            }
        }
        final List<WorkContinuationImpl> parents = workContinuationImpl.getParents();
        if (parents != null && !parents.isEmpty()) {
            final Iterator iterator2 = parents.iterator();
            while (iterator2.hasNext()) {
                if (hasCycles((WorkContinuationImpl)iterator2.next(), set)) {
                    return true;
                }
            }
        }
        set.removeAll(workContinuationImpl.getIds());
        return false;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static Set<String> prerequisitesFor(final WorkContinuationImpl workContinuationImpl) {
        final HashSet set = new HashSet();
        final List<WorkContinuationImpl> parents = workContinuationImpl.getParents();
        if (parents != null && !parents.isEmpty()) {
            final Iterator iterator = parents.iterator();
            while (iterator.hasNext()) {
                set.addAll(((WorkContinuationImpl)iterator.next()).getIds());
            }
        }
        return set;
    }
    
    @NonNull
    @Override
    protected WorkContinuation combineInternal(@NonNull final List<WorkContinuation> list) {
        final OneTimeWorkRequest o = ((WorkRequest.Builder<B, OneTimeWorkRequest>)new OneTimeWorkRequest.Builder(CombineContinuationsWorker.class).setInputMerger(ArrayCreatingInputMerger.class)).build();
        final ArrayList list2 = new ArrayList(list.size());
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            list2.add(iterator.next());
        }
        return new WorkContinuationImpl(this.mWorkManagerImpl, null, ExistingWorkPolicy.KEEP, Collections.singletonList(o), list2);
    }
    
    @NonNull
    @Override
    public Operation enqueue() {
        if (!this.mEnqueued) {
            final EnqueueRunnable enqueueRunnable = new EnqueueRunnable(this);
            this.mWorkManagerImpl.getWorkTaskExecutor().executeOnBackgroundThread(enqueueRunnable);
            this.mOperation = enqueueRunnable.getOperation();
        }
        else {
            Logger.get().warning(WorkContinuationImpl.TAG, String.format("Already enqueued work ids (%s)", TextUtils.join((CharSequence)", ", (Iterable)this.mIds)), new Throwable[0]);
        }
        return this.mOperation;
    }
    
    public List<String> getAllIds() {
        return this.mAllIds;
    }
    
    public ExistingWorkPolicy getExistingWorkPolicy() {
        return this.mExistingWorkPolicy;
    }
    
    @NonNull
    public List<String> getIds() {
        return this.mIds;
    }
    
    @Nullable
    public String getName() {
        return this.mName;
    }
    
    public List<WorkContinuationImpl> getParents() {
        return this.mParents;
    }
    
    @NonNull
    public List<? extends WorkRequest> getWork() {
        return this.mWork;
    }
    
    @NonNull
    @Override
    public ListenableFuture<List<WorkInfo>> getWorkInfos() {
        final StatusRunnable<List<WorkInfo>> forStringIds = StatusRunnable.forStringIds(this.mWorkManagerImpl, this.mAllIds);
        this.mWorkManagerImpl.getWorkTaskExecutor().executeOnBackgroundThread(forStringIds);
        return forStringIds.getFuture();
    }
    
    @NonNull
    @Override
    public LiveData<List<WorkInfo>> getWorkInfosLiveData() {
        return this.mWorkManagerImpl.getWorkInfosById(this.mAllIds);
    }
    
    @NonNull
    public WorkManagerImpl getWorkManagerImpl() {
        return this.mWorkManagerImpl;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public boolean hasCycles() {
        return hasCycles(this, new HashSet<String>());
    }
    
    public boolean isEnqueued() {
        return this.mEnqueued;
    }
    
    public void markEnqueued() {
        this.mEnqueued = true;
    }
    
    @NonNull
    @Override
    public WorkContinuation then(@NonNull final List<OneTimeWorkRequest> list) {
        if (list.isEmpty()) {
            return this;
        }
        return new WorkContinuationImpl(this.mWorkManagerImpl, this.mName, ExistingWorkPolicy.KEEP, list, Collections.singletonList(this));
    }
}

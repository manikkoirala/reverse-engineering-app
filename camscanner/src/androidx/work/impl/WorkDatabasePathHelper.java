// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import androidx.annotation.RequiresApi;
import androidx.annotation.VisibleForTesting;
import android.os.Build$VERSION;
import java.io.File;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.work.Logger;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class WorkDatabasePathHelper
{
    private static final String[] DATABASE_EXTRA_FILES;
    private static final String TAG;
    private static final String WORK_DATABASE_NAME = "androidx.work.workdb";
    
    static {
        TAG = Logger.tagWithPrefix("WrkDbPathHelper");
        DATABASE_EXTRA_FILES = new String[] { "-journal", "-shm", "-wal" };
    }
    
    private WorkDatabasePathHelper() {
    }
    
    @NonNull
    @VisibleForTesting
    public static File getDatabasePath(@NonNull final Context context) {
        if (Build$VERSION.SDK_INT < 23) {
            return getDefaultDatabasePath(context);
        }
        return getNoBackupPath(context, "androidx.work.workdb");
    }
    
    @NonNull
    @VisibleForTesting
    public static File getDefaultDatabasePath(@NonNull final Context context) {
        return context.getDatabasePath("androidx.work.workdb");
    }
    
    @RequiresApi(23)
    private static File getNoBackupPath(@NonNull final Context context, @NonNull final String child) {
        return new File(context.getNoBackupFilesDir(), child);
    }
    
    @NonNull
    public static String getWorkDatabaseName() {
        return "androidx.work.workdb";
    }
    
    public static void migrateDatabase(@NonNull final Context context) {
        final File defaultDatabasePath = getDefaultDatabasePath(context);
        if (Build$VERSION.SDK_INT >= 23 && defaultDatabasePath.exists()) {
            Logger.get().debug(WorkDatabasePathHelper.TAG, "Migrating WorkDatabase to the no-backup directory", new Throwable[0]);
            final Map<File, File> migrationPaths = migrationPaths(context);
            for (final File file : migrationPaths.keySet()) {
                final File dest = migrationPaths.get(file);
                if (file.exists() && dest != null) {
                    if (dest.exists()) {
                        Logger.get().warning(WorkDatabasePathHelper.TAG, String.format("Over-writing contents of %s", dest), new Throwable[0]);
                    }
                    String s;
                    if (file.renameTo(dest)) {
                        s = String.format("Migrated %s to %s", file, dest);
                    }
                    else {
                        s = String.format("Renaming %s to %s failed", file, dest);
                    }
                    Logger.get().debug(WorkDatabasePathHelper.TAG, s, new Throwable[0]);
                }
            }
        }
    }
    
    @NonNull
    @VisibleForTesting
    public static Map<File, File> migrationPaths(@NonNull final Context context) {
        final HashMap hashMap = new HashMap();
        if (Build$VERSION.SDK_INT >= 23) {
            final File defaultDatabasePath = getDefaultDatabasePath(context);
            final File databasePath = getDatabasePath(context);
            hashMap.put(defaultDatabasePath, databasePath);
            for (final String s : WorkDatabasePathHelper.DATABASE_EXTRA_FILES) {
                final StringBuilder sb = new StringBuilder();
                sb.append(defaultDatabasePath.getPath());
                sb.append(s);
                final File file = new File(sb.toString());
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(databasePath.getPath());
                sb2.append(s);
                hashMap.put(file, new File(sb2.toString()));
            }
        }
        return hashMap;
    }
}

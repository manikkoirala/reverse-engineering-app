// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import java.util.UUID;
import androidx.room.Relation;
import java.util.Iterator;
import java.util.ArrayList;
import androidx.work.Logger;
import androidx.annotation.IntRange;
import androidx.work.OutOfQuotaPolicy;
import androidx.work.Data;
import androidx.room.PrimaryKey;
import androidx.room.Embedded;
import androidx.work.Constraints;
import androidx.annotation.NonNull;
import androidx.work.BackoffPolicy;
import androidx.room.ColumnInfo;
import androidx.work.WorkInfo;
import java.util.List;
import androidx.arch.core.util.Function;
import androidx.room.Index;
import androidx.room.Entity;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
@Entity(indices = { @Index({ "schedule_requested_at" }), @Index({ "period_start_time" }) })
public final class WorkSpec
{
    public static final long SCHEDULE_NOT_REQUESTED_YET = -1L;
    private static final String TAG;
    public static final Function<List<WorkInfoPojo>, List<WorkInfo>> WORK_INFO_MAPPER;
    @ColumnInfo(name = "backoff_delay_duration")
    public long backoffDelayDuration;
    @NonNull
    @ColumnInfo(name = "backoff_policy")
    public BackoffPolicy backoffPolicy;
    @NonNull
    @Embedded
    public Constraints constraints;
    @ColumnInfo(name = "run_in_foreground")
    public boolean expedited;
    @ColumnInfo(name = "flex_duration")
    public long flexDuration;
    @NonNull
    @ColumnInfo(name = "id")
    @PrimaryKey
    public String id;
    @ColumnInfo(name = "initial_delay")
    public long initialDelay;
    @NonNull
    @ColumnInfo(name = "input")
    public Data input;
    @ColumnInfo(name = "input_merger_class_name")
    public String inputMergerClassName;
    @ColumnInfo(name = "interval_duration")
    public long intervalDuration;
    @ColumnInfo(name = "minimum_retention_duration")
    public long minimumRetentionDuration;
    @NonNull
    @ColumnInfo(name = "out_of_quota_policy")
    public OutOfQuotaPolicy outOfQuotaPolicy;
    @NonNull
    @ColumnInfo(name = "output")
    public Data output;
    @ColumnInfo(name = "period_start_time")
    public long periodStartTime;
    @IntRange(from = 0L)
    @ColumnInfo(name = "run_attempt_count")
    public int runAttemptCount;
    @ColumnInfo(name = "schedule_requested_at")
    public long scheduleRequestedAt;
    @NonNull
    @ColumnInfo(name = "state")
    public WorkInfo.State state;
    @NonNull
    @ColumnInfo(name = "worker_class_name")
    public String workerClassName;
    
    static {
        TAG = Logger.tagWithPrefix("WorkSpec");
        WORK_INFO_MAPPER = new Function<List<WorkInfoPojo>, List<WorkInfo>>() {
            @Override
            public List<WorkInfo> apply(final List<WorkInfoPojo> list) {
                if (list == null) {
                    return null;
                }
                final ArrayList list2 = new ArrayList(list.size());
                final Iterator iterator = list.iterator();
                while (iterator.hasNext()) {
                    list2.add(((WorkInfoPojo)iterator.next()).toWorkInfo());
                }
                return list2;
            }
        };
    }
    
    public WorkSpec(@NonNull final WorkSpec workSpec) {
        this.state = WorkInfo.State.ENQUEUED;
        final Data empty = Data.EMPTY;
        this.input = empty;
        this.output = empty;
        this.constraints = Constraints.NONE;
        this.backoffPolicy = BackoffPolicy.EXPONENTIAL;
        this.backoffDelayDuration = 30000L;
        this.scheduleRequestedAt = -1L;
        this.outOfQuotaPolicy = OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST;
        this.id = workSpec.id;
        this.workerClassName = workSpec.workerClassName;
        this.state = workSpec.state;
        this.inputMergerClassName = workSpec.inputMergerClassName;
        this.input = new Data(workSpec.input);
        this.output = new Data(workSpec.output);
        this.initialDelay = workSpec.initialDelay;
        this.intervalDuration = workSpec.intervalDuration;
        this.flexDuration = workSpec.flexDuration;
        this.constraints = new Constraints(workSpec.constraints);
        this.runAttemptCount = workSpec.runAttemptCount;
        this.backoffPolicy = workSpec.backoffPolicy;
        this.backoffDelayDuration = workSpec.backoffDelayDuration;
        this.periodStartTime = workSpec.periodStartTime;
        this.minimumRetentionDuration = workSpec.minimumRetentionDuration;
        this.scheduleRequestedAt = workSpec.scheduleRequestedAt;
        this.expedited = workSpec.expedited;
        this.outOfQuotaPolicy = workSpec.outOfQuotaPolicy;
    }
    
    public WorkSpec(@NonNull final String id, @NonNull final String workerClassName) {
        this.state = WorkInfo.State.ENQUEUED;
        final Data empty = Data.EMPTY;
        this.input = empty;
        this.output = empty;
        this.constraints = Constraints.NONE;
        this.backoffPolicy = BackoffPolicy.EXPONENTIAL;
        this.backoffDelayDuration = 30000L;
        this.scheduleRequestedAt = -1L;
        this.outOfQuotaPolicy = OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST;
        this.id = id;
        this.workerClassName = workerClassName;
    }
    
    public long calculateNextRunTime() {
        final boolean backedOff = this.isBackedOff();
        boolean b = false;
        final int n = 0;
        if (backedOff) {
            int n2 = n;
            if (this.backoffPolicy == BackoffPolicy.LINEAR) {
                n2 = 1;
            }
            long b2;
            if (n2 != 0) {
                b2 = this.backoffDelayDuration * this.runAttemptCount;
            }
            else {
                b2 = (long)Math.scalb((float)this.backoffDelayDuration, this.runAttemptCount - 1);
            }
            return this.periodStartTime + Math.min(18000000L, b2);
        }
        final boolean periodic = this.isPeriodic();
        long n3 = 0L;
        if (!periodic) {
            long n4;
            if ((n4 = this.periodStartTime) == 0L) {
                n4 = System.currentTimeMillis();
            }
            return n4 + this.initialDelay;
        }
        final long currentTimeMillis = System.currentTimeMillis();
        final long periodStartTime = this.periodStartTime;
        long n5;
        if (periodStartTime == 0L) {
            n5 = currentTimeMillis + this.initialDelay;
        }
        else {
            n5 = periodStartTime;
        }
        final long flexDuration = this.flexDuration;
        final long intervalDuration = this.intervalDuration;
        if (flexDuration != intervalDuration) {
            b = true;
        }
        if (b) {
            if (periodStartTime == 0L) {
                n3 = flexDuration * -1L;
            }
            return n5 + intervalDuration + n3;
        }
        if (periodStartTime != 0L) {
            n3 = intervalDuration;
        }
        return n5 + n3;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o == null || WorkSpec.class != o.getClass()) {
            return false;
        }
        final WorkSpec workSpec = (WorkSpec)o;
        if (this.initialDelay != workSpec.initialDelay) {
            return false;
        }
        if (this.intervalDuration != workSpec.intervalDuration) {
            return false;
        }
        if (this.flexDuration != workSpec.flexDuration) {
            return false;
        }
        if (this.runAttemptCount != workSpec.runAttemptCount) {
            return false;
        }
        if (this.backoffDelayDuration != workSpec.backoffDelayDuration) {
            return false;
        }
        if (this.periodStartTime != workSpec.periodStartTime) {
            return false;
        }
        if (this.minimumRetentionDuration != workSpec.minimumRetentionDuration) {
            return false;
        }
        if (this.scheduleRequestedAt != workSpec.scheduleRequestedAt) {
            return false;
        }
        if (this.expedited != workSpec.expedited) {
            return false;
        }
        if (!this.id.equals(workSpec.id)) {
            return false;
        }
        if (this.state != workSpec.state) {
            return false;
        }
        if (!this.workerClassName.equals(workSpec.workerClassName)) {
            return false;
        }
        final String inputMergerClassName = this.inputMergerClassName;
        Label_0231: {
            if (inputMergerClassName != null) {
                if (inputMergerClassName.equals(workSpec.inputMergerClassName)) {
                    break Label_0231;
                }
            }
            else if (workSpec.inputMergerClassName == null) {
                break Label_0231;
            }
            return false;
        }
        if (!this.input.equals(workSpec.input)) {
            return false;
        }
        if (!this.output.equals(workSpec.output)) {
            return false;
        }
        if (!this.constraints.equals(workSpec.constraints)) {
            return false;
        }
        if (this.backoffPolicy != workSpec.backoffPolicy) {
            return false;
        }
        if (this.outOfQuotaPolicy != workSpec.outOfQuotaPolicy) {
            b = false;
        }
        return b;
    }
    
    public boolean hasConstraints() {
        return Constraints.NONE.equals(this.constraints) ^ true;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.id.hashCode();
        final int hashCode2 = this.state.hashCode();
        final int hashCode3 = this.workerClassName.hashCode();
        final String inputMergerClassName = this.inputMergerClassName;
        int hashCode4;
        if (inputMergerClassName != null) {
            hashCode4 = inputMergerClassName.hashCode();
        }
        else {
            hashCode4 = 0;
        }
        final int hashCode5 = this.input.hashCode();
        final int hashCode6 = this.output.hashCode();
        final long initialDelay = this.initialDelay;
        final int n = (int)(initialDelay ^ initialDelay >>> 32);
        final long intervalDuration = this.intervalDuration;
        final int n2 = (int)(intervalDuration ^ intervalDuration >>> 32);
        final long flexDuration = this.flexDuration;
        final int n3 = (int)(flexDuration ^ flexDuration >>> 32);
        final int hashCode7 = this.constraints.hashCode();
        final int runAttemptCount = this.runAttemptCount;
        final int hashCode8 = this.backoffPolicy.hashCode();
        final long backoffDelayDuration = this.backoffDelayDuration;
        final int n4 = (int)(backoffDelayDuration ^ backoffDelayDuration >>> 32);
        final long periodStartTime = this.periodStartTime;
        final int n5 = (int)(periodStartTime ^ periodStartTime >>> 32);
        final long minimumRetentionDuration = this.minimumRetentionDuration;
        final int n6 = (int)(minimumRetentionDuration ^ minimumRetentionDuration >>> 32);
        final long scheduleRequestedAt = this.scheduleRequestedAt;
        return ((((((((((((((((hashCode * 31 + hashCode2) * 31 + hashCode3) * 31 + hashCode4) * 31 + hashCode5) * 31 + hashCode6) * 31 + n) * 31 + n2) * 31 + n3) * 31 + hashCode7) * 31 + runAttemptCount) * 31 + hashCode8) * 31 + n4) * 31 + n5) * 31 + n6) * 31 + (int)(scheduleRequestedAt ^ scheduleRequestedAt >>> 32)) * 31 + (this.expedited ? 1 : 0)) * 31 + this.outOfQuotaPolicy.hashCode();
    }
    
    public boolean isBackedOff() {
        return this.state == WorkInfo.State.ENQUEUED && this.runAttemptCount > 0;
    }
    
    public boolean isPeriodic() {
        return this.intervalDuration != 0L;
    }
    
    public void setBackoffDelayDuration(long backoffDelayDuration) {
        long n = backoffDelayDuration;
        if (backoffDelayDuration > 18000000L) {
            Logger.get().warning(WorkSpec.TAG, "Backoff delay duration exceeds maximum value", new Throwable[0]);
            n = 18000000L;
        }
        backoffDelayDuration = n;
        if (n < 10000L) {
            Logger.get().warning(WorkSpec.TAG, "Backoff delay duration less than minimum value", new Throwable[0]);
            backoffDelayDuration = 10000L;
        }
        this.backoffDelayDuration = backoffDelayDuration;
    }
    
    public void setPeriodic(final long n) {
        long n2 = n;
        if (n < 900000L) {
            Logger.get().warning(WorkSpec.TAG, String.format("Interval duration lesser than minimum allowed value; Changed to %s", 900000L), new Throwable[0]);
            n2 = 900000L;
        }
        this.setPeriodic(n2, n2);
    }
    
    public void setPeriodic(long n, long flexDuration) {
        long n2 = n;
        if (n < 900000L) {
            Logger.get().warning(WorkSpec.TAG, String.format("Interval duration lesser than minimum allowed value; Changed to %s", 900000L), new Throwable[0]);
            n2 = 900000L;
        }
        n = flexDuration;
        if (flexDuration < 300000L) {
            Logger.get().warning(WorkSpec.TAG, String.format("Flex duration lesser than minimum allowed value; Changed to %s", 300000L), new Throwable[0]);
            n = 300000L;
        }
        flexDuration = n;
        if (n > n2) {
            Logger.get().warning(WorkSpec.TAG, String.format("Flex duration greater than interval duration; Changed to %s", n2), new Throwable[0]);
            flexDuration = n2;
        }
        this.intervalDuration = n2;
        this.flexDuration = flexDuration;
    }
    
    @NonNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("{WorkSpec: ");
        sb.append(this.id);
        sb.append("}");
        return sb.toString();
    }
    
    public static class IdAndState
    {
        @ColumnInfo(name = "id")
        public String id;
        @ColumnInfo(name = "state")
        public WorkInfo.State state;
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof IdAndState)) {
                return false;
            }
            final IdAndState idAndState = (IdAndState)o;
            return this.state == idAndState.state && this.id.equals(idAndState.id);
        }
        
        @Override
        public int hashCode() {
            return this.id.hashCode() * 31 + this.state.hashCode();
        }
    }
    
    public static class WorkInfoPojo
    {
        @ColumnInfo(name = "id")
        public String id;
        @ColumnInfo(name = "output")
        public Data output;
        @Relation(entity = WorkProgress.class, entityColumn = "work_spec_id", parentColumn = "id", projection = { "progress" })
        public List<Data> progress;
        @ColumnInfo(name = "run_attempt_count")
        public int runAttemptCount;
        @ColumnInfo(name = "state")
        public WorkInfo.State state;
        @Relation(entity = WorkTag.class, entityColumn = "work_spec_id", parentColumn = "id", projection = { "tag" })
        public List<String> tags;
        
        @Override
        public boolean equals(final Object o) {
            boolean equals = true;
            if (this == o) {
                return true;
            }
            if (!(o instanceof WorkInfoPojo)) {
                return false;
            }
            final WorkInfoPojo workInfoPojo = (WorkInfoPojo)o;
            if (this.runAttemptCount != workInfoPojo.runAttemptCount) {
                return false;
            }
            final String id = this.id;
            Label_0068: {
                if (id != null) {
                    if (id.equals(workInfoPojo.id)) {
                        break Label_0068;
                    }
                }
                else if (workInfoPojo.id == null) {
                    break Label_0068;
                }
                return false;
            }
            if (this.state != workInfoPojo.state) {
                return false;
            }
            final Data output = this.output;
            Label_0113: {
                if (output != null) {
                    if (output.equals(workInfoPojo.output)) {
                        break Label_0113;
                    }
                }
                else if (workInfoPojo.output == null) {
                    break Label_0113;
                }
                return false;
            }
            final List<String> tags = this.tags;
            Label_0147: {
                if (tags != null) {
                    if (tags.equals(workInfoPojo.tags)) {
                        break Label_0147;
                    }
                }
                else if (workInfoPojo.tags == null) {
                    break Label_0147;
                }
                return false;
            }
            final List<Data> progress = this.progress;
            final List<Data> progress2 = workInfoPojo.progress;
            if (progress != null) {
                equals = progress.equals(progress2);
            }
            else if (progress2 != null) {
                equals = false;
            }
            return equals;
        }
        
        @Override
        public int hashCode() {
            final String id = this.id;
            int hashCode = 0;
            int hashCode2;
            if (id != null) {
                hashCode2 = id.hashCode();
            }
            else {
                hashCode2 = 0;
            }
            final WorkInfo.State state = this.state;
            int hashCode3;
            if (state != null) {
                hashCode3 = state.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            final Data output = this.output;
            int hashCode4;
            if (output != null) {
                hashCode4 = output.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            final int runAttemptCount = this.runAttemptCount;
            final List<String> tags = this.tags;
            int hashCode5;
            if (tags != null) {
                hashCode5 = tags.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            final List<Data> progress = this.progress;
            if (progress != null) {
                hashCode = progress.hashCode();
            }
            return ((((hashCode2 * 31 + hashCode3) * 31 + hashCode4) * 31 + runAttemptCount) * 31 + hashCode5) * 31 + hashCode;
        }
        
        @NonNull
        public WorkInfo toWorkInfo() {
            final List<Data> progress = this.progress;
            Data empty;
            if (progress != null && !progress.isEmpty()) {
                empty = this.progress.get(0);
            }
            else {
                empty = Data.EMPTY;
            }
            return new WorkInfo(UUID.fromString(this.id), this.state, this.output, this.tags, empty, this.runAttemptCount);
        }
    }
}

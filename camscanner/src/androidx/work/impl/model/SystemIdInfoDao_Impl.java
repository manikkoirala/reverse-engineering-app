// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import java.util.ArrayList;
import java.util.List;
import android.database.Cursor;
import androidx.room.util.CursorUtil;
import android.os.CancellationSignal;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.room.util.DBUtil;
import androidx.room.RoomSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteStatement;
import androidx.room.SharedSQLiteStatement;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;

public final class SystemIdInfoDao_Impl implements SystemIdInfoDao
{
    private final RoomDatabase __db;
    private final EntityInsertionAdapter<SystemIdInfo> __insertionAdapterOfSystemIdInfo;
    private final SharedSQLiteStatement __preparedStmtOfRemoveSystemIdInfo;
    
    public SystemIdInfoDao_Impl(final RoomDatabase _db) {
        this.__db = _db;
        this.__insertionAdapterOfSystemIdInfo = new EntityInsertionAdapter<SystemIdInfo>(this, _db) {
            final SystemIdInfoDao_Impl this$0;
            
            public void bind(final SupportSQLiteStatement supportSQLiteStatement, final SystemIdInfo systemIdInfo) {
                final String workSpecId = systemIdInfo.workSpecId;
                if (workSpecId == null) {
                    supportSQLiteStatement.bindNull(1);
                }
                else {
                    supportSQLiteStatement.bindString(1, workSpecId);
                }
                supportSQLiteStatement.bindLong(2, systemIdInfo.systemId);
            }
            
            public String createQuery() {
                return "INSERT OR REPLACE INTO `SystemIdInfo` (`work_spec_id`,`system_id`) VALUES (?,?)";
            }
        };
        this.__preparedStmtOfRemoveSystemIdInfo = new SharedSQLiteStatement(this, _db) {
            final SystemIdInfoDao_Impl this$0;
            
            public String createQuery() {
                return "DELETE FROM SystemIdInfo where work_spec_id=?";
            }
        };
    }
    
    @Override
    public SystemIdInfo getSystemIdInfo(final String s) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT `SystemIdInfo`.`work_spec_id` AS `work_spec_id`, `SystemIdInfo`.`system_id` AS `system_id` FROM SystemIdInfo WHERE work_spec_id=?", 1);
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.assertNotSuspendingTransaction();
        final RoomDatabase _db = this.__db;
        SystemIdInfo systemIdInfo = null;
        final Cursor query = DBUtil.query(_db, acquire, false, null);
        try {
            final int columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow(query, "work_spec_id");
            final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow(query, "system_id");
            if (query.moveToFirst()) {
                systemIdInfo = new SystemIdInfo(query.getString(columnIndexOrThrow), query.getInt(columnIndexOrThrow2));
            }
            return systemIdInfo;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public List<String> getWorkSpecIds() {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT DISTINCT work_spec_id FROM SystemIdInfo", 0);
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final ArrayList list = new ArrayList(query.getCount());
            while (query.moveToNext()) {
                list.add((Object)query.getString(0));
            }
            return (List<String>)list;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public void insertSystemIdInfo(final SystemIdInfo systemIdInfo) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfSystemIdInfo.insert(systemIdInfo);
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
        }
    }
    
    @Override
    public void removeSystemIdInfo(final String s) {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfRemoveSystemIdInfo.acquire();
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveSystemIdInfo.release(acquire);
        }
    }
}

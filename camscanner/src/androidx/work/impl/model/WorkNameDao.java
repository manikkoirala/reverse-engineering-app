// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import androidx.room.Insert;
import androidx.room.Query;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.room.Dao;

@Dao
public interface WorkNameDao
{
    @NonNull
    @Query("SELECT name FROM workname WHERE work_spec_id=:workSpecId")
    List<String> getNamesForWorkSpecId(@NonNull final String p0);
    
    @Query("SELECT work_spec_id FROM workname WHERE name=:name")
    List<String> getWorkSpecIdsWithName(final String p0);
    
    @Insert(onConflict = 5)
    void insert(final WorkName p0);
}

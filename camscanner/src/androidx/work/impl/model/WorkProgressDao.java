// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import androidx.room.Insert;
import java.util.List;
import androidx.annotation.Nullable;
import androidx.work.Data;
import androidx.room.Query;
import androidx.annotation.NonNull;
import androidx.room.Dao;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
@Dao
public interface WorkProgressDao
{
    @Query("DELETE from WorkProgress where work_spec_id=:workSpecId")
    void delete(@NonNull final String p0);
    
    @Query("DELETE FROM WorkProgress")
    void deleteAll();
    
    @Nullable
    @Query("SELECT progress FROM WorkProgress WHERE work_spec_id=:workSpecId")
    Data getProgressForWorkSpecId(@NonNull final String p0);
    
    @NonNull
    @Query("SELECT progress FROM WorkProgress WHERE work_spec_id IN (:workSpecIds)")
    List<Data> getProgressForWorkSpecIds(@NonNull final List<String> p0);
    
    @Insert(onConflict = 1)
    void insert(@NonNull final WorkProgress p0);
}

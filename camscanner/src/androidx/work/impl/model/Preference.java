// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import androidx.annotation.Nullable;
import androidx.room.PrimaryKey;
import androidx.room.ColumnInfo;
import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
@Entity
public class Preference
{
    @NonNull
    @ColumnInfo(name = "key")
    @PrimaryKey
    public String mKey;
    @Nullable
    @ColumnInfo(name = "long_value")
    public Long mValue;
    
    public Preference(@NonNull final String mKey, final long l) {
        this.mKey = mKey;
        this.mValue = l;
    }
    
    public Preference(@NonNull final String s, final boolean b) {
        long n;
        if (b) {
            n = 1L;
        }
        else {
            n = 0L;
        }
        this(s, n);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean equals = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof Preference)) {
            return false;
        }
        final Preference preference = (Preference)o;
        if (!this.mKey.equals(preference.mKey)) {
            return false;
        }
        final Long mValue = this.mValue;
        final Long mValue2 = preference.mValue;
        if (mValue != null) {
            equals = mValue.equals(mValue2);
        }
        else if (mValue2 != null) {
            equals = false;
        }
        return equals;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.mKey.hashCode();
        final Long mValue = this.mValue;
        int hashCode2;
        if (mValue != null) {
            hashCode2 = mValue.hashCode();
        }
        else {
            hashCode2 = 0;
        }
        return hashCode * 31 + hashCode2;
    }
}

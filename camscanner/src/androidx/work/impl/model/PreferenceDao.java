// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import androidx.room.Insert;
import androidx.lifecycle.LiveData;
import androidx.room.Query;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.room.Dao;

@Dao
public interface PreferenceDao
{
    @Nullable
    @Query("SELECT long_value FROM Preference where `key`=:key")
    Long getLongValue(@NonNull final String p0);
    
    @NonNull
    @Query("SELECT long_value FROM Preference where `key`=:key")
    LiveData<Long> getObservableLongValue(@NonNull final String p0);
    
    @Insert(onConflict = 1)
    void insertPreference(@NonNull final Preference p0);
}

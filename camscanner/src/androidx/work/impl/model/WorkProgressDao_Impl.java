// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import java.util.Iterator;
import java.util.ArrayList;
import androidx.room.util.StringUtil;
import java.util.List;
import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.room.util.DBUtil;
import androidx.room.RoomSQLiteQuery;
import androidx.work.Data;
import androidx.sqlite.db.SupportSQLiteStatement;
import androidx.room.SharedSQLiteStatement;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;

public final class WorkProgressDao_Impl implements WorkProgressDao
{
    private final RoomDatabase __db;
    private final EntityInsertionAdapter<WorkProgress> __insertionAdapterOfWorkProgress;
    private final SharedSQLiteStatement __preparedStmtOfDelete;
    private final SharedSQLiteStatement __preparedStmtOfDeleteAll;
    
    public WorkProgressDao_Impl(final RoomDatabase _db) {
        this.__db = _db;
        this.__insertionAdapterOfWorkProgress = new EntityInsertionAdapter<WorkProgress>(this, _db) {
            final WorkProgressDao_Impl this$0;
            
            public void bind(final SupportSQLiteStatement supportSQLiteStatement, final WorkProgress workProgress) {
                final String mWorkSpecId = workProgress.mWorkSpecId;
                if (mWorkSpecId == null) {
                    supportSQLiteStatement.bindNull(1);
                }
                else {
                    supportSQLiteStatement.bindString(1, mWorkSpecId);
                }
                final byte[] byteArrayInternal = Data.toByteArrayInternal(workProgress.mProgress);
                if (byteArrayInternal == null) {
                    supportSQLiteStatement.bindNull(2);
                }
                else {
                    supportSQLiteStatement.bindBlob(2, byteArrayInternal);
                }
            }
            
            public String createQuery() {
                return "INSERT OR REPLACE INTO `WorkProgress` (`work_spec_id`,`progress`) VALUES (?,?)";
            }
        };
        this.__preparedStmtOfDelete = new SharedSQLiteStatement(this, _db) {
            final WorkProgressDao_Impl this$0;
            
            public String createQuery() {
                return "DELETE from WorkProgress where work_spec_id=?";
            }
        };
        this.__preparedStmtOfDeleteAll = new SharedSQLiteStatement(this, _db) {
            final WorkProgressDao_Impl this$0;
            
            public String createQuery() {
                return "DELETE FROM WorkProgress";
            }
        };
    }
    
    @Override
    public void delete(final String s) {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfDelete.acquire();
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDelete.release(acquire);
        }
    }
    
    @Override
    public void deleteAll() {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfDeleteAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAll.release(acquire);
        }
    }
    
    @Override
    public Data getProgressForWorkSpecId(final String s) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT progress FROM WorkProgress WHERE work_spec_id=?", 1);
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.assertNotSuspendingTransaction();
        final RoomDatabase _db = this.__db;
        Data fromByteArray = null;
        final Cursor query = DBUtil.query(_db, acquire, false, null);
        try {
            if (query.moveToFirst()) {
                fromByteArray = Data.fromByteArray(query.getBlob(0));
            }
            return fromByteArray;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public List<Data> getProgressForWorkSpecIds(List<String> query) {
        final StringBuilder stringBuilder = StringUtil.newStringBuilder();
        stringBuilder.append("SELECT progress FROM WorkProgress WHERE work_spec_id IN (");
        final int size = ((List)query).size();
        StringUtil.appendPlaceholders(stringBuilder, size);
        stringBuilder.append(")");
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire(stringBuilder.toString(), size + 0);
        final Iterator iterator = ((List)query).iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final String s = (String)iterator.next();
            if (s == null) {
                acquire.bindNull(n);
            }
            else {
                acquire.bindString(n, s);
            }
            ++n;
        }
        this.__db.assertNotSuspendingTransaction();
        query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final ArrayList list = new ArrayList(query.getCount());
            while (query.moveToNext()) {
                list.add((Object)Data.fromByteArray(query.getBlob(0)));
            }
            return (List<Data>)list;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public void insert(final WorkProgress workProgress) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWorkProgress.insert(workProgress);
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
        }
    }
}

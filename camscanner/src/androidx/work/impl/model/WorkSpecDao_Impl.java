// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import androidx.work.WorkInfo;
import java.util.concurrent.Callable;
import androidx.lifecycle.LiveData;
import java.util.List;
import android.database.Cursor;
import java.util.Iterator;
import java.util.Set;
import androidx.room.util.CursorUtil;
import android.os.CancellationSignal;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.room.util.DBUtil;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.StringUtil;
import java.util.ArrayList;
import androidx.collection.ArrayMap;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.sqlite.db.SupportSQLiteStatement;
import androidx.room.SharedSQLiteStatement;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;

public final class WorkSpecDao_Impl implements WorkSpecDao
{
    private final RoomDatabase __db;
    private final EntityInsertionAdapter<WorkSpec> __insertionAdapterOfWorkSpec;
    private final SharedSQLiteStatement __preparedStmtOfDelete;
    private final SharedSQLiteStatement __preparedStmtOfIncrementWorkSpecRunAttemptCount;
    private final SharedSQLiteStatement __preparedStmtOfMarkWorkSpecScheduled;
    private final SharedSQLiteStatement __preparedStmtOfPruneFinishedWorkWithZeroDependentsIgnoringKeepForAtLeast;
    private final SharedSQLiteStatement __preparedStmtOfResetScheduledState;
    private final SharedSQLiteStatement __preparedStmtOfResetWorkSpecRunAttemptCount;
    private final SharedSQLiteStatement __preparedStmtOfSetOutput;
    private final SharedSQLiteStatement __preparedStmtOfSetPeriodStartTime;
    
    public WorkSpecDao_Impl(final RoomDatabase _db) {
        this.__db = _db;
        this.__insertionAdapterOfWorkSpec = new EntityInsertionAdapter<WorkSpec>(this, _db) {
            final WorkSpecDao_Impl this$0;
            
            public void bind(final SupportSQLiteStatement supportSQLiteStatement, final WorkSpec workSpec) {
                final String id = workSpec.id;
                if (id == null) {
                    supportSQLiteStatement.bindNull(1);
                }
                else {
                    supportSQLiteStatement.bindString(1, id);
                }
                supportSQLiteStatement.bindLong(2, WorkTypeConverters.stateToInt(workSpec.state));
                final String workerClassName = workSpec.workerClassName;
                if (workerClassName == null) {
                    supportSQLiteStatement.bindNull(3);
                }
                else {
                    supportSQLiteStatement.bindString(3, workerClassName);
                }
                final String inputMergerClassName = workSpec.inputMergerClassName;
                if (inputMergerClassName == null) {
                    supportSQLiteStatement.bindNull(4);
                }
                else {
                    supportSQLiteStatement.bindString(4, inputMergerClassName);
                }
                final byte[] byteArrayInternal = Data.toByteArrayInternal(workSpec.input);
                if (byteArrayInternal == null) {
                    supportSQLiteStatement.bindNull(5);
                }
                else {
                    supportSQLiteStatement.bindBlob(5, byteArrayInternal);
                }
                final byte[] byteArrayInternal2 = Data.toByteArrayInternal(workSpec.output);
                if (byteArrayInternal2 == null) {
                    supportSQLiteStatement.bindNull(6);
                }
                else {
                    supportSQLiteStatement.bindBlob(6, byteArrayInternal2);
                }
                supportSQLiteStatement.bindLong(7, workSpec.initialDelay);
                supportSQLiteStatement.bindLong(8, workSpec.intervalDuration);
                supportSQLiteStatement.bindLong(9, workSpec.flexDuration);
                supportSQLiteStatement.bindLong(10, workSpec.runAttemptCount);
                supportSQLiteStatement.bindLong(11, WorkTypeConverters.backoffPolicyToInt(workSpec.backoffPolicy));
                supportSQLiteStatement.bindLong(12, workSpec.backoffDelayDuration);
                supportSQLiteStatement.bindLong(13, workSpec.periodStartTime);
                supportSQLiteStatement.bindLong(14, workSpec.minimumRetentionDuration);
                supportSQLiteStatement.bindLong(15, workSpec.scheduleRequestedAt);
                supportSQLiteStatement.bindLong(16, workSpec.expedited ? 1 : 0);
                supportSQLiteStatement.bindLong(17, WorkTypeConverters.outOfQuotaPolicyToInt(workSpec.outOfQuotaPolicy));
                final Constraints constraints = workSpec.constraints;
                if (constraints != null) {
                    supportSQLiteStatement.bindLong(18, WorkTypeConverters.networkTypeToInt(constraints.getRequiredNetworkType()));
                    supportSQLiteStatement.bindLong(19, constraints.requiresCharging() ? 1 : 0);
                    supportSQLiteStatement.bindLong(20, constraints.requiresDeviceIdle() ? 1 : 0);
                    supportSQLiteStatement.bindLong(21, constraints.requiresBatteryNotLow() ? 1 : 0);
                    supportSQLiteStatement.bindLong(22, constraints.requiresStorageNotLow() ? 1 : 0);
                    supportSQLiteStatement.bindLong(23, constraints.getTriggerContentUpdateDelay());
                    supportSQLiteStatement.bindLong(24, constraints.getTriggerMaxContentDelay());
                    final byte[] contentUriTriggersToByteArray = WorkTypeConverters.contentUriTriggersToByteArray(constraints.getContentUriTriggers());
                    if (contentUriTriggersToByteArray == null) {
                        supportSQLiteStatement.bindNull(25);
                    }
                    else {
                        supportSQLiteStatement.bindBlob(25, contentUriTriggersToByteArray);
                    }
                }
                else {
                    supportSQLiteStatement.bindNull(18);
                    supportSQLiteStatement.bindNull(19);
                    supportSQLiteStatement.bindNull(20);
                    supportSQLiteStatement.bindNull(21);
                    supportSQLiteStatement.bindNull(22);
                    supportSQLiteStatement.bindNull(23);
                    supportSQLiteStatement.bindNull(24);
                    supportSQLiteStatement.bindNull(25);
                }
            }
            
            public String createQuery() {
                return "INSERT OR IGNORE INTO `WorkSpec` (`id`,`state`,`worker_class_name`,`input_merger_class_name`,`input`,`output`,`initial_delay`,`interval_duration`,`flex_duration`,`run_attempt_count`,`backoff_policy`,`backoff_delay_duration`,`period_start_time`,`minimum_retention_duration`,`schedule_requested_at`,`run_in_foreground`,`out_of_quota_policy`,`required_network_type`,`requires_charging`,`requires_device_idle`,`requires_battery_not_low`,`requires_storage_not_low`,`trigger_content_update_delay`,`trigger_max_content_delay`,`content_uri_triggers`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            }
        };
        this.__preparedStmtOfDelete = new SharedSQLiteStatement(this, _db) {
            final WorkSpecDao_Impl this$0;
            
            public String createQuery() {
                return "DELETE FROM workspec WHERE id=?";
            }
        };
        this.__preparedStmtOfSetOutput = new SharedSQLiteStatement(this, _db) {
            final WorkSpecDao_Impl this$0;
            
            public String createQuery() {
                return "UPDATE workspec SET output=? WHERE id=?";
            }
        };
        this.__preparedStmtOfSetPeriodStartTime = new SharedSQLiteStatement(this, _db) {
            final WorkSpecDao_Impl this$0;
            
            public String createQuery() {
                return "UPDATE workspec SET period_start_time=? WHERE id=?";
            }
        };
        this.__preparedStmtOfIncrementWorkSpecRunAttemptCount = new SharedSQLiteStatement(this, _db) {
            final WorkSpecDao_Impl this$0;
            
            public String createQuery() {
                return "UPDATE workspec SET run_attempt_count=run_attempt_count+1 WHERE id=?";
            }
        };
        this.__preparedStmtOfResetWorkSpecRunAttemptCount = new SharedSQLiteStatement(this, _db) {
            final WorkSpecDao_Impl this$0;
            
            public String createQuery() {
                return "UPDATE workspec SET run_attempt_count=0 WHERE id=?";
            }
        };
        this.__preparedStmtOfMarkWorkSpecScheduled = new SharedSQLiteStatement(this, _db) {
            final WorkSpecDao_Impl this$0;
            
            public String createQuery() {
                return "UPDATE workspec SET schedule_requested_at=? WHERE id=?";
            }
        };
        this.__preparedStmtOfResetScheduledState = new SharedSQLiteStatement(this, _db) {
            final WorkSpecDao_Impl this$0;
            
            public String createQuery() {
                return "UPDATE workspec SET schedule_requested_at=-1 WHERE state NOT IN (2, 3, 5)";
            }
        };
        this.__preparedStmtOfPruneFinishedWorkWithZeroDependentsIgnoringKeepForAtLeast = new SharedSQLiteStatement(this, _db) {
            final WorkSpecDao_Impl this$0;
            
            public String createQuery() {
                return "DELETE FROM workspec WHERE state IN (2, 3, 5) AND (SELECT COUNT(*)=0 FROM dependency WHERE     prerequisite_id=id AND     work_spec_id NOT IN         (SELECT id FROM workspec WHERE state IN (2, 3, 5)))";
            }
        };
    }
    
    private void __fetchRelationshipWorkProgressAsandroidxWorkData(final ArrayMap<String, ArrayList<Data>> arrayMap) {
        final Set<Object> keySet = arrayMap.keySet();
        if (keySet.isEmpty()) {
            return;
        }
        if (arrayMap.size() > 999) {
            ArrayMap arrayMap2 = new ArrayMap(999);
            final int size = arrayMap.size();
            int n = 0;
            int n2 = 0;
        Label_0047:
            while (true) {
                n2 = 0;
                int i = n;
                while (i < size) {
                    arrayMap2.put(arrayMap.keyAt(i), arrayMap.valueAt(i));
                    n = i + 1;
                    final int n3 = n2 + 1;
                    i = n;
                    if ((n2 = n3) == 999) {
                        this.__fetchRelationshipWorkProgressAsandroidxWorkData(arrayMap2);
                        arrayMap2 = new ArrayMap(999);
                        continue Label_0047;
                    }
                }
                break;
            }
            if (n2 > 0) {
                this.__fetchRelationshipWorkProgressAsandroidxWorkData(arrayMap2);
            }
            return;
        }
        final StringBuilder stringBuilder = StringUtil.newStringBuilder();
        stringBuilder.append("SELECT `progress`,`work_spec_id` FROM `WorkProgress` WHERE `work_spec_id` IN (");
        final int size2 = keySet.size();
        StringUtil.appendPlaceholders(stringBuilder, size2);
        stringBuilder.append(")");
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire(stringBuilder.toString(), size2 + 0);
        final Iterator iterator = keySet.iterator();
        int n4 = 1;
        while (iterator.hasNext()) {
            final String s = (String)iterator.next();
            if (s == null) {
                acquire.bindNull(n4);
            }
            else {
                acquire.bindString(n4, s);
            }
            ++n4;
        }
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final int columnIndex = CursorUtil.getColumnIndex(query, "work_spec_id");
            if (columnIndex == -1) {
                return;
            }
            while (query.moveToNext()) {
                if (!query.isNull(columnIndex)) {
                    final ArrayList list = arrayMap.get(query.getString(columnIndex));
                    if (list == null) {
                        continue;
                    }
                    list.add(Data.fromByteArray(query.getBlob(0)));
                }
            }
        }
        finally {
            query.close();
        }
    }
    
    private void __fetchRelationshipWorkTagAsjavaLangString(final ArrayMap<String, ArrayList<String>> arrayMap) {
        final Set<Object> keySet = arrayMap.keySet();
        if (keySet.isEmpty()) {
            return;
        }
        if (arrayMap.size() > 999) {
            ArrayMap arrayMap2 = new ArrayMap(999);
            final int size = arrayMap.size();
            int i = 0;
            int n = 0;
        Label_0047:
            while (true) {
                n = 0;
                while (i < size) {
                    arrayMap2.put(arrayMap.keyAt(i), arrayMap.valueAt(i));
                    final int n2 = i + 1;
                    final int n3 = n + 1;
                    i = n2;
                    if ((n = n3) == 999) {
                        this.__fetchRelationshipWorkTagAsjavaLangString(arrayMap2);
                        arrayMap2 = new ArrayMap(999);
                        i = n2;
                        continue Label_0047;
                    }
                }
                break;
            }
            if (n > 0) {
                this.__fetchRelationshipWorkTagAsjavaLangString(arrayMap2);
            }
            return;
        }
        final StringBuilder stringBuilder = StringUtil.newStringBuilder();
        stringBuilder.append("SELECT `tag`,`work_spec_id` FROM `WorkTag` WHERE `work_spec_id` IN (");
        final int size2 = keySet.size();
        StringUtil.appendPlaceholders(stringBuilder, size2);
        stringBuilder.append(")");
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire(stringBuilder.toString(), size2 + 0);
        final Iterator iterator = keySet.iterator();
        int n4 = 1;
        while (iterator.hasNext()) {
            final String s = (String)iterator.next();
            if (s == null) {
                acquire.bindNull(n4);
            }
            else {
                acquire.bindString(n4, s);
            }
            ++n4;
        }
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final int columnIndex = CursorUtil.getColumnIndex(query, "work_spec_id");
            if (columnIndex == -1) {
                return;
            }
            while (query.moveToNext()) {
                if (!query.isNull(columnIndex)) {
                    final ArrayList list = arrayMap.get(query.getString(columnIndex));
                    if (list == null) {
                        continue;
                    }
                    list.add(query.getString(0));
                }
            }
        }
        finally {
            query.close();
        }
    }
    
    @Override
    public void delete(final String s) {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfDelete.acquire();
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDelete.release(acquire);
        }
    }
    
    @Override
    public List<WorkSpec> getAllEligibleWorkSpecsForScheduling(int columnIndexOrThrow) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground`, `WorkSpec`.`out_of_quota_policy` AS `out_of_quota_policy` FROM workspec WHERE state=0 ORDER BY period_start_time LIMIT ?", 1);
        acquire.bindLong(1, columnIndexOrThrow);
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow(query, "required_network_type");
            final int columnIndexOrThrow3 = CursorUtil.getColumnIndexOrThrow(query, "requires_charging");
            final int columnIndexOrThrow4 = CursorUtil.getColumnIndexOrThrow(query, "requires_device_idle");
            final int columnIndexOrThrow5 = CursorUtil.getColumnIndexOrThrow(query, "requires_battery_not_low");
            final int columnIndexOrThrow6 = CursorUtil.getColumnIndexOrThrow(query, "requires_storage_not_low");
            final int columnIndexOrThrow7 = CursorUtil.getColumnIndexOrThrow(query, "trigger_content_update_delay");
            final int columnIndexOrThrow8 = CursorUtil.getColumnIndexOrThrow(query, "trigger_max_content_delay");
            final int columnIndexOrThrow9 = CursorUtil.getColumnIndexOrThrow(query, "content_uri_triggers");
            final int columnIndexOrThrow10 = CursorUtil.getColumnIndexOrThrow(query, "id");
            final int columnIndexOrThrow11 = CursorUtil.getColumnIndexOrThrow(query, "state");
            final int columnIndexOrThrow12 = CursorUtil.getColumnIndexOrThrow(query, "worker_class_name");
            final int columnIndexOrThrow13 = CursorUtil.getColumnIndexOrThrow(query, "input_merger_class_name");
            final int columnIndexOrThrow14 = CursorUtil.getColumnIndexOrThrow(query, "input");
            final int columnIndexOrThrow15 = CursorUtil.getColumnIndexOrThrow(query, "output");
            try {
                final int columnIndexOrThrow16 = CursorUtil.getColumnIndexOrThrow(query, "initial_delay");
                final int columnIndexOrThrow17 = CursorUtil.getColumnIndexOrThrow(query, "interval_duration");
                final int columnIndexOrThrow18 = CursorUtil.getColumnIndexOrThrow(query, "flex_duration");
                final int columnIndexOrThrow19 = CursorUtil.getColumnIndexOrThrow(query, "run_attempt_count");
                columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow(query, "backoff_policy");
                final int columnIndexOrThrow20 = CursorUtil.getColumnIndexOrThrow(query, "backoff_delay_duration");
                final int columnIndexOrThrow21 = CursorUtil.getColumnIndexOrThrow(query, "period_start_time");
                final int columnIndexOrThrow22 = CursorUtil.getColumnIndexOrThrow(query, "minimum_retention_duration");
                final int columnIndexOrThrow23 = CursorUtil.getColumnIndexOrThrow(query, "schedule_requested_at");
                final int columnIndexOrThrow24 = CursorUtil.getColumnIndexOrThrow(query, "run_in_foreground");
                final int columnIndexOrThrow25 = CursorUtil.getColumnIndexOrThrow(query, "out_of_quota_policy");
                final ArrayList list = new ArrayList(query.getCount());
                while (query.moveToNext()) {
                    final String string = query.getString(columnIndexOrThrow10);
                    final String string2 = query.getString(columnIndexOrThrow12);
                    final Constraints constraints = new Constraints();
                    constraints.setRequiredNetworkType(WorkTypeConverters.intToNetworkType(query.getInt(columnIndexOrThrow2)));
                    constraints.setRequiresCharging(query.getInt(columnIndexOrThrow3) != 0);
                    constraints.setRequiresDeviceIdle(query.getInt(columnIndexOrThrow4) != 0);
                    constraints.setRequiresBatteryNotLow(query.getInt(columnIndexOrThrow5) != 0);
                    constraints.setRequiresStorageNotLow(query.getInt(columnIndexOrThrow6) != 0);
                    constraints.setTriggerContentUpdateDelay(query.getLong(columnIndexOrThrow7));
                    constraints.setTriggerMaxContentDelay(query.getLong(columnIndexOrThrow8));
                    constraints.setContentUriTriggers(WorkTypeConverters.byteArrayToContentUriTriggers(query.getBlob(columnIndexOrThrow9)));
                    final WorkSpec workSpec = new WorkSpec(string, string2);
                    workSpec.state = WorkTypeConverters.intToState(query.getInt(columnIndexOrThrow11));
                    workSpec.inputMergerClassName = query.getString(columnIndexOrThrow13);
                    workSpec.input = Data.fromByteArray(query.getBlob(columnIndexOrThrow14));
                    workSpec.output = Data.fromByteArray(query.getBlob(columnIndexOrThrow15));
                    workSpec.initialDelay = query.getLong(columnIndexOrThrow16);
                    workSpec.intervalDuration = query.getLong(columnIndexOrThrow17);
                    workSpec.flexDuration = query.getLong(columnIndexOrThrow18);
                    workSpec.runAttemptCount = query.getInt(columnIndexOrThrow19);
                    workSpec.backoffPolicy = WorkTypeConverters.intToBackoffPolicy(query.getInt(columnIndexOrThrow));
                    workSpec.backoffDelayDuration = query.getLong(columnIndexOrThrow20);
                    workSpec.periodStartTime = query.getLong(columnIndexOrThrow21);
                    workSpec.minimumRetentionDuration = query.getLong(columnIndexOrThrow22);
                    workSpec.scheduleRequestedAt = query.getLong(columnIndexOrThrow23);
                    workSpec.expedited = (query.getInt(columnIndexOrThrow24) != 0);
                    workSpec.outOfQuotaPolicy = WorkTypeConverters.intToOutOfQuotaPolicy(query.getInt(columnIndexOrThrow25));
                    workSpec.constraints = constraints;
                    list.add((Object)workSpec);
                }
                query.close();
                acquire.release();
                return (List<WorkSpec>)list;
            }
            finally {}
        }
        finally {}
        query.close();
        acquire.release();
    }
    
    @Override
    public List<String> getAllUnfinishedWork() {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5)", 0);
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final ArrayList list = new ArrayList(query.getCount());
            while (query.moveToNext()) {
                list.add((Object)query.getString(0));
            }
            return (List<String>)list;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public List<String> getAllWorkSpecIds() {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT id FROM workspec", 0);
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final ArrayList list = new ArrayList(query.getCount());
            while (query.moveToNext()) {
                list.add((Object)query.getString(0));
            }
            return (List<String>)list;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public LiveData<List<String>> getAllWorkSpecIdsLiveData() {
        return this.__db.getInvalidationTracker().createLiveData(new String[] { "workspec" }, true, (Callable<List<String>>)new Callable<List<String>>(this, RoomSQLiteQuery.acquire("SELECT id FROM workspec", 0)) {
            final WorkSpecDao_Impl this$0;
            final RoomSQLiteQuery val$_statement;
            
            @Override
            public List<String> call() throws Exception {
                this.this$0.__db.beginTransaction();
                try {
                    final Cursor query = DBUtil.query(this.this$0.__db, this.val$_statement, false, null);
                    try {
                        final ArrayList list = new ArrayList(query.getCount());
                        while (query.moveToNext()) {
                            list.add((Object)query.getString(0));
                        }
                        this.this$0.__db.setTransactionSuccessful();
                        return (List<String>)list;
                    }
                    finally {
                        query.close();
                    }
                }
                finally {
                    this.this$0.__db.endTransaction();
                }
            }
            
            @Override
            protected void finalize() {
                this.val$_statement.release();
            }
        });
    }
    
    @Override
    public List<WorkSpec> getEligibleWorkForScheduling(int columnIndexOrThrow) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground`, `WorkSpec`.`out_of_quota_policy` AS `out_of_quota_policy` FROM workspec WHERE state=0 AND schedule_requested_at=-1 ORDER BY period_start_time LIMIT (SELECT MAX(?-COUNT(*), 0) FROM workspec WHERE schedule_requested_at<>-1 AND state NOT IN (2, 3, 5))", 1);
        acquire.bindLong(1, columnIndexOrThrow);
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow(query, "required_network_type");
            final int columnIndexOrThrow3 = CursorUtil.getColumnIndexOrThrow(query, "requires_charging");
            final int columnIndexOrThrow4 = CursorUtil.getColumnIndexOrThrow(query, "requires_device_idle");
            final int columnIndexOrThrow5 = CursorUtil.getColumnIndexOrThrow(query, "requires_battery_not_low");
            final int columnIndexOrThrow6 = CursorUtil.getColumnIndexOrThrow(query, "requires_storage_not_low");
            final int columnIndexOrThrow7 = CursorUtil.getColumnIndexOrThrow(query, "trigger_content_update_delay");
            final int columnIndexOrThrow8 = CursorUtil.getColumnIndexOrThrow(query, "trigger_max_content_delay");
            final int columnIndexOrThrow9 = CursorUtil.getColumnIndexOrThrow(query, "content_uri_triggers");
            final int columnIndexOrThrow10 = CursorUtil.getColumnIndexOrThrow(query, "id");
            final int columnIndexOrThrow11 = CursorUtil.getColumnIndexOrThrow(query, "state");
            final int columnIndexOrThrow12 = CursorUtil.getColumnIndexOrThrow(query, "worker_class_name");
            final int columnIndexOrThrow13 = CursorUtil.getColumnIndexOrThrow(query, "input_merger_class_name");
            final int columnIndexOrThrow14 = CursorUtil.getColumnIndexOrThrow(query, "input");
            final int columnIndexOrThrow15 = CursorUtil.getColumnIndexOrThrow(query, "output");
            try {
                final int columnIndexOrThrow16 = CursorUtil.getColumnIndexOrThrow(query, "initial_delay");
                final int columnIndexOrThrow17 = CursorUtil.getColumnIndexOrThrow(query, "interval_duration");
                final int columnIndexOrThrow18 = CursorUtil.getColumnIndexOrThrow(query, "flex_duration");
                final int columnIndexOrThrow19 = CursorUtil.getColumnIndexOrThrow(query, "run_attempt_count");
                columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow(query, "backoff_policy");
                final int columnIndexOrThrow20 = CursorUtil.getColumnIndexOrThrow(query, "backoff_delay_duration");
                final int columnIndexOrThrow21 = CursorUtil.getColumnIndexOrThrow(query, "period_start_time");
                final int columnIndexOrThrow22 = CursorUtil.getColumnIndexOrThrow(query, "minimum_retention_duration");
                final int columnIndexOrThrow23 = CursorUtil.getColumnIndexOrThrow(query, "schedule_requested_at");
                final int columnIndexOrThrow24 = CursorUtil.getColumnIndexOrThrow(query, "run_in_foreground");
                final int columnIndexOrThrow25 = CursorUtil.getColumnIndexOrThrow(query, "out_of_quota_policy");
                final ArrayList list = new ArrayList(query.getCount());
                while (query.moveToNext()) {
                    final String string = query.getString(columnIndexOrThrow10);
                    final String string2 = query.getString(columnIndexOrThrow12);
                    final Constraints constraints = new Constraints();
                    constraints.setRequiredNetworkType(WorkTypeConverters.intToNetworkType(query.getInt(columnIndexOrThrow2)));
                    constraints.setRequiresCharging(query.getInt(columnIndexOrThrow3) != 0);
                    constraints.setRequiresDeviceIdle(query.getInt(columnIndexOrThrow4) != 0);
                    constraints.setRequiresBatteryNotLow(query.getInt(columnIndexOrThrow5) != 0);
                    constraints.setRequiresStorageNotLow(query.getInt(columnIndexOrThrow6) != 0);
                    constraints.setTriggerContentUpdateDelay(query.getLong(columnIndexOrThrow7));
                    constraints.setTriggerMaxContentDelay(query.getLong(columnIndexOrThrow8));
                    constraints.setContentUriTriggers(WorkTypeConverters.byteArrayToContentUriTriggers(query.getBlob(columnIndexOrThrow9)));
                    final WorkSpec workSpec = new WorkSpec(string, string2);
                    workSpec.state = WorkTypeConverters.intToState(query.getInt(columnIndexOrThrow11));
                    workSpec.inputMergerClassName = query.getString(columnIndexOrThrow13);
                    workSpec.input = Data.fromByteArray(query.getBlob(columnIndexOrThrow14));
                    workSpec.output = Data.fromByteArray(query.getBlob(columnIndexOrThrow15));
                    workSpec.initialDelay = query.getLong(columnIndexOrThrow16);
                    workSpec.intervalDuration = query.getLong(columnIndexOrThrow17);
                    workSpec.flexDuration = query.getLong(columnIndexOrThrow18);
                    workSpec.runAttemptCount = query.getInt(columnIndexOrThrow19);
                    workSpec.backoffPolicy = WorkTypeConverters.intToBackoffPolicy(query.getInt(columnIndexOrThrow));
                    workSpec.backoffDelayDuration = query.getLong(columnIndexOrThrow20);
                    workSpec.periodStartTime = query.getLong(columnIndexOrThrow21);
                    workSpec.minimumRetentionDuration = query.getLong(columnIndexOrThrow22);
                    workSpec.scheduleRequestedAt = query.getLong(columnIndexOrThrow23);
                    workSpec.expedited = (query.getInt(columnIndexOrThrow24) != 0);
                    workSpec.outOfQuotaPolicy = WorkTypeConverters.intToOutOfQuotaPolicy(query.getInt(columnIndexOrThrow25));
                    workSpec.constraints = constraints;
                    list.add((Object)workSpec);
                }
                query.close();
                acquire.release();
                return (List<WorkSpec>)list;
            }
            finally {}
        }
        finally {}
        query.close();
        acquire.release();
    }
    
    @Override
    public List<Data> getInputsFromPrerequisites(String query) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT output FROM workspec WHERE id IN (SELECT prerequisite_id FROM dependency WHERE work_spec_id=?)", 1);
        if (query == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, query);
        }
        this.__db.assertNotSuspendingTransaction();
        query = (String)DBUtil.query(this.__db, acquire, false, null);
        try {
            final ArrayList list = new ArrayList(((Cursor)query).getCount());
            while (((Cursor)query).moveToNext()) {
                list.add(Data.fromByteArray(((Cursor)query).getBlob(0)));
            }
            return list;
        }
        finally {
            ((Cursor)query).close();
            acquire.release();
        }
    }
    
    @Override
    public List<WorkSpec> getRecentlyCompletedWork(final long n) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground`, `WorkSpec`.`out_of_quota_policy` AS `out_of_quota_policy` FROM workspec WHERE period_start_time >= ? AND state IN (2, 3, 5) ORDER BY period_start_time DESC", 1);
        acquire.bindLong(1, n);
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final int columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow(query, "required_network_type");
            final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow(query, "requires_charging");
            final int columnIndexOrThrow3 = CursorUtil.getColumnIndexOrThrow(query, "requires_device_idle");
            final int columnIndexOrThrow4 = CursorUtil.getColumnIndexOrThrow(query, "requires_battery_not_low");
            final int columnIndexOrThrow5 = CursorUtil.getColumnIndexOrThrow(query, "requires_storage_not_low");
            final int columnIndexOrThrow6 = CursorUtil.getColumnIndexOrThrow(query, "trigger_content_update_delay");
            final int columnIndexOrThrow7 = CursorUtil.getColumnIndexOrThrow(query, "trigger_max_content_delay");
            final int columnIndexOrThrow8 = CursorUtil.getColumnIndexOrThrow(query, "content_uri_triggers");
            final int columnIndexOrThrow9 = CursorUtil.getColumnIndexOrThrow(query, "id");
            final int columnIndexOrThrow10 = CursorUtil.getColumnIndexOrThrow(query, "state");
            final int columnIndexOrThrow11 = CursorUtil.getColumnIndexOrThrow(query, "worker_class_name");
            final int columnIndexOrThrow12 = CursorUtil.getColumnIndexOrThrow(query, "input_merger_class_name");
            final int columnIndexOrThrow13 = CursorUtil.getColumnIndexOrThrow(query, "input");
            final int columnIndexOrThrow14 = CursorUtil.getColumnIndexOrThrow(query, "output");
            try {
                final int columnIndexOrThrow15 = CursorUtil.getColumnIndexOrThrow(query, "initial_delay");
                final int columnIndexOrThrow16 = CursorUtil.getColumnIndexOrThrow(query, "interval_duration");
                final int columnIndexOrThrow17 = CursorUtil.getColumnIndexOrThrow(query, "flex_duration");
                final int columnIndexOrThrow18 = CursorUtil.getColumnIndexOrThrow(query, "run_attempt_count");
                final int columnIndexOrThrow19 = CursorUtil.getColumnIndexOrThrow(query, "backoff_policy");
                final int columnIndexOrThrow20 = CursorUtil.getColumnIndexOrThrow(query, "backoff_delay_duration");
                final int columnIndexOrThrow21 = CursorUtil.getColumnIndexOrThrow(query, "period_start_time");
                final int columnIndexOrThrow22 = CursorUtil.getColumnIndexOrThrow(query, "minimum_retention_duration");
                final int columnIndexOrThrow23 = CursorUtil.getColumnIndexOrThrow(query, "schedule_requested_at");
                final int columnIndexOrThrow24 = CursorUtil.getColumnIndexOrThrow(query, "run_in_foreground");
                final int columnIndexOrThrow25 = CursorUtil.getColumnIndexOrThrow(query, "out_of_quota_policy");
                final ArrayList list = new ArrayList(query.getCount());
                while (query.moveToNext()) {
                    final String string = query.getString(columnIndexOrThrow9);
                    final String string2 = query.getString(columnIndexOrThrow11);
                    final Constraints constraints = new Constraints();
                    constraints.setRequiredNetworkType(WorkTypeConverters.intToNetworkType(query.getInt(columnIndexOrThrow)));
                    constraints.setRequiresCharging(query.getInt(columnIndexOrThrow2) != 0);
                    constraints.setRequiresDeviceIdle(query.getInt(columnIndexOrThrow3) != 0);
                    constraints.setRequiresBatteryNotLow(query.getInt(columnIndexOrThrow4) != 0);
                    constraints.setRequiresStorageNotLow(query.getInt(columnIndexOrThrow5) != 0);
                    constraints.setTriggerContentUpdateDelay(query.getLong(columnIndexOrThrow6));
                    constraints.setTriggerMaxContentDelay(query.getLong(columnIndexOrThrow7));
                    constraints.setContentUriTriggers(WorkTypeConverters.byteArrayToContentUriTriggers(query.getBlob(columnIndexOrThrow8)));
                    final WorkSpec workSpec = new WorkSpec(string, string2);
                    workSpec.state = WorkTypeConverters.intToState(query.getInt(columnIndexOrThrow10));
                    workSpec.inputMergerClassName = query.getString(columnIndexOrThrow12);
                    workSpec.input = Data.fromByteArray(query.getBlob(columnIndexOrThrow13));
                    workSpec.output = Data.fromByteArray(query.getBlob(columnIndexOrThrow14));
                    workSpec.initialDelay = query.getLong(columnIndexOrThrow15);
                    workSpec.intervalDuration = query.getLong(columnIndexOrThrow16);
                    workSpec.flexDuration = query.getLong(columnIndexOrThrow17);
                    workSpec.runAttemptCount = query.getInt(columnIndexOrThrow18);
                    workSpec.backoffPolicy = WorkTypeConverters.intToBackoffPolicy(query.getInt(columnIndexOrThrow19));
                    workSpec.backoffDelayDuration = query.getLong(columnIndexOrThrow20);
                    workSpec.periodStartTime = query.getLong(columnIndexOrThrow21);
                    workSpec.minimumRetentionDuration = query.getLong(columnIndexOrThrow22);
                    workSpec.scheduleRequestedAt = query.getLong(columnIndexOrThrow23);
                    workSpec.expedited = (query.getInt(columnIndexOrThrow24) != 0);
                    workSpec.outOfQuotaPolicy = WorkTypeConverters.intToOutOfQuotaPolicy(query.getInt(columnIndexOrThrow25));
                    workSpec.constraints = constraints;
                    list.add((Object)workSpec);
                }
                query.close();
                acquire.release();
                return (List<WorkSpec>)list;
            }
            finally {}
        }
        finally {}
        query.close();
        acquire.release();
    }
    
    @Override
    public List<WorkSpec> getRunningWork() {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground`, `WorkSpec`.`out_of_quota_policy` AS `out_of_quota_policy` FROM workspec WHERE state=1", 0);
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final int columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow(query, "required_network_type");
            final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow(query, "requires_charging");
            final int columnIndexOrThrow3 = CursorUtil.getColumnIndexOrThrow(query, "requires_device_idle");
            final int columnIndexOrThrow4 = CursorUtil.getColumnIndexOrThrow(query, "requires_battery_not_low");
            final int columnIndexOrThrow5 = CursorUtil.getColumnIndexOrThrow(query, "requires_storage_not_low");
            final int columnIndexOrThrow6 = CursorUtil.getColumnIndexOrThrow(query, "trigger_content_update_delay");
            final int columnIndexOrThrow7 = CursorUtil.getColumnIndexOrThrow(query, "trigger_max_content_delay");
            final int columnIndexOrThrow8 = CursorUtil.getColumnIndexOrThrow(query, "content_uri_triggers");
            final int columnIndexOrThrow9 = CursorUtil.getColumnIndexOrThrow(query, "id");
            final int columnIndexOrThrow10 = CursorUtil.getColumnIndexOrThrow(query, "state");
            final int columnIndexOrThrow11 = CursorUtil.getColumnIndexOrThrow(query, "worker_class_name");
            final int columnIndexOrThrow12 = CursorUtil.getColumnIndexOrThrow(query, "input_merger_class_name");
            final int columnIndexOrThrow13 = CursorUtil.getColumnIndexOrThrow(query, "input");
            final int columnIndexOrThrow14 = CursorUtil.getColumnIndexOrThrow(query, "output");
            try {
                final int columnIndexOrThrow15 = CursorUtil.getColumnIndexOrThrow(query, "initial_delay");
                final int columnIndexOrThrow16 = CursorUtil.getColumnIndexOrThrow(query, "interval_duration");
                final int columnIndexOrThrow17 = CursorUtil.getColumnIndexOrThrow(query, "flex_duration");
                final int columnIndexOrThrow18 = CursorUtil.getColumnIndexOrThrow(query, "run_attempt_count");
                final int columnIndexOrThrow19 = CursorUtil.getColumnIndexOrThrow(query, "backoff_policy");
                final int columnIndexOrThrow20 = CursorUtil.getColumnIndexOrThrow(query, "backoff_delay_duration");
                final int columnIndexOrThrow21 = CursorUtil.getColumnIndexOrThrow(query, "period_start_time");
                final int columnIndexOrThrow22 = CursorUtil.getColumnIndexOrThrow(query, "minimum_retention_duration");
                final int columnIndexOrThrow23 = CursorUtil.getColumnIndexOrThrow(query, "schedule_requested_at");
                final int columnIndexOrThrow24 = CursorUtil.getColumnIndexOrThrow(query, "run_in_foreground");
                final int columnIndexOrThrow25 = CursorUtil.getColumnIndexOrThrow(query, "out_of_quota_policy");
                final ArrayList list = new ArrayList(query.getCount());
                while (query.moveToNext()) {
                    final String string = query.getString(columnIndexOrThrow9);
                    final String string2 = query.getString(columnIndexOrThrow11);
                    final Constraints constraints = new Constraints();
                    constraints.setRequiredNetworkType(WorkTypeConverters.intToNetworkType(query.getInt(columnIndexOrThrow)));
                    constraints.setRequiresCharging(query.getInt(columnIndexOrThrow2) != 0);
                    constraints.setRequiresDeviceIdle(query.getInt(columnIndexOrThrow3) != 0);
                    constraints.setRequiresBatteryNotLow(query.getInt(columnIndexOrThrow4) != 0);
                    constraints.setRequiresStorageNotLow(query.getInt(columnIndexOrThrow5) != 0);
                    constraints.setTriggerContentUpdateDelay(query.getLong(columnIndexOrThrow6));
                    constraints.setTriggerMaxContentDelay(query.getLong(columnIndexOrThrow7));
                    constraints.setContentUriTriggers(WorkTypeConverters.byteArrayToContentUriTriggers(query.getBlob(columnIndexOrThrow8)));
                    final WorkSpec workSpec = new WorkSpec(string, string2);
                    workSpec.state = WorkTypeConverters.intToState(query.getInt(columnIndexOrThrow10));
                    workSpec.inputMergerClassName = query.getString(columnIndexOrThrow12);
                    workSpec.input = Data.fromByteArray(query.getBlob(columnIndexOrThrow13));
                    workSpec.output = Data.fromByteArray(query.getBlob(columnIndexOrThrow14));
                    workSpec.initialDelay = query.getLong(columnIndexOrThrow15);
                    workSpec.intervalDuration = query.getLong(columnIndexOrThrow16);
                    workSpec.flexDuration = query.getLong(columnIndexOrThrow17);
                    workSpec.runAttemptCount = query.getInt(columnIndexOrThrow18);
                    workSpec.backoffPolicy = WorkTypeConverters.intToBackoffPolicy(query.getInt(columnIndexOrThrow19));
                    workSpec.backoffDelayDuration = query.getLong(columnIndexOrThrow20);
                    workSpec.periodStartTime = query.getLong(columnIndexOrThrow21);
                    workSpec.minimumRetentionDuration = query.getLong(columnIndexOrThrow22);
                    workSpec.scheduleRequestedAt = query.getLong(columnIndexOrThrow23);
                    workSpec.expedited = (query.getInt(columnIndexOrThrow24) != 0);
                    workSpec.outOfQuotaPolicy = WorkTypeConverters.intToOutOfQuotaPolicy(query.getInt(columnIndexOrThrow25));
                    workSpec.constraints = constraints;
                    list.add((Object)workSpec);
                }
                query.close();
                acquire.release();
                return (List<WorkSpec>)list;
            }
            finally {}
        }
        finally {}
        query.close();
        acquire.release();
    }
    
    @Override
    public LiveData<Long> getScheduleRequestedAtLiveData(final String s) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT schedule_requested_at FROM workspec WHERE id=?", 1);
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        return this.__db.getInvalidationTracker().createLiveData(new String[] { "workspec" }, false, (Callable<Long>)new Callable<Long>(this, acquire) {
            final WorkSpecDao_Impl this$0;
            final RoomSQLiteQuery val$_statement;
            
            @Override
            public Long call() throws Exception {
                final RoomDatabase access$000 = this.this$0.__db;
                final RoomSQLiteQuery val$_statement = this.val$_statement;
                final Long n = null;
                final Cursor query = DBUtil.query(access$000, val$_statement, false, null);
                Long value = n;
                try {
                    if (query.moveToFirst()) {
                        if (query.isNull(0)) {
                            value = n;
                        }
                        else {
                            value = query.getLong(0);
                        }
                    }
                    return value;
                }
                finally {
                    query.close();
                }
            }
            
            @Override
            protected void finalize() {
                this.val$_statement.release();
            }
        });
    }
    
    @Override
    public List<WorkSpec> getScheduledWork() {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground`, `WorkSpec`.`out_of_quota_policy` AS `out_of_quota_policy` FROM workspec WHERE state=0 AND schedule_requested_at<>-1", 0);
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final int columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow(query, "required_network_type");
            final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow(query, "requires_charging");
            final int columnIndexOrThrow3 = CursorUtil.getColumnIndexOrThrow(query, "requires_device_idle");
            final int columnIndexOrThrow4 = CursorUtil.getColumnIndexOrThrow(query, "requires_battery_not_low");
            final int columnIndexOrThrow5 = CursorUtil.getColumnIndexOrThrow(query, "requires_storage_not_low");
            final int columnIndexOrThrow6 = CursorUtil.getColumnIndexOrThrow(query, "trigger_content_update_delay");
            final int columnIndexOrThrow7 = CursorUtil.getColumnIndexOrThrow(query, "trigger_max_content_delay");
            final int columnIndexOrThrow8 = CursorUtil.getColumnIndexOrThrow(query, "content_uri_triggers");
            final int columnIndexOrThrow9 = CursorUtil.getColumnIndexOrThrow(query, "id");
            final int columnIndexOrThrow10 = CursorUtil.getColumnIndexOrThrow(query, "state");
            final int columnIndexOrThrow11 = CursorUtil.getColumnIndexOrThrow(query, "worker_class_name");
            final int columnIndexOrThrow12 = CursorUtil.getColumnIndexOrThrow(query, "input_merger_class_name");
            final int columnIndexOrThrow13 = CursorUtil.getColumnIndexOrThrow(query, "input");
            final int columnIndexOrThrow14 = CursorUtil.getColumnIndexOrThrow(query, "output");
            try {
                final int columnIndexOrThrow15 = CursorUtil.getColumnIndexOrThrow(query, "initial_delay");
                final int columnIndexOrThrow16 = CursorUtil.getColumnIndexOrThrow(query, "interval_duration");
                final int columnIndexOrThrow17 = CursorUtil.getColumnIndexOrThrow(query, "flex_duration");
                final int columnIndexOrThrow18 = CursorUtil.getColumnIndexOrThrow(query, "run_attempt_count");
                final int columnIndexOrThrow19 = CursorUtil.getColumnIndexOrThrow(query, "backoff_policy");
                final int columnIndexOrThrow20 = CursorUtil.getColumnIndexOrThrow(query, "backoff_delay_duration");
                final int columnIndexOrThrow21 = CursorUtil.getColumnIndexOrThrow(query, "period_start_time");
                final int columnIndexOrThrow22 = CursorUtil.getColumnIndexOrThrow(query, "minimum_retention_duration");
                final int columnIndexOrThrow23 = CursorUtil.getColumnIndexOrThrow(query, "schedule_requested_at");
                final int columnIndexOrThrow24 = CursorUtil.getColumnIndexOrThrow(query, "run_in_foreground");
                final int columnIndexOrThrow25 = CursorUtil.getColumnIndexOrThrow(query, "out_of_quota_policy");
                final ArrayList list = new ArrayList(query.getCount());
                while (query.moveToNext()) {
                    final String string = query.getString(columnIndexOrThrow9);
                    final String string2 = query.getString(columnIndexOrThrow11);
                    final Constraints constraints = new Constraints();
                    constraints.setRequiredNetworkType(WorkTypeConverters.intToNetworkType(query.getInt(columnIndexOrThrow)));
                    constraints.setRequiresCharging(query.getInt(columnIndexOrThrow2) != 0);
                    constraints.setRequiresDeviceIdle(query.getInt(columnIndexOrThrow3) != 0);
                    constraints.setRequiresBatteryNotLow(query.getInt(columnIndexOrThrow4) != 0);
                    constraints.setRequiresStorageNotLow(query.getInt(columnIndexOrThrow5) != 0);
                    constraints.setTriggerContentUpdateDelay(query.getLong(columnIndexOrThrow6));
                    constraints.setTriggerMaxContentDelay(query.getLong(columnIndexOrThrow7));
                    constraints.setContentUriTriggers(WorkTypeConverters.byteArrayToContentUriTriggers(query.getBlob(columnIndexOrThrow8)));
                    final WorkSpec workSpec = new WorkSpec(string, string2);
                    workSpec.state = WorkTypeConverters.intToState(query.getInt(columnIndexOrThrow10));
                    workSpec.inputMergerClassName = query.getString(columnIndexOrThrow12);
                    workSpec.input = Data.fromByteArray(query.getBlob(columnIndexOrThrow13));
                    workSpec.output = Data.fromByteArray(query.getBlob(columnIndexOrThrow14));
                    workSpec.initialDelay = query.getLong(columnIndexOrThrow15);
                    workSpec.intervalDuration = query.getLong(columnIndexOrThrow16);
                    workSpec.flexDuration = query.getLong(columnIndexOrThrow17);
                    workSpec.runAttemptCount = query.getInt(columnIndexOrThrow18);
                    workSpec.backoffPolicy = WorkTypeConverters.intToBackoffPolicy(query.getInt(columnIndexOrThrow19));
                    workSpec.backoffDelayDuration = query.getLong(columnIndexOrThrow20);
                    workSpec.periodStartTime = query.getLong(columnIndexOrThrow21);
                    workSpec.minimumRetentionDuration = query.getLong(columnIndexOrThrow22);
                    workSpec.scheduleRequestedAt = query.getLong(columnIndexOrThrow23);
                    workSpec.expedited = (query.getInt(columnIndexOrThrow24) != 0);
                    workSpec.outOfQuotaPolicy = WorkTypeConverters.intToOutOfQuotaPolicy(query.getInt(columnIndexOrThrow25));
                    workSpec.constraints = constraints;
                    list.add((Object)workSpec);
                }
                query.close();
                acquire.release();
                return (List<WorkSpec>)list;
            }
            finally {}
        }
        finally {}
        query.close();
        acquire.release();
    }
    
    @Override
    public WorkInfo.State getState(final String s) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT state FROM workspec WHERE id=?", 1);
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.assertNotSuspendingTransaction();
        final RoomDatabase _db = this.__db;
        WorkInfo.State intToState = null;
        final Cursor query = DBUtil.query(_db, acquire, false, null);
        try {
            if (query.moveToFirst()) {
                intToState = WorkTypeConverters.intToState(query.getInt(0));
            }
            return intToState;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public List<String> getUnfinishedWorkWithName(String query) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5) AND id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (query == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, query);
        }
        this.__db.assertNotSuspendingTransaction();
        query = (String)DBUtil.query(this.__db, acquire, false, null);
        try {
            final ArrayList list = new ArrayList(((Cursor)query).getCount());
            while (((Cursor)query).moveToNext()) {
                list.add(((Cursor)query).getString(0));
            }
            return list;
        }
        finally {
            ((Cursor)query).close();
            acquire.release();
        }
    }
    
    @Override
    public List<String> getUnfinishedWorkWithTag(String query) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5) AND id IN (SELECT work_spec_id FROM worktag WHERE tag=?)", 1);
        if (query == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, query);
        }
        this.__db.assertNotSuspendingTransaction();
        query = (String)DBUtil.query(this.__db, acquire, false, null);
        try {
            final ArrayList list = new ArrayList(((Cursor)query).getCount());
            while (((Cursor)query).moveToNext()) {
                list.add(((Cursor)query).getString(0));
            }
            return list;
        }
        finally {
            ((Cursor)query).close();
            acquire.release();
        }
    }
    
    @Override
    public WorkSpec getWorkSpec(final String s) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground`, `WorkSpec`.`out_of_quota_policy` AS `out_of_quota_policy` FROM workspec WHERE id=?", 1);
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final int columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow(query, "required_network_type");
            final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow(query, "requires_charging");
            final int columnIndexOrThrow3 = CursorUtil.getColumnIndexOrThrow(query, "requires_device_idle");
            final int columnIndexOrThrow4 = CursorUtil.getColumnIndexOrThrow(query, "requires_battery_not_low");
            final int columnIndexOrThrow5 = CursorUtil.getColumnIndexOrThrow(query, "requires_storage_not_low");
            final int columnIndexOrThrow6 = CursorUtil.getColumnIndexOrThrow(query, "trigger_content_update_delay");
            final int columnIndexOrThrow7 = CursorUtil.getColumnIndexOrThrow(query, "trigger_max_content_delay");
            final int columnIndexOrThrow8 = CursorUtil.getColumnIndexOrThrow(query, "content_uri_triggers");
            final int columnIndexOrThrow9 = CursorUtil.getColumnIndexOrThrow(query, "id");
            final int columnIndexOrThrow10 = CursorUtil.getColumnIndexOrThrow(query, "state");
            final int columnIndexOrThrow11 = CursorUtil.getColumnIndexOrThrow(query, "worker_class_name");
            final int columnIndexOrThrow12 = CursorUtil.getColumnIndexOrThrow(query, "input_merger_class_name");
            final int columnIndexOrThrow13 = CursorUtil.getColumnIndexOrThrow(query, "input");
            final int columnIndexOrThrow14 = CursorUtil.getColumnIndexOrThrow(query, "output");
            try {
                final int columnIndexOrThrow15 = CursorUtil.getColumnIndexOrThrow(query, "initial_delay");
                final int columnIndexOrThrow16 = CursorUtil.getColumnIndexOrThrow(query, "interval_duration");
                final int columnIndexOrThrow17 = CursorUtil.getColumnIndexOrThrow(query, "flex_duration");
                final int columnIndexOrThrow18 = CursorUtil.getColumnIndexOrThrow(query, "run_attempt_count");
                final int columnIndexOrThrow19 = CursorUtil.getColumnIndexOrThrow(query, "backoff_policy");
                final int columnIndexOrThrow20 = CursorUtil.getColumnIndexOrThrow(query, "backoff_delay_duration");
                final int columnIndexOrThrow21 = CursorUtil.getColumnIndexOrThrow(query, "period_start_time");
                final int columnIndexOrThrow22 = CursorUtil.getColumnIndexOrThrow(query, "minimum_retention_duration");
                final int columnIndexOrThrow23 = CursorUtil.getColumnIndexOrThrow(query, "schedule_requested_at");
                final int columnIndexOrThrow24 = CursorUtil.getColumnIndexOrThrow(query, "run_in_foreground");
                final int columnIndexOrThrow25 = CursorUtil.getColumnIndexOrThrow(query, "out_of_quota_policy");
                WorkSpec workSpec;
                if (query.moveToFirst()) {
                    final String string = query.getString(columnIndexOrThrow9);
                    final String string2 = query.getString(columnIndexOrThrow11);
                    final Constraints constraints = new Constraints();
                    constraints.setRequiredNetworkType(WorkTypeConverters.intToNetworkType(query.getInt(columnIndexOrThrow)));
                    constraints.setRequiresCharging(query.getInt(columnIndexOrThrow2) != 0);
                    constraints.setRequiresDeviceIdle(query.getInt(columnIndexOrThrow3) != 0);
                    constraints.setRequiresBatteryNotLow(query.getInt(columnIndexOrThrow4) != 0);
                    constraints.setRequiresStorageNotLow(query.getInt(columnIndexOrThrow5) != 0);
                    constraints.setTriggerContentUpdateDelay(query.getLong(columnIndexOrThrow6));
                    constraints.setTriggerMaxContentDelay(query.getLong(columnIndexOrThrow7));
                    constraints.setContentUriTriggers(WorkTypeConverters.byteArrayToContentUriTriggers(query.getBlob(columnIndexOrThrow8)));
                    workSpec = new WorkSpec(string, string2);
                    workSpec.state = WorkTypeConverters.intToState(query.getInt(columnIndexOrThrow10));
                    workSpec.inputMergerClassName = query.getString(columnIndexOrThrow12);
                    workSpec.input = Data.fromByteArray(query.getBlob(columnIndexOrThrow13));
                    workSpec.output = Data.fromByteArray(query.getBlob(columnIndexOrThrow14));
                    workSpec.initialDelay = query.getLong(columnIndexOrThrow15);
                    workSpec.intervalDuration = query.getLong(columnIndexOrThrow16);
                    workSpec.flexDuration = query.getLong(columnIndexOrThrow17);
                    workSpec.runAttemptCount = query.getInt(columnIndexOrThrow18);
                    workSpec.backoffPolicy = WorkTypeConverters.intToBackoffPolicy(query.getInt(columnIndexOrThrow19));
                    workSpec.backoffDelayDuration = query.getLong(columnIndexOrThrow20);
                    workSpec.periodStartTime = query.getLong(columnIndexOrThrow21);
                    workSpec.minimumRetentionDuration = query.getLong(columnIndexOrThrow22);
                    workSpec.scheduleRequestedAt = query.getLong(columnIndexOrThrow23);
                    workSpec.expedited = (query.getInt(columnIndexOrThrow24) != 0);
                    workSpec.outOfQuotaPolicy = WorkTypeConverters.intToOutOfQuotaPolicy(query.getInt(columnIndexOrThrow25));
                    workSpec.constraints = constraints;
                }
                else {
                    workSpec = null;
                }
                query.close();
                acquire.release();
                return workSpec;
            }
            finally {}
        }
        finally {}
        query.close();
        acquire.release();
    }
    
    @Override
    public List<WorkSpec.IdAndState> getWorkSpecIdAndStatesForName(String query) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT id, state FROM workspec WHERE id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (query == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, query);
        }
        this.__db.assertNotSuspendingTransaction();
        query = (String)DBUtil.query(this.__db, acquire, false, null);
        try {
            final int columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow((Cursor)query, "id");
            final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow((Cursor)query, "state");
            final ArrayList list = new ArrayList(((Cursor)query).getCount());
            while (((Cursor)query).moveToNext()) {
                final WorkSpec.IdAndState idAndState = new WorkSpec.IdAndState();
                idAndState.id = ((Cursor)query).getString(columnIndexOrThrow);
                idAndState.state = WorkTypeConverters.intToState(((Cursor)query).getInt(columnIndexOrThrow2));
                list.add(idAndState);
            }
            return list;
        }
        finally {
            ((Cursor)query).close();
            acquire.release();
        }
    }
    
    @Override
    public WorkSpec[] getWorkSpecs(final List<String> list) {
        final StringBuilder stringBuilder = StringUtil.newStringBuilder();
        stringBuilder.append("SELECT ");
        stringBuilder.append("*");
        stringBuilder.append(" FROM workspec WHERE id IN (");
        final int size = list.size();
        StringUtil.appendPlaceholders(stringBuilder, size);
        stringBuilder.append(")");
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire(stringBuilder.toString(), size + 0);
        final Iterator iterator = list.iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final String s = (String)iterator.next();
            if (s == null) {
                acquire.bindNull(n);
            }
            else {
                acquire.bindString(n, s);
            }
            ++n;
        }
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final int columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow(query, "required_network_type");
            final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow(query, "requires_charging");
            final int columnIndexOrThrow3 = CursorUtil.getColumnIndexOrThrow(query, "requires_device_idle");
            final int columnIndexOrThrow4 = CursorUtil.getColumnIndexOrThrow(query, "requires_battery_not_low");
            final int columnIndexOrThrow5 = CursorUtil.getColumnIndexOrThrow(query, "requires_storage_not_low");
            final int columnIndexOrThrow6 = CursorUtil.getColumnIndexOrThrow(query, "trigger_content_update_delay");
            final int columnIndexOrThrow7 = CursorUtil.getColumnIndexOrThrow(query, "trigger_max_content_delay");
            final int columnIndexOrThrow8 = CursorUtil.getColumnIndexOrThrow(query, "content_uri_triggers");
            final int columnIndexOrThrow9 = CursorUtil.getColumnIndexOrThrow(query, "id");
            final int columnIndexOrThrow10 = CursorUtil.getColumnIndexOrThrow(query, "state");
            final int columnIndexOrThrow11 = CursorUtil.getColumnIndexOrThrow(query, "worker_class_name");
            final int columnIndexOrThrow12 = CursorUtil.getColumnIndexOrThrow(query, "input_merger_class_name");
            final int columnIndexOrThrow13 = CursorUtil.getColumnIndexOrThrow(query, "input");
            final int columnIndexOrThrow14 = CursorUtil.getColumnIndexOrThrow(query, "output");
            try {
                final int columnIndexOrThrow15 = CursorUtil.getColumnIndexOrThrow(query, "initial_delay");
                final int columnIndexOrThrow16 = CursorUtil.getColumnIndexOrThrow(query, "interval_duration");
                final int columnIndexOrThrow17 = CursorUtil.getColumnIndexOrThrow(query, "flex_duration");
                final int columnIndexOrThrow18 = CursorUtil.getColumnIndexOrThrow(query, "run_attempt_count");
                final int columnIndexOrThrow19 = CursorUtil.getColumnIndexOrThrow(query, "backoff_policy");
                final int columnIndexOrThrow20 = CursorUtil.getColumnIndexOrThrow(query, "backoff_delay_duration");
                final int columnIndexOrThrow21 = CursorUtil.getColumnIndexOrThrow(query, "period_start_time");
                final int columnIndexOrThrow22 = CursorUtil.getColumnIndexOrThrow(query, "minimum_retention_duration");
                final int columnIndexOrThrow23 = CursorUtil.getColumnIndexOrThrow(query, "schedule_requested_at");
                final int columnIndexOrThrow24 = CursorUtil.getColumnIndexOrThrow(query, "run_in_foreground");
                final int columnIndexOrThrow25 = CursorUtil.getColumnIndexOrThrow(query, "out_of_quota_policy");
                final WorkSpec[] array = new WorkSpec[query.getCount()];
                int n2 = 0;
                while (query.moveToNext()) {
                    final String string = query.getString(columnIndexOrThrow9);
                    final String string2 = query.getString(columnIndexOrThrow11);
                    final Constraints constraints = new Constraints();
                    constraints.setRequiredNetworkType(WorkTypeConverters.intToNetworkType(query.getInt(columnIndexOrThrow)));
                    constraints.setRequiresCharging(query.getInt(columnIndexOrThrow2) != 0);
                    constraints.setRequiresDeviceIdle(query.getInt(columnIndexOrThrow3) != 0);
                    constraints.setRequiresBatteryNotLow(query.getInt(columnIndexOrThrow4) != 0);
                    constraints.setRequiresStorageNotLow(query.getInt(columnIndexOrThrow5) != 0);
                    constraints.setTriggerContentUpdateDelay(query.getLong(columnIndexOrThrow6));
                    constraints.setTriggerMaxContentDelay(query.getLong(columnIndexOrThrow7));
                    constraints.setContentUriTriggers(WorkTypeConverters.byteArrayToContentUriTriggers(query.getBlob(columnIndexOrThrow8)));
                    final WorkSpec workSpec = new WorkSpec(string, string2);
                    workSpec.state = WorkTypeConverters.intToState(query.getInt(columnIndexOrThrow10));
                    workSpec.inputMergerClassName = query.getString(columnIndexOrThrow12);
                    workSpec.input = Data.fromByteArray(query.getBlob(columnIndexOrThrow13));
                    workSpec.output = Data.fromByteArray(query.getBlob(columnIndexOrThrow14));
                    workSpec.initialDelay = query.getLong(columnIndexOrThrow15);
                    workSpec.intervalDuration = query.getLong(columnIndexOrThrow16);
                    workSpec.flexDuration = query.getLong(columnIndexOrThrow17);
                    workSpec.runAttemptCount = query.getInt(columnIndexOrThrow18);
                    workSpec.backoffPolicy = WorkTypeConverters.intToBackoffPolicy(query.getInt(columnIndexOrThrow19));
                    workSpec.backoffDelayDuration = query.getLong(columnIndexOrThrow20);
                    workSpec.periodStartTime = query.getLong(columnIndexOrThrow21);
                    workSpec.minimumRetentionDuration = query.getLong(columnIndexOrThrow22);
                    workSpec.scheduleRequestedAt = query.getLong(columnIndexOrThrow23);
                    workSpec.expedited = (query.getInt(columnIndexOrThrow24) != 0);
                    workSpec.outOfQuotaPolicy = WorkTypeConverters.intToOutOfQuotaPolicy(query.getInt(columnIndexOrThrow25));
                    workSpec.constraints = constraints;
                    array[n2] = workSpec;
                    ++n2;
                }
                query.close();
                acquire.release();
                return array;
            }
            finally {}
        }
        finally {}
        query.close();
        acquire.release();
    }
    
    @Override
    public WorkSpec.WorkInfoPojo getWorkStatusPojoForId(final String s) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT id, state, output, run_attempt_count FROM workspec WHERE id=?", 1);
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            final RoomDatabase _db = this.__db;
            WorkSpec.WorkInfoPojo workInfoPojo = null;
            final ArrayList list = null;
            final Cursor query = DBUtil.query(_db, acquire, true, null);
            try {
                final int columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow(query, "id");
                final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow(query, "state");
                final int columnIndexOrThrow3 = CursorUtil.getColumnIndexOrThrow(query, "output");
                final int columnIndexOrThrow4 = CursorUtil.getColumnIndexOrThrow(query, "run_attempt_count");
                final ArrayMap<Object, ArrayList> arrayMap = new ArrayMap<Object, ArrayList>();
                final ArrayMap<String, ArrayList> arrayMap2 = new ArrayMap<String, ArrayList>();
                while (query.moveToNext()) {
                    if (!query.isNull(columnIndexOrThrow)) {
                        final String string = query.getString(columnIndexOrThrow);
                        if (arrayMap.get(string) == null) {
                            arrayMap.put(string, new ArrayList<String>());
                        }
                    }
                    if (!query.isNull(columnIndexOrThrow)) {
                        final String string2 = query.getString(columnIndexOrThrow);
                        if (arrayMap2.get(string2) != null) {
                            continue;
                        }
                        arrayMap2.put(string2, new ArrayList<Data>());
                    }
                }
                query.moveToPosition(-1);
                this.__fetchRelationshipWorkTagAsjavaLangString((ArrayMap<String, ArrayList<String>>)arrayMap);
                this.__fetchRelationshipWorkProgressAsandroidxWorkData((ArrayMap<String, ArrayList<Data>>)arrayMap2);
                if (query.moveToFirst()) {
                    ArrayList list2;
                    if (!query.isNull(columnIndexOrThrow)) {
                        list2 = arrayMap.get(query.getString(columnIndexOrThrow));
                    }
                    else {
                        list2 = null;
                    }
                    ArrayList tags = list2;
                    if (list2 == null) {
                        tags = new ArrayList();
                    }
                    ArrayList list3 = list;
                    if (!query.isNull(columnIndexOrThrow)) {
                        list3 = arrayMap2.get(query.getString(columnIndexOrThrow));
                    }
                    ArrayList progress;
                    if ((progress = list3) == null) {
                        progress = new ArrayList();
                    }
                    workInfoPojo = new WorkSpec.WorkInfoPojo();
                    workInfoPojo.id = query.getString(columnIndexOrThrow);
                    workInfoPojo.state = WorkTypeConverters.intToState(query.getInt(columnIndexOrThrow2));
                    workInfoPojo.output = Data.fromByteArray(query.getBlob(columnIndexOrThrow3));
                    workInfoPojo.runAttemptCount = query.getInt(columnIndexOrThrow4);
                    workInfoPojo.tags = tags;
                    workInfoPojo.progress = progress;
                }
                this.__db.setTransactionSuccessful();
                return workInfoPojo;
            }
            finally {
                query.close();
                acquire.release();
            }
        }
        finally {
            this.__db.endTransaction();
        }
    }
    
    @Override
    public List<WorkSpec.WorkInfoPojo> getWorkStatusPojoForIds(final List<String> list) {
        final StringBuilder stringBuilder = StringUtil.newStringBuilder();
        stringBuilder.append("SELECT id, state, output, run_attempt_count FROM workspec WHERE id IN (");
        final int size = list.size();
        StringUtil.appendPlaceholders(stringBuilder, size);
        stringBuilder.append(")");
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire(stringBuilder.toString(), size + 0);
        final Iterator iterator = list.iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final String s = (String)iterator.next();
            if (s == null) {
                acquire.bindNull(n);
            }
            else {
                acquire.bindString(n, s);
            }
            ++n;
        }
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            final Cursor query = DBUtil.query(this.__db, acquire, true, null);
            try {
                final int columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow(query, "id");
                final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow(query, "state");
                final int columnIndexOrThrow3 = CursorUtil.getColumnIndexOrThrow(query, "output");
                final int columnIndexOrThrow4 = CursorUtil.getColumnIndexOrThrow(query, "run_attempt_count");
                final ArrayMap<Object, ArrayList> arrayMap = new ArrayMap<Object, ArrayList>();
                final ArrayMap<Object, ArrayList> arrayMap2 = new ArrayMap<Object, ArrayList>();
                while (query.moveToNext()) {
                    if (!query.isNull(columnIndexOrThrow)) {
                        final String string = query.getString(columnIndexOrThrow);
                        if (arrayMap.get(string) == null) {
                            arrayMap.put(string, new ArrayList<String>());
                        }
                    }
                    if (!query.isNull(columnIndexOrThrow)) {
                        final String string2 = query.getString(columnIndexOrThrow);
                        if (arrayMap2.get(string2) != null) {
                            continue;
                        }
                        arrayMap2.put(string2, new ArrayList<Data>());
                    }
                }
                query.moveToPosition(-1);
                this.__fetchRelationshipWorkTagAsjavaLangString((ArrayMap<String, ArrayList<String>>)arrayMap);
                this.__fetchRelationshipWorkProgressAsandroidxWorkData((ArrayMap<String, ArrayList<Data>>)arrayMap2);
                final ArrayList list2 = new ArrayList(query.getCount());
                while (query.moveToNext()) {
                    ArrayList list3;
                    if (!query.isNull(columnIndexOrThrow)) {
                        list3 = arrayMap.get(query.getString(columnIndexOrThrow));
                    }
                    else {
                        list3 = null;
                    }
                    ArrayList tags = list3;
                    if (list3 == null) {
                        tags = new ArrayList();
                    }
                    ArrayList list4;
                    if (!query.isNull(columnIndexOrThrow)) {
                        list4 = arrayMap2.get(query.getString(columnIndexOrThrow));
                    }
                    else {
                        list4 = null;
                    }
                    ArrayList progress = list4;
                    if (list4 == null) {
                        progress = new ArrayList();
                    }
                    final WorkSpec.WorkInfoPojo workInfoPojo = new WorkSpec.WorkInfoPojo();
                    workInfoPojo.id = query.getString(columnIndexOrThrow);
                    workInfoPojo.state = WorkTypeConverters.intToState(query.getInt(columnIndexOrThrow2));
                    workInfoPojo.output = Data.fromByteArray(query.getBlob(columnIndexOrThrow3));
                    workInfoPojo.runAttemptCount = query.getInt(columnIndexOrThrow4);
                    workInfoPojo.tags = tags;
                    workInfoPojo.progress = progress;
                    list2.add((Object)workInfoPojo);
                }
                this.__db.setTransactionSuccessful();
                return (List<WorkSpec.WorkInfoPojo>)list2;
            }
            finally {
                query.close();
                acquire.release();
            }
        }
        finally {
            this.__db.endTransaction();
        }
    }
    
    @Override
    public List<WorkSpec.WorkInfoPojo> getWorkStatusPojoForName(String string) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT id, state, output, run_attempt_count FROM workspec WHERE id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (string == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, string);
        }
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            final Cursor query = DBUtil.query(this.__db, acquire, true, null);
            try {
                final int columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow(query, "id");
                final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow(query, "state");
                final int columnIndexOrThrow3 = CursorUtil.getColumnIndexOrThrow(query, "output");
                final int columnIndexOrThrow4 = CursorUtil.getColumnIndexOrThrow(query, "run_attempt_count");
                final ArrayMap<Object, ArrayList> arrayMap = new ArrayMap<Object, ArrayList>();
                final ArrayMap<Object, ArrayList> arrayMap2 = new ArrayMap<Object, ArrayList>();
                while (query.moveToNext()) {
                    if (!query.isNull(columnIndexOrThrow)) {
                        string = query.getString(columnIndexOrThrow);
                        if (arrayMap.get(string) == null) {
                            arrayMap.put(string, new ArrayList<String>());
                        }
                    }
                    if (!query.isNull(columnIndexOrThrow)) {
                        final String string2 = query.getString(columnIndexOrThrow);
                        if (arrayMap2.get(string2) != null) {
                            continue;
                        }
                        arrayMap2.put(string2, new ArrayList<Data>());
                    }
                }
                query.moveToPosition(-1);
                this.__fetchRelationshipWorkTagAsjavaLangString((ArrayMap<String, ArrayList<String>>)arrayMap);
                this.__fetchRelationshipWorkProgressAsandroidxWorkData((ArrayMap<String, ArrayList<Data>>)arrayMap2);
                final ArrayList list = new ArrayList(query.getCount());
                while (query.moveToNext()) {
                    ArrayList list2;
                    if (!query.isNull(columnIndexOrThrow)) {
                        list2 = arrayMap.get(query.getString(columnIndexOrThrow));
                    }
                    else {
                        list2 = null;
                    }
                    ArrayList tags = list2;
                    if (list2 == null) {
                        tags = new ArrayList();
                    }
                    ArrayList list3;
                    if (!query.isNull(columnIndexOrThrow)) {
                        list3 = arrayMap2.get(query.getString(columnIndexOrThrow));
                    }
                    else {
                        list3 = null;
                    }
                    ArrayList progress = list3;
                    if (list3 == null) {
                        progress = new ArrayList();
                    }
                    final WorkSpec.WorkInfoPojo workInfoPojo = new WorkSpec.WorkInfoPojo();
                    workInfoPojo.id = query.getString(columnIndexOrThrow);
                    workInfoPojo.state = WorkTypeConverters.intToState(query.getInt(columnIndexOrThrow2));
                    workInfoPojo.output = Data.fromByteArray(query.getBlob(columnIndexOrThrow3));
                    workInfoPojo.runAttemptCount = query.getInt(columnIndexOrThrow4);
                    workInfoPojo.tags = tags;
                    workInfoPojo.progress = progress;
                    list.add((Object)workInfoPojo);
                }
                this.__db.setTransactionSuccessful();
                return (List<WorkSpec.WorkInfoPojo>)list;
            }
            finally {
                query.close();
                acquire.release();
            }
        }
        finally {
            this.__db.endTransaction();
        }
    }
    
    @Override
    public List<WorkSpec.WorkInfoPojo> getWorkStatusPojoForTag(String string) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT id, state, output, run_attempt_count FROM workspec WHERE id IN (SELECT work_spec_id FROM worktag WHERE tag=?)", 1);
        if (string == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, string);
        }
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            final Cursor query = DBUtil.query(this.__db, acquire, true, null);
            try {
                final int columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow(query, "id");
                final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow(query, "state");
                final int columnIndexOrThrow3 = CursorUtil.getColumnIndexOrThrow(query, "output");
                final int columnIndexOrThrow4 = CursorUtil.getColumnIndexOrThrow(query, "run_attempt_count");
                final ArrayMap<Object, ArrayList> arrayMap = new ArrayMap<Object, ArrayList>();
                final ArrayMap<Object, ArrayList> arrayMap2 = new ArrayMap<Object, ArrayList>();
                while (query.moveToNext()) {
                    if (!query.isNull(columnIndexOrThrow)) {
                        string = query.getString(columnIndexOrThrow);
                        if (arrayMap.get(string) == null) {
                            arrayMap.put(string, new ArrayList<String>());
                        }
                    }
                    if (!query.isNull(columnIndexOrThrow)) {
                        final String string2 = query.getString(columnIndexOrThrow);
                        if (arrayMap2.get(string2) != null) {
                            continue;
                        }
                        arrayMap2.put(string2, new ArrayList<Data>());
                    }
                }
                query.moveToPosition(-1);
                this.__fetchRelationshipWorkTagAsjavaLangString((ArrayMap<String, ArrayList<String>>)arrayMap);
                this.__fetchRelationshipWorkProgressAsandroidxWorkData((ArrayMap<String, ArrayList<Data>>)arrayMap2);
                final ArrayList list = new ArrayList(query.getCount());
                while (query.moveToNext()) {
                    ArrayList list2;
                    if (!query.isNull(columnIndexOrThrow)) {
                        list2 = arrayMap.get(query.getString(columnIndexOrThrow));
                    }
                    else {
                        list2 = null;
                    }
                    ArrayList tags = list2;
                    if (list2 == null) {
                        tags = new ArrayList();
                    }
                    ArrayList list3;
                    if (!query.isNull(columnIndexOrThrow)) {
                        list3 = arrayMap2.get(query.getString(columnIndexOrThrow));
                    }
                    else {
                        list3 = null;
                    }
                    ArrayList progress = list3;
                    if (list3 == null) {
                        progress = new ArrayList();
                    }
                    final WorkSpec.WorkInfoPojo workInfoPojo = new WorkSpec.WorkInfoPojo();
                    workInfoPojo.id = query.getString(columnIndexOrThrow);
                    workInfoPojo.state = WorkTypeConverters.intToState(query.getInt(columnIndexOrThrow2));
                    workInfoPojo.output = Data.fromByteArray(query.getBlob(columnIndexOrThrow3));
                    workInfoPojo.runAttemptCount = query.getInt(columnIndexOrThrow4);
                    workInfoPojo.tags = tags;
                    workInfoPojo.progress = progress;
                    list.add((Object)workInfoPojo);
                }
                this.__db.setTransactionSuccessful();
                return (List<WorkSpec.WorkInfoPojo>)list;
            }
            finally {
                query.close();
                acquire.release();
            }
        }
        finally {
            this.__db.endTransaction();
        }
    }
    
    @Override
    public LiveData<List<WorkSpec.WorkInfoPojo>> getWorkStatusPojoLiveDataForIds(final List<String> list) {
        final StringBuilder stringBuilder = StringUtil.newStringBuilder();
        stringBuilder.append("SELECT id, state, output, run_attempt_count FROM workspec WHERE id IN (");
        final int size = list.size();
        StringUtil.appendPlaceholders(stringBuilder, size);
        stringBuilder.append(")");
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire(stringBuilder.toString(), size + 0);
        final Iterator iterator = list.iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final String s = (String)iterator.next();
            if (s == null) {
                acquire.bindNull(n);
            }
            else {
                acquire.bindString(n, s);
            }
            ++n;
        }
        return this.__db.getInvalidationTracker().createLiveData(new String[] { "WorkTag", "WorkProgress", "workspec" }, true, (Callable<List<WorkSpec.WorkInfoPojo>>)new Callable<List<WorkSpec.WorkInfoPojo>>(this, acquire) {
            final WorkSpecDao_Impl this$0;
            final RoomSQLiteQuery val$_statement;
            
            @Override
            public List<WorkSpec.WorkInfoPojo> call() throws Exception {
                this.this$0.__db.beginTransaction();
                try {
                    final Cursor query = DBUtil.query(this.this$0.__db, this.val$_statement, true, null);
                    try {
                        final int columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow(query, "id");
                        final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow(query, "state");
                        final int columnIndexOrThrow3 = CursorUtil.getColumnIndexOrThrow(query, "output");
                        final int columnIndexOrThrow4 = CursorUtil.getColumnIndexOrThrow(query, "run_attempt_count");
                        final ArrayMap arrayMap = new ArrayMap();
                        final ArrayMap arrayMap2 = new ArrayMap();
                        while (query.moveToNext()) {
                            if (!query.isNull(columnIndexOrThrow)) {
                                final String string = query.getString(columnIndexOrThrow);
                                if (arrayMap.get(string) == null) {
                                    arrayMap.put(string, new ArrayList());
                                }
                            }
                            if (!query.isNull(columnIndexOrThrow)) {
                                final String string2 = query.getString(columnIndexOrThrow);
                                if (arrayMap2.get(string2) != null) {
                                    continue;
                                }
                                arrayMap2.put(string2, new ArrayList());
                            }
                        }
                        query.moveToPosition(-1);
                        this.this$0.__fetchRelationshipWorkTagAsjavaLangString(arrayMap);
                        this.this$0.__fetchRelationshipWorkProgressAsandroidxWorkData(arrayMap2);
                        final ArrayList list = new ArrayList(query.getCount());
                        while (query.moveToNext()) {
                            ArrayList list2;
                            if (!query.isNull(columnIndexOrThrow)) {
                                list2 = (ArrayList)arrayMap.get(query.getString(columnIndexOrThrow));
                            }
                            else {
                                list2 = null;
                            }
                            ArrayList tags = list2;
                            if (list2 == null) {
                                tags = new ArrayList();
                            }
                            ArrayList list3;
                            if (!query.isNull(columnIndexOrThrow)) {
                                list3 = (ArrayList)arrayMap2.get(query.getString(columnIndexOrThrow));
                            }
                            else {
                                list3 = null;
                            }
                            ArrayList progress = list3;
                            if (list3 == null) {
                                progress = new ArrayList();
                            }
                            final WorkSpec.WorkInfoPojo workInfoPojo = new WorkSpec.WorkInfoPojo();
                            workInfoPojo.id = query.getString(columnIndexOrThrow);
                            workInfoPojo.state = WorkTypeConverters.intToState(query.getInt(columnIndexOrThrow2));
                            workInfoPojo.output = Data.fromByteArray(query.getBlob(columnIndexOrThrow3));
                            workInfoPojo.runAttemptCount = query.getInt(columnIndexOrThrow4);
                            workInfoPojo.tags = tags;
                            workInfoPojo.progress = progress;
                            list.add((Object)workInfoPojo);
                        }
                        this.this$0.__db.setTransactionSuccessful();
                        return (List<WorkSpec.WorkInfoPojo>)list;
                    }
                    finally {
                        query.close();
                    }
                }
                finally {
                    this.this$0.__db.endTransaction();
                }
            }
            
            @Override
            protected void finalize() {
                this.val$_statement.release();
            }
        });
    }
    
    @Override
    public LiveData<List<WorkSpec.WorkInfoPojo>> getWorkStatusPojoLiveDataForName(final String s) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT id, state, output, run_attempt_count FROM workspec WHERE id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        return this.__db.getInvalidationTracker().createLiveData(new String[] { "WorkTag", "WorkProgress", "workspec", "workname" }, true, (Callable<List<WorkSpec.WorkInfoPojo>>)new Callable<List<WorkSpec.WorkInfoPojo>>(this, acquire) {
            final WorkSpecDao_Impl this$0;
            final RoomSQLiteQuery val$_statement;
            
            @Override
            public List<WorkSpec.WorkInfoPojo> call() throws Exception {
                this.this$0.__db.beginTransaction();
                try {
                    final Cursor query = DBUtil.query(this.this$0.__db, this.val$_statement, true, null);
                    try {
                        final int columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow(query, "id");
                        final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow(query, "state");
                        final int columnIndexOrThrow3 = CursorUtil.getColumnIndexOrThrow(query, "output");
                        final int columnIndexOrThrow4 = CursorUtil.getColumnIndexOrThrow(query, "run_attempt_count");
                        final ArrayMap arrayMap = new ArrayMap();
                        final ArrayMap arrayMap2 = new ArrayMap();
                        while (query.moveToNext()) {
                            if (!query.isNull(columnIndexOrThrow)) {
                                final String string = query.getString(columnIndexOrThrow);
                                if (arrayMap.get(string) == null) {
                                    arrayMap.put(string, new ArrayList());
                                }
                            }
                            if (!query.isNull(columnIndexOrThrow)) {
                                final String string2 = query.getString(columnIndexOrThrow);
                                if (arrayMap2.get(string2) != null) {
                                    continue;
                                }
                                arrayMap2.put(string2, new ArrayList());
                            }
                        }
                        query.moveToPosition(-1);
                        this.this$0.__fetchRelationshipWorkTagAsjavaLangString(arrayMap);
                        this.this$0.__fetchRelationshipWorkProgressAsandroidxWorkData(arrayMap2);
                        final ArrayList list = new ArrayList(query.getCount());
                        while (query.moveToNext()) {
                            ArrayList list2;
                            if (!query.isNull(columnIndexOrThrow)) {
                                list2 = (ArrayList)arrayMap.get(query.getString(columnIndexOrThrow));
                            }
                            else {
                                list2 = null;
                            }
                            ArrayList tags = list2;
                            if (list2 == null) {
                                tags = new ArrayList();
                            }
                            ArrayList list3;
                            if (!query.isNull(columnIndexOrThrow)) {
                                list3 = (ArrayList)arrayMap2.get(query.getString(columnIndexOrThrow));
                            }
                            else {
                                list3 = null;
                            }
                            ArrayList progress = list3;
                            if (list3 == null) {
                                progress = new ArrayList();
                            }
                            final WorkSpec.WorkInfoPojo workInfoPojo = new WorkSpec.WorkInfoPojo();
                            workInfoPojo.id = query.getString(columnIndexOrThrow);
                            workInfoPojo.state = WorkTypeConverters.intToState(query.getInt(columnIndexOrThrow2));
                            workInfoPojo.output = Data.fromByteArray(query.getBlob(columnIndexOrThrow3));
                            workInfoPojo.runAttemptCount = query.getInt(columnIndexOrThrow4);
                            workInfoPojo.tags = tags;
                            workInfoPojo.progress = progress;
                            list.add((Object)workInfoPojo);
                        }
                        this.this$0.__db.setTransactionSuccessful();
                        return (List<WorkSpec.WorkInfoPojo>)list;
                    }
                    finally {
                        query.close();
                    }
                }
                finally {
                    this.this$0.__db.endTransaction();
                }
            }
            
            @Override
            protected void finalize() {
                this.val$_statement.release();
            }
        });
    }
    
    @Override
    public LiveData<List<WorkSpec.WorkInfoPojo>> getWorkStatusPojoLiveDataForTag(final String s) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT id, state, output, run_attempt_count FROM workspec WHERE id IN (SELECT work_spec_id FROM worktag WHERE tag=?)", 1);
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        return this.__db.getInvalidationTracker().createLiveData(new String[] { "WorkTag", "WorkProgress", "workspec", "worktag" }, true, (Callable<List<WorkSpec.WorkInfoPojo>>)new Callable<List<WorkSpec.WorkInfoPojo>>(this, acquire) {
            final WorkSpecDao_Impl this$0;
            final RoomSQLiteQuery val$_statement;
            
            @Override
            public List<WorkSpec.WorkInfoPojo> call() throws Exception {
                this.this$0.__db.beginTransaction();
                try {
                    final Cursor query = DBUtil.query(this.this$0.__db, this.val$_statement, true, null);
                    try {
                        final int columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow(query, "id");
                        final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow(query, "state");
                        final int columnIndexOrThrow3 = CursorUtil.getColumnIndexOrThrow(query, "output");
                        final int columnIndexOrThrow4 = CursorUtil.getColumnIndexOrThrow(query, "run_attempt_count");
                        final ArrayMap arrayMap = new ArrayMap();
                        final ArrayMap arrayMap2 = new ArrayMap();
                        while (query.moveToNext()) {
                            if (!query.isNull(columnIndexOrThrow)) {
                                final String string = query.getString(columnIndexOrThrow);
                                if (arrayMap.get(string) == null) {
                                    arrayMap.put(string, new ArrayList());
                                }
                            }
                            if (!query.isNull(columnIndexOrThrow)) {
                                final String string2 = query.getString(columnIndexOrThrow);
                                if (arrayMap2.get(string2) != null) {
                                    continue;
                                }
                                arrayMap2.put(string2, new ArrayList());
                            }
                        }
                        query.moveToPosition(-1);
                        this.this$0.__fetchRelationshipWorkTagAsjavaLangString(arrayMap);
                        this.this$0.__fetchRelationshipWorkProgressAsandroidxWorkData(arrayMap2);
                        final ArrayList list = new ArrayList(query.getCount());
                        while (query.moveToNext()) {
                            ArrayList list2;
                            if (!query.isNull(columnIndexOrThrow)) {
                                list2 = (ArrayList)arrayMap.get(query.getString(columnIndexOrThrow));
                            }
                            else {
                                list2 = null;
                            }
                            ArrayList tags = list2;
                            if (list2 == null) {
                                tags = new ArrayList();
                            }
                            ArrayList list3;
                            if (!query.isNull(columnIndexOrThrow)) {
                                list3 = (ArrayList)arrayMap2.get(query.getString(columnIndexOrThrow));
                            }
                            else {
                                list3 = null;
                            }
                            ArrayList progress = list3;
                            if (list3 == null) {
                                progress = new ArrayList();
                            }
                            final WorkSpec.WorkInfoPojo workInfoPojo = new WorkSpec.WorkInfoPojo();
                            workInfoPojo.id = query.getString(columnIndexOrThrow);
                            workInfoPojo.state = WorkTypeConverters.intToState(query.getInt(columnIndexOrThrow2));
                            workInfoPojo.output = Data.fromByteArray(query.getBlob(columnIndexOrThrow3));
                            workInfoPojo.runAttemptCount = query.getInt(columnIndexOrThrow4);
                            workInfoPojo.tags = tags;
                            workInfoPojo.progress = progress;
                            list.add((Object)workInfoPojo);
                        }
                        this.this$0.__db.setTransactionSuccessful();
                        return (List<WorkSpec.WorkInfoPojo>)list;
                    }
                    finally {
                        query.close();
                    }
                }
                finally {
                    this.this$0.__db.endTransaction();
                }
            }
            
            @Override
            protected void finalize() {
                this.val$_statement.release();
            }
        });
    }
    
    @Override
    public boolean hasUnfinishedWork() {
        final boolean b = false;
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT COUNT(*) > 0 FROM workspec WHERE state NOT IN (2, 3, 5) LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        boolean b2 = b;
        try {
            if (query.moveToFirst()) {
                final int int1 = query.getInt(0);
                b2 = b;
                if (int1 != 0) {
                    b2 = true;
                }
            }
            return b2;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public int incrementWorkSpecRunAttemptCount(final String s) {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfIncrementWorkSpecRunAttemptCount.acquire();
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.beginTransaction();
        try {
            final int executeUpdateDelete = acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
            return executeUpdateDelete;
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfIncrementWorkSpecRunAttemptCount.release(acquire);
        }
    }
    
    @Override
    public void insertWorkSpec(final WorkSpec workSpec) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWorkSpec.insert(workSpec);
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
        }
    }
    
    @Override
    public int markWorkSpecScheduled(final String s, final long n) {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfMarkWorkSpecScheduled.acquire();
        acquire.bindLong(1, n);
        if (s == null) {
            acquire.bindNull(2);
        }
        else {
            acquire.bindString(2, s);
        }
        this.__db.beginTransaction();
        try {
            final int executeUpdateDelete = acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
            return executeUpdateDelete;
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfMarkWorkSpecScheduled.release(acquire);
        }
    }
    
    @Override
    public void pruneFinishedWorkWithZeroDependentsIgnoringKeepForAtLeast() {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfPruneFinishedWorkWithZeroDependentsIgnoringKeepForAtLeast.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfPruneFinishedWorkWithZeroDependentsIgnoringKeepForAtLeast.release(acquire);
        }
    }
    
    @Override
    public int resetScheduledState() {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfResetScheduledState.acquire();
        this.__db.beginTransaction();
        try {
            final int executeUpdateDelete = acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
            return executeUpdateDelete;
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfResetScheduledState.release(acquire);
        }
    }
    
    @Override
    public int resetWorkSpecRunAttemptCount(final String s) {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfResetWorkSpecRunAttemptCount.acquire();
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.beginTransaction();
        try {
            final int executeUpdateDelete = acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
            return executeUpdateDelete;
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfResetWorkSpecRunAttemptCount.release(acquire);
        }
    }
    
    @Override
    public void setOutput(final String s, final Data data) {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfSetOutput.acquire();
        final byte[] byteArrayInternal = Data.toByteArrayInternal(data);
        if (byteArrayInternal == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindBlob(1, byteArrayInternal);
        }
        if (s == null) {
            acquire.bindNull(2);
        }
        else {
            acquire.bindString(2, s);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfSetOutput.release(acquire);
        }
    }
    
    @Override
    public void setPeriodStartTime(final String s, final long n) {
        this.__db.assertNotSuspendingTransaction();
        final SupportSQLiteStatement acquire = this.__preparedStmtOfSetPeriodStartTime.acquire();
        acquire.bindLong(1, n);
        if (s == null) {
            acquire.bindNull(2);
        }
        else {
            acquire.bindString(2, s);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
            this.__preparedStmtOfSetPeriodStartTime.release(acquire);
        }
    }
    
    @Override
    public int setState(final WorkInfo.State state, final String... array) {
        this.__db.assertNotSuspendingTransaction();
        final StringBuilder stringBuilder = StringUtil.newStringBuilder();
        stringBuilder.append("UPDATE workspec SET state=");
        stringBuilder.append("?");
        stringBuilder.append(" WHERE id IN (");
        StringUtil.appendPlaceholders(stringBuilder, array.length);
        stringBuilder.append(")");
        final SupportSQLiteStatement compileStatement = this.__db.compileStatement(stringBuilder.toString());
        compileStatement.bindLong(1, WorkTypeConverters.stateToInt(state));
        final int length = array.length;
        int n = 2;
        for (final String s : array) {
            if (s == null) {
                compileStatement.bindNull(n);
            }
            else {
                compileStatement.bindString(n, s);
            }
            ++n;
        }
        this.__db.beginTransaction();
        try {
            final int executeUpdateDelete = compileStatement.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
            return executeUpdateDelete;
        }
        finally {
            this.__db.endTransaction();
        }
    }
}

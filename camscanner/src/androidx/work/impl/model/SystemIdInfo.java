// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.ForeignKey;
import androidx.room.Entity;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
@Entity(foreignKeys = { @ForeignKey(childColumns = { "work_spec_id" }, entity = WorkSpec.class, onDelete = 5, onUpdate = 5, parentColumns = { "id" }) })
public class SystemIdInfo
{
    @ColumnInfo(name = "system_id")
    public final int systemId;
    @NonNull
    @ColumnInfo(name = "work_spec_id")
    @PrimaryKey
    public final String workSpecId;
    
    public SystemIdInfo(@NonNull final String workSpecId, final int systemId) {
        this.workSpecId = workSpecId;
        this.systemId = systemId;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SystemIdInfo)) {
            return false;
        }
        final SystemIdInfo systemIdInfo = (SystemIdInfo)o;
        return this.systemId == systemIdInfo.systemId && this.workSpecId.equals(systemIdInfo.workSpecId);
    }
    
    @Override
    public int hashCode() {
        return this.workSpecId.hashCode() * 31 + this.systemId;
    }
}

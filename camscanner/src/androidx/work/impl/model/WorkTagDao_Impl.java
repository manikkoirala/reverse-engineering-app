// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import android.database.Cursor;
import java.util.ArrayList;
import android.os.CancellationSignal;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.room.util.DBUtil;
import androidx.room.RoomSQLiteQuery;
import java.util.List;
import androidx.sqlite.db.SupportSQLiteStatement;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;

public final class WorkTagDao_Impl implements WorkTagDao
{
    private final RoomDatabase __db;
    private final EntityInsertionAdapter<WorkTag> __insertionAdapterOfWorkTag;
    
    public WorkTagDao_Impl(final RoomDatabase _db) {
        this.__db = _db;
        this.__insertionAdapterOfWorkTag = new EntityInsertionAdapter<WorkTag>(this, _db) {
            final WorkTagDao_Impl this$0;
            
            public void bind(final SupportSQLiteStatement supportSQLiteStatement, final WorkTag workTag) {
                final String tag = workTag.tag;
                if (tag == null) {
                    supportSQLiteStatement.bindNull(1);
                }
                else {
                    supportSQLiteStatement.bindString(1, tag);
                }
                final String workSpecId = workTag.workSpecId;
                if (workSpecId == null) {
                    supportSQLiteStatement.bindNull(2);
                }
                else {
                    supportSQLiteStatement.bindString(2, workSpecId);
                }
            }
            
            public String createQuery() {
                return "INSERT OR IGNORE INTO `WorkTag` (`tag`,`work_spec_id`) VALUES (?,?)";
            }
        };
    }
    
    @Override
    public List<String> getTagsForWorkSpecId(String query) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT DISTINCT tag FROM worktag WHERE work_spec_id=?", 1);
        if (query == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, query);
        }
        this.__db.assertNotSuspendingTransaction();
        query = (String)DBUtil.query(this.__db, acquire, false, null);
        try {
            final ArrayList list = new ArrayList(((Cursor)query).getCount());
            while (((Cursor)query).moveToNext()) {
                list.add(((Cursor)query).getString(0));
            }
            return list;
        }
        finally {
            ((Cursor)query).close();
            acquire.release();
        }
    }
    
    @Override
    public List<String> getWorkSpecIdsWithTag(String query) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT work_spec_id FROM worktag WHERE tag=?", 1);
        if (query == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, query);
        }
        this.__db.assertNotSuspendingTransaction();
        query = (String)DBUtil.query(this.__db, acquire, false, null);
        try {
            final ArrayList list = new ArrayList(((Cursor)query).getCount());
            while (((Cursor)query).moveToNext()) {
                list.add(((Cursor)query).getString(0));
            }
            return list;
        }
        finally {
            ((Cursor)query).close();
            acquire.release();
        }
    }
    
    @Override
    public void insert(final WorkTag workTag) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWorkTag.insert(workTag);
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
        }
    }
}

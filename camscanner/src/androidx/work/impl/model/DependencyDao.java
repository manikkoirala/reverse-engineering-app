// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import androidx.room.Insert;
import androidx.room.Query;
import java.util.List;
import androidx.room.Dao;

@Dao
public interface DependencyDao
{
    @Query("SELECT work_spec_id FROM dependency WHERE prerequisite_id=:id")
    List<String> getDependentWorkIds(final String p0);
    
    @Query("SELECT prerequisite_id FROM dependency WHERE work_spec_id=:id")
    List<String> getPrerequisites(final String p0);
    
    @Query("SELECT COUNT(*)=0 FROM dependency WHERE work_spec_id=:id AND prerequisite_id IN (SELECT id FROM workspec WHERE state!=2)")
    boolean hasCompletedAllPrerequisites(final String p0);
    
    @Query("SELECT COUNT(*)>0 FROM dependency WHERE prerequisite_id=:id")
    boolean hasDependents(final String p0);
    
    @Insert(onConflict = 5)
    void insertDependency(final Dependency p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import androidx.room.Insert;
import java.util.List;
import androidx.room.Query;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.room.Dao;

@Dao
public interface SystemIdInfoDao
{
    @Nullable
    @Query("SELECT * FROM SystemIdInfo WHERE work_spec_id=:workSpecId")
    SystemIdInfo getSystemIdInfo(@NonNull final String p0);
    
    @NonNull
    @Query("SELECT DISTINCT work_spec_id FROM SystemIdInfo")
    List<String> getWorkSpecIds();
    
    @Insert(onConflict = 1)
    void insertSystemIdInfo(@NonNull final SystemIdInfo p0);
    
    @Query("DELETE FROM SystemIdInfo where work_spec_id=:workSpecId")
    void removeSystemIdInfo(@NonNull final String p0);
}

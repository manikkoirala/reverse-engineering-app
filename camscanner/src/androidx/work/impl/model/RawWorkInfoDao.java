// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import androidx.lifecycle.LiveData;
import androidx.room.RawQuery;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.room.Dao;

@Dao
public interface RawWorkInfoDao
{
    @NonNull
    @RawQuery(observedEntities = { WorkSpec.class })
    List<WorkSpec.WorkInfoPojo> getWorkInfoPojos(@NonNull final SupportSQLiteQuery p0);
    
    @NonNull
    @RawQuery(observedEntities = { WorkSpec.class })
    LiveData<List<WorkSpec.WorkInfoPojo>> getWorkInfoPojosLiveData(@NonNull final SupportSQLiteQuery p0);
}

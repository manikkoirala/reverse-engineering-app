// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import androidx.room.ColumnInfo;
import androidx.annotation.NonNull;
import androidx.room.Index;
import androidx.room.ForeignKey;
import androidx.room.Entity;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
@Entity(foreignKeys = { @ForeignKey(childColumns = { "work_spec_id" }, entity = WorkSpec.class, onDelete = 5, onUpdate = 5, parentColumns = { "id" }) }, indices = { @Index({ "work_spec_id" }) }, primaryKeys = { "name", "work_spec_id" })
public class WorkName
{
    @NonNull
    @ColumnInfo(name = "name")
    public final String name;
    @NonNull
    @ColumnInfo(name = "work_spec_id")
    public final String workSpecId;
    
    public WorkName(@NonNull final String name, @NonNull final String workSpecId) {
        this.name = name;
        this.workSpecId = workSpecId;
    }
}

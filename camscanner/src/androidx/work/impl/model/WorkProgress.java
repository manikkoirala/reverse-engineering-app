// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import androidx.room.PrimaryKey;
import androidx.room.ColumnInfo;
import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.room.ForeignKey;
import androidx.room.Entity;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
@Entity(foreignKeys = { @ForeignKey(childColumns = { "work_spec_id" }, entity = WorkSpec.class, onDelete = 5, onUpdate = 5, parentColumns = { "id" }) })
public class WorkProgress
{
    @NonNull
    @ColumnInfo(name = "progress")
    public final Data mProgress;
    @NonNull
    @ColumnInfo(name = "work_spec_id")
    @PrimaryKey
    public final String mWorkSpecId;
    
    public WorkProgress(@NonNull final String mWorkSpecId, @NonNull final Data mProgress) {
        this.mWorkSpecId = mWorkSpecId;
        this.mProgress = mProgress;
    }
}

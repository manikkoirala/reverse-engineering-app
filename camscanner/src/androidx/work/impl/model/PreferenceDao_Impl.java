// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import java.util.concurrent.Callable;
import androidx.lifecycle.LiveData;
import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.room.util.DBUtil;
import androidx.room.RoomSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteStatement;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;

public final class PreferenceDao_Impl implements PreferenceDao
{
    private final RoomDatabase __db;
    private final EntityInsertionAdapter<Preference> __insertionAdapterOfPreference;
    
    public PreferenceDao_Impl(final RoomDatabase _db) {
        this.__db = _db;
        this.__insertionAdapterOfPreference = new EntityInsertionAdapter<Preference>(this, _db) {
            final PreferenceDao_Impl this$0;
            
            public void bind(final SupportSQLiteStatement supportSQLiteStatement, final Preference preference) {
                final String mKey = preference.mKey;
                if (mKey == null) {
                    supportSQLiteStatement.bindNull(1);
                }
                else {
                    supportSQLiteStatement.bindString(1, mKey);
                }
                final Long mValue = preference.mValue;
                if (mValue == null) {
                    supportSQLiteStatement.bindNull(2);
                }
                else {
                    supportSQLiteStatement.bindLong(2, mValue);
                }
            }
            
            public String createQuery() {
                return "INSERT OR REPLACE INTO `Preference` (`key`,`long_value`) VALUES (?,?)";
            }
        };
    }
    
    @Override
    public Long getLongValue(final String s) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT long_value FROM Preference where `key`=?", 1);
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        this.__db.assertNotSuspendingTransaction();
        final RoomDatabase _db = this.__db;
        final Long n = null;
        final Cursor query = DBUtil.query(_db, acquire, false, null);
        Long value = n;
        try {
            if (query.moveToFirst()) {
                if (query.isNull(0)) {
                    value = n;
                }
                else {
                    value = query.getLong(0);
                }
            }
            return value;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public LiveData<Long> getObservableLongValue(final String s) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("SELECT long_value FROM Preference where `key`=?", 1);
        if (s == null) {
            acquire.bindNull(1);
        }
        else {
            acquire.bindString(1, s);
        }
        return this.__db.getInvalidationTracker().createLiveData(new String[] { "Preference" }, false, (Callable<Long>)new Callable<Long>(this, acquire) {
            final PreferenceDao_Impl this$0;
            final RoomSQLiteQuery val$_statement;
            
            @Override
            public Long call() throws Exception {
                final RoomDatabase access$000 = this.this$0.__db;
                final RoomSQLiteQuery val$_statement = this.val$_statement;
                final Long n = null;
                final Cursor query = DBUtil.query(access$000, val$_statement, false, null);
                Long value = n;
                try {
                    if (query.moveToFirst()) {
                        if (query.isNull(0)) {
                            value = n;
                        }
                        else {
                            value = query.getLong(0);
                        }
                    }
                    return value;
                }
                finally {
                    query.close();
                }
            }
            
            @Override
            protected void finalize() {
                this.val$_statement.release();
            }
        });
    }
    
    @Override
    public void insertPreference(final Preference preference) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfPreference.insert(preference);
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
        }
    }
}

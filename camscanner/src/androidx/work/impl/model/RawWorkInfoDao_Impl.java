// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import java.util.concurrent.Callable;
import androidx.lifecycle.LiveData;
import java.util.List;
import android.database.Cursor;
import java.util.Iterator;
import java.util.Set;
import androidx.room.util.CursorUtil;
import android.os.CancellationSignal;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.room.util.DBUtil;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.StringUtil;
import androidx.work.Data;
import java.util.ArrayList;
import androidx.collection.ArrayMap;
import androidx.room.RoomDatabase;

public final class RawWorkInfoDao_Impl implements RawWorkInfoDao
{
    private final RoomDatabase __db;
    
    public RawWorkInfoDao_Impl(final RoomDatabase _db) {
        this.__db = _db;
    }
    
    private void __fetchRelationshipWorkProgressAsandroidxWorkData(final ArrayMap<String, ArrayList<Data>> arrayMap) {
        final Set<Object> keySet = arrayMap.keySet();
        if (keySet.isEmpty()) {
            return;
        }
        if (arrayMap.size() > 999) {
            ArrayMap arrayMap2 = new ArrayMap(999);
            final int size = arrayMap.size();
            int i = 0;
            int n = 0;
        Label_0047:
            while (true) {
                n = 0;
                while (i < size) {
                    arrayMap2.put(arrayMap.keyAt(i), arrayMap.valueAt(i));
                    final int n2 = i + 1;
                    final int n3 = n + 1;
                    i = n2;
                    if ((n = n3) == 999) {
                        this.__fetchRelationshipWorkProgressAsandroidxWorkData(arrayMap2);
                        arrayMap2 = new ArrayMap(999);
                        i = n2;
                        continue Label_0047;
                    }
                }
                break;
            }
            if (n > 0) {
                this.__fetchRelationshipWorkProgressAsandroidxWorkData(arrayMap2);
            }
            return;
        }
        final StringBuilder stringBuilder = StringUtil.newStringBuilder();
        stringBuilder.append("SELECT `progress`,`work_spec_id` FROM `WorkProgress` WHERE `work_spec_id` IN (");
        final int size2 = keySet.size();
        StringUtil.appendPlaceholders(stringBuilder, size2);
        stringBuilder.append(")");
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire(stringBuilder.toString(), size2 + 0);
        final Iterator iterator = keySet.iterator();
        int n4 = 1;
        while (iterator.hasNext()) {
            final String s = (String)iterator.next();
            if (s == null) {
                acquire.bindNull(n4);
            }
            else {
                acquire.bindString(n4, s);
            }
            ++n4;
        }
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final int columnIndex = CursorUtil.getColumnIndex(query, "work_spec_id");
            if (columnIndex == -1) {
                return;
            }
            while (query.moveToNext()) {
                if (!query.isNull(columnIndex)) {
                    final ArrayList list = arrayMap.get(query.getString(columnIndex));
                    if (list == null) {
                        continue;
                    }
                    list.add(Data.fromByteArray(query.getBlob(0)));
                }
            }
        }
        finally {
            query.close();
        }
    }
    
    private void __fetchRelationshipWorkTagAsjavaLangString(final ArrayMap<String, ArrayList<String>> arrayMap) {
        final Set<Object> keySet = arrayMap.keySet();
        if (keySet.isEmpty()) {
            return;
        }
        if (arrayMap.size() > 999) {
            ArrayMap arrayMap2 = new ArrayMap(999);
            final int size = arrayMap.size();
            int i = 0;
            int n = 0;
        Label_0047:
            while (true) {
                n = 0;
                while (i < size) {
                    arrayMap2.put(arrayMap.keyAt(i), arrayMap.valueAt(i));
                    final int n2 = i + 1;
                    final int n3 = n + 1;
                    i = n2;
                    if ((n = n3) == 999) {
                        this.__fetchRelationshipWorkTagAsjavaLangString(arrayMap2);
                        arrayMap2 = new ArrayMap(999);
                        i = n2;
                        continue Label_0047;
                    }
                }
                break;
            }
            if (n > 0) {
                this.__fetchRelationshipWorkTagAsjavaLangString(arrayMap2);
            }
            return;
        }
        final StringBuilder stringBuilder = StringUtil.newStringBuilder();
        stringBuilder.append("SELECT `tag`,`work_spec_id` FROM `WorkTag` WHERE `work_spec_id` IN (");
        final int size2 = keySet.size();
        StringUtil.appendPlaceholders(stringBuilder, size2);
        stringBuilder.append(")");
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire(stringBuilder.toString(), size2 + 0);
        final Iterator iterator = keySet.iterator();
        int n4 = 1;
        while (iterator.hasNext()) {
            final String s = (String)iterator.next();
            if (s == null) {
                acquire.bindNull(n4);
            }
            else {
                acquire.bindString(n4, s);
            }
            ++n4;
        }
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final int columnIndex = CursorUtil.getColumnIndex(query, "work_spec_id");
            if (columnIndex == -1) {
                return;
            }
            while (query.moveToNext()) {
                if (!query.isNull(columnIndex)) {
                    final ArrayList list = arrayMap.get(query.getString(columnIndex));
                    if (list == null) {
                        continue;
                    }
                    list.add(query.getString(0));
                }
            }
        }
        finally {
            query.close();
        }
    }
    
    @Override
    public List<WorkSpec.WorkInfoPojo> getWorkInfoPojos(final SupportSQLiteQuery supportSQLiteQuery) {
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, supportSQLiteQuery, true, null);
        try {
            final int columnIndex = CursorUtil.getColumnIndex(query, "id");
            final int columnIndex2 = CursorUtil.getColumnIndex(query, "state");
            final int columnIndex3 = CursorUtil.getColumnIndex(query, "output");
            final int columnIndex4 = CursorUtil.getColumnIndex(query, "run_attempt_count");
            final ArrayMap<Object, ArrayList> arrayMap = new ArrayMap<Object, ArrayList>();
            final ArrayMap<Object, ArrayList> arrayMap2 = new ArrayMap<Object, ArrayList>();
            while (query.moveToNext()) {
                if (!query.isNull(columnIndex)) {
                    final String string = query.getString(columnIndex);
                    if (arrayMap.get(string) == null) {
                        arrayMap.put(string, new ArrayList<String>());
                    }
                }
                if (!query.isNull(columnIndex)) {
                    final String string2 = query.getString(columnIndex);
                    if (arrayMap2.get(string2) != null) {
                        continue;
                    }
                    arrayMap2.put(string2, new ArrayList<Data>());
                }
            }
            query.moveToPosition(-1);
            this.__fetchRelationshipWorkTagAsjavaLangString((ArrayMap<String, ArrayList<String>>)arrayMap);
            this.__fetchRelationshipWorkProgressAsandroidxWorkData((ArrayMap<String, ArrayList<Data>>)arrayMap2);
            final ArrayList list = new ArrayList(query.getCount());
            while (query.moveToNext()) {
                ArrayList list2;
                if (!query.isNull(columnIndex)) {
                    list2 = arrayMap.get(query.getString(columnIndex));
                }
                else {
                    list2 = null;
                }
                ArrayList tags = list2;
                if (list2 == null) {
                    tags = new ArrayList();
                }
                ArrayList list3;
                if (!query.isNull(columnIndex)) {
                    list3 = arrayMap2.get(query.getString(columnIndex));
                }
                else {
                    list3 = null;
                }
                ArrayList progress = list3;
                if (list3 == null) {
                    progress = new ArrayList();
                }
                final WorkSpec.WorkInfoPojo workInfoPojo = new WorkSpec.WorkInfoPojo();
                if (columnIndex != -1) {
                    workInfoPojo.id = query.getString(columnIndex);
                }
                if (columnIndex2 != -1) {
                    workInfoPojo.state = WorkTypeConverters.intToState(query.getInt(columnIndex2));
                }
                if (columnIndex3 != -1) {
                    workInfoPojo.output = Data.fromByteArray(query.getBlob(columnIndex3));
                }
                if (columnIndex4 != -1) {
                    workInfoPojo.runAttemptCount = query.getInt(columnIndex4);
                }
                workInfoPojo.tags = tags;
                workInfoPojo.progress = progress;
                list.add((Object)workInfoPojo);
            }
            return (List<WorkSpec.WorkInfoPojo>)list;
        }
        finally {
            query.close();
        }
    }
    
    @Override
    public LiveData<List<WorkSpec.WorkInfoPojo>> getWorkInfoPojosLiveData(final SupportSQLiteQuery supportSQLiteQuery) {
        return this.__db.getInvalidationTracker().createLiveData(new String[] { "WorkTag", "WorkProgress", "WorkSpec" }, false, (Callable<List<WorkSpec.WorkInfoPojo>>)new Callable<List<WorkSpec.WorkInfoPojo>>(this, supportSQLiteQuery) {
            final RawWorkInfoDao_Impl this$0;
            final SupportSQLiteQuery val$_internalQuery;
            
            @Override
            public List<WorkSpec.WorkInfoPojo> call() throws Exception {
                final Cursor query = DBUtil.query(this.this$0.__db, this.val$_internalQuery, true, null);
                try {
                    final int columnIndex = CursorUtil.getColumnIndex(query, "id");
                    final int columnIndex2 = CursorUtil.getColumnIndex(query, "state");
                    final int columnIndex3 = CursorUtil.getColumnIndex(query, "output");
                    final int columnIndex4 = CursorUtil.getColumnIndex(query, "run_attempt_count");
                    final ArrayMap arrayMap = new ArrayMap();
                    final ArrayMap arrayMap2 = new ArrayMap();
                    while (query.moveToNext()) {
                        if (!query.isNull(columnIndex)) {
                            final String string = query.getString(columnIndex);
                            if (arrayMap.get(string) == null) {
                                arrayMap.put(string, new ArrayList());
                            }
                        }
                        if (!query.isNull(columnIndex)) {
                            final String string2 = query.getString(columnIndex);
                            if (arrayMap2.get(string2) != null) {
                                continue;
                            }
                            arrayMap2.put(string2, new ArrayList());
                        }
                    }
                    query.moveToPosition(-1);
                    this.this$0.__fetchRelationshipWorkTagAsjavaLangString(arrayMap);
                    this.this$0.__fetchRelationshipWorkProgressAsandroidxWorkData(arrayMap2);
                    final ArrayList list = new ArrayList(query.getCount());
                    while (query.moveToNext()) {
                        ArrayList list2;
                        if (!query.isNull(columnIndex)) {
                            list2 = (ArrayList)arrayMap.get(query.getString(columnIndex));
                        }
                        else {
                            list2 = null;
                        }
                        ArrayList tags = list2;
                        if (list2 == null) {
                            tags = new ArrayList();
                        }
                        ArrayList list3;
                        if (!query.isNull(columnIndex)) {
                            list3 = (ArrayList)arrayMap2.get(query.getString(columnIndex));
                        }
                        else {
                            list3 = null;
                        }
                        ArrayList progress = list3;
                        if (list3 == null) {
                            progress = new ArrayList();
                        }
                        final WorkSpec.WorkInfoPojo workInfoPojo = new WorkSpec.WorkInfoPojo();
                        if (columnIndex != -1) {
                            workInfoPojo.id = query.getString(columnIndex);
                        }
                        if (columnIndex2 != -1) {
                            workInfoPojo.state = WorkTypeConverters.intToState(query.getInt(columnIndex2));
                        }
                        if (columnIndex3 != -1) {
                            workInfoPojo.output = Data.fromByteArray(query.getBlob(columnIndex3));
                        }
                        if (columnIndex4 != -1) {
                            workInfoPojo.runAttemptCount = query.getInt(columnIndex4);
                        }
                        workInfoPojo.tags = tags;
                        workInfoPojo.progress = progress;
                        list.add((Object)workInfoPojo);
                    }
                    return (List<WorkSpec.WorkInfoPojo>)list;
                }
                finally {
                    query.close();
                }
            }
        });
    }
}

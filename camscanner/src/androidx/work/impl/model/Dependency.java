// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import androidx.room.ColumnInfo;
import androidx.annotation.NonNull;
import androidx.room.Index;
import androidx.room.ForeignKey;
import androidx.room.Entity;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
@Entity(foreignKeys = { @ForeignKey(childColumns = { "work_spec_id" }, entity = WorkSpec.class, onDelete = 5, onUpdate = 5, parentColumns = { "id" }), @ForeignKey(childColumns = { "prerequisite_id" }, entity = WorkSpec.class, onDelete = 5, onUpdate = 5, parentColumns = { "id" }) }, indices = { @Index({ "work_spec_id" }), @Index({ "prerequisite_id" }) }, primaryKeys = { "work_spec_id", "prerequisite_id" })
public class Dependency
{
    @NonNull
    @ColumnInfo(name = "prerequisite_id")
    public final String prerequisiteId;
    @NonNull
    @ColumnInfo(name = "work_spec_id")
    public final String workSpecId;
    
    public Dependency(@NonNull final String workSpecId, @NonNull final String prerequisiteId) {
        this.workSpecId = workSpecId;
        this.prerequisiteId = prerequisiteId;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.model;

import androidx.room.Insert;
import androidx.room.Query;
import java.util.List;
import androidx.room.Dao;

@Dao
public interface WorkTagDao
{
    @Query("SELECT DISTINCT tag FROM worktag WHERE work_spec_id=:id")
    List<String> getTagsForWorkSpecId(final String p0);
    
    @Query("SELECT work_spec_id FROM worktag WHERE tag=:tag")
    List<String> getWorkSpecIdsWithTag(final String p0);
    
    @Insert(onConflict = 5)
    void insert(final WorkTag p0);
}

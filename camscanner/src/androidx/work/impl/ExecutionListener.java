// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public interface ExecutionListener
{
    void onExecuted(@NonNull final String p0, final boolean p1);
}

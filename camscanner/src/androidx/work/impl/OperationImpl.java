// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.lifecycle.MutableLiveData;
import androidx.work.impl.utils.futures.SettableFuture;
import androidx.annotation.RestrictTo;
import androidx.work.Operation;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class OperationImpl implements Operation
{
    private final SettableFuture<SUCCESS> mOperationFuture;
    private final MutableLiveData<State> mOperationState;
    
    public OperationImpl() {
        this.mOperationState = new MutableLiveData<State>();
        this.mOperationFuture = SettableFuture.create();
        this.setState((State)Operation.IN_PROGRESS);
    }
    
    @NonNull
    @Override
    public ListenableFuture<SUCCESS> getResult() {
        return (ListenableFuture<SUCCESS>)this.mOperationFuture;
    }
    
    @NonNull
    @Override
    public LiveData<State> getState() {
        return this.mOperationState;
    }
    
    public void setState(@NonNull final State state) {
        this.mOperationState.postValue(state);
        if (state instanceof SUCCESS) {
            this.mOperationFuture.set((SUCCESS)state);
        }
        else if (state instanceof FAILURE) {
            this.mOperationFuture.setException(((FAILURE)state).getThrowable());
        }
    }
}

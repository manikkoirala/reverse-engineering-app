// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import androidx.work.impl.utils.StopWorkRunnable;
import androidx.work.impl.utils.StartWorkRunnable;
import androidx.work.WorkerParameters;
import androidx.work.impl.background.systemjob.SystemJobScheduler;
import androidx.work.impl.utils.PruneWorkRunnable;
import androidx.work.impl.utils.RawQueries;
import androidx.work.WorkQuery;
import androidx.work.impl.utils.LiveDataUtils;
import androidx.work.impl.model.WorkSpec;
import androidx.arch.core.util.Function;
import androidx.work.impl.utils.StatusRunnable;
import androidx.work.WorkInfo;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.work.impl.utils.futures.SettableFuture;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.Collections;
import androidx.work.PeriodicWorkRequest;
import androidx.work.ExistingPeriodicWorkPolicy;
import java.util.Arrays;
import androidx.work.impl.background.greedy.GreedyScheduler;
import android.content.Intent;
import androidx.core.os.BuildCompat;
import androidx.work.impl.foreground.SystemForegroundDispatcher;
import android.app.PendingIntent;
import java.util.UUID;
import androidx.work.impl.utils.CancelWorkRunnable;
import androidx.work.Operation;
import androidx.work.WorkRequest;
import androidx.work.WorkContinuation;
import androidx.work.OneTimeWorkRequest;
import androidx.work.ExistingWorkPolicy;
import androidx.work.impl.utils.ForceStopRunnable;
import androidx.core.content.\u3007o\u3007;
import android.os.Build$VERSION;
import androidx.work.impl.utils.taskexecutor.WorkManagerTaskExecutor;
import androidx.annotation.Nullable;
import java.util.concurrent.Executor;
import androidx.work.R;
import androidx.annotation.NonNull;
import androidx.work.Logger;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import java.util.List;
import android.content.BroadcastReceiver$PendingResult;
import androidx.work.multiprocess.RemoteWorkManager;
import androidx.work.impl.utils.PreferenceUtils;
import android.content.Context;
import androidx.work.Configuration;
import androidx.annotation.RestrictTo;
import androidx.work.WorkManager;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class WorkManagerImpl extends WorkManager
{
    public static final int MAX_PRE_JOB_SCHEDULER_API_LEVEL = 22;
    public static final int MIN_JOB_SCHEDULER_API_LEVEL = 23;
    public static final String REMOTE_WORK_MANAGER_CLIENT = "androidx.work.multiprocess.RemoteWorkManagerClient";
    private static final String TAG;
    private static WorkManagerImpl sDefaultInstance;
    private static WorkManagerImpl sDelegatedInstance;
    private static final Object sLock;
    private Configuration mConfiguration;
    private Context mContext;
    private boolean mForceStopRunnableCompleted;
    private PreferenceUtils mPreferenceUtils;
    private Processor mProcessor;
    private volatile RemoteWorkManager mRemoteWorkManager;
    private BroadcastReceiver$PendingResult mRescheduleReceiverResult;
    private List<Scheduler> mSchedulers;
    private WorkDatabase mWorkDatabase;
    private TaskExecutor mWorkTaskExecutor;
    
    static {
        TAG = Logger.tagWithPrefix("WorkManagerImpl");
        WorkManagerImpl.sDelegatedInstance = null;
        WorkManagerImpl.sDefaultInstance = null;
        sLock = new Object();
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public WorkManagerImpl(@NonNull final Context context, @NonNull final Configuration configuration, @NonNull final TaskExecutor taskExecutor) {
        this(context, configuration, taskExecutor, context.getResources().getBoolean(R.bool.workmanager_test_configuration));
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public WorkManagerImpl(@NonNull final Context context, @NonNull final Configuration configuration, @NonNull final TaskExecutor taskExecutor, @NonNull final WorkDatabase workDatabase) {
        final Context applicationContext = context.getApplicationContext();
        Logger.setLogger(new Logger.LogcatLogger(configuration.getMinimumLoggingLevel()));
        final List<Scheduler> schedulers = this.createSchedulers(applicationContext, configuration, taskExecutor);
        this.internalInit(context, configuration, taskExecutor, workDatabase, schedulers, new Processor(context, configuration, taskExecutor, workDatabase, schedulers));
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public WorkManagerImpl(@NonNull final Context context, @NonNull final Configuration configuration, @NonNull final TaskExecutor taskExecutor, @NonNull final WorkDatabase workDatabase, @NonNull final List<Scheduler> list, @NonNull final Processor processor) {
        this.internalInit(context, configuration, taskExecutor, workDatabase, list, processor);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public WorkManagerImpl(@NonNull final Context context, @NonNull final Configuration configuration, @NonNull final TaskExecutor taskExecutor, final boolean b) {
        this(context, configuration, taskExecutor, WorkDatabase.create(context.getApplicationContext(), taskExecutor.getBackgroundExecutor(), b));
    }
    
    @Deprecated
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static WorkManagerImpl getInstance() {
        synchronized (WorkManagerImpl.sLock) {
            final WorkManagerImpl sDelegatedInstance = WorkManagerImpl.sDelegatedInstance;
            if (sDelegatedInstance != null) {
                return sDelegatedInstance;
            }
            return WorkManagerImpl.sDefaultInstance;
        }
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static WorkManagerImpl getInstance(@NonNull Context applicationContext) {
        synchronized (WorkManagerImpl.sLock) {
            WorkManagerImpl workManagerImpl;
            if ((workManagerImpl = getInstance()) == null) {
                applicationContext = applicationContext.getApplicationContext();
                if (!(applicationContext instanceof Configuration.Provider)) {
                    throw new IllegalStateException("WorkManager is not initialized properly.  You have explicitly disabled WorkManagerInitializer in your manifest, have not manually called WorkManager#initialize at this point, and your Application does not implement Configuration.Provider.");
                }
                initialize(applicationContext, ((Configuration.Provider)applicationContext).getWorkManagerConfiguration());
                workManagerImpl = getInstance(applicationContext);
            }
            return workManagerImpl;
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static void initialize(@NonNull final Context context, @NonNull final Configuration configuration) {
        synchronized (WorkManagerImpl.sLock) {
            final WorkManagerImpl sDelegatedInstance = WorkManagerImpl.sDelegatedInstance;
            if (sDelegatedInstance != null && WorkManagerImpl.sDefaultInstance != null) {
                throw new IllegalStateException("WorkManager is already initialized.  Did you try to initialize it manually without disabling WorkManagerInitializer? See WorkManager#initialize(Context, Configuration) or the class level Javadoc for more information.");
            }
            if (sDelegatedInstance == null) {
                final Context applicationContext = context.getApplicationContext();
                if (WorkManagerImpl.sDefaultInstance == null) {
                    WorkManagerImpl.sDefaultInstance = new WorkManagerImpl(applicationContext, configuration, new WorkManagerTaskExecutor(configuration.getTaskExecutor()));
                }
                WorkManagerImpl.sDelegatedInstance = WorkManagerImpl.sDefaultInstance;
            }
        }
    }
    
    private void internalInit(@NonNull Context applicationContext, @NonNull final Configuration mConfiguration, @NonNull final TaskExecutor mWorkTaskExecutor, @NonNull final WorkDatabase mWorkDatabase, @NonNull final List<Scheduler> mSchedulers, @NonNull final Processor mProcessor) {
        applicationContext = applicationContext.getApplicationContext();
        this.mContext = applicationContext;
        this.mConfiguration = mConfiguration;
        this.mWorkTaskExecutor = mWorkTaskExecutor;
        this.mWorkDatabase = mWorkDatabase;
        this.mSchedulers = mSchedulers;
        this.mProcessor = mProcessor;
        this.mPreferenceUtils = new PreferenceUtils(mWorkDatabase);
        this.mForceStopRunnableCompleted = false;
        if (Build$VERSION.SDK_INT >= 24 && \u3007o\u3007.\u3007080(applicationContext)) {
            throw new IllegalStateException("Cannot initialize WorkManager in direct boot mode");
        }
        this.mWorkTaskExecutor.executeOnBackgroundThread(new ForceStopRunnable(applicationContext, this));
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static void setDelegate(@Nullable final WorkManagerImpl sDelegatedInstance) {
        synchronized (WorkManagerImpl.sLock) {
            WorkManagerImpl.sDelegatedInstance = sDelegatedInstance;
        }
    }
    
    private void tryInitializeMultiProcessSupport() {
        try {
            this.mRemoteWorkManager = (RemoteWorkManager)Class.forName("androidx.work.multiprocess.RemoteWorkManagerClient").getConstructor(Context.class, WorkManagerImpl.class).newInstance(this.mContext, this);
        }
        finally {
            final Throwable t;
            Logger.get().debug(WorkManagerImpl.TAG, "Unable to initialize multi-process support", t);
        }
    }
    
    @NonNull
    @Override
    public WorkContinuation beginUniqueWork(@NonNull final String s, @NonNull final ExistingWorkPolicy existingWorkPolicy, @NonNull final List<OneTimeWorkRequest> list) {
        if (!list.isEmpty()) {
            return new WorkContinuationImpl(this, s, existingWorkPolicy, list);
        }
        throw new IllegalArgumentException("beginUniqueWork needs at least one OneTimeWorkRequest.");
    }
    
    @NonNull
    @Override
    public WorkContinuation beginWith(@NonNull final List<OneTimeWorkRequest> list) {
        if (!list.isEmpty()) {
            return new WorkContinuationImpl(this, list);
        }
        throw new IllegalArgumentException("beginWith needs at least one OneTimeWorkRequest.");
    }
    
    @NonNull
    @Override
    public Operation cancelAllWork() {
        final CancelWorkRunnable forAll = CancelWorkRunnable.forAll(this);
        this.mWorkTaskExecutor.executeOnBackgroundThread(forAll);
        return forAll.getOperation();
    }
    
    @NonNull
    @Override
    public Operation cancelAllWorkByTag(@NonNull final String s) {
        final CancelWorkRunnable forTag = CancelWorkRunnable.forTag(s, this);
        this.mWorkTaskExecutor.executeOnBackgroundThread(forTag);
        return forTag.getOperation();
    }
    
    @NonNull
    @Override
    public Operation cancelUniqueWork(@NonNull final String s) {
        final CancelWorkRunnable forName = CancelWorkRunnable.forName(s, this, true);
        this.mWorkTaskExecutor.executeOnBackgroundThread(forName);
        return forName.getOperation();
    }
    
    @NonNull
    @Override
    public Operation cancelWorkById(@NonNull final UUID uuid) {
        final CancelWorkRunnable forId = CancelWorkRunnable.forId(uuid, this);
        this.mWorkTaskExecutor.executeOnBackgroundThread(forId);
        return forId.getOperation();
    }
    
    @NonNull
    @Override
    public PendingIntent createCancelPendingIntent(@NonNull final UUID uuid) {
        final Intent cancelWorkIntent = SystemForegroundDispatcher.createCancelWorkIntent(this.mContext, uuid.toString());
        int n;
        if (BuildCompat.isAtLeastS()) {
            n = 167772160;
        }
        else {
            n = 134217728;
        }
        return PendingIntent.getService(this.mContext, 0, cancelWorkIntent, n);
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public List<Scheduler> createSchedulers(@NonNull final Context context, @NonNull final Configuration configuration, @NonNull final TaskExecutor taskExecutor) {
        return Arrays.asList(Schedulers.createBestAvailableBackgroundScheduler(context, this), new GreedyScheduler(context, configuration, taskExecutor, this));
    }
    
    @NonNull
    public WorkContinuationImpl createWorkContinuationForUniquePeriodicWork(@NonNull final String s, @NonNull final ExistingPeriodicWorkPolicy existingPeriodicWorkPolicy, @NonNull final PeriodicWorkRequest o) {
        ExistingWorkPolicy existingWorkPolicy;
        if (existingPeriodicWorkPolicy == ExistingPeriodicWorkPolicy.KEEP) {
            existingWorkPolicy = ExistingWorkPolicy.KEEP;
        }
        else {
            existingWorkPolicy = ExistingWorkPolicy.REPLACE;
        }
        return new WorkContinuationImpl(this, s, existingWorkPolicy, Collections.singletonList(o));
    }
    
    @NonNull
    @Override
    public Operation enqueue(@NonNull final List<? extends WorkRequest> list) {
        if (!list.isEmpty()) {
            return new WorkContinuationImpl(this, list).enqueue();
        }
        throw new IllegalArgumentException("enqueue needs at least one WorkRequest.");
    }
    
    @NonNull
    @Override
    public Operation enqueueUniquePeriodicWork(@NonNull final String s, @NonNull final ExistingPeriodicWorkPolicy existingPeriodicWorkPolicy, @NonNull final PeriodicWorkRequest periodicWorkRequest) {
        return this.createWorkContinuationForUniquePeriodicWork(s, existingPeriodicWorkPolicy, periodicWorkRequest).enqueue();
    }
    
    @NonNull
    @Override
    public Operation enqueueUniqueWork(@NonNull final String s, @NonNull final ExistingWorkPolicy existingWorkPolicy, @NonNull final List<OneTimeWorkRequest> list) {
        return new WorkContinuationImpl(this, s, existingWorkPolicy, list).enqueue();
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public Context getApplicationContext() {
        return this.mContext;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public Configuration getConfiguration() {
        return this.mConfiguration;
    }
    
    @NonNull
    @Override
    public ListenableFuture<Long> getLastCancelAllTimeMillis() {
        final SettableFuture<Object> create = SettableFuture.create();
        this.mWorkTaskExecutor.executeOnBackgroundThread(new Runnable(this, create, this.mPreferenceUtils) {
            final WorkManagerImpl this$0;
            final SettableFuture val$future;
            final PreferenceUtils val$preferenceUtils;
            
            @Override
            public void run() {
                try {
                    this.val$future.set(this.val$preferenceUtils.getLastCancelAllTimeMillis());
                }
                finally {
                    final Throwable exception;
                    this.val$future.setException(exception);
                }
            }
        });
        return (ListenableFuture<Long>)create;
    }
    
    @NonNull
    @Override
    public LiveData<Long> getLastCancelAllTimeMillisLiveData() {
        return this.mPreferenceUtils.getLastCancelAllTimeMillisLiveData();
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public PreferenceUtils getPreferenceUtils() {
        return this.mPreferenceUtils;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public Processor getProcessor() {
        return this.mProcessor;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public RemoteWorkManager getRemoteWorkManager() {
        if (this.mRemoteWorkManager == null) {
            synchronized (WorkManagerImpl.sLock) {
                if (this.mRemoteWorkManager == null) {
                    this.tryInitializeMultiProcessSupport();
                    if (this.mRemoteWorkManager == null) {
                        if (!TextUtils.isEmpty((CharSequence)this.mConfiguration.getDefaultProcessName())) {
                            throw new IllegalStateException("Invalid multiprocess configuration. Define an `implementation` dependency on :work:work-multiprocess library");
                        }
                    }
                }
            }
        }
        return this.mRemoteWorkManager;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public List<Scheduler> getSchedulers() {
        return this.mSchedulers;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public WorkDatabase getWorkDatabase() {
        return this.mWorkDatabase;
    }
    
    @NonNull
    @Override
    public ListenableFuture<WorkInfo> getWorkInfoById(@NonNull final UUID uuid) {
        final StatusRunnable<WorkInfo> forUUID = StatusRunnable.forUUID(this, uuid);
        this.mWorkTaskExecutor.getBackgroundExecutor().execute(forUUID);
        return forUUID.getFuture();
    }
    
    @NonNull
    @Override
    public LiveData<WorkInfo> getWorkInfoByIdLiveData(@NonNull final UUID uuid) {
        return LiveDataUtils.dedupedMappedLiveDataFor(this.mWorkDatabase.workSpecDao().getWorkStatusPojoLiveDataForIds(Collections.singletonList(uuid.toString())), (Function<List<WorkSpec.WorkInfoPojo>, WorkInfo>)new Function<List<WorkSpec.WorkInfoPojo>, WorkInfo>(this) {
            final WorkManagerImpl this$0;
            
            @Override
            public WorkInfo apply(final List<WorkSpec.WorkInfoPojo> list) {
                WorkInfo workInfo;
                if (list != null && list.size() > 0) {
                    workInfo = ((WorkSpec.WorkInfoPojo)list.get(0)).toWorkInfo();
                }
                else {
                    workInfo = null;
                }
                return workInfo;
            }
        }, this.mWorkTaskExecutor);
    }
    
    @NonNull
    @Override
    public ListenableFuture<List<WorkInfo>> getWorkInfos(@NonNull final WorkQuery workQuery) {
        final StatusRunnable<List<WorkInfo>> forWorkQuerySpec = StatusRunnable.forWorkQuerySpec(this, workQuery);
        this.mWorkTaskExecutor.getBackgroundExecutor().execute(forWorkQuerySpec);
        return forWorkQuerySpec.getFuture();
    }
    
    LiveData<List<WorkInfo>> getWorkInfosById(@NonNull final List<String> list) {
        return LiveDataUtils.dedupedMappedLiveDataFor(this.mWorkDatabase.workSpecDao().getWorkStatusPojoLiveDataForIds(list), WorkSpec.WORK_INFO_MAPPER, this.mWorkTaskExecutor);
    }
    
    @NonNull
    @Override
    public ListenableFuture<List<WorkInfo>> getWorkInfosByTag(@NonNull final String s) {
        final StatusRunnable<List<WorkInfo>> forTag = StatusRunnable.forTag(this, s);
        this.mWorkTaskExecutor.getBackgroundExecutor().execute(forTag);
        return forTag.getFuture();
    }
    
    @NonNull
    @Override
    public LiveData<List<WorkInfo>> getWorkInfosByTagLiveData(@NonNull final String s) {
        return LiveDataUtils.dedupedMappedLiveDataFor(this.mWorkDatabase.workSpecDao().getWorkStatusPojoLiveDataForTag(s), WorkSpec.WORK_INFO_MAPPER, this.mWorkTaskExecutor);
    }
    
    @NonNull
    @Override
    public ListenableFuture<List<WorkInfo>> getWorkInfosForUniqueWork(@NonNull final String s) {
        final StatusRunnable<List<WorkInfo>> forUniqueWork = StatusRunnable.forUniqueWork(this, s);
        this.mWorkTaskExecutor.getBackgroundExecutor().execute(forUniqueWork);
        return forUniqueWork.getFuture();
    }
    
    @NonNull
    @Override
    public LiveData<List<WorkInfo>> getWorkInfosForUniqueWorkLiveData(@NonNull final String s) {
        return LiveDataUtils.dedupedMappedLiveDataFor(this.mWorkDatabase.workSpecDao().getWorkStatusPojoLiveDataForName(s), WorkSpec.WORK_INFO_MAPPER, this.mWorkTaskExecutor);
    }
    
    @NonNull
    @Override
    public LiveData<List<WorkInfo>> getWorkInfosLiveData(@NonNull final WorkQuery workQuery) {
        return LiveDataUtils.dedupedMappedLiveDataFor(this.mWorkDatabase.rawWorkInfoDao().getWorkInfoPojosLiveData(RawQueries.workQueryToRawQuery(workQuery)), WorkSpec.WORK_INFO_MAPPER, this.mWorkTaskExecutor);
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public TaskExecutor getWorkTaskExecutor() {
        return this.mWorkTaskExecutor;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void onForceStopRunnableCompleted() {
        synchronized (WorkManagerImpl.sLock) {
            this.mForceStopRunnableCompleted = true;
            final BroadcastReceiver$PendingResult mRescheduleReceiverResult = this.mRescheduleReceiverResult;
            if (mRescheduleReceiverResult != null) {
                mRescheduleReceiverResult.finish();
                this.mRescheduleReceiverResult = null;
            }
        }
    }
    
    @NonNull
    @Override
    public Operation pruneWork() {
        final PruneWorkRunnable pruneWorkRunnable = new PruneWorkRunnable(this);
        this.mWorkTaskExecutor.executeOnBackgroundThread(pruneWorkRunnable);
        return pruneWorkRunnable.getOperation();
    }
    
    public void rescheduleEligibleWork() {
        if (Build$VERSION.SDK_INT >= 23) {
            SystemJobScheduler.cancelAll(this.getApplicationContext());
        }
        this.getWorkDatabase().workSpecDao().resetScheduledState();
        Schedulers.schedule(this.getConfiguration(), this.getWorkDatabase(), this.getSchedulers());
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void setReschedulePendingResult(@NonNull final BroadcastReceiver$PendingResult mRescheduleReceiverResult) {
        synchronized (WorkManagerImpl.sLock) {
            this.mRescheduleReceiverResult = mRescheduleReceiverResult;
            if (this.mForceStopRunnableCompleted) {
                mRescheduleReceiverResult.finish();
                this.mRescheduleReceiverResult = null;
            }
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void startWork(@NonNull final String s) {
        this.startWork(s, null);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void startWork(@NonNull final String s, @Nullable final WorkerParameters.RuntimeExtras runtimeExtras) {
        this.mWorkTaskExecutor.executeOnBackgroundThread(new StartWorkRunnable(this, s, runtimeExtras));
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void stopForegroundWork(@NonNull final String s) {
        this.mWorkTaskExecutor.executeOnBackgroundThread(new StopWorkRunnable(this, s, true));
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void stopWork(@NonNull final String s) {
        this.mWorkTaskExecutor.executeOnBackgroundThread(new StopWorkRunnable(this, s, false));
    }
}

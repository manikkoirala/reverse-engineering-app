// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import java.util.Set;
import androidx.work.impl.model.WorkSpec;
import androidx.annotation.NonNull;
import java.util.UUID;
import androidx.annotation.RestrictTo;
import androidx.work.WorkRequest;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class WorkRequestHolder extends WorkRequest
{
    public WorkRequestHolder(@NonNull final UUID uuid, @NonNull final WorkSpec workSpec, @NonNull final Set<String> set) {
        super(uuid, workSpec, set);
    }
}

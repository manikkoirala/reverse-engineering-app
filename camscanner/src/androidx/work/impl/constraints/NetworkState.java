// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints;

import androidx.annotation.NonNull;

public class NetworkState
{
    private boolean mIsConnected;
    private boolean mIsMetered;
    private boolean mIsNotRoaming;
    private boolean mIsValidated;
    
    public NetworkState(final boolean mIsConnected, final boolean mIsValidated, final boolean mIsMetered, final boolean mIsNotRoaming) {
        this.mIsConnected = mIsConnected;
        this.mIsValidated = mIsValidated;
        this.mIsMetered = mIsMetered;
        this.mIsNotRoaming = mIsNotRoaming;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof NetworkState)) {
            return false;
        }
        final NetworkState networkState = (NetworkState)o;
        if (this.mIsConnected != networkState.mIsConnected || this.mIsValidated != networkState.mIsValidated || this.mIsMetered != networkState.mIsMetered || this.mIsNotRoaming != networkState.mIsNotRoaming) {
            b = false;
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int mIsConnected;
        final int n = mIsConnected = (this.mIsConnected ? 1 : 0);
        if (this.mIsValidated) {
            mIsConnected = n + 16;
        }
        int n2 = mIsConnected;
        if (this.mIsMetered) {
            n2 = mIsConnected + 256;
        }
        int n3 = n2;
        if (this.mIsNotRoaming) {
            n3 = n2 + 4096;
        }
        return n3;
    }
    
    public boolean isConnected() {
        return this.mIsConnected;
    }
    
    public boolean isMetered() {
        return this.mIsMetered;
    }
    
    public boolean isNotRoaming() {
        return this.mIsNotRoaming;
    }
    
    public boolean isValidated() {
        return this.mIsValidated;
    }
    
    @NonNull
    @Override
    public String toString() {
        return String.format("[ Connected=%b Validated=%b Metered=%b NotRoaming=%b ]", this.mIsConnected, this.mIsValidated, this.mIsMetered, this.mIsNotRoaming);
    }
}

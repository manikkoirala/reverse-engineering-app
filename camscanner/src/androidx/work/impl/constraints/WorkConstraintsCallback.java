// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints;

import androidx.annotation.NonNull;
import java.util.List;

public interface WorkConstraintsCallback
{
    void onAllConstraintsMet(@NonNull final List<String> p0);
    
    void onAllConstraintsNotMet(@NonNull final List<String> p0);
}

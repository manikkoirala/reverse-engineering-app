// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints.controllers;

import androidx.annotation.NonNull;
import androidx.work.impl.model.WorkSpec;
import androidx.work.impl.constraints.trackers.ConstraintTracker;
import androidx.work.impl.constraints.trackers.Trackers;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import android.content.Context;

public class BatteryChargingController extends ConstraintController<Boolean>
{
    public BatteryChargingController(final Context context, final TaskExecutor taskExecutor) {
        super(Trackers.getInstance(context, taskExecutor).getBatteryChargingTracker());
    }
    
    @Override
    boolean hasConstraint(@NonNull final WorkSpec workSpec) {
        return workSpec.constraints.requiresCharging();
    }
    
    @Override
    boolean isConstrained(@NonNull final Boolean b) {
        return b ^ true;
    }
}

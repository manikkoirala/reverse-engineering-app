// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints.controllers;

import java.util.Iterator;
import androidx.annotation.NonNull;
import androidx.work.impl.model.WorkSpec;
import androidx.annotation.Nullable;
import java.util.ArrayList;
import androidx.work.impl.constraints.trackers.ConstraintTracker;
import java.util.List;
import androidx.work.impl.constraints.ConstraintListener;

public abstract class ConstraintController<T> implements ConstraintListener<T>
{
    private OnConstraintUpdatedCallback mCallback;
    private T mCurrentValue;
    private final List<String> mMatchingWorkSpecIds;
    private ConstraintTracker<T> mTracker;
    
    ConstraintController(final ConstraintTracker<T> mTracker) {
        this.mMatchingWorkSpecIds = new ArrayList<String>();
        this.mTracker = mTracker;
    }
    
    private void updateCallback(@Nullable final OnConstraintUpdatedCallback onConstraintUpdatedCallback, @Nullable final T t) {
        if (!this.mMatchingWorkSpecIds.isEmpty()) {
            if (onConstraintUpdatedCallback != null) {
                if (t != null && !this.isConstrained(t)) {
                    onConstraintUpdatedCallback.onConstraintMet(this.mMatchingWorkSpecIds);
                }
                else {
                    onConstraintUpdatedCallback.onConstraintNotMet(this.mMatchingWorkSpecIds);
                }
            }
        }
    }
    
    abstract boolean hasConstraint(@NonNull final WorkSpec p0);
    
    abstract boolean isConstrained(@NonNull final T p0);
    
    public boolean isWorkSpecConstrained(@NonNull final String s) {
        final T mCurrentValue = this.mCurrentValue;
        return mCurrentValue != null && this.isConstrained(mCurrentValue) && this.mMatchingWorkSpecIds.contains(s);
    }
    
    @Override
    public void onConstraintChanged(@Nullable final T mCurrentValue) {
        this.mCurrentValue = mCurrentValue;
        this.updateCallback(this.mCallback, mCurrentValue);
    }
    
    public void replace(@NonNull final Iterable<WorkSpec> iterable) {
        this.mMatchingWorkSpecIds.clear();
        for (final WorkSpec workSpec : iterable) {
            if (this.hasConstraint(workSpec)) {
                this.mMatchingWorkSpecIds.add(workSpec.id);
            }
        }
        if (this.mMatchingWorkSpecIds.isEmpty()) {
            this.mTracker.removeListener(this);
        }
        else {
            this.mTracker.addListener(this);
        }
        this.updateCallback(this.mCallback, this.mCurrentValue);
    }
    
    public void reset() {
        if (!this.mMatchingWorkSpecIds.isEmpty()) {
            this.mMatchingWorkSpecIds.clear();
            this.mTracker.removeListener(this);
        }
    }
    
    public void setCallback(@Nullable final OnConstraintUpdatedCallback mCallback) {
        if (this.mCallback != mCallback) {
            this.updateCallback(this.mCallback = mCallback, this.mCurrentValue);
        }
    }
    
    public interface OnConstraintUpdatedCallback
    {
        void onConstraintMet(@NonNull final List<String> p0);
        
        void onConstraintNotMet(@NonNull final List<String> p0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints.controllers;

import android.os.Build$VERSION;
import androidx.work.NetworkType;
import androidx.annotation.NonNull;
import androidx.work.impl.model.WorkSpec;
import androidx.work.impl.constraints.trackers.ConstraintTracker;
import androidx.work.impl.constraints.trackers.Trackers;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import android.content.Context;
import androidx.work.impl.constraints.NetworkState;

public class NetworkConnectedController extends ConstraintController<NetworkState>
{
    public NetworkConnectedController(final Context context, final TaskExecutor taskExecutor) {
        super(Trackers.getInstance(context, taskExecutor).getNetworkStateTracker());
    }
    
    @Override
    boolean hasConstraint(@NonNull final WorkSpec workSpec) {
        return workSpec.constraints.getRequiredNetworkType() == NetworkType.CONNECTED;
    }
    
    @Override
    boolean isConstrained(@NonNull final NetworkState networkState) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        final boolean b = true;
        if (sdk_INT >= 26) {
            boolean b2 = b;
            if (networkState.isConnected()) {
                b2 = (!networkState.isValidated() && b);
            }
            return b2;
        }
        return networkState.isConnected() ^ true;
    }
}

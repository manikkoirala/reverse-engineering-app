// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints.trackers;

import androidx.annotation.VisibleForTesting;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class Trackers
{
    private static Trackers sInstance;
    private BatteryChargingTracker mBatteryChargingTracker;
    private BatteryNotLowTracker mBatteryNotLowTracker;
    private NetworkStateTracker mNetworkStateTracker;
    private StorageNotLowTracker mStorageNotLowTracker;
    
    private Trackers(@NonNull Context applicationContext, @NonNull final TaskExecutor taskExecutor) {
        applicationContext = applicationContext.getApplicationContext();
        this.mBatteryChargingTracker = new BatteryChargingTracker(applicationContext, taskExecutor);
        this.mBatteryNotLowTracker = new BatteryNotLowTracker(applicationContext, taskExecutor);
        this.mNetworkStateTracker = new NetworkStateTracker(applicationContext, taskExecutor);
        this.mStorageNotLowTracker = new StorageNotLowTracker(applicationContext, taskExecutor);
    }
    
    @NonNull
    public static Trackers getInstance(final Context context, final TaskExecutor taskExecutor) {
        synchronized (Trackers.class) {
            if (Trackers.sInstance == null) {
                Trackers.sInstance = new Trackers(context, taskExecutor);
            }
            return Trackers.sInstance;
        }
    }
    
    @VisibleForTesting
    public static void setInstance(@NonNull final Trackers sInstance) {
        synchronized (Trackers.class) {
            Trackers.sInstance = sInstance;
        }
    }
    
    @NonNull
    public BatteryChargingTracker getBatteryChargingTracker() {
        return this.mBatteryChargingTracker;
    }
    
    @NonNull
    public BatteryNotLowTracker getBatteryNotLowTracker() {
        return this.mBatteryNotLowTracker;
    }
    
    @NonNull
    public NetworkStateTracker getNetworkStateTracker() {
        return this.mNetworkStateTracker;
    }
    
    @NonNull
    public StorageNotLowTracker getStorageNotLowTracker() {
        return this.mStorageNotLowTracker;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints.trackers;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.os.Build$VERSION;
import android.content.Intent;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.work.Logger;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class BatteryChargingTracker extends BroadcastReceiverConstraintTracker<Boolean>
{
    private static final String TAG;
    
    static {
        TAG = Logger.tagWithPrefix("BatteryChrgTracker");
    }
    
    public BatteryChargingTracker(@NonNull final Context context, @NonNull final TaskExecutor taskExecutor) {
        super(context, taskExecutor);
    }
    
    private boolean isBatteryChangedIntentCharging(final Intent intent) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        final boolean b = true;
        if (sdk_INT >= 23) {
            final int intExtra = intent.getIntExtra("status", -1);
            boolean b2 = b;
            if (intExtra == 2) {
                return b2;
            }
            if (intExtra == 5) {
                b2 = b;
                return b2;
            }
        }
        else if (intent.getIntExtra("plugged", 0) != 0) {
            return b;
        }
        return false;
    }
    
    @Override
    public Boolean getInitialState() {
        final Intent registerReceiver = super.mAppContext.registerReceiver((BroadcastReceiver)null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            Logger.get().error(BatteryChargingTracker.TAG, "getInitialState - null intent received", new Throwable[0]);
            return null;
        }
        return this.isBatteryChangedIntentCharging(registerReceiver);
    }
    
    @Override
    public IntentFilter getIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        if (Build$VERSION.SDK_INT >= 23) {
            intentFilter.addAction("android.os.action.CHARGING");
            intentFilter.addAction("android.os.action.DISCHARGING");
        }
        else {
            intentFilter.addAction("android.intent.action.ACTION_POWER_CONNECTED");
            intentFilter.addAction("android.intent.action.ACTION_POWER_DISCONNECTED");
        }
        return intentFilter;
    }
    
    @Override
    public void onBroadcastReceive(final Context context, @NonNull final Intent intent) {
        final String action = intent.getAction();
        if (action == null) {
            return;
        }
        final Logger value = Logger.get();
        final String tag = BatteryChargingTracker.TAG;
        int n = 1;
        value.debug(tag, String.format("Received %s", action), new Throwable[0]);
        Label_0161: {
            switch (action) {
                case "android.intent.action.ACTION_POWER_CONNECTED": {
                    n = 3;
                    break Label_0161;
                }
                case "android.os.action.CHARGING": {
                    n = 2;
                    break Label_0161;
                }
                case "android.os.action.DISCHARGING": {
                    break Label_0161;
                }
                case "android.intent.action.ACTION_POWER_DISCONNECTED": {
                    n = 0;
                    break Label_0161;
                }
                default:
                    break;
            }
            n = -1;
        }
        switch (n) {
            case 3: {
                this.setState(Boolean.TRUE);
                break;
            }
            case 2: {
                this.setState(Boolean.TRUE);
                break;
            }
            case 1: {
                this.setState(Boolean.FALSE);
                break;
            }
            case 0: {
                this.setState(Boolean.FALSE);
                break;
            }
        }
    }
}

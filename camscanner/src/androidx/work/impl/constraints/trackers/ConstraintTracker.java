// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints.trackers;

import java.util.Iterator;
import java.util.List;
import java.util.Collection;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import androidx.annotation.NonNull;
import androidx.work.Logger;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import androidx.work.impl.constraints.ConstraintListener;
import java.util.Set;
import android.content.Context;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public abstract class ConstraintTracker<T>
{
    private static final String TAG;
    protected final Context mAppContext;
    T mCurrentState;
    private final Set<ConstraintListener<T>> mListeners;
    private final Object mLock;
    protected final TaskExecutor mTaskExecutor;
    
    static {
        TAG = Logger.tagWithPrefix("ConstraintTracker");
    }
    
    ConstraintTracker(@NonNull final Context context, @NonNull final TaskExecutor mTaskExecutor) {
        this.mLock = new Object();
        this.mListeners = new LinkedHashSet<ConstraintListener<T>>();
        this.mAppContext = context.getApplicationContext();
        this.mTaskExecutor = mTaskExecutor;
    }
    
    public void addListener(final ConstraintListener<T> constraintListener) {
        synchronized (this.mLock) {
            if (this.mListeners.add(constraintListener)) {
                if (this.mListeners.size() == 1) {
                    this.mCurrentState = this.getInitialState();
                    Logger.get().debug(ConstraintTracker.TAG, String.format("%s: initial state = %s", this.getClass().getSimpleName(), this.mCurrentState), new Throwable[0]);
                    this.startTracking();
                }
                constraintListener.onConstraintChanged(this.mCurrentState);
            }
        }
    }
    
    public abstract T getInitialState();
    
    public void removeListener(final ConstraintListener<T> constraintListener) {
        synchronized (this.mLock) {
            if (this.mListeners.remove(constraintListener) && this.mListeners.isEmpty()) {
                this.stopTracking();
            }
        }
    }
    
    public void setState(final T t) {
        synchronized (this.mLock) {
            final T mCurrentState = this.mCurrentState;
            if (mCurrentState != t && (mCurrentState == null || !mCurrentState.equals(t))) {
                this.mCurrentState = t;
                this.mTaskExecutor.getMainThreadExecutor().execute(new Runnable(this, new ArrayList(this.mListeners)) {
                    final ConstraintTracker this$0;
                    final List val$listenersList;
                    
                    @Override
                    public void run() {
                        final Iterator iterator = this.val$listenersList.iterator();
                        while (iterator.hasNext()) {
                            ((ConstraintListener<T>)iterator.next()).onConstraintChanged(this.this$0.mCurrentState);
                        }
                    }
                });
            }
        }
    }
    
    public abstract void startTracking();
    
    public abstract void stopTracking();
}

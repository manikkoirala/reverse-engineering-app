// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints.trackers;

import android.net.Network;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.net.ConnectivityManager$NetworkCallback;
import androidx.annotation.VisibleForTesting;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import androidx.core.net.ConnectivityManagerCompat;
import android.os.Build$VERSION;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.work.Logger;
import androidx.annotation.RequiresApi;
import android.net.ConnectivityManager;
import androidx.annotation.RestrictTo;
import androidx.work.impl.constraints.NetworkState;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class NetworkStateTracker extends ConstraintTracker<NetworkState>
{
    static final String TAG;
    private NetworkStateBroadcastReceiver mBroadcastReceiver;
    private final ConnectivityManager mConnectivityManager;
    @RequiresApi(24)
    private NetworkStateCallback mNetworkCallback;
    
    static {
        TAG = Logger.tagWithPrefix("NetworkStateTracker");
    }
    
    public NetworkStateTracker(@NonNull final Context context, @NonNull final TaskExecutor taskExecutor) {
        super(context, taskExecutor);
        this.mConnectivityManager = (ConnectivityManager)super.mAppContext.getSystemService("connectivity");
        if (isNetworkCallbackSupported()) {
            this.mNetworkCallback = new NetworkStateCallback();
        }
        else {
            this.mBroadcastReceiver = new NetworkStateBroadcastReceiver();
        }
    }
    
    private static boolean isNetworkCallbackSupported() {
        return Build$VERSION.SDK_INT >= 24;
    }
    
    NetworkState getActiveNetworkState() {
        final NetworkInfo activeNetworkInfo = this.mConnectivityManager.getActiveNetworkInfo();
        boolean b = true;
        final boolean b2 = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        final boolean activeNetworkValidated = this.isActiveNetworkValidated();
        final boolean activeNetworkMetered = ConnectivityManagerCompat.isActiveNetworkMetered(this.mConnectivityManager);
        if (activeNetworkInfo == null || activeNetworkInfo.isRoaming()) {
            b = false;
        }
        return new NetworkState(b2, activeNetworkValidated, activeNetworkMetered, b);
    }
    
    @Override
    public NetworkState getInitialState() {
        return this.getActiveNetworkState();
    }
    
    @VisibleForTesting
    boolean isActiveNetworkValidated() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        final boolean b = false;
        if (sdk_INT < 23) {
            return false;
        }
        try {
            final NetworkCapabilities networkCapabilities = this.mConnectivityManager.getNetworkCapabilities(\u3007080.\u3007080(this.mConnectivityManager));
            boolean b2 = b;
            if (networkCapabilities != null) {
                final boolean hasCapability = networkCapabilities.hasCapability(16);
                b2 = b;
                if (hasCapability) {
                    b2 = true;
                }
            }
            return b2;
        }
        catch (final SecurityException ex) {
            Logger.get().error(NetworkStateTracker.TAG, "Unable to validate active network", ex);
            return false;
        }
    }
    
    @Override
    public void startTracking() {
        if (isNetworkCallbackSupported()) {
            try {
                Logger.get().debug(NetworkStateTracker.TAG, "Registering network callback", new Throwable[0]);
                \u3007o00\u3007\u3007Oo.\u3007080(this.mConnectivityManager, (ConnectivityManager$NetworkCallback)this.mNetworkCallback);
                return;
            }
            catch (final SecurityException ex) {}
            catch (final IllegalArgumentException ex2) {}
            final SecurityException ex;
            Logger.get().error(NetworkStateTracker.TAG, "Received exception while registering network callback", ex);
        }
        else {
            Logger.get().debug(NetworkStateTracker.TAG, "Registering broadcast receiver", new Throwable[0]);
            super.mAppContext.registerReceiver((BroadcastReceiver)this.mBroadcastReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
    }
    
    @Override
    public void stopTracking() {
        if (isNetworkCallbackSupported()) {
            try {
                Logger.get().debug(NetworkStateTracker.TAG, "Unregistering network callback", new Throwable[0]);
                this.mConnectivityManager.unregisterNetworkCallback((ConnectivityManager$NetworkCallback)this.mNetworkCallback);
                return;
            }
            catch (final SecurityException ex) {}
            catch (final IllegalArgumentException ex2) {}
            final SecurityException ex;
            Logger.get().error(NetworkStateTracker.TAG, "Received exception while unregistering network callback", ex);
        }
        else {
            Logger.get().debug(NetworkStateTracker.TAG, "Unregistering broadcast receiver", new Throwable[0]);
            super.mAppContext.unregisterReceiver((BroadcastReceiver)this.mBroadcastReceiver);
        }
    }
    
    private class NetworkStateBroadcastReceiver extends BroadcastReceiver
    {
        final NetworkStateTracker this$0;
        
        NetworkStateBroadcastReceiver(final NetworkStateTracker this$0) {
            this.this$0 = this$0;
        }
        
        public void onReceive(final Context context, final Intent intent) {
            if (intent != null) {
                if (intent.getAction() != null) {
                    if (intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                        Logger.get().debug(NetworkStateTracker.TAG, "Network broadcast received", new Throwable[0]);
                        final NetworkStateTracker this$0 = this.this$0;
                        this$0.setState(this$0.getActiveNetworkState());
                    }
                }
            }
        }
    }
    
    @RequiresApi(24)
    private class NetworkStateCallback extends ConnectivityManager$NetworkCallback
    {
        final NetworkStateTracker this$0;
        
        NetworkStateCallback(final NetworkStateTracker this$0) {
            this.this$0 = this$0;
        }
        
        public void onCapabilitiesChanged(@NonNull final Network network, @NonNull final NetworkCapabilities networkCapabilities) {
            Logger.get().debug(NetworkStateTracker.TAG, String.format("Network capabilities changed: %s", networkCapabilities), new Throwable[0]);
            final NetworkStateTracker this$0 = this.this$0;
            this$0.setState(this$0.getActiveNetworkState());
        }
        
        public void onLost(@NonNull final Network network) {
            Logger.get().debug(NetworkStateTracker.TAG, "Network connection lost", new Throwable[0]);
            final NetworkStateTracker this$0 = this.this$0;
            this$0.setState(this$0.getActiveNetworkState());
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints.trackers;

import android.content.IntentFilter;
import android.content.Intent;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.work.Logger;
import android.content.BroadcastReceiver;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public abstract class BroadcastReceiverConstraintTracker<T> extends ConstraintTracker<T>
{
    private static final String TAG;
    private final BroadcastReceiver mBroadcastReceiver;
    
    static {
        TAG = Logger.tagWithPrefix("BrdcstRcvrCnstrntTrckr");
    }
    
    public BroadcastReceiverConstraintTracker(@NonNull final Context context, @NonNull final TaskExecutor taskExecutor) {
        super(context, taskExecutor);
        this.mBroadcastReceiver = new BroadcastReceiver() {
            final BroadcastReceiverConstraintTracker this$0;
            
            public void onReceive(final Context context, final Intent intent) {
                if (intent != null) {
                    this.this$0.onBroadcastReceive(context, intent);
                }
            }
        };
    }
    
    public abstract IntentFilter getIntentFilter();
    
    public abstract void onBroadcastReceive(final Context p0, @NonNull final Intent p1);
    
    @Override
    public void startTracking() {
        Logger.get().debug(BroadcastReceiverConstraintTracker.TAG, String.format("%s: registering receiver", this.getClass().getSimpleName()), new Throwable[0]);
        super.mAppContext.registerReceiver(this.mBroadcastReceiver, this.getIntentFilter());
    }
    
    @Override
    public void stopTracking() {
        Logger.get().debug(BroadcastReceiverConstraintTracker.TAG, String.format("%s: unregistering receiver", this.getClass().getSimpleName()), new Throwable[0]);
        super.mAppContext.unregisterReceiver(this.mBroadcastReceiver);
    }
}

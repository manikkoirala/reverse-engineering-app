// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints.trackers;

import android.content.IntentFilter;
import android.content.Intent;
import android.content.BroadcastReceiver;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.work.Logger;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class StorageNotLowTracker extends BroadcastReceiverConstraintTracker<Boolean>
{
    private static final String TAG;
    
    static {
        TAG = Logger.tagWithPrefix("StorageNotLowTracker");
    }
    
    public StorageNotLowTracker(@NonNull final Context context, @NonNull final TaskExecutor taskExecutor) {
        super(context, taskExecutor);
    }
    
    @Override
    public Boolean getInitialState() {
        final Intent registerReceiver = super.mAppContext.registerReceiver((BroadcastReceiver)null, this.getIntentFilter());
        if (registerReceiver == null || registerReceiver.getAction() == null) {
            return Boolean.TRUE;
        }
        final String action = registerReceiver.getAction();
        action.hashCode();
        if (action.equals("android.intent.action.DEVICE_STORAGE_LOW")) {
            return Boolean.FALSE;
        }
        if (!action.equals("android.intent.action.DEVICE_STORAGE_OK")) {
            return null;
        }
        return Boolean.TRUE;
    }
    
    @Override
    public IntentFilter getIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.DEVICE_STORAGE_OK");
        intentFilter.addAction("android.intent.action.DEVICE_STORAGE_LOW");
        return intentFilter;
    }
    
    @Override
    public void onBroadcastReceive(final Context context, @NonNull final Intent intent) {
        if (intent.getAction() == null) {
            return;
        }
        Logger.get().debug(StorageNotLowTracker.TAG, String.format("Received %s", intent.getAction()), new Throwable[0]);
        final String action = intent.getAction();
        action.hashCode();
        if (!action.equals("android.intent.action.DEVICE_STORAGE_LOW")) {
            if (action.equals("android.intent.action.DEVICE_STORAGE_OK")) {
                this.setState(Boolean.TRUE);
            }
        }
        else {
            this.setState(Boolean.FALSE);
        }
    }
}

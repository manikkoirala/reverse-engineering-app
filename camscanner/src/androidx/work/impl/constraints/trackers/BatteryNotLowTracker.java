// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.constraints.trackers;

import android.content.Intent;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.work.Logger;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class BatteryNotLowTracker extends BroadcastReceiverConstraintTracker<Boolean>
{
    static final float BATTERY_LOW_THRESHOLD = 0.15f;
    private static final String TAG;
    
    static {
        TAG = Logger.tagWithPrefix("BatteryNotLowTracker");
    }
    
    public BatteryNotLowTracker(@NonNull final Context context, @NonNull final TaskExecutor taskExecutor) {
        super(context, taskExecutor);
    }
    
    @Override
    public Boolean getInitialState() {
        final Intent registerReceiver = super.mAppContext.registerReceiver((BroadcastReceiver)null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        boolean b = false;
        if (registerReceiver == null) {
            Logger.get().error(BatteryNotLowTracker.TAG, "getInitialState - null intent received", new Throwable[0]);
            return null;
        }
        final int intExtra = registerReceiver.getIntExtra("status", -1);
        final float n = registerReceiver.getIntExtra("level", -1) / (float)registerReceiver.getIntExtra("scale", -1);
        if (intExtra == 1 || n > 0.15f) {
            b = true;
        }
        return b;
    }
    
    @Override
    public IntentFilter getIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.BATTERY_OKAY");
        intentFilter.addAction("android.intent.action.BATTERY_LOW");
        return intentFilter;
    }
    
    @Override
    public void onBroadcastReceive(final Context context, @NonNull final Intent intent) {
        if (intent.getAction() == null) {
            return;
        }
        Logger.get().debug(BatteryNotLowTracker.TAG, String.format("Received %s", intent.getAction()), new Throwable[0]);
        final String action = intent.getAction();
        action.hashCode();
        if (!action.equals("android.intent.action.BATTERY_OKAY")) {
            if (action.equals("android.intent.action.BATTERY_LOW")) {
                this.setState(Boolean.FALSE);
            }
        }
        else {
            this.setState(Boolean.TRUE);
        }
    }
}

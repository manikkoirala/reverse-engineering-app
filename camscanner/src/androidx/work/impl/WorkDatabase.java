// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import androidx.work.impl.model.WorkTagDao;
import androidx.work.impl.model.WorkSpecDao;
import androidx.work.impl.model.WorkProgressDao;
import androidx.work.impl.model.WorkNameDao;
import androidx.work.impl.model.SystemIdInfoDao;
import androidx.work.impl.model.RawWorkInfoDao;
import androidx.work.impl.model.PreferenceDao;
import androidx.work.impl.model.DependencyDao;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.framework.FrameworkSQLiteOpenHelperFactory;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.room.Room;
import java.util.concurrent.Executor;
import androidx.annotation.NonNull;
import android.content.Context;
import java.util.concurrent.TimeUnit;
import androidx.work.impl.model.WorkTypeConverters;
import androidx.work.Data;
import androidx.room.TypeConverters;
import androidx.work.impl.model.Preference;
import androidx.work.impl.model.WorkProgress;
import androidx.work.impl.model.WorkName;
import androidx.work.impl.model.SystemIdInfo;
import androidx.work.impl.model.WorkTag;
import androidx.work.impl.model.WorkSpec;
import androidx.work.impl.model.Dependency;
import androidx.room.Database;
import androidx.annotation.RestrictTo;
import androidx.room.RoomDatabase;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
@Database(entities = { Dependency.class, WorkSpec.class, WorkTag.class, SystemIdInfo.class, WorkName.class, WorkProgress.class, Preference.class }, version = 12)
@TypeConverters({ Data.class, WorkTypeConverters.class })
public abstract class WorkDatabase extends RoomDatabase
{
    private static final String PRUNE_SQL_FORMAT_PREFIX = "DELETE FROM workspec WHERE state IN (2, 3, 5) AND (period_start_time + minimum_retention_duration) < ";
    private static final String PRUNE_SQL_FORMAT_SUFFIX = " AND (SELECT COUNT(*)=0 FROM dependency WHERE     prerequisite_id=id AND     work_spec_id NOT IN         (SELECT id FROM workspec WHERE state IN (2, 3, 5)))";
    private static final long PRUNE_THRESHOLD_MILLIS;
    
    static {
        PRUNE_THRESHOLD_MILLIS = TimeUnit.DAYS.toMillis(1L);
    }
    
    @NonNull
    public static WorkDatabase create(@NonNull final Context context, @NonNull final Executor queryExecutor, final boolean b) {
        Object o;
        if (b) {
            o = Room.inMemoryDatabaseBuilder(context, WorkDatabase.class).allowMainThreadQueries();
        }
        else {
            o = Room.databaseBuilder(context, WorkDatabase.class, WorkDatabasePathHelper.getWorkDatabaseName());
            ((Builder)o).openHelperFactory(new SupportSQLiteOpenHelper.Factory(context) {
                final Context val$context;
                
                @NonNull
                @Override
                public SupportSQLiteOpenHelper create(@NonNull final Configuration configuration) {
                    final SupportSQLiteOpenHelper.Configuration.Builder builder = SupportSQLiteOpenHelper.Configuration.builder(this.val$context);
                    builder.name(configuration.name).callback(configuration.callback).noBackupDirectory(true);
                    return new FrameworkSQLiteOpenHelperFactory().create(builder.build());
                }
            });
        }
        return (WorkDatabase)((Builder)o).setQueryExecutor(queryExecutor).addCallback(generateCleanupCallback()).addMigrations(WorkDatabaseMigrations.MIGRATION_1_2).addMigrations(new WorkDatabaseMigrations.RescheduleMigration(context, 2, 3)).addMigrations(WorkDatabaseMigrations.MIGRATION_3_4).addMigrations(WorkDatabaseMigrations.MIGRATION_4_5).addMigrations(new WorkDatabaseMigrations.RescheduleMigration(context, 5, 6)).addMigrations(WorkDatabaseMigrations.MIGRATION_6_7).addMigrations(WorkDatabaseMigrations.MIGRATION_7_8).addMigrations(WorkDatabaseMigrations.MIGRATION_8_9).addMigrations(new WorkDatabaseMigrations.WorkMigration9To10(context)).addMigrations(new WorkDatabaseMigrations.RescheduleMigration(context, 10, 11)).addMigrations(WorkDatabaseMigrations.MIGRATION_11_12).fallbackToDestructiveMigration().build();
    }
    
    static Callback generateCleanupCallback() {
        return new Callback() {
            @Override
            public void onOpen(@NonNull final SupportSQLiteDatabase supportSQLiteDatabase) {
                super.onOpen(supportSQLiteDatabase);
                supportSQLiteDatabase.beginTransaction();
                try {
                    supportSQLiteDatabase.execSQL(WorkDatabase.getPruneSQL());
                    supportSQLiteDatabase.setTransactionSuccessful();
                }
                finally {
                    supportSQLiteDatabase.endTransaction();
                }
            }
        };
    }
    
    static long getPruneDate() {
        return System.currentTimeMillis() - WorkDatabase.PRUNE_THRESHOLD_MILLIS;
    }
    
    @NonNull
    static String getPruneSQL() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM workspec WHERE state IN (2, 3, 5) AND (period_start_time + minimum_retention_duration) < ");
        sb.append(getPruneDate());
        sb.append(" AND (SELECT COUNT(*)=0 FROM dependency WHERE     prerequisite_id=id AND     work_spec_id NOT IN         (SELECT id FROM workspec WHERE state IN (2, 3, 5)))");
        return sb.toString();
    }
    
    @NonNull
    public abstract DependencyDao dependencyDao();
    
    @NonNull
    public abstract PreferenceDao preferenceDao();
    
    @NonNull
    public abstract RawWorkInfoDao rawWorkInfoDao();
    
    @NonNull
    public abstract SystemIdInfoDao systemIdInfoDao();
    
    @NonNull
    public abstract WorkNameDao workNameDao();
    
    @NonNull
    public abstract WorkProgressDao workProgressDao();
    
    @NonNull
    public abstract WorkSpecDao workSpecDao();
    
    @NonNull
    public abstract WorkTagDao workTagDao();
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.foreground;

import java.util.List;
import androidx.work.impl.WorkDatabase;
import java.util.Iterator;
import android.os.Build$VERSION;
import android.app.Notification;
import androidx.annotation.MainThread;
import java.util.UUID;
import android.text.TextUtils;
import android.os.Parcelable;
import android.net.Uri;
import android.content.Intent;
import androidx.annotation.VisibleForTesting;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import androidx.annotation.NonNull;
import androidx.work.Logger;
import androidx.work.impl.WorkManagerImpl;
import androidx.work.impl.model.WorkSpec;
import java.util.Set;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import androidx.work.ForegroundInfo;
import java.util.Map;
import android.content.Context;
import androidx.work.impl.constraints.WorkConstraintsTracker;
import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
import androidx.work.impl.ExecutionListener;
import androidx.work.impl.constraints.WorkConstraintsCallback;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class SystemForegroundDispatcher implements WorkConstraintsCallback, ExecutionListener
{
    private static final String ACTION_CANCEL_WORK = "ACTION_CANCEL_WORK";
    private static final String ACTION_NOTIFY = "ACTION_NOTIFY";
    private static final String ACTION_START_FOREGROUND = "ACTION_START_FOREGROUND";
    private static final String ACTION_STOP_FOREGROUND = "ACTION_STOP_FOREGROUND";
    private static final String KEY_FOREGROUND_SERVICE_TYPE = "KEY_FOREGROUND_SERVICE_TYPE";
    private static final String KEY_NOTIFICATION = "KEY_NOTIFICATION";
    private static final String KEY_NOTIFICATION_ID = "KEY_NOTIFICATION_ID";
    private static final String KEY_WORKSPEC_ID = "KEY_WORKSPEC_ID";
    static final String TAG;
    @Nullable
    private Callback mCallback;
    final WorkConstraintsTracker mConstraintsTracker;
    private Context mContext;
    String mCurrentForegroundWorkSpecId;
    final Map<String, ForegroundInfo> mForegroundInfoById;
    final Object mLock;
    private final TaskExecutor mTaskExecutor;
    final Set<WorkSpec> mTrackedWorkSpecs;
    private WorkManagerImpl mWorkManagerImpl;
    final Map<String, WorkSpec> mWorkSpecById;
    
    static {
        TAG = Logger.tagWithPrefix("SystemFgDispatcher");
    }
    
    SystemForegroundDispatcher(@NonNull final Context mContext) {
        this.mContext = mContext;
        this.mLock = new Object();
        final WorkManagerImpl instance = WorkManagerImpl.getInstance(mContext);
        this.mWorkManagerImpl = instance;
        final TaskExecutor workTaskExecutor = instance.getWorkTaskExecutor();
        this.mTaskExecutor = workTaskExecutor;
        this.mCurrentForegroundWorkSpecId = null;
        this.mForegroundInfoById = new LinkedHashMap<String, ForegroundInfo>();
        this.mTrackedWorkSpecs = new HashSet<WorkSpec>();
        this.mWorkSpecById = new HashMap<String, WorkSpec>();
        this.mConstraintsTracker = new WorkConstraintsTracker(this.mContext, workTaskExecutor, this);
        this.mWorkManagerImpl.getProcessor().addExecutionListener(this);
    }
    
    @VisibleForTesting
    SystemForegroundDispatcher(@NonNull final Context mContext, @NonNull final WorkManagerImpl mWorkManagerImpl, @NonNull final WorkConstraintsTracker mConstraintsTracker) {
        this.mContext = mContext;
        this.mLock = new Object();
        this.mWorkManagerImpl = mWorkManagerImpl;
        this.mTaskExecutor = mWorkManagerImpl.getWorkTaskExecutor();
        this.mCurrentForegroundWorkSpecId = null;
        this.mForegroundInfoById = new LinkedHashMap<String, ForegroundInfo>();
        this.mTrackedWorkSpecs = new HashSet<WorkSpec>();
        this.mWorkSpecById = new HashMap<String, WorkSpec>();
        this.mConstraintsTracker = mConstraintsTracker;
        this.mWorkManagerImpl.getProcessor().addExecutionListener(this);
    }
    
    @NonNull
    public static Intent createCancelWorkIntent(@NonNull final Context context, @NonNull final String s) {
        final Intent intent = new Intent(context, (Class)SystemForegroundService.class);
        intent.setAction("ACTION_CANCEL_WORK");
        intent.setData(Uri.parse(String.format("workspec://%s", s)));
        intent.putExtra("KEY_WORKSPEC_ID", s);
        return intent;
    }
    
    @NonNull
    public static Intent createNotifyIntent(@NonNull final Context context, @NonNull final String s, @NonNull final ForegroundInfo foregroundInfo) {
        final Intent intent = new Intent(context, (Class)SystemForegroundService.class);
        intent.setAction("ACTION_NOTIFY");
        intent.putExtra("KEY_NOTIFICATION_ID", foregroundInfo.getNotificationId());
        intent.putExtra("KEY_FOREGROUND_SERVICE_TYPE", foregroundInfo.getForegroundServiceType());
        intent.putExtra("KEY_NOTIFICATION", (Parcelable)foregroundInfo.getNotification());
        intent.putExtra("KEY_WORKSPEC_ID", s);
        return intent;
    }
    
    @NonNull
    public static Intent createStartForegroundIntent(@NonNull final Context context, @NonNull final String s, @NonNull final ForegroundInfo foregroundInfo) {
        final Intent intent = new Intent(context, (Class)SystemForegroundService.class);
        intent.setAction("ACTION_START_FOREGROUND");
        intent.putExtra("KEY_WORKSPEC_ID", s);
        intent.putExtra("KEY_NOTIFICATION_ID", foregroundInfo.getNotificationId());
        intent.putExtra("KEY_FOREGROUND_SERVICE_TYPE", foregroundInfo.getForegroundServiceType());
        intent.putExtra("KEY_NOTIFICATION", (Parcelable)foregroundInfo.getNotification());
        intent.putExtra("KEY_WORKSPEC_ID", s);
        return intent;
    }
    
    @NonNull
    public static Intent createStopForegroundIntent(@NonNull final Context context) {
        final Intent intent = new Intent(context, (Class)SystemForegroundService.class);
        intent.setAction("ACTION_STOP_FOREGROUND");
        return intent;
    }
    
    @MainThread
    private void handleCancelWork(@NonNull final Intent intent) {
        Logger.get().info(SystemForegroundDispatcher.TAG, String.format("Stopping foreground work for %s", intent), new Throwable[0]);
        final String stringExtra = intent.getStringExtra("KEY_WORKSPEC_ID");
        if (stringExtra != null && !TextUtils.isEmpty((CharSequence)stringExtra)) {
            this.mWorkManagerImpl.cancelWorkById(UUID.fromString(stringExtra));
        }
    }
    
    @MainThread
    private void handleNotify(@NonNull final Intent intent) {
        int n = 0;
        final int intExtra = intent.getIntExtra("KEY_NOTIFICATION_ID", 0);
        final int intExtra2 = intent.getIntExtra("KEY_FOREGROUND_SERVICE_TYPE", 0);
        final String stringExtra = intent.getStringExtra("KEY_WORKSPEC_ID");
        final Notification notification = (Notification)intent.getParcelableExtra("KEY_NOTIFICATION");
        Logger.get().debug(SystemForegroundDispatcher.TAG, String.format("Notifying with (id: %s, workSpecId: %s, notificationType: %s)", intExtra, stringExtra, intExtra2), new Throwable[0]);
        if (notification != null && this.mCallback != null) {
            this.mForegroundInfoById.put(stringExtra, new ForegroundInfo(intExtra, notification, intExtra2));
            if (TextUtils.isEmpty((CharSequence)this.mCurrentForegroundWorkSpecId)) {
                this.mCurrentForegroundWorkSpecId = stringExtra;
                this.mCallback.startForeground(intExtra, intExtra2, notification);
            }
            else {
                this.mCallback.notify(intExtra, notification);
                if (intExtra2 != 0 && Build$VERSION.SDK_INT >= 29) {
                    final Iterator<Map.Entry<String, ForegroundInfo>> iterator = this.mForegroundInfoById.entrySet().iterator();
                    while (iterator.hasNext()) {
                        n |= ((Map.Entry<K, ForegroundInfo>)iterator.next()).getValue().getForegroundServiceType();
                    }
                    final ForegroundInfo foregroundInfo = this.mForegroundInfoById.get(this.mCurrentForegroundWorkSpecId);
                    if (foregroundInfo != null) {
                        this.mCallback.startForeground(foregroundInfo.getNotificationId(), n, foregroundInfo.getNotification());
                    }
                }
            }
        }
    }
    
    @MainThread
    private void handleStartForeground(@NonNull final Intent intent) {
        Logger.get().info(SystemForegroundDispatcher.TAG, String.format("Started foreground service %s", intent), new Throwable[0]);
        this.mTaskExecutor.executeOnBackgroundThread(new Runnable(this, this.mWorkManagerImpl.getWorkDatabase(), intent.getStringExtra("KEY_WORKSPEC_ID")) {
            final SystemForegroundDispatcher this$0;
            final WorkDatabase val$database;
            final String val$workSpecId;
            
            @Override
            public void run() {
                final WorkSpec workSpec = this.val$database.workSpecDao().getWorkSpec(this.val$workSpecId);
                if (workSpec != null && workSpec.hasConstraints()) {
                    synchronized (this.this$0.mLock) {
                        this.this$0.mWorkSpecById.put(this.val$workSpecId, workSpec);
                        this.this$0.mTrackedWorkSpecs.add(workSpec);
                        final SystemForegroundDispatcher this$0 = this.this$0;
                        this$0.mConstraintsTracker.replace(this$0.mTrackedWorkSpecs);
                    }
                }
            }
        });
    }
    
    WorkManagerImpl getWorkManager() {
        return this.mWorkManagerImpl;
    }
    
    @MainThread
    void handleStop(@NonNull final Intent intent) {
        Logger.get().info(SystemForegroundDispatcher.TAG, "Stopping foreground service", new Throwable[0]);
        final Callback mCallback = this.mCallback;
        if (mCallback != null) {
            mCallback.stop();
        }
    }
    
    @Override
    public void onAllConstraintsMet(@NonNull final List<String> list) {
    }
    
    @Override
    public void onAllConstraintsNotMet(@NonNull final List<String> list) {
        if (!list.isEmpty()) {
            for (final String s : list) {
                Logger.get().debug(SystemForegroundDispatcher.TAG, String.format("Constraints unmet for WorkSpec %s", s), new Throwable[0]);
                this.mWorkManagerImpl.stopForegroundWork(s);
            }
        }
    }
    
    @MainThread
    void onDestroy() {
        this.mCallback = null;
        synchronized (this.mLock) {
            this.mConstraintsTracker.reset();
            monitorexit(this.mLock);
            this.mWorkManagerImpl.getProcessor().removeExecutionListener(this);
        }
    }
    
    @MainThread
    @Override
    public void onExecuted(@NonNull final String s, final boolean b) {
        Object o = this.mLock;
        synchronized (o) {
            final WorkSpec workSpec = this.mWorkSpecById.remove(s);
            if (workSpec != null && this.mTrackedWorkSpecs.remove(workSpec)) {
                this.mConstraintsTracker.replace(this.mTrackedWorkSpecs);
            }
            monitorexit(o);
            final ForegroundInfo foregroundInfo = this.mForegroundInfoById.remove(s);
            if (s.equals(this.mCurrentForegroundWorkSpecId) && this.mForegroundInfoById.size() > 0) {
                final Iterator<Map.Entry<String, ForegroundInfo>> iterator = this.mForegroundInfoById.entrySet().iterator();
                o = iterator.next();
                while (iterator.hasNext()) {
                    o = iterator.next();
                }
                this.mCurrentForegroundWorkSpecId = ((Map.Entry<String, ForegroundInfo>)o).getKey();
                if (this.mCallback != null) {
                    o = ((Map.Entry<String, ForegroundInfo>)o).getValue();
                    this.mCallback.startForeground(((ForegroundInfo)o).getNotificationId(), ((ForegroundInfo)o).getForegroundServiceType(), ((ForegroundInfo)o).getNotification());
                    this.mCallback.cancelNotification(((ForegroundInfo)o).getNotificationId());
                }
            }
            o = this.mCallback;
            if (foregroundInfo != null && o != null) {
                Logger.get().debug(SystemForegroundDispatcher.TAG, String.format("Removing Notification (id: %s, workSpecId: %s ,notificationType: %s)", foregroundInfo.getNotificationId(), s, foregroundInfo.getForegroundServiceType()), new Throwable[0]);
                ((Callback)o).cancelNotification(foregroundInfo.getNotificationId());
            }
        }
    }
    
    void onStartCommand(@NonNull final Intent intent) {
        final String action = intent.getAction();
        if ("ACTION_START_FOREGROUND".equals(action)) {
            this.handleStartForeground(intent);
            this.handleNotify(intent);
        }
        else if ("ACTION_NOTIFY".equals(action)) {
            this.handleNotify(intent);
        }
        else if ("ACTION_CANCEL_WORK".equals(action)) {
            this.handleCancelWork(intent);
        }
        else if ("ACTION_STOP_FOREGROUND".equals(action)) {
            this.handleStop(intent);
        }
    }
    
    @MainThread
    void setCallback(@NonNull final Callback mCallback) {
        if (this.mCallback != null) {
            Logger.get().error(SystemForegroundDispatcher.TAG, "A callback already exists.", new Throwable[0]);
            return;
        }
        this.mCallback = mCallback;
    }
    
    interface Callback
    {
        void cancelNotification(final int p0);
        
        void notify(final int p0, @NonNull final Notification p1);
        
        void startForeground(final int p0, final int p1, @NonNull final Notification p2);
        
        void stop();
    }
}

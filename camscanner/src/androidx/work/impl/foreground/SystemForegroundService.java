// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.foreground;

import android.content.Context;
import android.os.Build$VERSION;
import android.content.Intent;
import androidx.annotation.NonNull;
import android.app.Notification;
import androidx.annotation.MainThread;
import android.os.Looper;
import androidx.work.Logger;
import android.app.NotificationManager;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
import androidx.lifecycle.LifecycleService;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class SystemForegroundService extends LifecycleService implements Callback
{
    private static final String TAG;
    @Nullable
    private static SystemForegroundService sForegroundService;
    SystemForegroundDispatcher mDispatcher;
    private Handler mHandler;
    private boolean mIsShutdown;
    NotificationManager mNotificationManager;
    
    static {
        TAG = Logger.tagWithPrefix("SystemFgService");
        SystemForegroundService.sForegroundService = null;
    }
    
    @Nullable
    public static SystemForegroundService getInstance() {
        return SystemForegroundService.sForegroundService;
    }
    
    @MainThread
    private void initializeDispatcher() {
        this.mHandler = new Handler(Looper.getMainLooper());
        this.mNotificationManager = (NotificationManager)((Context)this).getApplicationContext().getSystemService("notification");
        (this.mDispatcher = new SystemForegroundDispatcher(((Context)this).getApplicationContext())).setCallback((SystemForegroundDispatcher.Callback)this);
    }
    
    @Override
    public void cancelNotification(final int n) {
        this.mHandler.post((Runnable)new Runnable(this, n) {
            final SystemForegroundService this$0;
            final int val$notificationId;
            
            @Override
            public void run() {
                this.this$0.mNotificationManager.cancel(this.val$notificationId);
            }
        });
    }
    
    @Override
    public void notify(final int n, @NonNull final Notification notification) {
        this.mHandler.post((Runnable)new Runnable(this, n, notification) {
            final SystemForegroundService this$0;
            final Notification val$notification;
            final int val$notificationId;
            
            @Override
            public void run() {
                this.this$0.mNotificationManager.notify(this.val$notificationId, this.val$notification);
            }
        });
    }
    
    @Override
    public void onCreate() {
        super.onCreate();
        (SystemForegroundService.sForegroundService = this).initializeDispatcher();
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mDispatcher.onDestroy();
    }
    
    @Override
    public int onStartCommand(@Nullable final Intent intent, final int n, final int n2) {
        super.onStartCommand(intent, n, n2);
        if (this.mIsShutdown) {
            Logger.get().info(SystemForegroundService.TAG, "Re-initializing SystemForegroundService after a request to shut-down.", new Throwable[0]);
            this.mDispatcher.onDestroy();
            this.initializeDispatcher();
            this.mIsShutdown = false;
        }
        if (intent != null) {
            this.mDispatcher.onStartCommand(intent);
        }
        return 3;
    }
    
    @Override
    public void startForeground(final int n, final int n2, @NonNull final Notification notification) {
        this.mHandler.post((Runnable)new Runnable(this, n, notification, n2) {
            final SystemForegroundService this$0;
            final Notification val$notification;
            final int val$notificationId;
            final int val$notificationType;
            
            @Override
            public void run() {
                if (Build$VERSION.SDK_INT >= 29) {
                    this.this$0.startForeground(this.val$notificationId, this.val$notification, this.val$notificationType);
                }
                else {
                    this.this$0.startForeground(this.val$notificationId, this.val$notification);
                }
            }
        });
    }
    
    @MainThread
    @Override
    public void stop() {
        this.mIsShutdown = true;
        Logger.get().debug(SystemForegroundService.TAG, "All commands completed.", new Throwable[0]);
        if (Build$VERSION.SDK_INT >= 26) {
            this.stopForeground(true);
        }
        SystemForegroundService.sForegroundService = null;
        this.stopSelf();
    }
}

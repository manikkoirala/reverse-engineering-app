// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.foreground;

import androidx.work.ForegroundInfo;
import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public interface ForegroundProcessor
{
    void startForeground(@NonNull final String p0, @NonNull final ForegroundInfo p1);
    
    void stopForeground(@NonNull final String p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import androidx.work.impl.model.WorkSpec;
import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public interface Scheduler
{
    public static final int MAX_GREEDY_SCHEDULER_LIMIT = 200;
    public static final int MAX_SCHEDULER_LIMIT = 50;
    
    void cancel(@NonNull final String p0);
    
    boolean hasLimitedSchedulingSlots();
    
    void schedule(@NonNull final WorkSpec... p0);
}

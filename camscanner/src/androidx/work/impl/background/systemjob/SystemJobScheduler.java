// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemjob;

import androidx.work.impl.WorkDatabase;
import androidx.room.RoomDatabase;
import android.os.BaseBundle;
import androidx.work.OutOfQuotaPolicy;
import androidx.work.impl.model.WorkSpec;
import androidx.work.impl.model.WorkSpecDao;
import android.text.TextUtils;
import java.util.HashSet;
import android.os.PersistableBundle;
import android.content.ComponentName;
import androidx.annotation.Nullable;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Iterator;
import java.util.List;
import android.app.job.JobInfo;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.NonNull;
import androidx.work.Logger;
import androidx.work.impl.WorkManagerImpl;
import android.app.job.JobScheduler;
import android.content.Context;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;
import androidx.work.impl.Scheduler;

@RequiresApi(23)
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class SystemJobScheduler implements Scheduler
{
    private static final String TAG;
    private final Context mContext;
    private final JobScheduler mJobScheduler;
    private final SystemJobInfoConverter mSystemJobInfoConverter;
    private final WorkManagerImpl mWorkManager;
    
    static {
        TAG = Logger.tagWithPrefix("SystemJobScheduler");
    }
    
    public SystemJobScheduler(@NonNull final Context context, @NonNull final WorkManagerImpl workManagerImpl) {
        this(context, workManagerImpl, (JobScheduler)context.getSystemService("jobscheduler"), new SystemJobInfoConverter(context));
    }
    
    @VisibleForTesting
    public SystemJobScheduler(final Context mContext, final WorkManagerImpl mWorkManager, final JobScheduler mJobScheduler, final SystemJobInfoConverter mSystemJobInfoConverter) {
        this.mContext = mContext;
        this.mWorkManager = mWorkManager;
        this.mJobScheduler = mJobScheduler;
        this.mSystemJobInfoConverter = mSystemJobInfoConverter;
    }
    
    public static void cancelAll(@NonNull final Context context) {
        final JobScheduler jobScheduler = (JobScheduler)context.getSystemService("jobscheduler");
        if (jobScheduler != null) {
            final List<JobInfo> pendingJobs = getPendingJobs(context, jobScheduler);
            if (pendingJobs != null && !pendingJobs.isEmpty()) {
                final Iterator iterator = pendingJobs.iterator();
                while (iterator.hasNext()) {
                    cancelJobById(jobScheduler, ((JobInfo)iterator.next()).getId());
                }
            }
        }
    }
    
    private static void cancelJobById(@NonNull final JobScheduler jobScheduler, final int i) {
        try {
            jobScheduler.cancel(i);
        }
        finally {
            final Throwable t;
            Logger.get().error(SystemJobScheduler.TAG, String.format(Locale.getDefault(), "Exception while trying to cancel job (%d)", i), t);
        }
    }
    
    @Nullable
    private static List<Integer> getPendingJobIds(@NonNull final Context context, @NonNull final JobScheduler jobScheduler, @NonNull final String s) {
        final List<JobInfo> pendingJobs = getPendingJobs(context, jobScheduler);
        if (pendingJobs == null) {
            return null;
        }
        final ArrayList list = new ArrayList(2);
        for (final JobInfo jobInfo : pendingJobs) {
            if (s.equals(getWorkSpecIdFromJobInfo(jobInfo))) {
                list.add(jobInfo.getId());
            }
        }
        return list;
    }
    
    @Nullable
    private static List<JobInfo> getPendingJobs(@NonNull final Context context, @NonNull final JobScheduler jobScheduler) {
        List list = null;
        try {
            jobScheduler.getAllPendingJobs();
        }
        finally {
            final Throwable t;
            Logger.get().error(SystemJobScheduler.TAG, "getAllPendingJobs() is not reliable on this device.", t);
            list = null;
        }
        if (list == null) {
            return null;
        }
        final ArrayList list2 = new ArrayList(list.size());
        final ComponentName componentName = new ComponentName(context, (Class)SystemJobService.class);
        for (final JobInfo jobInfo : list) {
            if (componentName.equals((Object)jobInfo.getService())) {
                list2.add((Object)jobInfo);
            }
        }
        return (List<JobInfo>)list2;
    }
    
    @Nullable
    private static String getWorkSpecIdFromJobInfo(@NonNull final JobInfo jobInfo) {
        final PersistableBundle extras = jobInfo.getExtras();
        Label_0027: {
            if (extras == null) {
                break Label_0027;
            }
            try {
                if (((BaseBundle)extras).containsKey("EXTRA_WORK_SPEC_ID")) {
                    return ((BaseBundle)extras).getString("EXTRA_WORK_SPEC_ID");
                }
                return null;
            }
            catch (final NullPointerException ex) {
                return null;
            }
        }
    }
    
    public static boolean reconcileJobs(@NonNull final Context context, @NonNull WorkManagerImpl workDatabase) {
        final JobScheduler jobScheduler = (JobScheduler)context.getSystemService("jobscheduler");
        final List<JobInfo> pendingJobs = getPendingJobs(context, jobScheduler);
        final List<String> workSpecIds = workDatabase.getWorkDatabase().systemIdInfoDao().getWorkSpecIds();
        final boolean b = false;
        int size;
        if (pendingJobs != null) {
            size = pendingJobs.size();
        }
        else {
            size = 0;
        }
        final HashSet set = new HashSet(size);
        if (pendingJobs != null && !pendingJobs.isEmpty()) {
            for (final JobInfo jobInfo : pendingJobs) {
                final String workSpecIdFromJobInfo = getWorkSpecIdFromJobInfo(jobInfo);
                if (!TextUtils.isEmpty((CharSequence)workSpecIdFromJobInfo)) {
                    set.add((Object)workSpecIdFromJobInfo);
                }
                else {
                    cancelJobById(jobScheduler, jobInfo.getId());
                }
            }
        }
        final Iterator<String> iterator2 = workSpecIds.iterator();
        while (true) {
            do {
                final boolean b2 = b;
                if (iterator2.hasNext()) {
                    continue;
                }
                if (b2) {
                    workDatabase = (WorkManagerImpl)workDatabase.getWorkDatabase();
                    ((RoomDatabase)workDatabase).beginTransaction();
                    try {
                        final WorkSpecDao workSpecDao = ((WorkDatabase)workDatabase).workSpecDao();
                        final Iterator<String> iterator3 = workSpecIds.iterator();
                        while (iterator3.hasNext()) {
                            workSpecDao.markWorkSpecScheduled(iterator3.next(), -1L);
                        }
                        ((RoomDatabase)workDatabase).setTransactionSuccessful();
                    }
                    finally {
                        ((RoomDatabase)workDatabase).endTransaction();
                    }
                }
                return b2;
            } while (set.contains(iterator2.next()));
            Logger.get().debug(SystemJobScheduler.TAG, "Reconciling jobs", new Throwable[0]);
            final boolean b2 = true;
            continue;
        }
    }
    
    @Override
    public void cancel(@NonNull final String s) {
        final List<Integer> pendingJobIds = getPendingJobIds(this.mContext, this.mJobScheduler, s);
        if (pendingJobIds != null && !pendingJobIds.isEmpty()) {
            final Iterator iterator = pendingJobIds.iterator();
            while (iterator.hasNext()) {
                cancelJobById(this.mJobScheduler, (int)iterator.next());
            }
            this.mWorkManager.getWorkDatabase().systemIdInfoDao().removeSystemIdInfo(s);
        }
    }
    
    @Override
    public boolean hasLimitedSchedulingSlots() {
        return true;
    }
    
    @Override
    public void schedule(@NonNull final WorkSpec... p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        androidx/work/impl/background/systemjob/SystemJobScheduler.mWorkManager:Landroidx/work/impl/WorkManagerImpl;
        //     4: invokevirtual   androidx/work/impl/WorkManagerImpl.getWorkDatabase:()Landroidx/work/impl/WorkDatabase;
        //     7: astore          5
        //     9: new             Landroidx/work/impl/utils/IdGenerator;
        //    12: dup            
        //    13: aload           5
        //    15: invokespecial   androidx/work/impl/utils/IdGenerator.<init>:(Landroidx/work/impl/WorkDatabase;)V
        //    18: astore          7
        //    20: aload_1        
        //    21: arraylength    
        //    22: istore          4
        //    24: iconst_0       
        //    25: istore_2       
        //    26: iload_2        
        //    27: iload           4
        //    29: if_icmpge       464
        //    32: aload_1        
        //    33: iload_2        
        //    34: aaload         
        //    35: astore          6
        //    37: aload           5
        //    39: invokevirtual   androidx/room/RoomDatabase.beginTransaction:()V
        //    42: aload           5
        //    44: invokevirtual   androidx/work/impl/WorkDatabase.workSpecDao:()Landroidx/work/impl/model/WorkSpecDao;
        //    47: aload           6
        //    49: getfield        androidx/work/impl/model/WorkSpec.id:Ljava/lang/String;
        //    52: invokeinterface androidx/work/impl/model/WorkSpecDao.getWorkSpec:(Ljava/lang/String;)Landroidx/work/impl/model/WorkSpec;
        //    57: astore          8
        //    59: aload           8
        //    61: ifnonnull       137
        //    64: invokestatic    androidx/work/Logger.get:()Landroidx/work/Logger;
        //    67: astore          8
        //    69: getstatic       androidx/work/impl/background/systemjob/SystemJobScheduler.TAG:Ljava/lang/String;
        //    72: astore          10
        //    74: new             Ljava/lang/StringBuilder;
        //    77: astore          9
        //    79: aload           9
        //    81: invokespecial   java/lang/StringBuilder.<init>:()V
        //    84: aload           9
        //    86: ldc_w           "Skipping scheduling "
        //    89: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    92: pop            
        //    93: aload           9
        //    95: aload           6
        //    97: getfield        androidx/work/impl/model/WorkSpec.id:Ljava/lang/String;
        //   100: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   103: pop            
        //   104: aload           9
        //   106: ldc_w           " because it's no longer in the DB"
        //   109: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   112: pop            
        //   113: aload           8
        //   115: aload           10
        //   117: aload           9
        //   119: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   122: iconst_0       
        //   123: anewarray       Ljava/lang/Throwable;
        //   126: invokevirtual   androidx/work/Logger.warning:(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Throwable;)V
        //   129: aload           5
        //   131: invokevirtual   androidx/room/RoomDatabase.setTransactionSuccessful:()V
        //   134: goto            445
        //   137: aload           8
        //   139: getfield        androidx/work/impl/model/WorkSpec.state:Landroidx/work/WorkInfo$State;
        //   142: getstatic       androidx/work/WorkInfo$State.ENQUEUED:Landroidx/work/WorkInfo$State;
        //   145: if_acmpeq       221
        //   148: invokestatic    androidx/work/Logger.get:()Landroidx/work/Logger;
        //   151: astore          8
        //   153: getstatic       androidx/work/impl/background/systemjob/SystemJobScheduler.TAG:Ljava/lang/String;
        //   156: astore          9
        //   158: new             Ljava/lang/StringBuilder;
        //   161: astore          10
        //   163: aload           10
        //   165: invokespecial   java/lang/StringBuilder.<init>:()V
        //   168: aload           10
        //   170: ldc_w           "Skipping scheduling "
        //   173: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   176: pop            
        //   177: aload           10
        //   179: aload           6
        //   181: getfield        androidx/work/impl/model/WorkSpec.id:Ljava/lang/String;
        //   184: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   187: pop            
        //   188: aload           10
        //   190: ldc_w           " because it is no longer enqueued"
        //   193: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   196: pop            
        //   197: aload           8
        //   199: aload           9
        //   201: aload           10
        //   203: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   206: iconst_0       
        //   207: anewarray       Ljava/lang/Throwable;
        //   210: invokevirtual   androidx/work/Logger.warning:(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Throwable;)V
        //   213: aload           5
        //   215: invokevirtual   androidx/room/RoomDatabase.setTransactionSuccessful:()V
        //   218: goto            445
        //   221: aload           5
        //   223: invokevirtual   androidx/work/impl/WorkDatabase.systemIdInfoDao:()Landroidx/work/impl/model/SystemIdInfoDao;
        //   226: aload           6
        //   228: getfield        androidx/work/impl/model/WorkSpec.id:Ljava/lang/String;
        //   231: invokeinterface androidx/work/impl/model/SystemIdInfoDao.getSystemIdInfo:(Ljava/lang/String;)Landroidx/work/impl/model/SystemIdInfo;
        //   236: astore          8
        //   238: aload           8
        //   240: ifnull          252
        //   243: aload           8
        //   245: getfield        androidx/work/impl/model/SystemIdInfo.systemId:I
        //   248: istore_3       
        //   249: goto            278
        //   252: aload           7
        //   254: aload_0        
        //   255: getfield        androidx/work/impl/background/systemjob/SystemJobScheduler.mWorkManager:Landroidx/work/impl/WorkManagerImpl;
        //   258: invokevirtual   androidx/work/impl/WorkManagerImpl.getConfiguration:()Landroidx/work/Configuration;
        //   261: invokevirtual   androidx/work/Configuration.getMinJobSchedulerId:()I
        //   264: aload_0        
        //   265: getfield        androidx/work/impl/background/systemjob/SystemJobScheduler.mWorkManager:Landroidx/work/impl/WorkManagerImpl;
        //   268: invokevirtual   androidx/work/impl/WorkManagerImpl.getConfiguration:()Landroidx/work/Configuration;
        //   271: invokevirtual   androidx/work/Configuration.getMaxJobSchedulerId:()I
        //   274: invokevirtual   androidx/work/impl/utils/IdGenerator.nextJobSchedulerIdWithRange:(II)I
        //   277: istore_3       
        //   278: aload           8
        //   280: ifnonnull       316
        //   283: new             Landroidx/work/impl/model/SystemIdInfo;
        //   286: astore          8
        //   288: aload           8
        //   290: aload           6
        //   292: getfield        androidx/work/impl/model/WorkSpec.id:Ljava/lang/String;
        //   295: iload_3        
        //   296: invokespecial   androidx/work/impl/model/SystemIdInfo.<init>:(Ljava/lang/String;I)V
        //   299: aload_0        
        //   300: getfield        androidx/work/impl/background/systemjob/SystemJobScheduler.mWorkManager:Landroidx/work/impl/WorkManagerImpl;
        //   303: invokevirtual   androidx/work/impl/WorkManagerImpl.getWorkDatabase:()Landroidx/work/impl/WorkDatabase;
        //   306: invokevirtual   androidx/work/impl/WorkDatabase.systemIdInfoDao:()Landroidx/work/impl/model/SystemIdInfoDao;
        //   309: aload           8
        //   311: invokeinterface androidx/work/impl/model/SystemIdInfoDao.insertSystemIdInfo:(Landroidx/work/impl/model/SystemIdInfo;)V
        //   316: aload_0        
        //   317: aload           6
        //   319: iload_3        
        //   320: invokevirtual   androidx/work/impl/background/systemjob/SystemJobScheduler.scheduleInternal:(Landroidx/work/impl/model/WorkSpec;I)V
        //   323: getstatic       android/os/Build$VERSION.SDK_INT:I
        //   326: bipush          23
        //   328: if_icmpne       440
        //   331: aload_0        
        //   332: getfield        androidx/work/impl/background/systemjob/SystemJobScheduler.mContext:Landroid/content/Context;
        //   335: aload_0        
        //   336: getfield        androidx/work/impl/background/systemjob/SystemJobScheduler.mJobScheduler:Landroid/app/job/JobScheduler;
        //   339: aload           6
        //   341: getfield        androidx/work/impl/model/WorkSpec.id:Ljava/lang/String;
        //   344: invokestatic    androidx/work/impl/background/systemjob/SystemJobScheduler.getPendingJobIds:(Landroid/content/Context;Landroid/app/job/JobScheduler;Ljava/lang/String;)Ljava/util/List;
        //   347: astore          8
        //   349: aload           8
        //   351: ifnull          440
        //   354: aload           8
        //   356: iload_3        
        //   357: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //   360: invokeinterface java/util/List.indexOf:(Ljava/lang/Object;)I
        //   365: istore_3       
        //   366: iload_3        
        //   367: iflt            379
        //   370: aload           8
        //   372: iload_3        
        //   373: invokeinterface java/util/List.remove:(I)Ljava/lang/Object;
        //   378: pop            
        //   379: aload           8
        //   381: invokeinterface java/util/List.isEmpty:()Z
        //   386: ifne            407
        //   389: aload           8
        //   391: iconst_0       
        //   392: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   397: checkcast       Ljava/lang/Integer;
        //   400: invokevirtual   java/lang/Integer.intValue:()I
        //   403: istore_3       
        //   404: goto            433
        //   407: aload           7
        //   409: aload_0        
        //   410: getfield        androidx/work/impl/background/systemjob/SystemJobScheduler.mWorkManager:Landroidx/work/impl/WorkManagerImpl;
        //   413: invokevirtual   androidx/work/impl/WorkManagerImpl.getConfiguration:()Landroidx/work/Configuration;
        //   416: invokevirtual   androidx/work/Configuration.getMinJobSchedulerId:()I
        //   419: aload_0        
        //   420: getfield        androidx/work/impl/background/systemjob/SystemJobScheduler.mWorkManager:Landroidx/work/impl/WorkManagerImpl;
        //   423: invokevirtual   androidx/work/impl/WorkManagerImpl.getConfiguration:()Landroidx/work/Configuration;
        //   426: invokevirtual   androidx/work/Configuration.getMaxJobSchedulerId:()I
        //   429: invokevirtual   androidx/work/impl/utils/IdGenerator.nextJobSchedulerIdWithRange:(II)I
        //   432: istore_3       
        //   433: aload_0        
        //   434: aload           6
        //   436: iload_3        
        //   437: invokevirtual   androidx/work/impl/background/systemjob/SystemJobScheduler.scheduleInternal:(Landroidx/work/impl/model/WorkSpec;I)V
        //   440: aload           5
        //   442: invokevirtual   androidx/room/RoomDatabase.setTransactionSuccessful:()V
        //   445: aload           5
        //   447: invokevirtual   androidx/room/RoomDatabase.endTransaction:()V
        //   450: iinc            2, 1
        //   453: goto            26
        //   456: astore_1       
        //   457: aload           5
        //   459: invokevirtual   androidx/room/RoomDatabase.endTransaction:()V
        //   462: aload_1        
        //   463: athrow         
        //   464: return         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  42     59     456    464    Any
        //  64     134    456    464    Any
        //  137    218    456    464    Any
        //  221    238    456    464    Any
        //  243    249    456    464    Any
        //  252    278    456    464    Any
        //  283    316    456    464    Any
        //  316    349    456    464    Any
        //  354    366    456    464    Any
        //  370    379    456    464    Any
        //  379    404    456    464    Any
        //  407    433    456    464    Any
        //  433    440    456    464    Any
        //  440    445    456    464    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException: Cannot invoke "com.strobel.assembler.metadata.TypeReference.getSimpleType()" because "type" is null
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:837)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2086)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @VisibleForTesting
    public void scheduleInternal(final WorkSpec cause, int size) {
        final JobInfo convert = this.mSystemJobInfoConverter.convert((WorkSpec)cause, size);
        final Logger value = Logger.get();
        final String tag = SystemJobScheduler.TAG;
        value.debug(tag, String.format("Scheduling work ID %s Job ID %s", ((WorkSpec)cause).id, size), new Throwable[0]);
        try {
            if (this.mJobScheduler.schedule(convert) != 0) {
                goto Label_0189;
            }
            Logger.get().warning(tag, String.format("Unable to schedule work ID %s", ((WorkSpec)cause).id), new Throwable[0]);
            if (((WorkSpec)cause).expedited && ((WorkSpec)cause).outOfQuotaPolicy == OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST) {
                ((WorkSpec)cause).expedited = false;
                Logger.get().debug(tag, String.format("Scheduling a non-expedited job (work ID %s)", ((WorkSpec)cause).id), new Throwable[0]);
                this.scheduleInternal((WorkSpec)cause, size);
                goto Label_0189;
            }
            goto Label_0189;
        }
        catch (final IllegalStateException cause) {
            final List<JobInfo> pendingJobs = getPendingJobs(this.mContext, this.mJobScheduler);
            if (pendingJobs != null) {
                size = pendingJobs.size();
            }
            else {
                size = 0;
            }
            final String format = String.format(Locale.getDefault(), "JobScheduler 100 job limit exceeded.  We count %d WorkManager jobs in JobScheduler; we have %d tracked jobs in our DB; our Configuration limit is %d.", size, this.mWorkManager.getWorkDatabase().workSpecDao().getScheduledWork().size(), this.mWorkManager.getConfiguration().getMaxSchedulerLimit());
            Logger.get().error(SystemJobScheduler.TAG, format, new Throwable[0]);
            throw new IllegalStateException(format, cause);
        }
        finally {
            final Throwable t;
            Logger.get().error(SystemJobScheduler.TAG, String.format("Unable to schedule %s", cause), t);
        }
    }
}

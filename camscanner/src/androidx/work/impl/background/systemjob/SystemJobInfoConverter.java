// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemjob;

import android.os.BaseBundle;
import java.util.Iterator;
import androidx.work.Constraints;
import androidx.core.os.BuildCompat;
import androidx.work.BackoffPolicy;
import androidx.core.app.o08O;
import android.os.PersistableBundle;
import android.app.job.JobInfo;
import androidx.work.impl.model.WorkSpec;
import android.net.NetworkRequest$Builder;
import android.app.job.JobInfo$Builder;
import android.os.Build$VERSION;
import androidx.work.NetworkType;
import android.app.job.JobInfo$TriggerContentUri;
import androidx.work.ContentUriTriggers;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.work.Logger;
import android.content.ComponentName;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;

@RequiresApi(api = 23)
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
class SystemJobInfoConverter
{
    static final String EXTRA_IS_PERIODIC = "EXTRA_IS_PERIODIC";
    static final String EXTRA_WORK_SPEC_ID = "EXTRA_WORK_SPEC_ID";
    private static final String TAG;
    private final ComponentName mWorkServiceComponent;
    
    static {
        TAG = Logger.tagWithPrefix("SystemJobInfoConverter");
    }
    
    @VisibleForTesting(otherwise = 3)
    SystemJobInfoConverter(@NonNull final Context context) {
        this.mWorkServiceComponent = new ComponentName(context.getApplicationContext(), (Class)SystemJobService.class);
    }
    
    @RequiresApi(24)
    private static JobInfo$TriggerContentUri convertContentUriTrigger(final ContentUriTriggers.Trigger trigger) {
        return new JobInfo$TriggerContentUri(trigger.getUri(), (int)(trigger.shouldTriggerForDescendants() ? 1 : 0));
    }
    
    static int convertNetworkType(final NetworkType networkType) {
        final int n = SystemJobInfoConverter$1.$SwitchMap$androidx$work$NetworkType[networkType.ordinal()];
        if (n == 1) {
            return 0;
        }
        if (n == 2) {
            return 1;
        }
        if (n != 3) {
            if (n != 4) {
                if (n == 5) {
                    if (Build$VERSION.SDK_INT >= 26) {
                        return 4;
                    }
                }
            }
            else if (Build$VERSION.SDK_INT >= 24) {
                return 3;
            }
            Logger.get().debug(SystemJobInfoConverter.TAG, String.format("API version too low. Cannot convert network type value %s", networkType), new Throwable[0]);
            return 1;
        }
        return 2;
    }
    
    static void setRequiredNetwork(@NonNull final JobInfo$Builder jobInfo$Builder, @NonNull final NetworkType networkType) {
        if (Build$VERSION.SDK_INT >= 30 && networkType == NetworkType.TEMPORARILY_UNMETERED) {
            \u3007080.\u3007080(jobInfo$Builder, new NetworkRequest$Builder().addCapability(25).build());
        }
        else {
            jobInfo$Builder.setRequiredNetworkType(convertNetworkType(networkType));
        }
    }
    
    JobInfo convert(final WorkSpec workSpec, int sdk_INT) {
        final Constraints constraints = workSpec.constraints;
        final PersistableBundle extras = new PersistableBundle();
        ((BaseBundle)extras).putString("EXTRA_WORK_SPEC_ID", workSpec.id);
        o08O.\u3007080(extras, "EXTRA_IS_PERIODIC", workSpec.isPeriodic());
        final JobInfo$Builder setExtras = new JobInfo$Builder(sdk_INT, this.mWorkServiceComponent).setRequiresCharging(constraints.requiresCharging()).setRequiresDeviceIdle(constraints.requiresDeviceIdle()).setExtras(extras);
        setRequiredNetwork(setExtras, constraints.getRequiredNetworkType());
        final boolean requiresDeviceIdle = constraints.requiresDeviceIdle();
        final int n = 0;
        if (!requiresDeviceIdle) {
            if (workSpec.backoffPolicy == BackoffPolicy.LINEAR) {
                sdk_INT = 0;
            }
            else {
                sdk_INT = 1;
            }
            setExtras.setBackoffCriteria(workSpec.backoffDelayDuration, sdk_INT);
        }
        final long max = Math.max(workSpec.calculateNextRunTime() - System.currentTimeMillis(), 0L);
        sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT <= 28) {
            setExtras.setMinimumLatency(max);
        }
        else if (max > 0L) {
            setExtras.setMinimumLatency(max);
        }
        else if (!workSpec.expedited) {
            \u3007o00\u3007\u3007Oo.\u3007080(setExtras, true);
        }
        if (sdk_INT >= 24 && constraints.hasContentUriTriggers()) {
            final Iterator<ContentUriTriggers.Trigger> iterator = constraints.getContentUriTriggers().getTriggers().iterator();
            while (iterator.hasNext()) {
                \u3007o\u3007.\u3007080(setExtras, convertContentUriTrigger(iterator.next()));
            }
            O8.\u3007080(setExtras, constraints.getTriggerContentUpdateDelay());
            Oo08.\u3007080(setExtras, constraints.getTriggerMaxContentDelay());
        }
        setExtras.setPersisted(false);
        if (Build$VERSION.SDK_INT >= 26) {
            o\u30070.\u3007080(setExtras, constraints.requiresBatteryNotLow());
            \u3007\u3007888.\u3007080(setExtras, constraints.requiresStorageNotLow());
        }
        sdk_INT = n;
        if (workSpec.runAttemptCount > 0) {
            sdk_INT = 1;
        }
        if (BuildCompat.isAtLeastS() && workSpec.expedited && sdk_INT == 0) {
            oO80.\u3007080(setExtras, true);
        }
        return setExtras.build();
    }
}

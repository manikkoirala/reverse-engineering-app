// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemjob;

import android.app.Service;
import android.content.Context;
import android.os.BaseBundle;
import java.util.Arrays;
import androidx.work.WorkerParameters;
import android.os.Build$VERSION;
import android.text.TextUtils;
import android.app.Application;
import androidx.annotation.Nullable;
import android.os.PersistableBundle;
import androidx.annotation.NonNull;
import java.util.HashMap;
import androidx.work.Logger;
import androidx.work.impl.WorkManagerImpl;
import android.app.job.JobParameters;
import java.util.Map;
import androidx.annotation.RestrictTo;
import androidx.annotation.RequiresApi;
import androidx.work.impl.ExecutionListener;
import android.app.job.JobService;

@RequiresApi(23)
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class SystemJobService extends JobService implements ExecutionListener
{
    private static final String TAG;
    private final Map<String, JobParameters> mJobParameters;
    private WorkManagerImpl mWorkManagerImpl;
    
    static {
        TAG = Logger.tagWithPrefix("SystemJobService");
    }
    
    public SystemJobService() {
        this.mJobParameters = new HashMap<String, JobParameters>();
    }
    
    @Nullable
    private static String getWorkSpecIdFromJobParameters(@NonNull final JobParameters jobParameters) {
        try {
            final PersistableBundle extras = jobParameters.getExtras();
            if (extras != null && ((BaseBundle)extras).containsKey("EXTRA_WORK_SPEC_ID")) {
                return ((BaseBundle)extras).getString("EXTRA_WORK_SPEC_ID");
            }
            return null;
        }
        catch (final NullPointerException ex) {
            return null;
        }
    }
    
    public void onCreate() {
        super.onCreate();
        try {
            final WorkManagerImpl instance = WorkManagerImpl.getInstance(((Context)this).getApplicationContext());
            this.mWorkManagerImpl = instance;
            instance.getProcessor().addExecutionListener(this);
        }
        catch (final IllegalStateException ex) {
            if (!Application.class.equals(((Service)this).getApplication().getClass())) {
                throw new IllegalStateException("WorkManager needs to be initialized via a ContentProvider#onCreate() or an Application#onCreate().");
            }
            Logger.get().warning(SystemJobService.TAG, "Could not find WorkManager instance; this may be because an auto-backup is in progress. Ignoring JobScheduler commands for now. Please make sure that you are initializing WorkManager if you have manually disabled WorkManagerInitializer.", new Throwable[0]);
        }
    }
    
    public void onDestroy() {
        super.onDestroy();
        final WorkManagerImpl mWorkManagerImpl = this.mWorkManagerImpl;
        if (mWorkManagerImpl != null) {
            mWorkManagerImpl.getProcessor().removeExecutionListener(this);
        }
    }
    
    public void onExecuted(@NonNull final String s, final boolean b) {
        Logger.get().debug(SystemJobService.TAG, String.format("%s executed on JobScheduler", s), new Throwable[0]);
        synchronized (this.mJobParameters) {
            final JobParameters jobParameters = this.mJobParameters.remove(s);
            monitorexit(this.mJobParameters);
            if (jobParameters != null) {
                this.jobFinished(jobParameters, b);
            }
        }
    }
    
    public boolean onStartJob(@NonNull final JobParameters jobParameters) {
        if (this.mWorkManagerImpl == null) {
            Logger.get().debug(SystemJobService.TAG, "WorkManager is not initialized; requesting retry.", new Throwable[0]);
            this.jobFinished(jobParameters, true);
            return false;
        }
        final String workSpecIdFromJobParameters = getWorkSpecIdFromJobParameters(jobParameters);
        if (TextUtils.isEmpty((CharSequence)workSpecIdFromJobParameters)) {
            Logger.get().error(SystemJobService.TAG, "WorkSpec id not found!", new Throwable[0]);
            return false;
        }
        Object mJobParameters = this.mJobParameters;
        synchronized (mJobParameters) {
            if (this.mJobParameters.containsKey(workSpecIdFromJobParameters)) {
                Logger.get().debug(SystemJobService.TAG, String.format("Job is already being executed by SystemJobService: %s", workSpecIdFromJobParameters), new Throwable[0]);
                return false;
            }
            Logger.get().debug(SystemJobService.TAG, String.format("onStartJob for %s", workSpecIdFromJobParameters), new Throwable[0]);
            this.mJobParameters.put(workSpecIdFromJobParameters, jobParameters);
            monitorexit(mJobParameters);
            final int sdk_INT = Build$VERSION.SDK_INT;
            if (sdk_INT >= 24) {
                final WorkerParameters.RuntimeExtras runtimeExtras = new WorkerParameters.RuntimeExtras();
                if (\u300780\u3007808\u3007O.\u3007080(jobParameters) != null) {
                    runtimeExtras.triggeredContentUris = Arrays.asList(\u300780\u3007808\u3007O.\u3007080(jobParameters));
                }
                if (OO0o\u3007\u3007\u3007\u30070.\u3007080(jobParameters) != null) {
                    runtimeExtras.triggeredContentAuthorities = Arrays.asList(OO0o\u3007\u3007\u3007\u30070.\u3007080(jobParameters));
                }
                mJobParameters = runtimeExtras;
                if (sdk_INT >= 28) {
                    runtimeExtras.network = \u30078o8o\u3007.\u3007080(jobParameters);
                    mJobParameters = runtimeExtras;
                }
            }
            else {
                mJobParameters = null;
            }
            this.mWorkManagerImpl.startWork(workSpecIdFromJobParameters, (WorkerParameters.RuntimeExtras)mJobParameters);
            return true;
        }
    }
    
    public boolean onStopJob(@NonNull final JobParameters jobParameters) {
        if (this.mWorkManagerImpl == null) {
            Logger.get().debug(SystemJobService.TAG, "WorkManager is not initialized; requesting retry.", new Throwable[0]);
            return true;
        }
        final String workSpecIdFromJobParameters = getWorkSpecIdFromJobParameters(jobParameters);
        if (TextUtils.isEmpty((CharSequence)workSpecIdFromJobParameters)) {
            Logger.get().error(SystemJobService.TAG, "WorkSpec id not found!", new Throwable[0]);
            return false;
        }
        Logger.get().debug(SystemJobService.TAG, String.format("onStopJob for %s", workSpecIdFromJobParameters), new Throwable[0]);
        synchronized (this.mJobParameters) {
            this.mJobParameters.remove(workSpecIdFromJobParameters);
            monitorexit(this.mJobParameters);
            this.mWorkManagerImpl.stopWork(workSpecIdFromJobParameters);
            return this.mWorkManagerImpl.getProcessor().isCancelled(workSpecIdFromJobParameters) ^ true;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.greedy;

import java.util.Collection;
import android.text.TextUtils;
import android.os.Build$VERSION;
import androidx.work.WorkInfo;
import java.util.List;
import java.util.Iterator;
import androidx.work.impl.utils.ProcessUtils;
import androidx.annotation.VisibleForTesting;
import java.util.HashSet;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import androidx.work.Configuration;
import androidx.annotation.NonNull;
import androidx.work.Logger;
import androidx.work.impl.WorkManagerImpl;
import androidx.work.impl.constraints.WorkConstraintsTracker;
import android.content.Context;
import androidx.work.impl.model.WorkSpec;
import java.util.Set;
import androidx.annotation.RestrictTo;
import androidx.work.impl.ExecutionListener;
import androidx.work.impl.constraints.WorkConstraintsCallback;
import androidx.work.impl.Scheduler;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class GreedyScheduler implements Scheduler, WorkConstraintsCallback, ExecutionListener
{
    private static final String TAG;
    private final Set<WorkSpec> mConstrainedWorkSpecs;
    private final Context mContext;
    private DelayedWorkTracker mDelayedWorkTracker;
    Boolean mInDefaultProcess;
    private final Object mLock;
    private boolean mRegisteredExecutionListener;
    private final WorkConstraintsTracker mWorkConstraintsTracker;
    private final WorkManagerImpl mWorkManagerImpl;
    
    static {
        TAG = Logger.tagWithPrefix("GreedyScheduler");
    }
    
    public GreedyScheduler(@NonNull final Context mContext, @NonNull final Configuration configuration, @NonNull final TaskExecutor taskExecutor, @NonNull final WorkManagerImpl mWorkManagerImpl) {
        this.mConstrainedWorkSpecs = new HashSet<WorkSpec>();
        this.mContext = mContext;
        this.mWorkManagerImpl = mWorkManagerImpl;
        this.mWorkConstraintsTracker = new WorkConstraintsTracker(mContext, taskExecutor, this);
        this.mDelayedWorkTracker = new DelayedWorkTracker(this, configuration.getRunnableScheduler());
        this.mLock = new Object();
    }
    
    @VisibleForTesting
    public GreedyScheduler(@NonNull final Context mContext, @NonNull final WorkManagerImpl mWorkManagerImpl, @NonNull final WorkConstraintsTracker mWorkConstraintsTracker) {
        this.mConstrainedWorkSpecs = new HashSet<WorkSpec>();
        this.mContext = mContext;
        this.mWorkManagerImpl = mWorkManagerImpl;
        this.mWorkConstraintsTracker = mWorkConstraintsTracker;
        this.mLock = new Object();
    }
    
    private void checkDefaultProcess() {
        this.mInDefaultProcess = ProcessUtils.isDefaultProcess(this.mContext, this.mWorkManagerImpl.getConfiguration());
    }
    
    private void registerExecutionListenerIfNeeded() {
        if (!this.mRegisteredExecutionListener) {
            this.mWorkManagerImpl.getProcessor().addExecutionListener(this);
            this.mRegisteredExecutionListener = true;
        }
    }
    
    private void removeConstraintTrackingFor(@NonNull final String anObject) {
        synchronized (this.mLock) {
            for (final WorkSpec workSpec : this.mConstrainedWorkSpecs) {
                if (workSpec.id.equals(anObject)) {
                    Logger.get().debug(GreedyScheduler.TAG, String.format("Stopping tracking for %s", anObject), new Throwable[0]);
                    this.mConstrainedWorkSpecs.remove(workSpec);
                    this.mWorkConstraintsTracker.replace(this.mConstrainedWorkSpecs);
                    break;
                }
            }
        }
    }
    
    @Override
    public void cancel(@NonNull final String s) {
        if (this.mInDefaultProcess == null) {
            this.checkDefaultProcess();
        }
        if (!this.mInDefaultProcess) {
            Logger.get().info(GreedyScheduler.TAG, "Ignoring schedule request in non-main process", new Throwable[0]);
            return;
        }
        this.registerExecutionListenerIfNeeded();
        Logger.get().debug(GreedyScheduler.TAG, String.format("Cancelling work ID %s", s), new Throwable[0]);
        final DelayedWorkTracker mDelayedWorkTracker = this.mDelayedWorkTracker;
        if (mDelayedWorkTracker != null) {
            mDelayedWorkTracker.unschedule(s);
        }
        this.mWorkManagerImpl.stopWork(s);
    }
    
    @Override
    public boolean hasLimitedSchedulingSlots() {
        return false;
    }
    
    @Override
    public void onAllConstraintsMet(@NonNull final List<String> list) {
        for (final String s : list) {
            Logger.get().debug(GreedyScheduler.TAG, String.format("Constraints met: Scheduling work ID %s", s), new Throwable[0]);
            this.mWorkManagerImpl.startWork(s);
        }
    }
    
    @Override
    public void onAllConstraintsNotMet(@NonNull final List<String> list) {
        for (final String s : list) {
            Logger.get().debug(GreedyScheduler.TAG, String.format("Constraints not met: Cancelling work ID %s", s), new Throwable[0]);
            this.mWorkManagerImpl.stopWork(s);
        }
    }
    
    @Override
    public void onExecuted(@NonNull final String s, final boolean b) {
        this.removeConstraintTrackingFor(s);
    }
    
    @Override
    public void schedule(@NonNull final WorkSpec... array) {
        if (this.mInDefaultProcess == null) {
            this.checkDefaultProcess();
        }
        if (!this.mInDefaultProcess) {
            Logger.get().info(GreedyScheduler.TAG, "Ignoring schedule request in a secondary process", new Throwable[0]);
            return;
        }
        this.registerExecutionListenerIfNeeded();
        final HashSet set = new HashSet();
        final HashSet set2 = new HashSet();
        for (final WorkSpec workSpec : array) {
            final long calculateNextRunTime = workSpec.calculateNextRunTime();
            final long currentTimeMillis = System.currentTimeMillis();
            if (workSpec.state == WorkInfo.State.ENQUEUED) {
                if (currentTimeMillis < calculateNextRunTime) {
                    final DelayedWorkTracker mDelayedWorkTracker = this.mDelayedWorkTracker;
                    if (mDelayedWorkTracker != null) {
                        mDelayedWorkTracker.schedule(workSpec);
                    }
                }
                else if (workSpec.hasConstraints()) {
                    final int sdk_INT = Build$VERSION.SDK_INT;
                    if (sdk_INT >= 23 && workSpec.constraints.requiresDeviceIdle()) {
                        Logger.get().debug(GreedyScheduler.TAG, String.format("Ignoring WorkSpec %s, Requires device idle.", workSpec), new Throwable[0]);
                    }
                    else if (sdk_INT >= 24 && workSpec.constraints.hasContentUriTriggers()) {
                        Logger.get().debug(GreedyScheduler.TAG, String.format("Ignoring WorkSpec %s, Requires ContentUri triggers.", workSpec), new Throwable[0]);
                    }
                    else {
                        set.add(workSpec);
                        set2.add(workSpec.id);
                    }
                }
                else {
                    Logger.get().debug(GreedyScheduler.TAG, String.format("Starting work for %s", workSpec.id), new Throwable[0]);
                    this.mWorkManagerImpl.startWork(workSpec.id);
                }
            }
        }
        synchronized (this.mLock) {
            if (!set.isEmpty()) {
                Logger.get().debug(GreedyScheduler.TAG, String.format("Starting tracking for [%s]", TextUtils.join((CharSequence)",", (Iterable)set2)), new Throwable[0]);
                this.mConstrainedWorkSpecs.addAll(set);
                this.mWorkConstraintsTracker.replace(this.mConstrainedWorkSpecs);
            }
        }
    }
    
    @VisibleForTesting
    public void setDelayedWorkTracker(@NonNull final DelayedWorkTracker mDelayedWorkTracker) {
        this.mDelayedWorkTracker = mDelayedWorkTracker;
    }
}

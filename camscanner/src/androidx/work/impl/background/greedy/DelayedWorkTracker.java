// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.greedy;

import androidx.work.impl.model.WorkSpec;
import java.util.HashMap;
import androidx.annotation.NonNull;
import androidx.work.Logger;
import java.util.Map;
import androidx.work.RunnableScheduler;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class DelayedWorkTracker
{
    static final String TAG;
    final GreedyScheduler mGreedyScheduler;
    private final RunnableScheduler mRunnableScheduler;
    private final Map<String, Runnable> mRunnables;
    
    static {
        TAG = Logger.tagWithPrefix("DelayedWorkTracker");
    }
    
    public DelayedWorkTracker(@NonNull final GreedyScheduler mGreedyScheduler, @NonNull final RunnableScheduler mRunnableScheduler) {
        this.mGreedyScheduler = mGreedyScheduler;
        this.mRunnableScheduler = mRunnableScheduler;
        this.mRunnables = new HashMap<String, Runnable>();
    }
    
    public void schedule(@NonNull final WorkSpec workSpec) {
        final Runnable runnable = this.mRunnables.remove(workSpec.id);
        if (runnable != null) {
            this.mRunnableScheduler.cancel(runnable);
        }
        final Runnable runnable2 = new Runnable(this, workSpec) {
            final DelayedWorkTracker this$0;
            final WorkSpec val$workSpec;
            
            @Override
            public void run() {
                Logger.get().debug(DelayedWorkTracker.TAG, String.format("Scheduling work %s", this.val$workSpec.id), new Throwable[0]);
                this.this$0.mGreedyScheduler.schedule(this.val$workSpec);
            }
        };
        this.mRunnables.put(workSpec.id, runnable2);
        this.mRunnableScheduler.scheduleWithDelay(workSpec.calculateNextRunTime() - System.currentTimeMillis(), runnable2);
    }
    
    public void unschedule(@NonNull final String s) {
        final Runnable runnable = this.mRunnables.remove(s);
        if (runnable != null) {
            this.mRunnableScheduler.cancel(runnable);
        }
    }
}

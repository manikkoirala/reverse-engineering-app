// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemalarm;

import androidx.work.impl.WorkDatabase;
import androidx.room.RoomDatabase;
import android.os.BaseBundle;
import androidx.annotation.WorkerThread;
import androidx.annotation.Nullable;
import androidx.work.impl.model.WorkSpec;
import android.os.Bundle;
import android.content.Intent;
import java.util.HashMap;
import androidx.annotation.NonNull;
import androidx.work.Logger;
import java.util.Map;
import android.content.Context;
import androidx.annotation.RestrictTo;
import androidx.work.impl.ExecutionListener;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class CommandHandler implements ExecutionListener
{
    static final String ACTION_CONSTRAINTS_CHANGED = "ACTION_CONSTRAINTS_CHANGED";
    static final String ACTION_DELAY_MET = "ACTION_DELAY_MET";
    static final String ACTION_EXECUTION_COMPLETED = "ACTION_EXECUTION_COMPLETED";
    static final String ACTION_RESCHEDULE = "ACTION_RESCHEDULE";
    static final String ACTION_SCHEDULE_WORK = "ACTION_SCHEDULE_WORK";
    static final String ACTION_STOP_WORK = "ACTION_STOP_WORK";
    private static final String KEY_NEEDS_RESCHEDULE = "KEY_NEEDS_RESCHEDULE";
    private static final String KEY_WORKSPEC_ID = "KEY_WORKSPEC_ID";
    private static final String TAG;
    static final long WORK_PROCESSING_TIME_IN_MS = 600000L;
    private final Context mContext;
    private final Object mLock;
    private final Map<String, ExecutionListener> mPendingDelayMet;
    
    static {
        TAG = Logger.tagWithPrefix("CommandHandler");
    }
    
    CommandHandler(@NonNull final Context mContext) {
        this.mContext = mContext;
        this.mPendingDelayMet = new HashMap<String, ExecutionListener>();
        this.mLock = new Object();
    }
    
    static Intent createConstraintsChangedIntent(@NonNull final Context context) {
        final Intent intent = new Intent(context, (Class)SystemAlarmService.class);
        intent.setAction("ACTION_CONSTRAINTS_CHANGED");
        return intent;
    }
    
    static Intent createDelayMetIntent(@NonNull final Context context, @NonNull final String s) {
        final Intent intent = new Intent(context, (Class)SystemAlarmService.class);
        intent.setAction("ACTION_DELAY_MET");
        intent.putExtra("KEY_WORKSPEC_ID", s);
        return intent;
    }
    
    static Intent createExecutionCompletedIntent(@NonNull final Context context, @NonNull final String s, final boolean b) {
        final Intent intent = new Intent(context, (Class)SystemAlarmService.class);
        intent.setAction("ACTION_EXECUTION_COMPLETED");
        intent.putExtra("KEY_WORKSPEC_ID", s);
        intent.putExtra("KEY_NEEDS_RESCHEDULE", b);
        return intent;
    }
    
    static Intent createRescheduleIntent(@NonNull final Context context) {
        final Intent intent = new Intent(context, (Class)SystemAlarmService.class);
        intent.setAction("ACTION_RESCHEDULE");
        return intent;
    }
    
    static Intent createScheduleWorkIntent(@NonNull final Context context, @NonNull final String s) {
        final Intent intent = new Intent(context, (Class)SystemAlarmService.class);
        intent.setAction("ACTION_SCHEDULE_WORK");
        intent.putExtra("KEY_WORKSPEC_ID", s);
        return intent;
    }
    
    static Intent createStopWorkIntent(@NonNull final Context context, @NonNull final String s) {
        final Intent intent = new Intent(context, (Class)SystemAlarmService.class);
        intent.setAction("ACTION_STOP_WORK");
        intent.putExtra("KEY_WORKSPEC_ID", s);
        return intent;
    }
    
    private void handleConstraintsChanged(@NonNull final Intent intent, final int n, @NonNull final SystemAlarmDispatcher systemAlarmDispatcher) {
        Logger.get().debug(CommandHandler.TAG, String.format("Handling constraints changed %s", intent), new Throwable[0]);
        new ConstraintsCommandHandler(this.mContext, n, systemAlarmDispatcher).handleConstraintsChanged();
    }
    
    private void handleDelayMet(@NonNull final Intent intent, final int n, @NonNull final SystemAlarmDispatcher systemAlarmDispatcher) {
        final Bundle extras = intent.getExtras();
        synchronized (this.mLock) {
            final String string = ((BaseBundle)extras).getString("KEY_WORKSPEC_ID");
            final Logger value = Logger.get();
            final String tag = CommandHandler.TAG;
            value.debug(tag, String.format("Handing delay met for %s", string), new Throwable[0]);
            if (!this.mPendingDelayMet.containsKey(string)) {
                final DelayMetCommandHandler delayMetCommandHandler = new DelayMetCommandHandler(this.mContext, n, string, systemAlarmDispatcher);
                this.mPendingDelayMet.put(string, delayMetCommandHandler);
                delayMetCommandHandler.handleProcessWork();
            }
            else {
                Logger.get().debug(tag, String.format("WorkSpec %s is already being handled for ACTION_DELAY_MET", string), new Throwable[0]);
            }
        }
    }
    
    private void handleExecutionCompleted(@NonNull final Intent intent, final int i) {
        final Bundle extras = intent.getExtras();
        final String string = ((BaseBundle)extras).getString("KEY_WORKSPEC_ID");
        final boolean boolean1 = extras.getBoolean("KEY_NEEDS_RESCHEDULE");
        Logger.get().debug(CommandHandler.TAG, String.format("Handling onExecutionCompleted %s, %s", intent, i), new Throwable[0]);
        this.onExecuted(string, boolean1);
    }
    
    private void handleReschedule(@NonNull final Intent intent, final int i, @NonNull final SystemAlarmDispatcher systemAlarmDispatcher) {
        Logger.get().debug(CommandHandler.TAG, String.format("Handling reschedule %s, %s", intent, i), new Throwable[0]);
        systemAlarmDispatcher.getWorkManager().rescheduleEligibleWork();
    }
    
    private void handleScheduleWorkIntent(@NonNull Intent workDatabase, final int n, @NonNull final SystemAlarmDispatcher systemAlarmDispatcher) {
        final String string = ((BaseBundle)workDatabase.getExtras()).getString("KEY_WORKSPEC_ID");
        final Logger value = Logger.get();
        final String tag = CommandHandler.TAG;
        value.debug(tag, String.format("Handling schedule work for %s", string), new Throwable[0]);
        workDatabase = (Intent)systemAlarmDispatcher.getWorkManager().getWorkDatabase();
        ((RoomDatabase)workDatabase).beginTransaction();
        try {
            final WorkSpec workSpec = ((WorkDatabase)workDatabase).workSpecDao().getWorkSpec(string);
            if (workSpec == null) {
                final Logger value2 = Logger.get();
                final StringBuilder sb = new StringBuilder();
                sb.append("Skipping scheduling ");
                sb.append(string);
                sb.append(" because it's no longer in the DB");
                value2.warning(tag, sb.toString(), new Throwable[0]);
                return;
            }
            if (workSpec.state.isFinished()) {
                final Logger value3 = Logger.get();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Skipping scheduling ");
                sb2.append(string);
                sb2.append("because it is finished.");
                value3.warning(tag, sb2.toString(), new Throwable[0]);
                return;
            }
            final long calculateNextRunTime = workSpec.calculateNextRunTime();
            if (!workSpec.hasConstraints()) {
                Logger.get().debug(tag, String.format("Setting up Alarms for %s at %s", string, calculateNextRunTime), new Throwable[0]);
                Alarms.setAlarm(this.mContext, systemAlarmDispatcher.getWorkManager(), string, calculateNextRunTime);
            }
            else {
                Logger.get().debug(tag, String.format("Opportunistically setting an alarm for %s at %s", string, calculateNextRunTime), new Throwable[0]);
                Alarms.setAlarm(this.mContext, systemAlarmDispatcher.getWorkManager(), string, calculateNextRunTime);
                systemAlarmDispatcher.postOnMainThread(new SystemAlarmDispatcher.AddRunnable(systemAlarmDispatcher, createConstraintsChangedIntent(this.mContext), n));
            }
            ((RoomDatabase)workDatabase).setTransactionSuccessful();
        }
        finally {
            ((RoomDatabase)workDatabase).endTransaction();
        }
    }
    
    private void handleStopWork(@NonNull final Intent intent, @NonNull final SystemAlarmDispatcher systemAlarmDispatcher) {
        final String string = ((BaseBundle)intent.getExtras()).getString("KEY_WORKSPEC_ID");
        Logger.get().debug(CommandHandler.TAG, String.format("Handing stopWork work for %s", string), new Throwable[0]);
        systemAlarmDispatcher.getWorkManager().stopWork(string);
        Alarms.cancelAlarm(this.mContext, systemAlarmDispatcher.getWorkManager(), string);
        systemAlarmDispatcher.onExecuted(string, false);
    }
    
    private static boolean hasKeys(@Nullable final Bundle bundle, @NonNull final String... array) {
        if (bundle != null && !((BaseBundle)bundle).isEmpty()) {
            for (int length = array.length, i = 0; i < length; ++i) {
                if (((BaseBundle)bundle).get(array[i]) == null) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    boolean hasPendingCommands() {
        synchronized (this.mLock) {
            return !this.mPendingDelayMet.isEmpty();
        }
    }
    
    @Override
    public void onExecuted(@NonNull final String s, final boolean b) {
        synchronized (this.mLock) {
            final ExecutionListener executionListener = this.mPendingDelayMet.remove(s);
            if (executionListener != null) {
                executionListener.onExecuted(s, b);
            }
        }
    }
    
    @WorkerThread
    void onHandleIntent(@NonNull final Intent intent, final int n, @NonNull final SystemAlarmDispatcher systemAlarmDispatcher) {
        final String action = intent.getAction();
        if ("ACTION_CONSTRAINTS_CHANGED".equals(action)) {
            this.handleConstraintsChanged(intent, n, systemAlarmDispatcher);
        }
        else if ("ACTION_RESCHEDULE".equals(action)) {
            this.handleReschedule(intent, n, systemAlarmDispatcher);
        }
        else if (!hasKeys(intent.getExtras(), "KEY_WORKSPEC_ID")) {
            Logger.get().error(CommandHandler.TAG, String.format("Invalid request for %s, requires %s.", action, "KEY_WORKSPEC_ID"), new Throwable[0]);
        }
        else if ("ACTION_SCHEDULE_WORK".equals(action)) {
            this.handleScheduleWorkIntent(intent, n, systemAlarmDispatcher);
        }
        else if ("ACTION_DELAY_MET".equals(action)) {
            this.handleDelayMet(intent, n, systemAlarmDispatcher);
        }
        else if ("ACTION_STOP_WORK".equals(action)) {
            this.handleStopWork(intent, systemAlarmDispatcher);
        }
        else if ("ACTION_EXECUTION_COMPLETED".equals(action)) {
            this.handleExecutionCompleted(intent, n);
        }
        else {
            Logger.get().warning(CommandHandler.TAG, String.format("Ignoring intent %s", intent), new Throwable[0]);
        }
    }
}

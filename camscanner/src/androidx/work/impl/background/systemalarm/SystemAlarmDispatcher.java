// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemalarm;

import androidx.work.impl.utils.SerialExecutor;
import android.text.TextUtils;
import android.os.PowerManager$WakeLock;
import androidx.work.impl.utils.WakeLocks;
import androidx.annotation.MainThread;
import java.util.Iterator;
import androidx.annotation.VisibleForTesting;
import android.os.Looper;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import androidx.work.Logger;
import androidx.work.impl.utils.WorkTimer;
import androidx.work.impl.WorkManagerImpl;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import androidx.work.impl.Processor;
import android.os.Handler;
import java.util.List;
import android.content.Intent;
import android.content.Context;
import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
import androidx.work.impl.ExecutionListener;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class SystemAlarmDispatcher implements ExecutionListener
{
    private static final int DEFAULT_START_ID = 0;
    private static final String KEY_START_ID = "KEY_START_ID";
    private static final String PROCESS_COMMAND_TAG = "ProcessCommand";
    static final String TAG;
    final CommandHandler mCommandHandler;
    @Nullable
    private CommandsCompletedListener mCompletedListener;
    final Context mContext;
    Intent mCurrentIntent;
    final List<Intent> mIntents;
    private final Handler mMainHandler;
    private final Processor mProcessor;
    private final TaskExecutor mTaskExecutor;
    private final WorkManagerImpl mWorkManager;
    private final WorkTimer mWorkTimer;
    
    static {
        TAG = Logger.tagWithPrefix("SystemAlarmDispatcher");
    }
    
    SystemAlarmDispatcher(@NonNull final Context context) {
        this(context, null, null);
    }
    
    @VisibleForTesting
    SystemAlarmDispatcher(@NonNull final Context context, @Nullable Processor processor, @Nullable WorkManagerImpl instance) {
        final Context applicationContext = context.getApplicationContext();
        this.mContext = applicationContext;
        this.mCommandHandler = new CommandHandler(applicationContext);
        this.mWorkTimer = new WorkTimer();
        if (instance == null) {
            instance = WorkManagerImpl.getInstance(context);
        }
        this.mWorkManager = instance;
        if (processor == null) {
            processor = instance.getProcessor();
        }
        this.mProcessor = processor;
        this.mTaskExecutor = instance.getWorkTaskExecutor();
        processor.addExecutionListener(this);
        this.mIntents = new ArrayList<Intent>();
        this.mCurrentIntent = null;
        this.mMainHandler = new Handler(Looper.getMainLooper());
    }
    
    private void assertMainThread() {
        if (this.mMainHandler.getLooper().getThread() == Thread.currentThread()) {
            return;
        }
        throw new IllegalStateException("Needs to be invoked on the main thread.");
    }
    
    @MainThread
    private boolean hasIntentWithAction(@NonNull final String s) {
        this.assertMainThread();
        synchronized (this.mIntents) {
            final Iterator<Intent> iterator = this.mIntents.iterator();
            while (iterator.hasNext()) {
                if (s.equals(iterator.next().getAction())) {
                    return true;
                }
            }
            return false;
        }
    }
    
    @MainThread
    private void processCommand() {
        this.assertMainThread();
        final PowerManager$WakeLock wakeLock = WakeLocks.newWakeLock(this.mContext, "ProcessCommand");
        try {
            wakeLock.acquire();
            this.mWorkManager.getWorkTaskExecutor().executeOnBackgroundThread(new Runnable(this) {
                final SystemAlarmDispatcher this$0;
                
                @Override
                public void run() {
                    Object o = this.this$0.mIntents;
                    synchronized (o) {
                        final SystemAlarmDispatcher this$0 = this.this$0;
                        this$0.mCurrentIntent = this$0.mIntents.get(0);
                        monitorexit(o);
                        o = this.this$0.mCurrentIntent;
                        if (o != null) {
                            o = ((Intent)o).getAction();
                            final int intExtra = this.this$0.mCurrentIntent.getIntExtra("KEY_START_ID", 0);
                            final Logger value = Logger.get();
                            final String tag = SystemAlarmDispatcher.TAG;
                            value.debug(tag, String.format("Processing command %s, %s", this.this$0.mCurrentIntent, intExtra), new Throwable[0]);
                            Object wakeLock = WakeLocks.newWakeLock(this.this$0.mContext, String.format("%s (%s)", o, intExtra));
                            Label_0323: {
                                final Throwable t2;
                                try {
                                    Logger.get().debug(tag, String.format("Acquiring operation wake lock (%s) %s", o, wakeLock), new Throwable[0]);
                                    ((PowerManager$WakeLock)wakeLock).acquire();
                                    final SystemAlarmDispatcher this$2 = this.this$0;
                                    this$2.mCommandHandler.onHandleIntent(this$2.mCurrentIntent, intExtra, this$2);
                                    Logger.get().debug(tag, String.format("Releasing operation wake lock (%s) %s", o, wakeLock), new Throwable[0]);
                                    ((PowerManager$WakeLock)wakeLock).release();
                                    o = this.this$0;
                                    wakeLock = new DequeueAndCheckForCompletion((SystemAlarmDispatcher)o);
                                    break Label_0323;
                                }
                                finally {
                                    final Logger logger = Logger.get();
                                    final String s = SystemAlarmDispatcher.TAG;
                                    final Logger logger2 = logger;
                                    final String s2 = s;
                                    final String s3 = "Unexpected error in onHandleIntent";
                                    final int n = 1;
                                    final Throwable[] array = new Throwable[n];
                                    final int n2 = 0;
                                    final Throwable t = t2;
                                    array[n2] = t;
                                    logger2.error(s2, s3, array);
                                    final Logger logger3 = Logger.get();
                                    final String s4 = s;
                                    final String s5 = "Releasing operation wake lock (%s) %s";
                                    final int n3 = 2;
                                    final Object[] array2 = new Object[n3];
                                    final int n4 = 0;
                                    array2[n4] = o;
                                    final int n5 = 1;
                                    final PowerManager$WakeLock powerManager$WakeLock = (PowerManager$WakeLock)wakeLock;
                                    array2[n5] = powerManager$WakeLock;
                                    final String s6 = String.format(s5, array2);
                                    final int n6 = 0;
                                    final Throwable[] array3 = new Throwable[n6];
                                    logger3.debug(s4, s6, array3);
                                    final PowerManager$WakeLock powerManager$WakeLock2 = (PowerManager$WakeLock)wakeLock;
                                    powerManager$WakeLock2.release();
                                    final Runnable runnable = this;
                                    o = runnable.this$0;
                                    final DequeueAndCheckForCompletion dequeueAndCheckForCompletion = (DequeueAndCheckForCompletion)(wakeLock = new DequeueAndCheckForCompletion((SystemAlarmDispatcher)o));
                                }
                                try {
                                    final Logger logger = Logger.get();
                                    final String s = SystemAlarmDispatcher.TAG;
                                    final Logger logger2 = logger;
                                    final String s2 = s;
                                    final String s3 = "Unexpected error in onHandleIntent";
                                    final int n = 1;
                                    final Throwable[] array = new Throwable[n];
                                    final int n2 = 0;
                                    final Throwable t = t2;
                                    array[n2] = t;
                                    logger2.error(s2, s3, array);
                                    final Logger logger3 = Logger.get();
                                    final String s4 = s;
                                    final String s5 = "Releasing operation wake lock (%s) %s";
                                    final int n3 = 2;
                                    final Object[] array2 = new Object[n3];
                                    final int n4 = 0;
                                    array2[n4] = o;
                                    final int n5 = 1;
                                    final PowerManager$WakeLock powerManager$WakeLock = (PowerManager$WakeLock)wakeLock;
                                    array2[n5] = powerManager$WakeLock;
                                    final String s6 = String.format(s5, array2);
                                    final int n6 = 0;
                                    final Throwable[] array3 = new Throwable[n6];
                                    logger3.debug(s4, s6, array3);
                                    final PowerManager$WakeLock powerManager$WakeLock2 = (PowerManager$WakeLock)wakeLock;
                                    powerManager$WakeLock2.release();
                                    final Runnable runnable = this;
                                    o = runnable.this$0;
                                    wakeLock = new DequeueAndCheckForCompletion((SystemAlarmDispatcher)o);
                                    ((SystemAlarmDispatcher)o).postOnMainThread((Runnable)wakeLock);
                                }
                                finally {
                                    Logger.get().debug(SystemAlarmDispatcher.TAG, String.format("Releasing operation wake lock (%s) %s", o, wakeLock), new Throwable[0]);
                                    ((PowerManager$WakeLock)wakeLock).release();
                                    o = this.this$0;
                                    ((SystemAlarmDispatcher)o).postOnMainThread(new DequeueAndCheckForCompletion((SystemAlarmDispatcher)o));
                                }
                            }
                        }
                    }
                }
            });
        }
        finally {
            wakeLock.release();
        }
    }
    
    @MainThread
    public boolean add(@NonNull final Intent intent, int i) {
        final Logger value = Logger.get();
        final String tag = SystemAlarmDispatcher.TAG;
        final int n = 0;
        value.debug(tag, String.format("Adding command %s (%s)", intent, i), new Throwable[0]);
        this.assertMainThread();
        final String action = intent.getAction();
        if (TextUtils.isEmpty((CharSequence)action)) {
            Logger.get().warning(tag, "Unknown command. Ignoring", new Throwable[0]);
            return false;
        }
        if ("ACTION_CONSTRAINTS_CHANGED".equals(action) && this.hasIntentWithAction("ACTION_CONSTRAINTS_CHANGED")) {
            return false;
        }
        intent.putExtra("KEY_START_ID", i);
        final List<Intent> mIntents = this.mIntents;
        monitorenter(mIntents);
        i = n;
        try {
            if (!this.mIntents.isEmpty()) {
                i = 1;
            }
            this.mIntents.add(intent);
            if (i == 0) {
                this.processCommand();
            }
            return true;
        }
        finally {
            monitorexit(mIntents);
        }
    }
    
    @MainThread
    void dequeueAndCheckForCompletion() {
        final Logger value = Logger.get();
        final String tag = SystemAlarmDispatcher.TAG;
        value.debug(tag, "Checking if commands are complete.", new Throwable[0]);
        this.assertMainThread();
        synchronized (this.mIntents) {
            if (this.mCurrentIntent != null) {
                Logger.get().debug(tag, String.format("Removing command %s", this.mCurrentIntent), new Throwable[0]);
                if (!this.mIntents.remove(0).equals(this.mCurrentIntent)) {
                    throw new IllegalStateException("Dequeue-d command is not the first.");
                }
                this.mCurrentIntent = null;
            }
            final SerialExecutor backgroundExecutor = this.mTaskExecutor.getBackgroundExecutor();
            if (!this.mCommandHandler.hasPendingCommands() && this.mIntents.isEmpty() && !backgroundExecutor.hasPendingTasks()) {
                Logger.get().debug(tag, "No more commands & intents.", new Throwable[0]);
                final CommandsCompletedListener mCompletedListener = this.mCompletedListener;
                if (mCompletedListener != null) {
                    mCompletedListener.onAllCommandsCompleted();
                }
            }
            else if (!this.mIntents.isEmpty()) {
                this.processCommand();
            }
        }
    }
    
    Processor getProcessor() {
        return this.mProcessor;
    }
    
    TaskExecutor getTaskExecutor() {
        return this.mTaskExecutor;
    }
    
    WorkManagerImpl getWorkManager() {
        return this.mWorkManager;
    }
    
    WorkTimer getWorkTimer() {
        return this.mWorkTimer;
    }
    
    void onDestroy() {
        Logger.get().debug(SystemAlarmDispatcher.TAG, "Destroying SystemAlarmDispatcher", new Throwable[0]);
        this.mProcessor.removeExecutionListener(this);
        this.mWorkTimer.onDestroy();
        this.mCompletedListener = null;
    }
    
    @Override
    public void onExecuted(@NonNull final String s, final boolean b) {
        this.postOnMainThread(new AddRunnable(this, CommandHandler.createExecutionCompletedIntent(this.mContext, s, b), 0));
    }
    
    void postOnMainThread(@NonNull final Runnable runnable) {
        this.mMainHandler.post(runnable);
    }
    
    void setCompletedListener(@NonNull final CommandsCompletedListener mCompletedListener) {
        if (this.mCompletedListener != null) {
            Logger.get().error(SystemAlarmDispatcher.TAG, "A completion listener for SystemAlarmDispatcher already exists.", new Throwable[0]);
            return;
        }
        this.mCompletedListener = mCompletedListener;
    }
    
    static class AddRunnable implements Runnable
    {
        private final SystemAlarmDispatcher mDispatcher;
        private final Intent mIntent;
        private final int mStartId;
        
        AddRunnable(@NonNull final SystemAlarmDispatcher mDispatcher, @NonNull final Intent mIntent, final int mStartId) {
            this.mDispatcher = mDispatcher;
            this.mIntent = mIntent;
            this.mStartId = mStartId;
        }
        
        @Override
        public void run() {
            this.mDispatcher.add(this.mIntent, this.mStartId);
        }
    }
    
    interface CommandsCompletedListener
    {
        void onAllCommandsCompleted();
    }
    
    static class DequeueAndCheckForCompletion implements Runnable
    {
        private final SystemAlarmDispatcher mDispatcher;
        
        DequeueAndCheckForCompletion(@NonNull final SystemAlarmDispatcher mDispatcher) {
            this.mDispatcher = mDispatcher;
        }
        
        @Override
        public void run() {
            this.mDispatcher.dequeueAndCheckForCompletion();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemalarm;

import android.content.Intent;
import androidx.work.impl.utils.WakeLocks;
import androidx.annotation.MainThread;
import android.content.Context;
import androidx.work.Logger;
import androidx.annotation.RestrictTo;
import androidx.lifecycle.LifecycleService;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class SystemAlarmService extends LifecycleService implements CommandsCompletedListener
{
    private static final String TAG;
    private SystemAlarmDispatcher mDispatcher;
    private boolean mIsShutdown;
    
    static {
        TAG = Logger.tagWithPrefix("SystemAlarmService");
    }
    
    @MainThread
    private void initializeDispatcher() {
        (this.mDispatcher = new SystemAlarmDispatcher((Context)this)).setCompletedListener((SystemAlarmDispatcher.CommandsCompletedListener)this);
    }
    
    @MainThread
    @Override
    public void onAllCommandsCompleted() {
        this.mIsShutdown = true;
        Logger.get().debug(SystemAlarmService.TAG, "All commands completed in dispatcher", new Throwable[0]);
        WakeLocks.checkWakeLocks();
        this.stopSelf();
    }
    
    @Override
    public void onCreate() {
        super.onCreate();
        this.initializeDispatcher();
        this.mIsShutdown = false;
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mIsShutdown = true;
        this.mDispatcher.onDestroy();
    }
    
    @Override
    public int onStartCommand(final Intent intent, final int n, final int n2) {
        super.onStartCommand(intent, n, n2);
        if (this.mIsShutdown) {
            Logger.get().info(SystemAlarmService.TAG, "Re-initializing SystemAlarmDispatcher after a request to shut-down.", new Throwable[0]);
            this.mDispatcher.onDestroy();
            this.initializeDispatcher();
            this.mIsShutdown = false;
        }
        if (intent != null) {
            this.mDispatcher.add(intent, n2);
        }
        return 3;
    }
}

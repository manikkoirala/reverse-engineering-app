// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemalarm;

import androidx.annotation.WorkerThread;
import android.content.Intent;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import androidx.work.impl.model.WorkSpec;
import androidx.work.impl.constraints.WorkConstraintsCallback;
import androidx.annotation.NonNull;
import androidx.work.Logger;
import androidx.work.impl.constraints.WorkConstraintsTracker;
import android.content.Context;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
class ConstraintsCommandHandler
{
    private static final String TAG;
    private final Context mContext;
    private final SystemAlarmDispatcher mDispatcher;
    private final int mStartId;
    private final WorkConstraintsTracker mWorkConstraintsTracker;
    
    static {
        TAG = Logger.tagWithPrefix("ConstraintsCmdHandler");
    }
    
    ConstraintsCommandHandler(@NonNull final Context mContext, final int mStartId, @NonNull final SystemAlarmDispatcher mDispatcher) {
        this.mContext = mContext;
        this.mStartId = mStartId;
        this.mDispatcher = mDispatcher;
        this.mWorkConstraintsTracker = new WorkConstraintsTracker(mContext, mDispatcher.getTaskExecutor(), null);
    }
    
    @WorkerThread
    void handleConstraintsChanged() {
        final List<WorkSpec> scheduledWork = this.mDispatcher.getWorkManager().getWorkDatabase().workSpecDao().getScheduledWork();
        ConstraintProxy.updateAll(this.mContext, scheduledWork);
        this.mWorkConstraintsTracker.replace(scheduledWork);
        final ArrayList list = new ArrayList(scheduledWork.size());
        final long currentTimeMillis = System.currentTimeMillis();
        for (final WorkSpec workSpec : scheduledWork) {
            final String id = workSpec.id;
            if (currentTimeMillis >= workSpec.calculateNextRunTime() && (!workSpec.hasConstraints() || this.mWorkConstraintsTracker.areAllConstraintsMet(id))) {
                list.add((Object)workSpec);
            }
        }
        final Iterator iterator2 = list.iterator();
        while (iterator2.hasNext()) {
            final String id2 = ((WorkSpec)iterator2.next()).id;
            final Intent delayMetIntent = CommandHandler.createDelayMetIntent(this.mContext, id2);
            Logger.get().debug(ConstraintsCommandHandler.TAG, String.format("Creating a delay_met command for workSpec with id (%s)", id2), new Throwable[0]);
            final SystemAlarmDispatcher mDispatcher = this.mDispatcher;
            mDispatcher.postOnMainThread(new SystemAlarmDispatcher.AddRunnable(mDispatcher, delayMetIntent, this.mStartId));
        }
        this.mWorkConstraintsTracker.reset();
    }
}

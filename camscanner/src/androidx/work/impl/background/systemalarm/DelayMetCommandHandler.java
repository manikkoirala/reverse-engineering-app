// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemalarm;

import java.util.List;
import androidx.annotation.WorkerThread;
import androidx.work.impl.model.WorkSpec;
import java.util.Collections;
import androidx.work.impl.utils.WakeLocks;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.work.Logger;
import androidx.work.impl.constraints.WorkConstraintsTracker;
import androidx.annotation.Nullable;
import android.os.PowerManager$WakeLock;
import android.content.Context;
import androidx.annotation.RestrictTo;
import androidx.work.impl.utils.WorkTimer;
import androidx.work.impl.ExecutionListener;
import androidx.work.impl.constraints.WorkConstraintsCallback;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class DelayMetCommandHandler implements WorkConstraintsCallback, ExecutionListener, TimeLimitExceededListener
{
    private static final int STATE_INITIAL = 0;
    private static final int STATE_START_REQUESTED = 1;
    private static final int STATE_STOP_REQUESTED = 2;
    private static final String TAG;
    private final Context mContext;
    private int mCurrentState;
    private final SystemAlarmDispatcher mDispatcher;
    private boolean mHasConstraints;
    private final Object mLock;
    private final int mStartId;
    @Nullable
    private PowerManager$WakeLock mWakeLock;
    private final WorkConstraintsTracker mWorkConstraintsTracker;
    private final String mWorkSpecId;
    
    static {
        TAG = Logger.tagWithPrefix("DelayMetCommandHandler");
    }
    
    DelayMetCommandHandler(@NonNull final Context mContext, final int mStartId, @NonNull final String mWorkSpecId, @NonNull final SystemAlarmDispatcher mDispatcher) {
        this.mContext = mContext;
        this.mStartId = mStartId;
        this.mDispatcher = mDispatcher;
        this.mWorkSpecId = mWorkSpecId;
        this.mWorkConstraintsTracker = new WorkConstraintsTracker(mContext, mDispatcher.getTaskExecutor(), this);
        this.mHasConstraints = false;
        this.mCurrentState = 0;
        this.mLock = new Object();
    }
    
    private void cleanUp() {
        synchronized (this.mLock) {
            this.mWorkConstraintsTracker.reset();
            this.mDispatcher.getWorkTimer().stopTimer(this.mWorkSpecId);
            final PowerManager$WakeLock mWakeLock = this.mWakeLock;
            if (mWakeLock != null && mWakeLock.isHeld()) {
                Logger.get().debug(DelayMetCommandHandler.TAG, String.format("Releasing wakelock %s for WorkSpec %s", this.mWakeLock, this.mWorkSpecId), new Throwable[0]);
                this.mWakeLock.release();
            }
        }
    }
    
    private void stopWork() {
        synchronized (this.mLock) {
            if (this.mCurrentState < 2) {
                this.mCurrentState = 2;
                final Logger value = Logger.get();
                final String tag = DelayMetCommandHandler.TAG;
                value.debug(tag, String.format("Stopping work for WorkSpec %s", this.mWorkSpecId), new Throwable[0]);
                final Intent stopWorkIntent = CommandHandler.createStopWorkIntent(this.mContext, this.mWorkSpecId);
                final SystemAlarmDispatcher mDispatcher = this.mDispatcher;
                mDispatcher.postOnMainThread(new SystemAlarmDispatcher.AddRunnable(mDispatcher, stopWorkIntent, this.mStartId));
                if (this.mDispatcher.getProcessor().isEnqueued(this.mWorkSpecId)) {
                    Logger.get().debug(tag, String.format("WorkSpec %s needs to be rescheduled", this.mWorkSpecId), new Throwable[0]);
                    final Intent scheduleWorkIntent = CommandHandler.createScheduleWorkIntent(this.mContext, this.mWorkSpecId);
                    final SystemAlarmDispatcher mDispatcher2 = this.mDispatcher;
                    mDispatcher2.postOnMainThread(new SystemAlarmDispatcher.AddRunnable(mDispatcher2, scheduleWorkIntent, this.mStartId));
                }
                else {
                    Logger.get().debug(tag, String.format("Processor does not have WorkSpec %s. No need to reschedule ", this.mWorkSpecId), new Throwable[0]);
                }
            }
            else {
                Logger.get().debug(DelayMetCommandHandler.TAG, String.format("Already stopped work for %s", this.mWorkSpecId), new Throwable[0]);
            }
        }
    }
    
    @WorkerThread
    void handleProcessWork() {
        this.mWakeLock = WakeLocks.newWakeLock(this.mContext, String.format("%s (%s)", this.mWorkSpecId, this.mStartId));
        final Logger value = Logger.get();
        final String tag = DelayMetCommandHandler.TAG;
        value.debug(tag, String.format("Acquiring wakelock %s for WorkSpec %s", this.mWakeLock, this.mWorkSpecId), new Throwable[0]);
        this.mWakeLock.acquire();
        final WorkSpec workSpec = this.mDispatcher.getWorkManager().getWorkDatabase().workSpecDao().getWorkSpec(this.mWorkSpecId);
        if (workSpec == null) {
            this.stopWork();
            return;
        }
        if (!(this.mHasConstraints = workSpec.hasConstraints())) {
            Logger.get().debug(tag, String.format("No constraints for %s", this.mWorkSpecId), new Throwable[0]);
            this.onAllConstraintsMet(Collections.singletonList(this.mWorkSpecId));
        }
        else {
            this.mWorkConstraintsTracker.replace(Collections.singletonList(workSpec));
        }
    }
    
    @Override
    public void onAllConstraintsMet(@NonNull final List<String> list) {
        if (!list.contains(this.mWorkSpecId)) {
            return;
        }
        synchronized (this.mLock) {
            if (this.mCurrentState == 0) {
                this.mCurrentState = 1;
                Logger.get().debug(DelayMetCommandHandler.TAG, String.format("onAllConstraintsMet for %s", this.mWorkSpecId), new Throwable[0]);
                if (this.mDispatcher.getProcessor().startWork(this.mWorkSpecId)) {
                    this.mDispatcher.getWorkTimer().startTimer(this.mWorkSpecId, 600000L, (WorkTimer.TimeLimitExceededListener)this);
                }
                else {
                    this.cleanUp();
                }
            }
            else {
                Logger.get().debug(DelayMetCommandHandler.TAG, String.format("Already started work for %s", this.mWorkSpecId), new Throwable[0]);
            }
        }
    }
    
    @Override
    public void onAllConstraintsNotMet(@NonNull final List<String> list) {
        this.stopWork();
    }
    
    @Override
    public void onExecuted(@NonNull final String s, final boolean b) {
        Logger.get().debug(DelayMetCommandHandler.TAG, String.format("onExecuted %s, %s", s, b), new Throwable[0]);
        this.cleanUp();
        if (b) {
            final Intent scheduleWorkIntent = CommandHandler.createScheduleWorkIntent(this.mContext, this.mWorkSpecId);
            final SystemAlarmDispatcher mDispatcher = this.mDispatcher;
            mDispatcher.postOnMainThread(new SystemAlarmDispatcher.AddRunnable(mDispatcher, scheduleWorkIntent, this.mStartId));
        }
        if (this.mHasConstraints) {
            final Intent constraintsChangedIntent = CommandHandler.createConstraintsChangedIntent(this.mContext);
            final SystemAlarmDispatcher mDispatcher2 = this.mDispatcher;
            mDispatcher2.postOnMainThread(new SystemAlarmDispatcher.AddRunnable(mDispatcher2, constraintsChangedIntent, this.mStartId));
        }
    }
    
    @Override
    public void onTimeLimitExceeded(@NonNull final String s) {
        Logger.get().debug(DelayMetCommandHandler.TAG, String.format("Exceeded time limits on execution for %s", s), new Throwable[0]);
        this.stopWork();
    }
}

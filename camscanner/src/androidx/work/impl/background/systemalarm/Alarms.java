// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.background.systemalarm;

import androidx.work.impl.WorkDatabase;
import androidx.work.impl.utils.IdGenerator;
import android.content.Intent;
import android.app.PendingIntent;
import android.os.Build$VERSION;
import android.app.AlarmManager;
import androidx.work.impl.model.SystemIdInfo;
import androidx.work.impl.model.SystemIdInfoDao;
import androidx.work.impl.WorkManagerImpl;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.work.Logger;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
class Alarms
{
    private static final String TAG;
    
    static {
        TAG = Logger.tagWithPrefix("Alarms");
    }
    
    private Alarms() {
    }
    
    public static void cancelAlarm(@NonNull final Context context, @NonNull final WorkManagerImpl workManagerImpl, @NonNull final String s) {
        final SystemIdInfoDao systemIdInfoDao = workManagerImpl.getWorkDatabase().systemIdInfoDao();
        final SystemIdInfo systemIdInfo = systemIdInfoDao.getSystemIdInfo(s);
        if (systemIdInfo != null) {
            cancelExactAlarm(context, s, systemIdInfo.systemId);
            Logger.get().debug(Alarms.TAG, String.format("Removing SystemIdInfo for workSpecId (%s)", s), new Throwable[0]);
            systemIdInfoDao.removeSystemIdInfo(s);
        }
    }
    
    private static void cancelExactAlarm(@NonNull final Context context, @NonNull final String s, final int i) {
        final AlarmManager alarmManager = (AlarmManager)context.getSystemService("alarm");
        final Intent delayMetIntent = CommandHandler.createDelayMetIntent(context, s);
        int n;
        if (Build$VERSION.SDK_INT >= 23) {
            n = 603979776;
        }
        else {
            n = 536870912;
        }
        final PendingIntent service = PendingIntent.getService(context, i, delayMetIntent, n);
        if (service != null && alarmManager != null) {
            Logger.get().debug(Alarms.TAG, String.format("Cancelling existing alarm with (workSpecId, systemId) (%s, %s)", s, i), new Throwable[0]);
            alarmManager.cancel(service);
        }
    }
    
    public static void setAlarm(@NonNull final Context context, @NonNull final WorkManagerImpl workManagerImpl, @NonNull final String s, final long n) {
        final WorkDatabase workDatabase = workManagerImpl.getWorkDatabase();
        final SystemIdInfoDao systemIdInfoDao = workDatabase.systemIdInfoDao();
        final SystemIdInfo systemIdInfo = systemIdInfoDao.getSystemIdInfo(s);
        if (systemIdInfo != null) {
            cancelExactAlarm(context, s, systemIdInfo.systemId);
            setExactAlarm(context, s, systemIdInfo.systemId, n);
        }
        else {
            final int nextAlarmManagerId = new IdGenerator(workDatabase).nextAlarmManagerId();
            systemIdInfoDao.insertSystemIdInfo(new SystemIdInfo(s, nextAlarmManagerId));
            setExactAlarm(context, s, nextAlarmManagerId, n);
        }
    }
    
    private static void setExactAlarm(@NonNull final Context context, @NonNull final String s, final int n, final long n2) {
        final AlarmManager alarmManager = (AlarmManager)context.getSystemService("alarm");
        int n3;
        if (Build$VERSION.SDK_INT >= 23) {
            n3 = 201326592;
        }
        else {
            n3 = 134217728;
        }
        final PendingIntent service = PendingIntent.getService(context, n, CommandHandler.createDelayMetIntent(context, s), n3);
        if (alarmManager != null) {
            alarmManager.setExact(0, n2, service);
        }
    }
}

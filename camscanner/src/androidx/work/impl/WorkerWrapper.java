// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import java.util.concurrent.Future;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.WorkerThread;
import androidx.work.InputMerger;
import java.util.concurrent.Executor;
import android.annotation.SuppressLint;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import androidx.work.impl.utils.WorkForegroundRunnable;
import androidx.work.ForegroundUpdater;
import androidx.work.ProgressUpdater;
import androidx.work.impl.utils.WorkForegroundUpdater;
import androidx.work.impl.utils.WorkProgressUpdater;
import java.util.UUID;
import androidx.work.Data;
import java.util.ArrayList;
import androidx.work.impl.utils.PackageManagerHelper;
import androidx.work.impl.background.systemalarm.RescheduleReceiver;
import java.util.Collection;
import androidx.work.WorkInfo;
import java.util.LinkedList;
import java.util.Iterator;
import androidx.work.Logger;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import androidx.work.impl.model.WorkTagDao;
import androidx.work.impl.model.WorkSpecDao;
import androidx.work.impl.model.WorkSpec;
import java.util.List;
import androidx.work.WorkerParameters;
import androidx.annotation.Nullable;
import androidx.work.ListenableWorker;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.annotation.NonNull;
import androidx.work.impl.utils.futures.SettableFuture;
import androidx.work.impl.foreground.ForegroundProcessor;
import androidx.work.impl.model.DependencyDao;
import androidx.work.Configuration;
import android.content.Context;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class WorkerWrapper implements Runnable
{
    static final String TAG;
    Context mAppContext;
    private Configuration mConfiguration;
    private DependencyDao mDependencyDao;
    private ForegroundProcessor mForegroundProcessor;
    @NonNull
    SettableFuture<Boolean> mFuture;
    @Nullable
    ListenableFuture<ListenableWorker.Result> mInnerFuture;
    private volatile boolean mInterrupted;
    @NonNull
    ListenableWorker.Result mResult;
    private WorkerParameters.RuntimeExtras mRuntimeExtras;
    private List<Scheduler> mSchedulers;
    private List<String> mTags;
    private WorkDatabase mWorkDatabase;
    private String mWorkDescription;
    WorkSpec mWorkSpec;
    private WorkSpecDao mWorkSpecDao;
    private String mWorkSpecId;
    private WorkTagDao mWorkTagDao;
    TaskExecutor mWorkTaskExecutor;
    ListenableWorker mWorker;
    
    static {
        TAG = Logger.tagWithPrefix("WorkerWrapper");
    }
    
    WorkerWrapper(@NonNull final Builder builder) {
        this.mResult = ListenableWorker.Result.failure();
        this.mFuture = SettableFuture.create();
        this.mInnerFuture = null;
        this.mAppContext = builder.mAppContext;
        this.mWorkTaskExecutor = builder.mWorkTaskExecutor;
        this.mForegroundProcessor = builder.mForegroundProcessor;
        this.mWorkSpecId = builder.mWorkSpecId;
        this.mSchedulers = builder.mSchedulers;
        this.mRuntimeExtras = builder.mRuntimeExtras;
        this.mWorker = builder.mWorker;
        this.mConfiguration = builder.mConfiguration;
        final WorkDatabase mWorkDatabase = builder.mWorkDatabase;
        this.mWorkDatabase = mWorkDatabase;
        this.mWorkSpecDao = mWorkDatabase.workSpecDao();
        this.mDependencyDao = this.mWorkDatabase.dependencyDao();
        this.mWorkTagDao = this.mWorkDatabase.workTagDao();
    }
    
    private String createWorkDescription(final List<String> list) {
        final StringBuilder sb = new StringBuilder("Work [ id=");
        sb.append(this.mWorkSpecId);
        sb.append(", tags={ ");
        final Iterator<String> iterator = list.iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final String str = iterator.next();
            if (n != 0) {
                n = 0;
            }
            else {
                sb.append(", ");
            }
            sb.append(str);
        }
        sb.append(" } ]");
        return sb.toString();
    }
    
    private void handleResult(final ListenableWorker.Result result) {
        if (result instanceof ListenableWorker.Result.Success) {
            Logger.get().info(WorkerWrapper.TAG, String.format("Worker result SUCCESS for %s", this.mWorkDescription), new Throwable[0]);
            if (this.mWorkSpec.isPeriodic()) {
                this.resetPeriodicAndResolve();
            }
            else {
                this.setSucceededAndResolve();
            }
        }
        else if (result instanceof ListenableWorker.Result.Retry) {
            Logger.get().info(WorkerWrapper.TAG, String.format("Worker result RETRY for %s", this.mWorkDescription), new Throwable[0]);
            this.rescheduleAndResolve();
        }
        else {
            Logger.get().info(WorkerWrapper.TAG, String.format("Worker result FAILURE for %s", this.mWorkDescription), new Throwable[0]);
            if (this.mWorkSpec.isPeriodic()) {
                this.resetPeriodicAndResolve();
            }
            else {
                this.setFailedAndResolve();
            }
        }
    }
    
    private void iterativelyFailWorkAndDependents(String e) {
        final LinkedList list = new LinkedList();
        list.add(e);
        while (!list.isEmpty()) {
            e = (String)list.remove();
            if (this.mWorkSpecDao.getState(e) != WorkInfo.State.CANCELLED) {
                this.mWorkSpecDao.setState(WorkInfo.State.FAILED, e);
            }
            list.addAll(this.mDependencyDao.getDependentWorkIds(e));
        }
    }
    
    private void rescheduleAndResolve() {
        this.mWorkDatabase.beginTransaction();
        try {
            this.mWorkSpecDao.setState(WorkInfo.State.ENQUEUED, this.mWorkSpecId);
            this.mWorkSpecDao.setPeriodStartTime(this.mWorkSpecId, System.currentTimeMillis());
            this.mWorkSpecDao.markWorkSpecScheduled(this.mWorkSpecId, -1L);
            this.mWorkDatabase.setTransactionSuccessful();
        }
        finally {
            this.mWorkDatabase.endTransaction();
            this.resolve(true);
        }
    }
    
    private void resetPeriodicAndResolve() {
        this.mWorkDatabase.beginTransaction();
        try {
            this.mWorkSpecDao.setPeriodStartTime(this.mWorkSpecId, System.currentTimeMillis());
            this.mWorkSpecDao.setState(WorkInfo.State.ENQUEUED, this.mWorkSpecId);
            this.mWorkSpecDao.resetWorkSpecRunAttemptCount(this.mWorkSpecId);
            this.mWorkSpecDao.markWorkSpecScheduled(this.mWorkSpecId, -1L);
            this.mWorkDatabase.setTransactionSuccessful();
        }
        finally {
            this.mWorkDatabase.endTransaction();
            this.resolve(false);
        }
    }
    
    private void resolve(final boolean b) {
        this.mWorkDatabase.beginTransaction();
        try {
            if (!this.mWorkDatabase.workSpecDao().hasUnfinishedWork()) {
                PackageManagerHelper.setComponentEnabled(this.mAppContext, RescheduleReceiver.class, false);
            }
            if (b) {
                this.mWorkSpecDao.setState(WorkInfo.State.ENQUEUED, this.mWorkSpecId);
                this.mWorkSpecDao.markWorkSpecScheduled(this.mWorkSpecId, -1L);
            }
            if (this.mWorkSpec != null) {
                final ListenableWorker mWorker = this.mWorker;
                if (mWorker != null && mWorker.isRunInForeground()) {
                    this.mForegroundProcessor.stopForeground(this.mWorkSpecId);
                }
            }
            this.mWorkDatabase.setTransactionSuccessful();
            this.mWorkDatabase.endTransaction();
            this.mFuture.set(b);
        }
        finally {
            this.mWorkDatabase.endTransaction();
        }
    }
    
    private void resolveIncorrectStatus() {
        final WorkInfo.State state = this.mWorkSpecDao.getState(this.mWorkSpecId);
        if (state == WorkInfo.State.RUNNING) {
            Logger.get().debug(WorkerWrapper.TAG, String.format("Status for %s is RUNNING;not doing any work and rescheduling for later execution", this.mWorkSpecId), new Throwable[0]);
            this.resolve(true);
        }
        else {
            Logger.get().debug(WorkerWrapper.TAG, String.format("Status for %s is %s; not doing any work", this.mWorkSpecId, state), new Throwable[0]);
            this.resolve(false);
        }
    }
    
    private void runWorker() {
        if (this.tryCheckForInterruptionAndResolve()) {
            return;
        }
        this.mWorkDatabase.beginTransaction();
        try {
            final WorkSpec workSpec = this.mWorkSpecDao.getWorkSpec(this.mWorkSpecId);
            this.mWorkSpec = workSpec;
            if (workSpec == null) {
                Logger.get().error(WorkerWrapper.TAG, String.format("Didn't find WorkSpec for id %s", this.mWorkSpecId), new Throwable[0]);
                this.resolve(false);
                this.mWorkDatabase.setTransactionSuccessful();
                return;
            }
            if (workSpec.state != WorkInfo.State.ENQUEUED) {
                this.resolveIncorrectStatus();
                this.mWorkDatabase.setTransactionSuccessful();
                Logger.get().debug(WorkerWrapper.TAG, String.format("%s is not in ENQUEUED state. Nothing more to do.", this.mWorkSpec.workerClassName), new Throwable[0]);
                return;
            }
            if (workSpec.isPeriodic() || this.mWorkSpec.isBackedOff()) {
                final long currentTimeMillis = System.currentTimeMillis();
                final WorkSpec mWorkSpec = this.mWorkSpec;
                if (mWorkSpec.periodStartTime != 0L && currentTimeMillis < mWorkSpec.calculateNextRunTime()) {
                    Logger.get().debug(WorkerWrapper.TAG, String.format("Delaying execution for %s because it is being executed before schedule.", this.mWorkSpec.workerClassName), new Throwable[0]);
                    this.resolve(true);
                    this.mWorkDatabase.setTransactionSuccessful();
                    return;
                }
            }
            this.mWorkDatabase.setTransactionSuccessful();
            this.mWorkDatabase.endTransaction();
            Data data;
            if (this.mWorkSpec.isPeriodic()) {
                data = this.mWorkSpec.input;
            }
            else {
                final InputMerger inputMergerWithDefaultFallback = this.mConfiguration.getInputMergerFactory().createInputMergerWithDefaultFallback(this.mWorkSpec.inputMergerClassName);
                if (inputMergerWithDefaultFallback == null) {
                    Logger.get().error(WorkerWrapper.TAG, String.format("Could not create Input Merger %s", this.mWorkSpec.inputMergerClassName), new Throwable[0]);
                    this.setFailedAndResolve();
                    return;
                }
                final ArrayList list = new ArrayList();
                list.add(this.mWorkSpec.input);
                list.addAll(this.mWorkSpecDao.getInputsFromPrerequisites(this.mWorkSpecId));
                data = inputMergerWithDefaultFallback.merge(list);
            }
            final WorkerParameters workerParameters = new WorkerParameters(UUID.fromString(this.mWorkSpecId), data, this.mTags, this.mRuntimeExtras, this.mWorkSpec.runAttemptCount, this.mConfiguration.getExecutor(), this.mWorkTaskExecutor, this.mConfiguration.getWorkerFactory(), new WorkProgressUpdater(this.mWorkDatabase, this.mWorkTaskExecutor), new WorkForegroundUpdater(this.mWorkDatabase, this.mForegroundProcessor, this.mWorkTaskExecutor));
            if (this.mWorker == null) {
                this.mWorker = this.mConfiguration.getWorkerFactory().createWorkerWithDefaultFallback(this.mAppContext, this.mWorkSpec.workerClassName, workerParameters);
            }
            final ListenableWorker mWorker = this.mWorker;
            if (mWorker == null) {
                Logger.get().error(WorkerWrapper.TAG, String.format("Could not create Worker %s", this.mWorkSpec.workerClassName), new Throwable[0]);
                this.setFailedAndResolve();
                return;
            }
            if (mWorker.isUsed()) {
                Logger.get().error(WorkerWrapper.TAG, String.format("Received an already-used Worker %s; WorkerFactory should return new instances", this.mWorkSpec.workerClassName), new Throwable[0]);
                this.setFailedAndResolve();
                return;
            }
            this.mWorker.setUsed();
            if (this.trySetRunning()) {
                if (this.tryCheckForInterruptionAndResolve()) {
                    return;
                }
                final SettableFuture<Object> create = SettableFuture.create();
                final WorkForegroundRunnable workForegroundRunnable = new WorkForegroundRunnable(this.mAppContext, this.mWorkSpec, this.mWorker, workerParameters.getForegroundUpdater(), this.mWorkTaskExecutor);
                this.mWorkTaskExecutor.getMainThreadExecutor().execute(workForegroundRunnable);
                final ListenableFuture<Void> future = workForegroundRunnable.getFuture();
                future.addListener((Runnable)new Runnable(this, future, create) {
                    final WorkerWrapper this$0;
                    final SettableFuture val$future;
                    final ListenableFuture val$runExpedited;
                    
                    @Override
                    public void run() {
                        try {
                            ((Future<Object>)this.val$runExpedited).get();
                            Logger.get().debug(WorkerWrapper.TAG, String.format("Starting work for %s", this.this$0.mWorkSpec.workerClassName), new Throwable[0]);
                            final WorkerWrapper this$0 = this.this$0;
                            this$0.mInnerFuture = this$0.mWorker.startWork();
                            this.val$future.setFuture(this.this$0.mInnerFuture);
                        }
                        finally {
                            final Throwable exception;
                            this.val$future.setException(exception);
                        }
                    }
                }, this.mWorkTaskExecutor.getMainThreadExecutor());
                create.addListener(new Runnable(this, create, this.mWorkDescription) {
                    final WorkerWrapper this$0;
                    final SettableFuture val$future;
                    final String val$workDescription;
                    
                    @SuppressLint({ "SyntheticAccessor" })
                    @Override
                    public void run() {
                        try {
                            try {
                                final ListenableWorker.Result mResult = (ListenableWorker.Result)this.val$future.get();
                                if (mResult == null) {
                                    Logger.get().error(WorkerWrapper.TAG, String.format("%s returned a null result. Treating it as a failure.", this.this$0.mWorkSpec.workerClassName), new Throwable[0]);
                                }
                                Logger.get().debug(WorkerWrapper.TAG, String.format("%s returned a %s result.", this.this$0.mWorkSpec.workerClassName, mResult), new Throwable[0]);
                                this.this$0.mResult = mResult;
                            }
                            finally {}
                        }
                        catch (final ExecutionException t) {
                            goto Label_0112;
                        }
                        catch (final InterruptedException ex) {}
                        catch (final CancellationException ex2) {
                            Logger.get().info(WorkerWrapper.TAG, String.format("%s was cancelled", this.val$workDescription), ex2);
                        }
                        this.this$0.onWorkFinished();
                        return;
                        this.this$0.onWorkFinished();
                    }
                }, this.mWorkTaskExecutor.getBackgroundExecutor());
            }
            else {
                this.resolveIncorrectStatus();
            }
        }
        finally {
            this.mWorkDatabase.endTransaction();
        }
    }
    
    private void setSucceededAndResolve() {
        this.mWorkDatabase.beginTransaction();
        try {
            this.mWorkSpecDao.setState(WorkInfo.State.SUCCEEDED, this.mWorkSpecId);
            this.mWorkSpecDao.setOutput(this.mWorkSpecId, ((ListenableWorker.Result.Success)this.mResult).getOutputData());
            final long currentTimeMillis = System.currentTimeMillis();
            for (final String s : this.mDependencyDao.getDependentWorkIds(this.mWorkSpecId)) {
                if (this.mWorkSpecDao.getState(s) == WorkInfo.State.BLOCKED && this.mDependencyDao.hasCompletedAllPrerequisites(s)) {
                    Logger.get().info(WorkerWrapper.TAG, String.format("Setting status to enqueued for %s", s), new Throwable[0]);
                    this.mWorkSpecDao.setState(WorkInfo.State.ENQUEUED, s);
                    this.mWorkSpecDao.setPeriodStartTime(s, currentTimeMillis);
                }
            }
            this.mWorkDatabase.setTransactionSuccessful();
        }
        finally {
            this.mWorkDatabase.endTransaction();
            this.resolve(false);
        }
    }
    
    private boolean tryCheckForInterruptionAndResolve() {
        if (this.mInterrupted) {
            Logger.get().debug(WorkerWrapper.TAG, String.format("Work interrupted for %s", this.mWorkDescription), new Throwable[0]);
            final WorkInfo.State state = this.mWorkSpecDao.getState(this.mWorkSpecId);
            if (state == null) {
                this.resolve(false);
            }
            else {
                this.resolve(state.isFinished() ^ true);
            }
            return true;
        }
        return false;
    }
    
    private boolean trySetRunning() {
        this.mWorkDatabase.beginTransaction();
        try {
            final WorkInfo.State state = this.mWorkSpecDao.getState(this.mWorkSpecId);
            final WorkInfo.State enqueued = WorkInfo.State.ENQUEUED;
            boolean b = false;
            if (state == enqueued) {
                this.mWorkSpecDao.setState(WorkInfo.State.RUNNING, this.mWorkSpecId);
                this.mWorkSpecDao.incrementWorkSpecRunAttemptCount(this.mWorkSpecId);
                b = true;
            }
            this.mWorkDatabase.setTransactionSuccessful();
            return b;
        }
        finally {
            this.mWorkDatabase.endTransaction();
        }
    }
    
    @NonNull
    public ListenableFuture<Boolean> getFuture() {
        return (ListenableFuture<Boolean>)this.mFuture;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public void interrupt() {
        this.mInterrupted = true;
        this.tryCheckForInterruptionAndResolve();
        final ListenableFuture<ListenableWorker.Result> mInnerFuture = this.mInnerFuture;
        boolean done;
        if (mInnerFuture != null) {
            done = ((Future)mInnerFuture).isDone();
            ((Future)this.mInnerFuture).cancel(true);
        }
        else {
            done = false;
        }
        final ListenableWorker mWorker = this.mWorker;
        if (mWorker != null && !done) {
            mWorker.stop();
        }
        else {
            Logger.get().debug(WorkerWrapper.TAG, String.format("WorkSpec %s is already done. Not interrupting.", this.mWorkSpec), new Throwable[0]);
        }
    }
    
    void onWorkFinished() {
        if (!this.tryCheckForInterruptionAndResolve()) {
            this.mWorkDatabase.beginTransaction();
            try {
                final WorkInfo.State state = this.mWorkSpecDao.getState(this.mWorkSpecId);
                this.mWorkDatabase.workProgressDao().delete(this.mWorkSpecId);
                if (state == null) {
                    this.resolve(false);
                }
                else if (state == WorkInfo.State.RUNNING) {
                    this.handleResult(this.mResult);
                }
                else if (!state.isFinished()) {
                    this.rescheduleAndResolve();
                }
                this.mWorkDatabase.setTransactionSuccessful();
            }
            finally {
                this.mWorkDatabase.endTransaction();
            }
        }
        final List<Scheduler> mSchedulers = this.mSchedulers;
        if (mSchedulers != null) {
            final Iterator<Scheduler> iterator = mSchedulers.iterator();
            while (iterator.hasNext()) {
                iterator.next().cancel(this.mWorkSpecId);
            }
            Schedulers.schedule(this.mConfiguration, this.mWorkDatabase, this.mSchedulers);
        }
    }
    
    @WorkerThread
    @Override
    public void run() {
        final List<String> tagsForWorkSpecId = this.mWorkTagDao.getTagsForWorkSpecId(this.mWorkSpecId);
        this.mTags = tagsForWorkSpecId;
        this.mWorkDescription = this.createWorkDescription(tagsForWorkSpecId);
        this.runWorker();
    }
    
    @VisibleForTesting
    void setFailedAndResolve() {
        this.mWorkDatabase.beginTransaction();
        try {
            this.iterativelyFailWorkAndDependents(this.mWorkSpecId);
            this.mWorkSpecDao.setOutput(this.mWorkSpecId, ((ListenableWorker.Result.Failure)this.mResult).getOutputData());
            this.mWorkDatabase.setTransactionSuccessful();
        }
        finally {
            this.mWorkDatabase.endTransaction();
            this.resolve(false);
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static class Builder
    {
        @NonNull
        Context mAppContext;
        @NonNull
        Configuration mConfiguration;
        @NonNull
        ForegroundProcessor mForegroundProcessor;
        @NonNull
        WorkerParameters.RuntimeExtras mRuntimeExtras;
        List<Scheduler> mSchedulers;
        @NonNull
        WorkDatabase mWorkDatabase;
        @NonNull
        String mWorkSpecId;
        @NonNull
        TaskExecutor mWorkTaskExecutor;
        @Nullable
        ListenableWorker mWorker;
        
        public Builder(@NonNull final Context context, @NonNull final Configuration mConfiguration, @NonNull final TaskExecutor mWorkTaskExecutor, @NonNull final ForegroundProcessor mForegroundProcessor, @NonNull final WorkDatabase mWorkDatabase, @NonNull final String mWorkSpecId) {
            this.mRuntimeExtras = new WorkerParameters.RuntimeExtras();
            this.mAppContext = context.getApplicationContext();
            this.mWorkTaskExecutor = mWorkTaskExecutor;
            this.mForegroundProcessor = mForegroundProcessor;
            this.mConfiguration = mConfiguration;
            this.mWorkDatabase = mWorkDatabase;
            this.mWorkSpecId = mWorkSpecId;
        }
        
        @NonNull
        public WorkerWrapper build() {
            return new WorkerWrapper(this);
        }
        
        @NonNull
        public Builder withRuntimeExtras(@Nullable final WorkerParameters.RuntimeExtras mRuntimeExtras) {
            if (mRuntimeExtras != null) {
                this.mRuntimeExtras = mRuntimeExtras;
            }
            return this;
        }
        
        @NonNull
        public Builder withSchedulers(@NonNull final List<Scheduler> mSchedulers) {
            this.mSchedulers = mSchedulers;
            return this;
        }
        
        @NonNull
        @VisibleForTesting
        public Builder withWorker(@NonNull final ListenableWorker mWorker) {
            this.mWorker = mWorker;
            return this;
        }
    }
}

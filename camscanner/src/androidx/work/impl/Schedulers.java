// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import androidx.annotation.Nullable;
import java.util.Iterator;
import androidx.work.impl.model.WorkSpecDao;
import androidx.work.impl.model.WorkSpec;
import java.util.List;
import androidx.work.Configuration;
import androidx.work.impl.background.systemalarm.SystemAlarmService;
import androidx.work.impl.background.systemalarm.SystemAlarmScheduler;
import androidx.work.impl.utils.PackageManagerHelper;
import androidx.work.impl.background.systemjob.SystemJobService;
import androidx.work.impl.background.systemjob.SystemJobScheduler;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.work.Logger;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class Schedulers
{
    public static final String GCM_SCHEDULER = "androidx.work.impl.background.gcm.GcmScheduler";
    private static final String TAG;
    
    static {
        TAG = Logger.tagWithPrefix("Schedulers");
    }
    
    private Schedulers() {
    }
    
    @NonNull
    static Scheduler createBestAvailableBackgroundScheduler(@NonNull final Context context, @NonNull final WorkManagerImpl workManagerImpl) {
        Scheduler tryCreateGcmBasedScheduler;
        if (Build$VERSION.SDK_INT >= 23) {
            tryCreateGcmBasedScheduler = new SystemJobScheduler(context, workManagerImpl);
            PackageManagerHelper.setComponentEnabled(context, SystemJobService.class, true);
            Logger.get().debug(Schedulers.TAG, "Created SystemJobScheduler and enabled SystemJobService", new Throwable[0]);
        }
        else if ((tryCreateGcmBasedScheduler = tryCreateGcmBasedScheduler(context)) == null) {
            tryCreateGcmBasedScheduler = new SystemAlarmScheduler(context);
            PackageManagerHelper.setComponentEnabled(context, SystemAlarmService.class, true);
            Logger.get().debug(Schedulers.TAG, "Created SystemAlarmScheduler", new Throwable[0]);
        }
        return tryCreateGcmBasedScheduler;
    }
    
    public static void schedule(@NonNull final Configuration configuration, @NonNull WorkDatabase workDatabase, final List<Scheduler> list) {
        if (list != null) {
            if (list.size() != 0) {
                final WorkSpecDao workSpecDao = workDatabase.workSpecDao();
                workDatabase.beginTransaction();
                try {
                    final List<WorkSpec> eligibleWorkForScheduling = workSpecDao.getEligibleWorkForScheduling(configuration.getMaxSchedulerLimit());
                    final List<WorkSpec> allEligibleWorkSpecsForScheduling = workSpecDao.getAllEligibleWorkSpecsForScheduling(200);
                    if (eligibleWorkForScheduling != null && eligibleWorkForScheduling.size() > 0) {
                        final long currentTimeMillis = System.currentTimeMillis();
                        final Iterator iterator = eligibleWorkForScheduling.iterator();
                        while (iterator.hasNext()) {
                            workSpecDao.markWorkSpecScheduled(((WorkSpec)iterator.next()).id, currentTimeMillis);
                        }
                    }
                    workDatabase.setTransactionSuccessful();
                    workDatabase.endTransaction();
                    if (eligibleWorkForScheduling != null && eligibleWorkForScheduling.size() > 0) {
                        final WorkSpec[] array = eligibleWorkForScheduling.toArray(new WorkSpec[eligibleWorkForScheduling.size()]);
                        workDatabase = (WorkDatabase)list.iterator();
                        while (((Iterator)workDatabase).hasNext()) {
                            final Scheduler scheduler = ((Iterator<Scheduler>)workDatabase).next();
                            if (scheduler.hasLimitedSchedulingSlots()) {
                                scheduler.schedule(array);
                            }
                        }
                    }
                    if (allEligibleWorkSpecsForScheduling != null && allEligibleWorkSpecsForScheduling.size() > 0) {
                        final WorkSpec[] array2 = allEligibleWorkSpecsForScheduling.toArray(new WorkSpec[allEligibleWorkSpecsForScheduling.size()]);
                        workDatabase = (WorkDatabase)list.iterator();
                        while (((Iterator)workDatabase).hasNext()) {
                            final Scheduler scheduler2 = ((Iterator<Scheduler>)workDatabase).next();
                            if (!scheduler2.hasLimitedSchedulingSlots()) {
                                scheduler2.schedule(array2);
                            }
                        }
                    }
                }
                finally {
                    workDatabase.endTransaction();
                }
            }
        }
    }
    
    @Nullable
    private static Scheduler tryCreateGcmBasedScheduler(@NonNull final Context context) {
        try {
            final Scheduler scheduler = (Scheduler)Class.forName("androidx.work.impl.background.gcm.GcmScheduler").getConstructor(Context.class).newInstance(context);
            Logger.get().debug(Schedulers.TAG, String.format("Created %s", "androidx.work.impl.background.gcm.GcmScheduler"), new Throwable[0]);
            return scheduler;
        }
        finally {
            final Throwable t;
            Logger.get().debug(Schedulers.TAG, "Unable to create GCM Scheduler", t);
            return null;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import androidx.work.impl.model.Preference;
import android.content.SharedPreferences;
import androidx.sqlite.db.SupportSQLiteDatabase;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.work.impl.WorkDatabase;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class IdGenerator
{
    public static final int INITIAL_ID = 0;
    public static final String NEXT_ALARM_MANAGER_ID_KEY = "next_alarm_manager_id";
    public static final String NEXT_JOB_SCHEDULER_ID_KEY = "next_job_scheduler_id";
    public static final String PREFERENCE_FILE_KEY = "androidx.work.util.id";
    private final WorkDatabase mWorkDatabase;
    
    public IdGenerator(@NonNull final WorkDatabase mWorkDatabase) {
        this.mWorkDatabase = mWorkDatabase;
    }
    
    public static void migrateLegacyIdGenerator(@NonNull final Context context, @NonNull final SupportSQLiteDatabase supportSQLiteDatabase) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences("androidx.work.util.id", 0);
        if (!sharedPreferences.contains("next_job_scheduler_id") && !sharedPreferences.contains("next_job_scheduler_id")) {
            return;
        }
        final int int1 = sharedPreferences.getInt("next_job_scheduler_id", 0);
        final int int2 = sharedPreferences.getInt("next_alarm_manager_id", 0);
        supportSQLiteDatabase.beginTransaction();
        try {
            supportSQLiteDatabase.execSQL("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[] { "next_job_scheduler_id", int1 });
            supportSQLiteDatabase.execSQL("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[] { "next_alarm_manager_id", int2 });
            sharedPreferences.edit().clear().apply();
            supportSQLiteDatabase.setTransactionSuccessful();
        }
        finally {
            supportSQLiteDatabase.endTransaction();
        }
    }
    
    private int nextId(final String s) {
        this.mWorkDatabase.beginTransaction();
        try {
            final Long longValue = this.mWorkDatabase.preferenceDao().getLongValue(s);
            int n = 0;
            int intValue;
            if (longValue != null) {
                intValue = longValue.intValue();
            }
            else {
                intValue = 0;
            }
            if (intValue != Integer.MAX_VALUE) {
                n = intValue + 1;
            }
            this.update(s, n);
            this.mWorkDatabase.setTransactionSuccessful();
            return intValue;
        }
        finally {
            this.mWorkDatabase.endTransaction();
        }
    }
    
    private void update(final String s, final int n) {
        this.mWorkDatabase.preferenceDao().insertPreference(new Preference(s, n));
    }
    
    public int nextAlarmManagerId() {
        synchronized (IdGenerator.class) {
            return this.nextId("next_alarm_manager_id");
        }
    }
    
    public int nextJobSchedulerIdWithRange(int n, final int n2) {
        synchronized (IdGenerator.class) {
            final int nextId = this.nextId("next_job_scheduler_id");
            if (nextId >= n && nextId <= n2) {
                n = nextId;
            }
            else {
                this.update("next_job_scheduler_id", n + 1);
            }
            return n;
        }
    }
}

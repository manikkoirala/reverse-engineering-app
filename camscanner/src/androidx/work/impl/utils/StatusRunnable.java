// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import androidx.annotation.WorkerThread;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.work.WorkQuery;
import java.util.UUID;
import androidx.work.impl.model.WorkSpec;
import androidx.work.WorkInfo;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.work.impl.WorkManagerImpl;
import androidx.work.impl.utils.futures.SettableFuture;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public abstract class StatusRunnable<T> implements Runnable
{
    private final SettableFuture<T> mFuture;
    
    public StatusRunnable() {
        this.mFuture = SettableFuture.create();
    }
    
    @NonNull
    public static StatusRunnable<List<WorkInfo>> forStringIds(@NonNull final WorkManagerImpl workManagerImpl, @NonNull final List<String> list) {
        return new StatusRunnable<List<WorkInfo>>(workManagerImpl, list) {
            final List val$ids;
            final WorkManagerImpl val$workManager;
            
            public List<WorkInfo> runInternal() {
                return WorkSpec.WORK_INFO_MAPPER.apply(this.val$workManager.getWorkDatabase().workSpecDao().getWorkStatusPojoForIds(this.val$ids));
            }
        };
    }
    
    @NonNull
    public static StatusRunnable<List<WorkInfo>> forTag(@NonNull final WorkManagerImpl workManagerImpl, @NonNull final String s) {
        return new StatusRunnable<List<WorkInfo>>(workManagerImpl, s) {
            final String val$tag;
            final WorkManagerImpl val$workManager;
            
            @Override
            List<WorkInfo> runInternal() {
                return WorkSpec.WORK_INFO_MAPPER.apply(this.val$workManager.getWorkDatabase().workSpecDao().getWorkStatusPojoForTag(this.val$tag));
            }
        };
    }
    
    @NonNull
    public static StatusRunnable<WorkInfo> forUUID(@NonNull final WorkManagerImpl workManagerImpl, @NonNull final UUID uuid) {
        return new StatusRunnable<WorkInfo>(workManagerImpl, uuid) {
            final UUID val$id;
            final WorkManagerImpl val$workManager;
            
            @Override
            WorkInfo runInternal() {
                final WorkSpec.WorkInfoPojo workStatusPojoForId = this.val$workManager.getWorkDatabase().workSpecDao().getWorkStatusPojoForId(this.val$id.toString());
                WorkInfo workInfo;
                if (workStatusPojoForId != null) {
                    workInfo = workStatusPojoForId.toWorkInfo();
                }
                else {
                    workInfo = null;
                }
                return workInfo;
            }
        };
    }
    
    @NonNull
    public static StatusRunnable<List<WorkInfo>> forUniqueWork(@NonNull final WorkManagerImpl workManagerImpl, @NonNull final String s) {
        return new StatusRunnable<List<WorkInfo>>(workManagerImpl, s) {
            final String val$name;
            final WorkManagerImpl val$workManager;
            
            @Override
            List<WorkInfo> runInternal() {
                return WorkSpec.WORK_INFO_MAPPER.apply(this.val$workManager.getWorkDatabase().workSpecDao().getWorkStatusPojoForName(this.val$name));
            }
        };
    }
    
    @NonNull
    public static StatusRunnable<List<WorkInfo>> forWorkQuerySpec(@NonNull final WorkManagerImpl workManagerImpl, @NonNull final WorkQuery workQuery) {
        return new StatusRunnable<List<WorkInfo>>(workManagerImpl, workQuery) {
            final WorkQuery val$querySpec;
            final WorkManagerImpl val$workManager;
            
            @Override
            List<WorkInfo> runInternal() {
                return WorkSpec.WORK_INFO_MAPPER.apply(this.val$workManager.getWorkDatabase().rawWorkInfoDao().getWorkInfoPojos(RawQueries.workQueryToRawQuery(this.val$querySpec)));
            }
        };
    }
    
    @NonNull
    public ListenableFuture<T> getFuture() {
        return (ListenableFuture<T>)this.mFuture;
    }
    
    @Override
    public void run() {
        try {
            this.mFuture.set(this.runInternal());
        }
        finally {
            final Throwable exception;
            this.mFuture.setException(exception);
        }
    }
    
    @WorkerThread
    abstract T runInternal();
}

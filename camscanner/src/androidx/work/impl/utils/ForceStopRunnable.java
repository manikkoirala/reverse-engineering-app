// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import androidx.annotation.Nullable;
import android.content.BroadcastReceiver;
import androidx.work.InitializationExceptionHandler;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteTableLockedException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteAccessPermException;
import androidx.work.impl.WorkDatabasePathHelper;
import androidx.work.Configuration;
import android.text.TextUtils;
import android.app.ApplicationExitInfo;
import android.app.ActivityManager;
import androidx.work.impl.Schedulers;
import java.util.Iterator;
import java.util.List;
import androidx.work.impl.model.WorkProgressDao;
import androidx.work.impl.model.WorkSpecDao;
import androidx.work.impl.WorkDatabase;
import androidx.work.WorkInfo;
import androidx.work.impl.model.WorkSpec;
import androidx.work.impl.background.systemjob.SystemJobScheduler;
import android.os.Build$VERSION;
import android.annotation.SuppressLint;
import androidx.core.os.BuildCompat;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Intent;
import androidx.annotation.NonNull;
import java.util.concurrent.TimeUnit;
import androidx.work.Logger;
import androidx.work.impl.WorkManagerImpl;
import android.content.Context;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class ForceStopRunnable implements Runnable
{
    @VisibleForTesting
    static final String ACTION_FORCE_STOP_RESCHEDULE = "ACTION_FORCE_STOP_RESCHEDULE";
    private static final int ALARM_ID = -1;
    private static final long BACKOFF_DURATION_MS = 300L;
    @VisibleForTesting
    static final int MAX_ATTEMPTS = 3;
    private static final String TAG;
    private static final long TEN_YEARS;
    private final Context mContext;
    private int mRetryCount;
    private final WorkManagerImpl mWorkManager;
    
    static {
        TAG = Logger.tagWithPrefix("ForceStopRunnable");
        TEN_YEARS = TimeUnit.DAYS.toMillis(3650L);
    }
    
    public ForceStopRunnable(@NonNull final Context context, @NonNull final WorkManagerImpl mWorkManager) {
        this.mContext = context.getApplicationContext();
        this.mWorkManager = mWorkManager;
        this.mRetryCount = 0;
    }
    
    @VisibleForTesting
    static Intent getIntent(final Context context) {
        final Intent intent = new Intent();
        intent.setComponent(new ComponentName(context, (Class)BroadcastReceiver.class));
        intent.setAction("ACTION_FORCE_STOP_RESCHEDULE");
        return intent;
    }
    
    private static PendingIntent getPendingIntent(final Context context, final int n) {
        return PendingIntent.getBroadcast(context, -1, getIntent(context), n);
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    static void setAlarm(final Context context) {
        final AlarmManager alarmManager = (AlarmManager)context.getSystemService("alarm");
        int n;
        if (BuildCompat.isAtLeastS()) {
            n = 167772160;
        }
        else {
            n = 134217728;
        }
        final PendingIntent pendingIntent = getPendingIntent(context, n);
        final long currentTimeMillis = System.currentTimeMillis();
        final long ten_YEARS = ForceStopRunnable.TEN_YEARS;
        if (alarmManager != null) {
            alarmManager.setExact(0, currentTimeMillis + ten_YEARS, pendingIntent);
        }
    }
    
    @VisibleForTesting
    public boolean cleanUp() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        boolean b = false;
        final boolean b2 = sdk_INT >= 23 && SystemJobScheduler.reconcileJobs(this.mContext, this.mWorkManager);
        final WorkDatabase workDatabase = this.mWorkManager.getWorkDatabase();
        final WorkSpecDao workSpecDao = workDatabase.workSpecDao();
        final WorkProgressDao workProgressDao = workDatabase.workProgressDao();
        workDatabase.beginTransaction();
        try {
            final List<WorkSpec> runningWork = workSpecDao.getRunningWork();
            final boolean b3 = runningWork != null && !runningWork.isEmpty();
            if (b3) {
                for (final WorkSpec workSpec : runningWork) {
                    workSpecDao.setState(WorkInfo.State.ENQUEUED, workSpec.id);
                    workSpecDao.markWorkSpecScheduled(workSpec.id, -1L);
                }
            }
            workProgressDao.deleteAll();
            workDatabase.setTransactionSuccessful();
            workDatabase.endTransaction();
            if (b3 || b2) {
                b = true;
            }
            return b;
        }
        finally {
            workDatabase.endTransaction();
        }
    }
    
    @VisibleForTesting
    public void forceStopRunnable() {
        final boolean cleanUp = this.cleanUp();
        if (this.shouldRescheduleWorkers()) {
            Logger.get().debug(ForceStopRunnable.TAG, "Rescheduling Workers.", new Throwable[0]);
            this.mWorkManager.rescheduleEligibleWork();
            this.mWorkManager.getPreferenceUtils().setNeedsReschedule(false);
        }
        else if (this.isForceStopped()) {
            Logger.get().debug(ForceStopRunnable.TAG, "Application was force-stopped, rescheduling.", new Throwable[0]);
            this.mWorkManager.rescheduleEligibleWork();
        }
        else if (cleanUp) {
            Logger.get().debug(ForceStopRunnable.TAG, "Found unfinished work, scheduling it.", new Throwable[0]);
            Schedulers.schedule(this.mWorkManager.getConfiguration(), this.mWorkManager.getWorkDatabase(), this.mWorkManager.getSchedulers());
        }
    }
    
    @SuppressLint({ "ClassVerificationFailure" })
    @VisibleForTesting
    public boolean isForceStopped() {
        Object o = null;
        try {
            int n;
            if (BuildCompat.isAtLeastS()) {
                n = 570425344;
            }
            else {
                n = 536870912;
            }
            o = getPendingIntent(this.mContext, n);
            if (Build$VERSION.SDK_INT >= 30) {
                if (o != null) {
                    ((PendingIntent)o).cancel();
                }
                o = \u3007080.\u3007080((ActivityManager)this.mContext.getSystemService("activity"), (String)null, 0, 0);
                if (o != null && !((List)o).isEmpty()) {
                    for (int i = 0; i < ((List)o).size(); ++i) {
                        if (\u3007o00\u3007\u3007Oo.\u3007080((ApplicationExitInfo)((List)o).get(i)) == 10) {
                            return true;
                        }
                    }
                }
            }
            else if (o == null) {
                setAlarm(this.mContext);
                return true;
            }
            return false;
        }
        catch (final IllegalArgumentException o) {}
        catch (final SecurityException ex) {}
        Logger.get().warning(ForceStopRunnable.TAG, "Ignoring exception", (Throwable)o);
        return true;
    }
    
    @VisibleForTesting
    public boolean multiProcessChecks() {
        final Configuration configuration = this.mWorkManager.getConfiguration();
        if (TextUtils.isEmpty((CharSequence)configuration.getDefaultProcessName())) {
            Logger.get().debug(ForceStopRunnable.TAG, "The default process name was not specified.", new Throwable[0]);
            return true;
        }
        final boolean defaultProcess = ProcessUtils.isDefaultProcess(this.mContext, configuration);
        Logger.get().debug(ForceStopRunnable.TAG, String.format("Is default app process = %s", defaultProcess), new Throwable[0]);
        return defaultProcess;
    }
    
    @Override
    public void run() {
        try {
            if (!this.multiProcessChecks()) {
                return;
            }
            while (true) {
                WorkDatabasePathHelper.migrateDatabase(this.mContext);
                Logger.get().debug(ForceStopRunnable.TAG, "Performing cleanup operations.", new Throwable[0]);
                try {
                    this.forceStopRunnable();
                    break;
                }
                catch (final SQLiteAccessPermException cause) {}
                catch (final SQLiteConstraintException cause) {}
                catch (final SQLiteTableLockedException cause) {}
                catch (final SQLiteDatabaseLockedException cause) {}
                catch (final SQLiteDatabaseCorruptException cause) {}
                catch (final SQLiteCantOpenDatabaseException ex) {}
                final int mRetryCount = this.mRetryCount + 1;
                this.mRetryCount = mRetryCount;
                if (mRetryCount >= 3) {
                    final Logger value = Logger.get();
                    final String tag = ForceStopRunnable.TAG;
                    final SQLiteAccessPermException cause;
                    value.error(tag, "The file system on the device is in a bad state. WorkManager cannot access the app's internal data store.", (Throwable)cause);
                    final IllegalStateException ex2 = new IllegalStateException("The file system on the device is in a bad state. WorkManager cannot access the app's internal data store.", (Throwable)cause);
                    final InitializationExceptionHandler exceptionHandler = this.mWorkManager.getConfiguration().getExceptionHandler();
                    if (exceptionHandler != null) {
                        Logger.get().debug(tag, "Routing exception to the specified exception handler", ex2);
                        exceptionHandler.handleException(ex2);
                        break;
                    }
                    throw ex2;
                }
                else {
                    final SQLiteAccessPermException cause;
                    Logger.get().debug(ForceStopRunnable.TAG, String.format("Retrying after %s", mRetryCount * 300L), (Throwable)cause);
                    this.sleep(this.mRetryCount * 300L);
                }
            }
        }
        finally {
            this.mWorkManager.onForceStopRunnableCompleted();
        }
    }
    
    @VisibleForTesting
    boolean shouldRescheduleWorkers() {
        return this.mWorkManager.getPreferenceUtils().getNeedsReschedule();
    }
    
    @VisibleForTesting
    public void sleep(final long n) {
        try {
            Thread.sleep(n);
        }
        catch (final InterruptedException ex) {}
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static class BroadcastReceiver extends android.content.BroadcastReceiver
    {
        private static final String TAG;
        
        static {
            TAG = Logger.tagWithPrefix("ForceStopRunnable$Rcvr");
        }
        
        public void onReceive(@NonNull final Context alarm, @Nullable final Intent intent) {
            if (intent != null && "ACTION_FORCE_STOP_RESCHEDULE".equals(intent.getAction())) {
                Logger.get().verbose(BroadcastReceiver.TAG, "Rescheduling alarm that keeps track of force-stops.", new Throwable[0]);
                ForceStopRunnable.setAlarm(alarm);
            }
        }
    }
}

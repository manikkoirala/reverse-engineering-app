// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import androidx.work.Operation;
import androidx.work.impl.WorkManagerImpl;
import androidx.work.impl.OperationImpl;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class PruneWorkRunnable implements Runnable
{
    private final OperationImpl mOperation;
    private final WorkManagerImpl mWorkManagerImpl;
    
    public PruneWorkRunnable(final WorkManagerImpl mWorkManagerImpl) {
        this.mWorkManagerImpl = mWorkManagerImpl;
        this.mOperation = new OperationImpl();
    }
    
    public Operation getOperation() {
        return this.mOperation;
    }
    
    @Override
    public void run() {
        try {
            this.mWorkManagerImpl.getWorkDatabase().workSpecDao().pruneFinishedWorkWithZeroDependentsIgnoringKeepForAtLeast();
            this.mOperation.setState((Operation.State)Operation.SUCCESS);
        }
        finally {
            final Throwable t;
            this.mOperation.setState((Operation.State)new Operation.State.FAILURE(t));
        }
    }
}

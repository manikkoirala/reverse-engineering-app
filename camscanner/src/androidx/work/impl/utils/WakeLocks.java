// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import java.util.Iterator;
import android.os.PowerManager;
import androidx.annotation.NonNull;
import android.content.Context;
import java.util.Map;
import java.util.HashMap;
import androidx.work.Logger;
import android.os.PowerManager$WakeLock;
import java.util.WeakHashMap;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class WakeLocks
{
    private static final String TAG;
    private static final WeakHashMap<PowerManager$WakeLock, String> sWakeLocks;
    
    static {
        TAG = Logger.tagWithPrefix("WakeLocks");
        sWakeLocks = new WeakHashMap<PowerManager$WakeLock, String>();
    }
    
    private WakeLocks() {
    }
    
    public static void checkWakeLocks() {
        final HashMap hashMap = new HashMap();
        Object o = WakeLocks.sWakeLocks;
        synchronized (o) {
            hashMap.putAll((Map)o);
            monitorexit(o);
            o = hashMap.keySet().iterator();
            while (((Iterator)o).hasNext()) {
                final PowerManager$WakeLock powerManager$WakeLock = (PowerManager$WakeLock)((Iterator)o).next();
                if (powerManager$WakeLock != null && powerManager$WakeLock.isHeld()) {
                    Logger.get().warning(WakeLocks.TAG, String.format("WakeLock held for %s", hashMap.get(powerManager$WakeLock)), new Throwable[0]);
                }
            }
        }
    }
    
    public static PowerManager$WakeLock newWakeLock(@NonNull final Context context, @NonNull String sWakeLocks) {
        final PowerManager powerManager = (PowerManager)context.getApplicationContext().getSystemService("power");
        final StringBuilder sb = new StringBuilder();
        sb.append("WorkManager: ");
        sb.append(sWakeLocks);
        final String string = sb.toString();
        final PowerManager$WakeLock wakeLock = powerManager.newWakeLock(1, string);
        sWakeLocks = (String)WakeLocks.sWakeLocks;
        synchronized (sWakeLocks) {
            ((WeakHashMap<PowerManager$WakeLock, String>)sWakeLocks).put(wakeLock, string);
            return wakeLock;
        }
    }
}

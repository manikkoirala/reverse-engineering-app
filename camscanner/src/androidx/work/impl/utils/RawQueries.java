// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import java.util.Iterator;
import java.util.List;
import androidx.sqlite.db.SimpleSQLiteQuery;
import java.util.UUID;
import java.util.Collection;
import androidx.work.impl.model.WorkTypeConverters;
import androidx.work.WorkInfo;
import java.util.ArrayList;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.work.WorkQuery;
import androidx.annotation.NonNull;

public final class RawQueries
{
    private RawQueries() {
    }
    
    private static void bindings(@NonNull final StringBuilder sb, final int n) {
        if (n <= 0) {
            return;
        }
        sb.append("?");
        for (int i = 1; i < n; ++i) {
            sb.append(",");
            sb.append("?");
        }
    }
    
    @NonNull
    public static SupportSQLiteQuery workQueryToRawQuery(@NonNull final WorkQuery workQuery) {
        final ArrayList list = new ArrayList();
        final StringBuilder sb = new StringBuilder("SELECT * FROM workspec");
        final List<WorkInfo.State> states = workQuery.getStates();
        final boolean empty = states.isEmpty();
        final String s = " AND";
        String str = " WHERE";
        if (!empty) {
            final ArrayList list2 = new ArrayList(states.size());
            final Iterator iterator = states.iterator();
            while (iterator.hasNext()) {
                list2.add((Object)WorkTypeConverters.stateToInt((WorkInfo.State)iterator.next()));
            }
            sb.append(" WHERE");
            sb.append(" state IN (");
            bindings(sb, list2.size());
            sb.append(")");
            list.addAll(list2);
            str = " AND";
        }
        final List<UUID> ids = workQuery.getIds();
        String str2 = str;
        if (!ids.isEmpty()) {
            final ArrayList list3 = new ArrayList(ids.size());
            final Iterator iterator2 = ids.iterator();
            while (iterator2.hasNext()) {
                list3.add((Object)((UUID)iterator2.next()).toString());
            }
            sb.append(str);
            sb.append(" id IN (");
            bindings(sb, ids.size());
            sb.append(")");
            list.addAll(list3);
            str2 = " AND";
        }
        final List<String> tags = workQuery.getTags();
        String str3;
        if (!tags.isEmpty()) {
            sb.append(str2);
            sb.append(" id IN (SELECT work_spec_id FROM worktag WHERE tag IN (");
            bindings(sb, tags.size());
            sb.append("))");
            list.addAll(tags);
            str3 = s;
        }
        else {
            str3 = str2;
        }
        final List<String> uniqueWorkNames = workQuery.getUniqueWorkNames();
        if (!uniqueWorkNames.isEmpty()) {
            sb.append(str3);
            sb.append(" id IN (SELECT work_spec_id FROM workname WHERE name IN (");
            bindings(sb, uniqueWorkNames.size());
            sb.append("))");
            list.addAll(uniqueWorkNames);
        }
        sb.append(";");
        return new SimpleSQLiteQuery(sb.toString(), list.toArray());
    }
}

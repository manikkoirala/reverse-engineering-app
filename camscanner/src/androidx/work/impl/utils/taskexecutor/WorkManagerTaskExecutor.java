// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils.taskexecutor;

import android.os.Looper;
import androidx.annotation.NonNull;
import android.os.Handler;
import java.util.concurrent.Executor;
import androidx.work.impl.utils.SerialExecutor;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class WorkManagerTaskExecutor implements TaskExecutor
{
    private final SerialExecutor mBackgroundExecutor;
    private final Executor mMainThreadExecutor;
    private final Handler mMainThreadHandler;
    
    public WorkManagerTaskExecutor(@NonNull final Executor executor) {
        this.mMainThreadHandler = new Handler(Looper.getMainLooper());
        this.mMainThreadExecutor = new Executor() {
            final WorkManagerTaskExecutor this$0;
            
            @Override
            public void execute(@NonNull final Runnable runnable) {
                this.this$0.postToMainThread(runnable);
            }
        };
        this.mBackgroundExecutor = new SerialExecutor(executor);
    }
    
    @Override
    public void executeOnBackgroundThread(final Runnable runnable) {
        this.mBackgroundExecutor.execute(runnable);
    }
    
    @NonNull
    @Override
    public SerialExecutor getBackgroundExecutor() {
        return this.mBackgroundExecutor;
    }
    
    @Override
    public Executor getMainThreadExecutor() {
        return this.mMainThreadExecutor;
    }
    
    @Override
    public void postToMainThread(final Runnable runnable) {
        this.mMainThreadHandler.post(runnable);
    }
}

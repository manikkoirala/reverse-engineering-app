// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils.taskexecutor;

import java.util.concurrent.Executor;
import androidx.work.impl.utils.SerialExecutor;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public interface TaskExecutor
{
    void executeOnBackgroundThread(final Runnable p0);
    
    SerialExecutor getBackgroundExecutor();
    
    Executor getMainThreadExecutor();
    
    void postToMainThread(final Runnable p0);
}

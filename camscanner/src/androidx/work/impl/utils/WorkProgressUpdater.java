// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import androidx.work.impl.model.WorkSpec;
import androidx.work.impl.model.WorkProgress;
import androidx.work.WorkInfo;
import androidx.work.impl.utils.futures.SettableFuture;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.work.Data;
import java.util.UUID;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.work.Logger;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import androidx.annotation.RestrictTo;
import androidx.work.ProgressUpdater;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class WorkProgressUpdater implements ProgressUpdater
{
    static final String TAG;
    final TaskExecutor mTaskExecutor;
    final WorkDatabase mWorkDatabase;
    
    static {
        TAG = Logger.tagWithPrefix("WorkProgressUpdater");
    }
    
    public WorkProgressUpdater(@NonNull final WorkDatabase mWorkDatabase, @NonNull final TaskExecutor mTaskExecutor) {
        this.mWorkDatabase = mWorkDatabase;
        this.mTaskExecutor = mTaskExecutor;
    }
    
    @NonNull
    @Override
    public ListenableFuture<Void> updateProgress(@NonNull final Context context, @NonNull final UUID uuid, @NonNull final Data data) {
        final SettableFuture<Object> create = SettableFuture.create();
        this.mTaskExecutor.executeOnBackgroundThread(new Runnable(this, uuid, data, create) {
            final WorkProgressUpdater this$0;
            final Data val$data;
            final SettableFuture val$future;
            final UUID val$id;
            
            @Override
            public void run() {
                final String string = this.val$id.toString();
                final Logger value = Logger.get();
                final String tag = WorkProgressUpdater.TAG;
                value.debug(tag, String.format("Updating progress for %s (%s)", this.val$id, this.val$data), new Throwable[0]);
                this.this$0.mWorkDatabase.beginTransaction();
                final Throwable t2;
                try {
                    final WorkSpec workSpec = this.this$0.mWorkDatabase.workSpecDao().getWorkSpec(string);
                    if (workSpec != null) {
                        if (workSpec.state == WorkInfo.State.RUNNING) {
                            this.this$0.mWorkDatabase.workProgressDao().insert(new WorkProgress(string, this.val$data));
                        }
                        else {
                            Logger.get().warning(tag, String.format("Ignoring setProgressAsync(...). WorkSpec (%s) is not in a RUNNING state.", string), new Throwable[0]);
                        }
                        this.val$future.set(null);
                        this.this$0.mWorkDatabase.setTransactionSuccessful();
                        return;
                    }
                    throw new IllegalStateException("Calls to setProgressAsync() must complete before a ListenableWorker signals completion of work by returning an instance of Result.");
                }
                finally {
                    final Logger logger = Logger.get();
                    final String s = WorkProgressUpdater.TAG;
                    final String s2 = "Error updating Worker progress";
                    final int n = 1;
                    final Throwable[] array = new Throwable[n];
                    final int n2 = 0;
                    final Throwable t = t2;
                    array[n2] = t;
                    logger.error(s, s2, array);
                    final Runnable runnable = this;
                    final SettableFuture settableFuture = runnable.val$future;
                    final Throwable t3 = t2;
                    settableFuture.setException(t3);
                }
                try {
                    final Logger logger = Logger.get();
                    final String s = WorkProgressUpdater.TAG;
                    final String s2 = "Error updating Worker progress";
                    final int n = 1;
                    final Throwable[] array = new Throwable[n];
                    final int n2 = 0;
                    final Throwable t = t2;
                    array[n2] = t;
                    logger.error(s, s2, array);
                    final Runnable runnable = this;
                    final SettableFuture settableFuture = runnable.val$future;
                    final Throwable t3 = t2;
                    settableFuture.setException(t3);
                }
                finally {
                    this.this$0.mWorkDatabase.endTransaction();
                }
            }
        });
        return (ListenableFuture<Void>)create;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import androidx.work.impl.model.WorkSpecDao;
import androidx.work.impl.Processor;
import androidx.work.impl.WorkDatabase;
import androidx.work.WorkInfo;
import androidx.annotation.NonNull;
import androidx.work.Logger;
import androidx.work.impl.WorkManagerImpl;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class StopWorkRunnable implements Runnable
{
    private static final String TAG;
    private final boolean mStopInForeground;
    private final WorkManagerImpl mWorkManagerImpl;
    private final String mWorkSpecId;
    
    static {
        TAG = Logger.tagWithPrefix("StopWorkRunnable");
    }
    
    public StopWorkRunnable(@NonNull final WorkManagerImpl mWorkManagerImpl, @NonNull final String mWorkSpecId, final boolean mStopInForeground) {
        this.mWorkManagerImpl = mWorkManagerImpl;
        this.mWorkSpecId = mWorkSpecId;
        this.mStopInForeground = mStopInForeground;
    }
    
    @Override
    public void run() {
        final WorkDatabase workDatabase = this.mWorkManagerImpl.getWorkDatabase();
        final Processor processor = this.mWorkManagerImpl.getProcessor();
        final WorkSpecDao workSpecDao = workDatabase.workSpecDao();
        workDatabase.beginTransaction();
        try {
            final boolean enqueuedInForeground = processor.isEnqueuedInForeground(this.mWorkSpecId);
            boolean b;
            if (this.mStopInForeground) {
                b = this.mWorkManagerImpl.getProcessor().stopForegroundWork(this.mWorkSpecId);
            }
            else {
                if (!enqueuedInForeground && workSpecDao.getState(this.mWorkSpecId) == WorkInfo.State.RUNNING) {
                    workSpecDao.setState(WorkInfo.State.ENQUEUED, this.mWorkSpecId);
                }
                b = this.mWorkManagerImpl.getProcessor().stopWork(this.mWorkSpecId);
            }
            Logger.get().debug(StopWorkRunnable.TAG, String.format("StopWorkRunnable for %s; Processor.stopWork = %s", this.mWorkSpecId, b), new Throwable[0]);
            workDatabase.setTransactionSuccessful();
        }
        finally {
            workDatabase.endTransaction();
        }
    }
}

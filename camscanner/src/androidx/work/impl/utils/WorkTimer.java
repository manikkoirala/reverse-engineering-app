// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import java.util.concurrent.TimeUnit;
import androidx.annotation.VisibleForTesting;
import java.util.HashMap;
import java.util.concurrent.Executors;
import androidx.annotation.NonNull;
import androidx.work.Logger;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class WorkTimer
{
    private static final String TAG;
    private final ThreadFactory mBackgroundThreadFactory;
    private final ScheduledExecutorService mExecutorService;
    final Map<String, TimeLimitExceededListener> mListeners;
    final Object mLock;
    final Map<String, WorkTimerRunnable> mTimerMap;
    
    static {
        TAG = Logger.tagWithPrefix("WorkTimer");
    }
    
    public WorkTimer() {
        final ThreadFactory threadFactory = new ThreadFactory() {
            private int mThreadsCreated = 0;
            final WorkTimer this$0;
            
            @Override
            public Thread newThread(@NonNull final Runnable runnable) {
                final Thread thread = Executors.defaultThreadFactory().newThread(runnable);
                final StringBuilder sb = new StringBuilder();
                sb.append("WorkManager-WorkTimer-thread-");
                sb.append(this.mThreadsCreated);
                thread.setName(sb.toString());
                ++this.mThreadsCreated;
                return thread;
            }
        };
        this.mBackgroundThreadFactory = threadFactory;
        this.mTimerMap = new HashMap<String, WorkTimerRunnable>();
        this.mListeners = new HashMap<String, TimeLimitExceededListener>();
        this.mLock = new Object();
        this.mExecutorService = Executors.newSingleThreadScheduledExecutor(threadFactory);
    }
    
    @NonNull
    @VisibleForTesting
    public ScheduledExecutorService getExecutorService() {
        return this.mExecutorService;
    }
    
    @NonNull
    @VisibleForTesting
    public Map<String, TimeLimitExceededListener> getListeners() {
        synchronized (this) {
            return this.mListeners;
        }
    }
    
    @NonNull
    @VisibleForTesting
    public Map<String, WorkTimerRunnable> getTimerMap() {
        synchronized (this) {
            return this.mTimerMap;
        }
    }
    
    public void onDestroy() {
        if (!this.mExecutorService.isShutdown()) {
            this.mExecutorService.shutdownNow();
        }
    }
    
    public void startTimer(@NonNull final String s, final long n, @NonNull final TimeLimitExceededListener timeLimitExceededListener) {
        synchronized (this.mLock) {
            Logger.get().debug(WorkTimer.TAG, String.format("Starting timer for %s", s), new Throwable[0]);
            this.stopTimer(s);
            final WorkTimerRunnable workTimerRunnable = new WorkTimerRunnable(this, s);
            this.mTimerMap.put(s, workTimerRunnable);
            this.mListeners.put(s, timeLimitExceededListener);
            this.mExecutorService.schedule(workTimerRunnable, n, TimeUnit.MILLISECONDS);
        }
    }
    
    public void stopTimer(@NonNull final String s) {
        synchronized (this.mLock) {
            if (this.mTimerMap.remove(s) != null) {
                Logger.get().debug(WorkTimer.TAG, String.format("Stopping timer for %s", s), new Throwable[0]);
                this.mListeners.remove(s);
            }
        }
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public interface TimeLimitExceededListener
    {
        void onTimeLimitExceeded(@NonNull final String p0);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static class WorkTimerRunnable implements Runnable
    {
        static final String TAG = "WrkTimerRunnable";
        private final String mWorkSpecId;
        private final WorkTimer mWorkTimer;
        
        WorkTimerRunnable(@NonNull final WorkTimer mWorkTimer, @NonNull final String mWorkSpecId) {
            this.mWorkTimer = mWorkTimer;
            this.mWorkSpecId = mWorkSpecId;
        }
        
        @Override
        public void run() {
            synchronized (this.mWorkTimer.mLock) {
                if (this.mWorkTimer.mTimerMap.remove(this.mWorkSpecId) != null) {
                    final TimeLimitExceededListener timeLimitExceededListener = this.mWorkTimer.mListeners.remove(this.mWorkSpecId);
                    if (timeLimitExceededListener != null) {
                        timeLimitExceededListener.onTimeLimitExceeded(this.mWorkSpecId);
                    }
                }
                else {
                    Logger.get().debug("WrkTimerRunnable", String.format("Timer with %s is already marked as complete.", this.mWorkSpecId), new Throwable[0]);
                }
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import androidx.work.WorkInfo;
import androidx.work.impl.foreground.SystemForegroundDispatcher;
import androidx.work.impl.utils.futures.SettableFuture;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.work.ForegroundInfo;
import java.util.UUID;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.work.impl.WorkDatabase;
import androidx.work.Logger;
import androidx.work.impl.model.WorkSpecDao;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import androidx.work.impl.foreground.ForegroundProcessor;
import androidx.annotation.RestrictTo;
import androidx.work.ForegroundUpdater;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class WorkForegroundUpdater implements ForegroundUpdater
{
    private static final String TAG;
    final ForegroundProcessor mForegroundProcessor;
    private final TaskExecutor mTaskExecutor;
    final WorkSpecDao mWorkSpecDao;
    
    static {
        TAG = Logger.tagWithPrefix("WMFgUpdater");
    }
    
    public WorkForegroundUpdater(@NonNull final WorkDatabase workDatabase, @NonNull final ForegroundProcessor mForegroundProcessor, @NonNull final TaskExecutor mTaskExecutor) {
        this.mForegroundProcessor = mForegroundProcessor;
        this.mTaskExecutor = mTaskExecutor;
        this.mWorkSpecDao = workDatabase.workSpecDao();
    }
    
    @NonNull
    @Override
    public ListenableFuture<Void> setForegroundAsync(@NonNull final Context context, @NonNull final UUID uuid, @NonNull final ForegroundInfo foregroundInfo) {
        final SettableFuture<Object> create = SettableFuture.create();
        this.mTaskExecutor.executeOnBackgroundThread(new Runnable(this, create, uuid, foregroundInfo, context) {
            final WorkForegroundUpdater this$0;
            final Context val$context;
            final ForegroundInfo val$foregroundInfo;
            final SettableFuture val$future;
            final UUID val$id;
            
            @Override
            public void run() {
                try {
                    if (!this.val$future.isCancelled()) {
                        final String string = this.val$id.toString();
                        final WorkInfo.State state = this.this$0.mWorkSpecDao.getState(string);
                        if (state == null || state.isFinished()) {
                            throw new IllegalStateException("Calls to setForegroundAsync() must complete before a ListenableWorker signals completion of work by returning an instance of Result.");
                        }
                        this.this$0.mForegroundProcessor.startForeground(string, this.val$foregroundInfo);
                        this.val$context.startService(SystemForegroundDispatcher.createNotifyIntent(this.val$context, string, this.val$foregroundInfo));
                    }
                    this.val$future.set(null);
                }
                finally {
                    final Throwable exception;
                    this.val$future.setException(exception);
                }
            }
        });
        return (ListenableFuture<Void>)create;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import android.text.TextUtils;
import androidx.work.Configuration;
import androidx.annotation.Nullable;
import android.annotation.SuppressLint;
import java.util.Iterator;
import java.util.List;
import java.lang.reflect.Method;
import android.app.ActivityManager$RunningAppProcessInfo;
import android.app.ActivityManager;
import android.os.Process;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.work.Logger;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class ProcessUtils
{
    private static final String TAG;
    
    static {
        TAG = Logger.tagWithPrefix("ProcessUtils");
    }
    
    private ProcessUtils() {
    }
    
    @SuppressLint({ "PrivateApi", "DiscouragedPrivateApi" })
    @Nullable
    public static String getProcessName(@NonNull final Context context) {
        if (Build$VERSION.SDK_INT >= 28) {
            return \u3007o\u3007.\u3007080();
        }
        try {
            final Method declaredMethod = Class.forName("android.app.ActivityThread", false, ProcessUtils.class.getClassLoader()).getDeclaredMethod("currentProcessName", (Class<?>[])new Class[0]);
            declaredMethod.setAccessible(true);
            final Object invoke = declaredMethod.invoke(null, new Object[0]);
            if (invoke instanceof String) {
                return (String)invoke;
            }
        }
        finally {
            final Throwable t;
            Logger.get().debug(ProcessUtils.TAG, "Unable to check ActivityThread for processName", t);
        }
        final int myPid = Process.myPid();
        final ActivityManager activityManager = (ActivityManager)context.getSystemService("activity");
        if (activityManager != null) {
            final List runningAppProcesses = activityManager.getRunningAppProcesses();
            if (runningAppProcesses != null && !runningAppProcesses.isEmpty()) {
                for (final ActivityManager$RunningAppProcessInfo activityManager$RunningAppProcessInfo : runningAppProcesses) {
                    if (activityManager$RunningAppProcessInfo.pid == myPid) {
                        return activityManager$RunningAppProcessInfo.processName;
                    }
                }
            }
        }
        return null;
    }
    
    public static boolean isDefaultProcess(@NonNull final Context context, @NonNull final Configuration configuration) {
        final String processName = getProcessName(context);
        if (!TextUtils.isEmpty((CharSequence)configuration.getDefaultProcessName())) {
            return TextUtils.equals((CharSequence)processName, (CharSequence)configuration.getDefaultProcessName());
        }
        return TextUtils.equals((CharSequence)processName, (CharSequence)context.getApplicationInfo().processName);
    }
}

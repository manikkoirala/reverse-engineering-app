// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import android.content.pm.PackageManager;
import androidx.annotation.NonNull;
import android.content.ComponentName;
import android.content.Context;
import androidx.work.Logger;

public class PackageManagerHelper
{
    private static final String TAG;
    
    static {
        TAG = Logger.tagWithPrefix("PackageManagerHelper");
    }
    
    private PackageManagerHelper() {
    }
    
    public static boolean isComponentExplicitlyEnabled(final Context context, final Class<?> clazz) {
        return isComponentExplicitlyEnabled(context, clazz.getName());
    }
    
    public static boolean isComponentExplicitlyEnabled(final Context context, final String s) {
        final int componentEnabledSetting = context.getPackageManager().getComponentEnabledSetting(new ComponentName(context, s));
        boolean b = true;
        if (componentEnabledSetting != 1) {
            b = false;
        }
        return b;
    }
    
    public static void setComponentEnabled(@NonNull final Context context, @NonNull final Class<?> clazz, final boolean b) {
        final String s = "enabled";
        try {
            final PackageManager packageManager = context.getPackageManager();
            final ComponentName componentName = new ComponentName(context, clazz.getName());
            int n;
            if (b) {
                n = 1;
            }
            else {
                n = 2;
            }
            packageManager.setComponentEnabledSetting(componentName, n, 1);
            final Logger value = Logger.get();
            final String tag = PackageManagerHelper.TAG;
            final String name = clazz.getName();
            String s2;
            if (b) {
                s2 = "enabled";
            }
            else {
                s2 = "disabled";
            }
            value.debug(tag, String.format("%s %s", name, s2), new Throwable[0]);
        }
        catch (final Exception ex) {
            final Logger value2 = Logger.get();
            final String tag2 = PackageManagerHelper.TAG;
            final String name2 = clazz.getName();
            String s3;
            if (b) {
                s3 = s;
            }
            else {
                s3 = "disabled";
            }
            value2.debug(tag2, String.format("%s could not be %s", name2, s3), ex);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils.futures;

import com.google.common.util.concurrent.ListenableFuture;
import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public final class SettableFuture<V> extends AbstractFuture<V>
{
    private SettableFuture() {
    }
    
    public static <V> SettableFuture<V> create() {
        return new SettableFuture<V>();
    }
    
    public boolean set(@Nullable final V v) {
        return super.set(v);
    }
    
    public boolean setException(final Throwable exception) {
        return super.setException(exception);
    }
    
    public boolean setFuture(final ListenableFuture<? extends V> future) {
        return super.setFuture(future);
    }
}

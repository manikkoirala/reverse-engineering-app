// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import androidx.annotation.VisibleForTesting;
import androidx.annotation.NonNull;
import java.util.ArrayDeque;
import java.util.concurrent.Executor;

public class SerialExecutor implements Executor
{
    private volatile Runnable mActive;
    private final Executor mExecutor;
    private final Object mLock;
    private final ArrayDeque<Task> mTasks;
    
    public SerialExecutor(@NonNull final Executor mExecutor) {
        this.mExecutor = mExecutor;
        this.mTasks = new ArrayDeque<Task>();
        this.mLock = new Object();
    }
    
    @Override
    public void execute(@NonNull final Runnable runnable) {
        synchronized (this.mLock) {
            this.mTasks.add(new Task(this, runnable));
            if (this.mActive == null) {
                this.scheduleNext();
            }
        }
    }
    
    @NonNull
    @VisibleForTesting
    public Executor getDelegatedExecutor() {
        return this.mExecutor;
    }
    
    public boolean hasPendingTasks() {
        synchronized (this.mLock) {
            return !this.mTasks.isEmpty();
        }
    }
    
    void scheduleNext() {
        synchronized (this.mLock) {
            final Runnable mActive = this.mTasks.poll();
            this.mActive = mActive;
            if (mActive != null) {
                this.mExecutor.execute(this.mActive);
            }
        }
    }
    
    static class Task implements Runnable
    {
        final Runnable mRunnable;
        final SerialExecutor mSerialExecutor;
        
        Task(@NonNull final SerialExecutor mSerialExecutor, @NonNull final Runnable mRunnable) {
            this.mSerialExecutor = mSerialExecutor;
            this.mRunnable = mRunnable;
        }
        
        @Override
        public void run() {
            try {
                this.mRunnable.run();
            }
            finally {
                this.mSerialExecutor.scheduleNext();
            }
        }
    }
}

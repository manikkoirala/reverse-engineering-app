// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;
import java.util.concurrent.Executor;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class SynchronousExecutor implements Executor
{
    @Override
    public void execute(@NonNull final Runnable runnable) {
        runnable.run();
    }
}

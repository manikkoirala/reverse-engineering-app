// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import androidx.work.ForegroundInfo;
import androidx.core.os.BuildCompat;
import com.google.common.util.concurrent.ListenableFuture;
import android.annotation.SuppressLint;
import androidx.annotation.NonNull;
import androidx.work.Logger;
import androidx.work.ListenableWorker;
import androidx.work.impl.model.WorkSpec;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import androidx.work.impl.utils.futures.SettableFuture;
import androidx.work.ForegroundUpdater;
import android.content.Context;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class WorkForegroundRunnable implements Runnable
{
    static final String TAG;
    final Context mContext;
    final ForegroundUpdater mForegroundUpdater;
    final SettableFuture<Void> mFuture;
    final TaskExecutor mTaskExecutor;
    final WorkSpec mWorkSpec;
    final ListenableWorker mWorker;
    
    static {
        TAG = Logger.tagWithPrefix("WorkForegroundRunnable");
    }
    
    @SuppressLint({ "LambdaLast" })
    public WorkForegroundRunnable(@NonNull final Context mContext, @NonNull final WorkSpec mWorkSpec, @NonNull final ListenableWorker mWorker, @NonNull final ForegroundUpdater mForegroundUpdater, @NonNull final TaskExecutor mTaskExecutor) {
        this.mFuture = SettableFuture.create();
        this.mContext = mContext;
        this.mWorkSpec = mWorkSpec;
        this.mWorker = mWorker;
        this.mForegroundUpdater = mForegroundUpdater;
        this.mTaskExecutor = mTaskExecutor;
    }
    
    @NonNull
    public ListenableFuture<Void> getFuture() {
        return (ListenableFuture<Void>)this.mFuture;
    }
    
    @SuppressLint({ "UnsafeExperimentalUsageError" })
    @Override
    public void run() {
        if (this.mWorkSpec.expedited && !BuildCompat.isAtLeastS()) {
            final SettableFuture<Object> create = SettableFuture.create();
            this.mTaskExecutor.getMainThreadExecutor().execute(new Runnable(this, create) {
                final WorkForegroundRunnable this$0;
                final SettableFuture val$foregroundFuture;
                
                @Override
                public void run() {
                    this.val$foregroundFuture.setFuture(this.this$0.mWorker.getForegroundInfoAsync());
                }
            });
            create.addListener(new Runnable(this, create) {
                final WorkForegroundRunnable this$0;
                final SettableFuture val$foregroundFuture;
                
                @Override
                public void run() {
                    try {
                        final ForegroundInfo foregroundInfo = (ForegroundInfo)this.val$foregroundFuture.get();
                        if (foregroundInfo == null) {
                            throw new IllegalStateException(String.format("Worker was marked important (%s) but did not provide ForegroundInfo", this.this$0.mWorkSpec.workerClassName));
                        }
                        Logger.get().debug(WorkForegroundRunnable.TAG, String.format("Updating notification for %s", this.this$0.mWorkSpec.workerClassName), new Throwable[0]);
                        this.this$0.mWorker.setRunInForeground(true);
                        final WorkForegroundRunnable this$0 = this.this$0;
                        this$0.mFuture.setFuture(this$0.mForegroundUpdater.setForegroundAsync(this$0.mContext, this$0.mWorker.getId(), foregroundInfo));
                    }
                    finally {
                        final Throwable exception;
                        this.this$0.mFuture.setException(exception);
                    }
                }
            }, this.mTaskExecutor.getMainThreadExecutor());
            return;
        }
        this.mFuture.set(null);
    }
}

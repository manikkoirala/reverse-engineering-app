// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.utils;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.MediatorLiveData;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import androidx.arch.core.util.Function;
import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class LiveDataUtils
{
    private LiveDataUtils() {
    }
    
    public static <In, Out> LiveData<Out> dedupedMappedLiveDataFor(@NonNull final LiveData<In> liveData, @NonNull final Function<In, Out> function, @NonNull final TaskExecutor taskExecutor) {
        final Object o = new Object();
        final MediatorLiveData mediatorLiveData = new MediatorLiveData();
        mediatorLiveData.addSource(liveData, new Observer<In>(taskExecutor, o, function, mediatorLiveData) {
            Out mCurrentOutput = null;
            final Object val$lock;
            final Function val$mappingMethod;
            final MediatorLiveData val$outputLiveData;
            final TaskExecutor val$workTaskExecutor;
            
            @Override
            public void onChanged(@Nullable final In in) {
                this.val$workTaskExecutor.executeOnBackgroundThread(new Runnable(this, in) {
                    final LiveDataUtils$1 this$0;
                    final Object val$input;
                    
                    @Override
                    public void run() {
                        synchronized (this.this$0.val$lock) {
                            final Out apply = this.this$0.val$mappingMethod.apply(this.val$input);
                            final Observer<In> this$0 = this.this$0;
                            final Out mCurrentOutput = this$0.mCurrentOutput;
                            if (mCurrentOutput == null && apply != null) {
                                this$0.mCurrentOutput = apply;
                                this$0.val$outputLiveData.postValue(apply);
                            }
                            else if (mCurrentOutput != null && !mCurrentOutput.equals(apply)) {
                                final Observer<In> this$2 = this.this$0;
                                this$2.mCurrentOutput = apply;
                                this$2.val$outputLiveData.postValue(apply);
                            }
                        }
                    }
                });
            }
        });
        return mediatorLiveData;
    }
}

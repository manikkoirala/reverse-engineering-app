// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import java.util.concurrent.Future;
import java.util.concurrent.ExecutionException;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.work.WorkerParameters;
import androidx.core.content.ContextCompat;
import androidx.work.impl.utils.WakeLocks;
import androidx.work.ForegroundInfo;
import java.util.Iterator;
import android.content.Intent;
import androidx.work.impl.foreground.SystemForegroundDispatcher;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.HashMap;
import androidx.annotation.NonNull;
import androidx.work.Logger;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import java.util.List;
import androidx.annotation.Nullable;
import android.os.PowerManager$WakeLock;
import java.util.Map;
import androidx.work.Configuration;
import java.util.Set;
import android.content.Context;
import androidx.annotation.RestrictTo;
import androidx.work.impl.foreground.ForegroundProcessor;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class Processor implements ExecutionListener, ForegroundProcessor
{
    private static final String FOREGROUND_WAKELOCK_TAG = "ProcessorForegroundLck";
    private static final String TAG;
    private Context mAppContext;
    private Set<String> mCancelledIds;
    private Configuration mConfiguration;
    private Map<String, WorkerWrapper> mEnqueuedWorkMap;
    @Nullable
    private PowerManager$WakeLock mForegroundLock;
    private Map<String, WorkerWrapper> mForegroundWorkMap;
    private final Object mLock;
    private final List<ExecutionListener> mOuterListeners;
    private List<Scheduler> mSchedulers;
    private WorkDatabase mWorkDatabase;
    private TaskExecutor mWorkTaskExecutor;
    
    static {
        TAG = Logger.tagWithPrefix("Processor");
    }
    
    public Processor(@NonNull final Context mAppContext, @NonNull final Configuration mConfiguration, @NonNull final TaskExecutor mWorkTaskExecutor, @NonNull final WorkDatabase mWorkDatabase, @NonNull final List<Scheduler> mSchedulers) {
        this.mAppContext = mAppContext;
        this.mConfiguration = mConfiguration;
        this.mWorkTaskExecutor = mWorkTaskExecutor;
        this.mWorkDatabase = mWorkDatabase;
        this.mEnqueuedWorkMap = new HashMap<String, WorkerWrapper>();
        this.mForegroundWorkMap = new HashMap<String, WorkerWrapper>();
        this.mSchedulers = mSchedulers;
        this.mCancelledIds = new HashSet<String>();
        this.mOuterListeners = new ArrayList<ExecutionListener>();
        this.mForegroundLock = null;
        this.mLock = new Object();
    }
    
    private static boolean interrupt(@NonNull final String s, @Nullable final WorkerWrapper workerWrapper) {
        if (workerWrapper != null) {
            workerWrapper.interrupt();
            Logger.get().debug(Processor.TAG, String.format("WorkerWrapper interrupted for %s", s), new Throwable[0]);
            return true;
        }
        Logger.get().debug(Processor.TAG, String.format("WorkerWrapper could not be found for %s", s), new Throwable[0]);
        return false;
    }
    
    private void stopForegroundService() {
        synchronized (this.mLock) {
            if (!(this.mForegroundWorkMap.isEmpty() ^ true)) {
                final Intent stopForegroundIntent = SystemForegroundDispatcher.createStopForegroundIntent(this.mAppContext);
                try {
                    this.mAppContext.startService(stopForegroundIntent);
                }
                finally {
                    final Throwable t;
                    Logger.get().error(Processor.TAG, "Unable to stop foreground service", t);
                }
                final PowerManager$WakeLock mForegroundLock = this.mForegroundLock;
                if (mForegroundLock != null) {
                    mForegroundLock.release();
                    this.mForegroundLock = null;
                }
            }
        }
    }
    
    public void addExecutionListener(@NonNull final ExecutionListener executionListener) {
        synchronized (this.mLock) {
            this.mOuterListeners.add(executionListener);
        }
    }
    
    public boolean hasWork() {
        synchronized (this.mLock) {
            return !this.mEnqueuedWorkMap.isEmpty() || !this.mForegroundWorkMap.isEmpty();
        }
    }
    
    public boolean isCancelled(@NonNull final String s) {
        synchronized (this.mLock) {
            return this.mCancelledIds.contains(s);
        }
    }
    
    public boolean isEnqueued(@NonNull final String s) {
        synchronized (this.mLock) {
            return this.mEnqueuedWorkMap.containsKey(s) || this.mForegroundWorkMap.containsKey(s);
        }
    }
    
    public boolean isEnqueuedInForeground(@NonNull final String s) {
        synchronized (this.mLock) {
            return this.mForegroundWorkMap.containsKey(s);
        }
    }
    
    @Override
    public void onExecuted(@NonNull final String s, final boolean b) {
        synchronized (this.mLock) {
            this.mEnqueuedWorkMap.remove(s);
            Logger.get().debug(Processor.TAG, String.format("%s %s executed; reschedule = %s", this.getClass().getSimpleName(), s, b), new Throwable[0]);
            final Iterator<ExecutionListener> iterator = this.mOuterListeners.iterator();
            while (iterator.hasNext()) {
                iterator.next().onExecuted(s, b);
            }
        }
    }
    
    public void removeExecutionListener(@NonNull final ExecutionListener executionListener) {
        synchronized (this.mLock) {
            this.mOuterListeners.remove(executionListener);
        }
    }
    
    @Override
    public void startForeground(@NonNull final String s, @NonNull final ForegroundInfo foregroundInfo) {
        synchronized (this.mLock) {
            Logger.get().info(Processor.TAG, String.format("Moving WorkSpec (%s) to the foreground", s), new Throwable[0]);
            final WorkerWrapper workerWrapper = this.mEnqueuedWorkMap.remove(s);
            if (workerWrapper != null) {
                if (this.mForegroundLock == null) {
                    (this.mForegroundLock = WakeLocks.newWakeLock(this.mAppContext, "ProcessorForegroundLck")).acquire();
                }
                this.mForegroundWorkMap.put(s, workerWrapper);
                ContextCompat.startForegroundService(this.mAppContext, SystemForegroundDispatcher.createStartForegroundIntent(this.mAppContext, s, foregroundInfo));
            }
        }
    }
    
    public boolean startWork(@NonNull final String s) {
        return this.startWork(s, null);
    }
    
    public boolean startWork(@NonNull final String s, @Nullable final WorkerParameters.RuntimeExtras runtimeExtras) {
        synchronized (this.mLock) {
            if (this.isEnqueued(s)) {
                Logger.get().debug(Processor.TAG, String.format("Work %s is already enqueued for processing", s), new Throwable[0]);
                return false;
            }
            final WorkerWrapper build = new WorkerWrapper.Builder(this.mAppContext, this.mConfiguration, this.mWorkTaskExecutor, this, this.mWorkDatabase, s).withSchedulers(this.mSchedulers).withRuntimeExtras(runtimeExtras).build();
            final ListenableFuture<Boolean> future = build.getFuture();
            future.addListener((Runnable)new FutureListener(this, s, future), this.mWorkTaskExecutor.getMainThreadExecutor());
            this.mEnqueuedWorkMap.put(s, build);
            monitorexit(this.mLock);
            this.mWorkTaskExecutor.getBackgroundExecutor().execute(build);
            Logger.get().debug(Processor.TAG, String.format("%s: processing %s", this.getClass().getSimpleName(), s), new Throwable[0]);
            return true;
        }
    }
    
    public boolean stopAndCancelWork(@NonNull final String s) {
        synchronized (this.mLock) {
            final Logger value = Logger.get();
            final String tag = Processor.TAG;
            boolean b = true;
            value.debug(tag, String.format("Processor cancelling %s", s), new Throwable[0]);
            this.mCancelledIds.add(s);
            final WorkerWrapper workerWrapper = this.mForegroundWorkMap.remove(s);
            if (workerWrapper == null) {
                b = false;
            }
            WorkerWrapper workerWrapper2 = workerWrapper;
            if (workerWrapper == null) {
                workerWrapper2 = this.mEnqueuedWorkMap.remove(s);
            }
            final boolean interrupt = interrupt(s, workerWrapper2);
            if (b) {
                this.stopForegroundService();
            }
            return interrupt;
        }
    }
    
    @Override
    public void stopForeground(@NonNull final String s) {
        synchronized (this.mLock) {
            this.mForegroundWorkMap.remove(s);
            this.stopForegroundService();
        }
    }
    
    public boolean stopForegroundWork(@NonNull final String s) {
        synchronized (this.mLock) {
            Logger.get().debug(Processor.TAG, String.format("Processor stopping foreground work %s", s), new Throwable[0]);
            return interrupt(s, this.mForegroundWorkMap.remove(s));
        }
    }
    
    public boolean stopWork(@NonNull final String s) {
        synchronized (this.mLock) {
            Logger.get().debug(Processor.TAG, String.format("Processor stopping background work %s", s), new Throwable[0]);
            return interrupt(s, this.mEnqueuedWorkMap.remove(s));
        }
    }
    
    private static class FutureListener implements Runnable
    {
        @NonNull
        private ExecutionListener mExecutionListener;
        @NonNull
        private ListenableFuture<Boolean> mFuture;
        @NonNull
        private String mWorkSpecId;
        
        FutureListener(@NonNull final ExecutionListener mExecutionListener, @NonNull final String mWorkSpecId, @NonNull final ListenableFuture<Boolean> mFuture) {
            this.mExecutionListener = mExecutionListener;
            this.mWorkSpecId = mWorkSpecId;
            this.mFuture = mFuture;
        }
        
        @Override
        public void run() {
            boolean booleanValue;
            try {
                booleanValue = ((Future<Boolean>)this.mFuture).get();
            }
            catch (final InterruptedException | ExecutionException ex) {
                booleanValue = true;
            }
            this.mExecutionListener.onExecuted(this.mWorkSpecId, booleanValue);
        }
    }
}

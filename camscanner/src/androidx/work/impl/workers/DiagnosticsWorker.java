// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.workers;

import androidx.work.impl.model.WorkSpecDao;
import androidx.work.impl.WorkDatabase;
import java.util.concurrent.TimeUnit;
import androidx.work.impl.WorkManagerImpl;
import androidx.work.ListenableWorker;
import androidx.work.impl.model.SystemIdInfo;
import java.util.Iterator;
import android.text.TextUtils;
import android.os.Build$VERSION;
import java.util.List;
import androidx.work.impl.model.SystemIdInfoDao;
import androidx.work.impl.model.WorkTagDao;
import androidx.work.impl.model.WorkNameDao;
import androidx.annotation.Nullable;
import androidx.work.impl.model.WorkSpec;
import androidx.work.WorkerParameters;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.work.Logger;
import androidx.annotation.RestrictTo;
import androidx.work.Worker;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class DiagnosticsWorker extends Worker
{
    private static final String TAG;
    
    static {
        TAG = Logger.tagWithPrefix("DiagnosticsWrkr");
    }
    
    public DiagnosticsWorker(@NonNull final Context context, @NonNull final WorkerParameters workerParameters) {
        super(context, workerParameters);
    }
    
    @NonNull
    private static String workSpecRow(@NonNull final WorkSpec workSpec, @Nullable final String s, @Nullable final Integer n, @NonNull final String s2) {
        return String.format("\n%s\t %s\t %s\t %s\t %s\t %s\t", workSpec.id, workSpec.workerClassName, n, workSpec.state.name(), s, s2);
    }
    
    @NonNull
    private static String workSpecRows(@NonNull final WorkNameDao workNameDao, @NonNull final WorkTagDao workTagDao, @NonNull final SystemIdInfoDao systemIdInfoDao, @NonNull final List<WorkSpec> list) {
        final StringBuilder sb = new StringBuilder();
        String s;
        if (Build$VERSION.SDK_INT >= 23) {
            s = "Job Id";
        }
        else {
            s = "Alarm Id";
        }
        sb.append(String.format("\n Id \t Class Name\t %s\t State\t Unique Name\t Tags\t", s));
        for (final WorkSpec workSpec : list) {
            final SystemIdInfo systemIdInfo = systemIdInfoDao.getSystemIdInfo(workSpec.id);
            Integer value;
            if (systemIdInfo != null) {
                value = systemIdInfo.systemId;
            }
            else {
                value = null;
            }
            sb.append(workSpecRow(workSpec, TextUtils.join((CharSequence)",", (Iterable)workNameDao.getNamesForWorkSpecId(workSpec.id)), value, TextUtils.join((CharSequence)",", (Iterable)workTagDao.getTagsForWorkSpecId(workSpec.id))));
        }
        return sb.toString();
    }
    
    @NonNull
    @Override
    public Result doWork() {
        final WorkDatabase workDatabase = WorkManagerImpl.getInstance(this.getApplicationContext()).getWorkDatabase();
        final WorkSpecDao workSpecDao = workDatabase.workSpecDao();
        final WorkNameDao workNameDao = workDatabase.workNameDao();
        final WorkTagDao workTagDao = workDatabase.workTagDao();
        final SystemIdInfoDao systemIdInfoDao = workDatabase.systemIdInfoDao();
        final List<WorkSpec> recentlyCompletedWork = workSpecDao.getRecentlyCompletedWork(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(1L));
        final List<WorkSpec> runningWork = workSpecDao.getRunningWork();
        final List<WorkSpec> allEligibleWorkSpecsForScheduling = workSpecDao.getAllEligibleWorkSpecsForScheduling(200);
        if (recentlyCompletedWork != null && !recentlyCompletedWork.isEmpty()) {
            final Logger value = Logger.get();
            final String tag = DiagnosticsWorker.TAG;
            value.info(tag, "Recently completed work:\n\n", new Throwable[0]);
            Logger.get().info(tag, workSpecRows(workNameDao, workTagDao, systemIdInfoDao, recentlyCompletedWork), new Throwable[0]);
        }
        if (runningWork != null && !runningWork.isEmpty()) {
            final Logger value2 = Logger.get();
            final String tag2 = DiagnosticsWorker.TAG;
            value2.info(tag2, "Running work:\n\n", new Throwable[0]);
            Logger.get().info(tag2, workSpecRows(workNameDao, workTagDao, systemIdInfoDao, runningWork), new Throwable[0]);
        }
        if (allEligibleWorkSpecsForScheduling != null && !allEligibleWorkSpecsForScheduling.isEmpty()) {
            final Logger value3 = Logger.get();
            final String tag3 = DiagnosticsWorker.TAG;
            value3.info(tag3, "Enqueued work:\n\n", new Throwable[0]);
            Logger.get().info(tag3, workSpecRows(workNameDao, workTagDao, systemIdInfoDao, allEligibleWorkSpecsForScheduling), new Throwable[0]);
        }
        return Result.success();
    }
}

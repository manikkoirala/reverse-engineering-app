// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.workers;

import com.google.common.util.concurrent.ListenableFuture;
import androidx.work.impl.model.WorkSpec;
import java.util.Collections;
import androidx.work.impl.constraints.WorkConstraintsTracker;
import android.text.TextUtils;
import java.util.List;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.WorkManagerImpl;
import androidx.work.impl.utils.taskexecutor.TaskExecutor;
import androidx.annotation.VisibleForTesting;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.work.Logger;
import androidx.work.WorkerParameters;
import androidx.work.impl.utils.futures.SettableFuture;
import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
import androidx.work.impl.constraints.WorkConstraintsCallback;
import androidx.work.ListenableWorker;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class ConstraintTrackingWorker extends ListenableWorker implements WorkConstraintsCallback
{
    public static final String ARGUMENT_CLASS_NAME = "androidx.work.impl.workers.ConstraintTrackingWorker.ARGUMENT_CLASS_NAME";
    private static final String TAG;
    volatile boolean mAreConstraintsUnmet;
    @Nullable
    private ListenableWorker mDelegate;
    SettableFuture<Result> mFuture;
    final Object mLock;
    private WorkerParameters mWorkerParameters;
    
    static {
        TAG = Logger.tagWithPrefix("ConstraintTrkngWrkr");
    }
    
    public ConstraintTrackingWorker(@NonNull final Context context, @NonNull final WorkerParameters mWorkerParameters) {
        super(context, mWorkerParameters);
        this.mWorkerParameters = mWorkerParameters;
        this.mLock = new Object();
        this.mAreConstraintsUnmet = false;
        this.mFuture = SettableFuture.create();
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @VisibleForTesting
    public ListenableWorker getDelegate() {
        return this.mDelegate;
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @VisibleForTesting
    @Override
    public TaskExecutor getTaskExecutor() {
        return WorkManagerImpl.getInstance(this.getApplicationContext()).getWorkTaskExecutor();
    }
    
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @VisibleForTesting
    public WorkDatabase getWorkDatabase() {
        return WorkManagerImpl.getInstance(this.getApplicationContext()).getWorkDatabase();
    }
    
    @Override
    public boolean isRunInForeground() {
        final ListenableWorker mDelegate = this.mDelegate;
        return mDelegate != null && mDelegate.isRunInForeground();
    }
    
    @Override
    public void onAllConstraintsMet(@NonNull final List<String> list) {
    }
    
    @Override
    public void onAllConstraintsNotMet(@NonNull final List<String> list) {
        Logger.get().debug(ConstraintTrackingWorker.TAG, String.format("Constraints changed for %s", list), new Throwable[0]);
        synchronized (this.mLock) {
            this.mAreConstraintsUnmet = true;
        }
    }
    
    @Override
    public void onStopped() {
        super.onStopped();
        final ListenableWorker mDelegate = this.mDelegate;
        if (mDelegate != null && !mDelegate.isStopped()) {
            this.mDelegate.stop();
        }
    }
    
    void setFutureFailed() {
        this.mFuture.set(Result.failure());
    }
    
    void setFutureRetry() {
        this.mFuture.set(Result.retry());
    }
    
    void setupAndRunConstraintTrackingWork() {
        final String string = this.getInputData().getString("androidx.work.impl.workers.ConstraintTrackingWorker.ARGUMENT_CLASS_NAME");
        if (TextUtils.isEmpty((CharSequence)string)) {
            Logger.get().error(ConstraintTrackingWorker.TAG, "No worker to delegate to.", new Throwable[0]);
            this.setFutureFailed();
            return;
        }
        if ((this.mDelegate = this.getWorkerFactory().createWorkerWithDefaultFallback(this.getApplicationContext(), string, this.mWorkerParameters)) == null) {
            Logger.get().debug(ConstraintTrackingWorker.TAG, "No worker to delegate to.", new Throwable[0]);
            this.setFutureFailed();
            return;
        }
        final WorkSpec workSpec = this.getWorkDatabase().workSpecDao().getWorkSpec(this.getId().toString());
        if (workSpec == null) {
            this.setFutureFailed();
            return;
        }
        final WorkConstraintsTracker workConstraintsTracker = new WorkConstraintsTracker(this.getApplicationContext(), this.getTaskExecutor(), this);
        workConstraintsTracker.replace(Collections.singletonList(workSpec));
        if (workConstraintsTracker.areAllConstraintsMet(this.getId().toString())) {
            Logger.get().debug(ConstraintTrackingWorker.TAG, String.format("Constraints met for delegate %s", string), new Throwable[0]);
            try {
                final ListenableFuture<Result> startWork = this.mDelegate.startWork();
                startWork.addListener((Runnable)new Runnable(this, startWork) {
                    final ConstraintTrackingWorker this$0;
                    final ListenableFuture val$innerFuture;
                    
                    @Override
                    public void run() {
                        synchronized (this.this$0.mLock) {
                            if (this.this$0.mAreConstraintsUnmet) {
                                this.this$0.setFutureRetry();
                            }
                            else {
                                this.this$0.mFuture.setFuture((com.google.common.util.concurrent.ListenableFuture<? extends Result>)this.val$innerFuture);
                            }
                        }
                    }
                }, this.getBackgroundExecutor());
                return;
            }
            finally {
                final Logger value = Logger.get();
                final String tag = ConstraintTrackingWorker.TAG;
                final Throwable t;
                value.debug(tag, String.format("Delegated worker %s threw exception in startWork.", string), t);
                synchronized (this.mLock) {
                    if (this.mAreConstraintsUnmet) {
                        Logger.get().debug(tag, "Constraints were unmet, Retrying.", new Throwable[0]);
                        this.setFutureRetry();
                    }
                    else {
                        this.setFutureFailed();
                    }
                }
            }
        }
        Logger.get().debug(ConstraintTrackingWorker.TAG, String.format("Constraints not met for delegate %s. Requesting retry.", string), new Throwable[0]);
        this.setFutureRetry();
    }
    
    @NonNull
    @Override
    public ListenableFuture<Result> startWork() {
        this.getBackgroundExecutor().execute(new Runnable(this) {
            final ConstraintTrackingWorker this$0;
            
            @Override
            public void run() {
                this.this$0.setupAndRunConstraintTrackingWork();
            }
        });
        return (ListenableFuture<Result>)this.mFuture;
    }
}

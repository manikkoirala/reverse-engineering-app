// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.workers;

import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.annotation.RestrictTo;
import androidx.work.Worker;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class CombineContinuationsWorker extends Worker
{
    public CombineContinuationsWorker(@NonNull final Context context, @NonNull final WorkerParameters workerParameters) {
        super(context, workerParameters);
    }
    
    @NonNull
    @Override
    public Result doWork() {
        return Result.success(this.getInputData());
    }
}

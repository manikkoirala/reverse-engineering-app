// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl.diagnostics;

import androidx.work.WorkRequest;
import androidx.work.ListenableWorker;
import androidx.work.OneTimeWorkRequest;
import androidx.work.impl.workers.DiagnosticsWorker;
import androidx.work.WorkManager;
import androidx.annotation.Nullable;
import android.content.Intent;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.work.Logger;
import androidx.annotation.RestrictTo;
import android.content.BroadcastReceiver;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class DiagnosticsReceiver extends BroadcastReceiver
{
    private static final String TAG;
    
    static {
        TAG = Logger.tagWithPrefix("DiagnosticsRcvr");
    }
    
    public void onReceive(@NonNull final Context context, @Nullable final Intent intent) {
        if (intent == null) {
            return;
        }
        Logger.get().debug(DiagnosticsReceiver.TAG, "Requesting diagnostics", new Throwable[0]);
        try {
            WorkManager.getInstance(context).enqueue(OneTimeWorkRequest.from(DiagnosticsWorker.class));
        }
        catch (final IllegalStateException ex) {
            Logger.get().error(DiagnosticsReceiver.TAG, "WorkManager is not initialized", ex);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work.impl;

import androidx.annotation.VisibleForTesting;
import androidx.annotation.NonNull;
import androidx.core.os.HandlerCompat;
import android.os.Looper;
import android.os.Handler;
import androidx.annotation.RestrictTo;
import androidx.work.RunnableScheduler;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
public class DefaultRunnableScheduler implements RunnableScheduler
{
    private final Handler mHandler;
    
    public DefaultRunnableScheduler() {
        this.mHandler = HandlerCompat.createAsync(Looper.getMainLooper());
    }
    
    @VisibleForTesting
    public DefaultRunnableScheduler(@NonNull final Handler mHandler) {
        this.mHandler = mHandler;
    }
    
    @Override
    public void cancel(@NonNull final Runnable runnable) {
        this.mHandler.removeCallbacks(runnable);
    }
    
    @NonNull
    public Handler getHandler() {
        return this.mHandler;
    }
    
    @Override
    public void scheduleWithDelay(final long n, @NonNull final Runnable runnable) {
        this.mHandler.postDelayed(runnable, n);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import android.os.Build$VERSION;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.\u3007080;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import androidx.annotation.NonNull;
import android.annotation.SuppressLint;

public final class PeriodicWorkRequest extends WorkRequest
{
    @SuppressLint({ "MinMaxConstant" })
    public static final long MIN_PERIODIC_FLEX_MILLIS = 300000L;
    @SuppressLint({ "MinMaxConstant" })
    public static final long MIN_PERIODIC_INTERVAL_MILLIS = 900000L;
    
    PeriodicWorkRequest(final Builder builder) {
        super(builder.mId, builder.mWorkSpec, builder.mTags);
    }
    
    public static final class Builder extends WorkRequest.Builder<Builder, PeriodicWorkRequest>
    {
        public Builder(@NonNull final Class<? extends ListenableWorker> clazz, final long duration, @NonNull final TimeUnit timeUnit) {
            super(clazz);
            super.mWorkSpec.setPeriodic(timeUnit.toMillis(duration));
        }
        
        public Builder(@NonNull final Class<? extends ListenableWorker> clazz, final long duration, @NonNull final TimeUnit timeUnit, final long duration2, @NonNull final TimeUnit timeUnit2) {
            super(clazz);
            super.mWorkSpec.setPeriodic(timeUnit.toMillis(duration), timeUnit2.toMillis(duration2));
        }
        
        @RequiresApi(26)
        public Builder(@NonNull final Class<? extends ListenableWorker> clazz, @NonNull final Duration duration) {
            super(clazz);
            super.mWorkSpec.setPeriodic(\u3007080.\u3007080(duration));
        }
        
        @RequiresApi(26)
        public Builder(@NonNull final Class<? extends ListenableWorker> clazz, @NonNull final Duration duration, @NonNull final Duration duration2) {
            super(clazz);
            super.mWorkSpec.setPeriodic(\u3007080.\u3007080(duration), \u3007080.\u3007080(duration2));
        }
        
        @NonNull
        PeriodicWorkRequest buildInternal() {
            if (super.mBackoffCriteriaSet && Build$VERSION.SDK_INT >= 23 && super.mWorkSpec.constraints.requiresDeviceIdle()) {
                throw new IllegalArgumentException("Cannot set backoff criteria on an idle mode job");
            }
            return new PeriodicWorkRequest(this);
        }
        
        @NonNull
        Builder getThis() {
            return this;
        }
    }
}

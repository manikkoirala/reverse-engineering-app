// 
// Decompiled by Procyon v0.6.0
// 

package androidx.work;

import java.util.Collections;
import java.util.List;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.startup.Initializer;

public final class WorkManagerInitializer implements Initializer<WorkManager>
{
    private static final String TAG;
    
    static {
        TAG = Logger.tagWithPrefix("WrkMgrInitializer");
    }
    
    @NonNull
    @Override
    public WorkManager create(@NonNull final Context context) {
        Logger.get().debug(WorkManagerInitializer.TAG, "Initializing WorkManager with default configuration.", new Throwable[0]);
        WorkManager.initialize(context, new Configuration.Builder().build());
        return WorkManager.getInstance(context);
    }
    
    @NonNull
    @Override
    public List<Class<? extends Initializer<?>>> dependencies() {
        return Collections.emptyList();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.loader.content;

import androidx.core.os.OperationCanceledException;
import android.database.ContentObserver;
import androidx.core.content.ContentResolverCompat;
import java.util.Arrays;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import android.content.Context;
import android.net.Uri;
import androidx.core.os.CancellationSignal;
import android.database.Cursor;

public class CursorLoader extends AsyncTaskLoader<Cursor>
{
    CancellationSignal mCancellationSignal;
    Cursor mCursor;
    final ForceLoadContentObserver mObserver;
    String[] mProjection;
    String mSelection;
    String[] mSelectionArgs;
    String mSortOrder;
    Uri mUri;
    
    public CursorLoader(@NonNull final Context context) {
        super(context);
        this.mObserver = new ForceLoadContentObserver();
    }
    
    public CursorLoader(@NonNull final Context context, @NonNull final Uri mUri, @Nullable final String[] mProjection, @Nullable final String mSelection, @Nullable final String[] mSelectionArgs, @Nullable final String mSortOrder) {
        super(context);
        this.mObserver = new ForceLoadContentObserver();
        this.mUri = mUri;
        this.mProjection = mProjection;
        this.mSelection = mSelection;
        this.mSelectionArgs = mSelectionArgs;
        this.mSortOrder = mSortOrder;
    }
    
    @Override
    public void cancelLoadInBackground() {
        super.cancelLoadInBackground();
        synchronized (this) {
            final CancellationSignal mCancellationSignal = this.mCancellationSignal;
            if (mCancellationSignal != null) {
                mCancellationSignal.cancel();
            }
        }
    }
    
    @Override
    public void deliverResult(final Cursor mCursor) {
        if (this.isReset()) {
            if (mCursor != null) {
                mCursor.close();
            }
            return;
        }
        final Cursor mCursor2 = this.mCursor;
        this.mCursor = mCursor;
        if (this.isStarted()) {
            super.deliverResult(mCursor);
        }
        if (mCursor2 != null && mCursor2 != mCursor && !mCursor2.isClosed()) {
            mCursor2.close();
        }
    }
    
    @Deprecated
    @Override
    public void dump(final String s, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
        super.dump(s, fileDescriptor, printWriter, array);
        printWriter.print(s);
        printWriter.print("mUri=");
        printWriter.println(this.mUri);
        printWriter.print(s);
        printWriter.print("mProjection=");
        printWriter.println(Arrays.toString(this.mProjection));
        printWriter.print(s);
        printWriter.print("mSelection=");
        printWriter.println(this.mSelection);
        printWriter.print(s);
        printWriter.print("mSelectionArgs=");
        printWriter.println(Arrays.toString(this.mSelectionArgs));
        printWriter.print(s);
        printWriter.print("mSortOrder=");
        printWriter.println(this.mSortOrder);
        printWriter.print(s);
        printWriter.print("mCursor=");
        printWriter.println(this.mCursor);
        printWriter.print(s);
        printWriter.print("mContentChanged=");
        printWriter.println(super.mContentChanged);
    }
    
    @Nullable
    public String[] getProjection() {
        return this.mProjection;
    }
    
    @Nullable
    public String getSelection() {
        return this.mSelection;
    }
    
    @Nullable
    public String[] getSelectionArgs() {
        return this.mSelectionArgs;
    }
    
    @Nullable
    public String getSortOrder() {
        return this.mSortOrder;
    }
    
    @NonNull
    public Uri getUri() {
        return this.mUri;
    }
    
    @Override
    public Cursor loadInBackground() {
        synchronized (this) {
            if (!this.isLoadInBackgroundCanceled()) {
                this.mCancellationSignal = new CancellationSignal();
                monitorexit(this);
                try {
                    final Cursor query = ContentResolverCompat.query(this.getContext().getContentResolver(), this.mUri, this.mProjection, this.mSelection, this.mSelectionArgs, this.mSortOrder, this.mCancellationSignal);
                    if (query != null) {
                        try {
                            query.getCount();
                            query.registerContentObserver((ContentObserver)this.mObserver);
                        }
                        catch (final RuntimeException ex) {
                            query.close();
                            throw ex;
                        }
                    }
                    synchronized (this) {
                        this.mCancellationSignal = null;
                        return query;
                    }
                }
                finally {
                    synchronized (this) {
                        this.mCancellationSignal = null;
                    }
                }
            }
            throw new OperationCanceledException();
        }
    }
    
    @Override
    public void onCanceled(final Cursor cursor) {
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }
    
    @Override
    protected void onReset() {
        super.onReset();
        this.onStopLoading();
        final Cursor mCursor = this.mCursor;
        if (mCursor != null && !mCursor.isClosed()) {
            this.mCursor.close();
        }
        this.mCursor = null;
    }
    
    @Override
    protected void onStartLoading() {
        final Cursor mCursor = this.mCursor;
        if (mCursor != null) {
            this.deliverResult(mCursor);
        }
        if (this.takeContentChanged() || this.mCursor == null) {
            this.forceLoad();
        }
    }
    
    @Override
    protected void onStopLoading() {
        this.cancelLoad();
    }
    
    public void setProjection(@Nullable final String[] mProjection) {
        this.mProjection = mProjection;
    }
    
    public void setSelection(@Nullable final String mSelection) {
        this.mSelection = mSelection;
    }
    
    public void setSelectionArgs(@Nullable final String[] mSelectionArgs) {
        this.mSelectionArgs = mSelectionArgs;
    }
    
    public void setSortOrder(@Nullable final String mSortOrder) {
        this.mSortOrder = mSortOrder;
    }
    
    public void setUri(@NonNull final Uri mUri) {
        this.mUri = mUri;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.RequiresApi;
import android.app.Application$ActivityLifecycleCallbacks;
import androidx.annotation.Nullable;
import android.os.Build$VERSION;
import android.os.Bundle;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import androidx.annotation.NonNull;
import android.os.Handler;
import androidx.annotation.VisibleForTesting;

public class ProcessLifecycleOwner implements LifecycleOwner
{
    @VisibleForTesting
    static final long TIMEOUT_MS = 700L;
    private static final ProcessLifecycleOwner sInstance;
    private Runnable mDelayedPauseRunnable;
    private Handler mHandler;
    ReportFragment.ActivityInitializationListener mInitializationListener;
    private boolean mPauseSent;
    private final LifecycleRegistry mRegistry;
    private int mResumedCounter;
    private int mStartedCounter;
    private boolean mStopSent;
    
    static {
        sInstance = new ProcessLifecycleOwner();
    }
    
    private ProcessLifecycleOwner() {
        this.mStartedCounter = 0;
        this.mResumedCounter = 0;
        this.mPauseSent = true;
        this.mStopSent = true;
        this.mRegistry = new LifecycleRegistry(this);
        this.mDelayedPauseRunnable = new Runnable() {
            final ProcessLifecycleOwner this$0;
            
            @Override
            public void run() {
                this.this$0.dispatchPauseIfNeeded();
                this.this$0.dispatchStopIfNeeded();
            }
        };
        this.mInitializationListener = new ReportFragment.ActivityInitializationListener() {
            final ProcessLifecycleOwner this$0;
            
            @Override
            public void onCreate() {
            }
            
            @Override
            public void onResume() {
                this.this$0.activityResumed();
            }
            
            @Override
            public void onStart() {
                this.this$0.activityStarted();
            }
        };
    }
    
    @NonNull
    public static LifecycleOwner get() {
        return ProcessLifecycleOwner.sInstance;
    }
    
    static void init(final Context context) {
        ProcessLifecycleOwner.sInstance.attach(context);
    }
    
    void activityPaused() {
        final int mResumedCounter = this.mResumedCounter - 1;
        this.mResumedCounter = mResumedCounter;
        if (mResumedCounter == 0) {
            this.mHandler.postDelayed(this.mDelayedPauseRunnable, 700L);
        }
    }
    
    void activityResumed() {
        final int mResumedCounter = this.mResumedCounter + 1;
        this.mResumedCounter = mResumedCounter;
        if (mResumedCounter == 1) {
            if (this.mPauseSent) {
                this.mRegistry.handleLifecycleEvent(Lifecycle.Event.ON_RESUME);
                this.mPauseSent = false;
            }
            else {
                this.mHandler.removeCallbacks(this.mDelayedPauseRunnable);
            }
        }
    }
    
    void activityStarted() {
        final int mStartedCounter = this.mStartedCounter + 1;
        this.mStartedCounter = mStartedCounter;
        if (mStartedCounter == 1 && this.mStopSent) {
            this.mRegistry.handleLifecycleEvent(Lifecycle.Event.ON_START);
            this.mStopSent = false;
        }
    }
    
    void activityStopped() {
        --this.mStartedCounter;
        this.dispatchStopIfNeeded();
    }
    
    void attach(final Context context) {
        this.mHandler = new Handler();
        this.mRegistry.handleLifecycleEvent(Lifecycle.Event.ON_CREATE);
        ((Application)context.getApplicationContext()).registerActivityLifecycleCallbacks((Application$ActivityLifecycleCallbacks)new EmptyActivityLifecycleCallbacks(this) {
            final ProcessLifecycleOwner this$0;
            
            @Override
            public void onActivityCreated(final Activity activity, final Bundle bundle) {
                if (Build$VERSION.SDK_INT < 29) {
                    ReportFragment.get(activity).setProcessListener(this.this$0.mInitializationListener);
                }
            }
            
            @Override
            public void onActivityPaused(final Activity activity) {
                this.this$0.activityPaused();
            }
            
            @RequiresApi(29)
            public void onActivityPreCreated(@NonNull final Activity activity, @Nullable final Bundle bundle) {
                \u3007\u3007888.\u3007080(activity, (Application$ActivityLifecycleCallbacks)new EmptyActivityLifecycleCallbacks(this) {
                    final ProcessLifecycleOwner$3 this$1;
                    
                    public void onActivityPostResumed(@NonNull final Activity activity) {
                        this.this$1.this$0.activityResumed();
                    }
                    
                    public void onActivityPostStarted(@NonNull final Activity activity) {
                        this.this$1.this$0.activityStarted();
                    }
                });
            }
            
            @Override
            public void onActivityStopped(final Activity activity) {
                this.this$0.activityStopped();
            }
        });
    }
    
    void dispatchPauseIfNeeded() {
        if (this.mResumedCounter == 0) {
            this.mPauseSent = true;
            this.mRegistry.handleLifecycleEvent(Lifecycle.Event.ON_PAUSE);
        }
    }
    
    void dispatchStopIfNeeded() {
        if (this.mStartedCounter == 0 && this.mPauseSent) {
            this.mRegistry.handleLifecycleEvent(Lifecycle.Event.ON_STOP);
            this.mStopSent = true;
        }
    }
    
    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return this.mRegistry;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.NonNull;

class SingleGeneratedAdapterObserver implements LifecycleEventObserver
{
    private final GeneratedAdapter mGeneratedAdapter;
    
    SingleGeneratedAdapterObserver(final GeneratedAdapter mGeneratedAdapter) {
        this.mGeneratedAdapter = mGeneratedAdapter;
    }
    
    @Override
    public void onStateChanged(@NonNull final LifecycleOwner lifecycleOwner, @NonNull final Lifecycle.Event event) {
        this.mGeneratedAdapter.callMethods(lifecycleOwner, event, false, null);
        this.mGeneratedAdapter.callMethods(lifecycleOwner, event, true, null);
    }
}

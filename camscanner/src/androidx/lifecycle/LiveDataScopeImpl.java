// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlinx.coroutines.DisposableHandle;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.BuildersKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlinx.coroutines.Dispatchers;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.CoroutineContext;
import kotlin.Metadata;

@Metadata
public final class LiveDataScopeImpl<T> implements LiveDataScope<T>
{
    @NotNull
    private final CoroutineContext coroutineContext;
    @NotNull
    private CoroutineLiveData<T> target;
    
    public LiveDataScopeImpl(@NotNull final CoroutineLiveData<T> target, @NotNull final CoroutineContext coroutineContext) {
        Intrinsics.checkNotNullParameter((Object)target, "target");
        Intrinsics.checkNotNullParameter((Object)coroutineContext, "context");
        this.target = target;
        this.coroutineContext = coroutineContext.plus((CoroutineContext)Dispatchers.\u3007o\u3007().\u30078());
    }
    
    @Override
    public Object emit(final T t, @NotNull final Continuation<? super Unit> continuation) {
        final Object oo08 = BuildersKt.Oo08(this.coroutineContext, (Function2)new LiveDataScopeImpl$emit.LiveDataScopeImpl$emit$2(this, (Object)t, (Continuation)null), (Continuation)continuation);
        if (oo08 == IntrinsicsKt.O8()) {
            return oo08;
        }
        return Unit.\u3007080;
    }
    
    @Override
    public Object emitSource(@NotNull final LiveData<T> liveData, @NotNull final Continuation<? super DisposableHandle> continuation) {
        return BuildersKt.Oo08(this.coroutineContext, (Function2)new LiveDataScopeImpl$emitSource.LiveDataScopeImpl$emitSource$2(this, (LiveData)liveData, (Continuation)null), (Continuation)continuation);
    }
    
    @Override
    public T getLatestValue() {
        return this.target.getValue();
    }
    
    @NotNull
    public final CoroutineLiveData<T> getTarget$lifecycle_livedata_ktx_release() {
        return this.target;
    }
    
    public final void setTarget$lifecycle_livedata_ktx_release(@NotNull final CoroutineLiveData<T> target) {
        Intrinsics.checkNotNullParameter((Object)target, "<set-?>");
        this.target = target;
    }
}

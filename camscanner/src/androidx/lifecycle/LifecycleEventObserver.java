// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.NonNull;

public interface LifecycleEventObserver extends LifecycleObserver
{
    void onStateChanged(@NonNull final LifecycleOwner p0, @NonNull final Lifecycle.Event p1);
}

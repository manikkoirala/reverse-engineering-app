// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class LifecycleOwnerKt
{
    @NotNull
    public static final LifecycleCoroutineScope getLifecycleScope(@NotNull final LifecycleOwner lifecycleOwner) {
        Intrinsics.checkNotNullParameter((Object)lifecycleOwner, "<this>");
        final Lifecycle lifecycle = lifecycleOwner.getLifecycle();
        Intrinsics.checkNotNullExpressionValue((Object)lifecycle, "lifecycle");
        return LifecycleKt.getCoroutineScope(lifecycle);
    }
}

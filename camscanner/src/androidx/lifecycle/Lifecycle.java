// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.Nullable;
import androidx.annotation.MainThread;
import androidx.annotation.RestrictTo;
import androidx.annotation.NonNull;
import java.util.concurrent.atomic.AtomicReference;

public abstract class Lifecycle
{
    @NonNull
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    AtomicReference<Object> mInternalScopeRef;
    
    public Lifecycle() {
        this.mInternalScopeRef = new AtomicReference<Object>();
    }
    
    @MainThread
    public abstract void addObserver(@NonNull final LifecycleObserver p0);
    
    @MainThread
    @NonNull
    public abstract State getCurrentState();
    
    @MainThread
    public abstract void removeObserver(@NonNull final LifecycleObserver p0);
    
    public enum Event
    {
        private static final Event[] $VALUES;
        
        ON_ANY, 
        ON_CREATE, 
        ON_DESTROY, 
        ON_PAUSE, 
        ON_RESUME, 
        ON_START, 
        ON_STOP;
        
        @Nullable
        public static Event downFrom(@NonNull final State state) {
            final int n = Lifecycle$1.$SwitchMap$androidx$lifecycle$Lifecycle$State[state.ordinal()];
            if (n == 1) {
                return Event.ON_DESTROY;
            }
            if (n == 2) {
                return Event.ON_STOP;
            }
            if (n != 3) {
                return null;
            }
            return Event.ON_PAUSE;
        }
        
        @Nullable
        public static Event downTo(@NonNull final State state) {
            final int n = Lifecycle$1.$SwitchMap$androidx$lifecycle$Lifecycle$State[state.ordinal()];
            if (n == 1) {
                return Event.ON_STOP;
            }
            if (n == 2) {
                return Event.ON_PAUSE;
            }
            if (n != 4) {
                return null;
            }
            return Event.ON_DESTROY;
        }
        
        @Nullable
        public static Event upFrom(@NonNull final State state) {
            final int n = Lifecycle$1.$SwitchMap$androidx$lifecycle$Lifecycle$State[state.ordinal()];
            if (n == 1) {
                return Event.ON_START;
            }
            if (n == 2) {
                return Event.ON_RESUME;
            }
            if (n != 5) {
                return null;
            }
            return Event.ON_CREATE;
        }
        
        @Nullable
        public static Event upTo(@NonNull final State state) {
            final int n = Lifecycle$1.$SwitchMap$androidx$lifecycle$Lifecycle$State[state.ordinal()];
            if (n == 1) {
                return Event.ON_CREATE;
            }
            if (n == 2) {
                return Event.ON_START;
            }
            if (n != 3) {
                return null;
            }
            return Event.ON_RESUME;
        }
        
        @NonNull
        public State getTargetState() {
            switch (Lifecycle$1.$SwitchMap$androidx$lifecycle$Lifecycle$Event[this.ordinal()]) {
                default: {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this);
                    sb.append(" has no target state");
                    throw new IllegalArgumentException(sb.toString());
                }
                case 6: {
                    return State.DESTROYED;
                }
                case 5: {
                    return State.RESUMED;
                }
                case 3:
                case 4: {
                    return State.STARTED;
                }
                case 1:
                case 2: {
                    return State.CREATED;
                }
            }
        }
    }
    
    public enum State
    {
        private static final State[] $VALUES;
        
        CREATED, 
        DESTROYED, 
        INITIALIZED, 
        RESUMED, 
        STARTED;
        
        public boolean isAtLeast(@NonNull final State o) {
            return this.compareTo(o) >= 0;
        }
    }
}

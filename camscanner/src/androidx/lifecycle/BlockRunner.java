// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.util.concurrent.CancellationException;
import kotlinx.coroutines.Job$DefaultImpls;
import androidx.annotation.MainThread;
import kotlinx.coroutines.CoroutineStart;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.BuildersKt;
import kotlinx.coroutines.Dispatchers;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineScope;
import kotlin.jvm.functions.Function0;
import kotlinx.coroutines.Job;
import org.jetbrains.annotations.NotNull;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlin.Metadata;

@Metadata
public final class BlockRunner<T>
{
    @NotNull
    private final Function2<LiveDataScope<T>, Continuation<? super Unit>, Object> block;
    private Job cancellationJob;
    @NotNull
    private final CoroutineLiveData<T> liveData;
    @NotNull
    private final Function0<Unit> onDone;
    private Job runningJob;
    @NotNull
    private final CoroutineScope scope;
    private final long timeoutInMs;
    
    public BlockRunner(@NotNull final CoroutineLiveData<T> liveData, @NotNull final Function2<? super LiveDataScope<T>, ? super Continuation<? super Unit>, ?> block, final long timeoutInMs, @NotNull final CoroutineScope scope, @NotNull final Function0<Unit> onDone) {
        Intrinsics.checkNotNullParameter((Object)liveData, "liveData");
        Intrinsics.checkNotNullParameter((Object)block, "block");
        Intrinsics.checkNotNullParameter((Object)scope, "scope");
        Intrinsics.checkNotNullParameter((Object)onDone, "onDone");
        this.liveData = liveData;
        this.block = (Function2<LiveDataScope<T>, Continuation<? super Unit>, Object>)block;
        this.timeoutInMs = timeoutInMs;
        this.scope = scope;
        this.onDone = onDone;
    }
    
    @MainThread
    public final void cancel() {
        if (this.cancellationJob == null) {
            this.cancellationJob = BuildersKt.O8(this.scope, (CoroutineContext)Dispatchers.\u3007o\u3007().\u30078(), (CoroutineStart)null, (Function2)new BlockRunner$cancel.BlockRunner$cancel$1(this, (Continuation)null), 2, (Object)null);
            return;
        }
        throw new IllegalStateException("Cancel call cannot happen without a maybeRun".toString());
    }
    
    @MainThread
    public final void maybeRun() {
        final Job cancellationJob = this.cancellationJob;
        if (cancellationJob != null) {
            Job$DefaultImpls.\u3007080(cancellationJob, (CancellationException)null, 1, (Object)null);
        }
        this.cancellationJob = null;
        if (this.runningJob != null) {
            return;
        }
        this.runningJob = BuildersKt.O8(this.scope, (CoroutineContext)null, (CoroutineStart)null, (Function2)new BlockRunner$maybeRun.BlockRunner$maybeRun$1(this, (Continuation)null), 3, (Object)null);
    }
}

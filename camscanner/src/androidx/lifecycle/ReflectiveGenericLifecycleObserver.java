// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.NonNull;

@Deprecated
class ReflectiveGenericLifecycleObserver implements LifecycleEventObserver
{
    private final ClassesInfoCache.CallbackInfo mInfo;
    private final Object mWrapped;
    
    ReflectiveGenericLifecycleObserver(final Object mWrapped) {
        this.mWrapped = mWrapped;
        this.mInfo = ClassesInfoCache.sInstance.getInfo(mWrapped.getClass());
    }
    
    @Override
    public void onStateChanged(@NonNull final LifecycleOwner lifecycleOwner, @NonNull final Lifecycle.Event event) {
        this.mInfo.invokeCallbacks(lifecycleOwner, event, this.mWrapped);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.jvm.functions.Function1;
import kotlin.reflect.KClass;
import kotlin.jvm.internal.Reflection;
import androidx.lifecycle.viewmodel.InitializerViewModelFactoryBuilder;
import androidx.savedstate.SavedStateRegistry;
import androidx.annotation.MainThread;
import kotlin.jvm.internal.Intrinsics;
import androidx.savedstate.SavedStateRegistryOwner;
import org.jetbrains.annotations.NotNull;
import android.os.Bundle;
import androidx.lifecycle.viewmodel.CreationExtras;
import kotlin.Metadata;

@Metadata
public final class SavedStateHandleSupport
{
    @NotNull
    public static final CreationExtras.Key<Bundle> DEFAULT_ARGS_KEY;
    @NotNull
    private static final String SAVED_STATE_KEY = "androidx.lifecycle.internal.SavedStateHandlesProvider";
    @NotNull
    public static final CreationExtras.Key<SavedStateRegistryOwner> SAVED_STATE_REGISTRY_OWNER_KEY;
    @NotNull
    private static final String VIEWMODEL_KEY = "androidx.lifecycle.internal.SavedStateHandlesVM";
    @NotNull
    public static final CreationExtras.Key<ViewModelStoreOwner> VIEW_MODEL_STORE_OWNER_KEY;
    
    static {
        SAVED_STATE_REGISTRY_OWNER_KEY = (CreationExtras.Key)new SavedStateHandleSupport$SAVED_STATE_REGISTRY_OWNER_KEY.SavedStateHandleSupport$SAVED_STATE_REGISTRY_OWNER_KEY$1();
        VIEW_MODEL_STORE_OWNER_KEY = (CreationExtras.Key)new SavedStateHandleSupport$VIEW_MODEL_STORE_OWNER_KEY.SavedStateHandleSupport$VIEW_MODEL_STORE_OWNER_KEY$1();
        DEFAULT_ARGS_KEY = (CreationExtras.Key)new SavedStateHandleSupport$DEFAULT_ARGS_KEY.SavedStateHandleSupport$DEFAULT_ARGS_KEY$1();
    }
    
    @MainThread
    @NotNull
    public static final SavedStateHandle createSavedStateHandle(@NotNull final CreationExtras creationExtras) {
        Intrinsics.checkNotNullParameter((Object)creationExtras, "<this>");
        final SavedStateRegistryOwner savedStateRegistryOwner = creationExtras.get(SavedStateHandleSupport.SAVED_STATE_REGISTRY_OWNER_KEY);
        if (savedStateRegistryOwner == null) {
            throw new IllegalArgumentException("CreationExtras must have a value by `SAVED_STATE_REGISTRY_OWNER_KEY`");
        }
        final ViewModelStoreOwner viewModelStoreOwner = creationExtras.get(SavedStateHandleSupport.VIEW_MODEL_STORE_OWNER_KEY);
        if (viewModelStoreOwner == null) {
            throw new IllegalArgumentException("CreationExtras must have a value by `VIEW_MODEL_STORE_OWNER_KEY`");
        }
        final Bundle bundle = creationExtras.get(SavedStateHandleSupport.DEFAULT_ARGS_KEY);
        final String s = creationExtras.get(ViewModelProvider.NewInstanceFactory.VIEW_MODEL_KEY);
        if (s != null) {
            return createSavedStateHandle(savedStateRegistryOwner, viewModelStoreOwner, s, bundle);
        }
        throw new IllegalArgumentException("CreationExtras must have a value by `VIEW_MODEL_KEY`");
    }
    
    private static final SavedStateHandle createSavedStateHandle(final SavedStateRegistryOwner savedStateRegistryOwner, final ViewModelStoreOwner viewModelStoreOwner, final String s, final Bundle bundle) {
        final SavedStateHandlesProvider savedStateHandlesProvider = getSavedStateHandlesProvider(savedStateRegistryOwner);
        final SavedStateHandlesVM savedStateHandlesVM = getSavedStateHandlesVM(viewModelStoreOwner);
        SavedStateHandle handle;
        if ((handle = savedStateHandlesVM.getHandles().get(s)) == null) {
            handle = SavedStateHandle.Companion.createHandle(savedStateHandlesProvider.consumeRestoredStateForKey(s), bundle);
            savedStateHandlesVM.getHandles().put(s, handle);
        }
        return handle;
    }
    
    @MainThread
    public static final <T extends SavedStateRegistryOwner & ViewModelStoreOwner> void enableSavedStateHandles(@NotNull final T t) {
        Intrinsics.checkNotNullParameter((Object)t, "<this>");
        final Lifecycle.State currentState = t.getLifecycle().getCurrentState();
        Intrinsics.checkNotNullExpressionValue((Object)currentState, "lifecycle.currentState");
        if (currentState == Lifecycle.State.INITIALIZED || currentState == Lifecycle.State.CREATED) {
            if (t.getSavedStateRegistry().getSavedStateProvider("androidx.lifecycle.internal.SavedStateHandlesProvider") == null) {
                final SavedStateHandlesProvider savedStateHandlesProvider = new SavedStateHandlesProvider(t.getSavedStateRegistry(), t);
                t.getSavedStateRegistry().registerSavedStateProvider("androidx.lifecycle.internal.SavedStateHandlesProvider", (SavedStateRegistry.SavedStateProvider)savedStateHandlesProvider);
                t.getLifecycle().addObserver(new SavedStateHandleAttacher(savedStateHandlesProvider));
            }
            return;
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }
    
    @NotNull
    public static final SavedStateHandlesProvider getSavedStateHandlesProvider(@NotNull final SavedStateRegistryOwner savedStateRegistryOwner) {
        Intrinsics.checkNotNullParameter((Object)savedStateRegistryOwner, "<this>");
        final SavedStateRegistry.SavedStateProvider savedStateProvider = savedStateRegistryOwner.getSavedStateRegistry().getSavedStateProvider("androidx.lifecycle.internal.SavedStateHandlesProvider");
        SavedStateHandlesProvider savedStateHandlesProvider;
        if (savedStateProvider instanceof SavedStateHandlesProvider) {
            savedStateHandlesProvider = (SavedStateHandlesProvider)savedStateProvider;
        }
        else {
            savedStateHandlesProvider = null;
        }
        if (savedStateHandlesProvider != null) {
            return savedStateHandlesProvider;
        }
        throw new IllegalStateException("enableSavedStateHandles() wasn't called prior to createSavedStateHandle() call");
    }
    
    @NotNull
    public static final SavedStateHandlesVM getSavedStateHandlesVM(@NotNull final ViewModelStoreOwner viewModelStoreOwner) {
        Intrinsics.checkNotNullParameter((Object)viewModelStoreOwner, "<this>");
        final InitializerViewModelFactoryBuilder initializerViewModelFactoryBuilder = new InitializerViewModelFactoryBuilder();
        initializerViewModelFactoryBuilder.addInitializer((kotlin.reflect.KClass<ViewModel>)Reflection.\u3007o00\u3007\u3007Oo((Class)SavedStateHandlesVM.class), (kotlin.jvm.functions.Function1<? super CreationExtras, ? extends ViewModel>)SavedStateHandleSupport$savedStateHandlesVM$1.SavedStateHandleSupport$savedStateHandlesVM$1$1.INSTANCE);
        return new ViewModelProvider(viewModelStoreOwner, initializerViewModelFactoryBuilder.build()).get("androidx.lifecycle.internal.SavedStateHandlesVM", SavedStateHandlesVM.class);
    }
}

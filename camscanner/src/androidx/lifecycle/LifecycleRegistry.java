// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.MainThread;
import androidx.annotation.Nullable;
import androidx.arch.core.internal.SafeIterableMap;
import android.annotation.SuppressLint;
import androidx.arch.core.executor.ArchTaskExecutor;
import androidx.annotation.VisibleForTesting;
import java.util.Iterator;
import java.util.Map;
import androidx.annotation.NonNull;
import java.util.ArrayList;
import androidx.arch.core.internal.FastSafeIterableMap;
import java.lang.ref.WeakReference;

public class LifecycleRegistry extends Lifecycle
{
    private int mAddingObserverCounter;
    private final boolean mEnforceMainThread;
    private boolean mHandlingEvent;
    private final WeakReference<LifecycleOwner> mLifecycleOwner;
    private boolean mNewEventOccurred;
    private FastSafeIterableMap<LifecycleObserver, ObserverWithState> mObserverMap;
    private ArrayList<State> mParentStates;
    private State mState;
    
    public LifecycleRegistry(@NonNull final LifecycleOwner lifecycleOwner) {
        this(lifecycleOwner, true);
    }
    
    private LifecycleRegistry(@NonNull final LifecycleOwner referent, final boolean mEnforceMainThread) {
        this.mObserverMap = new FastSafeIterableMap<LifecycleObserver, ObserverWithState>();
        this.mAddingObserverCounter = 0;
        this.mHandlingEvent = false;
        this.mNewEventOccurred = false;
        this.mParentStates = new ArrayList<State>();
        this.mLifecycleOwner = new WeakReference<LifecycleOwner>(referent);
        this.mState = State.INITIALIZED;
        this.mEnforceMainThread = mEnforceMainThread;
    }
    
    private void backwardPass(final LifecycleOwner lifecycleOwner) {
        final Iterator<Map.Entry<Object, Object>> descendingIterator = this.mObserverMap.descendingIterator();
        while (descendingIterator.hasNext() && !this.mNewEventOccurred) {
            final Map.Entry<K, ObserverWithState> entry = descendingIterator.next();
            final ObserverWithState observerWithState = entry.getValue();
            while (observerWithState.mState.compareTo(this.mState) > 0 && !this.mNewEventOccurred && this.mObserverMap.contains((LifecycleObserver)entry.getKey())) {
                final Event down = Event.downFrom(observerWithState.mState);
                if (down == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("no event down from ");
                    sb.append(observerWithState.mState);
                    throw new IllegalStateException(sb.toString());
                }
                this.pushParentState(down.getTargetState());
                observerWithState.dispatchEvent(lifecycleOwner, down);
                this.popParentState();
            }
        }
    }
    
    private State calculateTargetState(final LifecycleObserver lifecycleObserver) {
        final Map.Entry<LifecycleObserver, ObserverWithState> ceil = this.mObserverMap.ceil(lifecycleObserver);
        State state = null;
        State mState;
        if (ceil != null) {
            mState = ceil.getValue().mState;
        }
        else {
            mState = null;
        }
        if (!this.mParentStates.isEmpty()) {
            final ArrayList<State> mParentStates = this.mParentStates;
            state = mParentStates.get(mParentStates.size() - 1);
        }
        return min(min(this.mState, mState), state);
    }
    
    @NonNull
    @VisibleForTesting
    public static LifecycleRegistry createUnsafe(@NonNull final LifecycleOwner lifecycleOwner) {
        return new LifecycleRegistry(lifecycleOwner, false);
    }
    
    @SuppressLint({ "RestrictedApi" })
    private void enforceMainThreadIfNeeded(final String str) {
        if (this.mEnforceMainThread && !ArchTaskExecutor.getInstance().isMainThread()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Method ");
            sb.append(str);
            sb.append(" must be called on the main thread");
            throw new IllegalStateException(sb.toString());
        }
    }
    
    private void forwardPass(final LifecycleOwner lifecycleOwner) {
        final SafeIterableMap.IteratorWithAdditions iteratorWithAdditions = this.mObserverMap.iteratorWithAdditions();
        while (iteratorWithAdditions.hasNext() && !this.mNewEventOccurred) {
            final Map.Entry<K, ObserverWithState> entry = ((Iterator<Map.Entry<K, ObserverWithState>>)iteratorWithAdditions).next();
            final ObserverWithState observerWithState = entry.getValue();
            while (observerWithState.mState.compareTo(this.mState) < 0 && !this.mNewEventOccurred && this.mObserverMap.contains((LifecycleObserver)entry.getKey())) {
                this.pushParentState(observerWithState.mState);
                final Event up = Event.upFrom(observerWithState.mState);
                if (up == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("no event up from ");
                    sb.append(observerWithState.mState);
                    throw new IllegalStateException(sb.toString());
                }
                observerWithState.dispatchEvent(lifecycleOwner, up);
                this.popParentState();
            }
        }
    }
    
    private boolean isSynced() {
        final int size = this.mObserverMap.size();
        boolean b = true;
        if (size == 0) {
            return true;
        }
        final State mState = this.mObserverMap.eldest().getValue().mState;
        final State mState2 = this.mObserverMap.newest().getValue().mState;
        if (mState != mState2 || this.mState != mState2) {
            b = false;
        }
        return b;
    }
    
    static State min(@NonNull final State o, @Nullable final State state) {
        State state2 = o;
        if (state != null) {
            state2 = o;
            if (state.compareTo(o) < 0) {
                state2 = state;
            }
        }
        return state2;
    }
    
    private void moveToState(final State mState) {
        final State mState2 = this.mState;
        if (mState2 == mState) {
            return;
        }
        if (mState2 == State.INITIALIZED && mState == State.DESTROYED) {
            final StringBuilder sb = new StringBuilder();
            sb.append("no event down from ");
            sb.append(this.mState);
            throw new IllegalStateException(sb.toString());
        }
        this.mState = mState;
        if (!this.mHandlingEvent && this.mAddingObserverCounter == 0) {
            this.mHandlingEvent = true;
            this.sync();
            this.mHandlingEvent = false;
            if (this.mState == State.DESTROYED) {
                this.mObserverMap = new FastSafeIterableMap<LifecycleObserver, ObserverWithState>();
            }
            return;
        }
        this.mNewEventOccurred = true;
    }
    
    private void popParentState() {
        final ArrayList<State> mParentStates = this.mParentStates;
        mParentStates.remove(mParentStates.size() - 1);
    }
    
    private void pushParentState(final State e) {
        this.mParentStates.add(e);
    }
    
    private void sync() {
        final LifecycleOwner lifecycleOwner = this.mLifecycleOwner.get();
        if (lifecycleOwner != null) {
            while (!this.isSynced()) {
                this.mNewEventOccurred = false;
                if (this.mState.compareTo(this.mObserverMap.eldest().getValue().mState) < 0) {
                    this.backwardPass(lifecycleOwner);
                }
                final Map.Entry<LifecycleObserver, ObserverWithState> newest = this.mObserverMap.newest();
                if (!this.mNewEventOccurred && newest != null && this.mState.compareTo(newest.getValue().mState) > 0) {
                    this.forwardPass(lifecycleOwner);
                }
            }
            this.mNewEventOccurred = false;
            return;
        }
        throw new IllegalStateException("LifecycleOwner of this LifecycleRegistry is alreadygarbage collected. It is too late to change lifecycle state.");
    }
    
    @Override
    public void addObserver(@NonNull final LifecycleObserver lifecycleObserver) {
        this.enforceMainThreadIfNeeded("addObserver");
        final State mState = this.mState;
        State state = State.DESTROYED;
        if (mState != state) {
            state = State.INITIALIZED;
        }
        final ObserverWithState observerWithState = new ObserverWithState(lifecycleObserver, state);
        if (this.mObserverMap.putIfAbsent(lifecycleObserver, observerWithState) != null) {
            return;
        }
        final LifecycleOwner lifecycleOwner = this.mLifecycleOwner.get();
        if (lifecycleOwner == null) {
            return;
        }
        final boolean b = this.mAddingObserverCounter != 0 || this.mHandlingEvent;
        State o = this.calculateTargetState(lifecycleObserver);
        ++this.mAddingObserverCounter;
        while (observerWithState.mState.compareTo(o) < 0 && this.mObserverMap.contains(lifecycleObserver)) {
            this.pushParentState(observerWithState.mState);
            final Event up = Event.upFrom(observerWithState.mState);
            if (up == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("no event up from ");
                sb.append(observerWithState.mState);
                throw new IllegalStateException(sb.toString());
            }
            observerWithState.dispatchEvent(lifecycleOwner, up);
            this.popParentState();
            o = this.calculateTargetState(lifecycleObserver);
        }
        if (!b) {
            this.sync();
        }
        --this.mAddingObserverCounter;
    }
    
    @NonNull
    @Override
    public State getCurrentState() {
        return this.mState;
    }
    
    public int getObserverCount() {
        this.enforceMainThreadIfNeeded("getObserverCount");
        return this.mObserverMap.size();
    }
    
    public void handleLifecycleEvent(@NonNull final Event event) {
        this.enforceMainThreadIfNeeded("handleLifecycleEvent");
        this.moveToState(event.getTargetState());
    }
    
    @Deprecated
    @MainThread
    public void markState(@NonNull final State currentState) {
        this.enforceMainThreadIfNeeded("markState");
        this.setCurrentState(currentState);
    }
    
    @Override
    public void removeObserver(@NonNull final LifecycleObserver lifecycleObserver) {
        this.enforceMainThreadIfNeeded("removeObserver");
        this.mObserverMap.remove(lifecycleObserver);
    }
    
    @MainThread
    public void setCurrentState(@NonNull final State state) {
        this.enforceMainThreadIfNeeded("setCurrentState");
        this.moveToState(state);
    }
    
    static class ObserverWithState
    {
        LifecycleEventObserver mLifecycleObserver;
        State mState;
        
        ObserverWithState(final LifecycleObserver lifecycleObserver, final State mState) {
            this.mLifecycleObserver = Lifecycling.lifecycleEventObserver(lifecycleObserver);
            this.mState = mState;
        }
        
        void dispatchEvent(final LifecycleOwner lifecycleOwner, final Event event) {
            final State targetState = event.getTargetState();
            this.mState = LifecycleRegistry.min(this.mState, targetState);
            this.mLifecycleObserver.onStateChanged(lifecycleOwner, event);
            this.mState = targetState;
        }
    }
}

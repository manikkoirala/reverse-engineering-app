// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.BuildersKt;
import kotlinx.coroutines.Dispatchers;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.Continuation;
import kotlinx.coroutines.CoroutineScope;
import kotlin.jvm.functions.Function2;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class PausingDispatcherKt
{
    public static final <T> Object whenCreated(@NotNull final Lifecycle lifecycle, @NotNull final Function2<? super CoroutineScope, ? super Continuation<? super T>, ?> function2, @NotNull final Continuation<? super T> continuation) {
        return whenStateAtLeast(lifecycle, Lifecycle.State.CREATED, (kotlin.jvm.functions.Function2<? super CoroutineScope, ? super kotlin.coroutines.Continuation<? super Object>, ?>)function2, (kotlin.coroutines.Continuation<? super Object>)continuation);
    }
    
    public static final <T> Object whenCreated(@NotNull final LifecycleOwner lifecycleOwner, @NotNull final Function2<? super CoroutineScope, ? super Continuation<? super T>, ?> function2, @NotNull final Continuation<? super T> continuation) {
        final Lifecycle lifecycle = lifecycleOwner.getLifecycle();
        Intrinsics.checkNotNullExpressionValue((Object)lifecycle, "lifecycle");
        return whenCreated(lifecycle, (kotlin.jvm.functions.Function2<? super CoroutineScope, ? super kotlin.coroutines.Continuation<? super Object>, ?>)function2, (kotlin.coroutines.Continuation<? super Object>)continuation);
    }
    
    public static final <T> Object whenResumed(@NotNull final Lifecycle lifecycle, @NotNull final Function2<? super CoroutineScope, ? super Continuation<? super T>, ?> function2, @NotNull final Continuation<? super T> continuation) {
        return whenStateAtLeast(lifecycle, Lifecycle.State.RESUMED, (kotlin.jvm.functions.Function2<? super CoroutineScope, ? super kotlin.coroutines.Continuation<? super Object>, ?>)function2, (kotlin.coroutines.Continuation<? super Object>)continuation);
    }
    
    public static final <T> Object whenResumed(@NotNull final LifecycleOwner lifecycleOwner, @NotNull final Function2<? super CoroutineScope, ? super Continuation<? super T>, ?> function2, @NotNull final Continuation<? super T> continuation) {
        final Lifecycle lifecycle = lifecycleOwner.getLifecycle();
        Intrinsics.checkNotNullExpressionValue((Object)lifecycle, "lifecycle");
        return whenResumed(lifecycle, (kotlin.jvm.functions.Function2<? super CoroutineScope, ? super kotlin.coroutines.Continuation<? super Object>, ?>)function2, (kotlin.coroutines.Continuation<? super Object>)continuation);
    }
    
    public static final <T> Object whenStarted(@NotNull final Lifecycle lifecycle, @NotNull final Function2<? super CoroutineScope, ? super Continuation<? super T>, ?> function2, @NotNull final Continuation<? super T> continuation) {
        return whenStateAtLeast(lifecycle, Lifecycle.State.STARTED, (kotlin.jvm.functions.Function2<? super CoroutineScope, ? super kotlin.coroutines.Continuation<? super Object>, ?>)function2, (kotlin.coroutines.Continuation<? super Object>)continuation);
    }
    
    public static final <T> Object whenStarted(@NotNull final LifecycleOwner lifecycleOwner, @NotNull final Function2<? super CoroutineScope, ? super Continuation<? super T>, ?> function2, @NotNull final Continuation<? super T> continuation) {
        final Lifecycle lifecycle = lifecycleOwner.getLifecycle();
        Intrinsics.checkNotNullExpressionValue((Object)lifecycle, "lifecycle");
        return whenStarted(lifecycle, (kotlin.jvm.functions.Function2<? super CoroutineScope, ? super kotlin.coroutines.Continuation<? super Object>, ?>)function2, (kotlin.coroutines.Continuation<? super Object>)continuation);
    }
    
    public static final <T> Object whenStateAtLeast(@NotNull final Lifecycle lifecycle, @NotNull final Lifecycle.State state, @NotNull final Function2<? super CoroutineScope, ? super Continuation<? super T>, ?> function2, @NotNull final Continuation<? super T> continuation) {
        return BuildersKt.Oo08((CoroutineContext)Dispatchers.\u3007o\u3007().\u30078(), (Function2)new PausingDispatcherKt$whenStateAtLeast.PausingDispatcherKt$whenStateAtLeast$2(lifecycle, state, (Function2)function2, (Continuation)null), (Continuation)continuation);
    }
}

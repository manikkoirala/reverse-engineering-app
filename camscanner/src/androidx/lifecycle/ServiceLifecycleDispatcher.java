// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.NonNull;
import android.os.Handler;

public class ServiceLifecycleDispatcher
{
    private final Handler mHandler;
    private DispatchRunnable mLastDispatchRunnable;
    private final LifecycleRegistry mRegistry;
    
    public ServiceLifecycleDispatcher(@NonNull final LifecycleOwner lifecycleOwner) {
        this.mRegistry = new LifecycleRegistry(lifecycleOwner);
        this.mHandler = new Handler();
    }
    
    private void postDispatchRunnable(final Lifecycle.Event event) {
        final DispatchRunnable mLastDispatchRunnable = this.mLastDispatchRunnable;
        if (mLastDispatchRunnable != null) {
            mLastDispatchRunnable.run();
        }
        final DispatchRunnable mLastDispatchRunnable2 = new DispatchRunnable(this.mRegistry, event);
        this.mLastDispatchRunnable = mLastDispatchRunnable2;
        this.mHandler.postAtFrontOfQueue((Runnable)mLastDispatchRunnable2);
    }
    
    @NonNull
    public Lifecycle getLifecycle() {
        return this.mRegistry;
    }
    
    public void onServicePreSuperOnBind() {
        this.postDispatchRunnable(Lifecycle.Event.ON_START);
    }
    
    public void onServicePreSuperOnCreate() {
        this.postDispatchRunnable(Lifecycle.Event.ON_CREATE);
    }
    
    public void onServicePreSuperOnDestroy() {
        this.postDispatchRunnable(Lifecycle.Event.ON_STOP);
        this.postDispatchRunnable(Lifecycle.Event.ON_DESTROY);
    }
    
    public void onServicePreSuperOnStart() {
        this.postDispatchRunnable(Lifecycle.Event.ON_START);
    }
    
    static class DispatchRunnable implements Runnable
    {
        final Lifecycle.Event mEvent;
        private final LifecycleRegistry mRegistry;
        private boolean mWasExecuted;
        
        DispatchRunnable(@NonNull final LifecycleRegistry mRegistry, final Lifecycle.Event mEvent) {
            this.mWasExecuted = false;
            this.mRegistry = mRegistry;
            this.mEvent = mEvent;
        }
        
        @Override
        public void run() {
            if (!this.mWasExecuted) {
                this.mRegistry.handleLifecycleEvent(this.mEvent);
                this.mWasExecuted = true;
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.Nullable;
import android.view.ViewParent;
import androidx.lifecycle.viewmodel.R;
import androidx.annotation.NonNull;
import android.view.View;

public class ViewTreeViewModelStoreOwner
{
    private ViewTreeViewModelStoreOwner() {
    }
    
    @Nullable
    public static ViewModelStoreOwner get(@NonNull final View view) {
        final ViewModelStoreOwner viewModelStoreOwner = (ViewModelStoreOwner)view.getTag(R.id.view_tree_view_model_store_owner);
        if (viewModelStoreOwner != null) {
            return viewModelStoreOwner;
        }
        ViewParent viewParent;
        ViewModelStoreOwner viewModelStoreOwner2;
        View view2;
        for (viewParent = view.getParent(), viewModelStoreOwner2 = viewModelStoreOwner; viewModelStoreOwner2 == null && viewParent instanceof View; viewModelStoreOwner2 = (ViewModelStoreOwner)view2.getTag(R.id.view_tree_view_model_store_owner), viewParent = view2.getParent()) {
            view2 = (View)viewParent;
        }
        return viewModelStoreOwner2;
    }
    
    public static void set(@NonNull final View view, @Nullable final ViewModelStoreOwner viewModelStoreOwner) {
        view.setTag(R.id.view_tree_view_model_store_owner, (Object)viewModelStoreOwner);
    }
}

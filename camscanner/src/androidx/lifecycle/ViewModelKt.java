// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.Job;
import kotlinx.coroutines.SupervisorKt;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineScope;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class ViewModelKt
{
    @NotNull
    private static final String JOB_KEY = "androidx.lifecycle.ViewModelCoroutineScope.JOB_KEY";
    
    @NotNull
    public static final CoroutineScope getViewModelScope(@NotNull final ViewModel viewModel) {
        Intrinsics.checkNotNullParameter((Object)viewModel, "<this>");
        final CoroutineScope coroutineScope = viewModel.getTag("androidx.lifecycle.ViewModelCoroutineScope.JOB_KEY");
        if (coroutineScope != null) {
            return coroutineScope;
        }
        final CloseableCoroutineScope setTagIfAbsent = viewModel.setTagIfAbsent("androidx.lifecycle.ViewModelCoroutineScope.JOB_KEY", new CloseableCoroutineScope(((CoroutineContext)SupervisorKt.\u3007o00\u3007\u3007Oo((Job)null, 1, (Object)null)).plus((CoroutineContext)Dispatchers.\u3007o\u3007().\u30078())));
        Intrinsics.checkNotNullExpressionValue((Object)setTagIfAbsent, "setTagIfAbsent(\n        \u2026Main.immediate)\n        )");
        return (CoroutineScope)setTagIfAbsent;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.util.concurrent.CancellationException;
import kotlinx.coroutines.Job$DefaultImpls;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.Job;
import org.jetbrains.annotations.NotNull;
import androidx.annotation.MainThread;
import kotlin.Metadata;

@Metadata
@MainThread
public final class LifecycleController
{
    @NotNull
    private final DispatchQueue dispatchQueue;
    @NotNull
    private final Lifecycle lifecycle;
    @NotNull
    private final Lifecycle.State minState;
    @NotNull
    private final LifecycleEventObserver observer;
    
    public LifecycleController(@NotNull final Lifecycle lifecycle, @NotNull final Lifecycle.State minState, @NotNull final DispatchQueue dispatchQueue, @NotNull final Job job) {
        Intrinsics.checkNotNullParameter((Object)lifecycle, "lifecycle");
        Intrinsics.checkNotNullParameter((Object)minState, "minState");
        Intrinsics.checkNotNullParameter((Object)dispatchQueue, "dispatchQueue");
        Intrinsics.checkNotNullParameter((Object)job, "parentJob");
        this.lifecycle = lifecycle;
        this.minState = minState;
        this.dispatchQueue = dispatchQueue;
        final Oo08 observer = new Oo08(this, job);
        this.observer = observer;
        if (lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
            Job$DefaultImpls.\u3007080(job, (CancellationException)null, 1, (Object)null);
            this.finish();
        }
        else {
            lifecycle.addObserver(observer);
        }
    }
    
    private final void handleDestroy(final Job job) {
        Job$DefaultImpls.\u3007080(job, (CancellationException)null, 1, (Object)null);
        this.finish();
    }
    
    private static final void observer$lambda-0(final LifecycleController lifecycleController, final Job job, final LifecycleOwner lifecycleOwner, final Lifecycle.Event event) {
        Intrinsics.checkNotNullParameter((Object)lifecycleController, "this$0");
        Intrinsics.checkNotNullParameter((Object)job, "$parentJob");
        Intrinsics.checkNotNullParameter((Object)lifecycleOwner, "source");
        Intrinsics.checkNotNullParameter((Object)event, "<anonymous parameter 1>");
        if (lifecycleOwner.getLifecycle().getCurrentState() == Lifecycle.State.DESTROYED) {
            Job$DefaultImpls.\u3007080(job, (CancellationException)null, 1, (Object)null);
            lifecycleController.finish();
        }
        else if (lifecycleOwner.getLifecycle().getCurrentState().compareTo(lifecycleController.minState) < 0) {
            lifecycleController.dispatchQueue.pause();
        }
        else {
            lifecycleController.dispatchQueue.resume();
        }
    }
    
    @MainThread
    public final void finish() {
        this.lifecycle.removeObserver(this.observer);
        this.dispatchQueue.finish();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.NonNull;

public interface DefaultLifecycleObserver extends FullLifecycleObserver
{
    void onCreate(@NonNull final LifecycleOwner p0);
    
    void onDestroy(@NonNull final LifecycleOwner p0);
    
    void onPause(@NonNull final LifecycleOwner p0);
    
    void onResume(@NonNull final LifecycleOwner p0);
    
    void onStart(@NonNull final LifecycleOwner p0);
    
    void onStop(@NonNull final LifecycleOwner p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.util.Iterator;
import androidx.annotation.NonNull;
import java.util.Map;
import androidx.annotation.Nullable;
import androidx.annotation.MainThread;
import androidx.arch.core.executor.ArchTaskExecutor;
import androidx.arch.core.internal.SafeIterableMap;

public abstract class LiveData<T>
{
    static final Object NOT_SET;
    static final int START_VERSION = -1;
    int mActiveCount;
    private boolean mChangingActiveState;
    private volatile Object mData;
    final Object mDataLock;
    private boolean mDispatchInvalidated;
    private boolean mDispatchingValue;
    private SafeIterableMap<Observer<? super T>, ObserverWrapper> mObservers;
    volatile Object mPendingData;
    private final Runnable mPostValueRunnable;
    private int mVersion;
    
    static {
        NOT_SET = new Object();
    }
    
    public LiveData() {
        this.mDataLock = new Object();
        this.mObservers = new SafeIterableMap<Observer<? super T>, ObserverWrapper>();
        this.mActiveCount = 0;
        final Object not_SET = LiveData.NOT_SET;
        this.mPendingData = not_SET;
        this.mPostValueRunnable = new Runnable() {
            final LiveData this$0;
            
            @Override
            public void run() {
                synchronized (this.this$0.mDataLock) {
                    final Object mPendingData = this.this$0.mPendingData;
                    this.this$0.mPendingData = LiveData.NOT_SET;
                    monitorexit(this.this$0.mDataLock);
                    this.this$0.setValue(mPendingData);
                }
            }
        };
        this.mData = not_SET;
        this.mVersion = -1;
    }
    
    public LiveData(final T mData) {
        this.mDataLock = new Object();
        this.mObservers = new SafeIterableMap<Observer<? super T>, ObserverWrapper>();
        this.mActiveCount = 0;
        this.mPendingData = LiveData.NOT_SET;
        this.mPostValueRunnable = new Runnable() {
            final LiveData this$0;
            
            @Override
            public void run() {
                synchronized (this.this$0.mDataLock) {
                    final Object mPendingData = this.this$0.mPendingData;
                    this.this$0.mPendingData = LiveData.NOT_SET;
                    monitorexit(this.this$0.mDataLock);
                    this.this$0.setValue(mPendingData);
                }
            }
        };
        this.mData = mData;
        this.mVersion = 0;
    }
    
    static void assertMainThread(final String str) {
        if (ArchTaskExecutor.getInstance().isMainThread()) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Cannot invoke ");
        sb.append(str);
        sb.append(" on a background thread");
        throw new IllegalStateException(sb.toString());
    }
    
    private void considerNotify(final ObserverWrapper observerWrapper) {
        if (!observerWrapper.mActive) {
            return;
        }
        if (!observerWrapper.shouldBeActive()) {
            observerWrapper.activeStateChanged(false);
            return;
        }
        final int mLastVersion = observerWrapper.mLastVersion;
        final int mVersion = this.mVersion;
        if (mLastVersion >= mVersion) {
            return;
        }
        observerWrapper.mLastVersion = mVersion;
        observerWrapper.mObserver.onChanged((Object)this.mData);
    }
    
    @MainThread
    void changeActiveCounter(int n) {
        int mActiveCount = this.mActiveCount;
        this.mActiveCount = n + mActiveCount;
        if (this.mChangingActiveState) {
            return;
        }
        this.mChangingActiveState = true;
        try {
            while (true) {
                final int mActiveCount2 = this.mActiveCount;
                if (mActiveCount == mActiveCount2) {
                    break;
                }
                if (mActiveCount == 0 && mActiveCount2 > 0) {
                    n = 1;
                }
                else {
                    n = 0;
                }
                final boolean b = mActiveCount > 0 && mActiveCount2 == 0;
                if (n != 0) {
                    this.onActive();
                }
                else if (b) {
                    this.onInactive();
                }
                mActiveCount = mActiveCount2;
            }
        }
        finally {
            this.mChangingActiveState = false;
        }
    }
    
    void dispatchingValue(@Nullable ObserverWrapper observerWrapper) {
        if (this.mDispatchingValue) {
            this.mDispatchInvalidated = true;
            return;
        }
        this.mDispatchingValue = true;
        do {
            this.mDispatchInvalidated = false;
            ObserverWrapper observerWrapper2 = null;
            Label_0086: {
                if (observerWrapper != null) {
                    this.considerNotify(observerWrapper);
                    observerWrapper2 = null;
                }
                else {
                    final SafeIterableMap.IteratorWithAdditions iteratorWithAdditions = this.mObservers.iteratorWithAdditions();
                    do {
                        observerWrapper2 = observerWrapper;
                        if (!iteratorWithAdditions.hasNext()) {
                            break Label_0086;
                        }
                        this.considerNotify(((Iterator<Map.Entry<K, ObserverWrapper>>)iteratorWithAdditions).next().getValue());
                    } while (!this.mDispatchInvalidated);
                    observerWrapper2 = observerWrapper;
                }
            }
            observerWrapper = observerWrapper2;
        } while (this.mDispatchInvalidated);
        this.mDispatchingValue = false;
    }
    
    @Nullable
    public T getValue() {
        final Object mData = this.mData;
        if (mData != LiveData.NOT_SET) {
            return (T)mData;
        }
        return null;
    }
    
    int getVersion() {
        return this.mVersion;
    }
    
    public boolean hasActiveObservers() {
        return this.mActiveCount > 0;
    }
    
    public boolean hasObservers() {
        return this.mObservers.size() > 0;
    }
    
    @MainThread
    public void observe(@NonNull final LifecycleOwner lifecycleOwner, @NonNull final Observer<? super T> observer) {
        assertMainThread("observe");
        if (lifecycleOwner.getLifecycle().getCurrentState() == Lifecycle.State.DESTROYED) {
            return;
        }
        final LifecycleBoundObserver lifecycleBoundObserver = new LifecycleBoundObserver(lifecycleOwner, observer);
        final ObserverWrapper observerWrapper = this.mObservers.putIfAbsent(observer, (ObserverWrapper)lifecycleBoundObserver);
        if (observerWrapper != null && !observerWrapper.isAttachedTo(lifecycleOwner)) {
            throw new IllegalArgumentException("Cannot add the same observer with different lifecycles");
        }
        if (observerWrapper != null) {
            return;
        }
        lifecycleOwner.getLifecycle().addObserver(lifecycleBoundObserver);
    }
    
    @MainThread
    public void observeForever(@NonNull final Observer<? super T> observer) {
        assertMainThread("observeForever");
        final AlwaysActiveObserver alwaysActiveObserver = new AlwaysActiveObserver(observer);
        final ObserverWrapper observerWrapper = this.mObservers.putIfAbsent(observer, (ObserverWrapper)alwaysActiveObserver);
        if (observerWrapper instanceof LifecycleBoundObserver) {
            throw new IllegalArgumentException("Cannot add the same observer with different lifecycles");
        }
        if (observerWrapper != null) {
            return;
        }
        ((ObserverWrapper)alwaysActiveObserver).activeStateChanged(true);
    }
    
    protected void onActive() {
    }
    
    protected void onInactive() {
    }
    
    protected void postValue(final T mPendingData) {
        synchronized (this.mDataLock) {
            final boolean b = this.mPendingData == LiveData.NOT_SET;
            this.mPendingData = mPendingData;
            monitorexit(this.mDataLock);
            if (!b) {
                return;
            }
            ArchTaskExecutor.getInstance().postToMainThread(this.mPostValueRunnable);
        }
    }
    
    @MainThread
    public void removeObserver(@NonNull final Observer<? super T> observer) {
        assertMainThread("removeObserver");
        final ObserverWrapper observerWrapper = this.mObservers.remove(observer);
        if (observerWrapper == null) {
            return;
        }
        observerWrapper.detachObserver();
        observerWrapper.activeStateChanged(false);
    }
    
    @MainThread
    public void removeObservers(@NonNull final LifecycleOwner lifecycleOwner) {
        assertMainThread("removeObservers");
        for (final Map.Entry<K, ObserverWrapper> entry : this.mObservers) {
            if (entry.getValue().isAttachedTo(lifecycleOwner)) {
                this.removeObserver((Observer<? super T>)entry.getKey());
            }
        }
    }
    
    @MainThread
    protected void setValue(final T mData) {
        assertMainThread("setValue");
        ++this.mVersion;
        this.mData = mData;
        this.dispatchingValue(null);
    }
    
    private class AlwaysActiveObserver extends ObserverWrapper
    {
        final LiveData this$0;
        
        AlwaysActiveObserver(final LiveData this$0, final Observer<? super T> observer) {
            super(observer);
        }
        
        @Override
        boolean shouldBeActive() {
            return true;
        }
    }
    
    private abstract class ObserverWrapper
    {
        boolean mActive;
        int mLastVersion;
        final Observer<? super T> mObserver;
        final LiveData this$0;
        
        ObserverWrapper(final LiveData this$0, final Observer<? super T> mObserver) {
            this.this$0 = this$0;
            this.mLastVersion = -1;
            this.mObserver = mObserver;
        }
        
        void activeStateChanged(final boolean mActive) {
            if (mActive == this.mActive) {
                return;
            }
            this.mActive = mActive;
            final LiveData this$0 = this.this$0;
            int n;
            if (mActive) {
                n = 1;
            }
            else {
                n = -1;
            }
            this$0.changeActiveCounter(n);
            if (this.mActive) {
                this.this$0.dispatchingValue(this);
            }
        }
        
        void detachObserver() {
        }
        
        boolean isAttachedTo(final LifecycleOwner lifecycleOwner) {
            return false;
        }
        
        abstract boolean shouldBeActive();
    }
    
    class LifecycleBoundObserver extends ObserverWrapper implements LifecycleEventObserver
    {
        @NonNull
        final LifecycleOwner mOwner;
        final LiveData this$0;
        
        LifecycleBoundObserver(@NonNull final LiveData this$0, final LifecycleOwner mOwner, final Observer<? super T> observer) {
            super(observer);
            this.mOwner = mOwner;
        }
        
        @Override
        void detachObserver() {
            this.mOwner.getLifecycle().removeObserver(this);
        }
        
        @Override
        boolean isAttachedTo(final LifecycleOwner lifecycleOwner) {
            return this.mOwner == lifecycleOwner;
        }
        
        @Override
        public void onStateChanged(@NonNull final LifecycleOwner lifecycleOwner, @NonNull final Lifecycle.Event event) {
            Enum<Lifecycle.State> currentState = this.mOwner.getLifecycle().getCurrentState();
            if (currentState == Lifecycle.State.DESTROYED) {
                this.this$0.removeObserver(super.mObserver);
                return;
            }
            Lifecycle.State currentState2;
            for (Enum<Lifecycle.State> enum1 = null; enum1 != currentState; enum1 = currentState, currentState = currentState2) {
                ((ObserverWrapper)this).activeStateChanged(this.shouldBeActive());
                currentState2 = this.mOwner.getLifecycle().getCurrentState();
            }
        }
        
        @Override
        boolean shouldBeActive() {
            return this.mOwner.getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED);
        }
    }
}

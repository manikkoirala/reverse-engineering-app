// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.BuildersKt;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.Job;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.CoroutineContext;
import kotlin.Metadata;
import kotlinx.coroutines.CoroutineScope;

@Metadata
public abstract class LifecycleCoroutineScope implements CoroutineScope
{
    @NotNull
    public abstract Lifecycle getLifecycle$lifecycle_runtime_ktx_release();
    
    @NotNull
    public final Job launchWhenCreated(@NotNull final Function2<? super CoroutineScope, ? super Continuation<? super Unit>, ?> function2) {
        Intrinsics.checkNotNullParameter((Object)function2, "block");
        return BuildersKt.O8((CoroutineScope)this, (CoroutineContext)null, (CoroutineStart)null, (Function2)new LifecycleCoroutineScope$launchWhenCreated.LifecycleCoroutineScope$launchWhenCreated$1(this, (Function2)function2, (Continuation)null), 3, (Object)null);
    }
    
    @NotNull
    public final Job launchWhenResumed(@NotNull final Function2<? super CoroutineScope, ? super Continuation<? super Unit>, ?> function2) {
        Intrinsics.checkNotNullParameter((Object)function2, "block");
        return BuildersKt.O8((CoroutineScope)this, (CoroutineContext)null, (CoroutineStart)null, (Function2)new LifecycleCoroutineScope$launchWhenResumed.LifecycleCoroutineScope$launchWhenResumed$1(this, (Function2)function2, (Continuation)null), 3, (Object)null);
    }
    
    @NotNull
    public final Job launchWhenStarted(@NotNull final Function2<? super CoroutineScope, ? super Continuation<? super Unit>, ?> function2) {
        Intrinsics.checkNotNullParameter((Object)function2, "block");
        return BuildersKt.O8((CoroutineScope)this, (CoroutineContext)null, (CoroutineStart)null, (Function2)new LifecycleCoroutineScope$launchWhenStarted.LifecycleCoroutineScope$launchWhenStarted$1(this, (Function2)function2, (Continuation)null), 3, (Object)null);
    }
}

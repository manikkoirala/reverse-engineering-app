// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlinx.coroutines.CoroutineScopeKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlinx.coroutines.CoroutineScope;
import kotlin.jvm.functions.Function2;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class RepeatOnLifecycleKt
{
    public static final Object repeatOnLifecycle(@NotNull final Lifecycle lifecycle, @NotNull final Lifecycle.State state, @NotNull final Function2<? super CoroutineScope, ? super Continuation<? super Unit>, ?> function2, @NotNull final Continuation<? super Unit> continuation) {
        if (state == Lifecycle.State.INITIALIZED) {
            throw new IllegalArgumentException("repeatOnLifecycle cannot start work with the INITIALIZED lifecycle state.".toString());
        }
        if (lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
            return Unit.\u3007080;
        }
        final Object oo08 = CoroutineScopeKt.Oo08((Function2)new RepeatOnLifecycleKt$repeatOnLifecycle.RepeatOnLifecycleKt$repeatOnLifecycle$3(lifecycle, state, (Function2)function2, (Continuation)null), (Continuation)continuation);
        if (oo08 == IntrinsicsKt.O8()) {
            return oo08;
        }
        return Unit.\u3007080;
    }
    
    public static final Object repeatOnLifecycle(@NotNull final LifecycleOwner lifecycleOwner, @NotNull final Lifecycle.State state, @NotNull final Function2<? super CoroutineScope, ? super Continuation<? super Unit>, ?> function2, @NotNull final Continuation<? super Unit> continuation) {
        final Lifecycle lifecycle = lifecycleOwner.getLifecycle();
        Intrinsics.checkNotNullExpressionValue((Object)lifecycle, "lifecycle");
        final Object repeatOnLifecycle = repeatOnLifecycle(lifecycle, state, function2, continuation);
        if (repeatOnLifecycle == IntrinsicsKt.O8()) {
            return repeatOnLifecycle;
        }
        return Unit.\u3007080;
    }
}

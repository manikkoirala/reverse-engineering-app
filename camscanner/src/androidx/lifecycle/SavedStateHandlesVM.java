// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.util.LinkedHashMap;
import org.jetbrains.annotations.NotNull;
import java.util.Map;
import kotlin.Metadata;

@Metadata
public final class SavedStateHandlesVM extends ViewModel
{
    @NotNull
    private final Map<String, SavedStateHandle> handles;
    
    public SavedStateHandlesVM() {
        this.handles = new LinkedHashMap<String, SavedStateHandle>();
    }
    
    @NotNull
    public final Map<String, SavedStateHandle> getHandles() {
        return this.handles;
    }
}

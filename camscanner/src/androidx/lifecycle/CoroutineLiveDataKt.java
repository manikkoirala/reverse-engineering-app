// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.coroutines.EmptyCoroutineContext;
import androidx.annotation.RequiresApi;
import java.time.Duration;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.BuildersKt;
import kotlinx.coroutines.Dispatchers;
import kotlin.coroutines.Continuation;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class CoroutineLiveDataKt
{
    public static final long DEFAULT_TIMEOUT = 5000L;
    
    public static final <T> Object addDisposableSource(@NotNull final MediatorLiveData<T> mediatorLiveData, @NotNull final LiveData<T> liveData, @NotNull final Continuation<? super EmittedSource> continuation) {
        return BuildersKt.Oo08((CoroutineContext)Dispatchers.\u3007o\u3007().\u30078(), (Function2)new CoroutineLiveDataKt$addDisposableSource.CoroutineLiveDataKt$addDisposableSource$2((MediatorLiveData)mediatorLiveData, (LiveData)liveData, (Continuation)null), (Continuation)continuation);
    }
    
    @NotNull
    public static final <T> LiveData<T> liveData(@NotNull final CoroutineContext coroutineContext, final long n, @NotNull final Function2<? super LiveDataScope<T>, ? super Continuation<? super Unit>, ?> function2) {
        Intrinsics.checkNotNullParameter((Object)coroutineContext, "context");
        Intrinsics.checkNotNullParameter((Object)function2, "block");
        return new CoroutineLiveData<T>(coroutineContext, n, function2);
    }
    
    @RequiresApi(26)
    @NotNull
    public static final <T> LiveData<T> liveData(@NotNull final CoroutineContext coroutineContext, @NotNull final Duration duration, @NotNull final Function2<? super LiveDataScope<T>, ? super Continuation<? super Unit>, ?> function2) {
        Intrinsics.checkNotNullParameter((Object)coroutineContext, "context");
        Intrinsics.checkNotNullParameter((Object)duration, "timeout");
        Intrinsics.checkNotNullParameter((Object)function2, "block");
        return new CoroutineLiveData<T>(coroutineContext, \u3007080.\u3007080(duration), function2);
    }
}

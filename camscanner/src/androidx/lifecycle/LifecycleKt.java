// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.Job;
import kotlinx.coroutines.SupervisorKt;
import java.util.concurrent.atomic.AtomicReference;
import androidx.camera.view.OO0o\u3007\u3007\u3007\u30070;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class LifecycleKt
{
    @NotNull
    public static final LifecycleCoroutineScope getCoroutineScope(@NotNull final Lifecycle lifecycle) {
        Intrinsics.checkNotNullParameter((Object)lifecycle, "<this>");
        LifecycleCoroutineScopeImpl lifecycleCoroutineScopeImpl;
        do {
            final LifecycleCoroutineScopeImpl lifecycleCoroutineScopeImpl2 = lifecycle.mInternalScopeRef.get();
            if (lifecycleCoroutineScopeImpl2 != null) {
                return lifecycleCoroutineScopeImpl2;
            }
            lifecycleCoroutineScopeImpl = new LifecycleCoroutineScopeImpl(lifecycle, ((CoroutineContext)SupervisorKt.\u3007o00\u3007\u3007Oo((Job)null, 1, (Object)null)).plus((CoroutineContext)Dispatchers.\u3007o\u3007().\u30078()));
        } while (!OO0o\u3007\u3007\u3007\u30070.\u3007080(lifecycle.mInternalScopeRef, null, lifecycleCoroutineScopeImpl));
        lifecycleCoroutineScopeImpl.register();
        return lifecycleCoroutineScopeImpl;
    }
}

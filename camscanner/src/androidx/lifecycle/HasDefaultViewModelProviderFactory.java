// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.NonNull;
import androidx.lifecycle.viewmodel.CreationExtras;

public interface HasDefaultViewModelProviderFactory
{
    @NonNull
    CreationExtras getDefaultViewModelCreationExtras();
    
    @NonNull
    ViewModelProvider.Factory getDefaultViewModelProviderFactory();
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.NonNull;

public interface ViewModelStoreOwner
{
    @NonNull
    ViewModelStore getViewModelStore();
}

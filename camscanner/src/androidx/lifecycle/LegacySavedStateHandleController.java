// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.util.Iterator;
import androidx.savedstate.SavedStateRegistryOwner;
import androidx.annotation.NonNull;
import android.os.Bundle;
import androidx.savedstate.SavedStateRegistry;

class LegacySavedStateHandleController
{
    static final String TAG_SAVED_STATE_HANDLE_CONTROLLER = "androidx.lifecycle.savedstate.vm.tag";
    
    private LegacySavedStateHandleController() {
    }
    
    static void attachHandleIfNeeded(final ViewModel viewModel, final SavedStateRegistry savedStateRegistry, final Lifecycle lifecycle) {
        final SavedStateHandleController savedStateHandleController = viewModel.getTag("androidx.lifecycle.savedstate.vm.tag");
        if (savedStateHandleController != null && !savedStateHandleController.isAttached()) {
            savedStateHandleController.attachToLifecycle(savedStateRegistry, lifecycle);
            tryToAddRecreator(savedStateRegistry, lifecycle);
        }
    }
    
    static SavedStateHandleController create(final SavedStateRegistry savedStateRegistry, final Lifecycle lifecycle, final String s, final Bundle bundle) {
        final SavedStateHandleController savedStateHandleController = new SavedStateHandleController(s, SavedStateHandle.createHandle(savedStateRegistry.consumeRestoredStateForKey(s), bundle));
        savedStateHandleController.attachToLifecycle(savedStateRegistry, lifecycle);
        tryToAddRecreator(savedStateRegistry, lifecycle);
        return savedStateHandleController;
    }
    
    private static void tryToAddRecreator(final SavedStateRegistry savedStateRegistry, final Lifecycle lifecycle) {
        final Lifecycle.State currentState = lifecycle.getCurrentState();
        if (currentState != Lifecycle.State.INITIALIZED && !currentState.isAtLeast(Lifecycle.State.STARTED)) {
            lifecycle.addObserver(new LifecycleEventObserver(lifecycle, savedStateRegistry) {
                final Lifecycle val$lifecycle;
                final SavedStateRegistry val$registry;
                
                @Override
                public void onStateChanged(@NonNull final LifecycleOwner lifecycleOwner, @NonNull final Lifecycle.Event event) {
                    if (event == Lifecycle.Event.ON_START) {
                        this.val$lifecycle.removeObserver(this);
                        this.val$registry.runOnNextRecreation((Class<? extends SavedStateRegistry.AutoRecreated>)OnRecreation.class);
                    }
                }
            });
        }
        else {
            savedStateRegistry.runOnNextRecreation((Class<? extends SavedStateRegistry.AutoRecreated>)OnRecreation.class);
        }
    }
    
    static final class OnRecreation implements AutoRecreated
    {
        @Override
        public void onRecreated(@NonNull final SavedStateRegistryOwner savedStateRegistryOwner) {
            if (savedStateRegistryOwner instanceof ViewModelStoreOwner) {
                final ViewModelStore viewModelStore = ((ViewModelStoreOwner)savedStateRegistryOwner).getViewModelStore();
                final SavedStateRegistry savedStateRegistry = savedStateRegistryOwner.getSavedStateRegistry();
                final Iterator<String> iterator = viewModelStore.keys().iterator();
                while (iterator.hasNext()) {
                    LegacySavedStateHandleController.attachHandleIfNeeded(viewModelStore.get(iterator.next()), savedStateRegistry, savedStateRegistryOwner.getLifecycle());
                }
                if (!viewModelStore.keys().isEmpty()) {
                    savedStateRegistry.runOnNextRecreation((Class<? extends SavedStateRegistry.AutoRecreated>)OnRecreation.class);
                }
                return;
            }
            throw new IllegalStateException("Internal error: OnRecreation should be registered only on components that implement ViewModelStoreOwner");
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

public interface Observer<T>
{
    void onChanged(final T p0);
}

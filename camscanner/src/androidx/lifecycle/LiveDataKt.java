// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.MainThread;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class LiveDataKt
{
    @MainThread
    @NotNull
    public static final <T> Observer<T> observe(@NotNull final LiveData<T> liveData, @NotNull final LifecycleOwner lifecycleOwner, @NotNull final Function1<? super T, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)liveData, "<this>");
        Intrinsics.checkNotNullParameter((Object)lifecycleOwner, "owner");
        Intrinsics.checkNotNullParameter((Object)function1, "onChanged");
        final LiveDataKt$observe$wrappedObserver.LiveDataKt$observe$wrappedObserver$1 liveDataKt$observe$wrappedObserver$1 = new LiveDataKt$observe$wrappedObserver.LiveDataKt$observe$wrappedObserver$1((Function1)function1);
        liveData.observe(lifecycleOwner, (Observer<? super T>)liveDataKt$observe$wrappedObserver$1);
        return (Observer<T>)liveDataKt$observe$wrappedObserver$1;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import android.os.BaseBundle;
import java.util.Iterator;
import java.util.Map;
import kotlin.jvm.functions.Function0;
import kotlin.LazyKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Lazy;
import org.jetbrains.annotations.NotNull;
import android.os.Bundle;
import kotlin.Metadata;
import androidx.savedstate.SavedStateRegistry;

@Metadata
public final class SavedStateHandlesProvider implements SavedStateProvider
{
    private boolean restored;
    private Bundle restoredState;
    @NotNull
    private final SavedStateRegistry savedStateRegistry;
    @NotNull
    private final Lazy viewModel$delegate;
    
    public SavedStateHandlesProvider(@NotNull final SavedStateRegistry savedStateRegistry, @NotNull final ViewModelStoreOwner viewModelStoreOwner) {
        Intrinsics.checkNotNullParameter((Object)savedStateRegistry, "savedStateRegistry");
        Intrinsics.checkNotNullParameter((Object)viewModelStoreOwner, "viewModelStoreOwner");
        this.savedStateRegistry = savedStateRegistry;
        this.viewModel$delegate = LazyKt.\u3007o00\u3007\u3007Oo((Function0)new SavedStateHandlesProvider$viewModel.SavedStateHandlesProvider$viewModel$2(viewModelStoreOwner));
    }
    
    private final SavedStateHandlesVM getViewModel() {
        return (SavedStateHandlesVM)this.viewModel$delegate.getValue();
    }
    
    public final Bundle consumeRestoredStateForKey(@NotNull final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        this.performRestore();
        final Bundle restoredState = this.restoredState;
        Bundle bundle;
        if (restoredState != null) {
            bundle = restoredState.getBundle(s);
        }
        else {
            bundle = null;
        }
        final Bundle restoredState2 = this.restoredState;
        if (restoredState2 != null) {
            restoredState2.remove(s);
        }
        final Bundle restoredState3 = this.restoredState;
        int n = 0;
        if (restoredState3 != null) {
            n = n;
            if (((BaseBundle)restoredState3).isEmpty()) {
                n = 1;
            }
        }
        if (n != 0) {
            this.restoredState = null;
        }
        return bundle;
    }
    
    public final void performRestore() {
        if (!this.restored) {
            this.restoredState = this.savedStateRegistry.consumeRestoredStateForKey("androidx.lifecycle.internal.SavedStateHandlesProvider");
            this.restored = true;
            this.getViewModel();
        }
    }
    
    @NotNull
    @Override
    public Bundle saveState() {
        final Bundle bundle = new Bundle();
        final Bundle restoredState = this.restoredState;
        if (restoredState != null) {
            bundle.putAll(restoredState);
        }
        for (final Map.Entry<String, V> entry : this.getViewModel().getHandles().entrySet()) {
            final String s = entry.getKey();
            final Bundle saveState = ((SavedStateHandle)entry.getValue()).savedStateProvider().saveState();
            if (!Intrinsics.\u3007o\u3007((Object)saveState, (Object)Bundle.EMPTY)) {
                bundle.putBundle(s, saveState);
            }
        }
        this.restored = false;
        return bundle;
    }
}

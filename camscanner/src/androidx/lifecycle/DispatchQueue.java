// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlinx.coroutines.CoroutineDispatcher;
import androidx.annotation.AnyThread;
import android.annotation.SuppressLint;
import kotlinx.coroutines.MainCoroutineDispatcher;
import kotlinx.coroutines.Dispatchers;
import kotlin.coroutines.CoroutineContext;
import androidx.annotation.MainThread;
import kotlin.jvm.internal.Intrinsics;
import java.util.ArrayDeque;
import org.jetbrains.annotations.NotNull;
import java.util.Queue;
import kotlin.Metadata;

@Metadata
public final class DispatchQueue
{
    private boolean finished;
    private boolean isDraining;
    private boolean paused;
    @NotNull
    private final Queue<Runnable> queue;
    
    public DispatchQueue() {
        this.paused = true;
        this.queue = new ArrayDeque<Runnable>();
    }
    
    private static final void dispatchAndEnqueue$lambda-2$lambda-1(final DispatchQueue dispatchQueue, final Runnable runnable) {
        Intrinsics.checkNotNullParameter((Object)dispatchQueue, "this$0");
        Intrinsics.checkNotNullParameter((Object)runnable, "$runnable");
        dispatchQueue.enqueue(runnable);
    }
    
    @MainThread
    private final void enqueue(final Runnable runnable) {
        if (this.queue.offer(runnable)) {
            this.drainQueue();
            return;
        }
        throw new IllegalStateException("cannot enqueue any more runnables".toString());
    }
    
    @MainThread
    public final boolean canRun() {
        return this.finished || !this.paused;
    }
    
    @SuppressLint({ "WrongThread" })
    @AnyThread
    public final void dispatchAndEnqueue(@NotNull final CoroutineContext coroutineContext, @NotNull final Runnable runnable) {
        Intrinsics.checkNotNullParameter((Object)coroutineContext, "context");
        Intrinsics.checkNotNullParameter((Object)runnable, "runnable");
        final MainCoroutineDispatcher \u30078 = Dispatchers.\u3007o\u3007().\u30078();
        if (!((CoroutineDispatcher)\u30078).isDispatchNeeded(coroutineContext) && !this.canRun()) {
            this.enqueue(runnable);
        }
        else {
            ((CoroutineDispatcher)\u30078).dispatch(coroutineContext, (Runnable)new \u3007o\u3007(this, runnable));
        }
    }
    
    @MainThread
    public final void drainQueue() {
        if (this.isDraining) {
            return;
        }
        try {
            this.isDraining = true;
            while ((this.queue.isEmpty() ^ true) && this.canRun()) {
                final Runnable runnable = this.queue.poll();
                if (runnable != null) {
                    runnable.run();
                }
            }
        }
        finally {
            this.isDraining = false;
        }
    }
    
    @MainThread
    public final void finish() {
        this.finished = true;
        this.drainQueue();
    }
    
    @MainThread
    public final void pause() {
        this.paused = true;
    }
    
    @MainThread
    public final void resume() {
        if (!this.paused) {
            return;
        }
        if (this.finished ^ true) {
            this.paused = false;
            this.drainQueue();
            return;
        }
        throw new IllegalStateException("Cannot resume a finished dispatcher".toString());
    }
}

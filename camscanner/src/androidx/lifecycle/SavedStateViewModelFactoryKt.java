// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collection;
import kotlin.collections.ArraysKt;
import kotlin.jvm.internal.Intrinsics;
import java.lang.reflect.Constructor;
import kotlin.collections.CollectionsKt;
import android.app.Application;
import org.jetbrains.annotations.NotNull;
import java.util.List;
import androidx.annotation.RestrictTo;
import kotlin.Metadata;

@Metadata
@RestrictTo({ RestrictTo.Scope.LIBRARY })
public final class SavedStateViewModelFactoryKt
{
    @NotNull
    private static final List<Class<?>> ANDROID_VIEWMODEL_SIGNATURE;
    @NotNull
    private static final List<Class<?>> VIEWMODEL_SIGNATURE;
    
    static {
        ANDROID_VIEWMODEL_SIGNATURE = CollectionsKt.\u3007O8o08O((Object[])new Class[] { Application.class, SavedStateHandle.class });
        VIEWMODEL_SIGNATURE = CollectionsKt.O8((Object)SavedStateHandle.class);
    }
    
    public static final <T> Constructor<T> findMatchingConstructor(@NotNull final Class<T> clazz, @NotNull final List<? extends Class<?>> obj) {
        Intrinsics.checkNotNullParameter((Object)clazz, "modelClass");
        Intrinsics.checkNotNullParameter((Object)obj, "signature");
        final Constructor[] constructors = clazz.getConstructors();
        Intrinsics.checkNotNullExpressionValue((Object)constructors, "modelClass.constructors");
        for (final Constructor constructor : constructors) {
            final Class[] parameterTypes = constructor.getParameterTypes();
            Intrinsics.checkNotNullExpressionValue((Object)parameterTypes, "constructor.parameterTypes");
            final List o000 = ArraysKt.O000((Object[])parameterTypes);
            if (Intrinsics.\u3007o\u3007((Object)obj, (Object)o000)) {
                return constructor;
            }
            if (obj.size() == o000.size() && o000.containsAll(obj)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Class ");
                sb.append(clazz.getSimpleName());
                sb.append(" must have parameters in the proper order: ");
                sb.append(obj);
                throw new UnsupportedOperationException(sb.toString());
            }
        }
        return null;
    }
    
    public static final <T extends ViewModel> T newInstance(@NotNull final Class<T> obj, @NotNull final Constructor<T> constructor, @NotNull final Object... original) {
        Intrinsics.checkNotNullParameter((Object)obj, "modelClass");
        Intrinsics.checkNotNullParameter((Object)constructor, "constructor");
        Intrinsics.checkNotNullParameter((Object)original, "params");
        try {
            return constructor.newInstance(Arrays.copyOf(original, original.length));
        }
        catch (final InvocationTargetException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("An exception happened in constructor of ");
            sb.append(obj);
            throw new RuntimeException(sb.toString(), ex.getCause());
        }
        catch (final InstantiationException cause) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("A ");
            sb2.append(obj);
            sb2.append(" cannot be instantiated.");
            throw new RuntimeException(sb2.toString(), cause);
        }
        catch (final IllegalAccessException cause2) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Failed to access ");
            sb3.append(obj);
            throw new RuntimeException(sb3.toString(), cause2);
        }
    }
}

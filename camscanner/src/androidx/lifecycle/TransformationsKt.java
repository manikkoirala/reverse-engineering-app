// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.arch.core.util.Function;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class TransformationsKt
{
    @NotNull
    public static final <X> LiveData<X> distinctUntilChanged(@NotNull final LiveData<X> liveData) {
        Intrinsics.checkNotNullParameter((Object)liveData, "<this>");
        final LiveData<X> distinctUntilChanged = Transformations.distinctUntilChanged(liveData);
        Intrinsics.checkNotNullExpressionValue((Object)distinctUntilChanged, "distinctUntilChanged(this)");
        return distinctUntilChanged;
    }
    
    @NotNull
    public static final <X, Y> LiveData<Y> map(@NotNull final LiveData<X> liveData, @NotNull final Function1<? super X, ? extends Y> function1) {
        Intrinsics.checkNotNullParameter((Object)liveData, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "transform");
        final LiveData<Object> map = Transformations.map(liveData, (Function<X, Object>)new TransformationsKt$map.TransformationsKt$map$1((Function1)function1));
        Intrinsics.checkNotNullExpressionValue((Object)map, "crossinline transform: (\u2026p(this) { transform(it) }");
        return (LiveData<Y>)map;
    }
    
    @NotNull
    public static final <X, Y> LiveData<Y> switchMap(@NotNull final LiveData<X> liveData, @NotNull final Function1<? super X, ? extends LiveData<Y>> function1) {
        Intrinsics.checkNotNullParameter((Object)liveData, "<this>");
        Intrinsics.checkNotNullParameter((Object)function1, "transform");
        final LiveData<Object> switchMap = Transformations.switchMap(liveData, (Function<X, LiveData<Object>>)new TransformationsKt$switchMap.TransformationsKt$switchMap$1((Function1)function1));
        Intrinsics.checkNotNullExpressionValue((Object)switchMap, "crossinline transform: (\u2026p(this) { transform(it) }");
        return (LiveData<Y>)switchMap;
    }
}

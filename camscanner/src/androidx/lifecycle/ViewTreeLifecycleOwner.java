// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.Nullable;
import android.view.ViewParent;
import androidx.lifecycle.runtime.R;
import androidx.annotation.NonNull;
import android.view.View;

public class ViewTreeLifecycleOwner
{
    private ViewTreeLifecycleOwner() {
    }
    
    @Nullable
    public static LifecycleOwner get(@NonNull View view) {
        LifecycleOwner lifecycleOwner = (LifecycleOwner)view.getTag(R.id.view_tree_lifecycle_owner);
        if (lifecycleOwner != null) {
            return lifecycleOwner;
        }
        for (ViewParent viewParent = view.getParent(); lifecycleOwner == null && viewParent instanceof View; lifecycleOwner = (LifecycleOwner)view.getTag(R.id.view_tree_lifecycle_owner), viewParent = view.getParent()) {
            view = (View)viewParent;
        }
        return lifecycleOwner;
    }
    
    public static void set(@NonNull final View view, @Nullable final LifecycleOwner lifecycleOwner) {
        view.setTag(R.id.view_tree_lifecycle_owner, (Object)lifecycleOwner);
    }
}

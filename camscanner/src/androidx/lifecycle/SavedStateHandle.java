// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import android.os.BaseBundle;
import java.util.HashMap;
import kotlin.collections.SetsKt;
import kotlinx.coroutines.flow.FlowKt;
import kotlinx.coroutines.flow.StateFlowKt;
import kotlinx.coroutines.flow.StateFlow;
import androidx.annotation.MainThread;
import java.util.Set;
import java.util.Iterator;
import androidx.core.os.BundleKt;
import kotlin.TuplesKt;
import kotlin.Pair;
import kotlin.collections.MapsKt;
import androidx.annotation.RestrictTo;
import kotlin.jvm.internal.Intrinsics;
import java.util.LinkedHashMap;
import android.util.SizeF;
import android.util.Size;
import android.util.SparseArray;
import java.io.Serializable;
import android.os.Parcelable;
import java.util.ArrayList;
import android.os.Bundle;
import android.os.Binder;
import kotlin.jvm.internal.DefaultConstructorMarker;
import androidx.savedstate.SavedStateRegistry;
import kotlinx.coroutines.flow.MutableStateFlow;
import java.util.Map;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class SavedStateHandle
{
    @NotNull
    private static final Class<?>[] ACCEPTABLE_CLASSES;
    @NotNull
    public static final Companion Companion;
    @NotNull
    private static final String KEYS = "keys";
    @NotNull
    private static final String VALUES = "values";
    @NotNull
    private final Map<String, MutableStateFlow<Object>> flows;
    @NotNull
    private final Map<String, SavingStateLiveData<?>> liveDatas;
    @NotNull
    private final Map<String, Object> regular;
    @NotNull
    private final SavedStateRegistry.SavedStateProvider savedStateProvider;
    @NotNull
    private final Map<String, SavedStateRegistry.SavedStateProvider> savedStateProviders;
    
    static {
        Companion = new Companion(null);
        ACCEPTABLE_CLASSES = new Class[] { Boolean.TYPE, boolean[].class, Double.TYPE, double[].class, Integer.TYPE, int[].class, Long.TYPE, long[].class, String.class, String[].class, Binder.class, Bundle.class, Byte.TYPE, byte[].class, Character.TYPE, char[].class, CharSequence.class, CharSequence[].class, ArrayList.class, Float.TYPE, float[].class, Parcelable.class, Parcelable[].class, Serializable.class, Short.TYPE, short[].class, SparseArray.class, Size.class, SizeF.class };
    }
    
    public SavedStateHandle() {
        this.regular = new LinkedHashMap<String, Object>();
        this.savedStateProviders = new LinkedHashMap<String, SavedStateRegistry.SavedStateProvider>();
        this.liveDatas = new LinkedHashMap<String, SavingStateLiveData<?>>();
        this.flows = new LinkedHashMap<String, MutableStateFlow<Object>>();
        this.savedStateProvider = new oO80(this);
    }
    
    public SavedStateHandle(@NotNull final Map<String, ?> map) {
        Intrinsics.checkNotNullParameter((Object)map, "initialState");
        final LinkedHashMap regular = new LinkedHashMap();
        this.regular = regular;
        this.savedStateProviders = new LinkedHashMap<String, SavedStateRegistry.SavedStateProvider>();
        this.liveDatas = new LinkedHashMap<String, SavingStateLiveData<?>>();
        this.flows = new LinkedHashMap<String, MutableStateFlow<Object>>();
        this.savedStateProvider = new oO80(this);
        regular.putAll(map);
    }
    
    public static final /* synthetic */ Class[] access$getACCEPTABLE_CLASSES$cp() {
        return SavedStateHandle.ACCEPTABLE_CLASSES;
    }
    
    public static final /* synthetic */ Map access$getFlows$p(final SavedStateHandle savedStateHandle) {
        return savedStateHandle.flows;
    }
    
    public static final /* synthetic */ Map access$getRegular$p(final SavedStateHandle savedStateHandle) {
        return savedStateHandle.regular;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @NotNull
    public static final SavedStateHandle createHandle(final Bundle bundle, final Bundle bundle2) {
        return SavedStateHandle.Companion.createHandle(bundle, bundle2);
    }
    
    private final <T> MutableLiveData<T> getLiveDataInternal(final String s, final boolean b, final T t) {
        final SavingStateLiveData<?> value = this.liveDatas.get(s);
        MutableLiveData mutableLiveData;
        if (value instanceof MutableLiveData) {
            mutableLiveData = value;
        }
        else {
            mutableLiveData = null;
        }
        if (mutableLiveData != null) {
            return mutableLiveData;
        }
        SavingStateLiveData<Object> savingStateLiveData;
        if (this.regular.containsKey(s)) {
            savingStateLiveData = new SavingStateLiveData<Object>(this, s, this.regular.get(s));
        }
        else if (b) {
            this.regular.put(s, t);
            savingStateLiveData = new SavingStateLiveData<Object>(this, s, t);
        }
        else {
            savingStateLiveData = new SavingStateLiveData<Object>(this, s);
        }
        this.liveDatas.put(s, savingStateLiveData);
        return (MutableLiveData<T>)savingStateLiveData;
    }
    
    private static final Bundle savedStateProvider$lambda-0(final SavedStateHandle savedStateHandle) {
        Intrinsics.checkNotNullParameter((Object)savedStateHandle, "this$0");
        for (final Map.Entry<String, V> entry : MapsKt.\u3007O\u3007((Map)savedStateHandle.savedStateProviders).entrySet()) {
            savedStateHandle.set(entry.getKey(), ((SavedStateRegistry.SavedStateProvider)entry.getValue()).saveState());
        }
        final Set<String> keySet = savedStateHandle.regular.keySet();
        final ArrayList list = new ArrayList(keySet.size());
        final ArrayList list2 = new ArrayList<Object>(list.size());
        for (final String e : keySet) {
            list.add((Object)e);
            list2.add(savedStateHandle.regular.get(e));
        }
        return BundleKt.bundleOf(TuplesKt.\u3007080((Object)"keys", (Object)list), TuplesKt.\u3007080((Object)"values", (Object)list2));
    }
    
    @MainThread
    public final void clearSavedStateProvider(@NotNull final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        this.savedStateProviders.remove(s);
    }
    
    @MainThread
    public final boolean contains(@NotNull final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        return this.regular.containsKey(s);
    }
    
    @MainThread
    public final <T> T get(@NotNull final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        return (T)this.regular.get(s);
    }
    
    @MainThread
    @NotNull
    public final <T> MutableLiveData<T> getLiveData(@NotNull final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        return this.getLiveDataInternal(s, false, (T)null);
    }
    
    @MainThread
    @NotNull
    public final <T> MutableLiveData<T> getLiveData(@NotNull final String s, final T t) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        return this.getLiveDataInternal(s, true, t);
    }
    
    @MainThread
    @NotNull
    public final <T> StateFlow<T> getStateFlow(@NotNull final String s, final T t) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        final Map<String, MutableStateFlow<Object>> flows = this.flows;
        MutableStateFlow mutableStateFlow;
        if ((mutableStateFlow = flows.get(s)) == null) {
            if (!this.regular.containsKey(s)) {
                this.regular.put(s, t);
            }
            mutableStateFlow = StateFlowKt.\u3007080(this.regular.get(s));
            this.flows.put(s, (MutableStateFlow<Object>)mutableStateFlow);
            flows.put(s, (MutableStateFlow<Object>)mutableStateFlow);
        }
        return (StateFlow<T>)FlowKt.\u3007o00\u3007\u3007Oo((MutableStateFlow)mutableStateFlow);
    }
    
    @MainThread
    @NotNull
    public final Set<String> keys() {
        return SetsKt.oO80(SetsKt.oO80((Set)this.regular.keySet(), (Iterable)this.savedStateProviders.keySet()), (Iterable)this.liveDatas.keySet());
    }
    
    @MainThread
    public final <T> T remove(@NotNull final String s) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        final Object remove = this.regular.remove(s);
        final SavingStateLiveData savingStateLiveData = this.liveDatas.remove(s);
        if (savingStateLiveData != null) {
            savingStateLiveData.detach();
        }
        this.flows.remove(s);
        return (T)remove;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @NotNull
    public final SavedStateRegistry.SavedStateProvider savedStateProvider() {
        return this.savedStateProvider;
    }
    
    @MainThread
    public final <T> void set(@NotNull final String s, final T t) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        if (SavedStateHandle.Companion.validateValue(t)) {
            final SavingStateLiveData<?> value = this.liveDatas.get(s);
            MutableLiveData mutableLiveData;
            if (value instanceof MutableLiveData) {
                mutableLiveData = value;
            }
            else {
                mutableLiveData = null;
            }
            if (mutableLiveData != null) {
                mutableLiveData.setValue(t);
            }
            else {
                this.regular.put(s, t);
            }
            final MutableStateFlow mutableStateFlow = this.flows.get(s);
            if (mutableStateFlow != null) {
                mutableStateFlow.setValue((Object)t);
            }
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Can't put value with type ");
        Intrinsics.Oo08((Object)t);
        sb.append(t.getClass());
        sb.append(" into saved state");
        throw new IllegalArgumentException(sb.toString());
    }
    
    @MainThread
    public final void setSavedStateProvider(@NotNull final String s, @NotNull final SavedStateRegistry.SavedStateProvider savedStateProvider) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        Intrinsics.checkNotNullParameter((Object)savedStateProvider, "provider");
        this.savedStateProviders.put(s, savedStateProvider);
    }
    
    @Metadata
    public static final class Companion
    {
        private Companion() {
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        @NotNull
        public final SavedStateHandle createHandle(final Bundle bundle, final Bundle bundle2) {
            if (bundle == null) {
                SavedStateHandle savedStateHandle;
                if (bundle2 == null) {
                    savedStateHandle = new SavedStateHandle();
                }
                else {
                    final HashMap hashMap = new HashMap();
                    for (final String s : ((BaseBundle)bundle2).keySet()) {
                        Intrinsics.checkNotNullExpressionValue((Object)s, "key");
                        hashMap.put(s, ((BaseBundle)bundle2).get(s));
                    }
                    savedStateHandle = new SavedStateHandle(hashMap);
                }
                return savedStateHandle;
            }
            final ArrayList parcelableArrayList = bundle.getParcelableArrayList("keys");
            final ArrayList parcelableArrayList2 = bundle.getParcelableArrayList("values");
            final int n = 0;
            if (parcelableArrayList != null && parcelableArrayList2 != null && parcelableArrayList.size() == parcelableArrayList2.size()) {
                final LinkedHashMap linkedHashMap = new LinkedHashMap();
                for (int size = parcelableArrayList.size(), i = n; i < size; ++i) {
                    final Object value = parcelableArrayList.get(i);
                    if (value == null) {
                        throw new NullPointerException("null cannot be cast to non-null type kotlin.String");
                    }
                    linkedHashMap.put(value, parcelableArrayList2.get(i));
                }
                return new SavedStateHandle(linkedHashMap);
            }
            throw new IllegalStateException("Invalid bundle passed as restored state".toString());
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        public final boolean validateValue(final Object o) {
            if (o == null) {
                return true;
            }
            for (final Class clazz : SavedStateHandle.access$getACCEPTABLE_CLASSES$cp()) {
                Intrinsics.Oo08((Object)clazz);
                if (clazz.isInstance(o)) {
                    return true;
                }
            }
            return false;
        }
    }
    
    @Metadata
    public static final class SavingStateLiveData<T> extends MutableLiveData<T>
    {
        private SavedStateHandle handle;
        @NotNull
        private String key;
        
        public SavingStateLiveData(final SavedStateHandle handle, @NotNull final String key) {
            Intrinsics.checkNotNullParameter((Object)key, "key");
            this.key = key;
            this.handle = handle;
        }
        
        public SavingStateLiveData(final SavedStateHandle handle, @NotNull final String key, final T t) {
            Intrinsics.checkNotNullParameter((Object)key, "key");
            super(t);
            this.key = key;
            this.handle = handle;
        }
        
        public final void detach() {
            this.handle = null;
        }
        
        @Override
        public void setValue(final T t) {
            final SavedStateHandle handle = this.handle;
            if (handle != null) {
                SavedStateHandle.access$getRegular$p(handle).put(this.key, t);
                final MutableStateFlow mutableStateFlow = SavedStateHandle.access$getFlows$p(handle).get(this.key);
                if (mutableStateFlow != null) {
                    mutableStateFlow.setValue((Object)t);
                }
            }
            super.setValue(t);
        }
    }
}

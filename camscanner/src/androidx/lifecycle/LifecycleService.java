// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.Nullable;
import androidx.annotation.CallSuper;
import android.os.IBinder;
import android.content.Intent;
import androidx.annotation.NonNull;
import android.app.Service;

public class LifecycleService extends Service implements LifecycleOwner
{
    private final ServiceLifecycleDispatcher mDispatcher;
    
    public LifecycleService() {
        this.mDispatcher = new ServiceLifecycleDispatcher(this);
    }
    
    @NonNull
    public Lifecycle getLifecycle() {
        return this.mDispatcher.getLifecycle();
    }
    
    @CallSuper
    @Nullable
    public IBinder onBind(@NonNull final Intent intent) {
        this.mDispatcher.onServicePreSuperOnBind();
        return null;
    }
    
    @CallSuper
    public void onCreate() {
        this.mDispatcher.onServicePreSuperOnCreate();
        super.onCreate();
    }
    
    @CallSuper
    public void onDestroy() {
        this.mDispatcher.onServicePreSuperOnDestroy();
        super.onDestroy();
    }
    
    @CallSuper
    public void onStart(@NonNull final Intent intent, final int n) {
        this.mDispatcher.onServicePreSuperOnStart();
        super.onStart(intent, n);
    }
    
    @CallSuper
    public int onStartCommand(@NonNull final Intent intent, final int n, final int n2) {
        return super.onStartCommand(intent, n, n2);
    }
}

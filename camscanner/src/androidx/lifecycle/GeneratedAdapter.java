// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public interface GeneratedAdapter
{
    void callMethods(final LifecycleOwner p0, final Lifecycle.Event p1, final boolean p2, final MethodCallsLogger p3);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Map;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public class Lifecycling
{
    private static final int GENERATED_CALLBACK = 2;
    private static final int REFLECTIVE_CALLBACK = 1;
    private static Map<Class<?>, Integer> sCallbackCache;
    private static Map<Class<?>, List<Constructor<? extends GeneratedAdapter>>> sClassToAdapters;
    
    static {
        Lifecycling.sCallbackCache = new HashMap<Class<?>, Integer>();
        Lifecycling.sClassToAdapters = new HashMap<Class<?>, List<Constructor<? extends GeneratedAdapter>>>();
    }
    
    private Lifecycling() {
    }
    
    private static GeneratedAdapter createGeneratedAdapter(final Constructor<? extends GeneratedAdapter> constructor, final Object o) {
        try {
            return (GeneratedAdapter)constructor.newInstance(o);
        }
        catch (final InvocationTargetException cause) {
            throw new RuntimeException(cause);
        }
        catch (final InstantiationException cause2) {
            throw new RuntimeException(cause2);
        }
        catch (final IllegalAccessException cause3) {
            throw new RuntimeException(cause3);
        }
    }
    
    @Nullable
    private static Constructor<? extends GeneratedAdapter> generatedConstructor(final Class<?> clazz) {
        try {
            final Package package1 = clazz.getPackage();
            String s = clazz.getCanonicalName();
            String name;
            if (package1 != null) {
                name = package1.getName();
            }
            else {
                name = "";
            }
            if (!name.isEmpty()) {
                s = s.substring(name.length() + 1);
            }
            final String adapterName = getAdapterName(s);
            String string;
            if (name.isEmpty()) {
                string = adapterName;
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append(name);
                sb.append(".");
                sb.append(adapterName);
                string = sb.toString();
            }
            final Constructor<?> declaredConstructor = Class.forName(string).getDeclaredConstructor(clazz);
            if (!declaredConstructor.isAccessible()) {
                declaredConstructor.setAccessible(true);
            }
            return (Constructor<? extends GeneratedAdapter>)declaredConstructor;
        }
        catch (final NoSuchMethodException cause) {
            throw new RuntimeException(cause);
        }
        catch (final ClassNotFoundException ex) {
            return null;
        }
    }
    
    public static String getAdapterName(final String s) {
        final StringBuilder sb = new StringBuilder();
        sb.append(s.replace(".", "_"));
        sb.append("_LifecycleAdapter");
        return sb.toString();
    }
    
    @Deprecated
    @NonNull
    static GenericLifecycleObserver getCallback(final Object o) {
        return new GenericLifecycleObserver(lifecycleEventObserver(o)) {
            final LifecycleEventObserver val$observer;
            
            @Override
            public void onStateChanged(@NonNull final LifecycleOwner lifecycleOwner, @NonNull final Lifecycle.Event event) {
                this.val$observer.onStateChanged(lifecycleOwner, event);
            }
        };
    }
    
    private static int getObserverConstructorType(final Class<?> clazz) {
        final Integer n = Lifecycling.sCallbackCache.get(clazz);
        if (n != null) {
            return n;
        }
        final int resolveObserverCallbackType = resolveObserverCallbackType(clazz);
        Lifecycling.sCallbackCache.put(clazz, resolveObserverCallbackType);
        return resolveObserverCallbackType;
    }
    
    private static boolean isLifecycleParent(final Class<?> clazz) {
        return clazz != null && LifecycleObserver.class.isAssignableFrom(clazz);
    }
    
    @NonNull
    static LifecycleEventObserver lifecycleEventObserver(final Object o) {
        final boolean b = o instanceof LifecycleEventObserver;
        final boolean b2 = o instanceof FullLifecycleObserver;
        if (b && b2) {
            return new FullLifecycleObserverAdapter((FullLifecycleObserver)o, (LifecycleEventObserver)o);
        }
        if (b2) {
            return new FullLifecycleObserverAdapter((FullLifecycleObserver)o, null);
        }
        if (b) {
            return (LifecycleEventObserver)o;
        }
        final Class<?> class1 = o.getClass();
        if (getObserverConstructorType(class1) != 2) {
            return new ReflectiveGenericLifecycleObserver(o);
        }
        final List list = Lifecycling.sClassToAdapters.get(class1);
        final int size = list.size();
        int i = 0;
        if (size == 1) {
            return new SingleGeneratedAdapterObserver(createGeneratedAdapter((Constructor<? extends GeneratedAdapter>)list.get(0), o));
        }
        final GeneratedAdapter[] array = new GeneratedAdapter[list.size()];
        while (i < list.size()) {
            array[i] = createGeneratedAdapter((Constructor<? extends GeneratedAdapter>)list.get(i), o);
            ++i;
        }
        return new CompositeGeneratedAdaptersObserver(array);
    }
    
    private static int resolveObserverCallbackType(final Class<?> clazz) {
        if (clazz.getCanonicalName() == null) {
            return 1;
        }
        final Constructor<? extends GeneratedAdapter> generatedConstructor = generatedConstructor(clazz);
        if (generatedConstructor != null) {
            Lifecycling.sClassToAdapters.put(clazz, Collections.singletonList(generatedConstructor));
            return 2;
        }
        if (ClassesInfoCache.sInstance.hasLifecycleMethods(clazz)) {
            return 1;
        }
        final Class superclass = clazz.getSuperclass();
        List list;
        if (isLifecycleParent(superclass)) {
            if (getObserverConstructorType(superclass) == 1) {
                return 1;
            }
            list = new ArrayList(Lifecycling.sClassToAdapters.get(superclass));
        }
        else {
            list = null;
        }
        for (final Class clazz2 : clazz.getInterfaces()) {
            if (isLifecycleParent(clazz2)) {
                if (getObserverConstructorType(clazz2) == 1) {
                    return 1;
                }
                List list2;
                if ((list2 = list) == null) {
                    list2 = new ArrayList();
                }
                list2.addAll(Lifecycling.sClassToAdapters.get(clazz2));
                list = list2;
            }
        }
        if (list != null) {
            Lifecycling.sClassToAdapters.put(clazz, list);
            return 2;
        }
        return 1;
    }
}

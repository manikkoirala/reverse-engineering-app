// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.RestrictTo;
import androidx.lifecycle.viewmodel.CreationExtras;
import android.annotation.SuppressLint;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.savedstate.SavedStateRegistryOwner;
import androidx.savedstate.SavedStateRegistry;
import android.os.Bundle;

public abstract class AbstractSavedStateViewModelFactory extends OnRequeryFactory implements Factory
{
    static final String TAG_SAVED_STATE_HANDLE_CONTROLLER = "androidx.lifecycle.savedstate.vm.tag";
    private Bundle mDefaultArgs;
    private Lifecycle mLifecycle;
    private SavedStateRegistry mSavedStateRegistry;
    
    public AbstractSavedStateViewModelFactory() {
    }
    
    @SuppressLint({ "LambdaLast" })
    public AbstractSavedStateViewModelFactory(@NonNull final SavedStateRegistryOwner savedStateRegistryOwner, @Nullable final Bundle mDefaultArgs) {
        this.mSavedStateRegistry = savedStateRegistryOwner.getSavedStateRegistry();
        this.mLifecycle = savedStateRegistryOwner.getLifecycle();
        this.mDefaultArgs = mDefaultArgs;
    }
    
    @NonNull
    private <T extends ViewModel> T create(@NonNull final String s, @NonNull final Class<T> clazz) {
        final SavedStateHandleController create = LegacySavedStateHandleController.create(this.mSavedStateRegistry, this.mLifecycle, s, this.mDefaultArgs);
        final ViewModel create2 = this.create(s, clazz, create.getHandle());
        create2.setTagIfAbsent("androidx.lifecycle.savedstate.vm.tag", create);
        return (T)create2;
    }
    
    @NonNull
    @Override
    public final <T extends ViewModel> T create(@NonNull final Class<T> clazz) {
        final String canonicalName = clazz.getCanonicalName();
        if (canonicalName == null) {
            throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
        }
        if (this.mLifecycle != null) {
            return this.create(canonicalName, clazz);
        }
        throw new UnsupportedOperationException("AbstractSavedStateViewModelFactory constructed with empty constructor supports only calls to create(modelClass: Class<T>, extras: CreationExtras).");
    }
    
    @NonNull
    @Override
    public final <T extends ViewModel> T create(@NonNull final Class<T> clazz, @NonNull final CreationExtras creationExtras) {
        final String s = creationExtras.get(NewInstanceFactory.VIEW_MODEL_KEY);
        if (s == null) {
            throw new IllegalStateException("VIEW_MODEL_KEY must always be provided by ViewModelProvider");
        }
        if (this.mSavedStateRegistry != null) {
            return this.create(s, clazz);
        }
        return this.create(s, clazz, SavedStateHandleSupport.createSavedStateHandle(creationExtras));
    }
    
    @NonNull
    protected abstract <T extends ViewModel> T create(@NonNull final String p0, @NonNull final Class<T> p1, @NonNull final SavedStateHandle p2);
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    public void onRequery(@NonNull final ViewModel viewModel) {
        final SavedStateRegistry mSavedStateRegistry = this.mSavedStateRegistry;
        if (mSavedStateRegistry != null) {
            LegacySavedStateHandleController.attachHandleIfNeeded(viewModel, mSavedStateRegistry, this.mLifecycle);
        }
    }
}

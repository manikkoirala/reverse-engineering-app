// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlinx.coroutines.DisposableHandle;
import org.jetbrains.annotations.NotNull;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.Metadata;

@Metadata
public interface LiveDataScope<T>
{
    Object emit(final T p0, @NotNull final Continuation<? super Unit> p1);
    
    Object emitSource(@NotNull final LiveData<T> p0, @NotNull final Continuation<? super DisposableHandle> p1);
    
    T getLatestValue();
}

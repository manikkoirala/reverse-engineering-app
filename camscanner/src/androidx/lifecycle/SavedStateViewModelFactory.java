// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.RestrictTo;
import java.lang.reflect.Constructor;
import java.util.List;
import androidx.lifecycle.viewmodel.CreationExtras;
import android.annotation.SuppressLint;
import kotlin.jvm.internal.Intrinsics;
import androidx.savedstate.SavedStateRegistryOwner;
import androidx.savedstate.SavedStateRegistry;
import org.jetbrains.annotations.NotNull;
import android.os.Bundle;
import android.app.Application;
import kotlin.Metadata;

@Metadata
public final class SavedStateViewModelFactory extends OnRequeryFactory implements Factory
{
    private Application application;
    private Bundle defaultArgs;
    @NotNull
    private final Factory factory;
    private Lifecycle lifecycle;
    private SavedStateRegistry savedStateRegistry;
    
    public SavedStateViewModelFactory() {
        this.factory = new ViewModelProvider.AndroidViewModelFactory();
    }
    
    public SavedStateViewModelFactory(final Application application, @NotNull final SavedStateRegistryOwner savedStateRegistryOwner) {
        Intrinsics.checkNotNullParameter((Object)savedStateRegistryOwner, "owner");
        this(application, savedStateRegistryOwner, null);
    }
    
    @SuppressLint({ "LambdaLast" })
    public SavedStateViewModelFactory(final Application application, @NotNull final SavedStateRegistryOwner savedStateRegistryOwner, final Bundle defaultArgs) {
        Intrinsics.checkNotNullParameter((Object)savedStateRegistryOwner, "owner");
        this.savedStateRegistry = savedStateRegistryOwner.getSavedStateRegistry();
        this.lifecycle = savedStateRegistryOwner.getLifecycle();
        this.defaultArgs = defaultArgs;
        this.application = application;
        NewInstanceFactory instance;
        if (application != null) {
            instance = AndroidViewModelFactory.Companion.getInstance(application);
        }
        else {
            instance = new ViewModelProvider.AndroidViewModelFactory();
        }
        this.factory = instance;
    }
    
    @NotNull
    @Override
    public <T extends ViewModel> T create(@NotNull final Class<T> clazz) {
        Intrinsics.checkNotNullParameter((Object)clazz, "modelClass");
        final String canonicalName = clazz.getCanonicalName();
        if (canonicalName != null) {
            return this.create(canonicalName, clazz);
        }
        throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }
    
    @NotNull
    @Override
    public <T extends ViewModel> T create(@NotNull final Class<T> clazz, @NotNull final CreationExtras creationExtras) {
        Intrinsics.checkNotNullParameter((Object)clazz, "modelClass");
        Intrinsics.checkNotNullParameter((Object)creationExtras, "extras");
        final String s = creationExtras.get(NewInstanceFactory.VIEW_MODEL_KEY);
        if (s != null) {
            ViewModel viewModel;
            if (creationExtras.get(SavedStateHandleSupport.SAVED_STATE_REGISTRY_OWNER_KEY) != null && creationExtras.get(SavedStateHandleSupport.VIEW_MODEL_STORE_OWNER_KEY) != null) {
                final Application application = creationExtras.get(AndroidViewModelFactory.APPLICATION_KEY);
                final boolean assignable = AndroidViewModel.class.isAssignableFrom(clazz);
                Constructor<T> constructor;
                if (assignable && application != null) {
                    constructor = SavedStateViewModelFactoryKt.findMatchingConstructor(clazz, SavedStateViewModelFactoryKt.access$getANDROID_VIEWMODEL_SIGNATURE$p());
                }
                else {
                    constructor = SavedStateViewModelFactoryKt.findMatchingConstructor(clazz, SavedStateViewModelFactoryKt.access$getVIEWMODEL_SIGNATURE$p());
                }
                if (constructor == null) {
                    return this.factory.create(clazz, creationExtras);
                }
                if (assignable && application != null) {
                    viewModel = SavedStateViewModelFactoryKt.newInstance(clazz, constructor, application, SavedStateHandleSupport.createSavedStateHandle(creationExtras));
                }
                else {
                    viewModel = SavedStateViewModelFactoryKt.newInstance(clazz, constructor, SavedStateHandleSupport.createSavedStateHandle(creationExtras));
                }
            }
            else {
                if (this.lifecycle == null) {
                    throw new IllegalStateException("SAVED_STATE_REGISTRY_OWNER_KEY andVIEW_MODEL_STORE_OWNER_KEY must be provided in the creation extras tosuccessfully create a ViewModel.");
                }
                viewModel = this.create(s, clazz);
            }
            return (T)viewModel;
        }
        throw new IllegalStateException("VIEW_MODEL_KEY must always be provided by ViewModelProvider");
    }
    
    @NotNull
    public final <T extends ViewModel> T create(@NotNull final String s, @NotNull final Class<T> clazz) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        Intrinsics.checkNotNullParameter((Object)clazz, "modelClass");
        if (this.lifecycle == null) {
            throw new UnsupportedOperationException("SavedStateViewModelFactory constructed with empty constructor supports only calls to create(modelClass: Class<T>, extras: CreationExtras).");
        }
        final boolean assignable = AndroidViewModel.class.isAssignableFrom(clazz);
        Constructor<T> constructor;
        if (assignable && this.application != null) {
            constructor = SavedStateViewModelFactoryKt.findMatchingConstructor(clazz, SavedStateViewModelFactoryKt.access$getANDROID_VIEWMODEL_SIGNATURE$p());
        }
        else {
            constructor = SavedStateViewModelFactoryKt.findMatchingConstructor(clazz, SavedStateViewModelFactoryKt.access$getVIEWMODEL_SIGNATURE$p());
        }
        if (constructor == null) {
            ViewModel viewModel;
            if (this.application != null) {
                viewModel = this.factory.create(clazz);
            }
            else {
                viewModel = NewInstanceFactory.Companion.getInstance().create(clazz);
            }
            return (T)viewModel;
        }
        final SavedStateHandleController create = LegacySavedStateHandleController.create(this.savedStateRegistry, this.lifecycle, s, this.defaultArgs);
        ViewModel viewModel2 = null;
        Label_0196: {
            if (assignable) {
                final Application application = this.application;
                if (application != null) {
                    Intrinsics.Oo08((Object)application);
                    final SavedStateHandle handle = create.getHandle();
                    Intrinsics.checkNotNullExpressionValue((Object)handle, "controller.handle");
                    viewModel2 = SavedStateViewModelFactoryKt.newInstance(clazz, constructor, application, handle);
                    break Label_0196;
                }
            }
            final SavedStateHandle handle2 = create.getHandle();
            Intrinsics.checkNotNullExpressionValue((Object)handle2, "controller.handle");
            viewModel2 = SavedStateViewModelFactoryKt.newInstance(clazz, constructor, handle2);
        }
        viewModel2.setTagIfAbsent("androidx.lifecycle.savedstate.vm.tag", create);
        return (T)viewModel2;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    @Override
    public void onRequery(@NotNull final ViewModel viewModel) {
        Intrinsics.checkNotNullParameter((Object)viewModel, "viewModel");
        final Lifecycle lifecycle = this.lifecycle;
        if (lifecycle != null) {
            LegacySavedStateHandleController.attachHandleIfNeeded(viewModel, this.savedStateRegistry, lifecycle);
        }
    }
}

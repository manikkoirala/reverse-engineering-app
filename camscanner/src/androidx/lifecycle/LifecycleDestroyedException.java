// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.Metadata;
import java.util.concurrent.CancellationException;

@Metadata
public final class LifecycleDestroyedException extends CancellationException
{
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.BuildersKt;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineScopeKt;
import kotlinx.coroutines.Dispatchers;
import androidx.annotation.MainThread;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;
import kotlinx.coroutines.DisposableHandle;

@Metadata
public final class EmittedSource implements DisposableHandle
{
    private boolean disposed;
    @NotNull
    private final MediatorLiveData<?> mediator;
    @NotNull
    private final LiveData<?> source;
    
    public EmittedSource(@NotNull final LiveData<?> source, @NotNull final MediatorLiveData<?> mediator) {
        Intrinsics.checkNotNullParameter((Object)source, "source");
        Intrinsics.checkNotNullParameter((Object)mediator, "mediator");
        this.source = source;
        this.mediator = mediator;
    }
    
    @MainThread
    private final void removeSource() {
        if (!this.disposed) {
            this.mediator.removeSource(this.source);
            this.disposed = true;
        }
    }
    
    public void dispose() {
        BuildersKt.O8(CoroutineScopeKt.\u3007080((CoroutineContext)Dispatchers.\u3007o\u3007().\u30078()), (CoroutineContext)null, (CoroutineStart)null, (Function2)new EmittedSource$dispose.EmittedSource$dispose$1(this, (Continuation)null), 3, (Object)null);
    }
    
    public final Object disposeNow(@NotNull final Continuation<? super Unit> continuation) {
        final Object oo08 = BuildersKt.Oo08((CoroutineContext)Dispatchers.\u3007o\u3007().\u30078(), (Function2)new EmittedSource$disposeNow.EmittedSource$disposeNow$2(this, (Continuation)null), (Continuation)continuation);
        if (oo08 == IntrinsicsKt.O8()) {
            return oo08;
        }
        return Unit.\u3007080;
    }
}

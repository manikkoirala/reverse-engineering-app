// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.Nullable;
import androidx.annotation.CallSuper;
import java.util.Iterator;
import java.util.Map;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.arch.core.internal.SafeIterableMap;

public class MediatorLiveData<T> extends MutableLiveData<T>
{
    private SafeIterableMap<LiveData<?>, Source<?>> mSources;
    
    public MediatorLiveData() {
        this.mSources = new SafeIterableMap<LiveData<?>, Source<?>>();
    }
    
    @MainThread
    public <S> void addSource(@NonNull final LiveData<S> liveData, @NonNull final Observer<? super S> observer) {
        final Source source = new Source((LiveData<V>)liveData, (Observer<? super V>)observer);
        final Source source2 = this.mSources.putIfAbsent(liveData, source);
        if (source2 != null && source2.mObserver != observer) {
            throw new IllegalArgumentException("This source was already added with the different observer");
        }
        if (source2 != null) {
            return;
        }
        if (this.hasActiveObservers()) {
            source.plug();
        }
    }
    
    @CallSuper
    @Override
    protected void onActive() {
        final Iterator<Map.Entry<LiveData<?>, Source<?>>> iterator = this.mSources.iterator();
        while (iterator.hasNext()) {
            ((Map.Entry<K, Source>)iterator.next()).getValue().plug();
        }
    }
    
    @CallSuper
    @Override
    protected void onInactive() {
        final Iterator<Map.Entry<LiveData<?>, Source<?>>> iterator = this.mSources.iterator();
        while (iterator.hasNext()) {
            ((Map.Entry<K, Source>)iterator.next()).getValue().unplug();
        }
    }
    
    @MainThread
    public <S> void removeSource(@NonNull final LiveData<S> liveData) {
        final Source source = this.mSources.remove(liveData);
        if (source != null) {
            source.unplug();
        }
    }
    
    private static class Source<V> implements Observer<V>
    {
        final LiveData<V> mLiveData;
        final Observer<? super V> mObserver;
        int mVersion;
        
        Source(final LiveData<V> mLiveData, final Observer<? super V> mObserver) {
            this.mVersion = -1;
            this.mLiveData = mLiveData;
            this.mObserver = mObserver;
        }
        
        @Override
        public void onChanged(@Nullable final V v) {
            if (this.mVersion != this.mLiveData.getVersion()) {
                this.mVersion = this.mLiveData.getVersion();
                this.mObserver.onChanged(v);
            }
        }
        
        void plug() {
            this.mLiveData.observeForever(this);
        }
        
        void unplug() {
            this.mLiveData.removeObserver(this);
        }
    }
}

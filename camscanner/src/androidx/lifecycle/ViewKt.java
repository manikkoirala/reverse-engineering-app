// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.view.View;
import kotlin.Metadata;

@Metadata
public final class ViewKt
{
    public static final LifecycleOwner findViewTreeLifecycleOwner(@NotNull final View view) {
        Intrinsics.checkNotNullParameter((Object)view, "<this>");
        return ViewTreeLifecycleOwner.get(view);
    }
}

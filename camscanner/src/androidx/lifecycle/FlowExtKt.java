// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.flow.FlowKt;
import kotlin.coroutines.Continuation;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlinx.coroutines.flow.Flow;
import kotlin.Metadata;

@Metadata
public final class FlowExtKt
{
    @NotNull
    public static final <T> Flow<T> flowWithLifecycle(@NotNull final Flow<? extends T> flow, @NotNull final Lifecycle lifecycle, @NotNull final Lifecycle.State state) {
        Intrinsics.checkNotNullParameter((Object)flow, "<this>");
        Intrinsics.checkNotNullParameter((Object)lifecycle, "lifecycle");
        Intrinsics.checkNotNullParameter((Object)state, "minActiveState");
        return (Flow<T>)FlowKt.Oo08((Function2)new FlowExtKt$flowWithLifecycle.FlowExtKt$flowWithLifecycle$1(lifecycle, state, (Flow)flow, (Continuation)null));
    }
}

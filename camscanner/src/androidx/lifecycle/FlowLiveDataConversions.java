// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.coroutines.EmptyCoroutineContext;
import androidx.annotation.RequiresApi;
import java.time.Duration;
import kotlin.Unit;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.flow.FlowKt;
import kotlin.coroutines.Continuation;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.flow.Flow;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class FlowLiveDataConversions
{
    @NotNull
    public static final <T> Flow<T> asFlow(@NotNull final LiveData<T> liveData) {
        Intrinsics.checkNotNullParameter((Object)liveData, "<this>");
        return (Flow<T>)FlowKt.\u3007O00((Function2)new FlowLiveDataConversions$asFlow.FlowLiveDataConversions$asFlow$1((LiveData)liveData, (Continuation)null));
    }
    
    @NotNull
    public static final <T> LiveData<T> asLiveData(@NotNull final Flow<? extends T> flow) {
        Intrinsics.checkNotNullParameter((Object)flow, "<this>");
        return asLiveData$default(flow, null, 0L, 3, null);
    }
    
    @NotNull
    public static final <T> LiveData<T> asLiveData(@NotNull final Flow<? extends T> flow, @NotNull final CoroutineContext coroutineContext) {
        Intrinsics.checkNotNullParameter((Object)flow, "<this>");
        Intrinsics.checkNotNullParameter((Object)coroutineContext, "context");
        return asLiveData$default(flow, coroutineContext, 0L, 2, null);
    }
    
    @NotNull
    public static final <T> LiveData<T> asLiveData(@NotNull final Flow<? extends T> flow, @NotNull final CoroutineContext coroutineContext, final long n) {
        Intrinsics.checkNotNullParameter((Object)flow, "<this>");
        Intrinsics.checkNotNullParameter((Object)coroutineContext, "context");
        return CoroutineLiveDataKt.liveData(coroutineContext, n, (kotlin.jvm.functions.Function2<? super LiveDataScope<T>, ? super Continuation<? super Unit>, ?>)new FlowLiveDataConversions$asLiveData.FlowLiveDataConversions$asLiveData$1((Flow)flow, (Continuation)null));
    }
    
    @RequiresApi(26)
    @NotNull
    public static final <T> LiveData<T> asLiveData(@NotNull final Flow<? extends T> flow, @NotNull final CoroutineContext coroutineContext, @NotNull final Duration duration) {
        Intrinsics.checkNotNullParameter((Object)flow, "<this>");
        Intrinsics.checkNotNullParameter((Object)coroutineContext, "context");
        Intrinsics.checkNotNullParameter((Object)duration, "timeout");
        return asLiveData(flow, coroutineContext, \u3007080.\u3007080(duration));
    }
    
    public static /* synthetic */ LiveData asLiveData$default(final Flow flow, CoroutineContext instance, long n, final int n2, final Object o) {
        if ((n2 & 0x1) != 0x0) {
            instance = (CoroutineContext)EmptyCoroutineContext.INSTANCE;
        }
        if ((n2 & 0x2) != 0x0) {
            n = 5000L;
        }
        return asLiveData((kotlinx.coroutines.flow.Flow<?>)flow, instance, n);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.Nullable;
import androidx.arch.core.util.Function;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;

public class Transformations
{
    private Transformations() {
    }
    
    @MainThread
    @NonNull
    public static <X> LiveData<X> distinctUntilChanged(@NonNull final LiveData<X> liveData) {
        final MediatorLiveData mediatorLiveData = new MediatorLiveData();
        mediatorLiveData.addSource(liveData, new Observer<X>(mediatorLiveData) {
            boolean mFirstTime = true;
            final MediatorLiveData val$outputLiveData;
            
            @Override
            public void onChanged(final X x) {
                final Object value = this.val$outputLiveData.getValue();
                if (this.mFirstTime || (value == null && x != null) || (value != null && !value.equals(x))) {
                    this.mFirstTime = false;
                    this.val$outputLiveData.setValue(x);
                }
            }
        });
        return mediatorLiveData;
    }
    
    @MainThread
    @NonNull
    public static <X, Y> LiveData<Y> map(@NonNull final LiveData<X> liveData, @NonNull final Function<X, Y> function) {
        final MediatorLiveData mediatorLiveData = new MediatorLiveData();
        mediatorLiveData.addSource(liveData, new Observer<X>(mediatorLiveData, function) {
            final Function val$mapFunction;
            final MediatorLiveData val$result;
            
            @Override
            public void onChanged(@Nullable final X x) {
                this.val$result.setValue(this.val$mapFunction.apply(x));
            }
        });
        return mediatorLiveData;
    }
    
    @MainThread
    @NonNull
    public static <X, Y> LiveData<Y> switchMap(@NonNull final LiveData<X> liveData, @NonNull final Function<X, LiveData<Y>> function) {
        final MediatorLiveData mediatorLiveData = new MediatorLiveData();
        mediatorLiveData.addSource(liveData, new Observer<X>(function, mediatorLiveData) {
            LiveData<Y> mSource;
            final MediatorLiveData val$result;
            final Function val$switchMapFunction;
            
            @Override
            public void onChanged(@Nullable final X x) {
                final LiveData mSource = this.val$switchMapFunction.apply(x);
                final LiveData<Y> mSource2 = this.mSource;
                if (mSource2 == mSource) {
                    return;
                }
                if (mSource2 != null) {
                    this.val$result.removeSource(mSource2);
                }
                if ((this.mSource = mSource) != null) {
                    this.val$result.addSource(mSource, new Observer<Y>(this) {
                        final Transformations$2 this$0;
                        
                        @Override
                        public void onChanged(@Nullable final Y value) {
                            this.this$0.val$result.setValue(value);
                        }
                    });
                }
            }
        });
        return mediatorLiveData;
    }
}

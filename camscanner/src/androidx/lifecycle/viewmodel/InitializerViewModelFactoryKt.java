// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle.viewmodel;

import androidx.lifecycle.ViewModelProvider;
import org.jetbrains.annotations.NotNull;
import kotlin.Unit;
import kotlin.reflect.KClass;
import kotlin.jvm.internal.Reflection;
import kotlin.jvm.internal.Intrinsics;
import androidx.lifecycle.ViewModel;
import kotlin.jvm.functions.Function1;
import kotlin.Metadata;

@Metadata
public final class InitializerViewModelFactoryKt
{
    @NotNull
    public static final ViewModelProvider.Factory viewModelFactory(@NotNull final Function1<? super InitializerViewModelFactoryBuilder, Unit> function1) {
        Intrinsics.checkNotNullParameter((Object)function1, "builder");
        final InitializerViewModelFactoryBuilder initializerViewModelFactoryBuilder = new InitializerViewModelFactoryBuilder();
        function1.invoke((Object)initializerViewModelFactoryBuilder);
        return initializerViewModelFactoryBuilder.build();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle.viewmodel;

import kotlin.Metadata;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.RUNTIME)
@Metadata
public @interface ViewModelFactoryDsl {
}

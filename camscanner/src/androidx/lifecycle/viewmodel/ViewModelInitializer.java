// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle.viewmodel;

import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.functions.Function1;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;
import androidx.lifecycle.ViewModel;

@Metadata
public final class ViewModelInitializer<T extends ViewModel>
{
    @NotNull
    private final Class<T> clazz;
    @NotNull
    private final Function1<CreationExtras, T> initializer;
    
    public ViewModelInitializer(@NotNull final Class<T> clazz, @NotNull final Function1<? super CreationExtras, ? extends T> initializer) {
        Intrinsics.checkNotNullParameter((Object)clazz, "clazz");
        Intrinsics.checkNotNullParameter((Object)initializer, "initializer");
        this.clazz = clazz;
        this.initializer = (Function1<CreationExtras, T>)initializer;
    }
    
    @NotNull
    public final Class<T> getClazz$lifecycle_viewmodel_release() {
        return this.clazz;
    }
    
    @NotNull
    public final Function1<CreationExtras, T> getInitializer$lifecycle_viewmodel_release() {
        return this.initializer;
    }
}

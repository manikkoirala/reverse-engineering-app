// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle.viewmodel;

import kotlin.jvm.internal.Intrinsics;
import java.util.LinkedHashMap;
import org.jetbrains.annotations.NotNull;
import java.util.Map;
import kotlin.Metadata;

@Metadata
public abstract class CreationExtras
{
    @NotNull
    private final Map<Key<?>, Object> map;
    
    public CreationExtras() {
        this.map = new LinkedHashMap<Key<?>, Object>();
    }
    
    public abstract <T> T get(@NotNull final Key<T> p0);
    
    @NotNull
    public final Map<Key<?>, Object> getMap$lifecycle_viewmodel_release() {
        return this.map;
    }
    
    @Metadata
    public static final class Empty extends CreationExtras
    {
        @NotNull
        public static final Empty INSTANCE;
        
        static {
            INSTANCE = new Empty();
        }
        
        private Empty() {
        }
        
        @Override
        public <T> T get(@NotNull final Key<T> key) {
            Intrinsics.checkNotNullParameter((Object)key, "key");
            return null;
        }
    }
    
    @Metadata
    public interface Key<T>
    {
    }
}

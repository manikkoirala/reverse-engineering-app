// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle.viewmodel;

import java.util.Arrays;
import java.util.Collection;
import androidx.lifecycle.ViewModelProvider;
import kotlin.jvm.JvmClassMappingKt;
import kotlin.jvm.internal.Intrinsics;
import androidx.lifecycle.ViewModel;
import kotlin.jvm.functions.Function1;
import kotlin.reflect.KClass;
import java.util.ArrayList;
import org.jetbrains.annotations.NotNull;
import java.util.List;
import kotlin.Metadata;

@ViewModelFactoryDsl
@Metadata
public final class InitializerViewModelFactoryBuilder
{
    @NotNull
    private final List<ViewModelInitializer<?>> initializers;
    
    public InitializerViewModelFactoryBuilder() {
        this.initializers = new ArrayList<ViewModelInitializer<?>>();
    }
    
    public final <T extends ViewModel> void addInitializer(@NotNull final KClass<T> kClass, @NotNull final Function1<? super CreationExtras, ? extends T> function1) {
        Intrinsics.checkNotNullParameter((Object)kClass, "clazz");
        Intrinsics.checkNotNullParameter((Object)function1, "initializer");
        this.initializers.add(new ViewModelInitializer<Object>(JvmClassMappingKt.\u3007080((KClass)kClass), function1));
    }
    
    @NotNull
    public final ViewModelProvider.Factory build() {
        final ViewModelInitializer[] array = this.initializers.toArray(new ViewModelInitializer[0]);
        if (array != null) {
            final ViewModelInitializer[] original = array;
            return new InitializerViewModelFactory((ViewModelInitializer<?>[])Arrays.copyOf(original, original.length));
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
    }
}

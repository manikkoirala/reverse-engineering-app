// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle.viewmodel;

import java.util.Map;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;

@Metadata
public final class MutableCreationExtras extends CreationExtras
{
    public MutableCreationExtras() {
        this(null, 1, null);
    }
    
    public MutableCreationExtras(@NotNull final CreationExtras creationExtras) {
        Intrinsics.checkNotNullParameter((Object)creationExtras, "initialExtras");
        this.getMap$lifecycle_viewmodel_release().putAll(creationExtras.getMap$lifecycle_viewmodel_release());
    }
    
    @Override
    public <T> T get(@NotNull final Key<T> key) {
        Intrinsics.checkNotNullParameter((Object)key, "key");
        return (T)this.getMap$lifecycle_viewmodel_release().get(key);
    }
    
    public final <T> void set(@NotNull final Key<T> key, final T t) {
        Intrinsics.checkNotNullParameter((Object)key, "key");
        this.getMap$lifecycle_viewmodel_release().put((Key<?>)key, t);
    }
}

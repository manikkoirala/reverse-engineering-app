// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle.viewmodel;

import androidx.lifecycle.\u300780\u3007808\u3007O;
import androidx.lifecycle.ViewModel;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;
import androidx.lifecycle.ViewModelProvider;

@Metadata
public final class InitializerViewModelFactory implements Factory
{
    @NotNull
    private final ViewModelInitializer<?>[] initializers;
    
    public InitializerViewModelFactory(@NotNull final ViewModelInitializer<?>... initializers) {
        Intrinsics.checkNotNullParameter((Object)initializers, "initializers");
        this.initializers = initializers;
    }
    
    @NotNull
    @Override
    public <T extends ViewModel> T create(@NotNull final Class<T> clazz, @NotNull final CreationExtras creationExtras) {
        Intrinsics.checkNotNullParameter((Object)clazz, "modelClass");
        Intrinsics.checkNotNullParameter((Object)creationExtras, "extras");
        final ViewModelInitializer<?>[] initializers = this.initializers;
        final int length = initializers.length;
        int i = 0;
        ViewModel viewModel = null;
        while (i < length) {
            final ViewModelInitializer<?> viewModelInitializer = initializers[i];
            if (Intrinsics.\u3007o\u3007((Object)viewModelInitializer.getClazz$lifecycle_viewmodel_release(), (Object)clazz)) {
                final Object invoke = viewModelInitializer.getInitializer$lifecycle_viewmodel_release().invoke((Object)creationExtras);
                if (invoke instanceof ViewModel) {
                    viewModel = (T)invoke;
                }
                else {
                    viewModel = null;
                }
            }
            ++i;
        }
        if (viewModel != null) {
            return (T)viewModel;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("No initializer set for given class ");
        sb.append(clazz.getName());
        throw new IllegalArgumentException(sb.toString());
    }
}

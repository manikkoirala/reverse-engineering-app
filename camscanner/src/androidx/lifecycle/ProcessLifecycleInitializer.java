// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.util.Collections;
import java.util.List;
import androidx.startup.AppInitializer;
import androidx.annotation.NonNull;
import android.content.Context;
import androidx.startup.Initializer;

public final class ProcessLifecycleInitializer implements Initializer<LifecycleOwner>
{
    @NonNull
    @Override
    public LifecycleOwner create(@NonNull final Context context) {
        if (AppInitializer.getInstance(context).isEagerlyInitialized(ProcessLifecycleInitializer.class)) {
            LifecycleDispatcher.init(context);
            ProcessLifecycleOwner.init(context);
            return ProcessLifecycleOwner.get();
        }
        throw new IllegalStateException("ProcessLifecycleInitializer cannot be initialized lazily. \nPlease ensure that you have: \n<meta-data\n    android:name='androidx.lifecycle.ProcessLifecycleInitializer' \n    android:value='androidx.startup' /> \nunder InitializationProvider in your AndroidManifest.xml");
    }
    
    @NonNull
    @Override
    public List<Class<? extends Initializer<?>>> dependencies() {
        return Collections.emptyList();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlinx.coroutines.Dispatchers;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.CoroutineContext;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;
import kotlinx.coroutines.CoroutineDispatcher;

@Metadata
public final class PausingDispatcher extends CoroutineDispatcher
{
    @NotNull
    public final DispatchQueue dispatchQueue;
    
    public PausingDispatcher() {
        this.dispatchQueue = new DispatchQueue();
    }
    
    public void dispatch(@NotNull final CoroutineContext coroutineContext, @NotNull final Runnable runnable) {
        Intrinsics.checkNotNullParameter((Object)coroutineContext, "context");
        Intrinsics.checkNotNullParameter((Object)runnable, "block");
        this.dispatchQueue.dispatchAndEnqueue(coroutineContext, runnable);
    }
    
    public boolean isDispatchNeeded(@NotNull final CoroutineContext coroutineContext) {
        Intrinsics.checkNotNullParameter((Object)coroutineContext, "context");
        return ((CoroutineDispatcher)Dispatchers.\u3007o\u3007().\u30078()).isDispatchNeeded(coroutineContext) || (this.dispatchQueue.canRun() ^ true);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.NonNull;
import androidx.savedstate.SavedStateRegistry;

final class SavedStateHandleController implements LifecycleEventObserver
{
    private final SavedStateHandle mHandle;
    private boolean mIsAttached;
    private final String mKey;
    
    SavedStateHandleController(final String mKey, final SavedStateHandle mHandle) {
        this.mIsAttached = false;
        this.mKey = mKey;
        this.mHandle = mHandle;
    }
    
    void attachToLifecycle(final SavedStateRegistry savedStateRegistry, final Lifecycle lifecycle) {
        if (!this.mIsAttached) {
            this.mIsAttached = true;
            lifecycle.addObserver(this);
            savedStateRegistry.registerSavedStateProvider(this.mKey, this.mHandle.savedStateProvider());
            return;
        }
        throw new IllegalStateException("Already attached to lifecycleOwner");
    }
    
    SavedStateHandle getHandle() {
        return this.mHandle;
    }
    
    boolean isAttached() {
        return this.mIsAttached;
    }
    
    @Override
    public void onStateChanged(@NonNull final LifecycleOwner lifecycleOwner, @NonNull final Lifecycle.Event event) {
        if (event == Lifecycle.Event.ON_DESTROY) {
            this.mIsAttached = false;
            lifecycleOwner.getLifecycle().removeObserver(this);
        }
    }
}

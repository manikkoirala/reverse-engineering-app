// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.MainThread;
import androidx.annotation.WorkerThread;
import androidx.annotation.NonNull;
import androidx.arch.core.executor.ArchTaskExecutor;
import androidx.annotation.VisibleForTesting;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public abstract class ComputableLiveData<T>
{
    final AtomicBoolean mComputing;
    final Executor mExecutor;
    final AtomicBoolean mInvalid;
    @VisibleForTesting
    final Runnable mInvalidationRunnable;
    final LiveData<T> mLiveData;
    @VisibleForTesting
    final Runnable mRefreshRunnable;
    
    public ComputableLiveData() {
        this(ArchTaskExecutor.getIOThreadExecutor());
    }
    
    public ComputableLiveData(@NonNull final Executor mExecutor) {
        this.mInvalid = new AtomicBoolean(true);
        this.mComputing = new AtomicBoolean(false);
        this.mRefreshRunnable = new Runnable() {
            final ComputableLiveData this$0;
            
            @WorkerThread
            @Override
            public void run() {
                boolean b;
                do {
                    final AtomicBoolean mComputing = this.this$0.mComputing;
                    b = false;
                    if (mComputing.compareAndSet(false, true)) {
                        T compute = null;
                        b = false;
                        try {
                            while (this.this$0.mInvalid.compareAndSet(true, false)) {
                                compute = this.this$0.compute();
                                b = true;
                            }
                            if (!b) {
                                continue;
                            }
                            this.this$0.mLiveData.postValue(compute);
                        }
                        finally {
                            this.this$0.mComputing.set(false);
                        }
                    }
                } while (b && this.this$0.mInvalid.get());
            }
        };
        this.mInvalidationRunnable = new Runnable() {
            final ComputableLiveData this$0;
            
            @MainThread
            @Override
            public void run() {
                final boolean hasActiveObservers = this.this$0.mLiveData.hasActiveObservers();
                if (this.this$0.mInvalid.compareAndSet(false, true) && hasActiveObservers) {
                    final ComputableLiveData this$0 = this.this$0;
                    this$0.mExecutor.execute(this$0.mRefreshRunnable);
                }
            }
        };
        this.mExecutor = mExecutor;
        this.mLiveData = new LiveData<T>(this) {
            final ComputableLiveData this$0;
            
            @Override
            protected void onActive() {
                final ComputableLiveData this$0 = this.this$0;
                this$0.mExecutor.execute(this$0.mRefreshRunnable);
            }
        };
    }
    
    @WorkerThread
    protected abstract T compute();
    
    @NonNull
    public LiveData<T> getLiveData() {
        return this.mLiveData;
    }
    
    public void invalidate() {
        ArchTaskExecutor.getInstance().executeOnMainThread(this.mInvalidationRunnable);
    }
}

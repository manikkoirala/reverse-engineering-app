// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.jvm.JvmClassMappingKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.reflect.KClass;
import org.jetbrains.annotations.NotNull;
import androidx.lifecycle.viewmodel.CreationExtras;
import kotlin.jvm.functions.Function0;
import kotlin.Metadata;
import kotlin.Lazy;

@Metadata
public final class ViewModelLazy<VM extends ViewModel> implements Lazy<VM>
{
    private VM cached;
    @NotNull
    private final Function0<CreationExtras> extrasProducer;
    @NotNull
    private final Function0<ViewModelProvider.Factory> factoryProducer;
    @NotNull
    private final Function0<ViewModelStore> storeProducer;
    @NotNull
    private final KClass<VM> viewModelClass;
    
    public ViewModelLazy(@NotNull final KClass<VM> kClass, @NotNull final Function0<? extends ViewModelStore> function0, @NotNull final Function0<? extends ViewModelProvider.Factory> function2) {
        Intrinsics.checkNotNullParameter((Object)kClass, "viewModelClass");
        Intrinsics.checkNotNullParameter((Object)function0, "storeProducer");
        Intrinsics.checkNotNullParameter((Object)function2, "factoryProducer");
        this(kClass, function0, function2, null, 8, null);
    }
    
    public ViewModelLazy(@NotNull final KClass<VM> viewModelClass, @NotNull final Function0<? extends ViewModelStore> storeProducer, @NotNull final Function0<? extends ViewModelProvider.Factory> factoryProducer, @NotNull final Function0<? extends CreationExtras> extrasProducer) {
        Intrinsics.checkNotNullParameter((Object)viewModelClass, "viewModelClass");
        Intrinsics.checkNotNullParameter((Object)storeProducer, "storeProducer");
        Intrinsics.checkNotNullParameter((Object)factoryProducer, "factoryProducer");
        Intrinsics.checkNotNullParameter((Object)extrasProducer, "extrasProducer");
        this.viewModelClass = viewModelClass;
        this.storeProducer = (Function0<ViewModelStore>)storeProducer;
        this.factoryProducer = (Function0<ViewModelProvider.Factory>)factoryProducer;
        this.extrasProducer = (Function0<CreationExtras>)extrasProducer;
    }
    
    @NotNull
    public VM getValue() {
        ViewModel cached;
        if ((cached = this.cached) == null) {
            cached = new ViewModelProvider((ViewModelStore)this.storeProducer.invoke(), (ViewModelProvider.Factory)this.factoryProducer.invoke(), (CreationExtras)this.extrasProducer.invoke()).get((Class<VM>)JvmClassMappingKt.\u3007080((KClass)this.viewModelClass));
            this.cached = (VM)cached;
        }
        return (VM)cached;
    }
    
    public boolean isInitialized() {
        return this.cached != null;
    }
}

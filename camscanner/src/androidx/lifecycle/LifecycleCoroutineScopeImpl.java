// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.BuildersKt;
import kotlin.coroutines.Continuation;
import kotlinx.coroutines.Dispatchers;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.JobKt;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.CoroutineContext;
import kotlin.Metadata;

@Metadata
public final class LifecycleCoroutineScopeImpl extends LifecycleCoroutineScope implements LifecycleEventObserver
{
    @NotNull
    private final CoroutineContext coroutineContext;
    @NotNull
    private final Lifecycle lifecycle;
    
    public LifecycleCoroutineScopeImpl(@NotNull final Lifecycle lifecycle, @NotNull final CoroutineContext coroutineContext) {
        Intrinsics.checkNotNullParameter((Object)lifecycle, "lifecycle");
        Intrinsics.checkNotNullParameter((Object)coroutineContext, "coroutineContext");
        this.lifecycle = lifecycle;
        this.coroutineContext = coroutineContext;
        if (this.getLifecycle$lifecycle_runtime_ktx_release().getCurrentState() == Lifecycle.State.DESTROYED) {
            JobKt.O8(this.getCoroutineContext(), (CancellationException)null, 1, (Object)null);
        }
    }
    
    @NotNull
    @Override
    public CoroutineContext getCoroutineContext() {
        return this.coroutineContext;
    }
    
    @NotNull
    @Override
    public Lifecycle getLifecycle$lifecycle_runtime_ktx_release() {
        return this.lifecycle;
    }
    
    @Override
    public void onStateChanged(@NotNull final LifecycleOwner lifecycleOwner, @NotNull final Lifecycle.Event event) {
        Intrinsics.checkNotNullParameter((Object)lifecycleOwner, "source");
        Intrinsics.checkNotNullParameter((Object)event, "event");
        if (this.getLifecycle$lifecycle_runtime_ktx_release().getCurrentState().compareTo(Lifecycle.State.DESTROYED) <= 0) {
            this.getLifecycle$lifecycle_runtime_ktx_release().removeObserver(this);
            JobKt.O8(this.getCoroutineContext(), (CancellationException)null, 1, (Object)null);
        }
    }
    
    public final void register() {
        BuildersKt.O8((CoroutineScope)this, (CoroutineContext)Dispatchers.\u3007o\u3007().\u30078(), (CoroutineStart)null, (Function2)new LifecycleCoroutineScopeImpl$register.LifecycleCoroutineScopeImpl$register$1(this, (Continuation)null), 2, (Object)null);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.jvm.internal.InlineMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.MainCoroutineDispatcher;
import kotlinx.coroutines.Dispatchers;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlin.jvm.functions.Function1;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlinx.coroutines.CancellableContinuation;
import kotlinx.coroutines.CancellableContinuationImpl;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function0;
import kotlinx.coroutines.CoroutineDispatcher;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class WithLifecycleStateKt
{
    public static final <R> Object suspendWithStateAtLeastUnchecked(@NotNull final Lifecycle lifecycle, @NotNull final Lifecycle.State state, final boolean b, @NotNull final CoroutineDispatcher coroutineDispatcher, @NotNull final Function0<? extends R> function0, @NotNull final Continuation<? super R> continuation) {
        final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt.\u3007o\u3007((Continuation)continuation), 1);
        cancellableContinuationImpl.O8ooOoo\u3007();
        final WithLifecycleStateKt$suspendWithStateAtLeastUnchecked$2$observer.WithLifecycleStateKt$suspendWithStateAtLeastUnchecked$2$observer$1 withLifecycleStateKt$suspendWithStateAtLeastUnchecked$2$observer$1 = new WithLifecycleStateKt$suspendWithStateAtLeastUnchecked$2$observer.WithLifecycleStateKt$suspendWithStateAtLeastUnchecked$2$observer$1(state, lifecycle, (CancellableContinuation)cancellableContinuationImpl, (Function0)function0);
        if (b) {
            coroutineDispatcher.dispatch((CoroutineContext)EmptyCoroutineContext.INSTANCE, (Runnable)new WithLifecycleStateKt$suspendWithStateAtLeastUnchecked$2.WithLifecycleStateKt$suspendWithStateAtLeastUnchecked$2$1(lifecycle, withLifecycleStateKt$suspendWithStateAtLeastUnchecked$2$observer$1));
        }
        else {
            lifecycle.addObserver((LifecycleObserver)withLifecycleStateKt$suspendWithStateAtLeastUnchecked$2$observer$1);
        }
        ((CancellableContinuation)cancellableContinuationImpl).\u30070\u3007O0088o((Function1)new WithLifecycleStateKt$suspendWithStateAtLeastUnchecked$2.WithLifecycleStateKt$suspendWithStateAtLeastUnchecked$2$2(coroutineDispatcher, lifecycle, withLifecycleStateKt$suspendWithStateAtLeastUnchecked$2$observer$1));
        final Object o\u3007O8\u3007\u3007o = cancellableContinuationImpl.o\u3007O8\u3007\u3007o();
        if (o\u3007O8\u3007\u3007o == IntrinsicsKt.O8()) {
            DebugProbesKt.\u3007o\u3007((Continuation)continuation);
        }
        return o\u3007O8\u3007\u3007o;
    }
    
    public static final <R> Object withCreated(@NotNull final Lifecycle lifecycle, @NotNull final Function0<? extends R> function0, @NotNull final Continuation<? super R> continuation) {
        final Lifecycle.State created = Lifecycle.State.CREATED;
        final MainCoroutineDispatcher \u30078 = Dispatchers.\u3007o\u3007().\u30078();
        final boolean dispatchNeeded = ((CoroutineDispatcher)\u30078).isDispatchNeeded(continuation.getContext());
        if (!dispatchNeeded) {
            if (lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
                throw new LifecycleDestroyedException();
            }
            if (lifecycle.getCurrentState().compareTo(created) >= 0) {
                return function0.invoke();
            }
        }
        return suspendWithStateAtLeastUnchecked(lifecycle, created, dispatchNeeded, (CoroutineDispatcher)\u30078, (kotlin.jvm.functions.Function0<?>)new WithLifecycleStateKt$withStateAtLeastUnchecked.WithLifecycleStateKt$withStateAtLeastUnchecked$2((Function0)function0), (kotlin.coroutines.Continuation<? super Object>)continuation);
    }
    
    public static final <R> Object withCreated(@NotNull final LifecycleOwner lifecycleOwner, @NotNull final Function0<? extends R> function0, @NotNull final Continuation<? super R> continuation) {
        final Lifecycle lifecycle = lifecycleOwner.getLifecycle();
        Intrinsics.checkNotNullExpressionValue((Object)lifecycle, "lifecycle");
        final Lifecycle.State created = Lifecycle.State.CREATED;
        final MainCoroutineDispatcher \u30078 = Dispatchers.\u3007o\u3007().\u30078();
        final boolean dispatchNeeded = ((CoroutineDispatcher)\u30078).isDispatchNeeded(continuation.getContext());
        if (!dispatchNeeded) {
            if (lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
                throw new LifecycleDestroyedException();
            }
            if (lifecycle.getCurrentState().compareTo(created) >= 0) {
                return function0.invoke();
            }
        }
        return suspendWithStateAtLeastUnchecked(lifecycle, created, dispatchNeeded, (CoroutineDispatcher)\u30078, (kotlin.jvm.functions.Function0<?>)new WithLifecycleStateKt$withStateAtLeastUnchecked.WithLifecycleStateKt$withStateAtLeastUnchecked$2((Function0)function0), (kotlin.coroutines.Continuation<? super Object>)continuation);
    }
    
    private static final <R> Object withCreated$$forInline(final Lifecycle lifecycle, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        final Lifecycle.State destroyed = Lifecycle.State.DESTROYED;
        Dispatchers.\u3007o\u3007().\u30078();
        InlineMarker.\u3007o\u3007(3);
        throw null;
    }
    
    private static final <R> Object withCreated$$forInline(final LifecycleOwner lifecycleOwner, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        Intrinsics.checkNotNullExpressionValue((Object)lifecycleOwner.getLifecycle(), "lifecycle");
        final Lifecycle.State destroyed = Lifecycle.State.DESTROYED;
        Dispatchers.\u3007o\u3007().\u30078();
        InlineMarker.\u3007o\u3007(3);
        throw null;
    }
    
    public static final <R> Object withResumed(@NotNull final Lifecycle lifecycle, @NotNull final Function0<? extends R> function0, @NotNull final Continuation<? super R> continuation) {
        final Lifecycle.State resumed = Lifecycle.State.RESUMED;
        final MainCoroutineDispatcher \u30078 = Dispatchers.\u3007o\u3007().\u30078();
        final boolean dispatchNeeded = ((CoroutineDispatcher)\u30078).isDispatchNeeded(continuation.getContext());
        if (!dispatchNeeded) {
            if (lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
                throw new LifecycleDestroyedException();
            }
            if (lifecycle.getCurrentState().compareTo(resumed) >= 0) {
                return function0.invoke();
            }
        }
        return suspendWithStateAtLeastUnchecked(lifecycle, resumed, dispatchNeeded, (CoroutineDispatcher)\u30078, (kotlin.jvm.functions.Function0<?>)new WithLifecycleStateKt$withStateAtLeastUnchecked.WithLifecycleStateKt$withStateAtLeastUnchecked$2((Function0)function0), (kotlin.coroutines.Continuation<? super Object>)continuation);
    }
    
    public static final <R> Object withResumed(@NotNull final LifecycleOwner lifecycleOwner, @NotNull final Function0<? extends R> function0, @NotNull final Continuation<? super R> continuation) {
        final Lifecycle lifecycle = lifecycleOwner.getLifecycle();
        Intrinsics.checkNotNullExpressionValue((Object)lifecycle, "lifecycle");
        final Lifecycle.State resumed = Lifecycle.State.RESUMED;
        final MainCoroutineDispatcher \u30078 = Dispatchers.\u3007o\u3007().\u30078();
        final boolean dispatchNeeded = ((CoroutineDispatcher)\u30078).isDispatchNeeded(continuation.getContext());
        if (!dispatchNeeded) {
            if (lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
                throw new LifecycleDestroyedException();
            }
            if (lifecycle.getCurrentState().compareTo(resumed) >= 0) {
                return function0.invoke();
            }
        }
        return suspendWithStateAtLeastUnchecked(lifecycle, resumed, dispatchNeeded, (CoroutineDispatcher)\u30078, (kotlin.jvm.functions.Function0<?>)new WithLifecycleStateKt$withStateAtLeastUnchecked.WithLifecycleStateKt$withStateAtLeastUnchecked$2((Function0)function0), (kotlin.coroutines.Continuation<? super Object>)continuation);
    }
    
    private static final <R> Object withResumed$$forInline(final Lifecycle lifecycle, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        final Lifecycle.State destroyed = Lifecycle.State.DESTROYED;
        Dispatchers.\u3007o\u3007().\u30078();
        InlineMarker.\u3007o\u3007(3);
        throw null;
    }
    
    private static final <R> Object withResumed$$forInline(final LifecycleOwner lifecycleOwner, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        Intrinsics.checkNotNullExpressionValue((Object)lifecycleOwner.getLifecycle(), "lifecycle");
        final Lifecycle.State destroyed = Lifecycle.State.DESTROYED;
        Dispatchers.\u3007o\u3007().\u30078();
        InlineMarker.\u3007o\u3007(3);
        throw null;
    }
    
    public static final <R> Object withStarted(@NotNull final Lifecycle lifecycle, @NotNull final Function0<? extends R> function0, @NotNull final Continuation<? super R> continuation) {
        final Lifecycle.State started = Lifecycle.State.STARTED;
        final MainCoroutineDispatcher \u30078 = Dispatchers.\u3007o\u3007().\u30078();
        final boolean dispatchNeeded = ((CoroutineDispatcher)\u30078).isDispatchNeeded(continuation.getContext());
        if (!dispatchNeeded) {
            if (lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
                throw new LifecycleDestroyedException();
            }
            if (lifecycle.getCurrentState().compareTo(started) >= 0) {
                return function0.invoke();
            }
        }
        return suspendWithStateAtLeastUnchecked(lifecycle, started, dispatchNeeded, (CoroutineDispatcher)\u30078, (kotlin.jvm.functions.Function0<?>)new WithLifecycleStateKt$withStateAtLeastUnchecked.WithLifecycleStateKt$withStateAtLeastUnchecked$2((Function0)function0), (kotlin.coroutines.Continuation<? super Object>)continuation);
    }
    
    public static final <R> Object withStarted(@NotNull final LifecycleOwner lifecycleOwner, @NotNull final Function0<? extends R> function0, @NotNull final Continuation<? super R> continuation) {
        final Lifecycle lifecycle = lifecycleOwner.getLifecycle();
        Intrinsics.checkNotNullExpressionValue((Object)lifecycle, "lifecycle");
        final Lifecycle.State started = Lifecycle.State.STARTED;
        final MainCoroutineDispatcher \u30078 = Dispatchers.\u3007o\u3007().\u30078();
        final boolean dispatchNeeded = ((CoroutineDispatcher)\u30078).isDispatchNeeded(continuation.getContext());
        if (!dispatchNeeded) {
            if (lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
                throw new LifecycleDestroyedException();
            }
            if (lifecycle.getCurrentState().compareTo(started) >= 0) {
                return function0.invoke();
            }
        }
        return suspendWithStateAtLeastUnchecked(lifecycle, started, dispatchNeeded, (CoroutineDispatcher)\u30078, (kotlin.jvm.functions.Function0<?>)new WithLifecycleStateKt$withStateAtLeastUnchecked.WithLifecycleStateKt$withStateAtLeastUnchecked$2((Function0)function0), (kotlin.coroutines.Continuation<? super Object>)continuation);
    }
    
    private static final <R> Object withStarted$$forInline(final Lifecycle lifecycle, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        final Lifecycle.State destroyed = Lifecycle.State.DESTROYED;
        Dispatchers.\u3007o\u3007().\u30078();
        InlineMarker.\u3007o\u3007(3);
        throw null;
    }
    
    private static final <R> Object withStarted$$forInline(final LifecycleOwner lifecycleOwner, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        Intrinsics.checkNotNullExpressionValue((Object)lifecycleOwner.getLifecycle(), "lifecycle");
        final Lifecycle.State destroyed = Lifecycle.State.DESTROYED;
        Dispatchers.\u3007o\u3007().\u30078();
        InlineMarker.\u3007o\u3007(3);
        throw null;
    }
    
    public static final <R> Object withStateAtLeast(@NotNull final Lifecycle lifecycle, @NotNull final Lifecycle.State state, @NotNull final Function0<? extends R> function0, @NotNull final Continuation<? super R> continuation) {
        if (state.compareTo(Lifecycle.State.CREATED) >= 0) {
            final MainCoroutineDispatcher \u30078 = Dispatchers.\u3007o\u3007().\u30078();
            final boolean dispatchNeeded = ((CoroutineDispatcher)\u30078).isDispatchNeeded(continuation.getContext());
            if (!dispatchNeeded) {
                if (lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
                    throw new LifecycleDestroyedException();
                }
                if (lifecycle.getCurrentState().compareTo(state) >= 0) {
                    return function0.invoke();
                }
            }
            return suspendWithStateAtLeastUnchecked(lifecycle, state, dispatchNeeded, (CoroutineDispatcher)\u30078, (kotlin.jvm.functions.Function0<?>)new WithLifecycleStateKt$withStateAtLeastUnchecked.WithLifecycleStateKt$withStateAtLeastUnchecked$2((Function0)function0), (kotlin.coroutines.Continuation<? super Object>)continuation);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("target state must be CREATED or greater, found ");
        sb.append(state);
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    public static final <R> Object withStateAtLeast(@NotNull final LifecycleOwner lifecycleOwner, @NotNull final Lifecycle.State state, @NotNull final Function0<? extends R> function0, @NotNull final Continuation<? super R> continuation) {
        final Lifecycle lifecycle = lifecycleOwner.getLifecycle();
        Intrinsics.checkNotNullExpressionValue((Object)lifecycle, "lifecycle");
        if (state.compareTo(Lifecycle.State.CREATED) >= 0) {
            final MainCoroutineDispatcher \u30078 = Dispatchers.\u3007o\u3007().\u30078();
            final boolean dispatchNeeded = ((CoroutineDispatcher)\u30078).isDispatchNeeded(continuation.getContext());
            if (!dispatchNeeded) {
                if (lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
                    throw new LifecycleDestroyedException();
                }
                if (lifecycle.getCurrentState().compareTo(state) >= 0) {
                    return function0.invoke();
                }
            }
            return suspendWithStateAtLeastUnchecked(lifecycle, state, dispatchNeeded, (CoroutineDispatcher)\u30078, (kotlin.jvm.functions.Function0<?>)new WithLifecycleStateKt$withStateAtLeastUnchecked.WithLifecycleStateKt$withStateAtLeastUnchecked$2((Function0)function0), (kotlin.coroutines.Continuation<? super Object>)continuation);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("target state must be CREATED or greater, found ");
        sb.append(state);
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    private static final <R> Object withStateAtLeast$$forInline(final Lifecycle lifecycle, final Lifecycle.State obj, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        if (obj.compareTo(Lifecycle.State.CREATED) < 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("target state must be CREATED or greater, found ");
            sb.append(obj);
            throw new IllegalArgumentException(sb.toString().toString());
        }
        Dispatchers.\u3007o\u3007().\u30078();
        InlineMarker.\u3007o\u3007(3);
        throw null;
    }
    
    private static final <R> Object withStateAtLeast$$forInline(final LifecycleOwner lifecycleOwner, final Lifecycle.State obj, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        Intrinsics.checkNotNullExpressionValue((Object)lifecycleOwner.getLifecycle(), "lifecycle");
        if (obj.compareTo(Lifecycle.State.CREATED) < 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("target state must be CREATED or greater, found ");
            sb.append(obj);
            throw new IllegalArgumentException(sb.toString().toString());
        }
        Dispatchers.\u3007o\u3007().\u30078();
        InlineMarker.\u3007o\u3007(3);
        throw null;
    }
    
    public static final <R> Object withStateAtLeastUnchecked(@NotNull final Lifecycle lifecycle, @NotNull final Lifecycle.State o, @NotNull final Function0<? extends R> function0, @NotNull final Continuation<? super R> continuation) {
        final MainCoroutineDispatcher \u30078 = Dispatchers.\u3007o\u3007().\u30078();
        final boolean dispatchNeeded = ((CoroutineDispatcher)\u30078).isDispatchNeeded(continuation.getContext());
        if (!dispatchNeeded) {
            if (lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
                throw new LifecycleDestroyedException();
            }
            if (lifecycle.getCurrentState().compareTo(o) >= 0) {
                return function0.invoke();
            }
        }
        return suspendWithStateAtLeastUnchecked(lifecycle, o, dispatchNeeded, (CoroutineDispatcher)\u30078, (kotlin.jvm.functions.Function0<?>)new WithLifecycleStateKt$withStateAtLeastUnchecked.WithLifecycleStateKt$withStateAtLeastUnchecked$2((Function0)function0), (kotlin.coroutines.Continuation<? super Object>)continuation);
    }
    
    private static final <R> Object withStateAtLeastUnchecked$$forInline(final Lifecycle lifecycle, final Lifecycle.State state, final Function0<? extends R> function0, final Continuation<? super R> continuation) {
        Dispatchers.\u3007o\u3007().\u30078();
        InlineMarker.\u3007o\u3007(3);
        throw null;
    }
}

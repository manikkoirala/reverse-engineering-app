// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.util.concurrent.CancellationException;
import kotlinx.coroutines.JobKt;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.CoroutineContext;
import kotlin.Metadata;
import kotlinx.coroutines.CoroutineScope;
import java.io.Closeable;

@Metadata
public final class CloseableCoroutineScope implements Closeable, CoroutineScope
{
    @NotNull
    private final CoroutineContext coroutineContext;
    
    public CloseableCoroutineScope(@NotNull final CoroutineContext coroutineContext) {
        Intrinsics.checkNotNullParameter((Object)coroutineContext, "context");
        this.coroutineContext = coroutineContext;
    }
    
    @Override
    public void close() {
        JobKt.O8(this.getCoroutineContext(), (CancellationException)null, 1, (Object)null);
    }
    
    @NotNull
    public CoroutineContext getCoroutineContext() {
        return this.coroutineContext;
    }
}

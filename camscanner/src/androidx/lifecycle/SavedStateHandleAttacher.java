// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class SavedStateHandleAttacher implements LifecycleEventObserver
{
    @NotNull
    private final SavedStateHandlesProvider provider;
    
    public SavedStateHandleAttacher(@NotNull final SavedStateHandlesProvider provider) {
        Intrinsics.checkNotNullParameter((Object)provider, "provider");
        this.provider = provider;
    }
    
    @Override
    public void onStateChanged(@NotNull final LifecycleOwner lifecycleOwner, @NotNull final Lifecycle.Event obj) {
        Intrinsics.checkNotNullParameter((Object)lifecycleOwner, "source");
        Intrinsics.checkNotNullParameter((Object)obj, "event");
        if (obj == Lifecycle.Event.ON_CREATE) {
            lifecycleOwner.getLifecycle().removeObserver(this);
            this.provider.performRestore();
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Next event must be ON_CREATE, it was ");
        sb.append(obj);
        throw new IllegalStateException(sb.toString().toString());
    }
}

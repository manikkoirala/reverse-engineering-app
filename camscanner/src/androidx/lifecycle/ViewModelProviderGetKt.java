// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.MainThread;
import kotlin.jvm.internal.Intrinsics;
import androidx.lifecycle.viewmodel.CreationExtras;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class ViewModelProviderGetKt
{
    @NotNull
    public static final CreationExtras defaultCreationExtras(@NotNull final ViewModelStoreOwner viewModelStoreOwner) {
        Intrinsics.checkNotNullParameter((Object)viewModelStoreOwner, "owner");
        CreationExtras creationExtras;
        if (viewModelStoreOwner instanceof HasDefaultViewModelProviderFactory) {
            creationExtras = ((HasDefaultViewModelProviderFactory)viewModelStoreOwner).getDefaultViewModelCreationExtras();
            Intrinsics.checkNotNullExpressionValue((Object)creationExtras, "{\n        owner.defaultV\u2026ModelCreationExtras\n    }");
        }
        else {
            creationExtras = CreationExtras.Empty.INSTANCE;
        }
        return creationExtras;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.NonNull;

class CompositeGeneratedAdaptersObserver implements LifecycleEventObserver
{
    private final GeneratedAdapter[] mGeneratedAdapters;
    
    CompositeGeneratedAdaptersObserver(final GeneratedAdapter[] mGeneratedAdapters) {
        this.mGeneratedAdapters = mGeneratedAdapters;
    }
    
    @Override
    public void onStateChanged(@NonNull final LifecycleOwner lifecycleOwner, @NonNull final Lifecycle.Event event) {
        final MethodCallsLogger methodCallsLogger = new MethodCallsLogger();
        final GeneratedAdapter[] mGeneratedAdapters = this.mGeneratedAdapters;
        final int length = mGeneratedAdapters.length;
        final int n = 0;
        for (int i = 0; i < length; ++i) {
            mGeneratedAdapters[i].callMethods(lifecycleOwner, event, false, methodCallsLogger);
        }
        final GeneratedAdapter[] mGeneratedAdapters2 = this.mGeneratedAdapters;
        for (int length2 = mGeneratedAdapters2.length, j = n; j < length2; ++j) {
            mGeneratedAdapters2[j].callMethods(lifecycleOwner, event, true, methodCallsLogger);
        }
    }
}

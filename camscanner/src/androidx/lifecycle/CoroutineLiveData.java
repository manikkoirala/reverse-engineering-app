// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import kotlin.coroutines.AbstractCoroutineContextElement;
import kotlinx.coroutines.DisposableHandle;
import kotlin.ResultKt;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Lambda;
import kotlin.jvm.functions.Function0;
import kotlinx.coroutines.CoroutineScopeKt;
import kotlinx.coroutines.SupervisorKt;
import kotlin.coroutines.CoroutineContext$Key;
import kotlinx.coroutines.Job;
import kotlinx.coroutines.Dispatchers;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.CoroutineContext;
import kotlin.Metadata;

@Metadata
public final class CoroutineLiveData<T> extends MediatorLiveData<T>
{
    private BlockRunner<T> blockRunner;
    private EmittedSource emittedSource;
    
    public CoroutineLiveData(@NotNull final CoroutineContext coroutineContext, final long n, @NotNull final Function2<? super LiveDataScope<T>, ? super Continuation<? super Unit>, ?> function2) {
        Intrinsics.checkNotNullParameter((Object)coroutineContext, "context");
        Intrinsics.checkNotNullParameter((Object)function2, "block");
        this.blockRunner = new BlockRunner<T>(this, function2, n, CoroutineScopeKt.\u3007080(((AbstractCoroutineContextElement)Dispatchers.\u3007o\u3007().\u30078()).plus(coroutineContext).plus((CoroutineContext)SupervisorKt.\u3007080((Job)coroutineContext.get((CoroutineContext$Key)Job.\u3007o\u3007)))), (Function0<Unit>)new Function0<Unit>(this) {
            final CoroutineLiveData<T> this$0;
            
            public final void invoke() {
                CoroutineLiveData.access$setBlockRunner$p(this.this$0, null);
            }
        });
    }
    
    public static final /* synthetic */ void access$setBlockRunner$p(final CoroutineLiveData coroutineLiveData, final BlockRunner blockRunner) {
        coroutineLiveData.blockRunner = blockRunner;
    }
    
    public final Object clearSource$lifecycle_livedata_ktx_release(@NotNull final Continuation<? super Unit> continuation) {
        Object o = null;
        Label_0047: {
            if (continuation instanceof CoroutineLiveData$clearSource.CoroutineLiveData$clearSource$1) {
                final CoroutineLiveData$clearSource.CoroutineLiveData$clearSource$1 coroutineLiveData$clearSource$1 = (CoroutineLiveData$clearSource.CoroutineLiveData$clearSource$1)continuation;
                final int label = coroutineLiveData$clearSource$1.label;
                if ((label & Integer.MIN_VALUE) != 0x0) {
                    coroutineLiveData$clearSource$1.label = label + Integer.MIN_VALUE;
                    o = coroutineLiveData$clearSource$1;
                    break Label_0047;
                }
            }
            o = new CoroutineLiveData$clearSource.CoroutineLiveData$clearSource$1(this, (Continuation)continuation);
        }
        final Object result = ((CoroutineLiveData$clearSource.CoroutineLiveData$clearSource$1)o).result;
        final Object o2 = IntrinsicsKt.O8();
        final int label2 = ((CoroutineLiveData$clearSource.CoroutineLiveData$clearSource$1)o).label;
        CoroutineLiveData coroutineLiveData;
        if (label2 != 0) {
            if (label2 != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            coroutineLiveData = (CoroutineLiveData)((CoroutineLiveData$clearSource.CoroutineLiveData$clearSource$1)o).L$0;
            ResultKt.\u3007o00\u3007\u3007Oo(result);
        }
        else {
            ResultKt.\u3007o00\u3007\u3007Oo(result);
            final EmittedSource emittedSource = this.emittedSource;
            if (emittedSource != null) {
                ((CoroutineLiveData$clearSource.CoroutineLiveData$clearSource$1)o).L$0 = this;
                ((CoroutineLiveData$clearSource.CoroutineLiveData$clearSource$1)o).label = 1;
                if (emittedSource.disposeNow((Continuation<? super Unit>)o) == o2) {
                    return o2;
                }
            }
            coroutineLiveData = this;
        }
        coroutineLiveData.emittedSource = null;
        return Unit.\u3007080;
    }
    
    public final Object emitSource$lifecycle_livedata_ktx_release(@NotNull final LiveData<T> l$1, @NotNull final Continuation<? super DisposableHandle> continuation) {
        Object o = null;
        Label_0051: {
            if (continuation instanceof CoroutineLiveData$emitSource.CoroutineLiveData$emitSource$1) {
                final CoroutineLiveData$emitSource.CoroutineLiveData$emitSource$1 coroutineLiveData$emitSource$1 = (CoroutineLiveData$emitSource.CoroutineLiveData$emitSource$1)continuation;
                final int label = coroutineLiveData$emitSource$1.label;
                if ((label & Integer.MIN_VALUE) != 0x0) {
                    coroutineLiveData$emitSource$1.label = label + Integer.MIN_VALUE;
                    o = coroutineLiveData$emitSource$1;
                    break Label_0051;
                }
            }
            o = new CoroutineLiveData$emitSource.CoroutineLiveData$emitSource$1(this, (Continuation)continuation);
        }
        final Object result = ((CoroutineLiveData$emitSource.CoroutineLiveData$emitSource$1)o).result;
        final Object o2 = IntrinsicsKt.O8();
        final int label2 = ((CoroutineLiveData$emitSource.CoroutineLiveData$emitSource$1)o).label;
        CoroutineLiveData l$2;
        LiveData<T> liveData2;
        if (label2 != 0) {
            if (label2 != 1) {
                if (label2 == 2) {
                    l$2 = (CoroutineLiveData)((CoroutineLiveData$emitSource.CoroutineLiveData$emitSource$1)o).L$0;
                    ResultKt.\u3007o00\u3007\u3007Oo(result);
                    final Object addDisposableSource = result;
                    return l$2.emittedSource = (EmittedSource)addDisposableSource;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            else {
                final LiveData liveData = (LiveData)((CoroutineLiveData$emitSource.CoroutineLiveData$emitSource$1)o).L$1;
                l$2 = (CoroutineLiveData)((CoroutineLiveData$emitSource.CoroutineLiveData$emitSource$1)o).L$0;
                ResultKt.\u3007o00\u3007\u3007Oo(result);
                liveData2 = liveData;
            }
        }
        else {
            ResultKt.\u3007o00\u3007\u3007Oo(result);
            ((CoroutineLiveData$emitSource.CoroutineLiveData$emitSource$1)o).L$0 = this;
            ((CoroutineLiveData$emitSource.CoroutineLiveData$emitSource$1)o).L$1 = l$1;
            ((CoroutineLiveData$emitSource.CoroutineLiveData$emitSource$1)o).label = 1;
            if (this.clearSource$lifecycle_livedata_ktx_release((Continuation<? super Unit>)o) == o2) {
                return o2;
            }
            liveData2 = l$1;
            l$2 = this;
        }
        ((CoroutineLiveData$emitSource.CoroutineLiveData$emitSource$1)o).L$0 = l$2;
        ((CoroutineLiveData$emitSource.CoroutineLiveData$emitSource$1)o).L$1 = null;
        ((CoroutineLiveData$emitSource.CoroutineLiveData$emitSource$1)o).label = 2;
        Object addDisposableSource;
        if ((addDisposableSource = CoroutineLiveDataKt.addDisposableSource(l$2, liveData2, (Continuation<? super EmittedSource>)o)) == o2) {
            return o2;
        }
        return l$2.emittedSource = (EmittedSource)addDisposableSource;
    }
    
    @Override
    protected void onActive() {
        super.onActive();
        final BlockRunner<T> blockRunner = this.blockRunner;
        if (blockRunner != null) {
            blockRunner.maybeRun();
        }
    }
    
    @Override
    protected void onInactive() {
        super.onInactive();
        final BlockRunner<T> blockRunner = this.blockRunner;
        if (blockRunner != null) {
            blockRunner.cancel();
        }
    }
}

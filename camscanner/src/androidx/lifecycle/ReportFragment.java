// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import android.app.Application$ActivityLifecycleCallbacks;
import android.os.Bundle;
import android.app.FragmentManager;
import android.os.Build$VERSION;
import androidx.annotation.NonNull;
import android.app.Activity;
import androidx.annotation.RestrictTo;
import android.app.Fragment;

@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public class ReportFragment extends Fragment
{
    private static final String REPORT_FRAGMENT_TAG = "androidx.lifecycle.LifecycleDispatcher.report_fragment_tag";
    private ActivityInitializationListener mProcessListener;
    
    static void dispatch(@NonNull final Activity activity, @NonNull final Lifecycle.Event event) {
        if (activity instanceof LifecycleRegistryOwner) {
            ((LifecycleRegistryOwner)activity).getLifecycle().handleLifecycleEvent(event);
            return;
        }
        if (activity instanceof LifecycleOwner) {
            final Lifecycle lifecycle = ((LifecycleOwner)activity).getLifecycle();
            if (lifecycle instanceof LifecycleRegistry) {
                ((LifecycleRegistry)lifecycle).handleLifecycleEvent(event);
            }
        }
    }
    
    private void dispatch(@NonNull final Lifecycle.Event event) {
        if (Build$VERSION.SDK_INT < 29) {
            dispatch(this.getActivity(), event);
        }
    }
    
    private void dispatchCreate(final ActivityInitializationListener activityInitializationListener) {
        if (activityInitializationListener != null) {
            activityInitializationListener.onCreate();
        }
    }
    
    private void dispatchResume(final ActivityInitializationListener activityInitializationListener) {
        if (activityInitializationListener != null) {
            activityInitializationListener.onResume();
        }
    }
    
    private void dispatchStart(final ActivityInitializationListener activityInitializationListener) {
        if (activityInitializationListener != null) {
            activityInitializationListener.onStart();
        }
    }
    
    static ReportFragment get(final Activity activity) {
        return (ReportFragment)activity.getFragmentManager().findFragmentByTag("androidx.lifecycle.LifecycleDispatcher.report_fragment_tag");
    }
    
    public static void injectIfNeededIn(final Activity activity) {
        if (Build$VERSION.SDK_INT >= 29) {
            LifecycleCallbacks.registerIn(activity);
        }
        final FragmentManager fragmentManager = activity.getFragmentManager();
        if (fragmentManager.findFragmentByTag("androidx.lifecycle.LifecycleDispatcher.report_fragment_tag") == null) {
            fragmentManager.beginTransaction().add((Fragment)new ReportFragment(), "androidx.lifecycle.LifecycleDispatcher.report_fragment_tag").commit();
            fragmentManager.executePendingTransactions();
        }
    }
    
    public void onActivityCreated(final Bundle bundle) {
        super.onActivityCreated(bundle);
        this.dispatchCreate(this.mProcessListener);
        this.dispatch(Lifecycle.Event.ON_CREATE);
    }
    
    public void onDestroy() {
        super.onDestroy();
        this.dispatch(Lifecycle.Event.ON_DESTROY);
        this.mProcessListener = null;
    }
    
    public void onPause() {
        super.onPause();
        this.dispatch(Lifecycle.Event.ON_PAUSE);
    }
    
    public void onResume() {
        super.onResume();
        this.dispatchResume(this.mProcessListener);
        this.dispatch(Lifecycle.Event.ON_RESUME);
    }
    
    public void onStart() {
        super.onStart();
        this.dispatchStart(this.mProcessListener);
        this.dispatch(Lifecycle.Event.ON_START);
    }
    
    public void onStop() {
        super.onStop();
        this.dispatch(Lifecycle.Event.ON_STOP);
    }
    
    void setProcessListener(final ActivityInitializationListener mProcessListener) {
        this.mProcessListener = mProcessListener;
    }
    
    interface ActivityInitializationListener
    {
        void onCreate();
        
        void onResume();
        
        void onStart();
    }
    
    @RequiresApi(29)
    static class LifecycleCallbacks implements Application$ActivityLifecycleCallbacks
    {
        static void registerIn(final Activity activity) {
            \u3007\u3007888.\u3007080(activity, (Application$ActivityLifecycleCallbacks)new LifecycleCallbacks());
        }
        
        public void onActivityCreated(@NonNull final Activity activity, @Nullable final Bundle bundle) {
        }
        
        public void onActivityDestroyed(@NonNull final Activity activity) {
        }
        
        public void onActivityPaused(@NonNull final Activity activity) {
        }
        
        public void onActivityPostCreated(@NonNull final Activity activity, @Nullable final Bundle bundle) {
            ReportFragment.dispatch(activity, Lifecycle.Event.ON_CREATE);
        }
        
        public void onActivityPostResumed(@NonNull final Activity activity) {
            ReportFragment.dispatch(activity, Lifecycle.Event.ON_RESUME);
        }
        
        public void onActivityPostStarted(@NonNull final Activity activity) {
            ReportFragment.dispatch(activity, Lifecycle.Event.ON_START);
        }
        
        public void onActivityPreDestroyed(@NonNull final Activity activity) {
            ReportFragment.dispatch(activity, Lifecycle.Event.ON_DESTROY);
        }
        
        public void onActivityPrePaused(@NonNull final Activity activity) {
            ReportFragment.dispatch(activity, Lifecycle.Event.ON_PAUSE);
        }
        
        public void onActivityPreStopped(@NonNull final Activity activity) {
            ReportFragment.dispatch(activity, Lifecycle.Event.ON_STOP);
        }
        
        public void onActivityResumed(@NonNull final Activity activity) {
        }
        
        public void onActivitySaveInstanceState(@NonNull final Activity activity, @NonNull final Bundle bundle) {
        }
        
        public void onActivityStarted(@NonNull final Activity activity) {
        }
        
        public void onActivityStopped(@NonNull final Activity activity) {
        }
    }
}

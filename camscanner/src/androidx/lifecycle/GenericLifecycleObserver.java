// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import androidx.annotation.RestrictTo;

@Deprecated
@RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP_PREFIX })
public interface GenericLifecycleObserver extends LifecycleEventObserver
{
}

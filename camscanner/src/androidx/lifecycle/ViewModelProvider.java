// 
// Decompiled by Procyon v0.6.0
// 

package androidx.lifecycle;

import java.io.Serializable;
import androidx.lifecycle.viewmodel.InitializerViewModelFactory;
import java.util.Arrays;
import androidx.lifecycle.viewmodel.ViewModelInitializer;
import androidx.annotation.RestrictTo;
import java.lang.reflect.InvocationTargetException;
import android.app.Application;
import androidx.lifecycle.viewmodel.MutableCreationExtras;
import androidx.annotation.MainThread;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import androidx.lifecycle.viewmodel.CreationExtras;
import kotlin.Metadata;

@Metadata
public class ViewModelProvider
{
    @NotNull
    private final CreationExtras defaultCreationExtras;
    @NotNull
    private final Factory factory;
    @NotNull
    private final ViewModelStore store;
    
    public ViewModelProvider(@NotNull final ViewModelStore viewModelStore, @NotNull final Factory factory) {
        Intrinsics.checkNotNullParameter((Object)viewModelStore, "store");
        Intrinsics.checkNotNullParameter((Object)factory, "factory");
        this(viewModelStore, factory, null, 4, null);
    }
    
    public ViewModelProvider(@NotNull final ViewModelStore store, @NotNull final Factory factory, @NotNull final CreationExtras defaultCreationExtras) {
        Intrinsics.checkNotNullParameter((Object)store, "store");
        Intrinsics.checkNotNullParameter((Object)factory, "factory");
        Intrinsics.checkNotNullParameter((Object)defaultCreationExtras, "defaultCreationExtras");
        this.store = store;
        this.factory = factory;
        this.defaultCreationExtras = defaultCreationExtras;
    }
    
    public ViewModelProvider(@NotNull final ViewModelStoreOwner viewModelStoreOwner) {
        Intrinsics.checkNotNullParameter((Object)viewModelStoreOwner, "owner");
        final ViewModelStore viewModelStore = viewModelStoreOwner.getViewModelStore();
        Intrinsics.checkNotNullExpressionValue((Object)viewModelStore, "owner.viewModelStore");
        this(viewModelStore, AndroidViewModelFactory.Companion.defaultFactory$lifecycle_viewmodel_release(viewModelStoreOwner), ViewModelProviderGetKt.defaultCreationExtras(viewModelStoreOwner));
    }
    
    public ViewModelProvider(@NotNull final ViewModelStoreOwner viewModelStoreOwner, @NotNull final Factory factory) {
        Intrinsics.checkNotNullParameter((Object)viewModelStoreOwner, "owner");
        Intrinsics.checkNotNullParameter((Object)factory, "factory");
        final ViewModelStore viewModelStore = viewModelStoreOwner.getViewModelStore();
        Intrinsics.checkNotNullExpressionValue((Object)viewModelStore, "owner.viewModelStore");
        this(viewModelStore, factory, ViewModelProviderGetKt.defaultCreationExtras(viewModelStoreOwner));
    }
    
    @MainThread
    @NotNull
    public <T extends ViewModel> T get(@NotNull final Class<T> clazz) {
        Intrinsics.checkNotNullParameter((Object)clazz, "modelClass");
        final String canonicalName = clazz.getCanonicalName();
        if (canonicalName != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("androidx.lifecycle.ViewModelProvider.DefaultKey:");
            sb.append(canonicalName);
            return this.get(sb.toString(), clazz);
        }
        throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }
    
    @MainThread
    @NotNull
    public <T extends ViewModel> T get(@NotNull final String s, @NotNull Class<T> o) {
        Intrinsics.checkNotNullParameter((Object)s, "key");
        Intrinsics.checkNotNullParameter(o, "modelClass");
        final ViewModel value = this.store.get(s);
        if (!((Class)o).isInstance(value)) {
            final MutableCreationExtras mutableCreationExtras = new MutableCreationExtras(this.defaultCreationExtras);
            mutableCreationExtras.set(NewInstanceFactory.VIEW_MODEL_KEY, s);
            try {
                o = this.factory.create((Class<T>)o, mutableCreationExtras);
            }
            catch (final AbstractMethodError abstractMethodError) {
                o = this.factory.create((Class<Class>)o);
            }
            this.store.put(s, (ViewModel)o);
            return (T)o;
        }
        final Factory factory = this.factory;
        OnRequeryFactory onRequeryFactory;
        if (factory instanceof OnRequeryFactory) {
            onRequeryFactory = (OnRequeryFactory)factory;
        }
        else {
            onRequeryFactory = null;
        }
        if (onRequeryFactory != null) {
            Intrinsics.checkNotNullExpressionValue((Object)value, "viewModel");
            onRequeryFactory.onRequery(value);
        }
        if (value != null) {
            return (T)value;
        }
        throw new NullPointerException("null cannot be cast to non-null type T of androidx.lifecycle.ViewModelProvider.get");
    }
    
    @Metadata
    public static class AndroidViewModelFactory extends NewInstanceFactory
    {
        @NotNull
        public static final CreationExtras.Key<Application> APPLICATION_KEY;
        @NotNull
        public static final Companion Companion;
        @NotNull
        public static final String DEFAULT_KEY = "androidx.lifecycle.ViewModelProvider.DefaultKey";
        private static AndroidViewModelFactory sInstance;
        private final Application application;
        
        static {
            Companion = new Companion(null);
            APPLICATION_KEY = ApplicationKeyImpl.INSTANCE;
        }
        
        public AndroidViewModelFactory() {
            this(null, 0);
        }
        
        public AndroidViewModelFactory(@NotNull final Application application) {
            Intrinsics.checkNotNullParameter((Object)application, "application");
            this(application, 0);
        }
        
        private AndroidViewModelFactory(final Application application, final int n) {
            this.application = application;
        }
        
        public static final /* synthetic */ AndroidViewModelFactory access$getSInstance$cp() {
            return AndroidViewModelFactory.sInstance;
        }
        
        public static final /* synthetic */ void access$setSInstance$cp(final AndroidViewModelFactory sInstance) {
            AndroidViewModelFactory.sInstance = sInstance;
        }
        
        private final <T extends ViewModel> T create(Class<T> create, final Application application) {
            if (AndroidViewModel.class.isAssignableFrom((Class<?>)create)) {
                try {
                    final ViewModel viewModel = (T)((Class<Class<Class<?>>>)create).getConstructor(Application.class).newInstance(application);
                    Intrinsics.checkNotNullExpressionValue((Object)viewModel, "{\n                try {\n\u2026          }\n            }");
                    create = (Serializable)viewModel;
                    return (T)create;
                }
                catch (final InvocationTargetException cause) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Cannot create an instance of ");
                    sb.append(create);
                    throw new RuntimeException(sb.toString(), cause);
                }
                catch (final InstantiationException cause2) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Cannot create an instance of ");
                    sb2.append(create);
                    throw new RuntimeException(sb2.toString(), cause2);
                }
                catch (final IllegalAccessException cause3) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Cannot create an instance of ");
                    sb3.append(create);
                    throw new RuntimeException(sb3.toString(), cause3);
                }
                catch (final NoSuchMethodException cause4) {
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("Cannot create an instance of ");
                    sb4.append(create);
                    throw new RuntimeException(sb4.toString(), cause4);
                }
            }
            create = super.create((Class<Class<Class<?>>>)create);
            return (T)create;
        }
        
        @NotNull
        public static final AndroidViewModelFactory getInstance(@NotNull final Application application) {
            return AndroidViewModelFactory.Companion.getInstance(application);
        }
        
        @NotNull
        @Override
        public <T extends ViewModel> T create(@NotNull final Class<T> clazz) {
            Intrinsics.checkNotNullParameter((Object)clazz, "modelClass");
            final Application application = this.application;
            if (application != null) {
                return this.create(clazz, application);
            }
            throw new UnsupportedOperationException("AndroidViewModelFactory constructed with empty constructor works only with create(modelClass: Class<T>, extras: CreationExtras).");
        }
        
        @NotNull
        @Override
        public <T extends ViewModel> T create(@NotNull final Class<T> clazz, @NotNull final CreationExtras creationExtras) {
            Intrinsics.checkNotNullParameter((Object)clazz, "modelClass");
            Intrinsics.checkNotNullParameter((Object)creationExtras, "extras");
            ViewModel viewModel;
            if (this.application != null) {
                viewModel = this.create(clazz);
            }
            else {
                final Application application = creationExtras.get(AndroidViewModelFactory.APPLICATION_KEY);
                if (application != null) {
                    viewModel = this.create(clazz, application);
                }
                else {
                    if (AndroidViewModel.class.isAssignableFrom(clazz)) {
                        throw new IllegalArgumentException("CreationExtras must have an application by `APPLICATION_KEY`");
                    }
                    viewModel = super.create(clazz);
                }
            }
            return (T)viewModel;
        }
        
        @Metadata
        public static final class Companion
        {
            private Companion() {
            }
            
            @NotNull
            public final Factory defaultFactory$lifecycle_viewmodel_release(@NotNull final ViewModelStoreOwner viewModelStoreOwner) {
                Intrinsics.checkNotNullParameter((Object)viewModelStoreOwner, "owner");
                Factory factory;
                if (viewModelStoreOwner instanceof HasDefaultViewModelProviderFactory) {
                    factory = ((HasDefaultViewModelProviderFactory)viewModelStoreOwner).getDefaultViewModelProviderFactory();
                    Intrinsics.checkNotNullExpressionValue((Object)factory, "owner.defaultViewModelProviderFactory");
                }
                else {
                    factory = NewInstanceFactory.Companion.getInstance();
                }
                return factory;
            }
            
            @NotNull
            public final AndroidViewModelFactory getInstance(@NotNull final Application application) {
                Intrinsics.checkNotNullParameter((Object)application, "application");
                if (AndroidViewModelFactory.access$getSInstance$cp() == null) {
                    AndroidViewModelFactory.access$setSInstance$cp(new AndroidViewModelFactory(application));
                }
                final AndroidViewModelFactory access$getSInstance$cp = AndroidViewModelFactory.access$getSInstance$cp();
                Intrinsics.Oo08((Object)access$getSInstance$cp);
                return access$getSInstance$cp;
            }
            
            @Metadata
            private static final class ApplicationKeyImpl implements Key<Application>
            {
                @NotNull
                public static final ApplicationKeyImpl INSTANCE;
                
                static {
                    INSTANCE = new ApplicationKeyImpl();
                }
            }
        }
    }
    
    @Metadata
    public static class NewInstanceFactory implements Factory
    {
        @NotNull
        public static final Companion Companion;
        @NotNull
        public static final CreationExtras.Key<String> VIEW_MODEL_KEY;
        private static NewInstanceFactory sInstance;
        
        static {
            Companion = new Companion(null);
            VIEW_MODEL_KEY = ViewModelKeyImpl.INSTANCE;
        }
        
        public static final /* synthetic */ NewInstanceFactory access$getSInstance$cp() {
            return NewInstanceFactory.sInstance;
        }
        
        public static final /* synthetic */ void access$setSInstance$cp(final NewInstanceFactory sInstance) {
            NewInstanceFactory.sInstance = sInstance;
        }
        
        @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
        @NotNull
        public static final NewInstanceFactory getInstance() {
            return NewInstanceFactory.Companion.getInstance();
        }
        
        @NotNull
        @Override
        public <T extends ViewModel> T create(@NotNull final Class<T> clazz) {
            Intrinsics.checkNotNullParameter((Object)clazz, "modelClass");
            try {
                final ViewModel instance = clazz.newInstance();
                Intrinsics.checkNotNullExpressionValue((Object)instance, "{\n                modelC\u2026wInstance()\n            }");
                return (T)instance;
            }
            catch (final IllegalAccessException cause) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Cannot create an instance of ");
                sb.append(clazz);
                throw new RuntimeException(sb.toString(), cause);
            }
            catch (final InstantiationException cause2) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Cannot create an instance of ");
                sb2.append(clazz);
                throw new RuntimeException(sb2.toString(), cause2);
            }
        }
        
        @Metadata
        public static final class Companion
        {
            private Companion() {
            }
            
            @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
            @NotNull
            public final NewInstanceFactory getInstance() {
                if (NewInstanceFactory.access$getSInstance$cp() == null) {
                    NewInstanceFactory.access$setSInstance$cp(new NewInstanceFactory());
                }
                final NewInstanceFactory access$getSInstance$cp = NewInstanceFactory.access$getSInstance$cp();
                Intrinsics.Oo08((Object)access$getSInstance$cp);
                return access$getSInstance$cp;
            }
            
            @Metadata
            private static final class ViewModelKeyImpl implements Key<String>
            {
                @NotNull
                public static final ViewModelKeyImpl INSTANCE;
                
                static {
                    INSTANCE = new ViewModelKeyImpl();
                }
            }
        }
    }
    
    @Metadata
    public interface Factory
    {
        @NotNull
        public static final Companion Companion = Factory.Companion.$$INSTANCE;
        
        @NotNull
         <T extends ViewModel> T create(@NotNull final Class<T> p0);
        
        @NotNull
         <T extends ViewModel> T create(@NotNull final Class<T> p0, @NotNull final CreationExtras p1);
        
        @Metadata
        public static final class Companion
        {
            static final Companion $$INSTANCE;
            
            static {
                $$INSTANCE = new Companion();
            }
            
            private Companion() {
            }
            
            @NotNull
            public final Factory from(@NotNull final ViewModelInitializer<?>... original) {
                Intrinsics.checkNotNullParameter((Object)original, "initializers");
                return (Factory)new InitializerViewModelFactory((ViewModelInitializer<?>[])Arrays.copyOf(original, original.length));
            }
        }
    }
    
    @Metadata
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public static class OnRequeryFactory
    {
        public void onRequery(@NotNull final ViewModel viewModel) {
            Intrinsics.checkNotNullParameter((Object)viewModel, "viewModel");
        }
    }
}

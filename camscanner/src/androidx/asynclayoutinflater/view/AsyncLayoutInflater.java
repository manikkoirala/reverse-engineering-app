// 
// Decompiled by Procyon v0.6.0
// 

package androidx.asynclayoutinflater.view;

import androidx.core.util.Pools;
import java.util.concurrent.ArrayBlockingQueue;
import android.view.View;
import android.util.AttributeSet;
import androidx.annotation.UiThread;
import androidx.annotation.Nullable;
import android.view.ViewGroup;
import androidx.annotation.LayoutRes;
import android.os.Message;
import androidx.annotation.NonNull;
import android.content.Context;
import android.view.LayoutInflater;
import android.os.Handler$Callback;
import android.os.Handler;

public final class AsyncLayoutInflater
{
    private static final String TAG = "AsyncLayoutInflater";
    Handler mHandler;
    private Handler$Callback mHandlerCallback;
    InflateThread mInflateThread;
    LayoutInflater mInflater;
    
    public AsyncLayoutInflater(@NonNull final Context context) {
        this.mHandlerCallback = (Handler$Callback)new Handler$Callback() {
            final AsyncLayoutInflater this$0;
            
            public boolean handleMessage(final Message message) {
                final InflateRequest inflateRequest = (InflateRequest)message.obj;
                if (inflateRequest.view == null) {
                    inflateRequest.view = this.this$0.mInflater.inflate(inflateRequest.resid, inflateRequest.parent, false);
                }
                inflateRequest.callback.onInflateFinished(inflateRequest.view, inflateRequest.resid, inflateRequest.parent);
                this.this$0.mInflateThread.releaseRequest(inflateRequest);
                return true;
            }
        };
        this.mInflater = new BasicInflater(context);
        this.mHandler = new Handler(this.mHandlerCallback);
        this.mInflateThread = InflateThread.getInstance();
    }
    
    @UiThread
    public void inflate(@LayoutRes final int resid, @Nullable final ViewGroup parent, @NonNull final OnInflateFinishedListener callback) {
        if (callback != null) {
            final InflateRequest obtainRequest = this.mInflateThread.obtainRequest();
            obtainRequest.inflater = this;
            obtainRequest.resid = resid;
            obtainRequest.parent = parent;
            obtainRequest.callback = callback;
            this.mInflateThread.enqueue(obtainRequest);
            return;
        }
        throw new NullPointerException("callback argument may not be null!");
    }
    
    private static class BasicInflater extends LayoutInflater
    {
        private static final String[] sClassPrefixList;
        
        static {
            sClassPrefixList = new String[] { "android.widget.", "android.webkit.", "android.app." };
        }
        
        BasicInflater(final Context context) {
            super(context);
        }
        
        public LayoutInflater cloneInContext(final Context context) {
            return new BasicInflater(context);
        }
        
        protected View onCreateView(final String s, final AttributeSet set) throws ClassNotFoundException {
            final String[] sClassPrefixList = BasicInflater.sClassPrefixList;
            final int length = sClassPrefixList.length;
            int n = 0;
        Label_0042_Outer:
            while (true) {
                Label_0048: {
                    if (n >= length) {
                        break Label_0048;
                    }
                    final String s2 = sClassPrefixList[n];
                    while (true) {
                        try {
                            final View view = this.createView(s, s2, set);
                            if (view != null) {
                                return view;
                            }
                            ++n;
                            continue Label_0042_Outer;
                            return super.onCreateView(s, set);
                        }
                        catch (final ClassNotFoundException ex) {
                            continue;
                        }
                        break;
                    }
                }
            }
        }
    }
    
    private static class InflateRequest
    {
        OnInflateFinishedListener callback;
        AsyncLayoutInflater inflater;
        ViewGroup parent;
        int resid;
        View view;
        
        InflateRequest() {
        }
    }
    
    private static class InflateThread extends Thread
    {
        private static final InflateThread sInstance;
        private ArrayBlockingQueue<InflateRequest> mQueue;
        private Pools.SynchronizedPool<InflateRequest> mRequestPool;
        
        static {
            (sInstance = new InflateThread()).start();
        }
        
        private InflateThread() {
            this.mQueue = new ArrayBlockingQueue<InflateRequest>(10);
            this.mRequestPool = (Pools.SynchronizedPool<InflateRequest>)new Pools.SynchronizedPool(10);
        }
        
        public static InflateThread getInstance() {
            return InflateThread.sInstance;
        }
        
        public void enqueue(final InflateRequest e) {
            try {
                this.mQueue.put(e);
            }
            catch (final InterruptedException cause) {
                throw new RuntimeException("Failed to enqueue async inflate request", cause);
            }
        }
        
        public InflateRequest obtainRequest() {
            InflateRequest inflateRequest;
            if ((inflateRequest = this.mRequestPool.acquire()) == null) {
                inflateRequest = new InflateRequest();
            }
            return inflateRequest;
        }
        
        public void releaseRequest(final InflateRequest inflateRequest) {
            inflateRequest.callback = null;
            inflateRequest.inflater = null;
            inflateRequest.parent = null;
            inflateRequest.resid = 0;
            inflateRequest.view = null;
            this.mRequestPool.release(inflateRequest);
        }
        
        @Override
        public void run() {
            while (true) {
                this.runInner();
            }
        }
        
        public void runInner() {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: getfield        androidx/asynclayoutinflater/view/AsyncLayoutInflater$InflateThread.mQueue:Ljava/util/concurrent/ArrayBlockingQueue;
            //     4: invokevirtual   java/util/concurrent/ArrayBlockingQueue.take:()Ljava/lang/Object;
            //     7: checkcast       Landroidx/asynclayoutinflater/view/AsyncLayoutInflater$InflateRequest;
            //    10: astore_2       
            //    11: aload_2        
            //    12: aload_2        
            //    13: getfield        androidx/asynclayoutinflater/view/AsyncLayoutInflater$InflateRequest.inflater:Landroidx/asynclayoutinflater/view/AsyncLayoutInflater;
            //    16: getfield        androidx/asynclayoutinflater/view/AsyncLayoutInflater.mInflater:Landroid/view/LayoutInflater;
            //    19: aload_2        
            //    20: getfield        androidx/asynclayoutinflater/view/AsyncLayoutInflater$InflateRequest.resid:I
            //    23: aload_2        
            //    24: getfield        androidx/asynclayoutinflater/view/AsyncLayoutInflater$InflateRequest.parent:Landroid/view/ViewGroup;
            //    27: iconst_0       
            //    28: invokevirtual   android/view/LayoutInflater.inflate:(ILandroid/view/ViewGroup;Z)Landroid/view/View;
            //    31: putfield        androidx/asynclayoutinflater/view/AsyncLayoutInflater$InflateRequest.view:Landroid/view/View;
            //    34: aload_2        
            //    35: getfield        androidx/asynclayoutinflater/view/AsyncLayoutInflater$InflateRequest.inflater:Landroidx/asynclayoutinflater/view/AsyncLayoutInflater;
            //    38: getfield        androidx/asynclayoutinflater/view/AsyncLayoutInflater.mHandler:Landroid/os/Handler;
            //    41: iconst_0       
            //    42: aload_2        
            //    43: invokestatic    android/os/Message.obtain:(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;
            //    46: invokevirtual   android/os/Message.sendToTarget:()V
            //    49: return         
            //    50: astore_1       
            //    51: goto            49
            //    54: astore_1       
            //    55: goto            34
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                            
            //  -----  -----  -----  -----  --------------------------------
            //  0      11     50     54     Ljava/lang/InterruptedException;
            //  11     34     54     58     Ljava/lang/RuntimeException;
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0034:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
    }
    
    public interface OnInflateFinishedListener
    {
        void onInflateFinished(@NonNull final View p0, @LayoutRes final int p1, @Nullable final ViewGroup p2);
    }
}

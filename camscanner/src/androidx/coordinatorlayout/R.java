// 
// Decompiled by Procyon v0.6.0
// 

package androidx.coordinatorlayout;

public final class R
{
    private R() {
    }
    
    public static final class attr
    {
        public static final int alpha = 2130968667;
        public static final int coordinatorLayoutStyle = 2130969011;
        public static final int font = 2130969287;
        public static final int fontProviderAuthority = 2130969289;
        public static final int fontProviderCerts = 2130969290;
        public static final int fontProviderFetchStrategy = 2130969291;
        public static final int fontProviderFetchTimeout = 2130969292;
        public static final int fontProviderPackage = 2130969293;
        public static final int fontProviderQuery = 2130969294;
        public static final int fontStyle = 2130969296;
        public static final int fontVariationSettings = 2130969297;
        public static final int fontWeight = 2130969298;
        public static final int keylines = 2130969453;
        public static final int layout_anchor = 2130969465;
        public static final int layout_anchorGravity = 2130969466;
        public static final int layout_behavior = 2130969467;
        public static final int layout_dodgeInsetEdges = 2130969516;
        public static final int layout_insetEdge = 2130969529;
        public static final int layout_keyline = 2130969530;
        public static final int statusBarBackground = 2130970010;
        public static final int ttcIndex = 2130970263;
        
        private attr() {
        }
    }
    
    public static final class color
    {
        public static final int notification_action_color_filter = 2131101184;
        public static final int notification_icon_bg_color = 2131101185;
        public static final int ripple_material_light = 2131101241;
        public static final int secondary_text_default_material_light = 2131101244;
        
        private color() {
        }
    }
    
    public static final class dimen
    {
        public static final int compat_button_inset_horizontal_material = 2131165438;
        public static final int compat_button_inset_vertical_material = 2131165439;
        public static final int compat_button_padding_horizontal_material = 2131165440;
        public static final int compat_button_padding_vertical_material = 2131165441;
        public static final int compat_control_corner_material = 2131165442;
        public static final int compat_notification_large_icon_max_height = 2131165443;
        public static final int compat_notification_large_icon_max_width = 2131165444;
        public static final int notification_action_icon_size = 2131166428;
        public static final int notification_action_text_size = 2131166429;
        public static final int notification_big_circle_margin = 2131166430;
        public static final int notification_content_margin_start = 2131166431;
        public static final int notification_large_icon_height = 2131166432;
        public static final int notification_large_icon_width = 2131166433;
        public static final int notification_main_column_padding_top = 2131166434;
        public static final int notification_media_narrow_margin = 2131166435;
        public static final int notification_right_icon_size = 2131166436;
        public static final int notification_right_side_padding_top = 2131166437;
        public static final int notification_small_icon_background_padding = 2131166438;
        public static final int notification_small_icon_size_as_large = 2131166439;
        public static final int notification_subtext_size = 2131166440;
        public static final int notification_top_pad = 2131166441;
        public static final int notification_top_pad_large_text = 2131166442;
        
        private dimen() {
        }
    }
    
    public static final class drawable
    {
        public static final int notification_action_background = 2131234818;
        public static final int notification_bg = 2131234819;
        public static final int notification_bg_low = 2131234820;
        public static final int notification_bg_low_normal = 2131234821;
        public static final int notification_bg_low_pressed = 2131234822;
        public static final int notification_bg_normal = 2131234823;
        public static final int notification_bg_normal_pressed = 2131234824;
        public static final int notification_icon_background = 2131234825;
        public static final int notification_template_icon_bg = 2131234827;
        public static final int notification_template_icon_low_bg = 2131234828;
        public static final int notification_tile_bg = 2131234829;
        public static final int notify_panel_notification_icon_bg = 2131234830;
        
        private drawable() {
        }
    }
    
    public static final class id
    {
        public static final int accessibility_action_clickable_span = 2131361821;
        public static final int accessibility_custom_action_0 = 2131361822;
        public static final int accessibility_custom_action_1 = 2131361823;
        public static final int accessibility_custom_action_10 = 2131361824;
        public static final int accessibility_custom_action_11 = 2131361825;
        public static final int accessibility_custom_action_12 = 2131361826;
        public static final int accessibility_custom_action_13 = 2131361827;
        public static final int accessibility_custom_action_14 = 2131361828;
        public static final int accessibility_custom_action_15 = 2131361829;
        public static final int accessibility_custom_action_16 = 2131361830;
        public static final int accessibility_custom_action_17 = 2131361831;
        public static final int accessibility_custom_action_18 = 2131361832;
        public static final int accessibility_custom_action_19 = 2131361833;
        public static final int accessibility_custom_action_2 = 2131361834;
        public static final int accessibility_custom_action_20 = 2131361835;
        public static final int accessibility_custom_action_21 = 2131361836;
        public static final int accessibility_custom_action_22 = 2131361837;
        public static final int accessibility_custom_action_23 = 2131361838;
        public static final int accessibility_custom_action_24 = 2131361839;
        public static final int accessibility_custom_action_25 = 2131361840;
        public static final int accessibility_custom_action_26 = 2131361841;
        public static final int accessibility_custom_action_27 = 2131361842;
        public static final int accessibility_custom_action_28 = 2131361843;
        public static final int accessibility_custom_action_29 = 2131361844;
        public static final int accessibility_custom_action_3 = 2131361845;
        public static final int accessibility_custom_action_30 = 2131361846;
        public static final int accessibility_custom_action_31 = 2131361847;
        public static final int accessibility_custom_action_4 = 2131361848;
        public static final int accessibility_custom_action_5 = 2131361849;
        public static final int accessibility_custom_action_6 = 2131361850;
        public static final int accessibility_custom_action_7 = 2131361851;
        public static final int accessibility_custom_action_8 = 2131361852;
        public static final int accessibility_custom_action_9 = 2131361853;
        public static final int action_container = 2131361894;
        public static final int action_divider = 2131361896;
        public static final int action_image = 2131361897;
        public static final int action_text = 2131361906;
        public static final int actions = 2131361907;
        public static final int async = 2131362147;
        public static final int blocking = 2131362267;
        public static final int bottom = 2131362274;
        public static final int chronometer = 2131362661;
        public static final int dialog_button = 2131363096;
        public static final int end = 2131363201;
        public static final int forever = 2131363435;
        public static final int icon = 2131363615;
        public static final int icon_group = 2131363617;
        public static final int info = 2131363725;
        public static final int italic = 2131363757;
        public static final int left = 2131364671;
        public static final int line1 = 2131364679;
        public static final int line3 = 2131364682;
        public static final int none = 2131365382;
        public static final int normal = 2131365383;
        public static final int notification_background = 2131365392;
        public static final int notification_main_column = 2131365393;
        public static final int notification_main_column_container = 2131365394;
        public static final int right = 2131365711;
        public static final int right_icon = 2131365713;
        public static final int right_side = 2131365715;
        public static final int start = 2131366175;
        public static final int tag_accessibility_actions = 2131366272;
        public static final int tag_accessibility_clickable_spans = 2131366273;
        public static final int tag_accessibility_heading = 2131366274;
        public static final int tag_accessibility_pane_title = 2131366275;
        public static final int tag_screen_reader_focusable = 2131366287;
        public static final int tag_transition_group = 2131366291;
        public static final int tag_unhandled_key_event_manager = 2131366292;
        public static final int tag_unhandled_key_listeners = 2131366293;
        public static final int text = 2131366301;
        public static final int text2 = 2131366303;
        public static final int time = 2131366344;
        public static final int title = 2131366353;
        public static final int top = 2131366382;
        
        private id() {
        }
    }
    
    public static final class integer
    {
        public static final int status_bar_notification_info_maxnum = 2131427400;
        
        private integer() {
        }
    }
    
    public static final class layout
    {
        public static final int custom_dialog = 2131558744;
        public static final int notification_action = 2131559950;
        public static final int notification_action_tombstone = 2131559951;
        public static final int notification_template_custom_big = 2131559958;
        public static final int notification_template_icon_group = 2131559959;
        public static final int notification_template_part_chronometer = 2131559963;
        public static final int notification_template_part_time = 2131559964;
        
        private layout() {
        }
    }
    
    public static final class string
    {
        public static final int status_bar_notification_info_overflow = 2131959498;
        
        private string() {
        }
    }
    
    public static final class style
    {
        public static final int TextAppearance_Compat_Notification = 2132017790;
        public static final int TextAppearance_Compat_Notification_Info = 2132017791;
        public static final int TextAppearance_Compat_Notification_Line2 = 2132017793;
        public static final int TextAppearance_Compat_Notification_Time = 2132017796;
        public static final int TextAppearance_Compat_Notification_Title = 2132017798;
        public static final int Widget_Compat_NotificationActionContainer = 2132018169;
        public static final int Widget_Compat_NotificationActionText = 2132018170;
        public static final int Widget_Support_CoordinatorLayout = 2132018469;
        
        private style() {
        }
    }
    
    public static final class styleable
    {
        public static final int[] ColorStateListItem;
        public static final int ColorStateListItem_alpha = 3;
        public static final int ColorStateListItem_android_alpha = 1;
        public static final int ColorStateListItem_android_color = 0;
        public static final int ColorStateListItem_android_lStar = 2;
        public static final int ColorStateListItem_lStar = 4;
        public static final int[] CoordinatorLayout;
        public static final int[] CoordinatorLayout_Layout;
        public static final int CoordinatorLayout_Layout_android_layout_gravity = 0;
        public static final int CoordinatorLayout_Layout_layout_anchor = 1;
        public static final int CoordinatorLayout_Layout_layout_anchorGravity = 2;
        public static final int CoordinatorLayout_Layout_layout_behavior = 3;
        public static final int CoordinatorLayout_Layout_layout_dodgeInsetEdges = 4;
        public static final int CoordinatorLayout_Layout_layout_insetEdge = 5;
        public static final int CoordinatorLayout_Layout_layout_keyline = 6;
        public static final int CoordinatorLayout_keylines = 0;
        public static final int CoordinatorLayout_statusBarBackground = 1;
        public static final int[] FontFamily;
        public static final int[] FontFamilyFont;
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_fontVariationSettings = 4;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_android_ttcIndex = 3;
        public static final int FontFamilyFont_font = 5;
        public static final int FontFamilyFont_fontStyle = 6;
        public static final int FontFamilyFont_fontVariationSettings = 7;
        public static final int FontFamilyFont_fontWeight = 8;
        public static final int FontFamilyFont_ttcIndex = 9;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
        public static final int FontFamily_fontProviderSystemFontFamily = 6;
        public static final int[] GradientColor;
        public static final int[] GradientColorItem;
        public static final int GradientColorItem_android_color = 0;
        public static final int GradientColorItem_android_offset = 1;
        public static final int GradientColor_android_centerColor = 7;
        public static final int GradientColor_android_centerX = 3;
        public static final int GradientColor_android_centerY = 4;
        public static final int GradientColor_android_endColor = 1;
        public static final int GradientColor_android_endX = 10;
        public static final int GradientColor_android_endY = 11;
        public static final int GradientColor_android_gradientRadius = 5;
        public static final int GradientColor_android_startColor = 0;
        public static final int GradientColor_android_startX = 8;
        public static final int GradientColor_android_startY = 9;
        public static final int GradientColor_android_tileMode = 6;
        public static final int GradientColor_android_type = 2;
        
        static {
            ColorStateListItem = new int[] { 16843173, 16843551, 16844359, 2130968667, 2130969454 };
            CoordinatorLayout = new int[] { 2130969453, 2130970010 };
            CoordinatorLayout_Layout = new int[] { 16842931, 2130969465, 2130969466, 2130969467, 2130969516, 2130969529, 2130969530 };
            FontFamily = new int[] { 2130969289, 2130969290, 2130969291, 2130969292, 2130969293, 2130969294, 2130969295 };
            FontFamilyFont = new int[] { 16844082, 16844083, 16844095, 16844143, 16844144, 2130969287, 2130969296, 2130969297, 2130969298, 2130970263 };
            GradientColor = new int[] { 16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051 };
            GradientColorItem = new int[] { 16843173, 16844052 };
        }
        
        private styleable() {
        }
    }
}

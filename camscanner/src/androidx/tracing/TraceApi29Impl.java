// 
// Decompiled by Procyon v0.6.0
// 

package androidx.tracing;

import androidx.core.os.\u30070\u3007O0088o;
import androidx.core.os.\u3007O888o0o;
import androidx.core.os.o800o8O;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(29)
final class TraceApi29Impl
{
    private TraceApi29Impl() {
    }
    
    public static void beginAsyncSection(@NonNull final String s, final int n) {
        o800o8O.\u3007080(s, n);
    }
    
    public static void endAsyncSection(@NonNull final String s, final int n) {
        \u3007O888o0o.\u3007080(s, n);
    }
    
    public static void setCounter(@NonNull final String s, final int n) {
        \u30070\u3007O0088o.\u3007080(s, (long)n);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.tracing;

import android.os.Trace;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

@RequiresApi(18)
final class TraceApi18Impl
{
    private TraceApi18Impl() {
    }
    
    public static void beginSection(@NonNull final String s) {
        Trace.beginSection(s);
    }
    
    public static void endSection() {
        Trace.endSection();
    }
}

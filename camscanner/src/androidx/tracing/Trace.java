// 
// Decompiled by Procyon v0.6.0
// 

package androidx.tracing;

import androidx.core.os.OoO8;
import java.lang.reflect.InvocationTargetException;
import android.annotation.SuppressLint;
import androidx.annotation.NonNull;
import java.lang.reflect.Method;

public final class Trace
{
    static final String TAG = "Trace";
    private static Method sAsyncTraceBeginMethod;
    private static Method sAsyncTraceEndMethod;
    private static Method sIsTagEnabledMethod;
    private static Method sTraceCounterMethod;
    private static long sTraceTagApp;
    
    private Trace() {
    }
    
    @SuppressLint({ "NewApi" })
    public static void beginAsyncSection(@NonNull final String s, final int n) {
        while (true) {
            try {
                if (Trace.sAsyncTraceBeginMethod == null) {
                    TraceApi29Impl.beginAsyncSection(s, n);
                    return;
                }
                beginAsyncSectionFallback(s, n);
            }
            catch (final NoSuchMethodError | NoClassDefFoundError noSuchMethodError | NoClassDefFoundError) {
                continue;
            }
            break;
        }
    }
    
    private static void beginAsyncSectionFallback(@NonNull final String s, final int i) {
        try {
            if (Trace.sAsyncTraceBeginMethod == null) {
                Trace.sAsyncTraceBeginMethod = android.os.Trace.class.getMethod("asyncTraceBegin", Long.TYPE, String.class, Integer.TYPE);
            }
            Trace.sAsyncTraceBeginMethod.invoke(null, Trace.sTraceTagApp, s, i);
        }
        catch (final Exception ex) {
            handleException("asyncTraceBegin", ex);
        }
    }
    
    public static void beginSection(@NonNull final String s) {
        TraceApi18Impl.beginSection(s);
    }
    
    @SuppressLint({ "NewApi" })
    public static void endAsyncSection(@NonNull final String s, final int n) {
        while (true) {
            try {
                if (Trace.sAsyncTraceEndMethod == null) {
                    TraceApi29Impl.endAsyncSection(s, n);
                    return;
                }
                endAsyncSectionFallback(s, n);
            }
            catch (final NoSuchMethodError | NoClassDefFoundError noSuchMethodError | NoClassDefFoundError) {
                continue;
            }
            break;
        }
    }
    
    private static void endAsyncSectionFallback(@NonNull final String s, final int i) {
        try {
            if (Trace.sAsyncTraceEndMethod == null) {
                Trace.sAsyncTraceEndMethod = android.os.Trace.class.getMethod("asyncTraceEnd", Long.TYPE, String.class, Integer.TYPE);
            }
            Trace.sAsyncTraceEndMethod.invoke(null, Trace.sTraceTagApp, s, i);
        }
        catch (final Exception ex) {
            handleException("asyncTraceEnd", ex);
        }
    }
    
    public static void endSection() {
        TraceApi18Impl.endSection();
    }
    
    private static void handleException(@NonNull final String str, @NonNull final Exception ex) {
        if (!(ex instanceof InvocationTargetException)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unable to call ");
            sb.append(str);
            sb.append(" via reflection");
            return;
        }
        final Throwable cause = ex.getCause();
        if (cause instanceof RuntimeException) {
            throw (RuntimeException)cause;
        }
        throw new RuntimeException(cause);
    }
    
    @SuppressLint({ "NewApi" })
    public static boolean isEnabled() {
        try {
            if (Trace.sIsTagEnabledMethod == null) {
                return OoO8.\u3007080();
            }
            return isEnabledFallback();
        }
        catch (final NoSuchMethodError | NoClassDefFoundError noSuchMethodError | NoClassDefFoundError) {
            return isEnabledFallback();
        }
    }
    
    private static boolean isEnabledFallback() {
        try {
            if (Trace.sIsTagEnabledMethod == null) {
                Trace.sTraceTagApp = android.os.Trace.class.getField("TRACE_TAG_APP").getLong(null);
                Trace.sIsTagEnabledMethod = android.os.Trace.class.getMethod("isTagEnabled", Long.TYPE);
            }
            return (boolean)Trace.sIsTagEnabledMethod.invoke(null, Trace.sTraceTagApp);
        }
        catch (final Exception ex) {
            handleException("isTagEnabled", ex);
            return false;
        }
    }
    
    @SuppressLint({ "NewApi" })
    public static void setCounter(@NonNull final String s, final int n) {
        while (true) {
            try {
                if (Trace.sTraceCounterMethod == null) {
                    TraceApi29Impl.setCounter(s, n);
                    return;
                }
                setCounterFallback(s, n);
            }
            catch (final NoSuchMethodError | NoClassDefFoundError noSuchMethodError | NoClassDefFoundError) {
                continue;
            }
            break;
        }
    }
    
    private static void setCounterFallback(@NonNull final String s, final int i) {
        try {
            if (Trace.sTraceCounterMethod == null) {
                Trace.sTraceCounterMethod = android.os.Trace.class.getMethod("traceCounter", Long.TYPE, String.class, Integer.TYPE);
            }
            Trace.sTraceCounterMethod.invoke(null, Trace.sTraceTagApp, s, i);
        }
        catch (final Exception ex) {
            handleException("traceCounter", ex);
        }
    }
}

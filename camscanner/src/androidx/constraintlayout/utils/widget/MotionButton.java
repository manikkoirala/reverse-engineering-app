// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.utils.widget;

import androidx.annotation.RequiresApi;
import android.graphics.Path$Direction;
import android.graphics.Outline;
import android.view.View;
import android.graphics.Canvas;
import android.content.res.TypedArray;
import androidx.constraintlayout.widget.R;
import android.util.AttributeSet;
import android.content.Context;
import android.view.ViewOutlineProvider;
import android.graphics.RectF;
import android.graphics.Path;
import androidx.appcompat.widget.AppCompatButton;

public class MotionButton extends AppCompatButton
{
    private Path mPath;
    RectF mRect;
    private float mRound;
    private float mRoundPercent;
    ViewOutlineProvider mViewOutlineProvider;
    
    public MotionButton(final Context context) {
        super(context);
        this.mRoundPercent = 0.0f;
        this.mRound = Float.NaN;
        this.init(context, null);
    }
    
    public MotionButton(final Context context, final AttributeSet set) {
        super(context, set);
        this.mRoundPercent = 0.0f;
        this.mRound = Float.NaN;
        this.init(context, set);
    }
    
    public MotionButton(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mRoundPercent = 0.0f;
        this.mRound = Float.NaN;
        this.init(context, set);
    }
    
    private void init(final Context context, final AttributeSet set) {
        int i = 0;
        ((View)this).setPadding(0, 0, 0, 0);
        if (set != null) {
            TypedArray obtainStyledAttributes;
            for (obtainStyledAttributes = ((View)this).getContext().obtainStyledAttributes(set, R.styleable.ImageFilterView); i < obtainStyledAttributes.getIndexCount(); ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == R.styleable.ImageFilterView_round) {
                    this.setRound(obtainStyledAttributes.getDimension(index, 0.0f));
                }
                else if (index == R.styleable.ImageFilterView_roundPercent) {
                    this.setRoundPercent(obtainStyledAttributes.getFloat(index, 0.0f));
                }
            }
            obtainStyledAttributes.recycle();
        }
    }
    
    public void draw(final Canvas canvas) {
        super.draw(canvas);
    }
    
    public float getRound() {
        return this.mRound;
    }
    
    public float getRoundPercent() {
        return this.mRoundPercent;
    }
    
    @RequiresApi(21)
    public void setRound(float n) {
        if (Float.isNaN(n)) {
            this.mRound = n;
            n = this.mRoundPercent;
            this.mRoundPercent = -1.0f;
            this.setRoundPercent(n);
            return;
        }
        final boolean b = this.mRound != n;
        this.mRound = n;
        if (n != 0.0f) {
            if (this.mPath == null) {
                this.mPath = new Path();
            }
            if (this.mRect == null) {
                this.mRect = new RectF();
            }
            if (this.mViewOutlineProvider == null) {
                ((View)this).setOutlineProvider(this.mViewOutlineProvider = new ViewOutlineProvider(this) {
                    final MotionButton this$0;
                    
                    public void getOutline(final View view, final Outline outline) {
                        outline.setRoundRect(0, 0, ((View)this.this$0).getWidth(), ((View)this.this$0).getHeight(), this.this$0.mRound);
                    }
                });
            }
            ((View)this).setClipToOutline(true);
            this.mRect.set(0.0f, 0.0f, (float)((View)this).getWidth(), (float)((View)this).getHeight());
            this.mPath.reset();
            final Path mPath = this.mPath;
            final RectF mRect = this.mRect;
            n = this.mRound;
            mPath.addRoundRect(mRect, n, n, Path$Direction.CW);
        }
        else {
            ((View)this).setClipToOutline(false);
        }
        if (b) {
            ((View)this).invalidateOutline();
        }
    }
    
    @RequiresApi(21)
    public void setRoundPercent(float mRoundPercent) {
        final boolean b = this.mRoundPercent != mRoundPercent;
        this.mRoundPercent = mRoundPercent;
        if (mRoundPercent != 0.0f) {
            if (this.mPath == null) {
                this.mPath = new Path();
            }
            if (this.mRect == null) {
                this.mRect = new RectF();
            }
            if (this.mViewOutlineProvider == null) {
                ((View)this).setOutlineProvider(this.mViewOutlineProvider = new ViewOutlineProvider(this) {
                    final MotionButton this$0;
                    
                    public void getOutline(final View view, final Outline outline) {
                        final int width = ((View)this.this$0).getWidth();
                        final int height = ((View)this.this$0).getHeight();
                        outline.setRoundRect(0, 0, width, height, Math.min(width, height) * this.this$0.mRoundPercent / 2.0f);
                    }
                });
            }
            ((View)this).setClipToOutline(true);
            final int width = ((View)this).getWidth();
            final int height = ((View)this).getHeight();
            mRoundPercent = Math.min(width, height) * this.mRoundPercent / 2.0f;
            this.mRect.set(0.0f, 0.0f, (float)width, (float)height);
            this.mPath.reset();
            this.mPath.addRoundRect(this.mRect, mRoundPercent, mRoundPercent, Path$Direction.CW);
        }
        else {
            ((View)this).setClipToOutline(false);
        }
        if (b) {
            ((View)this).invalidateOutline();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.state;

import androidx.constraintlayout.core.state.helpers.Facade;
import androidx.constraintlayout.core.widgets.ConstraintWidget;

public interface Reference
{
    void apply();
    
    ConstraintWidget getConstraintWidget();
    
    Facade getFacade();
    
    Object getKey();
    
    void setConstraintWidget(final ConstraintWidget p0);
    
    void setKey(final Object p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.state;

import androidx.constraintlayout.core.motion.key.MotionKeyPosition;
import androidx.constraintlayout.core.motion.key.MotionKeyCycle;
import androidx.constraintlayout.core.motion.key.MotionKey;
import androidx.constraintlayout.core.motion.utils.TypedValues;
import androidx.constraintlayout.core.motion.key.MotionKeyAttributes;
import androidx.constraintlayout.core.motion.utils.KeyCache;
import androidx.constraintlayout.core.motion.MotionWidget;
import java.util.ArrayList;
import androidx.constraintlayout.core.widgets.ConstraintWidgetContainer;
import java.util.Iterator;
import androidx.constraintlayout.core.motion.Motion;
import androidx.constraintlayout.core.motion.utils.TypedBundle;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import java.util.HashMap;

public class Transition
{
    public static final int END = 1;
    public static final int INTERPOLATED = 2;
    public static final int START = 0;
    HashMap<Integer, HashMap<String, KeyPosition>> keyPositions;
    private int pathMotionArc;
    HashMap<String, WidgetState> state;
    
    public Transition() {
        this.state = new HashMap<String, WidgetState>();
        this.keyPositions = new HashMap<Integer, HashMap<String, KeyPosition>>();
        this.pathMotionArc = -1;
    }
    
    private WidgetState getWidgetState(final String s, final ConstraintWidget constraintWidget, final int n) {
        WidgetState widgetState;
        if ((widgetState = this.state.get(s)) == null) {
            final WidgetState value = new WidgetState();
            final int pathMotionArc = this.pathMotionArc;
            if (pathMotionArc != -1) {
                value.motionControl.setPathMotionArc(pathMotionArc);
            }
            this.state.put(s, value);
            widgetState = value;
            if (constraintWidget != null) {
                value.update(constraintWidget, n);
                widgetState = value;
            }
        }
        return widgetState;
    }
    
    public void addCustomColor(final int n, final String s, final String s2, final int n2) {
        this.getWidgetState(s, null, n).getFrame(n).addCustomColor(s2, n2);
    }
    
    public void addCustomFloat(final int n, final String s, final String s2, final float n2) {
        this.getWidgetState(s, null, n).getFrame(n).addCustomFloat(s2, n2);
    }
    
    public void addKeyAttribute(final String s, final TypedBundle keyAttribute) {
        this.getWidgetState(s, null, 0).setKeyAttribute(keyAttribute);
    }
    
    public void addKeyCycle(final String s, final TypedBundle keyCycle) {
        this.getWidgetState(s, null, 0).setKeyCycle(keyCycle);
    }
    
    public void addKeyPosition(final String key, final int n, final int n2, final float n3, final float n4) {
        final TypedBundle keyPosition = new TypedBundle();
        keyPosition.add(510, 2);
        keyPosition.add(100, n);
        keyPosition.add(506, n3);
        keyPosition.add(507, n4);
        this.getWidgetState(key, null, 0).setKeyPosition(keyPosition);
        final KeyPosition value = new KeyPosition(key, n, n2, n3, n4);
        HashMap value2;
        if ((value2 = this.keyPositions.get(n)) == null) {
            value2 = new HashMap();
            this.keyPositions.put(n, value2);
        }
        value2.put(key, value);
    }
    
    public void addKeyPosition(final String s, final TypedBundle keyPosition) {
        this.getWidgetState(s, null, 0).setKeyPosition(keyPosition);
    }
    
    public void clear() {
        this.state.clear();
    }
    
    public boolean contains(final String key) {
        return this.state.containsKey(key);
    }
    
    public void fillKeyPositions(final WidgetFrame widgetFrame, final float[] array, final float[] array2, final float[] array3) {
        int i = 0;
        int n = 0;
        while (i <= 100) {
            final HashMap hashMap = this.keyPositions.get(i);
            int n2 = n;
            if (hashMap != null) {
                final KeyPosition keyPosition = (KeyPosition)hashMap.get(widgetFrame.widget.stringId);
                n2 = n;
                if (keyPosition != null) {
                    array[n] = keyPosition.x;
                    array2[n] = keyPosition.y;
                    array3[n] = (float)keyPosition.frame;
                    n2 = n + 1;
                }
            }
            ++i;
            n = n2;
        }
    }
    
    public KeyPosition findNextPosition(final String key, int i) {
        while (i <= 100) {
            final HashMap hashMap = this.keyPositions.get(i);
            if (hashMap != null) {
                final KeyPosition keyPosition = (KeyPosition)hashMap.get(key);
                if (keyPosition != null) {
                    return keyPosition;
                }
            }
            ++i;
        }
        return null;
    }
    
    public KeyPosition findPreviousPosition(final String key, int i) {
        while (i >= 0) {
            final HashMap hashMap = this.keyPositions.get(i);
            if (hashMap != null) {
                final KeyPosition keyPosition = (KeyPosition)hashMap.get(key);
                if (keyPosition != null) {
                    return keyPosition;
                }
            }
            --i;
        }
        return null;
    }
    
    public WidgetFrame getEnd(final ConstraintWidget constraintWidget) {
        return this.getWidgetState(constraintWidget.stringId, null, 1).end;
    }
    
    public WidgetFrame getEnd(final String key) {
        final WidgetState widgetState = this.state.get(key);
        if (widgetState == null) {
            return null;
        }
        return widgetState.end;
    }
    
    public WidgetFrame getInterpolated(final ConstraintWidget constraintWidget) {
        return this.getWidgetState(constraintWidget.stringId, null, 2).interpolated;
    }
    
    public WidgetFrame getInterpolated(final String key) {
        final WidgetState widgetState = this.state.get(key);
        if (widgetState == null) {
            return null;
        }
        return widgetState.interpolated;
    }
    
    public int getKeyFrames(final String key, final float[] array, final int[] array2, final int[] array3) {
        return this.state.get(key).motionControl.buildKeyFrames(array, array2, array3);
    }
    
    public Motion getMotion(final String s) {
        return this.getWidgetState(s, null, 0).motionControl;
    }
    
    public int getNumberKeyPositions(final WidgetFrame widgetFrame) {
        int i = 0;
        int n = 0;
        while (i <= 100) {
            final HashMap hashMap = this.keyPositions.get(i);
            int n2 = n;
            if (hashMap != null) {
                n2 = n;
                if (hashMap.get(widgetFrame.widget.stringId) != null) {
                    n2 = n + 1;
                }
            }
            ++i;
            n = n2;
        }
        return n;
    }
    
    public float[] getPath(final String key) {
        final WidgetState widgetState = this.state.get(key);
        final float[] array = new float[124];
        widgetState.motionControl.buildPath(array, 62);
        return array;
    }
    
    public WidgetFrame getStart(final ConstraintWidget constraintWidget) {
        return this.getWidgetState(constraintWidget.stringId, null, 0).start;
    }
    
    public WidgetFrame getStart(final String key) {
        final WidgetState widgetState = this.state.get(key);
        if (widgetState == null) {
            return null;
        }
        return widgetState.start;
    }
    
    public boolean hasPositionKeyframes() {
        return this.keyPositions.size() > 0;
    }
    
    public void interpolate(final int n, final int n2, final float n3) {
        final Iterator<String> iterator = this.state.keySet().iterator();
        while (iterator.hasNext()) {
            this.state.get(iterator.next()).interpolate(n, n2, n3, this);
        }
    }
    
    public boolean isEmpty() {
        return this.state.isEmpty();
    }
    
    public void setTransitionProperties(final TypedBundle typedBundle) {
        this.pathMotionArc = typedBundle.getInteger(509);
    }
    
    public void updateFrom(final ConstraintWidgetContainer constraintWidgetContainer, final int n) {
        final ArrayList<ConstraintWidget> children = constraintWidgetContainer.getChildren();
        for (int size = children.size(), i = 0; i < size; ++i) {
            final ConstraintWidget constraintWidget = children.get(i);
            this.getWidgetState(constraintWidget.stringId, null, n).update(constraintWidget, n);
        }
    }
    
    static class KeyPosition
    {
        int frame;
        String target;
        int type;
        float x;
        float y;
        
        public KeyPosition(final String target, final int frame, final int type, final float x, final float y) {
            this.target = target;
            this.frame = frame;
            this.type = type;
            this.x = x;
            this.y = y;
        }
    }
    
    static class WidgetState
    {
        WidgetFrame end;
        WidgetFrame interpolated;
        Motion motionControl;
        MotionWidget motionWidgetEnd;
        MotionWidget motionWidgetInterpolated;
        MotionWidget motionWidgetStart;
        KeyCache myKeyCache;
        int myParentHeight;
        int myParentWidth;
        WidgetFrame start;
        
        public WidgetState() {
            this.myKeyCache = new KeyCache();
            this.myParentHeight = -1;
            this.myParentWidth = -1;
            this.start = new WidgetFrame();
            this.end = new WidgetFrame();
            this.interpolated = new WidgetFrame();
            this.motionWidgetStart = new MotionWidget(this.start);
            this.motionWidgetEnd = new MotionWidget(this.end);
            this.motionWidgetInterpolated = new MotionWidget(this.interpolated);
            (this.motionControl = new Motion(this.motionWidgetStart)).setStart(this.motionWidgetStart);
            this.motionControl.setEnd(this.motionWidgetEnd);
        }
        
        public WidgetFrame getFrame(final int n) {
            if (n == 0) {
                return this.start;
            }
            if (n == 1) {
                return this.end;
            }
            return this.interpolated;
        }
        
        public void interpolate(final int myParentWidth, final int myParentHeight, final float interpolatedPos, final Transition transition) {
            this.myParentHeight = myParentHeight;
            this.myParentWidth = myParentWidth;
            this.motionControl.setup(myParentWidth, myParentHeight, 1.0f, System.nanoTime());
            WidgetFrame.interpolate(myParentWidth, myParentHeight, this.interpolated, this.start, this.end, transition, interpolatedPos);
            this.interpolated.interpolatedPos = interpolatedPos;
            this.motionControl.interpolate(this.motionWidgetInterpolated, interpolatedPos, System.nanoTime(), this.myKeyCache);
        }
        
        public void setKeyAttribute(final TypedBundle typedBundle) {
            final MotionKeyAttributes motionKeyAttributes = new MotionKeyAttributes();
            typedBundle.applyDelta(motionKeyAttributes);
            this.motionControl.addKey(motionKeyAttributes);
        }
        
        public void setKeyCycle(final TypedBundle typedBundle) {
            final MotionKeyCycle motionKeyCycle = new MotionKeyCycle();
            typedBundle.applyDelta(motionKeyCycle);
            this.motionControl.addKey(motionKeyCycle);
        }
        
        public void setKeyPosition(final TypedBundle typedBundle) {
            final MotionKeyPosition motionKeyPosition = new MotionKeyPosition();
            typedBundle.applyDelta(motionKeyPosition);
            this.motionControl.addKey(motionKeyPosition);
        }
        
        public void update(final ConstraintWidget constraintWidget, final int n) {
            if (n == 0) {
                this.start.update(constraintWidget);
                this.motionControl.setStart(this.motionWidgetStart);
            }
            else if (n == 1) {
                this.end.update(constraintWidget);
                this.motionControl.setEnd(this.motionWidgetEnd);
            }
            this.myParentWidth = -1;
        }
    }
}

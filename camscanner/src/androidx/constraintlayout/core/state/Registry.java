// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.state;

import java.util.Set;
import java.util.HashMap;

public class Registry
{
    private static final Registry sRegistry;
    private HashMap<String, RegistryCallback> mCallbacks;
    
    static {
        sRegistry = new Registry();
    }
    
    public Registry() {
        this.mCallbacks = new HashMap<String, RegistryCallback>();
    }
    
    public static Registry getInstance() {
        return Registry.sRegistry;
    }
    
    public String currentContent(final String key) {
        final RegistryCallback registryCallback = this.mCallbacks.get(key);
        if (registryCallback != null) {
            return registryCallback.currentMotionScene();
        }
        return null;
    }
    
    public String currentLayoutInformation(final String key) {
        final RegistryCallback registryCallback = this.mCallbacks.get(key);
        if (registryCallback != null) {
            return registryCallback.currentLayoutInformation();
        }
        return null;
    }
    
    public long getLastModified(final String key) {
        final RegistryCallback registryCallback = this.mCallbacks.get(key);
        if (registryCallback != null) {
            return registryCallback.getLastModified();
        }
        return Long.MAX_VALUE;
    }
    
    public Set<String> getLayoutList() {
        return this.mCallbacks.keySet();
    }
    
    public void register(final String key, final RegistryCallback value) {
        this.mCallbacks.put(key, value);
    }
    
    public void setDrawDebug(final String key, final int drawDebug) {
        final RegistryCallback registryCallback = this.mCallbacks.get(key);
        if (registryCallback != null) {
            registryCallback.setDrawDebug(drawDebug);
        }
    }
    
    public void setLayoutInformationMode(final String key, final int layoutInformationMode) {
        final RegistryCallback registryCallback = this.mCallbacks.get(key);
        if (registryCallback != null) {
            registryCallback.setLayoutInformationMode(layoutInformationMode);
        }
    }
    
    public void unregister(final String key, final RegistryCallback registryCallback) {
        this.mCallbacks.remove(key);
    }
    
    public void updateContent(final String key, final String s) {
        final RegistryCallback registryCallback = this.mCallbacks.get(key);
        if (registryCallback != null) {
            registryCallback.onNewMotionScene(s);
        }
    }
    
    public void updateDimensions(final String key, final int n, final int n2) {
        final RegistryCallback registryCallback = this.mCallbacks.get(key);
        if (registryCallback != null) {
            registryCallback.onDimensions(n, n2);
        }
    }
    
    public void updateProgress(final String key, final float n) {
        final RegistryCallback registryCallback = this.mCallbacks.get(key);
        if (registryCallback != null) {
            registryCallback.onProgress(n);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.state.helpers;

import androidx.constraintlayout.core.state.ConstraintReference;
import java.util.Iterator;
import androidx.constraintlayout.core.state.State;

public class VerticalChainReference extends ChainReference
{
    public VerticalChainReference(final State state) {
        super(state, State.Helper.VERTICAL_CHAIN);
    }
    
    @Override
    public void apply() {
        final Iterator<Object> iterator = super.mReferences.iterator();
        while (iterator.hasNext()) {
            super.mState.constraints(iterator.next()).clearVertical();
        }
        final Iterator<Object> iterator2 = super.mReferences.iterator();
        ConstraintReference constraintReference = null;
        ConstraintReference constraintReference2 = null;
        while (iterator2.hasNext()) {
            final ConstraintReference constraints = super.mState.constraints(iterator2.next());
            ConstraintReference constraintReference3;
            if ((constraintReference3 = constraintReference2) == null) {
                final Object mTopToTop = super.mTopToTop;
                if (mTopToTop != null) {
                    constraints.topToTop(mTopToTop);
                }
                else {
                    final Object mTopToBottom = super.mTopToBottom;
                    if (mTopToBottom != null) {
                        constraints.topToBottom(mTopToBottom);
                    }
                    else {
                        constraints.topToTop(State.PARENT);
                    }
                }
                constraintReference3 = constraints;
            }
            if (constraintReference != null) {
                constraintReference.bottomToTop(constraints.getKey());
                constraints.topToBottom(constraintReference.getKey());
            }
            constraintReference = constraints;
            constraintReference2 = constraintReference3;
        }
        if (constraintReference != null) {
            final Object mBottomToTop = super.mBottomToTop;
            if (mBottomToTop != null) {
                constraintReference.bottomToTop(mBottomToTop);
            }
            else {
                final Object mBottomToBottom = super.mBottomToBottom;
                if (mBottomToBottom != null) {
                    constraintReference.bottomToBottom(mBottomToBottom);
                }
                else {
                    constraintReference.bottomToBottom(State.PARENT);
                }
            }
        }
        if (constraintReference2 == null) {
            return;
        }
        final float mBias = super.mBias;
        if (mBias != 0.5f) {
            constraintReference2.verticalBias(mBias);
        }
        final int n = VerticalChainReference$1.$SwitchMap$androidx$constraintlayout$core$state$State$Chain[super.mStyle.ordinal()];
        if (n != 1) {
            if (n != 2) {
                if (n == 3) {
                    constraintReference2.setVerticalChainStyle(2);
                }
            }
            else {
                constraintReference2.setVerticalChainStyle(1);
            }
        }
        else {
            constraintReference2.setVerticalChainStyle(0);
        }
    }
}

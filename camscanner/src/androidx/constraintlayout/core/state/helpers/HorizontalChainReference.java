// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.state.helpers;

import androidx.constraintlayout.core.state.ConstraintReference;
import java.util.Iterator;
import androidx.constraintlayout.core.state.State;

public class HorizontalChainReference extends ChainReference
{
    public HorizontalChainReference(final State state) {
        super(state, State.Helper.HORIZONTAL_CHAIN);
    }
    
    @Override
    public void apply() {
        final Iterator<Object> iterator = super.mReferences.iterator();
        while (iterator.hasNext()) {
            super.mState.constraints(iterator.next()).clearHorizontal();
        }
        final Iterator<Object> iterator2 = super.mReferences.iterator();
        ConstraintReference constraintReference = null;
        ConstraintReference constraintReference2 = null;
        while (iterator2.hasNext()) {
            final ConstraintReference constraints = super.mState.constraints(iterator2.next());
            ConstraintReference constraintReference3;
            if ((constraintReference3 = constraintReference2) == null) {
                final Object mStartToStart = super.mStartToStart;
                if (mStartToStart != null) {
                    constraints.startToStart(mStartToStart).margin(super.mMarginStart);
                }
                else {
                    final Object mStartToEnd = super.mStartToEnd;
                    if (mStartToEnd != null) {
                        constraints.startToEnd(mStartToEnd).margin(super.mMarginStart);
                    }
                    else {
                        constraints.startToStart(State.PARENT);
                    }
                }
                constraintReference3 = constraints;
            }
            if (constraintReference != null) {
                constraintReference.endToStart(constraints.getKey());
                constraints.startToEnd(constraintReference.getKey());
            }
            constraintReference = constraints;
            constraintReference2 = constraintReference3;
        }
        if (constraintReference != null) {
            final Object mEndToStart = super.mEndToStart;
            if (mEndToStart != null) {
                constraintReference.endToStart(mEndToStart).margin(super.mMarginEnd);
            }
            else {
                final Object mEndToEnd = super.mEndToEnd;
                if (mEndToEnd != null) {
                    constraintReference.endToEnd(mEndToEnd).margin(super.mMarginEnd);
                }
                else {
                    constraintReference.endToEnd(State.PARENT);
                }
            }
        }
        if (constraintReference2 == null) {
            return;
        }
        final float mBias = super.mBias;
        if (mBias != 0.5f) {
            constraintReference2.horizontalBias(mBias);
        }
        final int n = HorizontalChainReference$1.$SwitchMap$androidx$constraintlayout$core$state$State$Chain[super.mStyle.ordinal()];
        if (n != 1) {
            if (n != 2) {
                if (n == 3) {
                    constraintReference2.setHorizontalChainStyle(2);
                }
            }
            else {
                constraintReference2.setHorizontalChainStyle(1);
            }
        }
        else {
            constraintReference2.setHorizontalChainStyle(0);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.state.helpers;

import androidx.constraintlayout.core.state.ConstraintReference;
import java.util.Iterator;
import androidx.constraintlayout.core.state.State;
import androidx.constraintlayout.core.state.HelperReference;

public class AlignHorizontallyReference extends HelperReference
{
    private float mBias;
    
    public AlignHorizontallyReference(final State state) {
        super(state, State.Helper.ALIGN_VERTICALLY);
        this.mBias = 0.5f;
    }
    
    @Override
    public void apply() {
        final Iterator<Object> iterator = super.mReferences.iterator();
        while (iterator.hasNext()) {
            final ConstraintReference constraints = super.mState.constraints(iterator.next());
            constraints.clearHorizontal();
            final Object mStartToStart = super.mStartToStart;
            if (mStartToStart != null) {
                constraints.startToStart(mStartToStart);
            }
            else {
                final Object mStartToEnd = super.mStartToEnd;
                if (mStartToEnd != null) {
                    constraints.startToEnd(mStartToEnd);
                }
                else {
                    constraints.startToStart(State.PARENT);
                }
            }
            final Object mEndToStart = super.mEndToStart;
            if (mEndToStart != null) {
                constraints.endToStart(mEndToStart);
            }
            else {
                final Object mEndToEnd = super.mEndToEnd;
                if (mEndToEnd != null) {
                    constraints.endToEnd(mEndToEnd);
                }
                else {
                    constraints.endToEnd(State.PARENT);
                }
            }
            final float mBias = this.mBias;
            if (mBias != 0.5f) {
                constraints.horizontalBias(mBias);
            }
        }
    }
}

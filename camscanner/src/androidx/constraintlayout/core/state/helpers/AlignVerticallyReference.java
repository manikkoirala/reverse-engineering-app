// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.state.helpers;

import androidx.constraintlayout.core.state.ConstraintReference;
import java.util.Iterator;
import androidx.constraintlayout.core.state.State;
import androidx.constraintlayout.core.state.HelperReference;

public class AlignVerticallyReference extends HelperReference
{
    private float mBias;
    
    public AlignVerticallyReference(final State state) {
        super(state, State.Helper.ALIGN_VERTICALLY);
        this.mBias = 0.5f;
    }
    
    @Override
    public void apply() {
        final Iterator<Object> iterator = super.mReferences.iterator();
        while (iterator.hasNext()) {
            final ConstraintReference constraints = super.mState.constraints(iterator.next());
            constraints.clearVertical();
            final Object mTopToTop = super.mTopToTop;
            if (mTopToTop != null) {
                constraints.topToTop(mTopToTop);
            }
            else {
                final Object mTopToBottom = super.mTopToBottom;
                if (mTopToBottom != null) {
                    constraints.topToBottom(mTopToBottom);
                }
                else {
                    constraints.topToTop(State.PARENT);
                }
            }
            final Object mBottomToTop = super.mBottomToTop;
            if (mBottomToTop != null) {
                constraints.bottomToTop(mBottomToTop);
            }
            else {
                final Object mBottomToBottom = super.mBottomToBottom;
                if (mBottomToBottom != null) {
                    constraints.bottomToBottom(mBottomToBottom);
                }
                else {
                    constraints.bottomToBottom(State.PARENT);
                }
            }
            final float mBias = this.mBias;
            if (mBias != 0.5f) {
                constraints.verticalBias(mBias);
            }
        }
    }
}

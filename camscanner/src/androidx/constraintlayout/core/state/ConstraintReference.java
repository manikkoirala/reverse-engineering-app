// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.state;

import java.util.ArrayList;
import java.util.Iterator;
import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.state.helpers.Facade;
import java.util.HashMap;
import androidx.constraintlayout.core.widgets.ConstraintWidget;

public class ConstraintReference implements Reference
{
    private Object key;
    float mAlpha;
    Object mBaselineToBaseline;
    protected Object mBottomToBottom;
    protected Object mBottomToTop;
    private float mCircularAngle;
    Object mCircularConstraint;
    private float mCircularDistance;
    private ConstraintWidget mConstraintWidget;
    private HashMap<String, Integer> mCustomColors;
    private HashMap<String, Float> mCustomFloats;
    protected Object mEndToEnd;
    protected Object mEndToStart;
    Facade mFacade;
    float mHorizontalBias;
    int mHorizontalChainStyle;
    Dimension mHorizontalDimension;
    State.Constraint mLast;
    Object mLeftToLeft;
    Object mLeftToRight;
    int mMarginBottom;
    int mMarginBottomGone;
    protected int mMarginEnd;
    int mMarginEndGone;
    int mMarginLeft;
    int mMarginLeftGone;
    int mMarginRight;
    int mMarginRightGone;
    protected int mMarginStart;
    int mMarginStartGone;
    int mMarginTop;
    int mMarginTopGone;
    float mPivotX;
    float mPivotY;
    Object mRightToLeft;
    Object mRightToRight;
    float mRotationX;
    float mRotationY;
    float mRotationZ;
    float mScaleX;
    float mScaleY;
    protected Object mStartToEnd;
    protected Object mStartToStart;
    final State mState;
    String mTag;
    protected Object mTopToBottom;
    protected Object mTopToTop;
    float mTranslationX;
    float mTranslationY;
    float mTranslationZ;
    float mVerticalBias;
    int mVerticalChainStyle;
    Dimension mVerticalDimension;
    private Object mView;
    int mVisibility;
    
    public ConstraintReference(final State mState) {
        this.mTag = null;
        this.mFacade = null;
        this.mHorizontalChainStyle = 0;
        this.mVerticalChainStyle = 0;
        this.mHorizontalBias = 0.5f;
        this.mVerticalBias = 0.5f;
        this.mMarginLeft = 0;
        this.mMarginRight = 0;
        this.mMarginStart = 0;
        this.mMarginEnd = 0;
        this.mMarginTop = 0;
        this.mMarginBottom = 0;
        this.mMarginLeftGone = 0;
        this.mMarginRightGone = 0;
        this.mMarginStartGone = 0;
        this.mMarginEndGone = 0;
        this.mMarginTopGone = 0;
        this.mMarginBottomGone = 0;
        this.mPivotX = Float.NaN;
        this.mPivotY = Float.NaN;
        this.mRotationX = Float.NaN;
        this.mRotationY = Float.NaN;
        this.mRotationZ = Float.NaN;
        this.mTranslationX = Float.NaN;
        this.mTranslationY = Float.NaN;
        this.mTranslationZ = Float.NaN;
        this.mAlpha = Float.NaN;
        this.mScaleX = Float.NaN;
        this.mScaleY = Float.NaN;
        this.mVisibility = 0;
        this.mLeftToLeft = null;
        this.mLeftToRight = null;
        this.mRightToLeft = null;
        this.mRightToRight = null;
        this.mStartToStart = null;
        this.mStartToEnd = null;
        this.mEndToStart = null;
        this.mEndToEnd = null;
        this.mTopToTop = null;
        this.mTopToBottom = null;
        this.mBottomToTop = null;
        this.mBottomToBottom = null;
        this.mBaselineToBaseline = null;
        this.mCircularConstraint = null;
        this.mLast = null;
        final Object wrap_DIMENSION = Dimension.WRAP_DIMENSION;
        this.mHorizontalDimension = Dimension.Fixed(wrap_DIMENSION);
        this.mVerticalDimension = Dimension.Fixed(wrap_DIMENSION);
        this.mCustomColors = new HashMap<String, Integer>();
        this.mCustomFloats = new HashMap<String, Float>();
        this.mState = mState;
    }
    
    private void applyConnection(final ConstraintWidget constraintWidget, final Object o, final State.Constraint constraint) {
        final ConstraintWidget target = this.getTarget(o);
        if (target == null) {
            return;
        }
        final int[] $SwitchMap$androidx$constraintlayout$core$state$State$Constraint = ConstraintReference$1.$SwitchMap$androidx$constraintlayout$core$state$State$Constraint;
        final int n = $SwitchMap$androidx$constraintlayout$core$state$State$Constraint[constraint.ordinal()];
        switch ($SwitchMap$androidx$constraintlayout$core$state$State$Constraint[constraint.ordinal()]) {
            case 14: {
                constraintWidget.connectCircularConstraint(target, this.mCircularAngle, (int)this.mCircularDistance);
                break;
            }
            case 13: {
                final ConstraintAnchor.Type baseline = ConstraintAnchor.Type.BASELINE;
                constraintWidget.immediateConnect(baseline, target, baseline, 0, 0);
                break;
            }
            case 12: {
                final ConstraintAnchor.Type bottom = ConstraintAnchor.Type.BOTTOM;
                constraintWidget.getAnchor(bottom).connect(target.getAnchor(bottom), this.mMarginBottom, this.mMarginBottomGone, false);
                break;
            }
            case 11: {
                constraintWidget.getAnchor(ConstraintAnchor.Type.BOTTOM).connect(target.getAnchor(ConstraintAnchor.Type.TOP), this.mMarginBottom, this.mMarginBottomGone, false);
                break;
            }
            case 10: {
                constraintWidget.getAnchor(ConstraintAnchor.Type.TOP).connect(target.getAnchor(ConstraintAnchor.Type.BOTTOM), this.mMarginTop, this.mMarginTopGone, false);
                break;
            }
            case 9: {
                final ConstraintAnchor.Type top = ConstraintAnchor.Type.TOP;
                constraintWidget.getAnchor(top).connect(target.getAnchor(top), this.mMarginTop, this.mMarginTopGone, false);
                break;
            }
            case 8: {
                final ConstraintAnchor.Type right = ConstraintAnchor.Type.RIGHT;
                constraintWidget.getAnchor(right).connect(target.getAnchor(right), this.mMarginEnd, this.mMarginEndGone, false);
                break;
            }
            case 7: {
                constraintWidget.getAnchor(ConstraintAnchor.Type.RIGHT).connect(target.getAnchor(ConstraintAnchor.Type.LEFT), this.mMarginEnd, this.mMarginEndGone, false);
                break;
            }
            case 6: {
                constraintWidget.getAnchor(ConstraintAnchor.Type.LEFT).connect(target.getAnchor(ConstraintAnchor.Type.RIGHT), this.mMarginStart, this.mMarginStartGone, false);
                break;
            }
            case 5: {
                final ConstraintAnchor.Type left = ConstraintAnchor.Type.LEFT;
                constraintWidget.getAnchor(left).connect(target.getAnchor(left), this.mMarginStart, this.mMarginStartGone, false);
                break;
            }
            case 4: {
                final ConstraintAnchor.Type right2 = ConstraintAnchor.Type.RIGHT;
                constraintWidget.getAnchor(right2).connect(target.getAnchor(right2), this.mMarginRight, this.mMarginRightGone, false);
                break;
            }
            case 3: {
                constraintWidget.getAnchor(ConstraintAnchor.Type.RIGHT).connect(target.getAnchor(ConstraintAnchor.Type.LEFT), this.mMarginRight, this.mMarginRightGone, false);
                break;
            }
            case 2: {
                constraintWidget.getAnchor(ConstraintAnchor.Type.LEFT).connect(target.getAnchor(ConstraintAnchor.Type.RIGHT), this.mMarginLeft, this.mMarginLeftGone, false);
                break;
            }
            case 1: {
                final ConstraintAnchor.Type left2 = ConstraintAnchor.Type.LEFT;
                constraintWidget.getAnchor(left2).connect(target.getAnchor(left2), this.mMarginLeft, this.mMarginLeftGone, false);
                break;
            }
        }
    }
    
    private void dereference() {
        this.mLeftToLeft = this.get(this.mLeftToLeft);
        this.mLeftToRight = this.get(this.mLeftToRight);
        this.mRightToLeft = this.get(this.mRightToLeft);
        this.mRightToRight = this.get(this.mRightToRight);
        this.mStartToStart = this.get(this.mStartToStart);
        this.mStartToEnd = this.get(this.mStartToEnd);
        this.mEndToStart = this.get(this.mEndToStart);
        this.mEndToEnd = this.get(this.mEndToEnd);
        this.mTopToTop = this.get(this.mTopToTop);
        this.mTopToBottom = this.get(this.mTopToBottom);
        this.mBottomToTop = this.get(this.mBottomToTop);
        this.mBottomToBottom = this.get(this.mBottomToBottom);
        this.mBaselineToBaseline = this.get(this.mBaselineToBaseline);
    }
    
    private Object get(final Object o) {
        if (o == null) {
            return null;
        }
        Object reference = o;
        if (!(o instanceof ConstraintReference)) {
            reference = this.mState.reference(o);
        }
        return reference;
    }
    
    private ConstraintWidget getTarget(final Object o) {
        if (o instanceof Reference) {
            return ((Reference)o).getConstraintWidget();
        }
        return null;
    }
    
    public void addCustomColor(final String key, final int i) {
        this.mCustomColors.put(key, i);
    }
    
    public void addCustomFloat(final String key, final float f) {
        if (this.mCustomFloats == null) {
            this.mCustomFloats = new HashMap<String, Float>();
        }
        this.mCustomFloats.put(key, f);
    }
    
    public ConstraintReference alpha(final float mAlpha) {
        this.mAlpha = mAlpha;
        return this;
    }
    
    @Override
    public void apply() {
        if (this.mConstraintWidget == null) {
            return;
        }
        final Facade mFacade = this.mFacade;
        if (mFacade != null) {
            mFacade.apply();
        }
        this.mHorizontalDimension.apply(this.mState, this.mConstraintWidget, 0);
        this.mVerticalDimension.apply(this.mState, this.mConstraintWidget, 1);
        this.dereference();
        this.applyConnection(this.mConstraintWidget, this.mLeftToLeft, State.Constraint.LEFT_TO_LEFT);
        this.applyConnection(this.mConstraintWidget, this.mLeftToRight, State.Constraint.LEFT_TO_RIGHT);
        this.applyConnection(this.mConstraintWidget, this.mRightToLeft, State.Constraint.RIGHT_TO_LEFT);
        this.applyConnection(this.mConstraintWidget, this.mRightToRight, State.Constraint.RIGHT_TO_RIGHT);
        this.applyConnection(this.mConstraintWidget, this.mStartToStart, State.Constraint.START_TO_START);
        this.applyConnection(this.mConstraintWidget, this.mStartToEnd, State.Constraint.START_TO_END);
        this.applyConnection(this.mConstraintWidget, this.mEndToStart, State.Constraint.END_TO_START);
        this.applyConnection(this.mConstraintWidget, this.mEndToEnd, State.Constraint.END_TO_END);
        this.applyConnection(this.mConstraintWidget, this.mTopToTop, State.Constraint.TOP_TO_TOP);
        this.applyConnection(this.mConstraintWidget, this.mTopToBottom, State.Constraint.TOP_TO_BOTTOM);
        this.applyConnection(this.mConstraintWidget, this.mBottomToTop, State.Constraint.BOTTOM_TO_TOP);
        this.applyConnection(this.mConstraintWidget, this.mBottomToBottom, State.Constraint.BOTTOM_TO_BOTTOM);
        this.applyConnection(this.mConstraintWidget, this.mBaselineToBaseline, State.Constraint.BASELINE_TO_BASELINE);
        this.applyConnection(this.mConstraintWidget, this.mCircularConstraint, State.Constraint.CIRCULAR_CONSTRAINT);
        final int mHorizontalChainStyle = this.mHorizontalChainStyle;
        if (mHorizontalChainStyle != 0) {
            this.mConstraintWidget.setHorizontalChainStyle(mHorizontalChainStyle);
        }
        final int mVerticalChainStyle = this.mVerticalChainStyle;
        if (mVerticalChainStyle != 0) {
            this.mConstraintWidget.setVerticalChainStyle(mVerticalChainStyle);
        }
        this.mConstraintWidget.setHorizontalBiasPercent(this.mHorizontalBias);
        this.mConstraintWidget.setVerticalBiasPercent(this.mVerticalBias);
        final ConstraintWidget mConstraintWidget = this.mConstraintWidget;
        final WidgetFrame frame = mConstraintWidget.frame;
        frame.pivotX = this.mPivotX;
        frame.pivotY = this.mPivotY;
        frame.rotationX = this.mRotationX;
        frame.rotationY = this.mRotationY;
        frame.rotationZ = this.mRotationZ;
        frame.translationX = this.mTranslationX;
        frame.translationY = this.mTranslationY;
        frame.translationZ = this.mTranslationZ;
        frame.scaleX = this.mScaleX;
        frame.scaleY = this.mScaleY;
        frame.alpha = this.mAlpha;
        mConstraintWidget.setVisibility(frame.visibility = this.mVisibility);
        final HashMap<String, Integer> mCustomColors = this.mCustomColors;
        if (mCustomColors != null) {
            for (final String key : mCustomColors.keySet()) {
                this.mConstraintWidget.frame.setCustomAttribute(key, 902, this.mCustomColors.get(key));
            }
        }
        final HashMap<String, Float> mCustomFloats = this.mCustomFloats;
        if (mCustomFloats != null) {
            for (final String key2 : mCustomFloats.keySet()) {
                this.mConstraintWidget.frame.setCustomAttribute(key2, 901, this.mCustomFloats.get(key2));
            }
        }
    }
    
    public ConstraintReference baseline() {
        this.mLast = State.Constraint.BASELINE_TO_BASELINE;
        return this;
    }
    
    public ConstraintReference baselineToBaseline(final Object mBaselineToBaseline) {
        this.mLast = State.Constraint.BASELINE_TO_BASELINE;
        this.mBaselineToBaseline = mBaselineToBaseline;
        return this;
    }
    
    public ConstraintReference bias(final float n) {
        final State.Constraint mLast = this.mLast;
        if (mLast == null) {
            return this;
        }
        switch (ConstraintReference$1.$SwitchMap$androidx$constraintlayout$core$state$State$Constraint[mLast.ordinal()]) {
            case 9:
            case 10:
            case 11:
            case 12:
            case 16: {
                this.mVerticalBias = n;
                break;
            }
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 15: {
                this.mHorizontalBias = n;
                break;
            }
        }
        return this;
    }
    
    public ConstraintReference bottom() {
        if (this.mBottomToTop != null) {
            this.mLast = State.Constraint.BOTTOM_TO_TOP;
        }
        else {
            this.mLast = State.Constraint.BOTTOM_TO_BOTTOM;
        }
        return this;
    }
    
    public ConstraintReference bottomToBottom(final Object mBottomToBottom) {
        this.mLast = State.Constraint.BOTTOM_TO_BOTTOM;
        this.mBottomToBottom = mBottomToBottom;
        return this;
    }
    
    public ConstraintReference bottomToTop(final Object mBottomToTop) {
        this.mLast = State.Constraint.BOTTOM_TO_TOP;
        this.mBottomToTop = mBottomToTop;
        return this;
    }
    
    public ConstraintReference centerHorizontally(Object value) {
        value = this.get(value);
        this.mStartToStart = value;
        this.mEndToEnd = value;
        this.mLast = State.Constraint.CENTER_HORIZONTALLY;
        this.mHorizontalBias = 0.5f;
        return this;
    }
    
    public ConstraintReference centerVertically(Object value) {
        value = this.get(value);
        this.mTopToTop = value;
        this.mBottomToBottom = value;
        this.mLast = State.Constraint.CENTER_VERTICALLY;
        this.mVerticalBias = 0.5f;
        return this;
    }
    
    public ConstraintReference circularConstraint(final Object o, final float mCircularAngle, final float mCircularDistance) {
        this.mCircularConstraint = this.get(o);
        this.mCircularAngle = mCircularAngle;
        this.mCircularDistance = mCircularDistance;
        this.mLast = State.Constraint.CIRCULAR_CONSTRAINT;
        return this;
    }
    
    public ConstraintReference clear() {
        final State.Constraint mLast = this.mLast;
        if (mLast != null) {
            switch (ConstraintReference$1.$SwitchMap$androidx$constraintlayout$core$state$State$Constraint[mLast.ordinal()]) {
                case 14: {
                    this.mCircularConstraint = null;
                    break;
                }
                case 13: {
                    this.mBaselineToBaseline = null;
                    break;
                }
                case 11:
                case 12: {
                    this.mBottomToTop = null;
                    this.mBottomToBottom = null;
                    this.mMarginBottom = 0;
                    this.mMarginBottomGone = 0;
                    break;
                }
                case 9:
                case 10: {
                    this.mTopToTop = null;
                    this.mTopToBottom = null;
                    this.mMarginTop = 0;
                    this.mMarginTopGone = 0;
                    break;
                }
                case 7:
                case 8: {
                    this.mEndToStart = null;
                    this.mEndToEnd = null;
                    this.mMarginEnd = 0;
                    this.mMarginEndGone = 0;
                    break;
                }
                case 5:
                case 6: {
                    this.mStartToStart = null;
                    this.mStartToEnd = null;
                    this.mMarginStart = 0;
                    this.mMarginStartGone = 0;
                    break;
                }
                case 3:
                case 4: {
                    this.mRightToLeft = null;
                    this.mRightToRight = null;
                    this.mMarginRight = 0;
                    this.mMarginRightGone = 0;
                    break;
                }
                case 1:
                case 2: {
                    this.mLeftToLeft = null;
                    this.mLeftToRight = null;
                    this.mMarginLeft = 0;
                    this.mMarginLeftGone = 0;
                    break;
                }
            }
        }
        else {
            this.mLeftToLeft = null;
            this.mLeftToRight = null;
            this.mMarginLeft = 0;
            this.mRightToLeft = null;
            this.mRightToRight = null;
            this.mMarginRight = 0;
            this.mStartToStart = null;
            this.mStartToEnd = null;
            this.mMarginStart = 0;
            this.mEndToStart = null;
            this.mEndToEnd = null;
            this.mMarginEnd = 0;
            this.mTopToTop = null;
            this.mTopToBottom = null;
            this.mMarginTop = 0;
            this.mBottomToTop = null;
            this.mBottomToBottom = null;
            this.mMarginBottom = 0;
            this.mBaselineToBaseline = null;
            this.mCircularConstraint = null;
            this.mHorizontalBias = 0.5f;
            this.mVerticalBias = 0.5f;
            this.mMarginLeftGone = 0;
            this.mMarginRightGone = 0;
            this.mMarginStartGone = 0;
            this.mMarginEndGone = 0;
            this.mMarginTopGone = 0;
            this.mMarginBottomGone = 0;
        }
        return this;
    }
    
    public ConstraintReference clearHorizontal() {
        this.start().clear();
        this.end().clear();
        this.left().clear();
        this.right().clear();
        return this;
    }
    
    public ConstraintReference clearVertical() {
        this.top().clear();
        this.baseline().clear();
        this.bottom().clear();
        return this;
    }
    
    public ConstraintWidget createConstraintWidget() {
        return new ConstraintWidget(this.getWidth().getValue(), this.getHeight().getValue());
    }
    
    public ConstraintReference end() {
        if (this.mEndToStart != null) {
            this.mLast = State.Constraint.END_TO_START;
        }
        else {
            this.mLast = State.Constraint.END_TO_END;
        }
        return this;
    }
    
    public ConstraintReference endToEnd(final Object mEndToEnd) {
        this.mLast = State.Constraint.END_TO_END;
        this.mEndToEnd = mEndToEnd;
        return this;
    }
    
    public ConstraintReference endToStart(final Object mEndToStart) {
        this.mLast = State.Constraint.END_TO_START;
        this.mEndToStart = mEndToStart;
        return this;
    }
    
    public float getAlpha() {
        return this.mAlpha;
    }
    
    @Override
    public ConstraintWidget getConstraintWidget() {
        if (this.mConstraintWidget == null) {
            (this.mConstraintWidget = this.createConstraintWidget()).setCompanionWidget(this.mView);
        }
        return this.mConstraintWidget;
    }
    
    @Override
    public Facade getFacade() {
        return this.mFacade;
    }
    
    public Dimension getHeight() {
        return this.mVerticalDimension;
    }
    
    public int getHorizontalChainStyle() {
        return this.mHorizontalChainStyle;
    }
    
    @Override
    public Object getKey() {
        return this.key;
    }
    
    public float getPivotX() {
        return this.mPivotX;
    }
    
    public float getPivotY() {
        return this.mPivotY;
    }
    
    public float getRotationX() {
        return this.mRotationX;
    }
    
    public float getRotationY() {
        return this.mRotationY;
    }
    
    public float getRotationZ() {
        return this.mRotationZ;
    }
    
    public float getScaleX() {
        return this.mScaleX;
    }
    
    public float getScaleY() {
        return this.mScaleY;
    }
    
    public String getTag() {
        return this.mTag;
    }
    
    public float getTranslationX() {
        return this.mTranslationX;
    }
    
    public float getTranslationY() {
        return this.mTranslationY;
    }
    
    public float getTranslationZ() {
        return this.mTranslationZ;
    }
    
    public int getVerticalChainStyle(final int n) {
        return this.mVerticalChainStyle;
    }
    
    public Object getView() {
        return this.mView;
    }
    
    public Dimension getWidth() {
        return this.mHorizontalDimension;
    }
    
    public ConstraintReference height(final Dimension height) {
        return this.setHeight(height);
    }
    
    public ConstraintReference horizontalBias(final float mHorizontalBias) {
        this.mHorizontalBias = mHorizontalBias;
        return this;
    }
    
    public ConstraintReference left() {
        if (this.mLeftToLeft != null) {
            this.mLast = State.Constraint.LEFT_TO_LEFT;
        }
        else {
            this.mLast = State.Constraint.LEFT_TO_RIGHT;
        }
        return this;
    }
    
    public ConstraintReference leftToLeft(final Object mLeftToLeft) {
        this.mLast = State.Constraint.LEFT_TO_LEFT;
        this.mLeftToLeft = mLeftToLeft;
        return this;
    }
    
    public ConstraintReference leftToRight(final Object mLeftToRight) {
        this.mLast = State.Constraint.LEFT_TO_RIGHT;
        this.mLeftToRight = mLeftToRight;
        return this;
    }
    
    public ConstraintReference margin(final int n) {
        final State.Constraint mLast = this.mLast;
        if (mLast != null) {
            switch (ConstraintReference$1.$SwitchMap$androidx$constraintlayout$core$state$State$Constraint[mLast.ordinal()]) {
                case 14: {
                    this.mCircularDistance = (float)n;
                    break;
                }
                case 11:
                case 12: {
                    this.mMarginBottom = n;
                    break;
                }
                case 9:
                case 10: {
                    this.mMarginTop = n;
                    break;
                }
                case 7:
                case 8: {
                    this.mMarginEnd = n;
                    break;
                }
                case 5:
                case 6: {
                    this.mMarginStart = n;
                    break;
                }
                case 3:
                case 4: {
                    this.mMarginRight = n;
                    break;
                }
                case 1:
                case 2: {
                    this.mMarginLeft = n;
                    break;
                }
            }
        }
        else {
            this.mMarginLeft = n;
            this.mMarginRight = n;
            this.mMarginStart = n;
            this.mMarginEnd = n;
            this.mMarginTop = n;
            this.mMarginBottom = n;
        }
        return this;
    }
    
    public ConstraintReference margin(final Object o) {
        return this.margin(this.mState.convertDimension(o));
    }
    
    public ConstraintReference marginGone(final int n) {
        final State.Constraint mLast = this.mLast;
        if (mLast != null) {
            switch (ConstraintReference$1.$SwitchMap$androidx$constraintlayout$core$state$State$Constraint[mLast.ordinal()]) {
                case 11:
                case 12: {
                    this.mMarginBottomGone = n;
                    break;
                }
                case 9:
                case 10: {
                    this.mMarginTopGone = n;
                    break;
                }
                case 7:
                case 8: {
                    this.mMarginEndGone = n;
                    break;
                }
                case 5:
                case 6: {
                    this.mMarginStartGone = n;
                    break;
                }
                case 3:
                case 4: {
                    this.mMarginRightGone = n;
                    break;
                }
                case 1:
                case 2: {
                    this.mMarginLeftGone = n;
                    break;
                }
            }
        }
        else {
            this.mMarginLeftGone = n;
            this.mMarginRightGone = n;
            this.mMarginStartGone = n;
            this.mMarginEndGone = n;
            this.mMarginTopGone = n;
            this.mMarginBottomGone = n;
        }
        return this;
    }
    
    public ConstraintReference pivotX(final float mPivotX) {
        this.mPivotX = mPivotX;
        return this;
    }
    
    public ConstraintReference pivotY(final float mPivotY) {
        this.mPivotY = mPivotY;
        return this;
    }
    
    public ConstraintReference right() {
        if (this.mRightToLeft != null) {
            this.mLast = State.Constraint.RIGHT_TO_LEFT;
        }
        else {
            this.mLast = State.Constraint.RIGHT_TO_RIGHT;
        }
        return this;
    }
    
    public ConstraintReference rightToLeft(final Object mRightToLeft) {
        this.mLast = State.Constraint.RIGHT_TO_LEFT;
        this.mRightToLeft = mRightToLeft;
        return this;
    }
    
    public ConstraintReference rightToRight(final Object mRightToRight) {
        this.mLast = State.Constraint.RIGHT_TO_RIGHT;
        this.mRightToRight = mRightToRight;
        return this;
    }
    
    public ConstraintReference rotationX(final float mRotationX) {
        this.mRotationX = mRotationX;
        return this;
    }
    
    public ConstraintReference rotationY(final float mRotationY) {
        this.mRotationY = mRotationY;
        return this;
    }
    
    public ConstraintReference rotationZ(final float mRotationZ) {
        this.mRotationZ = mRotationZ;
        return this;
    }
    
    public ConstraintReference scaleX(final float mScaleX) {
        this.mScaleX = mScaleX;
        return this;
    }
    
    public ConstraintReference scaleY(final float mScaleY) {
        this.mScaleY = mScaleY;
        return this;
    }
    
    @Override
    public void setConstraintWidget(final ConstraintWidget mConstraintWidget) {
        if (mConstraintWidget == null) {
            return;
        }
        (this.mConstraintWidget = mConstraintWidget).setCompanionWidget(this.mView);
    }
    
    public void setFacade(final Facade mFacade) {
        this.mFacade = mFacade;
        if (mFacade != null) {
            this.setConstraintWidget(mFacade.getConstraintWidget());
        }
    }
    
    public ConstraintReference setHeight(final Dimension mVerticalDimension) {
        this.mVerticalDimension = mVerticalDimension;
        return this;
    }
    
    public void setHorizontalChainStyle(final int mHorizontalChainStyle) {
        this.mHorizontalChainStyle = mHorizontalChainStyle;
    }
    
    @Override
    public void setKey(final Object key) {
        this.key = key;
    }
    
    public void setTag(final String mTag) {
        this.mTag = mTag;
    }
    
    public void setVerticalChainStyle(final int mVerticalChainStyle) {
        this.mVerticalChainStyle = mVerticalChainStyle;
    }
    
    public void setView(final Object o) {
        this.mView = o;
        final ConstraintWidget mConstraintWidget = this.mConstraintWidget;
        if (mConstraintWidget != null) {
            mConstraintWidget.setCompanionWidget(o);
        }
    }
    
    public ConstraintReference setWidth(final Dimension mHorizontalDimension) {
        this.mHorizontalDimension = mHorizontalDimension;
        return this;
    }
    
    public ConstraintReference start() {
        if (this.mStartToStart != null) {
            this.mLast = State.Constraint.START_TO_START;
        }
        else {
            this.mLast = State.Constraint.START_TO_END;
        }
        return this;
    }
    
    public ConstraintReference startToEnd(final Object mStartToEnd) {
        this.mLast = State.Constraint.START_TO_END;
        this.mStartToEnd = mStartToEnd;
        return this;
    }
    
    public ConstraintReference startToStart(final Object mStartToStart) {
        this.mLast = State.Constraint.START_TO_START;
        this.mStartToStart = mStartToStart;
        return this;
    }
    
    public ConstraintReference top() {
        if (this.mTopToTop != null) {
            this.mLast = State.Constraint.TOP_TO_TOP;
        }
        else {
            this.mLast = State.Constraint.TOP_TO_BOTTOM;
        }
        return this;
    }
    
    public ConstraintReference topToBottom(final Object mTopToBottom) {
        this.mLast = State.Constraint.TOP_TO_BOTTOM;
        this.mTopToBottom = mTopToBottom;
        return this;
    }
    
    public ConstraintReference topToTop(final Object mTopToTop) {
        this.mLast = State.Constraint.TOP_TO_TOP;
        this.mTopToTop = mTopToTop;
        return this;
    }
    
    public ConstraintReference translationX(final float mTranslationX) {
        this.mTranslationX = mTranslationX;
        return this;
    }
    
    public ConstraintReference translationY(final float mTranslationY) {
        this.mTranslationY = mTranslationY;
        return this;
    }
    
    public ConstraintReference translationZ(final float mTranslationZ) {
        this.mTranslationZ = mTranslationZ;
        return this;
    }
    
    public void validate() throws IncorrectConstraintException {
        final ArrayList list = new ArrayList();
        if (this.mLeftToLeft != null && this.mLeftToRight != null) {
            list.add("LeftToLeft and LeftToRight both defined");
        }
        if (this.mRightToLeft != null && this.mRightToRight != null) {
            list.add("RightToLeft and RightToRight both defined");
        }
        if (this.mStartToStart != null && this.mStartToEnd != null) {
            list.add("StartToStart and StartToEnd both defined");
        }
        if (this.mEndToStart != null && this.mEndToEnd != null) {
            list.add("EndToStart and EndToEnd both defined");
        }
        if ((this.mLeftToLeft != null || this.mLeftToRight != null || this.mRightToLeft != null || this.mRightToRight != null) && (this.mStartToStart != null || this.mStartToEnd != null || this.mEndToStart != null || this.mEndToEnd != null)) {
            list.add("Both left/right and start/end constraints defined");
        }
        if (list.size() <= 0) {
            return;
        }
        throw new IncorrectConstraintException(list);
    }
    
    public ConstraintReference verticalBias(final float mVerticalBias) {
        this.mVerticalBias = mVerticalBias;
        return this;
    }
    
    public ConstraintReference visibility(final int mVisibility) {
        this.mVisibility = mVisibility;
        return this;
    }
    
    public ConstraintReference width(final Dimension width) {
        return this.setWidth(width);
    }
    
    public interface ConstraintReferenceFactory
    {
        ConstraintReference create(final State p0);
    }
    
    static class IncorrectConstraintException extends Exception
    {
        private final ArrayList<String> mErrors;
        
        public IncorrectConstraintException(final ArrayList<String> mErrors) {
            this.mErrors = mErrors;
        }
        
        public ArrayList<String> getErrors() {
            return this.mErrors;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("IncorrectConstraintException: ");
            sb.append(this.mErrors.toString());
            return sb.toString();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.state;

import java.util.Iterator;
import androidx.constraintlayout.core.parser.CLParsingException;
import androidx.constraintlayout.core.parser.CLNumber;
import androidx.constraintlayout.core.parser.CLKey;
import androidx.constraintlayout.core.parser.CLObject;
import androidx.constraintlayout.core.parser.CLElement;
import java.io.PrintStream;
import java.util.Set;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.core.motion.CustomVariable;
import java.util.HashMap;

public class WidgetFrame
{
    private static final boolean OLD_SYSTEM = true;
    public static float phone_orientation = Float.NaN;
    public float alpha;
    public int bottom;
    public float interpolatedPos;
    public int left;
    public final HashMap<String, CustomVariable> mCustom;
    public String name;
    public float pivotX;
    public float pivotY;
    public int right;
    public float rotationX;
    public float rotationY;
    public float rotationZ;
    public float scaleX;
    public float scaleY;
    public int top;
    public float translationX;
    public float translationY;
    public float translationZ;
    public int visibility;
    public ConstraintWidget widget;
    
    public WidgetFrame() {
        this.widget = null;
        this.left = 0;
        this.top = 0;
        this.right = 0;
        this.bottom = 0;
        this.pivotX = Float.NaN;
        this.pivotY = Float.NaN;
        this.rotationX = Float.NaN;
        this.rotationY = Float.NaN;
        this.rotationZ = Float.NaN;
        this.translationX = Float.NaN;
        this.translationY = Float.NaN;
        this.translationZ = Float.NaN;
        this.scaleX = Float.NaN;
        this.scaleY = Float.NaN;
        this.alpha = Float.NaN;
        this.interpolatedPos = Float.NaN;
        this.visibility = 0;
        this.mCustom = new HashMap<String, CustomVariable>();
        this.name = null;
    }
    
    public WidgetFrame(final WidgetFrame widgetFrame) {
        this.widget = null;
        this.left = 0;
        this.top = 0;
        this.right = 0;
        this.bottom = 0;
        this.pivotX = Float.NaN;
        this.pivotY = Float.NaN;
        this.rotationX = Float.NaN;
        this.rotationY = Float.NaN;
        this.rotationZ = Float.NaN;
        this.translationX = Float.NaN;
        this.translationY = Float.NaN;
        this.translationZ = Float.NaN;
        this.scaleX = Float.NaN;
        this.scaleY = Float.NaN;
        this.alpha = Float.NaN;
        this.interpolatedPos = Float.NaN;
        this.visibility = 0;
        this.mCustom = new HashMap<String, CustomVariable>();
        this.name = null;
        this.widget = widgetFrame.widget;
        this.left = widgetFrame.left;
        this.top = widgetFrame.top;
        this.right = widgetFrame.right;
        this.bottom = widgetFrame.bottom;
        this.updateAttributes(widgetFrame);
    }
    
    public WidgetFrame(final ConstraintWidget widget) {
        this.widget = null;
        this.left = 0;
        this.top = 0;
        this.right = 0;
        this.bottom = 0;
        this.pivotX = Float.NaN;
        this.pivotY = Float.NaN;
        this.rotationX = Float.NaN;
        this.rotationY = Float.NaN;
        this.rotationZ = Float.NaN;
        this.translationX = Float.NaN;
        this.translationY = Float.NaN;
        this.translationZ = Float.NaN;
        this.scaleX = Float.NaN;
        this.scaleY = Float.NaN;
        this.alpha = Float.NaN;
        this.interpolatedPos = Float.NaN;
        this.visibility = 0;
        this.mCustom = new HashMap<String, CustomVariable>();
        this.name = null;
        this.widget = widget;
    }
    
    private static void add(final StringBuilder sb, final String str, final float n) {
        if (Float.isNaN(n)) {
            return;
        }
        sb.append(str);
        sb.append(": ");
        sb.append(n);
        sb.append(",\n");
    }
    
    private static void add(final StringBuilder sb, final String str, final int i) {
        sb.append(str);
        sb.append(": ");
        sb.append(i);
        sb.append(",\n");
    }
    
    private static float interpolate(float v, float v2, final float n, final float n2) {
        final boolean naN = Float.isNaN(v);
        final boolean naN2 = Float.isNaN(v2);
        if (naN && naN2) {
            return Float.NaN;
        }
        if (naN) {
            v = n;
        }
        if (naN2) {
            v2 = n;
        }
        return v + n2 * (v2 - v);
    }
    
    public static void interpolate(int frame, int top, final WidgetFrame widgetFrame, final WidgetFrame widgetFrame2, final WidgetFrame widgetFrame3, final Transition transition, final float n) {
        final float n2 = 100.0f * n;
        final int n3 = (int)n2;
        int left = widgetFrame2.left;
        int top2 = widgetFrame2.top;
        final int left2 = widgetFrame3.left;
        final int top3 = widgetFrame3.top;
        final int right = widgetFrame2.right;
        final int bottom = widgetFrame2.bottom;
        int n4 = widgetFrame3.right - left2;
        int n5 = widgetFrame3.bottom - top3;
        float alpha = widgetFrame2.alpha;
        final float alpha2 = widgetFrame3.alpha;
        int n6;
        int n7;
        if (widgetFrame2.visibility == 8) {
            left -= (int)(n4 / 2.0f);
            top2 -= (int)(n5 / 2.0f);
            if (Float.isNaN(alpha)) {
                n6 = n5;
                n7 = n4;
                alpha = 0.0f;
            }
            else {
                n7 = n4;
                n6 = n5;
            }
        }
        else {
            n7 = right - left;
            n6 = bottom - top2;
        }
        float n8 = alpha2;
        int n9 = left2;
        int n10 = top3;
        if (widgetFrame3.visibility == 8) {
            final int n11 = (int)(left2 - n7 / 2.0f);
            final int n12 = (int)(top3 - n6 / 2.0f);
            final boolean naN = Float.isNaN(alpha2);
            final int n13 = n7;
            final int n14 = n6;
            n8 = alpha2;
            n9 = n11;
            n10 = n12;
            n5 = n14;
            n4 = n13;
            if (naN) {
                n8 = 0.0f;
                n4 = n13;
                n5 = n14;
                n10 = n12;
                n9 = n11;
            }
        }
        if (Float.isNaN(alpha) && !Float.isNaN(n8)) {
            alpha = 1.0f;
        }
        float n15 = n8;
        if (!Float.isNaN(alpha)) {
            n15 = n8;
            if (Float.isNaN(n8)) {
                n15 = 1.0f;
            }
        }
        float n16;
        if (widgetFrame.widget != null && transition.hasPositionKeyframes()) {
            final Transition.KeyPosition previousPosition = transition.findPreviousPosition(widgetFrame.widget.stringId, n3);
            Object nextPosition;
            if (previousPosition == (nextPosition = transition.findNextPosition(widgetFrame.widget.stringId, n3))) {
                nextPosition = null;
            }
            int frame2;
            if (previousPosition != null) {
                left = (int)(previousPosition.x * frame);
                top2 = (int)(previousPosition.y * top);
                frame2 = previousPosition.frame;
            }
            else {
                frame2 = 0;
            }
            if (nextPosition != null) {
                n9 = (int)(((Transition.KeyPosition)nextPosition).x * frame);
                n10 = (int)(((Transition.KeyPosition)nextPosition).y * top);
                frame = ((Transition.KeyPosition)nextPosition).frame;
            }
            else {
                frame = 100;
            }
            n16 = (n2 - frame2) / (frame - frame2);
        }
        else {
            n16 = n;
        }
        widgetFrame.widget = widgetFrame2.widget;
        frame = (int)(left + (n9 - left) * n16);
        widgetFrame.left = frame;
        top = (int)(top2 + n16 * (n10 - top2));
        widgetFrame.top = top;
        final float n17 = 1.0f - n;
        final int n18 = (int)(n7 * n17 + n4 * n);
        final int n19 = (int)(n17 * n6 + n5 * n);
        widgetFrame.right = frame + n18;
        widgetFrame.bottom = top + n19;
        widgetFrame.pivotX = interpolate(widgetFrame2.pivotX, widgetFrame3.pivotX, 0.5f, n);
        widgetFrame.pivotY = interpolate(widgetFrame2.pivotY, widgetFrame3.pivotY, 0.5f, n);
        widgetFrame.rotationX = interpolate(widgetFrame2.rotationX, widgetFrame3.rotationX, 0.0f, n);
        widgetFrame.rotationY = interpolate(widgetFrame2.rotationY, widgetFrame3.rotationY, 0.0f, n);
        widgetFrame.rotationZ = interpolate(widgetFrame2.rotationZ, widgetFrame3.rotationZ, 0.0f, n);
        widgetFrame.scaleX = interpolate(widgetFrame2.scaleX, widgetFrame3.scaleX, 1.0f, n);
        widgetFrame.scaleY = interpolate(widgetFrame2.scaleY, widgetFrame3.scaleY, 1.0f, n);
        widgetFrame.translationX = interpolate(widgetFrame2.translationX, widgetFrame3.translationX, 0.0f, n);
        widgetFrame.translationY = interpolate(widgetFrame2.translationY, widgetFrame3.translationY, 0.0f, n);
        widgetFrame.translationZ = interpolate(widgetFrame2.translationZ, widgetFrame3.translationZ, 0.0f, n);
        widgetFrame.alpha = interpolate(alpha, n15, 1.0f, n);
    }
    
    public void addCustomColor(final String s, final int n) {
        this.setCustomAttribute(s, 902, n);
    }
    
    public void addCustomFloat(final String s, final float n) {
        this.setCustomAttribute(s, 901, n);
    }
    
    public float centerX() {
        final int left = this.left;
        return left + (this.right - left) / 2.0f;
    }
    
    public float centerY() {
        final int top = this.top;
        return top + (this.bottom - top) / 2.0f;
    }
    
    public CustomVariable getCustomAttribute(final String key) {
        return this.mCustom.get(key);
    }
    
    public Set<String> getCustomAttributeNames() {
        return this.mCustom.keySet();
    }
    
    public int getCustomColor(final String s) {
        if (this.mCustom.containsKey(s)) {
            return this.mCustom.get(s).getColorValue();
        }
        return -21880;
    }
    
    public float getCustomFloat(final String s) {
        if (this.mCustom.containsKey(s)) {
            return this.mCustom.get(s).getFloatValue();
        }
        return Float.NaN;
    }
    
    public int height() {
        return Math.max(0, this.bottom - this.top);
    }
    
    public boolean isDefaultTransform() {
        return Float.isNaN(this.rotationX) && Float.isNaN(this.rotationY) && Float.isNaN(this.rotationZ) && Float.isNaN(this.translationX) && Float.isNaN(this.translationY) && Float.isNaN(this.translationZ) && Float.isNaN(this.scaleX) && Float.isNaN(this.scaleY) && Float.isNaN(this.alpha);
    }
    
    void logv(final String str) {
        final StackTraceElement stackTraceElement = new Throwable().getStackTrace()[1];
        final StringBuilder sb = new StringBuilder();
        sb.append(".(");
        sb.append(stackTraceElement.getFileName());
        sb.append(":");
        sb.append(stackTraceElement.getLineNumber());
        sb.append(") ");
        sb.append(stackTraceElement.getMethodName());
        final String string = sb.toString();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(string);
        sb2.append(" ");
        sb2.append(this.hashCode() % 1000);
        final String string2 = sb2.toString();
        String str2;
        if (this.widget != null) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(string2);
            sb3.append("/");
            sb3.append(this.widget.hashCode() % 1000);
            str2 = sb3.toString();
        }
        else {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append(string2);
            sb4.append("/NULL");
            str2 = sb4.toString();
        }
        final PrintStream out = System.out;
        final StringBuilder sb5 = new StringBuilder();
        sb5.append(str2);
        sb5.append(" ");
        sb5.append(str);
        out.println(sb5.toString());
    }
    
    void parseCustom(final CLElement clElement) throws CLParsingException {
        final CLObject clObject = (CLObject)clElement;
        for (int size = clObject.size(), i = 0; i < size; ++i) {
            final CLKey clKey = (CLKey)clObject.get(i);
            clKey.content();
            final CLElement value = clKey.getValue();
            final String content = value.content();
            if (content.matches("#[0-9a-fA-F]+")) {
                this.setCustomAttribute(clKey.content(), 902, Integer.parseInt(content.substring(1), 16));
            }
            else if (value instanceof CLNumber) {
                this.setCustomAttribute(clKey.content(), 901, value.getFloat());
            }
            else {
                this.setCustomAttribute(clKey.content(), 903, content);
            }
        }
    }
    
    void printCustomAttributes() {
        final StackTraceElement stackTraceElement = new Throwable().getStackTrace()[1];
        final StringBuilder sb = new StringBuilder();
        sb.append(".(");
        sb.append(stackTraceElement.getFileName());
        sb.append(":");
        sb.append(stackTraceElement.getLineNumber());
        sb.append(") ");
        sb.append(stackTraceElement.getMethodName());
        final String string = sb.toString();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(string);
        sb2.append(" ");
        sb2.append(this.hashCode() % 1000);
        final String string2 = sb2.toString();
        String str;
        if (this.widget != null) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(string2);
            sb3.append("/");
            sb3.append(this.widget.hashCode() % 1000);
            sb3.append(" ");
            str = sb3.toString();
        }
        else {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append(string2);
            sb4.append("/NULL ");
            str = sb4.toString();
        }
        final HashMap<String, CustomVariable> mCustom = this.mCustom;
        if (mCustom != null) {
            for (final String key : mCustom.keySet()) {
                final PrintStream out = System.out;
                final StringBuilder sb5 = new StringBuilder();
                sb5.append(str);
                sb5.append(this.mCustom.get(key).toString());
                out.println(sb5.toString());
            }
        }
    }
    
    public StringBuilder serialize(final StringBuilder sb) {
        return this.serialize(sb, false);
    }
    
    public StringBuilder serialize(final StringBuilder sb, final boolean b) {
        sb.append("{\n");
        add(sb, "left", this.left);
        add(sb, "top", this.top);
        add(sb, "right", this.right);
        add(sb, "bottom", this.bottom);
        add(sb, "pivotX", this.pivotX);
        add(sb, "pivotY", this.pivotY);
        add(sb, "rotationX", this.rotationX);
        add(sb, "rotationY", this.rotationY);
        add(sb, "rotationZ", this.rotationZ);
        add(sb, "translationX", this.translationX);
        add(sb, "translationY", this.translationY);
        add(sb, "translationZ", this.translationZ);
        add(sb, "scaleX", this.scaleX);
        add(sb, "scaleY", this.scaleY);
        add(sb, "alpha", this.alpha);
        add(sb, "visibility", this.left);
        add(sb, "interpolatedPos", this.interpolatedPos);
        if (b) {
            add(sb, "phone_orientation", WidgetFrame.phone_orientation);
        }
        if (b) {
            add(sb, "phone_orientation", WidgetFrame.phone_orientation);
        }
        if (this.mCustom.size() != 0) {
            sb.append("custom : {\n");
            for (final String s : this.mCustom.keySet()) {
                final CustomVariable customVariable = this.mCustom.get(s);
                sb.append(s);
                sb.append(": ");
                switch (customVariable.getType()) {
                    default: {
                        continue;
                    }
                    case 904: {
                        sb.append("'");
                        sb.append(customVariable.getBooleanValue());
                        sb.append("',\n");
                        continue;
                    }
                    case 903: {
                        sb.append("'");
                        sb.append(customVariable.getStringValue());
                        sb.append("',\n");
                        continue;
                    }
                    case 902: {
                        sb.append("'");
                        sb.append(CustomVariable.colorString(customVariable.getIntegerValue()));
                        sb.append("',\n");
                        continue;
                    }
                    case 901:
                    case 905: {
                        sb.append(customVariable.getFloatValue());
                        sb.append(",\n");
                        continue;
                    }
                    case 900: {
                        sb.append(customVariable.getIntegerValue());
                        sb.append(",\n");
                        continue;
                    }
                }
            }
            sb.append("}\n");
        }
        sb.append("}\n");
        return sb;
    }
    
    public void setCustomAttribute(final String key, final int n, final float floatValue) {
        if (this.mCustom.containsKey(key)) {
            this.mCustom.get(key).setFloatValue(floatValue);
        }
        else {
            this.mCustom.put(key, new CustomVariable(key, n, floatValue));
        }
    }
    
    public void setCustomAttribute(final String key, final int n, final int intValue) {
        if (this.mCustom.containsKey(key)) {
            this.mCustom.get(key).setIntValue(intValue);
        }
        else {
            this.mCustom.put(key, new CustomVariable(key, n, intValue));
        }
    }
    
    public void setCustomAttribute(final String key, final int n, final String stringValue) {
        if (this.mCustom.containsKey(key)) {
            this.mCustom.get(key).setStringValue(stringValue);
        }
        else {
            this.mCustom.put(key, new CustomVariable(key, n, stringValue));
        }
    }
    
    public void setCustomAttribute(final String key, final int n, final boolean booleanValue) {
        if (this.mCustom.containsKey(key)) {
            this.mCustom.get(key).setBooleanValue(booleanValue);
        }
        else {
            this.mCustom.put(key, new CustomVariable(key, n, booleanValue));
        }
    }
    
    public boolean setValue(final String s, final CLElement clElement) throws CLParsingException {
        s.hashCode();
        final int hashCode = s.hashCode();
        int n = -1;
        switch (hashCode) {
            case 642850769: {
                if (!s.equals("interpolatedPos")) {
                    break;
                }
                n = 17;
                break;
            }
            case 108511772: {
                if (!s.equals("right")) {
                    break;
                }
                n = 16;
                break;
            }
            case 92909918: {
                if (!s.equals("alpha")) {
                    break;
                }
                n = 15;
                break;
            }
            case 3317767: {
                if (!s.equals("left")) {
                    break;
                }
                n = 14;
                break;
            }
            case 115029: {
                if (!s.equals("top")) {
                    break;
                }
                n = 13;
                break;
            }
            case -908189617: {
                if (!s.equals("scaleY")) {
                    break;
                }
                n = 12;
                break;
            }
            case -908189618: {
                if (!s.equals("scaleX")) {
                    break;
                }
                n = 11;
                break;
            }
            case -987906985: {
                if (!s.equals("pivotY")) {
                    break;
                }
                n = 10;
                break;
            }
            case -987906986: {
                if (!s.equals("pivotX")) {
                    break;
                }
                n = 9;
                break;
            }
            case -1225497655: {
                if (!s.equals("translationZ")) {
                    break;
                }
                n = 8;
                break;
            }
            case -1225497656: {
                if (!s.equals("translationY")) {
                    break;
                }
                n = 7;
                break;
            }
            case -1225497657: {
                if (!s.equals("translationX")) {
                    break;
                }
                n = 6;
                break;
            }
            case -1249320804: {
                if (!s.equals("rotationZ")) {
                    break;
                }
                n = 5;
                break;
            }
            case -1249320805: {
                if (!s.equals("rotationY")) {
                    break;
                }
                n = 4;
                break;
            }
            case -1249320806: {
                if (!s.equals("rotationX")) {
                    break;
                }
                n = 3;
                break;
            }
            case -1349088399: {
                if (!s.equals("custom")) {
                    break;
                }
                n = 2;
                break;
            }
            case -1383228885: {
                if (!s.equals("bottom")) {
                    break;
                }
                n = 1;
                break;
            }
            case -1881940865: {
                if (!s.equals("phone_orientation")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        switch (n) {
            default: {
                return false;
            }
            case 17: {
                this.interpolatedPos = clElement.getFloat();
                break;
            }
            case 16: {
                this.right = clElement.getInt();
                break;
            }
            case 15: {
                this.alpha = clElement.getFloat();
                break;
            }
            case 14: {
                this.left = clElement.getInt();
                break;
            }
            case 13: {
                this.top = clElement.getInt();
                break;
            }
            case 12: {
                this.scaleY = clElement.getFloat();
                break;
            }
            case 11: {
                this.scaleX = clElement.getFloat();
                break;
            }
            case 10: {
                this.pivotY = clElement.getFloat();
                break;
            }
            case 9: {
                this.pivotX = clElement.getFloat();
                break;
            }
            case 8: {
                this.translationZ = clElement.getFloat();
                break;
            }
            case 7: {
                this.translationY = clElement.getFloat();
                break;
            }
            case 6: {
                this.translationX = clElement.getFloat();
                break;
            }
            case 5: {
                this.rotationZ = clElement.getFloat();
                break;
            }
            case 4: {
                this.rotationY = clElement.getFloat();
                break;
            }
            case 3: {
                this.rotationX = clElement.getFloat();
                break;
            }
            case 2: {
                this.parseCustom(clElement);
                break;
            }
            case 1: {
                this.bottom = clElement.getInt();
                break;
            }
            case 0: {
                WidgetFrame.phone_orientation = clElement.getFloat();
                break;
            }
        }
        return true;
    }
    
    public WidgetFrame update() {
        final ConstraintWidget widget = this.widget;
        if (widget != null) {
            this.left = widget.getLeft();
            this.top = this.widget.getTop();
            this.right = this.widget.getRight();
            this.bottom = this.widget.getBottom();
            this.updateAttributes(this.widget.frame);
        }
        return this;
    }
    
    public WidgetFrame update(final ConstraintWidget widget) {
        if (widget == null) {
            return this;
        }
        this.widget = widget;
        this.update();
        return this;
    }
    
    public void updateAttributes(final WidgetFrame widgetFrame) {
        this.pivotX = widgetFrame.pivotX;
        this.pivotY = widgetFrame.pivotY;
        this.rotationX = widgetFrame.rotationX;
        this.rotationY = widgetFrame.rotationY;
        this.rotationZ = widgetFrame.rotationZ;
        this.translationX = widgetFrame.translationX;
        this.translationY = widgetFrame.translationY;
        this.translationZ = widgetFrame.translationZ;
        this.scaleX = widgetFrame.scaleX;
        this.scaleY = widgetFrame.scaleY;
        this.alpha = widgetFrame.alpha;
        this.visibility = widgetFrame.visibility;
        this.mCustom.clear();
        for (final CustomVariable customVariable : widgetFrame.mCustom.values()) {
            this.mCustom.put(customVariable.getName(), customVariable.copy());
        }
    }
    
    public int width() {
        return Math.max(0, this.right - this.left);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion;

public class CustomVariable
{
    private static final String TAG = "TransitionLayout";
    boolean mBooleanValue;
    private float mFloatValue;
    private int mIntegerValue;
    String mName;
    private String mStringValue;
    private int mType;
    
    public CustomVariable(final CustomVariable customVariable) {
        this.mIntegerValue = Integer.MIN_VALUE;
        this.mFloatValue = Float.NaN;
        this.mStringValue = null;
        this.mName = customVariable.mName;
        this.mType = customVariable.mType;
        this.mIntegerValue = customVariable.mIntegerValue;
        this.mFloatValue = customVariable.mFloatValue;
        this.mStringValue = customVariable.mStringValue;
        this.mBooleanValue = customVariable.mBooleanValue;
    }
    
    public CustomVariable(final CustomVariable customVariable, final Object value) {
        this.mIntegerValue = Integer.MIN_VALUE;
        this.mFloatValue = Float.NaN;
        this.mStringValue = null;
        this.mName = customVariable.mName;
        this.mType = customVariable.mType;
        this.setValue(value);
    }
    
    public CustomVariable(final String mName, final int mType) {
        this.mIntegerValue = Integer.MIN_VALUE;
        this.mFloatValue = Float.NaN;
        this.mStringValue = null;
        this.mName = mName;
        this.mType = mType;
    }
    
    public CustomVariable(final String mName, final int mType, final float mFloatValue) {
        this.mIntegerValue = Integer.MIN_VALUE;
        this.mStringValue = null;
        this.mName = mName;
        this.mType = mType;
        this.mFloatValue = mFloatValue;
    }
    
    public CustomVariable(final String mName, final int mType, final int mIntegerValue) {
        this.mIntegerValue = Integer.MIN_VALUE;
        this.mFloatValue = Float.NaN;
        this.mStringValue = null;
        this.mName = mName;
        this.mType = mType;
        if (mType == 901) {
            this.mFloatValue = (float)mIntegerValue;
        }
        else {
            this.mIntegerValue = mIntegerValue;
        }
    }
    
    public CustomVariable(final String mName, final int mType, final Object value) {
        this.mIntegerValue = Integer.MIN_VALUE;
        this.mFloatValue = Float.NaN;
        this.mStringValue = null;
        this.mName = mName;
        this.mType = mType;
        this.setValue(value);
    }
    
    public CustomVariable(final String mName, final int mType, final String mStringValue) {
        this.mIntegerValue = Integer.MIN_VALUE;
        this.mFloatValue = Float.NaN;
        this.mName = mName;
        this.mType = mType;
        this.mStringValue = mStringValue;
    }
    
    public CustomVariable(final String mName, final int mType, final boolean mBooleanValue) {
        this.mIntegerValue = Integer.MIN_VALUE;
        this.mFloatValue = Float.NaN;
        this.mStringValue = null;
        this.mName = mName;
        this.mType = mType;
        this.mBooleanValue = mBooleanValue;
    }
    
    private static int clamp(int n) {
        n = (n & ~(n >> 31)) - 255;
        return (n & n >> 31) + 255;
    }
    
    public static String colorString(final int i) {
        final StringBuilder sb = new StringBuilder();
        sb.append("00000000");
        sb.append(Integer.toHexString(i));
        final String string = sb.toString();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("#");
        sb2.append(string.substring(string.length() - 8));
        return sb2.toString();
    }
    
    public static int hsvToRgb(float n, final float n2, float n3) {
        n *= 6.0f;
        final int n4 = (int)n;
        n -= n4;
        n3 *= 255.0f;
        final int n5 = (int)((1.0f - n2) * n3 + 0.5f);
        final int n6 = (int)((1.0f - n * n2) * n3 + 0.5f);
        final int n7 = (int)((1.0f - (1.0f - n) * n2) * n3 + 0.5f);
        final int n8 = (int)(n3 + 0.5f);
        if (n4 == 0) {
            return (n8 << 16) + (n7 << 8) + n5 | 0xFF000000;
        }
        if (n4 == 1) {
            return (n6 << 16) + (n8 << 8) + n5 | 0xFF000000;
        }
        if (n4 == 2) {
            return (n5 << 16) + (n8 << 8) + n7 | 0xFF000000;
        }
        if (n4 == 3) {
            return (n5 << 16) + (n6 << 8) + n8 | 0xFF000000;
        }
        if (n4 == 4) {
            return (n7 << 16) + (n5 << 8) + n8 | 0xFF000000;
        }
        if (n4 != 5) {
            return 0;
        }
        return (n8 << 16) + (n5 << 8) + n6 | 0xFF000000;
    }
    
    public static int rgbaTocColor(final float n, final float n2, final float n3, final float n4) {
        return clamp((int)(n * 255.0f)) << 16 | clamp((int)(n4 * 255.0f)) << 24 | clamp((int)(n2 * 255.0f)) << 8 | clamp((int)(n3 * 255.0f));
    }
    
    public void applyToWidget(final MotionWidget motionWidget) {
        final int mType = this.mType;
        switch (mType) {
            case 904: {
                motionWidget.setCustomAttribute(this.mName, mType, this.mBooleanValue);
                break;
            }
            case 903: {
                motionWidget.setCustomAttribute(this.mName, mType, this.mStringValue);
                break;
            }
            case 901:
            case 905: {
                motionWidget.setCustomAttribute(this.mName, mType, this.mFloatValue);
                break;
            }
            case 900:
            case 902:
            case 906: {
                motionWidget.setCustomAttribute(this.mName, mType, this.mIntegerValue);
                break;
            }
        }
    }
    
    public CustomVariable copy() {
        return new CustomVariable(this);
    }
    
    public boolean diff(final CustomVariable customVariable) {
        final boolean b = false;
        final boolean b2 = false;
        final boolean b3 = false;
        final boolean b4 = false;
        final boolean b5 = false;
        final boolean b6 = false;
        boolean b7 = b5;
        if (customVariable != null) {
            final int mType = this.mType;
            if (mType != customVariable.mType) {
                b7 = b5;
            }
            else {
                switch (mType) {
                    default: {
                        return false;
                    }
                    case 905: {
                        boolean b8 = b6;
                        if (this.mFloatValue == customVariable.mFloatValue) {
                            b8 = true;
                        }
                        return b8;
                    }
                    case 904: {
                        boolean b9 = b;
                        if (this.mBooleanValue == customVariable.mBooleanValue) {
                            b9 = true;
                        }
                        return b9;
                    }
                    case 903: {
                        boolean b10 = b2;
                        if (this.mIntegerValue == customVariable.mIntegerValue) {
                            b10 = true;
                        }
                        return b10;
                    }
                    case 902: {
                        boolean b11 = b3;
                        if (this.mIntegerValue == customVariable.mIntegerValue) {
                            b11 = true;
                        }
                        return b11;
                    }
                    case 901: {
                        boolean b12 = b4;
                        if (this.mFloatValue == customVariable.mFloatValue) {
                            b12 = true;
                        }
                        return b12;
                    }
                    case 900:
                    case 906: {
                        b7 = b5;
                        if (this.mIntegerValue == customVariable.mIntegerValue) {
                            b7 = true;
                            break;
                        }
                        break;
                    }
                }
            }
        }
        return b7;
    }
    
    public boolean getBooleanValue() {
        return this.mBooleanValue;
    }
    
    public int getColorValue() {
        return this.mIntegerValue;
    }
    
    public float getFloatValue() {
        return this.mFloatValue;
    }
    
    public int getIntegerValue() {
        return this.mIntegerValue;
    }
    
    public int getInterpolatedColor(final float[] array) {
        return clamp((int)(array[3] * 255.0f)) << 24 | clamp((int)((float)Math.pow(array[0], 0.45454545454545453) * 255.0f)) << 16 | clamp((int)((float)Math.pow(array[1], 0.45454545454545453) * 255.0f)) << 8 | clamp((int)((float)Math.pow(array[2], 0.45454545454545453) * 255.0f));
    }
    
    public String getName() {
        return this.mName;
    }
    
    public String getStringValue() {
        return this.mStringValue;
    }
    
    public int getType() {
        return this.mType;
    }
    
    public float getValueToInterpolate() {
        switch (this.mType) {
            default: {
                return Float.NaN;
            }
            case 905: {
                return this.mFloatValue;
            }
            case 904: {
                float n;
                if (this.mBooleanValue) {
                    n = 1.0f;
                }
                else {
                    n = 0.0f;
                }
                return n;
            }
            case 903: {
                throw new RuntimeException("Cannot interpolate String");
            }
            case 902: {
                throw new RuntimeException("Color does not have a single color to interpolate");
            }
            case 901: {
                return this.mFloatValue;
            }
            case 900: {
                return (float)this.mIntegerValue;
            }
        }
    }
    
    public void getValuesToInterpolate(final float[] array) {
        switch (this.mType) {
            case 905: {
                array[0] = this.mFloatValue;
                break;
            }
            case 904: {
                float n;
                if (this.mBooleanValue) {
                    n = 1.0f;
                }
                else {
                    n = 0.0f;
                }
                array[0] = n;
                break;
            }
            case 903: {
                throw new RuntimeException("Color does not have a single color to interpolate");
            }
            case 902: {
                final int mIntegerValue = this.mIntegerValue;
                final float n2 = (float)Math.pow((mIntegerValue >> 16 & 0xFF) / 255.0f, 2.2);
                final float n3 = (float)Math.pow((mIntegerValue >> 8 & 0xFF) / 255.0f, 2.2);
                final float n4 = (float)Math.pow((mIntegerValue & 0xFF) / 255.0f, 2.2);
                array[0] = n2;
                array[1] = n3;
                array[2] = n4;
                array[3] = (mIntegerValue >> 24 & 0xFF) / 255.0f;
                break;
            }
            case 901: {
                array[0] = this.mFloatValue;
                break;
            }
            case 900: {
                array[0] = (float)this.mIntegerValue;
                break;
            }
        }
    }
    
    public boolean isContinuous() {
        final int mType = this.mType;
        return mType != 903 && mType != 904 && mType != 906;
    }
    
    public int numberOfInterpolatedValues() {
        if (this.mType != 902) {
            return 1;
        }
        return 4;
    }
    
    public void setBooleanValue(final boolean mBooleanValue) {
        this.mBooleanValue = mBooleanValue;
    }
    
    public void setFloatValue(final float mFloatValue) {
        this.mFloatValue = mFloatValue;
    }
    
    public void setIntValue(final int mIntegerValue) {
        this.mIntegerValue = mIntegerValue;
    }
    
    public void setInterpolatedValue(final MotionWidget motionWidget, final float[] array) {
        final int mType = this.mType;
        boolean b = true;
        switch (mType) {
            case 904: {
                final String mName = this.mName;
                if (array[0] <= 0.5f) {
                    b = false;
                }
                motionWidget.setCustomAttribute(mName, mType, b);
                break;
            }
            case 903:
            case 906: {
                final StringBuilder sb = new StringBuilder();
                sb.append("unable to interpolate ");
                sb.append(this.mName);
                throw new RuntimeException(sb.toString());
            }
            case 902: {
                motionWidget.setCustomAttribute(this.mName, this.mType, clamp((int)(array[3] * 255.0f)) << 24 | clamp((int)((float)Math.pow(array[0], 0.45454545454545453) * 255.0f)) << 16 | clamp((int)((float)Math.pow(array[1], 0.45454545454545453) * 255.0f)) << 8 | clamp((int)((float)Math.pow(array[2], 0.45454545454545453) * 255.0f)));
                break;
            }
            case 901:
            case 905: {
                motionWidget.setCustomAttribute(this.mName, mType, array[0]);
                break;
            }
            case 900: {
                motionWidget.setCustomAttribute(this.mName, mType, (int)array[0]);
                break;
            }
        }
    }
    
    public void setStringValue(final String mStringValue) {
        this.mStringValue = mStringValue;
    }
    
    public void setValue(final Object o) {
        switch (this.mType) {
            case 905: {
                this.mFloatValue = (float)o;
                break;
            }
            case 904: {
                this.mBooleanValue = (boolean)o;
                break;
            }
            case 903: {
                this.mStringValue = (String)o;
                break;
            }
            case 902: {
                this.mIntegerValue = (int)o;
                break;
            }
            case 901: {
                this.mFloatValue = (float)o;
                break;
            }
            case 900:
            case 906: {
                this.mIntegerValue = (int)o;
                break;
            }
        }
    }
    
    public void setValue(final float[] array) {
        final int mType = this.mType;
        boolean mBooleanValue = true;
        switch (mType) {
            case 904: {
                if (array[0] <= 0.5) {
                    mBooleanValue = false;
                }
                this.mBooleanValue = mBooleanValue;
                break;
            }
            case 903: {
                throw new RuntimeException("Color does not have a single color to interpolate");
            }
            case 902: {
                final int hsvToRgb = hsvToRgb(array[0], array[1], array[2]);
                this.mIntegerValue = hsvToRgb;
                this.mIntegerValue = (clamp((int)(array[3] * 255.0f)) << 24 | (hsvToRgb & 0xFFFFFF));
                break;
            }
            case 901:
            case 905: {
                this.mFloatValue = array[0];
                break;
            }
            case 900:
            case 906: {
                this.mIntegerValue = (int)array[0];
                break;
            }
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.mName);
        sb.append(':');
        final String string = sb.toString();
        switch (this.mType) {
            default: {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(string);
                sb2.append("????");
                return sb2.toString();
            }
            case 905: {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(string);
                sb3.append(this.mFloatValue);
                return sb3.toString();
            }
            case 904: {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append(string);
                sb4.append((Object)this.mBooleanValue);
                return sb4.toString();
            }
            case 903: {
                final StringBuilder sb5 = new StringBuilder();
                sb5.append(string);
                sb5.append(this.mStringValue);
                return sb5.toString();
            }
            case 902: {
                final StringBuilder sb6 = new StringBuilder();
                sb6.append(string);
                sb6.append(colorString(this.mIntegerValue));
                return sb6.toString();
            }
            case 901: {
                final StringBuilder sb7 = new StringBuilder();
                sb7.append(string);
                sb7.append(this.mFloatValue);
                return sb7.toString();
            }
            case 900: {
                final StringBuilder sb8 = new StringBuilder();
                sb8.append(string);
                sb8.append(this.mIntegerValue);
                return sb8.toString();
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.utils;

import java.io.PrintStream;

public class SpringStopEngine implements StopEngine
{
    private static final double UNSET = Double.MAX_VALUE;
    private int mBoundaryMode;
    double mDamping;
    private boolean mInitialized;
    private float mLastTime;
    private double mLastVelocity;
    private float mMass;
    private float mPos;
    private double mStiffness;
    private float mStopThreshold;
    private double mTargetPos;
    private float mV;
    
    public SpringStopEngine() {
        this.mDamping = 0.5;
        this.mInitialized = false;
        this.mBoundaryMode = 0;
    }
    
    private void compute(double n) {
        final double mStiffness = this.mStiffness;
        final double mDamping = this.mDamping;
        final int n2 = (int)(9.0 / (Math.sqrt(mStiffness / this.mMass) * n * 4.0) + 1.0);
        final double n3 = n / n2;
        int i = 0;
        n = mDamping;
        while (i < n2) {
            final float mPos = this.mPos;
            final double n4 = mPos;
            final double mTargetPos = this.mTargetPos;
            final double n5 = -mStiffness;
            final float mv = this.mV;
            final double n6 = mv;
            final float mMass = this.mMass;
            final double n7 = mv + (n5 * (n4 - mTargetPos) - n6 * n) / mMass * n3 / 2.0;
            final double n8 = (-(mPos + n3 * n7 / 2.0 - mTargetPos) * mStiffness - n7 * n) / mMass * n3;
            final double n9 = mv;
            final double n10 = n8 / 2.0;
            final float mv2 = (float)(mv + n8);
            this.mV = mv2;
            final float mPos2 = (float)(mPos + (n9 + n10) * n3);
            this.mPos = mPos2;
            final int mBoundaryMode = this.mBoundaryMode;
            if (mBoundaryMode > 0) {
                if (mPos2 < 0.0f && (mBoundaryMode & 0x1) == 0x1) {
                    this.mPos = -mPos2;
                    this.mV = -mv2;
                }
                final float mPos3 = this.mPos;
                if (mPos3 > 1.0f && (mBoundaryMode & 0x2) == 0x2) {
                    this.mPos = 2.0f - mPos3;
                    this.mV = -this.mV;
                }
            }
            ++i;
        }
    }
    
    @Override
    public String debug(final String s, final float n) {
        return null;
    }
    
    public float getAcceleration() {
        return (float)(-this.mStiffness * (this.mPos - this.mTargetPos) - this.mDamping * this.mV) / this.mMass;
    }
    
    @Override
    public float getInterpolation(final float mLastTime) {
        this.compute(mLastTime - this.mLastTime);
        this.mLastTime = mLastTime;
        return this.mPos;
    }
    
    @Override
    public float getVelocity() {
        return 0.0f;
    }
    
    @Override
    public float getVelocity(final float n) {
        return this.mV;
    }
    
    @Override
    public boolean isStopped() {
        final double n = this.mPos - this.mTargetPos;
        final double mStiffness = this.mStiffness;
        final double n2 = this.mV;
        return Math.sqrt((n2 * n2 * this.mMass + mStiffness * n * n) / mStiffness) <= this.mStopThreshold;
    }
    
    void log(final String str) {
        final StackTraceElement stackTraceElement = new Throwable().getStackTrace()[1];
        final StringBuilder sb = new StringBuilder();
        sb.append(".(");
        sb.append(stackTraceElement.getFileName());
        sb.append(":");
        sb.append(stackTraceElement.getLineNumber());
        sb.append(") ");
        sb.append(stackTraceElement.getMethodName());
        sb.append("() ");
        final String string = sb.toString();
        final PrintStream out = System.out;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(string);
        sb2.append(str);
        out.println(sb2.toString());
    }
    
    public void springConfig(final float mPos, final float n, final float n2, final float mMass, final float n3, final float n4, final float mStopThreshold, final int mBoundaryMode) {
        this.mTargetPos = n;
        this.mDamping = n4;
        this.mInitialized = false;
        this.mPos = mPos;
        this.mLastVelocity = n2;
        this.mStiffness = n3;
        this.mMass = mMass;
        this.mStopThreshold = mStopThreshold;
        this.mBoundaryMode = mBoundaryMode;
        this.mLastTime = 0.0f;
    }
}

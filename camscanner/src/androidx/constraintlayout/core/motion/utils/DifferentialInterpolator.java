// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.utils;

public interface DifferentialInterpolator
{
    float getInterpolation(final float p0);
    
    float getVelocity();
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.utils;

public class HyperSpline
{
    double[][] mCtl;
    Cubic[][] mCurve;
    double[] mCurveLength;
    int mDimensionality;
    int mPoints;
    double mTotalLength;
    
    public HyperSpline() {
    }
    
    public HyperSpline(final double[][] array) {
        this.setup(array);
    }
    
    static Cubic[] calcNaturalCubic(int i, final double[] array) {
        final double[] array2 = new double[i];
        final double[] array3 = new double[i];
        final double[] array4 = new double[i];
        final int n = i - 1;
        final int n2 = 0;
        array2[0] = 0.5;
        final int n3 = 1;
        for (i = 1; i < n; ++i) {
            array2[i] = 1.0 / (4.0 - array2[i - 1]);
        }
        final int n4 = n - 1;
        array2[n] = 1.0 / (2.0 - array2[n4]);
        array3[0] = (array[1] - array[0]) * 3.0 * array2[0];
        int n5;
        double n6;
        int n7;
        for (i = n3; i < n; i = n5) {
            n5 = i + 1;
            n6 = array[n5];
            n7 = i - 1;
            array3[i] = ((n6 - array[n7]) * 3.0 - array3[n7]) * array2[i];
        }
        array4[n] = (array3[n] = ((array[n] - array[n4]) * 3.0 - array3[n4]) * array2[n]);
        for (i = n4; i >= 0; --i) {
            array4[i] = array3[i] - array2[i] * array4[i + 1];
        }
        final Cubic[] array5 = new Cubic[n];
        double n8;
        double n9;
        double n10;
        int n11;
        double n12;
        double n13;
        for (i = n2; i < n; i = n11) {
            n8 = array[i];
            n9 = (float)n8;
            n10 = array4[i];
            n11 = i + 1;
            n12 = array[n11];
            n13 = array4[n11];
            array5[i] = new Cubic(n9, n10, (n12 - n8) * 3.0 - n10 * 2.0 - n13, (n8 - n12) * 2.0 + n10 + n13);
        }
        return array5;
    }
    
    public double approxLength(final Cubic[] array) {
        final int length = array.length;
        final double[] array2 = new double[array.length];
        final double n = 0.0;
        double n2 = 0.0;
        double n3 = 0.0;
        int i;
        double a;
        while (true) {
            i = 0;
            final int n4 = 0;
            a = n;
            if (n2 >= 1.0) {
                break;
            }
            double a2 = 0.0;
            for (int j = n4; j < array.length; ++j) {
                final double n5 = array2[j];
                final double eval = array[j].eval(n2);
                array2[j] = eval;
                final double n6 = n5 - eval;
                a2 += n6 * n6;
            }
            double n7 = n3;
            if (n2 > 0.0) {
                n7 = n3 + Math.sqrt(a2);
            }
            n2 += 0.1;
            n3 = n7;
        }
        while (i < array.length) {
            final double n8 = array2[i];
            final double eval2 = array[i].eval(1.0);
            array2[i] = eval2;
            final double n9 = n8 - eval2;
            a += n9 * n9;
            ++i;
        }
        return n3 + Math.sqrt(a);
    }
    
    public double getPos(double n, final int n2) {
        n *= this.mTotalLength;
        int n3 = 0;
        double[] mCurveLength;
        while (true) {
            mCurveLength = this.mCurveLength;
            if (n3 >= mCurveLength.length - 1) {
                break;
            }
            final double n4 = mCurveLength[n3];
            if (n4 >= n) {
                break;
            }
            n -= n4;
            ++n3;
        }
        return this.mCurve[n2][n3].eval(n / mCurveLength[n3]);
    }
    
    public void getPos(double n, final double[] array) {
        n *= this.mTotalLength;
        final int n2 = 0;
        int n3 = 0;
        int i;
        while (true) {
            final double[] mCurveLength = this.mCurveLength;
            i = n2;
            if (n3 >= mCurveLength.length - 1) {
                break;
            }
            final double n4 = mCurveLength[n3];
            i = n2;
            if (n4 >= n) {
                break;
            }
            n -= n4;
            ++n3;
        }
        while (i < array.length) {
            array[i] = this.mCurve[i][n3].eval(n / this.mCurveLength[n3]);
            ++i;
        }
    }
    
    public void getPos(double n, final float[] array) {
        n *= this.mTotalLength;
        final int n2 = 0;
        int n3 = 0;
        int i;
        while (true) {
            final double[] mCurveLength = this.mCurveLength;
            i = n2;
            if (n3 >= mCurveLength.length - 1) {
                break;
            }
            final double n4 = mCurveLength[n3];
            i = n2;
            if (n4 >= n) {
                break;
            }
            n -= n4;
            ++n3;
        }
        while (i < array.length) {
            array[i] = (float)this.mCurve[i][n3].eval(n / this.mCurveLength[n3]);
            ++i;
        }
    }
    
    public void getVelocity(double n, final double[] array) {
        n *= this.mTotalLength;
        final int n2 = 0;
        int n3 = 0;
        int i;
        while (true) {
            final double[] mCurveLength = this.mCurveLength;
            i = n2;
            if (n3 >= mCurveLength.length - 1) {
                break;
            }
            final double n4 = mCurveLength[n3];
            i = n2;
            if (n4 >= n) {
                break;
            }
            n -= n4;
            ++n3;
        }
        while (i < array.length) {
            array[i] = this.mCurve[i][n3].vel(n / this.mCurveLength[n3]);
            ++i;
        }
    }
    
    public void setup(final double[][] array) {
        final int length = array[0].length;
        this.mDimensionality = length;
        final int length2 = array.length;
        this.mPoints = length2;
        this.mCtl = new double[length][length2];
        this.mCurve = new Cubic[this.mDimensionality][];
        for (int i = 0; i < this.mDimensionality; ++i) {
            for (int j = 0; j < this.mPoints; ++j) {
                this.mCtl[i][j] = array[j][i];
            }
        }
        int n = 0;
        int mDimensionality;
        while (true) {
            mDimensionality = this.mDimensionality;
            if (n >= mDimensionality) {
                break;
            }
            final Cubic[][] mCurve = this.mCurve;
            final double[] array2 = this.mCtl[n];
            mCurve[n] = calcNaturalCubic(array2.length, array2);
            ++n;
        }
        this.mCurveLength = new double[this.mPoints - 1];
        this.mTotalLength = 0.0;
        final Cubic[] array3 = new Cubic[mDimensionality];
        for (int k = 0; k < this.mCurveLength.length; ++k) {
            for (int l = 0; l < this.mDimensionality; ++l) {
                array3[l] = this.mCurve[l][k];
            }
            final double mTotalLength = this.mTotalLength;
            final double[] mCurveLength = this.mCurveLength;
            final double approxLength = this.approxLength(array3);
            mCurveLength[k] = approxLength;
            this.mTotalLength = mTotalLength + approxLength;
        }
    }
    
    public static class Cubic
    {
        public static final double HALF = 0.5;
        public static final double THIRD = 0.3333333333333333;
        double mA;
        double mB;
        double mC;
        double mD;
        
        public Cubic(final double ma, final double mb, final double mc, final double md) {
            this.mA = ma;
            this.mB = mb;
            this.mC = mc;
            this.mD = md;
        }
        
        public double eval(final double n) {
            return ((this.mD * n + this.mC) * n + this.mB) * n + this.mA;
        }
        
        public double vel(final double n) {
            return (this.mD * 0.3333333333333333 * n + this.mC * 0.5) * n + this.mB;
        }
    }
}

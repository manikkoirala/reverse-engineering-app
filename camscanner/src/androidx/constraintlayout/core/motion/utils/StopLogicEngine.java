// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.utils;

public class StopLogicEngine implements StopEngine
{
    private static final float EPSILON = 1.0E-5f;
    private boolean mBackwards;
    private boolean mDone;
    private float mLastPosition;
    private int mNumberOfStages;
    private float mStage1Duration;
    private float mStage1EndPosition;
    private float mStage1Velocity;
    private float mStage2Duration;
    private float mStage2EndPosition;
    private float mStage2Velocity;
    private float mStage3Duration;
    private float mStage3EndPosition;
    private float mStage3Velocity;
    private float mStartPosition;
    private String mType;
    
    public StopLogicEngine() {
        this.mBackwards = false;
        this.mDone = false;
    }
    
    private float calcY(float n) {
        this.mDone = false;
        final float mStage1Duration = this.mStage1Duration;
        if (n <= mStage1Duration) {
            final float mStage1Velocity = this.mStage1Velocity;
            return mStage1Velocity * n + (this.mStage2Velocity - mStage1Velocity) * n * n / (mStage1Duration * 2.0f);
        }
        final int mNumberOfStages = this.mNumberOfStages;
        if (mNumberOfStages == 1) {
            return this.mStage1EndPosition;
        }
        final float n2 = n - mStage1Duration;
        n = this.mStage2Duration;
        if (n2 < n) {
            final float mStage1EndPosition = this.mStage1EndPosition;
            final float mStage2Velocity = this.mStage2Velocity;
            return mStage1EndPosition + mStage2Velocity * n2 + (this.mStage3Velocity - mStage2Velocity) * n2 * n2 / (n * 2.0f);
        }
        if (mNumberOfStages == 2) {
            return this.mStage2EndPosition;
        }
        final float n3 = n2 - n;
        n = this.mStage3Duration;
        if (n3 <= n) {
            final float mStage2EndPosition = this.mStage2EndPosition;
            final float mStage3Velocity = this.mStage3Velocity;
            return mStage2EndPosition + mStage3Velocity * n3 - mStage3Velocity * n3 * n3 / (n * 2.0f);
        }
        this.mDone = true;
        return this.mStage3EndPosition;
    }
    
    private void setup(float mStage3Duration, final float mStage3EndPosition, float n, float mStage3Velocity, float n2) {
        this.mDone = false;
        float n3 = mStage3Duration;
        if (mStage3Duration == 0.0f) {
            n3 = 1.0E-4f;
        }
        this.mStage1Velocity = n3;
        mStage3Duration = n3 / n;
        final float n4 = mStage3Duration * n3 / 2.0f;
        if (n3 < 0.0f) {
            mStage3Duration = (float)Math.sqrt((mStage3EndPosition - -n3 / n * n3 / 2.0f) * n);
            if (mStage3Duration < mStage3Velocity) {
                this.mType = "backward accelerate, decelerate";
                this.mNumberOfStages = 2;
                this.mStage1Velocity = n3;
                this.mStage2Velocity = mStage3Duration;
                this.mStage3Velocity = 0.0f;
                mStage3Velocity = (mStage3Duration - n3) / n;
                this.mStage1Duration = mStage3Velocity;
                this.mStage2Duration = mStage3Duration / n;
                this.mStage1EndPosition = (n3 + mStage3Duration) * mStage3Velocity / 2.0f;
                this.mStage2EndPosition = mStage3EndPosition;
                this.mStage3EndPosition = mStage3EndPosition;
                return;
            }
            this.mType = "backward accelerate cruse decelerate";
            this.mNumberOfStages = 3;
            this.mStage1Velocity = n3;
            this.mStage2Velocity = mStage3Velocity;
            this.mStage3Velocity = mStage3Velocity;
            n2 = (mStage3Velocity - n3) / n;
            this.mStage1Duration = n2;
            mStage3Duration = mStage3Velocity / n;
            this.mStage3Duration = mStage3Duration;
            n = (n3 + mStage3Velocity) * n2 / 2.0f;
            mStage3Duration = mStage3Duration * mStage3Velocity / 2.0f;
            this.mStage2Duration = (mStage3EndPosition - n - mStage3Duration) / mStage3Velocity;
            this.mStage1EndPosition = n;
            this.mStage2EndPosition = mStage3EndPosition - mStage3Duration;
            this.mStage3EndPosition = mStage3EndPosition;
        }
        else {
            if (n4 >= mStage3EndPosition) {
                this.mType = "hard stop";
                mStage3Duration = 2.0f * mStage3EndPosition / n3;
                this.mNumberOfStages = 1;
                this.mStage1Velocity = n3;
                this.mStage2Velocity = 0.0f;
                this.mStage1EndPosition = mStage3EndPosition;
                this.mStage1Duration = mStage3Duration;
                return;
            }
            final float mStage1EndPosition = mStage3EndPosition - n4;
            final float mStage1Duration = mStage1EndPosition / n3;
            if (mStage1Duration + mStage3Duration < n2) {
                this.mType = "cruse decelerate";
                this.mNumberOfStages = 2;
                this.mStage1Velocity = n3;
                this.mStage2Velocity = n3;
                this.mStage3Velocity = 0.0f;
                this.mStage1EndPosition = mStage1EndPosition;
                this.mStage2EndPosition = mStage3EndPosition;
                this.mStage1Duration = mStage1Duration;
                this.mStage2Duration = mStage3Duration;
                return;
            }
            final float mStage2Velocity = (float)Math.sqrt(n * mStage3EndPosition + n3 * n3 / 2.0f);
            mStage3Duration = (mStage2Velocity - n3) / n;
            this.mStage1Duration = mStage3Duration;
            n2 = mStage2Velocity / n;
            this.mStage2Duration = n2;
            if (mStage2Velocity < mStage3Velocity) {
                this.mType = "accelerate decelerate";
                this.mNumberOfStages = 2;
                this.mStage1Velocity = n3;
                this.mStage2Velocity = mStage2Velocity;
                this.mStage3Velocity = 0.0f;
                this.mStage1Duration = mStage3Duration;
                this.mStage2Duration = n2;
                this.mStage1EndPosition = (n3 + mStage2Velocity) * mStage3Duration / 2.0f;
                this.mStage2EndPosition = mStage3EndPosition;
                return;
            }
            this.mType = "accelerate cruse decelerate";
            this.mNumberOfStages = 3;
            this.mStage1Velocity = n3;
            this.mStage2Velocity = mStage3Velocity;
            this.mStage3Velocity = mStage3Velocity;
            n2 = (mStage3Velocity - n3) / n;
            this.mStage1Duration = n2;
            mStage3Duration = mStage3Velocity / n;
            this.mStage3Duration = mStage3Duration;
            n = (n3 + mStage3Velocity) * n2 / 2.0f;
            mStage3Duration = mStage3Duration * mStage3Velocity / 2.0f;
            this.mStage2Duration = (mStage3EndPosition - n - mStage3Duration) / mStage3Velocity;
            this.mStage1EndPosition = n;
            this.mStage2EndPosition = mStage3EndPosition - mStage3Duration;
            this.mStage3EndPosition = mStage3EndPosition;
        }
    }
    
    public void config(final float mStartPosition, final float n, final float n2, final float n3, final float n4, final float n5) {
        boolean mBackwards = false;
        this.mDone = false;
        this.mStartPosition = mStartPosition;
        if (mStartPosition > n) {
            mBackwards = true;
        }
        this.mBackwards = mBackwards;
        if (mBackwards) {
            this.setup(-n2, mStartPosition - n, n4, n5, n3);
        }
        else {
            this.setup(n2, n - mStartPosition, n4, n5, n3);
        }
    }
    
    @Override
    public String debug(final String str, float mStage2Duration) {
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" ===== ");
        sb.append(this.mType);
        sb.append("\n");
        final String string = sb.toString();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(string);
        sb2.append(str);
        String str2;
        if (this.mBackwards) {
            str2 = "backwards";
        }
        else {
            str2 = "forward ";
        }
        sb2.append(str2);
        sb2.append(" time = ");
        sb2.append(mStage2Duration);
        sb2.append("  stages ");
        sb2.append(this.mNumberOfStages);
        sb2.append("\n");
        final String string2 = sb2.toString();
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(string2);
        sb3.append(str);
        sb3.append(" dur ");
        sb3.append(this.mStage1Duration);
        sb3.append(" vel ");
        sb3.append(this.mStage1Velocity);
        sb3.append(" pos ");
        sb3.append(this.mStage1EndPosition);
        sb3.append("\n");
        String str4;
        final String str3 = str4 = sb3.toString();
        if (this.mNumberOfStages > 1) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append(str3);
            sb4.append(str);
            sb4.append(" dur ");
            sb4.append(this.mStage2Duration);
            sb4.append(" vel ");
            sb4.append(this.mStage2Velocity);
            sb4.append(" pos ");
            sb4.append(this.mStage2EndPosition);
            sb4.append("\n");
            str4 = sb4.toString();
        }
        String string3 = str4;
        if (this.mNumberOfStages > 2) {
            final StringBuilder sb5 = new StringBuilder();
            sb5.append(str4);
            sb5.append(str);
            sb5.append(" dur ");
            sb5.append(this.mStage3Duration);
            sb5.append(" vel ");
            sb5.append(this.mStage3Velocity);
            sb5.append(" pos ");
            sb5.append(this.mStage3EndPosition);
            sb5.append("\n");
            string3 = sb5.toString();
        }
        final float mStage1Duration = this.mStage1Duration;
        if (mStage2Duration <= mStage1Duration) {
            final StringBuilder sb6 = new StringBuilder();
            sb6.append(string3);
            sb6.append(str);
            sb6.append("stage 0\n");
            return sb6.toString();
        }
        final int mNumberOfStages = this.mNumberOfStages;
        if (mNumberOfStages == 1) {
            final StringBuilder sb7 = new StringBuilder();
            sb7.append(string3);
            sb7.append(str);
            sb7.append("end stage 0\n");
            return sb7.toString();
        }
        final float n = mStage2Duration - mStage1Duration;
        mStage2Duration = this.mStage2Duration;
        if (n < mStage2Duration) {
            final StringBuilder sb8 = new StringBuilder();
            sb8.append(string3);
            sb8.append(str);
            sb8.append(" stage 1\n");
            return sb8.toString();
        }
        if (mNumberOfStages == 2) {
            final StringBuilder sb9 = new StringBuilder();
            sb9.append(string3);
            sb9.append(str);
            sb9.append("end stage 1\n");
            return sb9.toString();
        }
        if (n - mStage2Duration < this.mStage3Duration) {
            final StringBuilder sb10 = new StringBuilder();
            sb10.append(string3);
            sb10.append(str);
            sb10.append(" stage 2\n");
            return sb10.toString();
        }
        final StringBuilder sb11 = new StringBuilder();
        sb11.append(string3);
        sb11.append(str);
        sb11.append(" end stage 2\n");
        return sb11.toString();
    }
    
    @Override
    public float getInterpolation(float mLastPosition) {
        final float calcY = this.calcY(mLastPosition);
        this.mLastPosition = mLastPosition;
        if (this.mBackwards) {
            mLastPosition = this.mStartPosition - calcY;
        }
        else {
            mLastPosition = this.mStartPosition + calcY;
        }
        return mLastPosition;
    }
    
    @Override
    public float getVelocity() {
        float velocity;
        if (this.mBackwards) {
            velocity = -this.getVelocity(this.mLastPosition);
        }
        else {
            velocity = this.getVelocity(this.mLastPosition);
        }
        return velocity;
    }
    
    @Override
    public float getVelocity(float mStage2Duration) {
        final float mStage1Duration = this.mStage1Duration;
        if (mStage2Duration <= mStage1Duration) {
            final float mStage1Velocity = this.mStage1Velocity;
            return mStage1Velocity + (this.mStage2Velocity - mStage1Velocity) * mStage2Duration / mStage1Duration;
        }
        final int mNumberOfStages = this.mNumberOfStages;
        if (mNumberOfStages == 1) {
            return 0.0f;
        }
        final float n = mStage2Duration - mStage1Duration;
        mStage2Duration = this.mStage2Duration;
        if (n < mStage2Duration) {
            final float mStage2Velocity = this.mStage2Velocity;
            return mStage2Velocity + (this.mStage3Velocity - mStage2Velocity) * n / mStage2Duration;
        }
        if (mNumberOfStages == 2) {
            return this.mStage2EndPosition;
        }
        mStage2Duration = n - mStage2Duration;
        final float mStage3Duration = this.mStage3Duration;
        if (mStage2Duration < mStage3Duration) {
            final float mStage3Velocity = this.mStage3Velocity;
            return mStage3Velocity - mStage2Duration * mStage3Velocity / mStage3Duration;
        }
        return this.mStage3EndPosition;
    }
    
    @Override
    public boolean isStopped() {
        return this.getVelocity() < 1.0E-5f && Math.abs(this.mStage3EndPosition - this.mLastPosition) < 1.0E-5f;
    }
}

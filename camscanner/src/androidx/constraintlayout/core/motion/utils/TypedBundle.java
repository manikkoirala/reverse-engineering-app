// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.utils;

import java.util.Arrays;

public class TypedBundle
{
    private static final int INITIAL_BOOLEAN = 4;
    private static final int INITIAL_FLOAT = 10;
    private static final int INITIAL_INT = 10;
    private static final int INITIAL_STRING = 5;
    int mCountBoolean;
    int mCountFloat;
    int mCountInt;
    int mCountString;
    int[] mTypeBoolean;
    int[] mTypeFloat;
    int[] mTypeInt;
    int[] mTypeString;
    boolean[] mValueBoolean;
    float[] mValueFloat;
    int[] mValueInt;
    String[] mValueString;
    
    public TypedBundle() {
        this.mTypeInt = new int[10];
        this.mValueInt = new int[10];
        this.mCountInt = 0;
        this.mTypeFloat = new int[10];
        this.mValueFloat = new float[10];
        this.mCountFloat = 0;
        this.mTypeString = new int[5];
        this.mValueString = new String[5];
        this.mCountString = 0;
        this.mTypeBoolean = new int[4];
        this.mValueBoolean = new boolean[4];
        this.mCountBoolean = 0;
    }
    
    public void add(final int n, final float n2) {
        final int mCountFloat = this.mCountFloat;
        final int[] mTypeFloat = this.mTypeFloat;
        if (mCountFloat >= mTypeFloat.length) {
            this.mTypeFloat = Arrays.copyOf(mTypeFloat, mTypeFloat.length * 2);
            final float[] mValueFloat = this.mValueFloat;
            this.mValueFloat = Arrays.copyOf(mValueFloat, mValueFloat.length * 2);
        }
        final int[] mTypeFloat2 = this.mTypeFloat;
        final int mCountFloat2 = this.mCountFloat;
        mTypeFloat2[mCountFloat2] = n;
        final float[] mValueFloat2 = this.mValueFloat;
        this.mCountFloat = mCountFloat2 + 1;
        mValueFloat2[mCountFloat2] = n2;
    }
    
    public void add(final int n, final int n2) {
        final int mCountInt = this.mCountInt;
        final int[] mTypeInt = this.mTypeInt;
        if (mCountInt >= mTypeInt.length) {
            this.mTypeInt = Arrays.copyOf(mTypeInt, mTypeInt.length * 2);
            final int[] mValueInt = this.mValueInt;
            this.mValueInt = Arrays.copyOf(mValueInt, mValueInt.length * 2);
        }
        final int[] mTypeInt2 = this.mTypeInt;
        final int mCountInt2 = this.mCountInt;
        mTypeInt2[mCountInt2] = n;
        final int[] mValueInt2 = this.mValueInt;
        this.mCountInt = mCountInt2 + 1;
        mValueInt2[mCountInt2] = n2;
    }
    
    public void add(final int n, final String s) {
        final int mCountString = this.mCountString;
        final int[] mTypeString = this.mTypeString;
        if (mCountString >= mTypeString.length) {
            this.mTypeString = Arrays.copyOf(mTypeString, mTypeString.length * 2);
            final String[] mValueString = this.mValueString;
            this.mValueString = Arrays.copyOf(mValueString, mValueString.length * 2);
        }
        final int[] mTypeString2 = this.mTypeString;
        final int mCountString2 = this.mCountString;
        mTypeString2[mCountString2] = n;
        final String[] mValueString2 = this.mValueString;
        this.mCountString = mCountString2 + 1;
        mValueString2[mCountString2] = s;
    }
    
    public void add(final int n, final boolean b) {
        final int mCountBoolean = this.mCountBoolean;
        final int[] mTypeBoolean = this.mTypeBoolean;
        if (mCountBoolean >= mTypeBoolean.length) {
            this.mTypeBoolean = Arrays.copyOf(mTypeBoolean, mTypeBoolean.length * 2);
            final boolean[] mValueBoolean = this.mValueBoolean;
            this.mValueBoolean = Arrays.copyOf(mValueBoolean, mValueBoolean.length * 2);
        }
        final int[] mTypeBoolean2 = this.mTypeBoolean;
        final int mCountBoolean2 = this.mCountBoolean;
        mTypeBoolean2[mCountBoolean2] = n;
        final boolean[] mValueBoolean2 = this.mValueBoolean;
        this.mCountBoolean = mCountBoolean2 + 1;
        mValueBoolean2[mCountBoolean2] = b;
    }
    
    public void addIfNotNull(final int n, final String s) {
        if (s != null) {
            this.add(n, s);
        }
    }
    
    public void applyDelta(final TypedValues typedValues) {
        final int n = 0;
        for (int i = 0; i < this.mCountInt; ++i) {
            typedValues.setValue(this.mTypeInt[i], this.mValueInt[i]);
        }
        for (int j = 0; j < this.mCountFloat; ++j) {
            typedValues.setValue(this.mTypeFloat[j], this.mValueFloat[j]);
        }
        int n2 = 0;
        int k;
        while (true) {
            k = n;
            if (n2 >= this.mCountString) {
                break;
            }
            typedValues.setValue(this.mTypeString[n2], this.mValueString[n2]);
            ++n2;
        }
        while (k < this.mCountBoolean) {
            typedValues.setValue(this.mTypeBoolean[k], this.mValueBoolean[k]);
            ++k;
        }
    }
    
    public void clear() {
        this.mCountBoolean = 0;
        this.mCountString = 0;
        this.mCountFloat = 0;
        this.mCountInt = 0;
    }
    
    public int getInteger(final int n) {
        for (int i = 0; i < this.mCountInt; ++i) {
            if (this.mTypeInt[i] == n) {
                return this.mValueInt[i];
            }
        }
        return -1;
    }
}

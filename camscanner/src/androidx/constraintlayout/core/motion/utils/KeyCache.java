// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.utils;

import java.util.Arrays;
import java.util.HashMap;

public class KeyCache
{
    HashMap<Object, HashMap<String, float[]>> map;
    
    public KeyCache() {
        this.map = new HashMap<Object, HashMap<String, float[]>>();
    }
    
    public float getFloatValue(final Object o, final String s, final int n) {
        if (!this.map.containsKey(o)) {
            return Float.NaN;
        }
        final HashMap hashMap = this.map.get(o);
        if (hashMap != null) {
            if (hashMap.containsKey(s)) {
                final float[] array = (float[])hashMap.get(s);
                if (array == null) {
                    return Float.NaN;
                }
                if (array.length > n) {
                    return array[n];
                }
            }
        }
        return Float.NaN;
    }
    
    public void setFloatValue(final Object o, final String key, final int n, final float n2) {
        if (!this.map.containsKey(o)) {
            final HashMap value = new HashMap();
            final float[] value2 = new float[n + 1];
            value2[n] = n2;
            value.put(key, value2);
            this.map.put(o, value);
        }
        else {
            HashMap value3;
            if ((value3 = this.map.get(o)) == null) {
                value3 = new HashMap();
            }
            if (!value3.containsKey(key)) {
                final float[] value4 = new float[n + 1];
                value4[n] = n2;
                value3.put(key, value4);
                this.map.put(o, value3);
            }
            else {
                float[] original;
                if ((original = (float[])value3.get(key)) == null) {
                    original = new float[0];
                }
                float[] copy = original;
                if (original.length <= n) {
                    copy = Arrays.copyOf(original, n + 1);
                }
                copy[n] = n2;
                value3.put(key, copy);
            }
        }
    }
}

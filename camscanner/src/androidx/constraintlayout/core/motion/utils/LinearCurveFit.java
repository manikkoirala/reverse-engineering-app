// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.utils;

public class LinearCurveFit extends CurveFit
{
    private static final String TAG = "LinearCurveFit";
    private boolean mExtrapolate;
    double[] mSlopeTemp;
    private double[] mT;
    private double mTotalLength;
    private double[][] mY;
    
    public LinearCurveFit(final double[] mt, final double[][] my) {
        this.mTotalLength = Double.NaN;
        this.mExtrapolate = true;
        final int length = mt.length;
        final int length2 = my[0].length;
        this.mSlopeTemp = new double[length2];
        this.mT = mt;
        this.mY = my;
        if (length2 > 2) {
            double n = 0.0;
            double n2 = 0.0;
            double n3;
            for (int i = 0; i < mt.length; ++i, n2 = (n = n3)) {
                n3 = my[i][0];
                if (i > 0) {
                    Math.hypot(n3 - n, n3 - n2);
                }
            }
            this.mTotalLength = 0.0;
        }
    }
    
    private double getLength2D(double n) {
        if (Double.isNaN(this.mTotalLength)) {
            return 0.0;
        }
        final double[] mt = this.mT;
        final int length = mt.length;
        if (n <= mt[0]) {
            return 0.0;
        }
        final int n2 = length - 1;
        if (n >= mt[n2]) {
            return this.mTotalLength;
        }
        double n3 = 0.0;
        double n5;
        double n4 = n5 = 0.0;
        double n6;
        double n7;
        double n8;
        int n10;
        for (int i = 0; i < n2; i = n10, n4 = n6, n5 = n7, n3 = n8) {
            final double[] array = this.mY[i];
            n6 = array[0];
            n7 = array[1];
            n8 = n3;
            if (i > 0) {
                n8 = n3 + Math.hypot(n6 - n4, n7 - n5);
            }
            final double[] mt2 = this.mT;
            final double n9 = mt2[i];
            if (n == n9) {
                return n8;
            }
            n10 = i + 1;
            final double n11 = mt2[n10];
            if (n < n11) {
                n = (n - n9) / (n11 - n9);
                final double[][] my = this.mY;
                final double[] array2 = my[i];
                final double n12 = array2[0];
                final double[] array3 = my[n10];
                final double n13 = array3[0];
                final double n14 = array2[1];
                final double n15 = array3[1];
                final double n16 = 1.0 - n;
                return n8 + Math.hypot(n7 - (n14 * n16 + n15 * n), n6 - (n12 * n16 + n13 * n));
            }
        }
        return 0.0;
    }
    
    @Override
    public double getPos(double n, final int n2) {
        final double[] mt = this.mT;
        final int length = mt.length;
        final boolean mExtrapolate = this.mExtrapolate;
        int i = 0;
        Label_0174: {
            if (mExtrapolate) {
                final double n3 = mt[0];
                double n5;
                double slope;
                if (n <= n3) {
                    final double n4 = this.mY[0][n2];
                    n5 = n - n3;
                    slope = this.getSlope(n3, n2);
                    n = n4;
                }
                else {
                    final int n6 = length - 1;
                    final double n7 = mt[n6];
                    if (n < n7) {
                        break Label_0174;
                    }
                    final double n8 = this.mY[n6][n2];
                    n5 = n - n7;
                    final double slope2 = this.getSlope(n7, n2);
                    n = n8;
                    slope = slope2;
                }
                return n + n5 * slope;
            }
            if (n <= mt[0]) {
                return this.mY[0][n2];
            }
            final int n9 = length - 1;
            if (n >= mt[n9]) {
                return this.mY[n9][n2];
            }
        }
        while (i < length - 1) {
            final double[] mt2 = this.mT;
            final double n10 = mt2[i];
            if (n == n10) {
                return this.mY[i][n2];
            }
            final int n11 = i + 1;
            final double n12 = mt2[n11];
            if (n < n12) {
                n = (n - n10) / (n12 - n10);
                final double[][] my = this.mY;
                return my[i][n2] * (1.0 - n) + my[n11][n2] * n;
            }
            i = n11;
        }
        return 0.0;
    }
    
    @Override
    public void getPos(double n, final double[] array) {
        final double[] mt = this.mT;
        final int length = mt.length;
        final double[][] my = this.mY;
        int i = 0;
        final int n2 = 0;
        final int n3 = 0;
        final int length2 = my[0].length;
        if (this.mExtrapolate) {
            final double n4 = mt[0];
            if (n <= n4) {
                this.getSlope(n4, this.mSlopeTemp);
                for (int j = 0; j < length2; ++j) {
                    array[j] = this.mY[0][j] + (n - this.mT[0]) * this.mSlopeTemp[j];
                }
                return;
            }
            final int n5 = length - 1;
            final double n6 = mt[n5];
            if (n >= n6) {
                this.getSlope(n6, this.mSlopeTemp);
                for (int k = n3; k < length2; ++k) {
                    array[k] = this.mY[n5][k] + (n - this.mT[n5]) * this.mSlopeTemp[k];
                }
                return;
            }
        }
        else {
            if (n <= mt[0]) {
                for (int l = 0; l < length2; ++l) {
                    array[l] = this.mY[0][l];
                }
                return;
            }
            final int n7 = length - 1;
            if (n >= mt[n7]) {
                while (i < length2) {
                    array[i] = this.mY[n7][i];
                    ++i;
                }
                return;
            }
        }
        int n10;
        for (int n8 = 0; n8 < length - 1; n8 = n10) {
            if (n == this.mT[n8]) {
                for (int n9 = 0; n9 < length2; ++n9) {
                    array[n9] = this.mY[n8][n9];
                }
            }
            final double[] mt2 = this.mT;
            n10 = n8 + 1;
            final double n11 = mt2[n10];
            if (n < n11) {
                final double n12 = mt2[n8];
                n = (n - n12) / (n11 - n12);
                for (int n13 = n2; n13 < length2; ++n13) {
                    final double[][] my2 = this.mY;
                    array[n13] = my2[n8][n13] * (1.0 - n) + my2[n10][n13] * n;
                }
                return;
            }
        }
    }
    
    @Override
    public void getPos(double n, final float[] array) {
        final double[] mt = this.mT;
        final int length = mt.length;
        final double[][] my = this.mY;
        int i = 0;
        final int n2 = 0;
        final int n3 = 0;
        final int length2 = my[0].length;
        if (this.mExtrapolate) {
            final double n4 = mt[0];
            if (n <= n4) {
                this.getSlope(n4, this.mSlopeTemp);
                for (int j = 0; j < length2; ++j) {
                    array[j] = (float)(this.mY[0][j] + (n - this.mT[0]) * this.mSlopeTemp[j]);
                }
                return;
            }
            final int n5 = length - 1;
            final double n6 = mt[n5];
            if (n >= n6) {
                this.getSlope(n6, this.mSlopeTemp);
                for (int k = n3; k < length2; ++k) {
                    array[k] = (float)(this.mY[n5][k] + (n - this.mT[n5]) * this.mSlopeTemp[k]);
                }
                return;
            }
        }
        else {
            if (n <= mt[0]) {
                for (int l = 0; l < length2; ++l) {
                    array[l] = (float)this.mY[0][l];
                }
                return;
            }
            final int n7 = length - 1;
            if (n >= mt[n7]) {
                while (i < length2) {
                    array[i] = (float)this.mY[n7][i];
                    ++i;
                }
                return;
            }
        }
        int n10;
        for (int n8 = 0; n8 < length - 1; n8 = n10) {
            if (n == this.mT[n8]) {
                for (int n9 = 0; n9 < length2; ++n9) {
                    array[n9] = (float)this.mY[n8][n9];
                }
            }
            final double[] mt2 = this.mT;
            n10 = n8 + 1;
            final double n11 = mt2[n10];
            if (n < n11) {
                final double n12 = mt2[n8];
                n = (n - n12) / (n11 - n12);
                for (int n13 = n2; n13 < length2; ++n13) {
                    final double[][] my2 = this.mY;
                    array[n13] = (float)(my2[n8][n13] * (1.0 - n) + my2[n10][n13] * n);
                }
                return;
            }
        }
    }
    
    @Override
    public double getSlope(double n, final int n2) {
        final double[] mt = this.mT;
        final int length = mt.length;
        final int n3 = 0;
        final double n4 = mt[0];
        while (true) {
            Label_0030: {
                if (n < n4) {
                    n = n4;
                    break Label_0030;
                }
                final double n5 = mt[length - 1];
                int i = n3;
                final double n6 = n;
                if (n >= n5) {
                    n = n5;
                    break Label_0030;
                }
                while (i < length - 1) {
                    final double[] mt2 = this.mT;
                    final int n7 = i + 1;
                    n = mt2[n7];
                    if (n6 <= n) {
                        final double n8 = mt2[i];
                        final double[][] my = this.mY;
                        return (my[n7][n2] - my[i][n2]) / (n - n8);
                    }
                    i = n7;
                }
                return 0.0;
            }
            int i = n3;
            final double n6 = n;
            continue;
        }
    }
    
    @Override
    public void getSlope(double n, final double[] array) {
        final double[] mt = this.mT;
        final int length = mt.length;
        final double[][] my = this.mY;
        int i = 0;
        final int length2 = my[0].length;
        final double n2 = mt[0];
        double n4 = 0.0;
        Label_0074: {
            if (n <= n2) {
                n = n2;
            }
            else {
                final double n3 = mt[length - 1];
                n4 = n;
                if (n < n3) {
                    break Label_0074;
                }
                n = n3;
            }
            n4 = n;
        }
        int n5;
        for (int j = 0; j < length - 1; j = n5) {
            final double[] mt2 = this.mT;
            n5 = j + 1;
            n = mt2[n5];
            if (n4 <= n) {
                final double n6 = mt2[j];
                while (i < length2) {
                    final double[][] my2 = this.mY;
                    array[i] = (my2[n5][i] - my2[j][i]) / (n - n6);
                    ++i;
                }
                break;
            }
        }
    }
    
    @Override
    public double[] getTimePoints() {
        return this.mT;
    }
}

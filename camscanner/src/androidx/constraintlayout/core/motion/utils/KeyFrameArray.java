// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.utils;

import androidx.constraintlayout.core.motion.CustomVariable;
import java.io.PrintStream;
import java.util.Arrays;
import androidx.constraintlayout.core.motion.CustomAttribute;

public class KeyFrameArray
{
    public static class CustomArray
    {
        private static final int EMPTY = 999;
        int count;
        int[] keys;
        CustomAttribute[] values;
        
        public CustomArray() {
            this.keys = new int[101];
            this.values = new CustomAttribute[101];
            this.clear();
        }
        
        public void append(final int n, final CustomAttribute customAttribute) {
            if (this.values[n] != null) {
                this.remove(n);
            }
            this.values[n] = customAttribute;
            final int[] keys = this.keys;
            keys[this.count++] = n;
            Arrays.sort(keys);
        }
        
        public void clear() {
            Arrays.fill(this.keys, 999);
            Arrays.fill(this.values, null);
            this.count = 0;
        }
        
        public void dump() {
            final PrintStream out = System.out;
            final StringBuilder sb = new StringBuilder();
            sb.append("V: ");
            sb.append(Arrays.toString(Arrays.copyOf(this.keys, this.count)));
            out.println(sb.toString());
            System.out.print("K: [");
            for (int i = 0; i < this.count; ++i) {
                final PrintStream out2 = System.out;
                final StringBuilder sb2 = new StringBuilder();
                String str;
                if (i == 0) {
                    str = "";
                }
                else {
                    str = ", ";
                }
                sb2.append(str);
                sb2.append(this.valueAt(i));
                out2.print(sb2.toString());
            }
            System.out.println("]");
        }
        
        public int keyAt(final int n) {
            return this.keys[n];
        }
        
        public void remove(final int n) {
            this.values[n] = null;
            int n2 = 0;
            int n3 = 0;
            int count;
            while (true) {
                count = this.count;
                if (n2 >= count) {
                    break;
                }
                final int[] keys = this.keys;
                int n4 = n3;
                if (n == keys[n2]) {
                    keys[n2] = 999;
                    n4 = n3 + 1;
                }
                if (n2 != n4) {
                    keys[n2] = keys[n4];
                }
                n3 = n4 + 1;
                ++n2;
            }
            this.count = count - 1;
        }
        
        public int size() {
            return this.count;
        }
        
        public CustomAttribute valueAt(final int n) {
            return this.values[this.keys[n]];
        }
    }
    
    public static class CustomVar
    {
        private static final int EMPTY = 999;
        int count;
        int[] keys;
        CustomVariable[] values;
        
        public CustomVar() {
            this.keys = new int[101];
            this.values = new CustomVariable[101];
            this.clear();
        }
        
        public void append(final int n, final CustomVariable customVariable) {
            if (this.values[n] != null) {
                this.remove(n);
            }
            this.values[n] = customVariable;
            final int[] keys = this.keys;
            keys[this.count++] = n;
            Arrays.sort(keys);
        }
        
        public void clear() {
            Arrays.fill(this.keys, 999);
            Arrays.fill(this.values, null);
            this.count = 0;
        }
        
        public void dump() {
            final PrintStream out = System.out;
            final StringBuilder sb = new StringBuilder();
            sb.append("V: ");
            sb.append(Arrays.toString(Arrays.copyOf(this.keys, this.count)));
            out.println(sb.toString());
            System.out.print("K: [");
            for (int i = 0; i < this.count; ++i) {
                final PrintStream out2 = System.out;
                final StringBuilder sb2 = new StringBuilder();
                String str;
                if (i == 0) {
                    str = "";
                }
                else {
                    str = ", ";
                }
                sb2.append(str);
                sb2.append(this.valueAt(i));
                out2.print(sb2.toString());
            }
            System.out.println("]");
        }
        
        public int keyAt(final int n) {
            return this.keys[n];
        }
        
        public void remove(final int n) {
            this.values[n] = null;
            int n2 = 0;
            int n3 = 0;
            int count;
            while (true) {
                count = this.count;
                if (n2 >= count) {
                    break;
                }
                final int[] keys = this.keys;
                int n4 = n3;
                if (n == keys[n2]) {
                    keys[n2] = 999;
                    n4 = n3 + 1;
                }
                if (n2 != n4) {
                    keys[n2] = keys[n4];
                }
                n3 = n4 + 1;
                ++n2;
            }
            this.count = count - 1;
        }
        
        public int size() {
            return this.count;
        }
        
        public CustomVariable valueAt(final int n) {
            return this.values[this.keys[n]];
        }
    }
    
    static class FloatArray
    {
        private static final int EMPTY = 999;
        int count;
        int[] keys;
        float[][] values;
        
        public FloatArray() {
            this.keys = new int[101];
            this.values = new float[101][];
            this.clear();
        }
        
        public void append(final int n, final float[] array) {
            if (this.values[n] != null) {
                this.remove(n);
            }
            this.values[n] = array;
            final int[] keys = this.keys;
            keys[this.count++] = n;
            Arrays.sort(keys);
        }
        
        public void clear() {
            Arrays.fill(this.keys, 999);
            Arrays.fill(this.values, null);
            this.count = 0;
        }
        
        public void dump() {
            final PrintStream out = System.out;
            final StringBuilder sb = new StringBuilder();
            sb.append("V: ");
            sb.append(Arrays.toString(Arrays.copyOf(this.keys, this.count)));
            out.println(sb.toString());
            System.out.print("K: [");
            for (int i = 0; i < this.count; ++i) {
                final PrintStream out2 = System.out;
                final StringBuilder sb2 = new StringBuilder();
                String str;
                if (i == 0) {
                    str = "";
                }
                else {
                    str = ", ";
                }
                sb2.append(str);
                sb2.append(Arrays.toString(this.valueAt(i)));
                out2.print(sb2.toString());
            }
            System.out.println("]");
        }
        
        public int keyAt(final int n) {
            return this.keys[n];
        }
        
        public void remove(final int n) {
            this.values[n] = null;
            int n2 = 0;
            int n3 = 0;
            int count;
            while (true) {
                count = this.count;
                if (n2 >= count) {
                    break;
                }
                final int[] keys = this.keys;
                int n4 = n3;
                if (n == keys[n2]) {
                    keys[n2] = 999;
                    n4 = n3 + 1;
                }
                if (n2 != n4) {
                    keys[n2] = keys[n4];
                }
                n3 = n4 + 1;
                ++n2;
            }
            this.count = count - 1;
        }
        
        public int size() {
            return this.count;
        }
        
        public float[] valueAt(final int n) {
            return this.values[this.keys[n]];
        }
    }
}

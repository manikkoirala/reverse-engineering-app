// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.utils;

public interface StopEngine
{
    String debug(final String p0, final float p1);
    
    float getInterpolation(final float p0);
    
    float getVelocity();
    
    float getVelocity(final float p0);
    
    boolean isStopped();
}

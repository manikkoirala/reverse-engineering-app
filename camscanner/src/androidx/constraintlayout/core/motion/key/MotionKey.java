// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion.key;

import java.util.HashSet;
import androidx.constraintlayout.core.motion.utils.SplineSet;
import androidx.constraintlayout.core.motion.CustomVariable;
import java.util.HashMap;
import androidx.constraintlayout.core.motion.utils.TypedValues;

public abstract class MotionKey implements TypedValues
{
    public static final String ALPHA = "alpha";
    public static final String CUSTOM = "CUSTOM";
    public static final String ELEVATION = "elevation";
    public static final String ROTATION = "rotationZ";
    public static final String ROTATION_X = "rotationX";
    public static final String SCALE_X = "scaleX";
    public static final String SCALE_Y = "scaleY";
    public static final String TRANSITION_PATH_ROTATE = "transitionPathRotate";
    public static final String TRANSLATION_X = "translationX";
    public static final String TRANSLATION_Y = "translationY";
    public static int UNSET = -1;
    public static final String VISIBILITY = "visibility";
    public HashMap<String, CustomVariable> mCustom;
    public int mFramePosition;
    int mTargetId;
    String mTargetString;
    public int mType;
    
    public MotionKey() {
        final int unset = MotionKey.UNSET;
        this.mFramePosition = unset;
        this.mTargetId = unset;
        this.mTargetString = null;
    }
    
    public abstract void addValues(final HashMap<String, SplineSet> p0);
    
    public abstract MotionKey clone();
    
    public MotionKey copy(final MotionKey motionKey) {
        this.mFramePosition = motionKey.mFramePosition;
        this.mTargetId = motionKey.mTargetId;
        this.mTargetString = motionKey.mTargetString;
        this.mType = motionKey.mType;
        return this;
    }
    
    public abstract void getAttributeNames(final HashSet<String> p0);
    
    public int getFramePosition() {
        return this.mFramePosition;
    }
    
    boolean matches(final String s) {
        final String mTargetString = this.mTargetString;
        return mTargetString != null && s != null && s.matches(mTargetString);
    }
    
    public void setCustomAttribute(final String key, final int n, final float n2) {
        this.mCustom.put(key, new CustomVariable(key, n, n2));
    }
    
    public void setCustomAttribute(final String key, final int n, final int n2) {
        this.mCustom.put(key, new CustomVariable(key, n, n2));
    }
    
    public void setCustomAttribute(final String key, final int n, final String s) {
        this.mCustom.put(key, new CustomVariable(key, n, s));
    }
    
    public void setCustomAttribute(final String key, final int n, final boolean b) {
        this.mCustom.put(key, new CustomVariable(key, n, b));
    }
    
    public void setFramePosition(final int mFramePosition) {
        this.mFramePosition = mFramePosition;
    }
    
    public void setInterpolation(final HashMap<String, Integer> hashMap) {
    }
    
    @Override
    public boolean setValue(final int n, final float n2) {
        return false;
    }
    
    @Override
    public boolean setValue(final int n, final int mFramePosition) {
        if (n != 100) {
            return false;
        }
        this.mFramePosition = mFramePosition;
        return true;
    }
    
    @Override
    public boolean setValue(final int n, final String mTargetString) {
        if (n != 101) {
            return false;
        }
        this.mTargetString = mTargetString;
        return true;
    }
    
    @Override
    public boolean setValue(final int n, final boolean b) {
        return false;
    }
    
    public MotionKey setViewId(final int mTargetId) {
        this.mTargetId = mTargetId;
        return this;
    }
    
    boolean toBoolean(final Object o) {
        boolean b;
        if (o instanceof Boolean) {
            b = (boolean)o;
        }
        else {
            b = Boolean.parseBoolean(o.toString());
        }
        return b;
    }
    
    float toFloat(final Object o) {
        float n;
        if (o instanceof Float) {
            n = (float)o;
        }
        else {
            n = Float.parseFloat(o.toString());
        }
        return n;
    }
    
    int toInt(final Object o) {
        int n;
        if (o instanceof Integer) {
            n = (int)o;
        }
        else {
            n = Integer.parseInt(o.toString());
        }
        return n;
    }
}

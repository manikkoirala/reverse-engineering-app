// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion;

import androidx.constraintlayout.core.motion.key.MotionKeyAttributes;
import androidx.constraintlayout.core.motion.utils.KeyFrameArray;
import androidx.constraintlayout.core.motion.key.MotionKeyTimeCycle;
import androidx.constraintlayout.core.motion.key.MotionKeyCycle;
import java.util.HashSet;
import androidx.constraintlayout.core.motion.utils.ViewState;
import androidx.constraintlayout.core.motion.utils.TypedValues;
import androidx.constraintlayout.core.motion.utils.KeyCache;
import androidx.constraintlayout.core.motion.utils.VelocityMatrix;
import androidx.constraintlayout.core.motion.utils.FloatRect;
import androidx.constraintlayout.core.motion.key.MotionKeyPosition;
import java.util.Arrays;
import java.util.Collection;
import androidx.constraintlayout.core.motion.utils.Utils;
import java.util.List;
import java.util.Collections;
import java.util.Iterator;
import androidx.constraintlayout.core.motion.utils.Easing;
import androidx.constraintlayout.core.motion.utils.TimeCycleSplineSet;
import androidx.constraintlayout.core.motion.utils.Rect;
import androidx.constraintlayout.core.motion.utils.DifferentialInterpolator;
import androidx.constraintlayout.core.motion.key.MotionKeyTrigger;
import androidx.constraintlayout.core.motion.key.MotionKey;
import java.util.ArrayList;
import androidx.constraintlayout.core.motion.utils.KeyCycleOscillator;
import androidx.constraintlayout.core.motion.utils.SplineSet;
import java.util.HashMap;
import androidx.constraintlayout.core.motion.utils.CurveFit;

public class Motion
{
    static final int BOUNCE = 4;
    private static final boolean DEBUG = false;
    public static final int DRAW_PATH_AS_CONFIGURED = 4;
    public static final int DRAW_PATH_BASIC = 1;
    public static final int DRAW_PATH_CARTESIAN = 3;
    public static final int DRAW_PATH_NONE = 0;
    public static final int DRAW_PATH_RECTANGLE = 5;
    public static final int DRAW_PATH_RELATIVE = 2;
    public static final int DRAW_PATH_SCREEN = 6;
    static final int EASE_IN = 1;
    static final int EASE_IN_OUT = 0;
    static final int EASE_OUT = 2;
    private static final boolean FAVOR_FIXED_SIZE_VIEWS = false;
    public static final int HORIZONTAL_PATH_X = 2;
    public static final int HORIZONTAL_PATH_Y = 3;
    private static final int INTERPOLATOR_REFERENCE_ID = -2;
    private static final int INTERPOLATOR_UNDEFINED = -3;
    static final int LINEAR = 3;
    static final int OVERSHOOT = 5;
    public static final int PATH_PERCENT = 0;
    public static final int PATH_PERPENDICULAR = 1;
    public static final int ROTATION_LEFT = 2;
    public static final int ROTATION_RIGHT = 1;
    private static final int SPLINE_STRING = -1;
    private static final String TAG = "MotionController";
    public static final int VERTICAL_PATH_X = 4;
    public static final int VERTICAL_PATH_Y = 5;
    private int MAX_DIMENSION;
    String[] attributeTable;
    private CurveFit mArcSpline;
    private int[] mAttributeInterpolatorCount;
    private String[] mAttributeNames;
    private HashMap<String, SplineSet> mAttributesMap;
    String mConstraintTag;
    float mCurrentCenterX;
    float mCurrentCenterY;
    private int mCurveFitType;
    private HashMap<String, KeyCycleOscillator> mCycleMap;
    private MotionPaths mEndMotionPath;
    private MotionConstrainedPoint mEndPoint;
    int mId;
    private double[] mInterpolateData;
    private int[] mInterpolateVariables;
    private double[] mInterpolateVelocity;
    private ArrayList<MotionKey> mKeyList;
    private MotionKeyTrigger[] mKeyTriggers;
    private ArrayList<MotionPaths> mMotionPaths;
    float mMotionStagger;
    private boolean mNoMovement;
    private int mPathMotionArc;
    private DifferentialInterpolator mQuantizeMotionInterpolator;
    private float mQuantizeMotionPhase;
    private int mQuantizeMotionSteps;
    private CurveFit[] mSpline;
    float mStaggerOffset;
    float mStaggerScale;
    private MotionPaths mStartMotionPath;
    private MotionConstrainedPoint mStartPoint;
    Rect mTempRect;
    private HashMap<String, TimeCycleSplineSet> mTimeCycleAttributesMap;
    private int mTransformPivotTarget;
    private MotionWidget mTransformPivotView;
    private float[] mValuesBuff;
    private float[] mVelocity;
    MotionWidget mView;
    
    public Motion(final MotionWidget view) {
        this.mTempRect = new Rect();
        this.mCurveFitType = -1;
        this.mStartMotionPath = new MotionPaths();
        this.mEndMotionPath = new MotionPaths();
        this.mStartPoint = new MotionConstrainedPoint();
        this.mEndPoint = new MotionConstrainedPoint();
        this.mMotionStagger = Float.NaN;
        this.mStaggerOffset = 0.0f;
        this.mStaggerScale = 1.0f;
        this.MAX_DIMENSION = 4;
        this.mValuesBuff = new float[4];
        this.mMotionPaths = new ArrayList<MotionPaths>();
        this.mVelocity = new float[1];
        this.mKeyList = new ArrayList<MotionKey>();
        this.mPathMotionArc = -1;
        this.mTransformPivotTarget = -1;
        this.mTransformPivotView = null;
        this.mQuantizeMotionSteps = -1;
        this.mQuantizeMotionPhase = Float.NaN;
        this.mQuantizeMotionInterpolator = null;
        this.mNoMovement = false;
        this.setView(view);
    }
    
    private float getAdjustedPosition(float time, final float[] array) {
        final float n = 0.0f;
        final float n2 = 1.0f;
        float min;
        if (array != null) {
            array[0] = 1.0f;
            min = time;
        }
        else {
            final float mStaggerScale = this.mStaggerScale;
            min = time;
            if (mStaggerScale != 1.0) {
                final float mStaggerOffset = this.mStaggerOffset;
                float n3 = time;
                if (time < mStaggerOffset) {
                    n3 = 0.0f;
                }
                min = n3;
                if (n3 > mStaggerOffset) {
                    min = n3;
                    if (n3 < 1.0) {
                        min = Math.min((n3 - mStaggerOffset) * mStaggerScale, 1.0f);
                    }
                }
            }
        }
        Easing mKeyFrameEasing = this.mStartMotionPath.mKeyFrameEasing;
        final Iterator<MotionPaths> iterator = this.mMotionPaths.iterator();
        time = Float.NaN;
        float n4 = n;
        while (iterator.hasNext()) {
            final MotionPaths motionPaths = iterator.next();
            final Easing mKeyFrameEasing2 = motionPaths.mKeyFrameEasing;
            if (mKeyFrameEasing2 != null) {
                final float time2 = motionPaths.time;
                if (time2 < min) {
                    mKeyFrameEasing = mKeyFrameEasing2;
                    n4 = time2;
                }
                else {
                    if (!Float.isNaN(time)) {
                        continue;
                    }
                    time = motionPaths.time;
                }
            }
        }
        float n5 = min;
        if (mKeyFrameEasing != null) {
            if (Float.isNaN(time)) {
                time = n2;
            }
            time -= n4;
            final double n6 = (min - n4) / time;
            time = (n5 = (float)mKeyFrameEasing.get(n6) * time + n4);
            if (array != null) {
                array[0] = (float)mKeyFrameEasing.getDiff(n6);
                n5 = time;
            }
        }
        return n5;
    }
    
    private static DifferentialInterpolator getInterpolator(final int n, final String s, final int n2) {
        if (n != -1) {
            return null;
        }
        return new DifferentialInterpolator(Easing.getInterpolator(s)) {
            float mX;
            final Easing val$easing;
            
            @Override
            public float getInterpolation(final float mx) {
                this.mX = mx;
                return (float)this.val$easing.get(mx);
            }
            
            @Override
            public float getVelocity() {
                return (float)this.val$easing.getDiff(this.mX);
            }
        };
    }
    
    private float getPreCycleDistance() {
        final float[] array = new float[2];
        final float n = 1.0f / 99;
        double n2 = 0.0;
        double n3 = 0.0;
        float n4 = 0.0f;
        for (int i = 0; i < 100; ++i) {
            final float n5 = i * n;
            double n6 = n5;
            Easing mKeyFrameEasing = this.mStartMotionPath.mKeyFrameEasing;
            final Iterator<MotionPaths> iterator = this.mMotionPaths.iterator();
            float n7 = Float.NaN;
            float n8 = 0.0f;
            while (iterator.hasNext()) {
                final MotionPaths motionPaths = iterator.next();
                final Easing mKeyFrameEasing2 = motionPaths.mKeyFrameEasing;
                Easing easing = mKeyFrameEasing;
                float time = n7;
                float time2 = n8;
                if (mKeyFrameEasing2 != null) {
                    time2 = motionPaths.time;
                    if (time2 < n5) {
                        easing = mKeyFrameEasing2;
                        time = n7;
                    }
                    else {
                        easing = mKeyFrameEasing;
                        time = n7;
                        time2 = n8;
                        if (Float.isNaN(n7)) {
                            time = motionPaths.time;
                            time2 = n8;
                            easing = mKeyFrameEasing;
                        }
                    }
                }
                mKeyFrameEasing = easing;
                n7 = time;
                n8 = time2;
            }
            if (mKeyFrameEasing != null) {
                float n9 = n7;
                if (Float.isNaN(n7)) {
                    n9 = 1.0f;
                }
                final float n10 = n9 - n8;
                n6 = (float)mKeyFrameEasing.get((n5 - n8) / n10) * n10 + n8;
            }
            this.mSpline[0].getPos(n6, this.mInterpolateData);
            this.mStartMotionPath.getCenter(n6, this.mInterpolateVariables, this.mInterpolateData, array, 0);
            if (i > 0) {
                n4 += (float)Math.hypot(n3 - array[1], n2 - array[0]);
            }
            n2 = array[0];
            n3 = array[1];
        }
        return n4;
    }
    
    private void insertKey(final MotionPaths motionPaths) {
        final Iterator<MotionPaths> iterator = this.mMotionPaths.iterator();
        Object o = null;
        while (iterator.hasNext()) {
            final MotionPaths motionPaths2 = iterator.next();
            if (motionPaths.position == motionPaths2.position) {
                o = motionPaths2;
            }
        }
        if (o != null) {
            this.mMotionPaths.remove(o);
        }
        final int binarySearch = Collections.binarySearch(this.mMotionPaths, motionPaths);
        if (binarySearch == 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append(" KeyPath position \"");
            sb.append(motionPaths.position);
            sb.append("\" outside of range");
            Utils.loge("MotionController", sb.toString());
        }
        this.mMotionPaths.add(-binarySearch - 1, motionPaths);
    }
    
    private void readView(final MotionPaths motionPaths) {
        motionPaths.setBounds((float)this.mView.getX(), (float)this.mView.getY(), (float)this.mView.getWidth(), (float)this.mView.getHeight());
    }
    
    public void addKey(final MotionKey e) {
        this.mKeyList.add(e);
    }
    
    void addKeys(final ArrayList<MotionKey> c) {
        this.mKeyList.addAll(c);
    }
    
    void buildBounds(final float[] array, final int n) {
        final float n2 = 1.0f / (n - 1);
        final HashMap<String, SplineSet> mAttributesMap = this.mAttributesMap;
        if (mAttributesMap != null) {
            final SplineSet set = mAttributesMap.get("translationX");
        }
        final HashMap<String, SplineSet> mAttributesMap2 = this.mAttributesMap;
        if (mAttributesMap2 != null) {
            final SplineSet set2 = mAttributesMap2.get("translationY");
        }
        final HashMap<String, KeyCycleOscillator> mCycleMap = this.mCycleMap;
        if (mCycleMap != null) {
            final KeyCycleOscillator keyCycleOscillator = mCycleMap.get("translationX");
        }
        final HashMap<String, KeyCycleOscillator> mCycleMap2 = this.mCycleMap;
        if (mCycleMap2 != null) {
            final KeyCycleOscillator keyCycleOscillator2 = mCycleMap2.get("translationY");
        }
        for (int i = 0; i < n; ++i) {
            final float n3 = i * n2;
            final float mStaggerScale = this.mStaggerScale;
            final float n4 = 0.0f;
            float min = n3;
            if (mStaggerScale != 1.0f) {
                final float mStaggerOffset = this.mStaggerOffset;
                float n5 = n3;
                if (n3 < mStaggerOffset) {
                    n5 = 0.0f;
                }
                min = n5;
                if (n5 > mStaggerOffset) {
                    min = n5;
                    if (n5 < 1.0) {
                        min = Math.min((n5 - mStaggerOffset) * mStaggerScale, 1.0f);
                    }
                }
            }
            double n6 = min;
            Easing mKeyFrameEasing = this.mStartMotionPath.mKeyFrameEasing;
            final Iterator<MotionPaths> iterator = this.mMotionPaths.iterator();
            float time = Float.NaN;
            float n7 = n4;
            while (iterator.hasNext()) {
                final MotionPaths motionPaths = iterator.next();
                final Easing mKeyFrameEasing2 = motionPaths.mKeyFrameEasing;
                if (mKeyFrameEasing2 != null) {
                    final float time2 = motionPaths.time;
                    if (time2 < min) {
                        mKeyFrameEasing = mKeyFrameEasing2;
                        n7 = time2;
                    }
                    else {
                        if (!Float.isNaN(time)) {
                            continue;
                        }
                        time = motionPaths.time;
                    }
                }
            }
            if (mKeyFrameEasing != null) {
                float n8 = time;
                if (Float.isNaN(time)) {
                    n8 = 1.0f;
                }
                final float n9 = n8 - n7;
                n6 = (float)mKeyFrameEasing.get((min - n7) / n9) * n9 + n7;
            }
            this.mSpline[0].getPos(n6, this.mInterpolateData);
            final CurveFit mArcSpline = this.mArcSpline;
            if (mArcSpline != null) {
                final double[] mInterpolateData = this.mInterpolateData;
                if (mInterpolateData.length > 0) {
                    mArcSpline.getPos(n6, mInterpolateData);
                }
            }
            this.mStartMotionPath.getBounds(this.mInterpolateVariables, this.mInterpolateData, array, i * 2);
        }
    }
    
    int buildKeyBounds(final float[] array, final int[] array2) {
        if (array != null) {
            final double[] timePoints = this.mSpline[0].getTimePoints();
            if (array2 != null) {
                final Iterator<MotionPaths> iterator = this.mMotionPaths.iterator();
                int n = 0;
                while (iterator.hasNext()) {
                    array2[n] = iterator.next().mMode;
                    ++n;
                }
            }
            int i = 0;
            int n2 = 0;
            while (i < timePoints.length) {
                this.mSpline[0].getPos(timePoints[i], this.mInterpolateData);
                this.mStartMotionPath.getBounds(this.mInterpolateVariables, this.mInterpolateData, array, n2);
                n2 += 2;
                ++i;
            }
            return n2 / 2;
        }
        return 0;
    }
    
    public int buildKeyFrames(final float[] array, final int[] array2, final int[] array3) {
        if (array != null) {
            final double[] timePoints = this.mSpline[0].getTimePoints();
            if (array2 != null) {
                final Iterator<MotionPaths> iterator = this.mMotionPaths.iterator();
                int n = 0;
                while (iterator.hasNext()) {
                    array2[n] = iterator.next().mMode;
                    ++n;
                }
            }
            if (array3 != null) {
                final Iterator<MotionPaths> iterator2 = this.mMotionPaths.iterator();
                int n2 = 0;
                while (iterator2.hasNext()) {
                    array3[n2] = (int)(iterator2.next().position * 100.0f);
                    ++n2;
                }
            }
            int i = 0;
            int n3 = 0;
            while (i < timePoints.length) {
                this.mSpline[0].getPos(timePoints[i], this.mInterpolateData);
                this.mStartMotionPath.getCenter(timePoints[i], this.mInterpolateVariables, this.mInterpolateData, array, n3);
                n3 += 2;
                ++i;
            }
            return n3 / 2;
        }
        return 0;
    }
    
    public void buildPath(final float[] array, final int n) {
        final float n2 = 1.0f / (n - 1);
        final HashMap<String, SplineSet> mAttributesMap = this.mAttributesMap;
        KeyCycleOscillator keyCycleOscillator = null;
        SplineSet set;
        if (mAttributesMap == null) {
            set = null;
        }
        else {
            set = mAttributesMap.get("translationX");
        }
        final HashMap<String, SplineSet> mAttributesMap2 = this.mAttributesMap;
        SplineSet set2;
        if (mAttributesMap2 == null) {
            set2 = null;
        }
        else {
            set2 = mAttributesMap2.get("translationY");
        }
        final HashMap<String, KeyCycleOscillator> mCycleMap = this.mCycleMap;
        KeyCycleOscillator keyCycleOscillator2;
        if (mCycleMap == null) {
            keyCycleOscillator2 = null;
        }
        else {
            keyCycleOscillator2 = mCycleMap.get("translationX");
        }
        final HashMap<String, KeyCycleOscillator> mCycleMap2 = this.mCycleMap;
        if (mCycleMap2 != null) {
            keyCycleOscillator = mCycleMap2.get("translationY");
        }
        for (int i = 0; i < n; ++i) {
            final float n3 = i * n2;
            final float mStaggerScale = this.mStaggerScale;
            final float n4 = 0.0f;
            float min = n3;
            if (mStaggerScale != 1.0f) {
                final float mStaggerOffset = this.mStaggerOffset;
                float n5 = n3;
                if (n3 < mStaggerOffset) {
                    n5 = 0.0f;
                }
                min = n5;
                if (n5 > mStaggerOffset) {
                    min = n5;
                    if (n5 < 1.0) {
                        min = Math.min((n5 - mStaggerOffset) * mStaggerScale, 1.0f);
                    }
                }
            }
            double n6 = min;
            Easing mKeyFrameEasing = this.mStartMotionPath.mKeyFrameEasing;
            final Iterator<MotionPaths> iterator = this.mMotionPaths.iterator();
            float n7 = Float.NaN;
            float n8 = n4;
            while (iterator.hasNext()) {
                final MotionPaths motionPaths = iterator.next();
                final Easing mKeyFrameEasing2 = motionPaths.mKeyFrameEasing;
                float time = n8;
                Easing easing = mKeyFrameEasing;
                float time2 = n7;
                if (mKeyFrameEasing2 != null) {
                    time = motionPaths.time;
                    if (time < min) {
                        easing = mKeyFrameEasing2;
                        time2 = n7;
                    }
                    else {
                        time = n8;
                        easing = mKeyFrameEasing;
                        time2 = n7;
                        if (Float.isNaN(n7)) {
                            time2 = motionPaths.time;
                            easing = mKeyFrameEasing;
                            time = n8;
                        }
                    }
                }
                n8 = time;
                mKeyFrameEasing = easing;
                n7 = time2;
            }
            if (mKeyFrameEasing != null) {
                float n9 = n7;
                if (Float.isNaN(n7)) {
                    n9 = 1.0f;
                }
                final float n10 = n9 - n8;
                n6 = (float)mKeyFrameEasing.get((min - n8) / n10) * n10 + n8;
            }
            this.mSpline[0].getPos(n6, this.mInterpolateData);
            final CurveFit mArcSpline = this.mArcSpline;
            if (mArcSpline != null) {
                final double[] mInterpolateData = this.mInterpolateData;
                if (mInterpolateData.length > 0) {
                    mArcSpline.getPos(n6, mInterpolateData);
                }
            }
            final MotionPaths mStartMotionPath = this.mStartMotionPath;
            final int[] mInterpolateVariables = this.mInterpolateVariables;
            final double[] mInterpolateData2 = this.mInterpolateData;
            int n11 = i * 2;
            mStartMotionPath.getCenter(n6, mInterpolateVariables, mInterpolateData2, array, n11);
            if (keyCycleOscillator2 != null) {
                array[n11] += keyCycleOscillator2.get(min);
            }
            else if (set != null) {
                array[n11] += set.get(min);
            }
            if (keyCycleOscillator != null) {
                ++n11;
                array[n11] += keyCycleOscillator.get(min);
            }
            else if (set2 != null) {
                ++n11;
                array[n11] += set2.get(min);
            }
        }
    }
    
    public void buildRect(float adjustedPosition, final float[] array, final int n) {
        adjustedPosition = this.getAdjustedPosition(adjustedPosition, null);
        this.mSpline[0].getPos(adjustedPosition, this.mInterpolateData);
        this.mStartMotionPath.getRect(this.mInterpolateVariables, this.mInterpolateData, array, n);
    }
    
    void buildRectangles(final float[] array, final int n) {
        final float n2 = 1.0f / (n - 1);
        for (int i = 0; i < n; ++i) {
            this.mSpline[0].getPos(this.getAdjustedPosition(i * n2, null), this.mInterpolateData);
            this.mStartMotionPath.getRect(this.mInterpolateVariables, this.mInterpolateData, array, i * 8);
        }
    }
    
    void endTrigger(final boolean b) {
    }
    
    public int getAnimateRelativeTo() {
        return this.mStartMotionPath.mAnimateRelativeTo;
    }
    
    int getAttributeValues(final String key, final float[] array, int i) {
        final SplineSet set = this.mAttributesMap.get(key);
        if (set == null) {
            return -1;
        }
        for (i = 0; i < array.length; ++i) {
            array[i] = set.get((float)(i / (array.length - 1)));
        }
        return array.length;
    }
    
    public void getCenter(final double n, final float[] array, final float[] a) {
        final double[] array2 = new double[4];
        final double[] array3 = new double[4];
        this.mSpline[0].getPos(n, array2);
        this.mSpline[0].getSlope(n, array3);
        Arrays.fill(a, 0.0f);
        this.mStartMotionPath.getCenter(n, this.mInterpolateVariables, array2, array, array3, a);
    }
    
    public float getCenterX() {
        return this.mCurrentCenterX;
    }
    
    public float getCenterY() {
        return this.mCurrentCenterY;
    }
    
    void getDpDt(float n, final float n2, final float n3, final float[] array) {
        n = this.getAdjustedPosition(n, this.mVelocity);
        final CurveFit[] mSpline = this.mSpline;
        int n4 = 0;
        if (mSpline == null) {
            final MotionPaths mEndMotionPath = this.mEndMotionPath;
            n = mEndMotionPath.x;
            final MotionPaths mStartMotionPath = this.mStartMotionPath;
            final float n5 = n - mStartMotionPath.x;
            final float n6 = mEndMotionPath.y - mStartMotionPath.y;
            n = mEndMotionPath.width;
            final float width = mStartMotionPath.width;
            final float height = mEndMotionPath.height;
            final float height2 = mStartMotionPath.height;
            array[0] = n5 * (1.0f - n2) + (n - width + n5) * n2;
            array[1] = n6 * (1.0f - n3) + (height - height2 + n6) * n3;
            return;
        }
        final CurveFit curveFit = mSpline[0];
        final double n7 = n;
        curveFit.getSlope(n7, this.mInterpolateVelocity);
        this.mSpline[0].getPos(n7, this.mInterpolateData);
        n = this.mVelocity[0];
        double[] mInterpolateVelocity;
        while (true) {
            mInterpolateVelocity = this.mInterpolateVelocity;
            if (n4 >= mInterpolateVelocity.length) {
                break;
            }
            mInterpolateVelocity[n4] *= n;
            ++n4;
        }
        final CurveFit mArcSpline = this.mArcSpline;
        if (mArcSpline != null) {
            final double[] mInterpolateData = this.mInterpolateData;
            if (mInterpolateData.length > 0) {
                mArcSpline.getPos(n7, mInterpolateData);
                this.mArcSpline.getSlope(n7, this.mInterpolateVelocity);
                this.mStartMotionPath.setDpDt(n2, n3, array, this.mInterpolateVariables, this.mInterpolateVelocity, this.mInterpolateData);
            }
            return;
        }
        this.mStartMotionPath.setDpDt(n2, n3, array, this.mInterpolateVariables, mInterpolateVelocity, this.mInterpolateData);
    }
    
    public int getDrawPath() {
        int n = this.mStartMotionPath.mDrawPath;
        final Iterator<MotionPaths> iterator = this.mMotionPaths.iterator();
        while (iterator.hasNext()) {
            n = Math.max(n, iterator.next().mDrawPath);
        }
        return Math.max(n, this.mEndMotionPath.mDrawPath);
    }
    
    public float getFinalHeight() {
        return this.mEndMotionPath.height;
    }
    
    public float getFinalWidth() {
        return this.mEndMotionPath.width;
    }
    
    public float getFinalX() {
        return this.mEndMotionPath.x;
    }
    
    public float getFinalY() {
        return this.mEndMotionPath.y;
    }
    
    public MotionPaths getKeyFrame(final int index) {
        return this.mMotionPaths.get(index);
    }
    
    public int getKeyFrameInfo(final int n, final int[] array) {
        final float[] array2 = new float[2];
        final Iterator<MotionKey> iterator = this.mKeyList.iterator();
        int n2 = 0;
        int n3 = 0;
        while (iterator.hasNext()) {
            final MotionKey motionKey = iterator.next();
            final int mType = motionKey.mType;
            if (mType != n && n == -1) {
                continue;
            }
            array[n3] = 0;
            int n4 = n3 + 1;
            array[n4] = mType;
            ++n4;
            final int mFramePosition = motionKey.mFramePosition;
            array[n4] = mFramePosition;
            final float n5 = mFramePosition / 100.0f;
            final CurveFit curveFit = this.mSpline[0];
            final double n6 = n5;
            curveFit.getPos(n6, this.mInterpolateData);
            this.mStartMotionPath.getCenter(n6, this.mInterpolateVariables, this.mInterpolateData, array2, 0);
            final int n7 = n4 + 1;
            array[n7] = Float.floatToIntBits(array2[0]);
            final int n8 = n7 + 1;
            array[n8] = Float.floatToIntBits(array2[1]);
            int n9 = n8;
            if (motionKey instanceof MotionKeyPosition) {
                final MotionKeyPosition motionKeyPosition = (MotionKeyPosition)motionKey;
                n9 = n8 + 1;
                array[n9] = motionKeyPosition.mPositionType;
                ++n9;
                array[n9] = Float.floatToIntBits(motionKeyPosition.mPercentX);
                ++n9;
                array[n9] = Float.floatToIntBits(motionKeyPosition.mPercentY);
            }
            ++n9;
            array[n3] = n9 - n3;
            ++n2;
            n3 = n9;
        }
        return n2;
    }
    
    float getKeyFrameParameter(final int n, float n2, float n3) {
        final MotionPaths mEndMotionPath = this.mEndMotionPath;
        final float x = mEndMotionPath.x;
        final MotionPaths mStartMotionPath = this.mStartMotionPath;
        final float x2 = mStartMotionPath.x;
        final float n4 = x - x2;
        final float y = mEndMotionPath.y;
        final float y2 = mStartMotionPath.y;
        final float n5 = y - y2;
        final float n6 = mStartMotionPath.width / 2.0f;
        final float n7 = mStartMotionPath.height / 2.0f;
        final float n8 = (float)Math.hypot(n4, n5);
        if (n8 < 1.0E-7) {
            return Float.NaN;
        }
        n2 -= x2 + n6;
        final float n9 = n3 - (y2 + n7);
        if ((float)Math.hypot(n2, n9) == 0.0f) {
            return 0.0f;
        }
        n3 = n2 * n4 + n9 * n5;
        if (n == 0) {
            return n3 / n8;
        }
        if (n == 1) {
            return (float)Math.sqrt(n8 * n8 - n3 * n3);
        }
        if (n == 2) {
            return n2 / n4;
        }
        if (n == 3) {
            return n9 / n4;
        }
        if (n == 4) {
            return n2 / n5;
        }
        if (n != 5) {
            return 0.0f;
        }
        return n9 / n5;
    }
    
    public int getKeyFramePositions(final int[] array, final float[] array2) {
        final Iterator<MotionKey> iterator = this.mKeyList.iterator();
        int n = 0;
        int n2 = 0;
        while (iterator.hasNext()) {
            final MotionKey motionKey = iterator.next();
            final int mFramePosition = motionKey.mFramePosition;
            array[n] = motionKey.mType * 1000 + mFramePosition;
            final float n3 = mFramePosition / 100.0f;
            final CurveFit curveFit = this.mSpline[0];
            final double n4 = n3;
            curveFit.getPos(n4, this.mInterpolateData);
            this.mStartMotionPath.getCenter(n4, this.mInterpolateVariables, this.mInterpolateData, array2, n2);
            n2 += 2;
            ++n;
        }
        return n;
    }
    
    double[] getPos(final double n) {
        this.mSpline[0].getPos(n, this.mInterpolateData);
        final CurveFit mArcSpline = this.mArcSpline;
        if (mArcSpline != null) {
            final double[] mInterpolateData = this.mInterpolateData;
            if (mInterpolateData.length > 0) {
                mArcSpline.getPos(n, mInterpolateData);
            }
        }
        return this.mInterpolateData;
    }
    
    MotionKeyPosition getPositionKeyframe(final int n, final int n2, final float n3, final float n4) {
        final FloatRect floatRect = new FloatRect();
        final MotionPaths mStartMotionPath = this.mStartMotionPath;
        final float x = mStartMotionPath.x;
        floatRect.left = x;
        final float y = mStartMotionPath.y;
        floatRect.top = y;
        floatRect.right = x + mStartMotionPath.width;
        floatRect.bottom = y + mStartMotionPath.height;
        final FloatRect floatRect2 = new FloatRect();
        final MotionPaths mEndMotionPath = this.mEndMotionPath;
        final float x2 = mEndMotionPath.x;
        floatRect2.left = x2;
        final float y2 = mEndMotionPath.y;
        floatRect2.top = y2;
        floatRect2.right = x2 + mEndMotionPath.width;
        floatRect2.bottom = y2 + mEndMotionPath.height;
        for (final MotionKey motionKey : this.mKeyList) {
            if (motionKey instanceof MotionKeyPosition) {
                final MotionKeyPosition motionKeyPosition = (MotionKeyPosition)motionKey;
                if (motionKeyPosition.intersects(n, n2, floatRect, floatRect2, n3, n4)) {
                    return motionKeyPosition;
                }
                continue;
            }
        }
        return null;
    }
    
    void getPostLayoutDvDp(float n, final int n2, final int n3, final float n4, final float n5, final float[] array) {
        n = this.getAdjustedPosition(n, this.mVelocity);
        final HashMap<String, SplineSet> mAttributesMap = this.mAttributesMap;
        KeyCycleOscillator keyCycleOscillator = null;
        SplineSet set;
        if (mAttributesMap == null) {
            set = null;
        }
        else {
            set = mAttributesMap.get("translationX");
        }
        final HashMap<String, SplineSet> mAttributesMap2 = this.mAttributesMap;
        SplineSet set2;
        if (mAttributesMap2 == null) {
            set2 = null;
        }
        else {
            set2 = mAttributesMap2.get("translationY");
        }
        final HashMap<String, SplineSet> mAttributesMap3 = this.mAttributesMap;
        SplineSet set3;
        if (mAttributesMap3 == null) {
            set3 = null;
        }
        else {
            set3 = mAttributesMap3.get("rotationZ");
        }
        final HashMap<String, SplineSet> mAttributesMap4 = this.mAttributesMap;
        SplineSet set4;
        if (mAttributesMap4 == null) {
            set4 = null;
        }
        else {
            set4 = mAttributesMap4.get("scaleX");
        }
        final HashMap<String, SplineSet> mAttributesMap5 = this.mAttributesMap;
        SplineSet set5;
        if (mAttributesMap5 == null) {
            set5 = null;
        }
        else {
            set5 = mAttributesMap5.get("scaleY");
        }
        final HashMap<String, KeyCycleOscillator> mCycleMap = this.mCycleMap;
        KeyCycleOscillator keyCycleOscillator2;
        if (mCycleMap == null) {
            keyCycleOscillator2 = null;
        }
        else {
            keyCycleOscillator2 = mCycleMap.get("translationX");
        }
        final HashMap<String, KeyCycleOscillator> mCycleMap2 = this.mCycleMap;
        KeyCycleOscillator keyCycleOscillator3;
        if (mCycleMap2 == null) {
            keyCycleOscillator3 = null;
        }
        else {
            keyCycleOscillator3 = mCycleMap2.get("translationY");
        }
        final HashMap<String, KeyCycleOscillator> mCycleMap3 = this.mCycleMap;
        KeyCycleOscillator keyCycleOscillator4;
        if (mCycleMap3 == null) {
            keyCycleOscillator4 = null;
        }
        else {
            keyCycleOscillator4 = mCycleMap3.get("rotationZ");
        }
        final HashMap<String, KeyCycleOscillator> mCycleMap4 = this.mCycleMap;
        KeyCycleOscillator keyCycleOscillator5;
        if (mCycleMap4 == null) {
            keyCycleOscillator5 = null;
        }
        else {
            keyCycleOscillator5 = mCycleMap4.get("scaleX");
        }
        final HashMap<String, KeyCycleOscillator> mCycleMap5 = this.mCycleMap;
        if (mCycleMap5 != null) {
            keyCycleOscillator = mCycleMap5.get("scaleY");
        }
        final VelocityMatrix velocityMatrix = new VelocityMatrix();
        velocityMatrix.clear();
        velocityMatrix.setRotationVelocity(set3, n);
        velocityMatrix.setTranslationVelocity(set, set2, n);
        velocityMatrix.setScaleVelocity(set4, set5, n);
        velocityMatrix.setRotationVelocity(keyCycleOscillator4, n);
        velocityMatrix.setTranslationVelocity(keyCycleOscillator2, keyCycleOscillator3, n);
        velocityMatrix.setScaleVelocity(keyCycleOscillator5, keyCycleOscillator, n);
        final CurveFit mArcSpline = this.mArcSpline;
        if (mArcSpline != null) {
            final double[] mInterpolateData = this.mInterpolateData;
            if (mInterpolateData.length > 0) {
                final double n6 = n;
                mArcSpline.getPos(n6, mInterpolateData);
                this.mArcSpline.getSlope(n6, this.mInterpolateVelocity);
                this.mStartMotionPath.setDpDt(n4, n5, array, this.mInterpolateVariables, this.mInterpolateVelocity, this.mInterpolateData);
            }
            velocityMatrix.applyTransform(n4, n5, n2, n3, array);
            return;
        }
        final CurveFit[] mSpline = this.mSpline;
        int n7 = 0;
        if (mSpline != null) {
            n = this.getAdjustedPosition(n, this.mVelocity);
            final CurveFit curveFit = this.mSpline[0];
            final double n8 = n;
            curveFit.getSlope(n8, this.mInterpolateVelocity);
            this.mSpline[0].getPos(n8, this.mInterpolateData);
            n = this.mVelocity[0];
            double[] mInterpolateVelocity;
            while (true) {
                mInterpolateVelocity = this.mInterpolateVelocity;
                if (n7 >= mInterpolateVelocity.length) {
                    break;
                }
                mInterpolateVelocity[n7] *= n;
                ++n7;
            }
            this.mStartMotionPath.setDpDt(n4, n5, array, this.mInterpolateVariables, mInterpolateVelocity, this.mInterpolateData);
            velocityMatrix.applyTransform(n4, n5, n2, n3, array);
            return;
        }
        final MotionPaths mEndMotionPath = this.mEndMotionPath;
        final float x = mEndMotionPath.x;
        final MotionPaths mStartMotionPath = this.mStartMotionPath;
        final float n9 = x - mStartMotionPath.x;
        final float n10 = mEndMotionPath.y - mStartMotionPath.y;
        final float width = mEndMotionPath.width;
        final float width2 = mStartMotionPath.width;
        final float height = mEndMotionPath.height;
        final float height2 = mStartMotionPath.height;
        array[0] = n9 * (1.0f - n4) + (width - width2 + n9) * n4;
        array[1] = n10 * (1.0f - n5) + (height - height2 + n10) * n5;
        velocityMatrix.clear();
        velocityMatrix.setRotationVelocity(set3, n);
        velocityMatrix.setTranslationVelocity(set, set2, n);
        velocityMatrix.setScaleVelocity(set4, set5, n);
        velocityMatrix.setRotationVelocity(keyCycleOscillator4, n);
        velocityMatrix.setTranslationVelocity(keyCycleOscillator2, keyCycleOscillator3, n);
        velocityMatrix.setScaleVelocity(keyCycleOscillator5, keyCycleOscillator, n);
        velocityMatrix.applyTransform(n4, n5, n2, n3, array);
    }
    
    public float getStartHeight() {
        return this.mStartMotionPath.height;
    }
    
    public float getStartWidth() {
        return this.mStartMotionPath.width;
    }
    
    public float getStartX() {
        return this.mStartMotionPath.x;
    }
    
    public float getStartY() {
        return this.mStartMotionPath.y;
    }
    
    public int getTransformPivotTarget() {
        return this.mTransformPivotTarget;
    }
    
    public MotionWidget getView() {
        return this.mView;
    }
    
    public boolean interpolate(final MotionWidget motionWidget, float interpolation, final long n, final KeyCache keyCache) {
        final float adjustedPosition = this.getAdjustedPosition(interpolation, null);
        final int mQuantizeMotionSteps = this.mQuantizeMotionSteps;
        interpolation = adjustedPosition;
        if (mQuantizeMotionSteps != -1) {
            final float n2 = 1.0f / mQuantizeMotionSteps;
            final float n3 = (float)Math.floor(adjustedPosition / n2);
            final float n4 = interpolation = adjustedPosition % n2 / n2;
            if (!Float.isNaN(this.mQuantizeMotionPhase)) {
                interpolation = (n4 + this.mQuantizeMotionPhase) % 1.0f;
            }
            final DifferentialInterpolator mQuantizeMotionInterpolator = this.mQuantizeMotionInterpolator;
            if (mQuantizeMotionInterpolator != null) {
                interpolation = mQuantizeMotionInterpolator.getInterpolation(interpolation);
            }
            else if (interpolation > 0.5) {
                interpolation = 1.0f;
            }
            else {
                interpolation = 0.0f;
            }
            interpolation = interpolation * n2 + n3 * n2;
        }
        final HashMap<String, SplineSet> mAttributesMap = this.mAttributesMap;
        if (mAttributesMap != null) {
            final Iterator<SplineSet> iterator = mAttributesMap.values().iterator();
            while (iterator.hasNext()) {
                iterator.next().setProperty(motionWidget, interpolation);
            }
        }
        final CurveFit[] mSpline = this.mSpline;
        if (mSpline != null) {
            final CurveFit curveFit = mSpline[0];
            final double n5 = interpolation;
            curveFit.getPos(n5, this.mInterpolateData);
            this.mSpline[0].getSlope(n5, this.mInterpolateVelocity);
            final CurveFit mArcSpline = this.mArcSpline;
            if (mArcSpline != null) {
                final double[] mInterpolateData = this.mInterpolateData;
                if (mInterpolateData.length > 0) {
                    mArcSpline.getPos(n5, mInterpolateData);
                    this.mArcSpline.getSlope(n5, this.mInterpolateVelocity);
                }
            }
            if (!this.mNoMovement) {
                this.mStartMotionPath.setView(interpolation, motionWidget, this.mInterpolateVariables, this.mInterpolateData, this.mInterpolateVelocity, null);
            }
            if (this.mTransformPivotTarget != -1) {
                if (this.mTransformPivotView == null) {
                    this.mTransformPivotView = motionWidget.getParent().findViewById(this.mTransformPivotTarget);
                }
                final MotionWidget mTransformPivotView = this.mTransformPivotView;
                if (mTransformPivotView != null) {
                    final float n6 = (mTransformPivotView.getTop() + this.mTransformPivotView.getBottom()) / 2.0f;
                    final float n7 = (this.mTransformPivotView.getLeft() + this.mTransformPivotView.getRight()) / 2.0f;
                    if (motionWidget.getRight() - motionWidget.getLeft() > 0 && motionWidget.getBottom() - motionWidget.getTop() > 0) {
                        final float n8 = (float)motionWidget.getLeft();
                        final float n9 = (float)motionWidget.getTop();
                        motionWidget.setPivotX(n7 - n8);
                        motionWidget.setPivotY(n6 - n9);
                    }
                }
            }
            int n10 = 1;
            while (true) {
                final CurveFit[] mSpline2 = this.mSpline;
                if (n10 >= mSpline2.length) {
                    break;
                }
                mSpline2[n10].getPos(n5, this.mValuesBuff);
                this.mStartMotionPath.customAttributes.get(this.mAttributeNames[n10 - 1]).setInterpolatedValue(motionWidget, this.mValuesBuff);
                ++n10;
            }
            final MotionConstrainedPoint mStartPoint = this.mStartPoint;
            if (mStartPoint.mVisibilityMode == 0) {
                if (interpolation <= 0.0f) {
                    motionWidget.setVisibility(mStartPoint.visibility);
                }
                else if (interpolation >= 1.0f) {
                    motionWidget.setVisibility(this.mEndPoint.visibility);
                }
                else if (this.mEndPoint.visibility != mStartPoint.visibility) {
                    motionWidget.setVisibility(4);
                }
            }
            if (this.mKeyTriggers != null) {
                int n11 = 0;
                while (true) {
                    final MotionKeyTrigger[] mKeyTriggers = this.mKeyTriggers;
                    if (n11 >= mKeyTriggers.length) {
                        break;
                    }
                    mKeyTriggers[n11].conditionallyFire(interpolation, motionWidget);
                    ++n11;
                }
            }
        }
        else {
            final MotionPaths mStartMotionPath = this.mStartMotionPath;
            final float x = mStartMotionPath.x;
            final MotionPaths mEndMotionPath = this.mEndMotionPath;
            final float x2 = mEndMotionPath.x;
            final float y = mStartMotionPath.y;
            final float y2 = mEndMotionPath.y;
            final float width = mStartMotionPath.width;
            final float width2 = mEndMotionPath.width;
            final float height = mStartMotionPath.height;
            final float height2 = mEndMotionPath.height;
            final float n12 = x + (x2 - x) * interpolation + 0.5f;
            final int n13 = (int)n12;
            final float n14 = y + (y2 - y) * interpolation + 0.5f;
            motionWidget.layout(n13, (int)n14, (int)(n12 + (width + (width2 - width) * interpolation)), (int)(n14 + (height + (height2 - height) * interpolation)));
        }
        final HashMap<String, KeyCycleOscillator> mCycleMap = this.mCycleMap;
        if (mCycleMap != null) {
            for (final KeyCycleOscillator keyCycleOscillator : mCycleMap.values()) {
                if (keyCycleOscillator instanceof KeyCycleOscillator.PathRotateSet) {
                    final KeyCycleOscillator.PathRotateSet set = (KeyCycleOscillator.PathRotateSet)keyCycleOscillator;
                    final double[] mInterpolateVelocity = this.mInterpolateVelocity;
                    set.setPathRotate(motionWidget, interpolation, mInterpolateVelocity[0], mInterpolateVelocity[1]);
                }
                else {
                    keyCycleOscillator.setProperty(motionWidget, interpolation);
                }
            }
        }
        return false;
    }
    
    String name() {
        return this.mView.getName();
    }
    
    void positionKeyframe(final MotionWidget motionWidget, final MotionKeyPosition motionKeyPosition, final float n, final float n2, final String[] array, final float[] array2) {
        final FloatRect floatRect = new FloatRect();
        final MotionPaths mStartMotionPath = this.mStartMotionPath;
        final float x = mStartMotionPath.x;
        floatRect.left = x;
        final float y = mStartMotionPath.y;
        floatRect.top = y;
        floatRect.right = x + mStartMotionPath.width;
        floatRect.bottom = y + mStartMotionPath.height;
        final FloatRect floatRect2 = new FloatRect();
        final MotionPaths mEndMotionPath = this.mEndMotionPath;
        final float x2 = mEndMotionPath.x;
        floatRect2.left = x2;
        final float y2 = mEndMotionPath.y;
        floatRect2.top = y2;
        floatRect2.right = x2 + mEndMotionPath.width;
        floatRect2.bottom = y2 + mEndMotionPath.height;
        motionKeyPosition.positionAttributes(motionWidget, floatRect, floatRect2, n, n2, array, array2);
    }
    
    void rotate(final Rect rect, final Rect rect2, int n, int left, int n2) {
        if (n != 1) {
            if (n != 2) {
                if (n != 3) {
                    if (n == 4) {
                        n2 = rect.left;
                        n = rect.right;
                        rect2.left = left - (rect.bottom + rect.top + rect.width()) / 2;
                        rect2.top = (n2 + n - rect.height()) / 2;
                        rect2.right = rect2.left + rect.width();
                        rect2.bottom = rect2.top + rect.height();
                    }
                }
                else {
                    n = rect.left + rect.right;
                    rect2.left = rect.height() / 2 + rect.top - n / 2;
                    rect2.top = n2 - (n + rect.height()) / 2;
                    rect2.right = rect2.left + rect.width();
                    rect2.bottom = rect2.top + rect.height();
                }
            }
            else {
                n = rect.left;
                n2 = rect.right;
                rect2.left = left - (rect.top + rect.bottom + rect.width()) / 2;
                rect2.top = (n + n2 - rect.height()) / 2;
                rect2.right = rect2.left + rect.width();
                rect2.bottom = rect2.top + rect.height();
            }
        }
        else {
            left = rect.left;
            n = rect.right;
            rect2.left = (rect.top + rect.bottom - rect.width()) / 2;
            rect2.top = n2 - (left + n + rect.height()) / 2;
            rect2.right = rect2.left + rect.width();
            rect2.bottom = rect2.top + rect.height();
        }
    }
    
    void setBothStates(final MotionWidget motionWidget) {
        final MotionPaths mStartMotionPath = this.mStartMotionPath;
        mStartMotionPath.time = 0.0f;
        mStartMotionPath.position = 0.0f;
        this.mNoMovement = true;
        mStartMotionPath.setBounds((float)motionWidget.getX(), (float)motionWidget.getY(), (float)motionWidget.getWidth(), (float)motionWidget.getHeight());
        this.mEndMotionPath.setBounds((float)motionWidget.getX(), (float)motionWidget.getY(), (float)motionWidget.getWidth(), (float)motionWidget.getHeight());
        this.mStartPoint.setState(motionWidget);
        this.mEndPoint.setState(motionWidget);
    }
    
    public void setDrawPath(final int mDrawPath) {
        this.mStartMotionPath.mDrawPath = mDrawPath;
    }
    
    public void setEnd(final MotionWidget state) {
        final MotionPaths mEndMotionPath = this.mEndMotionPath;
        mEndMotionPath.time = 1.0f;
        mEndMotionPath.position = 1.0f;
        this.readView(mEndMotionPath);
        this.mEndMotionPath.setBounds((float)state.getLeft(), (float)state.getTop(), (float)state.getWidth(), (float)state.getHeight());
        this.mEndMotionPath.applyParameters(state);
        this.mEndPoint.setState(state);
    }
    
    public void setPathMotionArc(final int mPathMotionArc) {
        this.mPathMotionArc = mPathMotionArc;
    }
    
    public void setStart(final MotionWidget state) {
        final MotionPaths mStartMotionPath = this.mStartMotionPath;
        mStartMotionPath.time = 0.0f;
        mStartMotionPath.position = 0.0f;
        mStartMotionPath.setBounds((float)state.getX(), (float)state.getY(), (float)state.getWidth(), (float)state.getHeight());
        this.mStartMotionPath.applyParameters(state);
        this.mStartPoint.setState(state);
    }
    
    public void setStartState(final ViewState viewState, final MotionWidget motionWidget, final int n, int right, int left) {
        final MotionPaths mStartMotionPath = this.mStartMotionPath;
        mStartMotionPath.time = 0.0f;
        mStartMotionPath.position = 0.0f;
        final Rect rect = new Rect();
        if (n != 1) {
            if (n == 2) {
                final int left2 = viewState.left;
                right = viewState.right;
                rect.left = left - (viewState.top + viewState.bottom + viewState.width()) / 2;
                rect.top = (left2 + right - viewState.height()) / 2;
                rect.right = rect.left + viewState.width();
                rect.bottom = rect.top + viewState.height();
            }
        }
        else {
            left = viewState.left;
            final int right2 = viewState.right;
            rect.left = (viewState.top + viewState.bottom - viewState.width()) / 2;
            rect.top = right - (left + right2 + viewState.height()) / 2;
            rect.right = rect.left + viewState.width();
            rect.bottom = rect.top + viewState.height();
        }
        this.mStartMotionPath.setBounds((float)rect.left, (float)rect.top, (float)rect.width(), (float)rect.height());
        this.mStartPoint.setState(rect, motionWidget, n, viewState.rotation);
    }
    
    public void setTransformPivotTarget(final int mTransformPivotTarget) {
        this.mTransformPivotTarget = mTransformPivotTarget;
        this.mTransformPivotView = null;
    }
    
    public void setView(final MotionWidget mView) {
        this.mView = mView;
    }
    
    public void setup(int i, int j, float v, final long n) {
        new HashSet();
        final HashSet set = new HashSet();
        final HashSet set2 = new HashSet();
        final HashSet set3 = new HashSet();
        final HashMap interpolation = new HashMap();
        final int mPathMotionArc = this.mPathMotionArc;
        if (mPathMotionArc != -1) {
            this.mStartMotionPath.mPathMotionArc = mPathMotionArc;
        }
        this.mStartPoint.different(this.mEndPoint, set2);
        final ArrayList<MotionKey> mKeyList = this.mKeyList;
        ArrayList<MotionKeyTrigger> list2;
        if (mKeyList != null) {
            final Iterator<MotionKey> iterator = mKeyList.iterator();
            ArrayList<MotionKeyTrigger> list = null;
            while (true) {
                list2 = list;
                if (!iterator.hasNext()) {
                    break;
                }
                final MotionKey motionKey = iterator.next();
                if (motionKey instanceof MotionKeyPosition) {
                    final MotionKeyPosition motionKeyPosition = (MotionKeyPosition)motionKey;
                    this.insertKey(new MotionPaths(i, j, motionKeyPosition, this.mStartMotionPath, this.mEndMotionPath));
                    final int mCurveFit = motionKeyPosition.mCurveFit;
                    if (mCurveFit == -1) {
                        continue;
                    }
                    this.mCurveFitType = mCurveFit;
                }
                else if (motionKey instanceof MotionKeyCycle) {
                    motionKey.getAttributeNames(set3);
                }
                else if (motionKey instanceof MotionKeyTimeCycle) {
                    motionKey.getAttributeNames(set);
                }
                else if (motionKey instanceof MotionKeyTrigger) {
                    ArrayList<MotionKeyTrigger> list3;
                    if ((list3 = list) == null) {
                        list3 = new ArrayList<MotionKeyTrigger>();
                    }
                    list3.add((MotionKeyTrigger)motionKey);
                    list = list3;
                }
                else {
                    motionKey.setInterpolation(interpolation);
                    motionKey.getAttributeNames(set2);
                }
            }
        }
        else {
            list2 = null;
        }
        if (list2 != null) {
            this.mKeyTriggers = list2.toArray(new MotionKeyTrigger[0]);
        }
        if (!set2.isEmpty()) {
            this.mAttributesMap = new HashMap<String, SplineSet>();
            for (final String s : set2) {
                SplineSet value;
                if (s.startsWith("CUSTOM,")) {
                    final KeyFrameArray.CustomVar customVar = new KeyFrameArray.CustomVar();
                    final String key = s.split(",")[1];
                    for (final MotionKey motionKey2 : this.mKeyList) {
                        final HashMap<String, CustomVariable> mCustom = motionKey2.mCustom;
                        if (mCustom == null) {
                            continue;
                        }
                        final CustomVariable customVariable = mCustom.get(key);
                        if (customVariable == null) {
                            continue;
                        }
                        customVar.append(motionKey2.mFramePosition, customVariable);
                    }
                    value = SplineSet.makeCustomSplineSet(s, customVar);
                }
                else {
                    value = SplineSet.makeSpline(s, n);
                }
                if (value == null) {
                    continue;
                }
                value.setType(s);
                this.mAttributesMap.put(s, value);
            }
            final ArrayList<MotionKey> mKeyList2 = this.mKeyList;
            if (mKeyList2 != null) {
                for (final MotionKey motionKey3 : mKeyList2) {
                    if (motionKey3 instanceof MotionKeyAttributes) {
                        motionKey3.addValues(this.mAttributesMap);
                    }
                }
            }
            this.mStartPoint.addValues(this.mAttributesMap, 0);
            this.mEndPoint.addValues(this.mAttributesMap, 100);
            for (final String key2 : this.mAttributesMap.keySet()) {
                Label_0674: {
                    if (interpolation.containsKey(key2)) {
                        final Integer n2 = interpolation.get(key2);
                        if (n2 != null) {
                            i = n2;
                            break Label_0674;
                        }
                    }
                    i = 0;
                }
                final SplineSet set4 = this.mAttributesMap.get(key2);
                if (set4 != null) {
                    set4.setup(i);
                }
            }
        }
        if (!set.isEmpty()) {
            if (this.mTimeCycleAttributesMap == null) {
                this.mTimeCycleAttributesMap = new HashMap<String, TimeCycleSplineSet>();
            }
            for (final String s2 : set) {
                if (this.mTimeCycleAttributesMap.containsKey(s2)) {
                    continue;
                }
                SplineSet set5;
                if (s2.startsWith("CUSTOM,")) {
                    final KeyFrameArray.CustomVar customVar2 = new KeyFrameArray.CustomVar();
                    final String key3 = s2.split(",")[1];
                    for (final MotionKey motionKey4 : this.mKeyList) {
                        final HashMap<String, CustomVariable> mCustom2 = motionKey4.mCustom;
                        if (mCustom2 == null) {
                            continue;
                        }
                        final CustomVariable customVariable2 = mCustom2.get(key3);
                        if (customVariable2 == null) {
                            continue;
                        }
                        customVar2.append(motionKey4.mFramePosition, customVariable2);
                    }
                    set5 = SplineSet.makeCustomSplineSet(s2, customVar2);
                }
                else {
                    set5 = SplineSet.makeSpline(s2, n);
                }
                if (set5 == null) {
                    continue;
                }
                set5.setType(s2);
            }
            final ArrayList<MotionKey> mKeyList3 = this.mKeyList;
            if (mKeyList3 != null) {
                for (final MotionKey motionKey5 : mKeyList3) {
                    if (motionKey5 instanceof MotionKeyTimeCycle) {
                        ((MotionKeyTimeCycle)motionKey5).addTimeValues(this.mTimeCycleAttributesMap);
                    }
                }
            }
            for (final String key4 : this.mTimeCycleAttributesMap.keySet()) {
                if (interpolation.containsKey(key4)) {
                    i = (int)interpolation.get(key4);
                }
                else {
                    i = 0;
                }
                this.mTimeCycleAttributesMap.get(key4).setup(i);
            }
        }
        final int n3 = this.mMotionPaths.size() + 2;
        final MotionPaths[] array = new MotionPaths[n3];
        array[0] = this.mStartMotionPath;
        array[n3 - 1] = this.mEndMotionPath;
        if (this.mMotionPaths.size() > 0 && this.mCurveFitType == MotionKey.UNSET) {
            this.mCurveFitType = 0;
        }
        final Iterator<MotionPaths> iterator10 = this.mMotionPaths.iterator();
        i = 1;
        while (iterator10.hasNext()) {
            array[i] = iterator10.next();
            ++i;
        }
        final HashSet<String> set6 = new HashSet<String>();
        for (final String e : this.mEndMotionPath.customAttributes.keySet()) {
            if (this.mStartMotionPath.customAttributes.containsKey(e)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("CUSTOM,");
                sb.append(e);
                if (set2.contains(sb.toString())) {
                    continue;
                }
                set6.add(e);
            }
        }
        final String[] mAttributeNames = set6.toArray(new String[0]);
        this.mAttributeNames = mAttributeNames;
        this.mAttributeInterpolatorCount = new int[mAttributeNames.length];
        i = 0;
        String[] mAttributeNames2;
        while (true) {
            mAttributeNames2 = this.mAttributeNames;
            if (i >= mAttributeNames2.length) {
                break;
            }
            final String s3 = mAttributeNames2[i];
            this.mAttributeInterpolatorCount[i] = 0;
            CustomVariable customVariable3;
            int[] mAttributeInterpolatorCount;
            for (j = 0; j < n3; ++j) {
                if (array[j].customAttributes.containsKey(s3)) {
                    customVariable3 = array[j].customAttributes.get(s3);
                    if (customVariable3 != null) {
                        mAttributeInterpolatorCount = this.mAttributeInterpolatorCount;
                        mAttributeInterpolatorCount[i] += customVariable3.numberOfInterpolatedValues();
                        break;
                    }
                }
            }
            ++i;
        }
        final boolean b = array[0].mPathMotionArc != -1;
        final int n4 = 18 + mAttributeNames2.length;
        final boolean[] array2 = new boolean[n4];
        for (i = 1; i < n3; ++i) {
            array[i].different(array[i - 1], array2, this.mAttributeNames, b);
        }
        j = 1;
        i = 0;
        while (j < n4) {
            int n5 = i;
            if (array2[j]) {
                n5 = i + 1;
            }
            ++j;
            i = n5;
        }
        this.mInterpolateVariables = new int[i];
        i = Math.max(2, i);
        this.mInterpolateData = new double[i];
        this.mInterpolateVelocity = new double[i];
        j = 1;
        i = 0;
        while (j < n4) {
            int n6 = i;
            if (array2[j]) {
                this.mInterpolateVariables[i] = j;
                n6 = i + 1;
            }
            ++j;
            i = n6;
        }
        i = this.mInterpolateVariables.length;
        final double[][] array3 = new double[n3][i];
        final double[] array4 = new double[n3];
        for (i = 0; i < n3; ++i) {
            array[i].fillStandard(array3[i], this.mInterpolateVariables);
            array4[i] = array[i].time;
        }
        i = 0;
        while (true) {
            final int[] mInterpolateVariables = this.mInterpolateVariables;
            if (i >= mInterpolateVariables.length) {
                break;
            }
            if (mInterpolateVariables[i] < MotionPaths.names.length) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(MotionPaths.names[this.mInterpolateVariables[i]]);
                sb2.append(" [");
                String str = sb2.toString();
                StringBuilder sb3;
                for (j = 0; j < n3; ++j) {
                    sb3 = new StringBuilder();
                    sb3.append(str);
                    sb3.append(array3[j][i]);
                    str = sb3.toString();
                }
            }
            ++i;
        }
        this.mSpline = new CurveFit[this.mAttributeNames.length + 1];
        i = 0;
        final MotionPaths[] array5 = array;
        while (true) {
            final String[] mAttributeNames3 = this.mAttributeNames;
            if (i >= mAttributeNames3.length) {
                break;
            }
            final String s4 = mAttributeNames3[i];
            j = 0;
            int n7 = 0;
            double[] original = null;
            double[][] original2 = null;
            while (j < n3) {
                if (array5[j].hasCustomData(s4)) {
                    if (original2 == null) {
                        original = new double[n3];
                        original2 = new double[n3][array5[j].getCustomDataCount(s4)];
                    }
                    final MotionPaths motionPaths = array5[j];
                    original[n7] = motionPaths.time;
                    motionPaths.getCustomData(s4, original2[n7], 0);
                    ++n7;
                }
                ++j;
            }
            final double[] copy = Arrays.copyOf(original, n7);
            final double[][] array6 = Arrays.copyOf(original2, n7);
            final CurveFit[] mSpline = this.mSpline;
            ++i;
            mSpline[i] = CurveFit.get(this.mCurveFitType, copy, array6);
        }
        this.mSpline[0] = CurveFit.get(this.mCurveFitType, array4, array3);
        if (array5[0].mPathMotionArc != -1) {
            final int[] array7 = new int[n3];
            final double[] array8 = new double[n3];
            final double[][] array9 = new double[n3][2];
            MotionPaths motionPaths2;
            double[] array10;
            for (i = 0; i < n3; ++i) {
                motionPaths2 = array5[i];
                array7[i] = motionPaths2.mPathMotionArc;
                array8[i] = motionPaths2.time;
                array10 = array9[i];
                array10[0] = motionPaths2.x;
                array10[1] = motionPaths2.y;
            }
            this.mArcSpline = CurveFit.getArc(array7, array8, array9);
        }
        this.mCycleMap = new HashMap<String, KeyCycleOscillator>();
        if (this.mKeyList != null) {
            final Iterator iterator12 = set3.iterator();
            v = Float.NaN;
            while (iterator12.hasNext()) {
                final String s5 = (String)iterator12.next();
                final KeyCycleOscillator widgetCycle = KeyCycleOscillator.makeWidgetCycle(s5);
                if (widgetCycle == null) {
                    continue;
                }
                float preCycleDistance = v;
                if (widgetCycle.variesByPath()) {
                    preCycleDistance = v;
                    if (Float.isNaN(v)) {
                        preCycleDistance = this.getPreCycleDistance();
                    }
                }
                widgetCycle.setType(s5);
                this.mCycleMap.put(s5, widgetCycle);
                v = preCycleDistance;
            }
            for (final MotionKey motionKey6 : this.mKeyList) {
                if (motionKey6 instanceof MotionKeyCycle) {
                    ((MotionKeyCycle)motionKey6).addCycleValues(this.mCycleMap);
                }
            }
            final Iterator<KeyCycleOscillator> iterator14 = this.mCycleMap.values().iterator();
            while (iterator14.hasNext()) {
                iterator14.next().setup(v);
            }
        }
    }
    
    public void setupRelative(final Motion motion) {
        this.mStartMotionPath.setupRelative(motion, motion.mStartMotionPath);
        this.mEndMotionPath.setupRelative(motion, motion.mEndMotionPath);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(" start: x: ");
        sb.append(this.mStartMotionPath.x);
        sb.append(" y: ");
        sb.append(this.mStartMotionPath.y);
        sb.append(" end: x: ");
        sb.append(this.mEndMotionPath.x);
        sb.append(" y: ");
        sb.append(this.mEndMotionPath.y);
        return sb.toString();
    }
}

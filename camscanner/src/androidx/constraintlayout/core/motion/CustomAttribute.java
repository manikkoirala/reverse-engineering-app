// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.motion;

import java.lang.reflect.Method;
import androidx.constraintlayout.core.motion.utils.Utils;
import java.util.Iterator;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

public class CustomAttribute
{
    private static final String TAG = "TransitionLayout";
    boolean mBooleanValue;
    private int mColorValue;
    private float mFloatValue;
    private int mIntegerValue;
    private boolean mMethod;
    String mName;
    private String mStringValue;
    private AttributeType mType;
    
    public CustomAttribute(final CustomAttribute customAttribute, final Object value) {
        this.mMethod = false;
        this.mName = customAttribute.mName;
        this.mType = customAttribute.mType;
        this.setValue(value);
    }
    
    public CustomAttribute(final String mName, final AttributeType mType) {
        this.mMethod = false;
        this.mName = mName;
        this.mType = mType;
    }
    
    public CustomAttribute(final String mName, final AttributeType mType, final Object value, final boolean mMethod) {
        this.mName = mName;
        this.mType = mType;
        this.mMethod = mMethod;
        this.setValue(value);
    }
    
    private static int clamp(int n) {
        n = (n & ~(n >> 31)) - 255;
        return (n & n >> 31) + 255;
    }
    
    public static HashMap<String, CustomAttribute> extractAttributes(final HashMap<String, CustomAttribute> hashMap, final Object obj) {
        final HashMap hashMap2 = new HashMap();
        final Class<?> class1 = obj.getClass();
        for (final String key : hashMap.keySet()) {
            final CustomAttribute customAttribute = hashMap.get(key);
            try {
                final StringBuilder sb = new StringBuilder();
                sb.append("getMap");
                sb.append(key);
                hashMap2.put(key, new CustomAttribute(customAttribute, class1.getMethod(sb.toString(), (Class<?>[])new Class[0]).invoke(obj, new Object[0])));
            }
            catch (final InvocationTargetException ex) {
                ex.printStackTrace();
            }
            catch (final IllegalAccessException ex2) {
                ex2.printStackTrace();
            }
            catch (final NoSuchMethodException ex3) {
                ex3.printStackTrace();
            }
        }
        return hashMap2;
    }
    
    public static int hsvToRgb(float n, final float n2, float n3) {
        n *= 6.0f;
        final int n4 = (int)n;
        n -= n4;
        n3 *= 255.0f;
        final int n5 = (int)((1.0f - n2) * n3 + 0.5f);
        final int n6 = (int)((1.0f - n * n2) * n3 + 0.5f);
        final int n7 = (int)((1.0f - (1.0f - n) * n2) * n3 + 0.5f);
        final int n8 = (int)(n3 + 0.5f);
        if (n4 == 0) {
            return (n8 << 16) + (n7 << 8) + n5 | 0xFF000000;
        }
        if (n4 == 1) {
            return (n6 << 16) + (n8 << 8) + n5 | 0xFF000000;
        }
        if (n4 == 2) {
            return (n5 << 16) + (n8 << 8) + n7 | 0xFF000000;
        }
        if (n4 == 3) {
            return (n5 << 16) + (n6 << 8) + n8 | 0xFF000000;
        }
        if (n4 == 4) {
            return (n7 << 16) + (n5 << 8) + n8 | 0xFF000000;
        }
        if (n4 != 5) {
            return 0;
        }
        return (n8 << 16) + (n5 << 8) + n6 | 0xFF000000;
    }
    
    public static void setAttributes(final Object obj, final HashMap<String, CustomAttribute> hashMap) {
        final Class<?> class1 = obj.getClass();
        for (final String str : hashMap.keySet()) {
            final CustomAttribute customAttribute = hashMap.get(str);
            String string;
            if (!customAttribute.mMethod) {
                final StringBuilder sb = new StringBuilder();
                sb.append("set");
                sb.append(str);
                string = sb.toString();
            }
            else {
                string = str;
            }
            try {
                switch (CustomAttribute$1.$SwitchMap$androidx$constraintlayout$core$motion$CustomAttribute$AttributeType[customAttribute.mType.ordinal()]) {
                    default: {
                        continue;
                    }
                    case 8: {
                        class1.getMethod(string, Float.TYPE).invoke(obj, customAttribute.mFloatValue);
                        continue;
                    }
                    case 7: {
                        class1.getMethod(string, Float.TYPE).invoke(obj, customAttribute.mFloatValue);
                        continue;
                    }
                    case 6: {
                        class1.getMethod(string, Integer.TYPE).invoke(obj, customAttribute.mIntegerValue);
                        continue;
                    }
                    case 4: {
                        class1.getMethod(string, Integer.TYPE).invoke(obj, customAttribute.mColorValue);
                        continue;
                    }
                    case 3: {
                        class1.getMethod(string, CharSequence.class).invoke(obj, customAttribute.mStringValue);
                        continue;
                    }
                    case 2: {
                        class1.getMethod(string, Boolean.TYPE).invoke(obj, customAttribute.mBooleanValue);
                        continue;
                    }
                    case 1: {
                        class1.getMethod(string, Integer.TYPE).invoke(obj, customAttribute.mIntegerValue);
                        continue;
                    }
                }
            }
            catch (final InvocationTargetException ex) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(" Custom Attribute \"");
                sb2.append(str);
                sb2.append("\" not found on ");
                sb2.append(class1.getName());
                Utils.loge("TransitionLayout", sb2.toString());
                ex.printStackTrace();
            }
            catch (final IllegalAccessException ex2) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(" Custom Attribute \"");
                sb3.append(str);
                sb3.append("\" not found on ");
                sb3.append(class1.getName());
                Utils.loge("TransitionLayout", sb3.toString());
                ex2.printStackTrace();
            }
            catch (final NoSuchMethodException ex3) {
                Utils.loge("TransitionLayout", ex3.getMessage());
                final StringBuilder sb4 = new StringBuilder();
                sb4.append(" Custom Attribute \"");
                sb4.append(str);
                sb4.append("\" not found on ");
                sb4.append(class1.getName());
                Utils.loge("TransitionLayout", sb4.toString());
                final StringBuilder sb5 = new StringBuilder();
                sb5.append(class1.getName());
                sb5.append(" must have a method ");
                sb5.append(string);
                Utils.loge("TransitionLayout", sb5.toString());
            }
        }
    }
    
    public void applyCustom(Object o) {
        final Class<?> class1 = o.getClass();
        final String mName = this.mName;
        String string;
        if (!this.mMethod) {
            final StringBuilder sb = new StringBuilder();
            sb.append("set");
            sb.append(mName);
            string = sb.toString();
        }
        else {
            string = mName;
        }
        try {
            switch (CustomAttribute$1.$SwitchMap$androidx$constraintlayout$core$motion$CustomAttribute$AttributeType[this.mType.ordinal()]) {
                case 8: {
                    class1.getMethod(string, Float.TYPE).invoke(o, this.mFloatValue);
                    break;
                }
                case 7: {
                    class1.getMethod(string, Float.TYPE).invoke(o, this.mFloatValue);
                    break;
                }
                case 4: {
                    class1.getMethod(string, Integer.TYPE).invoke(o, this.mColorValue);
                    break;
                }
                case 3: {
                    class1.getMethod(string, CharSequence.class).invoke(o, this.mStringValue);
                    break;
                }
                case 2: {
                    class1.getMethod(string, Boolean.TYPE).invoke(o, this.mBooleanValue);
                    break;
                }
                case 1:
                case 6: {
                    class1.getMethod(string, Integer.TYPE).invoke(o, this.mIntegerValue);
                    break;
                }
            }
        }
        catch (final InvocationTargetException ex) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(" Custom Attribute \"");
            sb2.append(mName);
            sb2.append("\" not found on ");
            sb2.append(class1.getName());
            Utils.loge("TransitionLayout", sb2.toString());
            ex.printStackTrace();
        }
        catch (final IllegalAccessException ex2) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(" Custom Attribute \"");
            sb3.append(mName);
            sb3.append("\" not found on ");
            sb3.append(class1.getName());
            Utils.loge("TransitionLayout", sb3.toString());
            ex2.printStackTrace();
        }
        catch (final NoSuchMethodException ex3) {
            Utils.loge("TransitionLayout", ex3.getMessage());
            o = new StringBuilder();
            ((StringBuilder)o).append(" Custom Attribute \"");
            ((StringBuilder)o).append(mName);
            ((StringBuilder)o).append("\" not found on ");
            ((StringBuilder)o).append(class1.getName());
            Utils.loge("TransitionLayout", ((StringBuilder)o).toString());
            o = new StringBuilder();
            ((StringBuilder)o).append(class1.getName());
            ((StringBuilder)o).append(" must have a method ");
            ((StringBuilder)o).append(string);
            Utils.loge("TransitionLayout", ((StringBuilder)o).toString());
        }
    }
    
    public boolean diff(final CustomAttribute customAttribute) {
        final boolean b = false;
        final boolean b2 = false;
        final boolean b3 = false;
        final boolean b4 = false;
        final boolean b5 = false;
        final boolean b6 = false;
        boolean b7 = b5;
        if (customAttribute != null) {
            final AttributeType mType = this.mType;
            if (mType != customAttribute.mType) {
                b7 = b5;
            }
            else {
                switch (CustomAttribute$1.$SwitchMap$androidx$constraintlayout$core$motion$CustomAttribute$AttributeType[mType.ordinal()]) {
                    default: {
                        return false;
                    }
                    case 8: {
                        boolean b8 = b6;
                        if (this.mFloatValue == customAttribute.mFloatValue) {
                            b8 = true;
                        }
                        return b8;
                    }
                    case 7: {
                        boolean b9 = b;
                        if (this.mFloatValue == customAttribute.mFloatValue) {
                            b9 = true;
                        }
                        return b9;
                    }
                    case 4:
                    case 5: {
                        boolean b10 = b2;
                        if (this.mColorValue == customAttribute.mColorValue) {
                            b10 = true;
                        }
                        return b10;
                    }
                    case 3: {
                        boolean b11 = b3;
                        if (this.mIntegerValue == customAttribute.mIntegerValue) {
                            b11 = true;
                        }
                        return b11;
                    }
                    case 2: {
                        boolean b12 = b4;
                        if (this.mBooleanValue == customAttribute.mBooleanValue) {
                            b12 = true;
                        }
                        return b12;
                    }
                    case 1:
                    case 6: {
                        b7 = b5;
                        if (this.mIntegerValue == customAttribute.mIntegerValue) {
                            b7 = true;
                            break;
                        }
                        break;
                    }
                }
            }
        }
        return b7;
    }
    
    public AttributeType getType() {
        return this.mType;
    }
    
    public float getValueToInterpolate() {
        switch (CustomAttribute$1.$SwitchMap$androidx$constraintlayout$core$motion$CustomAttribute$AttributeType[this.mType.ordinal()]) {
            default: {
                return Float.NaN;
            }
            case 8: {
                return this.mFloatValue;
            }
            case 7: {
                return this.mFloatValue;
            }
            case 6: {
                return (float)this.mIntegerValue;
            }
            case 4:
            case 5: {
                throw new RuntimeException("Color does not have a single color to interpolate");
            }
            case 3: {
                throw new RuntimeException("Cannot interpolate String");
            }
            case 2: {
                float n;
                if (this.mBooleanValue) {
                    n = 1.0f;
                }
                else {
                    n = 0.0f;
                }
                return n;
            }
        }
    }
    
    public void getValuesToInterpolate(final float[] array) {
        switch (CustomAttribute$1.$SwitchMap$androidx$constraintlayout$core$motion$CustomAttribute$AttributeType[this.mType.ordinal()]) {
            case 8: {
                array[0] = this.mFloatValue;
                break;
            }
            case 7: {
                array[0] = this.mFloatValue;
                break;
            }
            case 6: {
                array[0] = (float)this.mIntegerValue;
                break;
            }
            case 4:
            case 5: {
                final int mColorValue = this.mColorValue;
                final float n = (float)Math.pow((mColorValue >> 16 & 0xFF) / 255.0f, 2.2);
                final float n2 = (float)Math.pow((mColorValue >> 8 & 0xFF) / 255.0f, 2.2);
                final float n3 = (float)Math.pow((mColorValue & 0xFF) / 255.0f, 2.2);
                array[0] = n;
                array[1] = n2;
                array[2] = n3;
                array[3] = (mColorValue >> 24 & 0xFF) / 255.0f;
                break;
            }
            case 3: {
                throw new RuntimeException("Color does not have a single color to interpolate");
            }
            case 2: {
                float n4;
                if (this.mBooleanValue) {
                    n4 = 1.0f;
                }
                else {
                    n4 = 0.0f;
                }
                array[0] = n4;
                break;
            }
        }
    }
    
    public boolean isContinuous() {
        final int n = CustomAttribute$1.$SwitchMap$androidx$constraintlayout$core$motion$CustomAttribute$AttributeType[this.mType.ordinal()];
        return n != 1 && n != 2 && n != 3;
    }
    
    public int numberOfInterpolatedValues() {
        final int n = CustomAttribute$1.$SwitchMap$androidx$constraintlayout$core$motion$CustomAttribute$AttributeType[this.mType.ordinal()];
        if (n != 4 && n != 5) {
            return 1;
        }
        return 4;
    }
    
    public void setColorValue(final int mColorValue) {
        this.mColorValue = mColorValue;
    }
    
    public void setFloatValue(final float mFloatValue) {
        this.mFloatValue = mFloatValue;
    }
    
    public void setIntValue(final int mIntegerValue) {
        this.mIntegerValue = mIntegerValue;
    }
    
    public void setInterpolatedValue(final Object obj, final float[] array) {
        final Class<?> class1 = obj.getClass();
        final StringBuilder sb = new StringBuilder();
        sb.append("set");
        sb.append(this.mName);
        final String string = sb.toString();
        try {
            final int n = CustomAttribute$1.$SwitchMap$androidx$constraintlayout$core$motion$CustomAttribute$AttributeType[this.mType.ordinal()];
            boolean b = true;
            if (n != 2) {
                if (n == 3) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("unable to interpolate strings ");
                    sb2.append(this.mName);
                    throw new RuntimeException(sb2.toString());
                }
                if (n != 4) {
                    if (n != 6) {
                        if (n != 7) {
                            if (n == 8) {
                                class1.getMethod(string, Float.TYPE).invoke(obj, array[0]);
                            }
                        }
                        else {
                            class1.getMethod(string, Float.TYPE).invoke(obj, array[0]);
                        }
                    }
                    else {
                        class1.getMethod(string, Integer.TYPE).invoke(obj, (int)array[0]);
                    }
                }
                else {
                    class1.getMethod(string, Integer.TYPE).invoke(obj, clamp((int)((float)Math.pow(array[1], 0.45454545454545453) * 255.0f)) << 8 | (clamp((int)(array[3] * 255.0f)) << 24 | clamp((int)((float)Math.pow(array[0], 0.45454545454545453) * 255.0f)) << 16) | clamp((int)((float)Math.pow(array[2], 0.45454545454545453) * 255.0f)));
                }
            }
            else {
                final Method method = class1.getMethod(string, Boolean.TYPE);
                if (array[0] <= 0.5f) {
                    b = false;
                }
                method.invoke(obj, b);
            }
        }
        catch (final InvocationTargetException ex) {
            ex.printStackTrace();
        }
        catch (final IllegalAccessException ex2) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("cannot access method ");
            sb3.append(string);
            sb3.append(" on View \"");
            sb3.append(obj.getClass().getName());
            sb3.append("\"");
            Utils.loge("TransitionLayout", sb3.toString());
            ex2.printStackTrace();
        }
        catch (final NoSuchMethodException ex3) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("no method ");
            sb4.append(string);
            sb4.append(" on View \"");
            sb4.append(obj.getClass().getName());
            sb4.append("\"");
            Utils.loge("TransitionLayout", sb4.toString());
            ex3.printStackTrace();
        }
    }
    
    public void setStringValue(final String mStringValue) {
        this.mStringValue = mStringValue;
    }
    
    public void setValue(final Object o) {
        switch (CustomAttribute$1.$SwitchMap$androidx$constraintlayout$core$motion$CustomAttribute$AttributeType[this.mType.ordinal()]) {
            case 8: {
                this.mFloatValue = (float)o;
                break;
            }
            case 7: {
                this.mFloatValue = (float)o;
                break;
            }
            case 4:
            case 5: {
                this.mColorValue = (int)o;
                break;
            }
            case 3: {
                this.mStringValue = (String)o;
                break;
            }
            case 2: {
                this.mBooleanValue = (boolean)o;
                break;
            }
            case 1:
            case 6: {
                this.mIntegerValue = (int)o;
                break;
            }
        }
    }
    
    public void setValue(final float[] array) {
        final int n = CustomAttribute$1.$SwitchMap$androidx$constraintlayout$core$motion$CustomAttribute$AttributeType[this.mType.ordinal()];
        boolean mBooleanValue = true;
        switch (n) {
            case 8: {
                this.mFloatValue = array[0];
                break;
            }
            case 7: {
                this.mFloatValue = array[0];
                break;
            }
            case 4:
            case 5: {
                final int hsvToRgb = hsvToRgb(array[0], array[1], array[2]);
                this.mColorValue = hsvToRgb;
                this.mColorValue = (clamp((int)(array[3] * 255.0f)) << 24 | (hsvToRgb & 0xFFFFFF));
                break;
            }
            case 3: {
                throw new RuntimeException("Color does not have a single color to interpolate");
            }
            case 2: {
                if (array[0] <= 0.5) {
                    mBooleanValue = false;
                }
                this.mBooleanValue = mBooleanValue;
                break;
            }
            case 1:
            case 6: {
                this.mIntegerValue = (int)array[0];
                break;
            }
        }
    }
    
    public enum AttributeType
    {
        private static final AttributeType[] $VALUES;
        
        BOOLEAN_TYPE, 
        COLOR_DRAWABLE_TYPE, 
        COLOR_TYPE, 
        DIMENSION_TYPE, 
        FLOAT_TYPE, 
        INT_TYPE, 
        REFERENCE_TYPE, 
        STRING_TYPE;
    }
}

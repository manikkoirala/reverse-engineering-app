// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core;

import java.io.PrintStream;
import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import java.util.Arrays;
import java.util.HashMap;

public class LinearSystem
{
    public static long ARRAY_ROW_CREATION = 0L;
    public static final boolean DEBUG = false;
    private static final boolean DEBUG_CONSTRAINTS = false;
    public static final boolean FULL_DEBUG = false;
    public static final boolean MEASURE = false;
    public static long OPTIMIZED_ARRAY_ROW_CREATION = 0L;
    public static boolean OPTIMIZED_ENGINE = false;
    private static int POOL_SIZE = 1000;
    public static boolean SIMPLIFY_SYNONYMS = true;
    public static boolean SKIP_COLUMNS = true;
    public static boolean USE_BASIC_SYNONYMS = true;
    public static boolean USE_DEPENDENCY_ORDERING = false;
    public static boolean USE_SYNONYMS = true;
    public static Metrics sMetrics;
    private int TABLE_SIZE;
    public boolean graphOptimizer;
    public boolean hasSimpleDefinition;
    private boolean[] mAlreadyTestedCandidates;
    final Cache mCache;
    private Row mGoal;
    private int mMaxColumns;
    private int mMaxRows;
    int mNumColumns;
    int mNumRows;
    private SolverVariable[] mPoolVariables;
    private int mPoolVariablesCount;
    ArrayRow[] mRows;
    private Row mTempGoal;
    private HashMap<String, SolverVariable> mVariables;
    int mVariablesID;
    public boolean newgraphOptimizer;
    
    public LinearSystem() {
        this.hasSimpleDefinition = false;
        this.mVariablesID = 0;
        this.mVariables = null;
        this.TABLE_SIZE = 32;
        this.mMaxColumns = 32;
        this.mRows = null;
        this.graphOptimizer = false;
        this.newgraphOptimizer = false;
        this.mAlreadyTestedCandidates = new boolean[32];
        this.mNumColumns = 1;
        this.mNumRows = 0;
        this.mMaxRows = 32;
        this.mPoolVariables = new SolverVariable[LinearSystem.POOL_SIZE];
        this.mPoolVariablesCount = 0;
        this.mRows = new ArrayRow[32];
        this.releaseRows();
        final Cache mCache = new Cache();
        this.mCache = mCache;
        this.mGoal = (Row)new PriorityGoalRow(mCache);
        if (LinearSystem.OPTIMIZED_ENGINE) {
            this.mTempGoal = (Row)new ValuesRow(mCache);
        }
        else {
            this.mTempGoal = (Row)new ArrayRow(mCache);
        }
    }
    
    private SolverVariable acquireSolverVariable(final SolverVariable.Type type, final String s) {
        final SolverVariable solverVariable = this.mCache.solverVariablePool.acquire();
        SolverVariable solverVariable3;
        if (solverVariable == null) {
            final SolverVariable solverVariable2 = new SolverVariable(type, s);
            solverVariable2.setType(type, s);
            solverVariable3 = solverVariable2;
        }
        else {
            solverVariable.reset();
            solverVariable.setType(type, s);
            solverVariable3 = solverVariable;
        }
        final int mPoolVariablesCount = this.mPoolVariablesCount;
        final int pool_SIZE = LinearSystem.POOL_SIZE;
        if (mPoolVariablesCount >= pool_SIZE) {
            this.mPoolVariables = Arrays.copyOf(this.mPoolVariables, LinearSystem.POOL_SIZE = pool_SIZE * 2);
        }
        return this.mPoolVariables[this.mPoolVariablesCount++] = solverVariable3;
    }
    
    private void addError(final ArrayRow arrayRow) {
        arrayRow.addError(this, 0);
    }
    
    private final void addRow(ArrayRow arrayRow) {
        if (LinearSystem.SIMPLIFY_SYNONYMS && arrayRow.isSimpleDefinition) {
            arrayRow.variable.setFinalValue(this, arrayRow.constantValue);
        }
        else {
            final ArrayRow[] mRows = this.mRows;
            final int mNumRows = this.mNumRows;
            mRows[mNumRows] = arrayRow;
            final SolverVariable variable = arrayRow.variable;
            variable.definitionId = mNumRows;
            this.mNumRows = mNumRows + 1;
            variable.updateReferencesWithNewDefinition(this, arrayRow);
        }
        if (LinearSystem.SIMPLIFY_SYNONYMS && this.hasSimpleDefinition) {
            int n;
            for (int i = 0; i < this.mNumRows; i = n + 1) {
                if (this.mRows[i] == null) {
                    System.out.println("WTF");
                }
                arrayRow = this.mRows[i];
                n = i;
                if (arrayRow != null) {
                    n = i;
                    if (arrayRow.isSimpleDefinition) {
                        arrayRow.variable.setFinalValue(this, arrayRow.constantValue);
                        if (LinearSystem.OPTIMIZED_ENGINE) {
                            this.mCache.optimizedArrayRowPool.release(arrayRow);
                        }
                        else {
                            this.mCache.arrayRowPool.release(arrayRow);
                        }
                        this.mRows[i] = null;
                        int n3;
                        int n2 = n3 = i + 1;
                        int mNumRows2;
                        while (true) {
                            mNumRows2 = this.mNumRows;
                            if (n2 >= mNumRows2) {
                                break;
                            }
                            final ArrayRow[] mRows2 = this.mRows;
                            final int definitionId = n2 - 1;
                            arrayRow = mRows2[n2];
                            mRows2[definitionId] = arrayRow;
                            final SolverVariable variable2 = arrayRow.variable;
                            if (variable2.definitionId == n2) {
                                variable2.definitionId = definitionId;
                            }
                            n3 = n2;
                            ++n2;
                        }
                        if (n3 < mNumRows2) {
                            this.mRows[n3] = null;
                        }
                        this.mNumRows = mNumRows2 - 1;
                        n = i - 1;
                    }
                }
            }
            this.hasSimpleDefinition = false;
        }
    }
    
    private void addSingleError(final ArrayRow arrayRow, final int n) {
        this.addSingleError(arrayRow, n, 0);
    }
    
    private void computeValues() {
        for (int i = 0; i < this.mNumRows; ++i) {
            final ArrayRow arrayRow = this.mRows[i];
            arrayRow.variable.computedValue = arrayRow.constantValue;
        }
    }
    
    public static ArrayRow createRowDimensionPercent(final LinearSystem linearSystem, final SolverVariable solverVariable, final SolverVariable solverVariable2, final float n) {
        return linearSystem.createRow().createRowDimensionPercent(solverVariable, solverVariable2, n);
    }
    
    private SolverVariable createVariable(final String s, final SolverVariable.Type type) {
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.variables;
        }
        if (this.mNumColumns + 1 >= this.mMaxColumns) {
            this.increaseTableSize();
        }
        final SolverVariable acquireSolverVariable = this.acquireSolverVariable(type, null);
        acquireSolverVariable.setName(s);
        final int n = this.mVariablesID + 1;
        this.mVariablesID = n;
        ++this.mNumColumns;
        acquireSolverVariable.id = n;
        if (this.mVariables == null) {
            this.mVariables = new HashMap<String, SolverVariable>();
        }
        this.mVariables.put(s, acquireSolverVariable);
        return this.mCache.mIndexedVariables[this.mVariablesID] = acquireSolverVariable;
    }
    
    private void displayRows() {
        this.displaySolverVariables();
        String string = "";
        for (int i = 0; i < this.mNumRows; ++i) {
            final StringBuilder sb = new StringBuilder();
            sb.append(string);
            sb.append(this.mRows[i]);
            final String string2 = sb.toString();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(string2);
            sb2.append("\n");
            string = sb2.toString();
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(string);
        sb3.append(this.mGoal);
        sb3.append("\n");
        System.out.println(sb3.toString());
    }
    
    private void displaySolverVariables() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Display Rows (");
        sb.append(this.mNumRows);
        sb.append("x");
        sb.append(this.mNumColumns);
        sb.append(")\n");
        System.out.println(sb.toString());
    }
    
    private int enforceBFS(final Row row) throws Exception {
        while (true) {
            for (int i = 0; i < this.mNumRows; ++i) {
                final ArrayRow arrayRow = this.mRows[i];
                if (arrayRow.variable.mType != SolverVariable.Type.UNRESTRICTED) {
                    if (arrayRow.constantValue < 0.0f) {
                        final boolean b = true;
                        int n;
                        if (b) {
                            int j = 0;
                            n = 0;
                            while (j == 0) {
                                final Metrics sMetrics = LinearSystem.sMetrics;
                                if (sMetrics != null) {
                                    ++sMetrics.bfs;
                                }
                                final int n2 = n + 1;
                                float n3 = Float.MAX_VALUE;
                                int k = 0;
                                int definitionId = -1;
                                int id = -1;
                                int n4 = 0;
                                while (k < this.mNumRows) {
                                    final ArrayRow arrayRow2 = this.mRows[k];
                                    float n5;
                                    int n6;
                                    int n7;
                                    int n8;
                                    if (arrayRow2.variable.mType == SolverVariable.Type.UNRESTRICTED) {
                                        n5 = n3;
                                        n6 = definitionId;
                                        n7 = id;
                                        n8 = n4;
                                    }
                                    else if (arrayRow2.isSimpleDefinition) {
                                        n5 = n3;
                                        n6 = definitionId;
                                        n7 = id;
                                        n8 = n4;
                                    }
                                    else {
                                        n5 = n3;
                                        n6 = definitionId;
                                        n7 = id;
                                        n8 = n4;
                                        if (arrayRow2.constantValue < 0.0f) {
                                            if (LinearSystem.SKIP_COLUMNS) {
                                                final int currentSize = arrayRow2.variables.getCurrentSize();
                                                int n9 = 0;
                                                while (true) {
                                                    n5 = n3;
                                                    n6 = definitionId;
                                                    n7 = id;
                                                    n8 = n4;
                                                    if (n9 >= currentSize) {
                                                        break;
                                                    }
                                                    final SolverVariable variable = arrayRow2.variables.getVariable(n9);
                                                    final float value = arrayRow2.variables.get(variable);
                                                    float n10;
                                                    int n11;
                                                    int n12;
                                                    int n13;
                                                    if (value <= 0.0f) {
                                                        n10 = n3;
                                                        n11 = definitionId;
                                                        n12 = id;
                                                        n13 = n4;
                                                    }
                                                    else {
                                                        final int n14 = 0;
                                                        int n15 = definitionId;
                                                        int n16 = n14;
                                                        while (true) {
                                                            n10 = n3;
                                                            n11 = n15;
                                                            n12 = id;
                                                            n13 = n4;
                                                            if (n16 >= 9) {
                                                                break;
                                                            }
                                                            final float n17 = variable.strengthVector[n16] / value;
                                                            int n18;
                                                            if ((n17 < n3 && n16 == n4) || n16 > (n18 = n4)) {
                                                                id = variable.id;
                                                                n18 = n16;
                                                                n15 = k;
                                                                n3 = n17;
                                                            }
                                                            ++n16;
                                                            n4 = n18;
                                                        }
                                                    }
                                                    ++n9;
                                                    n3 = n10;
                                                    definitionId = n11;
                                                    id = n12;
                                                    n4 = n13;
                                                }
                                            }
                                            else {
                                                int n19 = 1;
                                                while (true) {
                                                    n5 = n3;
                                                    n6 = definitionId;
                                                    n7 = id;
                                                    n8 = n4;
                                                    if (n19 >= this.mNumColumns) {
                                                        break;
                                                    }
                                                    final SolverVariable solverVariable = this.mCache.mIndexedVariables[n19];
                                                    final float value2 = arrayRow2.variables.get(solverVariable);
                                                    float n20;
                                                    int n21;
                                                    int n22;
                                                    int n23;
                                                    if (value2 <= 0.0f) {
                                                        n20 = n3;
                                                        n21 = definitionId;
                                                        n22 = id;
                                                        n23 = n4;
                                                    }
                                                    else {
                                                        final int n24 = 0;
                                                        int n25 = definitionId;
                                                        int n26 = n24;
                                                        while (true) {
                                                            n20 = n3;
                                                            n21 = n25;
                                                            n22 = id;
                                                            n23 = n4;
                                                            if (n26 >= 9) {
                                                                break;
                                                            }
                                                            final float n27 = solverVariable.strengthVector[n26] / value2;
                                                            int n28;
                                                            if ((n27 < n3 && n26 == n4) || n26 > (n28 = n4)) {
                                                                id = n19;
                                                                n28 = n26;
                                                                n25 = k;
                                                                n3 = n27;
                                                            }
                                                            ++n26;
                                                            n4 = n28;
                                                        }
                                                    }
                                                    ++n19;
                                                    n3 = n20;
                                                    definitionId = n21;
                                                    id = n22;
                                                    n4 = n23;
                                                }
                                            }
                                        }
                                    }
                                    ++k;
                                    n3 = n5;
                                    definitionId = n6;
                                    id = n7;
                                    n4 = n8;
                                }
                                if (definitionId != -1) {
                                    final ArrayRow arrayRow3 = this.mRows[definitionId];
                                    arrayRow3.variable.definitionId = -1;
                                    final Metrics sMetrics2 = LinearSystem.sMetrics;
                                    if (sMetrics2 != null) {
                                        ++sMetrics2.pivots;
                                    }
                                    arrayRow3.pivot(this.mCache.mIndexedVariables[id]);
                                    final SolverVariable variable2 = arrayRow3.variable;
                                    variable2.definitionId = definitionId;
                                    variable2.updateReferencesWithNewDefinition(this, arrayRow3);
                                }
                                else {
                                    j = 1;
                                }
                                n = n2;
                                if (n2 > this.mNumColumns / 2) {
                                    j = 1;
                                    n = n2;
                                }
                            }
                        }
                        else {
                            n = 0;
                        }
                        return n;
                    }
                }
            }
            final boolean b = false;
            continue;
        }
    }
    
    private String getDisplaySize(int i) {
        final int j = i * 4;
        final int k = j / 1024;
        i = k / 1024;
        if (i > 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(i);
            sb.append(" Mb");
            return sb.toString();
        }
        if (k > 0) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("");
            sb2.append(k);
            sb2.append(" Kb");
            return sb2.toString();
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("");
        sb3.append(j);
        sb3.append(" bytes");
        return sb3.toString();
    }
    
    private String getDisplayStrength(final int n) {
        if (n == 1) {
            return "LOW";
        }
        if (n == 2) {
            return "MEDIUM";
        }
        if (n == 3) {
            return "HIGH";
        }
        if (n == 4) {
            return "HIGHEST";
        }
        if (n == 5) {
            return "EQUALITY";
        }
        if (n == 8) {
            return "FIXED";
        }
        if (n == 6) {
            return "BARRIER";
        }
        return "NONE";
    }
    
    public static Metrics getMetrics() {
        return LinearSystem.sMetrics;
    }
    
    private void increaseTableSize() {
        final int n = this.TABLE_SIZE * 2;
        this.TABLE_SIZE = n;
        this.mRows = Arrays.copyOf(this.mRows, n);
        final Cache mCache = this.mCache;
        mCache.mIndexedVariables = Arrays.copyOf(mCache.mIndexedVariables, this.TABLE_SIZE);
        final int table_SIZE = this.TABLE_SIZE;
        this.mAlreadyTestedCandidates = new boolean[table_SIZE];
        this.mMaxColumns = table_SIZE;
        this.mMaxRows = table_SIZE;
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.tableSizeIncrease;
            sMetrics.maxTableSize = Math.max(sMetrics.maxTableSize, table_SIZE);
            final Metrics sMetrics2 = LinearSystem.sMetrics;
            sMetrics2.lastTableSize = sMetrics2.maxTableSize;
        }
    }
    
    private final int optimize(final Row row, final boolean b) {
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.optimize;
        }
        for (int i = 0; i < this.mNumColumns; ++i) {
            this.mAlreadyTestedCandidates[i] = false;
        }
        int j = 0;
        int n = 0;
        while (j == 0) {
            final Metrics sMetrics2 = LinearSystem.sMetrics;
            if (sMetrics2 != null) {
                ++sMetrics2.iterations;
            }
            final int n2 = n + 1;
            if (n2 >= this.mNumColumns * 2) {
                return n2;
            }
            if (row.getKey() != null) {
                this.mAlreadyTestedCandidates[row.getKey().id] = true;
            }
            final SolverVariable pivotCandidate = row.getPivotCandidate(this, this.mAlreadyTestedCandidates);
            if (pivotCandidate != null) {
                final boolean[] mAlreadyTestedCandidates = this.mAlreadyTestedCandidates;
                final int id = pivotCandidate.id;
                if (mAlreadyTestedCandidates[id]) {
                    return n2;
                }
                mAlreadyTestedCandidates[id] = true;
            }
            if (pivotCandidate != null) {
                float n3 = Float.MAX_VALUE;
                int k = 0;
                int definitionId = -1;
                while (k < this.mNumRows) {
                    final ArrayRow arrayRow = this.mRows[k];
                    float n4;
                    int n5;
                    if (arrayRow.variable.mType == SolverVariable.Type.UNRESTRICTED) {
                        n4 = n3;
                        n5 = definitionId;
                    }
                    else if (arrayRow.isSimpleDefinition) {
                        n4 = n3;
                        n5 = definitionId;
                    }
                    else {
                        n4 = n3;
                        n5 = definitionId;
                        if (arrayRow.hasVariable(pivotCandidate)) {
                            final float value = arrayRow.variables.get(pivotCandidate);
                            n4 = n3;
                            n5 = definitionId;
                            if (value < 0.0f) {
                                final float n6 = -arrayRow.constantValue / value;
                                n4 = n3;
                                n5 = definitionId;
                                if (n6 < n3) {
                                    n5 = k;
                                    n4 = n6;
                                }
                            }
                        }
                    }
                    ++k;
                    n3 = n4;
                    definitionId = n5;
                }
                n = n2;
                if (definitionId <= -1) {
                    continue;
                }
                final ArrayRow arrayRow2 = this.mRows[definitionId];
                arrayRow2.variable.definitionId = -1;
                final Metrics sMetrics3 = LinearSystem.sMetrics;
                if (sMetrics3 != null) {
                    ++sMetrics3.pivots;
                }
                arrayRow2.pivot(pivotCandidate);
                final SolverVariable variable = arrayRow2.variable;
                variable.definitionId = definitionId;
                variable.updateReferencesWithNewDefinition(this, arrayRow2);
                n = n2;
            }
            else {
                j = 1;
                n = n2;
            }
        }
        return n;
    }
    
    private void releaseRows() {
        final boolean optimized_ENGINE = LinearSystem.OPTIMIZED_ENGINE;
        int i = 0;
        final int n = 0;
        if (optimized_ENGINE) {
            for (int j = n; j < this.mNumRows; ++j) {
                final ArrayRow arrayRow = this.mRows[j];
                if (arrayRow != null) {
                    this.mCache.optimizedArrayRowPool.release(arrayRow);
                }
                this.mRows[j] = null;
            }
        }
        else {
            while (i < this.mNumRows) {
                final ArrayRow arrayRow2 = this.mRows[i];
                if (arrayRow2 != null) {
                    this.mCache.arrayRowPool.release(arrayRow2);
                }
                this.mRows[i] = null;
                ++i;
            }
        }
    }
    
    public void addCenterPoint(final ConstraintWidget constraintWidget, final ConstraintWidget constraintWidget2, final float n, final int n2) {
        final ConstraintAnchor.Type left = ConstraintAnchor.Type.LEFT;
        final SolverVariable objectVariable = this.createObjectVariable(constraintWidget.getAnchor(left));
        final ConstraintAnchor.Type top = ConstraintAnchor.Type.TOP;
        final SolverVariable objectVariable2 = this.createObjectVariable(constraintWidget.getAnchor(top));
        final ConstraintAnchor.Type right = ConstraintAnchor.Type.RIGHT;
        final SolverVariable objectVariable3 = this.createObjectVariable(constraintWidget.getAnchor(right));
        final ConstraintAnchor.Type bottom = ConstraintAnchor.Type.BOTTOM;
        final SolverVariable objectVariable4 = this.createObjectVariable(constraintWidget.getAnchor(bottom));
        final SolverVariable objectVariable5 = this.createObjectVariable(constraintWidget2.getAnchor(left));
        final SolverVariable objectVariable6 = this.createObjectVariable(constraintWidget2.getAnchor(top));
        final SolverVariable objectVariable7 = this.createObjectVariable(constraintWidget2.getAnchor(right));
        final SolverVariable objectVariable8 = this.createObjectVariable(constraintWidget2.getAnchor(bottom));
        final ArrayRow row = this.createRow();
        final double n3 = n;
        final double sin = Math.sin(n3);
        final double n4 = n2;
        row.createRowWithAngle(objectVariable2, objectVariable4, objectVariable6, objectVariable8, (float)(sin * n4));
        this.addConstraint(row);
        final ArrayRow row2 = this.createRow();
        row2.createRowWithAngle(objectVariable, objectVariable3, objectVariable5, objectVariable7, (float)(Math.cos(n3) * n4));
        this.addConstraint(row2);
    }
    
    public void addCentering(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final float n2, final SolverVariable solverVariable3, final SolverVariable solverVariable4, final int n3, final int n4) {
        final ArrayRow row = this.createRow();
        row.createRowCentering(solverVariable, solverVariable2, n, n2, solverVariable3, solverVariable4, n3);
        if (n4 != 8) {
            row.addError(this, n4);
        }
        this.addConstraint(row);
    }
    
    public void addConstraint(final ArrayRow arrayRow) {
        if (arrayRow == null) {
            return;
        }
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.constraints;
            if (arrayRow.isSimpleDefinition) {
                ++sMetrics.simpleconstraints;
            }
        }
        final int mNumRows = this.mNumRows;
        final boolean b = true;
        if (mNumRows + 1 >= this.mMaxRows || this.mNumColumns + 1 >= this.mMaxColumns) {
            this.increaseTableSize();
        }
        final boolean isSimpleDefinition = arrayRow.isSimpleDefinition;
        int n = 0;
        if (!isSimpleDefinition) {
            arrayRow.updateFromSystem(this);
            if (arrayRow.isEmpty()) {
                return;
            }
            arrayRow.ensurePositiveConstant();
            Label_0301: {
                if (arrayRow.chooseSubject(this)) {
                    final SolverVariable extraVariable = this.createExtraVariable();
                    arrayRow.variable = extraVariable;
                    final int mNumRows2 = this.mNumRows;
                    this.addRow(arrayRow);
                    if (this.mNumRows == mNumRows2 + 1) {
                        this.mTempGoal.initFromRow((Row)arrayRow);
                        this.optimize(this.mTempGoal, true);
                        n = (b ? 1 : 0);
                        if (extraVariable.definitionId == -1) {
                            if (arrayRow.variable == extraVariable) {
                                final SolverVariable pickPivot = arrayRow.pickPivot(extraVariable);
                                if (pickPivot != null) {
                                    final Metrics sMetrics2 = LinearSystem.sMetrics;
                                    if (sMetrics2 != null) {
                                        ++sMetrics2.pivots;
                                    }
                                    arrayRow.pivot(pickPivot);
                                }
                            }
                            if (!arrayRow.isSimpleDefinition) {
                                arrayRow.variable.updateReferencesWithNewDefinition(this, arrayRow);
                            }
                            if (LinearSystem.OPTIMIZED_ENGINE) {
                                this.mCache.optimizedArrayRowPool.release(arrayRow);
                            }
                            else {
                                this.mCache.arrayRowPool.release(arrayRow);
                            }
                            --this.mNumRows;
                            n = (b ? 1 : 0);
                        }
                        break Label_0301;
                    }
                }
                n = 0;
            }
            if (!arrayRow.hasKeyVariable()) {
                return;
            }
        }
        if (n == 0) {
            this.addRow(arrayRow);
        }
    }
    
    public ArrayRow addEquality(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final int n2) {
        if (LinearSystem.USE_BASIC_SYNONYMS && n2 == 8 && solverVariable2.isFinalValue && solverVariable.definitionId == -1) {
            solverVariable.setFinalValue(this, solverVariable2.computedValue + n);
            return null;
        }
        final ArrayRow row = this.createRow();
        row.createRowEquals(solverVariable, solverVariable2, n);
        if (n2 != 8) {
            row.addError(this, n2);
        }
        this.addConstraint(row);
        return row;
    }
    
    public void addEquality(final SolverVariable solverVariable, int i) {
        if (LinearSystem.USE_BASIC_SYNONYMS && solverVariable.definitionId == -1) {
            final float n = (float)i;
            solverVariable.setFinalValue(this, n);
            SolverVariable solverVariable2;
            for (i = 0; i < this.mVariablesID + 1; ++i) {
                solverVariable2 = this.mCache.mIndexedVariables[i];
                if (solverVariable2 != null && solverVariable2.isSynonym && solverVariable2.synonym == solverVariable.id) {
                    solverVariable2.setFinalValue(this, solverVariable2.synonymDelta + n);
                }
            }
            return;
        }
        final int definitionId = solverVariable.definitionId;
        if (definitionId != -1) {
            final ArrayRow arrayRow = this.mRows[definitionId];
            if (arrayRow.isSimpleDefinition) {
                arrayRow.constantValue = (float)i;
            }
            else if (arrayRow.variables.getCurrentSize() == 0) {
                arrayRow.isSimpleDefinition = true;
                arrayRow.constantValue = (float)i;
            }
            else {
                final ArrayRow row = this.createRow();
                row.createRowEquals(solverVariable, i);
                this.addConstraint(row);
            }
        }
        else {
            final ArrayRow row2 = this.createRow();
            row2.createRowDefinition(solverVariable, i);
            this.addConstraint(row2);
        }
    }
    
    public void addGreaterBarrier(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final boolean b) {
        final ArrayRow row = this.createRow();
        final SolverVariable slackVariable = this.createSlackVariable();
        slackVariable.strength = 0;
        row.createRowGreaterThan(solverVariable, solverVariable2, slackVariable, n);
        this.addConstraint(row);
    }
    
    public void addGreaterThan(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final int n2) {
        final ArrayRow row = this.createRow();
        final SolverVariable slackVariable = this.createSlackVariable();
        slackVariable.strength = 0;
        row.createRowGreaterThan(solverVariable, solverVariable2, slackVariable, n);
        if (n2 != 8) {
            this.addSingleError(row, (int)(row.variables.get(slackVariable) * -1.0f), n2);
        }
        this.addConstraint(row);
    }
    
    public void addLowerBarrier(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final boolean b) {
        final ArrayRow row = this.createRow();
        final SolverVariable slackVariable = this.createSlackVariable();
        slackVariable.strength = 0;
        row.createRowLowerThan(solverVariable, solverVariable2, slackVariable, n);
        this.addConstraint(row);
    }
    
    public void addLowerThan(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final int n2) {
        final ArrayRow row = this.createRow();
        final SolverVariable slackVariable = this.createSlackVariable();
        slackVariable.strength = 0;
        row.createRowLowerThan(solverVariable, solverVariable2, slackVariable, n);
        if (n2 != 8) {
            this.addSingleError(row, (int)(row.variables.get(slackVariable) * -1.0f), n2);
        }
        this.addConstraint(row);
    }
    
    public void addRatio(final SolverVariable solverVariable, final SolverVariable solverVariable2, final SolverVariable solverVariable3, final SolverVariable solverVariable4, final float n, final int n2) {
        final ArrayRow row = this.createRow();
        row.createRowDimensionRatio(solverVariable, solverVariable2, solverVariable3, solverVariable4, n);
        if (n2 != 8) {
            row.addError(this, n2);
        }
        this.addConstraint(row);
    }
    
    void addSingleError(final ArrayRow arrayRow, final int n, final int n2) {
        arrayRow.addSingleError(this.createErrorVariable(n2, null), n);
    }
    
    public void addSynonym(SolverVariable solverVariable, final SolverVariable solverVariable2, final int n) {
        if (solverVariable.definitionId == -1 && n == 0) {
            SolverVariable solverVariable3 = solverVariable2;
            if (solverVariable2.isSynonym) {
                solverVariable3 = this.mCache.mIndexedVariables[solverVariable2.synonym];
            }
            if (solverVariable.isSynonym) {
                solverVariable = this.mCache.mIndexedVariables[solverVariable.synonym];
            }
            else {
                solverVariable.setSynonym(this, solverVariable3, 0.0f);
            }
        }
        else {
            this.addEquality(solverVariable, solverVariable2, n, 8);
        }
    }
    
    final void cleanupRows() {
        int n;
        for (int i = 0; i < this.mNumRows; i = n + 1) {
            final ArrayRow arrayRow = this.mRows[i];
            if (arrayRow.variables.getCurrentSize() == 0) {
                arrayRow.isSimpleDefinition = true;
            }
            n = i;
            if (arrayRow.isSimpleDefinition) {
                final SolverVariable variable = arrayRow.variable;
                variable.computedValue = arrayRow.constantValue;
                variable.removeFromRow(arrayRow);
                int n2 = i;
                int mNumRows;
                while (true) {
                    mNumRows = this.mNumRows;
                    if (n2 >= mNumRows - 1) {
                        break;
                    }
                    final ArrayRow[] mRows = this.mRows;
                    final int n3 = n2 + 1;
                    mRows[n2] = mRows[n3];
                    n2 = n3;
                }
                this.mRows[mNumRows - 1] = null;
                this.mNumRows = mNumRows - 1;
                n = i - 1;
                if (LinearSystem.OPTIMIZED_ENGINE) {
                    this.mCache.optimizedArrayRowPool.release(arrayRow);
                }
                else {
                    this.mCache.arrayRowPool.release(arrayRow);
                }
            }
        }
    }
    
    public SolverVariable createErrorVariable(final int strength, final String s) {
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.errors;
        }
        if (this.mNumColumns + 1 >= this.mMaxColumns) {
            this.increaseTableSize();
        }
        final SolverVariable acquireSolverVariable = this.acquireSolverVariable(SolverVariable.Type.ERROR, s);
        final int n = this.mVariablesID + 1;
        this.mVariablesID = n;
        ++this.mNumColumns;
        acquireSolverVariable.id = n;
        acquireSolverVariable.strength = strength;
        this.mCache.mIndexedVariables[n] = acquireSolverVariable;
        this.mGoal.addError(acquireSolverVariable);
        return acquireSolverVariable;
    }
    
    public SolverVariable createExtraVariable() {
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.extravariables;
        }
        if (this.mNumColumns + 1 >= this.mMaxColumns) {
            this.increaseTableSize();
        }
        final SolverVariable acquireSolverVariable = this.acquireSolverVariable(SolverVariable.Type.SLACK, null);
        final int n = this.mVariablesID + 1;
        this.mVariablesID = n;
        ++this.mNumColumns;
        acquireSolverVariable.id = n;
        return this.mCache.mIndexedVariables[n] = acquireSolverVariable;
    }
    
    public SolverVariable createObjectVariable(final Object o) {
        SolverVariable solverVariable = null;
        if (o == null) {
            return null;
        }
        if (this.mNumColumns + 1 >= this.mMaxColumns) {
            this.increaseTableSize();
        }
        if (o instanceof ConstraintAnchor) {
            final ConstraintAnchor constraintAnchor = (ConstraintAnchor)o;
            SolverVariable solverVariable2;
            if ((solverVariable2 = constraintAnchor.getSolverVariable()) == null) {
                constraintAnchor.resetSolverVariable(this.mCache);
                solverVariable2 = constraintAnchor.getSolverVariable();
            }
            final int id = solverVariable2.id;
            if (id != -1 && id <= this.mVariablesID) {
                solverVariable = solverVariable2;
                if (this.mCache.mIndexedVariables[id] != null) {
                    return solverVariable;
                }
            }
            if (id != -1) {
                solverVariable2.reset();
            }
            final int n = this.mVariablesID + 1;
            this.mVariablesID = n;
            ++this.mNumColumns;
            solverVariable2.id = n;
            solverVariable2.mType = SolverVariable.Type.UNRESTRICTED;
            this.mCache.mIndexedVariables[n] = solverVariable2;
            solverVariable = solverVariable2;
        }
        return solverVariable;
    }
    
    public ArrayRow createRow() {
        ArrayRow arrayRow;
        if (LinearSystem.OPTIMIZED_ENGINE) {
            arrayRow = this.mCache.optimizedArrayRowPool.acquire();
            if (arrayRow == null) {
                arrayRow = new ValuesRow(this.mCache);
                ++LinearSystem.OPTIMIZED_ARRAY_ROW_CREATION;
            }
            else {
                arrayRow.reset();
            }
        }
        else {
            arrayRow = this.mCache.arrayRowPool.acquire();
            if (arrayRow == null) {
                arrayRow = new ArrayRow(this.mCache);
                ++LinearSystem.ARRAY_ROW_CREATION;
            }
            else {
                arrayRow.reset();
            }
        }
        SolverVariable.increaseErrorId();
        return arrayRow;
    }
    
    public SolverVariable createSlackVariable() {
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.slackvariables;
        }
        if (this.mNumColumns + 1 >= this.mMaxColumns) {
            this.increaseTableSize();
        }
        final SolverVariable acquireSolverVariable = this.acquireSolverVariable(SolverVariable.Type.SLACK, null);
        final int n = this.mVariablesID + 1;
        this.mVariablesID = n;
        ++this.mNumColumns;
        acquireSolverVariable.id = n;
        return this.mCache.mIndexedVariables[n] = acquireSolverVariable;
    }
    
    public void displayReadableRows() {
        this.displaySolverVariables();
        final StringBuilder sb = new StringBuilder();
        sb.append(" num vars ");
        sb.append(this.mVariablesID);
        sb.append("\n");
        String string = sb.toString();
        final int n = 0;
        String string2;
        for (int i = 0; i < this.mVariablesID + 1; ++i, string = string2) {
            final SolverVariable obj = this.mCache.mIndexedVariables[i];
            string2 = string;
            if (obj != null) {
                string2 = string;
                if (obj.isFinalValue) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append(string);
                    sb2.append(" $[");
                    sb2.append(i);
                    sb2.append("] => ");
                    sb2.append(obj);
                    sb2.append(" = ");
                    sb2.append(obj.computedValue);
                    sb2.append("\n");
                    string2 = sb2.toString();
                }
            }
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(string);
        sb3.append("\n");
        String string3 = sb3.toString();
        String string4;
        for (int j = 0; j < this.mVariablesID + 1; ++j, string3 = string4) {
            final SolverVariable[] mIndexedVariables = this.mCache.mIndexedVariables;
            final SolverVariable obj2 = mIndexedVariables[j];
            string4 = string3;
            if (obj2 != null) {
                string4 = string3;
                if (obj2.isSynonym) {
                    final SolverVariable obj3 = mIndexedVariables[obj2.synonym];
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append(string3);
                    sb4.append(" ~[");
                    sb4.append(j);
                    sb4.append("] => ");
                    sb4.append(obj2);
                    sb4.append(" = ");
                    sb4.append(obj3);
                    sb4.append(" + ");
                    sb4.append(obj2.synonymDelta);
                    sb4.append("\n");
                    string4 = sb4.toString();
                }
            }
        }
        final StringBuilder sb5 = new StringBuilder();
        sb5.append(string3);
        sb5.append("\n\n #  ");
        String s = sb5.toString();
        for (int k = n; k < this.mNumRows; ++k) {
            final StringBuilder sb6 = new StringBuilder();
            sb6.append(s);
            sb6.append(this.mRows[k].toReadableString());
            final String string5 = sb6.toString();
            final StringBuilder sb7 = new StringBuilder();
            sb7.append(string5);
            sb7.append("\n #  ");
            s = sb7.toString();
        }
        String string6 = s;
        if (this.mGoal != null) {
            final StringBuilder sb8 = new StringBuilder();
            sb8.append(s);
            sb8.append("Goal: ");
            sb8.append(this.mGoal);
            sb8.append("\n");
            string6 = sb8.toString();
        }
        System.out.println(string6);
    }
    
    void displaySystemInformation() {
        int i = 0;
        int n = 0;
        while (i < this.TABLE_SIZE) {
            final ArrayRow arrayRow = this.mRows[i];
            int n2 = n;
            if (arrayRow != null) {
                n2 = n + arrayRow.sizeInBytes();
            }
            ++i;
            n = n2;
        }
        int j = 0;
        int n3 = 0;
        while (j < this.mNumRows) {
            final ArrayRow arrayRow2 = this.mRows[j];
            int n4 = n3;
            if (arrayRow2 != null) {
                n4 = n3 + arrayRow2.sizeInBytes();
            }
            ++j;
            n3 = n4;
        }
        final PrintStream out = System.out;
        final StringBuilder sb = new StringBuilder();
        sb.append("Linear System -> Table size: ");
        sb.append(this.TABLE_SIZE);
        sb.append(" (");
        final int table_SIZE = this.TABLE_SIZE;
        sb.append(this.getDisplaySize(table_SIZE * table_SIZE));
        sb.append(") -- row sizes: ");
        sb.append(this.getDisplaySize(n));
        sb.append(", actual size: ");
        sb.append(this.getDisplaySize(n3));
        sb.append(" rows: ");
        sb.append(this.mNumRows);
        sb.append("/");
        sb.append(this.mMaxRows);
        sb.append(" cols: ");
        sb.append(this.mNumColumns);
        sb.append("/");
        sb.append(this.mMaxColumns);
        sb.append(" ");
        sb.append(0);
        sb.append(" occupied cells, ");
        sb.append(this.getDisplaySize(0));
        out.println(sb.toString());
    }
    
    public void displayVariablesReadableRows() {
        this.displaySolverVariables();
        String s = "";
        String string;
        for (int i = 0; i < this.mNumRows; ++i, s = string) {
            string = s;
            if (this.mRows[i].variable.mType == SolverVariable.Type.UNRESTRICTED) {
                final StringBuilder sb = new StringBuilder();
                sb.append(s);
                sb.append(this.mRows[i].toReadableString());
                final String string2 = sb.toString();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(string2);
                sb2.append("\n");
                string = sb2.toString();
            }
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(s);
        sb3.append(this.mGoal);
        sb3.append("\n");
        System.out.println(sb3.toString());
    }
    
    public void fillMetrics(final Metrics sMetrics) {
        LinearSystem.sMetrics = sMetrics;
    }
    
    public Cache getCache() {
        return this.mCache;
    }
    
    Row getGoal() {
        return this.mGoal;
    }
    
    public int getMemoryUsed() {
        int i = 0;
        int n = 0;
        while (i < this.mNumRows) {
            final ArrayRow arrayRow = this.mRows[i];
            int n2 = n;
            if (arrayRow != null) {
                n2 = n + arrayRow.sizeInBytes();
            }
            ++i;
            n = n2;
        }
        return n;
    }
    
    public int getNumEquations() {
        return this.mNumRows;
    }
    
    public int getNumVariables() {
        return this.mVariablesID;
    }
    
    public int getObjectVariableValue(final Object o) {
        final SolverVariable solverVariable = ((ConstraintAnchor)o).getSolverVariable();
        if (solverVariable != null) {
            return (int)(solverVariable.computedValue + 0.5f);
        }
        return 0;
    }
    
    ArrayRow getRow(final int n) {
        return this.mRows[n];
    }
    
    float getValueFor(final String s) {
        final SolverVariable variable = this.getVariable(s, SolverVariable.Type.UNRESTRICTED);
        if (variable == null) {
            return 0.0f;
        }
        return variable.computedValue;
    }
    
    SolverVariable getVariable(final String key, final SolverVariable.Type type) {
        if (this.mVariables == null) {
            this.mVariables = new HashMap<String, SolverVariable>();
        }
        SolverVariable variable;
        if ((variable = this.mVariables.get(key)) == null) {
            variable = this.createVariable(key, type);
        }
        return variable;
    }
    
    public void minimize() throws Exception {
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.minimize;
        }
        if (this.mGoal.isEmpty()) {
            this.computeValues();
            return;
        }
        if (this.graphOptimizer || this.newgraphOptimizer) {
            final Metrics sMetrics2 = LinearSystem.sMetrics;
            if (sMetrics2 != null) {
                ++sMetrics2.graphOptimizer;
            }
            final int n = 0;
            int i = 0;
            while (true) {
                while (i < this.mNumRows) {
                    if (!this.mRows[i].isSimpleDefinition) {
                        final int n2 = n;
                        if (n2 == 0) {
                            this.minimizeGoal(this.mGoal);
                            return;
                        }
                        final Metrics sMetrics3 = LinearSystem.sMetrics;
                        if (sMetrics3 != null) {
                            ++sMetrics3.fullySolved;
                        }
                        this.computeValues();
                        return;
                    }
                    else {
                        ++i;
                    }
                }
                final int n2 = 1;
                continue;
            }
        }
        this.minimizeGoal(this.mGoal);
    }
    
    void minimizeGoal(final Row row) throws Exception {
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.minimizeGoal;
            sMetrics.maxVariables = Math.max(sMetrics.maxVariables, this.mNumColumns);
            final Metrics sMetrics2 = LinearSystem.sMetrics;
            sMetrics2.maxRows = Math.max(sMetrics2.maxRows, this.mNumRows);
        }
        this.enforceBFS(row);
        this.optimize(row, false);
        this.computeValues();
    }
    
    public void removeRow(final ArrayRow arrayRow) {
        if (arrayRow.isSimpleDefinition) {
            final SolverVariable variable = arrayRow.variable;
            if (variable != null) {
                int definitionId = variable.definitionId;
                if (definitionId != -1) {
                    int mNumRows;
                    while (true) {
                        mNumRows = this.mNumRows;
                        if (definitionId >= mNumRows - 1) {
                            break;
                        }
                        final ArrayRow[] mRows = this.mRows;
                        final int n = definitionId + 1;
                        final ArrayRow arrayRow2 = mRows[n];
                        final SolverVariable variable2 = arrayRow2.variable;
                        if (variable2.definitionId == n) {
                            variable2.definitionId = definitionId;
                        }
                        mRows[definitionId] = arrayRow2;
                        definitionId = n;
                    }
                    this.mNumRows = mNumRows - 1;
                }
                final SolverVariable variable3 = arrayRow.variable;
                if (!variable3.isFinalValue) {
                    variable3.setFinalValue(this, arrayRow.constantValue);
                }
                if (LinearSystem.OPTIMIZED_ENGINE) {
                    this.mCache.optimizedArrayRowPool.release(arrayRow);
                }
                else {
                    this.mCache.arrayRowPool.release(arrayRow);
                }
            }
        }
    }
    
    public void reset() {
        int n = 0;
        Cache mCache;
        while (true) {
            mCache = this.mCache;
            final SolverVariable[] mIndexedVariables = mCache.mIndexedVariables;
            if (n >= mIndexedVariables.length) {
                break;
            }
            final SolverVariable solverVariable = mIndexedVariables[n];
            if (solverVariable != null) {
                solverVariable.reset();
            }
            ++n;
        }
        mCache.solverVariablePool.releaseAll(this.mPoolVariables, this.mPoolVariablesCount);
        this.mPoolVariablesCount = 0;
        Arrays.fill(this.mCache.mIndexedVariables, null);
        final HashMap<String, SolverVariable> mVariables = this.mVariables;
        if (mVariables != null) {
            mVariables.clear();
        }
        this.mVariablesID = 0;
        this.mGoal.clear();
        this.mNumColumns = 1;
        for (int i = 0; i < this.mNumRows; ++i) {
            final ArrayRow arrayRow = this.mRows[i];
            if (arrayRow != null) {
                arrayRow.used = false;
            }
        }
        this.releaseRows();
        this.mNumRows = 0;
        if (LinearSystem.OPTIMIZED_ENGINE) {
            this.mTempGoal = (Row)new ValuesRow(this.mCache);
        }
        else {
            this.mTempGoal = (Row)new ArrayRow(this.mCache);
        }
    }
    
    interface Row
    {
        void addError(final SolverVariable p0);
        
        void clear();
        
        SolverVariable getKey();
        
        SolverVariable getPivotCandidate(final LinearSystem p0, final boolean[] p1);
        
        void initFromRow(final Row p0);
        
        boolean isEmpty();
        
        void updateFromFinalVariable(final LinearSystem p0, final SolverVariable p1, final boolean p2);
        
        void updateFromRow(final LinearSystem p0, final ArrayRow p1, final boolean p2);
        
        void updateFromSystem(final LinearSystem p0);
    }
    
    class ValuesRow extends ArrayRow
    {
        final LinearSystem this$0;
        
        public ValuesRow(final LinearSystem this$0, final Cache cache) {
            this.this$0 = this$0;
            super.variables = new SolverVariableValues(this, cache);
        }
    }
}

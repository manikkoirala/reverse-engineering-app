// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets;

import java.util.HashMap;
import androidx.constraintlayout.core.widgets.analyzer.Grouping;
import androidx.constraintlayout.core.widgets.analyzer.WidgetGroup;
import java.util.ArrayList;
import java.util.Arrays;

public class HelperWidget extends ConstraintWidget implements Helper
{
    public ConstraintWidget[] mWidgets;
    public int mWidgetsCount;
    
    public HelperWidget() {
        this.mWidgets = new ConstraintWidget[4];
        this.mWidgetsCount = 0;
    }
    
    @Override
    public void add(final ConstraintWidget constraintWidget) {
        if (constraintWidget != this) {
            if (constraintWidget != null) {
                final int mWidgetsCount = this.mWidgetsCount;
                final ConstraintWidget[] mWidgets = this.mWidgets;
                if (mWidgetsCount + 1 > mWidgets.length) {
                    this.mWidgets = Arrays.copyOf(mWidgets, mWidgets.length * 2);
                }
                final ConstraintWidget[] mWidgets2 = this.mWidgets;
                final int mWidgetsCount2 = this.mWidgetsCount;
                mWidgets2[mWidgetsCount2] = constraintWidget;
                this.mWidgetsCount = mWidgetsCount2 + 1;
            }
        }
    }
    
    public void addDependents(final ArrayList<WidgetGroup> list, final int n, final WidgetGroup widgetGroup) {
        final int n2 = 0;
        int n3 = 0;
        int i;
        while (true) {
            i = n2;
            if (n3 >= this.mWidgetsCount) {
                break;
            }
            widgetGroup.add(this.mWidgets[n3]);
            ++n3;
        }
        while (i < this.mWidgetsCount) {
            Grouping.findDependents(this.mWidgets[i], n, list, widgetGroup);
            ++i;
        }
    }
    
    @Override
    public void copy(final ConstraintWidget constraintWidget, final HashMap<ConstraintWidget, ConstraintWidget> hashMap) {
        super.copy(constraintWidget, hashMap);
        final HelperWidget helperWidget = (HelperWidget)constraintWidget;
        int i = 0;
        this.mWidgetsCount = 0;
        while (i < helperWidget.mWidgetsCount) {
            this.add(hashMap.get(helperWidget.mWidgets[i]));
            ++i;
        }
    }
    
    public int findGroupInDependents(final int n) {
        for (int i = 0; i < this.mWidgetsCount; ++i) {
            final ConstraintWidget constraintWidget = this.mWidgets[i];
            if (n == 0) {
                final int horizontalGroup = constraintWidget.horizontalGroup;
                if (horizontalGroup != -1) {
                    return horizontalGroup;
                }
            }
            if (n == 1) {
                final int verticalGroup = constraintWidget.verticalGroup;
                if (verticalGroup != -1) {
                    return verticalGroup;
                }
            }
        }
        return -1;
    }
    
    @Override
    public void removeAllIds() {
        this.mWidgetsCount = 0;
        Arrays.fill(this.mWidgets, null);
    }
    
    @Override
    public void updateConstraints(final ConstraintWidgetContainer constraintWidgetContainer) {
    }
}

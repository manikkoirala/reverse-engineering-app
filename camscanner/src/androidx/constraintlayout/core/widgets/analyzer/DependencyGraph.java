// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import androidx.constraintlayout.core.widgets.Barrier;
import java.util.Collection;
import androidx.constraintlayout.core.widgets.HelperWidget;
import java.util.HashSet;
import androidx.constraintlayout.core.widgets.Guideline;
import java.io.PrintStream;
import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import java.util.Iterator;
import java.util.ArrayList;
import androidx.constraintlayout.core.widgets.ConstraintWidgetContainer;

public class DependencyGraph
{
    private static final boolean USE_GROUPS = true;
    private ConstraintWidgetContainer container;
    private ConstraintWidgetContainer mContainer;
    ArrayList<RunGroup> mGroups;
    private BasicMeasure.Measure mMeasure;
    private BasicMeasure.Measurer mMeasurer;
    private boolean mNeedBuildGraph;
    private boolean mNeedRedoMeasures;
    private ArrayList<WidgetRun> mRuns;
    private ArrayList<RunGroup> runGroups;
    
    public DependencyGraph(final ConstraintWidgetContainer constraintWidgetContainer) {
        this.mNeedBuildGraph = true;
        this.mNeedRedoMeasures = true;
        this.mRuns = new ArrayList<WidgetRun>();
        this.runGroups = new ArrayList<RunGroup>();
        this.mMeasurer = null;
        this.mMeasure = new BasicMeasure.Measure();
        this.mGroups = new ArrayList<RunGroup>();
        this.container = constraintWidgetContainer;
        this.mContainer = constraintWidgetContainer;
    }
    
    private void applyGroup(final DependencyNode dependencyNode, final int n, final int n2, final DependencyNode dependencyNode2, final ArrayList<RunGroup> list, RunGroup runGroup) {
        final WidgetRun run = dependencyNode.run;
        if (run.runGroup != null) {
            return;
        }
        final ConstraintWidgetContainer container = this.container;
        if (run == container.horizontalRun) {
            return;
        }
        if (run == container.verticalRun) {
            return;
        }
        RunGroup runGroup2;
        if ((runGroup2 = runGroup) == null) {
            runGroup2 = new RunGroup(run, n2);
            list.add(runGroup2);
        }
        (run.runGroup = runGroup2).add(run);
        for (final Dependency dependency : run.start.dependencies) {
            if (dependency instanceof DependencyNode) {
                this.applyGroup((DependencyNode)dependency, n, 0, dependencyNode2, list, runGroup2);
            }
        }
        for (final Dependency dependency2 : run.end.dependencies) {
            if (dependency2 instanceof DependencyNode) {
                this.applyGroup((DependencyNode)dependency2, n, 1, dependencyNode2, list, runGroup2);
            }
        }
        if (n == 1 && run instanceof VerticalWidgetRun) {
            for (final Dependency dependency3 : ((VerticalWidgetRun)run).baseline.dependencies) {
                if (dependency3 instanceof DependencyNode) {
                    this.applyGroup((DependencyNode)dependency3, n, 2, dependencyNode2, list, runGroup2);
                }
            }
        }
        for (final DependencyNode dependencyNode3 : run.start.targets) {
            if (dependencyNode3 == dependencyNode2) {
                runGroup2.dual = true;
            }
            this.applyGroup(dependencyNode3, n, 0, dependencyNode2, list, runGroup2);
        }
        runGroup = (RunGroup)run.end.targets.iterator();
        while (((Iterator)runGroup).hasNext()) {
            final DependencyNode dependencyNode4 = ((Iterator<DependencyNode>)runGroup).next();
            if (dependencyNode4 == dependencyNode2) {
                runGroup2.dual = true;
            }
            this.applyGroup(dependencyNode4, n, 1, dependencyNode2, list, runGroup2);
        }
        if (n != 1 || !(run instanceof VerticalWidgetRun)) {
            return;
        }
        runGroup = (RunGroup)((VerticalWidgetRun)run).baseline.targets.iterator();
        while (true) {
            if (!((Iterator)runGroup).hasNext()) {
                return;
            }
            final DependencyNode dependencyNode5 = ((Iterator<DependencyNode>)runGroup).next();
            try {
                this.applyGroup(dependencyNode5, n, 2, dependencyNode2, list, runGroup2);
                continue;
            }
            finally {}
            break;
        }
    }
    
    private boolean basicMeasureWidgets(final ConstraintWidgetContainer constraintWidgetContainer) {
        for (final ConstraintWidget constraintWidget : constraintWidgetContainer.mChildren) {
            final ConstraintWidget.DimensionBehaviour[] mListDimensionBehaviors = constraintWidget.mListDimensionBehaviors;
            final ConstraintWidget.DimensionBehaviour dimensionBehaviour = mListDimensionBehaviors[0];
            final ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = mListDimensionBehaviors[1];
            if (constraintWidget.getVisibility() == 8) {
                constraintWidget.measured = true;
            }
            else {
                if (constraintWidget.mMatchConstraintPercentWidth < 1.0f && dimensionBehaviour == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    constraintWidget.mMatchConstraintDefaultWidth = 2;
                }
                if (constraintWidget.mMatchConstraintPercentHeight < 1.0f && dimensionBehaviour2 == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    constraintWidget.mMatchConstraintDefaultHeight = 2;
                }
                if (constraintWidget.getDimensionRatio() > 0.0f) {
                    final ConstraintWidget.DimensionBehaviour match_CONSTRAINT = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                    if (dimensionBehaviour == match_CONSTRAINT && (dimensionBehaviour2 == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT || dimensionBehaviour2 == ConstraintWidget.DimensionBehaviour.FIXED)) {
                        constraintWidget.mMatchConstraintDefaultWidth = 3;
                    }
                    else if (dimensionBehaviour2 == match_CONSTRAINT && (dimensionBehaviour == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT || dimensionBehaviour == ConstraintWidget.DimensionBehaviour.FIXED)) {
                        constraintWidget.mMatchConstraintDefaultHeight = 3;
                    }
                    else if (dimensionBehaviour == match_CONSTRAINT && dimensionBehaviour2 == match_CONSTRAINT) {
                        if (constraintWidget.mMatchConstraintDefaultWidth == 0) {
                            constraintWidget.mMatchConstraintDefaultWidth = 3;
                        }
                        if (constraintWidget.mMatchConstraintDefaultHeight == 0) {
                            constraintWidget.mMatchConstraintDefaultHeight = 3;
                        }
                    }
                }
                final ConstraintWidget.DimensionBehaviour match_CONSTRAINT2 = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                Enum<ConstraintWidget.DimensionBehaviour> dimensionBehavior = null;
                Label_0298: {
                    if ((dimensionBehavior = dimensionBehaviour) == match_CONSTRAINT2) {
                        dimensionBehavior = dimensionBehaviour;
                        if (constraintWidget.mMatchConstraintDefaultWidth == 1) {
                            if (constraintWidget.mLeft.mTarget != null) {
                                dimensionBehavior = dimensionBehaviour;
                                if (constraintWidget.mRight.mTarget != null) {
                                    break Label_0298;
                                }
                            }
                            dimensionBehavior = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                        }
                    }
                }
                Enum<ConstraintWidget.DimensionBehaviour> dimensionBehavior2 = null;
                Label_0353: {
                    if ((dimensionBehavior2 = dimensionBehaviour2) == match_CONSTRAINT2) {
                        dimensionBehavior2 = dimensionBehaviour2;
                        if (constraintWidget.mMatchConstraintDefaultHeight == 1) {
                            if (constraintWidget.mTop.mTarget != null) {
                                dimensionBehavior2 = dimensionBehaviour2;
                                if (constraintWidget.mBottom.mTarget != null) {
                                    break Label_0353;
                                }
                            }
                            dimensionBehavior2 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                        }
                    }
                }
                final HorizontalWidgetRun horizontalRun = constraintWidget.horizontalRun;
                horizontalRun.dimensionBehavior = (ConstraintWidget.DimensionBehaviour)dimensionBehavior;
                final int mMatchConstraintDefaultWidth = constraintWidget.mMatchConstraintDefaultWidth;
                horizontalRun.matchConstraintsType = mMatchConstraintDefaultWidth;
                final VerticalWidgetRun verticalRun = constraintWidget.verticalRun;
                verticalRun.dimensionBehavior = (ConstraintWidget.DimensionBehaviour)dimensionBehavior2;
                final int mMatchConstraintDefaultHeight = constraintWidget.mMatchConstraintDefaultHeight;
                verticalRun.matchConstraintsType = mMatchConstraintDefaultHeight;
                final ConstraintWidget.DimensionBehaviour match_PARENT = ConstraintWidget.DimensionBehaviour.MATCH_PARENT;
                if ((dimensionBehavior == match_PARENT || dimensionBehavior == ConstraintWidget.DimensionBehaviour.FIXED || dimensionBehavior == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) && (dimensionBehavior2 == match_PARENT || dimensionBehavior2 == ConstraintWidget.DimensionBehaviour.FIXED || dimensionBehavior2 == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT)) {
                    int width = constraintWidget.getWidth();
                    if (dimensionBehavior == match_PARENT) {
                        final int width2 = constraintWidgetContainer.getWidth();
                        final int mMargin = constraintWidget.mLeft.mMargin;
                        final int mMargin2 = constraintWidget.mRight.mMargin;
                        dimensionBehavior = ConstraintWidget.DimensionBehaviour.FIXED;
                        width = width2 - mMargin - mMargin2;
                    }
                    int height = constraintWidget.getHeight();
                    if (dimensionBehavior2 == match_PARENT) {
                        final int height2 = constraintWidgetContainer.getHeight();
                        final int mMargin3 = constraintWidget.mTop.mMargin;
                        final int mMargin4 = constraintWidget.mBottom.mMargin;
                        dimensionBehavior2 = ConstraintWidget.DimensionBehaviour.FIXED;
                        height = height2 - mMargin3 - mMargin4;
                    }
                    this.measure(constraintWidget, (ConstraintWidget.DimensionBehaviour)dimensionBehavior, width, (ConstraintWidget.DimensionBehaviour)dimensionBehavior2, height);
                    constraintWidget.horizontalRun.dimension.resolve(constraintWidget.getWidth());
                    constraintWidget.verticalRun.dimension.resolve(constraintWidget.getHeight());
                    constraintWidget.measured = true;
                }
                else {
                    if (dimensionBehavior == match_CONSTRAINT2) {
                        final ConstraintWidget.DimensionBehaviour wrap_CONTENT = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                        if (dimensionBehavior2 == wrap_CONTENT || dimensionBehavior2 == ConstraintWidget.DimensionBehaviour.FIXED) {
                            if (mMatchConstraintDefaultWidth == 3) {
                                if (dimensionBehavior2 == wrap_CONTENT) {
                                    this.measure(constraintWidget, wrap_CONTENT, 0, wrap_CONTENT, 0);
                                }
                                final int height3 = constraintWidget.getHeight();
                                final int n = (int)(height3 * constraintWidget.mDimensionRatio + 0.5f);
                                final ConstraintWidget.DimensionBehaviour fixed = ConstraintWidget.DimensionBehaviour.FIXED;
                                this.measure(constraintWidget, fixed, n, fixed, height3);
                                constraintWidget.horizontalRun.dimension.resolve(constraintWidget.getWidth());
                                constraintWidget.verticalRun.dimension.resolve(constraintWidget.getHeight());
                                constraintWidget.measured = true;
                                continue;
                            }
                            if (mMatchConstraintDefaultWidth == 1) {
                                this.measure(constraintWidget, wrap_CONTENT, 0, (ConstraintWidget.DimensionBehaviour)dimensionBehavior2, 0);
                                constraintWidget.horizontalRun.dimension.wrapValue = constraintWidget.getWidth();
                                continue;
                            }
                            if (mMatchConstraintDefaultWidth == 2) {
                                final ConstraintWidget.DimensionBehaviour dimensionBehaviour3 = constraintWidgetContainer.mListDimensionBehaviors[0];
                                final ConstraintWidget.DimensionBehaviour fixed2 = ConstraintWidget.DimensionBehaviour.FIXED;
                                if (dimensionBehaviour3 == fixed2 || dimensionBehaviour3 == match_PARENT) {
                                    this.measure(constraintWidget, fixed2, (int)(constraintWidget.mMatchConstraintPercentWidth * constraintWidgetContainer.getWidth() + 0.5f), (ConstraintWidget.DimensionBehaviour)dimensionBehavior2, constraintWidget.getHeight());
                                    constraintWidget.horizontalRun.dimension.resolve(constraintWidget.getWidth());
                                    constraintWidget.verticalRun.dimension.resolve(constraintWidget.getHeight());
                                    constraintWidget.measured = true;
                                    continue;
                                }
                            }
                            else {
                                final ConstraintAnchor[] mListAnchors = constraintWidget.mListAnchors;
                                if (mListAnchors[0].mTarget == null || mListAnchors[1].mTarget == null) {
                                    this.measure(constraintWidget, wrap_CONTENT, 0, (ConstraintWidget.DimensionBehaviour)dimensionBehavior2, 0);
                                    constraintWidget.horizontalRun.dimension.resolve(constraintWidget.getWidth());
                                    constraintWidget.verticalRun.dimension.resolve(constraintWidget.getHeight());
                                    constraintWidget.measured = true;
                                    continue;
                                }
                            }
                        }
                    }
                    if (dimensionBehavior2 == match_CONSTRAINT2) {
                        final ConstraintWidget.DimensionBehaviour wrap_CONTENT2 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                        if (dimensionBehavior == wrap_CONTENT2 || dimensionBehavior == ConstraintWidget.DimensionBehaviour.FIXED) {
                            if (mMatchConstraintDefaultHeight == 3) {
                                if (dimensionBehavior == wrap_CONTENT2) {
                                    this.measure(constraintWidget, wrap_CONTENT2, 0, wrap_CONTENT2, 0);
                                }
                                final int width3 = constraintWidget.getWidth();
                                float mDimensionRatio = constraintWidget.mDimensionRatio;
                                if (constraintWidget.getDimensionRatioSide() == -1) {
                                    mDimensionRatio = 1.0f / mDimensionRatio;
                                }
                                final int n2 = (int)(width3 * mDimensionRatio + 0.5f);
                                final ConstraintWidget.DimensionBehaviour fixed3 = ConstraintWidget.DimensionBehaviour.FIXED;
                                this.measure(constraintWidget, fixed3, width3, fixed3, n2);
                                constraintWidget.horizontalRun.dimension.resolve(constraintWidget.getWidth());
                                constraintWidget.verticalRun.dimension.resolve(constraintWidget.getHeight());
                                constraintWidget.measured = true;
                                continue;
                            }
                            if (mMatchConstraintDefaultHeight == 1) {
                                this.measure(constraintWidget, (ConstraintWidget.DimensionBehaviour)dimensionBehavior, 0, wrap_CONTENT2, 0);
                                constraintWidget.verticalRun.dimension.wrapValue = constraintWidget.getHeight();
                                continue;
                            }
                            if (mMatchConstraintDefaultHeight == 2) {
                                final ConstraintWidget.DimensionBehaviour dimensionBehaviour4 = constraintWidgetContainer.mListDimensionBehaviors[1];
                                final ConstraintWidget.DimensionBehaviour fixed4 = ConstraintWidget.DimensionBehaviour.FIXED;
                                if (dimensionBehaviour4 == fixed4 || dimensionBehaviour4 == match_PARENT) {
                                    this.measure(constraintWidget, (ConstraintWidget.DimensionBehaviour)dimensionBehavior, constraintWidget.getWidth(), fixed4, (int)(constraintWidget.mMatchConstraintPercentHeight * constraintWidgetContainer.getHeight() + 0.5f));
                                    constraintWidget.horizontalRun.dimension.resolve(constraintWidget.getWidth());
                                    constraintWidget.verticalRun.dimension.resolve(constraintWidget.getHeight());
                                    constraintWidget.measured = true;
                                    continue;
                                }
                            }
                            else {
                                final ConstraintAnchor[] mListAnchors2 = constraintWidget.mListAnchors;
                                if (mListAnchors2[2].mTarget == null || mListAnchors2[3].mTarget == null) {
                                    this.measure(constraintWidget, wrap_CONTENT2, 0, (ConstraintWidget.DimensionBehaviour)dimensionBehavior2, 0);
                                    constraintWidget.horizontalRun.dimension.resolve(constraintWidget.getWidth());
                                    constraintWidget.verticalRun.dimension.resolve(constraintWidget.getHeight());
                                    constraintWidget.measured = true;
                                    continue;
                                }
                            }
                        }
                    }
                    if (dimensionBehavior != match_CONSTRAINT2 || dimensionBehavior2 != match_CONSTRAINT2) {
                        continue;
                    }
                    if (mMatchConstraintDefaultWidth != 1 && mMatchConstraintDefaultHeight != 1) {
                        if (mMatchConstraintDefaultHeight != 2 || mMatchConstraintDefaultWidth != 2) {
                            continue;
                        }
                        final ConstraintWidget.DimensionBehaviour[] mListDimensionBehaviors2 = constraintWidgetContainer.mListDimensionBehaviors;
                        final ConstraintWidget.DimensionBehaviour dimensionBehaviour5 = mListDimensionBehaviors2[0];
                        final ConstraintWidget.DimensionBehaviour fixed5 = ConstraintWidget.DimensionBehaviour.FIXED;
                        if (dimensionBehaviour5 != fixed5 || mListDimensionBehaviors2[1] != fixed5) {
                            continue;
                        }
                        this.measure(constraintWidget, fixed5, (int)(constraintWidget.mMatchConstraintPercentWidth * constraintWidgetContainer.getWidth() + 0.5f), fixed5, (int)(constraintWidget.mMatchConstraintPercentHeight * constraintWidgetContainer.getHeight() + 0.5f));
                        constraintWidget.horizontalRun.dimension.resolve(constraintWidget.getWidth());
                        constraintWidget.verticalRun.dimension.resolve(constraintWidget.getHeight());
                        constraintWidget.measured = true;
                    }
                    else {
                        final ConstraintWidget.DimensionBehaviour wrap_CONTENT3 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                        this.measure(constraintWidget, wrap_CONTENT3, 0, wrap_CONTENT3, 0);
                        constraintWidget.horizontalRun.dimension.wrapValue = constraintWidget.getWidth();
                        constraintWidget.verticalRun.dimension.wrapValue = constraintWidget.getHeight();
                    }
                }
            }
        }
        return false;
    }
    
    private int computeWrap(final ConstraintWidgetContainer constraintWidgetContainer, final int n) {
        final int size = this.mGroups.size();
        long max = 0L;
        for (int i = 0; i < size; ++i) {
            max = Math.max(max, this.mGroups.get(i).computeWrapSize(constraintWidgetContainer, n));
        }
        return (int)max;
    }
    
    private void displayGraph() {
        final Iterator<WidgetRun> iterator = this.mRuns.iterator();
        String generateDisplayGraph = "digraph {\n";
        while (iterator.hasNext()) {
            generateDisplayGraph = this.generateDisplayGraph(iterator.next(), generateDisplayGraph);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(generateDisplayGraph);
        sb.append("\n}\n");
        final String string = sb.toString();
        final PrintStream out = System.out;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("content:<<\n");
        sb2.append(string);
        sb2.append("\n>>");
        out.println(sb2.toString());
    }
    
    private void findGroup(final WidgetRun widgetRun, final int n, final ArrayList<RunGroup> list) {
        for (final Dependency dependency : widgetRun.start.dependencies) {
            if (dependency instanceof DependencyNode) {
                this.applyGroup((DependencyNode)dependency, n, 0, widgetRun.end, list, null);
            }
            else {
                if (!(dependency instanceof WidgetRun)) {
                    continue;
                }
                this.applyGroup(((WidgetRun)dependency).start, n, 0, widgetRun.end, list, null);
            }
        }
        for (final Dependency dependency2 : widgetRun.end.dependencies) {
            if (dependency2 instanceof DependencyNode) {
                this.applyGroup((DependencyNode)dependency2, n, 1, widgetRun.start, list, null);
            }
            else {
                if (!(dependency2 instanceof WidgetRun)) {
                    continue;
                }
                this.applyGroup(((WidgetRun)dependency2).end, n, 1, widgetRun.start, list, null);
            }
        }
        if (n == 1) {
            for (final Dependency dependency3 : ((VerticalWidgetRun)widgetRun).baseline.dependencies) {
                if (dependency3 instanceof DependencyNode) {
                    this.applyGroup((DependencyNode)dependency3, n, 2, null, list, null);
                }
            }
        }
    }
    
    private String generateChainDisplayGraph(final ChainRun chainRun, final String str) {
        final int orientation = chainRun.orientation;
        final StringBuilder obj = new StringBuilder("subgraph ");
        obj.append("cluster_");
        obj.append(chainRun.widget.getDebugName());
        if (orientation == 0) {
            obj.append("_h");
        }
        else {
            obj.append("_v");
        }
        obj.append(" {\n");
        final Iterator<WidgetRun> iterator = chainRun.widgets.iterator();
        String generateDisplayGraph = "";
        while (iterator.hasNext()) {
            final WidgetRun widgetRun = iterator.next();
            obj.append(widgetRun.widget.getDebugName());
            if (orientation == 0) {
                obj.append("_HORIZONTAL");
            }
            else {
                obj.append("_VERTICAL");
            }
            obj.append(";\n");
            generateDisplayGraph = this.generateDisplayGraph(widgetRun, generateDisplayGraph);
        }
        obj.append("}\n");
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(generateDisplayGraph);
        sb.append((Object)obj);
        return sb.toString();
    }
    
    private String generateDisplayGraph(final WidgetRun widgetRun, String generateDisplayNode) {
        final DependencyNode start = widgetRun.start;
        final DependencyNode end = widgetRun.end;
        final StringBuilder sb = new StringBuilder(generateDisplayNode);
        if (!(widgetRun instanceof HelperReferences) && start.dependencies.isEmpty() && (end.dependencies.isEmpty() & start.targets.isEmpty()) && end.targets.isEmpty()) {
            return generateDisplayNode;
        }
        sb.append(this.nodeDefinition(widgetRun));
        final boolean centeredConnection = this.isCenteredConnection(start, end);
        final String generateDisplayNode2 = this.generateDisplayNode(end, centeredConnection, this.generateDisplayNode(start, centeredConnection, generateDisplayNode));
        final boolean b = widgetRun instanceof VerticalWidgetRun;
        generateDisplayNode = generateDisplayNode2;
        if (b) {
            generateDisplayNode = this.generateDisplayNode(((VerticalWidgetRun)widgetRun).baseline, centeredConnection, generateDisplayNode2);
        }
        Label_0717: {
            if (!(widgetRun instanceof HorizontalWidgetRun)) {
                final boolean b2 = widgetRun instanceof ChainRun;
                if (!b2 || widgetRun.orientation != 0) {
                    if (!b && (!b2 || widgetRun.orientation != 1)) {
                        break Label_0717;
                    }
                    final ConstraintWidget.DimensionBehaviour verticalDimensionBehaviour = widgetRun.widget.getVerticalDimensionBehaviour();
                    if (verticalDimensionBehaviour != ConstraintWidget.DimensionBehaviour.FIXED && verticalDimensionBehaviour != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                        if (verticalDimensionBehaviour == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && widgetRun.widget.getDimensionRatio() > 0.0f) {
                            sb.append("\n");
                            sb.append(widgetRun.widget.getDebugName());
                            sb.append("_VERTICAL -> ");
                            sb.append(widgetRun.widget.getDebugName());
                            sb.append("_HORIZONTAL;\n");
                        }
                        break Label_0717;
                    }
                    else {
                        if (!start.targets.isEmpty() && end.targets.isEmpty()) {
                            sb.append("\n");
                            sb.append(end.name());
                            sb.append(" -> ");
                            sb.append(start.name());
                            sb.append("\n");
                            break Label_0717;
                        }
                        if (start.targets.isEmpty() && !end.targets.isEmpty()) {
                            sb.append("\n");
                            sb.append(start.name());
                            sb.append(" -> ");
                            sb.append(end.name());
                            sb.append("\n");
                        }
                        break Label_0717;
                    }
                }
            }
            final ConstraintWidget.DimensionBehaviour horizontalDimensionBehaviour = widgetRun.widget.getHorizontalDimensionBehaviour();
            if (horizontalDimensionBehaviour != ConstraintWidget.DimensionBehaviour.FIXED && horizontalDimensionBehaviour != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                if (horizontalDimensionBehaviour == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && widgetRun.widget.getDimensionRatio() > 0.0f) {
                    sb.append("\n");
                    sb.append(widgetRun.widget.getDebugName());
                    sb.append("_HORIZONTAL -> ");
                    sb.append(widgetRun.widget.getDebugName());
                    sb.append("_VERTICAL;\n");
                }
            }
            else if (!start.targets.isEmpty() && end.targets.isEmpty()) {
                sb.append("\n");
                sb.append(end.name());
                sb.append(" -> ");
                sb.append(start.name());
                sb.append("\n");
            }
            else if (start.targets.isEmpty() && !end.targets.isEmpty()) {
                sb.append("\n");
                sb.append(start.name());
                sb.append(" -> ");
                sb.append(end.name());
                sb.append("\n");
            }
        }
        if (widgetRun instanceof ChainRun) {
            return this.generateChainDisplayGraph((ChainRun)widgetRun, generateDisplayNode);
        }
        return sb.toString();
    }
    
    private String generateDisplayNode(final DependencyNode dependencyNode, final boolean b, String s) {
        final StringBuilder sb = new StringBuilder(s);
        for (final DependencyNode dependencyNode2 : dependencyNode.targets) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("\n");
            sb2.append(dependencyNode.name());
            final String string = sb2.toString();
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(string);
            sb3.append(" -> ");
            sb3.append(dependencyNode2.name());
            final String string2 = sb3.toString();
            Label_0375: {
                if (dependencyNode.margin <= 0 && !b) {
                    s = string2;
                    if (!(dependencyNode.run instanceof HelperReferences)) {
                        break Label_0375;
                    }
                }
                final StringBuilder sb4 = new StringBuilder();
                sb4.append(string2);
                sb4.append("[");
                final String str = s = sb4.toString();
                if (dependencyNode.margin > 0) {
                    final StringBuilder sb5 = new StringBuilder();
                    sb5.append(str);
                    sb5.append("label=\"");
                    sb5.append(dependencyNode.margin);
                    sb5.append("\"");
                    final String str2 = s = sb5.toString();
                    if (b) {
                        final StringBuilder sb6 = new StringBuilder();
                        sb6.append(str2);
                        sb6.append(",");
                        s = sb6.toString();
                    }
                }
                String string3 = s;
                if (b) {
                    final StringBuilder sb7 = new StringBuilder();
                    sb7.append(s);
                    sb7.append(" style=dashed ");
                    string3 = sb7.toString();
                }
                s = string3;
                if (dependencyNode.run instanceof HelperReferences) {
                    final StringBuilder sb8 = new StringBuilder();
                    sb8.append(string3);
                    sb8.append(" style=bold,color=gray ");
                    s = sb8.toString();
                }
                final StringBuilder sb9 = new StringBuilder();
                sb9.append(s);
                sb9.append("]");
                s = sb9.toString();
            }
            final StringBuilder sb10 = new StringBuilder();
            sb10.append(s);
            sb10.append("\n");
            sb.append(sb10.toString());
        }
        return sb.toString();
    }
    
    private boolean isCenteredConnection(final DependencyNode dependencyNode, final DependencyNode dependencyNode2) {
        final Iterator<DependencyNode> iterator = dependencyNode.targets.iterator();
        final boolean b = false;
        int n = 0;
        while (iterator.hasNext()) {
            if (iterator.next() != dependencyNode2) {
                ++n;
            }
        }
        final Iterator<DependencyNode> iterator2 = dependencyNode2.targets.iterator();
        int n2 = 0;
        while (iterator2.hasNext()) {
            if (iterator2.next() != dependencyNode) {
                ++n2;
            }
        }
        boolean b2 = b;
        if (n > 0) {
            b2 = b;
            if (n2 > 0) {
                b2 = true;
            }
        }
        return b2;
    }
    
    private void measure(final ConstraintWidget constraintWidget, final ConstraintWidget.DimensionBehaviour horizontalBehavior, final int horizontalDimension, final ConstraintWidget.DimensionBehaviour verticalBehavior, final int verticalDimension) {
        final BasicMeasure.Measure mMeasure = this.mMeasure;
        mMeasure.horizontalBehavior = horizontalBehavior;
        mMeasure.verticalBehavior = verticalBehavior;
        mMeasure.horizontalDimension = horizontalDimension;
        mMeasure.verticalDimension = verticalDimension;
        this.mMeasurer.measure(constraintWidget, mMeasure);
        constraintWidget.setWidth(this.mMeasure.measuredWidth);
        constraintWidget.setHeight(this.mMeasure.measuredHeight);
        constraintWidget.setHasBaseline(this.mMeasure.measuredHasBaseline);
        constraintWidget.setBaselineDistance(this.mMeasure.measuredBaseline);
    }
    
    private String nodeDefinition(final WidgetRun widgetRun) {
        final boolean b = widgetRun instanceof VerticalWidgetRun;
        final String debugName = widgetRun.widget.getDebugName();
        final StringBuilder sb = new StringBuilder(debugName);
        final ConstraintWidget widget = widgetRun.widget;
        ConstraintWidget.DimensionBehaviour dimensionBehaviour;
        if (!b) {
            dimensionBehaviour = widget.getHorizontalDimensionBehaviour();
        }
        else {
            dimensionBehaviour = widget.getVerticalDimensionBehaviour();
        }
        final RunGroup runGroup = widgetRun.runGroup;
        if (!b) {
            sb.append("_HORIZONTAL");
        }
        else {
            sb.append("_VERTICAL");
        }
        sb.append(" [shape=none, label=<");
        sb.append("<TABLE BORDER=\"0\" CELLSPACING=\"0\" CELLPADDING=\"2\">");
        sb.append("  <TR>");
        if (!b) {
            sb.append("    <TD ");
            if (widgetRun.start.resolved) {
                sb.append(" BGCOLOR=\"green\"");
            }
            sb.append(" PORT=\"LEFT\" BORDER=\"1\">L</TD>");
        }
        else {
            sb.append("    <TD ");
            if (widgetRun.start.resolved) {
                sb.append(" BGCOLOR=\"green\"");
            }
            sb.append(" PORT=\"TOP\" BORDER=\"1\">T</TD>");
        }
        sb.append("    <TD BORDER=\"1\" ");
        final boolean resolved = widgetRun.dimension.resolved;
        if (resolved && !widgetRun.widget.measured) {
            sb.append(" BGCOLOR=\"green\" ");
        }
        else if (resolved) {
            sb.append(" BGCOLOR=\"lightgray\" ");
        }
        else if (widgetRun.widget.measured) {
            sb.append(" BGCOLOR=\"yellow\" ");
        }
        if (dimensionBehaviour == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            sb.append("style=\"dashed\"");
        }
        sb.append(">");
        sb.append(debugName);
        if (runGroup != null) {
            sb.append(" [");
            sb.append(runGroup.groupIndex + 1);
            sb.append("/");
            sb.append(RunGroup.index);
            sb.append("]");
        }
        sb.append(" </TD>");
        if (!b) {
            sb.append("    <TD ");
            if (widgetRun.end.resolved) {
                sb.append(" BGCOLOR=\"green\"");
            }
            sb.append(" PORT=\"RIGHT\" BORDER=\"1\">R</TD>");
        }
        else {
            sb.append("    <TD ");
            if (((VerticalWidgetRun)widgetRun).baseline.resolved) {
                sb.append(" BGCOLOR=\"green\"");
            }
            sb.append(" PORT=\"BASELINE\" BORDER=\"1\">b</TD>");
            sb.append("    <TD ");
            if (widgetRun.end.resolved) {
                sb.append(" BGCOLOR=\"green\"");
            }
            sb.append(" PORT=\"BOTTOM\" BORDER=\"1\">B</TD>");
        }
        sb.append("  </TR></TABLE>");
        sb.append(">];\n");
        return sb.toString();
    }
    
    public void buildGraph() {
        this.buildGraph(this.mRuns);
        this.mGroups.clear();
        RunGroup.index = 0;
        this.findGroup(this.container.horizontalRun, 0, this.mGroups);
        this.findGroup(this.container.verticalRun, 1, this.mGroups);
        this.mNeedBuildGraph = false;
    }
    
    public void buildGraph(final ArrayList<WidgetRun> list) {
        list.clear();
        this.mContainer.horizontalRun.clear();
        this.mContainer.verticalRun.clear();
        list.add(this.mContainer.horizontalRun);
        list.add(this.mContainer.verticalRun);
        final Iterator<ConstraintWidget> iterator = this.mContainer.mChildren.iterator();
        Collection<? extends HorizontalWidgetRun> c = null;
        while (iterator.hasNext()) {
            final ConstraintWidget constraintWidget = iterator.next();
            if (constraintWidget instanceof Guideline) {
                list.add(new GuidelineReference(constraintWidget));
            }
            else {
                if (constraintWidget.isInHorizontalChain()) {
                    if (constraintWidget.horizontalChainRun == null) {
                        constraintWidget.horizontalChainRun = new ChainRun(constraintWidget, 0);
                    }
                    HashSet<? extends HorizontalWidgetRun> set;
                    if ((set = (HashSet<? extends HorizontalWidgetRun>)c) == null) {
                        set = new HashSet<HorizontalWidgetRun>();
                    }
                    set.add((HorizontalWidgetRun)constraintWidget.horizontalChainRun);
                    c = set;
                }
                else {
                    list.add(constraintWidget.horizontalRun);
                }
                HashSet<? extends HorizontalWidgetRun> set2;
                if (constraintWidget.isInVerticalChain()) {
                    if (constraintWidget.verticalChainRun == null) {
                        constraintWidget.verticalChainRun = new ChainRun(constraintWidget, 1);
                    }
                    if ((set2 = (HashSet<? extends HorizontalWidgetRun>)c) == null) {
                        set2 = new HashSet<HorizontalWidgetRun>();
                    }
                    set2.add((HorizontalWidgetRun)constraintWidget.verticalChainRun);
                }
                else {
                    list.add(constraintWidget.verticalRun);
                    set2 = (HashSet<? extends HorizontalWidgetRun>)c;
                }
                c = set2;
                if (!(constraintWidget instanceof HelperWidget)) {
                    continue;
                }
                list.add(new HelperReferences(constraintWidget));
                c = set2;
            }
        }
        if (c != null) {
            list.addAll(c);
        }
        final Iterator iterator2 = list.iterator();
        while (iterator2.hasNext()) {
            ((WidgetRun)iterator2.next()).clear();
        }
        for (final WidgetRun widgetRun : list) {
            if (widgetRun.widget == this.mContainer) {
                continue;
            }
            widgetRun.apply();
        }
    }
    
    public void defineTerminalWidgets(final ConstraintWidget.DimensionBehaviour dimensionBehaviour, final ConstraintWidget.DimensionBehaviour dimensionBehaviour2) {
        if (this.mNeedBuildGraph) {
            this.buildGraph();
            final Iterator<ConstraintWidget> iterator = this.container.mChildren.iterator();
            boolean b = false;
            while (iterator.hasNext()) {
                final ConstraintWidget constraintWidget = iterator.next();
                final boolean[] isTerminalWidget = constraintWidget.isTerminalWidget;
                isTerminalWidget[1] = (isTerminalWidget[0] = true);
                if (constraintWidget instanceof Barrier) {
                    b = true;
                }
            }
            if (!b) {
                for (final RunGroup runGroup : this.mGroups) {
                    final ConstraintWidget.DimensionBehaviour wrap_CONTENT = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                    runGroup.defineTerminalWidgets(dimensionBehaviour == wrap_CONTENT, dimensionBehaviour2 == wrap_CONTENT);
                }
            }
        }
    }
    
    public boolean directMeasure(final boolean b) {
        final boolean b2 = true;
        final boolean b3 = b & true;
        if (this.mNeedBuildGraph || this.mNeedRedoMeasures) {
            for (final ConstraintWidget constraintWidget : this.container.mChildren) {
                constraintWidget.ensureWidgetRuns();
                constraintWidget.measured = false;
                constraintWidget.horizontalRun.reset();
                constraintWidget.verticalRun.reset();
            }
            this.container.ensureWidgetRuns();
            final ConstraintWidgetContainer container = this.container;
            container.measured = false;
            container.horizontalRun.reset();
            this.container.verticalRun.reset();
            this.mNeedRedoMeasures = false;
        }
        if (this.basicMeasureWidgets(this.mContainer)) {
            return false;
        }
        this.container.setX(0);
        this.container.setY(0);
        final ConstraintWidget.DimensionBehaviour dimensionBehaviour = this.container.getDimensionBehaviour(0);
        final ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = this.container.getDimensionBehaviour(1);
        if (this.mNeedBuildGraph) {
            this.buildGraph();
        }
        final int x = this.container.getX();
        final int y = this.container.getY();
        this.container.horizontalRun.start.resolve(x);
        this.container.verticalRun.start.resolve(y);
        this.measureWidgets();
        final ConstraintWidget.DimensionBehaviour wrap_CONTENT = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        if (dimensionBehaviour == wrap_CONTENT || dimensionBehaviour2 == wrap_CONTENT) {
            boolean b4 = false;
            Label_0303: {
                if (b4 = b3) {
                    final Iterator<WidgetRun> iterator2 = this.mRuns.iterator();
                    do {
                        b4 = b3;
                        if (iterator2.hasNext()) {
                            continue;
                        }
                        break Label_0303;
                    } while (iterator2.next().supportsWrapComputation());
                    b4 = false;
                }
            }
            if (b4 && dimensionBehaviour == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                this.container.setHorizontalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.FIXED);
                final ConstraintWidgetContainer container2 = this.container;
                container2.setWidth(this.computeWrap(container2, 0));
                final ConstraintWidgetContainer container3 = this.container;
                container3.horizontalRun.dimension.resolve(container3.getWidth());
            }
            if (b4 && dimensionBehaviour2 == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                this.container.setVerticalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.FIXED);
                final ConstraintWidgetContainer container4 = this.container;
                container4.setHeight(this.computeWrap(container4, 1));
                final ConstraintWidgetContainer container5 = this.container;
                container5.verticalRun.dimension.resolve(container5.getHeight());
            }
        }
        final ConstraintWidgetContainer container6 = this.container;
        final ConstraintWidget.DimensionBehaviour dimensionBehaviour3 = container6.mListDimensionBehaviors[0];
        final ConstraintWidget.DimensionBehaviour fixed = ConstraintWidget.DimensionBehaviour.FIXED;
        boolean b5;
        if (dimensionBehaviour3 != fixed && dimensionBehaviour3 != ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
            b5 = false;
        }
        else {
            final int n = container6.getWidth() + x;
            this.container.horizontalRun.end.resolve(n);
            this.container.horizontalRun.dimension.resolve(n - x);
            this.measureWidgets();
            final ConstraintWidgetContainer container7 = this.container;
            final ConstraintWidget.DimensionBehaviour dimensionBehaviour4 = container7.mListDimensionBehaviors[1];
            if (dimensionBehaviour4 == fixed || dimensionBehaviour4 == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
                final int n2 = container7.getHeight() + y;
                this.container.verticalRun.end.resolve(n2);
                this.container.verticalRun.dimension.resolve(n2 - y);
            }
            this.measureWidgets();
            b5 = true;
        }
        for (final WidgetRun widgetRun : this.mRuns) {
            if (widgetRun.widget == this.container && !widgetRun.resolved) {
                continue;
            }
            widgetRun.applyToWidget();
        }
        final Iterator<WidgetRun> iterator4 = this.mRuns.iterator();
        boolean b6 = false;
        Label_0773: {
            while (true) {
                b6 = b2;
                if (!iterator4.hasNext()) {
                    break Label_0773;
                }
                final WidgetRun widgetRun2 = iterator4.next();
                if (!b5 && widgetRun2.widget == this.container) {
                    continue;
                }
                if (!widgetRun2.start.resolved) {
                    break;
                }
                if (!widgetRun2.end.resolved && !(widgetRun2 instanceof GuidelineReference)) {
                    break;
                }
                if (!widgetRun2.dimension.resolved && !(widgetRun2 instanceof ChainRun) && !(widgetRun2 instanceof GuidelineReference)) {
                    break;
                }
            }
            b6 = false;
        }
        this.container.setHorizontalDimensionBehaviour(dimensionBehaviour);
        this.container.setVerticalDimensionBehaviour(dimensionBehaviour2);
        return b6;
    }
    
    public boolean directMeasureSetup(final boolean b) {
        if (this.mNeedBuildGraph) {
            for (final ConstraintWidget constraintWidget : this.container.mChildren) {
                constraintWidget.ensureWidgetRuns();
                constraintWidget.measured = false;
                final HorizontalWidgetRun horizontalRun = constraintWidget.horizontalRun;
                horizontalRun.dimension.resolved = false;
                horizontalRun.resolved = false;
                horizontalRun.reset();
                final VerticalWidgetRun verticalRun = constraintWidget.verticalRun;
                verticalRun.dimension.resolved = false;
                verticalRun.resolved = false;
                verticalRun.reset();
            }
            this.container.ensureWidgetRuns();
            final ConstraintWidgetContainer container = this.container;
            container.measured = false;
            final HorizontalWidgetRun horizontalRun2 = container.horizontalRun;
            horizontalRun2.dimension.resolved = false;
            horizontalRun2.resolved = false;
            horizontalRun2.reset();
            final VerticalWidgetRun verticalRun2 = this.container.verticalRun;
            verticalRun2.dimension.resolved = false;
            verticalRun2.resolved = false;
            verticalRun2.reset();
            this.buildGraph();
        }
        if (this.basicMeasureWidgets(this.mContainer)) {
            return false;
        }
        this.container.setX(0);
        this.container.setY(0);
        this.container.horizontalRun.start.resolve(0);
        this.container.verticalRun.start.resolve(0);
        return true;
    }
    
    public boolean directMeasureWithOrientation(final boolean b, final int n) {
        final boolean b2 = true;
        final boolean b3 = b & true;
        final ConstraintWidget.DimensionBehaviour dimensionBehaviour = this.container.getDimensionBehaviour(0);
        final ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = this.container.getDimensionBehaviour(1);
        final int x = this.container.getX();
        final int y = this.container.getY();
        Label_0254: {
            if (b3) {
                final ConstraintWidget.DimensionBehaviour wrap_CONTENT = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                if (dimensionBehaviour == wrap_CONTENT || dimensionBehaviour2 == wrap_CONTENT) {
                    final Iterator<WidgetRun> iterator = this.mRuns.iterator();
                    while (true) {
                        WidgetRun widgetRun;
                        do {
                            final boolean b4 = b3;
                            if (iterator.hasNext()) {
                                widgetRun = iterator.next();
                            }
                            else if (n == 0) {
                                if (b4 && dimensionBehaviour == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                                    this.container.setHorizontalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.FIXED);
                                    final ConstraintWidgetContainer container = this.container;
                                    container.setWidth(this.computeWrap(container, 0));
                                    final ConstraintWidgetContainer container2 = this.container;
                                    container2.horizontalRun.dimension.resolve(container2.getWidth());
                                }
                                break Label_0254;
                            }
                            else {
                                if (b4 && dimensionBehaviour2 == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                                    this.container.setVerticalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.FIXED);
                                    final ConstraintWidgetContainer container3 = this.container;
                                    container3.setHeight(this.computeWrap(container3, 1));
                                    final ConstraintWidgetContainer container4 = this.container;
                                    container4.verticalRun.dimension.resolve(container4.getHeight());
                                }
                                break Label_0254;
                            }
                        } while (widgetRun.orientation != n || widgetRun.supportsWrapComputation());
                        final boolean b4 = false;
                        continue;
                    }
                }
            }
        }
        boolean b5 = false;
        Label_0413: {
            Label_0411: {
                if (n == 0) {
                    final ConstraintWidgetContainer container5 = this.container;
                    final ConstraintWidget.DimensionBehaviour dimensionBehaviour3 = container5.mListDimensionBehaviors[0];
                    if (dimensionBehaviour3 == ConstraintWidget.DimensionBehaviour.FIXED || dimensionBehaviour3 == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
                        final int n2 = container5.getWidth() + x;
                        this.container.horizontalRun.end.resolve(n2);
                        this.container.horizontalRun.dimension.resolve(n2 - x);
                        break Label_0411;
                    }
                }
                else {
                    final ConstraintWidgetContainer container6 = this.container;
                    final ConstraintWidget.DimensionBehaviour dimensionBehaviour4 = container6.mListDimensionBehaviors[1];
                    if (dimensionBehaviour4 == ConstraintWidget.DimensionBehaviour.FIXED || dimensionBehaviour4 == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
                        final int n3 = container6.getHeight() + y;
                        this.container.verticalRun.end.resolve(n3);
                        this.container.verticalRun.dimension.resolve(n3 - y);
                        break Label_0411;
                    }
                }
                b5 = false;
                break Label_0413;
            }
            b5 = true;
        }
        this.measureWidgets();
        for (final WidgetRun widgetRun2 : this.mRuns) {
            if (widgetRun2.orientation != n) {
                continue;
            }
            if (widgetRun2.widget == this.container && !widgetRun2.resolved) {
                continue;
            }
            widgetRun2.applyToWidget();
        }
        final Iterator<WidgetRun> iterator3 = this.mRuns.iterator();
        boolean b6 = false;
        Label_0608: {
            while (true) {
                b6 = b2;
                if (!iterator3.hasNext()) {
                    break Label_0608;
                }
                final WidgetRun widgetRun3 = iterator3.next();
                if (widgetRun3.orientation != n) {
                    continue;
                }
                if (!b5 && widgetRun3.widget == this.container) {
                    continue;
                }
                if (!widgetRun3.start.resolved) {
                    break;
                }
                if (!widgetRun3.end.resolved) {
                    break;
                }
                if (!(widgetRun3 instanceof ChainRun) && !widgetRun3.dimension.resolved) {
                    break;
                }
            }
            b6 = false;
        }
        this.container.setHorizontalDimensionBehaviour(dimensionBehaviour);
        this.container.setVerticalDimensionBehaviour(dimensionBehaviour2);
        return b6;
    }
    
    public void invalidateGraph() {
        this.mNeedBuildGraph = true;
    }
    
    public void invalidateMeasures() {
        this.mNeedRedoMeasures = true;
    }
    
    public void measureWidgets() {
        for (final ConstraintWidget constraintWidget : this.container.mChildren) {
            if (constraintWidget.measured) {
                continue;
            }
            final ConstraintWidget.DimensionBehaviour[] mListDimensionBehaviors = constraintWidget.mListDimensionBehaviors;
            final int n = 0;
            final ConstraintWidget.DimensionBehaviour dimensionBehaviour = mListDimensionBehaviors[0];
            final ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = mListDimensionBehaviors[1];
            final int mMatchConstraintDefaultWidth = constraintWidget.mMatchConstraintDefaultWidth;
            final int mMatchConstraintDefaultHeight = constraintWidget.mMatchConstraintDefaultHeight;
            final ConstraintWidget.DimensionBehaviour wrap_CONTENT = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
            final boolean b = dimensionBehaviour == wrap_CONTENT || (dimensionBehaviour == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && mMatchConstraintDefaultWidth == 1);
            int n2 = 0;
            Label_0141: {
                if (dimensionBehaviour2 != wrap_CONTENT) {
                    n2 = n;
                    if (dimensionBehaviour2 != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                        break Label_0141;
                    }
                    n2 = n;
                    if (mMatchConstraintDefaultHeight != 1) {
                        break Label_0141;
                    }
                }
                n2 = 1;
            }
            final DimensionDependency dimension = constraintWidget.horizontalRun.dimension;
            final boolean resolved = dimension.resolved;
            final DimensionDependency dimension2 = constraintWidget.verticalRun.dimension;
            final boolean resolved2 = dimension2.resolved;
            if (resolved && resolved2) {
                final ConstraintWidget.DimensionBehaviour fixed = ConstraintWidget.DimensionBehaviour.FIXED;
                this.measure(constraintWidget, fixed, dimension.value, fixed, dimension2.value);
                constraintWidget.measured = true;
            }
            else if (resolved && n2 != 0) {
                this.measure(constraintWidget, ConstraintWidget.DimensionBehaviour.FIXED, dimension.value, wrap_CONTENT, dimension2.value);
                if (dimensionBehaviour2 == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    constraintWidget.verticalRun.dimension.wrapValue = constraintWidget.getHeight();
                }
                else {
                    constraintWidget.verticalRun.dimension.resolve(constraintWidget.getHeight());
                    constraintWidget.measured = true;
                }
            }
            else if (resolved2 && b) {
                this.measure(constraintWidget, wrap_CONTENT, dimension.value, ConstraintWidget.DimensionBehaviour.FIXED, dimension2.value);
                if (dimensionBehaviour == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    constraintWidget.horizontalRun.dimension.wrapValue = constraintWidget.getWidth();
                }
                else {
                    constraintWidget.horizontalRun.dimension.resolve(constraintWidget.getWidth());
                    constraintWidget.measured = true;
                }
            }
            if (!constraintWidget.measured) {
                continue;
            }
            final DimensionDependency baselineDimension = constraintWidget.verticalRun.baselineDimension;
            if (baselineDimension == null) {
                continue;
            }
            baselineDimension.resolve(constraintWidget.getBaselineDistance());
        }
    }
    
    public void setMeasurer(final BasicMeasure.Measurer mMeasurer) {
        this.mMeasurer = mMeasurer;
    }
}

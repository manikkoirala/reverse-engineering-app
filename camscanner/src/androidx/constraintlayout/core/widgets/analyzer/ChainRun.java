// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import java.util.Iterator;
import androidx.constraintlayout.core.widgets.ConstraintWidgetContainer;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import java.util.ArrayList;

public class ChainRun extends WidgetRun
{
    private int chainStyle;
    ArrayList<WidgetRun> widgets;
    
    public ChainRun(final ConstraintWidget constraintWidget, final int orientation) {
        super(constraintWidget);
        this.widgets = new ArrayList<WidgetRun>();
        super.orientation = orientation;
        this.build();
    }
    
    private void build() {
        ConstraintWidget widget = super.widget;
        ConstraintWidget previousChainMember2;
        for (ConstraintWidget previousChainMember = widget.getPreviousChainMember(super.orientation); previousChainMember != null; previousChainMember = previousChainMember2) {
            previousChainMember2 = previousChainMember.getPreviousChainMember(super.orientation);
            widget = previousChainMember;
        }
        super.widget = widget;
        this.widgets.add(widget.getRun(super.orientation));
        for (ConstraintWidget constraintWidget = widget.getNextChainMember(super.orientation); constraintWidget != null; constraintWidget = constraintWidget.getNextChainMember(super.orientation)) {
            this.widgets.add(constraintWidget.getRun(super.orientation));
        }
        for (final WidgetRun widgetRun : this.widgets) {
            final int orientation = super.orientation;
            if (orientation == 0) {
                widgetRun.widget.horizontalChainRun = this;
            }
            else {
                if (orientation != 1) {
                    continue;
                }
                widgetRun.widget.verticalChainRun = this;
            }
        }
        if (super.orientation == 0 && ((ConstraintWidgetContainer)super.widget.getParent()).isRtl() && this.widgets.size() > 1) {
            final ArrayList<WidgetRun> widgets = this.widgets;
            super.widget = ((WidgetRun)widgets.get(widgets.size() - 1)).widget;
        }
        int chainStyle;
        if (super.orientation == 0) {
            chainStyle = super.widget.getHorizontalChainStyle();
        }
        else {
            chainStyle = super.widget.getVerticalChainStyle();
        }
        this.chainStyle = chainStyle;
    }
    
    private ConstraintWidget getFirstVisibleWidget() {
        for (int i = 0; i < this.widgets.size(); ++i) {
            final WidgetRun widgetRun = this.widgets.get(i);
            if (widgetRun.widget.getVisibility() != 8) {
                return widgetRun.widget;
            }
        }
        return null;
    }
    
    private ConstraintWidget getLastVisibleWidget() {
        for (int i = this.widgets.size() - 1; i >= 0; --i) {
            final WidgetRun widgetRun = this.widgets.get(i);
            if (widgetRun.widget.getVisibility() != 8) {
                return widgetRun.widget;
            }
        }
        return null;
    }
    
    @Override
    void apply() {
        final Iterator<WidgetRun> iterator = this.widgets.iterator();
        while (iterator.hasNext()) {
            iterator.next().apply();
        }
        final int size = this.widgets.size();
        if (size < 1) {
            return;
        }
        final ConstraintWidget widget = this.widgets.get(0).widget;
        final ConstraintWidget widget2 = this.widgets.get(size - 1).widget;
        if (super.orientation == 0) {
            final ConstraintAnchor mLeft = widget.mLeft;
            final ConstraintAnchor mRight = widget2.mRight;
            final DependencyNode target = this.getTarget(mLeft, 0);
            int n = mLeft.getMargin();
            final ConstraintWidget firstVisibleWidget = this.getFirstVisibleWidget();
            if (firstVisibleWidget != null) {
                n = firstVisibleWidget.mLeft.getMargin();
            }
            if (target != null) {
                this.addTarget(super.start, target, n);
            }
            final DependencyNode target2 = this.getTarget(mRight, 0);
            int n2 = mRight.getMargin();
            final ConstraintWidget lastVisibleWidget = this.getLastVisibleWidget();
            if (lastVisibleWidget != null) {
                n2 = lastVisibleWidget.mRight.getMargin();
            }
            if (target2 != null) {
                this.addTarget(super.end, target2, -n2);
            }
        }
        else {
            final ConstraintAnchor mTop = widget.mTop;
            final ConstraintAnchor mBottom = widget2.mBottom;
            final DependencyNode target3 = this.getTarget(mTop, 1);
            int n3 = mTop.getMargin();
            final ConstraintWidget firstVisibleWidget2 = this.getFirstVisibleWidget();
            if (firstVisibleWidget2 != null) {
                n3 = firstVisibleWidget2.mTop.getMargin();
            }
            if (target3 != null) {
                this.addTarget(super.start, target3, n3);
            }
            final DependencyNode target4 = this.getTarget(mBottom, 1);
            int n4 = mBottom.getMargin();
            final ConstraintWidget lastVisibleWidget2 = this.getLastVisibleWidget();
            if (lastVisibleWidget2 != null) {
                n4 = lastVisibleWidget2.mBottom.getMargin();
            }
            if (target4 != null) {
                this.addTarget(super.end, target4, -n4);
            }
        }
        super.start.updateDelegate = this;
        super.end.updateDelegate = this;
    }
    
    public void applyToWidget() {
        for (int i = 0; i < this.widgets.size(); ++i) {
            this.widgets.get(i).applyToWidget();
        }
    }
    
    @Override
    void clear() {
        super.runGroup = null;
        final Iterator<WidgetRun> iterator = this.widgets.iterator();
        while (iterator.hasNext()) {
            iterator.next().clear();
        }
    }
    
    @Override
    public long getWrapDimension() {
        final int size = this.widgets.size();
        long n = 0L;
        for (int i = 0; i < size; ++i) {
            final WidgetRun widgetRun = this.widgets.get(i);
            n = n + widgetRun.start.margin + widgetRun.getWrapDimension() + widgetRun.end.margin;
        }
        return n;
    }
    
    @Override
    void reset() {
        super.start.resolved = false;
        super.end.resolved = false;
    }
    
    @Override
    boolean supportsWrapComputation() {
        for (int size = this.widgets.size(), i = 0; i < size; ++i) {
            if (!this.widgets.get(i).supportsWrapComputation()) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ChainRun ");
        String str;
        if (super.orientation == 0) {
            str = "horizontal : ";
        }
        else {
            str = "vertical : ";
        }
        sb.append(str);
        for (final WidgetRun obj : this.widgets) {
            sb.append("<");
            sb.append(obj);
            sb.append("> ");
        }
        return sb.toString();
    }
    
    @Override
    public void update(final Dependency dependency) {
        if (super.start.resolved) {
            if (super.end.resolved) {
                final ConstraintWidget parent = super.widget.getParent();
                final boolean b = parent instanceof ConstraintWidgetContainer && ((ConstraintWidgetContainer)parent).isRtl();
                final int n = super.end.value - super.start.value;
                final int size = this.widgets.size();
                int index = 0;
                int n2;
                int n3;
                while (true) {
                    n2 = -1;
                    if (index >= size) {
                        n3 = -1;
                        break;
                    }
                    n3 = index;
                    if (this.widgets.get(index).widget.getVisibility() != 8) {
                        break;
                    }
                    ++index;
                }
                int index2;
                final int n4 = index2 = size - 1;
                int n5;
                while (true) {
                    n5 = n2;
                    if (index2 < 0) {
                        break;
                    }
                    if (this.widgets.get(index2).widget.getVisibility() != 8) {
                        n5 = index2;
                        break;
                    }
                    --index2;
                }
                int i = 0;
                while (true) {
                    while (i < 2) {
                        int j = 0;
                        int n6 = 0;
                        int n7 = 0;
                        int n8 = 0;
                        float n9 = 0.0f;
                        while (j < size) {
                            final WidgetRun widgetRun = this.widgets.get(j);
                            int n10;
                            if (widgetRun.widget.getVisibility() == 8) {
                                n10 = n7;
                            }
                            else {
                                final int n11 = n8 + 1;
                                int n12 = n6;
                                if (j > 0) {
                                    n12 = n6;
                                    if (j >= n3) {
                                        n12 = n6 + widgetRun.start.margin;
                                    }
                                }
                                final DimensionDependency dimension = widgetRun.dimension;
                                int value = dimension.value;
                                int n13;
                                if (widgetRun.dimensionBehavior != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                                    n13 = 1;
                                }
                                else {
                                    n13 = 0;
                                }
                                Label_0423: {
                                    if (n13 != 0) {
                                        final int orientation = super.orientation;
                                        if (orientation == 0 && !widgetRun.widget.horizontalRun.dimension.resolved) {
                                            return;
                                        }
                                        if (orientation == 1 && !widgetRun.widget.verticalRun.dimension.resolved) {
                                            return;
                                        }
                                    }
                                    else {
                                        int wrapValue;
                                        if (widgetRun.matchConstraintsType == 1 && i == 0) {
                                            wrapValue = dimension.wrapValue;
                                            ++n7;
                                        }
                                        else {
                                            if (!dimension.resolved) {
                                                break Label_0423;
                                            }
                                            wrapValue = value;
                                        }
                                        final int n14 = 1;
                                        value = wrapValue;
                                        n13 = n14;
                                    }
                                }
                                int n17;
                                float n18;
                                if (n13 == 0) {
                                    final int n15 = n7 + 1;
                                    final float n16 = widgetRun.widget.mWeight[super.orientation];
                                    n17 = n12;
                                    n7 = n15;
                                    n18 = n9;
                                    if (n16 >= 0.0f) {
                                        n18 = n9 + n16;
                                        n17 = n12;
                                        n7 = n15;
                                    }
                                }
                                else {
                                    n17 = n12 + value;
                                    n18 = n9;
                                }
                                n6 = n17;
                                n10 = n7;
                                n8 = n11;
                                n9 = n18;
                                if (j < n4) {
                                    n6 = n17;
                                    n10 = n7;
                                    n8 = n11;
                                    n9 = n18;
                                    if (j < n5) {
                                        n6 = n17 + -widgetRun.end.margin;
                                        n9 = n18;
                                        n8 = n11;
                                        n10 = n7;
                                    }
                                }
                            }
                            ++j;
                            n7 = n10;
                        }
                        if (n6 >= n && n7 != 0) {
                            ++i;
                        }
                        else {
                            final int n19 = n8;
                            int n20 = n7;
                            int n21 = super.start.value;
                            if (b) {
                                n21 = super.end.value;
                            }
                            int n22 = n21;
                            if (n6 > n) {
                                if (b) {
                                    n22 = n21 + (int)((n6 - n) / 2.0f + 0.5f);
                                }
                                else {
                                    n22 = n21 - (int)((n6 - n) / 2.0f + 0.5f);
                                }
                            }
                            int n36;
                            if (n20 > 0) {
                                final float n23 = (float)(n - n6);
                                final int n24 = (int)(n23 / n20 + 0.5f);
                                int k = 0;
                                final int n25 = 0;
                                final int n26 = n6;
                                int n27 = n25;
                                final int n28 = n22;
                                while (k < size) {
                                    final WidgetRun widgetRun2 = this.widgets.get(k);
                                    if (widgetRun2.widget.getVisibility() != 8) {
                                        if (widgetRun2.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                                            final DimensionDependency dimension2 = widgetRun2.dimension;
                                            if (!dimension2.resolved) {
                                                int a;
                                                if (n9 > 0.0f) {
                                                    a = (int)(widgetRun2.widget.mWeight[super.orientation] * n23 / n9 + 0.5f);
                                                }
                                                else {
                                                    a = n24;
                                                }
                                                int a2;
                                                int a3;
                                                if (super.orientation == 0) {
                                                    final ConstraintWidget widget = widgetRun2.widget;
                                                    a2 = widget.mMatchConstraintMaxWidth;
                                                    a3 = widget.mMatchConstraintMinWidth;
                                                }
                                                else {
                                                    final ConstraintWidget widget2 = widgetRun2.widget;
                                                    a2 = widget2.mMatchConstraintMaxHeight;
                                                    a3 = widget2.mMatchConstraintMinHeight;
                                                }
                                                int min;
                                                if (widgetRun2.matchConstraintsType == 1) {
                                                    min = Math.min(a, dimension2.wrapValue);
                                                }
                                                else {
                                                    min = a;
                                                }
                                                int n29;
                                                final int b2 = n29 = Math.max(a3, min);
                                                if (a2 > 0) {
                                                    n29 = Math.min(a2, b2);
                                                }
                                                int n30 = a;
                                                int n31 = n27;
                                                if (n29 != a) {
                                                    n31 = n27 + 1;
                                                    n30 = n29;
                                                }
                                                widgetRun2.dimension.resolve(n30);
                                                n27 = n31;
                                            }
                                        }
                                    }
                                    ++k;
                                }
                                int n33;
                                if (n27 > 0) {
                                    final int n32 = n20 - n27;
                                    int l = 0;
                                    n33 = 0;
                                    while (l < size) {
                                        final WidgetRun widgetRun3 = this.widgets.get(l);
                                        if (widgetRun3.widget.getVisibility() != 8) {
                                            int n34 = n33;
                                            if (l > 0) {
                                                n34 = n33;
                                                if (l >= n3) {
                                                    n34 = n33 + widgetRun3.start.margin;
                                                }
                                            }
                                            final int n35 = n33 = n34 + widgetRun3.dimension.value;
                                            if (l < n4) {
                                                n33 = n35;
                                                if (l < n5) {
                                                    n33 = n35 + -widgetRun3.end.margin;
                                                }
                                            }
                                        }
                                        ++l;
                                    }
                                    n20 = n32;
                                }
                                else {
                                    n33 = n26;
                                }
                                if (this.chainStyle == 2 && n27 == 0) {
                                    this.chainStyle = 0;
                                    n6 = n33;
                                    n36 = n20;
                                    n22 = n28;
                                }
                                else {
                                    n6 = n33;
                                    n36 = n20;
                                    n22 = n28;
                                }
                            }
                            else {
                                n36 = n20;
                            }
                            if (n6 > n) {
                                this.chainStyle = 2;
                            }
                            if (n19 > 0 && n36 == 0 && n3 == n5) {
                                this.chainStyle = 2;
                            }
                            final int chainStyle = this.chainStyle;
                            if (chainStyle == 1) {
                                int n37;
                                if (n19 > 1) {
                                    n37 = (n - n6) / (n19 - 1);
                                }
                                else if (n19 == 1) {
                                    n37 = (n - n6) / 2;
                                }
                                else {
                                    n37 = 0;
                                }
                                int n38 = n37;
                                if (n36 > 0) {
                                    n38 = 0;
                                }
                                int n39 = 0;
                                int n40 = n22;
                                while (n39 < size) {
                                    int index3;
                                    if (b) {
                                        index3 = size - (n39 + 1);
                                    }
                                    else {
                                        index3 = n39;
                                    }
                                    final WidgetRun widgetRun4 = this.widgets.get(index3);
                                    int n41;
                                    if (widgetRun4.widget.getVisibility() == 8) {
                                        widgetRun4.start.resolve(n40);
                                        widgetRun4.end.resolve(n40);
                                        n41 = n40;
                                    }
                                    else {
                                        int n42 = n40;
                                        if (n39 > 0) {
                                            if (b) {
                                                n42 = n40 - n38;
                                            }
                                            else {
                                                n42 = n40 + n38;
                                            }
                                        }
                                        int n43 = n42;
                                        if (n39 > 0) {
                                            n43 = n42;
                                            if (n39 >= n3) {
                                                if (b) {
                                                    n43 = n42 - widgetRun4.start.margin;
                                                }
                                                else {
                                                    n43 = n42 + widgetRun4.start.margin;
                                                }
                                            }
                                        }
                                        if (b) {
                                            widgetRun4.end.resolve(n43);
                                        }
                                        else {
                                            widgetRun4.start.resolve(n43);
                                        }
                                        final DimensionDependency dimension3 = widgetRun4.dimension;
                                        int n45;
                                        final int n44 = n45 = dimension3.value;
                                        if (widgetRun4.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                                            n45 = n44;
                                            if (widgetRun4.matchConstraintsType == 1) {
                                                n45 = dimension3.wrapValue;
                                            }
                                        }
                                        int n46;
                                        if (b) {
                                            n46 = n43 - n45;
                                        }
                                        else {
                                            n46 = n43 + n45;
                                        }
                                        if (b) {
                                            widgetRun4.start.resolve(n46);
                                        }
                                        else {
                                            widgetRun4.end.resolve(n46);
                                        }
                                        widgetRun4.resolved = true;
                                        n41 = n46;
                                        if (n39 < n4) {
                                            n41 = n46;
                                            if (n39 < n5) {
                                                if (b) {
                                                    n41 = n46 - -widgetRun4.end.margin;
                                                }
                                                else {
                                                    n41 = n46 + -widgetRun4.end.margin;
                                                }
                                            }
                                        }
                                    }
                                    ++n39;
                                    n40 = n41;
                                }
                                return;
                            }
                            if (chainStyle == 0) {
                                int n47 = (n - n6) / (n19 + 1);
                                if (n36 > 0) {
                                    n47 = 0;
                                }
                                for (int n48 = 0; n48 < size; ++n48) {
                                    int index4;
                                    if (b) {
                                        index4 = size - (n48 + 1);
                                    }
                                    else {
                                        index4 = n48;
                                    }
                                    final WidgetRun widgetRun5 = this.widgets.get(index4);
                                    if (widgetRun5.widget.getVisibility() == 8) {
                                        widgetRun5.start.resolve(n22);
                                        widgetRun5.end.resolve(n22);
                                    }
                                    else {
                                        int n49;
                                        if (b) {
                                            n49 = n22 - n47;
                                        }
                                        else {
                                            n49 = n22 + n47;
                                        }
                                        int n50 = n49;
                                        if (n48 > 0) {
                                            n50 = n49;
                                            if (n48 >= n3) {
                                                if (b) {
                                                    n50 = n49 - widgetRun5.start.margin;
                                                }
                                                else {
                                                    n50 = n49 + widgetRun5.start.margin;
                                                }
                                            }
                                        }
                                        if (b) {
                                            widgetRun5.end.resolve(n50);
                                        }
                                        else {
                                            widgetRun5.start.resolve(n50);
                                        }
                                        final DimensionDependency dimension4 = widgetRun5.dimension;
                                        int n51;
                                        final int a4 = n51 = dimension4.value;
                                        if (widgetRun5.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                                            n51 = a4;
                                            if (widgetRun5.matchConstraintsType == 1) {
                                                n51 = Math.min(a4, dimension4.wrapValue);
                                            }
                                        }
                                        int n52;
                                        if (b) {
                                            n52 = n50 - n51;
                                        }
                                        else {
                                            n52 = n50 + n51;
                                        }
                                        if (b) {
                                            widgetRun5.start.resolve(n52);
                                        }
                                        else {
                                            widgetRun5.end.resolve(n52);
                                        }
                                        n22 = n52;
                                        if (n48 < n4) {
                                            n22 = n52;
                                            if (n48 < n5) {
                                                if (b) {
                                                    n22 = n52 - -widgetRun5.end.margin;
                                                }
                                                else {
                                                    n22 = n52 + -widgetRun5.end.margin;
                                                }
                                            }
                                        }
                                    }
                                }
                                return;
                            }
                            if (chainStyle == 2) {
                                float n53;
                                if (super.orientation == 0) {
                                    n53 = super.widget.getHorizontalBiasPercent();
                                }
                                else {
                                    n53 = super.widget.getVerticalBiasPercent();
                                }
                                float n54 = n53;
                                if (b) {
                                    n54 = 1.0f - n53;
                                }
                                int n55 = (int)((n - n6) * n54 + 0.5f);
                                if (n55 < 0 || n36 > 0) {
                                    n55 = 0;
                                }
                                int n56;
                                if (b) {
                                    n56 = n22 - n55;
                                }
                                else {
                                    n56 = n22 + n55;
                                }
                                for (int n57 = 0; n57 < size; ++n57) {
                                    int index5;
                                    if (b) {
                                        index5 = size - (n57 + 1);
                                    }
                                    else {
                                        index5 = n57;
                                    }
                                    final WidgetRun widgetRun6 = this.widgets.get(index5);
                                    if (widgetRun6.widget.getVisibility() == 8) {
                                        widgetRun6.start.resolve(n56);
                                        widgetRun6.end.resolve(n56);
                                    }
                                    else {
                                        int n58 = n56;
                                        if (n57 > 0) {
                                            n58 = n56;
                                            if (n57 >= n3) {
                                                if (b) {
                                                    n58 = n56 - widgetRun6.start.margin;
                                                }
                                                else {
                                                    n58 = n56 + widgetRun6.start.margin;
                                                }
                                            }
                                        }
                                        if (b) {
                                            widgetRun6.end.resolve(n58);
                                        }
                                        else {
                                            widgetRun6.start.resolve(n58);
                                        }
                                        final DimensionDependency dimension5 = widgetRun6.dimension;
                                        int n59 = dimension5.value;
                                        if (widgetRun6.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && widgetRun6.matchConstraintsType == 1) {
                                            n59 = dimension5.wrapValue;
                                        }
                                        int n60;
                                        if (b) {
                                            n60 = n58 - n59;
                                        }
                                        else {
                                            n60 = n58 + n59;
                                        }
                                        if (b) {
                                            widgetRun6.start.resolve(n60);
                                        }
                                        else {
                                            widgetRun6.end.resolve(n60);
                                        }
                                        n56 = n60;
                                        if (n57 < n4) {
                                            n56 = n60;
                                            if (n57 < n5) {
                                                if (b) {
                                                    n56 = n60 - -widgetRun6.end.margin;
                                                }
                                                else {
                                                    n56 = n60 + -widgetRun6.end.margin;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            return;
                        }
                    }
                    final int n19 = 0;
                    int n6 = 0;
                    int n20 = 0;
                    float n9 = 0.0f;
                    continue;
                }
            }
        }
    }
}

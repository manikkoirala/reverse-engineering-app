// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import java.util.Iterator;
import androidx.constraintlayout.core.Metrics;
import androidx.constraintlayout.core.widgets.Barrier;
import androidx.constraintlayout.core.widgets.Flow;
import androidx.constraintlayout.core.widgets.ConstraintWidgetContainer;
import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.Guideline;
import androidx.constraintlayout.core.widgets.HelperWidget;
import java.util.ArrayList;
import androidx.constraintlayout.core.widgets.ConstraintWidget;

public class Grouping
{
    private static final boolean DEBUG = false;
    private static final boolean DEBUG_GROUPING = false;
    
    public static WidgetGroup findDependents(final ConstraintWidget constraintWidget, final int n, final ArrayList<WidgetGroup> list, WidgetGroup o) {
        int n2;
        if (n == 0) {
            n2 = constraintWidget.horizontalGroup;
        }
        else {
            n2 = constraintWidget.verticalGroup;
        }
        final int n3 = 0;
        WidgetGroup widgetGroup;
        if (n2 != -1 && (o == null || n2 != o.id)) {
            int index = 0;
            while (true) {
                widgetGroup = o;
                if (index >= list.size()) {
                    break;
                }
                widgetGroup = list.get(index);
                if (widgetGroup.getId() == n2) {
                    if (o != null) {
                        o.moveTo(n, widgetGroup);
                        list.remove(o);
                    }
                    break;
                }
                ++index;
            }
        }
        else {
            widgetGroup = o;
            if (n2 != -1) {
                return o;
            }
        }
        if ((o = widgetGroup) == null) {
            o = widgetGroup;
            if (constraintWidget instanceof HelperWidget) {
                final int groupInDependents = ((HelperWidget)constraintWidget).findGroupInDependents(n);
                o = widgetGroup;
                if (groupInDependents != -1) {
                    int index2 = 0;
                    while (true) {
                        o = widgetGroup;
                        if (index2 >= list.size()) {
                            break;
                        }
                        o = (WidgetGroup)list.get(index2);
                        if (o.getId() == groupInDependents) {
                            break;
                        }
                        ++index2;
                    }
                }
            }
            WidgetGroup e;
            if ((e = o) == null) {
                e = new WidgetGroup(n);
            }
            list.add(e);
            o = e;
        }
        if (o.add(constraintWidget)) {
            if (constraintWidget instanceof Guideline) {
                final Guideline guideline = (Guideline)constraintWidget;
                final ConstraintAnchor anchor = guideline.getAnchor();
                int n4 = n3;
                if (guideline.getOrientation() == 0) {
                    n4 = 1;
                }
                anchor.findDependents(n4, list, o);
            }
            if (n == 0) {
                constraintWidget.horizontalGroup = o.getId();
                constraintWidget.mLeft.findDependents(n, list, o);
                constraintWidget.mRight.findDependents(n, list, o);
            }
            else {
                constraintWidget.verticalGroup = o.getId();
                constraintWidget.mTop.findDependents(n, list, o);
                constraintWidget.mBaseline.findDependents(n, list, o);
                constraintWidget.mBottom.findDependents(n, list, o);
            }
            constraintWidget.mCenter.findDependents(n, list, o);
        }
        return o;
    }
    
    private static WidgetGroup findGroup(final ArrayList<WidgetGroup> list, final int n) {
        for (int size = list.size(), i = 0; i < size; ++i) {
            final WidgetGroup widgetGroup = list.get(i);
            if (n == widgetGroup.id) {
                return widgetGroup;
            }
        }
        return null;
    }
    
    public static boolean simpleSolvingPass(final ConstraintWidgetContainer constraintWidgetContainer, final BasicMeasure.Measurer measurer) {
        final ArrayList<ConstraintWidget> children = constraintWidgetContainer.getChildren();
        final int size = children.size();
        for (int i = 0; i < size; ++i) {
            final ConstraintWidget constraintWidget = children.get(i);
            if (!validInGroup(constraintWidgetContainer.getHorizontalDimensionBehaviour(), constraintWidgetContainer.getVerticalDimensionBehaviour(), constraintWidget.getHorizontalDimensionBehaviour(), constraintWidget.getVerticalDimensionBehaviour())) {
                return false;
            }
            if (constraintWidget instanceof Flow) {
                return false;
            }
        }
        final Metrics mMetrics = constraintWidgetContainer.mMetrics;
        if (mMetrics != null) {
            ++mMetrics.grouping;
        }
        int j = 0;
        ArrayList list = null;
        ArrayList list2 = null;
        ArrayList list3 = null;
        ArrayList list4 = null;
        ArrayList<Barrier> list5 = null;
        ArrayList<Barrier> list6 = null;
        while (j < size) {
            final ConstraintWidget constraintWidget2 = children.get(j);
            if (!validInGroup(constraintWidgetContainer.getHorizontalDimensionBehaviour(), constraintWidgetContainer.getVerticalDimensionBehaviour(), constraintWidget2.getHorizontalDimensionBehaviour(), constraintWidget2.getVerticalDimensionBehaviour())) {
                ConstraintWidgetContainer.measure(0, constraintWidget2, measurer, constraintWidgetContainer.mMeasure, BasicMeasure.Measure.SELF_DIMENSIONS);
            }
            final boolean b = constraintWidget2 instanceof Guideline;
            ArrayList list7 = list;
            ArrayList list8 = list3;
            if (b) {
                final Guideline guideline = (Guideline)constraintWidget2;
                ArrayList list9 = list3;
                if (guideline.getOrientation() == 0) {
                    if ((list9 = list3) == null) {
                        list9 = new ArrayList();
                    }
                    list9.add(guideline);
                }
                list7 = list;
                list8 = list9;
                if (guideline.getOrientation() == 1) {
                    ArrayList list10;
                    if ((list10 = list) == null) {
                        list10 = new ArrayList();
                    }
                    list10.add(guideline);
                    list8 = list9;
                    list7 = list10;
                }
            }
            ArrayList list11 = list2;
            ArrayList list12 = list4;
            if (constraintWidget2 instanceof HelperWidget) {
                if (constraintWidget2 instanceof Barrier) {
                    final Barrier barrier = (Barrier)constraintWidget2;
                    ArrayList list13 = list2;
                    if (barrier.getOrientation() == 0) {
                        if ((list13 = list2) == null) {
                            list13 = new ArrayList();
                        }
                        list13.add(barrier);
                    }
                    list11 = list13;
                    list12 = list4;
                    if (barrier.getOrientation() == 1) {
                        if ((list12 = list4) == null) {
                            list12 = new ArrayList();
                        }
                        list12.add(barrier);
                        list11 = list13;
                    }
                }
                else {
                    final HelperWidget helperWidget = (HelperWidget)constraintWidget2;
                    if ((list11 = list2) == null) {
                        list11 = new ArrayList();
                    }
                    list11.add(helperWidget);
                    if ((list12 = list4) == null) {
                        list12 = new ArrayList();
                    }
                    list12.add(helperWidget);
                }
            }
            ArrayList<Barrier> list14 = list5;
            if (constraintWidget2.mLeft.mTarget == null) {
                list14 = list5;
                if (constraintWidget2.mRight.mTarget == null) {
                    list14 = list5;
                    if (!b) {
                        list14 = list5;
                        if (!(constraintWidget2 instanceof Barrier)) {
                            ArrayList<Barrier> list15;
                            if ((list15 = list5) == null) {
                                list15 = new ArrayList<Barrier>();
                            }
                            list15.add((Barrier)constraintWidget2);
                            list14 = list15;
                        }
                    }
                }
            }
            ArrayList<Barrier> list16 = list6;
            if (constraintWidget2.mTop.mTarget == null) {
                list16 = list6;
                if (constraintWidget2.mBottom.mTarget == null) {
                    list16 = list6;
                    if (constraintWidget2.mBaseline.mTarget == null) {
                        list16 = list6;
                        if (!b) {
                            list16 = list6;
                            if (!(constraintWidget2 instanceof Barrier)) {
                                ArrayList<Barrier> list17;
                                if ((list17 = list6) == null) {
                                    list17 = new ArrayList<Barrier>();
                                }
                                list17.add((Barrier)constraintWidget2);
                                list16 = list17;
                            }
                        }
                    }
                }
            }
            ++j;
            list = list7;
            list2 = list11;
            list3 = list8;
            list4 = list12;
            list5 = list14;
            list6 = list16;
        }
        final ArrayList<WidgetGroup> list18 = new ArrayList<WidgetGroup>();
        if (list != null) {
            final Iterator iterator = list.iterator();
            while (iterator.hasNext()) {
                findDependents((ConstraintWidget)iterator.next(), 0, list18, null);
            }
        }
        if (list2 != null) {
            for (final HelperWidget helperWidget2 : list2) {
                final WidgetGroup dependents = findDependents(helperWidget2, 0, list18, null);
                helperWidget2.addDependents(list18, 0, dependents);
                dependents.cleanup(list18);
            }
        }
        final ConstraintAnchor anchor = constraintWidgetContainer.getAnchor(ConstraintAnchor.Type.LEFT);
        if (anchor.getDependents() != null) {
            final Iterator<ConstraintAnchor> iterator3 = anchor.getDependents().iterator();
            while (iterator3.hasNext()) {
                findDependents(iterator3.next().mOwner, 0, list18, null);
            }
        }
        final ConstraintAnchor anchor2 = constraintWidgetContainer.getAnchor(ConstraintAnchor.Type.RIGHT);
        if (anchor2.getDependents() != null) {
            final Iterator<ConstraintAnchor> iterator4 = anchor2.getDependents().iterator();
            while (iterator4.hasNext()) {
                findDependents(iterator4.next().mOwner, 0, list18, null);
            }
        }
        final ConstraintAnchor anchor3 = constraintWidgetContainer.getAnchor(ConstraintAnchor.Type.CENTER);
        if (anchor3.getDependents() != null) {
            final Iterator<ConstraintAnchor> iterator5 = anchor3.getDependents().iterator();
            while (iterator5.hasNext()) {
                findDependents(iterator5.next().mOwner, 0, list18, null);
            }
        }
        if (list5 != null) {
            final Iterator<Barrier> iterator6 = list5.iterator();
            while (iterator6.hasNext()) {
                findDependents(iterator6.next(), 0, list18, null);
            }
        }
        if (list3 != null) {
            final Iterator iterator7 = list3.iterator();
            while (iterator7.hasNext()) {
                findDependents((ConstraintWidget)iterator7.next(), 1, list18, null);
            }
        }
        if (list4 != null) {
            for (final HelperWidget helperWidget3 : list4) {
                final WidgetGroup dependents2 = findDependents(helperWidget3, 1, list18, null);
                helperWidget3.addDependents(list18, 1, dependents2);
                dependents2.cleanup(list18);
            }
        }
        final ConstraintAnchor anchor4 = constraintWidgetContainer.getAnchor(ConstraintAnchor.Type.TOP);
        if (anchor4.getDependents() != null) {
            final Iterator<ConstraintAnchor> iterator9 = anchor4.getDependents().iterator();
            while (iterator9.hasNext()) {
                findDependents(iterator9.next().mOwner, 1, list18, null);
            }
        }
        final ConstraintAnchor anchor5 = constraintWidgetContainer.getAnchor(ConstraintAnchor.Type.BASELINE);
        if (anchor5.getDependents() != null) {
            final Iterator<ConstraintAnchor> iterator10 = anchor5.getDependents().iterator();
            while (iterator10.hasNext()) {
                findDependents(iterator10.next().mOwner, 1, list18, null);
            }
        }
        final ConstraintAnchor anchor6 = constraintWidgetContainer.getAnchor(ConstraintAnchor.Type.BOTTOM);
        if (anchor6.getDependents() != null) {
            final Iterator<ConstraintAnchor> iterator11 = anchor6.getDependents().iterator();
            while (iterator11.hasNext()) {
                findDependents(iterator11.next().mOwner, 1, list18, null);
            }
        }
        final ConstraintAnchor anchor7 = constraintWidgetContainer.getAnchor(ConstraintAnchor.Type.CENTER);
        if (anchor7.getDependents() != null) {
            final Iterator<ConstraintAnchor> iterator12 = anchor7.getDependents().iterator();
            while (iterator12.hasNext()) {
                findDependents(iterator12.next().mOwner, 1, list18, null);
            }
        }
        if (list6 != null) {
            final Iterator<Barrier> iterator13 = list6.iterator();
            while (iterator13.hasNext()) {
                findDependents(iterator13.next(), 1, list18, null);
            }
        }
        for (int k = 0; k < size; ++k) {
            final ConstraintWidget constraintWidget3 = children.get(k);
            if (constraintWidget3.oppositeDimensionsTied()) {
                final WidgetGroup group = findGroup(list18, constraintWidget3.horizontalGroup);
                final WidgetGroup group2 = findGroup(list18, constraintWidget3.verticalGroup);
                if (group != null && group2 != null) {
                    group.moveTo(0, group2);
                    group2.setOrientation(2);
                    list18.remove(group);
                }
            }
        }
        if (list18.size() <= 1) {
            return false;
        }
        WidgetGroup widgetGroup3 = null;
        Label_1562: {
            if (constraintWidgetContainer.getHorizontalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                final Iterator<WidgetGroup> iterator14 = list18.iterator();
                WidgetGroup widgetGroup = null;
                int width = 0;
                while (iterator14.hasNext()) {
                    final WidgetGroup widgetGroup2 = iterator14.next();
                    if (widgetGroup2.getOrientation() == 1) {
                        continue;
                    }
                    widgetGroup2.setAuthoritative(false);
                    final int measureWrap = widgetGroup2.measureWrap(constraintWidgetContainer.getSystem(), 0);
                    if (measureWrap <= width) {
                        continue;
                    }
                    widgetGroup = widgetGroup2;
                    width = measureWrap;
                }
                if (widgetGroup != null) {
                    constraintWidgetContainer.setHorizontalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.FIXED);
                    constraintWidgetContainer.setWidth(width);
                    widgetGroup.setAuthoritative(true);
                    widgetGroup3 = widgetGroup;
                    break Label_1562;
                }
            }
            widgetGroup3 = null;
        }
        if (constraintWidgetContainer.getVerticalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
            final Iterator<WidgetGroup> iterator15 = list18.iterator();
            WidgetGroup widgetGroup4 = null;
            int height = 0;
            while (iterator15.hasNext()) {
                final WidgetGroup widgetGroup5 = iterator15.next();
                if (widgetGroup5.getOrientation() == 0) {
                    continue;
                }
                widgetGroup5.setAuthoritative(false);
                final int measureWrap2 = widgetGroup5.measureWrap(constraintWidgetContainer.getSystem(), 1);
                if (measureWrap2 <= height) {
                    continue;
                }
                widgetGroup4 = widgetGroup5;
                height = measureWrap2;
            }
            if (widgetGroup4 != null) {
                constraintWidgetContainer.setVerticalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.FIXED);
                constraintWidgetContainer.setHeight(height);
                widgetGroup4.setAuthoritative(true);
                final WidgetGroup widgetGroup6 = widgetGroup4;
                return widgetGroup3 != null || widgetGroup6 != null;
            }
        }
        final WidgetGroup widgetGroup6 = null;
        return widgetGroup3 != null || widgetGroup6 != null;
    }
    
    public static boolean validInGroup(ConstraintWidget.DimensionBehaviour wrap_CONTENT, final ConstraintWidget.DimensionBehaviour dimensionBehaviour, final ConstraintWidget.DimensionBehaviour dimensionBehaviour2, final ConstraintWidget.DimensionBehaviour dimensionBehaviour3) {
        final ConstraintWidget.DimensionBehaviour fixed = ConstraintWidget.DimensionBehaviour.FIXED;
        boolean b = false;
        Label_0047: {
            if (dimensionBehaviour2 != fixed) {
                final ConstraintWidget.DimensionBehaviour wrap_CONTENT2 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                if (dimensionBehaviour2 != wrap_CONTENT2) {
                    if (dimensionBehaviour2 != ConstraintWidget.DimensionBehaviour.MATCH_PARENT || wrap_CONTENT == wrap_CONTENT2) {
                        b = false;
                        break Label_0047;
                    }
                }
            }
            b = true;
        }
        if (dimensionBehaviour3 != fixed) {
            wrap_CONTENT = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
            if (dimensionBehaviour3 != wrap_CONTENT) {
                if (dimensionBehaviour3 != ConstraintWidget.DimensionBehaviour.MATCH_PARENT || dimensionBehaviour == wrap_CONTENT) {
                    final boolean b2 = false;
                    return b || b2;
                }
            }
        }
        final boolean b2 = true;
        return b || b2;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import java.util.Iterator;

class DimensionDependency extends DependencyNode
{
    public int wrapValue;
    
    public DimensionDependency(final WidgetRun widgetRun) {
        super(widgetRun);
        if (widgetRun instanceof HorizontalWidgetRun) {
            super.type = Type.HORIZONTAL_DIMENSION;
        }
        else {
            super.type = Type.VERTICAL_DIMENSION;
        }
    }
    
    @Override
    public void resolve(final int value) {
        if (super.resolved) {
            return;
        }
        super.resolved = true;
        super.value = value;
        for (final Dependency dependency : super.dependencies) {
            dependency.update(dependency);
        }
    }
}

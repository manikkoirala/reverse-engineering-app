// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

class BaselineDimensionDependency extends DimensionDependency
{
    public BaselineDimensionDependency(final WidgetRun widgetRun) {
        super(widgetRun);
    }
    
    public void update(final DependencyNode dependencyNode) {
        final WidgetRun run = super.run;
        ((VerticalWidgetRun)run).baseline.margin = run.widget.getBaselineDistance();
        super.resolved = true;
    }
}

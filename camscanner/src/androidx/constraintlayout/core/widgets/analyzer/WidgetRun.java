// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.ConstraintWidget;

public abstract class WidgetRun implements Dependency
{
    DimensionDependency dimension;
    protected ConstraintWidget.DimensionBehaviour dimensionBehavior;
    public DependencyNode end;
    protected RunType mRunType;
    public int matchConstraintsType;
    public int orientation;
    boolean resolved;
    RunGroup runGroup;
    public DependencyNode start;
    ConstraintWidget widget;
    
    public WidgetRun(final ConstraintWidget widget) {
        this.dimension = new DimensionDependency(this);
        this.orientation = 0;
        this.resolved = false;
        this.start = new DependencyNode(this);
        this.end = new DependencyNode(this);
        this.mRunType = RunType.NONE;
        this.widget = widget;
    }
    
    private void resolveDimension(int limitedDimension, int b) {
        final int matchConstraintsType = this.matchConstraintsType;
        if (matchConstraintsType != 0) {
            if (matchConstraintsType != 1) {
                if (matchConstraintsType != 2) {
                    if (matchConstraintsType == 3) {
                        final ConstraintWidget widget = this.widget;
                        WidgetRun widgetRun = widget.horizontalRun;
                        final ConstraintWidget.DimensionBehaviour dimensionBehavior = widgetRun.dimensionBehavior;
                        final ConstraintWidget.DimensionBehaviour match_CONSTRAINT = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                        if (dimensionBehavior == match_CONSTRAINT && widgetRun.matchConstraintsType == 3) {
                            final VerticalWidgetRun verticalRun = widget.verticalRun;
                            if (verticalRun.dimensionBehavior == match_CONSTRAINT && verticalRun.matchConstraintsType == 3) {
                                return;
                            }
                        }
                        if (limitedDimension == 0) {
                            widgetRun = widget.verticalRun;
                        }
                        if (widgetRun.dimension.resolved) {
                            final float dimensionRatio = widget.getDimensionRatio();
                            if (limitedDimension == 1) {
                                limitedDimension = (int)(widgetRun.dimension.value / dimensionRatio + 0.5f);
                            }
                            else {
                                limitedDimension = (int)(dimensionRatio * widgetRun.dimension.value + 0.5f);
                            }
                            this.dimension.resolve(limitedDimension);
                        }
                    }
                }
                else {
                    final ConstraintWidget parent = this.widget.getParent();
                    if (parent != null) {
                        WidgetRun widgetRun2;
                        if (limitedDimension == 0) {
                            widgetRun2 = parent.horizontalRun;
                        }
                        else {
                            widgetRun2 = parent.verticalRun;
                        }
                        final DimensionDependency dimension = widgetRun2.dimension;
                        if (dimension.resolved) {
                            final ConstraintWidget widget2 = this.widget;
                            float n;
                            if (limitedDimension == 0) {
                                n = widget2.mMatchConstraintPercentWidth;
                            }
                            else {
                                n = widget2.mMatchConstraintPercentHeight;
                            }
                            b = (int)(dimension.value * n + 0.5f);
                            this.dimension.resolve(this.getLimitedDimension(b, limitedDimension));
                        }
                    }
                }
            }
            else {
                limitedDimension = this.getLimitedDimension(this.dimension.wrapValue, limitedDimension);
                this.dimension.resolve(Math.min(limitedDimension, b));
            }
        }
        else {
            this.dimension.resolve(this.getLimitedDimension(b, limitedDimension));
        }
    }
    
    protected final void addTarget(final DependencyNode dependencyNode, final DependencyNode dependencyNode2, final int margin) {
        dependencyNode.targets.add(dependencyNode2);
        dependencyNode.margin = margin;
        dependencyNode2.dependencies.add(dependencyNode);
    }
    
    protected final void addTarget(final DependencyNode dependencyNode, final DependencyNode dependencyNode2, final int marginFactor, final DimensionDependency marginDependency) {
        dependencyNode.targets.add(dependencyNode2);
        dependencyNode.targets.add(this.dimension);
        dependencyNode.marginFactor = marginFactor;
        dependencyNode.marginDependency = marginDependency;
        dependencyNode2.dependencies.add(dependencyNode);
        marginDependency.dependencies.add(dependencyNode);
    }
    
    abstract void apply();
    
    abstract void applyToWidget();
    
    abstract void clear();
    
    protected final int getLimitedDimension(final int n, int n2) {
        if (n2 == 0) {
            final ConstraintWidget widget = this.widget;
            final int mMatchConstraintMaxWidth = widget.mMatchConstraintMaxWidth;
            n2 = Math.max(widget.mMatchConstraintMinWidth, n);
            if (mMatchConstraintMaxWidth > 0) {
                n2 = Math.min(mMatchConstraintMaxWidth, n);
            }
            final int n3;
            if (n2 == (n3 = n)) {
                return n3;
            }
        }
        else {
            final ConstraintWidget widget2 = this.widget;
            final int mMatchConstraintMaxHeight = widget2.mMatchConstraintMaxHeight;
            n2 = Math.max(widget2.mMatchConstraintMinHeight, n);
            if (mMatchConstraintMaxHeight > 0) {
                n2 = Math.min(mMatchConstraintMaxHeight, n);
            }
            final int n3;
            if (n2 == (n3 = n)) {
                return n3;
            }
        }
        return n2;
    }
    
    protected final DependencyNode getTarget(final ConstraintAnchor constraintAnchor) {
        final ConstraintAnchor mTarget = constraintAnchor.mTarget;
        DependencyNode dependencyNode = null;
        if (mTarget == null) {
            return null;
        }
        final ConstraintWidget mOwner = mTarget.mOwner;
        final int n = WidgetRun$1.$SwitchMap$androidx$constraintlayout$core$widgets$ConstraintAnchor$Type[mTarget.mType.ordinal()];
        if (n != 1) {
            if (n != 2) {
                if (n != 3) {
                    if (n != 4) {
                        if (n == 5) {
                            dependencyNode = mOwner.verticalRun.end;
                        }
                    }
                    else {
                        dependencyNode = mOwner.verticalRun.baseline;
                    }
                }
                else {
                    dependencyNode = mOwner.verticalRun.start;
                }
            }
            else {
                dependencyNode = mOwner.horizontalRun.end;
            }
        }
        else {
            dependencyNode = mOwner.horizontalRun.start;
        }
        return dependencyNode;
    }
    
    protected final DependencyNode getTarget(final ConstraintAnchor constraintAnchor, int n) {
        final ConstraintAnchor mTarget = constraintAnchor.mTarget;
        final DependencyNode dependencyNode = null;
        if (mTarget == null) {
            return null;
        }
        final ConstraintWidget mOwner = mTarget.mOwner;
        WidgetRun widgetRun;
        if (n == 0) {
            widgetRun = mOwner.horizontalRun;
        }
        else {
            widgetRun = mOwner.verticalRun;
        }
        n = WidgetRun$1.$SwitchMap$androidx$constraintlayout$core$widgets$ConstraintAnchor$Type[mTarget.mType.ordinal()];
        if (n != 1) {
            if (n != 2) {
                if (n == 3) {
                    return widgetRun.start;
                }
                if (n != 5) {
                    return dependencyNode;
                }
            }
            return widgetRun.end;
        }
        return widgetRun.start;
    }
    
    public long getWrapDimension() {
        final DimensionDependency dimension = this.dimension;
        if (dimension.resolved) {
            return dimension.value;
        }
        return 0L;
    }
    
    public boolean isCenterConnection() {
        final int size = this.start.targets.size();
        boolean b = false;
        int i = 0;
        int n = 0;
        while (i < size) {
            int n2 = n;
            if (this.start.targets.get(i).run != this) {
                n2 = n + 1;
            }
            ++i;
            n = n2;
        }
        final int size2 = this.end.targets.size();
        final int n3 = 0;
        int n4 = n;
        int n5;
        for (int j = n3; j < size2; ++j, n4 = n5) {
            n5 = n4;
            if (this.end.targets.get(j).run != this) {
                n5 = n4 + 1;
            }
        }
        if (n4 >= 2) {
            b = true;
        }
        return b;
    }
    
    public boolean isDimensionResolved() {
        return this.dimension.resolved;
    }
    
    public boolean isResolved() {
        return this.resolved;
    }
    
    abstract void reset();
    
    abstract boolean supportsWrapComputation();
    
    @Override
    public void update(final Dependency dependency) {
    }
    
    protected void updateRunCenter(final Dependency dependency, final ConstraintAnchor constraintAnchor, final ConstraintAnchor constraintAnchor2, int value) {
        final DependencyNode target = this.getTarget(constraintAnchor);
        final DependencyNode target2 = this.getTarget(constraintAnchor2);
        if (target.resolved) {
            if (target2.resolved) {
                int value2 = target.value + constraintAnchor.getMargin();
                final int n = target2.value - constraintAnchor2.getMargin();
                final int n2 = n - value2;
                if (!this.dimension.resolved && this.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    this.resolveDimension(value, n2);
                }
                final DimensionDependency dimension = this.dimension;
                if (!dimension.resolved) {
                    return;
                }
                if (dimension.value == n2) {
                    this.start.resolve(value2);
                    this.end.resolve(n);
                    return;
                }
                final ConstraintWidget widget = this.widget;
                float n3;
                if (value == 0) {
                    n3 = widget.getHorizontalBiasPercent();
                }
                else {
                    n3 = widget.getVerticalBiasPercent();
                }
                value = n;
                if (target == target2) {
                    value2 = target.value;
                    value = target2.value;
                    n3 = 0.5f;
                }
                this.start.resolve((int)(value2 + 0.5f + (value - value2 - this.dimension.value) * n3));
                this.end.resolve(this.start.value + this.dimension.value);
            }
        }
    }
    
    protected void updateRunEnd(final Dependency dependency) {
    }
    
    protected void updateRunStart(final Dependency dependency) {
    }
    
    public long wrapSize(int margin) {
        final DimensionDependency dimension = this.dimension;
        if (dimension.resolved) {
            final long n = dimension.value;
            if (this.isCenterConnection()) {
                margin = this.start.margin - this.end.margin;
            }
            else {
                if (margin != 0) {
                    return n - this.end.margin;
                }
                margin = this.start.margin;
            }
            return n + margin;
        }
        return 0L;
    }
    
    enum RunType
    {
        private static final RunType[] $VALUES;
        
        CENTER, 
        END, 
        NONE, 
        START;
    }
}

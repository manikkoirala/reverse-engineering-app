// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import androidx.constraintlayout.core.widgets.ConstraintWidgetContainer;
import java.util.Iterator;
import java.util.ArrayList;

class RunGroup
{
    public static final int BASELINE = 2;
    public static final int END = 1;
    public static final int START = 0;
    public static int index;
    int direction;
    public boolean dual;
    WidgetRun firstRun;
    int groupIndex;
    WidgetRun lastRun;
    public int position;
    ArrayList<WidgetRun> runs;
    
    public RunGroup(final WidgetRun widgetRun, final int direction) {
        this.position = 0;
        this.dual = false;
        this.firstRun = null;
        this.lastRun = null;
        this.runs = new ArrayList<WidgetRun>();
        final int index = RunGroup.index;
        this.groupIndex = index;
        RunGroup.index = index + 1;
        this.firstRun = widgetRun;
        this.lastRun = widgetRun;
        this.direction = direction;
    }
    
    private boolean defineTerminalWidget(final WidgetRun widgetRun, final int n) {
        if (!widgetRun.widget.isTerminalWidget[n]) {
            return false;
        }
        for (final Dependency dependency : widgetRun.start.dependencies) {
            if (dependency instanceof DependencyNode) {
                final DependencyNode dependencyNode = (DependencyNode)dependency;
                final WidgetRun run = dependencyNode.run;
                if (run == widgetRun) {
                    continue;
                }
                if (dependencyNode != run.start) {
                    continue;
                }
                if (widgetRun instanceof ChainRun) {
                    final Iterator<WidgetRun> iterator2 = ((ChainRun)widgetRun).widgets.iterator();
                    while (iterator2.hasNext()) {
                        this.defineTerminalWidget(iterator2.next(), n);
                    }
                }
                else if (!(widgetRun instanceof HelperReferences)) {
                    widgetRun.widget.isTerminalWidget[n] = false;
                }
                this.defineTerminalWidget(dependencyNode.run, n);
            }
        }
        for (final Dependency dependency2 : widgetRun.end.dependencies) {
            if (dependency2 instanceof DependencyNode) {
                final DependencyNode dependencyNode2 = (DependencyNode)dependency2;
                final WidgetRun run2 = dependencyNode2.run;
                if (run2 == widgetRun) {
                    continue;
                }
                if (dependencyNode2 != run2.start) {
                    continue;
                }
                if (widgetRun instanceof ChainRun) {
                    final Iterator<WidgetRun> iterator4 = ((ChainRun)widgetRun).widgets.iterator();
                    while (iterator4.hasNext()) {
                        this.defineTerminalWidget(iterator4.next(), n);
                    }
                }
                else if (!(widgetRun instanceof HelperReferences)) {
                    widgetRun.widget.isTerminalWidget[n] = false;
                }
                this.defineTerminalWidget(dependencyNode2.run, n);
            }
        }
        return false;
    }
    
    private long traverseEnd(DependencyNode start, long n) {
        final WidgetRun run = start.run;
        if (run instanceof HelperReferences) {
            return n;
        }
        final int size = start.dependencies.size();
        int i = 0;
        long n2 = n;
        while (i < size) {
            final Dependency dependency = start.dependencies.get(i);
            long min = n2;
            if (dependency instanceof DependencyNode) {
                final DependencyNode dependencyNode = (DependencyNode)dependency;
                if (dependencyNode.run == run) {
                    min = n2;
                }
                else {
                    min = Math.min(n2, this.traverseEnd(dependencyNode, dependencyNode.margin + n));
                }
            }
            ++i;
            n2 = min;
        }
        long min2 = n2;
        if (start == run.end) {
            final long wrapDimension = run.getWrapDimension();
            start = run.start;
            n -= wrapDimension;
            min2 = Math.min(Math.min(n2, this.traverseEnd(start, n)), n - run.start.margin);
        }
        return min2;
    }
    
    private long traverseStart(DependencyNode end, long n) {
        final WidgetRun run = end.run;
        if (run instanceof HelperReferences) {
            return n;
        }
        final int size = end.dependencies.size();
        int i = 0;
        long n2 = n;
        while (i < size) {
            final Dependency dependency = end.dependencies.get(i);
            long max = n2;
            if (dependency instanceof DependencyNode) {
                final DependencyNode dependencyNode = (DependencyNode)dependency;
                if (dependencyNode.run == run) {
                    max = n2;
                }
                else {
                    max = Math.max(n2, this.traverseStart(dependencyNode, dependencyNode.margin + n));
                }
            }
            ++i;
            n2 = max;
        }
        long max2 = n2;
        if (end == run.start) {
            final long wrapDimension = run.getWrapDimension();
            end = run.end;
            n += wrapDimension;
            max2 = Math.max(Math.max(n2, this.traverseStart(end, n)), n - run.end.margin);
        }
        return max2;
    }
    
    public void add(final WidgetRun widgetRun) {
        this.runs.add(widgetRun);
        this.lastRun = widgetRun;
    }
    
    public long computeWrapSize(final ConstraintWidgetContainer constraintWidgetContainer, int n) {
        final WidgetRun firstRun = this.firstRun;
        final boolean b = firstRun instanceof ChainRun;
        long n2 = 0L;
        if (b) {
            if (((ChainRun)firstRun).orientation != n) {
                return 0L;
            }
        }
        else if (n == 0) {
            if (!(firstRun instanceof HorizontalWidgetRun)) {
                return 0L;
            }
        }
        else if (!(firstRun instanceof VerticalWidgetRun)) {
            return 0L;
        }
        WidgetRun widgetRun;
        if (n == 0) {
            widgetRun = constraintWidgetContainer.horizontalRun;
        }
        else {
            widgetRun = constraintWidgetContainer.verticalRun;
        }
        final DependencyNode start = widgetRun.start;
        WidgetRun widgetRun2;
        if (n == 0) {
            widgetRun2 = constraintWidgetContainer.horizontalRun;
        }
        else {
            widgetRun2 = constraintWidgetContainer.verticalRun;
        }
        final DependencyNode end = widgetRun2.end;
        final boolean contains = firstRun.start.targets.contains(start);
        final boolean contains2 = this.firstRun.end.targets.contains(end);
        final long wrapDimension = this.firstRun.getWrapDimension();
        long n11;
        if (contains && contains2) {
            final long traverseStart = this.traverseStart(this.firstRun.start, 0L);
            final long traverseEnd = this.traverseEnd(this.firstRun.end, 0L);
            final long n3 = traverseStart - wrapDimension;
            final WidgetRun firstRun2 = this.firstRun;
            final int margin = firstRun2.end.margin;
            long n4 = n3;
            if (n3 >= -margin) {
                n4 = n3 + margin;
            }
            final long n5 = -traverseEnd;
            final int margin2 = firstRun2.start.margin;
            long n7;
            final long n6 = n7 = n5 - wrapDimension - margin2;
            if (n6 >= margin2) {
                n7 = n6 - margin2;
            }
            final float biasPercent = firstRun2.widget.getBiasPercent(n);
            if (biasPercent > 0.0f) {
                n2 = (long)(n7 / biasPercent + n4 / (1.0f - biasPercent));
            }
            final float n8 = (float)n2;
            final long n9 = (long)(n8 * biasPercent + 0.5f);
            final long n10 = (long)(n8 * (1.0f - biasPercent) + 0.5f);
            final WidgetRun firstRun3 = this.firstRun;
            n11 = firstRun3.start.margin + (n9 + wrapDimension + n10);
            n = firstRun3.end.margin;
        }
        else {
            if (contains) {
                final DependencyNode start2 = this.firstRun.start;
                return Math.max(this.traverseStart(start2, start2.margin), this.firstRun.start.margin + wrapDimension);
            }
            if (contains2) {
                final DependencyNode end2 = this.firstRun.end;
                return Math.max(-this.traverseEnd(end2, end2.margin), -this.firstRun.end.margin + wrapDimension);
            }
            final WidgetRun firstRun4 = this.firstRun;
            n11 = firstRun4.start.margin + firstRun4.getWrapDimension();
            n = this.firstRun.end.margin;
        }
        return n11 - n;
    }
    
    public void defineTerminalWidgets(final boolean b, final boolean b2) {
        if (b) {
            final WidgetRun firstRun = this.firstRun;
            if (firstRun instanceof HorizontalWidgetRun) {
                this.defineTerminalWidget(firstRun, 0);
            }
        }
        if (b2) {
            final WidgetRun firstRun2 = this.firstRun;
            if (firstRun2 instanceof VerticalWidgetRun) {
                this.defineTerminalWidget(firstRun2, 1);
            }
        }
    }
}

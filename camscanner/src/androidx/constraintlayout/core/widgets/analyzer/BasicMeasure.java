// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import androidx.constraintlayout.core.widgets.Helper;
import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.LinearSystem;
import androidx.constraintlayout.core.widgets.Optimizer;
import androidx.constraintlayout.core.Metrics;
import androidx.constraintlayout.core.widgets.VirtualLayout;
import androidx.constraintlayout.core.widgets.Barrier;
import androidx.constraintlayout.core.widgets.Guideline;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import java.util.ArrayList;
import androidx.constraintlayout.core.widgets.ConstraintWidgetContainer;

public class BasicMeasure
{
    public static final int AT_MOST = Integer.MIN_VALUE;
    private static final boolean DEBUG = false;
    public static final int EXACTLY = 1073741824;
    public static final int FIXED = -3;
    public static final int MATCH_PARENT = -1;
    private static final int MODE_SHIFT = 30;
    public static final int UNSPECIFIED = 0;
    public static final int WRAP_CONTENT = -2;
    private ConstraintWidgetContainer constraintWidgetContainer;
    private Measure mMeasure;
    private final ArrayList<ConstraintWidget> mVariableDimensionsWidgets;
    
    public BasicMeasure(final ConstraintWidgetContainer constraintWidgetContainer) {
        this.mVariableDimensionsWidgets = new ArrayList<ConstraintWidget>();
        this.mMeasure = new Measure();
        this.constraintWidgetContainer = constraintWidgetContainer;
    }
    
    private boolean measure(final Measurer measurer, final ConstraintWidget constraintWidget, int measureStrategy) {
        this.mMeasure.horizontalBehavior = constraintWidget.getHorizontalDimensionBehaviour();
        this.mMeasure.verticalBehavior = constraintWidget.getVerticalDimensionBehaviour();
        this.mMeasure.horizontalDimension = constraintWidget.getWidth();
        this.mMeasure.verticalDimension = constraintWidget.getHeight();
        final Measure mMeasure = this.mMeasure;
        mMeasure.measuredNeedsSolverPass = false;
        mMeasure.measureStrategy = measureStrategy;
        final ConstraintWidget.DimensionBehaviour horizontalBehavior = mMeasure.horizontalBehavior;
        final ConstraintWidget.DimensionBehaviour match_CONSTRAINT = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
        if (horizontalBehavior == match_CONSTRAINT) {
            measureStrategy = 1;
        }
        else {
            measureStrategy = 0;
        }
        final boolean b = mMeasure.verticalBehavior == match_CONSTRAINT;
        if (measureStrategy != 0 && constraintWidget.mDimensionRatio > 0.0f) {
            measureStrategy = 1;
        }
        else {
            measureStrategy = 0;
        }
        final boolean b2 = b && constraintWidget.mDimensionRatio > 0.0f;
        if (measureStrategy != 0 && constraintWidget.mResolvedMatchConstraintDefault[0] == 4) {
            mMeasure.horizontalBehavior = ConstraintWidget.DimensionBehaviour.FIXED;
        }
        if (b2 && constraintWidget.mResolvedMatchConstraintDefault[1] == 4) {
            mMeasure.verticalBehavior = ConstraintWidget.DimensionBehaviour.FIXED;
        }
        measurer.measure(constraintWidget, mMeasure);
        constraintWidget.setWidth(this.mMeasure.measuredWidth);
        constraintWidget.setHeight(this.mMeasure.measuredHeight);
        constraintWidget.setHasBaseline(this.mMeasure.measuredHasBaseline);
        constraintWidget.setBaselineDistance(this.mMeasure.measuredBaseline);
        final Measure mMeasure2 = this.mMeasure;
        mMeasure2.measureStrategy = Measure.SELF_DIMENSIONS;
        return mMeasure2.measuredNeedsSolverPass;
    }
    
    private void measureChildren(final ConstraintWidgetContainer constraintWidgetContainer) {
        final int size = constraintWidgetContainer.mChildren.size();
        final boolean optimize = constraintWidgetContainer.optimizeFor(64);
        final Measurer measurer = constraintWidgetContainer.getMeasurer();
        for (int i = 0; i < size; ++i) {
            final ConstraintWidget constraintWidget = constraintWidgetContainer.mChildren.get(i);
            if (!(constraintWidget instanceof Guideline)) {
                if (!(constraintWidget instanceof Barrier)) {
                    if (!constraintWidget.isInVirtualLayout()) {
                        if (optimize) {
                            final HorizontalWidgetRun horizontalRun = constraintWidget.horizontalRun;
                            if (horizontalRun != null) {
                                final VerticalWidgetRun verticalRun = constraintWidget.verticalRun;
                                if (verticalRun != null && horizontalRun.dimension.resolved && verticalRun.dimension.resolved) {
                                    continue;
                                }
                            }
                        }
                        final ConstraintWidget.DimensionBehaviour dimensionBehaviour = constraintWidget.getDimensionBehaviour(0);
                        final int n = 1;
                        final ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = constraintWidget.getDimensionBehaviour(1);
                        final ConstraintWidget.DimensionBehaviour match_CONSTRAINT = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                        final boolean b = dimensionBehaviour == match_CONSTRAINT && constraintWidget.mMatchConstraintDefaultWidth != 1 && dimensionBehaviour2 == match_CONSTRAINT && constraintWidget.mMatchConstraintDefaultHeight != 1;
                        int n2 = 0;
                        Label_0337: {
                            if ((n2 = (b ? 1 : 0)) == 0) {
                                n2 = (b ? 1 : 0);
                                if (constraintWidgetContainer.optimizeFor(1)) {
                                    n2 = (b ? 1 : 0);
                                    if (!(constraintWidget instanceof VirtualLayout)) {
                                        int n3 = b ? 1 : 0;
                                        if (dimensionBehaviour == match_CONSTRAINT) {
                                            n3 = (b ? 1 : 0);
                                            if (constraintWidget.mMatchConstraintDefaultWidth == 0) {
                                                n3 = (b ? 1 : 0);
                                                if (dimensionBehaviour2 != match_CONSTRAINT) {
                                                    n3 = (b ? 1 : 0);
                                                    if (!constraintWidget.isInHorizontalChain()) {
                                                        n3 = 1;
                                                    }
                                                }
                                            }
                                        }
                                        int n4 = n3;
                                        if (dimensionBehaviour2 == match_CONSTRAINT) {
                                            n4 = n3;
                                            if (constraintWidget.mMatchConstraintDefaultHeight == 0) {
                                                n4 = n3;
                                                if (dimensionBehaviour != match_CONSTRAINT) {
                                                    n4 = n3;
                                                    if (!constraintWidget.isInHorizontalChain()) {
                                                        n4 = 1;
                                                    }
                                                }
                                            }
                                        }
                                        if (dimensionBehaviour != match_CONSTRAINT) {
                                            n2 = n4;
                                            if (dimensionBehaviour2 != match_CONSTRAINT) {
                                                break Label_0337;
                                            }
                                        }
                                        n2 = n4;
                                        if (constraintWidget.mDimensionRatio > 0.0f) {
                                            n2 = n;
                                        }
                                    }
                                }
                            }
                        }
                        if (n2 == 0) {
                            this.measure(measurer, constraintWidget, Measure.SELF_DIMENSIONS);
                            final Metrics mMetrics = constraintWidgetContainer.mMetrics;
                            if (mMetrics != null) {
                                ++mMetrics.measuredWidgets;
                            }
                        }
                    }
                }
            }
        }
        measurer.didMeasures();
    }
    
    private void solveLinearSystem(final ConstraintWidgetContainer constraintWidgetContainer, final String s, final int pass, final int width, final int height) {
        final int minWidth = constraintWidgetContainer.getMinWidth();
        final int minHeight = constraintWidgetContainer.getMinHeight();
        constraintWidgetContainer.setMinWidth(0);
        constraintWidgetContainer.setMinHeight(0);
        constraintWidgetContainer.setWidth(width);
        constraintWidgetContainer.setHeight(height);
        constraintWidgetContainer.setMinWidth(minWidth);
        constraintWidgetContainer.setMinHeight(minHeight);
        this.constraintWidgetContainer.setPass(pass);
        this.constraintWidgetContainer.layout();
    }
    
    public long solverMeasure(final ConstraintWidgetContainer constraintWidgetContainer, int a, int n, int optimizationLevel, int n2, int width, int i, int b, int n3, int size) {
        final Measurer measurer = constraintWidgetContainer.getMeasurer();
        size = constraintWidgetContainer.mChildren.size();
        final int width2 = constraintWidgetContainer.getWidth();
        final int height = constraintWidgetContainer.getHeight();
        final boolean enabled = Optimizer.enabled(a, 128);
        if (!enabled && !Optimizer.enabled(a, 64)) {
            a = 0;
        }
        else {
            a = 1;
        }
        optimizationLevel = a;
        Label_0238: {
            if (a != 0) {
                n = 0;
                while (true) {
                    optimizationLevel = a;
                    if (n >= size) {
                        break Label_0238;
                    }
                    final ConstraintWidget constraintWidget = constraintWidgetContainer.mChildren.get(n);
                    final ConstraintWidget.DimensionBehaviour horizontalDimensionBehaviour = constraintWidget.getHorizontalDimensionBehaviour();
                    final ConstraintWidget.DimensionBehaviour match_CONSTRAINT = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                    if (horizontalDimensionBehaviour == match_CONSTRAINT) {
                        optimizationLevel = 1;
                    }
                    else {
                        optimizationLevel = 0;
                    }
                    if (constraintWidget.getVerticalDimensionBehaviour() == match_CONSTRAINT) {
                        n3 = 1;
                    }
                    else {
                        n3 = 0;
                    }
                    if (optimizationLevel != 0 && n3 != 0 && constraintWidget.getDimensionRatio() > 0.0f) {
                        optimizationLevel = 1;
                    }
                    else {
                        optimizationLevel = 0;
                    }
                    if (constraintWidget.isInHorizontalChain() && optimizationLevel != 0) {
                        break;
                    }
                    if (constraintWidget.isInVerticalChain() && optimizationLevel != 0) {
                        break;
                    }
                    if (constraintWidget instanceof VirtualLayout) {
                        break;
                    }
                    if (constraintWidget.isInHorizontalChain()) {
                        break;
                    }
                    if (constraintWidget.isInVerticalChain()) {
                        break;
                    }
                    ++n;
                }
                optimizationLevel = 0;
            }
        }
        if (optimizationLevel != 0) {
            final Metrics sMetrics = LinearSystem.sMetrics;
            if (sMetrics != null) {
                ++sMetrics.measures;
            }
        }
        if ((n2 == 1073741824 && i == 1073741824) || enabled) {
            a = 1;
        }
        else {
            a = 0;
        }
        final int n4 = optimizationLevel & a;
        boolean b3;
        if (n4 != 0) {
            a = Math.min(constraintWidgetContainer.getMaxWidth(), width);
            n = Math.min(constraintWidgetContainer.getMaxHeight(), b);
            if (n2 == 1073741824 && constraintWidgetContainer.getWidth() != a) {
                constraintWidgetContainer.setWidth(a);
                constraintWidgetContainer.invalidateGraph();
            }
            if (i == 1073741824 && constraintWidgetContainer.getHeight() != n) {
                constraintWidgetContainer.setHeight(n);
                constraintWidgetContainer.invalidateGraph();
            }
            boolean b2;
            if (n2 == 1073741824 && i == 1073741824) {
                b2 = constraintWidgetContainer.directMeasure(enabled);
                a = 2;
            }
            else {
                b2 = constraintWidgetContainer.directMeasureSetup(enabled);
                if (n2 == 1073741824) {
                    b2 &= constraintWidgetContainer.directMeasureWithOrientation(enabled, 0);
                    a = 1;
                }
                else {
                    a = 0;
                }
                if (i == 1073741824) {
                    b2 &= constraintWidgetContainer.directMeasureWithOrientation(enabled, 1);
                    ++a;
                }
            }
            b3 = b2;
            n = a;
            if (b2) {
                constraintWidgetContainer.updateFromRuns(n2 == 1073741824, i == 1073741824);
                b3 = b2;
                n = a;
            }
        }
        else {
            b3 = false;
            n = 0;
        }
        if (!b3 || n != 2) {
            optimizationLevel = constraintWidgetContainer.getOptimizationLevel();
            if (size > 0) {
                this.measureChildren(constraintWidgetContainer);
            }
            this.updateHierarchy(constraintWidgetContainer);
            b = this.mVariableDimensionsWidgets.size();
            if (size > 0) {
                this.solveLinearSystem(constraintWidgetContainer, "First pass", 0, width2, height);
            }
            if (b > 0) {
                final ConstraintWidget.DimensionBehaviour horizontalDimensionBehaviour2 = constraintWidgetContainer.getHorizontalDimensionBehaviour();
                final ConstraintWidget.DimensionBehaviour wrap_CONTENT = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                if (horizontalDimensionBehaviour2 == wrap_CONTENT) {
                    n3 = 1;
                }
                else {
                    n3 = 0;
                }
                if (constraintWidgetContainer.getVerticalDimensionBehaviour() == wrap_CONTENT) {
                    size = 1;
                }
                else {
                    size = 0;
                }
                n = Math.max(constraintWidgetContainer.getWidth(), this.constraintWidgetContainer.getMinWidth());
                a = Math.max(constraintWidgetContainer.getHeight(), this.constraintWidgetContainer.getMinHeight());
                i = 0;
                n2 = 0;
                while (i < b) {
                    final ConstraintWidget constraintWidget2 = this.mVariableDimensionsWidgets.get(i);
                    if (!(constraintWidget2 instanceof VirtualLayout)) {
                        width = n2;
                    }
                    else {
                        width = constraintWidget2.getWidth();
                        final int height2 = constraintWidget2.getHeight();
                        final boolean measure = this.measure(measurer, constraintWidget2, Measure.TRY_GIVEN_DIMENSIONS);
                        final Metrics mMetrics = constraintWidgetContainer.mMetrics;
                        if (mMetrics != null) {
                            ++mMetrics.measuredMatchWidgets;
                        }
                        final int width3 = constraintWidget2.getWidth();
                        final int height3 = constraintWidget2.getHeight();
                        if (width3 != width) {
                            constraintWidget2.setWidth(width3);
                            n2 = n;
                            if (n3 != 0 && constraintWidget2.getRight() > (n2 = n)) {
                                n2 = Math.max(n, constraintWidget2.getRight() + constraintWidget2.getAnchor(ConstraintAnchor.Type.RIGHT).getMargin());
                            }
                            width = 1;
                            n = n2;
                        }
                        else {
                            width = ((measure ? 1 : 0) | n2);
                        }
                        n2 = a;
                        if (height3 != height2) {
                            constraintWidget2.setHeight(height3);
                            n2 = a;
                            if (size != 0 && constraintWidget2.getBottom() > (n2 = a)) {
                                n2 = Math.max(a, constraintWidget2.getBottom() + constraintWidget2.getAnchor(ConstraintAnchor.Type.BOTTOM).getMargin());
                            }
                            width = 1;
                        }
                        width |= (((VirtualLayout)constraintWidget2).needSolverPass() ? 1 : 0);
                        a = n2;
                    }
                    ++i;
                    n2 = width;
                }
                int j = 0;
                i = n2;
                n2 = b;
                width = n4;
                while (j < 2) {
                    int n5;
                    for (int k = 0; k < n2; ++k, n = n5, i = b) {
                        final ConstraintWidget constraintWidget3 = this.mVariableDimensionsWidgets.get(k);
                        if (!(constraintWidget3 instanceof Helper) || constraintWidget3 instanceof VirtualLayout) {
                            if (!(constraintWidget3 instanceof Guideline)) {
                                if (constraintWidget3.getVisibility() != 8) {
                                    if (width == 0 || !constraintWidget3.horizontalRun.dimension.resolved || !constraintWidget3.verticalRun.dimension.resolved) {
                                        if (!(constraintWidget3 instanceof VirtualLayout)) {
                                            final int width4 = constraintWidget3.getWidth();
                                            final int height4 = constraintWidget3.getHeight();
                                            final int baselineDistance = constraintWidget3.getBaselineDistance();
                                            b = Measure.TRY_GIVEN_DIMENSIONS;
                                            if (j == 1) {
                                                b = Measure.USE_GIVEN_DIMENSIONS;
                                            }
                                            b = ((this.measure(measurer, constraintWidget3, b) ? 1 : 0) | i);
                                            final Metrics mMetrics2 = constraintWidgetContainer.mMetrics;
                                            if (mMetrics2 != null) {
                                                ++mMetrics2.measuredMatchWidgets;
                                            }
                                            final int width5 = constraintWidget3.getWidth();
                                            final int height5 = constraintWidget3.getHeight();
                                            i = n;
                                            if (width5 != width4) {
                                                constraintWidget3.setWidth(width5);
                                                i = n;
                                                if (n3 != 0 && constraintWidget3.getRight() > (i = n)) {
                                                    i = Math.max(n, constraintWidget3.getRight() + constraintWidget3.getAnchor(ConstraintAnchor.Type.RIGHT).getMargin());
                                                }
                                                b = 1;
                                            }
                                            n = a;
                                            if (height5 != height4) {
                                                constraintWidget3.setHeight(height5);
                                                n = a;
                                                if (size != 0 && constraintWidget3.getBottom() > (n = a)) {
                                                    n = Math.max(a, constraintWidget3.getBottom() + constraintWidget3.getAnchor(ConstraintAnchor.Type.BOTTOM).getMargin());
                                                }
                                                b = 1;
                                            }
                                            if (constraintWidget3.hasBaseline() && baselineDistance != constraintWidget3.getBaselineDistance()) {
                                                b = 1;
                                                n5 = i;
                                                a = n;
                                                continue;
                                            }
                                            a = n;
                                            n5 = i;
                                            continue;
                                        }
                                    }
                                }
                            }
                        }
                        n5 = n;
                        b = i;
                    }
                    if (i == 0) {
                        break;
                    }
                    ++j;
                    this.solveLinearSystem(constraintWidgetContainer, "intermediate pass", j, width2, height);
                    i = 0;
                }
            }
            constraintWidgetContainer.setOptimizationLevel(optimizationLevel);
        }
        return 0L;
    }
    
    public void updateHierarchy(final ConstraintWidgetContainer constraintWidgetContainer) {
        this.mVariableDimensionsWidgets.clear();
        for (int size = constraintWidgetContainer.mChildren.size(), i = 0; i < size; ++i) {
            final ConstraintWidget e = constraintWidgetContainer.mChildren.get(i);
            final ConstraintWidget.DimensionBehaviour horizontalDimensionBehaviour = e.getHorizontalDimensionBehaviour();
            final ConstraintWidget.DimensionBehaviour match_CONSTRAINT = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
            if (horizontalDimensionBehaviour == match_CONSTRAINT || e.getVerticalDimensionBehaviour() == match_CONSTRAINT) {
                this.mVariableDimensionsWidgets.add(e);
            }
        }
        constraintWidgetContainer.invalidateGraph();
    }
    
    public static class Measure
    {
        public static int SELF_DIMENSIONS = 0;
        public static int TRY_GIVEN_DIMENSIONS = 1;
        public static int USE_GIVEN_DIMENSIONS = 2;
        public ConstraintWidget.DimensionBehaviour horizontalBehavior;
        public int horizontalDimension;
        public int measureStrategy;
        public int measuredBaseline;
        public boolean measuredHasBaseline;
        public int measuredHeight;
        public boolean measuredNeedsSolverPass;
        public int measuredWidth;
        public ConstraintWidget.DimensionBehaviour verticalBehavior;
        public int verticalDimension;
    }
    
    public interface Measurer
    {
        void didMeasures();
        
        void measure(final ConstraintWidget p0, final Measure p1);
    }
}

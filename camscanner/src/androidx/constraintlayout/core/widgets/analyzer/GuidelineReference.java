// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import androidx.constraintlayout.core.widgets.Guideline;
import androidx.constraintlayout.core.widgets.ConstraintWidget;

class GuidelineReference extends WidgetRun
{
    public GuidelineReference(final ConstraintWidget constraintWidget) {
        super(constraintWidget);
        constraintWidget.horizontalRun.clear();
        constraintWidget.verticalRun.clear();
        super.orientation = ((Guideline)constraintWidget).getOrientation();
    }
    
    private void addDependency(final DependencyNode dependencyNode) {
        super.start.dependencies.add(dependencyNode);
        dependencyNode.targets.add(super.start);
    }
    
    @Override
    void apply() {
        final Guideline guideline = (Guideline)super.widget;
        final int relativeBegin = guideline.getRelativeBegin();
        final int relativeEnd = guideline.getRelativeEnd();
        guideline.getRelativePercent();
        if (guideline.getOrientation() == 1) {
            if (relativeBegin != -1) {
                super.start.targets.add(super.widget.mParent.horizontalRun.start);
                super.widget.mParent.horizontalRun.start.dependencies.add(super.start);
                super.start.margin = relativeBegin;
            }
            else if (relativeEnd != -1) {
                super.start.targets.add(super.widget.mParent.horizontalRun.end);
                super.widget.mParent.horizontalRun.end.dependencies.add(super.start);
                super.start.margin = -relativeEnd;
            }
            else {
                final DependencyNode start = super.start;
                start.delegateToWidgetRun = true;
                start.targets.add(super.widget.mParent.horizontalRun.end);
                super.widget.mParent.horizontalRun.end.dependencies.add(super.start);
            }
            this.addDependency(super.widget.horizontalRun.start);
            this.addDependency(super.widget.horizontalRun.end);
        }
        else {
            if (relativeBegin != -1) {
                super.start.targets.add(super.widget.mParent.verticalRun.start);
                super.widget.mParent.verticalRun.start.dependencies.add(super.start);
                super.start.margin = relativeBegin;
            }
            else if (relativeEnd != -1) {
                super.start.targets.add(super.widget.mParent.verticalRun.end);
                super.widget.mParent.verticalRun.end.dependencies.add(super.start);
                super.start.margin = -relativeEnd;
            }
            else {
                final DependencyNode start2 = super.start;
                start2.delegateToWidgetRun = true;
                start2.targets.add(super.widget.mParent.verticalRun.end);
                super.widget.mParent.verticalRun.end.dependencies.add(super.start);
            }
            this.addDependency(super.widget.verticalRun.start);
            this.addDependency(super.widget.verticalRun.end);
        }
    }
    
    public void applyToWidget() {
        if (((Guideline)super.widget).getOrientation() == 1) {
            super.widget.setX(super.start.value);
        }
        else {
            super.widget.setY(super.start.value);
        }
    }
    
    @Override
    void clear() {
        super.start.clear();
    }
    
    @Override
    void reset() {
        super.start.resolved = false;
        super.end.resolved = false;
    }
    
    @Override
    boolean supportsWrapComputation() {
        return false;
    }
    
    @Override
    public void update(final Dependency dependency) {
        final DependencyNode start = super.start;
        if (!start.readyToSolve) {
            return;
        }
        if (start.resolved) {
            return;
        }
        super.start.resolve((int)(start.targets.get(0).value * ((Guideline)super.widget).getRelativePercent() + 0.5f));
    }
}

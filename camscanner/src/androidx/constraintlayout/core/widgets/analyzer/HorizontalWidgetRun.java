// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.Helper;
import androidx.constraintlayout.core.widgets.ConstraintWidget;

public class HorizontalWidgetRun extends WidgetRun
{
    private static int[] tempDimensions;
    
    static {
        HorizontalWidgetRun.tempDimensions = new int[2];
    }
    
    public HorizontalWidgetRun(final ConstraintWidget constraintWidget) {
        super(constraintWidget);
        super.start.type = DependencyNode.Type.LEFT;
        super.end.type = DependencyNode.Type.RIGHT;
        super.orientation = 0;
    }
    
    private void computeInsetRatio(final int[] array, int n, int n2, int n3, int n4, final float n5, final int n6) {
        n = n2 - n;
        n2 = n4 - n3;
        if (n6 != -1) {
            if (n6 != 0) {
                if (n6 == 1) {
                    n2 = (int)(n * n5 + 0.5f);
                    array[0] = n;
                    array[1] = n2;
                }
            }
            else {
                array[0] = (int)(n2 * n5 + 0.5f);
                array[1] = n2;
            }
        }
        else {
            n4 = (int)(n2 * n5 + 0.5f);
            n3 = (int)(n / n5 + 0.5f);
            if (n4 <= n) {
                array[0] = n4;
                array[1] = n2;
            }
            else if (n3 <= n2) {
                array[0] = n;
                array[1] = n3;
            }
        }
    }
    
    @Override
    void apply() {
        final ConstraintWidget widget = super.widget;
        if (widget.measured) {
            super.dimension.resolve(widget.getWidth());
        }
        if (!super.dimension.resolved) {
            final ConstraintWidget.DimensionBehaviour horizontalDimensionBehaviour = super.widget.getHorizontalDimensionBehaviour();
            if ((super.dimensionBehavior = horizontalDimensionBehaviour) != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                final ConstraintWidget.DimensionBehaviour match_PARENT = ConstraintWidget.DimensionBehaviour.MATCH_PARENT;
                if (horizontalDimensionBehaviour == match_PARENT) {
                    final ConstraintWidget parent = super.widget.getParent();
                    if (parent != null && (parent.getHorizontalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.FIXED || parent.getHorizontalDimensionBehaviour() == match_PARENT)) {
                        final int width = parent.getWidth();
                        final int margin = super.widget.mLeft.getMargin();
                        final int margin2 = super.widget.mRight.getMargin();
                        this.addTarget(super.start, parent.horizontalRun.start, super.widget.mLeft.getMargin());
                        this.addTarget(super.end, parent.horizontalRun.end, -super.widget.mRight.getMargin());
                        super.dimension.resolve(width - margin - margin2);
                        return;
                    }
                }
                if (super.dimensionBehavior == ConstraintWidget.DimensionBehaviour.FIXED) {
                    super.dimension.resolve(super.widget.getWidth());
                }
            }
        }
        else {
            final ConstraintWidget.DimensionBehaviour dimensionBehavior = super.dimensionBehavior;
            final ConstraintWidget.DimensionBehaviour match_PARENT2 = ConstraintWidget.DimensionBehaviour.MATCH_PARENT;
            if (dimensionBehavior == match_PARENT2) {
                final ConstraintWidget parent2 = super.widget.getParent();
                if (parent2 != null && (parent2.getHorizontalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.FIXED || parent2.getHorizontalDimensionBehaviour() == match_PARENT2)) {
                    this.addTarget(super.start, parent2.horizontalRun.start, super.widget.mLeft.getMargin());
                    this.addTarget(super.end, parent2.horizontalRun.end, -super.widget.mRight.getMargin());
                    return;
                }
            }
        }
        final DimensionDependency dimension = super.dimension;
        if (dimension.resolved) {
            final ConstraintWidget widget2 = super.widget;
            if (widget2.measured) {
                final ConstraintAnchor[] mListAnchors = widget2.mListAnchors;
                final ConstraintAnchor constraintAnchor = mListAnchors[0];
                final ConstraintAnchor mTarget = constraintAnchor.mTarget;
                if (mTarget != null && mListAnchors[1].mTarget != null) {
                    if (widget2.isInHorizontalChain()) {
                        super.start.margin = super.widget.mListAnchors[0].getMargin();
                        super.end.margin = -super.widget.mListAnchors[1].getMargin();
                        return;
                    }
                    final DependencyNode target = this.getTarget(super.widget.mListAnchors[0]);
                    if (target != null) {
                        this.addTarget(super.start, target, super.widget.mListAnchors[0].getMargin());
                    }
                    final DependencyNode target2 = this.getTarget(super.widget.mListAnchors[1]);
                    if (target2 != null) {
                        this.addTarget(super.end, target2, -super.widget.mListAnchors[1].getMargin());
                    }
                    super.start.delegateToWidgetRun = true;
                    super.end.delegateToWidgetRun = true;
                    return;
                }
                else if (mTarget != null) {
                    final DependencyNode target3 = this.getTarget(constraintAnchor);
                    if (target3 != null) {
                        this.addTarget(super.start, target3, super.widget.mListAnchors[0].getMargin());
                        this.addTarget(super.end, super.start, super.dimension.value);
                    }
                    return;
                }
                else {
                    final ConstraintAnchor constraintAnchor2 = mListAnchors[1];
                    if (constraintAnchor2.mTarget != null) {
                        final DependencyNode target4 = this.getTarget(constraintAnchor2);
                        if (target4 != null) {
                            this.addTarget(super.end, target4, -super.widget.mListAnchors[1].getMargin());
                            this.addTarget(super.start, super.end, -super.dimension.value);
                        }
                        return;
                    }
                    else {
                        if (!(widget2 instanceof Helper) && widget2.getParent() != null && super.widget.getAnchor(ConstraintAnchor.Type.CENTER).mTarget == null) {
                            this.addTarget(super.start, super.widget.getParent().horizontalRun.start, super.widget.getX());
                            this.addTarget(super.end, super.start, super.dimension.value);
                        }
                        return;
                    }
                }
            }
        }
        if (super.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            final ConstraintWidget widget3 = super.widget;
            final int mMatchConstraintDefaultWidth = widget3.mMatchConstraintDefaultWidth;
            if (mMatchConstraintDefaultWidth != 2) {
                if (mMatchConstraintDefaultWidth == 3) {
                    if (widget3.mMatchConstraintDefaultHeight == 3) {
                        super.start.updateDelegate = this;
                        super.end.updateDelegate = this;
                        final VerticalWidgetRun verticalRun = widget3.verticalRun;
                        verticalRun.start.updateDelegate = this;
                        verticalRun.end.updateDelegate = this;
                        dimension.updateDelegate = this;
                        if (widget3.isInVerticalChain()) {
                            super.dimension.targets.add(super.widget.verticalRun.dimension);
                            super.widget.verticalRun.dimension.dependencies.add(super.dimension);
                            final VerticalWidgetRun verticalRun2 = super.widget.verticalRun;
                            verticalRun2.dimension.updateDelegate = this;
                            super.dimension.targets.add(verticalRun2.start);
                            super.dimension.targets.add(super.widget.verticalRun.end);
                            super.widget.verticalRun.start.dependencies.add(super.dimension);
                            super.widget.verticalRun.end.dependencies.add(super.dimension);
                        }
                        else if (super.widget.isInHorizontalChain()) {
                            super.widget.verticalRun.dimension.targets.add(super.dimension);
                            super.dimension.dependencies.add(super.widget.verticalRun.dimension);
                        }
                        else {
                            super.widget.verticalRun.dimension.targets.add(super.dimension);
                        }
                    }
                    else {
                        final DimensionDependency dimension2 = widget3.verticalRun.dimension;
                        dimension.targets.add(dimension2);
                        dimension2.dependencies.add(super.dimension);
                        super.widget.verticalRun.start.dependencies.add(super.dimension);
                        super.widget.verticalRun.end.dependencies.add(super.dimension);
                        final DimensionDependency dimension3 = super.dimension;
                        dimension3.delegateToWidgetRun = true;
                        dimension3.dependencies.add(super.start);
                        super.dimension.dependencies.add(super.end);
                        super.start.targets.add(super.dimension);
                        super.end.targets.add(super.dimension);
                    }
                }
            }
            else {
                final ConstraintWidget parent3 = widget3.getParent();
                if (parent3 != null) {
                    final DimensionDependency dimension4 = parent3.verticalRun.dimension;
                    super.dimension.targets.add(dimension4);
                    dimension4.dependencies.add(super.dimension);
                    final DimensionDependency dimension5 = super.dimension;
                    dimension5.delegateToWidgetRun = true;
                    dimension5.dependencies.add(super.start);
                    super.dimension.dependencies.add(super.end);
                }
            }
        }
        final ConstraintWidget widget4 = super.widget;
        final ConstraintAnchor[] mListAnchors2 = widget4.mListAnchors;
        final ConstraintAnchor constraintAnchor3 = mListAnchors2[0];
        final ConstraintAnchor mTarget2 = constraintAnchor3.mTarget;
        if (mTarget2 != null && mListAnchors2[1].mTarget != null) {
            if (widget4.isInHorizontalChain()) {
                super.start.margin = super.widget.mListAnchors[0].getMargin();
                super.end.margin = -super.widget.mListAnchors[1].getMargin();
            }
            else {
                final DependencyNode target5 = this.getTarget(super.widget.mListAnchors[0]);
                final DependencyNode target6 = this.getTarget(super.widget.mListAnchors[1]);
                if (target5 != null) {
                    target5.addDependency(this);
                }
                if (target6 != null) {
                    target6.addDependency(this);
                }
                super.mRunType = RunType.CENTER;
            }
        }
        else if (mTarget2 != null) {
            final DependencyNode target7 = this.getTarget(constraintAnchor3);
            if (target7 != null) {
                this.addTarget(super.start, target7, super.widget.mListAnchors[0].getMargin());
                this.addTarget(super.end, super.start, 1, super.dimension);
            }
        }
        else {
            final ConstraintAnchor constraintAnchor4 = mListAnchors2[1];
            if (constraintAnchor4.mTarget != null) {
                final DependencyNode target8 = this.getTarget(constraintAnchor4);
                if (target8 != null) {
                    this.addTarget(super.end, target8, -super.widget.mListAnchors[1].getMargin());
                    this.addTarget(super.start, super.end, -1, super.dimension);
                }
            }
            else if (!(widget4 instanceof Helper) && widget4.getParent() != null) {
                this.addTarget(super.start, super.widget.getParent().horizontalRun.start, super.widget.getX());
                this.addTarget(super.end, super.start, 1, super.dimension);
            }
        }
    }
    
    public void applyToWidget() {
        final DependencyNode start = super.start;
        if (start.resolved) {
            super.widget.setX(start.value);
        }
    }
    
    @Override
    void clear() {
        super.runGroup = null;
        super.start.clear();
        super.end.clear();
        super.dimension.clear();
        super.resolved = false;
    }
    
    @Override
    void reset() {
        super.resolved = false;
        super.start.clear();
        super.start.resolved = false;
        super.end.clear();
        super.end.resolved = false;
        super.dimension.resolved = false;
    }
    
    @Override
    boolean supportsWrapComputation() {
        return super.dimensionBehavior != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT || super.widget.mMatchConstraintDefaultWidth == 0;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("HorizontalRun ");
        sb.append(super.widget.getDebugName());
        return sb.toString();
    }
    
    @Override
    public void update(final Dependency dependency) {
        final int n = HorizontalWidgetRun$1.$SwitchMap$androidx$constraintlayout$core$widgets$analyzer$WidgetRun$RunType[super.mRunType.ordinal()];
        if (n != 1) {
            if (n != 2) {
                if (n == 3) {
                    final ConstraintWidget widget = super.widget;
                    this.updateRunCenter(dependency, widget.mLeft, widget.mRight, 0);
                    return;
                }
            }
            else {
                this.updateRunEnd(dependency);
            }
        }
        else {
            this.updateRunStart(dependency);
        }
        Label_1576: {
            if (!super.dimension.resolved && super.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                final ConstraintWidget widget2 = super.widget;
                final int mMatchConstraintDefaultWidth = widget2.mMatchConstraintDefaultWidth;
                if (mMatchConstraintDefaultWidth != 2) {
                    if (mMatchConstraintDefaultWidth == 3) {
                        final int mMatchConstraintDefaultHeight = widget2.mMatchConstraintDefaultHeight;
                        if (mMatchConstraintDefaultHeight != 0 && mMatchConstraintDefaultHeight != 3) {
                            final int dimensionRatioSide = widget2.getDimensionRatioSide();
                            int n3 = 0;
                            Label_0252: {
                                float n2 = 0.0f;
                                Label_0245: {
                                    float n4;
                                    float n5;
                                    if (dimensionRatioSide != -1) {
                                        if (dimensionRatioSide == 0) {
                                            final ConstraintWidget widget3 = super.widget;
                                            n2 = widget3.verticalRun.dimension.value / widget3.getDimensionRatio();
                                            break Label_0245;
                                        }
                                        if (dimensionRatioSide != 1) {
                                            n3 = 0;
                                            break Label_0252;
                                        }
                                        final ConstraintWidget widget4 = super.widget;
                                        n4 = (float)widget4.verticalRun.dimension.value;
                                        n5 = widget4.getDimensionRatio();
                                    }
                                    else {
                                        final ConstraintWidget widget5 = super.widget;
                                        n4 = (float)widget5.verticalRun.dimension.value;
                                        n5 = widget5.getDimensionRatio();
                                    }
                                    n2 = n4 * n5;
                                }
                                n3 = (int)(n2 + 0.5f);
                            }
                            super.dimension.resolve(n3);
                        }
                        else {
                            final VerticalWidgetRun verticalRun = widget2.verticalRun;
                            final DependencyNode start = verticalRun.start;
                            final DependencyNode end = verticalRun.end;
                            final boolean b = widget2.mLeft.mTarget != null;
                            final boolean b2 = widget2.mTop.mTarget != null;
                            final boolean b3 = widget2.mRight.mTarget != null;
                            final boolean b4 = widget2.mBottom.mTarget != null;
                            final int dimensionRatioSide2 = widget2.getDimensionRatioSide();
                            if (b && b2 && b3 && b4) {
                                final float dimensionRatio = super.widget.getDimensionRatio();
                                if (start.resolved && end.resolved) {
                                    final DependencyNode start2 = super.start;
                                    if (start2.readyToSolve) {
                                        if (super.end.readyToSolve) {
                                            this.computeInsetRatio(HorizontalWidgetRun.tempDimensions, start2.targets.get(0).value + super.start.margin, super.end.targets.get(0).value - super.end.margin, start.value + start.margin, end.value - end.margin, dimensionRatio, dimensionRatioSide2);
                                            super.dimension.resolve(HorizontalWidgetRun.tempDimensions[0]);
                                            super.widget.verticalRun.dimension.resolve(HorizontalWidgetRun.tempDimensions[1]);
                                        }
                                    }
                                    return;
                                }
                                final DependencyNode start3 = super.start;
                                if (start3.resolved) {
                                    final DependencyNode end2 = super.end;
                                    if (end2.resolved) {
                                        if (!start.readyToSolve || !end.readyToSolve) {
                                            return;
                                        }
                                        this.computeInsetRatio(HorizontalWidgetRun.tempDimensions, start3.value + start3.margin, end2.value - end2.margin, start.targets.get(0).value + start.margin, end.targets.get(0).value - end.margin, dimensionRatio, dimensionRatioSide2);
                                        super.dimension.resolve(HorizontalWidgetRun.tempDimensions[0]);
                                        super.widget.verticalRun.dimension.resolve(HorizontalWidgetRun.tempDimensions[1]);
                                    }
                                }
                                final DependencyNode start4 = super.start;
                                if (!start4.readyToSolve || !super.end.readyToSolve || !start.readyToSolve || !end.readyToSolve) {
                                    return;
                                }
                                this.computeInsetRatio(HorizontalWidgetRun.tempDimensions, start4.targets.get(0).value + super.start.margin, super.end.targets.get(0).value - super.end.margin, start.targets.get(0).value + start.margin, end.targets.get(0).value - end.margin, dimensionRatio, dimensionRatioSide2);
                                super.dimension.resolve(HorizontalWidgetRun.tempDimensions[0]);
                                super.widget.verticalRun.dimension.resolve(HorizontalWidgetRun.tempDimensions[1]);
                            }
                            else if (b && b3) {
                                if (!super.start.readyToSolve || !super.end.readyToSolve) {
                                    return;
                                }
                                final float dimensionRatio2 = super.widget.getDimensionRatio();
                                final int n6 = super.start.targets.get(0).value + super.start.margin;
                                final int n7 = super.end.targets.get(0).value - super.end.margin;
                                if (dimensionRatioSide2 != -1 && dimensionRatioSide2 != 0) {
                                    if (dimensionRatioSide2 == 1) {
                                        int limitedDimension = this.getLimitedDimension(n7 - n6, 0);
                                        final int n8 = (int)(limitedDimension / dimensionRatio2 + 0.5f);
                                        final int limitedDimension2 = this.getLimitedDimension(n8, 1);
                                        if (n8 != limitedDimension2) {
                                            limitedDimension = (int)(limitedDimension2 * dimensionRatio2 + 0.5f);
                                        }
                                        super.dimension.resolve(limitedDimension);
                                        super.widget.verticalRun.dimension.resolve(limitedDimension2);
                                    }
                                }
                                else {
                                    int limitedDimension3 = this.getLimitedDimension(n7 - n6, 0);
                                    final int n9 = (int)(limitedDimension3 * dimensionRatio2 + 0.5f);
                                    final int limitedDimension4 = this.getLimitedDimension(n9, 1);
                                    if (n9 != limitedDimension4) {
                                        limitedDimension3 = (int)(limitedDimension4 / dimensionRatio2 + 0.5f);
                                    }
                                    super.dimension.resolve(limitedDimension3);
                                    super.widget.verticalRun.dimension.resolve(limitedDimension4);
                                }
                            }
                            else if (b2 && b4) {
                                if (!start.readyToSolve || !end.readyToSolve) {
                                    return;
                                }
                                final float dimensionRatio3 = super.widget.getDimensionRatio();
                                final int n10 = start.targets.get(0).value + start.margin;
                                final int n11 = end.targets.get(0).value - end.margin;
                                if (dimensionRatioSide2 != -1) {
                                    if (dimensionRatioSide2 == 0) {
                                        int limitedDimension5 = this.getLimitedDimension(n11 - n10, 1);
                                        final int n12 = (int)(limitedDimension5 * dimensionRatio3 + 0.5f);
                                        final int limitedDimension6 = this.getLimitedDimension(n12, 0);
                                        if (n12 != limitedDimension6) {
                                            limitedDimension5 = (int)(limitedDimension6 / dimensionRatio3 + 0.5f);
                                        }
                                        super.dimension.resolve(limitedDimension6);
                                        super.widget.verticalRun.dimension.resolve(limitedDimension5);
                                        break Label_1576;
                                    }
                                    if (dimensionRatioSide2 != 1) {
                                        break Label_1576;
                                    }
                                }
                                int limitedDimension7 = this.getLimitedDimension(n11 - n10, 1);
                                final int n13 = (int)(limitedDimension7 / dimensionRatio3 + 0.5f);
                                final int limitedDimension8 = this.getLimitedDimension(n13, 0);
                                if (n13 != limitedDimension8) {
                                    limitedDimension7 = (int)(limitedDimension8 * dimensionRatio3 + 0.5f);
                                }
                                super.dimension.resolve(limitedDimension8);
                                super.widget.verticalRun.dimension.resolve(limitedDimension7);
                            }
                        }
                    }
                }
                else {
                    final ConstraintWidget parent = widget2.getParent();
                    if (parent != null) {
                        final DimensionDependency dimension = parent.horizontalRun.dimension;
                        if (dimension.resolved) {
                            super.dimension.resolve((int)(dimension.value * super.widget.mMatchConstraintPercentWidth + 0.5f));
                        }
                    }
                }
            }
        }
        final DependencyNode start5 = super.start;
        if (start5.readyToSolve) {
            final DependencyNode end3 = super.end;
            if (end3.readyToSolve) {
                if (start5.resolved && end3.resolved && super.dimension.resolved) {
                    return;
                }
                if (!super.dimension.resolved && super.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    final ConstraintWidget widget6 = super.widget;
                    if (widget6.mMatchConstraintDefaultWidth == 0 && !widget6.isInHorizontalChain()) {
                        final DependencyNode dependencyNode = super.start.targets.get(0);
                        final DependencyNode dependencyNode2 = super.end.targets.get(0);
                        final int value = dependencyNode.value;
                        final DependencyNode start6 = super.start;
                        final int n14 = value + start6.margin;
                        final int n15 = dependencyNode2.value + super.end.margin;
                        start6.resolve(n14);
                        super.end.resolve(n15);
                        super.dimension.resolve(n15 - n14);
                        return;
                    }
                }
                if (!super.dimension.resolved && super.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && super.matchConstraintsType == 1 && super.start.targets.size() > 0 && super.end.targets.size() > 0) {
                    final int min = Math.min(super.end.targets.get(0).value + super.end.margin - (super.start.targets.get(0).value + super.start.margin), super.dimension.wrapValue);
                    final ConstraintWidget widget7 = super.widget;
                    final int mMatchConstraintMaxWidth = widget7.mMatchConstraintMaxWidth;
                    int b5 = Math.max(widget7.mMatchConstraintMinWidth, min);
                    if (mMatchConstraintMaxWidth > 0) {
                        b5 = Math.min(mMatchConstraintMaxWidth, b5);
                    }
                    super.dimension.resolve(b5);
                }
                if (!super.dimension.resolved) {
                    return;
                }
                final DependencyNode dependencyNode3 = super.start.targets.get(0);
                final DependencyNode dependencyNode4 = super.end.targets.get(0);
                int value2 = dependencyNode3.value + super.start.margin;
                int value3 = dependencyNode4.value + super.end.margin;
                float horizontalBiasPercent = super.widget.getHorizontalBiasPercent();
                if (dependencyNode3 == dependencyNode4) {
                    value2 = dependencyNode3.value;
                    value3 = dependencyNode4.value;
                    horizontalBiasPercent = 0.5f;
                }
                super.start.resolve((int)(value2 + 0.5f + (value3 - value2 - super.dimension.value) * horizontalBiasPercent));
                super.end.resolve(super.start.value + super.dimension.value);
            }
        }
    }
}

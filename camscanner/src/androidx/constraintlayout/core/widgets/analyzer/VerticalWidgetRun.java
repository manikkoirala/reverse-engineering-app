// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets.analyzer;

import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.Helper;
import androidx.constraintlayout.core.widgets.ConstraintWidget;

public class VerticalWidgetRun extends WidgetRun
{
    public DependencyNode baseline;
    DimensionDependency baselineDimension;
    
    public VerticalWidgetRun(final ConstraintWidget constraintWidget) {
        super(constraintWidget);
        final DependencyNode baseline = new DependencyNode(this);
        this.baseline = baseline;
        this.baselineDimension = null;
        super.start.type = DependencyNode.Type.TOP;
        super.end.type = DependencyNode.Type.BOTTOM;
        baseline.type = DependencyNode.Type.BASELINE;
        super.orientation = 1;
    }
    
    @Override
    void apply() {
        final ConstraintWidget widget = super.widget;
        if (widget.measured) {
            super.dimension.resolve(widget.getHeight());
        }
        if (!super.dimension.resolved) {
            super.dimensionBehavior = super.widget.getVerticalDimensionBehaviour();
            if (super.widget.hasBaseline()) {
                this.baselineDimension = new BaselineDimensionDependency(this);
            }
            final ConstraintWidget.DimensionBehaviour dimensionBehavior = super.dimensionBehavior;
            if (dimensionBehavior != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                if (dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
                    final ConstraintWidget parent = super.widget.getParent();
                    if (parent != null && parent.getVerticalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.FIXED) {
                        final int height = parent.getHeight();
                        final int margin = super.widget.mTop.getMargin();
                        final int margin2 = super.widget.mBottom.getMargin();
                        this.addTarget(super.start, parent.verticalRun.start, super.widget.mTop.getMargin());
                        this.addTarget(super.end, parent.verticalRun.end, -super.widget.mBottom.getMargin());
                        super.dimension.resolve(height - margin - margin2);
                        return;
                    }
                }
                if (super.dimensionBehavior == ConstraintWidget.DimensionBehaviour.FIXED) {
                    super.dimension.resolve(super.widget.getHeight());
                }
            }
        }
        else if (super.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
            final ConstraintWidget parent2 = super.widget.getParent();
            if (parent2 != null && parent2.getVerticalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.FIXED) {
                this.addTarget(super.start, parent2.verticalRun.start, super.widget.mTop.getMargin());
                this.addTarget(super.end, parent2.verticalRun.end, -super.widget.mBottom.getMargin());
                return;
            }
        }
        final DimensionDependency dimension = super.dimension;
        final boolean resolved = dimension.resolved;
        if (resolved) {
            final ConstraintWidget widget2 = super.widget;
            if (widget2.measured) {
                final ConstraintAnchor[] mListAnchors = widget2.mListAnchors;
                final ConstraintAnchor constraintAnchor = mListAnchors[2];
                final ConstraintAnchor mTarget = constraintAnchor.mTarget;
                if (mTarget != null && mListAnchors[3].mTarget != null) {
                    if (widget2.isInVerticalChain()) {
                        super.start.margin = super.widget.mListAnchors[2].getMargin();
                        super.end.margin = -super.widget.mListAnchors[3].getMargin();
                    }
                    else {
                        final DependencyNode target = this.getTarget(super.widget.mListAnchors[2]);
                        if (target != null) {
                            this.addTarget(super.start, target, super.widget.mListAnchors[2].getMargin());
                        }
                        final DependencyNode target2 = this.getTarget(super.widget.mListAnchors[3]);
                        if (target2 != null) {
                            this.addTarget(super.end, target2, -super.widget.mListAnchors[3].getMargin());
                        }
                        super.start.delegateToWidgetRun = true;
                        super.end.delegateToWidgetRun = true;
                    }
                    if (super.widget.hasBaseline()) {
                        this.addTarget(this.baseline, super.start, super.widget.getBaselineDistance());
                    }
                    return;
                }
                else if (mTarget != null) {
                    final DependencyNode target3 = this.getTarget(constraintAnchor);
                    if (target3 == null) {
                        return;
                    }
                    this.addTarget(super.start, target3, super.widget.mListAnchors[2].getMargin());
                    this.addTarget(super.end, super.start, super.dimension.value);
                    if (super.widget.hasBaseline()) {
                        this.addTarget(this.baseline, super.start, super.widget.getBaselineDistance());
                    }
                    return;
                }
                else {
                    final ConstraintAnchor constraintAnchor2 = mListAnchors[3];
                    if (constraintAnchor2.mTarget != null) {
                        final DependencyNode target4 = this.getTarget(constraintAnchor2);
                        if (target4 != null) {
                            this.addTarget(super.end, target4, -super.widget.mListAnchors[3].getMargin());
                            this.addTarget(super.start, super.end, -super.dimension.value);
                        }
                        if (super.widget.hasBaseline()) {
                            this.addTarget(this.baseline, super.start, super.widget.getBaselineDistance());
                        }
                        return;
                    }
                    else {
                        final ConstraintAnchor constraintAnchor3 = mListAnchors[4];
                        if (constraintAnchor3.mTarget != null) {
                            final DependencyNode target5 = this.getTarget(constraintAnchor3);
                            if (target5 != null) {
                                this.addTarget(this.baseline, target5, 0);
                                this.addTarget(super.start, this.baseline, -super.widget.getBaselineDistance());
                                this.addTarget(super.end, super.start, super.dimension.value);
                            }
                            return;
                        }
                        else {
                            if (widget2 instanceof Helper || widget2.getParent() == null || super.widget.getAnchor(ConstraintAnchor.Type.CENTER).mTarget != null) {
                                return;
                            }
                            this.addTarget(super.start, super.widget.getParent().verticalRun.start, super.widget.getY());
                            this.addTarget(super.end, super.start, super.dimension.value);
                            if (super.widget.hasBaseline()) {
                                this.addTarget(this.baseline, super.start, super.widget.getBaselineDistance());
                            }
                            return;
                        }
                    }
                }
            }
        }
        if (!resolved && super.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            final ConstraintWidget widget3 = super.widget;
            final int mMatchConstraintDefaultHeight = widget3.mMatchConstraintDefaultHeight;
            if (mMatchConstraintDefaultHeight != 2) {
                if (mMatchConstraintDefaultHeight == 3) {
                    if (!widget3.isInVerticalChain()) {
                        final ConstraintWidget widget4 = super.widget;
                        if (widget4.mMatchConstraintDefaultWidth != 3) {
                            final DimensionDependency dimension2 = widget4.horizontalRun.dimension;
                            super.dimension.targets.add(dimension2);
                            dimension2.dependencies.add(super.dimension);
                            final DimensionDependency dimension3 = super.dimension;
                            dimension3.delegateToWidgetRun = true;
                            dimension3.dependencies.add(super.start);
                            super.dimension.dependencies.add(super.end);
                        }
                    }
                }
            }
            else {
                final ConstraintWidget parent3 = widget3.getParent();
                if (parent3 != null) {
                    final DimensionDependency dimension4 = parent3.verticalRun.dimension;
                    super.dimension.targets.add(dimension4);
                    dimension4.dependencies.add(super.dimension);
                    final DimensionDependency dimension5 = super.dimension;
                    dimension5.delegateToWidgetRun = true;
                    dimension5.dependencies.add(super.start);
                    super.dimension.dependencies.add(super.end);
                }
            }
        }
        else {
            dimension.addDependency(this);
        }
        final ConstraintWidget widget5 = super.widget;
        final ConstraintAnchor[] mListAnchors2 = widget5.mListAnchors;
        final ConstraintAnchor constraintAnchor4 = mListAnchors2[2];
        final ConstraintAnchor mTarget2 = constraintAnchor4.mTarget;
        if (mTarget2 != null && mListAnchors2[3].mTarget != null) {
            if (widget5.isInVerticalChain()) {
                super.start.margin = super.widget.mListAnchors[2].getMargin();
                super.end.margin = -super.widget.mListAnchors[3].getMargin();
            }
            else {
                final DependencyNode target6 = this.getTarget(super.widget.mListAnchors[2]);
                final DependencyNode target7 = this.getTarget(super.widget.mListAnchors[3]);
                if (target6 != null) {
                    target6.addDependency(this);
                }
                if (target7 != null) {
                    target7.addDependency(this);
                }
                super.mRunType = RunType.CENTER;
            }
            if (super.widget.hasBaseline()) {
                this.addTarget(this.baseline, super.start, 1, this.baselineDimension);
            }
        }
        else if (mTarget2 != null) {
            final DependencyNode target8 = this.getTarget(constraintAnchor4);
            if (target8 != null) {
                this.addTarget(super.start, target8, super.widget.mListAnchors[2].getMargin());
                this.addTarget(super.end, super.start, 1, super.dimension);
                if (super.widget.hasBaseline()) {
                    this.addTarget(this.baseline, super.start, 1, this.baselineDimension);
                }
                final ConstraintWidget.DimensionBehaviour dimensionBehavior2 = super.dimensionBehavior;
                final ConstraintWidget.DimensionBehaviour match_CONSTRAINT = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                if (dimensionBehavior2 == match_CONSTRAINT && super.widget.getDimensionRatio() > 0.0f) {
                    final HorizontalWidgetRun horizontalRun = super.widget.horizontalRun;
                    if (horizontalRun.dimensionBehavior == match_CONSTRAINT) {
                        horizontalRun.dimension.dependencies.add(super.dimension);
                        super.dimension.targets.add(super.widget.horizontalRun.dimension);
                        super.dimension.updateDelegate = this;
                    }
                }
            }
        }
        else {
            final ConstraintAnchor constraintAnchor5 = mListAnchors2[3];
            if (constraintAnchor5.mTarget != null) {
                final DependencyNode target9 = this.getTarget(constraintAnchor5);
                if (target9 != null) {
                    this.addTarget(super.end, target9, -super.widget.mListAnchors[3].getMargin());
                    this.addTarget(super.start, super.end, -1, super.dimension);
                    if (super.widget.hasBaseline()) {
                        this.addTarget(this.baseline, super.start, 1, this.baselineDimension);
                    }
                }
            }
            else {
                final ConstraintAnchor constraintAnchor6 = mListAnchors2[4];
                if (constraintAnchor6.mTarget != null) {
                    final DependencyNode target10 = this.getTarget(constraintAnchor6);
                    if (target10 != null) {
                        this.addTarget(this.baseline, target10, 0);
                        this.addTarget(super.start, this.baseline, -1, this.baselineDimension);
                        this.addTarget(super.end, super.start, 1, super.dimension);
                    }
                }
                else if (!(widget5 instanceof Helper) && widget5.getParent() != null) {
                    this.addTarget(super.start, super.widget.getParent().verticalRun.start, super.widget.getY());
                    this.addTarget(super.end, super.start, 1, super.dimension);
                    if (super.widget.hasBaseline()) {
                        this.addTarget(this.baseline, super.start, 1, this.baselineDimension);
                    }
                    final ConstraintWidget.DimensionBehaviour dimensionBehavior3 = super.dimensionBehavior;
                    final ConstraintWidget.DimensionBehaviour match_CONSTRAINT2 = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                    if (dimensionBehavior3 == match_CONSTRAINT2 && super.widget.getDimensionRatio() > 0.0f) {
                        final HorizontalWidgetRun horizontalRun2 = super.widget.horizontalRun;
                        if (horizontalRun2.dimensionBehavior == match_CONSTRAINT2) {
                            horizontalRun2.dimension.dependencies.add(super.dimension);
                            super.dimension.targets.add(super.widget.horizontalRun.dimension);
                            super.dimension.updateDelegate = this;
                        }
                    }
                }
            }
        }
        if (super.dimension.targets.size() == 0) {
            super.dimension.readyToSolve = true;
        }
    }
    
    public void applyToWidget() {
        final DependencyNode start = super.start;
        if (start.resolved) {
            super.widget.setY(start.value);
        }
    }
    
    @Override
    void clear() {
        super.runGroup = null;
        super.start.clear();
        super.end.clear();
        this.baseline.clear();
        super.dimension.clear();
        super.resolved = false;
    }
    
    @Override
    void reset() {
        super.resolved = false;
        super.start.clear();
        super.start.resolved = false;
        super.end.clear();
        super.end.resolved = false;
        this.baseline.clear();
        this.baseline.resolved = false;
        super.dimension.resolved = false;
    }
    
    @Override
    boolean supportsWrapComputation() {
        return super.dimensionBehavior != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT || super.widget.mMatchConstraintDefaultHeight == 0;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("VerticalRun ");
        sb.append(super.widget.getDebugName());
        return sb.toString();
    }
    
    @Override
    public void update(final Dependency dependency) {
        final int n = VerticalWidgetRun$1.$SwitchMap$androidx$constraintlayout$core$widgets$analyzer$WidgetRun$RunType[super.mRunType.ordinal()];
        if (n != 1) {
            if (n != 2) {
                if (n == 3) {
                    final ConstraintWidget widget = super.widget;
                    this.updateRunCenter(dependency, widget.mTop, widget.mBottom, 1);
                    return;
                }
            }
            else {
                this.updateRunEnd(dependency);
            }
        }
        else {
            this.updateRunStart(dependency);
        }
        final DimensionDependency dimension = super.dimension;
        if (dimension.readyToSolve && !dimension.resolved && super.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            final ConstraintWidget widget2 = super.widget;
            final int mMatchConstraintDefaultHeight = widget2.mMatchConstraintDefaultHeight;
            if (mMatchConstraintDefaultHeight != 2) {
                if (mMatchConstraintDefaultHeight == 3) {
                    if (widget2.horizontalRun.dimension.resolved) {
                        final int dimensionRatioSide = widget2.getDimensionRatioSide();
                        int n3 = 0;
                        Label_0251: {
                            float n2 = 0.0f;
                            Label_0243: {
                                float n4;
                                float n5;
                                if (dimensionRatioSide != -1) {
                                    if (dimensionRatioSide == 0) {
                                        final ConstraintWidget widget3 = super.widget;
                                        n2 = widget3.horizontalRun.dimension.value * widget3.getDimensionRatio();
                                        break Label_0243;
                                    }
                                    if (dimensionRatioSide != 1) {
                                        n3 = 0;
                                        break Label_0251;
                                    }
                                    final ConstraintWidget widget4 = super.widget;
                                    n4 = (float)widget4.horizontalRun.dimension.value;
                                    n5 = widget4.getDimensionRatio();
                                }
                                else {
                                    final ConstraintWidget widget5 = super.widget;
                                    n4 = (float)widget5.horizontalRun.dimension.value;
                                    n5 = widget5.getDimensionRatio();
                                }
                                n2 = n4 / n5;
                            }
                            n3 = (int)(n2 + 0.5f);
                        }
                        super.dimension.resolve(n3);
                    }
                }
            }
            else {
                final ConstraintWidget parent = widget2.getParent();
                if (parent != null) {
                    final DimensionDependency dimension2 = parent.verticalRun.dimension;
                    if (dimension2.resolved) {
                        super.dimension.resolve((int)(dimension2.value * super.widget.mMatchConstraintPercentHeight + 0.5f));
                    }
                }
            }
        }
        final DependencyNode start = super.start;
        if (start.readyToSolve) {
            final DependencyNode end = super.end;
            if (end.readyToSolve) {
                if (start.resolved && end.resolved && super.dimension.resolved) {
                    return;
                }
                if (!super.dimension.resolved && super.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    final ConstraintWidget widget6 = super.widget;
                    if (widget6.mMatchConstraintDefaultWidth == 0 && !widget6.isInVerticalChain()) {
                        final DependencyNode dependencyNode = super.start.targets.get(0);
                        final DependencyNode dependencyNode2 = super.end.targets.get(0);
                        final int value = dependencyNode.value;
                        final DependencyNode start2 = super.start;
                        final int n6 = value + start2.margin;
                        final int n7 = dependencyNode2.value + super.end.margin;
                        start2.resolve(n6);
                        super.end.resolve(n7);
                        super.dimension.resolve(n7 - n6);
                        return;
                    }
                }
                if (!super.dimension.resolved && super.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && super.matchConstraintsType == 1 && super.start.targets.size() > 0 && super.end.targets.size() > 0) {
                    final int n8 = super.end.targets.get(0).value + super.end.margin - (super.start.targets.get(0).value + super.start.margin);
                    final DimensionDependency dimension3 = super.dimension;
                    final int wrapValue = dimension3.wrapValue;
                    if (n8 < wrapValue) {
                        dimension3.resolve(n8);
                    }
                    else {
                        dimension3.resolve(wrapValue);
                    }
                }
                if (!super.dimension.resolved) {
                    return;
                }
                if (super.start.targets.size() > 0 && super.end.targets.size() > 0) {
                    final DependencyNode dependencyNode3 = super.start.targets.get(0);
                    final DependencyNode dependencyNode4 = super.end.targets.get(0);
                    int value2 = dependencyNode3.value + super.start.margin;
                    int value3 = dependencyNode4.value + super.end.margin;
                    float verticalBiasPercent = super.widget.getVerticalBiasPercent();
                    if (dependencyNode3 == dependencyNode4) {
                        value2 = dependencyNode3.value;
                        value3 = dependencyNode4.value;
                        verticalBiasPercent = 0.5f;
                    }
                    super.start.resolve((int)(value2 + 0.5f + (value3 - value2 - super.dimension.value) * verticalBiasPercent));
                    super.end.resolve(super.start.value + super.dimension.value);
                }
            }
        }
    }
}

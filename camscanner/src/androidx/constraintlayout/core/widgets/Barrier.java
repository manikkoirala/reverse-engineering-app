// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets;

import java.util.HashMap;
import androidx.constraintlayout.core.SolverVariable;
import androidx.constraintlayout.core.LinearSystem;

public class Barrier extends HelperWidget
{
    public static final int BOTTOM = 3;
    public static final int LEFT = 0;
    public static final int RIGHT = 1;
    public static final int TOP = 2;
    private static final boolean USE_RELAX_GONE = false;
    private static final boolean USE_RESOLUTION = true;
    private boolean mAllowsGoneWidget;
    private int mBarrierType;
    private int mMargin;
    boolean resolved;
    
    public Barrier() {
        this.mBarrierType = 0;
        this.mAllowsGoneWidget = true;
        this.mMargin = 0;
        this.resolved = false;
    }
    
    public Barrier(final String debugName) {
        this.mBarrierType = 0;
        this.mAllowsGoneWidget = true;
        this.mMargin = 0;
        this.resolved = false;
        this.setDebugName(debugName);
    }
    
    @Override
    public void addToSolver(final LinearSystem linearSystem, final boolean b) {
        final ConstraintAnchor[] mListAnchors = super.mListAnchors;
        mListAnchors[0] = super.mLeft;
        mListAnchors[2] = super.mTop;
        mListAnchors[1] = super.mRight;
        mListAnchors[3] = super.mBottom;
        int n = 0;
        ConstraintAnchor[] mListAnchors2;
        while (true) {
            mListAnchors2 = super.mListAnchors;
            if (n >= mListAnchors2.length) {
                break;
            }
            final ConstraintAnchor constraintAnchor = mListAnchors2[n];
            constraintAnchor.mSolverVariable = linearSystem.createObjectVariable(constraintAnchor);
            ++n;
        }
        final int mBarrierType = this.mBarrierType;
        if (mBarrierType >= 0 && mBarrierType < 4) {
            final ConstraintAnchor constraintAnchor2 = mListAnchors2[mBarrierType];
            if (!this.resolved) {
                this.allSolved();
            }
            if (this.resolved) {
                this.resolved = false;
                final int mBarrierType2 = this.mBarrierType;
                if (mBarrierType2 != 0 && mBarrierType2 != 1) {
                    if (mBarrierType2 == 2 || mBarrierType2 == 3) {
                        linearSystem.addEquality(super.mTop.mSolverVariable, super.mY);
                        linearSystem.addEquality(super.mBottom.mSolverVariable, super.mY);
                    }
                }
                else {
                    linearSystem.addEquality(super.mLeft.mSolverVariable, super.mX);
                    linearSystem.addEquality(super.mRight.mSolverVariable, super.mX);
                }
                return;
            }
            while (true) {
                for (int i = 0; i < super.mWidgetsCount; ++i) {
                    final ConstraintWidget constraintWidget = super.mWidgets[i];
                    if (this.mAllowsGoneWidget || constraintWidget.allowedInBarrier()) {
                        final int mBarrierType3 = this.mBarrierType;
                        if ((mBarrierType3 != 0 && mBarrierType3 != 1) || constraintWidget.getHorizontalDimensionBehaviour() != DimensionBehaviour.MATCH_CONSTRAINT || constraintWidget.mLeft.mTarget == null || constraintWidget.mRight.mTarget == null) {
                            final int mBarrierType4 = this.mBarrierType;
                            if ((mBarrierType4 != 2 && mBarrierType4 != 3) || constraintWidget.getVerticalDimensionBehaviour() != DimensionBehaviour.MATCH_CONSTRAINT || constraintWidget.mTop.mTarget == null || constraintWidget.mBottom.mTarget == null) {
                                continue;
                            }
                        }
                        final boolean b2 = true;
                        final boolean b3 = super.mLeft.hasCenteredDependents() || super.mRight.hasCenteredDependents();
                        final boolean b4 = super.mTop.hasCenteredDependents() || super.mBottom.hasCenteredDependents();
                        boolean b5 = false;
                        Label_0484: {
                            if (!b2) {
                                final int mBarrierType5 = this.mBarrierType;
                                if ((mBarrierType5 == 0 && b3) || (mBarrierType5 == 2 && b4) || (mBarrierType5 == 1 && b3) || (mBarrierType5 == 3 && b4)) {
                                    b5 = true;
                                    break Label_0484;
                                }
                            }
                            b5 = false;
                        }
                        int n2;
                        if (!b5) {
                            n2 = 4;
                        }
                        else {
                            n2 = 5;
                        }
                        for (int j = 0; j < super.mWidgetsCount; ++j) {
                            final ConstraintWidget constraintWidget2 = super.mWidgets[j];
                            if (this.mAllowsGoneWidget || constraintWidget2.allowedInBarrier()) {
                                final SolverVariable objectVariable = linearSystem.createObjectVariable(constraintWidget2.mListAnchors[this.mBarrierType]);
                                final ConstraintAnchor[] mListAnchors3 = constraintWidget2.mListAnchors;
                                final int mBarrierType6 = this.mBarrierType;
                                final ConstraintAnchor constraintAnchor3 = mListAnchors3[mBarrierType6];
                                constraintAnchor3.mSolverVariable = objectVariable;
                                final ConstraintAnchor mTarget = constraintAnchor3.mTarget;
                                int n3;
                                if (mTarget != null && mTarget.mOwner == this) {
                                    n3 = constraintAnchor3.mMargin + 0;
                                }
                                else {
                                    n3 = 0;
                                }
                                if (mBarrierType6 != 0 && mBarrierType6 != 2) {
                                    linearSystem.addGreaterBarrier(constraintAnchor2.mSolverVariable, objectVariable, this.mMargin + n3, b2);
                                }
                                else {
                                    linearSystem.addLowerBarrier(constraintAnchor2.mSolverVariable, objectVariable, this.mMargin - n3, b2);
                                }
                                linearSystem.addEquality(constraintAnchor2.mSolverVariable, objectVariable, this.mMargin + n3, n2);
                            }
                        }
                        final int mBarrierType7 = this.mBarrierType;
                        if (mBarrierType7 == 0) {
                            linearSystem.addEquality(super.mRight.mSolverVariable, super.mLeft.mSolverVariable, 0, 8);
                            linearSystem.addEquality(super.mLeft.mSolverVariable, super.mParent.mRight.mSolverVariable, 0, 4);
                            linearSystem.addEquality(super.mLeft.mSolverVariable, super.mParent.mLeft.mSolverVariable, 0, 0);
                            return;
                        }
                        if (mBarrierType7 == 1) {
                            linearSystem.addEquality(super.mLeft.mSolverVariable, super.mRight.mSolverVariable, 0, 8);
                            linearSystem.addEquality(super.mLeft.mSolverVariable, super.mParent.mLeft.mSolverVariable, 0, 4);
                            linearSystem.addEquality(super.mLeft.mSolverVariable, super.mParent.mRight.mSolverVariable, 0, 0);
                            return;
                        }
                        if (mBarrierType7 == 2) {
                            linearSystem.addEquality(super.mBottom.mSolverVariable, super.mTop.mSolverVariable, 0, 8);
                            linearSystem.addEquality(super.mTop.mSolverVariable, super.mParent.mBottom.mSolverVariable, 0, 4);
                            linearSystem.addEquality(super.mTop.mSolverVariable, super.mParent.mTop.mSolverVariable, 0, 0);
                            return;
                        }
                        if (mBarrierType7 == 3) {
                            linearSystem.addEquality(super.mTop.mSolverVariable, super.mBottom.mSolverVariable, 0, 8);
                            linearSystem.addEquality(super.mTop.mSolverVariable, super.mParent.mTop.mSolverVariable, 0, 4);
                            linearSystem.addEquality(super.mTop.mSolverVariable, super.mParent.mBottom.mSolverVariable, 0, 0);
                        }
                        return;
                    }
                }
                final boolean b2 = false;
                continue;
            }
        }
    }
    
    public boolean allSolved() {
        int i = 0;
        int n = 0;
        int n2 = 1;
        int mWidgetsCount;
        while (true) {
            mWidgetsCount = super.mWidgetsCount;
            if (n >= mWidgetsCount) {
                break;
            }
            final ConstraintWidget constraintWidget = super.mWidgets[n];
            int n3 = 0;
            Label_0105: {
                if (!this.mAllowsGoneWidget && !constraintWidget.allowedInBarrier()) {
                    n3 = n2;
                }
                else {
                    final int mBarrierType = this.mBarrierType;
                    if ((mBarrierType != 0 && mBarrierType != 1) || constraintWidget.isResolvedHorizontally()) {
                        final int mBarrierType2 = this.mBarrierType;
                        if (mBarrierType2 != 2) {
                            n3 = n2;
                            if (mBarrierType2 != 3) {
                                break Label_0105;
                            }
                        }
                        n3 = n2;
                        if (constraintWidget.isResolvedVertically()) {
                            break Label_0105;
                        }
                    }
                    n3 = 0;
                }
            }
            ++n;
            n2 = n3;
        }
        if (n2 != 0 && mWidgetsCount > 0) {
            int n4 = 0;
            int n5 = 0;
            while (i < super.mWidgetsCount) {
                final ConstraintWidget constraintWidget2 = super.mWidgets[i];
                if (this.mAllowsGoneWidget || constraintWidget2.allowedInBarrier()) {
                    int n6 = n4;
                    int n7;
                    if ((n7 = n5) == 0) {
                        final int mBarrierType3 = this.mBarrierType;
                        if (mBarrierType3 == 0) {
                            n4 = constraintWidget2.getAnchor(ConstraintAnchor.Type.LEFT).getFinalValue();
                        }
                        else if (mBarrierType3 == 1) {
                            n4 = constraintWidget2.getAnchor(ConstraintAnchor.Type.RIGHT).getFinalValue();
                        }
                        else if (mBarrierType3 == 2) {
                            n4 = constraintWidget2.getAnchor(ConstraintAnchor.Type.TOP).getFinalValue();
                        }
                        else if (mBarrierType3 == 3) {
                            n4 = constraintWidget2.getAnchor(ConstraintAnchor.Type.BOTTOM).getFinalValue();
                        }
                        n7 = 1;
                        n6 = n4;
                    }
                    final int mBarrierType4 = this.mBarrierType;
                    if (mBarrierType4 == 0) {
                        n4 = Math.min(n6, constraintWidget2.getAnchor(ConstraintAnchor.Type.LEFT).getFinalValue());
                        n5 = n7;
                    }
                    else if (mBarrierType4 == 1) {
                        n4 = Math.max(n6, constraintWidget2.getAnchor(ConstraintAnchor.Type.RIGHT).getFinalValue());
                        n5 = n7;
                    }
                    else if (mBarrierType4 == 2) {
                        n4 = Math.min(n6, constraintWidget2.getAnchor(ConstraintAnchor.Type.TOP).getFinalValue());
                        n5 = n7;
                    }
                    else {
                        n4 = n6;
                        n5 = n7;
                        if (mBarrierType4 == 3) {
                            n4 = Math.max(n6, constraintWidget2.getAnchor(ConstraintAnchor.Type.BOTTOM).getFinalValue());
                            n5 = n7;
                        }
                    }
                }
                ++i;
            }
            final int n8 = n4 + this.mMargin;
            final int mBarrierType5 = this.mBarrierType;
            if (mBarrierType5 != 0 && mBarrierType5 != 1) {
                this.setFinalVertical(n8, n8);
            }
            else {
                this.setFinalHorizontal(n8, n8);
            }
            return this.resolved = true;
        }
        return false;
    }
    
    @Override
    public boolean allowedInBarrier() {
        return true;
    }
    
    @Deprecated
    public boolean allowsGoneWidget() {
        return this.mAllowsGoneWidget;
    }
    
    @Override
    public void copy(final ConstraintWidget constraintWidget, final HashMap<ConstraintWidget, ConstraintWidget> hashMap) {
        super.copy(constraintWidget, hashMap);
        final Barrier barrier = (Barrier)constraintWidget;
        this.mBarrierType = barrier.mBarrierType;
        this.mAllowsGoneWidget = barrier.mAllowsGoneWidget;
        this.mMargin = barrier.mMargin;
    }
    
    public boolean getAllowsGoneWidget() {
        return this.mAllowsGoneWidget;
    }
    
    public int getBarrierType() {
        return this.mBarrierType;
    }
    
    public int getMargin() {
        return this.mMargin;
    }
    
    public int getOrientation() {
        final int mBarrierType = this.mBarrierType;
        if (mBarrierType == 0 || mBarrierType == 1) {
            return 0;
        }
        if (mBarrierType != 2 && mBarrierType != 3) {
            return -1;
        }
        return 1;
    }
    
    @Override
    public boolean isResolvedHorizontally() {
        return this.resolved;
    }
    
    @Override
    public boolean isResolvedVertically() {
        return this.resolved;
    }
    
    protected void markWidgets() {
        for (int i = 0; i < super.mWidgetsCount; ++i) {
            final ConstraintWidget constraintWidget = super.mWidgets[i];
            if (this.mAllowsGoneWidget || constraintWidget.allowedInBarrier()) {
                final int mBarrierType = this.mBarrierType;
                if (mBarrierType != 0 && mBarrierType != 1) {
                    if (mBarrierType == 2 || mBarrierType == 3) {
                        constraintWidget.setInBarrier(1, true);
                    }
                }
                else {
                    constraintWidget.setInBarrier(0, true);
                }
            }
        }
    }
    
    public void setAllowsGoneWidget(final boolean mAllowsGoneWidget) {
        this.mAllowsGoneWidget = mAllowsGoneWidget;
    }
    
    public void setBarrierType(final int mBarrierType) {
        this.mBarrierType = mBarrierType;
    }
    
    public void setMargin(final int mMargin) {
        this.mMargin = mMargin;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[Barrier] ");
        sb.append(this.getDebugName());
        sb.append(" {");
        String s = sb.toString();
        for (int i = 0; i < super.mWidgetsCount; ++i) {
            final ConstraintWidget constraintWidget = super.mWidgets[i];
            String string = s;
            if (i > 0) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(s);
                sb2.append(", ");
                string = sb2.toString();
            }
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(string);
            sb3.append(constraintWidget.getDebugName());
            s = sb3.toString();
        }
        final StringBuilder sb4 = new StringBuilder();
        sb4.append(s);
        sb4.append("}");
        return sb4.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets;

import androidx.constraintlayout.core.ArrayRow;
import java.util.ArrayList;
import androidx.constraintlayout.core.SolverVariable;
import androidx.constraintlayout.core.LinearSystem;

public class Chain
{
    private static final boolean DEBUG = false;
    public static final boolean USE_CHAIN_OPTIMIZATION = false;
    
    static void applyChainConstraints(final ConstraintWidgetContainer constraintWidgetContainer, final LinearSystem linearSystem, int n, int margin, final ChainHead chainHead) {
        final ConstraintWidget mFirst = chainHead.mFirst;
        final ConstraintWidget mLast = chainHead.mLast;
        final ConstraintWidget mFirstVisibleWidget = chainHead.mFirstVisibleWidget;
        final ConstraintWidget mLastVisibleWidget = chainHead.mLastVisibleWidget;
        final ConstraintWidget mHead = chainHead.mHead;
        final float mTotalWeight = chainHead.mTotalWeight;
        final boolean b = constraintWidgetContainer.mListDimensionBehaviors[n] == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        boolean b2 = false;
        int n6 = 0;
        int n7 = 0;
        Label_0195: {
            int n4 = 0;
            int n5 = 0;
            Label_0184: {
                int n2;
                int n3;
                if (n == 0) {
                    final int mHorizontalChainStyle = mHead.mHorizontalChainStyle;
                    if (mHorizontalChainStyle == 0) {
                        n2 = 1;
                    }
                    else {
                        n2 = 0;
                    }
                    if (mHorizontalChainStyle == 1) {
                        n3 = 1;
                    }
                    else {
                        n3 = 0;
                    }
                    n4 = n2;
                    n5 = n3;
                    if (mHorizontalChainStyle != 2) {
                        break Label_0184;
                    }
                }
                else {
                    final int mVerticalChainStyle = mHead.mVerticalChainStyle;
                    if (mVerticalChainStyle == 0) {
                        n2 = 1;
                    }
                    else {
                        n2 = 0;
                    }
                    if (mVerticalChainStyle == 1) {
                        n3 = 1;
                    }
                    else {
                        n3 = 0;
                    }
                    n4 = n2;
                    n5 = n3;
                    if (mVerticalChainStyle != 2) {
                        break Label_0184;
                    }
                }
                b2 = true;
                n6 = n2;
                n7 = n3;
                break Label_0195;
            }
            b2 = false;
            n7 = n5;
            n6 = n4;
        }
        int n8 = 0;
        ConstraintWidget constraintWidget = mFirst;
        SolverVariable solverVariable;
        while (true) {
            solverVariable = null;
            final ConstraintWidget constraintWidget2 = null;
            if (n8 != 0) {
                break;
            }
            final ConstraintAnchor constraintAnchor = constraintWidget.mListAnchors[margin];
            int n9;
            if (b2) {
                n9 = 1;
            }
            else {
                n9 = 4;
            }
            final int margin2 = constraintAnchor.getMargin();
            final ConstraintWidget.DimensionBehaviour dimensionBehaviour = constraintWidget.mListDimensionBehaviors[n];
            final ConstraintWidget.DimensionBehaviour match_CONSTRAINT = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
            final boolean b3 = dimensionBehaviour == match_CONSTRAINT && constraintWidget.mResolvedMatchConstraintDefault[n] == 0;
            final ConstraintAnchor mTarget = constraintAnchor.mTarget;
            int n10 = margin2;
            if (mTarget != null) {
                n10 = margin2;
                if (constraintWidget != mFirst) {
                    n10 = margin2 + mTarget.getMargin();
                }
            }
            if (b2 && constraintWidget != mFirst && constraintWidget != mFirstVisibleWidget) {
                n9 = 8;
            }
            final ConstraintAnchor mTarget2 = constraintAnchor.mTarget;
            if (mTarget2 != null) {
                if (constraintWidget == mFirstVisibleWidget) {
                    linearSystem.addGreaterThan(constraintAnchor.mSolverVariable, mTarget2.mSolverVariable, n10, 6);
                }
                else {
                    linearSystem.addGreaterThan(constraintAnchor.mSolverVariable, mTarget2.mSolverVariable, n10, 8);
                }
                int n11 = n9;
                if (b3) {
                    n11 = n9;
                    if (!b2) {
                        n11 = 5;
                    }
                }
                if (constraintWidget == mFirstVisibleWidget && b2 && constraintWidget.isInBarrier(n)) {
                    n11 = 5;
                }
                linearSystem.addEquality(constraintAnchor.mSolverVariable, constraintAnchor.mTarget.mSolverVariable, n10, n11);
            }
            if (b) {
                if (constraintWidget.getVisibility() != 8 && constraintWidget.mListDimensionBehaviors[n] == match_CONSTRAINT) {
                    final ConstraintAnchor[] mListAnchors = constraintWidget.mListAnchors;
                    linearSystem.addGreaterThan(mListAnchors[margin + 1].mSolverVariable, mListAnchors[margin].mSolverVariable, 0, 5);
                }
                linearSystem.addGreaterThan(constraintWidget.mListAnchors[margin].mSolverVariable, constraintWidgetContainer.mListAnchors[margin].mSolverVariable, 0, 8);
            }
            final ConstraintAnchor mTarget3 = constraintWidget.mListAnchors[margin + 1].mTarget;
            ConstraintWidget constraintWidget3 = constraintWidget2;
            if (mTarget3 != null) {
                final ConstraintWidget mOwner = mTarget3.mOwner;
                final ConstraintAnchor mTarget4 = mOwner.mListAnchors[margin].mTarget;
                constraintWidget3 = constraintWidget2;
                if (mTarget4 != null) {
                    if (mTarget4.mOwner != constraintWidget) {
                        constraintWidget3 = constraintWidget2;
                    }
                    else {
                        constraintWidget3 = mOwner;
                    }
                }
            }
            if (constraintWidget3 != null) {
                constraintWidget = constraintWidget3;
            }
            else {
                n8 = 1;
            }
        }
        if (mLastVisibleWidget != null) {
            final ConstraintAnchor[] mListAnchors2 = mLast.mListAnchors;
            final int n12 = margin + 1;
            if (mListAnchors2[n12].mTarget != null) {
                final ConstraintAnchor constraintAnchor2 = mLastVisibleWidget.mListAnchors[n12];
                Label_0816: {
                    if (mLastVisibleWidget.mListDimensionBehaviors[n] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && mLastVisibleWidget.mResolvedMatchConstraintDefault[n] == 0 && !b2) {
                        final ConstraintAnchor mTarget5 = constraintAnchor2.mTarget;
                        if (mTarget5.mOwner == constraintWidgetContainer) {
                            linearSystem.addEquality(constraintAnchor2.mSolverVariable, mTarget5.mSolverVariable, -constraintAnchor2.getMargin(), 5);
                            break Label_0816;
                        }
                    }
                    if (b2) {
                        final ConstraintAnchor mTarget6 = constraintAnchor2.mTarget;
                        if (mTarget6.mOwner == constraintWidgetContainer) {
                            linearSystem.addEquality(constraintAnchor2.mSolverVariable, mTarget6.mSolverVariable, -constraintAnchor2.getMargin(), 4);
                        }
                    }
                }
                linearSystem.addLowerThan(constraintAnchor2.mSolverVariable, mLast.mListAnchors[n12].mTarget.mSolverVariable, -constraintAnchor2.getMargin(), 6);
            }
        }
        if (b) {
            final ConstraintAnchor[] mListAnchors3 = constraintWidgetContainer.mListAnchors;
            final int n13 = margin + 1;
            final SolverVariable mSolverVariable = mListAnchors3[n13].mSolverVariable;
            final ConstraintAnchor constraintAnchor3 = mLast.mListAnchors[n13];
            linearSystem.addGreaterThan(mSolverVariable, constraintAnchor3.mSolverVariable, constraintAnchor3.getMargin(), 8);
        }
        final ArrayList<ConstraintWidget> mWeightedMatchConstraintsWidgets = chainHead.mWeightedMatchConstraintsWidgets;
        if (mWeightedMatchConstraintsWidgets != null) {
            final int size = mWeightedMatchConstraintsWidgets.size();
            if (size > 1) {
                float n14;
                if (chainHead.mHasUndefinedWeights && !chainHead.mHasComplexMatchWeights) {
                    n14 = (float)chainHead.mWidgetsMatchCount;
                }
                else {
                    n14 = mTotalWeight;
                }
                ConstraintWidget constraintWidget4 = null;
                int i = 0;
                float n15 = 0.0f;
                while (i < size) {
                    final ConstraintWidget constraintWidget5 = mWeightedMatchConstraintsWidgets.get(i);
                    float n16 = constraintWidget5.mWeight[n];
                    Label_1188: {
                        Label_1080: {
                            if (n16 < 0.0f) {
                                if (chainHead.mHasComplexMatchWeights) {
                                    final ConstraintAnchor[] mListAnchors4 = constraintWidget5.mListAnchors;
                                    linearSystem.addEquality(mListAnchors4[margin + 1].mSolverVariable, mListAnchors4[margin].mSolverVariable, 0, 4);
                                    break Label_1080;
                                }
                                n16 = 1.0f;
                            }
                            if (n16 != 0.0f) {
                                if (constraintWidget4 != null) {
                                    final ConstraintAnchor[] mListAnchors5 = constraintWidget4.mListAnchors;
                                    final SolverVariable mSolverVariable2 = mListAnchors5[margin].mSolverVariable;
                                    final int n17 = margin + 1;
                                    final SolverVariable mSolverVariable3 = mListAnchors5[n17].mSolverVariable;
                                    final ConstraintAnchor[] mListAnchors6 = constraintWidget5.mListAnchors;
                                    final SolverVariable mSolverVariable4 = mListAnchors6[margin].mSolverVariable;
                                    final SolverVariable mSolverVariable5 = mListAnchors6[n17].mSolverVariable;
                                    final ArrayRow row = linearSystem.createRow();
                                    row.createRowEqualMatchDimensions(n15, n14, n16, mSolverVariable2, mSolverVariable3, mSolverVariable4, mSolverVariable5);
                                    linearSystem.addConstraint(row);
                                }
                                constraintWidget4 = constraintWidget5;
                                break Label_1188;
                            }
                            final ConstraintAnchor[] mListAnchors7 = constraintWidget5.mListAnchors;
                            linearSystem.addEquality(mListAnchors7[margin + 1].mSolverVariable, mListAnchors7[margin].mSolverVariable, 0, 8);
                        }
                        n16 = n15;
                    }
                    ++i;
                    n15 = n16;
                }
            }
        }
        if (mFirstVisibleWidget != null && (mFirstVisibleWidget == mLastVisibleWidget || b2)) {
            final ConstraintAnchor constraintAnchor4 = mFirst.mListAnchors[margin];
            final ConstraintAnchor[] mListAnchors8 = mLast.mListAnchors;
            final int n18 = margin + 1;
            ConstraintAnchor constraintAnchor5 = mListAnchors8[n18];
            final ConstraintAnchor mTarget7 = constraintAnchor4.mTarget;
            SolverVariable mSolverVariable6;
            if (mTarget7 != null) {
                mSolverVariable6 = mTarget7.mSolverVariable;
            }
            else {
                mSolverVariable6 = null;
            }
            final ConstraintAnchor mTarget8 = constraintAnchor5.mTarget;
            SolverVariable mSolverVariable7;
            if (mTarget8 != null) {
                mSolverVariable7 = mTarget8.mSolverVariable;
            }
            else {
                mSolverVariable7 = null;
            }
            final ConstraintAnchor constraintAnchor6 = mFirstVisibleWidget.mListAnchors[margin];
            if (mLastVisibleWidget != null) {
                constraintAnchor5 = mLastVisibleWidget.mListAnchors[n18];
            }
            if (mSolverVariable6 != null && mSolverVariable7 != null) {
                float n19;
                if (n == 0) {
                    n19 = mHead.mHorizontalBiasPercent;
                }
                else {
                    n19 = mHead.mVerticalBiasPercent;
                }
                n = constraintAnchor6.getMargin();
                linearSystem.addCentering(constraintAnchor6.mSolverVariable, mSolverVariable6, n, n19, mSolverVariable7, constraintAnchor5.mSolverVariable, constraintAnchor5.getMargin(), 7);
            }
        }
        else if (n6 != 0 && mFirstVisibleWidget != null) {
            final int mWidgetsMatchCount = chainHead.mWidgetsMatchCount;
            final boolean b4 = mWidgetsMatchCount > 0 && chainHead.mWidgetsCount == mWidgetsMatchCount;
            ConstraintWidget constraintWidget7;
            ConstraintWidget constraintWidget8;
            for (ConstraintWidget constraintWidget6 = constraintWidget7 = mFirstVisibleWidget; constraintWidget7 != null; constraintWidget7 = constraintWidget8) {
                for (constraintWidget8 = constraintWidget7.mNextChainWidget[n]; constraintWidget8 != null && constraintWidget8.getVisibility() == 8; constraintWidget8 = constraintWidget8.mNextChainWidget[n]) {}
                if (constraintWidget8 != null || constraintWidget7 == mLastVisibleWidget) {
                    final ConstraintAnchor constraintAnchor7 = constraintWidget7.mListAnchors[margin];
                    final SolverVariable mSolverVariable8 = constraintAnchor7.mSolverVariable;
                    final ConstraintAnchor mTarget9 = constraintAnchor7.mTarget;
                    SolverVariable solverVariable2;
                    if (mTarget9 != null) {
                        solverVariable2 = mTarget9.mSolverVariable;
                    }
                    else {
                        solverVariable2 = null;
                    }
                    if (constraintWidget6 != constraintWidget7) {
                        solverVariable2 = constraintWidget6.mListAnchors[margin + 1].mSolverVariable;
                    }
                    else if (constraintWidget7 == mFirstVisibleWidget) {
                        final ConstraintAnchor mTarget10 = mFirst.mListAnchors[margin].mTarget;
                        if (mTarget10 != null) {
                            solverVariable2 = mTarget10.mSolverVariable;
                        }
                        else {
                            solverVariable2 = null;
                        }
                    }
                    final int margin3 = constraintAnchor7.getMargin();
                    final ConstraintAnchor[] mListAnchors9 = constraintWidget7.mListAnchors;
                    final int n20 = margin + 1;
                    final int margin4 = mListAnchors9[n20].getMargin();
                    ConstraintAnchor mTarget11;
                    SolverVariable solverVariable3;
                    if (constraintWidget8 != null) {
                        mTarget11 = constraintWidget8.mListAnchors[margin];
                        solverVariable3 = mTarget11.mSolverVariable;
                    }
                    else {
                        mTarget11 = mLast.mListAnchors[n20].mTarget;
                        if (mTarget11 != null) {
                            solverVariable3 = mTarget11.mSolverVariable;
                        }
                        else {
                            solverVariable3 = null;
                        }
                    }
                    final SolverVariable mSolverVariable9 = constraintWidget7.mListAnchors[n20].mSolverVariable;
                    int margin5 = margin4;
                    if (mTarget11 != null) {
                        margin5 = margin4 + mTarget11.getMargin();
                    }
                    int margin6 = margin3 + constraintWidget6.mListAnchors[n20].getMargin();
                    if (mSolverVariable8 != null && solverVariable2 != null && solverVariable3 != null && mSolverVariable9 != null) {
                        if (constraintWidget7 == mFirstVisibleWidget) {
                            margin6 = mFirstVisibleWidget.mListAnchors[margin].getMargin();
                        }
                        if (constraintWidget7 == mLastVisibleWidget) {
                            margin5 = mLastVisibleWidget.mListAnchors[n20].getMargin();
                        }
                        int n21;
                        if (b4) {
                            n21 = 8;
                        }
                        else {
                            n21 = 5;
                        }
                        linearSystem.addCentering(mSolverVariable8, solverVariable2, margin6, 0.5f, solverVariable3, mSolverVariable9, margin5, n21);
                    }
                }
                if (constraintWidget7.getVisibility() == 8) {
                    constraintWidget7 = constraintWidget6;
                }
                constraintWidget6 = constraintWidget7;
            }
        }
        else if (n7 != 0 && mFirstVisibleWidget != null) {
            final int mWidgetsMatchCount2 = chainHead.mWidgetsMatchCount;
            final boolean b5 = mWidgetsMatchCount2 > 0 && chainHead.mWidgetsCount == mWidgetsMatchCount2;
            ConstraintWidget constraintWidget10;
            ConstraintWidget constraintWidget11;
            for (ConstraintWidget constraintWidget9 = constraintWidget10 = mFirstVisibleWidget; constraintWidget10 != null; constraintWidget10 = constraintWidget11) {
                for (constraintWidget11 = constraintWidget10.mNextChainWidget[n]; constraintWidget11 != null && constraintWidget11.getVisibility() == 8; constraintWidget11 = constraintWidget11.mNextChainWidget[n]) {}
                if (constraintWidget10 != mFirstVisibleWidget && constraintWidget10 != mLastVisibleWidget && constraintWidget11 != null) {
                    if (constraintWidget11 == mLastVisibleWidget) {
                        constraintWidget11 = null;
                    }
                    final ConstraintAnchor constraintAnchor8 = constraintWidget10.mListAnchors[margin];
                    final SolverVariable mSolverVariable10 = constraintAnchor8.mSolverVariable;
                    final ConstraintAnchor mTarget12 = constraintAnchor8.mTarget;
                    if (mTarget12 != null) {
                        final SolverVariable mSolverVariable11 = mTarget12.mSolverVariable;
                    }
                    final ConstraintAnchor[] mListAnchors10 = constraintWidget9.mListAnchors;
                    final int n22 = margin + 1;
                    final SolverVariable mSolverVariable12 = mListAnchors10[n22].mSolverVariable;
                    final int margin7 = constraintAnchor8.getMargin();
                    final int margin8 = constraintWidget10.mListAnchors[n22].getMargin();
                    ConstraintAnchor constraintAnchor9;
                    SolverVariable mSolverVariable13;
                    SolverVariable mSolverVariable15;
                    if (constraintWidget11 != null) {
                        constraintAnchor9 = constraintWidget11.mListAnchors[margin];
                        mSolverVariable13 = constraintAnchor9.mSolverVariable;
                        final ConstraintAnchor mTarget13 = constraintAnchor9.mTarget;
                        SolverVariable mSolverVariable14;
                        if (mTarget13 != null) {
                            mSolverVariable14 = mTarget13.mSolverVariable;
                        }
                        else {
                            mSolverVariable14 = null;
                        }
                        mSolverVariable15 = mSolverVariable14;
                    }
                    else {
                        constraintAnchor9 = mLastVisibleWidget.mListAnchors[margin];
                        SolverVariable mSolverVariable16;
                        if (constraintAnchor9 != null) {
                            mSolverVariable16 = constraintAnchor9.mSolverVariable;
                        }
                        else {
                            mSolverVariable16 = null;
                        }
                        mSolverVariable15 = constraintWidget10.mListAnchors[n22].mSolverVariable;
                        mSolverVariable13 = mSolverVariable16;
                    }
                    int n23 = margin8;
                    if (constraintAnchor9 != null) {
                        n23 = margin8 + constraintAnchor9.getMargin();
                    }
                    final int margin9 = constraintWidget9.mListAnchors[n22].getMargin();
                    int n24;
                    if (b5) {
                        n24 = 8;
                    }
                    else {
                        n24 = 4;
                    }
                    if (mSolverVariable10 != null && mSolverVariable12 != null && mSolverVariable13 != null && mSolverVariable15 != null) {
                        linearSystem.addCentering(mSolverVariable10, mSolverVariable12, margin9 + margin7, 0.5f, mSolverVariable13, mSolverVariable15, n23, n24);
                    }
                }
                if (constraintWidget10.getVisibility() != 8) {
                    constraintWidget9 = constraintWidget10;
                }
            }
            final ConstraintAnchor constraintAnchor10 = mFirstVisibleWidget.mListAnchors[margin];
            final ConstraintAnchor mTarget14 = mFirst.mListAnchors[margin].mTarget;
            final ConstraintAnchor[] mListAnchors11 = mLastVisibleWidget.mListAnchors;
            n = margin + 1;
            final ConstraintAnchor constraintAnchor11 = mListAnchors11[n];
            final ConstraintAnchor mTarget15 = mLast.mListAnchors[n].mTarget;
            if (mTarget14 != null) {
                if (mFirstVisibleWidget != mLastVisibleWidget) {
                    linearSystem.addEquality(constraintAnchor10.mSolverVariable, mTarget14.mSolverVariable, constraintAnchor10.getMargin(), 5);
                }
                else if (mTarget15 != null) {
                    linearSystem.addCentering(constraintAnchor10.mSolverVariable, mTarget14.mSolverVariable, constraintAnchor10.getMargin(), 0.5f, constraintAnchor11.mSolverVariable, mTarget15.mSolverVariable, constraintAnchor11.getMargin(), 5);
                }
            }
            if (mTarget15 != null && mFirstVisibleWidget != mLastVisibleWidget) {
                linearSystem.addEquality(constraintAnchor11.mSolverVariable, mTarget15.mSolverVariable, -constraintAnchor11.getMargin(), 5);
            }
        }
        if ((n6 != 0 || n7 != 0) && mFirstVisibleWidget != null && mFirstVisibleWidget != mLastVisibleWidget) {
            final ConstraintAnchor[] mListAnchors12 = mFirstVisibleWidget.mListAnchors;
            final ConstraintAnchor constraintAnchor12 = mListAnchors12[margin];
            ConstraintWidget constraintWidget12;
            if ((constraintWidget12 = mLastVisibleWidget) == null) {
                constraintWidget12 = mFirstVisibleWidget;
            }
            final ConstraintAnchor[] mListAnchors13 = constraintWidget12.mListAnchors;
            ++margin;
            ConstraintAnchor constraintAnchor13 = mListAnchors13[margin];
            final ConstraintAnchor mTarget16 = constraintAnchor12.mTarget;
            SolverVariable mSolverVariable17;
            if (mTarget16 != null) {
                mSolverVariable17 = mTarget16.mSolverVariable;
            }
            else {
                mSolverVariable17 = null;
            }
            final ConstraintAnchor mTarget17 = constraintAnchor13.mTarget;
            SolverVariable solverVariable4;
            if (mTarget17 != null) {
                solverVariable4 = mTarget17.mSolverVariable;
            }
            else {
                solverVariable4 = null;
            }
            if (mLast != constraintWidget12) {
                final ConstraintAnchor mTarget18 = mLast.mListAnchors[margin].mTarget;
                solverVariable4 = solverVariable;
                if (mTarget18 != null) {
                    solverVariable4 = mTarget18.mSolverVariable;
                }
            }
            if (mFirstVisibleWidget == constraintWidget12) {
                constraintAnchor13 = mListAnchors12[margin];
            }
            if (mSolverVariable17 != null && solverVariable4 != null) {
                n = constraintAnchor12.getMargin();
                margin = constraintWidget12.mListAnchors[margin].getMargin();
                linearSystem.addCentering(constraintAnchor12.mSolverVariable, mSolverVariable17, n, 0.5f, solverVariable4, constraintAnchor13.mSolverVariable, margin, 5);
            }
        }
    }
    
    public static void applyChainConstraints(final ConstraintWidgetContainer constraintWidgetContainer, final LinearSystem linearSystem, final ArrayList<ConstraintWidget> list, final int n) {
        int i = 0;
        int n2;
        ChainHead[] array;
        int n3;
        if (n == 0) {
            n2 = constraintWidgetContainer.mHorizontalChainsSize;
            array = constraintWidgetContainer.mHorizontalChainsArray;
            n3 = 0;
        }
        else {
            n2 = constraintWidgetContainer.mVerticalChainsSize;
            array = constraintWidgetContainer.mVerticalChainsArray;
            n3 = 2;
        }
        while (i < n2) {
            final ChainHead chainHead = array[i];
            chainHead.define();
            if (list == null || list.contains(chainHead.mFirst)) {
                applyChainConstraints(constraintWidgetContainer, linearSystem, n, n3, chainHead);
            }
            ++i;
        }
    }
}

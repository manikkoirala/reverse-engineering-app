// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.widgets;

import androidx.constraintlayout.core.Cache;
import java.util.ArrayList;

public class WidgetContainer extends ConstraintWidget
{
    public ArrayList<ConstraintWidget> mChildren;
    
    public WidgetContainer() {
        this.mChildren = new ArrayList<ConstraintWidget>();
    }
    
    public WidgetContainer(final int n, final int n2) {
        super(n, n2);
        this.mChildren = new ArrayList<ConstraintWidget>();
    }
    
    public WidgetContainer(final int n, final int n2, final int n3, final int n4) {
        super(n, n2, n3, n4);
        this.mChildren = new ArrayList<ConstraintWidget>();
    }
    
    public void add(final ConstraintWidget e) {
        this.mChildren.add(e);
        if (e.getParent() != null) {
            ((WidgetContainer)e.getParent()).remove(e);
        }
        e.setParent(this);
    }
    
    public void add(final ConstraintWidget... array) {
        for (int length = array.length, i = 0; i < length; ++i) {
            this.add(array[i]);
        }
    }
    
    public ArrayList<ConstraintWidget> getChildren() {
        return this.mChildren;
    }
    
    public ConstraintWidgetContainer getRootConstraintContainer() {
        ConstraintWidget parent = this.getParent();
        ConstraintWidgetContainer constraintWidgetContainer;
        if (this instanceof ConstraintWidgetContainer) {
            constraintWidgetContainer = (ConstraintWidgetContainer)this;
        }
        else {
            constraintWidgetContainer = null;
        }
        while (parent != null) {
            final ConstraintWidget parent2 = parent.getParent();
            if (parent instanceof ConstraintWidgetContainer) {
                constraintWidgetContainer = (ConstraintWidgetContainer)parent;
            }
            parent = parent2;
        }
        return constraintWidgetContainer;
    }
    
    public void layout() {
        final ArrayList<ConstraintWidget> mChildren = this.mChildren;
        if (mChildren == null) {
            return;
        }
        for (int size = mChildren.size(), i = 0; i < size; ++i) {
            final ConstraintWidget constraintWidget = this.mChildren.get(i);
            if (constraintWidget instanceof WidgetContainer) {
                ((WidgetContainer)constraintWidget).layout();
            }
        }
    }
    
    public void remove(final ConstraintWidget o) {
        this.mChildren.remove(o);
        o.reset();
    }
    
    public void removeAllChildren() {
        this.mChildren.clear();
    }
    
    @Override
    public void reset() {
        this.mChildren.clear();
        super.reset();
    }
    
    @Override
    public void resetSolverVariables(final Cache cache) {
        super.resetSolverVariables(cache);
        for (int size = this.mChildren.size(), i = 0; i < size; ++i) {
            this.mChildren.get(i).resetSolverVariables(cache);
        }
    }
    
    @Override
    public void setOffset(int i, int size) {
        super.setOffset(i, size);
        for (size = this.mChildren.size(), i = 0; i < size; ++i) {
            this.mChildren.get(i).setOffset(this.getRootX(), this.getRootY());
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core;

import java.util.ArrayList;

public class ArrayRow implements Row
{
    private static final boolean DEBUG = false;
    private static final boolean FULL_NEW_CHECK = false;
    float constantValue;
    boolean isSimpleDefinition;
    boolean used;
    SolverVariable variable;
    public ArrayRowVariables variables;
    ArrayList<SolverVariable> variablesToUpdate;
    
    public ArrayRow() {
        this.variable = null;
        this.constantValue = 0.0f;
        this.used = false;
        this.variablesToUpdate = new ArrayList<SolverVariable>();
        this.isSimpleDefinition = false;
    }
    
    public ArrayRow(final Cache cache) {
        this.variable = null;
        this.constantValue = 0.0f;
        this.used = false;
        this.variablesToUpdate = new ArrayList<SolverVariable>();
        this.isSimpleDefinition = false;
        this.variables = (ArrayRowVariables)new ArrayLinkedVariables(this, cache);
    }
    
    private boolean isNew(final SolverVariable solverVariable, final LinearSystem linearSystem) {
        final int usageInRowCount = solverVariable.usageInRowCount;
        boolean b = true;
        if (usageInRowCount > 1) {
            b = false;
        }
        return b;
    }
    
    private SolverVariable pickPivotInVariables(final boolean[] array, final SolverVariable solverVariable) {
        final int currentSize = this.variables.getCurrentSize();
        SolverVariable solverVariable2 = null;
        int i = 0;
        float n = 0.0f;
        while (i < currentSize) {
            final float variableValue = this.variables.getVariableValue(i);
            SolverVariable solverVariable3 = solverVariable2;
            float n2 = n;
            Label_0152: {
                if (variableValue < 0.0f) {
                    final SolverVariable variable = this.variables.getVariable(i);
                    if (array != null) {
                        solverVariable3 = solverVariable2;
                        n2 = n;
                        if (array[variable.id]) {
                            break Label_0152;
                        }
                    }
                    solverVariable3 = solverVariable2;
                    n2 = n;
                    if (variable != solverVariable) {
                        final SolverVariable.Type mType = variable.mType;
                        if (mType != SolverVariable.Type.SLACK) {
                            solverVariable3 = solverVariable2;
                            n2 = n;
                            if (mType != SolverVariable.Type.ERROR) {
                                break Label_0152;
                            }
                        }
                        solverVariable3 = solverVariable2;
                        n2 = n;
                        if (variableValue < n) {
                            n2 = variableValue;
                            solverVariable3 = variable;
                        }
                    }
                }
            }
            ++i;
            solverVariable2 = solverVariable3;
            n = n2;
        }
        return solverVariable2;
    }
    
    public ArrayRow addError(final LinearSystem linearSystem, final int n) {
        this.variables.put(linearSystem.createErrorVariable(n, "ep"), 1.0f);
        this.variables.put(linearSystem.createErrorVariable(n, "em"), -1.0f);
        return this;
    }
    
    @Override
    public void addError(final SolverVariable solverVariable) {
        final int strength = solverVariable.strength;
        float n = 1.0f;
        if (strength != 1) {
            if (strength == 2) {
                n = 1000.0f;
            }
            else if (strength == 3) {
                n = 1000000.0f;
            }
            else if (strength == 4) {
                n = 1.0E9f;
            }
            else if (strength == 5) {
                n = 1.0E12f;
            }
        }
        this.variables.put(solverVariable, n);
    }
    
    ArrayRow addSingleError(final SolverVariable solverVariable, final int n) {
        this.variables.put(solverVariable, (float)n);
        return this;
    }
    
    boolean chooseSubject(final LinearSystem linearSystem) {
        final SolverVariable chooseSubjectInVariables = this.chooseSubjectInVariables(linearSystem);
        boolean b;
        if (chooseSubjectInVariables == null) {
            b = true;
        }
        else {
            this.pivot(chooseSubjectInVariables);
            b = false;
        }
        if (this.variables.getCurrentSize() == 0) {
            this.isSimpleDefinition = true;
        }
        return b;
    }
    
    SolverVariable chooseSubjectInVariables(final LinearSystem linearSystem) {
        final int currentSize = this.variables.getCurrentSize();
        SolverVariable solverVariable = null;
        SolverVariable solverVariable2 = null;
        int i = 0;
        int n = 0;
        int n2 = 0;
        float n3 = 0.0f;
        float n4 = 0.0f;
        while (i < currentSize) {
            final float variableValue = this.variables.getVariableValue(i);
            final SolverVariable variable = this.variables.getVariable(i);
            int n5 = 0;
            SolverVariable solverVariable3 = null;
            SolverVariable solverVariable4 = null;
            int n6 = 0;
            float n7 = 0.0f;
            float n8 = 0.0f;
            Label_0411: {
                if (variable.mType == SolverVariable.Type.UNRESTRICTED) {
                    if (solverVariable == null) {
                        n5 = (this.isNew(variable, linearSystem) ? 1 : 0);
                    }
                    else if (n3 > variableValue) {
                        n5 = (this.isNew(variable, linearSystem) ? 1 : 0);
                    }
                    else {
                        solverVariable3 = solverVariable;
                        solverVariable4 = solverVariable2;
                        n5 = n;
                        n6 = n2;
                        n7 = n3;
                        n8 = n4;
                        if (n != 0) {
                            break Label_0411;
                        }
                        solverVariable3 = solverVariable;
                        solverVariable4 = solverVariable2;
                        n5 = n;
                        n6 = n2;
                        n7 = n3;
                        n8 = n4;
                        if (this.isNew(variable, linearSystem)) {
                            n5 = 1;
                            solverVariable3 = variable;
                            solverVariable4 = solverVariable2;
                            n6 = n2;
                            n7 = variableValue;
                            n8 = n4;
                        }
                        break Label_0411;
                    }
                    solverVariable3 = variable;
                    solverVariable4 = solverVariable2;
                    n6 = n2;
                    n7 = variableValue;
                    n8 = n4;
                }
                else {
                    solverVariable3 = solverVariable;
                    solverVariable4 = solverVariable2;
                    n5 = n;
                    n6 = n2;
                    n7 = n3;
                    n8 = n4;
                    if (solverVariable == null) {
                        solverVariable3 = solverVariable;
                        solverVariable4 = solverVariable2;
                        n5 = n;
                        n6 = n2;
                        n7 = n3;
                        n8 = n4;
                        if (variableValue < 0.0f) {
                            boolean b;
                            if (solverVariable2 == null) {
                                b = this.isNew(variable, linearSystem);
                            }
                            else if (n4 > variableValue) {
                                b = this.isNew(variable, linearSystem);
                            }
                            else {
                                solverVariable3 = solverVariable;
                                solverVariable4 = solverVariable2;
                                n5 = n;
                                n6 = n2;
                                n7 = n3;
                                n8 = n4;
                                if (n2 != 0) {
                                    break Label_0411;
                                }
                                solverVariable3 = solverVariable;
                                solverVariable4 = solverVariable2;
                                n5 = n;
                                n6 = n2;
                                n7 = n3;
                                n8 = n4;
                                if (this.isNew(variable, linearSystem)) {
                                    n6 = 1;
                                    n8 = variableValue;
                                    n7 = n3;
                                    n5 = n;
                                    solverVariable4 = variable;
                                    solverVariable3 = solverVariable;
                                }
                                break Label_0411;
                            }
                            n6 = (b ? 1 : 0);
                            solverVariable3 = solverVariable;
                            solverVariable4 = variable;
                            n5 = n;
                            n7 = n3;
                            n8 = variableValue;
                        }
                    }
                }
            }
            ++i;
            solverVariable = solverVariable3;
            solverVariable2 = solverVariable4;
            n = n5;
            n2 = n6;
            n3 = n7;
            n4 = n8;
        }
        if (solverVariable != null) {
            return solverVariable;
        }
        return solverVariable2;
    }
    
    @Override
    public void clear() {
        this.variables.clear();
        this.variable = null;
        this.constantValue = 0.0f;
    }
    
    ArrayRow createRowCentering(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final float n2, final SolverVariable solverVariable3, final SolverVariable solverVariable4, final int n3) {
        if (solverVariable2 == solverVariable3) {
            this.variables.put(solverVariable, 1.0f);
            this.variables.put(solverVariable4, 1.0f);
            this.variables.put(solverVariable2, -2.0f);
            return this;
        }
        if (n2 == 0.5f) {
            this.variables.put(solverVariable, 1.0f);
            this.variables.put(solverVariable2, -1.0f);
            this.variables.put(solverVariable3, -1.0f);
            this.variables.put(solverVariable4, 1.0f);
            if (n > 0 || n3 > 0) {
                this.constantValue = (float)(-n + n3);
            }
        }
        else if (n2 <= 0.0f) {
            this.variables.put(solverVariable, -1.0f);
            this.variables.put(solverVariable2, 1.0f);
            this.constantValue = (float)n;
        }
        else if (n2 >= 1.0f) {
            this.variables.put(solverVariable4, -1.0f);
            this.variables.put(solverVariable3, 1.0f);
            this.constantValue = (float)(-n3);
        }
        else {
            final ArrayRowVariables variables = this.variables;
            final float n4 = 1.0f - n2;
            variables.put(solverVariable, n4 * 1.0f);
            this.variables.put(solverVariable2, n4 * -1.0f);
            this.variables.put(solverVariable3, -1.0f * n2);
            this.variables.put(solverVariable4, 1.0f * n2);
            if (n > 0 || n3 > 0) {
                this.constantValue = -n * n4 + n3 * n2;
            }
        }
        return this;
    }
    
    ArrayRow createRowDefinition(final SolverVariable variable, final int n) {
        this.variable = variable;
        final float n2 = (float)n;
        variable.computedValue = n2;
        this.constantValue = n2;
        this.isSimpleDefinition = true;
        return this;
    }
    
    ArrayRow createRowDimensionPercent(final SolverVariable solverVariable, final SolverVariable solverVariable2, final float n) {
        this.variables.put(solverVariable, -1.0f);
        this.variables.put(solverVariable2, n);
        return this;
    }
    
    public ArrayRow createRowDimensionRatio(final SolverVariable solverVariable, final SolverVariable solverVariable2, final SolverVariable solverVariable3, final SolverVariable solverVariable4, final float n) {
        this.variables.put(solverVariable, -1.0f);
        this.variables.put(solverVariable2, 1.0f);
        this.variables.put(solverVariable3, n);
        this.variables.put(solverVariable4, -n);
        return this;
    }
    
    public ArrayRow createRowEqualDimension(float n, final float n2, final float n3, final SolverVariable solverVariable, final int n4, final SolverVariable solverVariable2, final int n5, final SolverVariable solverVariable3, final int n6, final SolverVariable solverVariable4, final int n7) {
        if (n2 != 0.0f && n != n3) {
            n = n / n2 / (n3 / n2);
            this.constantValue = -n4 - n5 + n6 * n + n7 * n;
            this.variables.put(solverVariable, 1.0f);
            this.variables.put(solverVariable2, -1.0f);
            this.variables.put(solverVariable4, n);
            this.variables.put(solverVariable3, -n);
        }
        else {
            this.constantValue = (float)(-n4 - n5 + n6 + n7);
            this.variables.put(solverVariable, 1.0f);
            this.variables.put(solverVariable2, -1.0f);
            this.variables.put(solverVariable4, 1.0f);
            this.variables.put(solverVariable3, -1.0f);
        }
        return this;
    }
    
    public ArrayRow createRowEqualMatchDimensions(float n, final float n2, final float n3, final SolverVariable solverVariable, final SolverVariable solverVariable2, final SolverVariable solverVariable3, final SolverVariable solverVariable4) {
        this.constantValue = 0.0f;
        if (n2 != 0.0f && n != n3) {
            if (n == 0.0f) {
                this.variables.put(solverVariable, 1.0f);
                this.variables.put(solverVariable2, -1.0f);
            }
            else if (n3 == 0.0f) {
                this.variables.put(solverVariable3, 1.0f);
                this.variables.put(solverVariable4, -1.0f);
            }
            else {
                n = n / n2 / (n3 / n2);
                this.variables.put(solverVariable, 1.0f);
                this.variables.put(solverVariable2, -1.0f);
                this.variables.put(solverVariable4, n);
                this.variables.put(solverVariable3, -n);
            }
        }
        else {
            this.variables.put(solverVariable, 1.0f);
            this.variables.put(solverVariable2, -1.0f);
            this.variables.put(solverVariable4, 1.0f);
            this.variables.put(solverVariable3, -1.0f);
        }
        return this;
    }
    
    public ArrayRow createRowEquals(final SolverVariable solverVariable, final int n) {
        if (n < 0) {
            this.constantValue = (float)(n * -1);
            this.variables.put(solverVariable, 1.0f);
        }
        else {
            this.constantValue = (float)n;
            this.variables.put(solverVariable, -1.0f);
        }
        return this;
    }
    
    public ArrayRow createRowEquals(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n) {
        int n2 = 0;
        final int n3 = 0;
        if (n != 0) {
            n2 = n3;
            int n4;
            if ((n4 = n) < 0) {
                n4 = n * -1;
                n2 = 1;
            }
            this.constantValue = (float)n4;
        }
        if (n2 == 0) {
            this.variables.put(solverVariable, -1.0f);
            this.variables.put(solverVariable2, 1.0f);
        }
        else {
            this.variables.put(solverVariable, 1.0f);
            this.variables.put(solverVariable2, -1.0f);
        }
        return this;
    }
    
    public ArrayRow createRowGreaterThan(final SolverVariable solverVariable, final int n, final SolverVariable solverVariable2) {
        this.constantValue = (float)n;
        this.variables.put(solverVariable, -1.0f);
        return this;
    }
    
    public ArrayRow createRowGreaterThan(final SolverVariable solverVariable, final SolverVariable solverVariable2, final SolverVariable solverVariable3, final int n) {
        int n2 = 0;
        final int n3 = 0;
        if (n != 0) {
            n2 = n3;
            int n4;
            if ((n4 = n) < 0) {
                n4 = n * -1;
                n2 = 1;
            }
            this.constantValue = (float)n4;
        }
        if (n2 == 0) {
            this.variables.put(solverVariable, -1.0f);
            this.variables.put(solverVariable2, 1.0f);
            this.variables.put(solverVariable3, 1.0f);
        }
        else {
            this.variables.put(solverVariable, 1.0f);
            this.variables.put(solverVariable2, -1.0f);
            this.variables.put(solverVariable3, -1.0f);
        }
        return this;
    }
    
    public ArrayRow createRowLowerThan(final SolverVariable solverVariable, final SolverVariable solverVariable2, final SolverVariable solverVariable3, final int n) {
        int n2 = 0;
        final int n3 = 0;
        if (n != 0) {
            n2 = n3;
            int n4;
            if ((n4 = n) < 0) {
                n4 = n * -1;
                n2 = 1;
            }
            this.constantValue = (float)n4;
        }
        if (n2 == 0) {
            this.variables.put(solverVariable, -1.0f);
            this.variables.put(solverVariable2, 1.0f);
            this.variables.put(solverVariable3, -1.0f);
        }
        else {
            this.variables.put(solverVariable, 1.0f);
            this.variables.put(solverVariable2, -1.0f);
            this.variables.put(solverVariable3, 1.0f);
        }
        return this;
    }
    
    public ArrayRow createRowWithAngle(final SolverVariable solverVariable, final SolverVariable solverVariable2, final SolverVariable solverVariable3, final SolverVariable solverVariable4, final float n) {
        this.variables.put(solverVariable3, 0.5f);
        this.variables.put(solverVariable4, 0.5f);
        this.variables.put(solverVariable, -0.5f);
        this.variables.put(solverVariable2, -0.5f);
        this.constantValue = -n;
        return this;
    }
    
    void ensurePositiveConstant() {
        final float constantValue = this.constantValue;
        if (constantValue < 0.0f) {
            this.constantValue = constantValue * -1.0f;
            this.variables.invert();
        }
    }
    
    @Override
    public SolverVariable getKey() {
        return this.variable;
    }
    
    @Override
    public SolverVariable getPivotCandidate(final LinearSystem linearSystem, final boolean[] array) {
        return this.pickPivotInVariables(array, null);
    }
    
    boolean hasKeyVariable() {
        final SolverVariable variable = this.variable;
        return variable != null && (variable.mType == SolverVariable.Type.UNRESTRICTED || this.constantValue >= 0.0f);
    }
    
    boolean hasVariable(final SolverVariable solverVariable) {
        return this.variables.contains(solverVariable);
    }
    
    @Override
    public void initFromRow(final Row row) {
        if (row instanceof ArrayRow) {
            final ArrayRow arrayRow = (ArrayRow)row;
            this.variable = null;
            this.variables.clear();
            for (int i = 0; i < arrayRow.variables.getCurrentSize(); ++i) {
                this.variables.add(arrayRow.variables.getVariable(i), arrayRow.variables.getVariableValue(i), true);
            }
        }
    }
    
    @Override
    public boolean isEmpty() {
        return this.variable == null && this.constantValue == 0.0f && this.variables.getCurrentSize() == 0;
    }
    
    public SolverVariable pickPivot(final SolverVariable solverVariable) {
        return this.pickPivotInVariables(null, solverVariable);
    }
    
    void pivot(final SolverVariable variable) {
        final SolverVariable variable2 = this.variable;
        if (variable2 != null) {
            this.variables.put(variable2, -1.0f);
            this.variable.definitionId = -1;
            this.variable = null;
        }
        final float n = this.variables.remove(variable, true) * -1.0f;
        this.variable = variable;
        if (n == 1.0f) {
            return;
        }
        this.constantValue /= n;
        this.variables.divideByAmount(n);
    }
    
    public void reset() {
        this.variable = null;
        this.variables.clear();
        this.constantValue = 0.0f;
        this.isSimpleDefinition = false;
    }
    
    int sizeInBytes() {
        int n;
        if (this.variable != null) {
            n = 4;
        }
        else {
            n = 0;
        }
        return n + 4 + 4 + this.variables.sizeInBytes();
    }
    
    String toReadableString() {
        String str;
        if (this.variable == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append("0");
            str = sb.toString();
        }
        else {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("");
            sb2.append(this.variable);
            str = sb2.toString();
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(str);
        sb3.append(" = ");
        String str2 = sb3.toString();
        final float constantValue = this.constantValue;
        int i = 0;
        int n;
        if (constantValue != 0.0f) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append(str2);
            sb4.append(this.constantValue);
            str2 = sb4.toString();
            n = 1;
        }
        else {
            n = 0;
        }
        while (i < this.variables.getCurrentSize()) {
            final SolverVariable variable = this.variables.getVariable(i);
            if (variable != null) {
                final float variableValue = this.variables.getVariableValue(i);
                final float n2 = fcmpl(variableValue, 0.0f);
                if (n2 != 0) {
                    final String string = variable.toString();
                    String s = null;
                    float f = 0.0f;
                    Label_0364: {
                        if (n == 0) {
                            s = str2;
                            f = variableValue;
                            if (variableValue >= 0.0f) {
                                break Label_0364;
                            }
                            final StringBuilder sb5 = new StringBuilder();
                            sb5.append(str2);
                            sb5.append("- ");
                            s = sb5.toString();
                        }
                        else {
                            if (n2 > 0) {
                                final StringBuilder sb6 = new StringBuilder();
                                sb6.append(str2);
                                sb6.append(" + ");
                                s = sb6.toString();
                                f = variableValue;
                                break Label_0364;
                            }
                            final StringBuilder sb7 = new StringBuilder();
                            sb7.append(str2);
                            sb7.append(" - ");
                            s = sb7.toString();
                        }
                        f = variableValue * -1.0f;
                    }
                    if (f == 1.0f) {
                        final StringBuilder sb8 = new StringBuilder();
                        sb8.append(s);
                        sb8.append(string);
                        str2 = sb8.toString();
                    }
                    else {
                        final StringBuilder sb9 = new StringBuilder();
                        sb9.append(s);
                        sb9.append(f);
                        sb9.append(" ");
                        sb9.append(string);
                        str2 = sb9.toString();
                    }
                    n = 1;
                }
            }
            ++i;
        }
        String string2 = str2;
        if (n == 0) {
            final StringBuilder sb10 = new StringBuilder();
            sb10.append(str2);
            sb10.append("0.0");
            string2 = sb10.toString();
        }
        return string2;
    }
    
    @Override
    public String toString() {
        return this.toReadableString();
    }
    
    @Override
    public void updateFromFinalVariable(final LinearSystem linearSystem, final SolverVariable solverVariable, final boolean b) {
        if (solverVariable != null) {
            if (solverVariable.isFinalValue) {
                this.constantValue += solverVariable.computedValue * this.variables.get(solverVariable);
                this.variables.remove(solverVariable, b);
                if (b) {
                    solverVariable.removeFromRow(this);
                }
                if (LinearSystem.SIMPLIFY_SYNONYMS && this.variables.getCurrentSize() == 0) {
                    this.isSimpleDefinition = true;
                    linearSystem.hasSimpleDefinition = true;
                }
            }
        }
    }
    
    @Override
    public void updateFromRow(final LinearSystem linearSystem, final ArrayRow arrayRow, final boolean b) {
        this.constantValue += arrayRow.constantValue * this.variables.use(arrayRow, b);
        if (b) {
            arrayRow.variable.removeFromRow(this);
        }
        if (LinearSystem.SIMPLIFY_SYNONYMS && this.variable != null && this.variables.getCurrentSize() == 0) {
            this.isSimpleDefinition = true;
            linearSystem.hasSimpleDefinition = true;
        }
    }
    
    public void updateFromSynonymVariable(final LinearSystem linearSystem, final SolverVariable solverVariable, final boolean b) {
        if (solverVariable != null) {
            if (solverVariable.isSynonym) {
                final float value = this.variables.get(solverVariable);
                this.constantValue += solverVariable.synonymDelta * value;
                this.variables.remove(solverVariable, b);
                if (b) {
                    solverVariable.removeFromRow(this);
                }
                this.variables.add(linearSystem.mCache.mIndexedVariables[solverVariable.synonym], value, b);
                if (LinearSystem.SIMPLIFY_SYNONYMS && this.variables.getCurrentSize() == 0) {
                    this.isSimpleDefinition = true;
                    linearSystem.hasSimpleDefinition = true;
                }
            }
        }
    }
    
    @Override
    public void updateFromSystem(final LinearSystem linearSystem) {
        if (linearSystem.mRows.length == 0) {
            return;
        }
        int i = 0;
        while (i == 0) {
            for (int currentSize = this.variables.getCurrentSize(), j = 0; j < currentSize; ++j) {
                final SolverVariable variable = this.variables.getVariable(j);
                if (variable.definitionId != -1 || variable.isFinalValue || variable.isSynonym) {
                    this.variablesToUpdate.add(variable);
                }
            }
            final int size = this.variablesToUpdate.size();
            if (size > 0) {
                for (int k = 0; k < size; ++k) {
                    final SolverVariable solverVariable = this.variablesToUpdate.get(k);
                    if (solverVariable.isFinalValue) {
                        this.updateFromFinalVariable(linearSystem, solverVariable, true);
                    }
                    else if (solverVariable.isSynonym) {
                        this.updateFromSynonymVariable(linearSystem, solverVariable, true);
                    }
                    else {
                        this.updateFromRow(linearSystem, linearSystem.mRows[solverVariable.definitionId], true);
                    }
                }
                this.variablesToUpdate.clear();
            }
            else {
                i = 1;
            }
        }
        if (LinearSystem.SIMPLIFY_SYNONYMS && this.variable != null && this.variables.getCurrentSize() == 0) {
            this.isSimpleDefinition = true;
            linearSystem.hasSimpleDefinition = true;
        }
    }
    
    public interface ArrayRowVariables
    {
        void add(final SolverVariable p0, final float p1, final boolean p2);
        
        void clear();
        
        boolean contains(final SolverVariable p0);
        
        void display();
        
        void divideByAmount(final float p0);
        
        float get(final SolverVariable p0);
        
        int getCurrentSize();
        
        SolverVariable getVariable(final int p0);
        
        float getVariableValue(final int p0);
        
        int indexOf(final SolverVariable p0);
        
        void invert();
        
        void put(final SolverVariable p0, final float p1);
        
        float remove(final SolverVariable p0, final boolean p1);
        
        int sizeInBytes();
        
        float use(final ArrayRow p0, final boolean p1);
    }
}

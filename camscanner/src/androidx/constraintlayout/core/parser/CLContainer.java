// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.parser;

import java.util.Iterator;
import java.io.PrintStream;
import java.util.ArrayList;

public class CLContainer extends CLElement
{
    ArrayList<CLElement> mElements;
    
    public CLContainer(final char[] array) {
        super(array);
        this.mElements = new ArrayList<CLElement>();
    }
    
    public static CLElement allocate(final char[] array) {
        return new CLContainer(array);
    }
    
    public void add(final CLElement clElement) {
        this.mElements.add(clElement);
        if (CLParser.DEBUG) {
            final PrintStream out = System.out;
            final StringBuilder sb = new StringBuilder();
            sb.append("added element ");
            sb.append(clElement);
            sb.append(" to ");
            sb.append(this);
            out.println(sb.toString());
        }
    }
    
    public CLElement get(final int n) throws CLParsingException {
        if (n >= 0 && n < this.mElements.size()) {
            return this.mElements.get(n);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("no element at index ");
        sb.append(n);
        throw new CLParsingException(sb.toString(), this);
    }
    
    public CLElement get(final String s) throws CLParsingException {
        for (final CLKey clKey : this.mElements) {
            if (clKey.content().equals(s)) {
                return clKey.getValue();
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("no element for key <");
        sb.append(s);
        sb.append(">");
        throw new CLParsingException(sb.toString(), this);
    }
    
    public CLArray getArray(final int i) throws CLParsingException {
        final CLElement value = this.get(i);
        if (value instanceof CLArray) {
            return (CLArray)value;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("no array at index ");
        sb.append(i);
        throw new CLParsingException(sb.toString(), this);
    }
    
    public CLArray getArray(final String str) throws CLParsingException {
        final CLElement value = this.get(str);
        if (value instanceof CLArray) {
            return (CLArray)value;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("no array found for key <");
        sb.append(str);
        sb.append(">, found [");
        sb.append(value.getStrClass());
        sb.append("] : ");
        sb.append(value);
        throw new CLParsingException(sb.toString(), this);
    }
    
    public CLArray getArrayOrNull(final String s) {
        final CLElement orNull = this.getOrNull(s);
        if (orNull instanceof CLArray) {
            return (CLArray)orNull;
        }
        return null;
    }
    
    public boolean getBoolean(final int i) throws CLParsingException {
        final CLElement value = this.get(i);
        if (value instanceof CLToken) {
            return ((CLToken)value).getBoolean();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("no boolean at index ");
        sb.append(i);
        throw new CLParsingException(sb.toString(), this);
    }
    
    public boolean getBoolean(final String str) throws CLParsingException {
        final CLElement value = this.get(str);
        if (value instanceof CLToken) {
            return ((CLToken)value).getBoolean();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("no boolean found for key <");
        sb.append(str);
        sb.append(">, found [");
        sb.append(value.getStrClass());
        sb.append("] : ");
        sb.append(value);
        throw new CLParsingException(sb.toString(), this);
    }
    
    public float getFloat(final int i) throws CLParsingException {
        final CLElement value = this.get(i);
        if (value != null) {
            return value.getFloat();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("no float at index ");
        sb.append(i);
        throw new CLParsingException(sb.toString(), this);
    }
    
    public float getFloat(final String str) throws CLParsingException {
        final CLElement value = this.get(str);
        if (value != null) {
            return value.getFloat();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("no float found for key <");
        sb.append(str);
        sb.append(">, found [");
        sb.append(value.getStrClass());
        sb.append("] : ");
        sb.append(value);
        throw new CLParsingException(sb.toString(), this);
    }
    
    public float getFloatOrNaN(final String s) {
        final CLElement orNull = this.getOrNull(s);
        if (orNull instanceof CLNumber) {
            return orNull.getFloat();
        }
        return Float.NaN;
    }
    
    public int getInt(final int i) throws CLParsingException {
        final CLElement value = this.get(i);
        if (value != null) {
            return value.getInt();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("no int at index ");
        sb.append(i);
        throw new CLParsingException(sb.toString(), this);
    }
    
    public int getInt(final String str) throws CLParsingException {
        final CLElement value = this.get(str);
        if (value != null) {
            return value.getInt();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("no int found for key <");
        sb.append(str);
        sb.append(">, found [");
        sb.append(value.getStrClass());
        sb.append("] : ");
        sb.append(value);
        throw new CLParsingException(sb.toString(), this);
    }
    
    public CLObject getObject(final int i) throws CLParsingException {
        final CLElement value = this.get(i);
        if (value instanceof CLObject) {
            return (CLObject)value;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("no object at index ");
        sb.append(i);
        throw new CLParsingException(sb.toString(), this);
    }
    
    public CLObject getObject(final String str) throws CLParsingException {
        final CLElement value = this.get(str);
        if (value instanceof CLObject) {
            return (CLObject)value;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("no object found for key <");
        sb.append(str);
        sb.append(">, found [");
        sb.append(value.getStrClass());
        sb.append("] : ");
        sb.append(value);
        throw new CLParsingException(sb.toString(), this);
    }
    
    public CLObject getObjectOrNull(final String s) {
        final CLElement orNull = this.getOrNull(s);
        if (orNull instanceof CLObject) {
            return (CLObject)orNull;
        }
        return null;
    }
    
    public CLElement getOrNull(final int index) {
        if (index >= 0 && index < this.mElements.size()) {
            return this.mElements.get(index);
        }
        return null;
    }
    
    public CLElement getOrNull(final String anObject) {
        for (final CLKey clKey : this.mElements) {
            if (clKey.content().equals(anObject)) {
                return clKey.getValue();
            }
        }
        return null;
    }
    
    public String getString(final int i) throws CLParsingException {
        final CLElement value = this.get(i);
        if (value instanceof CLString) {
            return value.content();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("no string at index ");
        sb.append(i);
        throw new CLParsingException(sb.toString(), this);
    }
    
    public String getString(final String str) throws CLParsingException {
        final CLElement value = this.get(str);
        if (value instanceof CLString) {
            return value.content();
        }
        String strClass;
        if (value != null) {
            strClass = value.getStrClass();
        }
        else {
            strClass = null;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("no string found for key <");
        sb.append(str);
        sb.append(">, found [");
        sb.append(strClass);
        sb.append("] : ");
        sb.append(value);
        throw new CLParsingException(sb.toString(), this);
    }
    
    public String getStringOrNull(final int n) {
        final CLElement orNull = this.getOrNull(n);
        if (orNull instanceof CLString) {
            return orNull.content();
        }
        return null;
    }
    
    public String getStringOrNull(final String s) {
        final CLElement orNull = this.getOrNull(s);
        if (orNull instanceof CLString) {
            return orNull.content();
        }
        return null;
    }
    
    public boolean has(final String anObject) {
        for (final CLElement clElement : this.mElements) {
            if (clElement instanceof CLKey && ((CLKey)clElement).content().equals(anObject)) {
                return true;
            }
        }
        return false;
    }
    
    public ArrayList<String> names() {
        final ArrayList list = new ArrayList();
        for (final CLElement clElement : this.mElements) {
            if (clElement instanceof CLKey) {
                list.add(((CLKey)clElement).content());
            }
        }
        return list;
    }
    
    public void put(final String anObject, final CLElement clElement) {
        for (final CLKey clKey : this.mElements) {
            if (clKey.content().equals(anObject)) {
                clKey.set(clElement);
                return;
            }
        }
        this.mElements.add(CLKey.allocate(anObject, clElement));
    }
    
    public void putNumber(final String s, final float n) {
        this.put(s, new CLNumber(n));
    }
    
    public void remove(final String anObject) {
        final ArrayList list = new ArrayList();
        for (final CLElement e : this.mElements) {
            if (((CLKey)e).content().equals(anObject)) {
                list.add(e);
            }
        }
        final Iterator iterator2 = list.iterator();
        while (iterator2.hasNext()) {
            this.mElements.remove(iterator2.next());
        }
    }
    
    public int size() {
        return this.mElements.size();
    }
    
    @Override
    public String toString() {
        final StringBuilder obj = new StringBuilder();
        for (final CLElement obj2 : this.mElements) {
            if (obj.length() > 0) {
                obj.append("; ");
            }
            obj.append(obj2);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append(" = <");
        sb.append((Object)obj);
        sb.append(" >");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.parser;

import java.io.PrintStream;

public class CLParser
{
    static boolean DEBUG = false;
    private boolean hasComment;
    private int lineNumber;
    private String mContent;
    
    public CLParser(final String mContent) {
        this.hasComment = false;
        this.mContent = mContent;
    }
    
    private CLElement createElement(final CLElement clElement, int n, final TYPE obj, final boolean b, final char[] array) {
        if (CLParser.DEBUG) {
            final PrintStream out = System.out;
            final StringBuilder sb = new StringBuilder();
            sb.append("CREATE ");
            sb.append(obj);
            sb.append(" at ");
            sb.append(array[n]);
            out.println(sb.toString());
        }
        CLElement clElement2 = null;
        Label_0167: {
            switch (CLParser$1.$SwitchMap$androidx$constraintlayout$core$parser$CLParser$TYPE[obj.ordinal()]) {
                default: {
                    clElement2 = null;
                    break Label_0167;
                }
                case 6: {
                    clElement2 = CLToken.allocate(array);
                    break Label_0167;
                }
                case 5: {
                    clElement2 = CLKey.allocate(array);
                    break Label_0167;
                }
                case 4: {
                    clElement2 = CLNumber.allocate(array);
                    break Label_0167;
                }
                case 3: {
                    clElement2 = CLString.allocate(array);
                    break Label_0167;
                }
                case 2: {
                    clElement2 = CLArray.allocate(array);
                    break;
                }
                case 1: {
                    clElement2 = CLObject.allocate(array);
                    break;
                }
            }
            ++n;
        }
        if (clElement2 == null) {
            return null;
        }
        clElement2.setLine(this.lineNumber);
        if (b) {
            clElement2.setStart(n);
        }
        if (clElement instanceof CLContainer) {
            clElement2.setContainer((CLContainer)clElement);
        }
        return clElement2;
    }
    
    private CLElement getNextJsonElement(int n, final char c, final CLElement clElement, final char[] array) throws CLParsingException {
        CLElement clElement2 = clElement;
        if (c != '\t') {
            clElement2 = clElement;
            if (c != '\n') {
                clElement2 = clElement;
                if (c != '\r') {
                    clElement2 = clElement;
                    if (c != ' ') {
                        if (c != '\"' && c != '\'') {
                            if (c != '[') {
                                if (c != ']') {
                                    if (c == '{') {
                                        clElement2 = this.createElement(clElement, n, TYPE.OBJECT, true, array);
                                        return clElement2;
                                    }
                                    if (c != '}') {
                                        clElement2 = clElement;
                                        switch (c) {
                                            default: {
                                                if (!(clElement instanceof CLContainer) || clElement instanceof CLObject) {
                                                    clElement2 = this.createElement(clElement, n, TYPE.KEY, true, array);
                                                    return clElement2;
                                                }
                                                clElement2 = this.createElement(clElement, n, TYPE.TOKEN, true, array);
                                                final CLToken clToken = (CLToken)clElement2;
                                                if (clToken.validate(c, n)) {
                                                    return clElement2;
                                                }
                                                final StringBuilder sb = new StringBuilder();
                                                sb.append("incorrect token <");
                                                sb.append(c);
                                                sb.append("> at line ");
                                                sb.append(this.lineNumber);
                                                throw new CLParsingException(sb.toString(), clToken);
                                            }
                                            case '/': {
                                                ++n;
                                                clElement2 = clElement;
                                                if (n >= array.length) {
                                                    return clElement2;
                                                }
                                                clElement2 = clElement;
                                                if (array[n] == '/') {
                                                    this.hasComment = true;
                                                    clElement2 = clElement;
                                                    return clElement2;
                                                }
                                                return clElement2;
                                            }
                                            case '+':
                                            case '-':
                                            case '.':
                                            case '0':
                                            case '1':
                                            case '2':
                                            case '3':
                                            case '4':
                                            case '5':
                                            case '6':
                                            case '7':
                                            case '8':
                                            case '9': {
                                                clElement2 = this.createElement(clElement, n, TYPE.NUMBER, true, array);
                                            }
                                            case ',':
                                            case ':': {
                                                return clElement2;
                                            }
                                        }
                                    }
                                }
                                clElement.setEnd(n - 1);
                                clElement2 = clElement.getContainer();
                                clElement2.setEnd(n);
                            }
                            else {
                                clElement2 = this.createElement(clElement, n, TYPE.ARRAY, true, array);
                            }
                        }
                        else if (clElement instanceof CLObject) {
                            clElement2 = this.createElement(clElement, n, TYPE.KEY, true, array);
                        }
                        else {
                            clElement2 = this.createElement(clElement, n, TYPE.STRING, true, array);
                        }
                    }
                }
            }
        }
        return clElement2;
    }
    
    public static CLObject parse(final String s) throws CLParsingException {
        return new CLParser(s).parse();
    }
    
    public CLObject parse() throws CLParsingException {
        final char[] charArray = this.mContent.toCharArray();
        final int length = charArray.length;
        this.lineNumber = 1;
        int i = 0;
        while (true) {
            while (i < length) {
                final char c = charArray[i];
                if (c == '{') {
                    if (i != -1) {
                        final CLObject allocate = CLObject.allocate(charArray);
                        allocate.setLine(this.lineNumber);
                        allocate.setStart(i);
                        ++i;
                        CLToken clToken = (CLToken)allocate;
                        CLElement container;
                        while (true) {
                            container = clToken;
                            if (i >= length) {
                                break;
                            }
                            final char c2 = charArray[i];
                            if (c2 == '\n') {
                                ++this.lineNumber;
                            }
                            CLElement container2 = null;
                            Label_0670: {
                                if (this.hasComment) {
                                    container2 = clToken;
                                    if (c2 != '\n') {
                                        break Label_0670;
                                    }
                                    this.hasComment = false;
                                }
                                if (clToken == null) {
                                    container = clToken;
                                    break;
                                }
                                CLElement clElement = null;
                                Label_0625: {
                                    if (clToken.isDone()) {
                                        clElement = this.getNextJsonElement(i, c2, clToken, charArray);
                                    }
                                    else if (clToken instanceof CLObject) {
                                        if (c2 == '}') {
                                            clToken.setEnd(i - 1);
                                            clElement = clToken;
                                        }
                                        else {
                                            clElement = this.getNextJsonElement(i, c2, clToken, charArray);
                                        }
                                    }
                                    else if (clToken instanceof CLArray) {
                                        if (c2 == ']') {
                                            clToken.setEnd(i - 1);
                                            clElement = clToken;
                                        }
                                        else {
                                            clElement = this.getNextJsonElement(i, c2, clToken, charArray);
                                        }
                                    }
                                    else {
                                        final boolean b = clToken instanceof CLString;
                                        if (b) {
                                            final long start = clToken.start;
                                            clElement = clToken;
                                            if (charArray[(int)start] == c2) {
                                                clToken.setStart(start + 1L);
                                                clToken.setEnd(i - 1);
                                                clElement = clToken;
                                            }
                                        }
                                        else {
                                            if (clToken instanceof CLToken) {
                                                final CLToken clToken2 = clToken;
                                                if (!clToken2.validate(c2, i)) {
                                                    final StringBuilder sb = new StringBuilder();
                                                    sb.append("parsing incorrect token ");
                                                    sb.append(clToken2.content());
                                                    sb.append(" at line ");
                                                    sb.append(this.lineNumber);
                                                    throw new CLParsingException(sb.toString(), clToken2);
                                                }
                                            }
                                            if (clToken instanceof CLKey || b) {
                                                final long start2 = clToken.start;
                                                final char c3 = charArray[(int)start2];
                                                if ((c3 == '\'' || c3 == '\"') && c3 == c2) {
                                                    clToken.setStart(start2 + 1L);
                                                    clToken.setEnd(i - 1);
                                                }
                                            }
                                            clElement = clToken;
                                            if (!clToken.isDone()) {
                                                if (c2 != '}' && c2 != ']' && c2 != ',' && c2 != ' ' && c2 != '\t' && c2 != '\r' && c2 != '\n') {
                                                    clElement = clToken;
                                                    if (c2 != ':') {
                                                        break Label_0625;
                                                    }
                                                }
                                                final long end = i - 1;
                                                clToken.setEnd(end);
                                                if (c2 != '}') {
                                                    clElement = clToken;
                                                    if (c2 != ']') {
                                                        break Label_0625;
                                                    }
                                                }
                                                final CLElement container3 = clToken.getContainer();
                                                container3.setEnd(end);
                                                clElement = container3;
                                                if (container3 instanceof CLKey) {
                                                    clElement = container3.getContainer();
                                                    clElement.setEnd(end);
                                                }
                                            }
                                        }
                                    }
                                }
                                container2 = clElement;
                                if (clElement.isDone()) {
                                    if (clElement instanceof CLKey) {
                                        container2 = clElement;
                                        if (((CLKey)clElement).mElements.size() <= 0) {
                                            break Label_0670;
                                        }
                                    }
                                    container2 = clElement.getContainer();
                                }
                            }
                            ++i;
                            clToken = (CLToken)container2;
                        }
                        while (container != null && !container.isDone()) {
                            if (container instanceof CLString) {
                                container.setStart((int)container.start + 1);
                            }
                            container.setEnd(length - 1);
                            container = container.getContainer();
                        }
                        if (CLParser.DEBUG) {
                            final PrintStream out = System.out;
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Root: ");
                            sb2.append(allocate.toJSON());
                            out.println(sb2.toString());
                        }
                        return allocate;
                    }
                    throw new CLParsingException("invalid json content", (CLElement)null);
                }
                else {
                    if (c == '\n') {
                        ++this.lineNumber;
                    }
                    ++i;
                }
            }
            i = -1;
            continue;
        }
    }
    
    enum TYPE
    {
        private static final TYPE[] $VALUES;
        
        ARRAY, 
        KEY, 
        NUMBER, 
        OBJECT, 
        STRING, 
        TOKEN, 
        UNKNOWN;
    }
}

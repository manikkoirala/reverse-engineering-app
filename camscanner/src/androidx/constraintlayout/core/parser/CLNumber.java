// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.core.parser;

public class CLNumber extends CLElement
{
    float value;
    
    public CLNumber(final float value) {
        super(null);
        this.value = value;
    }
    
    public CLNumber(final char[] array) {
        super(array);
        this.value = Float.NaN;
    }
    
    public static CLElement allocate(final char[] array) {
        return new CLNumber(array);
    }
    
    @Override
    public float getFloat() {
        if (Float.isNaN(this.value)) {
            this.value = Float.parseFloat(this.content());
        }
        return this.value;
    }
    
    @Override
    public int getInt() {
        if (Float.isNaN(this.value)) {
            this.value = (float)Integer.parseInt(this.content());
        }
        return (int)this.value;
    }
    
    public boolean isInt() {
        final float float1 = this.getFloat();
        return (int)float1 == float1;
    }
    
    public void putValue(final float value) {
        this.value = value;
    }
    
    @Override
    protected String toFormattedJSON(int i, final int n) {
        final StringBuilder sb = new StringBuilder();
        this.addIndent(sb, i);
        final float float1 = this.getFloat();
        i = (int)float1;
        if (i == float1) {
            sb.append(i);
        }
        else {
            sb.append(float1);
        }
        return sb.toString();
    }
    
    @Override
    protected String toJSON() {
        final float float1 = this.getFloat();
        final int i = (int)float1;
        if (i == float1) {
            final StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(i);
            return sb.toString();
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("");
        sb2.append(float1);
        return sb2.toString();
    }
}

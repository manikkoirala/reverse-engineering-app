// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.widget;

import android.view.ViewGroup$LayoutParams;
import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.content.res.TypedArray;
import androidx.constraintlayout.motion.widget.MotionLayout;
import android.util.AttributeSet;
import android.content.Context;
import android.view.View;

public class ReactiveGuide extends View implements SharedValuesListener
{
    private boolean mAnimateChange;
    private boolean mApplyToAllConstraintSets;
    private int mApplyToConstraintSetId;
    private int mAttributeId;
    
    public ReactiveGuide(final Context context) {
        super(context);
        this.mAttributeId = -1;
        this.mAnimateChange = false;
        this.mApplyToConstraintSetId = 0;
        this.mApplyToAllConstraintSets = true;
        super.setVisibility(8);
        this.init(null);
    }
    
    public ReactiveGuide(final Context context, final AttributeSet set) {
        super(context, set);
        this.mAttributeId = -1;
        this.mAnimateChange = false;
        this.mApplyToConstraintSetId = 0;
        this.mApplyToAllConstraintSets = true;
        super.setVisibility(8);
        this.init(set);
    }
    
    public ReactiveGuide(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mAttributeId = -1;
        this.mAnimateChange = false;
        this.mApplyToConstraintSetId = 0;
        this.mApplyToAllConstraintSets = true;
        super.setVisibility(8);
        this.init(set);
    }
    
    public ReactiveGuide(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n);
        this.mAttributeId = -1;
        this.mAnimateChange = false;
        this.mApplyToConstraintSetId = 0;
        this.mApplyToAllConstraintSets = true;
        super.setVisibility(8);
        this.init(set);
    }
    
    private void changeValue(final int n, final int n2, final MotionLayout motionLayout, final int n3) {
        final ConstraintSet constraintSet = motionLayout.getConstraintSet(n3);
        constraintSet.setGuidelineEnd(n2, n);
        motionLayout.updateState(n3, constraintSet);
    }
    
    private void init(final AttributeSet set) {
        if (set != null) {
            final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, R.styleable.ConstraintLayout_ReactiveGuide);
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == R.styleable.ConstraintLayout_ReactiveGuide_reactiveGuide_valueId) {
                    this.mAttributeId = obtainStyledAttributes.getResourceId(index, this.mAttributeId);
                }
                else if (index == R.styleable.ConstraintLayout_ReactiveGuide_reactiveGuide_animateChange) {
                    this.mAnimateChange = obtainStyledAttributes.getBoolean(index, this.mAnimateChange);
                }
                else if (index == R.styleable.ConstraintLayout_ReactiveGuide_reactiveGuide_applyToConstraintSet) {
                    this.mApplyToConstraintSetId = obtainStyledAttributes.getResourceId(index, this.mApplyToConstraintSetId);
                }
                else if (index == R.styleable.ConstraintLayout_ReactiveGuide_reactiveGuide_applyToAllConstraintSets) {
                    this.mApplyToAllConstraintSets = obtainStyledAttributes.getBoolean(index, this.mApplyToAllConstraintSets);
                }
            }
            obtainStyledAttributes.recycle();
        }
        if (this.mAttributeId != -1) {
            ConstraintLayout.getSharedValues().addListener(this.mAttributeId, (SharedValues.SharedValuesListener)this);
        }
    }
    
    @SuppressLint({ "MissingSuperCall" })
    public void draw(final Canvas canvas) {
    }
    
    public int getApplyToConstraintSetId() {
        return this.mApplyToConstraintSetId;
    }
    
    public int getAttributeId() {
        return this.mAttributeId;
    }
    
    public boolean isAnimatingChange() {
        return this.mAnimateChange;
    }
    
    protected void onMeasure(final int n, final int n2) {
        this.setMeasuredDimension(0, 0);
    }
    
    public void onNewValue(int i, final int guidelineBegin, int j) {
        this.setGuidelineBegin(guidelineBegin);
        final int id = this.getId();
        if (id <= 0) {
            return;
        }
        if (this.getParent() instanceof MotionLayout) {
            final MotionLayout motionLayout = (MotionLayout)this.getParent();
            i = motionLayout.getCurrentState();
            j = this.mApplyToConstraintSetId;
            if (j != 0) {
                i = j;
            }
            final boolean mAnimateChange = this.mAnimateChange;
            j = 0;
            final int n = 0;
            if (mAnimateChange) {
                if (this.mApplyToAllConstraintSets) {
                    int[] constraintSetIds;
                    int n2;
                    for (constraintSetIds = motionLayout.getConstraintSetIds(), j = n; j < constraintSetIds.length; ++j) {
                        n2 = constraintSetIds[j];
                        if (n2 != i) {
                            this.changeValue(guidelineBegin, id, motionLayout, n2);
                        }
                    }
                }
                final ConstraintSet cloneConstraintSet = motionLayout.cloneConstraintSet(i);
                cloneConstraintSet.setGuidelineEnd(id, guidelineBegin);
                motionLayout.updateStateAnimate(i, cloneConstraintSet, 1000);
            }
            else if (this.mApplyToAllConstraintSets) {
                int[] constraintSetIds2;
                for (constraintSetIds2 = motionLayout.getConstraintSetIds(), i = j; i < constraintSetIds2.length; ++i) {
                    this.changeValue(guidelineBegin, id, motionLayout, constraintSetIds2[i]);
                }
            }
            else {
                this.changeValue(guidelineBegin, id, motionLayout, i);
            }
        }
    }
    
    public void setAnimateChange(final boolean mAnimateChange) {
        this.mAnimateChange = mAnimateChange;
    }
    
    public void setApplyToConstraintSetId(final int mApplyToConstraintSetId) {
        this.mApplyToConstraintSetId = mApplyToConstraintSetId;
    }
    
    public void setAttributeId(final int mAttributeId) {
        final SharedValues sharedValues = ConstraintLayout.getSharedValues();
        final int mAttributeId2 = this.mAttributeId;
        if (mAttributeId2 != -1) {
            sharedValues.removeListener(mAttributeId2, (SharedValues.SharedValuesListener)this);
        }
        if ((this.mAttributeId = mAttributeId) != -1) {
            sharedValues.addListener(mAttributeId, (SharedValues.SharedValuesListener)this);
        }
    }
    
    public void setGuidelineBegin(final int guideBegin) {
        final ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams)this.getLayoutParams();
        layoutParams.guideBegin = guideBegin;
        this.setLayoutParams((ViewGroup$LayoutParams)layoutParams);
    }
    
    public void setGuidelineEnd(final int guideEnd) {
        final ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams)this.getLayoutParams();
        layoutParams.guideEnd = guideEnd;
        this.setLayoutParams((ViewGroup$LayoutParams)layoutParams);
    }
    
    public void setGuidelinePercent(final float guidePercent) {
        final ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams)this.getLayoutParams();
        layoutParams.guidePercent = guidePercent;
        this.setLayoutParams((ViewGroup$LayoutParams)layoutParams);
    }
    
    public void setVisibility(final int n) {
    }
}
